﻿namespace TrackControl.Reports
{
    partial class FormInterval
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if( disposing && ( components != null ) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControlInterval = new DevExpress.XtraEditors.PanelControl();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.checkEdit23 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit22 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit21 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit20 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit19 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit12 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit18 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit17 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit16 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit15 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit14 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit31 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit30 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit29 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit28 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit27 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit26 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit25 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit24 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit13 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit11 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit10 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit9 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit8 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit7 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit6 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit5 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit4 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit3 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit2 = new DevExpress.XtraEditors.CheckEdit();
            this.checkEdit1 = new DevExpress.XtraEditors.CheckEdit();
            this.ButtonSelectMonth = new DevExpress.XtraEditors.SimpleButton();
            this.ButtonOdd = new DevExpress.XtraEditors.SimpleButton();
            this.ButtonNoOdd = new DevExpress.XtraEditors.SimpleButton();
            this.ButtonMonthClear = new DevExpress.XtraEditors.SimpleButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.ButtonSelectDay = new DevExpress.XtraEditors.SimpleButton();
            this.ButtonDayClear = new DevExpress.XtraEditors.SimpleButton();
            this.weekDaysCheckEdit1 = new DevExpress.XtraScheduler.UI.WeekDaysCheckEdit();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.spinEditMinute2 = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.spinEditMinute1 = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.spinEditHours2 = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.spinEditHours1 = new DevExpress.XtraEditors.SpinEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.ButtonOK = new DevExpress.XtraEditors.SimpleButton();
            this.ButtonCancel = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.panelControlInterval)).BeginInit();
            this.panelControlInterval.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit23.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit22.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit21.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit20.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit19.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit12.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit18.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit17.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit16.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit15.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit14.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit31.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit30.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit29.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit28.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit27.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit26.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit25.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit24.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit13.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit11.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.weekDaysCheckEdit1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditMinute2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditMinute1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditHours2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditHours1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControlInterval
            // 
            this.panelControlInterval.Controls.Add(this.groupBox3);
            this.panelControlInterval.Controls.Add(this.groupBox2);
            this.panelControlInterval.Controls.Add(this.groupBox1);
            this.panelControlInterval.Controls.Add(this.ButtonOK);
            this.panelControlInterval.Controls.Add(this.ButtonCancel);
            this.panelControlInterval.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControlInterval.Location = new System.Drawing.Point(0, 0);
            this.panelControlInterval.Name = "panelControlInterval";
            this.panelControlInterval.Size = new System.Drawing.Size(459, 335);
            this.panelControlInterval.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.checkEdit23);
            this.groupBox3.Controls.Add(this.checkEdit22);
            this.groupBox3.Controls.Add(this.checkEdit21);
            this.groupBox3.Controls.Add(this.checkEdit20);
            this.groupBox3.Controls.Add(this.checkEdit19);
            this.groupBox3.Controls.Add(this.checkEdit12);
            this.groupBox3.Controls.Add(this.checkEdit18);
            this.groupBox3.Controls.Add(this.checkEdit17);
            this.groupBox3.Controls.Add(this.checkEdit16);
            this.groupBox3.Controls.Add(this.checkEdit15);
            this.groupBox3.Controls.Add(this.checkEdit14);
            this.groupBox3.Controls.Add(this.checkEdit31);
            this.groupBox3.Controls.Add(this.checkEdit30);
            this.groupBox3.Controls.Add(this.checkEdit29);
            this.groupBox3.Controls.Add(this.checkEdit28);
            this.groupBox3.Controls.Add(this.checkEdit27);
            this.groupBox3.Controls.Add(this.checkEdit26);
            this.groupBox3.Controls.Add(this.checkEdit25);
            this.groupBox3.Controls.Add(this.checkEdit24);
            this.groupBox3.Controls.Add(this.checkEdit13);
            this.groupBox3.Controls.Add(this.checkEdit11);
            this.groupBox3.Controls.Add(this.checkEdit10);
            this.groupBox3.Controls.Add(this.checkEdit9);
            this.groupBox3.Controls.Add(this.checkEdit8);
            this.groupBox3.Controls.Add(this.checkEdit7);
            this.groupBox3.Controls.Add(this.checkEdit6);
            this.groupBox3.Controls.Add(this.checkEdit5);
            this.groupBox3.Controls.Add(this.checkEdit4);
            this.groupBox3.Controls.Add(this.checkEdit3);
            this.groupBox3.Controls.Add(this.checkEdit2);
            this.groupBox3.Controls.Add(this.checkEdit1);
            this.groupBox3.Controls.Add(this.ButtonSelectMonth);
            this.groupBox3.Controls.Add(this.ButtonOdd);
            this.groupBox3.Controls.Add(this.ButtonNoOdd);
            this.groupBox3.Controls.Add(this.ButtonMonthClear);
            this.groupBox3.Location = new System.Drawing.Point(9, 159);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(442, 135);
            this.groupBox3.TabIndex = 4;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Дни месяца";
            // 
            // checkEdit23
            // 
            this.checkEdit23.Location = new System.Drawing.Point(6, 73);
            this.checkEdit23.Name = "checkEdit23";
            this.checkEdit23.Properties.Caption = "23";
            this.checkEdit23.Size = new System.Drawing.Size(40, 19);
            this.checkEdit23.TabIndex = 57;
            // 
            // checkEdit22
            // 
            this.checkEdit22.Location = new System.Drawing.Point(406, 46);
            this.checkEdit22.Name = "checkEdit22";
            this.checkEdit22.Properties.Caption = "22";
            this.checkEdit22.Size = new System.Drawing.Size(40, 19);
            this.checkEdit22.TabIndex = 56;
            // 
            // checkEdit21
            // 
            this.checkEdit21.Location = new System.Drawing.Point(366, 46);
            this.checkEdit21.Name = "checkEdit21";
            this.checkEdit21.Properties.Caption = "21";
            this.checkEdit21.Size = new System.Drawing.Size(40, 19);
            this.checkEdit21.TabIndex = 55;
            // 
            // checkEdit20
            // 
            this.checkEdit20.Location = new System.Drawing.Point(326, 46);
            this.checkEdit20.Name = "checkEdit20";
            this.checkEdit20.Properties.Caption = "20";
            this.checkEdit20.Size = new System.Drawing.Size(40, 19);
            this.checkEdit20.TabIndex = 54;
            // 
            // checkEdit19
            // 
            this.checkEdit19.Location = new System.Drawing.Point(286, 46);
            this.checkEdit19.Name = "checkEdit19";
            this.checkEdit19.Properties.Caption = "19";
            this.checkEdit19.Size = new System.Drawing.Size(40, 19);
            this.checkEdit19.TabIndex = 53;
            // 
            // checkEdit12
            // 
            this.checkEdit12.Location = new System.Drawing.Point(6, 46);
            this.checkEdit12.Name = "checkEdit12";
            this.checkEdit12.Properties.Caption = "12";
            this.checkEdit12.Size = new System.Drawing.Size(40, 19);
            this.checkEdit12.TabIndex = 47;
            // 
            // checkEdit18
            // 
            this.checkEdit18.Location = new System.Drawing.Point(246, 46);
            this.checkEdit18.Name = "checkEdit18";
            this.checkEdit18.Properties.Caption = "18";
            this.checkEdit18.Size = new System.Drawing.Size(40, 19);
            this.checkEdit18.TabIndex = 5;
            // 
            // checkEdit17
            // 
            this.checkEdit17.Location = new System.Drawing.Point(206, 46);
            this.checkEdit17.Name = "checkEdit17";
            this.checkEdit17.Properties.Caption = "17";
            this.checkEdit17.Size = new System.Drawing.Size(40, 19);
            this.checkEdit17.TabIndex = 52;
            // 
            // checkEdit16
            // 
            this.checkEdit16.Location = new System.Drawing.Point(166, 46);
            this.checkEdit16.Name = "checkEdit16";
            this.checkEdit16.Properties.Caption = "16";
            this.checkEdit16.Size = new System.Drawing.Size(40, 19);
            this.checkEdit16.TabIndex = 51;
            // 
            // checkEdit15
            // 
            this.checkEdit15.Location = new System.Drawing.Point(126, 46);
            this.checkEdit15.Name = "checkEdit15";
            this.checkEdit15.Properties.Caption = "15";
            this.checkEdit15.Size = new System.Drawing.Size(40, 19);
            this.checkEdit15.TabIndex = 50;
            // 
            // checkEdit14
            // 
            this.checkEdit14.Location = new System.Drawing.Point(86, 46);
            this.checkEdit14.Name = "checkEdit14";
            this.checkEdit14.Properties.Caption = "14";
            this.checkEdit14.Size = new System.Drawing.Size(40, 19);
            this.checkEdit14.TabIndex = 49;
            // 
            // checkEdit31
            // 
            this.checkEdit31.Location = new System.Drawing.Point(326, 73);
            this.checkEdit31.Name = "checkEdit31";
            this.checkEdit31.Properties.Caption = "31";
            this.checkEdit31.Size = new System.Drawing.Size(40, 19);
            this.checkEdit31.TabIndex = 65;
            // 
            // checkEdit30
            // 
            this.checkEdit30.Location = new System.Drawing.Point(286, 73);
            this.checkEdit30.Name = "checkEdit30";
            this.checkEdit30.Properties.Caption = "30";
            this.checkEdit30.Size = new System.Drawing.Size(40, 19);
            this.checkEdit30.TabIndex = 64;
            // 
            // checkEdit29
            // 
            this.checkEdit29.Location = new System.Drawing.Point(246, 73);
            this.checkEdit29.Name = "checkEdit29";
            this.checkEdit29.Properties.Caption = "29";
            this.checkEdit29.Size = new System.Drawing.Size(40, 19);
            this.checkEdit29.TabIndex = 63;
            // 
            // checkEdit28
            // 
            this.checkEdit28.Location = new System.Drawing.Point(206, 73);
            this.checkEdit28.Name = "checkEdit28";
            this.checkEdit28.Properties.Caption = "28";
            this.checkEdit28.Size = new System.Drawing.Size(40, 19);
            this.checkEdit28.TabIndex = 62;
            // 
            // checkEdit27
            // 
            this.checkEdit27.Location = new System.Drawing.Point(166, 73);
            this.checkEdit27.Name = "checkEdit27";
            this.checkEdit27.Properties.Caption = "27";
            this.checkEdit27.Size = new System.Drawing.Size(40, 19);
            this.checkEdit27.TabIndex = 61;
            // 
            // checkEdit26
            // 
            this.checkEdit26.Location = new System.Drawing.Point(126, 73);
            this.checkEdit26.Name = "checkEdit26";
            this.checkEdit26.Properties.Caption = "26";
            this.checkEdit26.Size = new System.Drawing.Size(40, 19);
            this.checkEdit26.TabIndex = 60;
            // 
            // checkEdit25
            // 
            this.checkEdit25.Location = new System.Drawing.Point(86, 73);
            this.checkEdit25.Name = "checkEdit25";
            this.checkEdit25.Properties.Caption = "25";
            this.checkEdit25.Size = new System.Drawing.Size(40, 19);
            this.checkEdit25.TabIndex = 59;
            // 
            // checkEdit24
            // 
            this.checkEdit24.Location = new System.Drawing.Point(46, 73);
            this.checkEdit24.Name = "checkEdit24";
            this.checkEdit24.Properties.Caption = "24";
            this.checkEdit24.Size = new System.Drawing.Size(40, 19);
            this.checkEdit24.TabIndex = 58;
            // 
            // checkEdit13
            // 
            this.checkEdit13.Location = new System.Drawing.Point(46, 46);
            this.checkEdit13.Name = "checkEdit13";
            this.checkEdit13.Properties.Caption = "13";
            this.checkEdit13.Size = new System.Drawing.Size(39, 19);
            this.checkEdit13.TabIndex = 48;
            // 
            // checkEdit11
            // 
            this.checkEdit11.Location = new System.Drawing.Point(406, 19);
            this.checkEdit11.Name = "checkEdit11";
            this.checkEdit11.Properties.Caption = "11";
            this.checkEdit11.Size = new System.Drawing.Size(39, 19);
            this.checkEdit11.TabIndex = 46;
            // 
            // checkEdit10
            // 
            this.checkEdit10.Location = new System.Drawing.Point(366, 19);
            this.checkEdit10.Name = "checkEdit10";
            this.checkEdit10.Properties.Caption = "10";
            this.checkEdit10.Size = new System.Drawing.Size(39, 19);
            this.checkEdit10.TabIndex = 45;
            // 
            // checkEdit9
            // 
            this.checkEdit9.Location = new System.Drawing.Point(326, 19);
            this.checkEdit9.Name = "checkEdit9";
            this.checkEdit9.Properties.Caption = "9";
            this.checkEdit9.Size = new System.Drawing.Size(31, 19);
            this.checkEdit9.TabIndex = 44;
            // 
            // checkEdit8
            // 
            this.checkEdit8.Location = new System.Drawing.Point(286, 19);
            this.checkEdit8.Name = "checkEdit8";
            this.checkEdit8.Properties.Caption = "8";
            this.checkEdit8.Size = new System.Drawing.Size(31, 19);
            this.checkEdit8.TabIndex = 43;
            // 
            // checkEdit7
            // 
            this.checkEdit7.Location = new System.Drawing.Point(246, 19);
            this.checkEdit7.Name = "checkEdit7";
            this.checkEdit7.Properties.Caption = "7";
            this.checkEdit7.Size = new System.Drawing.Size(31, 19);
            this.checkEdit7.TabIndex = 42;
            // 
            // checkEdit6
            // 
            this.checkEdit6.Location = new System.Drawing.Point(206, 19);
            this.checkEdit6.Name = "checkEdit6";
            this.checkEdit6.Properties.Caption = "6";
            this.checkEdit6.Size = new System.Drawing.Size(31, 19);
            this.checkEdit6.TabIndex = 41;
            // 
            // checkEdit5
            // 
            this.checkEdit5.Location = new System.Drawing.Point(166, 19);
            this.checkEdit5.Name = "checkEdit5";
            this.checkEdit5.Properties.Caption = "5";
            this.checkEdit5.Size = new System.Drawing.Size(31, 19);
            this.checkEdit5.TabIndex = 40;
            // 
            // checkEdit4
            // 
            this.checkEdit4.Location = new System.Drawing.Point(126, 19);
            this.checkEdit4.Name = "checkEdit4";
            this.checkEdit4.Properties.Caption = "4";
            this.checkEdit4.Size = new System.Drawing.Size(31, 19);
            this.checkEdit4.TabIndex = 39;
            // 
            // checkEdit3
            // 
            this.checkEdit3.Location = new System.Drawing.Point(86, 19);
            this.checkEdit3.Name = "checkEdit3";
            this.checkEdit3.Properties.Caption = "3";
            this.checkEdit3.Size = new System.Drawing.Size(31, 19);
            this.checkEdit3.TabIndex = 38;
            // 
            // checkEdit2
            // 
            this.checkEdit2.Location = new System.Drawing.Point(46, 19);
            this.checkEdit2.Name = "checkEdit2";
            this.checkEdit2.Properties.Caption = "2";
            this.checkEdit2.Size = new System.Drawing.Size(31, 19);
            this.checkEdit2.TabIndex = 37;
            // 
            // checkEdit1
            // 
            this.checkEdit1.Location = new System.Drawing.Point(6, 19);
            this.checkEdit1.Name = "checkEdit1";
            this.checkEdit1.Properties.Caption = "1";
            this.checkEdit1.Size = new System.Drawing.Size(31, 19);
            this.checkEdit1.TabIndex = 36;
            // 
            // ButtonSelectMonth
            // 
            this.ButtonSelectMonth.Location = new System.Drawing.Point(280, 106);
            this.ButtonSelectMonth.Name = "ButtonSelectMonth";
            this.ButtonSelectMonth.Size = new System.Drawing.Size(75, 23);
            this.ButtonSelectMonth.TabIndex = 3;
            this.ButtonSelectMonth.Text = "Выделить";
            this.ButtonSelectMonth.Click += new System.EventHandler(this.ButtonSelectMonth_Click);
            // 
            // ButtonOdd
            // 
            this.ButtonOdd.Location = new System.Drawing.Point(118, 106);
            this.ButtonOdd.Name = "ButtonOdd";
            this.ButtonOdd.Size = new System.Drawing.Size(75, 23);
            this.ButtonOdd.TabIndex = 2;
            this.ButtonOdd.Text = "Четные";
            this.ButtonOdd.Click += new System.EventHandler(this.ButtonOdd_Click);
            // 
            // ButtonNoOdd
            // 
            this.ButtonNoOdd.Location = new System.Drawing.Point(199, 106);
            this.ButtonNoOdd.Name = "ButtonNoOdd";
            this.ButtonNoOdd.Size = new System.Drawing.Size(75, 23);
            this.ButtonNoOdd.TabIndex = 1;
            this.ButtonNoOdd.Text = "Не Четные";
            this.ButtonNoOdd.Click += new System.EventHandler(this.ButtonNoOdd_Click);
            // 
            // ButtonMonthClear
            // 
            this.ButtonMonthClear.Location = new System.Drawing.Point(361, 106);
            this.ButtonMonthClear.Name = "ButtonMonthClear";
            this.ButtonMonthClear.Size = new System.Drawing.Size(75, 23);
            this.ButtonMonthClear.TabIndex = 0;
            this.ButtonMonthClear.Text = "Очистить";
            this.ButtonMonthClear.Click += new System.EventHandler(this.ButtonMonthClear_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.ButtonSelectDay);
            this.groupBox2.Controls.Add(this.ButtonDayClear);
            this.groupBox2.Controls.Add(this.weekDaysCheckEdit1);
            this.groupBox2.Location = new System.Drawing.Point(9, 75);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(442, 78);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Дни недели";
            // 
            // ButtonSelectDay
            // 
            this.ButtonSelectDay.Location = new System.Drawing.Point(280, 47);
            this.ButtonSelectDay.Name = "ButtonSelectDay";
            this.ButtonSelectDay.Size = new System.Drawing.Size(75, 23);
            this.ButtonSelectDay.TabIndex = 2;
            this.ButtonSelectDay.Text = "Выделить";
            this.ButtonSelectDay.Click += new System.EventHandler(this.ButtonSelectDay_Click);
            // 
            // ButtonDayClear
            // 
            this.ButtonDayClear.Location = new System.Drawing.Point(361, 47);
            this.ButtonDayClear.Name = "ButtonDayClear";
            this.ButtonDayClear.Size = new System.Drawing.Size(75, 23);
            this.ButtonDayClear.TabIndex = 1;
            this.ButtonDayClear.Text = "Очистить";
            this.ButtonDayClear.Click += new System.EventHandler(this.ButtonDayClear_Click);
            // 
            // weekDaysCheckEdit1
            // 
            this.weekDaysCheckEdit1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.weekDaysCheckEdit1.Appearance.Options.UseBackColor = true;
            this.weekDaysCheckEdit1.Location = new System.Drawing.Point(6, 19);
            this.weekDaysCheckEdit1.Name = "weekDaysCheckEdit1";
            this.weekDaysCheckEdit1.Size = new System.Drawing.Size(432, 51);
            this.weekDaysCheckEdit1.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.spinEditMinute2);
            this.groupBox1.Controls.Add(this.labelControl4);
            this.groupBox1.Controls.Add(this.spinEditMinute1);
            this.groupBox1.Controls.Add(this.labelControl3);
            this.groupBox1.Controls.Add(this.spinEditHours2);
            this.groupBox1.Controls.Add(this.labelControl2);
            this.groupBox1.Controls.Add(this.spinEditHours1);
            this.groupBox1.Controls.Add(this.labelControl1);
            this.groupBox1.Location = new System.Drawing.Point(9, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(442, 57);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Время";
            // 
            // spinEditMinute2
            // 
            this.spinEditMinute2.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditMinute2.Location = new System.Drawing.Point(352, 19);
            this.spinEditMinute2.Name = "spinEditMinute2";
            this.spinEditMinute2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEditMinute2.Size = new System.Drawing.Size(48, 20);
            this.spinEditMinute2.TabIndex = 7;
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(338, 22);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(8, 13);
            this.labelControl4.TabIndex = 6;
            this.labelControl4.Text = "--";
            // 
            // spinEditMinute1
            // 
            this.spinEditMinute1.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditMinute1.Location = new System.Drawing.Point(143, 19);
            this.spinEditMinute1.Name = "spinEditMinute1";
            this.spinEditMinute1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEditMinute1.Size = new System.Drawing.Size(48, 20);
            this.spinEditMinute1.TabIndex = 5;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(129, 22);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(8, 13);
            this.labelControl3.TabIndex = 4;
            this.labelControl3.Text = "--";
            // 
            // spinEditHours2
            // 
            this.spinEditHours2.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditHours2.Location = new System.Drawing.Point(284, 19);
            this.spinEditHours2.Name = "spinEditHours2";
            this.spinEditHours2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEditHours2.Size = new System.Drawing.Size(48, 20);
            this.spinEditHours2.TabIndex = 3;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(258, 22);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(18, 13);
            this.labelControl2.TabIndex = 2;
            this.labelControl2.Text = "До:";
            // 
            // spinEditHours1
            // 
            this.spinEditHours1.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditHours1.Location = new System.Drawing.Point(75, 19);
            this.spinEditHours1.Name = "spinEditHours1";
            this.spinEditHours1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEditHours1.Size = new System.Drawing.Size(48, 20);
            this.spinEditHours1.TabIndex = 1;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(50, 22);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(18, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "От:";
            // 
            // ButtonOK
            // 
            this.ButtonOK.Location = new System.Drawing.Point(287, 300);
            this.ButtonOK.Name = "ButtonOK";
            this.ButtonOK.Size = new System.Drawing.Size(75, 23);
            this.ButtonOK.TabIndex = 1;
            this.ButtonOK.Text = "Да";
            this.ButtonOK.Click += new System.EventHandler(this.ButtonOK_Click);
            // 
            // ButtonCancel
            // 
            this.ButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.ButtonCancel.Location = new System.Drawing.Point(368, 300);
            this.ButtonCancel.Name = "ButtonCancel";
            this.ButtonCancel.Size = new System.Drawing.Size(75, 23);
            this.ButtonCancel.TabIndex = 0;
            this.ButtonCancel.Text = "Отмена";
            this.ButtonCancel.Click += new System.EventHandler(this.ButtonCancel_Click);
            // 
            // FormInterval
            // 
            this.AcceptButton = this.ButtonOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.ButtonCancel;
            this.ClientSize = new System.Drawing.Size(459, 335);
            this.ControlBox = false;
            this.Controls.Add(this.panelControlInterval);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormInterval";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Интервалы времени";
            ((System.ComponentModel.ISupportInitialize)(this.panelControlInterval)).EndInit();
            this.panelControlInterval.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit23.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit22.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit21.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit20.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit19.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit12.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit18.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit17.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit16.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit15.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit14.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit31.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit30.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit29.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit28.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit27.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit26.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit25.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit24.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit13.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit11.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEdit1.Properties)).EndInit();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.weekDaysCheckEdit1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditMinute2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditMinute1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditHours2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditHours1.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControlInterval;
        private DevExpress.XtraEditors.SimpleButton ButtonOK;
        private DevExpress.XtraEditors.SimpleButton ButtonCancel;
        private System.Windows.Forms.GroupBox groupBox1;
        private DevExpress.XtraEditors.SpinEdit spinEditMinute2;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.SpinEdit spinEditMinute1;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.SpinEdit spinEditHours2;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.SpinEdit spinEditHours1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private System.Windows.Forms.GroupBox groupBox2;
        private DevExpress.XtraScheduler.UI.WeekDaysCheckEdit weekDaysCheckEdit1;
        private System.Windows.Forms.GroupBox groupBox3;
        private DevExpress.XtraEditors.SimpleButton ButtonOdd;
        private DevExpress.XtraEditors.SimpleButton ButtonNoOdd;
        private DevExpress.XtraEditors.SimpleButton ButtonMonthClear;
        private DevExpress.XtraEditors.SimpleButton ButtonDayClear;
        private DevExpress.XtraEditors.SimpleButton ButtonSelectMonth;
        private DevExpress.XtraEditors.SimpleButton ButtonSelectDay;
        private DevExpress.XtraEditors.CheckEdit checkEdit22;
        private DevExpress.XtraEditors.CheckEdit checkEdit21;
        private DevExpress.XtraEditors.CheckEdit checkEdit20;
        private DevExpress.XtraEditors.CheckEdit checkEdit19;
        private DevExpress.XtraEditors.CheckEdit checkEdit18;
        private DevExpress.XtraEditors.CheckEdit checkEdit17;
        private DevExpress.XtraEditors.CheckEdit checkEdit16;
        private DevExpress.XtraEditors.CheckEdit checkEdit15;
        private DevExpress.XtraEditors.CheckEdit checkEdit14;
        private DevExpress.XtraEditors.CheckEdit checkEdit13;
        private DevExpress.XtraEditors.CheckEdit checkEdit12;
        private DevExpress.XtraEditors.CheckEdit checkEdit11;
        private DevExpress.XtraEditors.CheckEdit checkEdit10;
        private DevExpress.XtraEditors.CheckEdit checkEdit9;
        private DevExpress.XtraEditors.CheckEdit checkEdit8;
        private DevExpress.XtraEditors.CheckEdit checkEdit7;
        private DevExpress.XtraEditors.CheckEdit checkEdit6;
        private DevExpress.XtraEditors.CheckEdit checkEdit5;
        private DevExpress.XtraEditors.CheckEdit checkEdit4;
        private DevExpress.XtraEditors.CheckEdit checkEdit3;
        private DevExpress.XtraEditors.CheckEdit checkEdit2;
        private DevExpress.XtraEditors.CheckEdit checkEdit1;
        private DevExpress.XtraEditors.CheckEdit checkEdit31;
        private DevExpress.XtraEditors.CheckEdit checkEdit30;
        private DevExpress.XtraEditors.CheckEdit checkEdit29;
        private DevExpress.XtraEditors.CheckEdit checkEdit28;
        private DevExpress.XtraEditors.CheckEdit checkEdit27;
        private DevExpress.XtraEditors.CheckEdit checkEdit26;
        private DevExpress.XtraEditors.CheckEdit checkEdit25;
        private DevExpress.XtraEditors.CheckEdit checkEdit24;
        private DevExpress.XtraEditors.CheckEdit checkEdit23;
    }
}