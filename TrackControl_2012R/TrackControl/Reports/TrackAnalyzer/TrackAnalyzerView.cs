using System;
using System.Collections.Generic;
using DevExpress.XtraEditors;
using DevExpress.XtraVerticalGrid.Rows;
using DevExpress.Data;
using TrackControl.Properties;

namespace TrackControl.Reports.TrackAnalyzer
{
    public partial class TrackAnalyzerView : XtraForm, TrackControl.Reports.TrackAnalyzer.ITrackView
  {
    public TrackAnalyzerView()
    {
      InitializeComponent();
      init();
    }

    public void UpdateView(TrackPointData data, ref ReportVehicle r)
    {
      clearGrid();
      _vehicleLbl.Text = data.VehicleInfo;
      if (!data.IsEmpty)
      {
        _dataGpsIdRow.Properties.Value = data.DataGpsID.ToString();
        _logIdRow.Properties.Value = data.LogID.ToString();
        _dateTimeRow.Properties.Value = data.DateTime.ToString("g");
        _eventRow.Properties.Value = data.Event;
        _speedRow.Properties.Value = data.Speed.ToString("N2");
        _accelRow.Properties.Value = data.Acceleration.ToString("N2");
        _locationRow.Properties.Value = data.Location;
        if (data.SensorsIndication.Count > 0)
        {
          _sensorsFolder.Visible = true;
          foreach (KeyValuePair<string, string> pair in data.SensorsIndication)
          {
            EditorRow row = new EditorRow();
            row.OptionsRow.AllowFocus = false;
            row.OptionsRow.BeginUpdate();
            row.Properties.Caption = pair.Key;
            row.Properties.UnboundType = UnboundColumnType.String;
            row.Properties.Value = pair.Value;
            row.OptionsRow.EndUpdate();
            _sensorsFolder.ChildRows.Add(row);
          }
        }
      }
    }

    void clearGrid()
    {
      _vehicleLbl.Text = String.Empty;
      _dataGpsIdRow.Properties.Value = String.Empty;
      _logIdRow.Properties.Value = String.Empty;
      _dateTimeRow.Properties.Value = String.Empty;
      _eventRow.Properties.Value = String.Empty;
      _speedRow.Properties.Value = String.Empty;
      _accelRow.Properties.Value = String.Empty;
      _locationRow.Properties.Value = String.Empty;
      _sensorsFolder.ChildRows.Clear();
      _sensorsFolder.Visible = false;
    }

    void _hideBtn_Click(object sender, EventArgs e)
    {
      Close();
    }

    void init()
    {
      _commons.Properties.Caption = Resources.CommonInfo;
      _dateTimeRow.Properties.Caption = Resources.DateTime;
      _eventRow.Properties.Caption = Resources.Event;
      _speedRow.Properties.Caption = Resources.SpeedCol;
      _accelRow.Properties.Caption = Resources.Acceleration;
      _locationRow.Properties.Caption = Resources.Location;
      _sensorsFolder.Properties.Caption = Resources.SensorsData;
      _hideBtn.Text = Resources.AnalysisStop;
      Text = Resources.TrackAnalyis;
    }
  }
}