﻿using System;
using System.Windows.Forms;
namespace TrackControl.Reports.TrackAnalyzer
{
    public interface ITrackView
    {
        void UpdateView(TrackPointData data, ref ReportVehicle rv);
        Form Owner { get; set; }
        void Dispose();
        void Show();
        bool IsDisposed { get; }
        void Close();
        event EventHandler Disposed;
    }
}
