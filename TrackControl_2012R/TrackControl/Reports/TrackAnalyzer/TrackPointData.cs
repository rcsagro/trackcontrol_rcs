using System;
using System.Collections.Generic;

namespace TrackControl.Reports.TrackAnalyzer
{
  public class TrackPointData
  {
    public string VehicleInfo
    {
      get { return _vehicleInfo; }
      set { _vehicleInfo = value; }
    }
    string _vehicleInfo = String.Empty;

    public System.Int64 DataGpsID
    {
      get { return _dataGpsId; }
      set { _dataGpsId = value; }
    }
    System.Int64 _dataGpsId;

    public int LogID
    {
      get { return _logId; }
      set { _logId = value; }
    }
    int _logId;

    public int IndexInArray
    {
        get { return _indexInArray; }
        set { _indexInArray = value; }
    }
    int _indexInArray;

    public DateTime DateTime
    {
      get { return _dateTime; }
      set { _dateTime = value; }
    }
    DateTime _dateTime;

    public double Lat
    {
        get { return _lat; }
        set { _lat = value; }
    }

    double _lat;

    public double Long
    {
        get { return _long; }
        set { _long = value; }
    }

    double _long;

    public string Event
    {
      get { return _event; }
      set { _event = value; }
    }
    string _event = String.Empty;

    public float Speed
    {
      get { return _speed; }
      set { _speed = value; }
    }
    float _speed;

    public double Acceleration
    {
      get { return _acceleration; }
      set { _acceleration = value; }
    }
    double _acceleration;

    public string Location
    {
      get { return _location; }
      set { _location = value; }
    }
    string _location = String.Empty;

    public Dictionary<string, string> SensorsIndication
    {
      get { return _sensorsIndication; }
      set { _sensorsIndication = value; }
    }
    Dictionary<string, string> _sensorsIndication = new Dictionary<string, string>();

    public bool IsEmpty
    {
      get { return _isEmpty; }
    }
    bool _isEmpty;

    public static TrackPointData Empty
    {
      get
      {
        TrackPointData data = new TrackPointData();
        data._isEmpty = true;
        return data;
      }
    }
  }
}
