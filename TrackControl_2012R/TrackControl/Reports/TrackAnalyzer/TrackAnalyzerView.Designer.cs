namespace TrackControl.Reports.TrackAnalyzer
{
  partial class TrackAnalyzerView
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TrackAnalyzerView));
      this._mainTable = new System.Windows.Forms.TableLayoutPanel();
      this._vehicleLbl = new DevExpress.XtraEditors.LabelControl();
      this._grid = new DevExpress.XtraVerticalGrid.VGridControl();
      this._locationRepo = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
      this._commons = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
      this._dataGpsIdRow = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
      this._logIdRow = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
      this._dateTimeRow = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
      this._eventRow = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
      this._speedRow = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
      this._accelRow = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
      this._locationRow = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
      this._sensorsFolder = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
      this._hideBtn = new DevExpress.XtraEditors.SimpleButton();
      this._mainTable.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this._grid)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this._locationRepo)).BeginInit();
      this.SuspendLayout();
      // 
      // _mainTable
      // 
      this._mainTable.ColumnCount = 1;
      this._mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this._mainTable.Controls.Add(this._vehicleLbl, 0, 0);
      this._mainTable.Controls.Add(this._grid, 0, 1);
      this._mainTable.Controls.Add(this._hideBtn, 0, 2);
      this._mainTable.Dock = System.Windows.Forms.DockStyle.Fill;
      this._mainTable.Location = new System.Drawing.Point(0, 0);
      this._mainTable.Name = "_mainTable";
      this._mainTable.RowCount = 3;
      this._mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
      this._mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this._mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
      this._mainTable.Size = new System.Drawing.Size(342, 324);
      this._mainTable.TabIndex = 0;
      // 
      // _vehicleLbl
      // 
      this._vehicleLbl.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
      this._vehicleLbl.Appearance.Options.UseFont = true;
      this._vehicleLbl.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
      this._vehicleLbl.Dock = System.Windows.Forms.DockStyle.Fill;
      this._vehicleLbl.Location = new System.Drawing.Point(3, 3);
      this._vehicleLbl.Name = "_vehicleLbl";
      this._vehicleLbl.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
      this._vehicleLbl.Size = new System.Drawing.Size(336, 18);
      this._vehicleLbl.TabIndex = 2;
      // 
      // _grid
      // 
      this._grid.Appearance.ReadOnlyRecordValue.BackColor = System.Drawing.SystemColors.Window;
      this._grid.Appearance.ReadOnlyRecordValue.ForeColor = System.Drawing.SystemColors.ControlText;
      this._grid.Appearance.ReadOnlyRecordValue.Options.UseBackColor = true;
      this._grid.Appearance.ReadOnlyRecordValue.Options.UseForeColor = true;
      this._grid.Appearance.ReadOnlyRow.ForeColor = System.Drawing.SystemColors.ControlText;
      this._grid.Appearance.ReadOnlyRow.Options.UseForeColor = true;
      this._grid.Appearance.RowHeaderPanel.ForeColor = System.Drawing.SystemColors.ControlText;
      this._grid.Appearance.RowHeaderPanel.Options.UseForeColor = true;
      this._grid.Appearance.RowHeaderPanel.Options.UseTextOptions = true;
      this._grid.Appearance.RowHeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
      this._grid.Dock = System.Windows.Forms.DockStyle.Fill;
      this._grid.Location = new System.Drawing.Point(3, 27);
      this._grid.Name = "_grid";
      this._grid.RecordWidth = 174;
      this._grid.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this._locationRepo});
      this._grid.RowHeaderWidth = 153;
      this._grid.Rows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this._commons,
            this._sensorsFolder});
      this._grid.Size = new System.Drawing.Size(336, 254);
      this._grid.TabIndex = 3;
      // 
      // _locationRepo
      // 
      this._locationRepo.Name = "_locationRepo";
      this._locationRepo.ReadOnly = true;
      // 
      // _commons
      // 
      this._commons.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this._dataGpsIdRow,
            this._logIdRow,
            this._dateTimeRow,
            this._eventRow,
            this._speedRow,
            this._accelRow,
            this._locationRow});
      this._commons.Name = "_commons";
      this._commons.OptionsRow.AllowFocus = false;
      this._commons.OptionsRow.AllowMove = false;
      this._commons.OptionsRow.AllowMoveToCustomizationForm = false;
      this._commons.OptionsRow.ShowInCustomizationForm = false;
      this._commons.Properties.Caption = "����� ����������";
      // 
      // _dataGpsIdRow
      // 
      this._dataGpsIdRow.Name = "_dataGpsIdRow";
      this._dataGpsIdRow.OptionsRow.AllowMove = false;
      this._dataGpsIdRow.OptionsRow.AllowMoveToCustomizationForm = false;
      this._dataGpsIdRow.OptionsRow.ShowInCustomizationForm = false;
      this._dataGpsIdRow.Properties.Caption = "DataGps_ID";
      this._dataGpsIdRow.Properties.ReadOnly = true;
      // 
      // _logIdRow
      // 
      this._logIdRow.Name = "_logIdRow";
      this._logIdRow.OptionsRow.AllowMove = false;
      this._logIdRow.OptionsRow.AllowMoveToCustomizationForm = false;
      this._logIdRow.OptionsRow.ShowInCustomizationForm = false;
      this._logIdRow.Properties.Caption = "LogID";
      this._logIdRow.Properties.ReadOnly = true;
      // 
      // _dateTimeRow
      // 
      this._dateTimeRow.Name = "_dateTimeRow";
      this._dateTimeRow.OptionsRow.AllowMove = false;
      this._dateTimeRow.OptionsRow.AllowMoveToCustomizationForm = false;
      this._dateTimeRow.OptionsRow.ShowInCustomizationForm = false;
      this._dateTimeRow.Properties.Caption = "���� � �����";
      this._dateTimeRow.Properties.ReadOnly = true;
      // 
      // _eventRow
      // 
      this._eventRow.Name = "_eventRow";
      this._eventRow.OptionsRow.AllowMove = false;
      this._eventRow.OptionsRow.AllowMoveToCustomizationForm = false;
      this._eventRow.OptionsRow.ShowInCustomizationForm = false;
      this._eventRow.Properties.Caption = "�������";
      this._eventRow.Properties.ReadOnly = true;
      // 
      // _speedRow
      // 
      this._speedRow.Name = "_speedRow";
      this._speedRow.OptionsRow.AllowMove = false;
      this._speedRow.OptionsRow.AllowMoveToCustomizationForm = false;
      this._speedRow.OptionsRow.ShowInCustomizationForm = false;
      this._speedRow.Properties.Caption = "��������, ��/�";
      this._speedRow.Properties.ReadOnly = true;
      // 
      // _accelRow
      // 
      this._accelRow.Name = "_accelRow";
      this._accelRow.OptionsRow.AllowMove = false;
      this._accelRow.OptionsRow.AllowMoveToCustomizationForm = false;
      this._accelRow.OptionsRow.ShowInCustomizationForm = false;
      this._accelRow.Properties.Caption = "���������";
      this._accelRow.Properties.ReadOnly = true;
      // 
      // _locationRow
      // 
      this._locationRow.Height = 60;
      this._locationRow.Name = "_locationRow";
      this._locationRow.OptionsRow.AllowMove = false;
      this._locationRow.OptionsRow.AllowMoveToCustomizationForm = false;
      this._locationRow.OptionsRow.ShowInCustomizationForm = false;
      this._locationRow.Properties.Caption = "��������������";
      this._locationRow.Properties.ReadOnly = true;
      this._locationRow.Properties.RowEdit = this._locationRepo;
      // 
      // _sensorsFolder
      // 
      this._sensorsFolder.Name = "_sensorsFolder";
      this._sensorsFolder.Properties.Caption = "��������� ��������";
      this._sensorsFolder.Visible = false;
      // 
      // _hideBtn
      // 
      this._hideBtn.Dock = System.Windows.Forms.DockStyle.Right;
      this._hideBtn.Image = ((System.Drawing.Image)(resources.GetObject("_hideBtn.Image")));
      this._hideBtn.Location = new System.Drawing.Point(206, 289);
      this._hideBtn.Margin = new System.Windows.Forms.Padding(10, 5, 10, 10);
      this._hideBtn.Name = "_hideBtn";
      this._hideBtn.Size = new System.Drawing.Size(126, 25);
      this._hideBtn.TabIndex = 4;
      this._hideBtn.Text = "��������� ������";
      this._hideBtn.Click += new System.EventHandler(this._hideBtn_Click);
      // 
      // TrackAnalyzerView
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(342, 324);
      this.ControlBox = false;
      this.Controls.Add(this._mainTable);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
      this.MaximizeBox = false;
      this.MaximumSize = new System.Drawing.Size(400, 500);
      this.MinimizeBox = false;
      this.MinimumSize = new System.Drawing.Size(340, 200);
      this.Name = "TrackAnalyzerView";
      this.ShowInTaskbar = false;
      this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = "������ ����� �����";
      this._mainTable.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this._grid)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this._locationRepo)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.TableLayoutPanel _mainTable;
    private DevExpress.XtraEditors.LabelControl _vehicleLbl;
    private DevExpress.XtraVerticalGrid.VGridControl _grid;
    private DevExpress.XtraVerticalGrid.Rows.CategoryRow _commons;
    private DevExpress.XtraVerticalGrid.Rows.EditorRow _dateTimeRow;
    private DevExpress.XtraVerticalGrid.Rows.EditorRow _dataGpsIdRow;
    private DevExpress.XtraVerticalGrid.Rows.EditorRow _logIdRow;
    private DevExpress.XtraVerticalGrid.Rows.EditorRow _speedRow;
    private DevExpress.XtraVerticalGrid.Rows.EditorRow _accelRow;
    private DevExpress.XtraVerticalGrid.Rows.EditorRow _eventRow;
    private DevExpress.XtraVerticalGrid.Rows.EditorRow _locationRow;
    private DevExpress.XtraVerticalGrid.Rows.CategoryRow _sensorsFolder;
    private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit _locationRepo;
    private DevExpress.XtraEditors.SimpleButton _hideBtn;
  }
}