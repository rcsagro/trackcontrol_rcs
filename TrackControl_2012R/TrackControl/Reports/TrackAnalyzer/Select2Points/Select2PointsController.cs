﻿using BaseReports.Procedure;
using BaseReports.Procedure.Calibration;
using LocalCache;

using System;
using System.Drawing;
using System.Collections;
using System.Windows.Forms;

using TrackControl.General;
using TrackControl.General.Services;
using TrackControl.GMap;
using TrackControl.GMap.UI ;
using TrackControl.Reports;
using TrackControl.Vehicles;
using TrackControl.Properties;


namespace TrackControl.Reports.TrackAnalyzer
{
    public class Select2PointsController 
    {
        bool _isOn;
        ITreeModel<ReportVehicle> _vehicleTree;
        GoogleMapControl _map;
        Form _topForm;
        protected ITrackView _view;
        
        public Select2PointsController(ITreeModel<ReportVehicle> vehicleTree, GoogleMapControl map)
        {
          _vehicleTree = vehicleTree;
          _map = map;

        }
        protected  void OpenView()
        {
            _view = new Select2Points();
        }

        public Form TopForm
        {
          set { _topForm = value; }
        }

        public void AssignVehicle(ReportVehicle vehicle)
        {
            if (!_isOn)
                return;

            _map.SelectTrack(vehicle.Mobitel.Id);
            map_OnTrackPointClicked(null,vehicle, 0, new Point(0, 0));
        }


        public void Start()
        {
            if (_isOn)
                return;
            _isOn = true;
            _map.OnTrackPointClicked += map_OnTrackPointClicked;
            _map.IsTrackAnalizer = true;
            OpenView();
            _view.Owner = _topForm;
            _view.Disposed += view_Disposed;
            _view.Show();

            ReportVehicle rv = _vehicleTree.Current;
            if (null != rv)
                AssignVehicle(rv);
        }

        public void Stop()
        {
          if (!_isOn)
            return;
          _isOn = false;
          _map.IsTrackAnalizer = false;
          _map.ClearZones();
          _map.Repaint();
          _map.OnTrackPointClicked -= map_OnTrackPointClicked;
          _view.Disposed -= view_Disposed;
          if (!_view.IsDisposed)
            _view.Close();
        }

    void view_Disposed(object sender, EventArgs e)
    {
      Stop();
    }

    void map_OnTrackPointClicked(GMapTrack tr, IVehicle vehicle,  int indexInArray, Point pt)
    {
        if (_view == null || tr == null || indexInArray == -1) return;
        IGeoPoint point = tr.GeoPoints[indexInArray]; 
        TrackPointData data;
        if (null != point)
        {
            data = new TrackPointData();
            GpsData dataGps = (GpsData)point;
            data.DataGpsID = dataGps.Id;
            data.LogID = dataGps.LogId;
            data.Lat = dataGps.LatLng.Lat;
            data.Long = dataGps.LatLng.Lng;
            data.DateTime = dataGps.Time;
            data.Event = MobitelEventsChecker.CheckEvents(dataGps.Events);
            data.Speed = dataGps.Speed;
            data.Acceleration = dataGps.Accel;
            data.Location = Algorithm.FindLocation(dataGps.LatLng);
            data.IndexInArray = indexInArray;
        }
        else
        {
            data = TrackPointData.Empty;
        }
        ReportVehicle rv = null;
        if (null != vehicle)
        {
            rv = (ReportVehicle)vehicle;
            data.VehicleInfo = String.Format("{0} \"{1}\"", rv.RegNumber, String.Format("{0} {1}", rv.CarMaker, rv.CarModel));
        }
        else
        {
            data.VehicleInfo = Resources.VehicleNotSelected;
        }
        _view.UpdateView(data, ref rv);
    }
    }
}
