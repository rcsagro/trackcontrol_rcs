namespace TrackControl.Reports.TrackAnalyzer
{
    partial class Select2Points
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Select2Points));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.vgSecond = new DevExpress.XtraVerticalGrid.VGridControl();
            this.repositoryItemMemoEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.riMemo = new DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit();
            this.riMemoEdit = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this._second = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this._dataGpsIdRowS = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this._indexS = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this._dateTimeRowS = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this._dateLongS = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this._dateLatS = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this._vehicleS = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.vgFirst = new DevExpress.XtraVerticalGrid.VGridControl();
            this._locationRepo = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this.riteFirst = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.riMemoEditF = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this._first = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this._dataGpsIdRowF = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this._indexF = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this._dateTimeRowF = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this._dateLongF = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this._dateLatF = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this._vehicleF = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.rgSelectPoint = new DevExpress.XtraEditors.RadioGroup();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.sbtExit = new DevExpress.XtraEditors.SimpleButton();
            this.sbtZoneCreate = new DevExpress.XtraEditors.SimpleButton();
            this.peZone = new DevExpress.XtraEditors.PictureEdit();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vgSecond)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.riMemo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.riMemoEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vgFirst)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._locationRepo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.riteFirst)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.riMemoEditF)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgSelectPoint.Properties)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.peZone.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.vgSecond, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.vgFirst, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.rgSelectPoint, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.peZone, 0, 3);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, -1);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 8F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(293, 585);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // vgSecond
            // 
            this.vgSecond.Appearance.ReadOnlyRecordValue.BackColor = System.Drawing.SystemColors.Window;
            this.vgSecond.Appearance.ReadOnlyRecordValue.ForeColor = System.Drawing.SystemColors.ControlText;
            this.vgSecond.Appearance.ReadOnlyRecordValue.Options.UseBackColor = true;
            this.vgSecond.Appearance.ReadOnlyRecordValue.Options.UseForeColor = true;
            this.vgSecond.Appearance.ReadOnlyRow.ForeColor = System.Drawing.SystemColors.ControlText;
            this.vgSecond.Appearance.ReadOnlyRow.Options.UseForeColor = true;
            this.vgSecond.Appearance.RowHeaderPanel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.vgSecond.Appearance.RowHeaderPanel.Options.UseForeColor = true;
            this.vgSecond.Appearance.RowHeaderPanel.Options.UseTextOptions = true;
            this.vgSecond.Appearance.RowHeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.vgSecond.Dock = System.Windows.Forms.DockStyle.Fill;
            this.vgSecond.Enabled = false;
            this.vgSecond.Location = new System.Drawing.Point(3, 187);
            this.vgSecond.Name = "vgSecond";
            this.vgSecond.RecordWidth = 174;
            this.vgSecond.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemMemoEdit1,
            this.riMemo,
            this.riMemoEdit});
            this.vgSecond.RowHeaderWidth = 125;
            this.vgSecond.Rows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this._second});
            this.vgSecond.ScrollVisibility = DevExpress.XtraVerticalGrid.ScrollVisibility.Never;
            this.vgSecond.Size = new System.Drawing.Size(287, 147);
            this.vgSecond.TabIndex = 14;
            // 
            // repositoryItemMemoEdit1
            // 
            this.repositoryItemMemoEdit1.Name = "repositoryItemMemoEdit1";
            this.repositoryItemMemoEdit1.ReadOnly = true;
            // 
            // riMemo
            // 
            this.riMemo.AutoHeight = false;
            this.riMemo.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.riMemo.Name = "riMemo";
            this.riMemo.ReadOnly = true;
            // 
            // riMemoEdit
            // 
            this.riMemoEdit.Name = "riMemoEdit";
            this.riMemoEdit.ReadOnly = true;
            // 
            // _second
            // 
            this._second.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this._dataGpsIdRowS,
            this._indexS,
            this._dateTimeRowS,
            this._dateLongS,
            this._dateLatS,
            this._vehicleS});
            this._second.Height = 19;
            this._second.Name = "_second";
            this._second.OptionsRow.AllowFocus = false;
            this._second.OptionsRow.AllowMove = false;
            this._second.OptionsRow.AllowMoveToCustomizationForm = false;
            this._second.OptionsRow.ShowInCustomizationForm = false;
            this._second.Properties.Caption = "��������� �����";
            // 
            // _dataGpsIdRowS
            // 
            this._dataGpsIdRowS.Height = 20;
            this._dataGpsIdRowS.Name = "_dataGpsIdRowS";
            this._dataGpsIdRowS.OptionsRow.AllowMove = false;
            this._dataGpsIdRowS.OptionsRow.AllowMoveToCustomizationForm = false;
            this._dataGpsIdRowS.OptionsRow.ShowInCustomizationForm = false;
            this._dataGpsIdRowS.Properties.Caption = "DataGps_ID";
            this._dataGpsIdRowS.Properties.ReadOnly = true;
            // 
            // _indexS
            // 
            this._indexS.Height = 20;
            this._indexS.Name = "_indexS";
            this._indexS.Properties.Caption = "����� � �����";
            this._indexS.Properties.ReadOnly = true;
            // 
            // _dateTimeRowS
            // 
            this._dateTimeRowS.Height = 18;
            this._dateTimeRowS.Name = "_dateTimeRowS";
            this._dateTimeRowS.OptionsRow.AllowMove = false;
            this._dateTimeRowS.OptionsRow.AllowMoveToCustomizationForm = false;
            this._dateTimeRowS.OptionsRow.ShowInCustomizationForm = false;
            this._dateTimeRowS.Properties.Caption = "���� � �����";
            this._dateTimeRowS.Properties.ReadOnly = true;
            // 
            // _dateLongS
            // 
            this._dateLongS.Height = 17;
            this._dateLongS.Name = "_dateLongS";
            this._dateLongS.OptionsRow.AllowMove = false;
            this._dateLongS.OptionsRow.AllowMoveToCustomizationForm = false;
            this._dateLongS.OptionsRow.ShowInCustomizationForm = false;
            this._dateLongS.Properties.Caption = "�������";
            this._dateLongS.Properties.ReadOnly = true;
            // 
            // _dateLatS
            // 
            this._dateLatS.Height = 18;
            this._dateLatS.Name = "_dateLatS";
            this._dateLatS.OptionsRow.AllowMove = false;
            this._dateLatS.OptionsRow.AllowMoveToCustomizationForm = false;
            this._dateLatS.OptionsRow.ShowInCustomizationForm = false;
            this._dateLatS.Properties.Caption = "������";
            this._dateLatS.Properties.ReadOnly = true;
            // 
            // _vehicleS
            // 
            this._vehicleS.Height = 20;
            this._vehicleS.Name = "_vehicleS";
            this._vehicleS.Properties.Caption = "���������";
            this._vehicleS.Properties.ReadOnly = true;
            // 
            // vgFirst
            // 
            this.vgFirst.Appearance.ReadOnlyRecordValue.BackColor = System.Drawing.SystemColors.Window;
            this.vgFirst.Appearance.ReadOnlyRecordValue.ForeColor = System.Drawing.SystemColors.ControlText;
            this.vgFirst.Appearance.ReadOnlyRecordValue.Options.UseBackColor = true;
            this.vgFirst.Appearance.ReadOnlyRecordValue.Options.UseForeColor = true;
            this.vgFirst.Appearance.ReadOnlyRow.ForeColor = System.Drawing.SystemColors.ControlText;
            this.vgFirst.Appearance.ReadOnlyRow.Options.UseForeColor = true;
            this.vgFirst.Appearance.RowHeaderPanel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.vgFirst.Appearance.RowHeaderPanel.Options.UseForeColor = true;
            this.vgFirst.Appearance.RowHeaderPanel.Options.UseTextOptions = true;
            this.vgFirst.Appearance.RowHeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.vgFirst.Dock = System.Windows.Forms.DockStyle.Fill;
            this.vgFirst.Location = new System.Drawing.Point(3, 34);
            this.vgFirst.Name = "vgFirst";
            this.vgFirst.RecordWidth = 174;
            this.vgFirst.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this._locationRepo,
            this.riteFirst,
            this.riMemoEditF});
            this.vgFirst.RowHeaderWidth = 125;
            this.vgFirst.Rows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this._first});
            this.vgFirst.ScrollVisibility = DevExpress.XtraVerticalGrid.ScrollVisibility.Never;
            this.vgFirst.Size = new System.Drawing.Size(287, 147);
            this.vgFirst.TabIndex = 11;
            // 
            // _locationRepo
            // 
            this._locationRepo.Name = "_locationRepo";
            this._locationRepo.ReadOnly = true;
            // 
            // riteFirst
            // 
            this.riteFirst.AutoHeight = false;
            this.riteFirst.Name = "riteFirst";
            // 
            // riMemoEditF
            // 
            this.riMemoEditF.Name = "riMemoEditF";
            this.riMemoEditF.ReadOnly = true;
            // 
            // _first
            // 
            this._first.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this._dataGpsIdRowF,
            this._indexF,
            this._dateTimeRowF,
            this._dateLongF,
            this._dateLatF,
            this._vehicleF});
            this._first.Height = 19;
            this._first.Name = "_first";
            this._first.OptionsRow.AllowFocus = false;
            this._first.OptionsRow.AllowMove = false;
            this._first.OptionsRow.AllowMoveToCustomizationForm = false;
            this._first.OptionsRow.ShowInCustomizationForm = false;
            this._first.Properties.Caption = "������ �����";
            // 
            // _dataGpsIdRowF
            // 
            this._dataGpsIdRowF.Height = 20;
            this._dataGpsIdRowF.Name = "_dataGpsIdRowF";
            this._dataGpsIdRowF.OptionsRow.AllowMove = false;
            this._dataGpsIdRowF.OptionsRow.AllowMoveToCustomizationForm = false;
            this._dataGpsIdRowF.OptionsRow.ShowInCustomizationForm = false;
            this._dataGpsIdRowF.Properties.Caption = "DataGps_ID";
            this._dataGpsIdRowF.Properties.ReadOnly = true;
            // 
            // _indexF
            // 
            this._indexF.Height = 20;
            this._indexF.Name = "_indexF";
            this._indexF.Properties.Caption = "����� � �����";
            this._indexF.Properties.ReadOnly = true;
            // 
            // _dateTimeRowF
            // 
            this._dateTimeRowF.Height = 18;
            this._dateTimeRowF.Name = "_dateTimeRowF";
            this._dateTimeRowF.OptionsRow.AllowMove = false;
            this._dateTimeRowF.OptionsRow.AllowMoveToCustomizationForm = false;
            this._dateTimeRowF.OptionsRow.ShowInCustomizationForm = false;
            this._dateTimeRowF.Properties.Caption = "���� � �����";
            this._dateTimeRowF.Properties.ReadOnly = true;
            // 
            // _dateLongF
            // 
            this._dateLongF.Height = 17;
            this._dateLongF.Name = "_dateLongF";
            this._dateLongF.OptionsRow.AllowMove = false;
            this._dateLongF.OptionsRow.AllowMoveToCustomizationForm = false;
            this._dateLongF.OptionsRow.ShowInCustomizationForm = false;
            this._dateLongF.Properties.Caption = "�������";
            this._dateLongF.Properties.ReadOnly = true;
            // 
            // _dateLatF
            // 
            this._dateLatF.Height = 18;
            this._dateLatF.Name = "_dateLatF";
            this._dateLatF.OptionsRow.AllowMove = false;
            this._dateLatF.OptionsRow.AllowMoveToCustomizationForm = false;
            this._dateLatF.OptionsRow.ShowInCustomizationForm = false;
            this._dateLatF.Properties.Caption = "������";
            this._dateLatF.Properties.ReadOnly = true;
            // 
            // _vehicleF
            // 
            this._vehicleF.Appearance.Options.UseTextOptions = true;
            this._vehicleF.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this._vehicleF.Height = 20;
            this._vehicleF.Name = "_vehicleF";
            this._vehicleF.Properties.Caption = "���������";
            this._vehicleF.Properties.ReadOnly = true;
            // 
            // rgSelectPoint
            // 
            this.rgSelectPoint.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rgSelectPoint.EditValue = 1;
            this.rgSelectPoint.Location = new System.Drawing.Point(3, 3);
            this.rgSelectPoint.Name = "rgSelectPoint";
            this.rgSelectPoint.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(1, "������ �����"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(2, "��������� �����")});
            this.rgSelectPoint.Size = new System.Drawing.Size(287, 25);
            this.rgSelectPoint.TabIndex = 0;
            this.rgSelectPoint.SelectedIndexChanged += new System.EventHandler(this.rgSelectPoint_SelectedIndexChanged);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.sbtExit, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.sbtZoneCreate, 0, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 540);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 41F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(287, 41);
            this.tableLayoutPanel2.TabIndex = 13;
            // 
            // sbtExit
            // 
            this.sbtExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.sbtExit.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.sbtExit.Image = ((System.Drawing.Image)(resources.GetObject("sbtExit.Image")));
            this.sbtExit.Location = new System.Drawing.Point(171, 6);
            this.sbtExit.Margin = new System.Windows.Forms.Padding(10, 5, 10, 10);
            this.sbtExit.Name = "sbtExit";
            this.sbtExit.Size = new System.Drawing.Size(106, 25);
            this.sbtExit.TabIndex = 12;
            this.sbtExit.Text = "�����";
            this.sbtExit.Click += new System.EventHandler(this.sbtExit_Click);
            // 
            // sbtZoneCreate
            // 
            this.sbtZoneCreate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.sbtZoneCreate.Enabled = false;
            this.sbtZoneCreate.Image = ((System.Drawing.Image)(resources.GetObject("sbtZoneCreate.Image")));
            this.sbtZoneCreate.Location = new System.Drawing.Point(27, 5);
            this.sbtZoneCreate.Margin = new System.Windows.Forms.Padding(10, 5, 10, 10);
            this.sbtZoneCreate.Name = "sbtZoneCreate";
            this.sbtZoneCreate.Size = new System.Drawing.Size(106, 26);
            this.sbtZoneCreate.TabIndex = 13;
            this.sbtZoneCreate.Text = "������� ����";
            this.sbtZoneCreate.Click += new System.EventHandler(this.sbtZoneCreate_Click);
            // 
            // peZone
            // 
            this.peZone.Dock = System.Windows.Forms.DockStyle.Fill;
            this.peZone.Location = new System.Drawing.Point(3, 340);
            this.peZone.Name = "peZone";
            this.peZone.Properties.NullText = "����������� ����";
            this.peZone.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            this.peZone.Size = new System.Drawing.Size(287, 194);
            this.peZone.TabIndex = 15;
            // 
            // Select2Points
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(293, 582);
            this.ControlBox = false;
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Select2Points";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "����� ������ � ��������� ����� �����";
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vgSecond)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemMemoEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.riMemo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.riMemoEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vgFirst)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._locationRepo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.riteFirst)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.riMemoEditF)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgSelectPoint.Properties)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.peZone.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private DevExpress.XtraEditors.RadioGroup rgSelectPoint;
        private DevExpress.XtraVerticalGrid.VGridControl vgFirst;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit _locationRepo;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow _first;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow _dataGpsIdRowF;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow _dateTimeRowF;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow _dateLongF;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow _dateLatF;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private DevExpress.XtraEditors.SimpleButton sbtZoneCreate;
        private DevExpress.XtraEditors.SimpleButton sbtExit;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow _vehicleF;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow _indexF;
        private DevExpress.XtraVerticalGrid.VGridControl vgSecond;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit repositoryItemMemoEdit1;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow _dataGpsIdRowS;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow _dateTimeRowS;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow _dateLongS;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow _dateLatS;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow _vehicleS;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow _indexS;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit riteFirst;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoExEdit riMemo;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit riMemoEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit riMemoEditF;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow _second;
        private DevExpress.XtraEditors.PictureEdit peZone;
    }
}