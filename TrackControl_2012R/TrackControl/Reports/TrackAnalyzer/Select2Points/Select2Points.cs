using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using TrackControl.General;
using TrackControl.GMap;
using TrackControl.Properties;

namespace TrackControl.Reports.TrackAnalyzer
{
    public partial class Select2Points : DevExpress.XtraEditors.XtraForm, TrackControl.Reports.TrackAnalyzer.ITrackView 
    {
        int _ind_first;
        int _ind_second;
        System.Int64 _gps_first;
        System.Int64 _gps_second;
        IGeoPoint[] g_poins;
        public static  event Action<IGeoPoint[]> ZoneCreate;
        public static event VoidHandler ShowZoneEditor; 
        public Select2Points()
        {
            InitializeComponent();
            Localization();
        }
        public void UpdateView(TrackPointData data,ref ReportVehicle rv)
        {
            try
            {
                if (rv == null) return;
                if ((int)rgSelectPoint.EditValue == 1)
                {
                    clearFirstGrid();
                    if (!data.IsEmpty)
                    {
                        _dataGpsIdRowF.Properties.Value = data.DataGpsID.ToString();
                        _gps_first = data.DataGpsID;
                        _indexF.Properties.Value = data.IndexInArray.ToString().Trim();
                        _ind_first = data.IndexInArray;
                        _dateTimeRowF.Properties.Value = data.DateTime.ToString("g");
                        _dateLongF.Properties.Value = data.Long.ToString().Trim();
                        _dateLatF.Properties.Value = data.Lat.ToString().Trim();
                        _vehicleF.Properties.Value = data.VehicleInfo;
                        if (_ind_second == 0 && _gps_second == 0) rgSelectPoint.SelectedIndex = 1;
                    }
                }
                else if ((int)rgSelectPoint.EditValue == 2)
                {
                    clearFecondGrid();
                    if (!data.IsEmpty)
                    {
                        _dataGpsIdRowS.Properties.Value = data.DataGpsID.ToString();
                        _gps_second = data.DataGpsID;
                        _indexS.Properties.Value = data.IndexInArray.ToString().Trim();
                        _ind_second = data.IndexInArray;
                        _dateTimeRowS.Properties.Value = data.DateTime.ToString("g");
                        _dateLongS.Properties.Value = data.Long.ToString().Trim();
                        _dateLatS.Properties.Value = data.Lat.ToString().Trim();
                        _vehicleS.Properties.Value = data.VehicleInfo;
                    }
                }
                // ���������� ���� ��� ����� ������ ������������� ��������

                if (_gps_first > 0 && _gps_second > 0 && (_ind_first + _ind_second) > 0)
                {
                    if (_vehicleF.Properties.Value.ToString() == _vehicleS.Properties.Value.ToString())
                    {
                        int min_index = Math.Min(_ind_first, _ind_second);
                        int max_index = Math.Max(_ind_first, _ind_second);
                        if (max_index - min_index > 2)
                        {
                            g_poins = new IGeoPoint[max_index - min_index];
                            rv.Track.GeoPoints.CopyTo(min_index, g_poins, 0, max_index - min_index);
                            PointLatLng[] points = new PointLatLng[g_poins.Length];
                            int i = 0;
                            Array.ForEach(g_poins,
                            delegate(IGeoPoint g_point)
                            {
                                points[i++] = new PointLatLng (g_point.LatLng.Lat,g_point.LatLng.Lng);
                            });

                            BmpLatLng bll = new BmpLatLng(points, peZone.Width, peZone.Height);
                            if (peZone.Image != null) peZone.Image.Dispose();
                            peZone.Image = bll.DrawOnBitmap();
                            if (ZoneCreate != null)
                            ZoneCreate(g_poins);
                        }
                        sbtZoneCreate.Enabled = true;
                    }
                    else
                    {
                        sbtZoneCreate.Enabled = false;
                        peZone.Image = null;
                    }
                }
                else
                {
                    sbtZoneCreate.Enabled = false;
                    peZone.Image = null;
                }
            }
            catch (Exception  ex)
            {
                XtraMessageBox.Show(ex.Message, "UpdateView");  
            }
            
        }

        void clearFirstGrid()
        {
            _ind_first = 0;
            _gps_first = 0;
            _dataGpsIdRowF.Properties.Value = String.Empty;
            _dateTimeRowF.Properties.Value = String.Empty;
            _dateLongF.Properties.Value = String.Empty;
            _dateLatF.Properties.Value = String.Empty;
            _vehicleF.Properties.Value = String.Empty;
            _indexF.Properties.Value = String.Empty; 
        }
        void clearFecondGrid()
        {
            _ind_second = 0;
            _gps_second = 0;
            _dataGpsIdRowS.Properties.Value = String.Empty;
            _dateTimeRowS.Properties.Value = String.Empty;
            _dateLongS.Properties.Value = String.Empty;
            _dateLatS.Properties.Value = String.Empty;
            _vehicleS.Properties.Value = String.Empty;
            _indexS.Properties.Value = String.Empty; 
        }

        private void rgSelectPoint_SelectedIndexChanged(object sender, EventArgs e)
        {
            if ((int)rgSelectPoint.EditValue == 1)
            {
                vgFirst.Enabled = true;
                vgSecond.Enabled = false;
            }
            else if ((int)rgSelectPoint.EditValue == 2)
            {
                vgFirst.Enabled = false;
                vgSecond.Enabled = true;
            }
        }

        private void sbtExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void sbtZoneCreate_Click(object sender, EventArgs e)
        {
            Close();
            if (ShowZoneEditor != null)
            ShowZoneEditor();
        }
        void Localization()
        {
             _dateTimeRowF.Properties.Value = Resources.DateTime;
             _dateLongF.Properties.Value = Resources.Longitude;
             _dateLatF.Properties.Value = Resources.Latitude;
            _vehicleF.Properties.Value = Resources.Transport;
            _indexF.Properties.Value = Resources.NumberPointInTrack;

            _dateTimeRowS.Properties.Value = Resources.DateTime;
            _dateLongS.Properties.Value = Resources.Longitude;
            _dateLatS.Properties.Value = Resources.Latitude;
            _vehicleS.Properties.Value = Resources.Transport;
            _indexS.Properties.Value = Resources.NumberPointInTrack;

            Text = Resources.Select2Points;

            _first.Properties.Caption = Resources.FirstPoint;
            _second.Properties.Caption = Resources.LastPoint;

            rgSelectPoint.Properties.Items[0].Description = Resources.FirstPoint;
            rgSelectPoint.Properties.Items[1].Description = Resources.LastPoint;

            peZone.Properties.NullText = Resources.CheckZone;

            sbtExit.Text = Resources.Tuning_Exit;
            sbtZoneCreate.Text = Resources.ZoneCreate; 
        }
        
    }
}