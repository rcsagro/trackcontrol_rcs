using System;
using System.Drawing;
using BaseReports.Procedure;
using DevExpress.Utils;
using TrackControl.General;
using TrackControl.General.Services;
using TrackControl.GMap;
using TrackControl.GMap.UI;
using TrackControl.Properties;
using TrackControl.Vehicles;
using System.Collections.Generic;

namespace TrackControl.Reports.TrackAnalyzer
{
    public class TrackAnalyzerController
    {
        private bool _isOn;
        private ITreeModel<ReportVehicle> _vehicleTree;
        private GoogleMapControl _map;
        private ToolTipController defController;
        private VehiclesModel _vehiclesModel;
        private IGeoPoint _pointPrev;
        private int _trackPrevHashCode;
        private List<Marker> _markerPointsClicked;

        public TrackAnalyzerController(ITreeModel<ReportVehicle> vehicleTree, GoogleMapControl map)
        {
            _vehicleTree = vehicleTree;
            _map = map;
            InitToolTipController();
        }

        public void AssignVehicleModel(VehiclesModel model)
        {
            if (null == model)
                throw new ArgumentNullException("model");
            _vehiclesModel = model;
        }

        private void InitToolTipController()
        {
            // Access the Default ToolTipController. 
            defController = ToolTipController.DefaultController;
            // Customize the controller's settings. 
            defController.Appearance.BackColor = Color.AntiqueWhite;
            defController.Appearance.ForeColor = Color.Black;
            defController.AppearanceTitle.ForeColor = Color.Black;
            defController.Rounded = true;
            defController.ShowBeak = true;
            defController.ShowShadow = true;
            defController.AllowHtmlText = true;
            defController.AutoPopDelay = 10000;
            //defController.ReshowDelay = 10000;
            defController.CloseOnClick = DefaultBoolean.True;
            _markerPointsClicked = new List<Marker>();
        }


        public void AssignVehicle(ReportVehicle vehicle, Point pt)
        {
            if (!_isOn)
                return;

            _map.SelectTrack(vehicle.Mobitel.Id);
            map_OnTrackPointClicked(null, vehicle, 0, pt);
        }

        public void Start()
        {
            if (_isOn)
                return;
            _isOn = true;
            _map.IsTrackAnalizer = true;
            _map.OnTrackPointClicked += map_OnTrackPointClicked;
            ReportVehicle rv = _vehicleTree.Current;
            if (null != rv)
                AssignVehicle(rv, new Point(0, 0));
        }

        public void Stop()
        {
            if (!_isOn)
                return;
            _map.OnTrackPointClicked -= map_OnTrackPointClicked;
            _isOn = false;
            _map.IsTrackAnalizer = false;
            _map.Repaint();
        }

        private void map_OnTrackPointClicked(GMapTrack tr, IVehicle vehicle, int indexInArray, Point pnt)
        {
            ReportVehicle rv = null;
            string VehicleInfo = Resources.VehicleNotSelected;

            if (null != vehicle)
            {
                rv = (ReportVehicle) vehicle;
                VehicleInfo = String.Format("<size=+4>{0} \"{1}\"</size>", rv.RegNumber,
                    String.Format("{0} {1}", rv.CarMaker, rv.CarModel));
            }

            if (tr == null) 
                return;

            if (indexInArray >= 0)
            {
                IGeoPoint point = tr.GeoPoints[indexInArray];

                defController.ShowHint( GetToolTipDetaled( tr, vehicle, point ), pnt );
                // defController.ShowHint(GetToolTipDetaled(tr, vehicle, point), VehicleInfo);
                
                Marker _markerPoint = new Marker(MarkerType.BallGreen, 0,
                    new PointLatLng(point.LatLng.Lat, point.LatLng.Lng), "", "");

                _map.AddMarker(_markerPoint);
                _markerPointsClicked.Add(_markerPoint);

                if (point != null && _markerPointsClicked.Count == 3)
                {
                    _map.RemoveMarker(_markerPointsClicked[0]);
                    _markerPointsClicked.Remove(_markerPointsClicked[0]);
                }

                _pointPrev = point;
                _trackPrevHashCode = tr.GetHashCode();
            } // if
        } // map_OnTrackPointClicked

        private string GetToolTipDetaled(GMapTrack tr, IVehicle vehicle, IGeoPoint point)
        {
            string toolTip;
            GpsData dataGps;
            GetToolTipDetaledTotalInfo(point, out toolTip, out dataGps);
            toolTip = GetToolTipDetaledSensorsInfo(vehicle, toolTip, dataGps);
            toolTip = GetToolTipDetaledBetweenPointsInfo(toolTip, tr, dataGps);
            return toolTip;
        }

        private string GetToolTipDetaledBetweenPointsInfo(string toolTip, GMapTrack tr, GpsData dataGps)
        {
            if (_pointPrev != null && TrackDontChange(tr))
            {
                GpsData dataGpsStart = (GpsData) _pointPrev;
                GpsData dataGpsEnd = dataGps;
                if (dataGpsStart.Time > dataGpsEnd.Time)
                {
                    dataGpsEnd = dataGpsStart;
                    dataGpsStart = dataGps;
                }
                double distTrack = 0;
                TimeSpan tsMove = TimeSpan.Zero;
                SetTrackDistance(tr, dataGpsStart, dataGpsEnd, ref distTrack, ref tsMove);
                TimeSpan ts = dataGpsEnd.Time.Subtract(dataGpsStart.Time);
                double distDirect = —GeoDistance.GetDistance(dataGpsStart.LatLng, dataGpsEnd.LatLng);
                ;
                toolTip +=
                    string.Format(
                        "{0}<b> <color=#696969>{1}: </b>{0} {2}: {3} ({13}: {12}){0} {4}: {5} {15}{0} {6}: {7} {14}{0} {8}: {9} {15}{0} {10}: {11} {14}",
                        Environment.NewLine,
                        Resources.BetweenPoints,
                        Resources.Time,
                        ts,
                        Resources.DistanceAroundTrack,
                        Math.Round(distTrack, 2),
                        Resources.AvgSpeedTrack,
                        tsMove.TotalHours > 0 ? Math.Round(distTrack/tsMove.TotalHours, 3) : 0,
                        Resources.DistanceInLine,
                        Math.Round(distDirect, 2),
                        Resources.AvgSpeedLine,
                        tsMove.TotalHours > 0 ? Math.Round(distDirect/tsMove.TotalHours, 3) : 0,
                        tsMove,
                        Resources.MovementIncluding,
                        Resources.SpeedUnit,
                        Resources.Km);
            }
            return toolTip;
        }

        private bool TrackDontChange(GMapTrack tr)
        {
            if (tr.GetHashCode() == _trackPrevHashCode)
                return true;
            else
                return false;
        }

        private void SetTrackDistance(GMapTrack tr, GpsData dataGpsStart, GpsData dataGpsEnd, ref double distTrack,
            ref TimeSpan tsMove)
        {
            GpsData dataPrev = null;
            foreach (IGeoPoint point in tr.GeoPoints)
            {
                GpsData data = (GpsData) point;
                if (data.Time > dataGpsStart.Time)
                {
                    distTrack += data.Dist;
                    if (dataPrev != null && data.Dist > 0) tsMove += data.Time.Subtract(dataPrev.Time);
                }
                if (data.Time >= dataGpsEnd.Time) break;
                dataPrev = data;
            }
        }

        private string GetToolTipDetaledSensorsInfo(IVehicle vehicle, string toolTip, GpsData dataGps)
        {
            if (vehicle != null && vehicle.Sensors != null)
            {
                vehicle.LastPoint = dataGps;
                VehicleSensors vs = new VehicleSensors(vehicle);
                vs.AssignVehicleModel(_vehiclesModel);
                vs.SetSensorValues();
                foreach (Sensor s in vehicle.Sensors)
                {
                    toolTip += string.Format("<b>{1}: </b>{2}{0}", Environment.NewLine, s.Name, s.SensorValue);
                }
            }
            return toolTip;
        }

        private void GetToolTipDetaledTotalInfo(IGeoPoint point, out string toolTip, out GpsData dataGps)
        {
            dataGps = (GpsData) point;
            toolTip =
                string.Format(
                    @"{0}<b>{1}: </b>{2}{0}<b>{3}: </b>{4}{0}<b>{5}: </b>{6}{0}<b>{7}: </b>{8}{0}<b>{9}: </b>{10}{0}{0}<b>{11}: </b>{12}{0}<b>{13}: </b>{14}{0}{0}",
                    Environment.NewLine
                    , Resources.DateTime, dataGps.Time
                    , Resources.Event, MobitelEventsChecker.CheckEvents(dataGps.Events)
                    , Resources.SpeedCol, Math.Round(dataGps.Speed, 2)
                    , string.Format("{0} {1}", Resources.Acceleration, Resources.Mc2), Math.Round(dataGps.Accel, 2)
                    , Resources.Location, Algorithm.FindLocation(dataGps.LatLng)
                    , "DataGps_Id", dataGps.Id
                    , "LogId", dataGps.LogId
                    );
        }

        private void handleSensorsState(IVehicle vehicle, GpsData dataGps)
        {
            vehicle.LastPoint = dataGps;
            VehicleSensors vehSensors = new VehicleSensors(vehicle);
            vehSensors.AssignVehicleModel(_vehiclesModel);
            vehSensors.SetSensorValues();
        }
    }
}
