namespace TrackControl.Reports
{
  partial class VehicleTreeView
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VehicleTreeView));
            this._barManager = new DevExpress.XtraBars.BarManager(this.components);
            this._tools = new DevExpress.XtraBars.Bar();
            this.bbiExpand = new DevExpress.XtraBars.BarButtonItem();
            this.bbiCollapce = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSynhro = new DevExpress.XtraBars.BarButtonItem();
            this._funnelBox = new DevExpress.XtraBars.BarEditItem();
            this._funnelRepo = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this._eraseBtn = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this._tree = new DevExpress.XtraTreeList.TreeList();
            this._titleCol = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this._descriptionCol = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.timeCol = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.rcbTime = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.timeImages = new DevExpress.Utils.ImageCollection(this.components);
            this.categCol = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this._loginCol = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this._images = new DevExpress.Utils.ImageCollection(this.components);
            this.ttcTree = new DevExpress.Utils.ToolTipController(this.components);
            ((System.ComponentModel.ISupportInitialize)(this._barManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._funnelRepo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._tree)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rcbTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeImages)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._images)).BeginInit();
            this.SuspendLayout();
            // 
            // _barManager
            // 
            this._barManager.AllowCustomization = false;
            this._barManager.AllowMoveBarOnToolbar = false;
            this._barManager.AllowQuickCustomization = false;
            this._barManager.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this._tools});
            this._barManager.DockControls.Add(this.barDockControlTop);
            this._barManager.DockControls.Add(this.barDockControlBottom);
            this._barManager.DockControls.Add(this.barDockControlLeft);
            this._barManager.DockControls.Add(this.barDockControlRight);
            this._barManager.Form = this;
            this._barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this._funnelBox,
            this._eraseBtn,
            this.bbiExpand,
            this.bbiCollapce,
            this.bbiSynhro});
            this._barManager.MaxItemId = 5;
            this._barManager.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this._funnelRepo});
            // 
            // _tools
            // 
            this._tools.BarName = "Tools";
            this._tools.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Top;
            this._tools.DockCol = 0;
            this._tools.DockRow = 0;
            this._tools.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this._tools.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiExpand),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiCollapce),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSynhro),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.Width, this._funnelBox, "", true, true, true, 100),
            new DevExpress.XtraBars.LinkPersistInfo(this._eraseBtn)});
            this._tools.OptionsBar.AllowQuickCustomization = false;
            this._tools.OptionsBar.DisableClose = true;
            this._tools.OptionsBar.DisableCustomization = true;
            this._tools.OptionsBar.DrawDragBorder = false;
            this._tools.OptionsBar.UseWholeRow = true;
            this._tools.Text = "Tools";
            // 
            // bbiExpand
            // 
            this.bbiExpand.Caption = "E";
            this.bbiExpand.Id = 2;
            this.bbiExpand.Name = "bbiExpand";
            this.bbiExpand.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiExpand_ItemClick);
            // 
            // bbiCollapce
            // 
            this.bbiCollapce.Caption = "C";
            this.bbiCollapce.Id = 3;
            this.bbiCollapce.Name = "bbiCollapce";
            this.bbiCollapce.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiCollapce_ItemClick);
            // 
            // bbiSynhro
            // 
            this.bbiSynhro.Caption = "S";
            this.bbiSynhro.Id = 4;
            this.bbiSynhro.Name = "bbiSynhro";
            this.bbiSynhro.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSynhro_ItemClick);
            // 
            // _funnelBox
            // 
            this._funnelBox.Caption = "������";
            this._funnelBox.Edit = this._funnelRepo;
            this._funnelBox.Hint = "������� ������� ���������� �� ���������������� ������";
            this._funnelBox.Id = 0;
            this._funnelBox.Name = "_funnelBox";
            this._funnelBox.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // _funnelRepo
            // 
            this._funnelRepo.AutoHeight = false;
            this._funnelRepo.MaxLength = 20;
            this._funnelRepo.Name = "_funnelRepo";
            this._funnelRepo.KeyUp += new System.Windows.Forms.KeyEventHandler(this.funnelRepo_KeyUp);
            // 
            // _eraseBtn
            // 
            this._eraseBtn.Caption = "��������";
            this._eraseBtn.Glyph = ((System.Drawing.Image)(resources.GetObject("_eraseBtn.Glyph")));
            this._eraseBtn.Hint = "�������� ���������� �������";
            this._eraseBtn.Id = 1;
            this._eraseBtn.Name = "_eraseBtn";
            this._eraseBtn.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.eraseBtn_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(364, 31);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 458);
            this.barDockControlBottom.Size = new System.Drawing.Size(364, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 31);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 427);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(364, 31);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 427);
            // 
            // _tree
            // 
            this._tree.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this._titleCol,
            this._descriptionCol,
            this.timeCol,
            this.categCol,
            this._loginCol});
            this._tree.Dock = System.Windows.Forms.DockStyle.Fill;
            this._tree.ImageIndexFieldName = "IconIndex";
            this._tree.Location = new System.Drawing.Point(0, 31);
            this._tree.Margin = new System.Windows.Forms.Padding(0);
            this._tree.Name = "_tree";
            this._tree.OptionsBehavior.AllowIndeterminateCheckState = true;
            this._tree.OptionsBehavior.Editable = false;
            this._tree.OptionsBehavior.EnableFiltering = true;
            this._tree.OptionsFind.ShowCloseButton = false;
            this._tree.OptionsFind.ShowFindButton = false;
            this._tree.OptionsLayout.RemoveOldColumns = true;
            this._tree.OptionsLayout.StoreAppearance = true;
            this._tree.OptionsMenu.EnableFooterMenu = false;
            this._tree.OptionsSelection.EnableAppearanceFocusedCell = false;
            this._tree.OptionsView.ShowCheckBoxes = true;
            this._tree.OptionsView.ShowIndicator = false;
            this._tree.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.rcbTime});
            this._tree.SelectImageList = this._images;
            this._tree.Size = new System.Drawing.Size(364, 427);
            this._tree.TabIndex = 4;
            this._tree.ToolTipController = this.ttcTree;
            this._tree.AfterFocusNode += new DevExpress.XtraTreeList.NodeEventHandler(this.tree_AfterFocusNode);
            this._tree.BeforeCheckNode += new DevExpress.XtraTreeList.CheckNodeEventHandler(this.tree_BeforeCheckNode);
            this._tree.AfterCheckNode += new DevExpress.XtraTreeList.NodeEventHandler(this.tree_AfterCheckNode);
            this._tree.CustomDrawNodeCell += new DevExpress.XtraTreeList.CustomDrawNodeCellEventHandler(this.tree_CustomDrawNodeCell);
            this._tree.CustomDrawNodeImages += new DevExpress.XtraTreeList.CustomDrawNodeImagesEventHandler(this.tree_CustomDrawNodeImages);
            this._tree.VirtualTreeGetChildNodes += new DevExpress.XtraTreeList.VirtualTreeGetChildNodesEventHandler(this.tree_VirtualTreeGetChildNodes);
            this._tree.VirtualTreeGetCellValue += new DevExpress.XtraTreeList.VirtualTreeGetCellValueEventHandler(this.tree_VirtualTreeGetCellValue);
            this._tree.FilterNode += new DevExpress.XtraTreeList.FilterNodeEventHandler(this.tree_FilterNode);
            this._tree.VisibleChanged += new System.EventHandler(this._tree_VisibleChanged);
            this._tree.DoubleClick += new System.EventHandler(this._tree_DoubleClick);
            // 
            // _titleCol
            // 
            this._titleCol.Caption = "��������, �����";
            this._titleCol.FieldName = "Title";
            this._titleCol.MinWidth = 150;
            this._titleCol.Name = "_titleCol";
            this._titleCol.OptionsColumn.AllowEdit = false;
            this._titleCol.OptionsColumn.AllowFocus = false;
            this._titleCol.SortMode = DevExpress.XtraGrid.ColumnSortMode.DisplayText;
            this._titleCol.SortOrder = System.Windows.Forms.SortOrder.Ascending;
            this._titleCol.UnboundType = DevExpress.XtraTreeList.Data.UnboundColumnType.String;
            this._titleCol.Visible = true;
            this._titleCol.VisibleIndex = 0;
            this._titleCol.Width = 153;
            // 
            // _descriptionCol
            // 
            this._descriptionCol.Caption = "��������";
            this._descriptionCol.FieldName = "Description";
            this._descriptionCol.MinWidth = 100;
            this._descriptionCol.Name = "_descriptionCol";
            this._descriptionCol.OptionsColumn.AllowEdit = false;
            this._descriptionCol.OptionsColumn.AllowFocus = false;
            this._descriptionCol.UnboundType = DevExpress.XtraTreeList.Data.UnboundColumnType.String;
            this._descriptionCol.Visible = true;
            this._descriptionCol.VisibleIndex = 1;
            this._descriptionCol.Width = 103;
            // 
            // timeCol
            // 
            this.timeCol.ColumnEdit = this.rcbTime;
            this.timeCol.Name = "timeCol";
            this.timeCol.OptionsColumn.AllowEdit = false;
            this.timeCol.OptionsColumn.AllowFocus = false;
            this.timeCol.OptionsColumn.AllowMove = false;
            this.timeCol.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.timeCol.OptionsColumn.AllowSize = false;
            this.timeCol.OptionsColumn.FixedWidth = true;
            this.timeCol.OptionsColumn.ReadOnly = true;
            this.timeCol.OptionsColumn.ShowInCustomizationForm = false;
            this.timeCol.OptionsFilter.AllowFilter = false;
            this.timeCol.Visible = true;
            this.timeCol.VisibleIndex = 2;
            this.timeCol.Width = 20;
            // 
            // rcbTime
            // 
            this.rcbTime.AutoHeight = false;
            this.rcbTime.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rcbTime.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("�������", 0, 0),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("�� ������� �����", 1, 1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("�� ������� �����", 2, 2),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("�� ������ �� ����������", 3, 3)});
            this.rcbTime.Name = "rcbTime";
            this.rcbTime.SmallImages = this.timeImages;
            // 
            // timeImages
            // 
            this.timeImages.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("timeImages.ImageStream")));
            this.timeImages.Images.SetKeyName(0, "status.png");
            this.timeImages.Images.SetKeyName(1, "status-away.png");
            this.timeImages.Images.SetKeyName(2, "status-busy.png");
            this.timeImages.Images.SetKeyName(3, "status-offline.png");
            // 
            // categCol
            // 
            this.categCol.Caption = "���������";
            this.categCol.FieldName = "Category";
            this.categCol.MinWidth = 50;
            this.categCol.Name = "categCol";
            this.categCol.OptionsColumn.AllowEdit = false;
            this.categCol.OptionsColumn.AllowFocus = false;
            this.categCol.Visible = true;
            this.categCol.VisibleIndex = 3;
            this.categCol.Width = 50;
            // 
            // _loginCol
            // 
            this._loginCol.AppearanceCell.Options.UseTextOptions = true;
            this._loginCol.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._loginCol.AppearanceHeader.Options.UseTextOptions = true;
            this._loginCol.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._loginCol.Caption = "�����";
            this._loginCol.FieldName = "Login";
            this._loginCol.MinWidth = 30;
            this._loginCol.Name = "_loginCol";
            this._loginCol.OptionsColumn.AllowEdit = false;
            this._loginCol.OptionsColumn.AllowFocus = false;
            this._loginCol.UnboundType = DevExpress.XtraTreeList.Data.UnboundColumnType.String;
            this._loginCol.Visible = true;
            this._loginCol.VisibleIndex = 4;
            this._loginCol.Width = 40;
            // 
            // _images
            // 
            this._images.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("_images.ImageStream")));
            // 
            // ttcTree
            // 
            this.ttcTree.GetActiveObjectInfo += new DevExpress.Utils.ToolTipControllerGetActiveObjectInfoEventHandler(this.ttcTree_GetActiveObjectInfo);
            // 
            // VehicleTreeView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._tree);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "VehicleTreeView";
            this.Size = new System.Drawing.Size(364, 458);
            this.Load += new System.EventHandler(this.this_Load);
            ((System.ComponentModel.ISupportInitialize)(this._barManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._funnelRepo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._tree)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rcbTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeImages)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._images)).EndInit();
            this.ResumeLayout(false);

    }

    #endregion

    private DevExpress.XtraBars.BarManager _barManager;
    private DevExpress.XtraBars.Bar _tools;
    private DevExpress.XtraBars.BarDockControl barDockControlTop;
    private DevExpress.XtraBars.BarDockControl barDockControlBottom;
    private DevExpress.XtraBars.BarDockControl barDockControlLeft;
    private DevExpress.XtraBars.BarDockControl barDockControlRight;
    private DevExpress.XtraTreeList.TreeList _tree;
    private DevExpress.XtraTreeList.Columns.TreeListColumn _titleCol;
    private DevExpress.XtraTreeList.Columns.TreeListColumn _descriptionCol;
    private DevExpress.XtraTreeList.Columns.TreeListColumn _loginCol;
    private DevExpress.Utils.ImageCollection _images;
    private DevExpress.XtraBars.BarEditItem _funnelBox;
    private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit _funnelRepo;
    private DevExpress.XtraBars.BarButtonItem _eraseBtn;
    private DevExpress.XtraBars.BarButtonItem bbiExpand;
    private DevExpress.XtraBars.BarButtonItem bbiCollapce;
    private DevExpress.XtraTreeList.Columns.TreeListColumn timeCol;
    private DevExpress.Utils.ImageCollection timeImages;
    private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox rcbTime;
    private DevExpress.Utils.ToolTipController ttcTree;
    private DevExpress.XtraBars.BarButtonItem bbiSynhro;
    private DevExpress.XtraTreeList.Columns.TreeListColumn categCol;
  }
}
