﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;
using TrackControl.General;
using TrackControl.General.DAL;
using TrackControl.General.DatabaseDriver;
using TrackControl.MySqlDal;
using TrackControl.Notification;
using TrackControl.Vehicles;

namespace TrackControl.Reports
{
    public class PointsAnalyzer
    {

        #region Поля
        /// <summary>
        /// Всего точек
        /// </summary>
        int _pointsTotal;

        /// <summary>
        /// mobitel id 
        /// </summary>
        int MobitelId;

        /// <summary>
        /// список полей значений датчика
        /// </summary>
        List<ValuerSensors> lstVlSens = new List<ValuerSensors>();

        public List<ValuerSensors> ListSensorsData
        {
            get { return lstVlSens; }
        }

        public int PointsTotal
        {
            get { return _pointsTotal; }
        }
        /// <summary>
        /// Точек пропущено (кол-во, процент)
        /// </summary>
        public string PointsSkip
        {
            get { return _pointsTotal == 0 ? "0" : _pointsSkip.ToString() + " (" + Math.Round((100 * (double)_pointsSkip / _pointsTotal), 2).ToString() + "%)"; }
        }
        public int PointsSkipCount
        {
            get { return _pointsSkip; }
        }
        public double PointsSkipPercent
        {
            get { return _pointsTotal == 0 ? 0 : Math.Round((100 * (double)_pointsSkip / _pointsTotal), 2); }
        }
        int _pointsSkip;
        public List<QueryLostData> MobitelLostData
        {
            get { return _MobitelLostData; }
        }
        /// <summary>
        /// Невалидных точек (кол-во, процент)
        /// </summary>
        int _novalid;

        bool _createDataForFormAnaliz;

        public string PointsNoValid
        {
            get { return _pointsTotal == 0 ? "0" : _novalid.ToString() + " (" + Math.Round((100 * (double)_novalid / _pointsTotal), 2).ToString() + "%)"; }
        }
        public double PointsNoValidPercent
        {
            get { return _pointsTotal == 0 ? 0 : Math.Round((100 * (double)_novalid / _pointsTotal), 2); }
        }
        public double PointsMaxInterval
        {
            get { return Math.Round(_ts_max.TotalMinutes, 2); }
        }
        string _sSql;
        Int64 _newConfirmedId = 0;
        /// <summary>
        /// Максимальный интервал между точками (минут) 
        /// </summary>
        TimeSpan _ts_max = TimeSpan.Zero;
        /// <summary>
        /// данные для дозапроса
        /// </summary>
        List<QueryLostData> _MobitelLostData = new List<QueryLostData>();
        bool _existNegativLodId;

        public bool ExistNegativLodId
        {
            get { return _existNegativLodId; }
            set { _existNegativLodId = value; }
        }

        public const int StartBreak = 2;

        #endregion

        #region События
        public event ProgressChanged ProgressStarted;
        public event Action<int> ProgressChanged;
        public event MethodInvoker ProgressFinished;
        #endregion

        public PointsAnalyzer()
        {
        }

        public void PointsAnaliz(DataTable dt, int MobitelId)
        {
                if (dt.Rows.Count == 0)
                    return;

                dt.Rows[0]["Break"] = 0;

                _pointsTotal = 1;
                _novalid = 0;
                _pointsSkip = 0;
                _ts_max = TimeSpan.Zero;

                var prevPoint = new PointLatLng();
                var point = new PointLatLng();

                string valbool = dt.Rows[0]["Valid"].ToString();
                if (valbool == "False" || valbool == "0" || valbool == "false")
                    valbool = "False";
                else
                {
                    valbool = "True";
                }

                bool valid = Convert.ToBoolean(valbool);
                dt.Rows[0]["ValidInt"] = valid;
                dt.Rows[0]["Dist"] = 0;
                dt.Rows[0]["SpeedCalc"] = 0;

                double lat = 0.0;
                double lon = 0.0;

                if (dt.Rows[0]["Lat"] != DBNull.Value)
                    lat = Convert.ToDouble(dt.Rows[0]["Lat"]);
                if (dt.Rows[0]["Lon"] != DBNull.Value)
                    lon = Convert.ToDouble(dt.Rows[0]["Lon"]);

                prevPoint = new PointLatLng(lat, lon);

                for (int i = 1; i < dt.Rows.Count; i++)
                {
                    if (_createDataForFormAnaliz)
                    {
                        CreateServiceData((DateTime)dt.Rows[i - 1]["UT"], ref prevPoint, ref point, dt.Rows[i]);
                    }

                    string valida = dt.Rows[i]["Valid"].ToString();
                    string validator = dt.Rows[i]["Valid"].ToString();

                    bool notValid = validator == "0" || valida == "False";

                    string valida1 = dt.Rows[i - 1]["Valid"].ToString();
                    string validator1 = dt.Rows[i - 1]["Valid"].ToString();

                    bool notValidPrev = validator1 == "0" || valida1 == "False";

                    if (notValid)
                        _novalid++;

                    int diffLogId = Math.Abs(Convert.ToInt32(dt.Rows[i - 1]["LogID"]) - Convert.ToInt32(dt.Rows[i]["LogID"]));

                    if (!notValid && !notValidPrev && diffLogId != 1)
                    {
                        _pointsSkip += diffLogId;

                        if (_createDataForFormAnaliz)
                        {
                            dt.Rows[i]["Break"] = 1;
                            dt.Rows[i - 1]["Break"] = StartBreak;

                            if (IsIntervalValid(dt, i))
                            {
                                var qld = new QueryLostData
                                {
                                    Begin_LogID = Convert.ToInt32(dt.Rows[i]["LogID"]),
                                    End_LogID = Convert.ToInt32(dt.Rows[i - 1]["LogID"]),
                                    Begin_SrvPacketID = (Int64)dt.Rows[i]["SrvPacketID"],
                                    End_SrvPacketID = (Int64)dt.Rows[i - 1]["SrvPacketID"],
                                    Mobitel_ID = MobitelId
                                };
                                _MobitelLostData.Add(qld);
                            }
                        }
                    }
                    else
                    {
                        dt.Rows[i]["Break"] = 0;
                    }

                    _pointsTotal += diffLogId;
                    TimeSpan tsWork = ((DateTime)dt.Rows[i - 1]["UT"]).Subtract((DateTime)dt.Rows[i]["UT"]);

                    if (tsWork.Subtract(_ts_max).TotalSeconds > 0)
                    {
                        _ts_max = tsWork;
                    }

                    if (!_existNegativLodId && Convert.ToInt32(dt.Rows[i - 1]["LogID"]) < 0)
                        _existNegativLodId = true;

                    if (_createDataForFormAnaliz)
                    {
                        object value = dt.Rows[i]["Valid"];
                        dt.Rows[i]["ValidInt"] = value;
                        if ((int)dt.Rows[i]["ValidInt"] == 0)
                            dt.Rows[i]["ValidIntNo"] = 1;
                    }
                } // for

        } // PointsAnalizis

        private bool IsIntervalValid(DataTable dt,int i)
        {
            int diffLogId = (int) dt.Rows[i]["LogID"] - (int) dt.Rows[i - 1]["LogID"];
            if (diffLogId >0)
            {
                TimeSpan tsWork = ((DateTime)dt.Rows[i]["UT"]).Subtract((DateTime)dt.Rows[i - 1]["UT"]);
                if (tsWork.TotalSeconds > diffLogId) return true;
            }
            return false;
        }

        private void CreateServiceData(DateTime prevTime, ref PointLatLng prevPoint, ref PointLatLng point, DataRow dr)
        {
            double lat = Convert.ToDouble(dr["Lat"]);
            double lon = Convert.ToDouble(dr["Lon"]);
            point = new PointLatLng(lat, lon);
            dr["Dist"] = Math.Round(СGeoDistance.GetDistance(prevPoint, point), 2);
            double timeDelta = prevTime.Subtract(((DateTime)dr["UT"])).TotalHours;
            if (timeDelta != 0)
            {
                dr["SpeedCalc"] = Math.Abs(Math.Round((double)dr["Dist"] / timeDelta, 2));
            }
            prevPoint = point;
        }

        public DataTable GetData(DateTime dateForAnaliz, int MobitelId)
        {
          //using (ConnectMySQL _cn = new ConnectMySQL())
            using(DriverDb db = new DriverDb())
            {
                db.ConnectDb();
                var veh = new Vehicle(MobitelId);
                string sqlSelect = string.Format(TrackControlQuery.PointsAnalyzer.SelectDatagps, MobitelId);
                if (veh.Mobitel.Is64BitPackets) sqlSelect = string.Format(TrackControlQuery.PointsAnalyzer.Selectdatagps64, MobitelId);
                //MySqlParameter[] parDate = new MySqlParameter[2];
                db.NewSqlParameterArray(2);
                //parDate[0] = new MySqlParameter("?TimeStart", MySqlDbType.DateTime);
                db.NewSqlParameter( db.ParamPrefics + "TimeStart", db.GettingDateTime(), 0 );
                //parDate[0].Value = new DateTime(dateForAnaliz.Year, dateForAnaliz.Month, dateForAnaliz.Day, 0, 0, 0);
                db.SetSqlParameterValue( new DateTime( dateForAnaliz.Year, dateForAnaliz.Month, dateForAnaliz.Day, 0, 0, 0 ), 0);
                //parDate[1] = new MySqlParameter("?TimeEnd", MySqlDbType.DateTime);
                db.NewSqlParameter( db.ParamPrefics + "TimeEnd", db.GettingDateTime(), 1);
                //parDate[1].Value = new DateTime(dateForAnaliz.Year, dateForAnaliz.Month, dateForAnaliz.Day, 23, 59, 59);
                db.SetSqlParameterValue( new DateTime( dateForAnaliz.Year, dateForAnaliz.Month, dateForAnaliz.Day, 23, 59, 59 ), 1);
                //MySqlDataReader dr = _cn.GetDataReader(sqlSelect, parDate);
                db.GetDataReader( sqlSelect, db.GetSqlParameterArray );
               
                DataTable dt = new DataTable();
                //dt.Load(dr);

                db.LoadDataTable( dt );

                //dr.Close();
                db.CloseDataReader();
                
                return dt;
            } // using
        }

        public void SetValidity(Int64 idGps, int validyty, int mobitelId)
        {
            using (DriverDb db = new DriverDb())
            {
                db.ConnectDb();

                string sql = "";
                Mobitel mobitel = VehicleProvider2.GetMobitel(mobitelId);
                if (mobitel.Is64BitPackets)
                {
                    sql = string.Format(TrackControlQuery.PointsAnalyzer.UpdateValid64, validyty, idGps);
                }
                else
                {
                    sql = string.Format(TrackControlQuery.PointsAnalyzer.UpdateValid, validyty, idGps);
                }
                db.ExecuteNonQueryCommand(sql);
            }
        }

        bool _isData64Packet = false;

        public bool IsDataGps64Packet
        {
            get { return _isData64Packet; }
        }

        public int SetMobitel
        {
            set { MobitelId = value; }
        }

        public IEnumerable ListGridColumn
        {
            get { return lstGridColumns; }
        }

        ValuerSensors valuer = null;
        List<GridColumn> lstGridColumns = new List<GridColumn>(); 

        public DataTable GetDataToForm(GridView forAnaliz, DateTime dateForAnaliz, int mobitelId)
        {
            DateTime start = new DateTime(dateForAnaliz.Year, dateForAnaliz.Month, dateForAnaliz.Day, 0, 0, 0);
            DateTime end = new DateTime(dateForAnaliz.Year, dateForAnaliz.Month, dateForAnaliz.Day, 23, 59, 59);
            IList<GpsData> gpsDatas = GpsDataProvider.GetGpsDataAllProtocolsAllPonts(mobitelId, start, end);
            DataTable dt = new DataTable();
            _isData64Packet = GpsDataProvider.IsGlobal64Packet;
            CreateColumnsForDataDable(dt);
            CreateColumnsForForm(forAnaliz, dt);
            FillDataTable(dt, gpsDatas);
            PointsAnaliz(dt, mobitelId);
            return dt;
        }

        private void FillDataTable(DataTable dt, IList<GpsData> gpsDatas)
        {
            try
            {
                foreach (GpsData gpsData in gpsDatas)
                {
                    var dr = dt.Rows.Add();

                    dr["DataGps_ID"] = gpsData.Id;
                    dr["Lat"] = gpsData.LatLng.Lat;
                    dr["Lon"] = gpsData.LatLng.Lng;
                    dr["UT"] = gpsData.Time;
                    dr["Speed"] = Math.Round(gpsData.Speed, 3);
                    dr["Valid"] = gpsData.Valid;

                    valuer = new ValuerSensors();

                    if (gpsData.Sensors == null)
                    {
                        byte[] snsr = new byte[1] {0};
                        dr["Sensors"] = BitConverter.ToString(snsr);
                        valuer.SetEmpty(snsr);
                    }
                    else
                    {
                        string snsr = BitConverter.ToString(gpsData.Sensors);
                        dr["Sensors"] = snsr;
                        valuer.SetData(gpsData.Sensors, MobitelId);
                    }

                    lstVlSens.Add(valuer);

                    string fild = "Field";

                    for (int i = 0; i < valuer.ListResultSensors.Count; i++ )
                    {
                        SensorResult ls = valuer.ListResultSensors[i];
                        string namefield = fild + i;
                        string namefieldtar = "tar" + fild + i;
                        dr[namefield] = ls.valueSensor;
                        dr[namefieldtar] = ls.tarifValue;
                        ls.field = namefield;
                        ls.tarfield = namefieldtar;
                        lstGridColumns[i * 2].Caption = ls.name;
                        lstGridColumns[i * 2].ToolTip = ls.description;
                        lstGridColumns[i * 2 + 1].Caption = ls.name + " (тарировка)";
                        lstGridColumns[i * 2 + 1].ToolTip = ls.description + "(тарировка)";
                    }

                    dr["LogID"] = gpsData.LogId;
                    dr["SrvPacketID"] = gpsData.SrvPacketID;

                    if (_isData64Packet)
                    {
                        dr["RssiGsm"] = gpsData.RssiGsm;
                        dr["Sattelites"] = gpsData.Satellites;
                        dr["Voltage"] = gpsData.sVoltage;
                        dr["Events"] = gpsData.sEvents;
                    }
                }
            }
            catch (Exception e)
            {
                XtraMessageBox.Show(e.Message + "\n" + e.StackTrace, "Fill data table", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        void CreateColumnsForDataDable(DataTable dt)
        {
            dt.Columns.Add(new DataColumn("DataGps_ID", Type.GetType("System.Int64")));
            dt.Columns.Add(new DataColumn("Lat", Type.GetType("System.Double")));
            dt.Columns.Add(new DataColumn("Lon", Type.GetType("System.Double")));
            dt.Columns.Add(new DataColumn("UT", Type.GetType("System.DateTime")));
            dt.Columns.Add(new DataColumn("Speed", Type.GetType("System.Double")));
            dt.Columns.Add(new DataColumn("Valid", Type.GetType("System.Boolean")));
            dt.Columns.Add(new DataColumn("SrvPacketID", Type.GetType("System.Int64")));
            dt.Columns.Add(new DataColumn("LogID", Type.GetType("System.Int32")));

            if (_isData64Packet)
            {
                dt.Columns.Add(new DataColumn("RssiGsm", Type.GetType("System.Int16")));
                dt.Columns.Add(new DataColumn("Sattelites", Type.GetType("System.Int16")));
                dt.Columns.Add(new DataColumn("Voltage", Type.GetType("System.Double")));
                dt.Columns.Add(new DataColumn("Events", Type.GetType("System.String")));
            }
        }

        void CreateColumnsForForm(GridView forAnaliz, DataTable dt)
        {
            _createDataForFormAnaliz = true;
            dt.Columns.Add(new DataColumn("ValidInt", Type.GetType("System.Int32")));
            dt.Columns.Add(new DataColumn("ValidIntNo", Type.GetType("System.Int32")));
            dt.Columns.Add(new DataColumn("Break", Type.GetType("System.Int32")));
            dt.Columns.Add(new DataColumn("Sensors", Type.GetType("System.String")));
            dt.Columns.Add(new DataColumn("Dist", Type.GetType("System.Double")));
            dt.Columns.Add(new DataColumn("SpeedCalc", Type.GetType("System.Double")));

            // сугубо для отображения реального значения распарсеных датчиков
            string fild = "Field";
            ValuerSensors valuer = new ValuerSensors();

            int numSensors = valuer.getNumberSensors(MobitelId);

            for (int i = 0; i < numSensors; i++)
            {
                string namefield = fild + i;
                string namefieldtar = "tar" + fild + i;
                dt.Columns.Add(new DataColumn(namefield, Type.GetType("System.String")));
                dt.Columns.Add(new DataColumn(namefieldtar, Type.GetType("System.String")));

                GridColumn gridColumn = new GridColumn();
                gridColumn.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Center;
                gridColumn.AppearanceCell.TextOptions.VAlignment = VertAlignment.Center;
                gridColumn.AppearanceCell.TextOptions.WordWrap = WordWrap.NoWrap;
                gridColumn.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center;
                gridColumn.AppearanceHeader.TextOptions.VAlignment = VertAlignment.Center;
                gridColumn.AppearanceHeader.TextOptions.WordWrap = WordWrap.Wrap;
                gridColumn.FieldName = namefield;
                gridColumn.Visible = false;
                gridColumn.VisibleIndex = -1;
                gridColumn.OptionsColumn.AllowEdit = false;
                gridColumn.OptionsColumn.AllowFocus = false;
                gridColumn.OptionsColumn.ReadOnly = false;
                lstGridColumns.Add(gridColumn);
                forAnaliz.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] { gridColumn });
                GridColumn gridColumnTar = new GridColumn();
                gridColumnTar.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Center;
                gridColumnTar.AppearanceCell.TextOptions.VAlignment = VertAlignment.Center;
                gridColumnTar.AppearanceCell.TextOptions.WordWrap = WordWrap.NoWrap;
                gridColumnTar.AppearanceHeader.TextOptions.HAlignment = HorzAlignment.Center;
                gridColumnTar.AppearanceHeader.TextOptions.VAlignment = VertAlignment.Center;
                gridColumnTar.AppearanceHeader.TextOptions.WordWrap = WordWrap.Wrap;
                gridColumnTar.FieldName = namefieldtar;
                gridColumnTar.Visible = false;
                gridColumnTar.VisibleIndex = -1;
                gridColumnTar.OptionsColumn.AllowEdit = false;
                gridColumnTar.OptionsColumn.AllowFocus = false;
                gridColumnTar.OptionsColumn.ReadOnly = false;
                lstGridColumns.Add(gridColumnTar);
                forAnaliz.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] { gridColumnTar });
            } // foreach
        } 

        public void QueryForLostData(List<QueryLostData> mobitelLostData)
        {
            //using (ConnectMySQL _cn = new ConnectMySQL())
            DriverDb db = new DriverDb();
            db.ConnectDb();
            {
                if (mobitelLostData.Count > 0)
                {
                    ProgressStarted("", "", mobitelLostData.Count);
                    _sSql = TrackControlQuery.PointsAnalyzer.TruncateDatagpsbuffer_on;
                   // _cn.ExecuteNonQueryCommand(sSQL);
                    db.ExecuteNonQueryCommand(_sSql);
                    _sSql = TrackControlQuery.PointsAnalyzer.TruncateDatagpslost_on;
                    //_cn.ExecuteNonQueryCommand(sSQL);
                    db.ExecuteNonQueryCommand( _sSql );
                    _sSql = TrackControlQuery.PointsAnalyzer.TruncateDatagpslost_ontmp;
                    //_cn.ExecuteNonQueryCommand(sSQL);
                    db.ExecuteNonQueryCommand( _sSql );

                    int m_id = 0;
                    int cnt = 0;
                    foreach (QueryLostData qld in mobitelLostData)
                    {
                        if (m_id != qld.Mobitel_ID)
                        {
                            if (m_id > 0)
                            {
                                _sSql = string.Format(TrackControlQuery.PointsAnalyzer.UpdateMobitels,_newConfirmedId, m_id);
                                //_cn.ExecuteNonQueryCommand(sSQL);
                                db.ExecuteNonQueryCommand(_sSql);
                            }
                            m_id = qld.Mobitel_ID;
                            _newConfirmedId = qld.Begin_LogID;
                        }
                        //************** Exception Text **************
                        // MySql.Data.MySqlClient.MySqlException: Duplicate entry '100-0' for key 3
                        //------------------------------------------------------------
                        //INDEX IDX_MobitelID USING BTREE (Mobitel_ID),
                        //UNIQUE INDEX IDX_MobitelID_BeginLogID USING BTREE (Mobitel_ID, Begin_LogID),
                        //UNIQUE INDEX IDX_MobitelID_BeginSrvPacketID USING BTREE (Mobitel_ID, Begin_SrvPacketID)
                        int cntUNIQUE = 0;
                        //using (ConnectMySQL _cnR = new ConnectMySQL())
                        DriverDb driverDb = new DriverDb();
                        driverDb.ConnectDb();
                        {
                            _sSql = string.Format(TrackControlQuery.PointsAnalyzer.SelectCount, qld.Mobitel_ID, qld.Begin_SrvPacketID, qld.Mobitel_ID, qld.Begin_LogID);

                            //cntUNIQUE = Convert.ToInt32(_cnR.GetScalarValue(sSQL));
                            cntUNIQUE = Convert.ToInt32( driverDb.GetScalarValue( _sSql ) );
                        }
                        //******************************************
                        if ((cntUNIQUE == 0))
                        {
                            _sSql = string.Format(TrackControlQuery.PointsAnalyzer.InsertIntoDatagpslost_on, qld.Mobitel_ID, qld.Begin_LogID, qld.End_LogID, qld.Begin_SrvPacketID, qld.End_SrvPacketID);

                            //_cn.ExecuteNonQueryCommand(sSQL);
                            db.ExecuteNonQueryCommand(_sSql);

                            if (_newConfirmedId > qld.Begin_LogID) _newConfirmedId = qld.Begin_LogID;
                            //pbAnaliz.Value += 1;
                            Application.DoEvents();
                        }
                        ProgressChanged(cnt++);
                    }
                    if (m_id > 0)
                    {
                        _sSql =  string.Format(TrackControlQuery.PointsAnalyzer.UpdateMobitelsSet, _newConfirmedId, m_id);
                        //_cn.ExecuteNonQueryCommand(sSQL);
                        db.ExecuteNonQueryCommand( _sSql );
                    }
                    ProgressFinished();
                }// If(mobitelLostData.Count >0)
            }
        db.CloseDbConnection();
        }
        /// <summary>
        /// если есть отрицательные значения LogID, то для определения
        ///нового значения ConfirmedID необходим дополнительный анализ:
        ///оцениваем какие значения LogID положительные или отрицательные
        ///более новые (по DataGpsId). 
        /// </summary>
        /// <param name="mobitelId">код тедетрека</param>
        public void QueryForLostDataNegativeLogId(int mobitelId)
        {
            //lbInfor.Text = (string)row.Cells[2].Value;
            //using (ConnectMySQL _cn = new ConnectMySQL())
            DriverDb db = new DriverDb();
            db.ConnectDb();
            {
                Application.DoEvents();

                _sSql = TrackControlQuery.PointsAnalyzer.SelectDataGpsLostOn + mobitelId;

                //if (_cn.GetDataReaderRecordsCount(sSQL) == 0)
                if( db.GetDataReaderRecordsCount( _sSql ) == 0 )
                {
                    _sSql = string.Format(TrackControlQuery.PointsAnalyzer.SelectTop1FromDataGps, mobitelId);

                    //if (_cn.GetDataReaderRecordsCount(sSQL) == 1)//есть отрицательные
                    if( db.GetDataReaderRecordsCount( _sSql ) == 1 )//есть отрицательные
                    {
                        _sSql = string.Format(TrackControlQuery.PointsAnalyzer.SelectMaxDatagps, mobitelId);

                        //Int64 MaxNegativeDataGpsId = Convert.ToInt64(_cn.GetScalarValue(sSQL));
                        Int64 MaxNegativeDataGpsId = Convert.ToInt64( db.GetScalarValue( _sSql ) );
                        Int64 MaxPositiveDataGpsId = 0;

                        _sSql = string.Format(TrackControlQuery.PointsAnalyzer.SelectTop1FromDataGps, mobitelId);

                        //if (_cn.GetDataReaderRecordsCount(sSQL) == 1)//есть положительные
                        if( db.GetDataReaderRecordsCount( _sSql ) == 1 )//есть положительные
                        {
                            _sSql = string.Format(TrackControlQuery.PointsAnalyzer.SelectMaxDatagpsid, mobitelId);

                            //MaxPositiveDataGpsId = Convert.ToInt64(_cn.GetScalarValue(sSQL));
                            MaxPositiveDataGpsId = Convert.ToInt64( db.GetScalarValue( _sSql ) );
                        }
                        if (MaxNegativeDataGpsId > MaxPositiveDataGpsId)
                        {
                            _sSql = string.Format(TrackControlQuery.PointsAnalyzer.SelectCoalesceMaxDatagps, mobitelId);

                            //Console.WriteLine(sSQL);
                            //NewConfirmedID = Convert.ToInt64(_cn.GetScalarValue(sSQL));
                            _newConfirmedId = Convert.ToInt64( db.GetScalarValue( _sSql ) );
                        }
                        else
                        {
                            _sSql = string.Format(TrackControlQuery.PointsAnalyzer.SelectCoalesceMaxLogId, mobitelId);

                            //NewConfirmedID = Convert.ToInt64(_cn.GetScalarValue(sSQL));
                            _newConfirmedId = Convert.ToInt64( db.GetScalarValue( _sSql ) );
                        }
                    }
                    else //только положительные
                    {
                        _sSql = string.Format(TrackControlQuery.PointsAnalyzer.SelectCoalesceDatagps, mobitelId);

                        //NewConfirmedID = Convert.ToInt64(_cn.GetScalarValue(sSQL));
                        _newConfirmedId = Convert.ToInt64( db.GetScalarValue( _sSql ) );
                    }

                    _sSql = string.Format(TrackControlQuery.PointsAnalyzer.UpdateMobitelsSetConfirmed, _newConfirmedId, mobitelId);

                    //_cn.ExecuteNonQueryCommand(sSQL);
                    db.ExecuteNonQueryCommand( _sSql );
                } // if
            }
        db.CloseDbConnection();
        }

        private string ConvertToHexReverce(string sensorByte)
        {
            BitArray bits = new BitArray(BitConverter.GetBytes(Convert.ToByte(sensorByte)));
            string conv = "";
            BitArray bitsRevers = new BitArray(8); 
            for (int i = 0; i < 8; i++)
            {
                bitsRevers[i] = bits[7 - i];
            }
            int[] array = new int[1];
            bitsRevers.CopyTo(array, 0);
            conv = string.Format("{0:x}", Convert.ToInt16(array[0]));
            if (conv.Length == 1) conv = "0" + conv;
            return conv.ToUpper();
        }

        private string ConvertToHex(string sensorByte)
        {
            string conv = string.Format("{0:x}", Convert.ToInt16(sensorByte));
            if (conv.Length == 1) conv = "0" + conv;
            return conv.ToUpper();
        }
    }
}
