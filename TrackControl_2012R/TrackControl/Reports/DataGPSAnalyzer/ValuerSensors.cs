﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BaseReports.EventTrack;
using BaseReports.Procedure;
using BaseReports.Procedure.Calibration;
using LocalCache;

namespace TrackControl.Reports
{
    public class SensorResult
    {
        public string field = "";
        public string name = "";
        public string valueSensor = "";
        public string description = "";
        public string tarifValue = "";
        public string tarfield = "";
    }

    public class ValuerSensors
    {
        public atlantaDataSet AtlantaDataSet { get; set; }

        private List<SensorResult> lstResult = new List<SensorResult>();

        public ValuerSensors()
        {
            AtlantaDataSet = Algorithm.AtlantaDataSet;
        }

        public List<SensorResult> ListResultSensors
        {
            get { return lstResult; }
        }


        public void SetEmpty(byte[] snsr)
        {
           // to do
        }

        public void SetData(byte[] snsrsdata, int mobitelid)
        {
            atlantaDataSet.sensorsRow[] sensorsTable = (atlantaDataSet.sensorsRow[])
                    AtlantaDataSet.sensors.Select(String.Format("mobitel_id = {0}", mobitelid));

            UInt16 identifier = 0;

            if (sensorsTable.Length > 0)
            {
                for (int i = 0; i < sensorsTable.Length; i++)
                {
                    // считаем без тарировочной таблицы
                    atlantaDataSet.sensorsRow sensor = sensorsTable[i];
                    atlantaDataSet.relationalgorithmsRow[] algs = sensor.GetrelationalgorithmsRows();
                    SensorResult snsrsRes = new SensorResult();
                    snsrsRes.name = sensor.Name;
                    snsrsRes.description = sensor.Description;
                    ulong res = Calibrate.ulongSector(snsrsdata, sensor.Length, sensor.StartBit);
                    snsrsRes.valueSensor = Convert.ToUInt64(res).ToString();

                    // считаем с тарировочной таблицей
                    Calibrate calibrate = new Calibrate();
                    foreach (atlantaDataSet.sensorcoefficientRow c_row in sensor.GetsensorcoefficientRows())
                    {
                        Coefficient coefficient = new Coefficient();
                        coefficient.User = c_row.UserValue;
                        coefficient.K = c_row.K;
                        coefficient.b = c_row.b;
                        calibrate.Collection.Add(c_row.SensorValue, coefficient);
                    }

                    snsrsRes.tarifValue = calibrate.GetUserValue(snsrsdata, sensor.Length, sensor.StartBit, sensor.K, sensor.B, sensor.S).ToString();
                     
                    lstResult.Add(snsrsRes);
                } // for
            } // if
        } // SetData

        public int getNumberSensors(int mobitelId)
        {
            atlantaDataSet.sensorsRow[] sensorsTable = (atlantaDataSet.sensorsRow[])
                    AtlantaDataSet.sensors.Select(String.Format("mobitel_id = {0}", mobitelId));
            return sensorsTable.Length;
        }
    }
}
