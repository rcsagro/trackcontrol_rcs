namespace TrackControl.Reports
{
    partial class DataGPSAnalyzer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DataGPSAnalyzer));
            DevExpress.XtraCharts.XYDiagram xyDiagram1 = new DevExpress.XtraCharts.XYDiagram();
            DevExpress.XtraCharts.Series series1 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.SideBySideBarSeriesLabel sideBySideBarSeriesLabel1 = new DevExpress.XtraCharts.SideBySideBarSeriesLabel();
            DevExpress.XtraCharts.PointOptions pointOptions1 = new DevExpress.XtraCharts.PointOptions();
            DevExpress.XtraCharts.SideBySideBarSeriesView sideBySideBarSeriesView1 = new DevExpress.XtraCharts.SideBySideBarSeriesView();
            DevExpress.XtraCharts.Series series2 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.SideBySideBarSeriesLabel sideBySideBarSeriesLabel2 = new DevExpress.XtraCharts.SideBySideBarSeriesLabel();
            DevExpress.XtraCharts.PointOptions pointOptions2 = new DevExpress.XtraCharts.PointOptions();
            DevExpress.XtraCharts.SideBySideBarSeriesView sideBySideBarSeriesView2 = new DevExpress.XtraCharts.SideBySideBarSeriesView();
            DevExpress.XtraCharts.SideBySideBarSeriesLabel sideBySideBarSeriesLabel3 = new DevExpress.XtraCharts.SideBySideBarSeriesLabel();
            this.gcGPS = new DevExpress.XtraGrid.GridControl();
            this.gvGPS = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.DataGps_ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.UT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Latitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Longitude = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Speed = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpeedCalc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Valid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.icbValid = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.icValid = new DevExpress.Utils.ImageCollection(this.components);
            this.LogID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SrvPacketID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Sensors = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDist = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRssiGsm = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSattelites = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVoltage = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEvents = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.pgValid = new DevExpress.XtraVerticalGrid.PropertyGridControl();
            this.crTytle = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.erPointsTotal = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erPointsSkip = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erPointsNoValid = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erPointsMaxInterval = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.chValid = new DevExpress.XtraCharts.ChartControl();
            this.bmAnalizData = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btQuery = new DevExpress.XtraBars.BarButtonItem();
            this.pbAnaliz = new DevExpress.XtraBars.BarEditItem();
            this.pbAnalizEdit = new DevExpress.XtraEditors.Repository.RepositoryItemProgressBar();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.printingSystem1 = new DevExpress.XtraPrinting.PrintingSystem(this.components);
            this.compositeLink1 = new DevExpress.XtraPrintingLinks.CompositeLink(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.gcGPS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvGPS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.icbValid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.icValid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pgValid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chValid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(xyDiagram1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bmAnalizData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbAnalizEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.compositeLink1.ImageCollection)).BeginInit();
            this.SuspendLayout();
            // 
            // gcGPS
            // 
            this.gcGPS.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            gridLevelNode1.RelationName = "Level1";
            this.gcGPS.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gcGPS.Location = new System.Drawing.Point(0, 205);
            this.gcGPS.MainView = this.gvGPS;
            this.gcGPS.Name = "gcGPS";
            this.gcGPS.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.icbValid,
            this.repositoryItemImageComboBox1});
            this.gcGPS.Size = new System.Drawing.Size(865, 292);
            this.gcGPS.TabIndex = 0;
            this.gcGPS.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvGPS});
            // 
            // gvGPS
            // 
            this.gvGPS.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.DataGps_ID,
            this.UT,
            this.Latitude,
            this.Longitude,
            this.Speed,
            this.colSpeedCalc,
            this.Valid,
            this.LogID,
            this.SrvPacketID,
            this.Sensors,
            this.colDist,
            this.colRssiGsm,
            this.colSattelites,
            this.colVoltage,
            this.colEvents});
            this.gvGPS.GridControl = this.gcGPS;
            this.gvGPS.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always;
            this.gvGPS.IndicatorWidth = 35;
            this.gvGPS.Name = "gvGPS";
            this.gvGPS.OptionsSelection.MultiSelect = true;
            this.gvGPS.OptionsView.ColumnAutoWidth = false;
            this.gvGPS.CustomDrawRowIndicator += new DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventHandler(this.gvGPS_CustomDrawRowIndicator);
            this.gvGPS.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gvGPS_CellValueChanged);
            // 
            // DataGps_ID
            // 
            this.DataGps_ID.AppearanceCell.Options.UseTextOptions = true;
            this.DataGps_ID.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DataGps_ID.AppearanceHeader.Options.UseTextOptions = true;
            this.DataGps_ID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DataGps_ID.Caption = "DataGps_ID";
            this.DataGps_ID.FieldName = "DataGps_ID";
            this.DataGps_ID.Name = "DataGps_ID";
            this.DataGps_ID.OptionsColumn.AllowEdit = false;
            this.DataGps_ID.OptionsColumn.AllowFocus = false;
            this.DataGps_ID.OptionsColumn.ReadOnly = true;
            this.DataGps_ID.Visible = true;
            this.DataGps_ID.VisibleIndex = 0;
            this.DataGps_ID.Width = 85;
            // 
            // UT
            // 
            this.UT.AppearanceCell.Options.UseTextOptions = true;
            this.UT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.UT.AppearanceHeader.Options.UseTextOptions = true;
            this.UT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.UT.Caption = "�����";
            this.UT.DisplayFormat.FormatString = "G";
            this.UT.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.UT.FieldName = "UT";
            this.UT.Name = "UT";
            this.UT.OptionsColumn.AllowEdit = false;
            this.UT.OptionsColumn.AllowFocus = false;
            this.UT.OptionsColumn.ReadOnly = true;
            this.UT.Visible = true;
            this.UT.VisibleIndex = 1;
            this.UT.Width = 142;
            // 
            // Latitude
            // 
            this.Latitude.AppearanceCell.Options.UseTextOptions = true;
            this.Latitude.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Latitude.AppearanceHeader.Options.UseTextOptions = true;
            this.Latitude.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Latitude.Caption = "������";
            this.Latitude.FieldName = "Lat";
            this.Latitude.Name = "Latitude";
            this.Latitude.OptionsColumn.AllowEdit = false;
            this.Latitude.OptionsColumn.AllowFocus = false;
            this.Latitude.OptionsColumn.ReadOnly = true;
            this.Latitude.Visible = true;
            this.Latitude.VisibleIndex = 3;
            this.Latitude.Width = 95;
            // 
            // Longitude
            // 
            this.Longitude.AppearanceCell.Options.UseTextOptions = true;
            this.Longitude.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Longitude.AppearanceHeader.Options.UseTextOptions = true;
            this.Longitude.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Longitude.Caption = "�������";
            this.Longitude.FieldName = "Lon";
            this.Longitude.Name = "Longitude";
            this.Longitude.OptionsColumn.AllowEdit = false;
            this.Longitude.OptionsColumn.AllowFocus = false;
            this.Longitude.OptionsColumn.ReadOnly = true;
            this.Longitude.Visible = true;
            this.Longitude.VisibleIndex = 4;
            this.Longitude.Width = 94;
            // 
            // Speed
            // 
            this.Speed.AppearanceCell.Options.UseTextOptions = true;
            this.Speed.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Speed.AppearanceHeader.Options.UseTextOptions = true;
            this.Speed.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Speed.Caption = "��������";
            this.Speed.FieldName = "Speed";
            this.Speed.Name = "Speed";
            this.Speed.OptionsColumn.AllowEdit = false;
            this.Speed.OptionsColumn.AllowFocus = false;
            this.Speed.OptionsColumn.ReadOnly = true;
            this.Speed.Visible = true;
            this.Speed.VisibleIndex = 6;
            this.Speed.Width = 86;
            // 
            // colSpeedCalc
            // 
            this.colSpeedCalc.AppearanceCell.Options.UseTextOptions = true;
            this.colSpeedCalc.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSpeedCalc.AppearanceHeader.Options.UseTextOptions = true;
            this.colSpeedCalc.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSpeedCalc.Caption = "�������� ���";
            this.colSpeedCalc.FieldName = "SpeedCalc";
            this.colSpeedCalc.Name = "colSpeedCalc";
            this.colSpeedCalc.Visible = true;
            this.colSpeedCalc.VisibleIndex = 7;
            this.colSpeedCalc.Width = 90;
            // 
            // Valid
            // 
            this.Valid.AppearanceHeader.Options.UseTextOptions = true;
            this.Valid.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Valid.Caption = "����������";
            this.Valid.ColumnEdit = this.icbValid;
            this.Valid.FieldName = "ValidInt";
            this.Valid.ImageAlignment = System.Drawing.StringAlignment.Center;
            this.Valid.Name = "Valid";
            this.Valid.Visible = true;
            this.Valid.VisibleIndex = 2;
            this.Valid.Width = 92;
            // 
            // icbValid
            // 
            this.icbValid.AutoHeight = false;
            this.icbValid.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.icbValid.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.icbValid.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("����������", 0, 0),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("��������", 1, 1)});
            this.icbValid.Name = "icbValid";
            this.icbValid.NullText = "NULL";
            this.icbValid.ShowDropDown = DevExpress.XtraEditors.Controls.ShowDropDown.DoubleClick;
            this.icbValid.SmallImages = this.icValid;
            // 
            // icValid
            // 
            this.icValid.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("icValid.ImageStream")));
            this.icValid.Images.SetKeyName(0, "cancl_16.gif");
            this.icValid.Images.SetKeyName(1, "check16on.png");
            // 
            // LogID
            // 
            this.LogID.AppearanceCell.Options.UseTextOptions = true;
            this.LogID.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.LogID.AppearanceHeader.Options.UseTextOptions = true;
            this.LogID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.LogID.Caption = "LogID";
            this.LogID.FieldName = "LogID";
            this.LogID.Name = "LogID";
            this.LogID.OptionsColumn.AllowEdit = false;
            this.LogID.OptionsColumn.AllowFocus = false;
            this.LogID.OptionsColumn.ReadOnly = true;
            this.LogID.Visible = true;
            this.LogID.VisibleIndex = 9;
            this.LogID.Width = 81;
            // 
            // SrvPacketID
            // 
            this.SrvPacketID.AppearanceCell.Options.UseTextOptions = true;
            this.SrvPacketID.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SrvPacketID.AppearanceHeader.Options.UseTextOptions = true;
            this.SrvPacketID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SrvPacketID.Caption = "SrvPacketID";
            this.SrvPacketID.FieldName = "SrvPacketID";
            this.SrvPacketID.Name = "SrvPacketID";
            this.SrvPacketID.OptionsColumn.AllowEdit = false;
            this.SrvPacketID.OptionsColumn.AllowFocus = false;
            this.SrvPacketID.OptionsColumn.ReadOnly = true;
            this.SrvPacketID.Visible = true;
            this.SrvPacketID.VisibleIndex = 10;
            this.SrvPacketID.Width = 107;
            // 
            // Sensors
            // 
            this.Sensors.AppearanceCell.Options.UseTextOptions = true;
            this.Sensors.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Sensors.AppearanceHeader.Options.UseTextOptions = true;
            this.Sensors.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Sensors.Caption = "�������";
            this.Sensors.FieldName = "Sensors";
            this.Sensors.Name = "Sensors";
            this.Sensors.OptionsColumn.AllowEdit = false;
            this.Sensors.OptionsColumn.AllowFocus = false;
            this.Sensors.OptionsColumn.ReadOnly = true;
            this.Sensors.Visible = true;
            this.Sensors.VisibleIndex = 8;
            this.Sensors.Width = 170;
            // 
            // colDist
            // 
            this.colDist.AppearanceHeader.Options.UseTextOptions = true;
            this.colDist.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDist.Caption = "����������,��";
            this.colDist.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colDist.FieldName = "Dist";
            this.colDist.Name = "colDist";
            this.colDist.Visible = true;
            this.colDist.VisibleIndex = 5;
            this.colDist.Width = 91;
            // 
            // colRssiGsm
            // 
            this.colRssiGsm.AppearanceCell.Options.UseTextOptions = true;
            this.colRssiGsm.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRssiGsm.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colRssiGsm.AppearanceHeader.Options.UseTextOptions = true;
            this.colRssiGsm.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRssiGsm.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colRssiGsm.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colRssiGsm.Caption = "RssiGsm";
            this.colRssiGsm.FieldName = "RssiGsm";
            this.colRssiGsm.Name = "colRssiGsm";
            this.colRssiGsm.OptionsColumn.AllowEdit = false;
            this.colRssiGsm.OptionsColumn.AllowFocus = false;
            this.colRssiGsm.OptionsColumn.ReadOnly = true;
            this.colRssiGsm.ToolTip = "���������� ������ ������� �������� RssiGsm";
            this.colRssiGsm.Visible = true;
            this.colRssiGsm.VisibleIndex = 11;
            // 
            // colSattelites
            // 
            this.colSattelites.AppearanceCell.Options.UseTextOptions = true;
            this.colSattelites.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSattelites.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colSattelites.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.colSattelites.AppearanceHeader.Options.UseTextOptions = true;
            this.colSattelites.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSattelites.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colSattelites.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colSattelites.Caption = "Sattelites";
            this.colSattelites.FieldName = "Sattelites";
            this.colSattelites.Name = "colSattelites";
            this.colSattelites.OptionsColumn.AllowEdit = false;
            this.colSattelites.OptionsColumn.AllowFocus = false;
            this.colSattelites.OptionsColumn.ReadOnly = true;
            this.colSattelites.ToolTip = "���������� ������������ ���������";
            this.colSattelites.Visible = true;
            this.colSattelites.VisibleIndex = 12;
            // 
            // colVoltage
            // 
            this.colVoltage.AppearanceCell.Options.UseTextOptions = true;
            this.colVoltage.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colVoltage.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colVoltage.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.colVoltage.AppearanceHeader.Options.UseTextOptions = true;
            this.colVoltage.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colVoltage.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colVoltage.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colVoltage.Caption = "Voltage";
            this.colVoltage.FieldName = "Voltage";
            this.colVoltage.Name = "colVoltage";
            this.colVoltage.OptionsColumn.AllowEdit = false;
            this.colVoltage.OptionsColumn.AllowFocus = false;
            this.colVoltage.OptionsColumn.ReadOnly = true;
            this.colVoltage.ToolTip = "���������� ������� � �������";
            this.colVoltage.Visible = true;
            this.colVoltage.VisibleIndex = 13;
            // 
            // colEvents
            // 
            this.colEvents.AppearanceCell.Options.UseTextOptions = true;
            this.colEvents.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colEvents.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colEvents.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.colEvents.AppearanceHeader.Options.UseTextOptions = true;
            this.colEvents.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colEvents.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colEvents.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colEvents.Caption = "Events";
            this.colEvents.FieldName = "Events";
            this.colEvents.Name = "colEvents";
            this.colEvents.OptionsColumn.AllowEdit = false;
            this.colEvents.OptionsColumn.AllowFocus = false;
            this.colEvents.OptionsColumn.ReadOnly = true;
            this.colEvents.ToolTip = "�������� ������� � ����������������� ����";
            this.colEvents.Visible = true;
            this.colEvents.VisibleIndex = 14;
            // 
            // repositoryItemImageComboBox1
            // 
            this.repositoryItemImageComboBox1.AutoHeight = false;
            this.repositoryItemImageComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox1.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", 0, -1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", 1, 0),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", 2, 1)});
            this.repositoryItemImageComboBox1.Name = "repositoryItemImageComboBox1";
            this.repositoryItemImageComboBox1.SmallImages = this.icValid;
            // 
            // panelControl1
            // 
            this.panelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl1.Controls.Add(this.pgValid);
            this.panelControl1.FireScrollEventOnMouseWheel = true;
            this.panelControl1.Location = new System.Drawing.Point(0, 25);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(861, 96);
            this.panelControl1.TabIndex = 1;
            // 
            // pgValid
            // 
            this.pgValid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pgValid.Location = new System.Drawing.Point(2, 2);
            this.pgValid.Name = "pgValid";
            this.pgValid.Rows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.crTytle,
            this.erPointsTotal,
            this.erPointsSkip,
            this.erPointsNoValid,
            this.erPointsMaxInterval});
            this.pgValid.Size = new System.Drawing.Size(857, 92);
            this.pgValid.TabIndex = 0;
            // 
            // crTytle
            // 
            this.crTytle.Name = "crTytle";
            this.crTytle.Properties.Caption = "������� GPS ������";
            // 
            // erPointsTotal
            // 
            this.erPointsTotal.Height = 16;
            this.erPointsTotal.Name = "erPointsTotal";
            this.erPointsTotal.Properties.Caption = "����� ����� ";
            this.erPointsTotal.Properties.FieldName = "PointsTotal";
            this.erPointsTotal.Properties.ReadOnly = true;
            // 
            // erPointsSkip
            // 
            this.erPointsSkip.Name = "erPointsSkip";
            this.erPointsSkip.Properties.Caption = "����� ��������� (���-��, �������)";
            this.erPointsSkip.Properties.FieldName = "PointsSkip";
            this.erPointsSkip.Properties.ReadOnly = true;
            // 
            // erPointsNoValid
            // 
            this.erPointsNoValid.Name = "erPointsNoValid";
            this.erPointsNoValid.Properties.Caption = "���������� ����� (���-��, �������) ";
            this.erPointsNoValid.Properties.FieldName = "PointsNoValid";
            this.erPointsNoValid.Properties.ReadOnly = true;
            // 
            // erPointsMaxInterval
            // 
            this.erPointsMaxInterval.Enabled = false;
            this.erPointsMaxInterval.Name = "erPointsMaxInterval";
            this.erPointsMaxInterval.Properties.Caption = "������������ �������� ����� ������� (�����)";
            this.erPointsMaxInterval.Properties.FieldName = "PointsMaxInterval";
            this.erPointsMaxInterval.Properties.ReadOnly = true;
            // 
            // panelControl2
            // 
            this.panelControl2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl2.Controls.Add(this.chValid);
            this.panelControl2.Location = new System.Drawing.Point(0, 120);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(861, 83);
            this.panelControl2.TabIndex = 2;
            // 
            // chValid
            // 
            this.chValid.AppearanceNameSerializable = "In A Fog";
            xyDiagram1.AxisX.DateTimeScaleOptions.GridAlignment = DevExpress.XtraCharts.DateTimeGridAlignment.Minute;
            xyDiagram1.AxisX.DateTimeScaleOptions.MeasureUnit = DevExpress.XtraCharts.DateTimeMeasureUnit.Minute;
            xyDiagram1.AxisX.Label.DateTimeOptions.Format = DevExpress.XtraCharts.DateTimeFormat.ShortTime;
            xyDiagram1.AxisX.Range.AlwaysShowZeroLevel = true;
            xyDiagram1.AxisX.Range.ScrollingRange.SideMarginsEnabled = true;
            xyDiagram1.AxisX.Range.SideMarginsEnabled = true;
            xyDiagram1.AxisX.VisibleInPanesSerializable = "-1";
            xyDiagram1.AxisY.Range.AlwaysShowZeroLevel = true;
            xyDiagram1.AxisY.Range.ScrollingRange.SideMarginsEnabled = true;
            xyDiagram1.AxisY.Range.SideMarginsEnabled = true;
            xyDiagram1.AxisY.Visible = false;
            xyDiagram1.AxisY.VisibleInPanesSerializable = "-1";
            this.chValid.Diagram = xyDiagram1;
            this.chValid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chValid.Location = new System.Drawing.Point(2, 2);
            this.chValid.Name = "chValid";
            series1.ArgumentDataMember = "UT";
            series1.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.DateTime;
            sideBySideBarSeriesLabel1.LineVisible = true;
            pointOptions1.ArgumentDateTimeOptions.Format = DevExpress.XtraCharts.DateTimeFormat.ShortTime;
            pointOptions1.ValueNumericOptions.Format = DevExpress.XtraCharts.NumericFormat.Number;
            pointOptions1.ValueNumericOptions.Precision = 0;
            sideBySideBarSeriesLabel1.PointOptions = pointOptions1;
            series1.Label = sideBySideBarSeriesLabel1;
            series1.LabelsVisibility = DevExpress.Utils.DefaultBoolean.False;
            series1.Name = "����������";
            series1.ValueDataMembersSerializable = "ValidIntNo";
            sideBySideBarSeriesView1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            sideBySideBarSeriesView1.FillStyle.FillMode = DevExpress.XtraCharts.FillMode.Solid;
            series1.View = sideBySideBarSeriesView1;
            series2.ArgumentDataMember = "UT";
            series2.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.DateTime;
            sideBySideBarSeriesLabel2.LineVisible = true;
            pointOptions2.ArgumentDateTimeOptions.Format = DevExpress.XtraCharts.DateTimeFormat.ShortTime;
            pointOptions2.ValueNumericOptions.Format = DevExpress.XtraCharts.NumericFormat.Number;
            pointOptions2.ValueNumericOptions.Precision = 0;
            sideBySideBarSeriesLabel2.PointOptions = pointOptions2;
            series2.Label = sideBySideBarSeriesLabel2;
            series2.LabelsVisibility = DevExpress.Utils.DefaultBoolean.False;
            series2.Name = "��������";
            series2.ValueDataMembersSerializable = "Break";
            sideBySideBarSeriesView2.Color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            sideBySideBarSeriesView2.FillStyle.FillMode = DevExpress.XtraCharts.FillMode.Solid;
            series2.View = sideBySideBarSeriesView2;
            this.chValid.SeriesSerializable = new DevExpress.XtraCharts.Series[] {
        series1,
        series2};
            sideBySideBarSeriesLabel3.LineVisible = true;
            this.chValid.SeriesTemplate.Label = sideBySideBarSeriesLabel3;
            this.chValid.Size = new System.Drawing.Size(857, 79);
            this.chValid.TabIndex = 1;
            // 
            // bmAnalizData
            // 
            this.bmAnalizData.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2,
            this.bar3});
            this.bmAnalizData.DockControls.Add(this.barDockControlTop);
            this.bmAnalizData.DockControls.Add(this.barDockControlBottom);
            this.bmAnalizData.DockControls.Add(this.barDockControlLeft);
            this.bmAnalizData.DockControls.Add(this.barDockControlRight);
            this.bmAnalizData.Form = this;
            this.bmAnalizData.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btQuery,
            this.pbAnaliz,
            this.barButtonItem1,
            this.barButtonItem2});
            this.bmAnalizData.MainMenu = this.bar2;
            this.bmAnalizData.MaxItemId = 5;
            this.bmAnalizData.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit1,
            this.pbAnalizEdit});
            this.bmAnalizData.StatusBar = this.bar3;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btQuery, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.Width, this.pbAnaliz, "", false, true, true, 784),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem2)});
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // btQuery
            // 
            this.btQuery.Caption = "�������� ������";
            this.btQuery.Enabled = false;
            this.btQuery.Glyph = ((System.Drawing.Image)(resources.GetObject("btQuery.Glyph")));
            this.btQuery.Id = 0;
            this.btQuery.Name = "btQuery";
            this.btQuery.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btQuery_ItemClick);
            // 
            // pbAnaliz
            // 
            this.pbAnaliz.Edit = this.pbAnalizEdit;
            this.pbAnaliz.Id = 2;
            this.pbAnaliz.Name = "pbAnaliz";
            this.pbAnaliz.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // pbAnalizEdit
            // 
            this.pbAnalizEdit.Name = "pbAnalizEdit";
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "�������� �������";
            this.barButtonItem1.Hint = "��������� ������������ ��������� �������� ��������";
            this.barButtonItem1.Id = 3;
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_ItemClick);
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "������";
            this.barButtonItem2.Hint = "������� ������� � ������ �������";
            this.barButtonItem2.Id = 4;
            this.barButtonItem2.Name = "barButtonItem2";
            this.barButtonItem2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem2_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(865, 24);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 476);
            this.barDockControlBottom.Size = new System.Drawing.Size(865, 23);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 24);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 452);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(865, 24);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 452);
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // printingSystem1
            // 
            this.printingSystem1.Links.AddRange(new object[] {
            this.compositeLink1});
            // 
            // compositeLink1
            // 
            // 
            // 
            // 
            this.compositeLink1.ImageCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("compositeLink1.ImageCollection.ImageStream")));
            this.compositeLink1.Landscape = true;
            this.compositeLink1.Margins = new System.Drawing.Printing.Margins(10, 10, 10, 10);
            this.compositeLink1.MinMargins = new System.Drawing.Printing.Margins(10, 10, 10, 10);
            this.compositeLink1.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.compositeLink1.PrintingSystemBase = this.printingSystem1;
            // 
            // DataGPSAnalyzer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(865, 499);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.gcGPS);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "DataGPSAnalyzer";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "���������";
            ((System.ComponentModel.ISupportInitialize)(this.gcGPS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvGPS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.icbValid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.icValid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pgValid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(xyDiagram1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(sideBySideBarSeriesLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chValid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bmAnalizData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbAnalizEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.compositeLink1.ImageCollection)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gcGPS;
        private DevExpress.XtraGrid.Views.Grid.GridView gvGPS;
        private DevExpress.XtraGrid.Columns.GridColumn DataGps_ID;
        private DevExpress.XtraGrid.Columns.GridColumn UT;
        private DevExpress.XtraGrid.Columns.GridColumn Latitude;
        private DevExpress.XtraGrid.Columns.GridColumn Longitude;
        private DevExpress.XtraGrid.Columns.GridColumn Speed;
        private DevExpress.XtraGrid.Columns.GridColumn Valid;
        private DevExpress.XtraGrid.Columns.GridColumn LogID;
        private DevExpress.XtraGrid.Columns.GridColumn SrvPacketID;
        private DevExpress.XtraGrid.Columns.GridColumn Sensors;
        private DevExpress.Utils.ImageCollection icValid;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox icbValid;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox1;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraVerticalGrid.PropertyGridControl pgValid;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow crTytle;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erPointsTotal;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erPointsSkip;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erPointsMaxInterval;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraCharts.ChartControl chValid;
        private DevExpress.XtraBars.BarManager bmAnalizData;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem btQuery;
        private DevExpress.XtraBars.BarEditItem pbAnaliz;
        private DevExpress.XtraEditors.Repository.RepositoryItemProgressBar pbAnalizEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erPointsNoValid;
        private DevExpress.XtraGrid.Columns.GridColumn colDist;
        private DevExpress.XtraGrid.Columns.GridColumn colSpeedCalc;
        private DevExpress.XtraGrid.Columns.GridColumn colRssiGsm;
        private DevExpress.XtraGrid.Columns.GridColumn colSattelites;
        private DevExpress.XtraGrid.Columns.GridColumn colVoltage;
        private DevExpress.XtraGrid.Columns.GridColumn colEvents;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraPrinting.PrintingSystem printingSystem1;
        private DevExpress.XtraPrintingLinks.CompositeLink compositeLink1;
    }
}