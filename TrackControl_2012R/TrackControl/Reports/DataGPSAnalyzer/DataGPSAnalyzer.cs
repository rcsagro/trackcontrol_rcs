using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrintingLinks;
using TrackControl.General;
using TrackControl.Properties;

namespace TrackControl.Reports
{
    public partial class DataGPSAnalyzer : DevExpress.XtraEditors.XtraForm
    {
        DateTime _dateForAnaliz;
        int _mobitelId;
        PointsAnalyzer _pa;

        public DataGPSAnalyzer(DateTime dateForAnaliz, ReportVehicle rv)
        {
            InitializeComponent();
            Localization();
            this.Text = Resources.Points + " " + rv.CarMaker + " " + rv.CarModel + " " + rv.RegNumber + " : " +
                        dateForAnaliz.ToString("D");
            _dateForAnaliz = dateForAnaliz;
            _mobitelId = rv.Mobitel.Id;
            LoadData();
        }

        bool _isDataGps64Packet = false;
        private bool flagShowFields = false;
        private DataTable _dt = null;
        private int NumberSensorsFields = 20;

        private void LoadData()
        {
            _pa = new PointsAnalyzer();
            _pa.SetMobitel = _mobitelId;
            _dt = _pa.GetDataToForm(gvGPS, _dateForAnaliz, _mobitelId);
            _isDataGps64Packet = _pa.IsDataGps64Packet;
            List<ValuerSensors> lstValSens = _pa.ListSensorsData;

            if(_isDataGps64Packet)
            {
                colEvents.Visible = true;
                colRssiGsm.Visible = true;
                colSattelites.Visible = true;
                colVoltage.Visible = true;
            }
            else
            {
                colEvents.Visible = false;
                colRssiGsm.Visible = false;
                colSattelites.Visible = false;
                colVoltage.Visible = false;
            }

            // �������������� ���� �������
            gcGPS.DataSource = _dt;

            pgValid.SelectedObject = _pa;
            DataRow[] graficRows = _dt.Select("ValidIntNo = 1 OR Break = 1 OR Break = " + PointsAnalyzer.StartBreak);
            DataTable dtGrafic = new DataTable();
            dtGrafic.Columns.Add(new DataColumn("UT", System.Type.GetType("System.DateTime")));
            dtGrafic.Columns.Add(new DataColumn("ValidIntNo", System.Type.GetType("System.Int32")));
            dtGrafic.Columns.Add(new DataColumn("Break", System.Type.GetType("System.Int32")));

            if (graficRows.Length > 0)
            {
                dtGrafic.BeginLoadData();
                for (int i = 0; i < graficRows.Length; i++)
                {
                    AddRowsForLossData(i, graficRows, dtGrafic);
                    if ((int) graficRows[i]["Break"] == PointsAnalyzer.StartBreak) graficRows[i]["Break"] = 0;
                    dtGrafic.Rows.Add(graficRows[i]["UT"], graficRows[i]["ValidIntNo"], graficRows[i]["Break"]);
                }
                dtGrafic.EndLoadData();
            }

            chValid.DataSource = dtGrafic;

            if (_pa.PointsSkipCount > 0 || _pa.ExistNegativLodId)
            {
                btQuery.Enabled = true;
                _pa.ProgressStarted += ProgressStarted;
                _pa.ProgressFinished += ProgressFinished;
                _pa.ProgressChanged += ProgressChanged;
            }
        }

        private static void AddRowsForLossData(int i, DataRow[] graficRows, DataTable dtGrafic)
        {
            if (i > 1 && (int) graficRows[i]["Break"] == 1 && (int) graficRows[i - 1]["Break"] == 2)
            {
                int seconds =
                    (int) ((DateTime) graficRows[i]["UT"]).Subtract((DateTime) graficRows[i - 1]["UT"]).TotalSeconds;
                if (seconds > 0)
                {
                    var dateRow = (DateTime) graficRows[i - 1]["UT"];
                    while (dateRow < (DateTime) graficRows[i]["UT"])
                    {
                        dateRow = dateRow.AddSeconds(30);
                        dtGrafic.Rows.Add(dateRow, 0, 1);
                    }
                }
            }
        }

        private void Localization()
        {
            UT.Caption = Resources.Time;
            Latitude.Caption = Resources.Latitude;
            Longitude.Caption = Resources.Longitude;
            Speed.Caption = Resources.Speed;
            colDist.Caption = Resources.PathKm;
            colSpeedCalc.Caption = Resources.SpeedEstimated;
            Valid.Caption = Resources.Validity;
            Sensors.Caption = Resources.Sensors;
            chValid.Series[0].Name = Resources.NonValid;
            chValid.Series[1].Name = Resources.Missed;
            crTytle.Properties.Caption = Resources.DataCompleteness;
            erPointsTotal.Properties.Caption = Resources.PointsTotal;
            erPointsSkip.Properties.Caption = Resources.Missed;
            erPointsNoValid.Properties.Caption = Resources.NonValid;
            erPointsMaxInterval.Properties.Caption = Resources.MaxInterval + "(" + Resources.MinutesShort + ")";
            btQuery.Caption = Resources.DataRefill;
            colEvents.Caption = Resources.EventsCaption;
            colEvents.ToolTip = Resources.EventsToolTip;
            colRssiGsm.Caption = Resources.RssiGsmCaption;
            colRssiGsm.ToolTip = Resources.RssiGsmToolTip;
            colVoltage.Caption = Resources.VoltageCaption;
            colVoltage.ToolTip = Resources.VoltageToolTip;
            colSattelites.Caption = Resources.SattelitesCaption;
            colSattelites.ToolTip = Resources.SattelitesToolTip;
        }

        private void btQuery_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            QueryData();
        }

        void ProgressStarted(string before, string after, int value)
        {
            pbAnaliz.Visibility = DevExpress.XtraBars.BarItemVisibility.Always;
            pbAnalizEdit.Minimum = 0;
            pbAnalizEdit.Maximum = value;
        }

        void ProgressFinished()
        {
            pbAnaliz.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
        }

        void ProgressChanged(int value)
        {
            pbAnaliz.EditValue = value;
        }

        void QueryData()
        {
            if (_pa.PointsSkipCount > 0) _pa.QueryForLostData(_pa.MobitelLostData);
            if (_pa.ExistNegativLodId) _pa.QueryForLostDataNegativeLogId(_mobitelId);
            LoadData();
        }

        private void gvGPS_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (e.Column.FieldName.ToString() == "ValidInt")
            {
                Int64 idGps;
                if (Int64.TryParse(gvGPS.GetRowCellValue(e.RowHandle, DataGps_ID).ToString(), out idGps))
                {
                    _pa.SetValidity(idGps, (int) e.Value, _mobitelId);
                }
            }
        }

        private void gvGPS_CustomDrawRowIndicator(object sender,
            DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventArgs e)
        {
            if (e.Info.IsRowIndicator)
            {
                e.Info.DisplayText = (e.RowHandle + 1).ToString();
            }
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            flagShowFields = !flagShowFields;

            if (flagShowFields)
            {
                ShowSensorsFields(gvGPS, gcGPS, _dt, _pa.ListSensorsData, _pa.ListGridColumn);
                barButtonItem1.Caption = "������ ���� ��������";
            }
            else
            {
                foreach (GridColumn col in _pa.ListGridColumn)
                {
                    col.Visible = false;
                    col.VisibleIndex = -1;
                }
                barButtonItem1.Caption = "�������� ���� ��������";
            }
        }

        private void ShowSensorsFields(GridView gvGps, GridControl gcGps, DataTable dt, List<ValuerSensors> listSensorsData, IEnumerable listGridColumn)
        {
            int numberIndex = 0;
            foreach (DevExpress.XtraGrid.Columns.GridColumn column in gvGPS.Columns)
            {
                if (column.Visible)
                    numberIndex++;
            }

            foreach (GridColumn col in listGridColumn)
            {
                col.Visible = true;
                col.VisibleIndex = numberIndex++;
            }
        }

        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            PrintingSystem printingSystem = new PrintingSystem();
            CompositeLink compositeLink = new DevExpress.XtraPrintingLinks.CompositeLink();
            System.ComponentModel.ComponentResourceManager resources =
                new System.ComponentModel.ComponentResourceManager(typeof(DataGPSAnalyzer));

            printingSystem.ExportOptions.Html.ExportMode = DevExpress.XtraPrinting.HtmlExportMode.SingleFilePageByPage;
            printingSystem.ExportOptions.Image.ExportMode = DevExpress.XtraPrinting.ImageExportMode.SingleFilePageByPage;
            printingSystem.ExportOptions.Mht.ExportMode = DevExpress.XtraPrinting.HtmlExportMode.SingleFilePageByPage;
            printingSystem.ExportOptions.Xls.ShowGridLines = true;
            printingSystem.ExportOptions.Xlsx.ShowGridLines = true;
            printingSystem.Links.AddRange(new object[] { compositeLink });

            compositeLink.Landscape = true;
            compositeLink.Margins = new System.Drawing.Printing.Margins(25, 25, 70, 40);
            compositeLink.MinMargins = new System.Drawing.Printing.Margins(25, 25, 15, 25);
            compositeLink.PaperKind = System.Drawing.Printing.PaperKind.A4;
            compositeLink.PrintingSystemBase = printingSystem;
            compositeLink.CreateMarginalHeaderArea += new CreateAreaEventHandler(composLink_CreateMarginalHeaderArea);
            compositeLink.CreateMarginalFooterArea += compositeLink_CreateMarginalFooterArea;

            PrintableComponentLink printableComponentLink1 = new PrintableComponentLink();

            XtraGridService.SetupGidViewForPrint(gvGPS, true, true);
            printableComponentLink1.Component = gcGPS;

            compositeLink.Links.Clear();
            compositeLink.Links.Add(printableComponentLink1);
            compositeLink.ShowPreview();
        }

        private void composLink_CreateMarginalHeaderArea(object sender, CreateAreaEventArgs e)
        {
            DevExpressReportSubHeader(this.Text, 20, 35, e);
        }

        protected void DevExpressReportSubHeader(string SubHeader, int height, int vertical, CreateAreaEventArgs e)
        {

            TextBrick brick = e.Graph.DrawString(SubHeader, Color.Black,
                new RectangleF(0, vertical, e.Graph.ClientPageSize.Width, height),
                DevExpress.XtraPrinting.BorderSide.None);
            brick.Font = new Font("Times New Roman", 10, FontStyle.Bold);
            brick.HorzAlignment = DevExpress.Utils.HorzAlignment.Near;
            brick.VertAlignment = DevExpress.Utils.VertAlignment.Center;
            brick.BorderWidth = 0;
            brick.BackColor = Color.Transparent;
            brick.StringFormat = new BrickStringFormat(StringAlignment.Center);
        }

        void compositeLink_CreateMarginalFooterArea(object sender, CreateAreaEventArgs e)
        {
            DevExpressReportFooter(e);
        }

        protected void DevExpressReportFooter(CreateAreaEventArgs e)
        {
            PageInfoBrick brick = e.Graph.DrawPageInfo(PageInfo.NumberOfTotal, "�������� {0} �� {1}", Color.Black, new RectangleF(0, 0, e.Graph.ClientPageSize.Width, 15), BorderSide.None);
            brick.Alignment = BrickAlignment.Far;
            brick.AutoWidth = true;
            brick.Font = new Font("Arial", 10, FontStyle.Regular);
            brick.StringFormat = new BrickStringFormat(StringAlignment.Center);
        }
    }
}