﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TrackControl.Reports
{
    public struct QueryLostData
    {
        /// <summary>
        /// поля таблицы datagpslost_on тип таблицы INNODB
        /// </summary>
        public int Mobitel_ID;
        /// <summary>
        /// Значение LogID после которого начинается разрыв'
        /// </summary>
        public long Begin_LogID;
        /// <summary>
        /// Значение LogID перед которым завершается разрыв
        /// </summary>
        public long End_LogID;
        /// <summary>
        /// Идентификаор серверного пакета в который входит запись с Begin_LogID
        /// </summary>
        public Int64 Begin_SrvPacketID;
        /// <summary>
        /// Идентификаор серверного пакета в который входит запись с End_LogID
        /// </summary>
        public Int64 End_SrvPacketID;
    }
}
