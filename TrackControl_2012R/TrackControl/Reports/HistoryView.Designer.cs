namespace TrackControl.Reports
{
  partial class HistoryView
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HistoryView));
            this._barManager = new DevExpress.XtraBars.BarManager(this.components);
            this._statusBar = new DevExpress.XtraBars.Bar();
            this._vehicleLbl = new DevExpress.XtraBars.BarStaticItem();
            this._stopBtn = new DevExpress.XtraBars.BarButtonItem();
            this._progress = new DevExpress.XtraBars.BarEditItem();
            this._progressRepo = new DevExpress.XtraEditors.Repository.RepositoryItemProgressBar();
            this._tools = new DevExpress.XtraBars.Bar();
            this._daysCount = new DevExpress.XtraBars.BarEditItem();
            this._daysCountRepo = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this._refreshBtn = new DevExpress.XtraBars.BarButtonItem();
            this._analizBtn = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this._asBegin = new DevExpress.XtraBars.BarButtonItem();
            this._asFinish = new DevExpress.XtraBars.BarButtonItem();
            this._asPeriod = new DevExpress.XtraBars.BarButtonItem();
            this._viewDataGps = new DevExpress.XtraBars.BarButtonItem();
            this._listBox = new DevExpress.XtraEditors.ListBoxControl();
            this.ttcList = new DevExpress.Utils.ToolTipController(this.components);
            this._popup = new DevExpress.XtraBars.PopupMenu(this.components);
            this._historyWorker = new System.ComponentModel.BackgroundWorker();
            this.icItemIcons = new DevExpress.Utils.ImageCollection(this.components);
            this.chDateDevice = new DevExpress.XtraBars.BarCheckItem();
            ((System.ComponentModel.ISupportInitialize)(this._barManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._progressRepo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._daysCountRepo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._listBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._popup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.icItemIcons)).BeginInit();
            this.SuspendLayout();
            // 
            // _barManager
            // 
            this._barManager.AllowCustomization = false;
            this._barManager.AllowMoveBarOnToolbar = false;
            this._barManager.AllowQuickCustomization = false;
            this._barManager.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this._statusBar,
            this._tools});
            this._barManager.DockControls.Add(this.barDockControlTop);
            this._barManager.DockControls.Add(this.barDockControlBottom);
            this._barManager.DockControls.Add(this.barDockControlLeft);
            this._barManager.DockControls.Add(this.barDockControlRight);
            this._barManager.Form = this;
            this._barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this._daysCount,
            this._refreshBtn,
            this._progress,
            this._asBegin,
            this._asFinish,
            this._asPeriod,
            this._vehicleLbl,
            this._viewDataGps,
            this._analizBtn,
            this._stopBtn,
            this.chDateDevice});
            this._barManager.MaxItemId = 15;
            this._barManager.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this._daysCountRepo,
            this._progressRepo});
            this._barManager.StatusBar = this._statusBar;
            // 
            // _statusBar
            // 
            this._statusBar.BarName = "Status bar";
            this._statusBar.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this._statusBar.DockCol = 0;
            this._statusBar.DockRow = 0;
            this._statusBar.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this._statusBar.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this._vehicleLbl),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this._stopBtn, DevExpress.XtraBars.BarItemPaintStyle.Standard),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.Width, this._progress, "", false, true, true, 123)});
            this._statusBar.OptionsBar.AllowQuickCustomization = false;
            this._statusBar.OptionsBar.DisableClose = true;
            this._statusBar.OptionsBar.DisableCustomization = true;
            this._statusBar.OptionsBar.DrawDragBorder = false;
            this._statusBar.OptionsBar.UseWholeRow = true;
            this._statusBar.Text = "Status bar";
            // 
            // _vehicleLbl
            // 
            this._vehicleLbl.Border = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this._vehicleLbl.Caption = "Reg Number";
            this._vehicleLbl.Id = 8;
            this._vehicleLbl.Name = "_vehicleLbl";
            this._vehicleLbl.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // _stopBtn
            // 
            this._stopBtn.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this._stopBtn.Caption = "�������";
            this._stopBtn.Glyph = ((System.Drawing.Image)(resources.GetObject("_stopBtn.Glyph")));
            this._stopBtn.Id = 13;
            this._stopBtn.Name = "_stopBtn";
            this._stopBtn.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this._stopBtn.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this._stopBtn_ItemClick);
            // 
            // _progress
            // 
            this._progress.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this._progress.Border = DevExpress.XtraEditors.Controls.BorderStyles.Default;
            this._progress.Caption = "Progress";
            this._progress.Edit = this._progressRepo;
            this._progress.Id = 4;
            this._progress.Name = "_progress";
            this._progress.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // _progressRepo
            // 
            this._progressRepo.Name = "_progressRepo";
            // 
            // _tools
            // 
            this._tools.BarName = "Custom 3";
            this._tools.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Top;
            this._tools.DockCol = 0;
            this._tools.DockRow = 0;
            this._tools.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this._tools.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this._daysCount),
            new DevExpress.XtraBars.LinkPersistInfo(this.chDateDevice, true),
            new DevExpress.XtraBars.LinkPersistInfo(this._refreshBtn),
            new DevExpress.XtraBars.LinkPersistInfo(this._analizBtn, true)});
            this._tools.OptionsBar.AllowQuickCustomization = false;
            this._tools.OptionsBar.DisableClose = true;
            this._tools.OptionsBar.DisableCustomization = true;
            this._tools.OptionsBar.DrawDragBorder = false;
            this._tools.OptionsBar.UseWholeRow = true;
            this._tools.Text = "Custom 3";
            // 
            // _daysCount
            // 
            this._daysCount.Caption = " ���� �������";
            this._daysCount.Edit = this._daysCountRepo;
            this._daysCount.Hint = "���������� ���� �������\n��� ����������� � ������";
            this._daysCount.Id = 2;
            this._daysCount.Name = "_daysCount";
            this._daysCount.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this._daysCount.EditValueChanged += new System.EventHandler(this._daysCount_EditValueChanged);
            // 
            // _daysCountRepo
            // 
            this._daysCountRepo.AutoHeight = false;
            this._daysCountRepo.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this._daysCountRepo.IsFloatValue = false;
            this._daysCountRepo.Mask.EditMask = "N00";
            this._daysCountRepo.MaxLength = 2;
            this._daysCountRepo.MaxValue = new decimal(new int[] {
            62,
            0,
            0,
            0});
            this._daysCountRepo.MinValue = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this._daysCountRepo.Name = "_daysCountRepo";
            // 
            // _refreshBtn
            // 
            this._refreshBtn.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this._refreshBtn.Caption = "��������";
            this._refreshBtn.Glyph = ((System.Drawing.Image)(resources.GetObject("_refreshBtn.Glyph")));
            this._refreshBtn.Hint = "������ ����� ��������\n� ������������ � �����\n��������� ���� �������";
            this._refreshBtn.Id = 3;
            this._refreshBtn.Name = "_refreshBtn";
            this._refreshBtn.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this._refreshBtn_ItemClick);
            // 
            // _analizBtn
            // 
            this._analizBtn.Caption = "������ ������";
            this._analizBtn.Glyph = ((System.Drawing.Image)(resources.GetObject("_analizBtn.Glyph")));
            this._analizBtn.Hint = "����������� ���������� � ��������� � ������";
            this._analizBtn.Id = 11;
            this._analizBtn.Name = "_analizBtn";
            this._analizBtn.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this._analizBtn_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(551, 32);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 324);
            this.barDockControlBottom.Size = new System.Drawing.Size(551, 27);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 32);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 292);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(551, 32);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 292);
            // 
            // _asBegin
            // 
            this._asBegin.Caption = "���������� ��� ������ �������";
            this._asBegin.Glyph = ((System.Drawing.Image)(resources.GetObject("_asBegin.Glyph")));
            this._asBegin.Id = 5;
            this._asBegin.Name = "_asBegin";
            this._asBegin.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this._asBegin_ItemClick);
            // 
            // _asFinish
            // 
            this._asFinish.Caption = "���������� ��� ����� �������";
            this._asFinish.Glyph = ((System.Drawing.Image)(resources.GetObject("_asFinish.Glyph")));
            this._asFinish.Id = 6;
            this._asFinish.Name = "_asFinish";
            this._asFinish.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this._asFinish_ItemClick);
            // 
            // _asPeriod
            // 
            this._asPeriod.Caption = "���������� ��� ������";
            this._asPeriod.Glyph = ((System.Drawing.Image)(resources.GetObject("_asPeriod.Glyph")));
            this._asPeriod.Id = 7;
            this._asPeriod.Name = "_asPeriod";
            this._asPeriod.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this._asPeriod_ItemClick);
            // 
            // _viewDataGps
            // 
            this._viewDataGps.Caption = "������ ��������";
            this._viewDataGps.Glyph = ((System.Drawing.Image)(resources.GetObject("_viewDataGps.Glyph")));
            this._viewDataGps.Id = 9;
            this._viewDataGps.Name = "_viewDataGps";
            this._viewDataGps.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this._viewDataGps_ItemClick);
            // 
            // _listBox
            // 
            this._listBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this._listBox.Location = new System.Drawing.Point(0, 32);
            this._listBox.Margin = new System.Windows.Forms.Padding(0);
            this._listBox.MultiColumn = true;
            this._listBox.Name = "_listBox";
            this._barManager.SetPopupContextMenu(this._listBox, this._popup);
            this._listBox.Size = new System.Drawing.Size(551, 292);
            this._listBox.TabIndex = 4;
            this._listBox.ToolTipController = this.ttcList;
            this._listBox.DrawItem += new DevExpress.XtraEditors.ListBoxDrawItemEventHandler(this._listBox_DrawItem);
            this._listBox.DoubleClick += new System.EventHandler(this._listBox_DoubleClick);
            // 
            // ttcList
            // 
            this.ttcList.GetActiveObjectInfo += new DevExpress.Utils.ToolTipControllerGetActiveObjectInfoEventHandler(this.ttcList_GetActiveObjectInfo);
            // 
            // _popup
            // 
            this._popup.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this._asBegin),
            new DevExpress.XtraBars.LinkPersistInfo(this._asFinish),
            new DevExpress.XtraBars.LinkPersistInfo(this._asPeriod),
            new DevExpress.XtraBars.LinkPersistInfo(this._viewDataGps)});
            this._popup.Manager = this._barManager;
            this._popup.Name = "_popup";
            this._popup.CloseUp += new System.EventHandler(this._popup_CloseUp);
            this._popup.BeforePopup += new System.ComponentModel.CancelEventHandler(this._popup_BeforePopup);
            // 
            // _historyWorker
            // 
            this._historyWorker.WorkerReportsProgress = true;
            this._historyWorker.WorkerSupportsCancellation = true;
            this._historyWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this._historyWorker_DoWork);
            this._historyWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this._historyWorker_ProgressChanged);
            this._historyWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this._historyWorker_RunWorkerCompleted);
            // 
            // icItemIcons
            // 
            this.icItemIcons.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("icItemIcons.ImageStream")));
            this.icItemIcons.Images.SetKeyName(0, "G");
            this.icItemIcons.Images.SetKeyName(1, "R");
            this.icItemIcons.Images.SetKeyName(2, "Y");
            // 
            // chDateDevice
            // 
            this.chDateDevice.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.chDateDevice.Border = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.chDateDevice.Caption = "�� ����";
            this.chDateDevice.Hint = "�������� ���� �� ���� �������";
            this.chDateDevice.Id = 14;
            this.chDateDevice.ImageIndex = 0;
            this.chDateDevice.Name = "chDateDevice";
            this.chDateDevice.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.chDateDevice_CheckedChanged);
            // 
            // HistoryView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._listBox);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "HistoryView";
            this.Size = new System.Drawing.Size(551, 351);
            this.Load += new System.EventHandler(this.HistoryView_Load);
            ((System.ComponentModel.ISupportInitialize)(this._barManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._progressRepo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._daysCountRepo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._listBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._popup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.icItemIcons)).EndInit();
            this.ResumeLayout(false);

    }

    #endregion

    private DevExpress.XtraBars.BarManager _barManager;
    private DevExpress.XtraBars.Bar _statusBar;
    private DevExpress.XtraBars.Bar _tools;
    private DevExpress.XtraBars.BarDockControl barDockControlTop;
    private DevExpress.XtraBars.BarDockControl barDockControlBottom;
    private DevExpress.XtraBars.BarDockControl barDockControlLeft;
    private DevExpress.XtraBars.BarDockControl barDockControlRight;
    private DevExpress.XtraBars.BarEditItem _daysCount;
    private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit _daysCountRepo;
    private DevExpress.XtraBars.BarButtonItem _refreshBtn;
    private DevExpress.XtraBars.BarEditItem _progress;
    private DevExpress.XtraEditors.Repository.RepositoryItemProgressBar _progressRepo;
    private DevExpress.XtraEditors.ListBoxControl _listBox;
    private System.ComponentModel.BackgroundWorker _historyWorker;
    private DevExpress.XtraBars.PopupMenu _popup;
    private DevExpress.XtraBars.BarButtonItem _asBegin;
    private DevExpress.XtraBars.BarButtonItem _asFinish;
    private DevExpress.XtraBars.BarButtonItem _asPeriod;
    private DevExpress.XtraBars.BarStaticItem _vehicleLbl;
    private DevExpress.XtraBars.BarButtonItem _viewDataGps;
    private DevExpress.XtraBars.BarButtonItem _analizBtn;
    private DevExpress.Utils.ToolTipController ttcList;
    private DevExpress.Utils.ImageCollection icItemIcons;
    private DevExpress.XtraBars.BarButtonItem _stopBtn;
    private DevExpress.XtraBars.BarCheckItem chDateDevice;
  }
}
