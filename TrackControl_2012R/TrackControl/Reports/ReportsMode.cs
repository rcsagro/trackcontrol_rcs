using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using System.Xml;
using BaseReports;
using BaseReports.CrossingCZ;
using BaseReports.Procedure;
using BaseReports.ReportsDE;
using BaseReports.ReportsOnDevExpress;
using DevExpress.XtraEditors;
using Graph;
using LocalCache;
using ReportsOnCheckZones;
using ReportsOnFuelExpenses;
using ReportsOnPassengerTraffic;
using TrackControl.General;
using TrackControl.GMap.UI;
using TrackControl.GMap.Core;
using TrackControl.Notification;
using TrackControl.Properties;
using TrackControl.Reports.SpeedAnalyzer;
using TrackControl.Reports.TrackAnalyzer;
using TrackControl.UkrGIS;
using TrackControl.UkrGIS.SearchEngine;
using TrackControl.Vehicles;
using TrackControl.Zones;
using TrackControl.Online.Services;
using TrackControl.GMap;

namespace TrackControl.Reports
{
    /// <summary>
    /// ����� ������ �������
    /// </summary>
    public class ReportsMode : IMode
    {
        private IMainView _main;
        public ReportsForm _view;
        public AppModel _app;

        private bool _allTrack; // �������� ��� �����
        private bool _autoZoom = true; // ������������ �������������������

        private List<Track> _segments = new List<Track>(); // ������� ������ �� �������
        private List<Marker> _markers = new List<Marker>(); // ������� �� �������

        private VehiclesModel _vehiclesModel;
        private ReportVehiclesModel _reportVehiclesModel;
        private TrackAnalyzerController _trackAnalizer;
        private SpeedAnalyzerController _speedAnalizer;
        private Select2PointsController _select2Points;

        private IZonesManager _zonesManager;
        private ReportZonesModel _reportZonesModel;

        public event MethodInvoker SynhroCheckedVehicles;

        public VehicleTreeView VehicleTree
        {
            get { return _vehicleTree; }
        }

        private VehicleTreeView _vehicleTree;

        public ZonesTreeView ZonesTree
        {
            get { return _zonesTree; }
        }

        private ZonesTreeView _zonesTree;

        public HistoryView HistoryView
        {
            get { return _historyView; }
        }

        private HistoryView _historyView;

        public ReportsControl Reports
        {
            get { return _reports; }
        }

        private ReportsControl _reports;

        public ZGraphControl ZGraph
        {
            get { return _zgraph; }
        }

        private ZGraphControl _zgraph;

        private AddressFinderController adrFinderCtr;
        private Marker _addressFinder;

        /// <summary>
        /// ������� � �������������� ����� ��������� ������ TrackControl.Reports.ReportsMode
        /// </summary>
        public ReportsMode(VehiclesModel vehiclesModel, IZonesManager zonesManager)
        {
            _vehiclesModel = vehiclesModel;

            _zonesManager = zonesManager;
            _reportZonesModel = new ReportZonesModel(_zonesManager);
            _reportZonesModel.Activate();
            _reportZonesModel.VisibilityChanged += zonesVisibilityChanged;
            _zonesTree = new ZonesTreeView(_zonesManager);
            _zonesTree.Dock = DockStyle.Fill;
            _zonesTree.VisibilityChanged += _reportZonesModel.ChangeVisibility;
            _zonesTree.ZoneNodeDoubleClicked += showCertainZone;
            _zonesTree.PanZoneClicked += showCertainZone;
            Select2Points.ZoneCreate += view_ZoneCreate;
            _app = AppModel.Instance;
            _app.GoogleMapControl.OnPointClicked += onFindAddressToPoint;
            _app.SynhroCheckedVehiclesFromOnlineToReport += onSetCheckedFromModel;
            initObjects();

            _reports.graphicsDataUpdate = new ReportsControl.GraphDataUpdate(redrawGraphObjects);
        }

        public void initObjects()
        {
                initVehicleTree();
                initHistoryView();
                initZGraph();
            try
            {
                initReports();
            }
            catch( Exception ex )
            {
                XtraMessageBox.Show( ex.ToString(), "Error initReportsMode", MessageBoxButtons.OK );
            }
                initWorker();
                initTrackAnalyzer();
                initSpeedAnalyzer();
                initSelect2Points();
        }

        public void ResetLayout()
        {
            ReportsForm.ResetLayout();
            if (null != _view && _view.IsHandleCreated)
                _view.RestoreLayout();
        }

        public Form View
        {
            get { return _view; }
        }

        public ReportsForm ViewForm
        {
            get { return _view; }
        }

        public bool ResultFlag
        {
            get { return flagResulting; }
        }

        public void DrawFromAgro(object sender, Form_Utils.DrawEventArgs e)
        {
            ReportPeriod period = PeriodService.Period;
            period.Begin = e.StartDate;
            period.End = e.EndDate;

            _view.ShowVehiclesTree();
            _vehicleTree.SelectByModitelId(e.Mobitel);
            _view.ShowZonesPanel();

            _zonesTree.DeselectAllNodes();

            if (e.getListZones != null)
            {
                for (int i = 0; i < e.getListZones.Count; i++)
                {
                    _zonesTree.SelectByZonesId(e.getListZones[i]);
                }
            }

            StartLoading();
        } // DrawFromAgro

        #region --   ����� ���������� IMode   --

        public void Advise(IMainView main)
        {
            _main = main;
            _select2Points.TopForm = (Form) _main;
            _reportVehiclesModel.Activate();
            _reportZonesModel.Activate();
            //_vehicleTree.RefreshTree();
            //_zonesTree.RefreshTree();

            _view = new ReportsForm(_vehicleTree, _zonesTree);
            _view.TopLevel = false;
            _view.Dock = DockStyle.Fill;
            _main.SetView(_view);
            _view.Show();
            _view.RestoreLayout();
            _view.AutoZoom = _autoZoom;

            try
            {
                _view.Advise();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show( ex.ToString(), "Error initReportsMode", MessageBoxButtons.OK );
            }

            _view.MapChanged += view_MapChanged;
            _view.StartClicked += StartLoading;
            _view.DrawTrackModeChanged += view_DrawTrackModeChanged;
            _view.AutoZoomModeChanged += view_AutoZoomModeChanged;
            _view.TrackAnalizerClicked += view_TrackAnalizerClicked;
            _view.TrackAnalizerStop += view_TrackAnalizerStop;
            _view.SpeedAnalizerClicked += view_SpeedAnalizerClicked;
            _view.RunAddressFinder += view_RunAddressFinder;
            _view.Select2PointsClicked += view_Select2PointsClicked;
            Select2Points.ShowZoneEditor += view_ShowZoneEditor;
            Select2Points.ZoneCreate += view_ZoneCreate;

            _view.RouteCreaterClicked += view_RouteCreaterClicked;

            Algorithm.AlgorithmStarted += _view.InitProgress;
            Algorithm.ProgressChanged += _view.UpdateProgress;
            Algorithm.AlgorithmFinished += _view.FinishLoading;

            NoticeProvider.StartLoading += _view.InitProgress;
            NoticeProvider.ProgressChanged += _view.UpdateProgress;
            NoticeProvider.EndLoading += _view.FinishLoading;

            if (_view.Map != null)
            {
                int zoom = Convert.ToInt32(UseMap.getZoomMap());
                _view.Map.State = new MapState(zoom, new PointLatLng(ConstsGen.KievLat,ConstsGen.KievLng));
            }

            zonesVisibilityChanged();
        }

        public void Unadvise()
        {
            _reportZonesModel.DeActivate();

            Algorithm.AlgorithmStarted -= _view.InitProgress;
            Algorithm.ProgressChanged -= _view.UpdateProgress;
            Algorithm.AlgorithmFinished -= _view.FinishLoading;

            NoticeProvider.StartLoading -= _view.InitProgress;
            NoticeProvider.ProgressChanged -= _view.UpdateProgress;
            NoticeProvider.EndLoading -= _view.FinishLoading;

            _view.MapChanged -= view_MapChanged;
            _view.StartClicked -= StartLoading;
            _view.DrawTrackModeChanged -= view_DrawTrackModeChanged;
            _view.AutoZoomModeChanged -= view_AutoZoomModeChanged;
            _view.TrackAnalizerClicked -= view_TrackAnalizerClicked;
            _view.TrackAnalizerStop -= view_TrackAnalizerStop;
            _view.SpeedAnalizerClicked -= view_SpeedAnalizerClicked;
            Select2Points.ZoneCreate -= view_ZoneCreate;
            Select2Points.ShowZoneEditor -= view_ShowZoneEditor;
            _view.RunAddressFinder -= view_RunAddressFinder;
            _view.SaveLayout();
            _view.Release();
            _view.Parent = null;
            _view.Close();
            _view = null;
            _trackAnalizer.Stop();
            _speedAnalizer.Stop();
        }

        public void RestoreLayout()
        {
            _view.RestoreLayout();
        }
        public void SaveLayout()
        {
            _view.SaveLayout();
        }
        #endregion

        #region --   Data Loading   --

        private string name;
        private BackgroundWorker _worker;
        private IList<ReportVehicle> _checkedVehicles;
        private bool flagResulting;

        private void initWorker()
        {
            _worker = new BackgroundWorker();
            _worker.WorkerReportsProgress = true;
            _worker.DoWork += _worker_DoWork;
            _worker.ProgressChanged += _worker_ProgressChanged;
            _worker.RunWorkerCompleted += _worker_RunWorkerCompleted;
        }

        private const bool START_PROCESSING = false;

        private void _worker_DoWork(object sender, DoWorkEventArgs e)
        {
            _checkedVehicles = _vehicleTree.Checked;

            foreach (atlantaDataSet.mobitelsRow mobitel in _app.DataSet.mobitels)
            {
                mobitel.Check = false;
            }

            if (_checkedVehicles.Count > 0)
            {
                foreach (ReportVehicle rv in _checkedVehicles)
                {
                    atlantaDataSet.mobitelsRow row = _app.DataSet.mobitels.FindByMobitel_ID(rv.Mobitel.Id);
                    row.Check = true;
                }
            }

            ReportPeriod period = PeriodService.Period;
            _view.ShowButtonCancel();
            _view.SetWorkProcessing(START_PROCESSING);
            _app.DataViewAdapter.WorkProcessState(_view.isWorkProcessing());
            _app.DataViewAdapter.Fill(_app.DataSet, period.Begin, period.End, _worker, ref name, DataSetManager.GpsDatas64);
        }

        private void _worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if( _view != null )
            {
                _app.DataViewAdapter.WorkProcessState( _view.isWorkProcessing() );

                _view.UpdateProgress( Resources.LoadingFromDB, name, e.ProgressPercentage );
            }
        }

        private void _worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (_view == null) 
                return;

            if (null != e.Error)
                _view.LoadingFailed(String.Format("{0}{2}{1}", Resources.Reports_LoadingFailed, e.Error.Message,
                                                  Environment.NewLine));
            else
            {
                _view.FinishLoading();
                _reports.EnableRunButtons();
            }

            prepareTracks();

            // ����������� ����� �� �����
            redrawMapObjects();

            // ����������� ������� �������� ������������� ��������
            redrawGraphObjects();
        }

        private void redrawGraphObjects()
        {
            if( _vehicleTree.Current == null )
                return;
            
                ReportVehicle rv = _vehicleTree.Current;

            _historyView.ShowDays(rv);

            if (!_worker.IsBusy && null != _view)
            {
                atlantaDataSet.mobitelsRow mobitel = _app.DataSet.mobitels.FindByMobitel_ID(rv.Mobitel.Id);
                GoogleMapControl.mobitelId = rv.Mobitel.Id; // ��� ������������� ������ --aketner 13.09.2013

                // ��� ��� ������� ===================
                ReportsControl.GraphClearSeries();
                _reports.SelectFiltr(mobitel);
                _reports.getReports[_reports.Tabs.SelectedTabPageIndex].SelectGraphic(mobitel);
                ReportsControl.ShowGraph(mobitel);
                _zgraph.Refresh();
                // ===================================
            }
        }

        #endregion

        private void view_MapChanged()
        {
            zonesVisibilityChanged();

            if (!_worker.IsBusy)
                redrawMapObjects();

            if (_view.Map is GisMapControl)
                _trackAnalizer.Stop();
        }

        /// <summary>
        /// ������������ ��� ��������� ���������� ���������� ����������� ���
        /// </summary>
        private void zonesVisibilityChanged()
        {
            if( _view.Map == null )
                return;
            
            IMap map = _view.Map;
            map.ClearZones();
            map.AddZones(_reportZonesModel.Checked);
        }

        private void vehicleTree_VehicleSelected(ReportVehicle rv)
        {
            _historyView.ShowDays(rv);

            if (!_worker.IsBusy && null != _view)
            {
                atlantaDataSet.mobitelsRow mobitel = _app.DataSet.mobitels.FindByMobitel_ID(rv.Mobitel.Id);
                GoogleMapControl.mobitelId = rv.Mobitel.Id; // ��� ������������� ������ --aketner 13.09.2013

                // ��� ��� ������� ===================
                ReportsControl.GraphClearSeries();
                _reports.SelectFiltr(mobitel);
                _reports.getReports[_reports.Tabs.SelectedTabPageIndex].SelectGraphic(mobitel);
                ReportsControl.ShowGraph(mobitel);
                _zgraph.Refresh();
                // ===================================

                redrawMapObjects();
            }

            // ���������� ����� �������� ������ � Excel �� ��������� �� ��� ������ � debug = mode
            //BaseReports.OutDataToExcel.OutDataForm.selectMobitel = rv.Mobitel.Id;
            _trackAnalizer.AssignVehicle(rv, new Point(0, 0));
        }

        private void vehicleTree_VehicleChecked()
        {
            _historyView.CheckedVehicles = _vehicleTree.Checked;
            ReportTabControl.CheckedVehicles = _vehicleTree.Checked;
            Algorithm.Period = PeriodService.Period;
        }

        private void showCertainZone(IZone zone)
        {
            _view.Map.PanTo(zone.Bounds);
        }

        private void StartLoading()
        {
            try
            {
                flagResulting = false;

                if( _worker.IsBusy)
                {
                    XtraMessageBox.Show( "Can not start again! Try again later.", "Error start loading ", MessageBoxButtons.OK );
                    flagResulting = true;
                    return;
                }
                
                _reports.ClearReportsData();

                atlantaDataSet ds = _app.DataSet;
                Algorithm.AtlantaDataSet = ds;
                Algorithm.Period = PeriodService.Period;
                ds.flowmeterReport.Clear();
                ds.RotateValue.Clear();
                CrossZones.Instance.Clear();
                ds.dataview.Clear();
                _reportVehiclesModel.ClearTracks();

                if (Algorithm.DicRfidRecords != null)
                    Algorithm.DicRfidRecords.Clear();

                _view.Map.ClearMarkers();
                _view.Map.ClearTracks();
                _view.Map.Repaint();
                _view.InitProgress(Resources.LoadingFromDB, String.Empty, 100);
                clearReportMapObjects();
                _worker.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show( ex.Message, "Error start loading ", MessageBoxButtons.OK );
                flagResulting = true;
            }
        }

        private void initVehicleTree()
        {
            _reportVehiclesModel = new ReportVehiclesModel(_vehiclesModel);
            _reportVehiclesModel.Activate();
            _vehicleTree = new VehicleTreeView(_vehiclesModel);
            _vehicleTree.Dock = DockStyle.Fill;

            _vehicleTree.VehicleSelected += vehicleTree_VehicleSelected;
            _vehicleTree.VehicleChecked += vehicleTree_VehicleChecked;
            _vehicleTree.SynhroCheckedVehicles += onSynhroCheckedVehicles;
        }

        private void initHistoryView()
        {
            _historyView = new HistoryView();
            _historyView.Dock = DockStyle.Fill;
        }

        private void initReports()
        {
            Algorithm.GeoLocator = GeoLocator.Instance;
            Algorithm.ZonesLocator = new ZonesLocator(_reportZonesModel);
            Algorithm.ZonesModel = _reportZonesModel;

            _reports = new ReportsControl(_app.DataSet, _zgraph);
            _reports.Dock = DockStyle.Fill;

            _reports.AddReport( /*new KilometrageC()*/ new KilometrageControl()); // aketner 26.07.2012
            _reports.AddReport( /*new KilometrageDay()*/ new KilometrageDayControl()); // aketner 11.07.2012
            _reports.AddReport(new KilometrageTotal()); // new report -- aketner 31.07.2012
            _reports.AddReport( /*new DevFuelControl()*/ new FuelControl()); // aketner 31.10.2012
            //_reports.AddReport( /*new DevFuelControl()*/ new FuelControlOnes()); // aketner 15.09.2017
            _reports.AddReport(new KilometrageControlDut()); // aketner 21.04.2017
            _reports.AddReport( /*new DevFuelExpensesCtrlDutDay()*/ new FuelExpensesControlDutDay()); // aketner 13.08.2012
            _reports.AddReport( /*new DevFuelExpensesCtrlDut()*/ new FuelExpensesControlDut()); // aketner 13.08.2012
            _reports.AddReport( /*new DevFlowmeterCntrl()*/ new FlowmeterControl()); // aketner 22.08.2012
            _reports.AddReport( /*new DevFuelExpensesCntrlDrt()*/ new FuelExpensesControlDrt()); // aketner 27.08.2012
            _reports.AddReport(new GrainLevel()); // aketner 11.05.2017
            _reports.AddReport( /*new DevRoadSafetyCtrl()*/ new RoadSafetyControl()); // aketner - 29.08.2012
            _reports.AddReport( /*new DevParamOfMovingCtrl()*/ new ParamOfMovingControl()); // aketner - 31.08.2012
            _reports.AddReport( /*new DevCzDetailed()*/ new CzDetailed()); // aketner - 10.09.2012
            _reports.AddReport( new CZ_Detailed_General() ); // aketner - 06.10.2014
            _reports.AddReport( /*new DevFuelCZ_Inkom_DRT()*/ new FuelCZ_Inkom_DRT()); // aketner - 10.09.2012
            _reports.AddReport( /*new DevFuelCZ_Inkom_DUT()*/ new FuelCZ_Inkom_DUT()); // aketner - 20.09.2012
            _reports.AddReport( /*new DevSensorEventsControl()*/ new SensorEventsControl()); // aketner - 17.10.2012
            _reports.AddReport( /*new DevRotationControl()*/ new RotationControl()); // aketner - 24.10.2012
            _reports.AddReport( /*new DevPassTrafficDaily(_app.ReportPassZonesModel)*/
                new PassTrafficDaily(_app.ReportPassZonesModel)); // aketner - 19.10.2012
            _reports.AddReport( /*new DevFuelerControlDrt()*/ new FuelerControlDrt()); // aketner - 23.10.2012
            _reports.AddReport(new ReportDrivers());
			//_reports.AddReport(new KZ_Visitors()); // aketner - 25.10.2016
            _reports.AddReport(new ReportVehicles()); // aketner - 16.05.2017
            _reports.AddReport(new ReportAggregates()); // aketner - 16.08.2021
            _reports.AddReport(new GrainShipmentControl()); // aketner - 31.08.2023

            ReportsControl.MapShowNeeded += reports_MapShowNeeded;
            ReportsControl.GraphShowNeeded += reports_GraphShowNeeded;
            ReportsControl.TrackSegmetsShowNeeded += reports_TrackSegmetsShowNeeded;
            ReportsControl.TrackSegmentsShowNeededWith += reports_TrackSegmentsShowNeededWith; // aketner - 09.08.2012
            ReportsControl.MarkersShowNeeded += reports_MarkersShowNeeded;
            ReportsControl.MarkersShowNeededWith += reports_MarkersShowNeededWith; // aketner - 09.08.2012
            ReportsControl.ClearMapObjectsNeeded += reports_ClearMapObjectsNeeded;
            ReportsControl.TrackSegmetsShowWithMarkersEvent += reports_TrackSegmetsShowNeededOnly;
        }

        private void initZGraph()
        {
            _zgraph = new ZGraphControl();
            _zgraph.Dock = DockStyle.Fill;
        }

        private void initTrackAnalyzer()
        {
            _trackAnalizer = new TrackAnalyzerController(_vehicleTree, _app.GoogleMapControl);
            _trackAnalizer.AssignVehicleModel(_vehiclesModel);
            if (_select2Points != null) _select2Points.Stop();

        }

        private void initSelect2Points()
        {
            _select2Points = new Select2PointsController(_vehicleTree, _app.GoogleMapControl);
            if (_trackAnalizer != null) _trackAnalizer.Stop();
        }

        private void initSpeedAnalyzer()
        {
            _speedAnalizer = new SpeedAnalyzerController(_app.GoogleMapControl);
        }

        private void reports_GraphShowNeeded()
        {
            _view.ShowGraphTab();
        }

        private void reports_MapShowNeeded()
        {
            _view.ShowMapTab();
        }

        private void reports_TrackSegmetsShowNeededOnly(IList<Track> segments, IList<Marker> markers)
        {
            clearReportMapObjects();
            _view.Map.ClearTracks();
            _view.Map.ClearMarkers();
            _segments.AddRange(segments);
            if (_segments.Count > 0)
                _view.Map.AddTracks(_segments);
            if (markers.Count > 0)
                _view.Map.AddMarkers(markers);
            foreach (Marker m in markers)
            {
                if (m.IsActive)
                {
                    _view.Map.PanTo(m.Point);
                    break;
                }
            }
            _view.Map.Repaint();
        }

        private void reports_TrackSegmetsShowNeeded(IList<Track> segments)
        {
            _segments.AddRange(segments);
            redrawMapObjects();
        }

        // aketner - 09.08.2012
        private void reports_TrackSegmentsShowNeededWith(IList<Track> segments)
        {
            _segments.AddRange(segments);
            redrawMapObjectsWith();
        }

        private void reports_MarkersShowNeeded(IList<Marker> markers)
        {
            _markers.AddRange(markers);
            redrawMapObjects();
        }

        // aketner - 09.08.2012
        private void reports_MarkersShowNeededWith(IList<Marker> markers)
        {
            _markers.AddRange(markers);
            redrawMapObjectsWith();
        }

        private void reports_ClearMapObjectsNeeded()
        {
            clearReportMapObjects();
            redrawMapObjects();
        }

        private void clearReportMapObjects()
        {
            _segments.Clear();
            _markers.Clear();
        }

        private void prepareTracks()
        {
            foreach (ReportVehicle rv in _checkedVehicles)
            {  
                atlantaDataSet.mobitelsRow mobitel = _app.DataSet.mobitels.FindByMobitel_ID(rv.Mobitel.Id);

                GpsData[] gpsDatas = DataSetManager.GetDataGpsArray(mobitel);

                if (gpsDatas != null)
                {
                    List<IGeoPoint> igps = new List<IGeoPoint>();
                    foreach (GpsData gps in gpsDatas)
                    {
                        rv.LastPoint = gps;
                        igps.Add(rv.LastPoint);
                    }
                    if (igps.Count > 0)
                    {
                        rv.Track = new Track(mobitel.Mobitel_ID, Color.FromArgb(mobitel.colorTrack), 1F, igps);
                        rv.Track.Tag = rv;
                    }
                }
            }
        }

        // aketner - 09.08.2012
        public void redrawMapObjectsWith()
        {
            // ������ �����
            _view.Map.ClearTracks();
            _view.Map.ClearMarkers();

            // ���������� ������� ����� � ������������ �� � ����
            RectLatLng bounds = RectLatLng.Empty;

            for (int i = 0; i < _segments.Count; i++)
            {
                if (null != _segments[i])
                {
                    if (_autoZoom && RectLatLng.Empty != _segments[i].Bounds)
                    {
                        if (RectLatLng.Empty != bounds)
                            bounds = RectLatLng.Union(bounds, _segments[i].Bounds);
                        else
                            bounds = _segments[i].Bounds;
                    } // if
                } // if
            } // for

            if (RectLatLng.Empty != bounds)
                _view.Map.PanTo(bounds);


            if (_segments.Count > 0) // �������� ��������
                _view.Map.AddTracks(_segments);

            if (_markers.Count > 0) // �������� �������
                _view.Map.AddMarkers(_markers);

            _view.Map.Repaint(); // ������������ �����
        } // redrawMapObjectsWith 

        public void redrawMapObjects()
        {
            _view.Map.ClearTracks();
            _view.Map.ClearMarkers();
            ReportVehicle current = _vehicleTree.Current;

            if (_allTrack)
            {
                RectLatLng bounds = RectLatLng.Empty;
                foreach (ReportVehicle rv in _vehicleTree.Checked)
                {
                    if (null != rv.Track)
                    {
                        rv.Track.IsActive = rv == current;
                        drawTrack(rv);
                        if (_autoZoom && RectLatLng.Empty != rv.Track.Bounds)
                        {
                            if (RectLatLng.Empty != bounds)
                                bounds = RectLatLng.Union(bounds, rv.Track.Bounds);
                            else
                                bounds = rv.Track.Bounds;
                        }
                    }
                }

                if (RectLatLng.Empty != bounds)
                    _view.Map.PanTo(bounds);
            }
            else
            {
                if (null != current && null != current.Track)
                {
                    current.Track.IsActive = true;
                    drawTrack(current);

                    if (_autoZoom && RectLatLng.Empty != current.Track.Bounds)
                        _view.Map.PanTo(current.Track.Bounds);
                }
            }

            if (_segments.Count > 0)
                _view.Map.AddTracks(_segments);

            if (_markers.Count > 0)
                _view.Map.AddMarkers(_markers);

            if (_addressFinder != null) 
                _view.Map.AddMarker(_addressFinder);

           //--------------------------------------
           //List<GpsData> gpsDatasDemo = DataSetManager.GpsDatasForDemoTest64Packet;
           //if (gpsDatasDemo.Count > 0)
           //{
           //    Track demoTrack = new Track(0, Color.Red, 1F, gpsDatasDemo);
           //    _view.Map.AddTrack(demoTrack);
           //}
           //--------------------------------------

            _view.Map.Repaint();
        }

        private void drawTrack(ReportVehicle rv)
        {
            rv.Track.Color = rv.Style.ColorTrack;
            _view.Map.AddTrack(rv.Track);
            Marker m = new Marker(MarkerType.Report, rv.Mobitel.Id,
                                  rv.Track.GeoPoints[rv.Track.GeoPoints.Count - 1].LatLng, rv.RegNumber + "\"" + rv.CarMaker + "\"",
                                  String.Format("<size=+4>{0} {1} {2}</size>", rv.RegNumber, rv.CarModel, rv.CarMaker));
            SetToolTipDetaled(rv, m);
            _view.Map.AddMarker(m);
        }

        private void SetToolTipDetaled(ReportVehicle rv, Marker m)
        {
            VehicleSensors vehSensors = new VehicleSensors(rv);
            vehSensors.AssignVehicleModel(_vehiclesModel);
            vehSensors.SetSensorValues();
            string nameDriver = rv.DriverNameStep;
            if (rv != null)
            {
                // to do this
            }

            m.DescriptoinDetailed =
                string.Format(
                    @"<b>{1}: </b>{2}{0}<b>{14} </b>{15}{0}<b>{3}: </b>{4}{0}<b>{5}: </b>{6}{0}<b>{7} </b>{8}{0}<b>{9}: </b>{10} {13}{0}<b>{11}: </b>{12}{0}{0}",
                    Environment.NewLine, 
                    Resources.Number, rv.RegNumber
                    , Resources.Group, rv.Group.Name, 
                    Resources.Driver, rv.DriverNameVersus
                    , string.Format("{0} {1}", Resources.PersonTelefone, " "), rv.DriverTelefone 
                    , Resources.Speed, rv.LastPoint.Speed
                    , Resources.LastData, rv.LastPoint.Time
                    , Resources.KmH
                    , Resources.SimTelNumber, " " + rv.Mobitel.Login + " " + "(" + rv.Mobitel.SimNumber + ")");
           
            if (rv.Sensors != null)
            {
                string strfuel = "";
                string otherfuel = "";
                string strfuelall = "";

                foreach (Sensor s in rv.Sensors)
                {
                    if (s.Algoritm == (int)AlgorithmType.FUEL1)
                    {
                        strfuel += string.Format("<b>{1}: </b>{2}{0}", Environment.NewLine, s.Name, s.SensorValue);
                    }
                    else if(s.Algoritm == (int)AlgorithmType.RANGEFINDER)
                    {
                        otherfuel += string.Format("<b>{1}: </b>{2}({3}){0}", Environment.NewLine, s.Name, s.Value, s.ValueStr);
                    }
                    else if (s.Algoritm == (int) AlgorithmType.FUELDRTADD)
                    {
                        strfuel += string.Format("<b>{1}: </b>{2}{0}", Environment.NewLine, s.Name, s.SensorValue);
                    }
                    else if (s.Algoritm == (int)AlgorithmType.DRIVER)
                    {
                        strfuel += string.Format("<b>{1}: </b>{2}{0}", Environment.NewLine, s.Name, s.RfidValue);
                    }
                    else
                    {
                        otherfuel += string.Format("<b>{1}: </b>{2}{0}", Environment.NewLine, s.Name, s.SensorValue);
                    }
                }

                strfuelall += string.Format("<b>{1}: </b>{2}{0}", Environment.NewLine, Resources.FuelAll, rv.GetAllFuel);
                if(strfuel != "")
                    m.DescriptoinDetailed += strfuel;
                if(strfuelall != "")
                    m.DescriptoinDetailed += strfuelall;
                if(otherfuel != "")
                    m.DescriptoinDetailed += otherfuel;
            }
        }

        private void view_DrawTrackModeChanged(bool allTrack)
        {
            _allTrack = allTrack;
            redrawMapObjects();
        }

        private void view_AutoZoomModeChanged(bool autoZoom)
        {
            _autoZoom = autoZoom;
            redrawMapObjects();
        }

        private void view_TrackAnalizerClicked()
        {
            _trackAnalizer.Start();
            redrawMapObjects();
        }

        private void view_TrackAnalizerStop()
        {
            _trackAnalizer.Stop();
            redrawMapObjects();
        }

        private void view_SpeedAnalizerClicked()
        {
            _speedAnalizer.ShowView((Form) _main);
        }

        private void view_Select2PointsClicked()
        {
            _select2Points.Start();
            if (_trackAnalizer != null) _trackAnalizer.Stop();
            redrawMapObjects();
        }

        private void view_RouteCreaterClicked()
        {
            RouterCreatorController rc = new RouterCreatorController(_app.GoogleMapControl, _view);
            rc.Start();
        }

        private void view_ZoneCreate(IGeoPoint[] g_points)
        {
            _view.DrawCratedZone(g_points);
        }

        private void view_ShowZoneEditor()
        {
            _view.ShowZoneEditor();
        }

        private void onSetCheckedFromModel()
        {
            _vehicleTree.SetCheckedFromModel();
        }

        private void onSynhroCheckedVehicles()
        {
            if (SynhroCheckedVehicles != null) SynhroCheckedVehicles();
        }

        #region �������� �����

        private void view_RunAddressFinder()
        {
            adrFinderCtr = AddressFinderController.Instance;
            adrFinderCtr.DrawPointOnMap += onDrawPointOnMap;
            adrFinderCtr.ClearMarker += onClearMarker;
            adrFinderCtr.StopSearching += onStopSearching;
            adrFinderCtr.Start(_main);
        }

        private void onDrawPointOnMap(PointLatLng point)
        {
            _addressFinder = new Marker(MarkerType.Pin, 0, point, "", "");
            redrawMapObjects();
            _view.Map.PanTo(point);
        }

        private void onClearMarker()
        {
            if (_addressFinder != null)
            {
                _addressFinder = null;
                redrawMapObjects();
            }
        }

        private void onFindAddressToPoint(PointLatLng point)
        {
            if (adrFinderCtr != null)
            {
                _addressFinder = new Marker(MarkerType.Pin, 0, point, "", "");
                redrawMapObjects();
                adrFinderCtr.FindAddressToPoint(point);
            }
        }

        private void onStopSearching()
        {
            adrFinderCtr.DrawPointOnMap -= onDrawPointOnMap;
            adrFinderCtr.ClearMarker -= onClearMarker;
            adrFinderCtr.StopSearching -= onStopSearching;
            onClearMarker();
            adrFinderCtr = null;
        }

        #endregion
    }
}
