﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraScheduler;

namespace TrackControl.Reports
{
    public partial class FormInterval : Form
    {
        private CheckEdit[] checkEdits;
        /// <summary>
        /// Конструктор
        /// </summary>
        public FormInterval()
        {
            InitializeComponent();

            checkEdits = new CheckEdit[31]; //  число дней в месяце
            checkEdits[0] = checkEdit1;
            checkEdits[1] = checkEdit2;
            checkEdits[2] = checkEdit3;
            checkEdits[3] = checkEdit4;
            checkEdits[4] = checkEdit5;
            checkEdits[5] = checkEdit6;
            checkEdits[6] = checkEdit7;
            checkEdits[7] = checkEdit8;
            checkEdits[8] = checkEdit9;
            checkEdits[9] = checkEdit10;
            checkEdits[10] = checkEdit11;
            checkEdits[11] = checkEdit12;
            checkEdits[12] = checkEdit13;
            checkEdits[13] = checkEdit14;
            checkEdits[14] = checkEdit15;
            checkEdits[15] = checkEdit16;
            checkEdits[16] = checkEdit17;
            checkEdits[17] = checkEdit18;
            checkEdits[18] = checkEdit19;
            checkEdits[19] = checkEdit20;
            checkEdits[20] = checkEdit21;
            checkEdits[21] = checkEdit22;
            checkEdits[22] = checkEdit23;
            checkEdits[23] = checkEdit24;
            checkEdits[24] = checkEdit25;
            checkEdits[25] = checkEdit26;
            checkEdits[26] = checkEdit27;
            checkEdits[27] = checkEdit28;
            checkEdits[28] = checkEdit29;
            checkEdits[29] = checkEdit30;
            checkEdits[30] = checkEdit31;

            spinEditHours1.Properties.MaxValue = 23;
            spinEditHours2.Properties.MaxValue = 23;
            spinEditMinute1.Properties.MaxValue = 59;
            spinEditMinute2.Properties.MaxValue = 59;
            spinEditHours1.Properties.MinValue = 0;
            spinEditHours2.Properties.MinValue = 0;
            spinEditMinute1.Properties.MinValue = 0;
            spinEditMinute2.Properties.MinValue = 0;
        }

        private void ButtonOK_Click( object sender, EventArgs e )
        {
            DialogResult = DialogResult.OK;
            // Формируем строку запроса на основе выбранных дат
            // formatingDateTimeIntervalSql();
        }

        private void ButtonCancel_Click( object sender, EventArgs e )
        {
            DialogResult = DialogResult.Cancel;
        }

        private void ButtonDayClear_Click( object sender, EventArgs e )
        {
            weekDaysCheckEdit1.WeekDays = (WeekDays)0;
        }

        private void ButtonMonthClear_Click( object sender, EventArgs e )
        {
            for( int i = 0; i < checkEdits.Length; i++ )
            {
                checkEdits[i].Checked = false;
            }
        }

        private void ButtonNoOdd_Click( object sender, EventArgs e )
        {
            for( int i = 0; i < checkEdits.Length; i++ )
            {
                checkEdits[i].Checked = false;
            }

            for( int i = 0; i < checkEdits.Length; i = i + 2 )
            {
                checkEdits[i].Checked = true;
            }
        }

        private void ButtonOdd_Click( object sender, EventArgs e )
        {
            for( int i = 0; i < checkEdits.Length; i++ )
            {
                checkEdits[i].Checked = false;
            }

            for( int i = 1; i < checkEdits.Length; i = i + 2 )
            {
                checkEdits[i].Checked = true;
            }
        }

        private void ButtonSelectDay_Click( object sender, EventArgs e )
        {
            weekDaysCheckEdit1.WeekDays = WeekDays.EveryDay;
        }

        private void ButtonSelectMonth_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < checkEdits.Length; i++)
            {
                checkEdits[i].Checked = true;
            }
        }
    }
}
