using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Forms;
using DevExpress.Data.PLinq.Helpers;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.XtraTreeList.Nodes.Operations;
using DevExpress.XtraTreeList.ViewInfo;
using DevExpress.XtraTreeList.Columns;
using DevExpress.Utils;
using DevExpress.XtraGrid.Views.Grid;
using TrackControl.General;
using TrackControl.Vehicles;
using TrackControl.Properties;

namespace TrackControl.Reports
{
    public partial class VehicleTreeView : XtraUserControl, ITreeModel<ReportVehicle>
    {
        private bool _isLoaded;
        private VehiclesModel _model;
        private string _condition; // ������� ������

        public event Action<ReportVehicle> VehicleSelected;
        public event MethodInvoker VehicleChecked;
        public event MethodInvoker SynhroCheckedVehicles;

        public VehicleTreeView(VehiclesModel model)
        {
            _model = model;
            InitializeComponent();
            Init();
            _model.RefreshTreeView += RefreshLastTime;
        }

        #region --   ����� ���������� ITreeModel<ReportVehicle>   --

        public ReportVehicle Current
        {
            get
            {
                ReportVehicle rv = null;
                IVehicle vehicle = _tree.GetDataRecordByNode(_tree.FocusedNode) as IVehicle;
                if (null != vehicle)
                    rv = vehicle.Tag as ReportVehicle;

                return rv;
            }
        }

        public IList<ReportVehicle> Checked
        {
            get
            {
                CheckedVehiclesOperation operation = new CheckedVehiclesOperation();
                _tree.NodesIterator.DoOperation(operation);
                return operation.Vehicles;
            }
        }

        public void UnCheckedNode()
        {
                UnCheckedVehiclesListOperation operation = new UnCheckedVehiclesListOperation();
                _tree.NodesIterator.DoOperation(operation);
        }

        public IList<IEntity> UnChecked
        {
            get
            {
                UnCheckedVehiclesOperation operation = new UnCheckedVehiclesOperation();
                _tree.NodesIterator.DoOperation(operation);
                return operation.VehiclesEntity;
            }
        }

        public IList<ReportVehicle> GetAll()
        {
            List<ReportVehicle> all = new List<ReportVehicle>();
            foreach (IVehicle vehicle in _model.Vehicles)
            {
                all.Add((ReportVehicle) vehicle.Tag);
            }
            return all.AsReadOnly();
        }

        public ReportVehicle GetById(int id)
        {
            return null;
        }

        #endregion

        public void SelectByModitelId(int id)
        {
            _tree.ExpandAll();
            TreeListNode root = _tree.Nodes[0];
            root.Checked = false;
            TreeViewService.SetCheckedChildNodes(root);
            _tree.NodesIterator.DoOperation(new SelectCertainVehicleOperation(id));
        }

        public void RefreshTree()
        {
            //_isLoaded = false;
           // _tree.RefreshDataSource();
         //   UnCheckedNode();
         //   TreeListNode root = _tree.Nodes[0];
         //   root.Checked = false;
            //RefreshingUnAfterRefresh();
            _isLoaded = false;
            _tree.RefreshDataSource();
            RefreshingAfterRefresh();
            //SetChecked();
        }

        private void RefreshLastTime()
        {
            MethodInvoker action = RefreshLastTimeInvoke;

            if (_tree.InvokeRequired)
            {
                _tree.Invoke(action);
            }
            else
            {
                action();
            }
        }

        private void RefreshLastTimeInvoke()
        {
            UpdateTimeStatus operation = new UpdateTimeStatus(timeCol);
            _tree.NodesIterator.DoLocalOperation(operation, _tree.Nodes);
        }

        public void UncheckUnvisible(List<IVehicle> unvisibleVehicles)
        {
            UncheckUnvisibleOperation operation = new UncheckUnvisibleOperation(unvisibleVehicles);
            _tree.NodesIterator.DoLocalOperation(operation, _tree.Nodes);
        }

        private void this_Load(object sender, EventArgs e)
        {
            _tree.DataSource = new object();
            TreeViewService.TreeListInitialView(_tree);
            SetCondition(String.Empty);
        }

        private void tree_VirtualTreeGetChildNodes(object sender, VirtualTreeGetChildNodesInfo e)
        {
            if (!_isLoaded)
            {
                e.Children = new VehiclesGroup[] {_model.Root};
                _isLoaded = true;
            }
            else
            {
                int j = 0;

                VehiclesGroup group = e.Node as VehiclesGroup;
                if (null != group)
                {
                    IList children = new List<object>();

                    foreach (VehiclesGroup vg in group.OwnGroups)
                    {
                        if (true /*vg.AllItems.Count > 0*/)
                            children.Add(vg);
                    }

                    foreach (IVehicle vehicle in group.OwnItems)
                    {
                        //vehicle.Checked = false;
                        children.Add(vehicle);
                    }

                    //group.NameWithCounter = string.Format("{0}: {1}", group.Name, group.OwnItems.Count); 

                    e.Children = children;
                }
                else // ���� ���� �� �������� �������, ������ �� ����� �������� �����
                {
                    e.Children = new object[] {};
                }
            }
        }

        private void tree_VirtualTreeGetCellValue(object sender, VirtualTreeGetCellValueInfo e)
        {
            if (e.Node is VehiclesGroup)
            {
                VehiclesGroup group = (VehiclesGroup) e.Node;
                if (e.Column == _titleCol)
                    e.CellData = group.NameWithCounter;
            }
            else if (e.Node is IVehicle)
            {
                IVehicle vehicle = (IVehicle) e.Node;

                if (e.Column == _titleCol)
                    e.CellData = vehicle.RegNumber;
                else if (e.Column == _descriptionCol)
                    e.CellData = String.Format(" {0} {1}", vehicle.CarMaker, vehicle.CarModel);
                else if (e.Column == timeCol)
                    e.CellData = vehicle.TimeStatus();
                else if (e.Column == categCol)
                    e.CellData = vehicle.CategoryMixed;
                else if (e.Column == _loginCol)
                    e.CellData = vehicle.Mobitel.Login;
            }
        }

        private void tree_BeforeCheckNode(object sender, CheckNodeEventArgs e)
        {
            TreeViewService.FixNodeState(e);
        }

        private void tree_AfterCheckNode(object sender, NodeEventArgs e)
        {
            try
            {
                TreeViewService.SetCheckedChildNodes(e.Node);
                TreeViewService.SetCheckedParentNodes(e.Node);
                _tree.FocusedNode = e.Node;
                IVehicle vehicle = _tree.GetDataRecordByNode(e.Node) as IVehicle;

                if (null != vehicle)
                {
                    ReportVehicle report = (ReportVehicle) vehicle.Tag;
                    report.Selected = e.Node.Checked;
                    vehicle.Checked = e.Node.Checked;
                }

                if (VehicleChecked != null)
                    VehicleChecked();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message + "\n" + ex.StackTrace, "VehicleTreeView: tree after check node", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        private void tree_CustomDrawNodeImages(object sender, CustomDrawNodeImagesEventArgs e)
        {
            Rectangle rect = e.SelectRect;
            rect.X += (rect.Width - 16)/2;
            rect.Y += (rect.Height - 16)/2;
            rect.Width = 16;
            rect.Height = 16;

            object obj = _tree.GetDataRecordByNode(e.Node);
            if (obj is IVehicle)
            {
                IVehicle vehicle = (IVehicle) obj;
                e.Graphics.DrawImage(vehicle.Style.Icon, rect);
            }
            else if (obj is VehiclesGroup)
            {
                VehiclesGroup group = (VehiclesGroup) obj;
                e.Graphics.DrawImage(group.Style.Icon, rect);
            }

            e.Handled = true;
        }

        private void tree_CustomDrawNodeCell(object sender, CustomDrawNodeCellEventArgs e)
        {
            object obj = _tree.GetDataRecordByNode(e.Node);

            if (obj is IVehicle)
            {
                if (e.Column.FieldName == "Login")
                {
                    int delta = 3;
                    e.Graphics.DrawLine(new Pen(((IVehicle) obj).Style.ColorTrack, 3F), e.Bounds.X + delta,
                        e.Bounds.Y + delta, e.Bounds.X + delta, e.Bounds.Y + e.Bounds.Height - delta);
                }
                return;
            }
            e.Appearance.Font = new Font(AppearanceObject.DefaultFont, FontStyle.Bold);

            if (e.Node != _tree.FocusedNode)
            {
                e.Appearance.BackColor = AppearanceObject.ControlAppearance.BorderColor;
            }
        }

        private void tree_AfterFocusNode(object sender, NodeEventArgs e)
        {
            Action<ReportVehicle> handler = VehicleSelected;
            if (null != handler)
            {
                IVehicle vehicle = _tree.GetDataRecordByNode(e.Node) as IVehicle;
                if (null != vehicle)
                {
                    if (vehicle.Tag is ReportVehicle)
                        handler((ReportVehicle) vehicle.Tag);
                }
            }
        }

        private void tree_FilterNode(object sender, FilterNodeEventArgs e)
        {
            IVehicle vehicle = _tree.GetDataRecordByNode(e.Node) as IVehicle;

            if (null != vehicle)
            {
                if (_condition != null)
                {
                    bool visibleRegNumber = vehicle.RegNumber.ToLower().Contains(_condition.ToLower());

                    bool visibleModel = vehicle.CarModel.ToLower().Contains(_condition.ToLower());
                    bool visibleMaker = vehicle.CarMaker.ToLower().Contains(_condition.ToLower());

                    bool visibleCategory4 = false;
                    if(vehicle.Category4 != null)
                        visibleCategory4 = vehicle.Category4.Name.ToLower().Contains(_condition.ToLower());

                    bool visibleCategory3 = false;
                    if(vehicle.Category3 != null)
                        visibleCategory3 = vehicle.Category3.Name.ToLower().Contains(_condition.ToLower());

                    bool visibleCategory2 = false;
                    if(vehicle.Category2 != null)
                        visibleCategory2 = vehicle.Category2.Name.ToLower().Contains(_condition.ToLower());

                    bool visibleCategory = false;
                    if(vehicle.Category != null)
                        visibleCategory = vehicle.Category.Name.ToLower().Contains(_condition.ToLower());

                    bool visibleCategoryMixed = false;
                    if(vehicle.CategoryMixed != null)
                        visibleCategoryMixed = vehicle.CategoryMixed.ToLower().Contains(_condition.ToLower());

                    e.Node.Visible = visibleRegNumber | visibleMaker | visibleModel | visibleCategory4 | visibleCategory3 | visibleCategory2 | visibleCategory | visibleCategoryMixed;
                }
                else
                {
                    e.Node.Visible = vehicle.RegNumber.Contains("");
                }

                e.Handled = true;
            }
        }

        private void funnelRepo_KeyUp(object sender, KeyEventArgs e)
        {
            TextEdit edit = (TextEdit) sender;
            SetCondition(edit.Text);
        }

        private void eraseBtn_ItemClick(object sender, ItemClickEventArgs e)
        {
            _funnelBox.EditValue = String.Empty;
            SetCondition(String.Empty);
        }

        private void hideEmpty()
        {
            TreeListNode root = _tree.Nodes[0];
            foreach (TreeListNode node in root.Nodes)
            {
                VehiclesGroup group = _tree.GetDataRecordByNode(node) as VehiclesGroup;
                if (null != group)
                {
                    if (TreeViewService.GetVisibleCount(node) > 0)
                        node.Visible = true;
                    else
                        node.Visible = false;
                }
            }
        }

        private void SetCondition(string condition)
        {
            _condition = condition;
            if (_condition.Length > 0)
            {
                _eraseBtn.Enabled = true;
                _tree.FilterNodes();
                hideEmpty();
            }
            else
            {
                _eraseBtn.Enabled = false;
                TreeListNode root = _tree.Nodes[0];
                TreeViewService.ShowAll(root);
            }
        }

        #region ---   Nested Classes   ---

        private class SelectCertainVehicleOperation : TreeListOperation
        {
            private int _mobitelId;

            public SelectCertainVehicleOperation(int mobitelId)
            {
                _mobitelId = mobitelId;
            }

            public override bool NeedsVisitChildren(TreeListNode node)
            {
                VehiclesGroup group = node.TreeList.GetDataRecordByNode(node) as VehiclesGroup;
                return group != null;
            }

            public override bool CanContinueIteration(TreeListNode node)
            {
                IVehicle vehicle = node.TreeList.GetDataRecordByNode(node) as IVehicle;
                if (null != vehicle && vehicle.Mobitel.Id == _mobitelId)
                {
                    node.TreeList.FocusedNode = node;
                    node.Checked = true;
                    TreeViewService.SetCheckedParentNodes(node);
                    return false;
                }
                else
                {
                    return true;
                }
            }

            public override void Execute(TreeListNode node)
            {
                return;
            }
        }

        private class CheckedVehiclesOperation : TreeListOperation
        {
            public List<ReportVehicle> Vehicles
            {
                get { return _vehicles; }
            }

            private List<ReportVehicle> _vehicles = new List<ReportVehicle>();
            private VehiclesGroup group;

            public override bool NeedsVisitChildren(TreeListNode node)
            {
                group = node.TreeList.GetDataRecordByNode(node) as VehiclesGroup;
                return group != null;
            }

            public override void Execute(TreeListNode node)
            {
                IVehicle vehicle = node.TreeList.GetDataRecordByNode(node) as IVehicle;
                if (null != vehicle && node.Checked)
                {
                    ReportVehicle rv = vehicle.Tag as ReportVehicle;
                    rv.Group = group;
                    _vehicles.Add(rv);
                }
            }
        }

        private class UnCheckedVehiclesListOperation : TreeListOperation
        {
            public List<ReportVehicle> Vehicles
            {
                get { return _vehicleslist; }
            }

            private List<ReportVehicle> _vehicleslist = new List<ReportVehicle>();
            private VehiclesGroup group;

            public override bool NeedsVisitChildren(TreeListNode node)
            {
                group = node.TreeList.GetDataRecordByNode(node) as VehiclesGroup;
                return group != null;
            }

            public override void Execute(TreeListNode node)
            {
                IVehicle vehicle = node.TreeList.GetDataRecordByNode(node) as IVehicle;
                if (null != vehicle && node.Checked)
                {
                    //ReportVehicle rv = vehicle.Tag as ReportVehicle;
                        //rv.Group = group;
                        node.Checked = false;
                        vehicle.Checked = false;
                }
            }
        }

        private class UnCheckedVehiclesOperation : TreeListOperation
        {
            public List<IEntity> VehiclesEntity
            {
                get { return _vehicles; }
            }

            private List<IEntity> _vehicles = new List<IEntity>();

            public override bool NeedsVisitChildren(TreeListNode node)
            {
                VehiclesGroup group = node.TreeList.GetDataRecordByNode(node) as VehiclesGroup;
                return group != null;
            }

            public override void Execute(TreeListNode node)
            {
                IVehicle vehicle = node.TreeList.GetDataRecordByNode(node) as IVehicle;

                if( null != vehicle && !node.Checked )
                {
                    if (((IEntity) vehicle.Tag).Id < 0)
                        vehicle.Id = -vehicle.Mobitel.Id;

                    _vehicles.Add( ( IEntity )vehicle.Tag );
                }
            }
        }

        private class RefreshNodeStatesOperation : TreeListOperation
        {
            public override bool NeedsVisitChildren(TreeListNode node)
            {
                VehiclesGroup group = node.TreeList.GetDataRecordByNode(node) as VehiclesGroup;
                return group != null;
            }

            public override void Execute(TreeListNode node)
            {
                IVehicle vehicle = node.TreeList.GetDataRecordByNode(node) as IVehicle;
            }
        }

        private class UpdateTimeStatus : TreeListOperation
        {
            private TreeListColumn _colTime;

            public UpdateTimeStatus(TreeListColumn colTime)
                : base()
            {
                _colTime = colTime;
            }

            public override void Execute(TreeListNode node)
            {
                object obj = node.TreeList.GetDataRecordByNode(node);

                if (obj is IVehicle)
                {
                    IVehicle vehicle = (IVehicle) obj;
                    node.SetValue(_colTime.VisibleIndex, vehicle.TimeStatus());
                }
            }
        }

        private class UncheckUnvisibleOperation : TreeListOperation
        {
            private List<IVehicle> unvisibleVehicles;

            public UncheckUnvisibleOperation(List<IVehicle> unvisibleVehicles)
            {
                this.unvisibleVehicles = unvisibleVehicles;
            }

            public override bool NeedsVisitChildren(TreeListNode node)
            {
                VehiclesGroup group = node.TreeList.GetDataRecordByNode(node) as VehiclesGroup;
                return group != null;
            }

            public override void Execute(TreeListNode node)
            {
                IVehicle vehicle = node.TreeList.GetDataRecordByNode(node) as IVehicle;
                node.Checked = true;

                foreach (IVehicle veh in unvisibleVehicles)
                {
                    if (veh == vehicle)
                    {
                        node.Checked = false;
                    }
                }

                TreeViewService.SetCheckedParentNodes(node);
            }
        }

        private class CheckNodeFromObjectOperation : TreeListOperation
        {
            public override bool NeedsVisitChildren(TreeListNode node)
            {
                VehiclesGroup group = node.TreeList.GetDataRecordByNode(node) as VehiclesGroup;
                return group != null;
            }

            public override void Execute(TreeListNode node)
            {
                if (SetChecked(node)) 
                    return;
                if (!node.Expanded)
                {
                    node.Expanded = true;
                    SetChecked(node);
                    node.Expanded = false;
                }
            }

            private bool SetChecked(TreeListNode node)
            {
                IVehicle vehicle = node.TreeList.GetDataRecordByNode(node) as IVehicle;
                if (null != vehicle)
                {
                    node.Checked = vehicle.Checked;
                    TreeViewService.SetCheckedParentNodes(node);
                    return true;
                }
                return false;
            }
        }

        private class CheckObjectFromNodeOperation : TreeListOperation
        {
            public override bool NeedsVisitChildren(TreeListNode node)
            {
                VehiclesGroup group = node.TreeList.GetDataRecordByNode(node) as VehiclesGroup;
                return group != null;
            }

            public override void Execute(TreeListNode node)
            {
                IVehicle vehicle = node.TreeList.GetDataRecordByNode(node) as IVehicle;
                if (null != vehicle)
                {
                    vehicle.Checked = node.Checked;
                }
            }
        }

        private class UnCheckNodeAfterRefresh : TreeListOperation
        {
            private List<string> visibleVehicl;

            public UnCheckNodeAfterRefresh(List<string> listNodes)
            {
                this.visibleVehicl = listNodes;
            }

            public override bool NeedsVisitChildren(TreeListNode node)
            {
                VehiclesGroup group = node.TreeList.GetDataRecordByNode(node) as VehiclesGroup;
                return group != null;
            }

            public void SetUnCheckedChildNodes(TreeListNode node)
            {
                for (int i = 0; i < node.Nodes.Count; i++)
                {
                    node.Nodes[i].Checked = false;
                    SetUnCheckedChildNodes(node.Nodes[i]);
                }
            }

            public override void Execute(TreeListNode node)
            {
                IVehicle vehicle = node.TreeList.GetDataRecordByNode(node) as IVehicle;

                if (null != vehicle)
                {
                    if(visibleVehicl != null)
                    {
                        foreach (string ernode in visibleVehicl)
                        {
                            if (vehicle.Info == ernode)
                            {
                                vehicle.Checked = false;
                                node.Checked = false;
                            }
                        }
                    }
                }
            }
        }

        private class CheckNodeAfterRefresh : TreeListOperation
        {
            private List<string> visibleVehicles;

            public CheckNodeAfterRefresh(List<string> listNodes)
            {
                this.visibleVehicles = listNodes;
            }

            public override bool NeedsVisitChildren(TreeListNode node)
            {
                VehiclesGroup group = node.TreeList.GetDataRecordByNode(node) as VehiclesGroup;
                return group != null;
            }

            public void SetUnCheckedChildNodes(TreeListNode node)
            {
                for (int i = 0; i < node.Nodes.Count; i++)
                {
                    node.Nodes[i].Checked = false;
                    SetUnCheckedChildNodes(node.Nodes[i]);
                }
            }

            public void SetCheckedChildNodes(TreeListNode node)
            {
                for (int i = 0; i < node.Nodes.Count; i++)
                {
                    node.Nodes[i].Checked = true;
                    SetCheckedChildNodes(node.Nodes[i]);
                }
            }

            public override void Execute(TreeListNode node)
            {
                IVehicle vehicle = node.TreeList.GetDataRecordByNode(node) as IVehicle;

                if (null != vehicle)
                {
                    foreach (string ernode in visibleVehicles)
                    {
                        if (vehicle.Info == ernode)
                        {
                            vehicle.Checked = true;
                            node.Checked = true;
                        }
                    }
                }
            }
        }

        #endregion

        private void Init()
        {
            _funnelBox.Caption = Resources.Funnel;
            _funnelBox.Hint = Resources.Vehicles_FunnelHint;
            _eraseBtn.Caption = Resources.Clear;
            _eraseBtn.Hint = Resources.Vehicles_ClearHint;
            _titleCol.Caption = Resources.Vehicles_TitleAndNumber;
            _descriptionCol.Caption = Resources.Description;
            _loginCol.Caption = Resources.Login;
            categCol.Caption = Resources.Category;
            bbiExpand.Glyph = Shared.Expand;
            bbiCollapce.Glyph = Shared.Collapce;
            bbiSynhro.Glyph = Shared.Synchro;
        }

        private void _tree_DoubleClick(object sender, EventArgs e)
        {
            TreeList tree = sender as TreeList;
            TreeListHitInfo hi = tree.CalcHitInfo(tree.PointToClient(Control.MousePosition));
            if (hi.Node != null)
            {
                object obj = _tree.GetDataRecordByNode(hi.Node);
                if (obj is IVehicle)
                {
                    IVehicle vehicle = (IVehicle) obj;
                    Color vehcol = vehicle.Style.ColorTrack;
                }
            }
        }

        private void bbiExpand_ItemClick(object sender, ItemClickEventArgs e)
        {
            _tree.ExpandAll();
        }

        private void bbiCollapce_ItemClick(object sender, ItemClickEventArgs e)
        {
            TreeViewService.TreeListInitialView(_tree);
        }

        private void ttcTree_GetActiveObjectInfo(object sender, ToolTipControllerGetActiveObjectInfoEventArgs e)
        {
            if (e.SelectedControl is DevExpress.XtraTreeList.TreeList)
            {
                TreeList tree = (TreeList) e.SelectedControl;
                TreeListHitInfo hit = tree.CalcHitInfo(e.ControlMousePosition);
                if (hit.Column == timeCol)
                {
                    if (hit.HitInfoType == HitInfoType.Cell)
                    {
                        object cellInfo = new TreeListCellToolTipInfo(hit.Node, hit.Column, null);
                        IVehicle vehicle = tree.GetDataRecordByNode(hit.Node) as IVehicle;
                        if (null != vehicle)
                        {
                            string toolTip = string.Format("{1}: {0}", vehicle.Mobitel.LastTimeGps.ToString(),
                                Resources.LastData);
                            e.Info = new DevExpress.Utils.ToolTipControlInfo(cellInfo, toolTip);
                        }

                    }
                    else if (hit.HitInfoType == HitInfoType.Column)
                    {
                        e.Info = new DevExpress.Utils.ToolTipControlInfo(hit.Column, TreeViewService.GetToolTip());
                    }
                }
                else if( hit.Column == _titleCol )
                {
                    object cellInfo = new TreeListCellToolTipInfo( hit.Node, hit.Column, null );
                    IVehicle vehicle = tree.GetDataRecordByNode( hit.Node ) as IVehicle;

                    if( null != vehicle )
                    {
                        string toolTip = string.Format( "{0}", vehicle.VehicleComment);
                        e.Info = new DevExpress.Utils.ToolTipControlInfo( cellInfo, toolTip );
                    }
                }
            }
        }

        public void SetChecked()
        {
            CheckNodeFromObjectOperation operation = new CheckNodeFromObjectOperation();
            _tree.NodesIterator.DoOperation(operation);
        }

        public void GetChecked()
        {
            CheckObjectFromNodeOperation operation = new CheckObjectFromNodeOperation();
            _tree.NodesIterator.DoOperation(operation);
        }

        public void SetCheckedFromModel()
        {
            SetChecked();
        }

        private void bbiSynhro_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (DialogResult.No ==
                XtraMessageBox.Show(Resources.ConfirmSynhro, StaticMethods.GetTotalMessageCaption(),
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question)) return;
            GetChecked();
            if (SynhroCheckedVehicles != null) 
                SynhroCheckedVehicles();
        }

        List<string> treeListVehicel;

        private void _tree_VisibleChanged(object sender, EventArgs e)
        {
            TreeList treeList = (TreeList)sender;
            List<TreeListNode> treeListNode = treeList.GetAllCheckedNodes();
            List<string> treeVehicel = new List<string>();

            foreach (TreeListNode node in treeListNode)
            {
                IVehicle vehicle = node.TreeList.GetDataRecordByNode(node) as IVehicle;
                if (vehicle != null)
                {
                    treeVehicel.Add(vehicle.Info);
                    //node.Checked = true;
                    //vehicle.Checked = true;
                }
            }

            treeListVehicel = treeVehicel;
            RefreshingAfterRefresh(); 
        }

        private void RefreshingAfterRefresh()
        {
            CheckNodeAfterRefresh operation = new CheckNodeAfterRefresh(treeListVehicel);
            _tree.NodesIterator.DoOperation(operation);
        }

        private void RefreshingUnAfterRefresh()
        {
            UnCheckNodeAfterRefresh operation = new UnCheckNodeAfterRefresh(treeListVehicel);
            _tree.NodesIterator.DoOperation(operation);
        }
    }
}
