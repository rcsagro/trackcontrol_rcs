namespace TrackControl.Reports
{
  partial class ReportsForm
  {
    // <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReportsForm));
            this._barManager = new DevExpress.XtraBars.BarManager(this.components);
            this._tools = new DevExpress.XtraBars.Bar();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this._runBtn = new DevExpress.XtraBars.BarLargeButtonItem();
            this._periodMenu = new DevExpress.XtraBars.PopupMenu(this.components);
            this._todayItem = new DevExpress.XtraBars.BarButtonItem();
            this._yesterdayItem = new DevExpress.XtraBars.BarButtonItem();
            this._weekItem = new DevExpress.XtraBars.BarButtonItem();
            this._last7daysItem = new DevExpress.XtraBars.BarButtonItem();
            this._monthItem = new DevExpress.XtraBars.BarButtonItem();
            this._last30daysItem = new DevExpress.XtraBars.BarButtonItem();
            this._fromItem = new DevExpress.XtraBars.BarEditItem();
            this._fromRepo = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this._toItem = new DevExpress.XtraBars.BarEditItem();
            this._toRepo = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.barButtonInterv = new DevExpress.XtraBars.BarButtonItem();
            this._allTracksBtn = new DevExpress.XtraBars.BarCheckItem();
            this._trackPoints = new DevExpress.XtraBars.BarCheckItem();
            this._autoZoomBtn = new DevExpress.XtraBars.BarCheckItem();
            this._viewBtn = new DevExpress.XtraBars.BarLargeButtonItem();
            this._viewMenu = new DevExpress.XtraBars.PopupMenu(this.components);
            this._resetItem = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSaveLayout = new DevExpress.XtraBars.BarButtonItem();
            this._sensorsItem = new DevExpress.XtraBars.BarCheckItem();
            this._historyItem = new DevExpress.XtraBars.BarCheckItem();
            this._logItem = new DevExpress.XtraBars.BarCheckItem();
            this._servicesBtn = new DevExpress.XtraBars.BarLargeButtonItem();
            this._servicesMenu = new DevExpress.XtraBars.PopupMenu(this.components);
            this.biAddressFinder = new DevExpress.XtraBars.BarButtonItem();
            this._trackPointsItem = new DevExpress.XtraBars.BarButtonItem();
            this._speedModeItem = new DevExpress.XtraBars.BarButtonItem();
            this._zoneCreateItem = new DevExpress.XtraBars.BarButtonItem();
            this.bbiCreateRoute = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this._statusLbBefore = new DevExpress.XtraBars.BarStaticItem();
            this._progress = new DevExpress.XtraBars.BarEditItem();
            this._progressRepo = new DevExpress.XtraEditors.Repository.RepositoryItemProgressBar();
            this._statusLbAfter = new DevExpress.XtraBars.BarStaticItem();
            this.btnCancelProcessing = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this._dockManager = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this._historyPanel = new DevExpress.XtraBars.Docking.DockPanel();
            this.historyPanel_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this._images = new DevExpress.Utils.ImageCollection(this.components);
            this._reportsPanel = new DevExpress.XtraBars.Docking.DockPanel();
            this.reportsPanel_DockContainer = new DevExpress.XtraBars.Docking.ControlContainer();
            this.panelContainer1 = new DevExpress.XtraBars.Docking.DockPanel();
            this._logPanel = new DevExpress.XtraBars.Docking.DockPanel();
            this.logPanel_controlContainer = new DevExpress.XtraBars.Docking.ControlContainer();
            this._eventsLogControl = new TrackControl.TtEvents.EventLog.EventLogControl();
            this._vehiclePanel = new DevExpress.XtraBars.Docking.DockPanel();
            this.controlContainer1 = new DevExpress.XtraBars.Docking.ControlContainer();
            this._zonesPanel = new DevExpress.XtraBars.Docking.DockPanel();
            this.controlContainer2 = new DevExpress.XtraBars.Docking.ControlContainer();
            this._noticePanel = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel1_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.noticeLog = new TrackControl.Notification.NoticeLogControl();
            this._sensorsPanel = new DevExpress.XtraBars.Docking.DockPanel();
            this.sensorPanel_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this._sensorsDetails = new TrackControl.Controls.SensorsView();
            this.barButtonInterval = new DevExpress.XtraBars.BarButtonItem();
            this._xtraTabs = new DevExpress.XtraTab.XtraTabControl();
            this._googleTab = new DevExpress.XtraTab.XtraTabPage();
            this._gisTab = new DevExpress.XtraTab.XtraTabPage();
            this._graphTab = new DevExpress.XtraTab.XtraTabPage();
            this.defaultToolTipController1 = new DevExpress.Utils.DefaultToolTipController(this.components);
            ((System.ComponentModel.ISupportInitialize)(this._barManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._periodMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._fromRepo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._fromRepo.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._toRepo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._toRepo.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._viewMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._servicesMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._progressRepo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._dockManager)).BeginInit();
            this._historyPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._images)).BeginInit();
            this._reportsPanel.SuspendLayout();
            this.panelContainer1.SuspendLayout();
            this._logPanel.SuspendLayout();
            this.logPanel_controlContainer.SuspendLayout();
            this._vehiclePanel.SuspendLayout();
            this._zonesPanel.SuspendLayout();
            this._noticePanel.SuspendLayout();
            this.dockPanel1_Container.SuspendLayout();
            this._sensorsPanel.SuspendLayout();
            this.sensorPanel_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._xtraTabs)).BeginInit();
            this._xtraTabs.SuspendLayout();
            this.SuspendLayout();
            // 
            // _barManager
            // 
            this._barManager.AllowCustomization = false;
            this._barManager.AllowQuickCustomization = false;
            this._barManager.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this._tools,
            this.bar3});
            this._barManager.DockControls.Add(this.barDockControlTop);
            this._barManager.DockControls.Add(this.barDockControlBottom);
            this._barManager.DockControls.Add(this.barDockControlLeft);
            this._barManager.DockControls.Add(this.barDockControlRight);
            this._barManager.DockManager = this._dockManager;
            this._barManager.Form = this;
            this._barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barStaticItem1,
            this._runBtn,
            this._todayItem,
            this._yesterdayItem,
            this._fromItem,
            this._toItem,
            this._weekItem,
            this._last7daysItem,
            this._monthItem,
            this._last30daysItem,
            this._statusLbBefore,
            this._progress,
            this._statusLbAfter,
            this._allTracksBtn,
            this._autoZoomBtn,
            this._trackPoints,
            this._viewBtn,
            this._resetItem,
            this._sensorsItem,
            this._historyItem,
            this._logItem,
            this._servicesBtn,
            this._trackPointsItem,
            this._speedModeItem,
            this._zoneCreateItem,
            this.bbiCreateRoute,
            this.biAddressFinder,
            this.barButtonInterv,
            this.bbiSaveLayout,
            this.btnCancelProcessing});
            this._barManager.MaxItemId = 44;
            this._barManager.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this._fromRepo,
            this._toRepo,
            this._progressRepo});
            this._barManager.ShowCloseButton = true;
            this._barManager.ShowFullMenus = true;
            this._barManager.StatusBar = this.bar3;
            // 
            // _tools
            // 
            this._tools.BarItemHorzIndent = 3;
            this._tools.BarName = "Tools";
            this._tools.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Top;
            this._tools.DockCol = 0;
            this._tools.DockRow = 0;
            this._tools.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this._tools.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this._runBtn, true),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.Width, this._fromItem, "", false, true, true, 119),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.Width, this._toItem, "", false, true, true, 117),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonInterv),
            new DevExpress.XtraBars.LinkPersistInfo(this._allTracksBtn, true),
            new DevExpress.XtraBars.LinkPersistInfo(this._trackPoints),
            new DevExpress.XtraBars.LinkPersistInfo(this._autoZoomBtn),
            new DevExpress.XtraBars.LinkPersistInfo(this._viewBtn, true),
            new DevExpress.XtraBars.LinkPersistInfo(this._servicesBtn)});
            this._tools.OptionsBar.AllowQuickCustomization = false;
            this._tools.OptionsBar.DisableClose = true;
            this._tools.OptionsBar.DisableCustomization = true;
            this._tools.OptionsBar.DrawDragBorder = false;
            this._tools.OptionsBar.UseWholeRow = true;
            this._tools.Text = "Tools";
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Border = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.barStaticItem1.Caption = " ������";
            this.barStaticItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("barStaticItem1.Glyph")));
            this.barStaticItem1.Id = 0;
            this.barStaticItem1.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.barStaticItem1.ItemAppearance.Normal.Options.UseFont = true;
            this.barStaticItem1.Name = "barStaticItem1";
            this.barStaticItem1.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.barStaticItem1.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // _runBtn
            // 
            this._runBtn.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this._runBtn.Caption = " ���������";
            this._runBtn.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this._runBtn.DropDownControl = this._periodMenu;
            this._runBtn.Glyph = ((System.Drawing.Image)(resources.GetObject("_runBtn.Glyph")));
            this._runBtn.Hint = "����� ��������� ������ �� ��������� ������ ��� ���������� ������������ �������";
            this._runBtn.Id = 1;
            this._runBtn.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._runBtn.ItemAppearance.Normal.Options.UseFont = true;
            this._runBtn.Name = "_runBtn";
            this._runBtn.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this._runBtn.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this._runBtn_ItemClick);
            // 
            // _periodMenu
            // 
            this._periodMenu.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this._todayItem),
            new DevExpress.XtraBars.LinkPersistInfo(this._yesterdayItem),
            new DevExpress.XtraBars.LinkPersistInfo(this._weekItem),
            new DevExpress.XtraBars.LinkPersistInfo(this._last7daysItem),
            new DevExpress.XtraBars.LinkPersistInfo(this._monthItem),
            new DevExpress.XtraBars.LinkPersistInfo(this._last30daysItem)});
            this._periodMenu.Manager = this._barManager;
            this._periodMenu.Name = "_periodMenu";
            // 
            // _todayItem
            // 
            this._todayItem.Caption = "�� �������";
            this._todayItem.Id = 2;
            this._todayItem.Name = "_todayItem";
            this._todayItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this._todayItem_ItemClick);
            // 
            // _yesterdayItem
            // 
            this._yesterdayItem.Caption = "�� �����";
            this._yesterdayItem.Id = 3;
            this._yesterdayItem.Name = "_yesterdayItem";
            this._yesterdayItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this._yesterdayItem_ItemClick);
            // 
            // _weekItem
            // 
            this._weekItem.Caption = "�� ��� ������";
            this._weekItem.Id = 9;
            this._weekItem.Name = "_weekItem";
            this._weekItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this._weekItem_ItemClick);
            // 
            // _last7daysItem
            // 
            this._last7daysItem.Caption = "�� ��������� 7 ����";
            this._last7daysItem.Id = 10;
            this._last7daysItem.Name = "_last7daysItem";
            this._last7daysItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this._last7daysItem_ItemClick);
            // 
            // _monthItem
            // 
            this._monthItem.Caption = "�� ���� �����";
            this._monthItem.Id = 11;
            this._monthItem.Name = "_monthItem";
            this._monthItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this._monthItem_ItemClick);
            // 
            // _last30daysItem
            // 
            this._last30daysItem.Caption = "�� ��������� 30 ����";
            this._last30daysItem.Id = 12;
            this._last30daysItem.Name = "_last30daysItem";
            this._last30daysItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this._last30daysItem_ItemClick);
            // 
            // _fromItem
            // 
            this._fromItem.Caption = "�";
            this._fromItem.Edit = this._fromRepo;
            this._fromItem.Hint = "���� � ����� ������ ������� ��� ������";
            this._fromItem.Id = 4;
            this._fromItem.Name = "_fromItem";
            this._fromItem.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this._fromItem.EditValueChanged += new System.EventHandler(this._fromItem_EditValueChanged);
            // 
            // _fromRepo
            // 
            this._fromRepo.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this._fromRepo.Appearance.Options.UseTextOptions = true;
            this._fromRepo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._fromRepo.AutoHeight = false;
            this._fromRepo.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this._fromRepo.CalendarTimeEditing = DevExpress.Utils.DefaultBoolean.True;
            this._fromRepo.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this._fromRepo.CalendarTimeProperties.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F4);
            this._fromRepo.CalendarTimeProperties.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Default;
            this._fromRepo.CalendarView = DevExpress.XtraEditors.Repository.CalendarView.Vista;
            this._fromRepo.DisplayFormat.FormatString = "dd/MM/yyyy  HH:mm";
            this._fromRepo.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this._fromRepo.EditFormat.FormatString = "dd/MM/yyyy  HH:mm";
            this._fromRepo.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this._fromRepo.Mask.EditMask = "dd/MM/yyyy  HH:mm";
            this._fromRepo.Name = "_fromRepo";
            this._fromRepo.VistaDisplayMode = DevExpress.Utils.DefaultBoolean.True;
            // 
            // _toItem
            // 
            this._toItem.Caption = " ��";
            this._toItem.Edit = this._toRepo;
            this._toItem.Hint = "���� � ����� ��������� ������� ��� ������";
            this._toItem.Id = 6;
            this._toItem.Name = "_toItem";
            this._toItem.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this._toItem.EditValueChanged += new System.EventHandler(this._toItem_EditValueChanged);
            // 
            // _toRepo
            // 
            this._toRepo.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this._toRepo.Appearance.Options.UseTextOptions = true;
            this._toRepo.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._toRepo.AutoHeight = false;
            this._toRepo.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this._toRepo.CalendarTimeEditing = DevExpress.Utils.DefaultBoolean.True;
            this._toRepo.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this._toRepo.CalendarTimeProperties.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F4);
            this._toRepo.CalendarTimeProperties.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Default;
            this._toRepo.CalendarView = DevExpress.XtraEditors.Repository.CalendarView.Vista;
            this._toRepo.DisplayFormat.FormatString = "dd/MM/yyyy  HH:mm";
            this._toRepo.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this._toRepo.EditFormat.FormatString = "dd/MM/yyyy  HH:mm";
            this._toRepo.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this._toRepo.Mask.EditMask = "dd/MM/yyyy  HH:mm";
            this._toRepo.Name = "_toRepo";
            this._toRepo.VistaDisplayMode = DevExpress.Utils.DefaultBoolean.True;
            // 
            // barButtonInterv
            // 
            this.barButtonInterv.Caption = "���������";
            this.barButtonInterv.Id = 39;
            this.barButtonInterv.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.barButtonInterv.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonInterv.Name = "barButtonInterv";
            this.barButtonInterv.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonInterv_ItemClick_1);
            // 
            // _allTracksBtn
            // 
            this._allTracksBtn.Caption = "��� �����";
            this._allTracksBtn.Glyph = ((System.Drawing.Image)(resources.GetObject("_allTracksBtn.Glyph")));
            this._allTracksBtn.Hint = "�� ����� ����� ������������ ����� ���� ���������� ������������ �������";
            this._allTracksBtn.Id = 20;
            this._allTracksBtn.Name = "_allTracksBtn";
            this._allTracksBtn.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this._allTracksBtn_CheckedChanged);
            // 
            // _trackPoints
            // 
            this._trackPoints.Caption = "barCheckItem1";
            this._trackPoints.Glyph = ((System.Drawing.Image)(resources.GetObject("_trackPoints.Glyph")));
            this._trackPoints.Id = 35;
            this._trackPoints.Name = "_trackPoints";
            this._trackPoints.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this._trackPoints_CheckedChanged);
            // 
            // _autoZoomBtn
            // 
            this._autoZoomBtn.Caption = "�������������������";
            this._autoZoomBtn.Glyph = ((System.Drawing.Image)(resources.GetObject("_autoZoomBtn.Glyph")));
            this._autoZoomBtn.Hint = "����� ����� ������������� ����������������, ����� ����������� �������� ����� ����" +
    "�������";
            this._autoZoomBtn.Id = 21;
            this._autoZoomBtn.Name = "_autoZoomBtn";
            this._autoZoomBtn.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this._autoZoomBtn_CheckedChanged);
            // 
            // _viewBtn
            // 
            this._viewBtn.ActAsDropDown = true;
            this._viewBtn.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this._viewBtn.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this._viewBtn.Caption = "��� ";
            this._viewBtn.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this._viewBtn.DropDownControl = this._viewMenu;
            this._viewBtn.Glyph = ((System.Drawing.Image)(resources.GetObject("_viewBtn.Glyph")));
            this._viewBtn.Id = 23;
            this._viewBtn.Name = "_viewBtn";
            this._viewBtn.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // _viewMenu
            // 
            this._viewMenu.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this._resetItem),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiSaveLayout),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this._sensorsItem, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(this._historyItem),
            new DevExpress.XtraBars.LinkPersistInfo(this._logItem)});
            this._viewMenu.Manager = this._barManager;
            this._viewMenu.Name = "_viewMenu";
            // 
            // _resetItem
            // 
            this._resetItem.Caption = "������������ ������ �� ���������...";
            this._resetItem.Glyph = ((System.Drawing.Image)(resources.GetObject("_resetItem.Glyph")));
            this._resetItem.Id = 24;
            this._resetItem.Name = "_resetItem";
            this._resetItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this._resetItem_ItemClick);
            // 
            // bbiSaveLayout
            // 
            this.bbiSaveLayout.Caption = "��������� ������ �����";
            this.bbiSaveLayout.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiSaveLayout.Glyph")));
            this.bbiSaveLayout.Id = 41;
            this.bbiSaveLayout.Name = "bbiSaveLayout";
            this.bbiSaveLayout.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSaveLayout_ItemClick);
            // 
            // _sensorsItem
            // 
            this._sensorsItem.Caption = "������ ��������";
            this._sensorsItem.Id = 25;
            this._sensorsItem.Name = "_sensorsItem";
            this._sensorsItem.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this._sensorItem_CheckedChanged);
            // 
            // _historyItem
            // 
            this._historyItem.Caption = "������ ���� � �������";
            this._historyItem.Id = 26;
            this._historyItem.Name = "_historyItem";
            this._historyItem.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this._historyItem_CheckedChanged);
            // 
            // _logItem
            // 
            this._logItem.Caption = "������ ������� �������";
            this._logItem.Id = 27;
            this._logItem.Name = "_logItem";
            this._logItem.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this._logItem_CheckedChanged);
            // 
            // _servicesBtn
            // 
            this._servicesBtn.ActAsDropDown = true;
            this._servicesBtn.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this._servicesBtn.Caption = "�����������";
            this._servicesBtn.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this._servicesBtn.DropDownControl = this._servicesMenu;
            this._servicesBtn.Glyph = ((System.Drawing.Image)(resources.GetObject("_servicesBtn.Glyph")));
            this._servicesBtn.Id = 28;
            this._servicesBtn.Name = "_servicesBtn";
            this._servicesBtn.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // _servicesMenu
            // 
            this._servicesMenu.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.biAddressFinder),
            new DevExpress.XtraBars.LinkPersistInfo(this._trackPointsItem),
            new DevExpress.XtraBars.LinkPersistInfo(this._speedModeItem),
            new DevExpress.XtraBars.LinkPersistInfo(this._zoneCreateItem),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiCreateRoute)});
            this._servicesMenu.Manager = this._barManager;
            this._servicesMenu.Name = "_servicesMenu";
            // 
            // biAddressFinder
            // 
            this.biAddressFinder.Caption = "�������� �����";
            this.biAddressFinder.Glyph = ((System.Drawing.Image)(resources.GetObject("biAddressFinder.Glyph")));
            this.biAddressFinder.Id = 36;
            this.biAddressFinder.Name = "biAddressFinder";
            this.biAddressFinder.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.biAddressFinder_ItemClick);
            // 
            // _trackPointsItem
            // 
            this._trackPointsItem.Caption = "������ ����� �����";
            this._trackPointsItem.Glyph = ((System.Drawing.Image)(resources.GetObject("_trackPointsItem.Glyph")));
            this._trackPointsItem.Id = 29;
            this._trackPointsItem.Name = "_trackPointsItem";
            this._trackPointsItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this._trackPointsItem_ItemClick);
            // 
            // _speedModeItem
            // 
            this._speedModeItem.Caption = "���������� �����";
            this._speedModeItem.Glyph = ((System.Drawing.Image)(resources.GetObject("_speedModeItem.Glyph")));
            this._speedModeItem.Id = 30;
            this._speedModeItem.Name = "_speedModeItem";
            this._speedModeItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this._speedModeItem_ItemClick);
            // 
            // _zoneCreateItem
            // 
            this._zoneCreateItem.Caption = "�������� ���� �� ������ �����";
            this._zoneCreateItem.Glyph = ((System.Drawing.Image)(resources.GetObject("_zoneCreateItem.Glyph")));
            this._zoneCreateItem.Id = 31;
            this._zoneCreateItem.Name = "_zoneCreateItem";
            this._zoneCreateItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this._zoneCreateItem_ItemClick);
            // 
            // bbiCreateRoute
            // 
            this.bbiCreateRoute.Caption = "��������� ��������";
            this.bbiCreateRoute.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiCreateRoute.Glyph")));
            this.bbiCreateRoute.Id = 32;
            this.bbiCreateRoute.Name = "bbiCreateRoute";
            this.bbiCreateRoute.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiCreateRoute_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this._statusLbBefore),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.Width, this._progress, "", false, true, true, 244),
            new DevExpress.XtraBars.LinkPersistInfo(this._statusLbAfter),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnCancelProcessing)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DisableClose = true;
            this.bar3.OptionsBar.DisableCustomization = true;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // _statusLbBefore
            // 
            this._statusLbBefore.Border = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this._statusLbBefore.Caption = "������";
            this._statusLbBefore.Glyph = ((System.Drawing.Image)(resources.GetObject("_statusLbBefore.Glyph")));
            this._statusLbBefore.Id = 17;
            this._statusLbBefore.Name = "_statusLbBefore";
            this._statusLbBefore.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this._statusLbBefore.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // _progress
            // 
            this._progress.Caption = "Progress";
            this._progress.Edit = this._progressRepo;
            this._progress.Id = 18;
            this._progress.Name = "_progress";
            this._progress.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this._progress.Width = 200;
            // 
            // _progressRepo
            // 
            this._progressRepo.Name = "_progressRepo";
            // 
            // _statusLbAfter
            // 
            this._statusLbAfter.Border = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this._statusLbAfter.Caption = " ";
            this._statusLbAfter.Id = 19;
            this._statusLbAfter.Name = "_statusLbAfter";
            this._statusLbAfter.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // btnCancelProcessing
            // 
            this.btnCancelProcessing.Caption = "Stop current processing";
            this.btnCancelProcessing.Glyph = ((System.Drawing.Image)(resources.GetObject("btnCancelProcessing.Glyph")));
            this.btnCancelProcessing.Hint = "Stop current processing";
            this.btnCancelProcessing.Id = 43;
            this.btnCancelProcessing.Name = "btnCancelProcessing";
            this.btnCancelProcessing.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.btnCancelProcessing.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnCancelProcessing_ItemClick);
            // 
            // barDockControlTop
            // 
            this.defaultToolTipController1.SetAllowHtmlText(this.barDockControlTop, DevExpress.Utils.DefaultBoolean.Default);
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(1109, 39);
            // 
            // barDockControlBottom
            // 
            this.defaultToolTipController1.SetAllowHtmlText(this.barDockControlBottom, DevExpress.Utils.DefaultBoolean.Default);
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 596);
            this.barDockControlBottom.Size = new System.Drawing.Size(1109, 27);
            // 
            // barDockControlLeft
            // 
            this.defaultToolTipController1.SetAllowHtmlText(this.barDockControlLeft, DevExpress.Utils.DefaultBoolean.Default);
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 39);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 557);
            // 
            // barDockControlRight
            // 
            this.defaultToolTipController1.SetAllowHtmlText(this.barDockControlRight, DevExpress.Utils.DefaultBoolean.Default);
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1109, 39);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 557);
            // 
            // _dockManager
            // 
            this._dockManager.DockingOptions.ShowCaptionImage = true;
            this._dockManager.Form = this;
            this._dockManager.HiddenPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this._historyPanel});
            this._dockManager.Images = this._images;
            this._dockManager.MenuManager = this._barManager;
            this._dockManager.RootPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this._reportsPanel,
            this.panelContainer1,
            this._noticePanel,
            this._sensorsPanel});
            this._dockManager.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "DevExpress.XtraBars.StandaloneBarDockControl",
            "System.Windows.Forms.StatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl"});
            // 
            // _historyPanel
            // 
            this._historyPanel.Controls.Add(this.historyPanel_Container);
            this._historyPanel.Dock = DevExpress.XtraBars.Docking.DockingStyle.Float;
            this._historyPanel.FloatLocation = new System.Drawing.Point(683, 276);
            this._historyPanel.FloatSize = new System.Drawing.Size(295, 100);
            this._historyPanel.FloatVertical = true;
            this._historyPanel.ID = new System.Guid("a54b44fd-38a0-4853-99d3-14c8db528c41");
            this._historyPanel.ImageIndex = 8;
            this._historyPanel.Location = new System.Drawing.Point(-32768, -32768);
            this._historyPanel.Name = "_historyPanel";
            this._historyPanel.Options.ShowMaximizeButton = false;
            this._historyPanel.OriginalSize = new System.Drawing.Size(268, 200);
            this._historyPanel.SavedIndex = 1;
            this._historyPanel.Size = new System.Drawing.Size(295, 100);
            this._historyPanel.TabText = "�������";
            this._historyPanel.Text = "��� � �������";
            this._historyPanel.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden;
            this._historyPanel.VisibilityChanged += new DevExpress.XtraBars.Docking.VisibilityChangedEventHandler(this._historyPanel_VisibilityChanged);
            // 
            // historyPanel_Container
            // 
            this.defaultToolTipController1.SetAllowHtmlText(this.historyPanel_Container, DevExpress.Utils.DefaultBoolean.Default);
            this.historyPanel_Container.Location = new System.Drawing.Point(2, 24);
            this.historyPanel_Container.Name = "historyPanel_Container";
            this.historyPanel_Container.Size = new System.Drawing.Size(291, 74);
            this.historyPanel_Container.TabIndex = 0;
            // 
            // _images
            // 
            this._images.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("_images.ImageStream")));
            this._images.Images.SetKeyName(0, "Vehicles");
            this._images.Images.SetKeyName(1, "Zones");
            this._images.Images.SetKeyName(2, "Gauge");
            this._images.Images.SetKeyName(3, "Reports");
            this._images.Images.SetKeyName(4, "UkrGis");
            this._images.Images.SetKeyName(5, "GMap");
            this._images.Images.SetKeyName(6, "Charts");
            this._images.Images.SetKeyName(7, "notebook--exclamation.png");
            this._images.Images.SetKeyName(8, "calendar-day.png");
            this._images.Images.SetKeyName(9, "notebook--pencil.png");
            // 
            // _reportsPanel
            // 
            this._reportsPanel.Appearance.Options.UseImage = true;
            this._reportsPanel.Controls.Add(this.reportsPanel_DockContainer);
            this._reportsPanel.Dock = DevExpress.XtraBars.Docking.DockingStyle.Bottom;
            this._reportsPanel.ID = new System.Guid("df15ff02-c7bb-4398-8e20-0c6f9ef74b4b");
            this._reportsPanel.ImageIndex = 3;
            this._reportsPanel.Location = new System.Drawing.Point(0, 501);
            this._reportsPanel.Name = "_reportsPanel";
            this._reportsPanel.Options.ShowCloseButton = false;
            this._reportsPanel.Options.ShowMaximizeButton = false;
            this._reportsPanel.OriginalSize = new System.Drawing.Size(200, 95);
            this._reportsPanel.Size = new System.Drawing.Size(1109, 95);
            this._reportsPanel.Text = "������";
            // 
            // reportsPanel_DockContainer
            // 
            this.defaultToolTipController1.SetAllowHtmlText(this.reportsPanel_DockContainer, DevExpress.Utils.DefaultBoolean.Default);
            this.reportsPanel_DockContainer.Location = new System.Drawing.Point(4, 23);
            this.reportsPanel_DockContainer.Name = "reportsPanel_DockContainer";
            this.reportsPanel_DockContainer.Size = new System.Drawing.Size(1101, 68);
            this.reportsPanel_DockContainer.TabIndex = 0;
            // 
            // panelContainer1
            // 
            this.panelContainer1.ActiveChild = this._logPanel;
            this.panelContainer1.Controls.Add(this._vehiclePanel);
            this.panelContainer1.Controls.Add(this._zonesPanel);
            this.panelContainer1.Controls.Add(this._logPanel);
            this.panelContainer1.Dock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.panelContainer1.FloatVertical = true;
            this.panelContainer1.ID = new System.Guid("db7be0a2-30fa-4658-9c84-1bb1232f5601");
            this.panelContainer1.Location = new System.Drawing.Point(0, 39);
            this.panelContainer1.Name = "panelContainer1";
            this.panelContainer1.Options.ShowCloseButton = false;
            this.panelContainer1.Options.ShowMaximizeButton = false;
            this.panelContainer1.OriginalSize = new System.Drawing.Size(299, 200);
            this.panelContainer1.Size = new System.Drawing.Size(299, 462);
            this.panelContainer1.Tabbed = true;
            // 
            // _logPanel
            // 
            this._logPanel.Controls.Add(this.logPanel_controlContainer);
            this._logPanel.Dock = DevExpress.XtraBars.Docking.DockingStyle.Fill;
            this._logPanel.ID = new System.Guid("05a9a908-bc41-4364-8cd2-6a75a3201189");
            this._logPanel.ImageIndex = 9;
            this._logPanel.Location = new System.Drawing.Point(4, 23);
            this._logPanel.Name = "_logPanel";
            this._logPanel.Options.ShowMaximizeButton = false;
            this._logPanel.OriginalSize = new System.Drawing.Size(291, 407);
            this._logPanel.Size = new System.Drawing.Size(291, 407);
            this._logPanel.TabText = "������";
            this._logPanel.Text = "������ �������";
            this._logPanel.VisibilityChanged += new DevExpress.XtraBars.Docking.VisibilityChangedEventHandler(this._logPanel_VisibilityChanged);
            // 
            // logPanel_controlContainer
            // 
            this.defaultToolTipController1.SetAllowHtmlText(this.logPanel_controlContainer, DevExpress.Utils.DefaultBoolean.Default);
            this.logPanel_controlContainer.Controls.Add(this._eventsLogControl);
            this.logPanel_controlContainer.Location = new System.Drawing.Point(0, 0);
            this.logPanel_controlContainer.Name = "logPanel_controlContainer";
            this.logPanel_controlContainer.Size = new System.Drawing.Size(291, 407);
            this.logPanel_controlContainer.TabIndex = 0;
            // 
            // _eventsLogControl
            // 
            this.defaultToolTipController1.SetAllowHtmlText(this._eventsLogControl, DevExpress.Utils.DefaultBoolean.Default);
            this._eventsLogControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._eventsLogControl.Location = new System.Drawing.Point(0, 0);
            this._eventsLogControl.Name = "_eventsLogControl";
            this._eventsLogControl.Size = new System.Drawing.Size(291, 407);
            this._eventsLogControl.TabIndex = 0;
            // 
            // _vehiclePanel
            // 
            this._vehiclePanel.Controls.Add(this.controlContainer1);
            this._vehiclePanel.Dock = DevExpress.XtraBars.Docking.DockingStyle.Fill;
            this._vehiclePanel.FloatVertical = true;
            this._vehiclePanel.ID = new System.Guid("eca2d8c1-6bff-4158-8e6b-058607013d9e");
            this._vehiclePanel.ImageIndex = 0;
            this._vehiclePanel.Location = new System.Drawing.Point(4, 23);
            this._vehiclePanel.Name = "_vehiclePanel";
            this._vehiclePanel.Options.ShowCloseButton = false;
            this._vehiclePanel.Options.ShowMaximizeButton = false;
            this._vehiclePanel.OriginalSize = new System.Drawing.Size(291, 407);
            this._vehiclePanel.Size = new System.Drawing.Size(291, 407);
            this._vehiclePanel.TabText = "����";
            this._vehiclePanel.Text = "������������ ��������";
            // 
            // controlContainer1
            // 
            this.defaultToolTipController1.SetAllowHtmlText(this.controlContainer1, DevExpress.Utils.DefaultBoolean.Default);
            this.controlContainer1.Location = new System.Drawing.Point(0, 0);
            this.controlContainer1.Name = "controlContainer1";
            this.controlContainer1.Size = new System.Drawing.Size(291, 407);
            this.controlContainer1.TabIndex = 0;
            // 
            // _zonesPanel
            // 
            this._zonesPanel.Controls.Add(this.controlContainer2);
            this._zonesPanel.Dock = DevExpress.XtraBars.Docking.DockingStyle.Fill;
            this._zonesPanel.ID = new System.Guid("7bbc192b-f9ce-4f5a-9f8c-9f83e6040155");
            this._zonesPanel.ImageIndex = 1;
            this._zonesPanel.Location = new System.Drawing.Point(4, 23);
            this._zonesPanel.Name = "_zonesPanel";
            this._zonesPanel.Options.ShowCloseButton = false;
            this._zonesPanel.Options.ShowMaximizeButton = false;
            this._zonesPanel.OriginalSize = new System.Drawing.Size(291, 407);
            this._zonesPanel.Size = new System.Drawing.Size(291, 407);
            this._zonesPanel.TabText = "����";
            this._zonesPanel.Text = "����������� ����";
            // 
            // controlContainer2
            // 
            this.defaultToolTipController1.SetAllowHtmlText(this.controlContainer2, DevExpress.Utils.DefaultBoolean.Default);
            this.controlContainer2.Location = new System.Drawing.Point(0, 0);
            this.controlContainer2.Name = "controlContainer2";
            this.controlContainer2.Size = new System.Drawing.Size(291, 407);
            this.controlContainer2.TabIndex = 0;
            // 
            // _noticePanel
            // 
            this._noticePanel.Appearance.BackColor = System.Drawing.Color.Transparent;
            this._noticePanel.Appearance.BackColor2 = System.Drawing.Color.Transparent;
            this._noticePanel.Appearance.Options.UseBackColor = true;
            this._noticePanel.Controls.Add(this.dockPanel1_Container);
            this._noticePanel.Dock = DevExpress.XtraBars.Docking.DockingStyle.Right;
            this._noticePanel.DockVertical = DevExpress.Utils.DefaultBoolean.True;
            this._noticePanel.FloatSize = new System.Drawing.Size(237, 200);
            this._noticePanel.FloatVertical = true;
            this._noticePanel.ID = new System.Guid("1b4ef0bf-abec-441e-a927-57cf8d480724");
            this._noticePanel.ImageIndex = 7;
            this._noticePanel.Location = new System.Drawing.Point(817, 39);
            this._noticePanel.Name = "_noticePanel";
            this._noticePanel.OriginalSize = new System.Drawing.Size(292, 369);
            this._noticePanel.Size = new System.Drawing.Size(292, 462);
            this._noticePanel.TabsPosition = DevExpress.XtraBars.Docking.TabsPosition.Right;
            this._noticePanel.TabText = "�����������";
            this._noticePanel.Text = "�����������������";
            // 
            // dockPanel1_Container
            // 
            this.defaultToolTipController1.SetAllowHtmlText(this.dockPanel1_Container, DevExpress.Utils.DefaultBoolean.Default);
            this.dockPanel1_Container.Controls.Add(this.noticeLog);
            this.dockPanel1_Container.Location = new System.Drawing.Point(4, 23);
            this.dockPanel1_Container.Name = "dockPanel1_Container";
            this.dockPanel1_Container.Size = new System.Drawing.Size(284, 435);
            this.dockPanel1_Container.TabIndex = 0;
            // 
            // noticeLog
            // 
            this.defaultToolTipController1.SetAllowHtmlText(this.noticeLog, DevExpress.Utils.DefaultBoolean.Default);
            this.noticeLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.noticeLog.Location = new System.Drawing.Point(0, 0);
            this.noticeLog.Name = "noticeLog";
            this.noticeLog.Size = new System.Drawing.Size(284, 435);
            this.noticeLog.TabIndex = 0;
            this.noticeLog.Load += new System.EventHandler(this.noticeLog_Load);
            // 
            // _sensorsPanel
            // 
            this._sensorsPanel.Controls.Add(this.sensorPanel_Container);
            this._sensorsPanel.Dock = DevExpress.XtraBars.Docking.DockingStyle.Bottom;
            this._sensorsPanel.FloatSize = new System.Drawing.Size(295, 100);
            this._sensorsPanel.ID = new System.Guid("a8128808-d1bb-410d-9cf0-8f1c9e7b8b7f");
            this._sensorsPanel.ImageIndex = 2;
            this._sensorsPanel.Location = new System.Drawing.Point(299, 296);
            this._sensorsPanel.Name = "_sensorsPanel";
            this._sensorsPanel.Options.ShowMaximizeButton = false;
            this._sensorsPanel.OriginalSize = new System.Drawing.Size(257, 205);
            this._sensorsPanel.Size = new System.Drawing.Size(518, 205);
            this._sensorsPanel.TabText = "�������";
            this._sensorsPanel.Text = "�������";
            this._sensorsPanel.VisibilityChanged += new DevExpress.XtraBars.Docking.VisibilityChangedEventHandler(this._sensorsPanel_VisibilityChanged);
            // 
            // sensorPanel_Container
            // 
            this.defaultToolTipController1.SetAllowHtmlText(this.sensorPanel_Container, DevExpress.Utils.DefaultBoolean.Default);
            this.sensorPanel_Container.Controls.Add(this._sensorsDetails);
            this.sensorPanel_Container.Location = new System.Drawing.Point(4, 23);
            this.sensorPanel_Container.Name = "sensorPanel_Container";
            this.sensorPanel_Container.Size = new System.Drawing.Size(510, 178);
            this.sensorPanel_Container.TabIndex = 0;
            // 
            // _sensorsDetails
            // 
            this.defaultToolTipController1.SetAllowHtmlText(this._sensorsDetails, DevExpress.Utils.DefaultBoolean.Default);
            this._sensorsDetails.Dock = System.Windows.Forms.DockStyle.Fill;
            this._sensorsDetails.Location = new System.Drawing.Point(0, 0);
            this._sensorsDetails.Name = "_sensorsDetails";
            this._sensorsDetails.Size = new System.Drawing.Size(510, 178);
            this._sensorsDetails.TabIndex = 0;
            // 
            // barButtonInterval
            // 
            this.barButtonInterval.ActAsDropDown = true;
            this.barButtonInterval.Border = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.barButtonInterval.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.barButtonInterval.Caption = "���������";
            this.barButtonInterval.Hint = "��������� ���������";
            this.barButtonInterval.Id = 37;
            this.barButtonInterval.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.barButtonInterval.ItemAppearance.Normal.Options.UseFont = true;
            this.barButtonInterval.Name = "barButtonInterval";
            // 
            // _xtraTabs
            // 
            this._xtraTabs.Dock = System.Windows.Forms.DockStyle.Fill;
            this._xtraTabs.Images = this._images;
            this._xtraTabs.Location = new System.Drawing.Point(299, 39);
            this._xtraTabs.Name = "_xtraTabs";
            this._xtraTabs.SelectedTabPage = this._googleTab;
            this._xtraTabs.Size = new System.Drawing.Size(518, 257);
            this._xtraTabs.TabIndex = 5;
            this._xtraTabs.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this._gisTab,
            this._googleTab,
            this._graphTab});
            this._xtraTabs.SelectedPageChanged += new DevExpress.XtraTab.TabPageChangedEventHandler(this._xtraTabs_SelectedPageChanged);
            // 
            // _googleTab
            // 
            this._googleTab.ImageIndex = 5;
            this._googleTab.Name = "_googleTab";
            this._googleTab.Size = new System.Drawing.Size(512, 226);
            this._googleTab.Text = "����� Web";
            // 
            // _gisTab
            // 
            this._gisTab.ImageIndex = 4;
            this._gisTab.Name = "_gisTab";
            this._gisTab.Size = new System.Drawing.Size(512, 226);
            this._gisTab.Text = "����� UkrGIS";
            // 
            // _graphTab
            // 
            this._graphTab.ImageIndex = 6;
            this._graphTab.Name = "_graphTab";
            this._graphTab.Size = new System.Drawing.Size(512, 226);
            this._graphTab.Text = "�������";
            // 
            // defaultToolTipController1
            // 
            // 
            // 
            // 
            this.defaultToolTipController1.DefaultController.AllowHtmlText = true;
            this.defaultToolTipController1.DefaultController.Appearance.BackColor = System.Drawing.Color.AntiqueWhite;
            this.defaultToolTipController1.DefaultController.Appearance.ForeColor = System.Drawing.Color.Black;
            this.defaultToolTipController1.DefaultController.Appearance.Options.UseBackColor = true;
            this.defaultToolTipController1.DefaultController.Appearance.Options.UseForeColor = true;
            this.defaultToolTipController1.DefaultController.AppearanceTitle.ForeColor = System.Drawing.Color.Black;
            this.defaultToolTipController1.DefaultController.AppearanceTitle.Options.UseForeColor = true;
            this.defaultToolTipController1.DefaultController.AutoPopDelay = 10000;
            this.defaultToolTipController1.DefaultController.Rounded = true;
            this.defaultToolTipController1.DefaultController.ShowBeak = true;
            // 
            // ReportsForm
            // 
            this.defaultToolTipController1.SetAllowHtmlText(this, DevExpress.Utils.DefaultBoolean.Default);
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1109, 623);
            this.Controls.Add(this._xtraTabs);
            this.Controls.Add(this._sensorsPanel);
            this.Controls.Add(this._noticePanel);
            this.Controls.Add(this.panelContainer1);
            this.Controls.Add(this._reportsPanel);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "ReportsForm";
            this.Text = "ReportsForm";
            ((System.ComponentModel.ISupportInitialize)(this._barManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._periodMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._fromRepo.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._fromRepo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._toRepo.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._toRepo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._viewMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._servicesMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._progressRepo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._dockManager)).EndInit();
            this._historyPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._images)).EndInit();
            this._reportsPanel.ResumeLayout(false);
            this.panelContainer1.ResumeLayout(false);
            this._logPanel.ResumeLayout(false);
            this.logPanel_controlContainer.ResumeLayout(false);
            this._vehiclePanel.ResumeLayout(false);
            this._zonesPanel.ResumeLayout(false);
            this._noticePanel.ResumeLayout(false);
            this.dockPanel1_Container.ResumeLayout(false);
            this._sensorsPanel.ResumeLayout(false);
            this.sensorPanel_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._xtraTabs)).EndInit();
            this._xtraTabs.ResumeLayout(false);
            this.ResumeLayout(false);

    }

    #endregion

    private DevExpress.XtraBars.BarManager _barManager;
    private DevExpress.XtraBars.Bar _tools;
    private DevExpress.XtraBars.Bar bar3;
    private DevExpress.XtraBars.BarDockControl barDockControlTop;
    private DevExpress.XtraBars.BarDockControl barDockControlBottom;
    private DevExpress.XtraBars.BarDockControl barDockControlLeft;
    private DevExpress.XtraBars.BarDockControl barDockControlRight;
    private DevExpress.XtraBars.Docking.DockManager _dockManager;
    private DevExpress.XtraBars.Docking.DockPanel panelContainer1;
    private DevExpress.XtraBars.Docking.DockPanel _zonesPanel;
    private DevExpress.XtraBars.Docking.ControlContainer controlContainer2;
    private DevExpress.XtraBars.Docking.DockPanel _vehiclePanel;
    private DevExpress.XtraBars.Docking.ControlContainer controlContainer1;
    private DevExpress.XtraBars.Docking.DockPanel _reportsPanel;
    private DevExpress.XtraBars.Docking.ControlContainer reportsPanel_DockContainer;
    private DevExpress.XtraTab.XtraTabControl _xtraTabs;
    private DevExpress.XtraTab.XtraTabPage _gisTab;
    private DevExpress.XtraTab.XtraTabPage _googleTab;
    private DevExpress.XtraTab.XtraTabPage _graphTab;
    private DevExpress.XtraBars.BarStaticItem barStaticItem1;
    private DevExpress.XtraBars.BarLargeButtonItem _runBtn;
    private DevExpress.XtraBars.PopupMenu _periodMenu;
    private DevExpress.XtraBars.BarButtonItem _todayItem;
    private DevExpress.XtraBars.BarButtonItem _yesterdayItem;
    private DevExpress.XtraBars.BarEditItem _fromItem;
    private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit _fromRepo;
    private DevExpress.XtraBars.BarEditItem _toItem;
    private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit _toRepo;
    private DevExpress.XtraBars.BarButtonItem _weekItem;
    private DevExpress.XtraBars.BarButtonItem _last7daysItem;
    private DevExpress.XtraBars.BarButtonItem _monthItem;
    private DevExpress.XtraBars.BarButtonItem _last30daysItem;
    private DevExpress.XtraBars.Docking.DockPanel _sensorsPanel;
    private DevExpress.XtraBars.Docking.ControlContainer sensorPanel_Container;
    private TrackControl.Controls.SensorsView _sensorsDetails;
    private DevExpress.XtraBars.Docking.DockPanel _historyPanel;
    private DevExpress.XtraBars.Docking.ControlContainer historyPanel_Container;
    private DevExpress.XtraBars.Docking.DockPanel _logPanel;
    private DevExpress.XtraBars.Docking.ControlContainer logPanel_controlContainer;
    private TrackControl.TtEvents.EventLog.EventLogControl _eventsLogControl;
    private DevExpress.XtraBars.BarStaticItem _statusLbBefore;
    private DevExpress.XtraBars.BarEditItem _progress;
    private DevExpress.XtraEditors.Repository.RepositoryItemProgressBar _progressRepo;
    private DevExpress.XtraBars.BarStaticItem _statusLbAfter;
    private DevExpress.XtraBars.BarCheckItem _allTracksBtn;
    private DevExpress.XtraBars.BarCheckItem _autoZoomBtn;
    private DevExpress.XtraBars.BarLargeButtonItem _viewBtn;
    private DevExpress.XtraBars.PopupMenu _viewMenu;
    private DevExpress.XtraBars.BarButtonItem _resetItem;
    private DevExpress.XtraBars.BarCheckItem _sensorsItem;
    private DevExpress.XtraBars.BarCheckItem _historyItem;
    private DevExpress.XtraBars.BarCheckItem _logItem;
    private DevExpress.XtraBars.BarLargeButtonItem _servicesBtn;
    private DevExpress.XtraBars.BarButtonItem _trackPointsItem;
    private DevExpress.XtraBars.PopupMenu _servicesMenu;
    private DevExpress.XtraBars.BarButtonItem _speedModeItem;
    private DevExpress.Utils.ImageCollection _images;
    private DevExpress.XtraBars.BarButtonItem _zoneCreateItem;
    private DevExpress.XtraBars.Docking.DockPanel _noticePanel;
    private DevExpress.XtraBars.Docking.ControlContainer dockPanel1_Container;
    private TrackControl.Notification.NoticeLogControl noticeLog;
    private DevExpress.XtraBars.BarButtonItem bbiCreateRoute;
    private DevExpress.XtraBars.BarCheckItem _trackPoints;
    private DevExpress.Utils.DefaultToolTipController defaultToolTipController1;
    private DevExpress.XtraBars.BarButtonItem biAddressFinder;
    protected DevExpress.XtraBars.BarButtonItem barButtonInterval;
    private DevExpress.XtraBars.BarButtonItem barButtonInterv;
    private DevExpress.XtraBars.BarButtonItem bbiSaveLayout;
    private DevExpress.XtraBars.BarButtonItem btnCancelProcessing;
  }
}