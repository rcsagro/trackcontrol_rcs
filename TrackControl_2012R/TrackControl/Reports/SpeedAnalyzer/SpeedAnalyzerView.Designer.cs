namespace TrackControl.Reports.SpeedAnalyzer
{
  partial class SpeedAnalyzerView
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SpeedAnalyzerView));
        this._mainTable = new System.Windows.Forms.TableLayoutPanel();
        this._startBtn = new DevExpress.XtraEditors.SimpleButton();
        this._applyBtn = new DevExpress.XtraEditors.SimpleButton();
        this._cancelBtn = new DevExpress.XtraEditors.SimpleButton();
        this._panel = new DevExpress.XtraEditors.GroupControl();
        this._table = new System.Windows.Forms.TableLayoutPanel();
        this.label1 = new System.Windows.Forms.Label();
        this.label2 = new System.Windows.Forms.Label();
        this.label3 = new System.Windows.Forms.Label();
        this._limitFirstEdit = new DevExpress.XtraEditors.SpinEdit();
        this._limitSecondEdit = new DevExpress.XtraEditors.SpinEdit();
        this._limitThirdEdit = new DevExpress.XtraEditors.SpinEdit();
        this.label4 = new System.Windows.Forms.Label();
        this.label5 = new System.Windows.Forms.Label();
        this.label6 = new System.Windows.Forms.Label();
        this._colorFirstEdit = new System.Windows.Forms.Label();
        this._colorSecondEdit = new System.Windows.Forms.Label();
        this._colorThirdEdit = new System.Windows.Forms.Label();
        this._mainTable.SuspendLayout();
        ((System.ComponentModel.ISupportInitialize)(this._panel)).BeginInit();
        this._panel.SuspendLayout();
        this._table.SuspendLayout();
        ((System.ComponentModel.ISupportInitialize)(this._limitFirstEdit.Properties)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this._limitSecondEdit.Properties)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this._limitThirdEdit.Properties)).BeginInit();
        this.SuspendLayout();
        // 
        // _mainTable
        // 
        this._mainTable.BackColor = System.Drawing.Color.Transparent;
        this._mainTable.ColumnCount = 7;
        this._mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
        this._mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
        this._mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
        this._mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 75F));
        this._mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 5F));
        this._mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 60F));
        this._mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
        this._mainTable.Controls.Add(this._startBtn, 1, 3);
        this._mainTable.Controls.Add(this._applyBtn, 3, 3);
        this._mainTable.Controls.Add(this._cancelBtn, 5, 3);
        this._mainTable.Controls.Add(this._panel, 1, 1);
        this._mainTable.Dock = System.Windows.Forms.DockStyle.Fill;
        this._mainTable.Location = new System.Drawing.Point(0, 0);
        this._mainTable.Margin = new System.Windows.Forms.Padding(0);
        this._mainTable.Name = "_mainTable";
        this._mainTable.RowCount = 5;
        this._mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 5F));
        this._mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 110F));
        this._mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 5F));
        this._mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
        this._mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
        this._mainTable.Size = new System.Drawing.Size(342, 159);
        this._mainTable.TabIndex = 0;
        this._mainTable.Click += new System.EventHandler(this.idle_Click);
        // 
        // _startBtn
        // 
        this._startBtn.Dock = System.Windows.Forms.DockStyle.Fill;
        this._startBtn.Image = ((System.Drawing.Image)(resources.GetObject("_startBtn.Image")));
        this._startBtn.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
        this._startBtn.Location = new System.Drawing.Point(15, 120);
        this._startBtn.Margin = new System.Windows.Forms.Padding(5, 0, 0, 0);
        this._startBtn.Name = "_startBtn";
        this._startBtn.Size = new System.Drawing.Size(75, 29);
        this._startBtn.TabIndex = 0;
        this._startBtn.Click += new System.EventHandler(this._startBtn_Click);
        // 
        // _applyBtn
        // 
        this._applyBtn.Dock = System.Windows.Forms.DockStyle.Fill;
        this._applyBtn.Location = new System.Drawing.Point(192, 120);
        this._applyBtn.Margin = new System.Windows.Forms.Padding(0);
        this._applyBtn.Name = "_applyBtn";
        this._applyBtn.Size = new System.Drawing.Size(75, 29);
        this._applyBtn.TabIndex = 1;
        this._applyBtn.Text = "���������";
        this._applyBtn.Click += new System.EventHandler(this._applyBtn_Click);
        // 
        // _cancelBtn
        // 
        this._cancelBtn.Dock = System.Windows.Forms.DockStyle.Fill;
        this._cancelBtn.Location = new System.Drawing.Point(272, 120);
        this._cancelBtn.Margin = new System.Windows.Forms.Padding(0, 0, 5, 0);
        this._cancelBtn.Name = "_cancelBtn";
        this._cancelBtn.Size = new System.Drawing.Size(55, 29);
        this._cancelBtn.TabIndex = 2;
        this._cancelBtn.Text = "������";
        this._cancelBtn.Click += new System.EventHandler(this._cancelBtn_Click);
        // 
        // _panel
        // 
        this._mainTable.SetColumnSpan(this._panel, 5);
        this._panel.Controls.Add(this._table);
        this._panel.Dock = System.Windows.Forms.DockStyle.Fill;
        this._panel.Location = new System.Drawing.Point(13, 8);
        this._panel.Name = "_panel";
        this._panel.Size = new System.Drawing.Size(316, 104);
        this._panel.TabIndex = 3;
        this._panel.Text = "���������� �������";
        this._panel.Click += new System.EventHandler(this.idle_Click);
        // 
        // _table
        // 
        this._table.BackColor = System.Drawing.Color.Transparent;
        this._table.ColumnCount = 6;
        this._table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 5F));
        this._table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 34F));
        this._table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 69F));
        this._table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 43F));
        this._table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
        this._table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 52F));
        this._table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
        this._table.Controls.Add(this.label1, 1, 1);
        this._table.Controls.Add(this.label2, 1, 2);
        this._table.Controls.Add(this.label3, 1, 3);
        this._table.Controls.Add(this._limitFirstEdit, 2, 1);
        this._table.Controls.Add(this._limitSecondEdit, 2, 2);
        this._table.Controls.Add(this._limitThirdEdit, 2, 3);
        this._table.Controls.Add(this.label4, 3, 1);
        this._table.Controls.Add(this.label5, 3, 2);
        this._table.Controls.Add(this.label6, 3, 3);
        this._table.Controls.Add(this._colorFirstEdit, 5, 1);
        this._table.Controls.Add(this._colorSecondEdit, 5, 2);
        this._table.Controls.Add(this._colorThirdEdit, 5, 3);
        this._table.Dock = System.Windows.Forms.DockStyle.Fill;
        this._table.Location = new System.Drawing.Point(2, 22);
        this._table.Name = "_table";
        this._table.RowCount = 5;
        this._table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 5F));
        this._table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33332F));
        this._table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
        this._table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
        this._table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 5F));
        this._table.Size = new System.Drawing.Size(312, 80);
        this._table.TabIndex = 0;
        this._table.Click += new System.EventHandler(this.idle_Click);
        // 
        // label1
        // 
        this.label1.AutoSize = true;
        this.label1.BackColor = System.Drawing.Color.Transparent;
        this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
        this.label1.Location = new System.Drawing.Point(5, 5);
        this.label1.Margin = new System.Windows.Forms.Padding(0);
        this.label1.Name = "label1";
        this.label1.Size = new System.Drawing.Size(34, 23);
        this.label1.TabIndex = 0;
        this.label1.Text = "��";
        this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
        // 
        // label2
        // 
        this.label2.AutoSize = true;
        this.label2.BackColor = System.Drawing.Color.Transparent;
        this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
        this.label2.Location = new System.Drawing.Point(5, 28);
        this.label2.Margin = new System.Windows.Forms.Padding(0);
        this.label2.Name = "label2";
        this.label2.Size = new System.Drawing.Size(34, 23);
        this.label2.TabIndex = 1;
        this.label2.Text = "��";
        this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
        // 
        // label3
        // 
        this.label3.AutoSize = true;
        this.label3.BackColor = System.Drawing.Color.Transparent;
        this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
        this.label3.Location = new System.Drawing.Point(5, 51);
        this.label3.Margin = new System.Windows.Forms.Padding(0);
        this.label3.Name = "label3";
        this.label3.Size = new System.Drawing.Size(34, 23);
        this.label3.TabIndex = 2;
        this.label3.Text = "��";
        this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
        // 
        // _limitFirstEdit
        // 
        this._limitFirstEdit.Dock = System.Windows.Forms.DockStyle.Fill;
        this._limitFirstEdit.EditValue = new decimal(new int[] {
            40,
            0,
            0,
            0});
        this._limitFirstEdit.Location = new System.Drawing.Point(42, 8);
        this._limitFirstEdit.Name = "_limitFirstEdit";
        this._limitFirstEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
        this._limitFirstEdit.Properties.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
        this._limitFirstEdit.Properties.Mask.EditMask = "N00";
        this._limitFirstEdit.Properties.MaxValue = new decimal(new int[] {
            500,
            0,
            0,
            0});
        this._limitFirstEdit.Size = new System.Drawing.Size(63, 20);
        this._limitFirstEdit.TabIndex = 3;
        this._limitFirstEdit.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this._limitEdit_EditValueChanging);
        // 
        // _limitSecondEdit
        // 
        this._limitSecondEdit.Dock = System.Windows.Forms.DockStyle.Fill;
        this._limitSecondEdit.EditValue = new decimal(new int[] {
            80,
            0,
            0,
            0});
        this._limitSecondEdit.Location = new System.Drawing.Point(42, 31);
        this._limitSecondEdit.Name = "_limitSecondEdit";
        this._limitSecondEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
        this._limitSecondEdit.Properties.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
        this._limitSecondEdit.Properties.Mask.EditMask = "N00";
        this._limitSecondEdit.Properties.MaxValue = new decimal(new int[] {
            500,
            0,
            0,
            0});
        this._limitSecondEdit.Size = new System.Drawing.Size(63, 20);
        this._limitSecondEdit.TabIndex = 4;
        this._limitSecondEdit.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this._limitEdit_EditValueChanging);
        // 
        // _limitThirdEdit
        // 
        this._limitThirdEdit.Dock = System.Windows.Forms.DockStyle.Fill;
        this._limitThirdEdit.EditValue = new decimal(new int[] {
            250,
            0,
            0,
            0});
        this._limitThirdEdit.Location = new System.Drawing.Point(42, 54);
        this._limitThirdEdit.Name = "_limitThirdEdit";
        this._limitThirdEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
        this._limitThirdEdit.Properties.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
        this._limitThirdEdit.Properties.Mask.EditMask = "N00";
        this._limitThirdEdit.Properties.MaxValue = new decimal(new int[] {
            500,
            0,
            0,
            0});
        this._limitThirdEdit.Size = new System.Drawing.Size(63, 20);
        this._limitThirdEdit.TabIndex = 5;
        this._limitThirdEdit.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this._limitEdit_EditValueChanging);
        // 
        // label4
        // 
        this.label4.AutoSize = true;
        this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
        this.label4.Location = new System.Drawing.Point(108, 5);
        this.label4.Margin = new System.Windows.Forms.Padding(0);
        this.label4.Name = "label4";
        this.label4.Size = new System.Drawing.Size(43, 23);
        this.label4.TabIndex = 6;
        this.label4.Text = "��/�";
        this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
        // 
        // label5
        // 
        this.label5.AutoSize = true;
        this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
        this.label5.Location = new System.Drawing.Point(108, 28);
        this.label5.Margin = new System.Windows.Forms.Padding(0);
        this.label5.Name = "label5";
        this.label5.Size = new System.Drawing.Size(43, 23);
        this.label5.TabIndex = 7;
        this.label5.Text = "��/�";
        this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
        // 
        // label6
        // 
        this.label6.AutoSize = true;
        this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
        this.label6.Location = new System.Drawing.Point(108, 51);
        this.label6.Margin = new System.Windows.Forms.Padding(0);
        this.label6.Name = "label6";
        this.label6.Size = new System.Drawing.Size(43, 23);
        this.label6.TabIndex = 8;
        this.label6.Text = "��/�";
        this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
        // 
        // _colorFirstEdit
        // 
        this._colorFirstEdit.AutoSize = true;
        this._colorFirstEdit.BackColor = System.Drawing.Color.Green;
        this._colorFirstEdit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
        this._colorFirstEdit.Cursor = System.Windows.Forms.Cursors.Hand;
        this._colorFirstEdit.Dock = System.Windows.Forms.DockStyle.Fill;
        this._colorFirstEdit.Location = new System.Drawing.Point(263, 8);
        this._colorFirstEdit.Margin = new System.Windows.Forms.Padding(3, 3, 10, 3);
        this._colorFirstEdit.Name = "_colorFirstEdit";
        this._colorFirstEdit.Size = new System.Drawing.Size(39, 17);
        this._colorFirstEdit.TabIndex = 9;
        this._colorFirstEdit.Click += new System.EventHandler(this._colorEdit_Click);
        // 
        // _colorSecondEdit
        // 
        this._colorSecondEdit.AutoSize = true;
        this._colorSecondEdit.BackColor = System.Drawing.Color.Yellow;
        this._colorSecondEdit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
        this._colorSecondEdit.Cursor = System.Windows.Forms.Cursors.Hand;
        this._colorSecondEdit.Dock = System.Windows.Forms.DockStyle.Fill;
        this._colorSecondEdit.Location = new System.Drawing.Point(263, 31);
        this._colorSecondEdit.Margin = new System.Windows.Forms.Padding(3, 3, 10, 3);
        this._colorSecondEdit.Name = "_colorSecondEdit";
        this._colorSecondEdit.Size = new System.Drawing.Size(39, 17);
        this._colorSecondEdit.TabIndex = 10;
        this._colorSecondEdit.Click += new System.EventHandler(this._colorEdit_Click);
        // 
        // _colorThirdEdit
        // 
        this._colorThirdEdit.AutoSize = true;
        this._colorThirdEdit.BackColor = System.Drawing.Color.Red;
        this._colorThirdEdit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
        this._colorThirdEdit.Cursor = System.Windows.Forms.Cursors.Hand;
        this._colorThirdEdit.Dock = System.Windows.Forms.DockStyle.Fill;
        this._colorThirdEdit.Location = new System.Drawing.Point(263, 54);
        this._colorThirdEdit.Margin = new System.Windows.Forms.Padding(3, 3, 10, 3);
        this._colorThirdEdit.Name = "_colorThirdEdit";
        this._colorThirdEdit.Size = new System.Drawing.Size(39, 17);
        this._colorThirdEdit.TabIndex = 11;
        this._colorThirdEdit.Click += new System.EventHandler(this._colorEdit_Click);
        // 
        // SpeedAnalyzerView
        // 
        this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        this.ClientSize = new System.Drawing.Size(342, 159);
        this.Controls.Add(this._mainTable);
        this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
        this.MaximumSize = new System.Drawing.Size(400, 250);
        this.MinimumSize = new System.Drawing.Size(320, 185);
        this.Name = "SpeedAnalyzerView";
        this.ShowInTaskbar = false;
        this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
        this.Text = "���������� �����";
        this._mainTable.ResumeLayout(false);
        ((System.ComponentModel.ISupportInitialize)(this._panel)).EndInit();
        this._panel.ResumeLayout(false);
        this._table.ResumeLayout(false);
        this._table.PerformLayout();
        ((System.ComponentModel.ISupportInitialize)(this._limitFirstEdit.Properties)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this._limitSecondEdit.Properties)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this._limitThirdEdit.Properties)).EndInit();
        this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.TableLayoutPanel _mainTable;
    private DevExpress.XtraEditors.SimpleButton _startBtn;
    private DevExpress.XtraEditors.SimpleButton _applyBtn;
    private DevExpress.XtraEditors.SimpleButton _cancelBtn;
    private DevExpress.XtraEditors.GroupControl _panel;
    private System.Windows.Forms.TableLayoutPanel _table;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label label3;
    private DevExpress.XtraEditors.SpinEdit _limitFirstEdit;
    private DevExpress.XtraEditors.SpinEdit _limitSecondEdit;
    private DevExpress.XtraEditors.SpinEdit _limitThirdEdit;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.Label _colorFirstEdit;
    private System.Windows.Forms.Label _colorSecondEdit;
    private System.Windows.Forms.Label _colorThirdEdit;
  }
}