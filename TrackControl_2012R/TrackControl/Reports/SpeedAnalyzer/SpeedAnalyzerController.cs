using System;
using System.Collections.Generic;
using System.Drawing;

using TrackControl.GMap;
using System.Windows.Forms;

namespace TrackControl.Reports.SpeedAnalyzer
{
  public class SpeedAnalyzerController
  {
    bool _isOn;
    KeyValuePair<float, Color> _limitFirst;
    KeyValuePair<float, Color> _limitSecond;
    KeyValuePair<float, Color> _limitThird;

    SpeedAnalyzerView _view;
    GoogleMapControl _map;

    public SpeedAnalyzerController(GoogleMapControl map)
    {
      _map = map;
      _limitFirst = new KeyValuePair<float, Color>(40f, Color.Blue);
      _limitSecond = new KeyValuePair<float, Color>(80f, Color.Green);
      _limitThird = new KeyValuePair<float, Color>(250f, Color.Red);
    }

    public void ShowView(Form owner)
    {
      if (null != _view && !_view.IsDisposed)
        return;

      _view = new SpeedAnalyzerView();
      _view.Owner = owner;
      _view.NewSettingsApplied += view_NewSettingsApplied;
      _view.NewSettingsCanceled += view_NewSettingsCanceled;
      _view.SpeedModeChanged += view_SpeedModeChanged;
      _view.Disposed += view_Disposed;
      _view.IsStarted = _isOn;
      resetView();
      _view.Show();
    }

    public void Start()
    {
      _isOn = true;
      Dictionary<float, Color> codes = getCodes();
      _map.SetValueCodes(codes);
      _map.DrawTrackMode = DrawTrackModes.DiffColors; 
      _map.Repaint();
    }

    public void Stop()
    {
      _isOn = false;
      _map.DrawTrackMode = DrawTrackModes.Simple; 
      _map.Repaint();
    }

    void view_NewSettingsApplied()
    {
      _limitFirst = _view.LimitFirst;
      _limitSecond = _view.LimitSecond;
      _limitThird = _view.LimitThird;

      Dictionary<float, Color> codes = getCodes();
      _map.SetValueCodes(codes);
      _map.Repaint();
    }

    void view_NewSettingsCanceled()
    {
      resetView();
    }

    void view_SpeedModeChanged()
    {
      if (_isOn)
        Stop();
      else
        Start();

      _view.IsStarted = _isOn;
    }

    void view_Disposed(object sender, EventArgs e)
    {
      _view.NewSettingsApplied -= view_NewSettingsApplied;
      _view.NewSettingsCanceled -= view_NewSettingsCanceled;
      _view.SpeedModeChanged -= view_SpeedModeChanged;
      _view.Disposed -= view_Disposed;
    }

    void resetView()
    {
      _view.LimitFirst = _limitFirst;
      _view.LimitSecond = _limitSecond;
      _view.LimitThird = _limitThird;
    }

    Dictionary<float, Color> getCodes()
    {
      Dictionary<float, Color> codes = new Dictionary<float, Color>(3);
      codes.Add(_limitFirst.Key, _limitFirst.Value);
      codes.Add(_limitSecond.Key, _limitSecond.Value);
      codes.Add(_limitThird.Key, _limitThird.Value);
      return codes;
    }
  }
}
