using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using TrackControl.General;
using TrackControl.Properties;

namespace TrackControl.Reports.SpeedAnalyzer
{
  public partial class SpeedAnalyzerView : XtraForm
  {
    public event MethodInvoker NewSettingsApplied = delegate { };
    public event MethodInvoker NewSettingsCanceled = delegate { };
    public event MethodInvoker SpeedModeChanged = delegate { };

    public SpeedAnalyzerView()
    {
      InitializeComponent();
      init();
      hideApplyCancelButtons();
    }

    public KeyValuePair<float, Color> LimitFirst
    {
      get
      {
        return new KeyValuePair<float, Color>(Convert.ToSingle(_limitFirstEdit.Value), _colorFirstEdit.BackColor);
      }
      set
      {
        _limitFirstEdit.EditValueChanging -= _limitEdit_EditValueChanging;
        _limitFirstEdit.Value = Convert.ToDecimal(value.Key);
        _colorFirstEdit.BackColor = value.Value;
        setNeutralFocus();
        _limitFirstEdit.EditValueChanging += _limitEdit_EditValueChanging;
      }
    }

    public KeyValuePair<float, Color> LimitSecond
    {
      get
      {
        return new KeyValuePair<float, Color>(Convert.ToSingle(_limitSecondEdit.Value), _colorSecondEdit.BackColor);
      }
      set
      {
        _limitSecondEdit.EditValueChanging -= _limitEdit_EditValueChanging;
        _limitSecondEdit.Value = Convert.ToDecimal(value.Key);
        _colorSecondEdit.BackColor = value.Value;
        setNeutralFocus();
        _limitSecondEdit.EditValueChanging += _limitEdit_EditValueChanging;
      }
    }

    public KeyValuePair<float, Color> LimitThird
    {
      get
      {
        return new KeyValuePair<float, Color>(Convert.ToSingle(_limitThirdEdit.Value), _colorThirdEdit.BackColor);
      }
      set
      {
        _limitThirdEdit.EditValueChanging -= _limitEdit_EditValueChanging;
        _limitThirdEdit.Value = Convert.ToDecimal(value.Key);
        _colorThirdEdit.BackColor = value.Value;
        setNeutralFocus();
        _limitThirdEdit.EditValueChanging += _limitEdit_EditValueChanging;
      }
    }

    public bool IsStarted
    {
      set
      {
        _startBtn.Image = value ? Shared.Stop : Shared.Start;
      }
    }

    void init()
    {
      _applyBtn.Text = Resources.Apply;
      _cancelBtn.Text = Resources.Cancel;
      _panel.Text = Resources.SpeedLimit;
      label1.Text = Resources.Before;
      label2.Text = Resources.Before;
      label3.Text = Resources.Before;
      label4.Text = Resources.SpeedUnit;
      label5.Text = Resources.SpeedUnit;
      label6.Text = Resources.SpeedUnit;
      Text = Resources.SpeedMode;
    }

    void idle_Click(object sender, EventArgs e)
    {
      setNeutralFocus();
    }

    void _limitEdit_EditValueChanging(object sender, ChangingEventArgs e)
    {
      showApplyCancelButtons();
    }

    void _colorEdit_Click(object sender, EventArgs e)
    {
      setNeutralFocus();
      using (ColorDialog dlg = new ColorDialog())
      {
        dlg.ShowHelp = false;
        if (DialogResult.OK == dlg.ShowDialog())
        {
          Label label = (Label)sender;
          label.BackColor = dlg.Color;
          showApplyCancelButtons();
        }
      }
    }

    void _startBtn_Click(object sender, EventArgs e)
    {
      SpeedModeChanged();
    }

    void _applyBtn_Click(object sender, EventArgs e)
    {
      hideApplyCancelButtons();
      NewSettingsApplied();
      setNeutralFocus();
    }

    void _cancelBtn_Click(object sender, EventArgs e)
    {
      hideApplyCancelButtons();
      NewSettingsCanceled();
    }

    void showApplyCancelButtons()
    {
      _startBtn.Visible = false;
      _applyBtn.Visible = true;
      _cancelBtn.Visible = true;
    }

    void hideApplyCancelButtons()
    {
      _startBtn.Visible = true;
      _applyBtn.Visible = false;
      _cancelBtn.Visible = false;
    }

    void setNeutralFocus()
    {
      ActiveControl = _panel;
    }
  }
}