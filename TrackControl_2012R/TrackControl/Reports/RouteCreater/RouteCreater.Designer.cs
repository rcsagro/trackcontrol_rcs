namespace TrackControl.Reports
{
    partial class RouteCreater
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gcPoints = new DevExpress.XtraGrid.GridControl();
            this.gvPoints = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colLat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLng = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDistance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.teLat = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.lbInfor = new DevExpress.XtraEditors.LabelControl();
            this.btCreateRoute = new DevExpress.XtraEditors.SimpleButton();
            this.rgMapType = new DevExpress.XtraEditors.RadioGroup();
            this.btClearRoute = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.gcPoints)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvPoints)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teLat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgMapType.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // gcPoints
            // 
            this.gcPoints.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gcPoints.Location = new System.Drawing.Point(0, 31);
            this.gcPoints.MainView = this.gvPoints;
            this.gcPoints.Name = "gcPoints";
            this.gcPoints.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.teLat});
            this.gcPoints.Size = new System.Drawing.Size(696, 275);
            this.gcPoints.TabIndex = 0;
            this.gcPoints.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvPoints,
            this.gridView2});
            // 
            // gvPoints
            // 
            this.gvPoints.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvPoints.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvPoints.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gvPoints.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvPoints.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvPoints.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvPoints.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvPoints.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvPoints.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gvPoints.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvPoints.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvPoints.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvPoints.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvPoints.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gvPoints.Appearance.Empty.Options.UseBackColor = true;
            this.gvPoints.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvPoints.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvPoints.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gvPoints.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvPoints.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gvPoints.Appearance.EvenRow.Options.UseForeColor = true;
            this.gvPoints.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvPoints.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvPoints.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gvPoints.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvPoints.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvPoints.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gvPoints.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvPoints.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gvPoints.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvPoints.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvPoints.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvPoints.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.gvPoints.Appearance.FixedLine.Options.UseBackColor = true;
            this.gvPoints.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gvPoints.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gvPoints.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gvPoints.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gvPoints.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(192)))), ((int)(((byte)(157)))));
            this.gvPoints.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(219)))), ((int)(((byte)(188)))));
            this.gvPoints.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gvPoints.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvPoints.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gvPoints.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvPoints.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvPoints.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvPoints.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvPoints.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvPoints.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvPoints.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gvPoints.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvPoints.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvPoints.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvPoints.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvPoints.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvPoints.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvPoints.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gvPoints.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvPoints.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvPoints.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gvPoints.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(242)))), ((int)(((byte)(213)))));
            this.gvPoints.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gvPoints.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gvPoints.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvPoints.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvPoints.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvPoints.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvPoints.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gvPoints.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvPoints.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gvPoints.Appearance.GroupRow.Options.UseForeColor = true;
            this.gvPoints.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvPoints.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvPoints.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gvPoints.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvPoints.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvPoints.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gvPoints.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvPoints.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvPoints.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gvPoints.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvPoints.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.gvPoints.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gvPoints.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvPoints.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvPoints.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvPoints.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvPoints.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gvPoints.Appearance.OddRow.Options.UseBackColor = true;
            this.gvPoints.Appearance.OddRow.Options.UseBorderColor = true;
            this.gvPoints.Appearance.OddRow.Options.UseForeColor = true;
            this.gvPoints.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(247)))));
            this.gvPoints.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gvPoints.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(148)))), ((int)(((byte)(148)))));
            this.gvPoints.Appearance.Preview.Options.UseBackColor = true;
            this.gvPoints.Appearance.Preview.Options.UseFont = true;
            this.gvPoints.Appearance.Preview.Options.UseForeColor = true;
            this.gvPoints.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvPoints.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gvPoints.Appearance.Row.Options.UseBackColor = true;
            this.gvPoints.Appearance.Row.Options.UseForeColor = true;
            this.gvPoints.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvPoints.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gvPoints.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvPoints.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.gvPoints.Appearance.SelectedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvPoints.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.gvPoints.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvPoints.Appearance.SelectedRow.Options.UseBorderColor = true;
            this.gvPoints.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gvPoints.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gvPoints.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gvPoints.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvPoints.Appearance.VertLine.Options.UseBackColor = true;
            this.gvPoints.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colLat,
            this.colLng,
            this.colDistance,
            this.colLocation});
            this.gvPoints.GridControl = this.gcPoints;
            this.gvPoints.IndicatorWidth = 30;
            this.gvPoints.Name = "gvPoints";
            this.gvPoints.OptionsView.EnableAppearanceEvenRow = true;
            this.gvPoints.OptionsView.EnableAppearanceOddRow = true;
            this.gvPoints.OptionsView.ShowGroupPanel = false;
            this.gvPoints.CustomDrawRowIndicator += new DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventHandler(this.gvPoints_CustomDrawRowIndicator);
            // 
            // colLat
            // 
            this.colLat.AppearanceCell.Options.UseTextOptions = true;
            this.colLat.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLat.AppearanceHeader.Options.UseTextOptions = true;
            this.colLat.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLat.Caption = "Lat";
            this.colLat.DisplayFormat.FormatString = "N5";
            this.colLat.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colLat.FieldName = "Lat";
            this.colLat.Name = "colLat";
            this.colLat.OptionsColumn.AllowEdit = false;
            this.colLat.OptionsColumn.AllowFocus = false;
            this.colLat.OptionsColumn.ReadOnly = true;
            this.colLat.Visible = true;
            this.colLat.VisibleIndex = 0;
            this.colLat.Width = 128;
            // 
            // colLng
            // 
            this.colLng.AppearanceCell.Options.UseTextOptions = true;
            this.colLng.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLng.AppearanceHeader.Options.UseTextOptions = true;
            this.colLng.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLng.Caption = "Lng";
            this.colLng.DisplayFormat.FormatString = "N5";
            this.colLng.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colLng.FieldName = "Lng";
            this.colLng.Name = "colLng";
            this.colLng.OptionsColumn.AllowEdit = false;
            this.colLng.OptionsColumn.AllowFocus = false;
            this.colLng.OptionsColumn.ReadOnly = true;
            this.colLng.Visible = true;
            this.colLng.VisibleIndex = 1;
            this.colLng.Width = 147;
            // 
            // colDistance
            // 
            this.colDistance.AppearanceCell.Options.UseTextOptions = true;
            this.colDistance.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDistance.AppearanceHeader.Options.UseTextOptions = true;
            this.colDistance.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDistance.Caption = "����, ��";
            this.colDistance.FieldName = "Distance";
            this.colDistance.Name = "colDistance";
            this.colDistance.OptionsColumn.AllowEdit = false;
            this.colDistance.OptionsColumn.ReadOnly = true;
            this.colDistance.Visible = true;
            this.colDistance.VisibleIndex = 2;
            this.colDistance.Width = 154;
            // 
            // colLocation
            // 
            this.colLocation.AppearanceHeader.Options.UseTextOptions = true;
            this.colLocation.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLocation.Caption = "��������������";
            this.colLocation.FieldName = "Location";
            this.colLocation.Name = "colLocation";
            this.colLocation.OptionsColumn.AllowEdit = false;
            this.colLocation.OptionsColumn.AllowFocus = false;
            this.colLocation.OptionsColumn.ReadOnly = true;
            this.colLocation.Visible = true;
            this.colLocation.VisibleIndex = 3;
            this.colLocation.Width = 808;
            // 
            // teLat
            // 
            this.teLat.AllowFocused = false;
            this.teLat.AutoHeight = false;
            this.teLat.Name = "teLat";
            // 
            // gridView2
            // 
            this.gridView2.GridControl = this.gcPoints;
            this.gridView2.Name = "gridView2";
            // 
            // lbInfor
            // 
            this.lbInfor.AllowHtmlString = true;
            this.lbInfor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lbInfor.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbInfor.Location = new System.Drawing.Point(12, 0);
            this.lbInfor.Name = "lbInfor";
            this.lbInfor.Size = new System.Drawing.Size(676, 25);
            this.lbInfor.TabIndex = 1;
            this.lbInfor.Text = "�������� �� �����  ������������������ ����� ��� ��������. ����� ����� ������� ���" +
                "���.";
            // 
            // btCreateRoute
            // 
            this.btCreateRoute.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btCreateRoute.Location = new System.Drawing.Point(604, 322);
            this.btCreateRoute.Name = "btCreateRoute";
            this.btCreateRoute.Size = new System.Drawing.Size(84, 23);
            this.btCreateRoute.TabIndex = 2;
            this.btCreateRoute.Text = "�������";
            this.btCreateRoute.Click += new System.EventHandler(this.btCreateRoute_Click);
            // 
            // rgMapType
            // 
            this.rgMapType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.rgMapType.EditValue = 1;
            this.rgMapType.Location = new System.Drawing.Point(12, 322);
            this.rgMapType.Name = "rgMapType";
            this.rgMapType.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.rgMapType.Properties.Appearance.Options.UseBackColor = true;
            this.rgMapType.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(1, "GoogleMap"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(32, "OpenStreetMap")});
            this.rgMapType.Size = new System.Drawing.Size(228, 23);
            this.rgMapType.TabIndex = 3;
            this.rgMapType.SelectedIndexChanged += new System.EventHandler(this.rgMapType_SelectedIndexChanged);
            // 
            // btClearRoute
            // 
            this.btClearRoute.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btClearRoute.Location = new System.Drawing.Point(500, 322);
            this.btClearRoute.Name = "btClearRoute";
            this.btClearRoute.Size = new System.Drawing.Size(84, 23);
            this.btClearRoute.TabIndex = 4;
            this.btClearRoute.Text = "��������";
            this.btClearRoute.Click += new System.EventHandler(this.btClearRoute_Click);
            // 
            // RouteCreater
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(700, 357);
            this.Controls.Add(this.btClearRoute);
            this.Controls.Add(this.rgMapType);
            this.Controls.Add(this.btCreateRoute);
            this.Controls.Add(this.lbInfor);
            this.Controls.Add(this.gcPoints);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "RouteCreater";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "�������� ��������";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.RouteCreater_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.gcPoints)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvPoints)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teLat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgMapType.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gcPoints;
        private DevExpress.XtraGrid.Views.Grid.GridView gvPoints;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn colLat;
        private DevExpress.XtraGrid.Columns.GridColumn colLng;
        private DevExpress.XtraGrid.Columns.GridColumn colLocation;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit teLat;
        private DevExpress.XtraEditors.LabelControl lbInfor;
        private DevExpress.XtraEditors.SimpleButton btCreateRoute;
        private DevExpress.XtraEditors.RadioGroup rgMapType;
        private DevExpress.XtraEditors.SimpleButton btClearRoute;
        private DevExpress.XtraGrid.Columns.GridColumn colDistance;
    }
}