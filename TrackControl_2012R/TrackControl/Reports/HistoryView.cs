using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using LocalCache;
using TrackControl.Properties;
using TrackControl.General.Patameters;

namespace TrackControl.Reports
{
    public partial class HistoryView : XtraUserControl
    {
        #region    -- ����  --

        AppModel _model = AppModel.Instance;
        CDailyHystory _dailyHistory = new CDailyHystory();
        int _depth = 31;
        ReportVehicle _reportVehicle;
        IList<ReportVehicle> _checkedVehicles;

        public IList<ReportVehicle> CheckedVehicles
        {
            get { return _checkedVehicles; }
            set { _checkedVehicles = value; }
        }

        /// <summary>
        /// ������������ ������ ������
        /// </summary>
        /// <param name="DatAnaliz"> ���� �������</param>
        /// <param name="TT"> �������� </param>
        /// <returns></returns>
        private delegate DataTable AnalizData(DateTime DatAnaliz, int TT);

        private struct SDailyAnaliz
        {
            public int PontsTotal;
            public double PointsSkipPercent;
            public double PointsNoValidPercent;
            public double PointsMaxInterval;
        }

        private Dictionary<int, Dictionary<DateTime, SDailyAnaliz>> _mobitelsParameters =
            new Dictionary<int, Dictionary<DateTime, SDailyAnaliz>>();

        enum CalcTypes
        {
            NothingType,
            DaysType,
            AnalizType
        }

        CalcTypes _currentCalcType = CalcTypes.NothingType;
        bool _stopAnaliz;

        #endregion

        public HistoryView()
        {
            InitializeComponent();
            init();
        }

        public void ShowDays(ReportVehicle reportVehicle)
        {
            _reportVehicle = reportVehicle;
            if (!_analizBtn.Enabled) return;
            if (!_historyWorker.IsBusy)
                bindVehicleDays();
        }

        #region --   GUI   --

        void HistoryView_Load(object sender, EventArgs e)
        {
            TotalParamsProvider provTotal = new TotalParamsProvider();

            if (provTotal.IsDataAnalizOn) 
                refreshListBox();
        }

        private void _listBox_DrawItem(object sender, ListBoxDrawItemEventArgs e)
        {
            AppearanceObject ap = e.Appearance;
            string text = String.Format("   {0:d}", e.Item);
            if ((e.State & DrawItemState.Selected) != 0)
            {
                e.Cache.FillRectangle(e.Cache.GetSolidBrush(ap.ForeColor), e.Bounds);
                e.Cache.DrawString(
                    text,
                    new Font(ap.Font.Name, ap.Font.Size, FontStyle.Bold),
                    e.Cache.GetSolidBrush(ap.BackColor),
                    e.Bounds,
                    ap.GetStringFormat());
            }
            else
            {
                e.Cache.DrawString(text, ap.Font, e.Cache.GetSolidBrush(ap.ForeColor), e.Bounds, ap.GetStringFormat());
            }
            // ������, ������������ ��������� ������ �� ���� 
            if ((_reportVehicle != null) && _mobitelsParameters.ContainsKey(_reportVehicle.Mobitel.Id))
            {
                Dictionary<DateTime, SDailyAnaliz> _datesParameters = _mobitelsParameters[_reportVehicle.Mobitel.Id];
                if (_datesParameters.ContainsKey((DateTime) e.Item))
                {
                    Point p = new Point(e.Bounds.X + 70, e.Bounds.Y);
                    if (_datesParameters[(DateTime) e.Item].PointsNoValidPercent +
                        _datesParameters[(DateTime) e.Item].PointsSkipPercent >= 5)
                    {
                        e.Cache.Graphics.DrawImage(icItemIcons.Images["R"], p);
                    }
                    else if (_datesParameters[(DateTime) e.Item].PointsNoValidPercent +
                             _datesParameters[(DateTime) e.Item].PointsSkipPercent >= 1)
                    {
                        e.Cache.Graphics.DrawImage(icItemIcons.Images["Y"], p);
                    }
                    else
                    {
                        e.Cache.Graphics.DrawImage(icItemIcons.Images["G"], p);
                    }
                }
            }
            e.Handled = true;
        }

        void _daysCount_EditValueChanged(object sender, EventArgs e)
        {
            _depth = Convert.ToInt32(_daysCount.EditValue);
        }

        void _refreshBtn_ItemClick(object sender, ItemClickEventArgs e)
        {
            refreshListBox();
        }

        private void _analizBtn_ItemClick(object sender, ItemClickEventArgs e)
        {
            EnabledControls(false);
            analizDataForValidAndSkip();
        }

        private void ttcList_GetActiveObjectInfo(object sender, ToolTipControllerGetActiveObjectInfoEventArgs e)
        {
            if (e.SelectedControl != _listBox) return;
            int index = _listBox.IndexFromPoint(e.ControlMousePosition);
            if (index < 0) return;
            object item = _listBox.GetItem(index);
            if (!_mobitelsParameters.ContainsKey(_reportVehicle.Mobitel.Id)) return;
            Dictionary<DateTime, SDailyAnaliz> _datesParameters = _mobitelsParameters[_reportVehicle.Mobitel.Id];
            if (_datesParameters.ContainsKey((DateTime) item))
            {
                string toolTip =
                    String.Format(
                        Resources.PointsTotal + ": {0}{1}" + Resources.Missed + ": {2}%{1}" + Resources.NonValid +
                        ": {3}%{1}" + Resources.MaxInterval + ": {4} " + Resources.MinutesShort,
                        _datesParameters[(DateTime) item].PontsTotal, Environment.NewLine,
                        _datesParameters[(DateTime) item].PointsSkipPercent,
                        _datesParameters[(DateTime) item].PointsNoValidPercent,
                        _datesParameters[(DateTime) item].PointsMaxInterval);
                e.Info = new ToolTipControlInfo(item, toolTip);
                Application.DoEvents();
            }
        }

        private void _listBox_DoubleClick(object sender, EventArgs e)
        {
            viewDataGps();
        }

        private void _stopBtn_ItemClick(object sender, ItemClickEventArgs e)
        {
            switch (_currentCalcType)
            {
                case CalcTypes.DaysType:
                    _stopBtn.Enabled = false;
                    _historyWorker.CancelAsync();

                    DateTime tm = DateTime.Now;

                    break;
                case CalcTypes.AnalizType:
                    _stopAnaliz = true;
                    break;
            }

        }

        #region --   Worker   --

        void _historyWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            if (chDateDevice.Checked)
            {
                if (_reportVehicle != null)
                {
                    int mobitel_current = _reportVehicle.Mobitel.Id;
                    _model.DataViewAdapter.GetDailyHistory(_model.DataSet, _dailyHistory, _depth, _historyWorker,
                        mobitel_current);
                }
                else
                {
                    XtraMessageBox.Show(Resources.MessageHistory, "History View", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                }
            }
            else
            {
                _model.DataViewAdapter.GetDailyHistory(_model.DataSet, _dailyHistory, _depth, _historyWorker);
            }
        }

        void _historyWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            _progress.EditValue = e.ProgressPercentage;
        }

        void _historyWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            EnabledControls(true);
            _progress.Visibility = BarItemVisibility.Never;
            _stopBtn.Visibility = _progress.Visibility;
            bindVehicleDays();
            _currentCalcType = CalcTypes.NothingType;
        }

        #endregion

        #region --    ����������� ����   --

        void _asBegin_ItemClick(object sender, ItemClickEventArgs e)
        {
            DateTime dt = (DateTime) _listBox.SelectedItem;
            PeriodService.Period.Begin = new DateTime(dt.Year, dt.Month, dt.Day, 0, 0, 0);
        }

        void _asFinish_ItemClick(object sender, ItemClickEventArgs e)
        {
            DateTime dt = (DateTime) _listBox.SelectedItem;
            PeriodService.Period.End = new DateTime(dt.Year, dt.Month, dt.Day, 23, 59, 0);
        }

        void _asPeriod_ItemClick(object sender, ItemClickEventArgs e)
        {
            DateTime dt = (DateTime) _listBox.SelectedItem;
            ReportPeriod period = new ReportPeriod(
                new DateTime(dt.Year, dt.Month, dt.Day, 0, 0, 0),
                new DateTime(dt.Year, dt.Month, dt.Day, 23, 59, 0));
            PeriodService.Period = period;
        }

        private void _viewDataGps_ItemClick(object sender, ItemClickEventArgs e)
        {
            viewDataGps();
        }

        void _popup_BeforePopup(object sender, CancelEventArgs e)
        {
            if (_listBox.SelectedIndex < 0)
            {
                e.Cancel = true;
                return;
            }
            _listBox.HotTrackItems = false;
        }

        void _popup_CloseUp(object sender, EventArgs e)
        {
            _listBox.HotTrackItems = true;
        }

        #endregion

        #endregion

        #region --   �������   --

        void init()
        {
            _daysCount.Caption = Resources.History_Count;
            _daysCount.EditValue = _depth;
            _daysCount.Hint = Resources.History_CountHint;
            _refreshBtn.Caption = Resources.Refresh;
            _refreshBtn.Hint = Resources.History_RefreshHint;
            _analizBtn.Caption = Resources.DataAnalysis;
            _analizBtn.Hint = Resources.DataAnalysisDecript;
            _stopBtn.Caption = Resources.Stop;
            _asBegin.Caption = Resources.SetAsBegin;
            _asFinish.Caption = Resources.SetAsEnd;
            _asPeriod.Caption = Resources.SetAsPeriod;
            _viewDataGps.Caption = Resources.DetailedDataGPS;
            _vehicleLbl.Caption = String.Empty;
            //
        }

        void refreshListBox()
        {
            try
            {
                if (_historyWorker.IsBusy)
                {
                    MessageBox.Show("Can not start again! Try again later.", "Error run refresh list box ",
                        MessageBoxButtons.OK);
                    return;
                }

                DateTime tm = DateTime.Now;

                EnabledControls(false);
                _currentCalcType = CalcTypes.DaysType;
                _progress.Visibility = BarItemVisibility.Always;
                _stopBtn.Visibility = _progress.Visibility;
                _stopBtn.Enabled = true;
                _progressRepo.Maximum = _model.DataSet.mobitels.Count;
                _historyWorker.RunWorkerAsync();
                tm = DateTime.Now;
            }
            catch (Exception ex)
            {
                throw new Exception("refreshListBox:" + String.Format("{0}\r\n{1}",
                    ex.Message, ex.InnerException == null ? "" : ex.InnerException.Message),
                    ex.InnerException);
            }
        }

        void bindVehicleDays()
        {
            if (null != _reportVehicle)
            {
                _vehicleLbl.Caption = String.Format(" {0}", _reportVehicle.RegNumber);

                DateTime[] days = _dailyHistory.GetData(_reportVehicle.Mobitel.Id);
                _listBox.DataSource = days;
                _listBox.HotTrackItems = days.Length > 0;
            }
            else
            {
                _vehicleLbl.Caption = Resources.VehicleNotSelected;
            }
        }

        void CreateColumnsForDataDable(DataTable dt)
        {
            if(!dt.Columns.Contains("DataGps_ID"))
                dt.Columns.Add(new DataColumn("DataGps_ID", Type.GetType("System.Int64")));
            if (!dt.Columns.Contains("Lat"))
                dt.Columns.Add(new DataColumn("Lat", Type.GetType("System.Double")));
            if (!dt.Columns.Contains("Lon"))
                dt.Columns.Add(new DataColumn("Lon", Type.GetType("System.Double")));
            if (!dt.Columns.Contains("UT"))
                dt.Columns.Add(new DataColumn("UT", Type.GetType("System.DateTime")));
            if (!dt.Columns.Contains("Speed"))
                dt.Columns.Add(new DataColumn("Speed", Type.GetType("System.Double")));
            if (!dt.Columns.Contains("Valid"))
                dt.Columns.Add(new DataColumn("Valid", Type.GetType("System.Boolean")));
            if (!dt.Columns.Contains("SrvPacketID"))
                dt.Columns.Add(new DataColumn("SrvPacketID", Type.GetType("System.Int64")));
            if (!dt.Columns.Contains("LogID"))
                dt.Columns.Add(new DataColumn("LogID", Type.GetType("System.Int32")));
            if (!dt.Columns.Contains("ValidInt"))
                dt.Columns.Add(new DataColumn("ValidInt", Type.GetType("System.Int32")));
            if (!dt.Columns.Contains("ValidIntNo"))
                dt.Columns.Add(new DataColumn("ValidIntNo", Type.GetType("System.Int32")));
            if (!dt.Columns.Contains("Break"))
                dt.Columns.Add(new DataColumn("Break", Type.GetType("System.Int32")));
            if (!dt.Columns.Contains("Sensors"))
                dt.Columns.Add(new DataColumn("Sensors", Type.GetType("System.String")));
            if (!dt.Columns.Contains("Dist"))
                dt.Columns.Add(new DataColumn("Dist", Type.GetType("System.Double")));
            if (!dt.Columns.Contains("SpeedCalc"))
                dt.Columns.Add(new DataColumn("SpeedCalc", Type.GetType("System.Double")));

            //if (_isTeletrack64)
            //{
            //    if (dt.Columns.Contains("RssiGsm"))
            //        dt.Columns.Add(new DataColumn("RssiGsm", Type.GetType("System.Int16")));
            //    if (dt.Columns.Contains("Sattelites"))
            //        dt.Columns.Add(new DataColumn("Sattelites", Type.GetType("System.Int16")));
            //    if (dt.Columns.Contains("Voltage"))
            //        dt.Columns.Add(new DataColumn("Voltage", Type.GetType("System.Double")));
            //    if (dt.Columns.Contains("Events"))
            //        dt.Columns.Add(new DataColumn("Events", Type.GetType("System.String")));
            //}
        }

        void analizDataForValidAndSkip()
        {
            _mobitelsParameters.Clear();
            if (_checkedVehicles == null || _checkedVehicles.Count == 0)
            {
                if (_reportVehicle == null)
                {
                    EnabledControls(true);
                    return;
                }
                else
                {
                    _checkedVehicles = new List<ReportVehicle>();
                    _checkedVehicles.Add(_reportVehicle);
                }
            }
            else
            {
                if (_reportVehicle != null && !_checkedVehicles.Contains(_reportVehicle))
                    _checkedVehicles.Add(_reportVehicle);
            }
            _currentCalcType = CalcTypes.AnalizType;
            PointsAnalyzer pa = new PointsAnalyzer();
            _progress.Visibility = BarItemVisibility.Always;
            _stopBtn.Visibility = _progress.Visibility;
            _stopBtn.Enabled = true;
            foreach (ReportVehicle rv in _checkedVehicles)
            {
                if (rv != null)
                {
                    _vehicleLbl.Caption = String.Format(" {0}", rv.RegNumber);

                    DateTime[] _days = _dailyHistory.GetData(rv.Mobitel.Id);
                    if (_days.Length > 0)
                    {
                        Dictionary<DateTime, SDailyAnaliz> _datesParameters = new Dictionary<DateTime, SDailyAnaliz>();
                        _mobitelsParameters.Add(rv.Mobitel.Id, _datesParameters);
                        _progressRepo.Maximum = _days.Length + 1;
                        AnalizData ad = new AnalizData(pa.GetData);
                        IAsyncResult iftAR = ad.BeginInvoke(_days[0], rv.Mobitel.Id, null, null);
                        for (int i = 1; i <= _days.Length; i++)
                        {
                            _progress.EditValue = i + 1;
                            while (!iftAR.IsCompleted)
                            {
                                Application.DoEvents();
                            }
                            DataTable dt = ad.EndInvoke(iftAR);
                            if (i < _days.Length) 
                                iftAR = ad.BeginInvoke(_days[i], rv.Mobitel.Id, null, null);

                            CreateColumnsForDataDable(dt);
                            pa.PointsAnaliz(dt, rv.Mobitel.Id);
                            SDailyAnaliz sda = new SDailyAnaliz();
                            sda.PontsTotal = pa.PointsTotal;
                            sda.PointsSkipPercent = pa.PointsSkipPercent;
                            sda.PointsNoValidPercent = pa.PointsNoValidPercent;
                            sda.PointsMaxInterval = pa.PointsMaxInterval;
                            _datesParameters.Add(_days[i - 1], sda);
                            if (_reportVehicle != null && rv.Mobitel.Id == _reportVehicle.Mobitel.Id)
                                _listBox.Refresh();
                            if (_stopAnaliz)
                            {
                                break;
                            }
                        } // for
                    } // if
                    if (_stopAnaliz)
                    {
                        _stopAnaliz = false;
                        break;
                    }
                } // if
            } // foreach
            _progress.Visibility = BarItemVisibility.Never;
            _stopBtn.Visibility = _progress.Visibility;
            EnabledControls(true);
            _currentCalcType = CalcTypes.NothingType;
        }

        void viewDataGps()
        {
            if (_model.VehiclesModel.Vehicles.Count > 0)
            {
                if (_listBox.SelectedItem != null)
                {
                    DataGPSAnalyzer da = new DataGPSAnalyzer((DateTime) _listBox.SelectedItem, _reportVehicle);
                    da.Show();
                }
            }
        }

        void EnabledControls(bool enable)
        {
            _listBox.Enabled = enable;
            _daysCount.Enabled = enable;
            _analizBtn.Enabled = enable;
            _refreshBtn.Enabled = enable;
        }

        #endregion

        private void chDateDevice_CheckedChanged(object sender, ItemClickEventArgs e)
        {
            if (chDateDevice.Checked == true)
            {
                chDateDevice.Caption = Resources.OneDate; // "�� ������";
                chDateDevice.Hint = Resources.DateDeviceHintOne; //"�������� ���� �� ���������� ������";
            }

            if (chDateDevice.Checked == false)
            {
                chDateDevice.Caption = Resources.ManyDate; //"�� ����";
                chDateDevice.Hint = Resources.DateDeviceHintMany; //"�������� ���� �� ���� �������";
            }
        }
    }
}
