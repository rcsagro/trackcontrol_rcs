using System.Collections.Generic;
using System.Drawing;
using DevExpress.XtraBars;
using DevExpress.XtraBars.Docking;
using DevExpress.XtraEditors;
using DevExpress.XtraTab;

using System;
using System.IO;
using System.Xml;
using System.Windows.Forms;
using LocalCache;
using TrackControl.General;
using TrackControl.GMap;
using TrackControl.GMap.Core;
using TrackControl.Properties;
using TrackControl.Reports;
using TrackControl.Vehicles;
using TrackControl.Zones;

using BaseReports;
using BaseReports.ReportsDE;
using BaseReports.Procedure;
using BaseReports.Properties;
using BaseReports.RFID;
using Report;

namespace TrackControl.Reports
{
    public partial class ReportsForm : XtraForm
    {
        private const string KEY = "Rep_ing";

        private AppModel _app;
        private IMap _map;
        private VehicleTreeView _vehicleTree;
        private ZonesTreeView _zonesTree;
        private FormInterval formInterval = null;
        private bool workProcessing = false;
       
        public event MethodInvoker MapChanged = delegate { };
        public event MethodInvoker ShowZoneEditorEvent;
        public event MethodInvoker StartClicked = delegate { };
        public event MethodInvoker TrackAnalizerClicked = delegate { };
        public event MethodInvoker TrackAnalizerStop = delegate { };
        public event MethodInvoker SpeedAnalizerClicked = delegate { };
        public event MethodInvoker Select2PointsClicked = delegate { };
        public event MethodInvoker RouteCreaterClicked = delegate { };
        public event Action<bool> DrawTrackModeChanged = delegate { };
        public event Action<bool> AutoZoomModeChanged = delegate { };
        //public event PointClick onPointClickedGoogle;
        public event MethodInvoker RunAddressFinder = delegate { };
        
        public ReportsForm(VehicleTreeView vehicleTree, ZonesTreeView zonesTree)
        {
            formInterval = new FormInterval();

            _vehicleTree = vehicleTree;
            _zonesTree = zonesTree;
            _app = AppModel.Instance;
            InitializeComponent();
            init();
            _eventsLogControl.Init(_app.DataSet);
            //_app.GoogleMapControl.MapType = MapType.GoogleMap;
            string currentMap = UseMap.getTypeMap();
            object mapEnum = System.Enum.Parse(typeof(MapType), currentMap);
            MapType selectMap = (MapType)mapEnum;
            _app.GoogleMapControl.MapType = selectMap;
            SizeChanged += ReportsForm_SizeChanged;
            _map = _app.GoogleMapControl;

            RefreshTree.refreshReport = new RefreshingTree(_vehicleTree.RefreshTree);
        }

        private void ReportsForm_SizeChanged(object sender, EventArgs e)
        {
            GoogleMapControl.xtabControl = _xtraTabs;
        }

        /// <summary>
        /// ��������� ������������� �����
        /// </summary>
        public IMap Map
        {
            get { return _map; }
        }

        /// <summary>
        /// ��������� ������ "�������������������"
        /// </summary>
        public bool AutoZoom
        {
            set { _autoZoomBtn.Checked = value; }
        }

        public void Advise()
        {
            _vehiclePanel.ControlContainer.Controls.Add(_vehicleTree);
            _vehicleTree.VehicleSelected += VehicleTree_VehicleSelected;
            _zonesPanel.ControlContainer.Controls.Add(_zonesTree);

            _app.ReportsMode.HistoryView.Parent = historyPanel_Container;
            _app.ReportsMode.ZGraph.Parent = _graphTab;
            _app.ReportsMode.Reports.Parent = reportsPanel_DockContainer;
            _gisTab.Controls.Add(_app.GisMapControl);
            _gisTab.Tag = _app.GisMapControl;

            // ������������� ������ ����� �����
            _app.GoogleMapControl.ShowVideoTrackControl = true;
            GoogleMapControl.mobitelId = KilometrageControl.MobitelId;
            GoogleMapControl.datasetForMap = ReportTabControl.Dataset;
            GoogleMapControl.WorkThread.trSegmentsShowNeededWith = new GoogleMapControl.WorkThread.TrackSegmentsShowNeededWith( ReportsControl.OnTrackSegmentsShowNeededWith );
            GoogleMapControl.WorkThread.mrkrShowNeededWith =
                new GoogleMapControl.WorkThread.MarkersShowNeededWith( ReportsControl.OnMarkersShowNeededWith );
            GoogleMapControl.WorkThread.clrMapObjectsNeeded =
                new GoogleMapControl.WorkThread.OnClearMapObjectsNeeded(ReportsControl.OnClearMapObjectsNeeded);
            // =================================================================================================

            _googleTab.Controls.Add(_app.GoogleMapControl);
            _googleTab.Tag = _app.GoogleMapControl;
            _map = _app.GoogleMapControl;
            noticeLog.StartMonitoringOnLine += AppController.Instance.RunMonitoringOnLine;
            noticeLog.StartMonitoringPeriod += AppController.Instance.RunMonitoringPeriod;
            noticeLog.StopMonitoringOnLine += AppController.Instance.StopMonitoringOnLine;
            noticeLog.StopMonitoringPeriod += AppController.Instance.StopMonitoringPeriod;
            PeriodService.PeriodChanged += updatePeriodEditors;
            updatePeriodEditors();
        }

        public IMap setMap
        {
            set { _map = value; }
        }

        public void Release()
        {
            _vehicleTree.Parent = null;
            _vehicleTree.VehicleSelected -= VehicleTree_VehicleSelected;
            _zonesTree.Parent = null;

            _app.ReportsMode.HistoryView.Parent = null;
            _app.ReportsMode.ZGraph.Parent = null;
            _app.ReportsMode.Reports.Parent = null;
            _app.GisMapControl.Parent = null;
            _app.GisMapControl.Tag = null;
            _app.GoogleMapControl.Parent = null;
            _app.GoogleMapControl.Tag = null;
            _app.GoogleMapControl.ShowVideoTrackControl = false;
            PeriodService.PeriodChanged -= updatePeriodEditors;
            noticeLog.StartMonitoringOnLine -= AppController.Instance.RunMonitoringOnLine;
            noticeLog.StartMonitoringPeriod -= AppController.Instance.RunMonitoringPeriod;
            noticeLog.StopMonitoringOnLine -= AppController.Instance.StopMonitoringOnLine;
            noticeLog.StopMonitoringPeriod -= AppController.Instance.StopMonitoringPeriod;
        }

        public void RestoreLayout()
        {
			try 
			{
				string path = LayoutService.GetPath (KEY);

				if (!File.Exists (path)) 
				{
					ResetLayout ();
				}

				try 
				{
					_dockManager.RestoreLayoutFromXml (path);
				} 
				catch (Exception) 
				{
					ResetLayout ();
					_dockManager.RestoreLayoutFromXml (path);
				}

				_sensorsItem.Checked = DockVisibility.Hidden != _sensorsPanel.Visibility;
				_historyItem.Checked = DockVisibility.Hidden != _historyPanel.Visibility;
				_logItem.Checked = DockVisibility.Hidden != _logPanel.Visibility;

				if (UserBaseCurrent.Instance.IsModuleVisible ((int)TrackControlModules.Notifications)) 
				{
					_noticePanel.Visibility = DockVisibility.AutoHide;
				} 
				else 
				{
					_noticePanel.Visibility = DockVisibility.Hidden;
				}
			}
			catch(Exception ex)
			{
				XtraMessageBox.Show (ex.Message + "\n" + ex.StackTrace, "Error Restore Layout", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
        } // RestoreLayout

		public void SaveLayout()
        {
            _dockManager.SaveLayoutToXml(LayoutService.GetPath(KEY));
        }

        public static void ResetLayout()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(Resources.Layout_Reports);
            string path = LayoutService.GetPath(KEY);
            doc.Save(path);
        }

        public void InitProgress(string before, string after, int value)
        {
            _statusLbBefore.Caption = before;
            _statusLbAfter.Caption = after;
            _progress.EditValue = 0;
            _progressRepo.Maximum = value;
            _progress.Visibility = BarItemVisibility.Always;
        }

        public void UpdateProgress(int value)
        {
            _progress.EditValue = value;
        }

        public bool isWorkProcessing()
        {
            return workProcessing;
        }

        public void SetWorkProcessing(bool state)
        {
            workProcessing = state;
        }

        public void ShowButtonCancel()
        {
            btnCancelProcessing.Visibility = BarItemVisibility.Always;
        }

        public void UpdateProgress(string before, string after, int value)
        {
            _statusLbBefore.Caption = before;
            _statusLbAfter.Caption = after;
            _progress.EditValue = value;
        }

        public void FinishLoading()
        {
            loadingEnded();
        }

        public void LoadingFailed(string message)
        {
            loadingEnded();
            XtraMessageBox.Show(message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public void ShowZonesPanel()
        {
            _zonesPanel.Show();
        }

        public void ShowMapTab()
        {
            _xtraTabs.SelectedTabPage = (_map == _app.GisMapControl) ? _gisTab : _googleTab;
        }

        public void ShowGraphTab()
        {
            _xtraTabs.SelectedTabPage = _graphTab;
        }

        public void ShowVehiclesTree()
        {
            _vehiclePanel.Show();
        }

        #region --   ����� ������� ��� �������   --

        /// <summary>
        /// ���� �� ������ "�� �������"
        /// </summary>
        private void _todayItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            PeriodService.Period = ReportPeriod.Today;
        }

        /// <summary>
        /// ���� �� ������ "�� �����"
        /// </summary>
        private void _yesterdayItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            PeriodService.Period = ReportPeriod.Yesterday;
        }

        /// <summary>
        /// ���� �� ������ "�� ��� ������"
        /// </summary>
        private void _weekItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            PeriodService.Period = ReportPeriod.ThisWeek;
        }

        /// <summary>
        /// ���� �� ������ "�� ��������� 7 ����"
        /// </summary>
        private void _last7daysItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            PeriodService.Period = ReportPeriod.LastSevenDays;
        }

        /// <summary>
        /// ���� �� ������ "�� ���� �����"
        /// </summary>
        private void _monthItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            PeriodService.Period = ReportPeriod.ThisMonth;
        }

        /// <summary>
        /// ���� �� ������ "�� ��������� 30 ����"
        /// </summary>
        private void _last30daysItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            PeriodService.Period = ReportPeriod.LastThirtyDays;
        }

        /// <summary>
        /// � ��������� ���� ������� ����� �������� ��� ������ �������
        /// </summary>
        private void _fromItem_EditValueChanged(object sender, EventArgs e)
        {
            PeriodService.Period.Begin = (DateTime) _fromItem.EditValue;
        }

        /// <summary>
        /// � ��������� ���� ������� ����� �������� ��� ��������� �������
        /// </summary>
        private void _toItem_EditValueChanged(object sender, EventArgs e)
        {
            PeriodService.Period.End = (DateTime) _toItem.EditValue;
        }

        /// <summary>
        /// ��������� ��������� ��������� ���������� ��� ������
        /// ��������� �������
        /// </summary>
        private void updatePeriodEditors()
        {
            ReportPeriod period = PeriodService.Period;
            _runBtn.Caption = period.Name;

            _fromItem.BeginUpdate();
            _fromItem.EditValue = period.Begin;
            _fromItem.EndUpdate();

            _toItem.BeginUpdate();
            _toItem.EditValue = period.End;
            _toItem.EndUpdate();
        }

        #endregion

        #region --   ��������� �������� ����   --

        /// <summary>
        /// ���������� ������ �� �������� �� ���������
        /// </summary>
        private void _resetItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            ResetLayout();
            RestoreLayout();
        }

        #region --   ������ ��������   --

        /// <summary>
        /// ������������ ���� �� ������ "������ ��������"
        /// </summary>
        private void _sensorItem_CheckedChanged(object sender, ItemClickEventArgs e)
        {
            if (_sensorsItem.Checked)
            {
                _sensorsPanel.Show();
            }
            else
            {
                _sensorsPanel.Hide();
            }
        }

        /// <summary>
        /// ������������ ��� ��������� ��������� ������ ��������
        /// </summary>
        private void _sensorsPanel_VisibilityChanged(object sender, VisibilityChangedEventArgs e)
        {
            _sensorsItem.Checked = DockVisibility.Hidden != _sensorsPanel.Visibility;
        }

        #endregion

        #region --   ������ ����, ���������� ������   --

        /// <summary>
        /// ������������ ���� �� ������ "������ ���� � �������"
        /// </summary>
        private void _historyItem_CheckedChanged(object sender, ItemClickEventArgs e)
        {
            if (_historyItem.Checked)
                _historyPanel.Show();
            else
                _historyPanel.Hide();
        }

        /// <summary>
        /// ������������ ��� ��������� ��������� ������ ����, ���������� ������
        /// </summary>
        private void _historyPanel_VisibilityChanged(object sender, VisibilityChangedEventArgs e)
        {
            _historyItem.Checked = DockVisibility.Hidden != _historyPanel.Visibility;
        }

        #endregion

        #region --   ������ ������� �������   --

        /// <summary>
        /// ������������ ���� �� ������ "������ ������� �������"
        /// </summary>
        private void _logItem_CheckedChanged(object sender, ItemClickEventArgs e)
        {
            if (_logItem.Checked)
                _logPanel.Show();
            else
                _logPanel.Hide();
        }

        /// <summary>
        /// ������������ ��� ��������� ��������� ������ ������� �������
        /// </summary>
        private void _logPanel_VisibilityChanged(object sender, VisibilityChangedEventArgs e)
        {
            _logItem.Checked = DockVisibility.Hidden != _logPanel.Visibility;
        }

        #endregion

        #endregion

        private void VehicleTree_VehicleSelected(ReportVehicle rv)
        {
            _sensorsDetails.ShowSensorsInfo(rv);
            _eventsLogControl.SetFilter(rv.Mobitel.Id);
        }

        private void _xtraTabs_SelectedPageChanged(object sender, TabPageChangedEventArgs e)
        {
            if (_map != null)
            {
                _map.ClearZones();
                _map.ClearMarkers();
                _map.ClearTracks();
            }

            _trackPointsItem.Enabled = false;
            _speedModeItem.Enabled = false;
            _zoneCreateItem.Enabled = false;

            if (e.Page.Tag is IMap)
            {
                IMap map = (IMap) e.Page.Tag;
                map.State = _map.State;
                _map = map;

                if (map is GoogleMapControl)
                {
                    _trackPointsItem.Enabled = true;
                    _speedModeItem.Enabled = true;
                    _zoneCreateItem.Enabled = true;
                }
                else
                {
                    if (Form_Utils.IsLoaded("TrackControl.Reports.TrackAnalyzer.Select2Points"))
                        Application.OpenForms["Select2Points"].Close();
                }

                MapChanged();
            }
            else
            {
                if (Form_Utils.IsLoaded("TrackControl.Reports.TrackAnalyzer.Select2Points"))
                    Application.OpenForms["Select2Points"].Close();
            }
        }

        /// <summary>
        /// ������������ ���� �� ������ "��������� ������ �� ������"
        /// </summary>
        private void _runBtn_ItemClick(object sender, ItemClickEventArgs e)
        {
            _app.ReportsMode.Reports.DisableRunButtons();
            _eventsLogControl.ClearEventLog();
            _eventsLogControl.Enabled = false;
            _runBtn.Enabled = false;
            
            StartClicked();

            if( _app.ReportsMode.ResultFlag )
            {
                _app.ReportsMode.Reports.EnableRunButtons();
                _eventsLogControl.Enabled = true;
                _runBtn.Enabled = true;
            }
        }
        
        private void _allTracksBtn_CheckedChanged(object sender, ItemClickEventArgs e)
        {
            DrawTrackModeChanged(_allTracksBtn.Checked);
        }

        private void _autoZoomBtn_CheckedChanged(object sender, ItemClickEventArgs e)
        {
            AutoZoomModeChanged(_autoZoomBtn.Checked);
        }

        private void loadingEnded()
        {
            _statusLbBefore.Caption = Resources.Done;
            _statusLbAfter.Caption = String.Empty;
            _progress.Visibility = BarItemVisibility.Never;
            btnCancelProcessing.Visibility = BarItemVisibility.Never;
            _eventsLogControl.Enabled = true;
            _runBtn.Enabled = true;
        }

        private void _trackPointsItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (!_trackPoints.Checked)
            {
                _map.ResetTools();
                _trackPoints.Checked = true;
            }
        }
        
        private void _trackPoints_CheckedChanged(object sender, ItemClickEventArgs e)
        {
            if (_trackPoints.Checked)
            {
                _map.ClearZones();
                if (TrackAnalizerClicked != null) 
                    TrackAnalizerClicked();
            }
            else
            {
                if (TrackAnalizerStop != null) 
                    TrackAnalizerStop();
            }
        }

        private void _speedModeItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (SpeedAnalizerClicked != null)
            {
                SpeedAnalizerClicked();
                _map.ResetTools();
            }
        }

        private void _zoneCreateItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (_trackPoints.Checked)
            {
                _trackPoints.Checked = false;
            }
            if (Select2PointsClicked != null)
            {
                _map.ResetTools();
                Select2PointsClicked();
            }
        }

        private void bbiCreateRoute_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (RouteCreaterClicked != null)
            {
                RouteCreaterClicked();
                _map.ResetTools();
            }
        }

        public void DrawCratedZone(IGeoPoint[] g_points)
        {
            _map.ClearZones();
            _map.PointsFromTrack.Clear();
            ZonesGroup zg = new ZonesGroup("Temp", "Groupe creating from track");
            Zone z = Zone.CreateNew(zg);

            PointLatLng[] pll = new PointLatLng[g_points.Length];
            for (int i = 0; i < g_points.Length; i++)
            {
                pll[i] = new PointLatLng(g_points[i].LatLng.Lat, g_points[i].LatLng.Lng);
                _map.PointsFromTrack.Add(pll[i]);
            }
            z.Points = pll;
            _map.AddZone(z);
        }

        public void ShowZoneEditor()
        {
            _map.ClearAll();
            if (ShowZoneEditorEvent != null)
                ShowZoneEditorEvent();
        }

        private void init()
        {
            barStaticItem1.Caption = String.Format("{0} ", Resources.Reports);
            _runBtn.Caption = String.Format(" {0}", Resources.Load);
            _runBtn.Hint = Resources.Reports_LoadHint;
            _todayItem.Caption = Resources.Period_forToday;
            _yesterdayItem.Caption = Resources.Period_forYesterday;
            _weekItem.Caption = Resources.Period_forThisWeek;
            _last7daysItem.Caption = Resources.Period_forLast7days;
            _monthItem.Caption = Resources.Period_forThisMonth;
            _last30daysItem.Caption = Resources.Period_forLast30days;
            _fromItem.Caption = Resources.Period_From;
            _fromItem.Hint = Resources.Period_FromHint;
            _toItem.Caption = String.Format(" {0}", Resources.Period_To);
            _toItem.Hint = Resources.Period_ToHint;
            _allTracksBtn.Caption = Resources.Reports_AllTracks;
            _allTracksBtn.Hint = Resources.Reports_AllTracksHint;
            _autoZoomBtn.Caption = Resources.AutoZoom;
            _autoZoomBtn.Hint = Resources.Reports_AutoZoomHint;
            _trackPoints.Caption = Resources.TrackAnalyis;
            _trackPoints.Hint = Resources.TrackAnalyis;
            _viewBtn.Caption = String.Format("{0} ", Resources.View);
            _resetItem.Caption = String.Format("{0}...", Resources.DefaultView);
            _sensorsItem.Caption = Resources.Reports_Sensors;
            _historyItem.Caption = Resources.Reports_History;
            _logItem.Caption = Resources.Reports_EventLog;
            _servicesBtn.Caption = Resources.Tools;
            _trackPointsItem.Caption = Resources.TrackAnalyis;
            _speedModeItem.Caption = Resources.SpeedMode;
            _zoneCreateItem.Caption = Resources.ZoneCreateFromTrack;
            _statusLbBefore.Caption = Resources.Done;
            _reportsPanel.Text = Resources.Reports;
            _vehiclePanel.Text = Resources.Vehicles;
            _zonesPanel.TabText = Resources.Zones;
            _zonesPanel.Text = Resources.CheckZones;
            _logPanel.TabText = Resources.Log;
            _logPanel.Text = Resources.EventLog;
            _sensorsPanel.TabText = Resources.Sensors;
            _sensorsPanel.Text = Resources.Sensors;
            _historyPanel.TabText = Resources.History;
            _historyPanel.Text = Resources.DaysWithData;
            _googleTab.Text = Resources.MapWeb;
            _gisTab.Text = Resources.MapUkrGIS;
            _graphTab.Text = Resources.Graphs;
            _noticePanel.Text = Resources.Notifications;
            _noticePanel.TabText = Resources.Notifications;
            biAddressFinder.Caption = Resources.AddressSearch;
            barButtonInterv.Caption = Resources.Intervals;
            barButtonInterval.Hint = Resources.IntervalHint;
            bbiSaveLayout.Caption = Resources.SaveLayout;
            btnCancelProcessing.Visibility = BarItemVisibility.Never;
            btnCancelProcessing.Caption = Resources.cancelBtnCaption;
            btnCancelProcessing.Hint = Resources.canselBtnHint;
            bbiCreateRoute.Caption = Resources.bbiCreateRoute;
        } // init

        private void biAddressFinder_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (RunAddressFinder != null)
            {
                _map.ResetTools();
                RunAddressFinder();
            }
        }

        private void barButtonInterv_ItemClick_1( object sender, ItemClickEventArgs e )
        {
            formInterval.ShowDialog();
        }

        private void bbiSaveLayout_ItemClick(object sender, ItemClickEventArgs e)
        {
            SaveLayout();
        }

        private void btnCancelProcessing_ItemClick( object sender, ItemClickEventArgs e )
        {
            workProcessing = workProcessing ? false : true;
        }

        private void _graphTab_TabIndexChanged( object sender, EventArgs e )
        {
            // to do
        }

        private void noticeLog_Load(object sender, EventArgs e)
        {
            // to do
        }
    }
}