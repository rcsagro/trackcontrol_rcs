using System;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars;
using DevExpress.XtraGrid.Views.Layout;
using System.Diagnostics;
using TrackControl.Properties;

namespace TrackControl
{
    public partial class DriversDictionary : XtraUserControl
    {
        public DriversDictionary()
        {
            InitializeComponent();
            init();
        }

        void init()
        {
            _firstNameCarouselColumn.Caption = Resources.NameText;
            _lastNameCarouselColumn.Caption = Resources.Surname;
            _dayOfBirthCarouselColumn.Caption = Resources.DayOfBirthShort;
            _photoCarouselColumn.Caption = Resources.Photo;
            _licenseCarouselColumn.Caption = Resources.DriverLicense;
            _categoriesCarouselColumn.Caption = Resources.Categories;
            _identifierCarouselColumn.Caption = Resources.Identificator;
            Group1.CustomizationFormText = Resources.DriverLicense;
            Group1.Text = Resources.DriverLicense;
            _layoutBtn.Caption = String.Format("{0}...", Resources.View);
            _viewTableBtn.Caption = Resources.View_TableMode;
            _viewCarouselBtn.Caption = Resources.View_CarouselMode;
            _viewCardBtn.Caption = Resources.View_CardMode;
            _exportBtn.Caption = String.Format("{0}...", Resources.Export);
            _toPdfBtn.Caption = Resources.Export_toPDF;
            _toHtmlBtn.Caption = Resources.Export_toMHT;
            _toXlsBtn.Caption = Resources.Export_toExcel;
            _printBtn.Caption = Resources.Print;
            _firstNameGridColumn.Caption = Resources.NameText;
            _lastNameGridColumn.Caption = Resources.Surname;
            _dayOfBirthGridColumn.Caption = Resources.DayOfBirth;
            _licenseBand.Caption = Resources.DriverLicense;
            _licenseGridColumn.Caption = Resources.DriverLicense;
            _categoriesGridColumn.Caption = Resources.Driver_OpenCategories;
            _identifier.Caption = Resources.Identificator;
            _photoBand.Caption = Resources.Photo;
            _photoGridColumn.Caption = Resources.Photo;

        }

        void viewTableBtn_ItemClick(object sender, ItemClickEventArgs e)
        {
            _grid.MainView = _bandedView;
        }

        void viewCarouselBtn_ItemClick(object sender, ItemClickEventArgs e)
        {
            _layoutView.OptionsView.ViewMode = LayoutViewMode.Carousel;
            _grid.MainView = _layoutView;
        }

        void viewCardBtn_ItemClick(object sender, ItemClickEventArgs e)
        {
            _layoutView.OptionsView.ViewMode = LayoutViewMode.MultiRow;
            _grid.MainView = _layoutView;
        }

        void this_Load(object sender, EventArgs e)
        {
            _grid.DataSource = AppModel.Instance.VehiclesModel.Drivers;
            _grid.Focus();
        }

        void toPdfBtn_ItemClick(object sender, ItemClickEventArgs e)
        {
            using (SaveFileDialog sfd = new SaveFileDialog())
            {
                sfd.Filter = "PDF Document (*.pdf)|*.pdf";
                sfd.FileName = Resources.Drivers;
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        _grid.ExportToPdf(sfd.FileName);
                        if (DialogResult.OK ==
                            XtraMessageBox.Show(Resources.Dictionary_AskOpenFile, "", MessageBoxButtons.OKCancel))
                        {
                            Process.Start(sfd.FileName);
                        }
                    }
                    catch
                    {
                        string message = String.Format("{0}{1}{2}",
                            Resources.Dictionary_FileNotSaved,
                            Environment.NewLine,
                            sfd.FileName);
                        string caption = Resources.Err_ExportFailed;
                        XtraMessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        void toHtmlBtn_ItemClick(object sender, ItemClickEventArgs e)
        {
            using (SaveFileDialog sfd = new SaveFileDialog())
            {
                sfd.Filter = String.Format("{0} (*.mht)|*.mht", Resources.Export_toMHT);
                sfd.FileName = Resources.Drivers;
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        _grid.ExportToMht(sfd.FileName);
                        if (DialogResult.OK ==
                            XtraMessageBox.Show(Resources.Dictionary_AskOpenFile, "", MessageBoxButtons.OKCancel))
                        {
                            Process.Start(sfd.FileName);
                        }
                    }
                    catch
                    {
                        string message = String.Format("{0}{1}{2}",
                            Resources.Dictionary_FileNotSaved,
                            Environment.NewLine,
                            sfd.FileName);
                        string caption = Resources.Err_ExportFailed;
                        XtraMessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        void toXlsBtn_ItemClick(object sender, ItemClickEventArgs e)
        {
            using (SaveFileDialog sfd = new SaveFileDialog())
            {
                sfd.Filter = "Excel document (*.xls)|*.xls";
                sfd.FileName = Resources.Drivers;
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        _grid.ExportToXls(sfd.FileName);
                        if (DialogResult.OK ==
                            XtraMessageBox.Show(Resources.Dictionary_AskOpenFile, "", MessageBoxButtons.OKCancel))
                        {
                            Process.Start(sfd.FileName);
                        }
                    }
                    catch
                    {
                        string message = String.Format("{0}{1}{2}",
                            Resources.Dictionary_FileNotSaved,
                            Environment.NewLine,
                            sfd.FileName);
                        string caption = Resources.Err_ExportFailed;
                        XtraMessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        void printBtn_ItemClick(object sender, ItemClickEventArgs e)
        {
            _grid.Print();
        }
    }
}
