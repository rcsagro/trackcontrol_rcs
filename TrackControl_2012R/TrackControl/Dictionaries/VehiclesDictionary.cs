using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraPrinting;
using DevExpress.XtraTreeList;
using DevExpress.Utils;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

using TrackControl.Vehicles;

using TrackControl.General;
using TrackControl.Properties;

namespace TrackControl
{
    public partial class VehiclesDictionary : XtraUserControl
    {
        private bool _isLoaded;
        private VehiclesModel _model;

        public VehiclesDictionary()
        {
            _model = AppModel.Instance.VehiclesModel;
            InitializeComponent();
            init();
        }

        private void init()
        {
            _exportBtn.Caption = String.Format("{0}...", Resources.Export);
            _toPdfBtn.Caption = Resources.Export_toPDF;
            _toMhtBtn.Caption = Resources.Export_toMHT;
            _toXlsBtn.Caption = Resources.Export_toExcel;
            _printBtn.Caption = Resources.Print;
            _regNumberCol.Caption = Resources.Vehicles_GroupAndNumber;
            _modelCol.Caption = Resources.Vehicles_Model;
            _loginCol.Caption = Resources.Login;
            _simNumberCol.Caption = Resources.SimNumber;
            _driverFullNameCol.Caption = Resources.Driver;
            _driverDayOfBirth.Caption = Resources.DriverBirth;
            _openCategoriesCol.Caption = Resources.Categories;
            _openCategoriesCol.ToolTip = Resources.Categories;
            categoriesColumn1.Caption = Resources.Category;
            categoriesColumn2.Caption = Resources.Category2;
            categoriesColumn1.ToolTip = Resources.CategoryTip;
            categoriesColumn2.ToolTip = Resources.CategoryTip2;
            columnFuelMotor.Caption = Resources.FuelMotor;
            columnFuelWay.Caption = Resources.FuelWays;
            colIdentifier.Caption = Resources.VehicleIdentifier;
            colComment.Caption = Resources.CommentVehicle;
            colComment.ToolTip = Resources.CommentVehicleTip;
        }

        private void this_Load(object sender, EventArgs e)
        {
            _tree.DataSource = new object();
            _tree.ExpandAll();
        }

        private void tree_CustomDrawNodeCell(object sender, CustomDrawNodeCellEventArgs e)
        {
            object obj = _tree.GetDataRecordByNode(e.Node);

            if (obj is IVehicle)
            {
                IVehicle vehicle = (IVehicle) obj;

                if (vehicle.Group.FuelWayKmGrp == vehicle.FuelWays)
                {
                    if (e.Column == columnFuelWay)
                        e.Appearance.ForeColor = Color.LightPink;
                }

                if (vehicle.Group.FuelMotoHrGrp == vehicle.FuelMotor)
                {
                    if (e.Column == columnFuelMotor)
                        e.Appearance.ForeColor = Color.LightSalmon;
                }

                return;
            }

            e.Appearance.Font = new Font(AppearanceObject.DefaultFont, FontStyle.Bold);

            if (e.Node != _tree.FocusedNode)
                e.Appearance.BackColor = AppearanceObject.ControlAppearance.BorderColor;
        }

        private void tree_CustomDrawNodeImages(object sender, CustomDrawNodeImagesEventArgs e)
        {
            Rectangle rect = e.SelectRect;
            rect.X += (rect.Width - 16)/2;
            rect.Y += (rect.Height - 16)/2;
            rect.Width = 16;
            rect.Height = 16;

            object obj = _tree.GetDataRecordByNode(e.Node);
            if (obj is IVehicle)
            {
                IVehicle vehicle = (IVehicle) obj;
                e.Graphics.DrawImage(vehicle.Style.Icon, rect);
            }
            else if (obj is VehiclesGroup)
            {
                VehiclesGroup group = (VehiclesGroup) obj;
                e.Graphics.DrawImage(group.Style.Icon, rect);
            }

            e.Handled = true;
        }

        private void tree_VirtualTreeGetCellValue(object sender, VirtualTreeGetCellValueInfo e)
        {
            if (e.Node is VehiclesGroup)
            {
                VehiclesGroup group = (VehiclesGroup) e.Node;
                if (e.Column == _regNumberCol)
                    e.CellData = group.Name;
                else if (e.Column == columnFuelWay)
                {
                    e.CellData = group.FuelWayKmGrp;
                }
                else if (e.Column == columnFuelMotor)
                {
                    e.CellData = group.FuelMotoHrGrp;
                }
            }
            else if (e.Node is IVehicle)
            {
                IVehicle vehicle = (IVehicle) e.Node;
                if (e.Column == _regNumberCol)
                    e.CellData = vehicle.RegNumber;
                else if (e.Column == _modelCol)
                    e.CellData = String.Format("{0} {1}", vehicle.CarMaker, vehicle.CarModel);
                else if (e.Column == _loginCol)
                    e.CellData = vehicle.Mobitel.Login;
                else if (e.Column == _simNumberCol)
                    e.CellData = vehicle.Mobitel.SimNumber;
                else if (e.Column == columnFuelWay)
                {
                    e.CellData = vehicle.FuelWays;
                }
                else if (e.Column == columnFuelMotor)
                {
                    e.CellData = vehicle.FuelMotor;
                }
                else if (e.Column == colIdentifier)
                {
                    e.CellData = vehicle.Identifier;
                }
                else if( e.Column == categoriesColumn1)
                {
                    e.CellData = vehicle.Category != null ? vehicle.Category.Name : "";
                }
                else if( e.Column == categoriesColumn2)
                {
                    e.CellData = vehicle.Category2 != null ? vehicle.Category2.Name : "";
                }
                else if( e.Column == categoriesColumn3 )
                {
                    e.CellData = vehicle.Category3 != null ? vehicle.Category3.Name : "";
                }
                else if( e.Column == categoriesColumn4 )
                {
                    e.CellData = vehicle.Category4 != null ? vehicle.Category4.Name : "";
                }
                else if( e.Column == colComment )
                {
                    e.CellData = vehicle.VehicleComment != null ? vehicle.VehicleComment : "";
                }

                Driver driver = vehicle.Driver;

                if (null != driver)
                {
                    if (e.Column == _driverFullNameCol)
                        e.CellData = String.Format("{0} {1}", driver.LastName, driver.FirstName);
                    else if (e.Column == _driverDayOfBirth && driver.DayOfBirth.HasValue)
                        e.CellData = driver.DayOfBirth.Value.ToShortDateString();
                    else if (e.Column == _openCategoriesCol)
                        e.CellData = driver.Categories;
                }
            }
        }

        private void tree_VirtualTreeGetChildNodes(object sender, VirtualTreeGetChildNodesInfo e)
        {
            if (!_isLoaded)
            {
                e.Children = new VehiclesGroup[] {_model.Root};
                _isLoaded = true;
            }
            else
            {
                VehiclesGroup group = e.Node as VehiclesGroup;

                if (null != group)
                {
                    IList children = new List<object>();

                    foreach (VehiclesGroup vg in group.OwnGroups)
                        if (vg.AllItems.Count > 0)
                            children.Add(vg);

                    foreach (IVehicle vehicle in group.OwnItems)
                        children.Add(vehicle);

                    e.Children = children;
                }
                else // ���� ���� �� �������� �������, ������ �� ����� �������� �����
                {
                    e.Children = new object[] {};
                }
            }
        }

        private void tree_AfterCheckNode(object sender, NodeEventArgs e)
        {
            TreeViewService.SetCheckedChildNodes(e.Node);
            TreeViewService.SetCheckedParentNodes(e.Node);
            _tree.FocusedNode = e.Node;
        }

        private void tree_BeforeCheckNode(object sender, CheckNodeEventArgs e)
        {
            TreeViewService.FixNodeState(e);
        }

        private void toPdfBtn_ItemClick(object sender, ItemClickEventArgs e)
        {
            using (SaveFileDialog sfd = new SaveFileDialog())
            {
                sfd.Filter = "PDF Document (*.pdf)|*.pdf";
                sfd.FileName = Resources.Transport;
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        _tree.ExportToPdf(sfd.FileName);
                        if (DialogResult.OK ==
                            XtraMessageBox.Show(Resources.Dictionary_AskOpenFile, "", MessageBoxButtons.OKCancel))
                        {
                            Process.Start(sfd.FileName);
                        }
                    }
                    catch
                    {
                        string message = String.Format("{0}{1}{2}",
                                                       Resources.Dictionary_FileNotSaved,
                                                       Environment.NewLine,
                                                       sfd.FileName);
                        string caption = Resources.Err_ExportFailed;
                        XtraMessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void toMhtBtn_ItemClick(object sender, ItemClickEventArgs e)
        {
            using (SaveFileDialog sfd = new SaveFileDialog())
            {
                sfd.Filter = String.Format("{0} (*.mht)|*.mht", Resources.Export_toMHT);
                sfd.FileName = Resources.Transport;
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        MhtExportOptions options = new MhtExportOptions("UTF-8");
                        options.ExportMode = HtmlExportMode.SingleFile;
                        options.Title = Resources.Vehicles;
                        _tree.ExportToMht(sfd.FileName, options);
                        if (DialogResult.OK ==
                            XtraMessageBox.Show(Resources.Dictionary_AskOpenFile, "", MessageBoxButtons.OKCancel))
                        {
                            Process.Start(sfd.FileName);
                        }
                    }
                    catch
                    {
                        string message = String.Format("{0}{1}{2}",
                                                       Resources.Dictionary_FileNotSaved,
                                                       Environment.NewLine,
                                                       sfd.FileName);
                        string caption = Resources.Err_ExportFailed;
                        XtraMessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void toXlsBtn_ItemClick(object sender, ItemClickEventArgs e)
        {
            using (SaveFileDialog sfd = new SaveFileDialog())
            {
                sfd.Filter = "Excel Document (*.xls)|*.xls";
                sfd.FileName = Resources.Transport;
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    try
                    {
                        XlsExportOptions options = new XlsExportOptions();
                        options.TextExportMode = TextExportMode.Text;
                        _tree.ExportToXls(sfd.FileName, options);
                        if (DialogResult.OK ==
                            XtraMessageBox.Show(Resources.Dictionary_AskOpenFile, "", MessageBoxButtons.OKCancel))
                        {
                            Process.Start(sfd.FileName);
                        }
                    }
                    catch
                    {
                        string message = String.Format("{0}{1}{2}",
                                                       Resources.Dictionary_FileNotSaved,
                                                       Environment.NewLine,
                                                       sfd.FileName);
                        string caption = Resources.Err_ExportFailed;
                        XtraMessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void printBtn_ItemClick(object sender, ItemClickEventArgs e)
        {
            _tree.Print();
        }
    }
}
