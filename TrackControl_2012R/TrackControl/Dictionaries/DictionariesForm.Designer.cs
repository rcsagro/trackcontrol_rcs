namespace TrackControl
{
  partial class DictionariesForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DictionariesForm));
      this._splitter = new DevExpress.XtraEditors.SplitContainerControl();
      this._navBar = new DevExpress.XtraNavBar.NavBarControl();
      this._chaptersGroup = new DevExpress.XtraNavBar.NavBarGroup();
      this._vehiclesItem = new DevExpress.XtraNavBar.NavBarItem();
      this._driverItem = new DevExpress.XtraNavBar.NavBarItem();
      ((System.ComponentModel.ISupportInitialize)(this._splitter)).BeginInit();
      this._splitter.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this._navBar)).BeginInit();
      this.SuspendLayout();
      // 
      // _splitter
      // 
      this._splitter.Dock = System.Windows.Forms.DockStyle.Fill;
      this._splitter.Location = new System.Drawing.Point(0, 0);
      this._splitter.Margin = new System.Windows.Forms.Padding(0);
      this._splitter.Name = "_splitter";
      this._splitter.Panel1.Controls.Add(this._navBar);
      this._splitter.Panel1.Text = "Panel1";
      this._splitter.Panel2.Text = "Panel2";
      this._splitter.Size = new System.Drawing.Size(936, 667);
      this._splitter.SplitterPosition = 162;
      this._splitter.TabIndex = 0;
      this._splitter.Text = "splitContainerControl1";
      // 
      // _navBar
      // 
      this._navBar.ActiveGroup = this._chaptersGroup;
      this._navBar.AllowSelectedLink = true;
      this._navBar.ContentButtonHint = null;
      this._navBar.Dock = System.Windows.Forms.DockStyle.Fill;
      this._navBar.DragDropFlags = DevExpress.XtraNavBar.NavBarDragDrop.None;
      this._navBar.Groups.AddRange(new DevExpress.XtraNavBar.NavBarGroup[] {
            this._chaptersGroup});
      this._navBar.HideGroupCaptions = true;
      this._navBar.Items.AddRange(new DevExpress.XtraNavBar.NavBarItem[] {
            this._driverItem,
            this._vehiclesItem});
      this._navBar.Location = new System.Drawing.Point(0, 0);
      this._navBar.Margin = new System.Windows.Forms.Padding(0);
      this._navBar.Name = "_navBar";
      this._navBar.OptionsNavPane.ExpandedWidth = 179;
      this._navBar.OptionsNavPane.ShowExpandButton = false;
      this._navBar.OptionsNavPane.ShowOverflowPanel = false;
      this._navBar.Size = new System.Drawing.Size(162, 667);
      this._navBar.TabIndex = 0;
      this._navBar.View = new DevExpress.XtraNavBar.ViewInfo.SkinNavigationPaneViewInfoRegistrator();
      this._navBar.SelectedLinkChanged += new DevExpress.XtraNavBar.ViewInfo.NavBarSelectedLinkChangedEventHandler(this.navBar_SelectedLinkChanged);
      // 
      // _chaptersGroup
      // 
      this._chaptersGroup.Caption = "�������� ������";
      this._chaptersGroup.Expanded = true;
      this._chaptersGroup.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this._vehiclesItem),
            new DevExpress.XtraNavBar.NavBarItemLink(this._driverItem)});
      this._chaptersGroup.Name = "_chaptersGroup";
      this._chaptersGroup.NavigationPaneVisible = false;
      this._chaptersGroup.SelectedLinkIndex = 0;
      // 
      // _vehiclesItem
      // 
      this._vehiclesItem.Caption = "���������";
      this._vehiclesItem.Name = "_vehiclesItem";
      this._vehiclesItem.SmallImage = TrackControl.General.Shared.Car;
      // 
      // _driverItem
      // 
      this._driverItem.Caption = "��������";
      this._driverItem.Name = "_driverItem";
      this._driverItem.SmallImage = TrackControl.General.Shared.User;
      // 
      // DictionariesForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(936, 667);
      this.Controls.Add(this._splitter);
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.MinimizeBox = false;
      this.Name = "DictionariesForm";
      this.ShowInTaskbar = false;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "�����������";
      this.Load += new System.EventHandler(this.this_Load);
      ((System.ComponentModel.ISupportInitialize)(this._splitter)).EndInit();
      this._splitter.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this._navBar)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private DevExpress.XtraEditors.SplitContainerControl _splitter;
    private DevExpress.XtraNavBar.NavBarControl _navBar;
    private DevExpress.XtraNavBar.NavBarItem _driverItem;
    private DevExpress.XtraNavBar.NavBarGroup _chaptersGroup;
    private DevExpress.XtraNavBar.NavBarItem _vehiclesItem;
  }
}