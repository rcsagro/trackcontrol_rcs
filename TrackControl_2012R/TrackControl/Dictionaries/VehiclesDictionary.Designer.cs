namespace TrackControl
{
  partial class VehiclesDictionary
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VehiclesDictionary));
            this._barManager = new DevExpress.XtraBars.BarManager();
            this._tools = new DevExpress.XtraBars.Bar();
            this._exportBtn = new DevExpress.XtraBars.BarButtonItem();
            this._exportPopup = new DevExpress.XtraBars.PopupMenu();
            this._toPdfBtn = new DevExpress.XtraBars.BarButtonItem();
            this._toMhtBtn = new DevExpress.XtraBars.BarButtonItem();
            this._toXlsBtn = new DevExpress.XtraBars.BarButtonItem();
            this._printBtn = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this._tree = new DevExpress.XtraTreeList.TreeList();
            this._regNumberCol = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this._modelCol = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this._loginCol = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this._simNumberCol = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this._driverFullNameCol = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this._driverDayOfBirth = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.categoriesColumn1 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.categoriesColumn2 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.categoriesColumn3 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.categoriesColumn4 = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this._openCategoriesCol = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colIdentifier = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnFuelWay = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.columnFuelMotor = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colComment = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this._images = new DevExpress.Utils.ImageCollection();
            ((System.ComponentModel.ISupportInitialize)(this._barManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._exportPopup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._tree)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._images)).BeginInit();
            this.SuspendLayout();
            // 
            // _barManager
            // 
            this._barManager.AllowCustomization = false;
            this._barManager.AllowQuickCustomization = false;
            this._barManager.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this._tools});
            this._barManager.DockControls.Add(this.barDockControlTop);
            this._barManager.DockControls.Add(this.barDockControlBottom);
            this._barManager.DockControls.Add(this.barDockControlLeft);
            this._barManager.DockControls.Add(this.barDockControlRight);
            this._barManager.Form = this;
            this._barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this._exportBtn,
            this._toPdfBtn,
            this._toMhtBtn,
            this._toXlsBtn,
            this._printBtn});
            this._barManager.MaxItemId = 5;
            // 
            // _tools
            // 
            this._tools.BarName = "Tools";
            this._tools.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Top;
            this._tools.DockCol = 0;
            this._tools.DockRow = 0;
            this._tools.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this._tools.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this._exportBtn),
            new DevExpress.XtraBars.LinkPersistInfo(this._printBtn)});
            this._tools.OptionsBar.AllowQuickCustomization = false;
            this._tools.OptionsBar.DisableClose = true;
            this._tools.OptionsBar.DisableCustomization = true;
            this._tools.OptionsBar.UseWholeRow = true;
            this._tools.Text = "Tools";
            // 
            // _exportBtn
            // 
            this._exportBtn.ActAsDropDown = true;
            this._exportBtn.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this._exportBtn.Caption = "�������...";
            this._exportBtn.DropDownControl = this._exportPopup;
            this._exportBtn.Glyph = ((System.Drawing.Image)(resources.GetObject("_exportBtn.Glyph")));
            this._exportBtn.Id = 0;
            this._exportBtn.Name = "_exportBtn";
            this._exportBtn.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // _exportPopup
            // 
            this._exportPopup.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this._toPdfBtn),
            new DevExpress.XtraBars.LinkPersistInfo(this._toMhtBtn),
            new DevExpress.XtraBars.LinkPersistInfo(this._toXlsBtn)});
            this._exportPopup.Manager = this._barManager;
            this._exportPopup.Name = "_exportPopup";
            // 
            // _toPdfBtn
            // 
            this._toPdfBtn.Caption = "��� PDF ��������";
            this._toPdfBtn.Glyph = ((System.Drawing.Image)(resources.GetObject("_toPdfBtn.Glyph")));
            this._toPdfBtn.Id = 1;
            this._toPdfBtn.Name = "_toPdfBtn";
            this._toPdfBtn.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.toPdfBtn_ItemClick);
            // 
            // _toMhtBtn
            // 
            this._toMhtBtn.Caption = "���-�������� ���������";
            this._toMhtBtn.Glyph = ((System.Drawing.Image)(resources.GetObject("_toMhtBtn.Glyph")));
            this._toMhtBtn.Id = 2;
            this._toMhtBtn.Name = "_toMhtBtn";
            this._toMhtBtn.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.toMhtBtn_ItemClick);
            // 
            // _toXlsBtn
            // 
            this._toXlsBtn.Caption = "�������� Excel";
            this._toXlsBtn.Glyph = ((System.Drawing.Image)(resources.GetObject("_toXlsBtn.Glyph")));
            this._toXlsBtn.Id = 3;
            this._toXlsBtn.Name = "_toXlsBtn";
            this._toXlsBtn.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.toXlsBtn_ItemClick);
            // 
            // _printBtn
            // 
            this._printBtn.Caption = "������";
            this._printBtn.Glyph = ((System.Drawing.Image)(resources.GetObject("_printBtn.Glyph")));
            this._printBtn.Id = 4;
            this._printBtn.Name = "_printBtn";
            this._printBtn.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this._printBtn.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.printBtn_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(811, 31);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 678);
            this.barDockControlBottom.Size = new System.Drawing.Size(811, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 31);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 647);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(811, 31);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 647);
            // 
            // _tree
            // 
            this._tree.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this._tree.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._tree.Appearance.HeaderPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._tree.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._tree.ColumnPanelRowHeight = 40;
            this._tree.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this._regNumberCol,
            this._modelCol,
            this._loginCol,
            this._simNumberCol,
            this._driverFullNameCol,
            this._driverDayOfBirth,
            this.categoriesColumn1,
            this.categoriesColumn2,
            this.categoriesColumn3,
            this.categoriesColumn4,
            this._openCategoriesCol,
            this.colIdentifier,
            this.columnFuelWay,
            this.columnFuelMotor,
            this.colComment});
            this._tree.Dock = System.Windows.Forms.DockStyle.Fill;
            this._tree.Location = new System.Drawing.Point(0, 31);
            this._tree.Margin = new System.Windows.Forms.Padding(0);
            this._tree.Name = "_tree";
            this._tree.OptionsBehavior.ImmediateEditor = false;
            this._tree.OptionsPrint.PrintReportFooter = false;
            this._tree.OptionsPrint.PrintTreeButtons = false;
            this._tree.OptionsSelection.EnableAppearanceFocusedCell = false;
            this._tree.SelectImageList = this._images;
            this._tree.Size = new System.Drawing.Size(811, 647);
            this._tree.TabIndex = 4;
            this._tree.BeforeCheckNode += new DevExpress.XtraTreeList.CheckNodeEventHandler(this.tree_BeforeCheckNode);
            this._tree.AfterCheckNode += new DevExpress.XtraTreeList.NodeEventHandler(this.tree_AfterCheckNode);
            this._tree.CustomDrawNodeCell += new DevExpress.XtraTreeList.CustomDrawNodeCellEventHandler(this.tree_CustomDrawNodeCell);
            this._tree.CustomDrawNodeImages += new DevExpress.XtraTreeList.CustomDrawNodeImagesEventHandler(this.tree_CustomDrawNodeImages);
            this._tree.VirtualTreeGetChildNodes += new DevExpress.XtraTreeList.VirtualTreeGetChildNodesEventHandler(this.tree_VirtualTreeGetChildNodes);
            this._tree.VirtualTreeGetCellValue += new DevExpress.XtraTreeList.VirtualTreeGetCellValueEventHandler(this.tree_VirtualTreeGetCellValue);
            // 
            // _regNumberCol
            // 
            this._regNumberCol.AppearanceCell.Options.UseTextOptions = true;
            this._regNumberCol.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this._regNumberCol.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._regNumberCol.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this._regNumberCol.AppearanceHeader.Options.UseTextOptions = true;
            this._regNumberCol.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._regNumberCol.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._regNumberCol.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._regNumberCol.Caption = "������, �����";
            this._regNumberCol.FieldName = "RegNumber";
            this._regNumberCol.MinWidth = 33;
            this._regNumberCol.Name = "_regNumberCol";
            this._regNumberCol.OptionsColumn.AllowMove = false;
            this._regNumberCol.OptionsColumn.AllowMoveToCustomizationForm = false;
            this._regNumberCol.OptionsColumn.ReadOnly = true;
            this._regNumberCol.OptionsColumn.ShowInCustomizationForm = false;
            this._regNumberCol.ToolTip = "������, �����";
            this._regNumberCol.Visible = true;
            this._regNumberCol.VisibleIndex = 0;
            this._regNumberCol.Width = 50;
            // 
            // _modelCol
            // 
            this._modelCol.AppearanceCell.Options.UseTextOptions = true;
            this._modelCol.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this._modelCol.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._modelCol.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this._modelCol.AppearanceHeader.Options.UseTextOptions = true;
            this._modelCol.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._modelCol.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._modelCol.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._modelCol.Caption = "�����, ������";
            this._modelCol.FieldName = "Model";
            this._modelCol.MinWidth = 16;
            this._modelCol.Name = "_modelCol";
            this._modelCol.OptionsColumn.AllowMoveToCustomizationForm = false;
            this._modelCol.OptionsColumn.ReadOnly = true;
            this._modelCol.OptionsColumn.ShowInCustomizationForm = false;
            this._modelCol.ToolTip = "�����, ������";
            this._modelCol.Visible = true;
            this._modelCol.VisibleIndex = 1;
            this._modelCol.Width = 55;
            // 
            // _loginCol
            // 
            this._loginCol.AppearanceCell.Options.UseTextOptions = true;
            this._loginCol.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this._loginCol.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._loginCol.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this._loginCol.AppearanceHeader.Options.UseTextOptions = true;
            this._loginCol.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._loginCol.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._loginCol.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._loginCol.Caption = "�����";
            this._loginCol.FieldName = "Login";
            this._loginCol.MinWidth = 16;
            this._loginCol.Name = "_loginCol";
            this._loginCol.OptionsColumn.AllowMoveToCustomizationForm = false;
            this._loginCol.OptionsColumn.ReadOnly = true;
            this._loginCol.OptionsColumn.ShowInCustomizationForm = false;
            this._loginCol.ToolTip = "�����";
            this._loginCol.Visible = true;
            this._loginCol.VisibleIndex = 2;
            this._loginCol.Width = 20;
            // 
            // _simNumberCol
            // 
            this._simNumberCol.AppearanceCell.Options.UseTextOptions = true;
            this._simNumberCol.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this._simNumberCol.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._simNumberCol.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this._simNumberCol.AppearanceHeader.Options.UseTextOptions = true;
            this._simNumberCol.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._simNumberCol.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._simNumberCol.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._simNumberCol.Caption = "����� SIM";
            this._simNumberCol.FieldName = "SimNumber";
            this._simNumberCol.MinWidth = 16;
            this._simNumberCol.Name = "_simNumberCol";
            this._simNumberCol.OptionsColumn.AllowMoveToCustomizationForm = false;
            this._simNumberCol.OptionsColumn.ReadOnly = true;
            this._simNumberCol.OptionsColumn.ShowInCustomizationForm = false;
            this._simNumberCol.ToolTip = "����� SIM";
            this._simNumberCol.Visible = true;
            this._simNumberCol.VisibleIndex = 3;
            this._simNumberCol.Width = 45;
            // 
            // _driverFullNameCol
            // 
            this._driverFullNameCol.AppearanceCell.Options.UseTextOptions = true;
            this._driverFullNameCol.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this._driverFullNameCol.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._driverFullNameCol.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this._driverFullNameCol.AppearanceHeader.Options.UseTextOptions = true;
            this._driverFullNameCol.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._driverFullNameCol.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._driverFullNameCol.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._driverFullNameCol.Caption = "��������";
            this._driverFullNameCol.FieldName = "DriverFullName";
            this._driverFullNameCol.MinWidth = 16;
            this._driverFullNameCol.Name = "_driverFullNameCol";
            this._driverFullNameCol.OptionsColumn.AllowMoveToCustomizationForm = false;
            this._driverFullNameCol.OptionsColumn.ReadOnly = true;
            this._driverFullNameCol.OptionsColumn.ShowInCustomizationForm = false;
            this._driverFullNameCol.ToolTip = "�������� ������������� ��������";
            this._driverFullNameCol.Visible = true;
            this._driverFullNameCol.VisibleIndex = 4;
            this._driverFullNameCol.Width = 65;
            // 
            // _driverDayOfBirth
            // 
            this._driverDayOfBirth.AppearanceCell.Options.UseTextOptions = true;
            this._driverDayOfBirth.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this._driverDayOfBirth.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._driverDayOfBirth.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this._driverDayOfBirth.AppearanceHeader.Options.UseTextOptions = true;
            this._driverDayOfBirth.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._driverDayOfBirth.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._driverDayOfBirth.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._driverDayOfBirth.Caption = "�. �. ��������";
            this._driverDayOfBirth.FieldName = "DriverDayOfBirth";
            this._driverDayOfBirth.MinWidth = 16;
            this._driverDayOfBirth.Name = "_driverDayOfBirth";
            this._driverDayOfBirth.OptionsColumn.AllowMoveToCustomizationForm = false;
            this._driverDayOfBirth.OptionsColumn.ReadOnly = true;
            this._driverDayOfBirth.OptionsColumn.ShowInCustomizationForm = false;
            this._driverDayOfBirth.ToolTip = "�.�. ��������";
            this._driverDayOfBirth.Visible = true;
            this._driverDayOfBirth.VisibleIndex = 5;
            this._driverDayOfBirth.Width = 45;
            // 
            // categoriesColumn1
            // 
            this.categoriesColumn1.AppearanceCell.Options.UseTextOptions = true;
            this.categoriesColumn1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.categoriesColumn1.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.categoriesColumn1.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.categoriesColumn1.AppearanceHeader.Options.UseTextOptions = true;
            this.categoriesColumn1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.categoriesColumn1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.categoriesColumn1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.categoriesColumn1.Caption = "���������";
            this.categoriesColumn1.FieldName = "VehicleCategories";
            this.categoriesColumn1.MinWidth = 16;
            this.categoriesColumn1.Name = "categoriesColumn1";
            this.categoriesColumn1.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.categoriesColumn1.OptionsColumn.ReadOnly = true;
            this.categoriesColumn1.OptionsColumn.ShowInCustomizationForm = false;
            this.categoriesColumn1.ToolTip = "��������� ����������";
            this.categoriesColumn1.Visible = true;
            this.categoriesColumn1.VisibleIndex = 6;
            this.categoriesColumn1.Width = 45;
            // 
            // categoriesColumn2
            // 
            this.categoriesColumn2.AppearanceCell.Options.UseTextOptions = true;
            this.categoriesColumn2.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.categoriesColumn2.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.categoriesColumn2.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.categoriesColumn2.AppearanceHeader.Options.UseTextOptions = true;
            this.categoriesColumn2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.categoriesColumn2.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.categoriesColumn2.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.categoriesColumn2.Caption = "���������2";
            this.categoriesColumn2.FieldName = "VehicleCategories2";
            this.categoriesColumn2.MinWidth = 16;
            this.categoriesColumn2.Name = "categoriesColumn2";
            this.categoriesColumn2.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.categoriesColumn2.OptionsColumn.ReadOnly = true;
            this.categoriesColumn2.OptionsColumn.ShowInCustomizationForm = false;
            this.categoriesColumn2.ToolTip = "��������� ����������2";
            this.categoriesColumn2.Visible = true;
            this.categoriesColumn2.VisibleIndex = 7;
            this.categoriesColumn2.Width = 46;
            // 
            // categoriesColumn3
            // 
            this.categoriesColumn3.AppearanceCell.Options.UseTextOptions = true;
            this.categoriesColumn3.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.categoriesColumn3.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.categoriesColumn3.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.categoriesColumn3.AppearanceHeader.Options.UseTextOptions = true;
            this.categoriesColumn3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.categoriesColumn3.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.categoriesColumn3.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.categoriesColumn3.Caption = "���������3";
            this.categoriesColumn3.FieldName = "VehicleCategories3";
            this.categoriesColumn3.MinWidth = 16;
            this.categoriesColumn3.Name = "categoriesColumn3";
            this.categoriesColumn3.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.categoriesColumn3.OptionsColumn.ReadOnly = true;
            this.categoriesColumn3.OptionsColumn.ShowInCustomizationForm = false;
            this.categoriesColumn3.ToolTip = "��������� ���������� 3";
            this.categoriesColumn3.Visible = true;
            this.categoriesColumn3.VisibleIndex = 8;
            this.categoriesColumn3.Width = 46;
            // 
            // categoriesColumn4
            // 
            this.categoriesColumn4.AppearanceCell.Options.UseTextOptions = true;
            this.categoriesColumn4.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.categoriesColumn4.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.categoriesColumn4.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.categoriesColumn4.AppearanceHeader.Options.UseTextOptions = true;
            this.categoriesColumn4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.categoriesColumn4.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.categoriesColumn4.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.categoriesColumn4.Caption = "���������4";
            this.categoriesColumn4.FieldName = "VehicleCategories4";
            this.categoriesColumn4.MinWidth = 16;
            this.categoriesColumn4.Name = "categoriesColumn4";
            this.categoriesColumn4.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.categoriesColumn4.OptionsColumn.ReadOnly = true;
            this.categoriesColumn4.OptionsColumn.ShowInCustomizationForm = false;
            this.categoriesColumn4.ToolTip = "��������� ���������� 4";
            this.categoriesColumn4.Visible = true;
            this.categoriesColumn4.VisibleIndex = 9;
            this.categoriesColumn4.Width = 46;
            // 
            // _openCategoriesCol
            // 
            this._openCategoriesCol.AppearanceCell.Options.UseTextOptions = true;
            this._openCategoriesCol.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this._openCategoriesCol.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._openCategoriesCol.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this._openCategoriesCol.AppearanceHeader.Options.UseTextOptions = true;
            this._openCategoriesCol.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._openCategoriesCol.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._openCategoriesCol.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._openCategoriesCol.Caption = "��������� ���� ��������";
            this._openCategoriesCol.FieldName = "OpenCategories";
            this._openCategoriesCol.MinWidth = 16;
            this._openCategoriesCol.Name = "_openCategoriesCol";
            this._openCategoriesCol.OptionsColumn.AllowMoveToCustomizationForm = false;
            this._openCategoriesCol.OptionsColumn.ReadOnly = true;
            this._openCategoriesCol.OptionsColumn.ShowInCustomizationForm = false;
            this._openCategoriesCol.ToolTip = "��������� ���� ��������";
            this._openCategoriesCol.Visible = true;
            this._openCategoriesCol.VisibleIndex = 10;
            this._openCategoriesCol.Width = 46;
            // 
            // colIdentifier
            // 
            this.colIdentifier.AppearanceCell.Options.UseTextOptions = true;
            this.colIdentifier.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colIdentifier.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colIdentifier.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.colIdentifier.AppearanceHeader.Options.UseTextOptions = true;
            this.colIdentifier.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIdentifier.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colIdentifier.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colIdentifier.Caption = "�������������";
            this.colIdentifier.FieldName = "�������������";
            this.colIdentifier.MinWidth = 16;
            this.colIdentifier.Name = "colIdentifier";
            this.colIdentifier.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.colIdentifier.OptionsColumn.ReadOnly = true;
            this.colIdentifier.OptionsColumn.ShowInCustomizationForm = false;
            this.colIdentifier.ToolTip = "�������������";
            this.colIdentifier.Visible = true;
            this.colIdentifier.VisibleIndex = 11;
            this.colIdentifier.Width = 46;
            // 
            // columnFuelWay
            // 
            this.columnFuelWay.AppearanceCell.Options.UseTextOptions = true;
            this.columnFuelWay.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.columnFuelWay.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnFuelWay.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.columnFuelWay.AppearanceHeader.Options.UseTextOptions = true;
            this.columnFuelWay.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnFuelWay.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnFuelWay.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.columnFuelWay.Caption = "������ ������� �/100 ��";
            this.columnFuelWay.FieldName = "FuelWayLiter";
            this.columnFuelWay.Format.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.columnFuelWay.MinWidth = 16;
            this.columnFuelWay.Name = "columnFuelWay";
            this.columnFuelWay.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.columnFuelWay.OptionsColumn.ReadOnly = true;
            this.columnFuelWay.OptionsColumn.ShowInCustomizationForm = false;
            this.columnFuelWay.ToolTip = "������ ������� �/100 ��";
            this.columnFuelWay.Visible = true;
            this.columnFuelWay.VisibleIndex = 12;
            this.columnFuelWay.Width = 45;
            // 
            // columnFuelMotor
            // 
            this.columnFuelMotor.AppearanceCell.Options.UseTextOptions = true;
            this.columnFuelMotor.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.columnFuelMotor.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnFuelMotor.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.columnFuelMotor.AppearanceHeader.Options.UseTextOptions = true;
            this.columnFuelMotor.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnFuelMotor.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnFuelMotor.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.columnFuelMotor.Caption = "������ ������� �/���";
            this.columnFuelMotor.FieldName = "FuelMotorLiter";
            this.columnFuelMotor.Format.FormatString = "N1";
            this.columnFuelMotor.Format.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.columnFuelMotor.MinWidth = 16;
            this.columnFuelMotor.Name = "columnFuelMotor";
            this.columnFuelMotor.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.columnFuelMotor.OptionsColumn.ReadOnly = true;
            this.columnFuelMotor.OptionsColumn.ShowInCustomizationForm = false;
            this.columnFuelMotor.ToolTip = "������ ������� �/���";
            this.columnFuelMotor.Visible = true;
            this.columnFuelMotor.VisibleIndex = 13;
            this.columnFuelMotor.Width = 45;
            // 
            // colComment
            // 
            this.colComment.Caption = "�����������";
            this.colComment.FieldName = "VehicleComment";
            this.colComment.MinWidth = 16;
            this.colComment.Name = "colComment";
            this.colComment.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.colComment.OptionsColumn.ReadOnly = true;
            this.colComment.OptionsColumn.ShowInCustomizationForm = false;
            this.colComment.ToolTip = "����������� ������";
            this.colComment.Visible = true;
            this.colComment.VisibleIndex = 14;
            this.colComment.Width = 55;
            // 
            // _images
            // 
            this._images.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("_images.ImageStream")));
            // 
            // VehiclesDictionary
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._tree);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "VehiclesDictionary";
            this.Size = new System.Drawing.Size(811, 678);
            this.Load += new System.EventHandler(this.this_Load);
            ((System.ComponentModel.ISupportInitialize)(this._barManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._exportPopup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._tree)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._images)).EndInit();
            this.ResumeLayout(false);

    }

    #endregion

    private DevExpress.XtraBars.BarManager _barManager;
    private DevExpress.XtraBars.Bar _tools;
    private DevExpress.XtraBars.BarDockControl barDockControlTop;
    private DevExpress.XtraBars.BarDockControl barDockControlBottom;
    private DevExpress.XtraBars.BarDockControl barDockControlLeft;
    private DevExpress.XtraBars.BarDockControl barDockControlRight;
    private DevExpress.XtraTreeList.TreeList _tree;
    private DevExpress.Utils.ImageCollection _images;
    private DevExpress.XtraTreeList.Columns.TreeListColumn _regNumberCol;
    private DevExpress.XtraTreeList.Columns.TreeListColumn _modelCol;
    private DevExpress.XtraTreeList.Columns.TreeListColumn _loginCol;
    private DevExpress.XtraTreeList.Columns.TreeListColumn _simNumberCol;
    private DevExpress.XtraTreeList.Columns.TreeListColumn _driverFullNameCol;
    private DevExpress.XtraTreeList.Columns.TreeListColumn _driverDayOfBirth;
    private DevExpress.XtraTreeList.Columns.TreeListColumn _openCategoriesCol;
    private DevExpress.XtraBars.BarButtonItem _exportBtn;
    private DevExpress.XtraBars.PopupMenu _exportPopup;
    private DevExpress.XtraBars.BarButtonItem _toPdfBtn;
    private DevExpress.XtraBars.BarButtonItem _toMhtBtn;
    private DevExpress.XtraBars.BarButtonItem _toXlsBtn;
    private DevExpress.XtraBars.BarButtonItem _printBtn;
    private DevExpress.XtraTreeList.Columns.TreeListColumn columnFuelWay;
    private DevExpress.XtraTreeList.Columns.TreeListColumn columnFuelMotor;
    private DevExpress.XtraTreeList.Columns.TreeListColumn colIdentifier;
    private DevExpress.XtraTreeList.Columns.TreeListColumn categoriesColumn1;
    private DevExpress.XtraTreeList.Columns.TreeListColumn categoriesColumn2;
    private DevExpress.XtraTreeList.Columns.TreeListColumn categoriesColumn3;
    private DevExpress.XtraTreeList.Columns.TreeListColumn categoriesColumn4;
    private DevExpress.XtraTreeList.Columns.TreeListColumn colComment;
  }
}
