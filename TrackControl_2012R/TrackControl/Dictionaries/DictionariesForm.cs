using System;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraNavBar;
using DevExpress.XtraNavBar.ViewInfo;
using TrackControl.Properties;

namespace TrackControl
{
    public partial class DictionariesForm : XtraForm
    {
        public DictionariesForm()
        {
            InitializeComponent();
            init();
        }

        void this_Load(object sender, EventArgs e)
        {
            adjust(_navBar.ActiveGroup.SelectedLink.Item);
        }

        void navBar_SelectedLinkChanged(object sender, NavBarSelectedLinkChangedEventArgs e)
        {
            adjust(e.Link.Item);
        }

        void init()
        {
            _chaptersGroup.Caption = Resources.Dictionary_SelectChapter;
            _vehiclesItem.Caption = Resources.Transport;
            _driverItem.Caption = Resources.Drivers;
            Text = Resources.Dictionaries;
        }

        void adjust(NavBarItem item)
        {
            _splitter.Panel2.Controls.Clear();

            Control view = null;
            if (item == _driverItem)
                view = new DriversDictionary();
            else if (item == _vehiclesItem)
                view = new VehiclesDictionary();

            if (null != view)
            {
                view.Dock = DockStyle.Fill;
                _splitter.Panel2.Controls.Add(view);
            }
        }
    }
}