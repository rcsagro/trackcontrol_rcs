﻿using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("TrackControl")]
[assembly: AssemblyDescription("Контроль транспорта")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("RCS Ltd")]
[assembly: AssemblyProduct("TrackControl")]
[assembly: AssemblyCopyright("Copyright © RCS 2008-2023")]
[assembly: AssemblyTrademark("AVS")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("b356df88-625b-4f16-b66d-9e5c4843d941")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyVersion("1.48.15.26")]
[assembly: AssemblyFileVersion("1.48.15.26")]
