﻿using System.Collections.Generic;
using System.Windows.Forms;
using TrackControl.General;
using TrackControl.General.Users;
using TrackControl.Properties;
using TrackControl.Reports;
using TrackControl.Zones;

namespace TrackControl.AccessObjects
{
    public class UserAccessZones : IUserAccessObject 
    {
        UserControl _dataViewer;
        UserRole _accessRole;
        List<IZone> _unvisibleItems;
        IZonesManager _zonesManager;

        public UserAccessZones()
        {
            _dataViewer = new ZonesTreeView(AppModel.Instance.ZonesManager);
            _dataViewer.Dock = DockStyle.Fill;
            _unvisibleItems = new List<IZone>();
            _zonesManager = AppModel.Instance.ZonesManager;

        }
        #region IUserAccessObject Members

        public UserControl DataViewer
        {
            get { return _dataViewer; }
        }

        public string ObjectName
        {
            get { return Resources.CheckZones; }
        }

        public int ObjectType
        {
            get { return (int)UserAccessObjectsTypes.CheckZones; }
        }

        public Control ParentDataViewer
        {
            set { _dataViewer.Parent = value ; }
        }

        public UserRole AccessRole
        {
            get { return _accessRole; }
            set
            {
                if (_accessRole != value)
                {
                    _accessRole = value;
                    GetUnvisibleRoleItems();
                    (_dataViewer as ZonesTreeView).UncheckUnvisible(_unvisibleItems);
                }
            }
        }

        void GetUnvisibleRoleItems()
        {
            _unvisibleItems.Clear();
            List<int> id_zones = UserBaseProvider.GetUnvisibleRoleItems(ObjectType, _accessRole.Id);
                foreach (int id_zone in id_zones)
                {
                    IZone zone = _zonesManager.GetById (id_zone);
                    if (zone != null)
                    {
                        if (!_unvisibleItems.Contains(zone)) _unvisibleItems.Add(zone);

                    }
                }
        }

        public void SaveUnvisibleRoleItems()
        {
            if (_accessRole == null) return;
            IList<IEntity> unvisible = (_dataViewer as ZonesTreeView).UnChecked;
            UserBaseProvider.SaveUnvisibleRoleItems(ObjectType, _accessRole.Id, unvisible);
        }
        #endregion
    }
}
