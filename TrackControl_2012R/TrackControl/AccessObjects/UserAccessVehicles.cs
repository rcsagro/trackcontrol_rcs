﻿using System.Collections.Generic;
using System.Windows.Forms;
using TrackControl.General;
using TrackControl.General.Users;
using TrackControl.Properties;
using TrackControl.Reports;
using TrackControl.Vehicles;

namespace TrackControl.AccessObjects
{
    public class UserAccessVehicles : IUserAccessObject
    {
        private UserControl _dataViewer;
        private UserRole _accessRole;
        private List<IVehicle> _unvisibleItems;
        private VehiclesModel _vModel;

        public UserAccessVehicles()
        {
            _dataViewer = new VehicleTreeView(AppModel.Instance.VehiclesModel);
            _dataViewer.Dock = DockStyle.Fill;
            _vModel = AppModel.Instance.VehiclesModel;
            _unvisibleItems = new List<IVehicle>();
        }

        #region IUserAccessObject Members

        public UserControl DataViewer
        {
            get { return _dataViewer; }
        }

        public string ObjectName
        {
            get { return Resources.Vehicles; }
        }

        public int ObjectType
        {
            get { return (int) UserAccessObjectsTypes.Vehicles; }
        }

        public Control ParentDataViewer
        {
            set { _dataViewer.Parent = value; }
        }

        public UserRole AccessRole
        {
            get { return _accessRole; }
            set
            {
                if (_accessRole != value)
                {
                    _accessRole = value;
                    GetUnvisibleRoleItems();
                    (_dataViewer as VehicleTreeView).UncheckUnvisible(_unvisibleItems);
                }
            }
        }

        public void GetUnvisibleRoleItems()
        {
            _unvisibleItems.Clear();
            List<int> id_vehs = UserBaseProvider.GetUnvisibleRoleItems(ObjectType, _accessRole.Id);

            foreach (Vehicle vehicle in _vModel.Vehicles)
            {
                foreach (int id_veh in id_vehs)
                {
                    if( vehicle.Id < 0 )
                    {
                        if( -vehicle.Mobitel.Id == id_veh )
                        {
                            if( !_unvisibleItems.Contains( vehicle ) )
                                _unvisibleItems.Add( vehicle );

                            continue;
                        }
                    }

                    if (vehicle.Id == id_veh)
                    {
                        if (!_unvisibleItems.Contains(vehicle)) 
                        _unvisibleItems.Add(vehicle);
                    }
                }
            }
        }

        public void SaveUnvisibleRoleItems()
        {
            if (_accessRole == null)
                return;

            IList<IEntity> unvisible = (_dataViewer as VehicleTreeView).UnChecked;
            UserBaseProvider.SaveUnvisibleRoleItems(ObjectType, _accessRole.Id, unvisible);
        }

        #endregion
    }
}
