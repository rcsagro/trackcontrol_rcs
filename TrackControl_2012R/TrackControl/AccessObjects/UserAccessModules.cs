﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using TrackControl.General;
using TrackControl.General.DatabaseDriver;
using TrackControl.General.Users;
using TrackControl.Properties;
using TrackControl.Setting;


namespace TrackControl.AccessObjects
{
    public class UserAccessModules : IUserAccessObject 
    {
        UserControl _dataViewer;
        UserRole _accessRole;
        List<TCModule> _unvisibleItems;
        BindingList<TCModule> _modules;

        public UserAccessModules()
        {
            _modules = ModulesController.GetModulesList();
            _dataViewer = new ModulesList(_modules);
            _dataViewer.Dock = DockStyle.Fill;
            _unvisibleItems = new List<TCModule>();
        }

        #region IUserAccessObject Members

        public UserControl DataViewer
        {
            get { return _dataViewer; }
        }

        public string ObjectName
        {
            get { return Resources.MainMenuModules; }
        }

        public int ObjectType
        {
            get { return (int)UserAccessObjectsTypes.ModulesList; }
        }

        public Control ParentDataViewer
        {
            set { _dataViewer.Parent = value; }
        }

        public UserRole AccessRole
        {
            get { return _accessRole; }
            set
            {
                if (_accessRole != value)
                {
                    _accessRole = value;
                    GetUnvisibleRoleItems();
                    (_dataViewer as ModulesList).UncheckUnvisible(_unvisibleItems);
                }
            }
        }

        void GetUnvisibleRoleItems()
        {
            _unvisibleItems.Clear();
            List<int> idModules = UserBaseProvider.GetUnvisibleRoleItems(ObjectType, _accessRole.Id);
            foreach (int idModule in idModules)
            {
                TCModule module = GetById(idModule);
                if (module != null)
                {
                    if (!_unvisibleItems.Contains(module)) _unvisibleItems.Add(module);

                }
            }
        }

        public void SaveUnvisibleRoleItems()
        {
            if (_accessRole == null) return;
            IList<IEntity> unvisible = (_dataViewer as ModulesList).UnChecked;
            UserBaseProvider.SaveUnvisibleRoleItems(ObjectType, _accessRole.Id, unvisible);
        }
        #endregion



        public TCModule GetById(int id)
        {
            return _modules.ToList().Find(
              delegate(TCModule m) { return m.Id == id; });
        }
    }
}
