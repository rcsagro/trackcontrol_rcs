﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using TrackControl.General.Users;
using TrackControl.Reports;
using TrackControl.General;
using TrackControl.Properties;
using TrackControl.Setting;
using BaseReports.ReportListSetting;

namespace TrackControl.AccessObjects
{
    public class UserAccessReports : IUserAccessObject 
    {
        
        UserControl _dataViewer;
        UserRole _accessRole;
        List<ReportSetting> _unvisibleItems;

        public UserAccessReports()
        {
            _dataViewer = new ReportsList();
            _dataViewer.Dock = DockStyle.Fill;
            _unvisibleItems = new List<ReportSetting>();
        }
        
        
        #region IUserAccessObject Members

        public UserControl DataViewer
        {
            get { return _dataViewer; }
        }

        public string ObjectName
        {
            get { return Resources.Reports; }
        }

        public int ObjectType
        {
            get { return (int)UserAccessObjectsTypes.ReportsList; }
        }

        public Control ParentDataViewer
        {
            set { _dataViewer.Parent = value; }
        }

        public UserRole AccessRole
        {
            get { return _accessRole; }
            set
            {
                if (_accessRole != value)
                {
                    _accessRole = value;
                    GetUnvisibleRoleItems();
                    (_dataViewer as ReportsList).UncheckUnvisible(_unvisibleItems);
                }
            }
        }

        void GetUnvisibleRoleItems()
        {
            _unvisibleItems.Clear();
            List<int> id_settings = UserBaseProvider.GetUnvisibleRoleItems(ObjectType, _accessRole.Id);
            ReportListCtrl _rl_ctrl = new ReportListCtrl();
            foreach (int id_setting in id_settings)
            {
                    ReportSetting settings = _rl_ctrl.GetById(id_setting);
                    if (settings != null)
                    {
                        if (!_unvisibleItems.Contains(settings)) _unvisibleItems.Add(settings);

                    }
            }
        }

        public void SaveUnvisibleRoleItems()
        {
            if (_accessRole == null) return;
            IList<IEntity> unvisible = (_dataViewer as ReportsList).UnChecked;
            UserBaseProvider.SaveUnvisibleRoleItems(ObjectType, _accessRole.Id, unvisible);
        }
        #endregion
    }
}
