﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using TrackControl.General;

namespace TrackControl.AccessObjects
{
    public interface  IUserAccessObject
    {
        /// <summary>
        /// Элемент для просмотра и редактирования данных
        /// </summary>
        UserControl DataViewer { get; }

        /// <summary>
        /// Название объекта доступа
        /// </summary>
        string ObjectName { get; }

        /// <summary>
        /// тип объекта доступа
        /// </summary>
        int ObjectType { get; }

        /// <summary>
        /// родительский объект для DataViewer
        /// </summary>
        Control ParentDataViewer { set; }

        /// <summary>
        /// роль пользователя для доступа к данным
        /// </summary>
        UserRole AccessRole { get; set; }

        /// <summary>
        /// Сохранение в базе всех объектов, которых нужно скрыть для роли
        /// </summary>
        void SaveUnvisibleRoleItems();
    }
}
