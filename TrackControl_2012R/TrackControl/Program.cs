﻿using System;
using System.Configuration;
using System.Threading;
using System.Windows.Forms;
using TrackControl.General;
using TrackControl.General.DatabaseDriver;
using TrackControl.Modals;
using TrackControl.MySqlDal;
using TrackControl.Properties;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Security.Cryptography;
using System.ComponentModel;
using System.Globalization;
using System.Reflection;
using DevExpress.XtraEditors;
using DevExpress.LookAndFeel;
using TrackControl.General.Services;
using DevExpress.XtraEditors.Controls;

namespace TrackControl
{
    public static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.ThreadException += Application_ThreadException;

            DevExpress.UserSkins.BonusSkins.Register();
            DevExpress.Skins.SkinManager.EnableFormSkins();

            // проверки на все случаи
            Login login = null;
            string cs = String.Empty; // изначально строка подключения пустая
            try
            {
                // сдесь проверить наличие строки типа БД, и если ее нет сгенерировать новую с подключением к MYSQL по умолчанию
                Crypto.CheckingStrTypeBD();
                //==========================================================================
                cs = Crypto.GetDecryptConnectionString(ConfigurationManager.ConnectionStrings["CS"].ConnectionString);

                if (!Crypto.isConnectString(cs))
                {
                    XtraMessageBox.Show(Resources.NonValidateConnParam, Resources.ConnectParameter, 
                        MessageBoxButtons.OK, MessageBoxIcon.Error);

                    Login login1 = new Login(false);
                    login1.SetStrConnect( cs );
                    if (login1.ShowDialog() != DialogResult.OK)
                    {
                        return;
                    }

                    login1.Close();

                    // перезапускаем сами себя
                    string path = System.Reflection.Assembly.GetExecutingAssembly().Location;
                    System.Diagnostics.Process.Start(path);
                    System.Diagnostics.Process.GetCurrentProcess().Kill();

                    return; 
                } // if
            } // try
            catch (ConfigurationErrorsException ex)
            {
                if (XtraMessageBox.Show(ex.Message + Resources.NonConfigFileValidate, 
                    Resources.ConfigFile, MessageBoxButtons.YesNo, 
                    MessageBoxIcon.Error) == DialogResult.Yes)
                {
                    if (!Crypto.WritingNewConfig())
                    {
                        XtraMessageBox.Show(Resources.ConfigFileNonRestore, Resources.ConfigFile,
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }

                    // перезапускаем сами себя
                    string path = System.Reflection.Assembly.GetExecutingAssembly().Location;
                    System.Diagnostics.Process.Start(path);
                    System.Diagnostics.Process.GetCurrentProcess().Kill();

                    return; 
                } // if
               
                XtraMessageBox.Show(Resources.ConfigFileIsNotRestore, 
                    Resources.ConfigFile, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            } // catch
            catch (NullReferenceException)
            {
                if (XtraMessageBox.Show(Resources.ParametersConnectMission, Resources.ParametersConnect, 
                    MessageBoxButtons.YesNo, MessageBoxIcon.Error) == DialogResult.Yes)
                {
                    Login login2 = new Login(false);
                    login2.SetStrConnect( cs );
                    if (login2.ShowDialog() != DialogResult.OK)
                    {
                        return;
                    }

                    login2.Close();

                    // перезапускаем сами себя
                    string path = System.Reflection.Assembly.GetExecutingAssembly().Location;
                    System.Diagnostics.Process.Start(path);
                    System.Diagnostics.Process.GetCurrentProcess().Kill();

                    return;
                } // if

                XtraMessageBox.Show(Resources.ParametersConnectNotRestore, 
                    Resources.ParametersConnect, MessageBoxButtons.OK, MessageBoxIcon.Information);

                return;
            } // catch

            //DbCommon dbCommon = new DbCommon(cs);
            DriverDb dbCommon = new DriverDb(cs);
            AppModel.Instance.DbCommon = dbCommon;

            if (!dbCommon.CheckConnection())
            {
                if( XtraMessageBox.Show( Resources.ConnectToDataBaseNotPresent,
                                           Resources.DataBaseMessage, MessageBoxButtons.YesNo,
                                           MessageBoxIcon.Error ) == DialogResult.Yes )
                {
                    Login login3 = new Login( false );
                    login3.SetStrConnect( cs );
                    if( login3.ShowDialog() != DialogResult.OK )
                    {
                        return;
                    }

                    login3.Close();

                    // перезапускаем сами себя
                    string path = System.Reflection.Assembly.GetExecutingAssembly().Location;
                    System.Diagnostics.Process.Start( path );
                    System.Diagnostics.Process.GetCurrentProcess().Kill();

                    return;
                } // if

                return;
            } // if

                // Проверяем возможность подключения и наличия необхидимых таблиц и полей в базе данных
                DbCommon dcommon = new DbCommon(cs);
                TestNewDataBaseObjects testObj = new TestNewDataBaseObjects(dcommon);
                if (!testObj.TestIsExistNewObjects())
                    //if (!dbCommon.CheckConnection())
                {
                    // using (NoConnection noConnectionForm = new NoConnection())
                    // {
                    //      Application.Run(noConnectionForm); // подключиться к базе данных не возможно, уходим отсюда
                    // }

                    if (XtraMessageBox.Show(Resources.NotConnectToDataBase,
                                            Resources.DataBaseMessage, MessageBoxButtons.YesNo,
                                            MessageBoxIcon.Error) == DialogResult.Yes)
                    {
                        Login login3 = new Login(false);
                        login3.SetStrConnect(cs);
                        if (login3.ShowDialog() != DialogResult.OK)
                        {
                            return;
                        }

                        login3.Close();

                        // перезапускаем сами себя
                        string path = System.Reflection.Assembly.GetExecutingAssembly().Location;
                        System.Diagnostics.Process.Start(path);
                        System.Diagnostics.Process.GetCurrentProcess().Kill();

                        return;
                    } // if

                    return;
                } // if

            login = new Login(true); // отобразим диалоговую форму
            login.SetStrConnect( cs );
            if (login.ShowDialog() != DialogResult.OK)
            {
                return;
            }
            else
            {
                UserBaseCurrent.Instance.Server = dbCommon.ServerName;
                UserBaseCurrent.Instance.Database = dbCommon.DbName;
            }

            EventTracker.Helper.InstallSounds(Globals.APP_DATA);
            Application.Run(new MainForm());
            EventTracker.Helper.UninstallSounds(Globals.APP_DATA);
        } // Main

        static void Application_ThreadException(object sender, ThreadExceptionEventArgs e)
        {
            AppController.ShowException(e.Exception);
            Application.ExitThread();
        }
    }
}