using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.LookAndFeel;
using TrackControl.General;
using TrackControl.General.Services;
using TrackControl.Properties;
using DevExpress.XtraEditors.Controls;
using TrackControl.Modals;
using TrackControl.MySqlDal;
using System.Configuration;

namespace TrackControl
{
    public partial class Login : DevExpress.XtraEditors.XtraForm
    {
        int OriginalSizeWithoutPanel = 294; /* ������ ����� � ������ ���� */
        bool flagPressed;
        bool flagPressedEditDB;
        bool isConnectType = true;
        BindingList<UserBaseProvider.DataBaseName> listDataBase;
        private int ServerTypeUse = Crypto.MYSQL;
        private int ConnectUserType = Crypto.AuthenWindows;
        private bool glWindowsAuth;
        private string currentDomainName = "";

        public Login(bool isConnect)
        {
            string skinName = SkinService.Get();
            UserLookAndFeel.Default.SetSkinStyle(skinName);
            InitializeComponent();
            setLanguage();
            Localization();
            checkButtonAdding.Text = Resources.ButtonParams;
            panelControlServerDB.Visible = false;
            Height = OriginalSizeWithoutPanel;
            flagPressed = false;
            flagPressedEditDB = false;
            textEditServerName.Text = "";
            textEditDB.Text = "";
            textEditUserName.Text = "";
            DialogResult = DialogResult.No;

            SizeChanged +=new EventHandler(Login_SizeChanged);

            ComboTypeBD.Closed +=new ClosedEventHandler(ComboTypeBD_Closed);
            ComboTypeBD.Properties.Items.Add( "MY SQL" );
            ComboTypeBD.Properties.Items.Add( "MS SQL" );
            ComboTypeBD.SelectedIndex = Crypto.ServerUse;
            
            comboTypeConnect.Closed += new ClosedEventHandler(ComboTypeConnect_Closed);
            comboTypeConnect.Enabled = true;
            comboTypeConnect.Properties.Items.Add(Resources.WindowsTypeConnect);
            comboTypeConnect.Properties.Items.Add(Resources.SqlTypeConnect);
            comboTypeConnect.SelectedIndex = 0;
            
            if (ComboTypeBD.SelectedIndex == Crypto.MYSQL)
            {
                comboTypeConnect.Enabled = false;
            }
            
            this.isConnectType = isConnect;

            if ( isConnect )
            {
                leUsers.Properties.DataSource = UserBaseProvider.GetList();
                listDataBase = UserBaseProvider.GetDataBaseList();
                
                for ( int i = 0 ; i < listDataBase.Count ; i++ )
                {
                    UserBaseProvider.DataBaseName udB;
                    udB = listDataBase[i];
                    textEditDB.Properties.Items.Add(udB.NameDB);
                }
            }
            else
            {
                ShowSetParamStr( true );
            }

            tePassword.TextChanged += TePasswordOnTextChanged;
            checkButtonAdding.Enabled = false;
            textWinLog.Enabled = false;
            sbEnter.Enabled = false;
            FormBorderStyle = FormBorderStyle.FixedDialog;
        }

        private void TePasswordOnTextChanged(object sender, EventArgs eventArgs)
        {
            if( tePassword.Text.Length == 0 )
            {
                if (comboBoxAuth.SelectedItem.Equals(Resources.TrackControl))
                {
                    dxErrP.SetError(tePassword, Resources.Modal_Auth_AccessDenied);
                }
                return;
            }

            if (comboBoxAuth.SelectedItem.Equals(Resources.TrackControl))
            {
                AuthorizationSpace();
            }

            if (!comboBoxAuth.SelectedItem.Equals(Resources.TrackControl))
            {
                dxErrP.ClearErrors();
            }
        }

        private void AuthorizationSpace()
        {
            try
            {
                UserBaseCurrent ubc = UserBaseCurrent.Instance;

                if (leUsers.EditValue == null)
                {
                    if (ubc.SetDefault(tePassword.Text))
                    {
                        checkButtonAdding.Enabled = true;
                        sbEnter.Enabled = true;
                        dxErrP.ClearErrors();
                    }
                    else
                    {
                        Refuse();
                    }
                }
                else
                {
                    UserBase ub = new UserBase();
                    if (ub.Validate((int) leUsers.EditValue, tePassword.Text))
                    {
                        ubc.SetUserBase(ub);
                        checkButtonAdding.Enabled = true;
                        sbEnter.Enabled = true;
                        dxErrP.ClearErrors();
                    }
                    else
                    {
                        Refuse();
                    }
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, Resources.Avtorization, MessageBoxButtons.OK);
            }
        }

        private void Login_SizeChanged(object sender, EventArgs e)
        {
            if (panelControlServerDB.Visible)
            {

                panelControlServerDB.Width = Width - 4;
                panelControlServerDB.Height = Height - 2;
            }
        }

        private void ComboTypeConnect_Closed(object sender, ClosedEventArgs e)
        {
            ComboBoxEdit combo = (ComboBoxEdit) sender;
            ConnectUserType = combo.SelectedIndex;

            if (ConnectUserType == Crypto.AuthenWindows)
            {
                textEditUserName.Enabled = false;
                textEditPassword.Enabled = false;
                checkEditPass.Enabled = false;
            }
            else if (ConnectUserType == Crypto.AuthenSqlServer)
            {
                textEditUserName.Enabled = true;
                textEditPassword.Enabled = true;
                checkEditPass.Enabled = true;
            }
        } // ComboTypeConnect_Closed

        private void ComboTypeBD_Closed(object sender, ClosedEventArgs e)
        {
            ComboBoxEdit combo = (ComboBoxEdit) sender;
            ServerTypeUse = combo.SelectedIndex;

            if (ServerTypeUse == Crypto.MYSQL)
            {
                comboTypeConnect.Enabled = false;
                textEditUserName.Enabled = true;
                textEditPassword.Enabled = true;
                checkEditPass.Enabled = true;
                return;
            }
            else if (ServerTypeUse == Crypto.MSSQL)
            {
                comboTypeConnect.Enabled = true;
            }

            ConnectUserType = comboTypeConnect.SelectedIndex;
            if( ConnectUserType == Crypto.AuthenWindows )
            {
                textEditUserName.Enabled = false;
                textEditPassword.Enabled = false;
                checkEditPass.Enabled = false;
            }
            else if( ConnectUserType == Crypto.AuthenSqlServer )
            {
                textEditUserName.Enabled = true;
                textEditPassword.Enabled = true;
                checkEditPass.Enabled = true;
            }
        }

        public void ShowSetParamStr(bool sFlag)
        {
            checkButtonAdding.Checked = sFlag;
            sbEnter.Enabled = false;
            panelControlServerDB.Visible = true;
            panelControlServerDB.Focus();
            textEditServerName.Select();
            textEditServerName.Focus();
        } // ShowSetParamStr

        private void ParsingMySqlString(string cS)
        {
            int i, j;
            string serv = String.Empty;
            if( cS.Contains( "server=" ) )
            {
                i = cS.IndexOf( "server=" );
                i = i + "server=".Length;
                j = cS.IndexOf( ";", i );
                serv = cS.Substring( i, j - i );
            }

            string user = String.Empty;
            if( cS.Contains( "user id=" ) )
            {
                i = cS.IndexOf( "user id=" );
                i = i + "user id=".Length;
                j = cS.IndexOf( ";", i );
                user = cS.Substring( i, j - i );
            }

            string passwd = String.Empty;
            if( cS.Contains( "password=" ) )
            {
                i = cS.IndexOf( "password=" );
                i = i + "password=".Length;
                j = cS.IndexOf( ";", i );
                passwd = cS.Substring( i, j - i );
            }

            string dataB = String.Empty;
            if( cS.Contains( "database=" ) )
            {
                i = cS.IndexOf( "database=" );
                i = i + "database=".Length;
                j = cS.Length;
                dataB = cS.Substring( i, j - i );
            }

            textEditServerName.Text = serv;
            textEditDB.Text = dataB;
            textEditUserName.Text = user;
            textEditPassword.Text = passwd;
        }


        public void SetStrConnect( string cS )
        {
            if (Crypto.MYSQL == Crypto.ServerUse)
            {
                ParsingMySqlString(cS);
            }
            else if (Crypto.MSSQL == Crypto.ServerUse)
            {
                ParsingMsSqlServerString(cS);
            }
        }

        private void ParsingMsSqlServerString(string cS)
        {
            if (cS.Contains("trusted_connection=yes"))
            {
                ConnectUserType = Crypto.AuthenWindows;
                comboTypeConnect.SelectedIndex = Crypto.AuthenWindows;
                textEditUserName.Enabled = false;
                textEditPassword.Enabled = false;
                checkEditPass.Enabled = false;

                int i, j;
                string serv = String.Empty;
                if (cS.Contains("Server="))
                {
                    i = cS.IndexOf("Server=");
                    i = i + "Server=".Length;
                    j = cS.IndexOf(";", i);
                    serv = cS.Substring(i, j - i);
                }

                string dataB = String.Empty;
                if (cS.Contains("database="))
                {
                    i = cS.IndexOf("database=");
                    i = i + "database=".Length;
                    j = cS.IndexOf(";", i);
                    dataB = cS.Substring(i, j - i);
                }

                textEditServerName.Text = serv;
                textEditDB.Text = dataB;
            } // if
            else
            {
                ConnectUserType = Crypto.AuthenSqlServer;
                comboTypeConnect.SelectedIndex = Crypto.AuthenSqlServer;
                textEditUserName.Enabled = true;
                textEditPassword.Enabled = true;
                checkEditPass.Enabled = true;

                int i, j;
                string serv = String.Empty;
                if( cS.Contains( "Server=" ) )
                {
                    i = cS.IndexOf( "Server=" );
                    i = i + "Server=".Length;
                    j = cS.IndexOf( ";", i );
                    if(j > 0)
                        serv = cS.Substring( i, j - i );
                }

                string dataB = String.Empty;
                if( cS.Contains( "database=" ) )
                {
                    i = cS.IndexOf( "database=" );
                    i = i + "database=".Length;
                    j = cS.IndexOf( ";", i );
                    if(j > 0)
                        dataB = cS.Substring( i, j - i );
                }

                string passwd = String.Empty;
                if( cS.Contains( "password=" ) )
                {
                    i = cS.IndexOf( "password=" );
                    i = i + "password=".Length;
                    j = cS.IndexOf( ";", i );
                    if (j > 0)
                        passwd = cS.Substring( i, j - i );
                }

                string user = String.Empty;
                if( cS.Contains( "user id=" ) )
                {
                    i = cS.IndexOf( "user id=" );
                    i = i + "user id=".Length;
                    j = cS.IndexOf( ";", i );
                    if(j > 0)
                        user = cS.Substring( i, j - i );
                }

                textEditServerName.Text = serv;
                textEditDB.Text = dataB;
                textEditUserName.Text = user;
                textEditPassword.Text = passwd;
            } // else
        }

        void Localization()
        {
            this.Text = StaticMethods.GetTotalMessageCaption();
            lbCopyright.Text = AssemblyCopyright;
            sbEnter.Text = Resources.Modal_Auth_ComeIn;
            labelUser.Text = Resources.User; 
            lbPassword.Text = Resources.Password;
            leUsers.Properties.NullText = Resources.SelectUser;
            checkButtonAdding.Text = Resources.AddingParams;
            checkButtonAdding.ToolTip = Resources.SetParamString;
            labelControlHelp.Text = Resources.AddingHelp;
            labelTypeDB.Text = Resources.TypeServerLabel + ":";
            labelControlServer.Text = Resources.NameServer + ":";
            labelControlDB.Text = Resources.NameDataBase + ":";
            labelControlUserName.Text = Resources.NameUser + ":";
            labelPassword.Text = Resources.NamePasswd + ":";
            textEditServerName.ToolTip = Resources.ServerNameToolTip;
            textEditDB.ToolTip = Resources.SelectDataBaseToolTip;
            //textEditDB.Properties.NullText = Resources.SelectDataBaseNullText;
            textEditUserName.ToolTip = Resources.UserNameToolTip;
            textEditPassword.ToolTip = Resources.EditPsswdToolTip;
            simpleButtonClear.Text = Resources.ClearRecord;
            simpleButtonClear.ToolTip = Resources.ClearRecordToolTip;
            simpleButtonSave.Text = Resources.SavingParameters;
            simpleButtonSave.ToolTip = Resources.SavingParametersTooltip;
            labelTypeConnect.Text = Resources.TypeConnect + ":";
            comboBoxAuth.ToolTip = Resources.ChangeAuth;
            textWinLog.ToolTip = Resources.WinLog;
            leUsers.ToolTip = Resources.UsersToolTip;
            tePassword.ToolTip = Resources.PasswordToolTip;
            comboBoxAuth.Properties.Items.Clear();
            comboBoxAuth.Properties.Items.Add(Resources.TrackControl);
            comboBoxAuth.Properties.Items.Add(Resources.WindowsDirectory);
            LdapAuthentication ldp = new LdapAuthentication();
            comboBoxDomens.Properties.Items.Add(ldp.GetCurrentDomain());
            currentDomainName = ldp.GetCurrentDomain();
            comboBoxDomens.Enabled = false;
            comboBoxDomens.SelectedIndex = -1;
        } // Localization

        private void sbEnter_Click(object sender, EventArgs e)
        {
            if (comboBoxAuth.SelectedItem.Equals(Resources.TrackControl))
            {
                if (tePassword.Text.Length == 0)
                {
                    dxErrP.SetError(tePassword, Resources.Modal_Auth_AccessDenied);
                    return;
                }

                Authirization();
            }
            else if (comboBoxAuth.SelectedItem.Equals(Resources.WindowsDirectory))
            {
                AuthWindowsDirectory();
            }
            else
            {
                throw new Exception(Resources.UnAuthorization);
            }
        }

        private  void AuthWindowsDirectory()
        {
            if (textWinLog.Text.Length == 0)
            {
                XtraMessageBox.Show(Resources.WinLogEmpty, "Error Windows Authorization", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                return;
            }
            if (tePassword.Text.Length == 0)
            {
                XtraMessageBox.Show(Resources.PasswordEmpty, "Error Windows Authorization", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                return;
            }

            LdapAuthentication ldapAuth = new LdapAuthentication(currentDomainName, textWinLog.Text, tePassword.Text);

            if (ldapAuth.ValidateCredentials(textWinLog.Text, tePassword.Text))
            {
                //if (ldapAuth.IsAccountLocked(textWinLog.Text))
                //{
                //    XtraMessageBox.Show(Resources.ErrorEntryDomain, "Error Windows Authorization", MessageBoxButtons.OK,
                //    MessageBoxIcon.Error);
                //    return;
                //}
            }
            else
            {
                XtraMessageBox.Show(Resources.ErrorEntryUser, "Error Windows Authorization", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                return;
            }

            UserBaseCurrent ubc = UserBaseCurrent.Instance;
            UserBase ub = new UserBase();
            int idUser = ub.Id_User(ldapAuth.UserName);
            if(idUser == -1)
            {
                XtraMessageBox.Show(Resources.UserExisting, "User validate", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                return;
            }

            if (ub.Validate(idUser))
            {
                ubc.SetUserBase(ub);
                DialogResult = DialogResult.OK;
                checkButtonAdding.Enabled = true;
            }
            else
            {
                Refuse();
            }
        } // AuthWindowsDirectory

        void Authirization()
        {
            UserBaseCurrent ubc = UserBaseCurrent.Instance; 
 
            if (leUsers.EditValue == null)
            {
                if( ubc.SetDefault( tePassword.Text ) )
                {
                    DialogResult = DialogResult.OK;
                }
                else
                {
                    Refuse();
                }
            }
            else
            {
                UserBase ub = new UserBase();
                if(ub.Validate((int)leUsers.EditValue, tePassword.Text))
                {
                    ubc.SetUserBase(ub);
                    DialogResult = DialogResult.OK;
                }
                else
                {
                    Refuse();
                }
            }
        }

        private void Refuse()
        {
            dxErrP.SetError(tePassword, Resources.Modal_Auth_AccessDenied);
            sbEnter.Enabled = false;
            checkButtonAdding.Enabled = false;
        }

        private string AssemblyCopyright
        {
            get
            {
                // Get all Copyright attributes on this assembly
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyCopyrightAttribute), false);
                // If there aren't any Copyright attributes, return an empty string
                if (attributes.Length == 0)
                    return "";
                // If there is a Copyright attribute, return its value
                return ((AssemblyCopyrightAttribute)attributes[0]).Copyright;
            }
        }

        private void tePassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
            {
                if (comboBoxAuth.SelectedItem.Equals(Resources.TrackControl))
                {
                    Authirization();
                }
                else if (comboBoxAuth.SelectedItem.Equals(Resources.WindowsDirectory))
                {
                    return;
                }
            }
        }

        /// <summary>
        /// ��������� ����� ���������� ����������
        /// </summary>
        void setLanguage()
        {
            string calture = LanguageService.Get();
            //if (CultureInfo.CurrentCulture.Name != calture)
            //{
            if (Thread.CurrentThread.CurrentUICulture.Name != calture)
            {
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(calture);
                Thread.CurrentThread.CurrentCulture = new CultureInfo(calture);
            }
            //}
        }

        /* aketner - 12.09.2012 */
        private void checkButtonAdding_CheckedChanged(object sender, EventArgs e)
        {
            if (checkButtonAdding.Checked == true)
            {
                panelControlServerDB.Visible = true;
                //Height = Height + panelControlServerDB.Height;
                Height = 294 + 224;
                panelControlServerDB.Height = 226;
                checkButtonAdding.Text = Resources.ButtonHide;
                checkButtonAdding.ImageIndex = 1;

                textEditServerName.Select();
                textEditServerName.Focus();
            } // if                   

            if (checkButtonAdding.Checked == false)
            {
                panelControlServerDB.Visible = false;
                //Height = Height - panelControlServerDB.Height;
                Height = 294;
                panelControlServerDB.Height = 224;
                checkButtonAdding.Text = Resources.ButtonParams;
                checkButtonAdding.ImageIndex = 0;

                tePassword.Select();
                tePassword.Focus();
            } // if
        } // checkButtonAdding_CheckedChanged

        private void CreateStringMySql()
        {
            dxErrP.ClearErrors();

            if( textEditServerName.Text.Length == 0 )
            {
                dxErrP.SetError( textEditServerName, Resources.Modal_Auth_AccessDenied );
                return;
            }
            string serverName = textEditServerName.Text;

            if( textEditDB.Text.Length == 0 )
            {
                dxErrP.SetError( textEditDB, Resources.Modal_Auth_AccessDenied );
                return;
            }
            string dataBase = textEditDB.Text;

            if( textEditUserName.Text.Length == 0 )
            {
                dxErrP.SetError( textEditUserName, Resources.Modal_Auth_AccessDenied );
                return;
            }
            string userName = textEditUserName.Text;

            string password = textEditPassword.Text;

            string cryptLoginName = String.Empty;
            try
            {
                cryptLoginName = Crypto.Encryption( String.Concat( "server=", serverName, ";user id=",
                    userName, ";password=", password, ";database=", dataBase ) );
            }
            catch( IOException err )
            {
                XtraMessageBox.Show(err.Message, "CreateStringMySql");
                return;
            } // catch

            Crypto.ServerUse = ComboTypeBD.SelectedIndex;
            Crypto.SaveInConfigFile( @".\TrackControl.exe.config", cryptLoginName );

            if( isConnectType )
            {
                // ������������� ���� ����
                string path = System.Reflection.Assembly.GetExecutingAssembly().Location;
                System.Diagnostics.Process.Start( path );
                System.Diagnostics.Process.GetCurrentProcess().Kill();
            }
            else
            {
                DialogResult = DialogResult.OK;
            }
        }

        private void CreateStringMSSql()
        {
            dxErrP.ClearErrors();

            if( textEditServerName.Text.Length == 0 )
            {
                dxErrP.SetError( textEditServerName, Resources.Modal_Auth_AccessDenied );
                return;
            }
            string serverName = textEditServerName.Text;

            if( textEditDB.Text.Length == 0 )
            {
                dxErrP.SetError( textEditDB, Resources.Modal_Auth_AccessDenied );
                return;
            }
            string dataBase = textEditDB.Text;

            string async_proc = "asynchronous processing=yes;";

            if (Crypto.AuthenWindows == comboTypeConnect.SelectedIndex)
            {
                string trusted_conn = "trusted_connection=yes;";

                string cryptLoginName = String.Empty;

                try
                {
                    cryptLoginName = Crypto.Encryption( String.Concat( "Server=", serverName, ";",
                        "database=", dataBase, ";", trusted_conn, async_proc) );
                }
                catch( IOException err )
                {
                    XtraMessageBox.Show(err.Message, "CreateStringMySql");
                    return;
                } // catch

                Crypto.ServerUse = ComboTypeBD.SelectedIndex;
                Crypto.SaveInConfigFile( @".\TrackControl.exe.config", cryptLoginName );

                if( isConnectType )
                {
                    // ������������� ���� ����
                    string path = System.Reflection.Assembly.GetExecutingAssembly().Location;
                    System.Diagnostics.Process.Start( path );
                    System.Diagnostics.Process.GetCurrentProcess().Kill();
                }
                else
                {
                    DialogResult = DialogResult.OK;
                }
            }
            else if (Crypto.AuthenSqlServer == comboTypeConnect.SelectedIndex)
            {
                if( textEditUserName.Text.Length == 0 )
                {
                    dxErrP.SetError( textEditUserName, Resources.Modal_Auth_AccessDenied );
                    return;
                }
                string userName = textEditUserName.Text;

                string password = textEditPassword.Text;

                string cryptLoginName = String.Empty;
                try
                {
                    cryptLoginName = Crypto.Encryption( String.Concat( "Server=", serverName, ";database=",
                        dataBase, ";user id=", userName, ";password=", password,";", async_proc ) );
                }
                catch( IOException err )
                {
                    XtraMessageBox.Show(err.Message, "CreateStringMySql");
                    return;
                } // catch

                Crypto.ServerUse = ComboTypeBD.SelectedIndex;
                Crypto.SaveInConfigFile( @".\TrackControl.exe.config", cryptLoginName );

                if( isConnectType )
                {
                    // ������������� ���� ����
                    string path = System.Reflection.Assembly.GetExecutingAssembly().Location;
                    System.Diagnostics.Process.Start( path );
                    System.Diagnostics.Process.GetCurrentProcess().Kill();
                }
                else
                {
                    DialogResult = DialogResult.OK;
                }
            }
        }

    // aketner - 12.09.2012
        private void simpleButtonSave_Click( object sender, EventArgs e )
        {
            if (Crypto.MYSQL == ComboTypeBD.SelectedIndex)
            {
                CreateStringMySql();
            }
            else if (Crypto.MSSQL == ComboTypeBD.SelectedIndex)
            {
                CreateStringMSSql();
            }
        } // simpleButtonSave_Click

        private void simpleButtonClear_Click(object sender, EventArgs e)
        {
            textEditServerName.Text = "";
            textEditUserName.Text = "";
            textEditPassword.Text = "";
            textEditDB.Properties.Items.Clear();
        }

        private void checkEditPass_CheckedChanged( object sender, EventArgs e )
        {
            if(checkEditPass.Checked == true)
                textEditPassword.Properties.PasswordChar = (char)0;

            if ( checkEditPass.Checked == false )
                textEditPassword.Properties.PasswordChar = '*';
        }

        private void comboBoxAuth_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxAuth.SelectedItem.Equals(Resources.WindowsDirectory))
            {
                leUsers.Enabled = false;
                textWinLog.Enabled = true;
                textWinLog.Text = "";
                tePassword.Text = "";
                checkButtonAdding.Enabled = false;
                sbEnter.Enabled = false;
                dxErrP.ClearErrors();
                comboBoxDomens.Enabled = true;
                comboBoxDomens.SelectedIndex = 0;
                sbEnter.Enabled = true;
                checkButtonAdding.Enabled = true;
                LdapAuthentication ldp = new LdapAuthentication();
                textWinLog.Text = ldp.GetCurrentDomainUser;
            }
            else if (comboBoxAuth.SelectedItem.Equals(Resources.TrackControl))
            {
                leUsers.Enabled = true;
                textWinLog.Enabled = false;
                textWinLog.Text = "";
                tePassword.Text = "";
                checkButtonAdding.Enabled = false;
                sbEnter.Enabled = false;
                comboBoxDomens.Enabled = false;
                comboBoxDomens.SelectedIndex = -1;
                sbEnter.Enabled = false;
                checkButtonAdding.Enabled = false;
            }
        }

        private void comboBoxDomens_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(comboBoxDomens.SelectedItem != null)
                currentDomainName = comboBoxDomens.SelectedItem.ToString();
        }
    } // Login
}