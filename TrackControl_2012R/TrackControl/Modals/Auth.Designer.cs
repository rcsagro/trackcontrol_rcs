namespace TrackControl.Modals
{
  partial class Auth
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Auth));
      this.mainTable = new System.Windows.Forms.TableLayoutPanel();
      this._button = new System.Windows.Forms.Button();
      this.capture = new System.Windows.Forms.Label();
      this.closeLbl = new System.Windows.Forms.Label();
      this.captureEnd = new System.Windows.Forms.Label();
      this._panel = new System.Windows.Forms.Panel();
      this._deniedTable = new System.Windows.Forms.TableLayoutPanel();
      this.iconDenied = new System.Windows.Forms.Label();
      this.deniedTextLbl = new System.Windows.Forms.Label();
      this._inputTable = new System.Windows.Forms.TableLayoutPanel();
      this.loginLbl = new System.Windows.Forms.Label();
      this.passLbl = new System.Windows.Forms.Label();
      this._loginBox = new System.Windows.Forms.TextBox();
      this._passBox = new System.Windows.Forms.TextBox();
      this.mainTable.SuspendLayout();
      this._panel.SuspendLayout();
      this._deniedTable.SuspendLayout();
      this._inputTable.SuspendLayout();
      this.SuspendLayout();
      // 
      // mainTable
      // 
      this.mainTable.BackColor = System.Drawing.Color.Transparent;
      this.mainTable.ColumnCount = 4;
      this.mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 61F));
      this.mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 29F));
      this.mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 8F));
      this.mainTable.Controls.Add(this._button, 1, 2);
      this.mainTable.Controls.Add(this.capture, 0, 0);
      this.mainTable.Controls.Add(this.closeLbl, 2, 0);
      this.mainTable.Controls.Add(this.captureEnd, 3, 0);
      this.mainTable.Controls.Add(this._panel, 0, 1);
      this.mainTable.Dock = System.Windows.Forms.DockStyle.Fill;
      this.mainTable.Location = new System.Drawing.Point(5, 0);
      this.mainTable.Name = "mainTable";
      this.mainTable.RowCount = 3;
      this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
      this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 144F));
      this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.mainTable.Size = new System.Drawing.Size(340, 215);
      this.mainTable.TabIndex = 0;
      // 
      // _button
      // 
      this.mainTable.SetColumnSpan(this._button, 2);
      this._button.Dock = System.Windows.Forms.DockStyle.Fill;
      this._button.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this._button.ForeColor = System.Drawing.Color.DarkBlue;
      this._button.Location = new System.Drawing.Point(245, 186);
      this._button.Margin = new System.Windows.Forms.Padding(3, 10, 3, 5);
      this._button.Name = "_button";
      this._button.Size = new System.Drawing.Size(84, 24);
      this._button.TabIndex = 0;
      this._button.Text = "�����";
      this._button.UseVisualStyleBackColor = true;
      // 
      // capture
      // 
      this.capture.AutoSize = true;
      this.mainTable.SetColumnSpan(this.capture, 2);
      this.capture.Dock = System.Windows.Forms.DockStyle.Fill;
      this.capture.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.capture.ForeColor = System.Drawing.Color.White;
      this.capture.Location = new System.Drawing.Point(3, 0);
      this.capture.Name = "capture";
      this.capture.Padding = new System.Windows.Forms.Padding(30, 0, 0, 0);
      this.capture.Size = new System.Drawing.Size(297, 32);
      this.capture.TabIndex = 1;
      this.capture.Text = "TrackControl";
      this.capture.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.capture.MouseDown += new System.Windows.Forms.MouseEventHandler(this.capture_MouseDown);
      // 
      // closeLbl
      // 
      this.closeLbl.AutoSize = true;
      this.closeLbl.BackColor = System.Drawing.Color.LightCyan;
      this.closeLbl.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this.closeLbl.Cursor = System.Windows.Forms.Cursors.Hand;
      this.closeLbl.Dock = System.Windows.Forms.DockStyle.Fill;
      this.closeLbl.Image = ((System.Drawing.Image)(resources.GetObject("closeLbl.Image")));
      this.closeLbl.Location = new System.Drawing.Point(305, 2);
      this.closeLbl.Margin = new System.Windows.Forms.Padding(2, 2, 2, 15);
      this.closeLbl.Name = "closeLbl";
      this.closeLbl.Size = new System.Drawing.Size(25, 15);
      this.closeLbl.TabIndex = 2;
      this.closeLbl.Click += new System.EventHandler(this.closeLbl_Click);
      // 
      // captureEnd
      // 
      this.captureEnd.AutoSize = true;
      this.captureEnd.Dock = System.Windows.Forms.DockStyle.Fill;
      this.captureEnd.Location = new System.Drawing.Point(332, 0);
      this.captureEnd.Margin = new System.Windows.Forms.Padding(0);
      this.captureEnd.Name = "captureEnd";
      this.captureEnd.Size = new System.Drawing.Size(8, 32);
      this.captureEnd.TabIndex = 3;
      this.captureEnd.MouseDown += new System.Windows.Forms.MouseEventHandler(this.capture_MouseDown);
      // 
      // _panel
      // 
      this.mainTable.SetColumnSpan(this._panel, 3);
      this._panel.Controls.Add(this._deniedTable);
      this._panel.Controls.Add(this._inputTable);
      this._panel.Dock = System.Windows.Forms.DockStyle.Fill;
      this._panel.Location = new System.Drawing.Point(10, 32);
      this._panel.Margin = new System.Windows.Forms.Padding(10, 0, 2, 0);
      this._panel.Name = "_panel";
      this._panel.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
      this._panel.Size = new System.Drawing.Size(320, 144);
      this._panel.TabIndex = 4;
      // 
      // _deniedTable
      // 
      this._deniedTable.ColumnCount = 3;
      this._deniedTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 32F));
      this._deniedTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 48F));
      this._deniedTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this._deniedTable.Controls.Add(this.iconDenied, 1, 1);
      this._deniedTable.Controls.Add(this.deniedTextLbl, 2, 1);
      this._deniedTable.Location = new System.Drawing.Point(171, 23);
      this._deniedTable.Name = "_deniedTable";
      this._deniedTable.RowCount = 3;
      this._deniedTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 73F));
      this._deniedTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 48F));
      this._deniedTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this._deniedTable.Size = new System.Drawing.Size(136, 77);
      this._deniedTable.TabIndex = 1;
      // 
      // iconDenied
      // 
      this.iconDenied.AutoSize = true;
      this.iconDenied.Dock = System.Windows.Forms.DockStyle.Fill;
      this.iconDenied.Image = ((System.Drawing.Image)(resources.GetObject("iconDenied.Image")));
      this.iconDenied.Location = new System.Drawing.Point(32, 73);
      this.iconDenied.Margin = new System.Windows.Forms.Padding(0);
      this.iconDenied.Name = "iconDenied";
      this.iconDenied.Size = new System.Drawing.Size(48, 48);
      this.iconDenied.TabIndex = 0;
      // 
      // deniedTextLbl
      // 
      this.deniedTextLbl.AutoSize = true;
      this.deniedTextLbl.Dock = System.Windows.Forms.DockStyle.Fill;
      this.deniedTextLbl.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.deniedTextLbl.ForeColor = System.Drawing.Color.Brown;
      this.deniedTextLbl.Location = new System.Drawing.Point(80, 73);
      this.deniedTextLbl.Margin = new System.Windows.Forms.Padding(0);
      this.deniedTextLbl.Name = "deniedTextLbl";
      this.deniedTextLbl.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
      this.deniedTextLbl.Size = new System.Drawing.Size(56, 48);
      this.deniedTextLbl.TabIndex = 1;
      this.deniedTextLbl.Text = "������ ��������";
      this.deniedTextLbl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // _inputTable
      // 
      this._inputTable.ColumnCount = 2;
      this._inputTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 73F));
      this._inputTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this._inputTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
      this._inputTable.Controls.Add(this.loginLbl, 0, 1);
      this._inputTable.Controls.Add(this.passLbl, 0, 2);
      this._inputTable.Controls.Add(this._loginBox, 1, 1);
      this._inputTable.Controls.Add(this._passBox, 1, 2);
      this._inputTable.Location = new System.Drawing.Point(0, 3);
      this._inputTable.Name = "_inputTable";
      this._inputTable.RowCount = 4;
      this._inputTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 77F));
      this._inputTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
      this._inputTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
      this._inputTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this._inputTable.Size = new System.Drawing.Size(152, 138);
      this._inputTable.TabIndex = 0;
      // 
      // loginLbl
      // 
      this.loginLbl.AutoSize = true;
      this.loginLbl.Dock = System.Windows.Forms.DockStyle.Fill;
      this.loginLbl.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.loginLbl.ForeColor = System.Drawing.Color.Navy;
      this.loginLbl.Location = new System.Drawing.Point(3, 77);
      this.loginLbl.Name = "loginLbl";
      this.loginLbl.Size = new System.Drawing.Size(67, 26);
      this.loginLbl.TabIndex = 0;
      this.loginLbl.Text = "�����:";
      this.loginLbl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // passLbl
      // 
      this.passLbl.AutoSize = true;
      this.passLbl.Dock = System.Windows.Forms.DockStyle.Fill;
      this.passLbl.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.passLbl.ForeColor = System.Drawing.Color.Navy;
      this.passLbl.Location = new System.Drawing.Point(3, 103);
      this.passLbl.Name = "passLbl";
      this.passLbl.Size = new System.Drawing.Size(67, 26);
      this.passLbl.TabIndex = 1;
      this.passLbl.Text = "������:";
      this.passLbl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // _loginBox
      // 
      this._loginBox.Dock = System.Windows.Forms.DockStyle.Fill;
      this._loginBox.Location = new System.Drawing.Point(76, 80);
      this._loginBox.MaxLength = 50;
      this._loginBox.Name = "_loginBox";
      this._loginBox.Size = new System.Drawing.Size(73, 20);
      this._loginBox.TabIndex = 2;
      this._loginBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this._loginBox_KeyPress);
      // 
      // _passBox
      // 
      this._passBox.Dock = System.Windows.Forms.DockStyle.Fill;
      this._passBox.Location = new System.Drawing.Point(76, 106);
      this._passBox.MaxLength = 50;
      this._passBox.Name = "_passBox";
      this._passBox.Size = new System.Drawing.Size(73, 20);
      this._passBox.TabIndex = 3;
      this._passBox.UseSystemPasswordChar = true;
      this._passBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this._passBox_KeyPress);
      // 
      // Auth
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.Color.LightSteelBlue;
      this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
      this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
      this.ClientSize = new System.Drawing.Size(350, 225);
      this.ControlBox = false;
      this.Controls.Add(this.mainTable);
      this.DoubleBuffered = true;
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.Name = "Auth";
      this.Padding = new System.Windows.Forms.Padding(5, 0, 5, 10);
      this.ShowIcon = false;
      this.ShowInTaskbar = false;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = "TrackControl  �����������";
      this.TransparencyKey = System.Drawing.Color.LightSteelBlue;
      this.Load += new System.EventHandler(this.Auth_Load);
      this.Shown += new System.EventHandler(this.Auth_Shown);
      this.mainTable.ResumeLayout(false);
      this.mainTable.PerformLayout();
      this._panel.ResumeLayout(false);
      this._deniedTable.ResumeLayout(false);
      this._deniedTable.PerformLayout();
      this._inputTable.ResumeLayout(false);
      this._inputTable.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.TableLayoutPanel mainTable;
    private System.Windows.Forms.Button _button;
    private System.Windows.Forms.Label capture;
    private System.Windows.Forms.Label closeLbl;
    private System.Windows.Forms.Label captureEnd;
    private System.Windows.Forms.Panel _panel;
    private System.Windows.Forms.TableLayoutPanel _inputTable;
    private System.Windows.Forms.Label loginLbl;
    private System.Windows.Forms.Label passLbl;
    private System.Windows.Forms.TextBox _loginBox;
    private System.Windows.Forms.TextBox _passBox;
    private System.Windows.Forms.TableLayoutPanel _deniedTable;
    private System.Windows.Forms.Label iconDenied;
    private System.Windows.Forms.Label deniedTextLbl;
  }
}