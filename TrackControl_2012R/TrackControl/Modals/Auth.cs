using System;
using System.Windows.Forms;
using TrackControl.General;
using TrackControl.General.Core;
using TrackControl.Properties;

namespace TrackControl.Modals
{
  public partial class Auth : Form
  {
    IAuthProvider _provider;

    public Auth(IAuthProvider provider)
    {
      _provider = provider;

      InitializeComponent();
      init();
    }

    private void Auth_Load(object sender, EventArgs e)
    {
      showLoginPanel();
    }

    void capture_MouseDown(object sender, MouseEventArgs e)
    {
      if (e.Button == MouseButtons.Left)
      {
        Win32.ReleaseCapture();
        Win32.SendMessage(Handle, Win32.WM_NCLBUTTONDOWN, Win32.HTCAPTION, 0);
      }
    }

    void closeLbl_Click(object sender, EventArgs e)
    {
      DialogResult = DialogResult.Abort;
      Close();
    }

    void showAccessDenied()
    {
      _inputTable.Enabled = false;
      _inputTable.Dock = DockStyle.None;
      _inputTable.Visible = false;

      _deniedTable.Enabled = true;
      _deniedTable.Dock = DockStyle.Fill;
      _deniedTable.Visible = true;

      _button.Text = Resources.Modal_Auth_OneMore;
      _button.Click -= login_Click;
      _button.Click += denied_Click;
    }

    void showLoginPanel()
    {
      _deniedTable.Enabled = false;
      _deniedTable.Dock = DockStyle.None;
      _deniedTable.Visible = false;

      _inputTable.Enabled = true;
      _inputTable.Dock = DockStyle.Fill;
      _inputTable.Visible = true;

      _button.Text = Resources.Modal_Auth_ComeIn;
      _button.Click -= denied_Click;
      _button.Click += login_Click;

      _loginBox.Text = "";
      _passBox.Text = "";
      _loginBox.Focus();
    }

    void login_Click(object sender, EventArgs e)
    {
      if (validate() && _provider.IsValidLogin(_loginBox.Text, _passBox.Text))
      {
        DialogResult = DialogResult.OK;
        Close();
      }
      else
      {
        showAccessDenied();
      }
    }

    void denied_Click(object sender, EventArgs e)
    {
      showLoginPanel();
    }

    bool validate()
    {
      if (_loginBox.Text.Length == 0)
      {
        return false;
      }
      return true;
    }

    void _passBox_KeyPress(object sender, KeyPressEventArgs e)
    {
      if (e.KeyChar.ToString() == "\r")
      {
        login_Click(null, null);
      }
    }

    void _loginBox_KeyPress(object sender, KeyPressEventArgs e)
    {
      if (e.KeyChar.ToString() == "\r")
      {
        _passBox.Focus();
      }
    }

    void Auth_Shown(object sender, EventArgs e)
    {
      _loginBox.Focus();
    }

    void init()
    {
      Text = Resources.Modal_Auth_Caption;
      _button.Text = Resources.Modal_Auth_ComeIn;
      deniedTextLbl.Text = Resources.Modal_Auth_AccessDenied;
      loginLbl.Text = Resources.Login;
      passLbl.Text = Resources.Password;
    }
  }
}