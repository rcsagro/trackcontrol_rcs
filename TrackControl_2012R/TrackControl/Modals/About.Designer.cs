namespace TrackControl.Modals
{
  partial class About
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(About));
            this.mainTable = new System.Windows.Forms.TableLayoutPanel();
            this._copyright = new System.Windows.Forms.Label();
            this.titleLbl = new System.Windows.Forms.Label();
            this._version = new System.Windows.Forms.Label();
            this._paragraph1 = new System.Windows.Forms.Label();
            this._paragraph2 = new System.Windows.Forms.Label();
            this.contactsTable = new System.Windows.Forms.TableLayoutPanel();
            this.logoLbl = new System.Windows.Forms.Label();
            this._contactsLbl = new System.Windows.Forms.Label();
            this._telTitle = new System.Windows.Forms.Label();
            this._faxTitle = new System.Windows.Forms.Label();
            this._tel = new System.Windows.Forms.Label();
            this._fax = new System.Windows.Forms.Label();
            this._webTitle = new System.Windows.Forms.Label();
            this.autovisionLnk = new System.Windows.Forms.LinkLabel();
            this.rcsLnk = new System.Windows.Forms.LinkLabel();
            this._mailTitle = new System.Windows.Forms.Label();
            this.officeMail = new System.Windows.Forms.Label();
            this.salesMail = new System.Windows.Forms.Label();
            this.supportMail = new System.Windows.Forms.Label();
            this.closeBtn = new DevExpress.XtraEditors.SimpleButton();
            this.mainTable.SuspendLayout();
            this.contactsTable.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainTable
            // 
            this.mainTable.BackColor = System.Drawing.Color.Transparent;
            this.mainTable.ColumnCount = 4;
            this.mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 13F));
            this.mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.mainTable.Controls.Add(this._copyright, 2, 7);
            this.mainTable.Controls.Add(this.titleLbl, 2, 1);
            this.mainTable.Controls.Add(this._version, 2, 2);
            this.mainTable.Controls.Add(this._paragraph1, 2, 3);
            this.mainTable.Controls.Add(this._paragraph2, 2, 4);
            this.mainTable.Controls.Add(this.contactsTable, 2, 5);
            this.mainTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainTable.Location = new System.Drawing.Point(0, 0);
            this.mainTable.Margin = new System.Windows.Forms.Padding(0);
            this.mainTable.Name = "mainTable";
            this.mainTable.RowCount = 8;
            this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 69F));
            this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 220F));
            this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.mainTable.Size = new System.Drawing.Size(387, 478);
            this.mainTable.TabIndex = 0;
            // 
            // _copyright
            // 
            this._copyright.AutoSize = true;
            this._copyright.Dock = System.Windows.Forms.DockStyle.Fill;
            this._copyright.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._copyright.ForeColor = System.Drawing.Color.White;
            this._copyright.Location = new System.Drawing.Point(48, 455);
            this._copyright.Margin = new System.Windows.Forms.Padding(0);
            this._copyright.Name = "_copyright";
            this._copyright.Size = new System.Drawing.Size(311, 23);
            this._copyright.TabIndex = 1;
            this._copyright.Text = "� 2008-2016 ��� ���ѻ. ��� ����� ��������.";
            this._copyright.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this._copyright.Click += new System.EventHandler(this._copyright_Click);
            // 
            // titleLbl
            // 
            this.titleLbl.AutoSize = true;
            this.titleLbl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.titleLbl.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.titleLbl.ForeColor = System.Drawing.Color.Navy;
            this.titleLbl.Location = new System.Drawing.Point(48, 50);
            this.titleLbl.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.titleLbl.Name = "titleLbl";
            this.titleLbl.Size = new System.Drawing.Size(308, 28);
            this.titleLbl.TabIndex = 2;
            this.titleLbl.Text = "TrackControl";
            this.titleLbl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // _version
            // 
            this._version.AutoSize = true;
            this._version.Dock = System.Windows.Forms.DockStyle.Fill;
            this._version.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._version.ForeColor = System.Drawing.Color.DimGray;
            this._version.Location = new System.Drawing.Point(58, 78);
            this._version.Margin = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this._version.Name = "_version";
            this._version.Size = new System.Drawing.Size(301, 15);
            this._version.TabIndex = 3;
            this._version.Text = "������ 1.32.0.0 - Beta";
            this._version.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // _paragraph1
            // 
            this._paragraph1.AutoSize = true;
            this.mainTable.SetColumnSpan(this._paragraph1, 2);
            this._paragraph1.Dock = System.Windows.Forms.DockStyle.Fill;
            this._paragraph1.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._paragraph1.ForeColor = System.Drawing.Color.Black;
            this._paragraph1.Location = new System.Drawing.Point(53, 103);
            this._paragraph1.Margin = new System.Windows.Forms.Padding(5, 10, 20, 0);
            this._paragraph1.Name = "_paragraph1";
            this._paragraph1.Size = new System.Drawing.Size(314, 59);
            this._paragraph1.TabIndex = 4;
            this._paragraph1.Text = "TrackControl � ������������� ����������� �����������, ��������������� ��� �������" +
    "�, ������� � ������������ ���������� ���������� ��������� ���������� ���� � ����" +
    "������.";
            // 
            // _paragraph2
            // 
            this._paragraph2.AutoSize = true;
            this.mainTable.SetColumnSpan(this._paragraph2, 2);
            this._paragraph2.Dock = System.Windows.Forms.DockStyle.Fill;
            this._paragraph2.Font = new System.Drawing.Font("Arial", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._paragraph2.ForeColor = System.Drawing.Color.Black;
            this._paragraph2.Location = new System.Drawing.Point(53, 172);
            this._paragraph2.Margin = new System.Windows.Forms.Padding(5, 10, 20, 0);
            this._paragraph2.Name = "_paragraph2";
            this._paragraph2.Size = new System.Drawing.Size(314, 50);
            this._paragraph2.TabIndex = 5;
            this._paragraph2.Text = "�� TrackControl �������� ������������ ������ ����������-����������� ��������� ���" +
    "�������� ���������, ����������� � ��������������� ���������� TELETRACK.";
            // 
            // contactsTable
            // 
            this.contactsTable.ColumnCount = 3;
            this.mainTable.SetColumnSpan(this.contactsTable, 2);
            this.contactsTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.contactsTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 47F));
            this.contactsTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.contactsTable.Controls.Add(this.logoLbl, 0, 0);
            this.contactsTable.Controls.Add(this._contactsLbl, 1, 0);
            this.contactsTable.Controls.Add(this._telTitle, 1, 1);
            this.contactsTable.Controls.Add(this._faxTitle, 1, 2);
            this.contactsTable.Controls.Add(this._tel, 2, 1);
            this.contactsTable.Controls.Add(this._fax, 2, 2);
            this.contactsTable.Controls.Add(this._webTitle, 1, 3);
            this.contactsTable.Controls.Add(this.autovisionLnk, 1, 4);
            this.contactsTable.Controls.Add(this.rcsLnk, 1, 5);
            this.contactsTable.Controls.Add(this._mailTitle, 1, 6);
            this.contactsTable.Controls.Add(this.officeMail, 1, 7);
            this.contactsTable.Controls.Add(this.salesMail, 1, 8);
            this.contactsTable.Controls.Add(this.supportMail, 1, 9);
            this.contactsTable.Controls.Add(this.closeBtn, 2, 11);
            this.contactsTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.contactsTable.Location = new System.Drawing.Point(48, 222);
            this.contactsTable.Margin = new System.Windows.Forms.Padding(0, 0, 15, 0);
            this.contactsTable.Name = "contactsTable";
            this.contactsTable.RowCount = 12;
            this.contactsTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.contactsTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 16F));
            this.contactsTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 16F));
            this.contactsTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.contactsTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.contactsTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.contactsTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.contactsTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.contactsTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.contactsTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.contactsTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.contactsTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this.contactsTable.Size = new System.Drawing.Size(324, 220);
            this.contactsTable.TabIndex = 6;
            // 
            // logoLbl
            // 
            this.logoLbl.AutoSize = true;
            this.logoLbl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.logoLbl.Image = ((System.Drawing.Image)(resources.GetObject("logoLbl.Image")));
            this.logoLbl.ImageAlign = System.Drawing.ContentAlignment.TopRight;
            this.logoLbl.Location = new System.Drawing.Point(5, 20);
            this.logoLbl.Margin = new System.Windows.Forms.Padding(5, 20, 0, 0);
            this.logoLbl.Name = "logoLbl";
            this.contactsTable.SetRowSpan(this.logoLbl, 4);
            this.logoLbl.Size = new System.Drawing.Size(115, 57);
            this.logoLbl.TabIndex = 0;
            // 
            // _contactsLbl
            // 
            this._contactsLbl.AutoSize = true;
            this.contactsTable.SetColumnSpan(this._contactsLbl, 2);
            this._contactsLbl.Dock = System.Windows.Forms.DockStyle.Fill;
            this._contactsLbl.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._contactsLbl.ForeColor = System.Drawing.Color.DimGray;
            this._contactsLbl.Location = new System.Drawing.Point(123, 0);
            this._contactsLbl.Name = "_contactsLbl";
            this._contactsLbl.Size = new System.Drawing.Size(198, 20);
            this._contactsLbl.TabIndex = 1;
            this._contactsLbl.Text = "��������";
            // 
            // _telTitle
            // 
            this._telTitle.AutoSize = true;
            this._telTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this._telTitle.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._telTitle.ForeColor = System.Drawing.Color.Black;
            this._telTitle.Location = new System.Drawing.Point(123, 20);
            this._telTitle.Name = "_telTitle";
            this._telTitle.Size = new System.Drawing.Size(41, 16);
            this._telTitle.TabIndex = 2;
            this._telTitle.Text = "���.:";
            this._telTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // _faxTitle
            // 
            this._faxTitle.AutoSize = true;
            this._faxTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this._faxTitle.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._faxTitle.ForeColor = System.Drawing.Color.Black;
            this._faxTitle.Location = new System.Drawing.Point(123, 36);
            this._faxTitle.Name = "_faxTitle";
            this._faxTitle.Size = new System.Drawing.Size(41, 16);
            this._faxTitle.TabIndex = 3;
            this._faxTitle.Text = "����:";
            this._faxTitle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // _tel
            // 
            this._tel.AutoSize = true;
            this._tel.Dock = System.Windows.Forms.DockStyle.Fill;
            this._tel.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._tel.ForeColor = System.Drawing.Color.DarkBlue;
            this._tel.Location = new System.Drawing.Point(167, 20);
            this._tel.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._tel.Name = "_tel";
            this._tel.Size = new System.Drawing.Size(154, 16);
            this._tel.TabIndex = 4;
            this._tel.Text = "+380 (44) 206-69-79";
            this._tel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // _fax
            // 
            this._fax.AutoSize = true;
            this._fax.Dock = System.Windows.Forms.DockStyle.Fill;
            this._fax.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._fax.ForeColor = System.Drawing.Color.DarkBlue;
            this._fax.Location = new System.Drawing.Point(167, 36);
            this._fax.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this._fax.Name = "_fax";
            this._fax.Size = new System.Drawing.Size(154, 16);
            this._fax.TabIndex = 5;
            this._fax.Text = "+380 (44) 206-42-34";
            this._fax.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // _webTitle
            // 
            this._webTitle.AutoSize = true;
            this._webTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this._webTitle.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._webTitle.ForeColor = System.Drawing.Color.Black;
            this._webTitle.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this._webTitle.Location = new System.Drawing.Point(123, 52);
            this._webTitle.Name = "_webTitle";
            this._webTitle.Size = new System.Drawing.Size(41, 25);
            this._webTitle.TabIndex = 6;
            this._webTitle.Text = "���";
            this._webTitle.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // autovisionLnk
            // 
            this.autovisionLnk.AutoSize = true;
            this.contactsTable.SetColumnSpan(this.autovisionLnk, 2);
            this.autovisionLnk.Dock = System.Windows.Forms.DockStyle.Fill;
            this.autovisionLnk.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.autovisionLnk.Location = new System.Drawing.Point(123, 77);
            this.autovisionLnk.Name = "autovisionLnk";
            this.autovisionLnk.Size = new System.Drawing.Size(198, 15);
            this.autovisionLnk.TabIndex = 7;
            this.autovisionLnk.TabStop = true;
            this.autovisionLnk.Text = "http://autovision.com.ua";
            this.autovisionLnk.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.autovisionLnk.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.autovisionLnk_LinkClicked);
            // 
            // rcsLnk
            // 
            this.rcsLnk.AutoSize = true;
            this.contactsTable.SetColumnSpan(this.rcsLnk, 2);
            this.rcsLnk.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rcsLnk.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.rcsLnk.Location = new System.Drawing.Point(123, 92);
            this.rcsLnk.Name = "rcsLnk";
            this.rcsLnk.Size = new System.Drawing.Size(198, 15);
            this.rcsLnk.TabIndex = 8;
            this.rcsLnk.TabStop = true;
            this.rcsLnk.Text = "http://rcs.kiev.ua";
            this.rcsLnk.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.rcsLnk.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.rcsLnk_LinkClicked);
            // 
            // _mailTitle
            // 
            this._mailTitle.AutoSize = true;
            this.contactsTable.SetColumnSpan(this._mailTitle, 2);
            this._mailTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this._mailTitle.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._mailTitle.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this._mailTitle.Location = new System.Drawing.Point(123, 107);
            this._mailTitle.Name = "_mailTitle";
            this._mailTitle.Size = new System.Drawing.Size(198, 25);
            this._mailTitle.TabIndex = 9;
            this._mailTitle.Text = "��. �����";
            this._mailTitle.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // officeMail
            // 
            this.officeMail.AutoSize = true;
            this.contactsTable.SetColumnSpan(this.officeMail, 2);
            this.officeMail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.officeMail.Font = new System.Drawing.Font("Arial Narrow", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.officeMail.ForeColor = System.Drawing.Color.Black;
            this.officeMail.Location = new System.Drawing.Point(123, 132);
            this.officeMail.Name = "officeMail";
            this.officeMail.Size = new System.Drawing.Size(198, 15);
            this.officeMail.TabIndex = 10;
            this.officeMail.Text = "����: rcs@rcs.kiev.ua";
            this.officeMail.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.officeMail.Click += new System.EventHandler(this.officeMail_Click);
            // 
            // salesMail
            // 
            this.salesMail.AutoSize = true;
            this.contactsTable.SetColumnSpan(this.salesMail, 2);
            this.salesMail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.salesMail.Font = new System.Drawing.Font("Arial Narrow", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.salesMail.Location = new System.Drawing.Point(123, 147);
            this.salesMail.Name = "salesMail";
            this.salesMail.Size = new System.Drawing.Size(198, 15);
            this.salesMail.TabIndex = 11;
            this.salesMail.Text = "����� ������: sales@rcs.kiev.ua";
            this.salesMail.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // supportMail
            // 
            this.supportMail.AutoSize = true;
            this.contactsTable.SetColumnSpan(this.supportMail, 2);
            this.supportMail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.supportMail.Font = new System.Drawing.Font("Arial Narrow", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.supportMail.Location = new System.Drawing.Point(123, 162);
            this.supportMail.Name = "supportMail";
            this.supportMail.Size = new System.Drawing.Size(198, 15);
            this.supportMail.TabIndex = 12;
            this.supportMail.Text = "������������: support@autovision.com.ua";
            // 
            // closeBtn
            // 
            this.closeBtn.Dock = System.Windows.Forms.DockStyle.Right;
            this.closeBtn.Location = new System.Drawing.Point(258, 189);
            this.closeBtn.Margin = new System.Windows.Forms.Padding(3, 3, 3, 5);
            this.closeBtn.Name = "closeBtn";
            this.closeBtn.Size = new System.Drawing.Size(63, 26);
            this.closeBtn.TabIndex = 13;
            this.closeBtn.Text = "Ok";
            this.closeBtn.Click += new System.EventHandler(this.closeBtn_Click);
            // 
            // About
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayoutStore = System.Windows.Forms.ImageLayout.Tile;
            this.BackgroundImageStore = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImageStore")));
            this.ClientSize = new System.Drawing.Size(387, 478);
            this.Controls.Add(this.mainTable);
            this.DoubleBuffered = true;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(403, 517);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(395, 513);
            this.Name = "About";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "� ��������� TrackControl";
            this.mainTable.ResumeLayout(false);
            this.mainTable.PerformLayout();
            this.contactsTable.ResumeLayout(false);
            this.contactsTable.PerformLayout();
            this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.TableLayoutPanel mainTable;
    private System.Windows.Forms.Label _copyright;
    private System.Windows.Forms.Label titleLbl;
    private System.Windows.Forms.Label _version;
    private System.Windows.Forms.Label _paragraph1;
    private System.Windows.Forms.Label _paragraph2;
    private System.Windows.Forms.TableLayoutPanel contactsTable;
    private System.Windows.Forms.Label logoLbl;
    private System.Windows.Forms.Label _contactsLbl;
    private System.Windows.Forms.Label _telTitle;
    private System.Windows.Forms.Label _faxTitle;
    private System.Windows.Forms.Label _tel;
    private System.Windows.Forms.Label _fax;
    private System.Windows.Forms.Label _webTitle;
    private System.Windows.Forms.LinkLabel autovisionLnk;
    private System.Windows.Forms.LinkLabel rcsLnk;
    private System.Windows.Forms.Label _mailTitle;
    private System.Windows.Forms.Label officeMail;
    private System.Windows.Forms.Label salesMail;
    private System.Windows.Forms.Label supportMail;
    private DevExpress.XtraEditors.SimpleButton closeBtn;
  }
}