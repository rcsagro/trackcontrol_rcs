namespace TrackControl.Modals
{
  partial class ErrorInfoForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ErrorInfoForm));
      this._table = new System.Windows.Forms.TableLayoutPanel();
      this._closeBtn = new DevExpress.XtraEditors.SimpleButton();
      this._infoBtn = new DevExpress.XtraEditors.SimpleButton();
      this._groupPanel = new DevExpress.XtraEditors.GroupControl();
      this._label = new DevExpress.XtraEditors.LabelControl();
      this._saveBtn = new DevExpress.XtraEditors.SimpleButton();
      this._table.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this._groupPanel)).BeginInit();
      this._groupPanel.SuspendLayout();
      this.SuspendLayout();
      // 
      // _table
      // 
      this._table.ColumnCount = 3;
      this._table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this._table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
      this._table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
      this._table.Controls.Add(this._closeBtn, 2, 1);
      this._table.Controls.Add(this._infoBtn, 1, 1);
      this._table.Controls.Add(this._groupPanel, 0, 0);
      this._table.Controls.Add(this._saveBtn, 0, 1);
      this._table.Dock = System.Windows.Forms.DockStyle.Fill;
      this._table.Location = new System.Drawing.Point(10, 10);
      this._table.Name = "_table";
      this._table.RowCount = 2;
      this._table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this._table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
      this._table.Size = new System.Drawing.Size(422, 310);
      this._table.TabIndex = 0;
      // 
      // _closeBtn
      // 
      this._closeBtn.Dock = System.Windows.Forms.DockStyle.Fill;
      this._closeBtn.Image = TrackControl.General.Shared.Cross;
      this._closeBtn.Location = new System.Drawing.Point(327, 280);
      this._closeBtn.Margin = new System.Windows.Forms.Padding(5);
      this._closeBtn.Name = "_closeBtn";
      this._closeBtn.Size = new System.Drawing.Size(90, 25);
      this._closeBtn.TabIndex = 0;
      this._closeBtn.Text = "�������";
      this._closeBtn.Click += new System.EventHandler(this._closeBtn_Click);
      // 
      // _infoBtn
      // 
      this._infoBtn.Dock = System.Windows.Forms.DockStyle.Fill;
      this._infoBtn.Image = TrackControl.General.Shared.Info;
      this._infoBtn.Location = new System.Drawing.Point(227, 280);
      this._infoBtn.Margin = new System.Windows.Forms.Padding(5);
      this._infoBtn.Name = "_infoBtn";
      this._infoBtn.Size = new System.Drawing.Size(90, 25);
      this._infoBtn.TabIndex = 1;
      this._infoBtn.Text = "���������";
      this._infoBtn.Click += new System.EventHandler(this._infoBtn_Click);
      // 
      // _groupPanel
      // 
      this._groupPanel.Appearance.BackColor = System.Drawing.Color.Transparent;
      this._groupPanel.Appearance.Options.UseBackColor = true;
      this._table.SetColumnSpan(this._groupPanel, 3);
      this._groupPanel.Controls.Add(this._label);
      this._groupPanel.Dock = System.Windows.Forms.DockStyle.Fill;
      this._groupPanel.Location = new System.Drawing.Point(0, 0);
      this._groupPanel.Margin = new System.Windows.Forms.Padding(0, 0, 0, 10);
      this._groupPanel.Name = "_groupPanel";
      this._groupPanel.Size = new System.Drawing.Size(422, 265);
      this._groupPanel.TabIndex = 2;
      this._groupPanel.Text = "Exception Message";
      // 
      // _label
      // 
      this._label.AllowHtmlString = true;
      this._label.Appearance.BackColor = System.Drawing.Color.WhiteSmoke;
      this._label.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("_label.Appearance.Image")));
      this._label.Appearance.Options.UseBackColor = true;
      this._label.Appearance.Options.UseImage = true;
      this._label.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
      this._label.Dock = System.Windows.Forms.DockStyle.Fill;
      this._label.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftTop;
      this._label.Location = new System.Drawing.Point(2, 22);
      this._label.Margin = new System.Windows.Forms.Padding(0);
      this._label.Name = "_label";
      this._label.Size = new System.Drawing.Size(418, 241);
      this._label.TabIndex = 0;
      this._label.Text = resources.GetString("_label.Text");
      // 
      // _saveBtn
      // 
      this._saveBtn.Dock = System.Windows.Forms.DockStyle.Left;
      this._saveBtn.Image = TrackControl.General.Shared.Save;
      this._saveBtn.Location = new System.Drawing.Point(5, 280);
      this._saveBtn.Margin = new System.Windows.Forms.Padding(5);
      this._saveBtn.Name = "_saveBtn";
      this._saveBtn.Size = new System.Drawing.Size(120, 25);
      this._saveBtn.TabIndex = 3;
      this._saveBtn.Text = "��������� � ����";
      this._saveBtn.Click += new System.EventHandler(this.saveBtn_Click);
      // 
      // ErrorInfoForm
      // 
      this.ClientSize = new System.Drawing.Size(442, 330);
      this.Controls.Add(this._table);
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.MaximizeBox = false;
      this.MaximumSize = new System.Drawing.Size(450, 1080);
      this.MinimizeBox = false;
      this.MinimumSize = new System.Drawing.Size(450, 365);
      this.Name = "ErrorInfoForm";
      this.Padding = new System.Windows.Forms.Padding(10);
      this.ShowInTaskbar = false;
      this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = " �������������� ������";
      this._table.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this._groupPanel)).EndInit();
      this._groupPanel.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.TableLayoutPanel _table;
    private DevExpress.XtraEditors.SimpleButton _closeBtn;
    private DevExpress.XtraEditors.SimpleButton _infoBtn;
    private DevExpress.XtraEditors.GroupControl _groupPanel;
    private DevExpress.XtraEditors.LabelControl _label;
    private DevExpress.XtraEditors.SimpleButton _saveBtn;
  }
}