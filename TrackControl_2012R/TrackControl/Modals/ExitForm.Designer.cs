namespace TrackControl
{
    partial class ExitForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ExitForm));
            this.sbCancel = new DevExpress.XtraEditors.SimpleButton();
            this.lbCopyright = new DevExpress.XtraEditors.LabelControl();
            this.dxErrP = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.sbOk = new DevExpress.XtraEditors.SimpleButton();
            this.lbQuestion = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // sbCancel
            // 
            this.sbCancel.Location = new System.Drawing.Point(225, 175);
            this.sbCancel.Name = "sbCancel";
            this.sbCancel.Size = new System.Drawing.Size(74, 27);
            this.sbCancel.TabIndex = 4;
            this.sbCancel.Text = "���";
            this.sbCancel.Click += new System.EventHandler(this.sbCancel_Click);
            // 
            // lbCopyright
            // 
            this.lbCopyright.Location = new System.Drawing.Point(12, 213);
            this.lbCopyright.Name = "lbCopyright";
            this.lbCopyright.Size = new System.Drawing.Size(47, 13);
            this.lbCopyright.TabIndex = 5;
            this.lbCopyright.Text = "Copyright";
            // 
            // dxErrP
            // 
            this.dxErrP.ContainerControl = this;
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.EditValue = ((object)(resources.GetObject("pictureEdit1.EditValue")));
            this.pictureEdit1.Location = new System.Drawing.Point(-3, -1);
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.pictureEdit1.Size = new System.Drawing.Size(350, 140);
            this.pictureEdit1.TabIndex = 6;
            // 
            // sbOk
            // 
            this.sbOk.Location = new System.Drawing.Point(26, 175);
            this.sbOk.Name = "sbOk";
            this.sbOk.Size = new System.Drawing.Size(74, 27);
            this.sbOk.TabIndex = 7;
            this.sbOk.Text = "��";
            this.sbOk.Click += new System.EventHandler(this.sbOk_Click);
            // 
            // lbQuestion
            // 
            this.lbQuestion.Appearance.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbQuestion.Appearance.Options.UseFont = true;
            this.lbQuestion.Appearance.Options.UseTextOptions = true;
            this.lbQuestion.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lbQuestion.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbQuestion.Location = new System.Drawing.Point(12, 145);
            this.lbQuestion.Name = "lbQuestion";
            this.lbQuestion.Size = new System.Drawing.Size(302, 18);
            this.lbQuestion.TabIndex = 8;
            this.lbQuestion.Text = "������������� ����� �� ����������?";
            // 
            // ExitForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(326, 238);
            this.Controls.Add(this.lbQuestion);
            this.Controls.Add(this.sbOk);
            this.Controls.Add(this.pictureEdit1);
            this.Controls.Add(this.lbCopyright);
            this.Controls.Add(this.sbCancel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ExitForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.TopMost = true;
            ((System.ComponentModel.ISupportInitialize)(this.dxErrP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton sbCancel;
        private DevExpress.XtraEditors.LabelControl lbCopyright;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrP;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraEditors.SimpleButton sbOk;
        private DevExpress.XtraEditors.LabelControl lbQuestion;
    }
}