namespace TrackControl.Modals
{
    partial class NoConnection
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainTable = new System.Windows.Forms.TableLayoutPanel();
            this.warningLbl = new System.Windows.Forms.Label();
            this.descriptionLbl = new System.Windows.Forms.Label();
            this.finalTable = new System.Windows.Forms.TableLayoutPanel();
            this.finalLbl = new System.Windows.Forms.Label();
            this.closeBtn = new System.Windows.Forms.Button();
            this.picturePnl = new System.Windows.Forms.Panel();
            this.mainTable.SuspendLayout();
            this.finalTable.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainTable
            // 
            this.mainTable.BackColor = System.Drawing.Color.Transparent;
            this.mainTable.ColumnCount = 3;
            this.mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 93F));
            this.mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 4.633204F));
            this.mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 95.3668F));
            this.mainTable.Controls.Add(this.warningLbl, 2, 0);
            this.mainTable.Controls.Add(this.descriptionLbl, 2, 1);
            this.mainTable.Controls.Add(this.finalTable, 0, 2);
            this.mainTable.Controls.Add(this.picturePnl, 0, 0);
            this.mainTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainTable.Location = new System.Drawing.Point(10, 10);
            this.mainTable.Margin = new System.Windows.Forms.Padding(10);
            this.mainTable.Name = "mainTable";
            this.mainTable.RowCount = 3;
            this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.mainTable.Size = new System.Drawing.Size(372, 157);
            this.mainTable.TabIndex = 0;
            // 
            // warningLbl
            // 
            this.warningLbl.AutoSize = true;
            this.warningLbl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.warningLbl.Font = new System.Drawing.Font("Arial", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.warningLbl.ForeColor = System.Drawing.Color.Navy;
            this.warningLbl.Location = new System.Drawing.Point(105, 0);
            this.warningLbl.Margin = new System.Windows.Forms.Padding(0);
            this.warningLbl.Name = "warningLbl";
            this.warningLbl.Size = new System.Drawing.Size(267, 32);
            this.warningLbl.TabIndex = 0;
            this.warningLbl.Text = "��� ����� �  ����� ������";
            this.warningLbl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // descriptionLbl
            // 
            this.descriptionLbl.AutoSize = true;
            this.descriptionLbl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.descriptionLbl.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.descriptionLbl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(33)))), ((int)(((byte)(33)))));
            this.descriptionLbl.Location = new System.Drawing.Point(105, 32);
            this.descriptionLbl.Margin = new System.Windows.Forms.Padding(0);
            this.descriptionLbl.Name = "descriptionLbl";
            this.descriptionLbl.Padding = new System.Windows.Forms.Padding(0, 10, 0, 0);
            this.descriptionLbl.Size = new System.Drawing.Size(267, 100);
            this.descriptionLbl.TabIndex = 1;
            this.descriptionLbl.Text = "���������� ��������� ������ �����������";
            // 
            // finalTable
            // 
            this.finalTable.ColumnCount = 2;
            this.mainTable.SetColumnSpan(this.finalTable, 3);
            this.finalTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 74.30939F));
            this.finalTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.69061F));
            this.finalTable.Controls.Add(this.finalLbl, 0, 0);
            this.finalTable.Controls.Add(this.closeBtn, 1, 0);
            this.finalTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.finalTable.Location = new System.Drawing.Point(0, 132);
            this.finalTable.Margin = new System.Windows.Forms.Padding(0);
            this.finalTable.Name = "finalTable";
            this.finalTable.RowCount = 1;
            this.finalTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.finalTable.Size = new System.Drawing.Size(372, 25);
            this.finalTable.TabIndex = 3;
            // 
            // finalLbl
            // 
            this.finalLbl.AutoSize = true;
            this.finalLbl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.finalLbl.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.finalLbl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(82)))), ((int)(((byte)(31)))), ((int)(((byte)(31)))));
            this.finalLbl.Location = new System.Drawing.Point(0, 0);
            this.finalLbl.Margin = new System.Windows.Forms.Padding(0);
            this.finalLbl.Name = "finalLbl";
            this.finalLbl.Size = new System.Drawing.Size(276, 25);
            this.finalLbl.TabIndex = 2;
            this.finalLbl.Text = "���������� ����� �������";
            this.finalLbl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // closeBtn
            // 
            this.closeBtn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.closeBtn.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.closeBtn.ForeColor = System.Drawing.Color.Maroon;
            this.closeBtn.Image = TrackControl.General.Shared.Cross;
            this.closeBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.closeBtn.Location = new System.Drawing.Point(276, 0);
            this.closeBtn.Margin = new System.Windows.Forms.Padding(0);
            this.closeBtn.Name = "closeBtn";
            this.closeBtn.Size = new System.Drawing.Size(96, 25);
            this.closeBtn.TabIndex = 3;
            this.closeBtn.Text = "�������";
            this.closeBtn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.closeBtn.UseVisualStyleBackColor = true;
            this.closeBtn.Click += new System.EventHandler(this.closeBtn_Click);
            // 
            // picturePnl
            // 
            this.picturePnl.BackgroundImage = TrackControl.General.Shared.BadConnection;
            this.picturePnl.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.picturePnl.Location = new System.Drawing.Point(0, 0);
            this.picturePnl.Margin = new System.Windows.Forms.Padding(0);
            this.picturePnl.Name = "picturePnl";
            this.mainTable.SetRowSpan(this.picturePnl, 2);
            this.picturePnl.Size = new System.Drawing.Size(93, 93);
            this.picturePnl.TabIndex = 4;
            // 
            // NoConnection
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(232)))), ((int)(((byte)(232)))));
            this.ClientSize = new System.Drawing.Size(392, 177);
            this.ControlBox = false;
            this.Controls.Add(this.mainTable);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(400, 185);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(400, 185);
            this.Name = "NoConnection";
            this.Padding = new System.Windows.Forms.Padding(10);
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.TopMost = true;
            this.mainTable.ResumeLayout(false);
            this.mainTable.PerformLayout();
            this.finalTable.ResumeLayout(false);
            this.finalTable.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel mainTable;
        private System.Windows.Forms.Label warningLbl;
        private System.Windows.Forms.Label descriptionLbl;
        private System.Windows.Forms.Label finalLbl;
        private System.Windows.Forms.TableLayoutPanel finalTable;
        private System.Windows.Forms.Button closeBtn;
        private System.Windows.Forms.Panel picturePnl;
    }
}