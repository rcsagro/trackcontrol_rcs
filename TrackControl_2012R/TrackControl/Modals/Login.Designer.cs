namespace TrackControl
{
    partial class Login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Login));
            this.leUsers = new DevExpress.XtraEditors.LookUpEdit();
            this.lbPassword = new DevExpress.XtraEditors.LabelControl();
            this.tePassword = new DevExpress.XtraEditors.TextEdit();
            this.sbEnter = new DevExpress.XtraEditors.SimpleButton();
            this.lbCopyright = new DevExpress.XtraEditors.LabelControl();
            this.dxErrP = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.panelControlServerDB = new DevExpress.XtraEditors.PanelControl();
            this.labelTypeConnect = new DevExpress.XtraEditors.LabelControl();
            this.comboTypeConnect = new DevExpress.XtraEditors.ComboBoxEdit();
            this.ComboTypeBD = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelTypeDB = new DevExpress.XtraEditors.LabelControl();
            this.textEditDB = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControlDB = new DevExpress.XtraEditors.LabelControl();
            this.checkEditPass = new DevExpress.XtraEditors.CheckEdit();
            this.labelPassword = new DevExpress.XtraEditors.LabelControl();
            this.textEditPassword = new DevExpress.XtraEditors.TextEdit();
            this.simpleButtonClear = new DevExpress.XtraEditors.SimpleButton();
            this.labelControlHelp = new DevExpress.XtraEditors.LabelControl();
            this.labelControlUserName = new DevExpress.XtraEditors.LabelControl();
            this.textEditUserName = new DevExpress.XtraEditors.TextEdit();
            this.simpleButtonSave = new DevExpress.XtraEditors.SimpleButton();
            this.textEditServerName = new DevExpress.XtraEditors.TextEdit();
            this.labelControlServer = new DevExpress.XtraEditors.LabelControl();
            this.checkButtonAdding = new DevExpress.XtraEditors.CheckButton();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.labelUser = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.comboBoxAuth = new DevExpress.XtraEditors.ComboBoxEdit();
            this.textWinLog = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.comboBoxDomens = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.leUsers.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tePassword.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControlServerDB)).BeginInit();
            this.panelControlServerDB.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboTypeConnect.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComboTypeBD.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditDB.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditPass.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPassword.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditUserName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditServerName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxAuth.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textWinLog.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxDomens.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // leUsers
            // 
            this.leUsers.Location = new System.Drawing.Point(104, 162);
            this.leUsers.Name = "leUsers";
            this.leUsers.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leUsers.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name")});
            this.leUsers.Properties.DisplayMember = "Name";
            this.leUsers.Properties.NullText = "������� ��� ������������";
            this.leUsers.Properties.ValueMember = "Id";
            this.leUsers.Size = new System.Drawing.Size(265, 20);
            this.leUsers.TabIndex = 0;
            this.leUsers.ToolTip = "������������ TrackControl";
            // 
            // lbPassword
            // 
            this.lbPassword.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.lbPassword.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbPassword.Location = new System.Drawing.Point(12, 188);
            this.lbPassword.Name = "lbPassword";
            this.lbPassword.Size = new System.Drawing.Size(80, 17);
            this.lbPassword.TabIndex = 13;
            this.lbPassword.Text = "������";
            // 
            // tePassword
            // 
            this.tePassword.Location = new System.Drawing.Point(104, 188);
            this.tePassword.Name = "tePassword";
            this.tePassword.Properties.PasswordChar = '*';
            this.tePassword.Size = new System.Drawing.Size(265, 20);
            this.tePassword.TabIndex = 1;
            this.tePassword.ToolTip = "������";
            this.tePassword.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tePassword_KeyDown);
            // 
            // sbEnter
            // 
            this.sbEnter.Location = new System.Drawing.Point(296, 218);
            this.sbEnter.Name = "sbEnter";
            this.sbEnter.Size = new System.Drawing.Size(74, 27);
            this.sbEnter.TabIndex = 2;
            this.sbEnter.Text = "�����";
            this.sbEnter.ToolTip = "����� � TrackControl";
            this.sbEnter.Click += new System.EventHandler(this.sbEnter_Click);
            // 
            // lbCopyright
            // 
            this.lbCopyright.Location = new System.Drawing.Point(3, 248);
            this.lbCopyright.Name = "lbCopyright";
            this.lbCopyright.Size = new System.Drawing.Size(47, 13);
            this.lbCopyright.TabIndex = 20;
            this.lbCopyright.Text = "Copyright";
            // 
            // dxErrP
            // 
            this.dxErrP.ContainerControl = this;
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.EditValue = ((object)(resources.GetObject("pictureEdit1.EditValue")));
            this.pictureEdit1.Location = new System.Drawing.Point(-3, -1);
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.pictureEdit1.Size = new System.Drawing.Size(383, 79);
            this.pictureEdit1.TabIndex = 6;
            // 
            // panelControlServerDB
            // 
            this.panelControlServerDB.Controls.Add(this.labelTypeConnect);
            this.panelControlServerDB.Controls.Add(this.comboTypeConnect);
            this.panelControlServerDB.Controls.Add(this.ComboTypeBD);
            this.panelControlServerDB.Controls.Add(this.labelTypeDB);
            this.panelControlServerDB.Controls.Add(this.textEditDB);
            this.panelControlServerDB.Controls.Add(this.labelControlDB);
            this.panelControlServerDB.Controls.Add(this.checkEditPass);
            this.panelControlServerDB.Controls.Add(this.labelPassword);
            this.panelControlServerDB.Controls.Add(this.textEditPassword);
            this.panelControlServerDB.Controls.Add(this.simpleButtonClear);
            this.panelControlServerDB.Controls.Add(this.labelControlHelp);
            this.panelControlServerDB.Controls.Add(this.labelControlUserName);
            this.panelControlServerDB.Controls.Add(this.textEditUserName);
            this.panelControlServerDB.Controls.Add(this.simpleButtonSave);
            this.panelControlServerDB.Controls.Add(this.textEditServerName);
            this.panelControlServerDB.Controls.Add(this.labelControlServer);
            this.panelControlServerDB.Location = new System.Drawing.Point(-4, 262);
            this.panelControlServerDB.Name = "panelControlServerDB";
            this.panelControlServerDB.Size = new System.Drawing.Size(384, 224);
            this.panelControlServerDB.TabIndex = 9;
            // 
            // labelTypeConnect
            // 
            this.labelTypeConnect.Location = new System.Drawing.Point(10, 54);
            this.labelTypeConnect.Name = "labelTypeConnect";
            this.labelTypeConnect.Size = new System.Drawing.Size(122, 13);
            this.labelTypeConnect.TabIndex = 15;
            this.labelTypeConnect.Text = "�������� �����������:";
            // 
            // comboTypeConnect
            // 
            this.comboTypeConnect.Location = new System.Drawing.Point(138, 51);
            this.comboTypeConnect.Name = "comboTypeConnect";
            this.comboTypeConnect.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboTypeConnect.Size = new System.Drawing.Size(235, 20);
            this.comboTypeConnect.TabIndex = 5;
            // 
            // ComboTypeBD
            // 
            this.ComboTypeBD.Location = new System.Drawing.Point(138, 24);
            this.ComboTypeBD.Name = "ComboTypeBD";
            this.ComboTypeBD.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ComboTypeBD.Size = new System.Drawing.Size(236, 20);
            this.ComboTypeBD.TabIndex = 4;
            // 
            // labelTypeDB
            // 
            this.labelTypeDB.Location = new System.Drawing.Point(10, 27);
            this.labelTypeDB.Name = "labelTypeDB";
            this.labelTypeDB.Size = new System.Drawing.Size(84, 13);
            this.labelTypeDB.TabIndex = 14;
            this.labelTypeDB.Text = "��� ������� ��:";
            // 
            // textEditDB
            // 
            this.textEditDB.Location = new System.Drawing.Point(138, 155);
            this.textEditDB.Name = "textEditDB";
            this.textEditDB.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.textEditDB.Properties.PopupSizeable = true;
            this.textEditDB.Size = new System.Drawing.Size(235, 20);
            this.textEditDB.TabIndex = 9;
            this.textEditDB.ToolTip = "������� ���� ������";
            // 
            // labelControlDB
            // 
            this.labelControlDB.Location = new System.Drawing.Point(10, 158);
            this.labelControlDB.Name = "labelControlDB";
            this.labelControlDB.Size = new System.Drawing.Size(69, 13);
            this.labelControlDB.TabIndex = 19;
            this.labelControlDB.Text = "���� ������:";
            // 
            // checkEditPass
            // 
            this.checkEditPass.Location = new System.Drawing.Point(358, 128);
            this.checkEditPass.Name = "checkEditPass";
            this.checkEditPass.Properties.Caption = "";
            this.checkEditPass.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Style1;
            this.checkEditPass.Size = new System.Drawing.Size(23, 22);
            this.checkEditPass.TabIndex = 13;
            this.checkEditPass.CheckedChanged += new System.EventHandler(this.checkEditPass_CheckedChanged);
            // 
            // labelPassword
            // 
            this.labelPassword.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelPassword.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelPassword.Location = new System.Drawing.Point(10, 130);
            this.labelPassword.Name = "labelPassword";
            this.labelPassword.Size = new System.Drawing.Size(80, 17);
            this.labelPassword.TabIndex = 18;
            this.labelPassword.Text = "������:";
            // 
            // textEditPassword
            // 
            this.textEditPassword.Location = new System.Drawing.Point(139, 129);
            this.textEditPassword.Name = "textEditPassword";
            this.textEditPassword.Properties.PasswordChar = '*';
            this.textEditPassword.Size = new System.Drawing.Size(215, 20);
            this.textEditPassword.TabIndex = 8;
            this.textEditPassword.ToolTip = "������� ����� ������ ������������";
            // 
            // simpleButtonClear
            // 
            this.simpleButtonClear.Location = new System.Drawing.Point(102, 187);
            this.simpleButtonClear.Name = "simpleButtonClear";
            this.simpleButtonClear.Size = new System.Drawing.Size(111, 27);
            this.simpleButtonClear.TabIndex = 11;
            this.simpleButtonClear.Text = "��������";
            this.simpleButtonClear.ToolTip = "�������� ����";
            this.simpleButtonClear.Click += new System.EventHandler(this.simpleButtonClear_Click);
            // 
            // labelControlHelp
            // 
            this.labelControlHelp.Location = new System.Drawing.Point(11, 5);
            this.labelControlHelp.Name = "labelControlHelp";
            this.labelControlHelp.Size = new System.Drawing.Size(225, 13);
            this.labelControlHelp.TabIndex = 21;
            this.labelControlHelp.Text = "������� ��������� ��� ����������� � ��";
            // 
            // labelControlUserName
            // 
            this.labelControlUserName.Location = new System.Drawing.Point(10, 106);
            this.labelControlUserName.Name = "labelControlUserName";
            this.labelControlUserName.Size = new System.Drawing.Size(76, 13);
            this.labelControlUserName.TabIndex = 17;
            this.labelControlUserName.Text = "������������:";
            // 
            // textEditUserName
            // 
            this.textEditUserName.Location = new System.Drawing.Point(139, 103);
            this.textEditUserName.Name = "textEditUserName";
            this.textEditUserName.Size = new System.Drawing.Size(234, 20);
            this.textEditUserName.TabIndex = 7;
            // 
            // simpleButtonSave
            // 
            this.simpleButtonSave.Location = new System.Drawing.Point(287, 187);
            this.simpleButtonSave.Name = "simpleButtonSave";
            this.simpleButtonSave.Size = new System.Drawing.Size(87, 27);
            this.simpleButtonSave.TabIndex = 10;
            this.simpleButtonSave.Text = "���������";
            this.simpleButtonSave.ToolTip = "��������� ���������";
            this.simpleButtonSave.Click += new System.EventHandler(this.simpleButtonSave_Click);
            // 
            // textEditServerName
            // 
            this.textEditServerName.Location = new System.Drawing.Point(138, 77);
            this.textEditServerName.Name = "textEditServerName";
            this.textEditServerName.Size = new System.Drawing.Size(235, 20);
            this.textEditServerName.TabIndex = 6;
            this.textEditServerName.ToolTip = "������� ��� ������� ��� ����������";
            // 
            // labelControlServer
            // 
            this.labelControlServer.Location = new System.Drawing.Point(10, 80);
            this.labelControlServer.Name = "labelControlServer";
            this.labelControlServer.Size = new System.Drawing.Size(84, 13);
            this.labelControlServer.TabIndex = 16;
            this.labelControlServer.Text = "��� ������� ��:";
            // 
            // checkButtonAdding
            // 
            this.checkButtonAdding.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.checkButtonAdding.ImageIndex = 0;
            this.checkButtonAdding.ImageList = this.imageCollection1;
            this.checkButtonAdding.Location = new System.Drawing.Point(98, 218);
            this.checkButtonAdding.Name = "checkButtonAdding";
            this.checkButtonAdding.Size = new System.Drawing.Size(92, 27);
            this.checkButtonAdding.TabIndex = 3;
            this.checkButtonAdding.Text = "���������";
            this.checkButtonAdding.ToolTip = "������� ������ ����������� � ��";
            this.checkButtonAdding.CheckedChanged += new System.EventHandler(this.checkButtonAdding_CheckedChanged);
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            this.imageCollection1.Images.SetKeyName(0, "Button_down.png");
            this.imageCollection1.Images.SetKeyName(1, "Button_up.png");
            // 
            // labelUser
            // 
            this.labelUser.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelUser.Location = new System.Drawing.Point(12, 161);
            this.labelUser.Name = "labelUser";
            this.labelUser.Size = new System.Drawing.Size(72, 13);
            this.labelUser.TabIndex = 12;
            this.labelUser.Text = "������������";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControl1.Location = new System.Drawing.Point(12, 87);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(66, 13);
            this.labelControl1.TabIndex = 21;
            this.labelControl1.Text = "�����������";
            // 
            // comboBoxAuth
            // 
            this.comboBoxAuth.EditValue = "TrackControl";
            this.comboBoxAuth.Location = new System.Drawing.Point(104, 84);
            this.comboBoxAuth.Name = "comboBoxAuth";
            this.comboBoxAuth.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxAuth.Properties.Items.AddRange(new object[] {
            "TrackControl",
            "WindowsDirectory"});
            this.comboBoxAuth.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxAuth.Size = new System.Drawing.Size(266, 20);
            this.comboBoxAuth.TabIndex = 22;
            this.comboBoxAuth.ToolTip = "����� ���� �����������";
            this.comboBoxAuth.SelectedIndexChanged += new System.EventHandler(this.comboBoxAuth_SelectedIndexChanged);
            // 
            // textWinLog
            // 
            this.textWinLog.Location = new System.Drawing.Point(104, 136);
            this.textWinLog.Name = "textWinLog";
            this.textWinLog.Size = new System.Drawing.Size(265, 20);
            this.textWinLog.TabIndex = 23;
            this.textWinLog.ToolTip = "�������� ��� � �����";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(12, 139);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(75, 13);
            this.labelControl2.TabIndex = 24;
            this.labelControl2.Text = "Windows �����";
            // 
            // comboBoxDomens
            // 
            this.comboBoxDomens.Location = new System.Drawing.Point(104, 110);
            this.comboBoxDomens.Name = "comboBoxDomens";
            this.comboBoxDomens.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxDomens.Properties.Sorted = true;
            this.comboBoxDomens.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxDomens.Size = new System.Drawing.Size(266, 20);
            this.comboBoxDomens.TabIndex = 25;
            this.comboBoxDomens.ToolTip = "�������� ����� ��� �����";
            this.comboBoxDomens.SelectedIndexChanged += new System.EventHandler(this.comboBoxDomens_SelectedIndexChanged);
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(12, 113);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(86, 13);
            this.labelControl3.TabIndex = 26;
            this.labelControl3.Text = "������ Windows";
            // 
            // Login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(379, 487);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.comboBoxDomens);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.textWinLog);
            this.Controls.Add(this.comboBoxAuth);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.labelUser);
            this.Controls.Add(this.checkButtonAdding);
            this.Controls.Add(this.panelControlServerDB);
            this.Controls.Add(this.pictureEdit1);
            this.Controls.Add(this.lbCopyright);
            this.Controls.Add(this.sbEnter);
            this.Controls.Add(this.tePassword);
            this.Controls.Add(this.lbPassword);
            this.Controls.Add(this.leUsers);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Login";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            ((System.ComponentModel.ISupportInitialize)(this.leUsers.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tePassword.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControlServerDB)).EndInit();
            this.panelControlServerDB.ResumeLayout(false);
            this.panelControlServerDB.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboTypeConnect.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComboTypeBD.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditDB.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkEditPass.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPassword.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditUserName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditServerName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxAuth.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textWinLog.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxDomens.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LookUpEdit leUsers;
        private DevExpress.XtraEditors.LabelControl lbPassword;
        private DevExpress.XtraEditors.TextEdit tePassword;
        private DevExpress.XtraEditors.SimpleButton sbEnter;
        private DevExpress.XtraEditors.LabelControl lbCopyright;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrP;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraEditors.PanelControl panelControlServerDB;
        private DevExpress.XtraEditors.LabelControl labelControlServer;
        private DevExpress.XtraEditors.SimpleButton simpleButtonSave;
        private DevExpress.XtraEditors.TextEdit textEditServerName;
        private DevExpress.XtraEditors.TextEdit textEditUserName;
        private DevExpress.XtraEditors.LabelControl labelControlUserName;
        private DevExpress.XtraEditors.LabelControl labelControlHelp;
        private DevExpress.XtraEditors.SimpleButton simpleButtonClear;
        private DevExpress.XtraEditors.CheckButton checkButtonAdding;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraEditors.LabelControl labelPassword;
        private DevExpress.XtraEditors.TextEdit textEditPassword;
        private DevExpress.XtraEditors.CheckEdit checkEditPass;
        private DevExpress.XtraEditors.LabelControl labelControlDB;
        private DevExpress.XtraEditors.ComboBoxEdit textEditDB;
        private DevExpress.XtraEditors.LabelControl labelTypeDB;
        private DevExpress.XtraEditors.ComboBoxEdit ComboTypeBD;
        private DevExpress.XtraEditors.LabelControl labelTypeConnect;
        private DevExpress.XtraEditors.ComboBoxEdit comboTypeConnect;
        private DevExpress.XtraEditors.LabelControl labelUser;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxAuth;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit textWinLog;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxDomens;
    }
}