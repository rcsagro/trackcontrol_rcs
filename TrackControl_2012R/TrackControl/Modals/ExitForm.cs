using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.LookAndFeel;
using TrackControl.General;
using TrackControl.General.Services;
using TrackControl.Properties;

namespace TrackControl
{
    public partial class ExitForm : DevExpress.XtraEditors.XtraForm
    {
        public event Action<bool> ConfirmExit;
        public ExitForm()
        {
            InitializeComponent();
            Localization();
        }
        void Localization()
        {
            this.Text = string.Format("TrackControl {0}", Application.ProductVersion);
            lbCopyright.Text = AssemblyCopyright;
            lbQuestion.Text = Resources.ConfirmExit;
            sbOk.Text = Resources.Yes;
            sbCancel.Text = Resources.No; 
        }

        private void sbCancel_Click(object sender, EventArgs e)
        {
            if (ConfirmExit!=null) ConfirmExit(false);
            this.Close(); 
        }


        private string AssemblyCopyright
        {
            get
            {
                // Get all Copyright attributes on this assembly
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyCopyrightAttribute), false);
                // If there aren't any Copyright attributes, return an empty string
                if (attributes.Length == 0)
                    return "";
                // If there is a Copyright attribute, return its value
                return ((AssemblyCopyrightAttribute)attributes[0]).Copyright;
            }
        }

        private void sbOk_Click(object sender, EventArgs e)
        {
            if (ConfirmExit != null) ConfirmExit(true);
            this.Close(); 
        }



    }
}