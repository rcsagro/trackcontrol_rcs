using System;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using System.ComponentModel;
using DevExpress.XtraEditors;
using System.IO;
using TrackControl.Properties;

namespace TrackControl.Modals
{
    public partial class ErrorInfoForm : XtraForm
    {
        private MemoEdit _stackMemo;
        private bool _isStackVisible;
        private Exception _exception;

        public ErrorInfoForm(Exception ex)
        {
            _exception = ex;
            InitializeComponent();
            init();
            _groupPanel.Text = _exception.Message + "\n" + _exception.StackTrace;
            initStackMemo(_exception);
            showWelcome();
        }

        #region --   GUI Handlers   --

        private void _closeBtn_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void _infoBtn_Click(object sender, EventArgs e)
        {
            if (_isStackVisible)
                showWelcome();
            else
                showDetails();
        }

        private void saveBtn_Click(object sender, EventArgs e)
        {
            saveErrorIntoFile();
        }

        #endregion

        private void initStackMemo(Exception ex)
        {
            _stackMemo = new MemoEdit();
            _stackMemo.Dock = DockStyle.Fill;
            _stackMemo.BackColor = Color.WhiteSmoke;
            ((ISupportInitialize) (_stackMemo.Properties)).BeginInit();
            StringBuilder message = new StringBuilder();
            message.AppendFormat("{0}: {1}{2}", ex.GetType(), ex.Message, Environment.NewLine);
            message.AppendLine();
            message.AppendLine("Stack Trace");
            message.AppendLine(ex.StackTrace);
            _stackMemo.Text = message.ToString();
            ((ISupportInitialize) (_stackMemo.Properties)).EndInit();
        }

        private void showWelcome()
        {
            _isStackVisible = false;
            _infoBtn.Text = Resources.Modal_ErrorInfo_Details;
            _stackMemo.Parent = null;
            _label.Parent = _groupPanel;
            ActiveControl = _groupPanel;
        }

        private void showDetails()
        {
            _isStackVisible = true;
            _infoBtn.Text = Resources.Back;
            _label.Parent = null;
            _stackMemo.Parent = _groupPanel;
            ActiveControl = _groupPanel;
        }

        private void saveErrorIntoFile()
        {
            using (SaveFileDialog sfd = new SaveFileDialog())
            {
                sfd.Filter = "Text file (*.txt)| *.txt";
                sfd.FileName = "TrackControl Bug";
                if (DialogResult.OK == sfd.ShowDialog())
                {
                    try
                    {
                        if (File.Exists(sfd.FileName))
                            File.Delete(sfd.FileName);

                        using (StreamWriter writer = File.AppendText(sfd.FileName))
                        {
                            writer.WriteLine(" --------------------------------------------------- ");
                            writer.WriteLine("    Bug in TrackControl - {0}", Application.ProductVersion);
                            writer.WriteLine(" --------------------------------------------------- ");
                            writer.WriteLine();
                            writer.WriteLine("Message: \"{0}\"", _exception.Message);
                            writer.WriteLine();
                            writer.WriteLine("Stack Trace:");
                            writer.WriteLine(_exception.StackTrace);
                            writer.WriteLine();
                            writer.WriteLine(" --------------------------------------------------- ");
                            writer.WriteLine("Framework: {0}", Environment.Version);
                        }
                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show(ex.Message, Resources.Modal_ErrorInfo_NotSaved);
                        return;
                    }

                    XtraMessageBox.Show(Resources.Modal_ErrorInfo_SuccessfullySaved);
                }
            }
        }

        private void init()
        {
            Text = Resources.Modal_ErrorInfo_Caption;
            _closeBtn.Text = Resources.Close;
            _infoBtn.Text = Resources.Modal_ErrorInfo_Details;
            _saveBtn.Text = Resources.Modal_ErrorInfo_SaveToFile;
        }
    }
}