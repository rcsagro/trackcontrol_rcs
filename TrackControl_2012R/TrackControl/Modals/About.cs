using System;
using System.Diagnostics;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using TrackControl.Properties;

namespace TrackControl.Modals
{
  public partial class About : XtraForm
  {
    public About()
    {
      InitializeComponent();
      init();
    }

    void init()
    {
      Text = Resources.Modal_About_Caption;
      _copyright.Text = Resources.Modal_About_Copyright;
      _paragraph1.Text = Resources.Modal_About_Paragraph1;
      _paragraph2.Text = Resources.Modal_About_Paragraph2;
      _contactsLbl.Text = Resources.Modal_About_Contacts;
      _telTitle.Text = Resources.Modal_About_Tel;
      _faxTitle.Text = Resources.Modal_About_Fax;
      _webTitle.Text = Resources.Modal_About_Web;
      _mailTitle.Text = Resources.Modal_About_Mail;
      officeMail.Text = Resources.Modal_About_Office;
      salesMail.Text = Resources.Modal_About_Sales;
      supportMail.Text = Resources.Modal_About_Support;
      _version.Text = String.Format("{0} {1}", Resources.Modal_About_Version, Application.ProductVersion);
    }

    void autovisionLnk_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
    {
      Process.Start("http://autovision.com.ua/");
    }

    void rcsLnk_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
    {
      Process.Start("http://rcs.kiev.ua/");
    }

    void closeBtn_Click(object sender, EventArgs e)
    {
      Close();
    }

    private void _copyright_Click(object sender, EventArgs e)
    {

    }

    private void officeMail_Click(object sender, EventArgs e)
    {

    }
  }
}