using System;
using System.Windows.Forms;
using TrackControl.Properties;

namespace TrackControl.Modals
{
  public partial class NoConnection : Form
  {
    public NoConnection()
    {
      InitializeComponent();
      init();
    }

    void init()
    {
      descriptionLbl.Text = Resources.Modal_NoConnection_Text;
      warningLbl.Text = Resources.Modal_NoConnection_Warning;
      finalLbl.Text = Resources.Modal_NoConnection_Final;
      closeBtn.Text = Resources.Close;
    }

    /// <summary>
    /// ������������ ���� �� ������ "�������".
    /// </summary>
    void closeBtn_Click(object sender, EventArgs e)
    {
      Application.Exit();
    }
  }
}