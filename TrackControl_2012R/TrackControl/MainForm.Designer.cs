﻿namespace TrackControl
{
  partial class MainForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            this._barManager = new DevExpress.XtraBars.BarManager(this.components);
            this._mainMenu = new DevExpress.XtraBars.Bar();
            this._fileItem = new DevExpress.XtraBars.BarSubItem();
            this._reportsItem = new DevExpress.XtraBars.BarButtonItem();
            this._monitoringItem = new DevExpress.XtraBars.BarButtonItem();
            this._exitItem = new DevExpress.XtraBars.BarButtonItem();
            this._notificationItem = new DevExpress.XtraBars.BarCheckItem();
            this._modulsItem = new DevExpress.XtraBars.BarSubItem();
            this._motordepotItem = new DevExpress.XtraBars.BarButtonItem();
            this._agroItem = new DevExpress.XtraBars.BarButtonItem();
            this._routesItem = new DevExpress.XtraBars.BarButtonItem();
            this._toolsItem = new DevExpress.XtraBars.BarSubItem();
            this._referenceItem = new DevExpress.XtraBars.BarButtonItem();
            this._vehiclesEditorBtn = new DevExpress.XtraBars.BarButtonItem();
            this._driversEditorBtn = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSetupNotice = new DevExpress.XtraBars.BarButtonItem();
            this._zonesEditorItem = new DevExpress.XtraBars.BarButtonItem();
            this._themesItem = new DevExpress.XtraBars.BarSubItem();
            this._languageItem = new DevExpress.XtraBars.BarSubItem();
            this._languageRU = new DevExpress.XtraBars.BarButtonItem();
            this._languageEN = new DevExpress.XtraBars.BarButtonItem();
            this._languagePT = new DevExpress.XtraBars.BarButtonItem();
            this._languageTH = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonEspanol = new DevExpress.XtraBars.BarButtonItem();
            this._languageUK = new DevExpress.XtraBars.BarButtonItem();
            this._settingsItem = new DevExpress.XtraBars.BarButtonItem();
            this._helpItem = new DevExpress.XtraBars.BarSubItem();
            this._1cIntegrationItem = new DevExpress.XtraBars.BarButtonItem();
            this.bbiUserGuide = new DevExpress.XtraBars.BarButtonItem();
            this._aboutItem = new DevExpress.XtraBars.BarButtonItem();
            this._testButtonItem = new DevExpress.XtraBars.BarButtonItem();
            this.bsiLastData = new DevExpress.XtraBars.BarStaticItem();
            this.icStatus = new DevExpress.XtraBars.BarEditItem();
            this.ricStatus = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.timeImages = new DevExpress.Utils.ImageCollection(this.components);
            this._barAndDockingController = new DevExpress.XtraBars.BarAndDockingController(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.bbiSaveLayoutItem = new DevExpress.XtraBars.BarButtonItem();
            this._mainPanel = new DevExpress.XtraEditors.PanelControl();
            ((System.ComponentModel.ISupportInitialize)(this._barManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ricStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeImages)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._barAndDockingController)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._mainPanel)).BeginInit();
            this.SuspendLayout();
            // 
            // _barManager
            // 
            this._barManager.AllowCustomization = false;
            this._barManager.AllowQuickCustomization = false;
            this._barManager.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this._mainMenu});
            this._barManager.Controller = this._barAndDockingController;
            this._barManager.DockControls.Add(this.barDockControlTop);
            this._barManager.DockControls.Add(this.barDockControlBottom);
            this._barManager.DockControls.Add(this.barDockControlLeft);
            this._barManager.DockControls.Add(this.barDockControlRight);
            this._barManager.Form = this;
            this._barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this._fileItem,
            this._reportsItem,
            this._monitoringItem,
            this._notificationItem,
            this._toolsItem,
            this._modulsItem,
            this._helpItem,
            this._aboutItem,
            this._testButtonItem,
            this._referenceItem,
            this._settingsItem,
            this._themesItem,
            this._languageItem,
            this._languageRU,
            this._languageEN,
            this._zonesEditorItem,
            this._1cIntegrationItem,
            this._vehiclesEditorBtn,
            this._driversEditorBtn,
            this._exitItem,
            this.bsiLastData,
            this.bbiSetupNotice,
            this.bbiUserGuide,
            this._languagePT,
            this._languageTH,
            this._motordepotItem,
            this._agroItem,
            this._routesItem,
            this.bbiSaveLayoutItem,
            this.barButtonEspanol,
            this._languageUK,
            this.icStatus});
            this._barManager.MainMenu = this._mainMenu;
            this._barManager.MaxItemId = 39;
            this._barManager.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.ricStatus});
            // 
            // _mainMenu
            // 
            this._mainMenu.BarName = "Main menu";
            this._mainMenu.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Top;
            this._mainMenu.DockCol = 0;
            this._mainMenu.DockRow = 0;
            this._mainMenu.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this._mainMenu.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.None, this._fileItem, "", false, false, true, 0),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.None, this._notificationItem, "", false, false, true, 0),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.None, this._modulsItem, "", false, false, true, 0),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.None, this._toolsItem, "", false, false, true, 0),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.None, this._helpItem, "", false, false, true, 0),
            new DevExpress.XtraBars.LinkPersistInfo(this._testButtonItem),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiLastData),
            new DevExpress.XtraBars.LinkPersistInfo(this.icStatus)});
            this._mainMenu.OptionsBar.AllowQuickCustomization = false;
            this._mainMenu.OptionsBar.DisableClose = true;
            this._mainMenu.OptionsBar.DisableCustomization = true;
            this._mainMenu.OptionsBar.DrawDragBorder = false;
            this._mainMenu.OptionsBar.MultiLine = true;
            this._mainMenu.OptionsBar.UseWholeRow = true;
            this._mainMenu.Text = "Main menu";
            // 
            // _fileItem
            // 
            this._fileItem.Caption = "Режим";
            this._fileItem.Id = 0;
            this._fileItem.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.None, this._reportsItem, "", false, false, true, 0),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.None, this._monitoringItem, "", false, false, true, 0),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.None, this._exitItem, "", true, false, true, 0)});
            this._fileItem.Name = "_fileItem";
            // 
            // _reportsItem
            // 
            this._reportsItem.Caption = "Отчеты";
            this._reportsItem.Glyph = ((System.Drawing.Image)(resources.GetObject("_reportsItem.Glyph")));
            this._reportsItem.Id = 1;
            this._reportsItem.Name = "_reportsItem";
            this._reportsItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.reportsItem_ItemClick);
            // 
            // _monitoringItem
            // 
            this._monitoringItem.Caption = "Слежение";
            this._monitoringItem.Glyph = ((System.Drawing.Image)(resources.GetObject("_monitoringItem.Glyph")));
            this._monitoringItem.Id = 2;
            this._monitoringItem.Name = "_monitoringItem";
            this._monitoringItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.monitoringItem_ItemClick);
            // 
            // _exitItem
            // 
            this._exitItem.Caption = "Выход";
            this._exitItem.Glyph = ((System.Drawing.Image)(resources.GetObject("_exitItem.Glyph")));
            this._exitItem.Id = 20;
            this._exitItem.Name = "_exitItem";
            this._exitItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.exitItem_ItemClick);
            // 
            // _notificationItem
            // 
            this._notificationItem.Caption = "Уведомления";
            this._notificationItem.Id = 3;
            this._notificationItem.Name = "_notificationItem";
            this._notificationItem.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this._notificationItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.notificationItem_ItemClick);
            // 
            // _modulsItem
            // 
            this._modulsItem.Caption = "Модули";
            this._modulsItem.Id = 5;
            this._modulsItem.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this._motordepotItem),
            new DevExpress.XtraBars.LinkPersistInfo(this._agroItem),
            new DevExpress.XtraBars.LinkPersistInfo(this._routesItem)});
            this._modulsItem.Name = "_modulsItem";
            this._modulsItem.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // _motordepotItem
            // 
            this._motordepotItem.Caption = "Модуль \"Автопредприятие\"";
            this._motordepotItem.Id = 33;
            this._motordepotItem.Name = "_motordepotItem";
            this._motordepotItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this._motordepotItem_ItemClick);
            // 
            // _agroItem
            // 
            this._agroItem.Caption = "Модуль AGRO";
            this._agroItem.Id = 31;
            this._agroItem.Name = "_agroItem";
            this._agroItem.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this._agroItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.agroItem_ItemClick);
            // 
            // _routesItem
            // 
            this._routesItem.Caption = "Модуль \"Маршруты\"";
            this._routesItem.Id = 32;
            this._routesItem.Name = "_routesItem";
            this._routesItem.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this._routesItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.routesItem_ItemClick);
            // 
            // _toolsItem
            // 
            this._toolsItem.Caption = "Сервис";
            this._toolsItem.Id = 4;
            this._toolsItem.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.None, this._referenceItem, "", false, false, true, 0),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.None, this._vehiclesEditorBtn, "", true, false, true, 0),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.None, this._driversEditorBtn, "", false, false, true, 0),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.None, this.bbiSetupNotice, "", false, false, true, 0),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.None, this._zonesEditorItem, "", false, false, true, 0),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.None, this._themesItem, "", true, false, true, 0),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.None, this._languageItem, "", true, false, true, 0),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.None, this._settingsItem, "", true, false, true, 0)});
            this._toolsItem.Name = "_toolsItem";
            // 
            // _referenceItem
            // 
            this._referenceItem.Caption = "Справочники...";
            this._referenceItem.Id = 9;
            this._referenceItem.Name = "_referenceItem";
            this._referenceItem.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this._referenceItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.referenceItem_ItemClick);
            // 
            // _vehiclesEditorBtn
            // 
            this._vehiclesEditorBtn.Caption = "Управление транспортом...";
            this._vehiclesEditorBtn.Glyph = ((System.Drawing.Image)(resources.GetObject("_vehiclesEditorBtn.Glyph")));
            this._vehiclesEditorBtn.Id = 18;
            this._vehiclesEditorBtn.Name = "_vehiclesEditorBtn";
            this._vehiclesEditorBtn.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.vehiclesEditorBtn_ItemClick);
            // 
            // _driversEditorBtn
            // 
            this._driversEditorBtn.Caption = "Управление водителями...";
            this._driversEditorBtn.Glyph = ((System.Drawing.Image)(resources.GetObject("_driversEditorBtn.Glyph")));
            this._driversEditorBtn.Id = 19;
            this._driversEditorBtn.Name = "_driversEditorBtn";
            this._driversEditorBtn.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.driversEditorBtn_ItemClick);
            // 
            // bbiSetupNotice
            // 
            this.bbiSetupNotice.Caption = "Управление уведомлениями...";
            this.bbiSetupNotice.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiSetupNotice.Glyph")));
            this.bbiSetupNotice.Id = 26;
            this.bbiSetupNotice.Name = "bbiSetupNotice";
            this.bbiSetupNotice.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSetupNotice_ItemClick);
            // 
            // _zonesEditorItem
            // 
            this._zonesEditorItem.Caption = "Редактор контрольных зон...";
            this._zonesEditorItem.Glyph = ((System.Drawing.Image)(resources.GetObject("_zonesEditorItem.Glyph")));
            this._zonesEditorItem.Id = 14;
            this._zonesEditorItem.Name = "_zonesEditorItem";
            this._zonesEditorItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.zonesEditorItem_ItemClick);
            // 
            // _themesItem
            // 
            this._themesItem.Caption = "Темы приложения";
            this._themesItem.Id = 13;
            this._themesItem.Name = "_themesItem";
            // 
            // _languageItem
            // 
            this._languageItem.Caption = "Язык";
            this._languageItem.Id = 22;
            this._languageItem.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this._languageRU),
            new DevExpress.XtraBars.LinkPersistInfo(this._languageEN),
            new DevExpress.XtraBars.LinkPersistInfo(this._languagePT),
            new DevExpress.XtraBars.LinkPersistInfo(this._languageTH),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonEspanol),
            new DevExpress.XtraBars.LinkPersistInfo(this._languageUK)});
            this._languageItem.Name = "_languageItem";
            // 
            // _languageRU
            // 
            this._languageRU.Caption = "Русский";
            this._languageRU.Id = 23;
            this._languageRU.Name = "_languageRU";
            this._languageRU.Tag = "ru-RU";
            this._languageRU.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.OnLangClick);
            // 
            // _languageEN
            // 
            this._languageEN.Caption = "English";
            this._languageEN.Id = 24;
            this._languageEN.Name = "_languageEN";
            this._languageEN.Tag = "en-US";
            this._languageEN.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.OnLangClick);
            // 
            // _languagePT
            // 
            this._languagePT.Caption = "Португальский";
            this._languagePT.Id = 28;
            this._languagePT.Name = "_languagePT";
            this._languagePT.Tag = "pt-PT";
            this._languagePT.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.OnLangClick);
            // 
            // _languageTH
            // 
            this._languageTH.Caption = "Таиландский";
            this._languageTH.Id = 29;
            this._languageTH.Name = "_languageTH";
            this._languageTH.Tag = "th-TH";
            this._languageTH.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.OnLangClick);
            // 
            // barButtonEspanol
            // 
            this.barButtonEspanol.Caption = "Español";
            this.barButtonEspanol.Id = 36;
            this.barButtonEspanol.Name = "barButtonEspanol";
            this.barButtonEspanol.Tag = "es-ES";
            this.barButtonEspanol.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.OnLangClick);
            // 
            // _languageUK
            // 
            this._languageUK.Caption = "Українська";
            this._languageUK.Id = 37;
            this._languageUK.Name = "_languageUK";
            this._languageUK.Tag = "uk-UA";
            this._languageUK.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.OnLangClick);
            // 
            // _settingsItem
            // 
            this._settingsItem.Caption = "Настройки...";
            this._settingsItem.Glyph = ((System.Drawing.Image)(resources.GetObject("_settingsItem.Glyph")));
            this._settingsItem.Id = 11;
            this._settingsItem.Name = "_settingsItem";
            this._settingsItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.settingsItem_ItemClick);
            // 
            // _helpItem
            // 
            this._helpItem.Caption = "Справка";
            this._helpItem.Id = 6;
            this._helpItem.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.None, this._1cIntegrationItem, "", true, false, true, 0),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.None, this.bbiUserGuide, "", true, false, true, 0),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.None, this._aboutItem, "", true, false, true, 0)});
            this._helpItem.Name = "_helpItem";
            // 
            // _1cIntegrationItem
            // 
            this._1cIntegrationItem.Caption = "Интеграция  с  1С";
            this._1cIntegrationItem.Glyph = ((System.Drawing.Image)(resources.GetObject("_1cIntegrationItem.Glyph")));
            this._1cIntegrationItem.Id = 16;
            this._1cIntegrationItem.Name = "_1cIntegrationItem";
            this._1cIntegrationItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this._1cIntegrationItem_ItemClick);
            // 
            // bbiUserGuide
            // 
            this.bbiUserGuide.Caption = "Руководство пользователя";
            this.bbiUserGuide.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiUserGuide.Glyph")));
            this.bbiUserGuide.Id = 27;
            this.bbiUserGuide.Name = "bbiUserGuide";
            this.bbiUserGuide.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiUserGuide_ItemClick);
            // 
            // _aboutItem
            // 
            this._aboutItem.Caption = "О программе";
            this._aboutItem.Glyph = ((System.Drawing.Image)(resources.GetObject("_aboutItem.Glyph")));
            this._aboutItem.Id = 7;
            this._aboutItem.Name = "_aboutItem";
            this._aboutItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this._aboutItem_ItemClick);
            // 
            // _testButtonItem
            // 
            this._testButtonItem.Caption = "Test";
            this._testButtonItem.Id = 8;
            this._testButtonItem.Name = "_testButtonItem";
            this._testButtonItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.testButtonItem_ItemClick);
            // 
            // bsiLastData
            // 
            this.bsiLastData.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bsiLastData.Caption = "Last data:";
            this.bsiLastData.Id = 25;
            this.bsiLastData.Name = "bsiLastData";
            this.bsiLastData.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // icStatus
            // 
            this.icStatus.Edit = this.ricStatus;
            this.icStatus.EditValue = 2;
            this.icStatus.Id = 38;
            this.icStatus.Name = "icStatus";
            this.icStatus.Width = 22;
            // 
            // ricStatus
            // 
            this.ricStatus.AllowDropDownWhenReadOnly = DevExpress.Utils.DefaultBoolean.False;
            this.ricStatus.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ricStatus.Appearance.BackColor2 = System.Drawing.Color.Transparent;
            this.ricStatus.Appearance.BorderColor = System.Drawing.Color.Transparent;
            this.ricStatus.Appearance.Options.UseBackColor = true;
            this.ricStatus.Appearance.Options.UseBorderColor = true;
            this.ricStatus.AppearanceReadOnly.BorderColor = System.Drawing.Color.Transparent;
            this.ricStatus.AppearanceReadOnly.Options.UseBorderColor = true;
            this.ricStatus.AutoHeight = false;
            this.ricStatus.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.ricStatus.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Plus, "", -1, true, false, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", null, null, true)});
            this.ricStatus.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.ricStatus.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", 0, 0),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", 1, 1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", 2, 2),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", 3, 3)});
            this.ricStatus.Name = "ricStatus";
            this.ricStatus.ReadOnly = true;
            this.ricStatus.ShowDropDown = DevExpress.XtraEditors.Controls.ShowDropDown.Never;
            this.ricStatus.SmallImages = this.timeImages;
            // 
            // timeImages
            // 
            this.timeImages.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("timeImages.ImageStream")));
            this.timeImages.Images.SetKeyName(0, "status.png");
            this.timeImages.Images.SetKeyName(1, "status-away.png");
            this.timeImages.Images.SetKeyName(2, "status-busy.png");
            this.timeImages.Images.SetKeyName(3, "status-offline.png");
            // 
            // _barAndDockingController
            // 
            this._barAndDockingController.PropertiesBar.DefaultGlyphSize = new System.Drawing.Size(16, 16);
            this._barAndDockingController.PropertiesBar.DefaultLargeGlyphSize = new System.Drawing.Size(32, 32);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(796, 22);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 495);
            this.barDockControlBottom.Size = new System.Drawing.Size(796, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 22);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 473);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(796, 22);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 473);
            // 
            // bbiSaveLayoutItem
            // 
            this.bbiSaveLayoutItem.Caption = "Сохранить расположение элементов формы";
            this.bbiSaveLayoutItem.Id = 34;
            this.bbiSaveLayoutItem.Name = "bbiSaveLayoutItem";
            // 
            // _mainPanel
            // 
            this._mainPanel.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this._mainPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this._mainPanel.Location = new System.Drawing.Point(0, 22);
            this._mainPanel.Margin = new System.Windows.Forms.Padding(0);
            this._mainPanel.Name = "_mainPanel";
            this._mainPanel.Size = new System.Drawing.Size(796, 473);
            this._mainPanel.TabIndex = 5;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(796, 495);
            this.Controls.Add(this._mainPanel);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.this_FormClosing);
            this.Load += new System.EventHandler(this.this_Load);
            this.LocationChanged += new System.EventHandler(this.MainForm_LocationChanged);
            this.SizeChanged += new System.EventHandler(this.this_SizeChanged);
            ((System.ComponentModel.ISupportInitialize)(this._barManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ricStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeImages)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._barAndDockingController)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._mainPanel)).EndInit();
            this.ResumeLayout(false);

    }

    #endregion

    private DevExpress.XtraBars.BarManager _barManager;
    private DevExpress.XtraBars.Bar _mainMenu;
    private DevExpress.XtraBars.BarDockControl barDockControlTop;
    private DevExpress.XtraBars.BarDockControl barDockControlBottom;
    private DevExpress.XtraBars.BarDockControl barDockControlLeft;
    private DevExpress.XtraBars.BarDockControl barDockControlRight;
    private DevExpress.XtraBars.BarAndDockingController _barAndDockingController;
    private DevExpress.XtraBars.BarSubItem _fileItem;
    private DevExpress.XtraBars.BarButtonItem _reportsItem;
    private DevExpress.XtraBars.BarButtonItem _monitoringItem;
    private DevExpress.XtraBars.BarCheckItem _notificationItem;
    private DevExpress.XtraBars.BarSubItem _toolsItem;
    private DevExpress.XtraBars.BarButtonItem _referenceItem;
    private DevExpress.XtraBars.BarSubItem _themesItem;
    private DevExpress.XtraBars.BarButtonItem _settingsItem;
    private DevExpress.XtraBars.BarSubItem _modulsItem;
    private DevExpress.XtraBars.BarSubItem _helpItem;
    private DevExpress.XtraBars.BarButtonItem _aboutItem;
    private DevExpress.XtraBars.BarButtonItem _testButtonItem;
    private DevExpress.XtraBars.BarButtonItem _zonesEditorItem;
    private DevExpress.XtraEditors.PanelControl _mainPanel;
    private DevExpress.XtraBars.BarButtonItem _1cIntegrationItem;
    private DevExpress.XtraBars.BarButtonItem _vehiclesEditorBtn;
    private DevExpress.XtraBars.BarButtonItem _driversEditorBtn;
    private DevExpress.XtraBars.BarButtonItem _exitItem;
    private DevExpress.XtraBars.BarSubItem _languageItem;
    private DevExpress.XtraBars.BarButtonItem _languageRU;
    private DevExpress.XtraBars.BarButtonItem _languageEN;
    private DevExpress.XtraBars.BarStaticItem bsiLastData;
    private DevExpress.XtraBars.BarButtonItem bbiSetupNotice;
    private DevExpress.XtraBars.BarButtonItem bbiUserGuide;
    private DevExpress.XtraBars.BarButtonItem _languagePT;
    private DevExpress.XtraBars.BarButtonItem _languageTH;
    private DevExpress.XtraBars.BarButtonItem _motordepotItem;
    private DevExpress.XtraBars.BarButtonItem _agroItem;
    private DevExpress.XtraBars.BarButtonItem _routesItem;
    private DevExpress.XtraBars.BarButtonItem bbiSaveLayoutItem;
    private DevExpress.XtraBars.BarButtonItem barButtonEspanol;
    private DevExpress.XtraBars.BarButtonItem _languageUK;
    private DevExpress.Utils.ImageCollection timeImages;
    private DevExpress.XtraBars.BarEditItem icStatus;
    private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox ricStatus;
  }
}