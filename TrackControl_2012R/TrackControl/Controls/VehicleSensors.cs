﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Windows.Forms;
using BaseReports.Procedure.Calibration;
using DevExpress.XtraEditors;
using LocalCache;
using TrackControl.General;
using TrackControl.General.DatabaseDriver;
using TrackControl.Properties;
using TrackControl.Reports;
using TrackControl.Vehicles;

namespace TrackControl
{
    public class VehicleSensors
    {
        private IVehicle _vehicle;
        private VehiclesModel _model;

        public VehicleSensors(IVehicle vehicle)
        {
            _vehicle = vehicle;
        }

        public void AssignVehicleModel(VehiclesModel model)
        {
            if (null == model)
                throw new ArgumentNullException("model");
            _model = model;
        }

        public void SetSensorValues()
        {
            try
            {
                if (_model == null || _vehicle == null) return;
                _vehicle.Sensors.Clear();

                atlantaDataSet.sensorsRow[] sensors =
                    (atlantaDataSet.sensorsRow[])
                        AppModel.Instance.DataSet.sensors.Select(String.Format("mobitel_id = {0}", _vehicle.Mobitel.Id));

                if (sensors.Length > 0)
                {
                    IList<Driver> drivers = _model.Drivers;

                    for (int i = 0; i < sensors.Length; i++)
                    {
                        atlantaDataSet.sensorsRow sensor = sensors[i];
                        _vehicle.LogicSensorState = (short) LogicSensorStates.Absence;

                        if (sensor.Length > 1)
                        {
                            SetSensor(drivers, sensor);
                        }
                        else if (sensor.Length == 1)
                        {
                            SetSensorLogic(sensor);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message + "\n" + ex.StackTrace, "Error SetSensorValues", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        private void SetSensorLogic(atlantaDataSet.sensorsRow sensor)
        {
            try
            {
                BitArray bits = new BitArray(_vehicle.LastPoint.Sensors);
                int val = 0;
                try
                {
                    val = bits[sensor.StartBit] ? 1 : 0;
                }
                catch(Exception ex)
                {
                    return;
                }

                foreach (atlantaDataSet.StateRow stateRow in sensor.GetStateRows())
                {
                    if (val == stateRow.MinValue)
                    {
                        atlantaDataSet.relationalgorithmsRow[] algs = sensor.GetrelationalgorithmsRows();

                        if (algs.Length > 0)
                            AddSensorToVehicle(sensor, 0, stateRow.Title, algs[0].AlgorithmID);

                        if (null != algs && algs.Length > 0 && algs[0].AlgorithmID == (int) AlgorithmType.ONLINE_LOGIC)
                        {
                            if (sensor.K != (float) val)
                                _vehicle.LogicSensorState = (short) LogicSensorStates.Off;
                            else
                                _vehicle.LogicSensorState = (short) LogicSensorStates.On;
                        }

                        break;
                    } // if
                } // foreach
            } // try
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message + "\n" + ex.StackTrace, "Error SetSensorLogic", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        } // SetSensorLogic

        private void SetSensor(IList<Driver> drivers, atlantaDataSet.sensorsRow sensor)
        {
            try
            {
                int _algorithm = 0;
                atlantaDataSet.relationalgorithmsRow[] algs = sensor.GetrelationalgorithmsRows();

                if (null != algs && algs.Length > 0 && algs[0].AlgorithmID == (int) AlgorithmType.DRIVER)
                {
                    SetSensorDriver(drivers, sensor, (int) AlgorithmType.DRIVER);
                }
                else if (null != algs && algs.Length > 0 && algs[0].AlgorithmID == (int) AlgorithmType.RANGEFINDER)
                {
                    SetSensorRANGEFINDER(sensor, (int) AlgorithmType.RANGEFINDER);
                }
                else
                {
                    Calibrate calibrate = new Calibrate();
                    Array.ForEach(sensor.GetsensorcoefficientRows(),
                        delegate(atlantaDataSet.sensorcoefficientRow c_row)
                        {
                            Coefficient coefficient = new Coefficient();
                            coefficient.User = c_row.UserValue;
                            coefficient.K = c_row.K;
                            coefficient.b = c_row.b;
                            calibrate.Collection.Add(c_row.SensorValue, coefficient);
                        });

                    double value = 0;
                    string nameAgregat = "";

                    if (null != algs && algs.Length > 0 && algs[0].AlgorithmID == (int) AlgorithmType.ROTATE_E)
                    {
                        value = calibrate.GetUserValueSuppressHighZero(_vehicle.LastPoint.Sensors, sensor.Length,
                            sensor.StartBit, sensor.K,sensor.B,sensor.S);
                        _algorithm = (int) AlgorithmType.ROTATE_E;
                    }
                    else if (null != algs && algs.Length > 0 && algs[0].AlgorithmID == (int) AlgorithmType.FUELDRTADD)
                    {
                        _algorithm = (int) AlgorithmType.FUELDRTADD;
                        value = calibrate.GetUserValue(_vehicle.LastPoint.Sensors, sensor.Length,
                            sensor.StartBit, sensor.K, sensor.B, sensor.S);
                        value = 0;
                    }
                    else if (null != algs && algs.Length > 0 && algs[0].AlgorithmID == (int) AlgorithmType.AGREGAT)
                    {
                        value = calibrate.GetUserValue(_vehicle.LastPoint.Sensors, sensor.Length, sensor.StartBit,
                            sensor.K,sensor.B,sensor.S);
                        _algorithm = (int) algs[0].AlgorithmID;

                        if (value <= 0)
                        {
                            // получим имя агрегата из справочника
                            nameAgregat = "(" + value + ")" + getNameAgregatFromReference(_vehicle.Id);
                        }
                        else
                        {
                            // получим имя агрегата на прямую
                            nameAgregat = "(" + value + ")" + getNameAgregat(value);
                        }

                        AddSensorToVehicle(sensor, nameAgregat,
                            String.Format("{0:F2} {1}", nameAgregat, sensor.NameUnit), _algorithm);
                        return;
                    }
                    else
                    {
                        value = calibrate.GetUserValue(_vehicle.LastPoint.Sensors, sensor.Length, sensor.StartBit,
                            sensor.K,sensor.B,sensor.S);
                        if (algs.Length > 0) _algorithm = (int) algs[0].AlgorithmID;
                    }

                    AddSensorToVehicle(sensor, value, String.Format("{0:F2} {1}", value, sensor.NameUnit), _algorithm);
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message + "\n" + ex.StackTrace, "Error in SetSensor", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        private string getNameAgregatFromReference(int id)
        {
            try
            {
                string sqls = "SELECT aav.Id_agregat FROM agro_agregat_vehicle aav WHERE aav.Id_vehicle = " + id;

                using (var driverDb = new DriverDb())
                {
                    
                    driverDb.ConnectDb();
                    if (!driverDb.IsTableExist("agro_agregat")) return "";
                    DataTable table = driverDb.GetDataTable(sqls);
                    if (table.Rows.Count > 0)
                    {
                        DataRow dr = table.Rows[0];
                        int idagregat = Convert.ToInt32(dr["Id_agregat"]);
                        sqls = "SELECT aa.Name FROM agro_agregat aa WHERE aa.Id = " + idagregat;
                        table = driverDb.GetDataTable(sqls);
                        if (table.Rows.Count > 0)
                        {
                            dr = table.Rows[0];
                            if (!dr.IsNull("Name"))
                            {
                                return dr["Name"].ToString();
                            }
                            return "";
                        }
                    }
                }

                return "";
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message + "\n" + ex.StackTrace, "Error in getNameAgregatFromReference", MessageBoxButtons.OK,
                   MessageBoxIcon.Error);
                return "";
            }
        }

        private string getNameAgregat(double id)
        {
            try
            {
                string sqls = "SELECT aa.Name FROM agro_agregat aa WHERE aa.Identifier = " + Convert.ToInt32(id);

                using (var driverDb = new DriverDb())
                {
                    driverDb.ConnectDb();
                    if (!driverDb.IsTableExist("agro_agregat")) return "";
                    DataTable table = driverDb.GetDataTable(sqls);
                    if (table.Rows.Count > 0)
                    {
                        DataRow dr = table.Rows[0];
                        if (!dr.IsNull("Name"))
                        {
                            return dr["Name"].ToString();
                        }
                    }
                }

                return "";
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message + "\n" + ex.StackTrace, "Error in getNameAgregat", MessageBoxButtons.OK,
                   MessageBoxIcon.Error);
                return "";
            }
        }

        private void SetSensorRANGEFINDER(atlantaDataSet.sensorsRow sensor, int idalgoritm)
        {
            try
            {
                Calibrate calibrate = new Calibrate();
                Array.ForEach(sensor.GetsensorcoefficientRows(),
                    delegate(atlantaDataSet.sensorcoefficientRow c_row)
                    {
                        Coefficient coefficient = new Coefficient();
                        coefficient.User = c_row.UserValue;
                        coefficient.K = c_row.K;
                        coefficient.b = c_row.b;
                        calibrate.Collection.Add(c_row.SensorValue, coefficient);
                    });

                double value = 0;

                value = calibrate.GetUserValue(_vehicle.LastPoint.Sensors, sensor.Length, sensor.StartBit, sensor.K,sensor.B,sensor.S);

                Sensor s = new Sensor(sensor.Name, "");
                s.Algoritm = idalgoritm;
                s.Value = value;
                atlantaDataSet.StateRow[] state = sensor.GetStateRows();

                if (state.Length > 0)
                    s.IDSensor = state[0].SensorId;
                else
                    s.IDSensor = -1;

                s.ValueStr = "нет";
                foreach (atlantaDataSet.StateRow sr in state)
                {
                    if ((sr.MaxValue >= value && sr.MinValue < value) || sr.MinValue == value)
                        s.ValueStr = sr.Title;
                }

                _vehicle.Sensors.Add(s);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message + "\n" + ex.StackTrace, "Error in SetSensorRANGEFINDER", MessageBoxButtons.OK,
                   MessageBoxIcon.Error);
            }
        }

        private void SetSensorDriver(IList<Driver> drivers, atlantaDataSet.sensorsRow sensor, int idalgoritm)
        {
            try
            {
                string driverName = Resources.SensorsViewCardNotIdentified;
                string numTelephone = "";

                UInt16 val =
                    Convert.ToUInt16(Calibrate.ulongSector(_vehicle.LastPoint.Sensors, sensor.Length, sensor.StartBit));

                if (val > 0)
                    driverName = string.Format("{0}({1})", Resources.NotPresent, val);

                if (val > 0 && val < Math.Pow(2, sensor.Length) - 1)
                {
                    foreach (Driver driver in drivers)
                    {
                        if (driver.Identifier.HasValue && val == driver.Identifier.Value)
                        {
                            driverName = string.Format("{0}({1})", driver.FullName, val);
                            numTelephone = driver.NumTelephone;
                            break;
                        }
                    }
                }
                else
                {
                    driverName = string.Format("{0}({1})", Resources.NotPresent, val);
                }

                AddSensorToVehicle(sensor, val, driverName, idalgoritm, numTelephone);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message + "\n" + ex.StackTrace, "Error in SetSensorDriver", MessageBoxButtons.OK,
                   MessageBoxIcon.Error);
            }
        }

        private void AddSensorToVehicle(atlantaDataSet.sensorsRow sensor, double value, string driverName,
            int idalgoritm, string numTel)
        {
            try
            {
                Sensor s = new Sensor(sensor.Name, driverName, value.ToString());
                s.NumTelephone = numTel;
                s.Algoritm = idalgoritm;
                _vehicle.Sensors.Add(s);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message + "\n" + ex.StackTrace, "Error in AddSensorToVehicle", MessageBoxButtons.OK,
                   MessageBoxIcon.Error);
            }
        }

        private void AddSensorToVehicle(atlantaDataSet.sensorsRow sensor, double value, string driverName, int algorithm)
        {
            try
            {
                Sensor s = new Sensor(sensor.Name, driverName);
                s.Algoritm = algorithm;
                s.Value = value;
                _vehicle.Sensors.Add(s);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message + "\n" + ex.StackTrace, "Error in AddSensorToVehicle", MessageBoxButtons.OK,
                   MessageBoxIcon.Error);
            }
        }

        private void AddSensorToVehicle(atlantaDataSet.sensorsRow sensor, string value, string driverName, int algorithm)
        {
            try
            {
                Sensor s = new Sensor(sensor.Name, driverName);
                s.Algoritm = algorithm;
                s.ValueStr = value;
                _vehicle.Sensors.Add(s);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message + "\n" + ex.StackTrace, "Error in AddSensorToVehicle", MessageBoxButtons.OK,
                   MessageBoxIcon.Error);
            }
        }
    }
}
