namespace TrackControl.Controls
{
  partial class SensorsView
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this._vehicleInfo = new DevExpress.XtraEditors.GroupControl();
        this._grid = new DevExpress.XtraGrid.GridControl();
        this._view = new DevExpress.XtraGrid.Views.Grid.GridView();
        this._sensorColumn = new DevExpress.XtraGrid.Columns.GridColumn();
        this._valueColumn = new DevExpress.XtraGrid.Columns.GridColumn();
        ((System.ComponentModel.ISupportInitialize)(this._vehicleInfo)).BeginInit();
        this._vehicleInfo.SuspendLayout();
        ((System.ComponentModel.ISupportInitialize)(this._grid)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this._view)).BeginInit();
        this.SuspendLayout();
        // 
        // _vehicleInfo
        // 
        this._vehicleInfo.Controls.Add(this._grid);
        this._vehicleInfo.Dock = System.Windows.Forms.DockStyle.Fill;
        this._vehicleInfo.Location = new System.Drawing.Point(0, 0);
        this._vehicleInfo.Margin = new System.Windows.Forms.Padding(20);
        this._vehicleInfo.Name = "_vehicleInfo";
        this._vehicleInfo.Size = new System.Drawing.Size(352, 347);
        this._vehicleInfo.TabIndex = 0;
        // 
        // _grid
        // 
        this._grid.Dock = System.Windows.Forms.DockStyle.Fill;
        this._grid.Location = new System.Drawing.Point(2, 22);
        this._grid.MainView = this._view;
        this._grid.Margin = new System.Windows.Forms.Padding(0);
        this._grid.Name = "_grid";
        this._grid.Size = new System.Drawing.Size(348, 323);
        this._grid.TabIndex = 0;
        this._grid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this._view});
        // 
        // _view
        // 
        this._view.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this._sensorColumn,
            this._valueColumn});
        this._view.GridControl = this._grid;
        this._view.Name = "_view";
        this._view.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
        this._view.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
        this._view.OptionsBehavior.Editable = false;
        this._view.OptionsCustomization.AllowGroup = false;
        this._view.OptionsMenu.EnableColumnMenu = false;
        this._view.OptionsMenu.EnableFooterMenu = false;
        this._view.OptionsMenu.EnableGroupPanelMenu = false;
        this._view.OptionsSelection.UseIndicatorForSelection = false;
        this._view.OptionsView.ShowDetailButtons = false;
        this._view.OptionsView.ShowGroupPanel = false;
        // 
        // _sensorColumn
        // 
        this._sensorColumn.Caption = "������";
        this._sensorColumn.FieldName = "Name";
        this._sensorColumn.Name = "_sensorColumn";
        this._sensorColumn.OptionsColumn.AllowEdit = false;
        this._sensorColumn.OptionsColumn.AllowFocus = false;
        this._sensorColumn.OptionsColumn.ReadOnly = true;
        this._sensorColumn.Visible = true;
        this._sensorColumn.VisibleIndex = 0;
        // 
        // _valueColumn
        // 
        this._valueColumn.Caption = "���������";
        this._valueColumn.FieldName = "SensorValue";
        this._valueColumn.Name = "_valueColumn";
        this._valueColumn.OptionsColumn.AllowEdit = false;
        this._valueColumn.OptionsColumn.AllowFocus = false;
        this._valueColumn.OptionsColumn.ReadOnly = true;
        this._valueColumn.Visible = true;
        this._valueColumn.VisibleIndex = 1;
        // 
        // SensorsView
        // 
        this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        this.Controls.Add(this._vehicleInfo);
        this.Name = "SensorsView";
        this.Size = new System.Drawing.Size(352, 347);
        ((System.ComponentModel.ISupportInitialize)(this._vehicleInfo)).EndInit();
        this._vehicleInfo.ResumeLayout(false);
        ((System.ComponentModel.ISupportInitialize)(this._grid)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this._view)).EndInit();
        this.ResumeLayout(false);

    }

    #endregion

    private DevExpress.XtraEditors.GroupControl _vehicleInfo;
    private DevExpress.XtraGrid.GridControl _grid;
    private DevExpress.XtraGrid.Views.Grid.GridView _view;
    private DevExpress.XtraGrid.Columns.GridColumn _sensorColumn;
    private DevExpress.XtraGrid.Columns.GridColumn _valueColumn;

  }
}
