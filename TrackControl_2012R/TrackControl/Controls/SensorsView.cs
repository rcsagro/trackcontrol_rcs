using System;
using System.Collections;
using System.Collections.Generic;
using BaseReports.Procedure.Calibration;
using DevExpress.XtraEditors;
using LocalCache;
using TrackControl.Online;
using TrackControl.Reports;
using TrackControl.Vehicles;
using TrackControl.Properties;
using TrackControl.General;

namespace TrackControl.Controls
{
    public partial class SensorsView : XtraUserControl
    {
        VehiclesModel _model;

        public SensorsView()
        {
            InitializeComponent();
            init();
        }

        public void AssignVehicleModel( VehiclesModel model )
        {
            if( null == model )
                throw new ArgumentNullException( "model" );

            _model = model;
        }

        public void ShowSensorsState( OnlineVehicle item )
        {
            if( null == _model )
                throw new Exception( Resources.Ex_SensorsViewVehicleModelNotAssigned );

            IList<Driver> drivers = _model.Drivers;

            if( item != null ) 
                item.Sensors.Clear();

            _valueColumn.Caption = Resources.State;
            _grid.DataSource = null;
            _grid.RefreshDataSource();

            if( null != item )
            {
                _vehicleInfo.Text = String.Format( "{0} {1} {2}", item.RegNumber, item.CarMaker, item.CarModel );

                if( item.LastPoint != null )
                {
                    VehicleSensors vehSensors = new VehicleSensors( item );
                    vehSensors.AssignVehicleModel( _model );
                    vehSensors.SetSensorValues();
                }

                _grid.DataSource = item.Sensors;
            }
            else
            {
                _vehicleInfo.Text = Resources.VehicleNotSelected;
            }
        }

        public void ShowSensorsInfo( ReportVehicle rv )
        {
            _valueColumn.Caption = Resources.SensorsViewUnitOfMeasure;
            _grid.DataSource = null;

            if( null != rv )
            {
                _vehicleInfo.Text = String.Format( "{0} {1} {2}", rv.RegNumber, rv.CarMaker, rv.CarModel );
                List<SensorItem> items = new List<SensorItem>();
                atlantaDataSet.sensorsRow[] sensors = ( atlantaDataSet.sensorsRow[] )AppModel.Instance.DataSet.sensors.Select( String.Format( "mobitel_id = {0}", rv.Mobitel.Id ) );

                if( sensors.Length > 0 )
                {
                    for( int i = 0; i < sensors.Length; i++ )
                    {
                        atlantaDataSet.sensorsRow sensor = sensors[i];
                        items.Add( new SensorItem( sensor.Name, sensor.NameUnit ) );
                    }
                }

                _grid.DataSource = items;
            }
            else
            {
                _vehicleInfo.Text = Resources.VehicleNotSelected;
            }
        }

        void init()
        {
            _sensorColumn.Caption = Resources.Sensor;
            _valueColumn.Caption = Resources.State;
        }

        #region --    Private Classes    --
        private class SensorItem
        {
            public string Name
            {
                get { return _name; }
            }
            string _name;

            public string SensorValue
            {
                get { return _value; }
            }
            string _value;

            public SensorItem( string name, string value )
            {
                _name = name;
                _value = value;
            }
        }
        #endregion
    }
}
