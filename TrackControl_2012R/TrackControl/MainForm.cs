﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Threading;
using System.Windows.Forms;
using DevExpress.LookAndFeel;
using DevExpress.Skins;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using TrackControl.General;
using TrackControl.General.Services;
using TrackControl.GMap;
using TrackControl.MySqlDal;
using TrackControl.Properties;
using TrackControl.GMap.MapProviders;
using TrackControl.SrvRepLib.COM;

//using TrackControl.GMap.Core;
//using TrackControl.SrvRepLib.COM;



namespace TrackControl
{
    /// <summary>
    /// Главная форма приложения
    /// </summary>
    public partial class MainForm : XtraForm, IMainView
    {
        private AppController _controller = AppController.Instance;

        #region --   Члены формы   --

        /// <summary>
        /// Создает и инициализирует новый экземпляр класса TrackControl.MainForm
        /// </summary>
        public MainForm()
        {
            InitializeComponent();
            //setLanguage(); 
            init();

            SizeChanged += MainForm_SizeChanged;

#if !DEBUG
      _testButtonItem.Visibility = BarItemVisibility.Never;
      //_exportToExcelButtonIteml.Visibility = BarItemVisibility.Never;
      //_motordepotItem.Visibility = BarItemVisibility.Never;
#endif
        }

        void MainForm_SizeChanged( object sender, EventArgs e )
        {
            GoogleMapControl.mainLocation = new Point( Location.X, Location.Y );
        }

        /// <summary>
        /// Отрабатывает при загрузке формы
        /// </summary>
        private void this_Load(object sender, EventArgs e)
        {
            initSkins();
            setLanguageMarker();
            _controller.Start(this);
            _controller.RefreshLastTimes += GetLastDataTime;
        }

        private bool _confirmExit;

        /// <summary>
        /// Отрабатывает при закрытии формы
        /// </summary>
        private void this_FormClosing(object sender, FormClosingEventArgs e)
        {
            _controller.RefreshLastTimes -= GetLastDataTime;

            if (Form_Utils.IsLoaded("TrackControl.ExitForm"))
            {
                e.Cancel = true;
                return;
            }
            ExitForm exitForm = new ExitForm();
            exitForm.ConfirmExit += SewtConfirmExit;
            exitForm.Show();
            do
            {
                Application.DoEvents();
            } while (Form_Utils.IsLoaded("TrackControl.ExitForm"));

            if (!_confirmExit)
            {
                e.Cancel = true;
                return;
            }

            try
            {
                _controller.Finish();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message + ex.StackTrace, "Error finishMainForm", MessageBoxButtons.OK);
            }
        }

        private void SewtConfirmExit(bool confirmExit)
        {
            _confirmExit = confirmExit;
        }

        private bool _isMinimized;

        private void this_SizeChanged(object sender, EventArgs e)
        {
            if (_isMinimized)
            {
                _controller.RestoreLayout();
            }
            _isMinimized = FormWindowState.Minimized == WindowState;
        }

        #endregion

        /// <summary>
        /// Состояние кнопки уведомлений
        /// </summary>
        public Boolean NotificationActivated
        {
            set { _notificationItem.Checked = value; }
        }

        /// <summary>
        /// Видимость пункта меню "Модуль AGRO"
        /// </summary>
        public Boolean AgroEnabled
        {
            set
            {
                if (value)
                {
                    _modulsItem.Visibility = BarItemVisibility.Always;
                    _agroItem.Visibility = BarItemVisibility.Always;
                }

            }
        }

        /// <summary>
        /// Видимость пункта меню "Модуль Маршруты"
        /// </summary>
        public bool RoutesEnabled
        {
            set
            {
                if (value)
                {
                    _modulsItem.Visibility = BarItemVisibility.Always;
                    _routesItem.Visibility = BarItemVisibility.Always;
                }
            }
        }

        /// <summary>
        /// Видимость пункта меню "Модуль Мотопредприятие"
        /// </summary>
        public bool MotoDeportEnabled
        {
            set
            {
                if (value)
                {
                    _modulsItem.Visibility = BarItemVisibility.Always;
                    _motordepotItem.Visibility = BarItemVisibility.Always;
                }
                else
                {
                    _motordepotItem.Visibility = BarItemVisibility.Never;
                }
            }
        }


        public PanelControl MainPanel
        {
            get { return _mainPanel; }
        }

        public void SetView(Control view)
        {
            foreach (Control child in _mainPanel.Controls)
                child.Parent = null;

            view.Parent = _mainPanel;
        }

        #region --   Обработчики  "Главного меню"    --

        #region --   Меню "Режим"   --

        /// <summary>
        /// Обрабатывает клик на кнопке "Отчеты"
        /// </summary>
        private void reportsItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            _controller.ShowReports();
        }

        /// <summary>
        /// Обрабатывает клик на кнопке "Слежение"
        /// </summary>
        private void monitoringItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            _controller.ShowOnline();
        }

        /// <summary>
        /// Обрабатывает клик на кнопке "Выход"
        /// </summary>
        private void exitItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            Close();
        }

        #endregion

        /// <summary>
        /// Обрабатывает клик на кнопке "Уведомления"
        /// </summary>
        private void notificationItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            _controller.SwitchNotifier();
        }

        #region --   Меню "Модули"   --

        /// <summary>
        /// Обрабатывает клик на кнопке "Модуль AGRO"
        /// </summary>
        private void agroItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            AppController.ShowAgro();
        }


        /// <summary>
        /// Отрабатывает клик на кнопке "Модуль Маршруты"
        /// </summary>
        private void routesItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            AppController.ShowRoutes();
        }

        private void _motordepotItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            AppController.ShowMotorDepot();
        }

        #endregion

        #region --   Меню "Сервис"   --

        /// <summary>
        /// Обрабатывает клик на кнопке "Справочники"
        /// </summary>
        private void referenceItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            _controller.ShowDictionaries();
        }

        private void vehiclesEditorBtn_ItemClick(object sender, ItemClickEventArgs e)
        {
            _controller.ShowVehiclesEditor();
        }

        private void driversEditorBtn_ItemClick(object sender, ItemClickEventArgs e)
        {
            _controller.ShowDriversEditor();
        }

        private void bbiSetupNotice_ItemClick(object sender, ItemClickEventArgs e)
        {
            _controller.ShowNotices();
        }

        /// <summary>
        /// Обрабатывает клик на кнопке "Контрольные зоны"
        /// </summary>
        private void zonesEditorItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            ShowZonesEditor();
        }

        internal void ShowZonesEditor()
        {
            if (null == _controller)
            {
                throw new Exception("AppController wasn't assigned");
            }

            _controller.ShowZonesEditor();
        }

        #region --   Темы   --

        /// <summary>
        /// Инициализация тем
        /// </summary>
        private void initSkins()
        {
            string name = UserLookAndFeel.Default.SkinName;
            foreach (SkinContainer cnt in SkinManager.Default.Skins)
            {
                BarButtonItem item = new BarButtonItem(_barManager, cnt.SkinName);
                item.Name = String.Format("bi{0}", cnt.SkinName);
                item.Id = _barManager.GetNewItemId();
                _themesItem.AddItem(item);
                item.ItemClick += OnSkinClick;

                if (name == cnt.SkinName)
                    item.Glyph = Shared.Right;
            }
        }

        private void OnSkinClick(object sender, ItemClickEventArgs e)
        {
            if( e.Item.Caption == UserLookAndFeel.Default.SkinName )
                return;

            foreach( BarButtonItemLink link in _themesItem.ItemLinks )
            {
                link.Item.Glyph = null;
            }

            BarItem item = e.Item;
            item.Glyph = Shared.Right;
            UserLookAndFeel.Default.SetSkinStyle( item.Caption );
            SkinService.Save( item.Caption );
        }

        #endregion

        #region --   Многоязычность   --

        /// <summary>
        /// Установка в меню метки языка интерфейса
        /// </summary>
        private void setLanguageMarker()
        {
            //string calture = CultureInfo.CurrentCulture.Name;
            string calture = Thread.CurrentThread.CurrentUICulture.Name;
            GMapProvider.LanguageStr = LayoutService.GetLocalization();
            foreach (BarButtonItemLink link in _languageItem.ItemLinks)
            {
                if (link.Item.Tag != null && link.Item.Tag.ToString() == calture)
                {
                    link.Item.Glyph = Shared.Right;
                    return;
                }
            }
        }

        private void OnLangClick(object sender, ItemClickEventArgs e)
        {
            if (e.Item.Tag != null)
            {
                if (e.Item.Tag.ToString() != CultureInfo.CurrentCulture.Name)
                {
                    Thread.CurrentThread.CurrentUICulture = new CultureInfo(e.Item.Tag.ToString());
                    Thread.CurrentThread.CurrentCulture = new CultureInfo(e.Item.Tag.ToString());
                    GMapProvider.LanguageStr = LayoutService.GetLocalization();
                    LanguageService.Save(e.Item.Tag.ToString());

                    foreach (BarButtonItemLink link in _languageItem.ItemLinks)
                    {
                        link.Item.Glyph = null;
                    }

                    e.Item.Glyph = Shared.Right;
                    init();
                    _controller.ChangeLanguage();
                }
            }
        }

        #endregion


        /// <summary>
        /// Обрабатывает клик на кнопке "Настройки"
        /// </summary>
        private void settingsItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            _controller.ShowSettings();
        }

        #endregion

        #region --   Меню "Справка"   --

        /// <summary>
        /// Обрабатывает клик на кнопке "О программе"
        /// </summary>
        private void _aboutItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            _controller.ShowAbouts();
        }

        /// <summary>
        /// Открывает руководство пользователя
        /// </summary>
        private void bbiUserGuide_ItemClick(object sender, ItemClickEventArgs e)
        {
            _controller.ShowUserGuide();
        }

        /// <summary>
        /// Обрабатывает клик на кнопке "Интеграция с 1С"
        /// </summary>
        private void _1cIntegrationItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            Process.Start("http://autovision.com.ua/blogcategory/sistema-teletrack/integraciya-s-1s");
        }

        #endregion

        /// <summary>
        /// Обрабатывает клик на кнопке "Тест"
        /// </summary>
        private void testButtonItem_ItemClick(object sender, ItemClickEventArgs e)
        {
//          GpsDataProvider.WritreSensorsData(109, new DateTime(2016, 09, 19, 7, 0, 0), new DateTime(2016, 09, 20, 7, 0, 0));
            GpsDataProvider.WritreSensorsData(100, new DateTime(2016, 08, 24, 7, 0, 0), new DateTime(2016, 08, 25, 7, 0, 0));
            return;
            //ZonesController zc = new ZonesController();
            //zc.TestSpatialLite(_controller._appModel.ZonesManager); 

            //int value = BitConverter.ToInt32(new byte[] { 0x00, 0x00, 0x00, 0x80 }, 0);
            //if (value < 0) value = (Int32)((Int64)value * -1);
            //int newVal = Math.Abs(value);
            //00100009
            ReporterCOM rCom = new ReporterCOM();
            rCom.Init("server = teletrack;database = vesnyane_agro;user id = user;password = user1");

            //rCom.LoadZonez();
            DateTime dt = new DateTime(2016, 07, 26);
            var fl = rCom.GetFuelLevel("B9DC", dt, dt.AddDays(1));
            var km = rCom.GetKilometrage("BC47", dt, dt.AddDays(1));
            var tmm = rCom.GetTotalMotionTime("BC47", dt, dt.AddDays(1));
            var tms = rCom.GetTotalStopsTime("BC47", dt, dt.AddDays(1));
            //var crossZones = rCom.GetCrossingZonesInfo("3CEA", dt, dt.AddDays(1), 1,1);
            //for (int i = 0; i < crossZones.GetUpperBound(0); i++)
            //{
            //    string print = string.Format("{0} {1} {2} {3} {4} {5}", crossZones[i, 0], crossZones[i, 1], crossZones[i, 2], crossZones[i, 3], crossZones[i, 4], crossZones[i, 5]);
            //    Console.WriteLine(print);
            //}
        }

        /// <summary>
        /// Обрабатывает клик на кнопке "Экспорт в Excel"
        /// Отображает форму экспорта данных в Excel по выбраному ТТ для режиме в debug=mode
        /// </summary>
        private void exportToExcelButtonIteml_ItemClick(object sender, ItemClickEventArgs e)
        {
            //AppController.ShowExportToExcel();
        }

        #endregion

        private void init()
        {
            Text = String.Format("TrackControl {0} – Server: {1}, Type DB: {2}, DB: {3}, {4}: {5}",
                Application.ProductVersion,
                AppModel.Instance.DbCommon.ServerName,
                AppModel.Instance.DbCommon.TypeDataBase,
                AppModel.Instance.DbCommon.DbName,
                Resources.User,
                UserBaseCurrent.Instance.Name);

            _fileItem.Caption = Resources.MainMenuMode;
            _reportsItem.Caption = Resources.Reports;
            _monitoringItem.Caption = Resources.MainMenuModeMonitoring;
            _exitItem.Caption = Resources.MainMenuModeExit;
            _notificationItem.Caption = Resources.MainMenuNotifications;
            _modulsItem.Caption = Resources.MainMenuModules;
            _agroItem.Caption = Resources.MainMenuModulesAgro;
            _routesItem.Caption = Resources.MainMenuModulesRoutes;
            _toolsItem.Caption = Resources.MainMenuServices;
            _referenceItem.Caption = Resources.Dictionaries;
            _themesItem.Caption = Resources.MainMenuServicesThemes;

            _vehiclesEditorBtn.Caption = Resources.MainMenuServicesVehicleSettings;
            _driversEditorBtn.Caption = Resources.MainMenuServicesDriverSettings;
            _zonesEditorItem.Caption = Resources.MainMenuServicesZonesEditor;
            _settingsItem.Caption = Resources.MainMenuServicesSettings;
            bbiSetupNotice.Caption = Resources.MainMenuNoticesSettings;
            _vehiclesEditorBtn.Enabled = UserBaseCurrent.Instance.Admin;
            _driversEditorBtn.Enabled = _vehiclesEditorBtn.Enabled;
            _zonesEditorItem.Enabled = _vehiclesEditorBtn.Enabled;
            _settingsItem.Enabled = _vehiclesEditorBtn.Enabled;
            bbiSetupNotice.Enabled = _vehiclesEditorBtn.Enabled;

            _helpItem.Caption = Resources.MainMenuHelp;
            _1cIntegrationItem.Caption = Resources.MainMenuHelp1S;
            _aboutItem.Caption = Resources.MainMenuHelpAbout;
            _languageItem.Caption = Resources.Language;
            _languageRU.Caption = Resources.Russian;
            _languagePT.Caption = Resources.Portuguese;
            _languageTH.Caption = Resources.Thai;
            _languageUK.Caption = Resources.Ukrainian;

            bbiUserGuide.Caption = Resources.UserGuideText;
        }

        private void GetLastDataTime()
        {
            DateTime lastData = GpsDataProvider.GetLastDataTime();
            Action<DateTime> action = DoChangeLastDataTime;

            if (InvokeRequired)
            {
                Invoke(action, lastData);
            }
            else
            {
                action(lastData);
            }
        }

        private void DoChangeLastDataTime(DateTime lastData)
        {
            bsiLastData.Caption = string.Format("{1}: {0}", lastData, Resources.NewestData);
            icStatus.EditValue =  StaticMethods.GetStatusColor(lastData);
        }

        private void MainForm_LocationChanged( object sender, EventArgs e )
        {
            GoogleMapControl.mainLocation = new Point( Location.X, Location.Y );
        }
    }
}