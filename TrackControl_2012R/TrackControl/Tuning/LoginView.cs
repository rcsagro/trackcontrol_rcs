using System;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using TrackControl.Properties;

namespace TrackControl.Tuning
{
  public partial class LoginView : XtraUserControl
  {
    public event AuthorizationRequested AuthRequsted;

    public LoginView()
    {
      InitializeComponent();
      init();
    }

    public void AuthRefused()
    {
      _errorLbl.Text = Resources.Tuning_AccessDenied;
      ActiveControl = _errorLbl;
    }

    public void SetFocus()
    {
      _passwordEdit.Focus();
    }

    void init()
    {
      _panel.Text = Resources.Auth;
      _loginLbl.Text = Resources.Login;
      _passwordLbl.Text = Resources.Password;
      _headerLbl.Text = Resources.Tuning_Login_Text;
      _loginBtn.Text = Resources.Entrence;
    }

    void loginBtn_Click(object sender, EventArgs e)
    {
      checkInput();
    }

    void edit_Enter(object sender, EventArgs e)
    {
      _errorLbl.Text = String.Empty;
    }

    void loginEdit_KeyPress(object sender, KeyPressEventArgs e)
    {
      if (e.KeyChar.ToString() == "\r")
        _passwordEdit.Focus();
    }

    void passwordEdit_KeyPress(object sender, KeyPressEventArgs e)
    {
      if (e.KeyChar.ToString() == "\r")
        checkInput();
    }

    void checkInput()
    {
      if (String.IsNullOrEmpty(_passwordEdit.Text))
        AuthRefused();
      else
        onAuthRequested(_loginEdit.Text, _passwordEdit.Text);
    }

    void onAuthRequested(string login, string password)
    {
      AuthorizationRequested handler = AuthRequsted;
      if (null != handler)
        handler(login, password);
    }
  }
}
