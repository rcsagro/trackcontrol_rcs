using System;

namespace TrackControl.Tuning
{
  public delegate void TuningLeavedOut();
  public delegate void AuthorizationRequested(string login, string password);
}
