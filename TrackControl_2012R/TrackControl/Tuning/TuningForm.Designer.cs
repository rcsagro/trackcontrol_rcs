namespace TrackControl.Tuning
{
  partial class TuningForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.components = new System.ComponentModel.Container();
        System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager( typeof( TuningForm ) );
        this._barManager = new DevExpress.XtraBars.BarManager( this.components );
        this._tools = new DevExpress.XtraBars.Bar();
        this._switchBtn = new DevExpress.XtraBars.BarButtonItem();
        this._mainBtn = new DevExpress.XtraBars.BarButtonItem();
        this._mainPopup = new DevExpress.XtraBars.PopupMenu( this.components );
        this._captionLabel = new DevExpress.XtraBars.BarStaticItem();
        this._returnBtn = new DevExpress.XtraBars.BarButtonItem();
        this._logoffBtn = new DevExpress.XtraBars.BarButtonItem();
        this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
        this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
        this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
        this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
        this._dockManager = new DevExpress.XtraBars.Docking.DockManager( this.components );
        this._navigatorPanel = new DevExpress.XtraBars.Docking.DockPanel();
        this.dockPanel1_Container = new DevExpress.XtraBars.Docking.ControlContainer();
        this._navBar = new DevExpress.XtraNavBar.NavBarControl();
        this._commonGroup = new DevExpress.XtraNavBar.NavBarGroup();
        this._modesGroup = new DevExpress.XtraNavBar.NavBarGroup();
        this._mainPanel = new DevExpress.XtraEditors.PanelControl();
        ( ( System.ComponentModel.ISupportInitialize )( this._barManager ) ).BeginInit();
        ( ( System.ComponentModel.ISupportInitialize )( this._mainPopup ) ).BeginInit();
        ( ( System.ComponentModel.ISupportInitialize )( this._dockManager ) ).BeginInit();
        this._navigatorPanel.SuspendLayout();
        this.dockPanel1_Container.SuspendLayout();
        ( ( System.ComponentModel.ISupportInitialize )( this._navBar ) ).BeginInit();
        ( ( System.ComponentModel.ISupportInitialize )( this._mainPanel ) ).BeginInit();
        this.SuspendLayout();
        // 
        // _barManager
        // 
        this._barManager.AllowCustomization = false;
        this._barManager.AllowMoveBarOnToolbar = false;
        this._barManager.AllowQuickCustomization = false;
        this._barManager.Bars.AddRange( new DevExpress.XtraBars.Bar[] {
            this._tools} );
        this._barManager.DockControls.Add( this.barDockControlTop );
        this._barManager.DockControls.Add( this.barDockControlBottom );
        this._barManager.DockControls.Add( this.barDockControlLeft );
        this._barManager.DockControls.Add( this.barDockControlRight );
        this._barManager.DockManager = this._dockManager;
        this._barManager.Form = this;
        this._barManager.Items.AddRange( new DevExpress.XtraBars.BarItem[] {
            this._captionLabel,
            this._logoffBtn,
            this._returnBtn,
            this._mainBtn,
            this._switchBtn} );
        this._barManager.MaxItemId = 5;
        // 
        // _tools
        // 
        this._tools.BarName = "Tools";
        this._tools.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Top;
        this._tools.DockCol = 0;
        this._tools.DockRow = 0;
        this._tools.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
        this._tools.LinksPersistInfo.AddRange( new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this._switchBtn),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.None, this._mainBtn, "", true, false, true, 0),
            new DevExpress.XtraBars.LinkPersistInfo(this._captionLabel),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.None, this._returnBtn, "", false, false, true, 0),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.None, this._logoffBtn, "", true, false, true, 0)} );
        this._tools.OptionsBar.AllowQuickCustomization = false;
        this._tools.OptionsBar.DisableClose = true;
        this._tools.OptionsBar.DisableCustomization = true;
        this._tools.OptionsBar.DrawDragBorder = false;
        this._tools.OptionsBar.UseWholeRow = true;
        this._tools.Text = "Tools";
        // 
        // _switchBtn
        // 
        this._switchBtn.Caption = "������������� ������";
        this._switchBtn.Glyph = ( ( System.Drawing.Image )( resources.GetObject( "_switchBtn.Glyph" ) ) );
        this._switchBtn.Hint = "�������� ��������� ������������� ������";
        this._switchBtn.Id = 4;
        this._switchBtn.Name = "_switchBtn";
        this._switchBtn.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler( this.switchBtn_ItemClick );
        // 
        // _mainBtn
        // 
        this._mainBtn.ActAsDropDown = true;
        this._mainBtn.Appearance.Font = new System.Drawing.Font( "Tahoma", 8.25F, System.Drawing.FontStyle.Bold );
        this._mainBtn.Appearance.Options.UseFont = true;
        this._mainBtn.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
        this._mainBtn.Caption = "���������:";
        this._mainBtn.DropDownControl = this._mainPopup;
        this._mainBtn.Glyph = ( ( System.Drawing.Image )( resources.GetObject( "_mainBtn.Glyph" ) ) );
        this._mainBtn.Id = 3;
        this._mainBtn.Name = "_mainBtn";
        this._mainBtn.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
        // 
        // _mainPopup
        // 
        this._mainPopup.Manager = this._barManager;
        this._mainPopup.Name = "_mainPopup";
        // 
        // _captionLabel
        // 
        this._captionLabel.Border = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
        this._captionLabel.Id = 0;
        this._captionLabel.Name = "_captionLabel";
        this._captionLabel.TextAlignment = System.Drawing.StringAlignment.Near;
        // 
        // _returnBtn
        // 
        this._returnBtn.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
        this._returnBtn.Caption = "���������";
        this._returnBtn.Glyph = ( ( System.Drawing.Image )( resources.GetObject( "_returnBtn.Glyph" ) ) );
        this._returnBtn.Id = 2;
        this._returnBtn.Name = "_returnBtn";
        this._returnBtn.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
        this._returnBtn.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler( this.returnBtn_ItemClick );
        // 
        // _logoffBtn
        // 
        this._logoffBtn.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
        this._logoffBtn.Caption = "�����";
        this._logoffBtn.Glyph = ( ( System.Drawing.Image )( resources.GetObject( "_logoffBtn.Glyph" ) ) );
        this._logoffBtn.Id = 1;
        this._logoffBtn.Name = "_logoffBtn";
        this._logoffBtn.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
        this._logoffBtn.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
        this._logoffBtn.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler( this._logoffBtn_ItemClick );
        // 
        // _dockManager
        // 
        this._dockManager.DockingOptions.FloatOnDblClick = false;
        this._dockManager.DockingOptions.ShowCaptionImage = true;
        this._dockManager.DockingOptions.ShowCloseButton = false;
        this._dockManager.DockingOptions.ShowMaximizeButton = false;
        this._dockManager.Form = this;
        this._dockManager.RootPanels.AddRange( new DevExpress.XtraBars.Docking.DockPanel[] {
            this._navigatorPanel} );
        this._dockManager.TopZIndexControls.AddRange( new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "DevExpress.XtraBars.StandaloneBarDockControl",
            "System.Windows.Forms.StatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl"} );
        // 
        // _navigatorPanel
        // 
        this._navigatorPanel.Controls.Add( this.dockPanel1_Container );
        this._navigatorPanel.Dock = DevExpress.XtraBars.Docking.DockingStyle.Left;
        this._navigatorPanel.ID = new System.Guid( "7db3c780-71da-4832-82c8-addd384241df" );
        this._navigatorPanel.Location = new System.Drawing.Point( 0, 26 );
        this._navigatorPanel.Name = "_navigatorPanel";
        this._navigatorPanel.Options.AllowDockBottom = false;
        this._navigatorPanel.Options.AllowDockFill = false;
        this._navigatorPanel.Options.AllowDockRight = false;
        this._navigatorPanel.Options.AllowDockTop = false;
        this._navigatorPanel.Options.AllowFloating = false;
        this._navigatorPanel.Options.FloatOnDblClick = false;
        this._navigatorPanel.Options.ShowAutoHideButton = false;
        this._navigatorPanel.Options.ShowCloseButton = false;
        this._navigatorPanel.Options.ShowMaximizeButton = false;
        this._navigatorPanel.OriginalSize = new System.Drawing.Size( 181, 549 );
        this._navigatorPanel.Size = new System.Drawing.Size( 181, 549 );
        this._navigatorPanel.TabText = "�������";
        this._navigatorPanel.Text = "�������";
        // 
        // dockPanel1_Container
        // 
        this.dockPanel1_Container.Controls.Add( this._navBar );
        this.dockPanel1_Container.Location = new System.Drawing.Point( 3, 25 );
        this.dockPanel1_Container.Name = "dockPanel1_Container";
        this.dockPanel1_Container.Size = new System.Drawing.Size( 175, 521 );
        this.dockPanel1_Container.TabIndex = 0;
        // 
        // _navBar
        // 
        this._navBar.ActiveGroup = this._commonGroup;
        this._navBar.ContentButtonHint = null;
        this._navBar.Dock = System.Windows.Forms.DockStyle.Fill;
        this._navBar.Groups.AddRange( new DevExpress.XtraNavBar.NavBarGroup[] {
            this._commonGroup,
            this._modesGroup} );
        this._navBar.Location = new System.Drawing.Point( 0, 0 );
        this._navBar.Margin = new System.Windows.Forms.Padding( 0 );
        this._navBar.Name = "_navBar";
        this._navBar.OptionsNavPane.ExpandedWidth = 227;
        this._navBar.Size = new System.Drawing.Size( 175, 521 );
        this._navBar.TabIndex = 0;
        this._navBar.Text = "navBarControl1";
        // 
        // _commonGroup
        // 
        this._commonGroup.Caption = "�����";
        this._commonGroup.Expanded = true;
        this._commonGroup.Name = "_commonGroup";
        this._commonGroup.SmallImage = ( ( System.Drawing.Image )( resources.GetObject( "_commonGroup.SmallImage" ) ) );
        // 
        // _modesGroup
        // 
        this._modesGroup.Caption = "�����������";
        this._modesGroup.Name = "_modesGroup";
        this._modesGroup.Visible = false;
        // 
        // _mainPanel
        // 
        this._mainPanel.Dock = System.Windows.Forms.DockStyle.Fill;
        this._mainPanel.Location = new System.Drawing.Point( 181, 26 );
        this._mainPanel.Margin = new System.Windows.Forms.Padding( 0 );
        this._mainPanel.Name = "_mainPanel";
        this._mainPanel.Size = new System.Drawing.Size( 589, 549 );
        this._mainPanel.TabIndex = 4;
        // 
        // TuningForm
        // 
        this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
        this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        this.ClientSize = new System.Drawing.Size( 770, 575 );
        this.ControlBox = false;
        this.Controls.Add( this._mainPanel );
        this.Controls.Add( this._navigatorPanel );
        this.Controls.Add( this.barDockControlLeft );
        this.Controls.Add( this.barDockControlRight );
        this.Controls.Add( this.barDockControlBottom );
        this.Controls.Add( this.barDockControlTop );
        this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
        this.Name = "TuningForm";
        this.ShowIcon = false;
        ( ( System.ComponentModel.ISupportInitialize )( this._barManager ) ).EndInit();
        ( ( System.ComponentModel.ISupportInitialize )( this._mainPopup ) ).EndInit();
        ( ( System.ComponentModel.ISupportInitialize )( this._dockManager ) ).EndInit();
        this._navigatorPanel.ResumeLayout( false );
        this.dockPanel1_Container.ResumeLayout( false );
        ( ( System.ComponentModel.ISupportInitialize )( this._navBar ) ).EndInit();
        ( ( System.ComponentModel.ISupportInitialize )( this._mainPanel ) ).EndInit();
        this.ResumeLayout( false );

    }

    #endregion

    private DevExpress.XtraBars.BarManager _barManager;
    private DevExpress.XtraBars.Bar _tools;
    private DevExpress.XtraBars.BarDockControl barDockControlTop;
    private DevExpress.XtraBars.BarDockControl barDockControlBottom;
    private DevExpress.XtraBars.BarDockControl barDockControlLeft;
    private DevExpress.XtraBars.BarDockControl barDockControlRight;
    private DevExpress.XtraBars.BarStaticItem _captionLabel;
    private DevExpress.XtraBars.BarButtonItem _logoffBtn;
    private DevExpress.XtraBars.Docking.DockManager _dockManager;
    private DevExpress.XtraBars.Docking.DockPanel _navigatorPanel;
    private DevExpress.XtraBars.Docking.ControlContainer dockPanel1_Container;
    private DevExpress.XtraEditors.PanelControl _mainPanel;
    private DevExpress.XtraNavBar.NavBarControl _navBar;
    private DevExpress.XtraNavBar.NavBarGroup _commonGroup;
    private DevExpress.XtraNavBar.NavBarGroup _modesGroup;
    private DevExpress.XtraBars.BarButtonItem _returnBtn;
    private DevExpress.XtraBars.BarButtonItem _mainBtn;
    private DevExpress.XtraBars.PopupMenu _mainPopup;
    private DevExpress.XtraBars.BarButtonItem _switchBtn;
  }
}