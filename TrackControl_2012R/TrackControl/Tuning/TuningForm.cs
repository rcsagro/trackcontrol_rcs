using System;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraBars;
using DevExpress.XtraBars.Docking;
using TrackControl.General;
using TrackControl.General.Services;
using DevExpress.XtraNavBar;
using TrackControl.Properties;

namespace TrackControl.Tuning
{
  /// <summary>
  /// ������� ����� ��������
  /// </summary>
  public partial class TuningForm : XtraForm
  {
    LoginView _loginView;

    public event VoidHandler AccessGranted;
    public event VoidHandler AccessDenied;
    public event VoidHandler SwitchClicked;
    public event Action<ITuning> LinkClicked;
    public event VoidHandler ReturnClicked;

    public TuningForm()
    {
      InitializeComponent();
      init();
    }

    public void AddCommonItem(ITuning tuning)
    {
      NavBarItem item = new NavBarItem(tuning.Caption);
      item.SmallImage = tuning.Icon;
      item.Tag = tuning;
      item.LinkClicked += item_LinkClicked;
      _commonGroup.ItemLinks.Add(item);
      _navBar.Items.Add(item);

      BarButtonItem barItem = new BarButtonItem();
      barItem.Caption = tuning.Caption;
      barItem.Glyph = tuning.Icon;
      barItem.Tag = tuning;
      barItem.ItemClick += barItem_ItemClick;
      _mainPopup.ItemLinks.Add(barItem);
    }

    public void SetTuning(ITuning tuning)
    {
      _captionLabel.Caption = tuning.Caption;
      _mainBtn.Glyph = tuning.Icon;
      setView(tuning.View);
    }

    public void ShowNavigation()
    {
      _navigatorPanel.Show();
    }

    public void HideNavigation()
    {
      _navigatorPanel.Hide();
    }

    public void DenyAccess()
    {
      _captionLabel.Caption = Resources.Auth;
      _logoffBtn.Visibility = BarItemVisibility.Never;
      _loginView = new LoginView();
      _loginView.AuthRequsted += loginView_AuthRequsted;
      _switchBtn.Visibility = BarItemVisibility.Never;
      _navigatorPanel.Hide();
      _mainBtn.DropDownControl = null;
      setView(_loginView);
      _loginView.SetFocus();
      onAccessDenied();
    }

    public void GrantAccess()
    {
      if (null != _loginView)
      {
        _loginView.AuthRequsted -= loginView_AuthRequsted;
        _loginView.Parent = null;
        _loginView.Dispose();
        _loginView = null;
      }
      _logoffBtn.Visibility = BarItemVisibility.Never;
      _switchBtn.Visibility = BarItemVisibility.Always;
      _mainBtn.DropDownControl = _mainPopup;
      onAccessGranted();
    }

    public void onAccessGranted()
    {
      VoidHandler handler = AccessGranted;
      if (null != handler)
        handler();
    }

    public void onAccessDenied()
    {
      VoidHandler handler = AccessDenied;
      if (null != handler)
        handler();
    }

    void init()
    {
      _navigatorPanel.Visibility = DockVisibility.Hidden;

      _switchBtn.Caption = Resources.Tuning_NavPanel;
      _switchBtn.Hint = Resources.Tuning_NavPanelHint;
      _mainBtn.Caption = Resources.Tuning_Settings;
      _returnBtn.Caption = Resources.Tuning_Back;
      _logoffBtn.Caption = Resources.Tuning_Exit;
      _navigatorPanel.TabText = Resources.Tuning_Chapters;
      _navigatorPanel.Text = Resources.Tuning_Chapters;
      _commonGroup.Caption = Resources.Tuning_Chapters_Common;
      _modesGroup.Caption = Resources.Tools;
    }

    void _logoffBtn_ItemClick(object sender, ItemClickEventArgs e)
    {
      DenyAccess();
    }

    void loginView_AuthRequsted(string login, string password)
    {
      if (PassService.CheckPassword(password))
        GrantAccess();
      else
        _loginView.AuthRefused();
    }

    void item_LinkClicked(object sender, NavBarLinkEventArgs e)
    {
      Action<ITuning> handler = LinkClicked;
      if (null != handler)
      {
        NavBarItem item = (NavBarItem)sender;
        handler((ITuning)item.Tag);
      }
    }

    void barItem_ItemClick(object sender, ItemClickEventArgs e)
    {
      Action<ITuning> handler = LinkClicked;
      if (null != handler)
        handler((ITuning)e.Item.Tag);
    }

    void switchBtn_ItemClick(object sender, ItemClickEventArgs e)
    {
      onSwitchClicked();
    }

    void returnBtn_ItemClick(object sender, ItemClickEventArgs e)
    {
      onReturnClicked();
    }

    void onSwitchClicked()
    {
      VoidHandler handler = SwitchClicked;
      if (null != handler)
        handler();
    }

    void onReturnClicked()
    {
      VoidHandler handler = ReturnClicked;
      if (null != handler)
        handler();
    }

    void setView(Control view)
    {
      if (null == view)
        throw new ArgumentNullException("view");

      view.Dock = DockStyle.Fill;
      while(_mainPanel.Controls.Count > 0)
        _mainPanel.Controls[0].Parent = null;

      _mainPanel.Controls.Add(view);

    }
  }
}