namespace TrackControl.Tuning
{
  partial class LoginView
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LoginView));
        this._panel = new DevExpress.XtraEditors.GroupControl();
        this._loginTable = new System.Windows.Forms.TableLayoutPanel();
        this._loginLbl = new DevExpress.XtraEditors.LabelControl();
        this._passwordLbl = new DevExpress.XtraEditors.LabelControl();
        this._headerLbl = new DevExpress.XtraEditors.LabelControl();
        this._loginEdit = new DevExpress.XtraEditors.TextEdit();
        this._passwordEdit = new DevExpress.XtraEditors.TextEdit();
        this._loginBtn = new DevExpress.XtraEditors.SimpleButton();
        this._splitter = new DevExpress.XtraEditors.SplitterControl();
        this._errorLbl = new DevExpress.XtraEditors.LabelControl();
        this._mainTable = new System.Windows.Forms.TableLayoutPanel();
        ((System.ComponentModel.ISupportInitialize)(this._panel)).BeginInit();
        this._panel.SuspendLayout();
        this._loginTable.SuspendLayout();
        ((System.ComponentModel.ISupportInitialize)(this._loginEdit.Properties)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this._passwordEdit.Properties)).BeginInit();
        this._mainTable.SuspendLayout();
        this.SuspendLayout();
        // 
        // _panel
        // 
        this._panel.Anchor = System.Windows.Forms.AnchorStyles.None;
        this._panel.Controls.Add(this._loginTable);
        this._panel.Location = new System.Drawing.Point(123, 109);
        this._panel.Name = "_panel";
        this._panel.Size = new System.Drawing.Size(323, 205);
        this._panel.TabIndex = 0;
        this._panel.Text = "�����������";
        // 
        // _loginTable
        // 
        this._loginTable.ColumnCount = 4;
        this._loginTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 97F));
        this._loginTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 97F));
        this._loginTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
        this._loginTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
        this._loginTable.Controls.Add(this._loginLbl, 0, 2);
        this._loginTable.Controls.Add(this._passwordLbl, 0, 3);
        this._loginTable.Controls.Add(this._headerLbl, 0, 0);
        this._loginTable.Controls.Add(this._loginEdit, 1, 2);
        this._loginTable.Controls.Add(this._passwordEdit, 1, 3);
        this._loginTable.Controls.Add(this._loginBtn, 2, 4);
        this._loginTable.Controls.Add(this._splitter, 0, 1);
        this._loginTable.Controls.Add(this._errorLbl, 0, 4);
        this._loginTable.Dock = System.Windows.Forms.DockStyle.Fill;
        this._loginTable.Location = new System.Drawing.Point(2, 22);
        this._loginTable.Name = "_loginTable";
        this._loginTable.RowCount = 6;
        this._loginTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
        this._loginTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
        this._loginTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
        this._loginTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
        this._loginTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 41F));
        this._loginTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
        this._loginTable.Size = new System.Drawing.Size(319, 181);
        this._loginTable.TabIndex = 0;
        // 
        // _loginLbl
        // 
        this._loginLbl.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
        this._loginLbl.Dock = System.Windows.Forms.DockStyle.Fill;
        this._loginLbl.Location = new System.Drawing.Point(20, 75);
        this._loginLbl.Margin = new System.Windows.Forms.Padding(20, 0, 0, 0);
        this._loginLbl.Name = "_loginLbl";
        this._loginLbl.Size = new System.Drawing.Size(77, 28);
        this._loginLbl.TabIndex = 0;
        this._loginLbl.Text = "�����";
        // 
        // _passwordLbl
        // 
        this._passwordLbl.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
        this._passwordLbl.Dock = System.Windows.Forms.DockStyle.Fill;
        this._passwordLbl.Location = new System.Drawing.Point(20, 103);
        this._passwordLbl.Margin = new System.Windows.Forms.Padding(20, 0, 0, 0);
        this._passwordLbl.Name = "_passwordLbl";
        this._passwordLbl.Size = new System.Drawing.Size(77, 26);
        this._passwordLbl.TabIndex = 1;
        this._passwordLbl.Text = "������";
        // 
        // _headerLbl
        // 
        this._headerLbl.Appearance.BackColor = System.Drawing.Color.White;
        this._headerLbl.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("_headerLbl.Appearance.Image")));
        this._headerLbl.Appearance.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
        this._headerLbl.Appearance.Options.UseBackColor = true;
        this._headerLbl.Appearance.Options.UseImage = true;
        this._headerLbl.Appearance.Options.UseTextOptions = true;
        this._headerLbl.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
        this._headerLbl.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
        this._loginTable.SetColumnSpan(this._headerLbl, 4);
        this._headerLbl.Dock = System.Windows.Forms.DockStyle.Fill;
        this._headerLbl.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
        this._headerLbl.Location = new System.Drawing.Point(0, 0);
        this._headerLbl.Margin = new System.Windows.Forms.Padding(0);
        this._headerLbl.Name = "_headerLbl";
        this._headerLbl.Size = new System.Drawing.Size(319, 60);
        this._headerLbl.TabIndex = 2;
        this._headerLbl.Text = "��� ������� � ���������� ���������� ����� ��������������� �����. ��� ���������� �" +
            "��� ���������� � ��������������.";
        // 
        // _loginEdit
        // 
        this._loginTable.SetColumnSpan(this._loginEdit, 2);
        this._loginEdit.Dock = System.Windows.Forms.DockStyle.Fill;
        this._loginEdit.EditValue = "Admin";
        this._loginEdit.Enabled = false;
        this._loginEdit.Location = new System.Drawing.Point(100, 78);
        this._loginEdit.Name = "_loginEdit";
        this._loginEdit.Size = new System.Drawing.Size(191, 20);
        this._loginEdit.TabIndex = 3;
        this._loginEdit.Enter += new System.EventHandler(this.edit_Enter);
        this._loginEdit.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.loginEdit_KeyPress);
        // 
        // _passwordEdit
        // 
        this._loginTable.SetColumnSpan(this._passwordEdit, 2);
        this._passwordEdit.Dock = System.Windows.Forms.DockStyle.Fill;
        this._passwordEdit.Location = new System.Drawing.Point(100, 106);
        this._passwordEdit.Name = "_passwordEdit";
        this._passwordEdit.Properties.PasswordChar = '*';
        this._passwordEdit.Size = new System.Drawing.Size(191, 20);
        this._passwordEdit.TabIndex = 4;
        this._passwordEdit.Enter += new System.EventHandler(this.edit_Enter);
        this._passwordEdit.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.passwordEdit_KeyPress);
        // 
        // _loginBtn
        // 
        this._loginBtn.Anchor = System.Windows.Forms.AnchorStyles.Right;
        this._loginBtn.Image = ((System.Drawing.Image)(resources.GetObject("_loginBtn.Image")));
        this._loginBtn.Location = new System.Drawing.Point(214, 138);
        this._loginBtn.Margin = new System.Windows.Forms.Padding(3, 3, 15, 3);
        this._loginBtn.Name = "_loginBtn";
        this._loginBtn.Size = new System.Drawing.Size(65, 23);
        this._loginBtn.TabIndex = 5;
        this._loginBtn.Text = "����";
        this._loginBtn.Click += new System.EventHandler(this.loginBtn_Click);
        // 
        // _splitter
        // 
        this._splitter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
        this._loginTable.SetColumnSpan(this._splitter, 4);
        this._splitter.Cursor = System.Windows.Forms.Cursors.AppStarting;
        this._splitter.Dock = System.Windows.Forms.DockStyle.Top;
        this._splitter.Enabled = false;
        this._splitter.Location = new System.Drawing.Point(0, 60);
        this._splitter.Margin = new System.Windows.Forms.Padding(0);
        this._splitter.Name = "_splitter";
        this._splitter.Size = new System.Drawing.Size(319, 6);
        this._splitter.TabIndex = 6;
        this._splitter.TabStop = false;
        // 
        // _errorLbl
        // 
        this._errorLbl.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
        this._errorLbl.Appearance.ForeColor = System.Drawing.Color.OrangeRed;
        this._errorLbl.Appearance.Options.UseFont = true;
        this._errorLbl.Appearance.Options.UseForeColor = true;
        this._errorLbl.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
        this._loginTable.SetColumnSpan(this._errorLbl, 2);
        this._errorLbl.Dock = System.Windows.Forms.DockStyle.Fill;
        this._errorLbl.Location = new System.Drawing.Point(20, 129);
        this._errorLbl.Margin = new System.Windows.Forms.Padding(20, 0, 0, 0);
        this._errorLbl.Name = "_errorLbl";
        this._errorLbl.Size = new System.Drawing.Size(174, 41);
        this._errorLbl.TabIndex = 7;
        // 
        // _mainTable
        // 
        this._mainTable.ColumnCount = 1;
        this._mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
        this._mainTable.Controls.Add(this._panel, 0, 0);
        this._mainTable.Dock = System.Windows.Forms.DockStyle.Fill;
        this._mainTable.Location = new System.Drawing.Point(0, 0);
        this._mainTable.Margin = new System.Windows.Forms.Padding(0);
        this._mainTable.Name = "_mainTable";
        this._mainTable.RowCount = 2;
        this._mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 85F));
        this._mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15F));
        this._mainTable.Size = new System.Drawing.Size(569, 499);
        this._mainTable.TabIndex = 1;
        // 
        // LoginView
        // 
        this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        this.Controls.Add(this._mainTable);
        this.Name = "LoginView";
        this.Size = new System.Drawing.Size(569, 499);
        ((System.ComponentModel.ISupportInitialize)(this._panel)).EndInit();
        this._panel.ResumeLayout(false);
        this._loginTable.ResumeLayout(false);
        ((System.ComponentModel.ISupportInitialize)(this._loginEdit.Properties)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this._passwordEdit.Properties)).EndInit();
        this._mainTable.ResumeLayout(false);
        this.ResumeLayout(false);

    }

    #endregion

    private DevExpress.XtraEditors.GroupControl _panel;
    private System.Windows.Forms.TableLayoutPanel _mainTable;
    private System.Windows.Forms.TableLayoutPanel _loginTable;
    private DevExpress.XtraEditors.LabelControl _loginLbl;
    private DevExpress.XtraEditors.LabelControl _passwordLbl;
    private DevExpress.XtraEditors.LabelControl _headerLbl;
    private DevExpress.XtraEditors.TextEdit _loginEdit;
    private DevExpress.XtraEditors.TextEdit _passwordEdit;
    private DevExpress.XtraEditors.SimpleButton _loginBtn;
    private DevExpress.XtraEditors.SplitterControl _splitter;
    private DevExpress.XtraEditors.LabelControl _errorLbl;
  }
}
