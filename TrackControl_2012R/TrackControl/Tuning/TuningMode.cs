using System;
using System.Collections.Generic;
using System.Windows.Forms;
using TrackControl.General;
using DevExpress.XtraEditors;

namespace TrackControl.Tuning
{
    public class TuningMode : IMode
    {
        private IMainView _main;
        private TuningForm _view;
        private ITuning _current;

        bool _isAuthorized;
        private bool _isNotifierActive;
        private bool _isFullScreen;

        private List<ITuning> _commons;

        public event TuningLeavedOut LeavedOut;

        public TuningMode()
        {
            _commons = new List<ITuning>();
        }

        #region IMode Members

        public void Advise(IMainView main)
        {
            _main = main;
            try
            {
                _isNotifierActive = AppModel.Instance.Notifier.IsActive;
                AppController.Instance.StopNotifier();
            }
            catch (Exception e)
            {
                XtraMessageBox.Show(e.Message, "Error TrackControl.Tuning.TuningMode.Avise", MessageBoxButtons.OK);
            }

            if (null != _current)
                _current.Advise();

            _view = new TuningForm();
            _view.TopLevel = false;
            _view.Dock = DockStyle.Fill;
            _main.SetView(_view);
            _view.AccessGranted += view_AccessGranted;
            _view.AccessDenied += view_AccessDenied;
            _view.LinkClicked += setTunning;
            _view.ReturnClicked += onLeavedOut;
            _view.SwitchClicked += view_SwitchClicked;

            _commons.ForEach(delegate(ITuning tuning)
            {
                _view.AddCommonItem(tuning);
            });
            _view.Show();

            //if (_isAuthorized)
            if (UserBaseCurrent.Instance.Admin)
                _view.GrantAccess();
            else
                _view.DenyAccess();
        }

        public void Unadvise()
        {
            if (null != _current)
                _current.Release();

            _view.AccessGranted -= view_AccessGranted;
            _view.AccessDenied -= view_AccessDenied;
            _view.SwitchClicked -= view_SwitchClicked;
            _view.LinkClicked -= setTunning;
            _view.ReturnClicked -= onLeavedOut;
            _view.Parent = null;
            _view.Close();
            _view = null;

            if (_isNotifierActive)
                AppController.Instance.RunNotifier();
        }

        public void RestoreLayout()
        {
            return;
        }

        public void SaveLayout()
        {
            return;
        }

        #endregion

        /// <summary>
        /// ��������� ��������� �������� � ������ ��������
        /// </summary>
        /// <param name="tuning">��������� ������������ ���������</param>
        public void AddTuning(ITuning tuning)
        {
            if (null == tuning)
                throw new ArgumentNullException("tuning");

            _commons.Add(tuning);
        }

        public void SelectTuning(Type T)
        {
            foreach (ITuning tuning in _commons)
            {
                if (T == tuning.GetType())
                {
                    if (null != _view)
                    {
                        setTunning(tuning);
                    }
                    else
                    {
                        if (null != _current && tuning != _current)
                            _current.Release();

                        _current = tuning;
                    }

                    break;
                }
            }
        }

        private void view_AccessDenied()
        {
            _isAuthorized = false;
        }

        private void view_AccessGranted()
        {
            _isAuthorized = true;
            setNavigationVisibility();
            if (null != _current)
                _view.SetTuning(_current);
            else
            {
                _current = _commons[0];
                _view.SetTuning(_current);
            }
        }

        private void view_SwitchClicked()
        {
            _isFullScreen = !_isFullScreen;
            setNavigationVisibility();
        }

        private void setNavigationVisibility()
        {
            if (_isFullScreen)
                _view.HideNavigation();
            else
                _view.ShowNavigation();
        }

        private void setTunning(ITuning tuning)
        {
            if (null != _current)
                _current.Release();

            _current = tuning;
            tuning.Advise();
            _view.SetTuning(_current);
        }

        private void onLeavedOut()
        {
            TuningLeavedOut handler = LeavedOut;
            if (null != handler)
                handler();
        }
    }
}
