using System;
using System.Drawing;
using System.Windows.Forms;
using TrackControl.Online;
using TrackControl.Properties;
using TrackControl.Setting;
using TrackControl.General;
using DevExpress.XtraEditors;
using System.Text;
using TrackControl.Vehicles.Tuning.Properties;
using TrackControl.Vehicles;
using TrackControl.Vehicles.Tuning;

namespace TrackControl.Tuning
{
    public class OldMode : ITuning
    {
        AdminForm _view;
        DevAdminForm admForm;
        DevVehicleControl vehiclesControl;
        VehiclesModel _model;

        public OldMode( VehiclesModel model )
        {
            if ( null == model )
                throw new ArgumentNullException( "model" );

            _model = model;

            _view = new AdminForm();
            admForm = new DevAdminForm();
            vehiclesControl = new DevVehicleControl( _model );
            admForm.ShowTree( vehiclesControl );
            _view.ShowDevAdminForm( admForm );
            vehiclesControl.SelectionChanged += tree_SelectionChanged;
            vehiclesControl.RemoveGroupClicked += tree_RemoveGroupClicked;
        } // OldMode

        #region ITuning Members

        public string Caption
        {
            get { return Resources.Administration; }
        }

        public Image Icon
        {
            get { return Shared.Settings; }
        }

        public Control View
        {
            get
            {
                if (null == _view)
                {
                    throw new Exception("View can't be NULL. Probably, the method Advise() has not been called.");
                }

                return _view;
            } // get
        } // View

        public void Advise()
        {
            //OnlineVehicle entity = vehiclesControl.Current; // 22.03.2013--aketner (�� ���������� �����)
        } // Advise

        public void Release()
        {
            _view.Parent = null;
        } // Release

        void tree_SelectionChanged( Entity entity )
        {
            // TODO:
        }

        void tree_RemoveGroupClicked( VehiclesGroup group )
        {
            // TODO: 
        }

        #endregion
    } // OldMode
} // TrackControl.Tuning
