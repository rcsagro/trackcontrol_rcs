﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Security.Cryptography;
using System.ComponentModel;
using System.Globalization;
using System.Reflection;
using System.Threading;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.LookAndFeel;
using TrackControl.General;
using TrackControl.General.DatabaseDriver;
using TrackControl.General.Services;
using DevExpress.XtraEditors.Controls;
using System.Configuration;

namespace TrackControl
{
    public static class Crypto
    {
        private static string[] arrayStr;
        private static string password = "iCount";
        private const int NUMBER_CONF_STRING = 23;
        private const string nameFileConfig = @"./TrackControl.exe.config";

        public const int MYSQL = 0;
        public const int MSSQL = 1;
        public static int ServerUse = MYSQL;
        public const int AuthenWindows = 0;
        public const int AuthenSqlServer = 1;

        private static byte[] Encrypt(byte[] data, string passwd)
        {
            SymmetricAlgorithm sa = Rijndael.Create();
            ICryptoTransform ct = sa.CreateEncryptor((new PasswordDeriveBytes(passwd, null)).GetBytes(16), new byte[16]);
            MemoryStream ms = new MemoryStream();
            CryptoStream cs = new CryptoStream(ms, ct, CryptoStreamMode.Write);
            cs.Write(data, 0, data.Length);
            cs.FlushFinalBlock();

            return ms.ToArray();
        } // Encrypt

        private static string Encrypt(string data, string passwd)
        {
            return Convert.ToBase64String(Encrypt(Encoding.UTF8.GetBytes(data), passwd));
        } // Encrypt

        public static string Encryption(string encStr)
        {
            return Encrypt(encStr, password);
        }

        public static byte[] Encryption(byte[] encByte)
        {
            return Encrypt(encByte, password);
        }

        private static byte[] Decrypt(byte[] data, string passwd)
        {
            BinaryReader br = new BinaryReader(InternalDecrypt(data, passwd));
            return br.ReadBytes((int) br.BaseStream.Length);
        } // Decrypt

        private static string Decrypt(string data, string passwd)
        {
            try
            {
                CryptoStream cs = InternalDecrypt(Convert.FromBase64String(data), passwd);
                StreamReader sr = new StreamReader(cs);
                return sr.ReadToEnd();
            }
            catch (Exception e)
            {
                return "";
            }
        } // Decrypt

        static CryptoStream InternalDecrypt(byte[] data, string passwd)
        {
            SymmetricAlgorithm sa = Rijndael.Create();
            ICryptoTransform ct = sa.CreateDecryptor((new PasswordDeriveBytes(passwd, null)).GetBytes(16), new byte[16]);
            MemoryStream ms = new MemoryStream(data);
            return new CryptoStream(ms, ct, CryptoStreamMode.Read);
        } // InternalDecrypt

        public static string GetDecryptConnectionString(string encryptStr)
        {
            DriverDb db = new DriverDb();
            db.AnalizeTypeDB();

            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                if (encryptStr.Contains("server=") &&
                    encryptStr.Contains(";user id=") &&
                    encryptStr.Contains(";database="))
                {
                    return encryptStr;
                }
                else
                {
                    return Decrypt(encryptStr, password);
                }
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                if (encryptStr.Contains("Server=") &&
                    encryptStr.Contains(";database=") &&
                    encryptStr.Contains(";asynchronous processing=yes;"))
                {
                    return encryptStr;
                }
                else
                {
                    return Decrypt(encryptStr, password);
                }
            } // else

            return "";
        } // GetDecryptConnectionString

        public static bool WritingNewConfig()
        {
            StreamWriter outStream = null;
            InitialConfigString();

            try
            {
                using (outStream = new StreamWriter(nameFileConfig))
                {
                    for (int i = 0; i < arrayStr.Length; i++)
                    {
                        outStream.WriteLine(arrayStr[i]);
                    }

                    outStream.Flush();
                    outStream.Close();
                } // using
            } // try
            catch (IOException err)
            {
                if (outStream != null)
                {
                    outStream.Close();
                }

                XtraMessageBox.Show(err.Message);

                return false;
            } // catch

            return true;
        } // WritingNewConfig

        public static bool isConnectString(string str)
        {
            DriverDb db = new DriverDb();
            db.AnalizeTypeDB();

            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                if (str.Contains("server=") && str.Contains(";user id=") && str.Contains(";database="))
                    return true;
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                if (str.Contains("Server=") && str.Contains(";database=") &&
                    str.Contains(";asynchronous processing=yes;"))
                    return true;
            }

            return false;
        } // isConnectString

        public static void SaveInConfigFile(string nameFile, string cryptLoginName)
        {
            List<string> listStr = new List<string>();
            StreamReader flStream = null;
            string str;
            string strNum = "50";
            string newConn = "<add name=\"CS\"  connectionString=\"";
            string strEnd = "\"/>";
            bool flagConnStrExist = false;
            int numStrConn = 0;

            try
            {
                using (flStream = new StreamReader(nameFile))
                {
                    while (flStream.Peek() > -1)
                    {
                        str = flStream.ReadLine();

                        if (str.Contains("<add value=\"DBType\" key=\"") && str.Contains("\" />"))
                        {
                            string Strt = "";

                            if (ServerUse == MYSQL)
                            {
                                Strt = "MYSQL";
                            }
                            else if (ServerUse == MSSQL)
                            {
                                Strt = "MSSQL";
                            }

                            listStr.Add(String.Concat("<add value=\"DBType\" key=\"", Strt, "\" />"));
                            continue;
                        } // if

                        if (str.Contains("<connectionStrings>"))
                        {
                            flagConnStrExist = true;
                            numStrConn = listStr.Count + 1;
                        }

                        if (str.Contains("\"CS\""))
                        {
                            flagConnStrExist = false;

                            if (str.Contains("connectionString="))
                            {
                                if ((str.Contains("server=") && str.Contains(";user id=") && str.Contains(";database=")) ||
                                    (str.Contains("Server=") && str.Contains(";database=") &&
                                     str.Contains(";asynchronous processing=yes;")))
                                {
                                    // переименовать строку подключения из CS в CS50 и сохранить ее
                                    int i = str.IndexOf("CS");
                                    string strA = str.Substring(1, i + 1);
                                    string strB = str.Substring(i + 2, str.Length - (i + 2));
                                    string Strt = strA + strNum + strB;
                                    listStr.Add(Strt);

                                    // сформировать новую строку подключения - зашифрованую
                                    string strConn = newConn + cryptLoginName + strEnd;
                                    listStr.Add(strConn);
                                    continue;
                                } // if
                                else
                                {
                                    // строка CS есть - но она зашифрованная или ошибочная, перезапишем ее
                                    int i = str.IndexOf("=");
                                    i = str.IndexOf("=", i + 1);
                                    string strA = str.Substring(0, i + 2);
                                    string lStr = strA + cryptLoginName + strEnd;
                                    listStr.Add(lStr);
                                    continue;
                                } // else
                            } // if
                            else
                            {
                                // there error - ошибочное начало строки, ошибочный тег connectionString=
                                // сформировать новую строку подключения - зашифрованую
                                string strConn = newConn + cryptLoginName + strEnd;
                                listStr.Add(strConn);
                                continue;
                            } // else
                        } // if

                        listStr.Add(str);
                    } // while
                } // using
            } // try
            catch (IOException err)
            {
                if (flStream != null)
                {
                    flStream.Close();
                }

                XtraMessageBox.Show(err.Message);
                return;
            } // catch
            finally
            {
                if (flStream != null)
                {
                    flStream.Close();
                }
            } // finally

            if (flagConnStrExist)
            {
                // сформировать новую строку подключения - зашифрованую
                string strConn = newConn + cryptLoginName + strEnd;
                listStr.Insert(numStrConn, strConn);
            }

            StreamWriter outStream = null;

            try
            {
                using (outStream = new StreamWriter(nameFile))
                {
                    for (int i = 0; i < listStr.Count; i++)
                    {
                        outStream.WriteLine(listStr[i]);
                    }

                    outStream.Flush();
                    outStream.Close();
                } // using
            } // try
            catch (IOException err)
            {
                if (outStream != null)
                {
                    outStream.Close();
                }

                XtraMessageBox.Show(err.Message);
                return;
            } // catch
            finally
            {
                if (outStream != null)
                {
                    outStream.Close();
                }
            } // finally
        } // SaveInFile

        private static void InitialConfigString()
        {
            arrayStr = new string[NUMBER_CONF_STRING];
            arrayStr[0] = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>";
            arrayStr[1] = " <configuration>";
            arrayStr[2] = "  <configSections>";
            arrayStr[3] =
                "   <sectionGroup name=\"userSettings\" type=\"System.Configuration.UserSettingsGroup, System, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\" >";
            arrayStr[4] =
                "    <section name=\"BaseReports.Properties.Settings\" type=\"System.Configuration.ClientSettingsSection, System, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\" allowExeDefinition=\"MachineToLocalUser\" requirePermission=\"false\" />";
            arrayStr[5] = "   </sectionGroup>";
            arrayStr[6] = "  </configSections>";
            arrayStr[7] = " <appSettings>";
            arrayStr[8] =
                " <!--Для подключения к БД MySql установите key в \"MYSQL\", для подключения к БД MS Sql установите key в \"MSSQL\"-->";
            arrayStr[9] =
                " <!-- for connect to MySql database change the key to \"MYSQL\", for connect to MS Sql database change the key to \"MSSQL\" -- >";
            arrayStr[10] = "   <add value=\"DBType\" key=\"MYSQL\"/>";
            arrayStr[11] = " </appSettings>";
            arrayStr[12] = "  <connectionStrings>";
            arrayStr[13] = "   <add name=\"CS\"  connectionString=\"\"/>";
            arrayStr[14] = "  </connectionStrings>";
            arrayStr[15] = " <userSettings>";
            arrayStr[16] = "  <BaseReports.Properties.Settings>";
            arrayStr[17] = "   <setting name=\"FlowmeterOneSubTwo\" serializeAs=\"String\">";
            arrayStr[18] = "    <value>False</value>";
            arrayStr[19] = "   </setting>";
            arrayStr[20] = "  </BaseReports.Properties.Settings>";
            arrayStr[21] = " </userSettings>";
            arrayStr[22] = "</configuration>";
        } // InitialConfigString

        public static void CheckingStrTypeBD()
        {
            bool flagStrDataBaseType = false;
            string nameDataBase = "";
            List<string> listStr = new List<string>();
            StreamReader flStream = null;
            StreamWriter outStream = null;
            string str = "";
            int begIndx = 0;
            int endIndx = 0;

            try
            {
                nameDataBase = ConfigurationManager.AppSettings.GetKey(0);
            }
            catch (Exception ex)
            {
                flagStrDataBaseType = true;
            }

            if (nameDataBase.Length != 0)
            {
                if (nameDataBase.Contains("MYSQL"))
                {
                    ServerUse = MYSQL;
                    return;
                }
                else if (nameDataBase.Contains("MSSQL"))
                {
                    ServerUse = MSSQL;
                    return;
                }

                throw new Exception("Unknown type of data base!");
            } // if
            else
            {
                flagStrDataBaseType = true;
            }

            if (flagStrDataBaseType)
            {
                try
                {
                    using (flStream = new StreamReader(nameFileConfig))
                    {
                        while (flStream.Peek() > -1)
                        {
                            str = flStream.ReadLine();
                            listStr.Add(str);
                            if (str.Contains("</configSections>"))
                            {
                                begIndx = listStr.Count;
                            }

                            if (str.Contains("<connectionStrings>"))
                            {
                                endIndx = listStr.Count;
                            }
                        } // while

                        for (int i = begIndx, k = 0; i < endIndx - 1 - k; k++)
                        {
                            listStr.RemoveAt(i);
                        }

                        listStr.Insert(begIndx, "\t<appSettings>");
                        listStr.Insert(begIndx + 1,
                            "\t<!--Для подключения к БД MySql установите key в \"MYSQL\", для подключения к БД MS Sql установите key в \"MSSQL\"-->");
                        listStr.Insert(begIndx + 2,
                            "\t<!-- for connect to MySql database change the key to \"MYSQL\", for connect to MS Sql database change the key to \"MSSQL\" -->");
                        listStr.Insert(begIndx + 3, "\t<add value=\"DBType\" key=\"MYSQL\"/>");
                        listStr.Insert(begIndx + 4, "\t</appSettings>");
                    } // using

                    using (outStream = new StreamWriter(nameFileConfig))
                    {
                        for (int i = 0; i < listStr.Count; i++)
                        {
                            outStream.WriteLine(listStr[i]);
                        }

                        outStream.Flush();
                        outStream.Close();

                        ServerUse = MYSQL;
                    } // using
                } // try
                catch (Exception ex)
                {
                    throw new Exception(ex.Message + "!");
                }
            } // if
        } // CheckingStrTypeBD
    } // Crypto
} // TrackControl
