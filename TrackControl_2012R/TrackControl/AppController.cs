using System;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;
using System.Xml;
using Agro;
using Agro.Dictionaries;
using DevExpress.XtraEditors;
using Route;
using TrackControl.General;
using TrackControl.General.Core;
using TrackControl.Hasp;
using TrackControl.Modals;
using TrackControl.Notification;
using TrackControl.Properties;
using TrackControl.Reports;
using TrackControl.Tuning;
using TrackControl.UkrGIS.Core;
using TrackControl.UkrGIS.SearchEngine;
using TrackControl.Vehicles.Tuning;
using TrackControl.Zones.Tuning;
using TrackControl.GMap.UI;
using MotorDepot;
using MotorDepot.MonitoringGps;

namespace TrackControl
{
    public class AppController : Singleton<AppController>
    {
        /// <summary>
        /// �������, �������� �� ����������.
        /// </summary>
        private Boolean _appStarted;

        public AppModel _appModel;

        /// <summary>
        /// ������ ���������� ���������� � ��������� ������
        /// </summary>
        private System.Threading.Timer _tm_LastData;

        private MainForm _form;
        private IMode _mode; // ������� ����� ������ ����������
        private IMode _beforeTuning; // �����, �� �������� ��� ������� � ���������

        public event MethodInvoker RefreshLastTimes;

        /// <summary>
        /// ����������� ��� TrackControl.AppController
        /// </summary>
        public AppController()
        {
            #region --   �������� �� �������������� ����������   --

            if (null != Instance)
            {
                throw new Exception(
                    "�� ��������� ������� ��������� Singlton-������, � ���������� ��� ��������������. �������� \"new AppController\" �� \"AppController.Instance\".");
            }

            #endregion

            _appStarted = false;
            _appModel = AppModel.Instance;
        }

        /// <summary>
        /// �������� ������ ����������
        /// </summary>
        /// <param name="mForm">������� ����� ����������</param>
        public void Start(MainForm form)
        {
            if (!checkHasp(form))
            {
                form.Close();
                Application.Exit();
                return;
            }

            _appStarted = true;
            _form = form;
            initAgro();
            initRoutes();
            initNotifications();

            try
            {
//#if !DEBUG -- ���������� 14.03.2014 aketner
                initUkrGIS();
//#endif
            }
            catch (Exception e)
            {
                XtraMessageBox.Show(e.ToString(), "Error initUkrGIS", MessageBoxButtons.OK);
            }

            initMotorDepot();
            initGmapZoneLocator();
            _appModel.TuningMode.LeavedOut += tuningMode_LeavedOut;
            ShowReports();
            _tm_LastData = new System.Threading.Timer(GetLastDataTime);
            _tm_LastData.Change(0, 60000);
        }

        /// <summary>
        /// ��������� ������ ����������
        /// </summary>
        public void Finish()
        {
            if (!_appStarted)
                return;

            if (null != _mode)
                _mode.Unadvise();

            _form.Enabled = false;
            _appModel.ReportsMode.Reports.SaveSetting();
            ComUkrGIS.Instance.Release();

            if (_tm_LastData != null) _tm_LastData.Dispose();

            if (NoticeMonitoringOnLine.IsActive)
                _appModel.MonitoringOnLine.StopOnLine();
        }

        /// <summary>
        /// ���������� ���������� � ��������� ������ - ����� ��������� ����� + ��������� ������ �� ������� ��������� 
        /// </summary>
        /// <param name="data"></param>
        private void GetLastDataTime(object data)
        {
            _appModel.VehiclesModel.RefreshLastTimeGps();

            if (RefreshLastTimes != null)
                RefreshLastTimes();
        }

        /// <summary>
        /// ���������� ����� �������
        /// </summary>
        public void ShowReports()
        {
            setMode(_appModel.ReportsMode);
            //_appModel.ReportsMode.ViewForm.ShowZoneEditorEvent += ShowZonesEditor;
        }

        /// <summary>
        /// ���������� ����� ������ ��������
        /// </summary>
        public void ShowOnline()
        {
            setMode(_appModel.OnlineMode);
            //_appModel.ReportsMode.ViewForm.ShowZoneEditorEvent -= ShowZonesEditor;
        }

        /// <summary>
        /// ���������� ���� "����"
        /// </summary>
        public static void ShowAgro()
        {
            AgroController.Instance.Start();
        }

        public static void ShowMotorDepot()
        {
            MDController.Instance.Start();
        }

        public static void ShowRoutes()
        {
            RouteController.Instance.Start();
        }

        public void ChangeLanguage()
        {
            _appModel.initReportsObjects();
            ShowReports();
            ReportsForm.ResetLayout();
            _mode.RestoreLayout();
        }

        /// <summary>
        /// ���������� ���� "� ���������"
        /// </summary>
        public void ShowAbouts()
        {
            using (About about = new About())
            {
                about.ShowDialog(_form);
            }
        }

        /// <summary>
        /// ��������� ����� ����������� �����������
        /// </summary>
        public void RunNotifier()
        {
            try
            {
                _appModel.Notifier.Start();
            }
            catch (Exception e)
            {
                XtraMessageBox.Show(e.Message, "AppController. RunNotifier", MessageBoxButtons.OK);
            }

            try
            {
                _form.NotificationActivated = _appModel.Notifier.IsActive ? true : false;
            }
            catch (Exception e)
            {
                XtraMessageBox.Show(e.Message, "AppController. RunNotifier", MessageBoxButtons.OK);
            }
        }

        /// <summary>
        /// ��������� ����������� OnLine ����� ����������� � ������ on-line ��������
        /// </summary>
        public void RunMonitoringOnLine()
        {
            _appModel.MonitoringOnLine.StartOnLine();
        }

        /// <summary>
        /// ��������� ����������� ����� ����������� � ������ ������� �� ������
        /// </summary>
        public void RunMonitoringPeriod(DateTime start, DateTime end)
        {
            _appModel.MonitoringPeriod.StartPeriod(start, end);
        }


        /// <summary>
        /// ������������� ����� ����������� ����������� 
        /// </summary>
        public void StopNotifier()
        {
            _appModel.Notifier.Stop();
            _form.NotificationActivated = false;
        }

        /// <summary>
        /// ������������� ����������� OnLine ����� ����������� 
        /// </summary>
        public void StopMonitoringOnLine()
        {
            _appModel.MonitoringOnLine.StopOnLine();
        }

        /// <summary>
        /// ������������� ����������� � ������ ������� �� ������
        /// </summary>
        public void StopMonitoringPeriod()
        {
            _appModel.MonitoringPeriod.StopPeriod();
        }

        /// <summary>
        /// ����������� ����� �����������
        /// </summary>
        public void SwitchNotifier()
        {
            if (_mode == _appModel.TuningMode)
            {
                _form.NotificationActivated = _appModel.Notifier.IsActive ? true : false;
                return;
            }

            bool blResult = false;

            try
            {
                blResult = _appModel.Notifier.IsActive;
            }
            catch (Exception e)
            {
                XtraMessageBox.Show(e.Message, "Error AppController. SwitchNotifier", MessageBoxButtons.OK);
            }

            if (blResult)
                StopNotifier();
            else
                RunNotifier();
        }

        /// <summary>
        /// ���������� ����� ��������
        /// </summary>
        public void ShowSettings()
        {
            if (!(_mode is TuningMode))
                _beforeTuning = _mode;
            setMode(_appModel.TuningMode);
        }

        /// <summary>
        /// ���������� �������� ����������� ���, ������� ������ ��������
        /// </summary>
        public void ShowZonesEditor()
        {
            _appModel.TuningMode.SelectTuning(typeof (ZonesTuningMode));

            if (!(_mode is TuningMode))
                ShowSettings();
        }

        /// <summary>
        /// ���������� ���������� �����������, ������� ����� ��������
        /// </summary>
        public void ShowVehiclesEditor()
        {
            _appModel.TuningMode.SelectTuning(typeof (VehiclesTuningMode));

            if (!(_mode is TuningMode))
                ShowSettings();
        }

        /// <summary>
        /// ���������� ���������� ����������, ������� ����� ��������
        /// </summary>
        public void ShowDriversEditor()
        {
            _appModel.TuningMode.SelectTuning(typeof (DriversTuningMode));

            if (!(_mode is TuningMode))
                ShowSettings();
        }

        /// <summary>
        /// ���������� ����� �� ������������� �� ������������ ���������
        /// </summary>
        public void ShowDictionaries()
        {
            _appModel.Dictionaries.ShowDialog(_form);
        }

        /// <summary>
        /// ���������� ����� ��������� �����������
        /// </summary>
        public void ShowNotices()
        {
            _appModel.TuningMode.SelectTuning(typeof (NoticeTuning));

            if (!(_mode is TuningMode))
                ShowSettings();
        }

        /// <summary>
        /// ��������� ����������� ������������
        /// </summary>
        public void ShowUserGuide()
        {
            string dir = Path.Combine(Application.StartupPath, Path.Combine("Help", Resources.UserGuide));
            Process.Start(dir);
        }


        /// <summary>
        /// ��������������� ������ ��� ����� �������� ������. �� ����, ���
        /// "�������" ��� ����������� �������������� �������� ���-�������, ������������
        /// ����� ����������� �����.
        /// </summary>
        public void RestoreLayout()
        {
            if (null != _mode)
                _mode.RestoreLayout();
        }

        public void SaveLayout()
        {
            if (null != _mode)
                _mode.SaveLayout();
        }

        /// <summary>
        /// ���������� ����� �������� ������ � Excel �� ��������� �� ��� ������ � debug=mode
        /// </summary>
        //public static void ShowExportToExcel()
        //{
        //  using (BaseReports.OutDataToExcel.OutDataForm odf = new BaseReports.OutDataToExcel.OutDataForm())
        //  {
        //    odf.ShowDialog();
        //  }
        //}

        public static void ShowException(Exception ex)
        {
            using (ErrorInfoForm form = new ErrorInfoForm(ex))
            {
                form.ShowDialog();
            }
        }

        #region --   ��������� ������   --

        /// <summary>
        /// ��������� ����������� ������ ����� ����
        /// </summary>
        private void initAgro()
        {
            if (UserBaseCurrent.Instance.IsModuleVisible((int) TrackControlModules.Agro))
            {
                OrderItem.ZonesModel = _appModel.ZonesManager;
                OrderItem.VehiclesModel = _appModel.VehiclesModel;
                DictionaryAgroField.ZonesModel = _appModel.ZonesManager;
                _form.AgroEnabled = true;
                if (Form_Utils.DrawMapEventHandler == null)
                    Form_Utils.DrawMapEventHandler = DrawMapFromAgro;
            }
        }

        //bool flagIsStart = false;

        //public void initVehiclesModel()
        //{
        //    if (flagIsStart)
        //    {
        //        if (UserBaseCurrent.Instance.IsModuleVisible((int) TrackControlModules.Route))
        //        {
        //            TaskItem.VehiclesModel = _appModel.VehiclesModel;
        //        }
        //    }
        //}

        private void initRoutes()
        {
            if (UserBaseCurrent.Instance.IsModuleVisible((int) TrackControlModules.Route))
            {
                DocItem.ZonesModel = _appModel.ZonesManager;
                DocItem.VehiclesModel = _appModel.VehiclesModel;
                //initVehiclesModel();
                _form.RoutesEnabled = true;
                if (Form_Utils.DrawMapEventHandler == null)
                    Form_Utils.DrawMapEventHandler = DrawMapFromAgro;
                //flagIsStart = true;
            }
        }


        private void initNotifications()
        {
            if (UserBaseCurrent.Instance.IsModuleVisible((int) TrackControlModules.Notifications))
            {
                NoticeItem.ZonesModel = _appModel.ZonesManager;
                NoticeLogControl.ZonesModel = _appModel.ZonesManager;
            }
        }

        private void initMotorDepot()
        {
            if (UserBaseCurrent.Instance.IsModuleVisible((int) TrackControlModules.MotoDeport))
            {
                _form.MotoDeportEnabled = true;
                EntityFrameworkModelConverter.ZonesModel = _appModel.ZonesManager;
                EntityFrameworkModelConverter.VehiclesModel = _appModel.VehiclesModel;
            }
            else
                _form.MotoDeportEnabled = false;
        }

        private void initGmapZoneLocator()
        {
            GMapOverlay.ZonesModel = _appModel.ZonesManager;
        }

        /// <summary>
        /// ������ ���� �� ����� � ���� �������.
        /// </summary>
        private void DrawMapFromAgro(object sender, Form_Utils.DrawEventArgs e)
        {
            ShowReports();
            _appModel.ReportsMode.DrawFromAgro(sender, e);
            foreach (Form frm in Application.OpenForms)
            {
                if (frm.GetType().ToString().Contains("MainForm"))
                {
                    frm.Activate(); 
                    break;
                }
            }
        }
        /// <summary>
        /// ���������� ������ �� ��������
        /// </summary>
        private void tuningMode_LeavedOut()
        {
            setMode(_beforeTuning);
        }

        /// <summary>
        /// ����������� ���������� � ����� ����� ������
        /// </summary>
        /// <param name="mode">����� ������ ����������</param>
        private void setMode(IMode mode)
        {
            if (null != _mode)
            {
                _mode.Unadvise();
            }

            _mode = mode;
            _mode.Advise(_form);

            if (_mode is ReportsMode)
            {
                _appModel.ReportsMode.ViewForm.ShowZoneEditorEvent += ShowZonesEditor;
            }
            else
            {
                //_appModel.ReportsMode.ViewForm.ShowZoneEditorEvent -= ShowZonesEditor;
            }
        }

        /// <summary>
        /// �������������� ��������� Eprasys UkrGIS
        /// </summary>
        public static void initUkrGIS()
        {
            XmlDocument doc = null;
            string path = "";

            try
            {
                doc = new XmlDocument();
                path = String.Format("{0}{1}map.xml", Globals.APP_DATA, Path.DirectorySeparatorChar);

                if (!File.Exists(path)) 
                    return;

                doc.Load(path);
                path = doc.DocumentElement.SelectSingleNode("path").InnerText;
            }
            catch (Exception e)
            {
                doc = null;
            }

            try
            {
                // ComUkrGIS.Instance.Open(path, HaspService.MapCodes);
                ComUkrGIS.Instance.Open(path, HaspWrap.MapCodes);
                GeoLocator.FixRepository();
            }
            catch (Exception e)
            {
                XtraMessageBox.Show(e.Message + "\n" + e.StackTrace, "Error", MessageBoxButtons.OK);
            }
        }

        private static bool checkHasp(IWin32Window form)
        {
#if DEBUG
            //Agro.GlobalVars.ResultLicenseAgro = HaspState.OK;
            return true;
#endif
            // 11.02.2014 - ���������� ������� �������� ����
            //try
            //{
            //    Agro.GlobalVars.ResultLicenseAgro = GetResultLicenseAgro(( int )Consts.L_NUMBER_AGRO );
            //}
            //catch( Exception e )
            //{
            //    XtraMessageBox.Show( e.Message, "Error MainAgro", MessageBoxButtons.OK );
            //}
            // ====================================================================================

            // ���������� ������� �������� TrackControl
            HaspState state = HaspWrap.GetHaspNumberState(GlobalsHasp.LICENSE_NUMBER);
            //HaspState state = HaspService.State;
            if (HaspState.HaspNotFound == state)
            {
                XtraMessageBox.Show(form, Resources.Err_HaspNotFound);
                return false;
            }
            else if (HaspState.LicenseNotFound == state)
            {
                XtraMessageBox.Show(form, Resources.Err_HaspNoTrackControlLicense);
                return false;
            }

            return true;
        }

        private static HaspState GetResultLicenseAgro(int L_NUMBER)
        {
            //HaspState State = HaspService.GetHaspNumberState(L_NUMBER);
            return HaspWrap.GetHaspNumberState(L_NUMBER);
        }
        #endregion
    }
}