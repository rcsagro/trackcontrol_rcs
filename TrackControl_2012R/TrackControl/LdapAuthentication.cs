﻿using System;
using System.Collections.Generic;
using System.Text;
using System.DirectoryServices.AccountManagement;
using System.DirectoryServices.ActiveDirectory;
using System.Windows.Forms;
using DevExpress.Utils.StructuredStorage.Reader;
using DevExpress.XtraEditors;
using DevExpress.XtraRichEdit.Model;

namespace TrackControl
{
    class LdapAuthentication
    {
        private String _path;
        private String _filterAttribute;

        private string sDomain = "";
        private string sDefaultOU = "OU=test,DC={0},DC={1}";
        private string sDefaultRootOU = "DC={0},DC={1}";
        private string sServiceUser = "";
        private string sServicePassword = "";

        public string UserName
        {
            get { return sServiceUser; }
        }

        public LdapAuthentication(String domain, string sUserName, string sPassword)
        {
            sServiceUser = sUserName;
            sServicePassword = sPassword;
            sDomain = domain;
            string[] stringDom = domain.Split('.');
            if (stringDom.Length == 2)
            {
                sDefaultOU = string.Format("OU=test,DC={0},DC={1}", stringDom[0], stringDom[1]);
                sDefaultRootOU = string.Format("DC={0},DC={1}", stringDom[0], stringDom[1]);
            }
            else if (stringDom.Length == 1)
            {
                //XtraMessageBox.Show("This computer is not memeber domain controller", "Error Authentication", MessageBoxButtons.OK, MessageBoxIcon.Information);
                sDefaultOU = string.Format("OU=test,DC={0},DC={1}", stringDom[0], "");
                sDefaultRootOU = string.Format("DC={0},DC={1}", stringDom[0], "");
            }
            else
            {
                XtraMessageBox.Show("Name domain's is invalid!", "Error Authentication", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        public LdapAuthentication()
        {
            // to do
        }

        /// <summary>
        /// Проверка имени пользователя и пароля
        /// </summary>
        /// <param name="sUserName">Имя пользователя</param>
        /// <param name="sPassword">Пароль</param>
        /// <returns>Возвращает true, если имя и пароль верны</returns>
        public bool ValidateCredentials(string sUserName, string sPassword)
        {
            return GetPrincipalContext().ValidateCredentials(sUserName, sPassword);
        }

        /// <summary>
        /// Проверяет блокировку пользователя
        /// </summary>
        /// <param name="sUserName">Имя пользователя</param>
        /// <returns>Возвращает true, если учетная запись заблокирована</returns>
        public bool IsAccountLocked(string sUserName)
        {
            return GetUser(sUserName).IsAccountLockedOut();
        }

        /// <summary>
        /// Получить указанного пользователя Active Directory
        /// </summary>
        /// <param name="sUserName">Имя пользователя для извлечения</param>
        /// <returns>Объект UserPrincipal</returns>
        public UserPrincipal GetUser(string sUserName)
        {
            PrincipalContext oPrincipalContext = GetPrincipalContext();
            return UserPrincipal.FindByIdentity(oPrincipalContext, IdentityType.SamAccountName, sUserName);
        }

        /// <summary>
        /// Получить базовый основной контекст
        /// </summary>
        /// <returns>Возвращает объект PrincipalContext</returns>
        public PrincipalContext GetPrincipalContext()
        {
            return new PrincipalContext(ContextType.Domain, sDomain, sDefaultRootOU, sServiceUser, sServicePassword);
        }

        /// <summary>
        /// Get all domain group names (sorted array) for current domain
        /// </summary>
        /// <returns>Domain groups names</returns>
        public string GetCurrentDomain()
        {
            try
            {
                Domain domain = Domain.GetCurrentDomain();
                return domain.Name;
            }
            catch (Exception e)
            {
                //XtraMessageBox.Show(e.Message, "Error Cet Current Domain Name", MessageBoxButtons.OK,
                    //MessageBoxIcon.Error);
                return Environment.UserDomainName; 
            }
        }

        public string GetCurrentDomainUser
        {
            get
            {
                string name = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
                string[] narry = name.Split('\\');
                if(narry.Length == 2)
                    return narry[1];
                else if (narry.Length == 1)
                    return narry[0];
                else
                    XtraMessageBox.Show("Error in current domain user name.", "GetCurrentDomainUser",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                return "";
            }
        }
    }
}
