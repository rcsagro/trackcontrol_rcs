using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Reflection;
using System.Windows.Forms;
using BaseReports.EventTrack;
using DevExpress.XtraEditors;
using LocalCache;
using TrackControl.General;
using TrackControl.General.DAL;
using TrackControl.General.DatabaseDriver;
using TrackControl.Properties;
using TrackControl.Reports;
using TrackControl.Vehicles;
using TrackControl.Vehicles.DAL;
using TrackControl.Vehicles.Tuning;

namespace TrackControl.Temp
{
    public static class VehiclesModelInitializer
    {
        private static VehicleStyle _vehicleStyle = VehicleStyle.Empty;
        private static VehiclesModel vhModel = null;
        private static List<Vehicle> lstVehicle = new List<Vehicle>(); 

        public static void InitFromDataSet(VehiclesModel model, atlantaDataSet dataset)
        {
            initDrivers(model, dataset);
            initVehicleTree(model, dataset);
        }

        public static VehiclesModel GetVehiclesModel
        {
            get { return vhModel; }
        }

        public static void RefreshVehicleProperty()
        {
            for (int i = 0; i < lstVehicle.Count; i++)
            {
                Vehicle vh = lstVehicle[i];
                vh.PcbVersion = PcbVersion( vh.MobitelId );
                TuningVehicle tnVh =  vh.Tag as TuningVehicle;

                if(tnVh == null)
                    return;

                tnVh.PcbVersus = vh.PcbVersion;
                tnVh.Status = TuningStatus.Ok;
            }
        }

        public static void initPcbVersion(VehiclesModel model)
        {
            if(model == null)
                return;

            vhModel = model;
            DriverDb db = new DriverDb();
            db.ConnectDb();
            string sql = TrackControlQuery.PcbSensors.SelectFromPcbVersion;
            DataTable pcbTable = db.GetDataTable( sql );
            db.CloseDbConnection();
            model.ClearPcbVersion();

            for (int i = 0; i < pcbTable.Rows.Count; i++)
            {
                int id = Convert.ToInt32(pcbTable.Rows[i]["Id"].ToString());
                string nm = pcbTable.Rows[i]["Name"].ToString();
                string comm = pcbTable.Rows[i]["Comment"].ToString();

                PcbVersions pcbv = new PcbVersions(id, nm, comm);
                model.AddPcbVersion(pcbv);
            }

            Vehicle.PcbVersionData = model.PcbVersion;
        } // initPcbVersion

        private static void initDrivers(VehiclesModel model, atlantaDataSet dataset)
        {
            Dictionary<int, DriverType> driverTypes = new Dictionary<int, DriverType>();
            foreach (atlantaDataSet.driverRow row in dataset.driver)
            {
                if (row.id == 1)
                    continue;

                Driver driver = new Driver(row.Name, row.Family);
                driver.Id = row.id;
                try
                {
                    if (row.ByrdDay==null)
                    {
                        driver.DayOfBirth = null;
                    }
                    else
                    {
                        driver.DayOfBirth = Convert.ToDateTime(row.ByrdDay);
                    }
                    
                }
                catch
                {
                    driver.DayOfBirth = null;
                }
                driver.Categories = row.Category;
                driver.License = row.Permis;
                driver.IdOutLink = row.OutLinkId;
                driver.Department = row.Department??"";
                try
                {
                    driver.NumTelephone = row.numTelephone;
                }
                catch
                {
                    driver.NumTelephone = "";
                }

                try
                {
                    ushort? ident = null;
                    if (row.Identifier != null) ident = Convert.ToUInt16(row.Identifier);
                    driver.Identifier = ident;
                }
                catch
                {
                    driver.Identifier = null;
                }
                try
                {
                    if(row.foto != null)
                        if (row.foto.Length > 0)
                            driver.Photo = ImageService.ToImage(row.foto);
                }
                catch
                {
                    driver.Photo = null;
                }
                if (row.idType > 0)
                {
                    if (driverTypes.ContainsKey(row.idType))
                    {
                        driver.TypeDriver = driverTypes[row.idType];
                    }
                    else
                    {
                        driver.TypeDriver = DriverVehicleProvider.GetDriverType(row.idType);
                        driverTypes.Add(row.idType, driver.TypeDriver);
                    }

                }

                model.AddDriver(driver);
            }
        }

        private static void initVehicleTree(VehiclesModel model, atlantaDataSet dataset)
        {
            GroupStyle groupStyle = GroupStyle.Empty;

            foreach (atlantaDataSet.teamRow team in dataset.team)
            {
                VehiclesGroup group = new VehiclesGroup(team.Name, team.Descr, groupStyle, team.OutLinkId,
                                                        team.FuelWayKmGrp, team.FuelMotoHrGrp);
                group.Id = team.id;

                #region --   ��������� ������   ---

                foreach (atlantaDataSet.ReferenceVehicalRow rv in team.GetReferenceVehicalRows())
                {
                    if (UserBaseCurrent.Instance.IsVehicleVisibleForRole(rv.vehicle_id))
                    {
                        Vehicle vehicle = createVehicle(rv, group, model, dataset);
                    }
                } // foreach

                if(group.OwnItems.Count > 0)
                    model.Root.AddGroup(group);

                #endregion
            } // foraech

            foreach (atlantaDataSet.ReferenceVehicalRow rv in (atlantaDataSet.ReferenceVehicalRow[])
                                                              dataset.ReferenceVehical.Select("Team_id IS NULL",
                                                                                              "NumberPlate ASC"))
            {
                if (UserBaseCurrent.Instance.IsVehicleVisibleForRole(rv.vehicle_id))
                {
                    Vehicle vehicle = createVehicle(rv, model.Root, model, dataset);
                }
            }
        }

// foreach} // initVehicleTre

        private static Vehicle createVehicle(atlantaDataSet.ReferenceVehicalRow row, VehiclesGroup group,
                                             VehiclesModel model, atlantaDataSet dataset)
        {
            Mobitel mobitel = new Mobitel(row.Mobitel_id, row.Num_TT, row.sim, row.LastTimeGps);

            mobitel.Is64BitPackets = row.mobitelsRow.Is64bitPackets;
            mobitel.IsNotDrawDgps = row.mobitelsRow.IsNotDrawDgps;

            string cmnts = VehicleCommentProvider.GetComment( row.Mobitel_id );

            Vehicle vehicle = new Vehicle(mobitel, row.Make,
                                          row.Model, row.NumberPlate, group,
                                          new VehicleStyle(Color.FromArgb(row.colorTrack)), row.OutLinkId,
                                          row.FuelWayLiter, row.FuelMotorLiter, row.Identifier, cmnts);

            vehicle.Settings = new VehicleSettings();

            if (row.vehicleRow != null)
            {
                vehicle.Id = row.vehicle_id;

                vehicle.Settings.Id = row.vehicleRow.setting_id;

                if (row.vehicleRow.Category_id > 0) 
                    vehicle.Category = new VehicleCategory(row.vehicleRow.Category_id);

                if (row.vehicleRow.Category_id2 > 0)
                    vehicle.Category2 = new VehicleCategory2(row.vehicleRow.Category_id2);

                if( row.vehicleRow.Category_id3 > 0 )
                    vehicle.Category3 = new VehicleCategory3( row.vehicleRow.Category_id3 );

                if( row.vehicleRow.Category_id4 > 0 )
                    vehicle.Category4 = new VehicleCategory4( row.vehicleRow.Category_id4 );


            }
            if (row.driverRow != null)
            {
                int driverId = row.driverRow.id;
                if (driverId > 1)
                {
                    foreach (Driver driver in model.Drivers)
                    {
                        if (driverId == driver.Id)
                        {
                            vehicle.Driver = driver;
                            break;
                        }
                    }
                }
            }

            if (row.mobitelsRow != null)
            {
                vehicle.Is64BitPackets = row.mobitelsRow.Is64bitPackets; 
            }
            vehicle.PcbVersion = PcbVersion(row.Mobitel_id);

            SetVehicleSensors(row, dataset, vehicle);

            lstVehicle.Add(vehicle);

            return vehicle;
        }

        private static void SetVehicleSensors(atlantaDataSet.ReferenceVehicalRow row, atlantaDataSet dataset, Vehicle vehicle)
        {
            var sRows = (atlantaDataSet.sensorsRow[]) dataset.sensors.Select(String.Format("mobitel_id= {0}", row.Mobitel_id));
            foreach (atlantaDataSet.sensorsRow sRow in sRows)
            {
                foreach (atlantaDataSet.relationalgorithmsRow rRow in sRow.GetrelationalgorithmsRows())
                {
                    if (rRow.AlgorithmID == (int) AlgorithmType.VEHICLE_UNLOAD)
                    {
                        var logicSensor = new LogicSensor(sRow.Name, sRow.Description, sRow.StartBit);
                        logicSensor.SetK(sRow.K);
                        vehicle.LogicSensorUpload = logicSensor;
                    }
                }
            }
        }

        private static PcbVersions PcbVersion(int idmobi)
        {
            DriverDb db = null;
            try
            {
                string sql = string.Format("select vehicle.PcbVersionId from vehicle where Mobitel_id = {0}", idmobi);
                db = new DriverDb();
                db.ConnectDb();
                DataTable tb = db.GetDataTable(sql);

                int ID = -1;

                if( tb.Rows.Count > 0 )
                {
                    string data = tb.Rows[0]["PcbVersionId"].ToString();

                    if( data == "" )
                        ID = Convert.ToInt32( "-1" );
                    else
                        ID = Convert.ToInt32( data );
                }
                else
                {
                    db.CloseDbConnection();
                    return new PcbVersions( ID, "-- " + Resources.Absent + " --", "" );
                }

                sql = string.Format("select * from pcbversion where Id = {0}", ID);
                DataTable tbs = db.GetDataTable(sql);
                db.CloseDbConnection();

                if (tbs.Rows.Count > 0)
                    return new PcbVersions(ID, tbs.Rows[0]["Name"].ToString(), tbs.Rows[0]["Comment"].ToString());

                return new PcbVersions(-1, "-- " + Resources.Absent + " --", "");
            }
            catch (Exception e)
            {
                // XtraMessageBox.Show(e.Message, Resources.ErrorPcb, MessageBoxButtons.OK);
                if(db != null)
                    db.CloseDbConnection();

                return new PcbVersions( -1, "-- " + Resources.Absent + " --", "" );
            }
        }
    }
}
