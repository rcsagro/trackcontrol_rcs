using System;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using LocalCache;
using TrackControl.General;
using TrackControl.GMap;
using TrackControl.Online.Services;
using TrackControl.Properties;
using TrackControl.Reports;
using TrackControl.UkrGIS;
using TrackControl.Vehicles;
using TrackControl.Zones;

namespace TrackControl.Online
{
    /// <summary>
    /// ����� ����� ������ ��������.
    /// </summary>
    public class OnlineMode : IMode
    {
        private bool _advised;
        private bool _isOnline;
        private bool _autoZoom;
        private int _tail = 50;
        private int _interval = 15000; // ������ ������� ��� ������ �������� � ��

        private AppModel _app;

        private IMainView _main;
        private OnlineForm _view;

        private VehiclesModel _vehiclesModel;
        private OnlineVehiclesModel _onlineVehiclesModel;

        private IZonesManager _zonesManager;
        private OnlineZonesModel _onlineZonesModel;

        private OnlineController _controller;
        private IOnlineDataProvider _provider;

        private VehicleTreeView _vehicleTree;
        private ZonesTreeView _zonesTree;
        private GoogleMapControl _gMap;
        private GisMapControl _gisMap;

        private ISettingsProvider _settingProvider;
        private AddressFinderController _adrFinderCtr;
        private Marker _addressFinder;

        public event MethodInvoker SynhroCheckedVehicles;

        /// <summary>
        /// ������� � �������������� ����� ��������� ������ TrackControl.Monitoring.OnlineMode
        /// </summary>
        public OnlineMode(VehiclesModel vehiclesModel, IZonesManager zonesManager, GoogleMapControl gMap,
            GisMapControl gisMap, IOnlineDataProvider provider, ISettingsProvider settingProvider)
        {
            if (null == vehiclesModel)
                throw new ArgumentNullException("vehiclesModel");
            if (null == zonesManager)
                throw new ArgumentNullException("zonesManager");
            if (null == gMap)
                throw new ArgumentNullException("gMap");
            if (null == gisMap)
                throw new ArgumentNullException("gisMap");
            if (null == provider)
                throw new ArgumentNullException("provider");
            if (null == settingProvider)
                throw new ArgumentNullException("settingProvider");
            _settingProvider = settingProvider;

            _vehiclesModel = vehiclesModel;
            _zonesManager = zonesManager;
            _gMap = gMap;
            _gisMap = gisMap;
            _provider = provider;
            _app = AppModel.Instance;
            _app.SynhroCheckedVehiclesFromReportToOnline += onSetCheckedFromModel;
            _gMap.OnPointClicked += onFindAddressToPoint;
            init();
        }

        public void ResetLayout()
        {
            OnlineForm.ResetLayout();
            if (null != _view && _view.IsHandleCreated)
                _view.RestoreLayout();
        }

        public void SaveLayout()
        {
            if (null != _view && _view.IsHandleCreated)
                _view.SaveLayout();
        }

        #region IMode Members

        public void Advise(IMainView main)
        {
            _advised = true;
            _main = main;
            _onlineVehiclesModel.Activate();
            _onlineZonesModel.Activate();
            _vehicleTree.RefreshTree();
            //_zonesTree.RefreshTree();
            if (_view != null) _view = null;
            _view = new OnlineForm(_vehicleTree, _zonesTree, _gMap, _gisMap);
            _view.AssignVehicleModel(_vehiclesModel);
            _view.IsOnline = _isOnline;
            _view.IsAutoZoom = _autoZoom;
            _view.Tail = _tail;
            _view.TopLevel = false;
            _view.Dock = DockStyle.Fill;
            _main.SetView(_view);
            _view.Show();
            _view.RestoreLayout();
            _view.Advise();
            _view.RunItemClicked += view_RunItemClicked;
            _view.AutoZoomItemClicked += view_AutoZoomItemClicked;
            _view.TailValueChanged += view_TailValueChanged;
            _view.DetailsRowDoubleClicked += view_DetailsRowDoubleClicked;
            _view.MapChanged += view_MapChanged;
            _view.RunAddressFinder += view_RunAddressFinder;
            _view.Map.State = new MapState(ConstsGen.KievZoom, new PointLatLng(ConstsGen.KievLat,ConstsGen.KievLng));
            updateView();
            zonesVisibilityChanged();
            if (_isOnline)
            {
                _controller.Start();
            }
        }

        public void Unadvise()
        {
            _advised = false;
            _controller.Stop();
            _onlineZonesModel.DeActivate();
            _view.RunItemClicked -= view_RunItemClicked;
            _view.AutoZoomItemClicked -= view_AutoZoomItemClicked;
            _view.TailValueChanged -= view_TailValueChanged;
            _view.DetailsRowDoubleClicked -= view_DetailsRowDoubleClicked;
            _view.MapChanged -= view_MapChanged;
            _view.RunAddressFinder -= view_RunAddressFinder;
            _view.SaveLayout();
            //_view.Release();
            _view.Parent = null;
            //_view.Close();
            //_view = null;
        }

        public void RestoreLayout()
        {
            _view.RestoreLayout();
        }

        #endregion

        #region --   Privates    --

        private void init()
        {
            _onlineVehiclesModel = new OnlineVehiclesModel(_vehiclesModel, _settingProvider);
            _onlineVehiclesModel.Activate();
            _vehicleTree = new VehicleTreeView(_vehiclesModel);
            _vehicleTree.Dock = DockStyle.Fill;
            _vehicleTree.SynhroCheckedVehicles += onSynhroCheckedVehicles;
            _onlineZonesModel = new OnlineZonesModel(_zonesManager);
            _onlineZonesModel.Activate();
            _onlineZonesModel.VisibilityChanged += zonesVisibilityChanged;
            _zonesTree = new ZonesTreeView(_zonesManager);
            _zonesTree.Dock = DockStyle.Fill;
            _zonesTree.VisibilityChanged += _onlineZonesModel.ChangeVisibility;
            _zonesTree.ZoneNodeDoubleClicked += showCertainZone;
            _zonesTree.PanZoneClicked += showCertainZone;
            _zonesTree.VehiclesInsideClicked += showVehiclesInZone;
            _autoZoom = true;
            _controller = new OnlineController(_vehicleTree, _tail, _interval, _provider);
            _controller.Handled += updateView;
        }

        private void view_RunItemClicked()
        {
            _isOnline = !_isOnline;
            _view.IsOnline = _isOnline;
            if (_isOnline)
                _controller.Start();
            else
                _controller.Stop();
        }

        private void view_AutoZoomItemClicked()
        {
            _autoZoom = _view.IsAutoZoom;
            updateView();
        }

        private void view_TailValueChanged()
        {
            _tail = _view.Tail;
            _controller.Tail = _tail;
            updateView();
        }

        /// <summary>
        /// ��������� ������������� (������ � �������������� ������� � ��������� �����)
        /// </summary>
        private void updateView()
        {
            try
            {
                // ���������� ������ ������ ���������� ����� ������� ����� ���������� �����
                GC.Collect();
                GC.WaitForPendingFinalizers();

                if (_advised && !_view.IsDisposed)
                {
                    if (_view.InvokeRequired)
                    {
                        MethodInvoker m = delegate()
                        {
                            _updateView();
                        };

                        if (_view.IsHandleCreated) 
                            _view.Invoke(m);
                    }
                    else
                    {
                        _updateView();
                    }
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message + "\n" + ex.StackTrace, "Error Online Mode", MessageBoxButtons.OK);
            }
        }

        private void _updateView()
        {
            try
            {
                if (null == _view)
                    return;

                _view.RefreshDetailsGrid();
                IMap map = _view.Map;

                List<Marker> markers = new List<Marker>();
                List<Track> tracks = new List<Track>();

                Marker topMaprker = null;

                foreach (OnlineVehicle ov in _vehicleTree.Checked)
                {
                    if (null != ov.Track)
                    {
                        tracks.Add(ov.Track);
                        MarkerType t;

                        if (ov.Speed.HasValue && ov.Speed.Value > ov.SpeedLimit)
                            t = MarkerType.Alarm;
                        else if (ov.MotionState == MotionState.Moving)
                            t = MarkerType.Moving;
                        else if (ov.MotionState == MotionState.Stopped)
                            t = MarkerType.Stop;
                        else if (ov.MotionState == MotionState.ParkingLong)
                            t = MarkerType.ParkingLong;
                        else
                            t = MarkerType.Parking;

                        _updateLogicalSensors(ov);

                        Marker m = new Marker(t, ov.Mobitel.Id, ov.Track.LastPoint.LatLng, ov.RegNumber,
                            String.Format("<size=+4>{0} {1} {2}</size>", ov.RegNumber, ov.CarModel, ov.CarMaker));

                        m.LogicSensorState = ov.LogicSensorState;

                        if (ov.IsSelected)
                        {
                            m.IsActive = true;
                            topMaprker = m;
                        }
                        else
                        {
                            markers.Add(m);
                        }

                        SetToolTipDetaled(ov, m);
                    }
                }

                if (null != topMaprker)
                {
                    markers.Add(topMaprker);
                }

                if (null != _addressFinder)
                {
                    markers.Add(_addressFinder);
                }

                //Application.DoEvents();

                map.ClearTracks();
                map.ClearMarkers();
                map.AddTracks(tracks);
                map.AddMarkers(markers);
                map.Repaint();

                if (_autoZoom)
                {
                    map.ZoomAndCenterAll();
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message + "\n" + ex.StackTrace, "Error Online Mode", MessageBoxButtons.OK);
            }
        }

        private void SetToolTipDetaled(OnlineVehicle ov, Marker m)
        {
            if (ov.Sensors != null)
            {
                if (ov.Sensors.Count == 0 && ov.LastPoint != null)
                {
                    VehicleSensors vehSensors = new VehicleSensors(ov);
                    vehSensors.AssignVehicleModel(_vehiclesModel);
                    vehSensors.SetSensorValues();
                }
            }

            string nameDrv = ov.DriverNameStep;

            m.DescriptoinDetailed =
                string.Format(
                    @"<b>{1}: </b>{2}{0}<b>{16} </b>{17}{0}<b>{3}: </b>{4}{0}<b>{5}: </b>{6}{0}<b>{7} </b>{8}{0}<b>{9}: </b>{10}{0}<b>{11}: </b>{12} {15}{0}<b>{13}: </b>{14}{0}{0}",
                    Environment.NewLine // 0
                    , Resources.Number, ov.RegNumber // 1, 2
                    , Resources.Group, ov.Group.Name // 3, 4
                    , Resources.Driver, ov.DriverNameVersus // 5, 6
                    , Resources.PersonTelefone, ov.DriverTelefone // 7,8
                    , Resources.State, ov.StateInfo // 9, 10
                    , Resources.Speed, ov.Speed // 11, 12
                    , Resources.LastData, ov.LastTime // 13, 14
                    , Resources.KmH // 15
                    , Resources.SimTelNumber, " " + ov.Mobitel.Login + " " + "(" + ov.Mobitel.SimNumber + ")"); // 16, 17

            if (ov.Sensors != null)
            {
                string strfuel = "";
                string otherfuel = "";
                string strfuelall = "";

                foreach (Sensor s in ov.Sensors)
                {
                    if (s.Algoritm == (int)AlgorithmType.FUEL1)
                    {
                        strfuel += string.Format("<b>{1}: </b>{2}{0}", Environment.NewLine, s.Name, s.SensorValue);
                    }
                    else if (s.Algoritm == (int)AlgorithmType.FUELDRTADD)
                    {
                        strfuel += string.Format("<b>{1}: </b>{2}{0}", Environment.NewLine, s.Name, s.SensorValue);
                    }
                    else if (s.Algoritm == (int)AlgorithmType.RANGEFINDER)
                    {
                        otherfuel += string.Format("<b>{1}: </b>{2}({3}){0}", Environment.NewLine, s.Name, s.Value, s.ValueStr);
                    }
                    else if (s.Algoritm == (int)AlgorithmType.DRIVER)
                    {
                        otherfuel += string.Format("<b>{1}: </b>{2}{0}", Environment.NewLine, s.Name, s.RfidValue);
                    }
                    else
                    {
                        otherfuel += string.Format("<b>{1}: </b>{2}{0}", Environment.NewLine, s.Name, s.SensorValue);
                    }
                }

                strfuelall += string.Format("<b>{1}: </b>{2}{0}", Environment.NewLine, Resources.FuelAll, ov.GetAllFuel);
                if (strfuel != "")
                    m.DescriptoinDetailed += strfuel;
                if (strfuelall != "")
                    m.DescriptoinDetailed += strfuelall;
                if (otherfuel != "")
                    m.DescriptoinDetailed += otherfuel;
            }
        }

        private void _updateLogicalSensors(OnlineVehicle ov)
        {
            ov.LogicSensorState = (short) LogicSensorStates.Absence;
            atlantaDataSet.sensorsRow[] sensors = (atlantaDataSet.sensorsRow[])AppModel.Instance.DataSet.sensors.Select(String.Format("mobitel_id = {0}", ov.Mobitel.Id));

            if (sensors.Length > 0)
            {
                for (int i = 0; i < sensors.Length; i++)
                {
                    atlantaDataSet.sensorsRow sensor = sensors[i];

                    if (sensor.Length == 1)
                    {
                        BitArray bits = new BitArray(ov.LastPoint.Sensors);
                        int val = bits[sensor.StartBit] ? 1 : 0;
                        
                        foreach (atlantaDataSet.StateRow stateRow in sensor.GetStateRows())
                        {
                            if (val == stateRow.MinValue)
                            {
                                atlantaDataSet.relationalgorithmsRow[] algs = sensor.GetrelationalgorithmsRows();

                                if (null != algs && algs.Length > 0 && algs[0].AlgorithmID == (int) AlgorithmType.ONLINE_LOGIC)
                                {
                                    if (sensor.K != (float) val)
                                        ov.LogicSensorState = (short) LogicSensorStates.Off;
                                    else
                                        ov.LogicSensorState = (short) LogicSensorStates.On;
                                }

                                break;
                            }
                        }
                    }
                }
            }
        }

        private void view_DetailsRowDoubleClicked(PointLatLng point)
        {
            updateView();
            if (!_autoZoom) _view.Map.PanTo(point);
        }

        private void view_MapChanged()
        {
            _updateView();
            zonesVisibilityChanged();
        }

        /// <summary>
        /// ������������ ��� ��������� ���������� ���������� ����������� ���
        /// </summary>
        private void zonesVisibilityChanged()
        {
            if (_view == null) return;
            IMap map = _view.Map;
            map.ClearZones();
            map.AddZones(_onlineZonesModel.CheckedZones);
        }

        private void showVehiclesInZone(IZone zone)
        {
            using (VehiclesInZoneForm form = new VehiclesInZoneForm(zone, _vehiclesModel))
            {
                form.StartPosition = FormStartPosition.CenterParent;
                form.ShowDialog();
            }
        }

        private void showCertainZone(IZone zone)
        {
            _view.Map.PanTo(zone.Bounds);
        }

        #endregion

        private void onSynhroCheckedVehicles()
        {
            if (SynhroCheckedVehicles != null) SynhroCheckedVehicles();
        }

        private void onSetCheckedFromModel()
        {
            _vehicleTree.SetCheckedFromModel();
        }

        #region �������� �����

        private void view_RunAddressFinder()
        {
            _adrFinderCtr = AddressFinderController.Instance;
            _adrFinderCtr.DrawPointOnMap += onDrawPointOnMap;
            _adrFinderCtr.ClearMarker += onClearMarker;
            _adrFinderCtr.StopSearching += onStopSearching;
            _adrFinderCtr.Start(_main);
        }

        private void onDrawPointOnMap(PointLatLng point)
        {
            if (_view == null) return;
            _addressFinder = new Marker(MarkerType.Pin, 0, point, "", "");
            updateView();
            _view.Map.PanTo(point);
        }

        private void onClearMarker()
        {
            if (_addressFinder != null)
            {
                _addressFinder = null;
                updateView();
            }

        }

        private void onFindAddressToPoint(PointLatLng point)
        {
            if (_adrFinderCtr != null)
            {
                _addressFinder = new Marker(MarkerType.Pin, 0, point, "", "");
                updateView();
                _adrFinderCtr.FindAddressToPoint(point);
            }
        }

        private void onStopSearching()
        {
            _adrFinderCtr.DrawPointOnMap -= onDrawPointOnMap;
            _adrFinderCtr.ClearMarker -= onClearMarker;
            _adrFinderCtr.StopSearching -= onStopSearching;
            onClearMarker();
            _adrFinderCtr = null;
        }

        #endregion
    }
}