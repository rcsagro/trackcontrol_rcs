using DevExpress.XtraBars;
using DevExpress.XtraBars.Docking;
using DevExpress.XtraEditors;
using DevExpress.XtraTab;

using System;
using System.IO;
using System.Windows.Forms;
using System.Xml;

using TrackControl.General;
using TrackControl.GMap;
using TrackControl.GMap.Core;
using TrackControl.UkrGIS;

using TrackControl.Properties;
using TrackControl.Vehicles;

using TrackControl.Notification;

namespace TrackControl.Online
{
    public partial class OnlineForm : XtraForm
    {
        private const string KEY = "Mon_ing";

        private IMap _map;
        private VehicleTreeView _vehicleTree;
        private ZonesTreeView _zonesTree;
        private GoogleMapControl _gMap;
        private GisMapControl _gisMap;
        private NoticeLogControl _noticeLogContr;

        public event MethodInvoker RunItemClicked = delegate { };
        public event MethodInvoker AutoZoomItemClicked = delegate { };
        public event MethodInvoker TailValueChanged = delegate { };
        public event Action<PointLatLng> DetailsRowDoubleClicked = delegate { };
        public event MethodInvoker RunAddressFinder = delegate { };
        public event MethodInvoker MapChanged = delegate { };

        public OnlineForm(VehicleTreeView vehicleTree, ZonesTreeView zonesTree, GoogleMapControl gMap,
            GisMapControl gisMap)
        {
            if (null == vehicleTree)
                throw new ArgumentNullException("vehicleTree");
            if (null == zonesTree)
                throw new ArgumentNullException("zonesTree");
            if (null == gMap)
                throw new ArgumentNullException("gMap");
            if (null == gisMap)
                throw new ArgumentNullException("gisMap");

            _vehicleTree = vehicleTree;
            _zonesTree = zonesTree;
            _gMap = gMap;
            _gisMap = gisMap;
            _noticeLogContr = new NoticeLogControl();
            InitializeComponent();
            init();
            RefreshTree.refreshOnline = new RefreshingTree(_vehicleTree.RefreshTree);
        }

        /// <summary>
        /// ��������� ��������� ������ "����"
        /// </summary>
        public bool IsOnline
        {
            set
            {
                if (value)
                {
                    _runButton.Glyph = Shared.Stop;
                    _runButton.Caption = Resources.Online_Stop;
                    _runButton.Hint = Resources.Online_StopHint;
                }
                else
                {
                    _runButton.Glyph = Shared.Start;
                    _runButton.Caption = Resources.Online_Start;
                    _runButton.Hint = Resources.Online_StartHint;
                }
            }
        }

        /// <summary>
        /// ��������� ������ �������������������
        /// </summary>
        public bool IsAutoZoom
        {
            get { return _autoZoomBtn.Checked; }
            set { _autoZoomBtn.Checked = value; }
        }

        /// <summary>
        /// �������� � ���� "�����"
        /// </summary>
        public int Tail
        {
            get { return Convert.ToInt32(_tailItem.EditValue); }
            set { _tailItem.EditValue = value; }
        }

        /// <summary>
        /// ��������� ������������� �����
        /// </summary>
        public IMap Map
        {
            get { return _map; }
        }

        public void Advise()
        {
            _vehiclePanel.ControlContainer.Controls.Add(_vehicleTree);
            _vehicleTree.CheckedChanged += _info.RefreshGrid;
            _zonesPanel.ControlContainer.Controls.Add(_zonesTree);
            _gisTab.Controls.Add(_gisMap);
            _gisTab.Tag = _gisMap;

            _googleTab.Controls.Add(_gMap);
            _googleTab.Tag = _gMap;
            _gMap.MarkerClick += _info.ScrollTo;

            _map = _gMap;
            _xtraTabs.SelectedTabPage = _googleTab;
            _info.VehicleChanged += _details.ShowSensorsState;
            _info.Selected += _info_Selected;
            _sensorPanel.VisibilityChanged += _sensorPanel_VisibilityChanged;

            _noticeLog.ControlContainer.Controls.Add(_noticeLogContr);
            _noticeLogContr.Dock = DockStyle.Fill;
        }

        public void Release()
        {
            _vehicleTree.Parent = null;
            _vehicleTree.CheckedChanged -= _info.RefreshGrid;
            _zonesTree.Parent = null;
            _gisMap.Parent = null;
            _gisTab.Tag = null;
            _gMap.Parent = null;
            _googleTab.Tag = null;
            _gMap.MarkerClick -= _info.ScrollTo;

            _sensorPanel.VisibilityChanged -= _sensorPanel_VisibilityChanged;
            _info.VehicleChanged -= _details.ShowSensorsState;
            _info.Selected -= _info_Selected;
            _map.ClearAll();
        }

        public void AssignVehicleModel(VehiclesModel model)
        {
            _details.AssignVehicleModel(model);
        }

        public void RefreshDetailsGrid()
        {
            _info.UpdateInfo();
        }

        public void RestoreLayout()
        {
            string path = LayoutService.GetPath(KEY);
            if (!File.Exists(path))
            {
                ResetLayout();
            }
            try
            {
                _dockManager.RestoreLayoutFromXml(path);
            }
            catch (Exception)
            {
                ResetLayout();
                _dockManager.RestoreLayoutFromXml(path);
            }
            _sensorsBtn.Checked = DockVisibility.Hidden != _sensorPanel.Visibility;
        }

        public void SaveLayout()
        {
            _dockManager.SaveLayoutToXml(LayoutService.GetPath(KEY));
        }

        public static void ResetLayout()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(Resources.Layout_Online);
            string path = LayoutService.GetPath(KEY);
            doc.Save(path);
        }

        private void init()
        {
            _infoPanel.Text = Resources.Information;
            lbiTools.Caption = Resources.Tools;
            _labelItem.Caption = Resources.Online_Capture;
            _runButton.Caption = Resources.Online_Start;
            _autoZoomBtn.Caption = Resources.AutoZoom;
            _autoZoomBtn.Hint = Resources.Online_AutoZoomHint;
            _sensorsBtn.Caption = Resources.Online_SensorsState;
            _sensorsBtn.Hint = Resources.Online_SensorsStateHint;
            _tailItem.Caption = Resources.Online_Tail;
            _tailItem.Hint = Resources.Online_TailHint;
            _resetBtn.Caption = Resources.DefaultView;
            _resetBtn.Hint = Resources.Online_DefaultViewHint;
            _gisTab.Text = Resources.MapUkrGIS;
            _googleTab.Text = Resources.MapWeb;
            biAddressFinder.Caption = Resources.AddressSearch;

            _info = new InfoControl(_vehicleTree);
            _info.Dock = DockStyle.Fill;
            infoPanel_Container.Controls.Add(_info);
            
            _gMap.MapType = MapType.GoogleMap;
        }

        #region --   GUI Handlers

        /// <summary>
        /// ������������ ���� �� ������ "����"
        /// </summary>
        private void _runButton_ItemClick(object sender, ItemClickEventArgs e)
        {
            RunItemClicked();
        }

        /// <summary>
        /// ������������ ���� �� ������ �������������������
        /// </summary>
        private void _autoZoomBtn_CheckedChanged(object sender, ItemClickEventArgs e)
        {
            AutoZoomItemClicked();
        }

        /// <summary>
        /// ������������ ���� �� ������ ��������
        /// </summary>
        private void _sensorsBtn_CheckedChanged(object sender, ItemClickEventArgs e)
        {
            if (_sensorsBtn.Checked)
            {
                _sensorPanel.Show();
            }
            else
            {
                _sensorPanel.Hide();
            }
        }

        /// <summary>
        /// ������������ ��������� �������� � ���� "�����"
        /// </summary>
        private void _tailItem_EditValueChanged(object sender, EventArgs e)
        {
            TailValueChanged();
        }

        /// <summary>
        /// ������������ ���� �� ������ "��� �� ���������"
        /// </summary>
        private void _resetBtn_ItemClick(object sender, ItemClickEventArgs e)
        {
            ResetLayout();
            RestoreLayout();
        }

        /// <summary>
        /// ������������ ��� ������������ ����� �������� ����
        /// </summary>
        private void _xtraTabs_SelectedPageChanged(object sender, TabPageChangedEventArgs e)
        {
            _map.ClearZones();
            _map.ClearMarkers();
            _map.ClearTracks();
            IMap map = (IMap) e.Page.Tag;
            map.State = _map.State;
            _map = map;
            MapChanged();
        }

        /// <summary>
        /// ������������ ������� ���� �� ������ � ����� ��������� ������������ �������
        /// </summary>
        /// <param name="point">��������� �� ����� ������������� ��������</param>
        private void _info_Selected(PointLatLng point)
        {
            DetailsRowDoubleClicked(point);
        }

        /// <summary>
        /// ������������ ��� ��������� ��������� ������ ��������
        /// </summary>
        private void _sensorPanel_VisibilityChanged(object sender, VisibilityChangedEventArgs e)
        {
            _sensorsBtn.Checked = DockVisibility.Hidden != _sensorPanel.Visibility;
        }

        #endregion

        private void biAddressFinder_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (RunAddressFinder != null)
            {
                _map.ResetTools();
                RunAddressFinder();
            }
        }
    }
}