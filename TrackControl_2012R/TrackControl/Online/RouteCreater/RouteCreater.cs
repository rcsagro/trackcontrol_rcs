using System;
using System.Collections.Generic;
using System.Windows.Forms;
using TrackControl.General;
using TrackControl.Properties;

namespace TrackControl.Reports
{
    public partial class RouteCreater : DevExpress.XtraEditors.XtraForm
    {
        public event VoidHandler CreateRoute;
        public event VoidHandler ClearRoute;
        public event VoidHandler StopRoute;
        public event Action<int> ChangeMapSource;

        public RouteCreater()
        {
            InitializeComponent();
            Localization();
        }

        void RouteCreater_FormClosed(object sender, FormClosedEventArgs e)
        {
            //_map.ClearAll();
            //_map.IsRouteMode = false;
            //_map.OnPointClicked -= addPointToCollaction;
           if (StopRoute!=null) StopRoute();
        }

        void gvPoints_CustomDrawRowIndicator(object sender, DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventArgs e)
        {
            if (e.Info.IsRowIndicator)
            {
                e.Info.DisplayText = (e.RowHandle + 1).ToString();
            }
        }

        void btCreateRoute_Click(object sender, EventArgs e)
        {
            if (CreateRoute != null) CreateRoute();
        }

        void btClearRoute_Click(object sender, EventArgs e)
        {
            if (ClearRoute != null) ClearRoute();
        }

        void Localization()
        {
            colLocation.Caption = Resources.Location;
            colDistance.Caption = Resources.PathKm;
            colLat.Caption = Resources.Latitude;
            colLng.Caption = Resources.Longitude;
            lbInfor.Text = Resources.CreateRouteMsg;
            btCreateRoute.Text = Resources.Create;   
            btClearRoute.Text = Resources.Clear;
            this.Text = Resources.CreateRoute;
            
        }

        private void rgMapType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ChangeMapSource!=null) ChangeMapSource((int)rgMapType.EditValue);
        }

        public void SetDataSource(List<RoutePoint> rpoints)
        {
            gcPoints.DataSource = rpoints;
        }

        public void RefreshDataSource()
        {
            gcPoints.RefreshDataSource();
        }

    }
}