﻿using System;
using System.Collections.Generic;
using System.Text;
using TrackControl.General;

namespace TrackControl.Reports
{
    public class RoutePoint 
    {
        public double Lng
        {
            get { return _lng; }
        }
        double _lng;

        public double Lat
        {
            get { return _lat; }
        }
        double _lat;

        string _location;
        public string Location
        {
            get { return _location; }
            set { _location = value; }
        }

        public RoutePoint(double lat, double lng)
        {
            _lat = lat;
            _lng = lng;
            _point = new PointLatLng(_lat, _lng); 
        }

        PointLatLng _point;

        public PointLatLng Point
        {
            get { return _point; }
        }

        double distance;

        public double Distance
        {
            get { return distance; }
            set { distance = value; }
        }
        Marker markerPoint;

        public Marker MarkerPoint
        {
            get { return markerPoint; }
            set { markerPoint = value; }
        }
    }
}
