﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using TrackControl.General;
using TrackControl.GMap;
using TrackControl.GMap.Core;
using TrackControl.GMap.MapProviders;
using TrackControl.Properties;
using TrackControl.Vehicles;

namespace TrackControl.Reports
{
    public class RouterCreatorController
    {
        List<RoutePoint> _rpoints;
        GoogleMapControl _map;
        RouteCreater _view;
        int _mapSource = (int)MapType.OpenStreetMap;
        Form _ownerView;

        public RouterCreatorController(GoogleMapControl map, Form owner)
        {
            _map = map;
            _map.OnPointClicked += OnAddPointToCollaction;
            _map.DrawTrackMode = DrawTrackModes.Route;
            _ownerView = owner;
        }

        public void Start()
        {
            _rpoints = new List<RoutePoint>();
            _map.ClearAll();
            _view = new RouteCreater();
            _view.CreateRoute += OnCreateRoute;
            _view.ClearRoute += OnClearRoute;
            _view.StopRoute += OnStopRoute;
            _view.ClearRoute += OnClearRoute;
            _view.ChangeMapSource  += OnChangeMapSource;
            _view.SetDataSource(_rpoints);
            _view.Owner = _ownerView;
            _view.Show(); 
        }

        private void OnAddPointToCollaction(PointLatLng pll)
        {
            RoutePoint rp = new RoutePoint(pll.Lat, pll.Lng);
            GetAddress(rp);
            Marker m = new Marker(MarkerType.Pin, 0, new PointLatLng(pll.Lat, pll.Lng), string.Format("{0} # {1}", Resources.Point, _rpoints.Count), "");
            _map.AddMarker(m);
            rp.MarkerPoint = m;
            _rpoints.Add(rp);
            _view.RefreshDataSource(); 
        }

        private void GetAddress(RoutePoint rp)
        {
            Placemark pos = null;
            GeoCoderStatusCode status = GeoCoderStatusCode.Unknow;
            //if (_mapSource == (int)MapType.GoogleMap)
            //{
            //    pos = GMapProviders.GoogleMap.GetPlacemark(rp.Point, out status);
            //}
            //else if (_mapSource == (int)MapType.OpenStreetMap)
            //{
            GMapProviders.SetStartParams();
            if(GMapProviders.getOpenStreetMap.IsActive)
            {
                pos = GMapProviders.getOpenStreetMap.GetPlacemarkOSM(rp.Point, out status);
            }
            else if (GMapProviders.getRcsNominatim.IsActive)
            {
                pos = GMapProviders.getRcsNominatim.GetPlacemark(rp.Point, out status);
            }
            //}
            if (status == GeoCoderStatusCode.G_GEO_SUCCESS && pos != null) 
                rp.Location = pos.Address;
        }
        private void CreateTrack(List<PointLatLng> points)
        {
            List<IGeoPoint> g_points = new List<IGeoPoint>();
            PointLatLng point_prev = PointLatLng.IncorrectPoint;
            foreach (PointLatLng point in points)
            {
                GpsData g_point = new GpsData();
                g_point.LatLng = point;
                g_points.Add(g_point);
                point_prev = point;
            }
            Track tr = new Track(1, g_points);
            tr.IsActive = true;
            _map.AddTrack(tr);
        }

        private List<PointLatLng> GetRoutePoints()
        {
            _map.ClearAll();
            RoutePoint rp_prev = null;
            List<PointLatLng> points = new List<PointLatLng>();
            if (_rpoints != null)
            {
                double dist = 0;
                foreach (RoutePoint rp in _rpoints)
                {
                    if (rp_prev != null)
                    {
                        List<PointLatLng> pointsGMap = GMaps.Instance.GetRouteBetweenPoints(rp_prev.Point, rp.Point, false, _map.Zoom, _mapSource);
                        if (pointsGMap != null)
                        {
                            points.AddRange(pointsGMap);
                            dist += СGeoDistance.GetDistance(pointsGMap);
                        }
                    }
                    rp.Distance = Math.Round(dist, 2);
                    SetMarkerTitle(rp);
                    _map.AddMarker(rp.MarkerPoint);
                    rp_prev = rp;
                }
            }
            return points;
        }

        private void SetMarkerTitle(RoutePoint rp)
        {
            int newLine = rp.MarkerPoint.Title.IndexOf(Environment.NewLine);
            if (newLine > 0) rp.MarkerPoint.Title = rp.MarkerPoint.Title.Substring(0, newLine);
            rp.MarkerPoint.Title = string.Format("{0}{2}{1} {3}", rp.MarkerPoint.Title, rp.Distance.ToString(), Environment.NewLine, "km");
        }

        void OnCreateRoute()
        {
            if (_rpoints.Count == 0) return;
            List<PointLatLng> points = GetRoutePoints();
            if (points.Count == 0) return;
            CreateTrack(points);
            _view.RefreshDataSource();
        }
        
        void OnClearRoute()
        {
            _rpoints.Clear();
            _view.RefreshDataSource(); 
            _map.ClearAll();
        }

        void OnStopRoute()
        {
            _map.ClearAll();
            _map.DrawTrackMode = DrawTrackModes.Simple  ;
            _map.OnPointClicked -= OnAddPointToCollaction;
        }

        void OnChangeMapSource(int source)
        {
            _mapSource = source; 
        }
    }
}
