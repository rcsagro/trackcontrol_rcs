namespace TrackControl.Online
{
  partial class OnlineForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OnlineForm));
            this._barManager = new DevExpress.XtraBars.BarManager(this.components);
            this._tools = new DevExpress.XtraBars.Bar();
            this._labelItem = new DevExpress.XtraBars.BarStaticItem();
            this._runButton = new DevExpress.XtraBars.BarLargeButtonItem();
            this._autoZoomBtn = new DevExpress.XtraBars.BarCheckItem();
            this._sensorsBtn = new DevExpress.XtraBars.BarCheckItem();
            this._tailItem = new DevExpress.XtraBars.BarEditItem();
            this._tailRepo = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this._resetBtn = new DevExpress.XtraBars.BarLargeButtonItem();
            this.lbiTools = new DevExpress.XtraBars.BarLargeButtonItem();
            this.ppTools = new DevExpress.XtraBars.PopupMenu(this.components);
            this.biAddressFinder = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this._dockManager = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this._sensorPanel = new DevExpress.XtraBars.Docking.DockPanel();
            this.sensorPanel_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this._details = new TrackControl.Controls.SensorsView();
            this._images = new DevExpress.Utils.ImageCollection(this.components);
            this._noticeLog = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel1_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this._infoPanel = new DevExpress.XtraBars.Docking.DockPanel();
            this.infoPanel_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.panelContainer1 = new DevExpress.XtraBars.Docking.DockPanel();
            this._vehiclePanel = new DevExpress.XtraBars.Docking.DockPanel();
            this.controlContainer1 = new DevExpress.XtraBars.Docking.ControlContainer();
            this._zonesPanel = new DevExpress.XtraBars.Docking.DockPanel();
            this.controlContainer2 = new DevExpress.XtraBars.Docking.ControlContainer();
            this._xtraTabs = new DevExpress.XtraTab.XtraTabControl();
            this._gisTab = new DevExpress.XtraTab.XtraTabPage();
            this._googleTab = new DevExpress.XtraTab.XtraTabPage();
            ((System.ComponentModel.ISupportInitialize)(this._barManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._tailRepo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ppTools)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._dockManager)).BeginInit();
            this._sensorPanel.SuspendLayout();
            this.sensorPanel_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._images)).BeginInit();
            this._noticeLog.SuspendLayout();
            this._infoPanel.SuspendLayout();
            this.panelContainer1.SuspendLayout();
            this._vehiclePanel.SuspendLayout();
            this._zonesPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._xtraTabs)).BeginInit();
            this._xtraTabs.SuspendLayout();
            this.SuspendLayout();
            // 
            // _barManager
            // 
            this._barManager.AllowCustomization = false;
            this._barManager.AllowMoveBarOnToolbar = false;
            this._barManager.AllowQuickCustomization = false;
            this._barManager.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this._tools,
            this.bar3});
            this._barManager.DockControls.Add(this.barDockControlTop);
            this._barManager.DockControls.Add(this.barDockControlBottom);
            this._barManager.DockControls.Add(this.barDockControlLeft);
            this._barManager.DockControls.Add(this.barDockControlRight);
            this._barManager.DockManager = this._dockManager;
            this._barManager.Form = this;
            this._barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this._labelItem,
            this._runButton,
            this._autoZoomBtn,
            this._sensorsBtn,
            this._tailItem,
            this._resetBtn,
            this.lbiTools,
            this.biAddressFinder});
            this._barManager.MaxItemId = 8;
            this._barManager.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this._tailRepo});
            this._barManager.StatusBar = this.bar3;
            // 
            // _tools
            // 
            this._tools.BarName = "Tools";
            this._tools.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Top;
            this._tools.DockCol = 0;
            this._tools.DockRow = 0;
            this._tools.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this._tools.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this._labelItem),
            new DevExpress.XtraBars.LinkPersistInfo(this._runButton, true),
            new DevExpress.XtraBars.LinkPersistInfo(this._autoZoomBtn),
            new DevExpress.XtraBars.LinkPersistInfo(this._sensorsBtn),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.Width, this._tailItem, "", true, true, true, 55),
            new DevExpress.XtraBars.LinkPersistInfo(this._resetBtn),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.lbiTools, "", true, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this._tools.OptionsBar.AllowQuickCustomization = false;
            this._tools.OptionsBar.DisableClose = true;
            this._tools.OptionsBar.DisableCustomization = true;
            this._tools.OptionsBar.DrawDragBorder = false;
            this._tools.OptionsBar.UseWholeRow = true;
            this._tools.Text = "Tools";
            // 
            // _labelItem
            // 
            this._labelItem.Border = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this._labelItem.Caption = "��������";
            this._labelItem.Glyph = ((System.Drawing.Image)(resources.GetObject("_labelItem.Glyph")));
            this._labelItem.Id = 0;
            this._labelItem.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._labelItem.ItemAppearance.Normal.Options.UseFont = true;
            this._labelItem.Name = "_labelItem";
            this._labelItem.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this._labelItem.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // _runButton
            // 
            this._runButton.Caption = "��������";
            this._runButton.Glyph = ((System.Drawing.Image)(resources.GetObject("_runButton.Glyph")));
            this._runButton.Id = 1;
            this._runButton.Name = "_runButton";
            this._runButton.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionInMenu;
            this._runButton.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this._runButton_ItemClick);
            // 
            // _autoZoomBtn
            // 
            this._autoZoomBtn.Caption = "�������������������";
            this._autoZoomBtn.Glyph = ((System.Drawing.Image)(resources.GetObject("_autoZoomBtn.Glyph")));
            this._autoZoomBtn.Hint = "�������� / ��������� �������������������\n��� ������ ������������ �������";
            this._autoZoomBtn.Id = 2;
            this._autoZoomBtn.Name = "_autoZoomBtn";
            this._autoZoomBtn.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionInMenu;
            this._autoZoomBtn.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this._autoZoomBtn_CheckedChanged);
            // 
            // _sensorsBtn
            // 
            this._sensorsBtn.Caption = "��������� ��������";
            this._sensorsBtn.Glyph = ((System.Drawing.Image)(resources.GetObject("_sensorsBtn.Glyph")));
            this._sensorsBtn.Hint = "���������� / �������� ������ ���������\n�������� ���������� �������������\n��������" +
    "";
            this._sensorsBtn.Id = 3;
            this._sensorsBtn.Name = "_sensorsBtn";
            this._sensorsBtn.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionInMenu;
            this._sensorsBtn.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this._sensorsBtn_CheckedChanged);
            // 
            // _tailItem
            // 
            this._tailItem.Caption = "\"�����\"";
            this._tailItem.Edit = this._tailRepo;
            this._tailItem.Hint = "���������� ����� ��� �����������\n���������� ������� ����";
            this._tailItem.Id = 4;
            this._tailItem.Name = "_tailItem";
            this._tailItem.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this._tailItem.EditValueChanged += new System.EventHandler(this._tailItem_EditValueChanged);
            // 
            // _tailRepo
            // 
            this._tailRepo.AutoHeight = false;
            this._tailRepo.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this._tailRepo.Increment = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this._tailRepo.IsFloatValue = false;
            this._tailRepo.Mask.EditMask = "N00";
            this._tailRepo.MaxLength = 3;
            this._tailRepo.MaxValue = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this._tailRepo.MinValue = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this._tailRepo.Name = "_tailRepo";
            // 
            // _resetBtn
            // 
            this._resetBtn.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this._resetBtn.Caption = "��� �� ���������";
            this._resetBtn.Glyph = ((System.Drawing.Image)(resources.GetObject("_resetBtn.Glyph")));
            this._resetBtn.Hint = "������������ ������� ���, �������� �� ���������";
            this._resetBtn.Id = 5;
            this._resetBtn.Name = "_resetBtn";
            this._resetBtn.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this._resetBtn_ItemClick);
            // 
            // lbiTools
            // 
            this.lbiTools.ActAsDropDown = true;
            this.lbiTools.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.lbiTools.Caption = "�����������";
            this.lbiTools.CaptionAlignment = DevExpress.XtraBars.BarItemCaptionAlignment.Right;
            this.lbiTools.DropDownControl = this.ppTools;
            this.lbiTools.Glyph = ((System.Drawing.Image)(resources.GetObject("lbiTools.Glyph")));
            this.lbiTools.Id = 6;
            this.lbiTools.Name = "lbiTools";
            // 
            // ppTools
            // 
            this.ppTools.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.biAddressFinder)});
            this.ppTools.Manager = this._barManager;
            this.ppTools.Name = "ppTools";
            // 
            // biAddressFinder
            // 
            this.biAddressFinder.Caption = "�������� �����";
            this.biAddressFinder.Glyph = ((System.Drawing.Image)(resources.GetObject("biAddressFinder.Glyph")));
            this.biAddressFinder.Id = 7;
            this.biAddressFinder.Name = "biAddressFinder";
            this.biAddressFinder.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.biAddressFinder_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DisableClose = true;
            this.bar3.OptionsBar.DisableCustomization = true;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            this.bar3.Visible = false;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(849, 36);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 521);
            this.barDockControlBottom.Size = new System.Drawing.Size(849, 23);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 36);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 485);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(849, 36);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 485);
            // 
            // _dockManager
            // 
            this._dockManager.DockingOptions.ShowCaptionImage = true;
            this._dockManager.DockingOptions.ShowMaximizeButton = false;
            this._dockManager.Form = this;
            this._dockManager.HiddenPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this._sensorPanel});
            this._dockManager.Images = this._images;
            this._dockManager.MenuManager = this._barManager;
            this._dockManager.RootPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this._noticeLog,
            this._infoPanel,
            this.panelContainer1});
            this._dockManager.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "DevExpress.XtraBars.StandaloneBarDockControl",
            "System.Windows.Forms.StatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl"});
            // 
            // _sensorPanel
            // 
            this._sensorPanel.Controls.Add(this.sensorPanel_Container);
            this._sensorPanel.Dock = DevExpress.XtraBars.Docking.DockingStyle.Float;
            this._sensorPanel.FloatLocation = new System.Drawing.Point(511, 253);
            this._sensorPanel.FloatSize = new System.Drawing.Size(257, 200);
            this._sensorPanel.ID = new System.Guid("e984c416-4eae-49ff-8120-db7258f53986");
            this._sensorPanel.ImageIndex = 2;
            this._sensorPanel.Location = new System.Drawing.Point(-32768, -32768);
            this._sensorPanel.Name = "_sensorPanel";
            this._sensorPanel.OriginalSize = new System.Drawing.Size(200, 200);
            this._sensorPanel.SavedIndex = 2;
            this._sensorPanel.Size = new System.Drawing.Size(257, 200);
            this._sensorPanel.Text = "�������";
            this._sensorPanel.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden;
            // 
            // sensorPanel_Container
            // 
            this.sensorPanel_Container.Controls.Add(this._details);
            this.sensorPanel_Container.Location = new System.Drawing.Point(2, 24);
            this.sensorPanel_Container.Name = "sensorPanel_Container";
            this.sensorPanel_Container.Size = new System.Drawing.Size(253, 174);
            this.sensorPanel_Container.TabIndex = 0;
            // 
            // _details
            // 
            this._details.Dock = System.Windows.Forms.DockStyle.Fill;
            this._details.Location = new System.Drawing.Point(0, 0);
            this._details.Name = "_details";
            this._details.Size = new System.Drawing.Size(253, 174);
            this._details.TabIndex = 0;
            // 
            // _images
            // 
            this._images.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("_images.ImageStream")));
            this._images.Images.SetKeyName(0, "Vehicles");
            this._images.Images.SetKeyName(1, "Zones");
            this._images.Images.SetKeyName(2, "Gauge");
            this._images.Images.SetKeyName(3, "Info");
            this._images.Images.SetKeyName(4, "UkrGis");
            this._images.Images.SetKeyName(5, "GMap");
            this._images.Images.SetKeyName(6, "notebook--exclamation.png");
            // 
            // _noticeLog
            // 
            this._noticeLog.Controls.Add(this.dockPanel1_Container);
            this._noticeLog.Dock = DevExpress.XtraBars.Docking.DockingStyle.Right;
            this._noticeLog.ID = new System.Guid("50e9de9f-9919-456c-880a-8a3f9999c410");
            this._noticeLog.ImageIndex = 6;
            this._noticeLog.Location = new System.Drawing.Point(649, 36);
            this._noticeLog.Name = "_noticeLog";
            this._noticeLog.OriginalSize = new System.Drawing.Size(200, 200);
            this._noticeLog.Size = new System.Drawing.Size(200, 485);
            this._noticeLog.TabText = "�����������";
            this._noticeLog.Text = "�����������";
            // 
            // dockPanel1_Container
            // 
            this.dockPanel1_Container.Location = new System.Drawing.Point(4, 23);
            this.dockPanel1_Container.Name = "dockPanel1_Container";
            this.dockPanel1_Container.Size = new System.Drawing.Size(192, 458);
            this.dockPanel1_Container.TabIndex = 0;
            // 
            // _infoPanel
            // 
            this._infoPanel.Controls.Add(this.infoPanel_Container);
            this._infoPanel.Dock = DevExpress.XtraBars.Docking.DockingStyle.Bottom;
            this._infoPanel.ID = new System.Guid("408677cf-5705-4c11-a8b6-2ccf40551cdd");
            this._infoPanel.ImageIndex = 3;
            this._infoPanel.Location = new System.Drawing.Point(0, 321);
            this._infoPanel.Name = "_infoPanel";
            this._infoPanel.Options.AllowFloating = false;
            this._infoPanel.Options.FloatOnDblClick = false;
            this._infoPanel.Options.ShowCloseButton = false;
            this._infoPanel.Options.ShowMaximizeButton = false;
            this._infoPanel.OriginalSize = new System.Drawing.Size(200, 200);
            this._infoPanel.Size = new System.Drawing.Size(649, 200);
            this._infoPanel.TabText = "����";
            this._infoPanel.Text = "����������";
            // 
            // infoPanel_Container
            // 
            this.infoPanel_Container.Location = new System.Drawing.Point(4, 23);
            this.infoPanel_Container.Name = "infoPanel_Container";
            this.infoPanel_Container.Size = new System.Drawing.Size(641, 173);
            this.infoPanel_Container.TabIndex = 0;
            // 
            // panelContainer1
            // 
            this.panelContainer1.ActiveChild = this._vehiclePanel;
            this.panelContainer1.Controls.Add(this._vehiclePanel);
            this.panelContainer1.Controls.Add(this._zonesPanel);
            this.panelContainer1.Dock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.panelContainer1.FloatVertical = true;
            this.panelContainer1.ID = new System.Guid("948e3d60-82e3-46a8-ba1a-ed3fc9dbd30e");
            this.panelContainer1.Location = new System.Drawing.Point(0, 36);
            this.panelContainer1.Name = "panelContainer1";
            this.panelContainer1.Options.AllowFloating = false;
            this.panelContainer1.Options.FloatOnDblClick = false;
            this.panelContainer1.Options.ShowCloseButton = false;
            this.panelContainer1.Options.ShowMaximizeButton = false;
            this.panelContainer1.OriginalSize = new System.Drawing.Size(323, 200);
            this.panelContainer1.Size = new System.Drawing.Size(323, 285);
            this.panelContainer1.Tabbed = true;
            this.panelContainer1.Text = "panelContainer1";
            // 
            // _vehiclePanel
            // 
            this._vehiclePanel.Controls.Add(this.controlContainer1);
            this._vehiclePanel.Dock = DevExpress.XtraBars.Docking.DockingStyle.Fill;
            this._vehiclePanel.FloatVertical = true;
            this._vehiclePanel.ID = new System.Guid("6086065b-a20e-4500-b3fd-841b50c75a41");
            this._vehiclePanel.ImageIndex = 0;
            this._vehiclePanel.Location = new System.Drawing.Point(4, 23);
            this._vehiclePanel.Name = "_vehiclePanel";
            this._vehiclePanel.Options.AllowFloating = false;
            this._vehiclePanel.Options.FloatOnDblClick = false;
            this._vehiclePanel.Options.ShowCloseButton = false;
            this._vehiclePanel.Options.ShowMaximizeButton = false;
            this._vehiclePanel.OriginalSize = new System.Drawing.Size(317, 240);
            this._vehiclePanel.Size = new System.Drawing.Size(315, 230);
            this._vehiclePanel.TabText = "����";
            this._vehiclePanel.Text = "������������ ��������";
            // 
            // controlContainer1
            // 
            this.controlContainer1.Location = new System.Drawing.Point(0, 0);
            this.controlContainer1.Name = "controlContainer1";
            this.controlContainer1.Size = new System.Drawing.Size(315, 230);
            this.controlContainer1.TabIndex = 0;
            // 
            // _zonesPanel
            // 
            this._zonesPanel.Controls.Add(this.controlContainer2);
            this._zonesPanel.Dock = DevExpress.XtraBars.Docking.DockingStyle.Fill;
            this._zonesPanel.ID = new System.Guid("d6425caa-4811-4c2b-88bf-48aced316441");
            this._zonesPanel.ImageIndex = 1;
            this._zonesPanel.Location = new System.Drawing.Point(4, 23);
            this._zonesPanel.Name = "_zonesPanel";
            this._zonesPanel.Options.AllowFloating = false;
            this._zonesPanel.Options.FloatOnDblClick = false;
            this._zonesPanel.Options.ShowCloseButton = false;
            this._zonesPanel.Options.ShowMaximizeButton = false;
            this._zonesPanel.OriginalSize = new System.Drawing.Size(317, 240);
            this._zonesPanel.Size = new System.Drawing.Size(315, 230);
            this._zonesPanel.TabText = "����";
            this._zonesPanel.Text = "����������� ����";
            // 
            // controlContainer2
            // 
            this.controlContainer2.Location = new System.Drawing.Point(0, 0);
            this.controlContainer2.Name = "controlContainer2";
            this.controlContainer2.Size = new System.Drawing.Size(315, 230);
            this.controlContainer2.TabIndex = 0;
            // 
            // _xtraTabs
            // 
            this._xtraTabs.Dock = System.Windows.Forms.DockStyle.Fill;
            this._xtraTabs.Images = this._images;
            this._xtraTabs.Location = new System.Drawing.Point(323, 36);
            this._xtraTabs.Margin = new System.Windows.Forms.Padding(0);
            this._xtraTabs.Name = "_xtraTabs";
            this._xtraTabs.SelectedTabPage = this._gisTab;
            this._xtraTabs.Size = new System.Drawing.Size(326, 285);
            this._xtraTabs.TabIndex = 5;
            this._xtraTabs.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this._gisTab,
            this._googleTab});
            this._xtraTabs.SelectedPageChanged += new DevExpress.XtraTab.TabPageChangedEventHandler(this._xtraTabs_SelectedPageChanged);
            // 
            // _gisTab
            // 
            this._gisTab.ImageIndex = 4;
            this._gisTab.Name = "_gisTab";
            this._gisTab.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this._gisTab.Size = new System.Drawing.Size(320, 254);
            this._gisTab.Text = "����� UkrGIS";
            // 
            // _googleTab
            // 
            this._googleTab.ImageIndex = 5;
            this._googleTab.Name = "_googleTab";
            this._googleTab.Size = new System.Drawing.Size(320, 254);
            this._googleTab.Text = "����� Web";
            // 
            // OnlineForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(849, 544);
            this.Controls.Add(this._xtraTabs);
            this.Controls.Add(this.panelContainer1);
            this.Controls.Add(this._infoPanel);
            this.Controls.Add(this._noticeLog);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "OnlineForm";
            this.Text = "OnlineForm";
            ((System.ComponentModel.ISupportInitialize)(this._barManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._tailRepo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ppTools)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._dockManager)).EndInit();
            this._sensorPanel.ResumeLayout(false);
            this.sensorPanel_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._images)).EndInit();
            this._noticeLog.ResumeLayout(false);
            this._infoPanel.ResumeLayout(false);
            this.panelContainer1.ResumeLayout(false);
            this._vehiclePanel.ResumeLayout(false);
            this._zonesPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._xtraTabs)).EndInit();
            this._xtraTabs.ResumeLayout(false);
            this.ResumeLayout(false);

    }

    #endregion

    private DevExpress.XtraBars.BarManager _barManager;
    private DevExpress.XtraBars.Bar _tools;
    private DevExpress.XtraBars.Bar bar3;
    private DevExpress.XtraBars.BarDockControl barDockControlTop;
    private DevExpress.XtraBars.BarDockControl barDockControlBottom;
    private DevExpress.XtraBars.BarDockControl barDockControlLeft;
    private DevExpress.XtraBars.BarDockControl barDockControlRight;
    private DevExpress.XtraBars.Docking.DockManager _dockManager;
    private DevExpress.XtraTab.XtraTabControl _xtraTabs;
    private DevExpress.XtraTab.XtraTabPage _gisTab;
    private DevExpress.XtraTab.XtraTabPage _googleTab;
    private DevExpress.XtraBars.Docking.DockPanel panelContainer1;
    private DevExpress.XtraBars.Docking.DockPanel _zonesPanel;
    private DevExpress.XtraBars.Docking.ControlContainer controlContainer2;
    private DevExpress.XtraBars.Docking.DockPanel _vehiclePanel;
    private DevExpress.XtraBars.Docking.ControlContainer controlContainer1;
    private DevExpress.XtraBars.Docking.DockPanel _infoPanel;
    private DevExpress.XtraBars.Docking.ControlContainer infoPanel_Container;
    private DevExpress.XtraBars.BarStaticItem _labelItem;
    private DevExpress.XtraBars.BarLargeButtonItem _runButton;
    private DevExpress.XtraBars.BarCheckItem _autoZoomBtn;
    private DevExpress.XtraBars.BarCheckItem _sensorsBtn;
    private DevExpress.XtraBars.BarEditItem _tailItem;
    private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit _tailRepo;
    private DevExpress.XtraBars.Docking.DockPanel _sensorPanel;
    private DevExpress.XtraBars.Docking.ControlContainer sensorPanel_Container;
    private TrackControl.Online.InfoControl _info;
    private TrackControl.Controls.SensorsView _details;
    private DevExpress.XtraBars.BarLargeButtonItem _resetBtn;
    private DevExpress.Utils.ImageCollection _images;
    private DevExpress.XtraBars.BarLargeButtonItem lbiTools;
    private DevExpress.XtraBars.PopupMenu ppTools;
    private DevExpress.XtraBars.BarButtonItem biAddressFinder;
    private DevExpress.XtraBars.Docking.DockPanel _noticeLog;
    private DevExpress.XtraBars.Docking.ControlContainer dockPanel1_Container;
  }
}