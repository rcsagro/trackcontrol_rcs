using System;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
using System.Configuration;
using DevExpress.XtraEditors;
using LocalCache;
using LocalCache.atlantaDataSetTableAdapters;
using MySql.Data.MySqlClient;
using TrackControl.General.DatabaseDriver;
using TrackControl.Properties;

namespace TrackControl.Setting
{
    [ToolboxItem(false)]
    public partial class SettingSensorsControl : UserControl
    {
        private bool startEditMode;
        private bool startEditRelateAlgorithm;

        /// <summary>
        /// ������� ��� ���������� ��������� � �� �������� ��������.
        /// </summary>
        private SensorsTableAdapter _sensorAdapter = new SensorsTableAdapter();

        private RelationalgorithmsTableAdapter _relationalgorithmAdapter = new RelationalgorithmsTableAdapter();
        private SensorcoefficientTableAdapter _sensorcoefficientAdapter = new SensorcoefficientTableAdapter();
        private StateTableAdapter _stateTableAdapter = new StateTableAdapter();
        private TransitionTableAdapter _transitionTableAdapter = new TransitionTableAdapter();

        //ComboBox algoritmComboBox = new ComboBox();

        public SettingSensorsControl()
        {
            InitializeComponent();
            if (this.DesignMode)
                return;
            init();

            mobitelsDataGridView.Dock = DockStyle.Fill;
            vehicleBindingNavigator.Visible = true;
            mobitelsDataGridView.Visible = true;
        }

        private void SettingsSensorControl_Load(object sender, EventArgs e)
        {
            if (DesignMode)
                return;
            
            sensorsDataGridView.Visible = false;
            atlantaDataSet = AppModel.Instance.DataSet;
            mobitelsBindingSource.DataSource = atlantaDataSet.mobitels;
            sensoralgorithmsBindingSource.DataSource = atlantaDataSet.sensoralgorithms;
            relationalgorithmsBindingSource.DataSource = atlantaDataSet.relationalgorithms;
            sensorcoefficientBindingSource.DataSource = atlantaDataSet.sensorcoefficient;
            vehicleBindingSource.DataSource = atlantaDataSet.vehicle;
            sensorsDataGridView.Visible = true;
            //       Refresh();

            string cs = Crypto.GetDecryptConnectionString(ConfigurationManager.ConnectionStrings["CS"].ConnectionString);

            DriverDb db0 = new DriverDb();
            db0.AdapNewConnection(cs);
            _sensorAdapter.Connection = db0.Connection; //new MySqlConnection(cs);

            DriverDb db1 = new DriverDb();
            db1.AdapNewConnection(cs);
            _relationalgorithmAdapter.Connection = db1.Connection; //new MySqlConnection(cs);

            DriverDb db2 = new DriverDb();
            db2.AdapNewConnection(cs);
            _sensorcoefficientAdapter.Connection = db2.Connection; //new MySqlConnection(cs);

            DriverDb db3 = new DriverDb();
            db3.AdapNewConnection(cs);
            _stateTableAdapter.Connection = db3.Connection; //new MySqlConnection(cs);

            DriverDb db4 = new DriverDb();
            db4.AdapNewConnection(cs);
            _transitionTableAdapter.Connection = db4.Connection; //new MySqlConnection(cs);

            _sensorcoefficientAdapter.Fill(atlantaDataSet.sensorcoefficient);

            columnAlgoritmComboBox.Items.Clear();
            foreach (
                atlantaDataSet.sensoralgorithmsRow saRow in
                    (atlantaDataSet.sensoralgorithmsDataTable) sensoralgorithmsBindingSource.DataSource)
                //sensoralgorithmsTableAdapter.GetData())
            {
                columnAlgoritmComboBox.Items.Add(saRow.Name); // algoritmComboBox.Items.Add(saRow.Name);
            }
        }

        private void refreshToolStripButton_Click(object sender, EventArgs e)
        {
            SettingsSensorControl_Load(this, null);
        }

        /// <summary>
        /// ���������� ������ ������� ����������
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mobitelsBindingSource_CurrentChanged(object sender, EventArgs e)
        {
            startEditMode = false;
            startEditRelateAlgorithm = false;
        }

        private void sensorsDataGridView_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            try
            {
                if (sensorsDataGridView == null || e == null)
                    return;
                int COL_SENSORID_INDEX = sensorsDataGridView.Columns["columnID"].Index;
                // ����� ����� �� ������ ���������
                if (e.ColumnIndex == sensorsDataGridView.Columns["columnCalibration"].Index && e.RowIndex >= 0)
                {
                    string title = null;
                    try
                    {
                        DataRow[] Rows = atlantaDataSet.sensorcoefficient.Select(string.Format("Sensor_ID={0}",
                            sensorsDataGridView[COL_SENSORID_INDEX, e.RowIndex].Value.ToString()));
                        if (Rows != null && Rows.Length > 0)
                            title = string.Format("{0}...", Rows.Length);
                        else
                            title = "X";
                    }
                    catch (Exception)
                    {
                        title = "!";
                    }
                    sensorsDataGridView[e.ColumnIndex, e.RowIndex].Value = title;
                }
                // ��������� comboBox � ����������� (� �������� ������)
                if (e.ColumnIndex == sensorsDataGridView.Columns["columnAlgoritmComboBox"].Index && e.RowIndex >= 0)
                {
                    int sensorID = (int) sensorsDataGridView.Rows[e.RowIndex].Cells["columnID"].Value;
                    foreach (atlantaDataSet.relationalgorithmsRow rel_algRow in
                        (atlantaDataSet.relationalgorithmsDataTable) atlantaDataSet.relationalgorithms)
                    {
                        if (rel_algRow.SensorID == sensorID)
                        {
                            int alg_ID = rel_algRow.AlgorithmID;
                            atlantaDataSet.sensoralgorithmsDataTable sensDT = atlantaDataSet.sensoralgorithms;
                            atlantaDataSet.sensoralgorithmsRow[] salg_row =
                                (atlantaDataSet.sensoralgorithmsRow[]) sensDT.Select("AlgorithmID = " + alg_ID);
                            string value = (string) (salg_row[0]["Name"]);
                            sensorsDataGridView[e.ColumnIndex, e.RowIndex].Value = value;
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "painting Error");
            }
        }

        private void sensorsDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (sensorsBindingSource.Current == null || e.RowIndex < 0)
                return;

            DataRowView rvView = (DataRowView) sensorsBindingSource.Current;
            int idRow = (int)rvView.Row["id"];

            atlantaDataSet.sensorsRow sensor = (atlantaDataSet.sensorsRow) (rvView).Row;

            if (e.ColumnIndex == sensorsDataGridView.Columns["States"].Index)
            {
                SensorStateFormDvx sensorStatesForm = new SensorStateFormDvx(_stateTableAdapter, _transitionTableAdapter,
                    atlantaDataSet, sensor);
                sensorStatesForm.ShowDialog(this);
            }
            else if (e.ColumnIndex == sensorsDataGridView.Columns["Transitions"].Index)
            {
                atlantaDataSet.StateRow[] states = sensor.GetStateRows();

                if (states.Length > 1)
                {
                    SensorTransitionFormDvx sensorTransitionForm = new SensorTransitionFormDvx(_transitionTableAdapter,atlantaDataSet, sensor);
                    sensorTransitionForm.ShowDialog(this);
                }
                else
                {
                    MessageBox.Show(Resources.Err_NotEnoughStatesForTransition);
                }
            }
            else if (e.ColumnIndex == sensorsDataGridView.Columns["columnCalibration"].Index)
            {
                ShowTarirovka(sensor.id);
            }
        }

        private void ShowTarirovka(int sensorid)
        {
            sensorsBindingSource.EndEdit();
            sensorcoefficientBindingSource.EndEdit();
            ExcelFormDvx form = new ExcelFormDvx();
            form.atlDataSet = atlantaDataSet;
            form.sensorIdentificator = sensorid;
            sensorcoefficientBindingSource.Filter = String.Format( "Sensor_id = {0}", sensorid );
            form.bindingSource = sensorcoefficientBindingSource;
            form.ShowDialog( this );
            sensorcoefficientBindingSource.EndEdit();
            _sensorcoefficientAdapter.Update( atlantaDataSet.sensorcoefficient );
            _sensorcoefficientAdapter.Fill( atlantaDataSet.sensorcoefficient );
        }

        private void sensorsDataGridView_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            startEditMode = false;
            if (e.ColumnIndex == sensorsDataGridView.Columns["columnAlgoritmComboBox"].Index && e.RowIndex >= 0)
            {
                startEditRelateAlgorithm = true;
            }
            else
            {
                startEditRelateAlgorithm = false;
                if (e.ColumnIndex != sensorsDataGridView.Columns["columnCalibration"].Index && e.RowIndex >= 0)
                    startEditMode = true;
            }
        }

        private void sensorsDataGridView_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            XtraMessageBox.Show(e.Exception.Message, e.Context.ToString());

            if ((e.Exception) is ConstraintException)
            {
                DataGridView view = (DataGridView) sender;
                view.Rows[e.RowIndex].ErrorText = "an error";
                view.Rows[e.RowIndex].Cells[e.ColumnIndex].ErrorText = "an error";
                e.ThrowException = false;
            }
        }

        private void buttonRefresh_Click(object sender, EventArgs e)
        {
            atlantaDataSet.RejectChanges();
        }

        private void bindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {
            if (sensorsDataGridView.CurrentRow == null)
            {
                XtraMessageBox.Show(Resources.RowForDeletingNotSelected, "DeleteRow Error");
                return;
            }
            try
            {
                int sensorID = (int) this.sensorsDataGridView.CurrentRow.Cells["columnID"].Value;
                DataRow[] delDataRow = atlantaDataSet.relationalgorithms.Select("SensorID = " + sensorID);
                if (delDataRow.Length > 0)
                {
                    foreach (DataRow tmpD_row in delDataRow)
                        tmpD_row.Delete();
                    relationalgorithmsBindingSource.EndEdit();
                    _relationalgorithmAdapter.Update(atlantaDataSet.relationalgorithms);
                    _relationalgorithmAdapter.Fill(atlantaDataSet.relationalgorithms);
                }
                delDataRow = atlantaDataSet.sensors.Select("id = " + sensorID);
                if (delDataRow.Length > 0)
                {
                    foreach (DataRow tmpD_row in delDataRow)
                        tmpD_row.Delete();
                    sensorsBindingSource.EndEdit();
                    _transitionTableAdapter.Update(atlantaDataSet.Transition);
                    _stateTableAdapter.Update(atlantaDataSet.State);
                    _sensorAdapter.Update(atlantaDataSet.sensors);
                    atlantaDataSet.AcceptChanges();
                }
                delDataRow = atlantaDataSet.sensorcoefficient.Select("Sensor_id = " + sensorID);
                if (delDataRow.Length > 0)
                {
                    foreach (DataRow tmpD_row in delDataRow)
                    {
                        tmpD_row.Delete();
                    }
                    sensorcoefficientBindingSource.EndEdit();
                    _sensorcoefficientAdapter.Update(atlantaDataSet.sensorcoefficient);
                    _sensorcoefficientAdapter.Fill(atlantaDataSet.sensorcoefficient);
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.StackTrace, "DeleteRow Error");
            }
            //sensorsDataGridView.Update();
            sensorsDataGridView.CurrentCell = null;
        }

        private void bindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            try
            {
                sensorsBindingSource.SuspendBinding();
                atlantaDataSet.sensorsRow newSens_row = (atlantaDataSet.sensorsRow) atlantaDataSet.sensors.NewRow();
                newSens_row.Description = "";
                newSens_row.K = 1;
                newSens_row.Length = 8;
                newSens_row.mobitel_id =
                    ((atlantaDataSet.mobitelsRow) ((DataRowView) this.mobitelsBindingSource.Current).Row).Mobitel_ID;
                newSens_row.NameUnit = "";
                newSens_row.StartBit = 0;
                newSens_row.Name = "";
                newSens_row.MobitelName = "";
                newSens_row.MobitelDescr = "";

                atlantaDataSet.sensors.AddsensorsRow(newSens_row);
                sensorsBindingSource.ResumeBinding();
                sensorsBindingSource.EndEdit();
                _sensorAdapter.Update(atlantaDataSet.sensors);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message + " " + ex.InnerException, "AddNewRow Error");
            }

            atlantaDataSet.AcceptChanges();
        }

        private void changeRelateAlgoritm(object sender, DataGridViewCellEventArgs e)
        {
            //SAVE CHANGE
            int new_algorithmID = -1;
            foreach (atlantaDataSet.sensoralgorithmsRow sa_row in
                ((atlantaDataSet.sensoralgorithmsDataTable) sensoralgorithmsBindingSource.DataSource).Rows)
            {
                if (sa_row.Name == (string) sensorsDataGridView[e.ColumnIndex, e.RowIndex].Value)
                {
                    new_algorithmID = sa_row.AlgorithmID;
                    break;
                }
            }
            try
            {
                relationalgorithmsBindingSource.SuspendBinding();
                DataRow[] data_row = ((atlantaDataSet.relationalgorithmsDataTable)
                    relationalgorithmsBindingSource.DataSource).
                    Select("SensorID = " + sensorsDataGridView.CurrentRow.Cells["columnID"].Value);
                if (data_row.Length > 0)
                {
                    ((atlantaDataSet.relationalgorithmsRow) data_row[0]).AlgorithmID = new_algorithmID;
                }
                else
                {
//���� � ��� ��� ������ ��������� ��������� - �� ������� ����� ������ � ������� ������
                    atlantaDataSet.relationalgorithmsRow relalg_row =
                        (atlantaDataSet.relationalgorithmsRow) atlantaDataSet.relationalgorithms.NewRow();
                    relalg_row.AlgorithmID = new_algorithmID;
                    relalg_row.SensorID = (int) sensorsDataGridView.CurrentRow.Cells["columnID"].Value;
                    atlantaDataSet.relationalgorithms.AddrelationalgorithmsRow(relalg_row);
                }
            }
            finally
            {
                relationalgorithmsBindingSource.ResumeBinding();
                relationalgorithmsBindingSource.EndEdit();
                //SAVE
                _relationalgorithmAdapter.Update(atlantaDataSet.relationalgorithms);
                _relationalgorithmAdapter.Fill(atlantaDataSet.relationalgorithms);
            }
        }

        private void sensorsDataGridView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (startEditRelateAlgorithm)
            {
                if (e.ColumnIndex == sensorsDataGridView.Columns["columnAlgoritmComboBox"].Index && e.RowIndex >= 0)
                {
                    changeRelateAlgoritm(sender, e);
                    startEditRelateAlgorithm = false;
                }
            }
            if (startEditMode)
            {
                sensorsBindingSource.EndEdit();
                _sensorAdapter.Update(atlantaDataSet.sensors);
                startEditMode = false;
            }
        }

        private void mobitelsDataGridView_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            if (e.RowIndex >= 0 && e.RowCount > 1)
            {
                for (int i = 0; i <= e.RowCount; i++)
                {
                    LocalCache.atlantaDataSet.vehicleRow[] vehicl_rows =
                        atlantaDataSet.mobitels.FindByMobitel_ID((int) mobitelsDataGridView["ColumnMobitelID", i].Value)
                            .GetvehicleRows();
                    if (vehicl_rows.Length > 0)
                    {
                        mobitelsDataGridView["ColumnNumber", i].Value = vehicl_rows[0].NumberPlate;
                        mobitelsDataGridView["ColumnMarkAndModel", i].Value = vehicl_rows[0].MakeCar;
                    }
                }
            }
        }

        // �������� ����� ������
        private void mobitelsDataGridView_SelectionChanged(object sender, EventArgs e)
        {
            int mob_id = -1;
            if (vehicleBindingSource.Current != null)
            {
                mob_id = ((atlantaDataSet.vehicleRow) (((DataRowView) vehicleBindingSource.Current).Row)).Mobitel_id;
                int itemFound = mobitelsBindingSource.Find("Mobitel_ID", mob_id);
                mobitelsBindingSource.Position = itemFound;
            }
        }

        private void init()
        {
            teletrekLabel.Text = Resources.Mobitels;
            nameDataGridViewTextBoxColumn.HeaderText = Resources.Title;
            columnStartBit.HeaderText = Resources.StartBit;
            columnLength.HeaderText = Resources.Length;
            columnNameUnit.HeaderText = Resources.UnitName;
            columnCalibration.HeaderText = Resources.Calibration;
            States.HeaderText = Resources.States;
            States.Text = Resources.Edit;
            States.ToolTipText = Resources.States_EditHint;
            Transitions.HeaderText = Resources.Transitions;
            Transitions.Text = Resources.Edit;
            Transitions.ToolTipText = Resources.Transitions_EditHint;
            columnDescription.HeaderText = Resources.Description;
            columnAlgoritmComboBox.HeaderText = Resources.Assignment;
            buttonRefresh.Text = Resources.Refresh;
            ColumnNumber.HeaderText = Resources.CarNumber;
            ColumnMarkAndModel.HeaderText = Resources.CarMark;
            ColumnModel.HeaderText = Resources.CarModel;

        }
    }
}
