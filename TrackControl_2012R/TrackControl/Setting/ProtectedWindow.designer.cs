namespace TrackControl.Setting.Controls
{
    partial class ProtectedWindow
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.warningPanel = new System.Windows.Forms.Panel();
            this.warningTable = new System.Windows.Forms.TableLayoutPanel();
            this.exitButton = new System.Windows.Forms.Panel();
            this.ExitLabel = new System.Windows.Forms.Label();
            this.warningLabel = new System.Windows.Forms.Label();
            this.contentPanel = new System.Windows.Forms.Panel();
            this.mainTableLayoutPanel.SuspendLayout();
            this.warningPanel.SuspendLayout();
            this.warningTable.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainTableLayoutPanel
            // 
            this.mainTableLayoutPanel.ColumnCount = 1;
            this.mainTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.mainTableLayoutPanel.Controls.Add(this.warningPanel, 0, 0);
            this.mainTableLayoutPanel.Controls.Add(this.contentPanel, 0, 1);
            this.mainTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainTableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.mainTableLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
            this.mainTableLayoutPanel.Name = "mainTableLayoutPanel";
            this.mainTableLayoutPanel.RowCount = 2;
            this.mainTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.mainTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.mainTableLayoutPanel.Size = new System.Drawing.Size(765, 537);
            this.mainTableLayoutPanel.TabIndex = 0;
            // 
            // warningPanel
            // 
            this.warningPanel.BackColor = System.Drawing.Color.PeachPuff;
            this.warningPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.warningPanel.Controls.Add(this.warningTable);
            this.warningPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.warningPanel.Location = new System.Drawing.Point(0, 0);
            this.warningPanel.Margin = new System.Windows.Forms.Padding(0);
            this.warningPanel.Name = "warningPanel";
            this.warningPanel.Size = new System.Drawing.Size(765, 26);
            this.warningPanel.TabIndex = 0;
            // 
            // warningTable
            // 
            this.warningTable.ColumnCount = 3;
            this.warningTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.warningTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.warningTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.warningTable.Controls.Add(this.exitButton, 2, 0);
            this.warningTable.Controls.Add(this.ExitLabel, 1, 0);
            this.warningTable.Controls.Add(this.warningLabel, 0, 0);
            this.warningTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.warningTable.Location = new System.Drawing.Point(0, 0);
            this.warningTable.Name = "warningTable";
            this.warningTable.Padding = new System.Windows.Forms.Padding(3);
            this.warningTable.RowCount = 1;
            this.warningTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.warningTable.Size = new System.Drawing.Size(763, 24);
            this.warningTable.TabIndex = 0;
            // 
            // exitButton
            // 
            this.exitButton.BackColor = System.Drawing.Color.Transparent;
            this.exitButton.BackgroundImage = global::TrackControl.Properties.Resources.exit;
            this.exitButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.exitButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.exitButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.exitButton.Location = new System.Drawing.Point(738, 3);
            this.exitButton.Margin = new System.Windows.Forms.Padding(0);
            this.exitButton.Name = "exitButton";
            this.exitButton.Size = new System.Drawing.Size(22, 18);
            this.exitButton.TabIndex = 0;
            this.exitButton.Click += new System.EventHandler(this.logOut);
            // 
            // ExitLabel
            // 
            this.ExitLabel.AutoSize = true;
            this.ExitLabel.BackColor = System.Drawing.Color.Transparent;
            this.ExitLabel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ExitLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ExitLabel.Font = new System.Drawing.Font("Arial", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ExitLabel.ForeColor = System.Drawing.Color.DarkRed;
            this.ExitLabel.Location = new System.Drawing.Point(671, 3);
            this.ExitLabel.Name = "ExitLabel";
            this.ExitLabel.Size = new System.Drawing.Size(64, 18);
            this.ExitLabel.TabIndex = 1;
            this.ExitLabel.Text = "�����";
            this.ExitLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.ExitLabel.Click += new System.EventHandler(this.logOut);
            // 
            // warningLabel
            // 
            this.warningLabel.AutoSize = true;
            this.warningLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.warningLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.warningLabel.Location = new System.Drawing.Point(3, 3);
            this.warningLabel.Margin = new System.Windows.Forms.Padding(0);
            this.warningLabel.Name = "warningLabel";
            this.warningLabel.Size = new System.Drawing.Size(665, 18);
            this.warningLabel.TabIndex = 2;
            this.warningLabel.Text = "�� �������� ��������� ����� ��������������";
            this.warningLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // contentPanel
            // 
            this.contentPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.contentPanel.Location = new System.Drawing.Point(0, 26);
            this.contentPanel.Margin = new System.Windows.Forms.Padding(0);
            this.contentPanel.Name = "contentPanel";
            this.contentPanel.Size = new System.Drawing.Size(765, 511);
            this.contentPanel.TabIndex = 1;
            // 
            // ProtectedWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.mainTableLayoutPanel);
            this.Name = "ProtectedWindow";
            this.Size = new System.Drawing.Size(765, 537);
            this.mainTableLayoutPanel.ResumeLayout(false);
            this.warningPanel.ResumeLayout(false);
            this.warningTable.ResumeLayout(false);
            this.warningTable.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel mainTableLayoutPanel;
        private System.Windows.Forms.Panel warningPanel;
        private System.Windows.Forms.TableLayoutPanel warningTable;
        private System.Windows.Forms.Panel exitButton;
        private System.Windows.Forms.Label ExitLabel;
        private System.Windows.Forms.Label warningLabel;
        protected System.Windows.Forms.Panel contentPanel;

    }
}
