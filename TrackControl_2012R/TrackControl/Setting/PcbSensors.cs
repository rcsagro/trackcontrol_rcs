﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Drawing;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Agro;
using DevExpress.XtraBars.Docking2010;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Base;
using SensorAdministratorCAN;
using TrackControl.General.DatabaseDriver;
using TrackControl.General.DAL;
using TrackControl.Properties;
using System.Xml;
using System.Xml.Serialization;
using System.Runtime.Serialization.Formatters.Soap;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid.Views.Grid;
using LocalCache;
using TrackControl.Temp;

namespace TrackControl.Setting
{
    public partial class PcbSensors : UserControl
    {
        [Serializable]
        public class PcbVersion
        {
            private int ID;
            private string Name;
            private string Comment;
            private int iCountClasses;

            public PcbVersion(int id, string name, string comment, int numberClasses)
            {
                this.ID = id;
                this.Name = name;
                this.Comment = comment;
                this.iCountClasses = numberClasses;
            }

            public int NumberClasses
            {
                get
                {
                    return this.iCountClasses;
                }
                set
                {
                    this.iCountClasses = value;
                }
            }

            public int IDPcb
            {
                get
                {
                    return this.ID;
                }
            }

            public string nmPcbs
            {
                get
                {
                    return this.Name;
                }

                set
                {
                    if (value != null)
                    {
                        if (value.Length > 50)
                        {
                            XtraMessageBox.Show(Resources.ErrorLengthType, Resources.ErrorPcb,
                                MessageBoxButtons.OK);
                            return;
                        }

                        this.Name = value;
                    }
                    else
                        this.Name = "****";
                }
            }

            public string Comms
            {
                get
                {
                    return this.Comment;
                }

                set
                {
                    if (value != null)
                    {
                        if( value.Length > 50 )
                        {
                            XtraMessageBox.Show( Resources.ErrorLengthComment, Resources.ErrorPcb,
                                MessageBoxButtons.OK );
                            return;
                        }

                        this.Comment = value;
                    }
                    else
                        this.Comment = "";
                }
            }
        }

        [Serializable]
        public class PcbDataVersion
        {
            private int Id;
            private string TypeData;
            private double KoeffK;
            private int StartBit;
            private int LengthBit;
            private string Comment;
            private int IDPcb;
            private int iCountClasses;
            private string nameValue;
            private int numberAlgorithm;
            private string algorithm;

            public PcbDataVersion(int id, string typedata, double koeffK, int startBit, int lengthBit, string comment, int idPcb, 
                int numberClasses, string nameVal, string nameAlgo, int numAlgo)
            {
                this.Id = id;
                this.TypeData = typedata;
                this.KoeffK = koeffK;
                this.StartBit = startBit;
                this.LengthBit = lengthBit;
                this.Comment = comment;
                this.IDPcb = idPcb;
                this.iCountClasses = numberClasses;
                nameValue = nameVal;
                numberAlgorithm = numAlgo;
                algorithm = nameAlgo;
            }

            public int NumberClassesData
            {
                get
                {
                    return this.iCountClasses;
                }
                set
                {
                    this.iCountClasses = value;
                }
            }

            public int ID
            {
                get { return this.Id; }
            }

            public string TypeDataPcb
            {
                get { return this.TypeData; }
                set
                {
                    if (value == null)
                        this.TypeData = "*";
                    else
                    {
                        if( value.Length > 255 )
                        {
                            XtraMessageBox.Show( Resources.ErrorLengthName, Resources.ErrorPcb,
                                MessageBoxButtons.OK );
                            return;
                        }

                        this.TypeData = value;
                    }
                }
            }

            public string KoefficK
            {
                get { return this.KoeffK.ToString(); }
                set
                {
                   this.KoeffK = Convert.ToDouble(value);
                }
            }

            public int StrtBit
            {
                get { return this.StartBit; }
                set
                {
                    if ((value + this.StrtBit) > 63)
                    {
                        // to do
                    }
                        //throw new Exception("Field width bits under the border 63 bit!");
                    this.StartBit = value;
                }
            }

            public int LngthBit
            {
                get
                {
                    return this.LengthBit;
                }
                set
                {
                    if ((value + this.StrtBit) > 63)
                    {
                        // to do
                    }
                    // throw new Exception("Field width bits under the border 63 bit!");

                    this.LengthBit = value;
                }
            }

            public string Commentary
            {
                get { return this.Comment; }

                set
                {
                    if (value == null)
                    {
                        this.Comment = "";
                    }
                    else
                    {
                        if (value.Length > 255)
                        {
                            XtraMessageBox.Show( Resources.ErrorLenthEcrise, Resources.ErrorPcb,
                                MessageBoxButtons.OK);
                            return;
                        }

                        this.Comment = value;
                    }
                }
            }

            public string NameValueMeasure
            {
                get { return nameValue; }
                set { nameValue = value; }
            }

            public string Algorithm
            {
                get { return algorithm; }
                set { algorithm = value; }
            }

            public int NumberAlgorithm
            {
                get { return numberAlgorithm; }
                set { numberAlgorithm = value; }
            }

            public int ID_Pcb
            {
                get { return this.IDPcb; }
                set
                {
                    this.IDPcb = value;
                }
            }
        }

        BindingList<PcbVersion> pcbVersionsList = new BindingList<PcbVersion>();
        BindingList<PcbDataVersion> pcbData = new BindingList<PcbDataVersion>();
        private int focusedRowChanged = -1;
        private int focusRowChange = -1;
        private SettingSensorUser settingSensorsUser = null;
        private LocalCache.atlantaDataSet atlantaDataSet;

        public PcbSensors()
        {
            InitializeComponent();
            atlantaDataSet = AppModel.Instance.DataSet;

            gridViewPcbList.FocusedRowChanged += gridViewPcbList_FocusedRowChanged;
            gridControlPcbList.EmbeddedNavigator.ButtonClick += EmbeddedNavigator_ButtonClick;
            gridControlPcbList.UseEmbeddedNavigator = true;
            gridControlPcbList.EmbeddedNavigator.Buttons.Append.Visible = false;
            gridControlPcbList.EmbeddedNavigator.Buttons.Append.Hint = Resources.RememebeData;
            gridControlPcbList.EmbeddedNavigator.Buttons.Remove.Hint = Resources.DeleteRow;
            gridControlPcbList.EmbeddedNavigator.Buttons.ImageList = imageList1;
            NavigatorCustomButton button = gridControlPcbList.EmbeddedNavigator.Buttons.CustomButtons.Add();
            button.ImageIndex = 2;
            button.Tag = "Adding";
            button.Hint = Resources.AddNewData;

            NavigatorCustomButton butt = gridControlPcbList.EmbeddedNavigator.Buttons.CustomButtons.Add();
            butt.ImageIndex = 3;
            butt.Tag = "Reload";
            butt.Hint = Resources.ReloadData;

            gridViewPcb.FocusedRowChanged += gridViewPcb_FocusedRowChanged;
            gridControlPcb.EmbeddedNavigator.ButtonClick += EmbeddedNavigator_BttnClick;
            gridControlPcb.UseEmbeddedNavigator = true;
            gridControlPcb.EmbeddedNavigator.Buttons.Append.Visible = false;
            gridControlPcb.EmbeddedNavigator.Buttons.Append.Hint = Resources.RememebeData;
            gridControlPcb.EmbeddedNavigator.Buttons.Remove.Hint = Resources.DeleteRow;
            gridControlPcb.EmbeddedNavigator.Buttons.ImageList = imageList1;
            NavigatorCustomButton bttn = gridControlPcb.EmbeddedNavigator.Buttons.CustomButtons.Add();
            bttn.ImageIndex = 2;
            bttn.Tag = "Adding";
            bttn.Hint = Resources.AddNewData;
            NavigatorCustomButton btn = gridControlPcb.EmbeddedNavigator.Buttons.CustomButtons.Add();
            btn.ImageIndex = 3;
            btn.Tag = "Reload";
            btn.Hint = Resources.ReloadData;

            btnExportesXml.Click += btnExportXml_Click;
            btnImportesXml.Click += btnImportXml_Click;

            gridViewPcb.CustomRowCellEdit += gridViewPcb_CustomRowCellEdit;

            RepositoryItemComboBox comboAlgorithmEdit =
               gridViewPcb.Columns["Algorithm"].ColumnEdit as RepositoryItemComboBox;

            settingSensorsUser = new SettingSensorUser();
            settingSensorsUser.InitialComboBoxAlgorithm(comboAlgorithmEdit);

            Localization();
        }

        void gridViewPcb_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            if (e.Column.FieldName == "Algorithm")
            {
                // to do
            }
        }

        private void Localization()
        {
            lblListPcb.Text = Resources.TextPcb;
            lblInfo.Text = Resources.TextPcbData;
            namePcbs.Caption = Resources.NamePcbs;
            namePcbs.ToolTip = Resources.NamePcbs;
            Comment.Caption = Resources.CommPcb;
            Comment.ToolTip = Resources.CommPcb;
            nameTypeData.Caption = Resources.NameTypeData;
            nameTypeData.ToolTip = Resources.NameTypeData;
            koeffK.Caption = Resources.Koefficient;
            koeffK.ToolTip = Resources.Koefficient;
            strtBit.Caption = Resources.StartBitPcb;
            strtBit.ToolTip = Resources.StartBitPcb;
            lnghtBit.Caption = Resources.LengthBitPcb;
            lnghtBit.ToolTip = Resources.LengthBitPcb;
            Commt.Caption = Resources.NotePcb;
            Commt.ToolTip = Resources.NotePcb;
            btnExportesXml.Text = Resources.ImportXml;
            btnExportesXml.ToolTip = Resources.ImportXmlTip;
            btnImportesXml.Text = Resources.ExportXml;
            btnImportesXml.ToolTip = Resources.ExportXmlTip;
        }

        private void gridViewPcb_FocusedRowChanged( object sender, FocusedRowChangedEventArgs e )
        {
            focusRowChange = e.FocusedRowHandle;
        }

        private void EmbeddedNavigator_BttnClick( object sender, NavigatorButtonClickEventArgs e )
        {
            try
            {
                if( "Adding".Equals( e.Button.Tag ) )
                {
                    if( gridControlPcb.FocusedView != null )
                    {
                        int id = Convert.ToInt32(gridViewPcbList.GetRowCellValue(gridViewPcbList.FocusedRowHandle, "IDPcb").ToString());
                        PcbDataVersion versPcbAdd = new PcbDataVersion(0, "*", 0.0, 0, 0, "", id, 0, "#", "--Алгоритм не выбран--", 0);
                        SaveIntoDataBase( versPcbAdd );
                        PcbDataGetFromDb( gridViewPcbList.GetRow( gridViewPcbList.FocusedRowHandle ) );
                        gridViewPcb.RefreshData();
                        int handleLastRow = gridViewPcb.GetRowHandle( gridViewPcb.RowCount - 1 );
                        gridViewPcb.FocusedRowHandle = handleLastRow;
                        gridViewPcb.SelectRow( handleLastRow );
                        e.Handled = true;
                    }
                }
                else if ("Reload".Equals(e.Button.Tag))
                {
                    PcbDataGetFromDb( gridViewPcbList.GetRow( gridViewPcbList.FocusedRowHandle ) );
                }
                else if (e.Button.ButtonType == NavigatorButtonType.EndEdit)
                {
                    SaveEditPcbTable();
                    gridViewPcb.RefreshData();

                    e.Handled = true;
                }
                else if (e.Button.ButtonType == NavigatorButtonType.Remove)
                {
                    if (MessageBox.Show(Resources.DeleteRowQuest, Resources.WarningPcb,
                        MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No)
                    {
                        e.Handled = true;
                        return;
                    }


                    if (gridViewPcb.RowCount == 1)
                        focusRowChange = 0;

                    RemovePcbRow(focusRowChange);
                    focusRowChange = -1;
                    PcbDataGetFromDb(gridViewPcbList.GetRow(gridViewPcbList.FocusedRowHandle));
                    gridViewPcb.RefreshData();
                    e.Handled = true;
                }
            }
            catch( Exception ex )
            {
                XtraMessageBox.Show( ex.Message, Resources.ErrorPcb, MessageBoxButtons.OK );
            }
        }

        int GetAlgorithmID(string algorithmName)
        {
            foreach (atlantaDataSet.sensoralgorithmsRow row in atlantaDataSet.sensoralgorithms)
            {
                if (row.Name == algorithmName)
                {
                    return row.AlgorithmID;
                }
            }

            return 0;
        }

        string GetAlgorithmName(int algorithmID)
        {
            foreach (atlantaDataSet.sensoralgorithmsRow row in atlantaDataSet.sensoralgorithms)
            {
                if (row.AlgorithmID == algorithmID)
                {
                    return row.Name;
                }
            }

            return String.Empty;
        }

        private void SaveEditPcbTable()
        {
            DriverDb db = null;
            try
            {
                db = new DriverDb();
                db.ConnectDb();
                string sqlUpdate = TrackControlQuery.PcbSensors.UpdatePcbData;

                for( int i = 0; i < gridViewPcb.RowCount; i++ )
                {
                    string koeffk = gridViewPcb.GetRowCellValue( i, "KoefficK" ).ToString();
                    int index = koeffk.IndexOf(",");
                    string temp = "";

                    if( index >= 0 )
                    {
                        temp = koeffk.Substring(0, index);
                        temp = temp + ".";
                        temp = temp + koeffk.Substring(index + 1, koeffk.Length - index - 1);
                        koeffk = temp;
                    }

                    int k = 0;
                    string nameAlg = gridViewPcb.GetRowCellValue(i, "Algorithm").ToString();
                    int algID = GetAlgorithmID(nameAlg);
                    string update = string.Format( sqlUpdate, gridViewPcb.GetRowCellValue( i, "ID" ), gridViewPcb.GetRowCellValue( i, "TypeDataPcb" ), koeffk,
                        gridViewPcb.GetRowCellValue( i, "StrtBit" ), gridViewPcb.GetRowCellValue( i, "LngthBit" ), gridViewPcb.GetRowCellValue( i, "Commentary" ),
                        gridViewPcb.GetRowCellValue(i, "NameValueMeasure"), algID);
                    db.ExecuteNonQueryCommand( update );
                }

                db.CloseDbConnection();
            }
            catch( Exception e )
            {
                if(db != null)
                    db.CloseDbConnection();

                XtraMessageBox.Show( e.Message, Resources.ErrorPcb, MessageBoxButtons.OK );
            }
        }

        private void RemovePcbRow( int focusRow )
        {
            if( focusRow == -1 )
                return;

            object hndl = gridViewPcb.GetRowCellValue( focusRow, "ID" );

            if( hndl == null )
                return;

            DriverDb db = new DriverDb();
            db.ConnectDb();

            string sqlRemove = TrackControlQuery.PcbSensors.DeleteFromPcbData + Convert.ToInt32( hndl.ToString() );

            db.ExecuteNonQueryCommand( sqlRemove );
            db.CloseDbConnection();
        }

        private void SaveIntoDataBase( PcbDataVersion versPcbAdd )
        {
            string sqlInsert = string.Format( TrackControlQuery.PcbSensors.InsertIntoPcbData, versPcbAdd.TypeDataPcb,
                versPcbAdd.KoefficK, versPcbAdd.StrtBit, versPcbAdd.LngthBit, versPcbAdd.Commentary, versPcbAdd.ID_Pcb, versPcbAdd.NameValueMeasure, versPcbAdd.NumberAlgorithm);
            DriverDb db = new DriverDb();
            db.ConnectDb();
            db.ExecuteNonQueryCommand( sqlInsert );
            db.CloseDbConnection();
        }

        private void EmbeddedNavigator_ButtonClick( object sender, NavigatorButtonClickEventArgs e )
        {
            try
            {
                if ("Adding".Equals(e.Button.Tag))
                {
                    if (gridControlPcbList.FocusedView != null)
                    {
                        PcbVersion versAdd = new PcbVersion(0, "SCAD_MR vers_01e", "для версии исполнения ПД SCAD", 0);
                        SaveIntoDB(versAdd);
                        GetPcbData();
                        gridViewPcbList.RefreshData();
                        int handleLastRow = gridViewPcbList.GetRowHandle(gridViewPcbList.RowCount - 1);
                        gridViewPcbList.FocusedRowHandle = handleLastRow;
                        gridViewPcbList.SelectRow(handleLastRow);
                        VehiclesModelInitializer.initPcbVersion( VehiclesModelInitializer.GetVehiclesModel );
                        e.Handled = true;
                    }
                }
                else if( "Reload".Equals( e.Button.Tag ) )
                {
                    GetPcbData();
                }
                else if (e.Button.ButtonType == NavigatorButtonType.EndEdit)
                {
                    SaveEditTable();
                    gridViewPcbList.RefreshData();
                   
                    object info = gridViewPcbList.GetRowCellValue( focusedRowChanged, "nmPcbs" );

                    if (info == null)
                    {
                        e.Handled = true;
                        return;
                    }

                    lblName.Text = info.ToString();
                    PcbDataGetFromDb( gridViewPcbList.GetRow( focusedRowChanged ) );
                    VehiclesModelInitializer.initPcbVersion( VehiclesModelInitializer.GetVehiclesModel );

                    e.Handled = true;
                }
                else if (e.Button.ButtonType == NavigatorButtonType.Remove)
                {
                    if (MessageBox.Show(Resources.DeleteRowQuest, Resources.WarningPcb,
                        MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No)
                    {
                        e.Handled = true;
                        return;
                    }

                    PcbVersion fRow = ( PcbVersion )gridViewPcbList.GetRow( gridViewPcbList.FocusedRowHandle );

                    RemovePcb(focusedRowChanged);
                    GetPcbData();
                    gridViewPcbList.RefreshData();
                    if (gridViewPcbList.FocusedRowHandle < 0)
                    {
                        PcbDataGetFromDb( fRow );
                    }
                    else
                    {
                        PcbDataGetFromDb( gridViewPcbList.GetRow( gridViewPcbList.FocusedRowHandle ) );
                    }
                    
                    gridViewPcb.RefreshData();
                    e.Handled = true;
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, Resources.ErrorPcb, MessageBoxButtons.OK);
            }
        }

        private void SaveEditTable()
        {
            DriverDb db = null;
            try
            {
                db = new DriverDb();
                db.ConnectDb();
                string sqlUpdate = TrackControlQuery.PcbSensors.UpdatePcbVersion;

                for (int i = 0; i < pcbVersionsList.Count; i++)
                {
                    PcbVersion pcbVer = pcbVersionsList[i];
                    string update = string.Format(sqlUpdate, pcbVer.IDPcb, pcbVer.nmPcbs, pcbVer.Comms);
                    db.ExecuteNonQueryCommand(update);
                }

                db.CloseDbConnection();
            }
            catch (Exception e)
            {
                if(db != null)
                    db.CloseDbConnection();

                XtraMessageBox.Show( e.Message, Resources.ErrorPcb, MessageBoxButtons.OK );
            }
        }

        private void SaveIntoDB( PcbVersion versAdd )
        {
            string sqlInsert = string.Format( TrackControlQuery.PcbSensors.InsertIntoPcbVersion, versAdd.nmPcbs, versAdd.Comms );
            DriverDb db = new DriverDb();
            db.ConnectDb();
            db.ExecuteNonQueryCommand( sqlInsert );
            db.CloseDbConnection();
        }

        private void RemovePcb( int focusedRowChanged )
        {
            if( focusedRowChanged == -1 )
                return;

            object hndl = gridViewPcbList.GetRowCellValue( focusedRowChanged, "IDPcb" );

            if( hndl == null )
                return;

            string sqlRemove = "";
            DriverDb db = new DriverDb();
            db.ConnectDb();

            sqlRemove = TrackControlQuery.PcbSensors.DeletePcbDataIdPcb + hndl;
            db.ExecuteNonQueryCommand(sqlRemove);
        
            sqlRemove = TrackControlQuery.PcbSensors.DeleteFromPcbVersion + hndl;
            db.ExecuteNonQueryCommand( sqlRemove );

            sqlRemove = string.Format(TrackControlQuery.PcbSensors.SelectVehiclePcbVersionId, hndl);
            DataTable tbl = db.GetDataTable(sqlRemove);

            string sql = TrackControlQuery.PcbSensors.UpdateVehicleSet;
            for(int k = 0; k < tbl.Rows.Count; k++)
            {
                string sqlr = string.Format(sql, tbl.Rows[k]["PcbVersionId"]);
                db.ExecuteNonQueryCommand( sqlr );
            }

            db.CloseDbConnection();

            VehiclesModelInitializer.initPcbVersion(VehiclesModelInitializer.GetVehiclesModel);
            VehiclesModelInitializer.RefreshVehicleProperty();
        }

        private void gridViewPcbList_FocusedRowChanged( object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e )
        {
            focusedRowChanged = e.FocusedRowHandle;

            object info = gridViewPcbList.GetRowCellValue( e.FocusedRowHandle, "nmPcbs" );

            if( info == null )
                return;

            lblName.Text = info.ToString();
            PcbDataGetFromDb( gridViewPcbList.GetRow(e.FocusedRowHandle) );
        }

        public void GetPcbData()
        {
            DriverDb db = null;

            try
            {
                db = new DriverDb();
                db.ConnectDb();
                DataTable tblpcb = db.GetDataTable(TrackControlQuery.PcbSensors.SelectFromPcbVersion);

                pcbVersionsList.Clear();
                gridControlPcbList.DataSource = null;
                for (int i = 0; i < tblpcb.Rows.Count; i++)
                {
                    PcbVersion pcbVersion = new PcbVersion(Convert.ToInt32(tblpcb.Rows[i]["Id"].ToString()), tblpcb.Rows[i]["Name"].ToString(),
                        tblpcb.Rows[i]["Comment"].ToString(), tblpcb.Rows.Count );
                    pcbVersionsList.Add(pcbVersion);
                }

                gridControlPcbList.DataSource = pcbVersionsList;

                if( pcbVersionsList.Count <= 0 )
                {
                    gridControlPcbList.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = false;
                    gridControlPcb.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = false;
                    gridControlPcb.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = false;
                    lblName.Text = "";
                }
                else
                {
                    gridControlPcbList.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = true;
                    gridControlPcb.EmbeddedNavigator.Buttons.CustomButtons[0].Enabled = true;
                    gridControlPcb.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = true;
                }

                db.CloseDbConnection();
            }
            catch (Exception e)
            {
                if(db != null)
                    db.CloseDbConnection();

                XtraMessageBox.Show(e.Message, Resources.ErrorPcb, MessageBoxButtons.OK);
            }
        } // GetPcbData

        public void PcbDataGetFromDb(object focusRow)
        {
            DriverDb dbb = null;

            try
            {
                if (focusRow == null)
                    return;
                
                PcbVersion fRow = (PcbVersion) focusRow;
                int id = fRow.IDPcb;
                string sqlGet = TrackControlQuery.PcbSensors.SelectFromPcbData + id;
                dbb = new DriverDb();
                dbb.ConnectDb();
                DataTable tbPcb = dbb.GetDataTable(sqlGet);

                pcbData.Clear();
                gridControlPcb.DataSource = null;
                
                for( int i = 0; i < tbPcb.Rows.Count; i++ )
                {
                    string nameAlgo = GetAlgorithmName(Convert.ToInt32(tbPcb.Rows[i]["NumAlg"].ToString()));
                    PcbDataVersion pcbDataVer = new PcbDataVersion( Convert.ToInt32(tbPcb.Rows[i]["Id"].ToString()), 
                        tbPcb.Rows[i]["Typedata"].ToString(), Convert.ToDouble( tbPcb.Rows[i]["koeffK"].ToString() ),
                        Convert.ToInt32( tbPcb.Rows[i]["Startbit"].ToString() ),
                        Convert.ToInt32( tbPcb.Rows[i]["Lengthbit"].ToString() ), tbPcb.Rows[i]["Comment"].ToString(),
                        Convert.ToInt32(tbPcb.Rows[i]["Idpcb"].ToString()), tbPcb.Rows.Count, tbPcb.Rows[i]["NameVal"].ToString(),
                        nameAlgo, Convert.ToInt32(tbPcb.Rows[i]["NumAlg"].ToString()));
                    pcbData.Add( pcbDataVer );
                }

                gridControlPcb.DataSource = pcbData;

                if (pcbData.Count <= 0)
                    gridControlPcb.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = false;
                else
                    gridControlPcb.EmbeddedNavigator.Buttons.CustomButtons[1].Enabled = true;

                dbb.CloseDbConnection();
            }
            catch (Exception ex)
            {
                if(dbb != null)
                    dbb.CloseDbConnection();

                XtraMessageBox.Show( ex.Message, Resources.ErrorPcb, MessageBoxButtons.OK );
            }
        }

        private void btnImportXml_Click( object sender, EventArgs e )
        {
            try
            {
                SaveFileDialog dlg = new SaveFileDialog();
                dlg.OverwritePrompt = true;
                dlg.CreatePrompt = false;
                dlg.RestoreDirectory = false;
                dlg.CheckFileExists = false;
                dlg.CheckPathExists = false;
                dlg.Filter = Resources.XmlDataDoc;
                dlg.Title = Resources.ExportDataXml;
                dlg.FileName = "PcbDataSource";

                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    FileStream fstream = new FileStream(dlg.FileName, FileMode.Create, FileAccess.Write);
                    SoapFormatter formater = new SoapFormatter();

                    for (int i = 0; i < pcbVersionsList.Count; i++)
                    {
                        PcbVersion pcbVer = pcbVersionsList[i];
                        pcbVer.NumberClasses = pcbVersionsList.Count;
                        formater.Serialize(fstream, pcbVer);
                    }

                    fstream.Close();

                    formater = new SoapFormatter();
                    fstream = new FileStream(dlg.FileName, FileMode.Append, FileAccess.Write);

                    DriverDb dbb = new DriverDb();
                    dbb.ConnectDb();
                    List<PcbDataVersion> pcbData = new List<PcbDataVersion>();
                    
                    for (int k = 0; k < pcbVersionsList.Count; k++)
                    {
                        PcbVersion fRow = pcbVersionsList[k];
                        int id = fRow.IDPcb;
                        string sqlGet = TrackControlQuery.PcbSensors.SelectFromPcbData + id;
                        DataTable tbPcb = dbb.GetDataTable(sqlGet);

                        for (int i = 0; i < tbPcb.Rows.Count; i++)
                        {
                            string nameAlgo = GetAlgorithmName(Convert.ToInt32(tbPcb.Rows[i]["NumAlg"].ToString()));
                            PcbDataVersion pcbDataVer =
                                new PcbDataVersion(Convert.ToInt32(tbPcb.Rows[i]["Id"].ToString()),
                                    tbPcb.Rows[i]["Typedata"].ToString(),
                                    Convert.ToDouble(tbPcb.Rows[i]["koeffK"].ToString()),
                                    Convert.ToInt32(tbPcb.Rows[i]["Startbit"].ToString()),
                                    Convert.ToInt32(tbPcb.Rows[i]["Lengthbit"].ToString()),
                                    tbPcb.Rows[i]["Comment"].ToString(),
                                    Convert.ToInt32(tbPcb.Rows[i]["Idpcb"].ToString()), 0,
                                    tbPcb.Rows[i]["NameVal"].ToString(), nameAlgo,
                                    Convert.ToInt32(tbPcb.Rows[i]["NumAlg"].ToString()));

                            pcbData.Add(pcbDataVer);
                        } // for
                    } // for

                    for (int i = 0; i < pcbData.Count; i++)
                    {
                        pcbData[i].NumberClassesData = pcbData.Count;
                        formater.Serialize( fstream, pcbData[i] );
                    }

                    fstream.Close();
                } // if
            } // try
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, Resources.ErrorPcb, MessageBoxButtons.OK);
            }
        } // btnImportXml_Click

        // Импорт данных из файлов XML
        private void btnExportXml_Click( object sender, EventArgs e )
        {
            FileStream fstream = null;
            DriverDb db = null;

            try
            {
                OpenFileDialog dlg = new OpenFileDialog();
                dlg.RestoreDirectory = true;
                dlg.CheckFileExists = false;
                dlg.CheckPathExists = true;
                dlg.Filter = Resources.XmlDataDoc;
                dlg.Title = Resources.ImportXmlData;

                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    // открываем и читаем первую часть файла XML
                    fstream = new FileStream(dlg.FileName, FileMode.Open, FileAccess.Read);
                    SoapFormatter formater = new SoapFormatter();

                    PcbVersion dsrlData = null;
                    BindingList<PcbVersion> pcbVerLst = new BindingList<PcbVersion>();
                    List<int> lastIndex = new List<int>();
                    
                    // формируем первый список данных из первой части файла XML
                    int iCount = 0;

                    do
                    {
                        iCount++;
                        dsrlData = (PcbVersion) formater.Deserialize(fstream);
                        pcbVerLst.Add(dsrlData);
                    } while (iCount < dsrlData.NumberClasses);

                    db = new DriverDb();
                    db.ConnectDb();

                    // перебираем первый список
                    for (int i = 0; i < pcbVerLst.Count; i++)
                    {
                        // вставить данные в pcbversion
                        string sqlInsert = string.Format(TrackControlQuery.PcbSensors.InsertIntoPcbVersion,
                            pcbVerLst[i].nmPcbs, pcbVerLst[i].Comms);

                        lastIndex.Add(db.ExecuteReturnLastInsert(sqlInsert));
                    } // for

                    db.CloseDbConnection();

                    if (pcbVerLst.Count > 0)
                    {
                        // перечитать данные из таблицы pcbversion и обновить списки и grid приложения
                        GetPcbData();
                        gridViewPcbList.RefreshData();
                        int handleLastRow = gridViewPcbList.GetRowHandle(gridViewPcbList.RowCount - 1);
                        gridViewPcbList.FocusedRowHandle = handleLastRow;
                        gridViewPcbList.SelectRow(handleLastRow);
                    }

                    // готовимся читать вторую часть XML - файла
                    
                    PcbDataVersion psbData = null;
                    BindingList<PcbDataVersion> pcbDataLst = new BindingList<PcbDataVersion>();

                    // читаем данные из второй части файла XML и формируем второй список
                    iCount = 0;
                    do
                    {
                        iCount++;
                        psbData = (PcbDataVersion) formater.Deserialize(fstream);
                        pcbDataLst.Add(psbData);
                    } while (iCount < psbData.NumberClassesData);


                    db = new DriverDb();
                    db.ConnectDb();

                    // сохраняем поелементно второй список в таблицу pcbdata
                    for (int i = 0; i < pcbDataLst.Count; i++)
                    {
                        PcbDataVersion psbDt = pcbDataLst[i];

                        int indexKey = psbDt.ID_Pcb;

                        for (int h = 0; h < pcbVerLst.Count; h++)
                        {
                            if (indexKey == pcbVerLst[h].IDPcb)
                            {
                                indexKey = h;
                                break;
                            }
                        }

                        string koeffk = psbDt.KoefficK;
                        int index = koeffk.IndexOf(",");
                        string temp = "";

                        if (index >= 0)
                        {
                            temp = koeffk.Substring(0, index);
                            temp = temp + ".";
                            temp = temp + koeffk.Substring(index + 1, koeffk.Length - index - 1);
                            koeffk = temp;
                        }

                        string sqlInsert = string.Format(TrackControlQuery.PcbSensors.InsertIntoPcbData,
                            psbDt.TypeDataPcb, koeffk, psbDt.StrtBit, psbDt.LngthBit, psbDt.Commentary,
                            lastIndex[indexKey], psbDt.NameValueMeasure, psbDt.NumberAlgorithm);
                        db.ExecuteNonQueryCommand(sqlInsert);
                    } // for

                    fstream.Close();
                    db.CloseDbConnection();

                    if (pcbDataLst.Count > 0)
                    {
                        // перечитываем таблицу pcbdata и обновляем списки и grid приложения
                        PcbDataGetFromDb(gridViewPcbList.GetRow(gridViewPcbList.FocusedRowHandle));
                        gridViewPcb.RefreshData();
                        int handleLastRow = gridViewPcb.GetRowHandle(gridViewPcb.RowCount - 1);
                        gridViewPcb.FocusedRowHandle = handleLastRow;
                        gridViewPcb.SelectRow(handleLastRow);
                    }
                }
            } // catch
            catch (Exception exc)
            {
                fstream.Close();

                if(db != null)
                    db.CloseDbConnection();

                XtraMessageBox.Show(exc.Message, Resources.ErrorPcb, MessageBoxButtons.OK);
            } // catch
        } // btnExportXml_Click

        private void importSCAD_Click( object sender, EventArgs e )
        {
            try
            {
                OpenFileDialog dlg = new OpenFileDialog();
                dlg.RestoreDirectory = true;
                dlg.CheckFileExists = false;
                dlg.CheckPathExists = true;
                dlg.Multiselect = true;
                dlg.Filter = "SCAD data document (*.sac)|*.sac";
                dlg.Title = "Импорт данных SCAD";

                string[] openFilesNames = null;
                FileStream fStream = null;
                List<ForSerrialize> psbLstData = null;

                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    openFilesNames = dlg.FileNames;

                    if (openFilesNames != null)
                    {
                        psbLstData = new List<ForSerrialize>();

                        for (int i = 0; i < openFilesNames.Length; i++)
                        {
                            string openScadFile = openFilesNames[i];

                            fStream = new FileStream( openScadFile, FileMode.Open, FileAccess.Read );
                            XmlSerializer xmlSerializer = new XmlSerializer(typeof(ForSerrialize));
                            ForSerrialize psbData = new ForSerrialize();
                            psbData = ( ForSerrialize )xmlSerializer.Deserialize( fStream );
                            fStream.Close();
                            psbLstData.Add(psbData);
                        }

                        List<PcbVersion> pcbVrsLst = new List<PcbVersion>(); // левая таблица
                        List< PcbDataVersion > pcbDtVrsList = new List<PcbDataVersion>(); // правая таблица
                        int countId = 0;

                        // разбор полученной информации
                        for (int i = 0; i < psbLstData.Count; i++)
                        {   
                            // формируем первую таблицу
                            ForSerrialize psbData = psbLstData[i];
                            char[]execution = psbData.devTun.execution;
                            string str = new string(execution);
                            int inx = str.IndexOf("\0");
                            
                            string versScad = str.Substring(0, inx);
                            string comment = "from SCAD: " + DateTime.Now;

                            // сдесь формируем список для таблицы первой
                            PcbVersion pcbVrs = new PcbVersion( countId, versScad, comment, psbLstData.Count );
                            pcbVrsLst.Add(pcbVrs);

                            // формируем вторую таблицу
                            SpiDataUnit[] spiData = psbData.spiTun.dataUnit;
                            CodeDataSource dataSource = new CodeDataSource();
                            int spiLength = 1;
                            int k = 0;

                            while( spiData[++k].codeType < 255 )
                                spiLength++;

                            for (int j = 0; j < spiData.Length; j++)
                            {
                                byte startBit = spiData[j].startBit;
                                byte lengthBit = spiData[j].lenghtBit;
                                byte codeBit = spiData[j].codeType;
                                string nameVal = spiData[j].nameVal;
                                int numberAlgo = spiData[j].numAlgo;
                                string note = "----";
                                double kofK = 1;

                                if ((startBit == 255) || (lengthBit == 255) || (codeBit == 255))
                                    break;

                                string nameCode = CodeDataSource.GetDataName(codeBit);

                                // сдесь формируем список для таблицы второй
                                PcbDataVersion pcbDtVrs = new PcbDataVersion( countId, nameCode, kofK, 
                                    startBit, lengthBit, note, countId, spiLength, nameVal, "", numberAlgo );
                                pcbDtVrsList.Add(pcbDtVrs);
                            } // for

                            countId++;
                        } // for

                        DialogResult dlgRes = DialogResult.OK;

                        // проверяем наличие данных и если есть спрашиваем че делать то будем?
                        if (gridViewPcbList.RowCount > 0)
                        {
                            dlgRes = XtraMessageBox.Show(
                                    "Таблица \"Список версий печатных плат датчиков\" не пуста!" + "\n" +
                                    "Желаете перезаписать данные новыми?", "Предупреждение",
                                    MessageBoxButtons.YesNo);

                            if (dlgRes == DialogResult.Yes)
                            {
                                DriverDb db = new DriverDb();
                                db.ConnectDb();

                                string sqlInsert = "DELETE FROM pcbdata";
                                db.ExecuteNonQueryCommand( sqlInsert );

                                sqlInsert = "DELETE FROM pcbversion";
                                db.ExecuteNonQueryCommand( sqlInsert );

                                db.CloseDbConnection();

                                pcbVersionsList.Clear();
                                gridControlPcbList.DataSource = null;

                                pcbData.Clear();
                                gridControlPcb.DataSource = null;
                            } // if
                        } // if
                     
                        // Добавляем первую таблицу
                        if (pcbVrsLst.Count > 0)
                        {
                            List<int> lastIndex = new List<int>();
                            DriverDb db = new DriverDb();
                            db.ConnectDb();

                            // перебираем первый список
                            for (int i = 0; i < pcbVrsLst.Count; i++)
                            {
                                // добавляем данные в pcbversion
                                string sqlInsert =
                                    string.Format(TrackControlQuery.PcbSensors.InsertIntoPcbVersion,
                                        pcbVrsLst[i].nmPcbs, pcbVrsLst[i].Comms);

                                lastIndex.Add(db.ExecuteReturnLastInsert(sqlInsert));
                            } // for

                            db.CloseDbConnection();

                            // перечитать данные из таблицы pcbversion и обновить списки и grid приложения
                            GetPcbData();
                            gridViewPcbList.RefreshData();
                            int handleLastRow = gridViewPcbList.GetRowHandle(gridViewPcbList.RowCount - 1);
                            gridViewPcbList.FocusedRowHandle = handleLastRow;
                            gridViewPcbList.SelectRow(handleLastRow);

                            // Добавляем вторую таблицу
                            db = new DriverDb();
                            db.ConnectDb();

                            // сохраняем поелементно второй список в таблицу pcbdata
                            for (int i = 0; i < pcbDtVrsList.Count; i++)
                            {
                                PcbDataVersion psbDt = pcbDtVrsList[i];

                                int indexKey = psbDt.ID_Pcb;

                                for( int h = 0; h < pcbVrsLst.Count; h++ )
                                {
                                    if( indexKey == pcbVrsLst[h].IDPcb )
                                    {
                                        indexKey = h;
                                        break;
                                    }
                                }

                                string koeffk = psbDt.KoefficK;
                                int index = koeffk.IndexOf(",");
                                string temp = "";

                                if (index >= 0)
                                {
                                    temp = koeffk.Substring(0, index);
                                    temp = temp + ".";
                                    temp = temp + koeffk.Substring(index + 1, koeffk.Length - index - 1);
                                    koeffk = temp;
                                }

                                string sqlInsert = string.Format(TrackControlQuery.PcbSensors.InsertIntoPcbData,
                                    psbDt.TypeDataPcb, koeffk, psbDt.StrtBit, psbDt.LngthBit, psbDt.Commentary, lastIndex[indexKey], psbDt.NameValueMeasure, psbDt.NumberAlgorithm);
                                db.ExecuteNonQueryCommand(sqlInsert);
                            } // for

                            db.CloseDbConnection();

                            if( pcbDtVrsList.Count > 0 )
                            {
                                // перечитываем таблицу pcbdata и обновляем списки и grid приложения
                                PcbDataGetFromDb( gridViewPcbList.GetRow( gridViewPcbList.FocusedRowHandle ) );
                                gridViewPcb.RefreshData();
                                handleLastRow = gridViewPcb.GetRowHandle( gridViewPcb.RowCount - 1 );
                                gridViewPcb.FocusedRowHandle = handleLastRow;
                                gridViewPcb.SelectRow( handleLastRow );
                            }
                        } // if

                        XtraMessageBox.Show("Данные успешно внесены в базу данных", "Информацмя",
                            MessageBoxButtons.OK);
                    } // if
                } // if
            } // try
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message + "\n" + ex.StackTrace, "Error", MessageBoxButtons.OK);
            }
        }
    } // PcbSensors
} // Setting
