﻿using System;
using System.ComponentModel.Design;
using System.Data;
using System.Windows.Forms;
using System.Configuration;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Repository;
using LocalCache;
using LocalCache.atlantaDataSetTableAdapters;
using TrackControl.General.DatabaseDriver;
using TrackControl.Properties;

namespace TrackControl.Setting
{
    public partial class SettingSensorUser : UserControl
    {
        /// <summary>
        /// Адаптер для сохранения изменений в БД настроек датчиков.
        /// </summary>
        private SensorsTableAdapter _sensorAdapter = new SensorsTableAdapter();

        private RelationalgorithmsTableAdapter _relationalgorithmAdapter = new RelationalgorithmsTableAdapter();
        private SensorcoefficientTableAdapter _sensorcoefficientAdapter = new SensorcoefficientTableAdapter();
        private StateTableAdapter _stateTableAdapter = new StateTableAdapter();
        private TransitionTableAdapter _transitionTableAdapter = new TransitionTableAdapter();
        private LocalCache.atlantaDataSet atlantaDataSet;

        public SettingSensorUser()
        {
            InitializeComponent();
            SettingsSensorControlLoad();
        }

        public DataTable GetSensorsID(int mobitelid)
        {
            DataTable tbTable = new DataTable();

            try
            {
                DriverDb db = new DriverDb();
                db.ConnectDb();
                string sql = "SELECT id FROM sensors WHERE mobitel_id = " + mobitelid;
                tbTable = db.GetDataTable(sql);
                db.CloseDbConnection();

                return tbTable;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Error Get ID sensors", MessageBoxButtons.OK);
                return tbTable;
            }
        } // GetSensorsID

        public BindingSource GetBindingSensors(int mobitelid)
        {
            mobitelsBindingSource.Filter = string.Format("Mobitel_id = {0}", mobitelid);
            
            return sensorsBindingSource;
        }

        public void FillSensorBindingSource()
        {
            _sensorAdapter.Fill(atlantaDataSet.sensors);
        }

        private void SettingsSensorControlLoad()
        {
            try
            {
                atlantaDataSet = AppModel.Instance.DataSet;
                mobitelsBindingSource.DataSource = atlantaDataSet.mobitels;
                sensoralgorithmsBindingSource.DataSource = atlantaDataSet.sensoralgorithms;
                relationalgorithmsBindingSource.DataSource = atlantaDataSet.relationalgorithms;
                sensorcoefficientBindingSource.DataSource = atlantaDataSet.sensorcoefficient;
                vehicleBindingSource.DataSource = atlantaDataSet.vehicle;

                string cs =
                    Crypto.GetDecryptConnectionString(ConfigurationManager.ConnectionStrings["CS"].ConnectionString);

                DriverDb db0 = new DriverDb();
                db0.AdapNewConnection(cs);
                _sensorAdapter.Connection = db0.Connection; //new MySqlConnection(cs);

                DriverDb db1 = new DriverDb();
                db1.AdapNewConnection(cs);
                _relationalgorithmAdapter.Connection = db1.Connection; //new MySqlConnection(cs);

                DriverDb db2 = new DriverDb();
                db2.AdapNewConnection(cs);
                _sensorcoefficientAdapter.Connection = db2.Connection; //new MySqlConnection(cs);

                DriverDb db3 = new DriverDb();
                db3.AdapNewConnection(cs);
                _stateTableAdapter.Connection = db3.Connection; //new MySqlConnection(cs);

                DriverDb db4 = new DriverDb();
                db4.AdapNewConnection(cs);
                _transitionTableAdapter.Connection = db4.Connection; //new MySqlConnection(cs);

                _sensorcoefficientAdapter.Fill(atlantaDataSet.sensorcoefficient);
            }
            catch (Exception e)
            {
                XtraMessageBox.Show(e.Message, "Error", MessageBoxButtons.OK);
            }
        }

        public void InitialComboBoxAlgorithm(RepositoryItemComboBox columnAlgoritmComboBox)
        {
            columnAlgoritmComboBox.Items.Clear();

            foreach(atlantaDataSet.sensoralgorithmsRow saRow in
                ( atlantaDataSet.sensoralgorithmsDataTable )sensoralgorithmsBindingSource.DataSource )
            {
                columnAlgoritmComboBox.Items.Add( saRow.Name );
            }
        }

        public string GetTarirovkaNumber( int idRow )
        {
            DataRowView rvView = null;

            for( int i = 0; i < sensorsBindingSource.Count; i++ )
            {
                rvView = ( DataRowView )sensorsBindingSource[i];

                if( ( int )rvView.Row["id"] == idRow )
                    break;
            }

            atlantaDataSet.sensorsRow sensor = ( atlantaDataSet.sensorsRow )( rvView ).Row;
            string numberRow = GetTextNumberTarirovok( sensor.id );
            return numberRow;
        }

        public string GetTarirovkaTable(int idRow)
        {
            try
            {
                DataRowView rvView = null;

                for (int i = 0; i < sensorsBindingSource.Count; i++)
                {
                    rvView = (DataRowView) sensorsBindingSource[i];

                    if ((int) rvView.Row["id"] == idRow)
                        break;
                }
                atlantaDataSet.sensorsRow sensor = (atlantaDataSet.sensorsRow) (rvView).Row;
                ShowTarirovka(sensor.id);
                string numberRow = GetTextNumberTarirovok(sensor.id);
                return numberRow;
            }
            catch (Exception e)
            {
                XtraMessageBox.Show(e.Message, "Error Get Tarirovka Table", MessageBoxButtons.OK);
                return "";
            }
            return null;}

        private void ShowTarirovka(int sensorid)
        {
            try
            {
                sensorsBindingSource.EndEdit();
                sensorcoefficientBindingSource.EndEdit();

                ExcelFormDvx form = new ExcelFormDvx();
                form.atlDataSet = atlantaDataSet;
                form.sensorIdentificator = sensorid;
                sensorcoefficientBindingSource.Filter = String.Format("Sensor_id = {0}", sensorid);
                form.bindingSource = sensorcoefficientBindingSource;
                form.ShowDialog(this);

                sensorcoefficientBindingSource.EndEdit();
                _sensorcoefficientAdapter.Update(atlantaDataSet.sensorcoefficient);
                _sensorcoefficientAdapter.Fill(atlantaDataSet.sensorcoefficient);
            }
            catch (Exception e)
            {
                XtraMessageBox.Show(e.Message, "Error ShowTarirovka", MessageBoxButtons.OK);
            }
        }

        public string GetTextNumberTarirovok( int sensor_id )
        {
            string title = null;

            try
            {
                DataRow[] Rows = atlantaDataSet.sensorcoefficient.Select( string.Format( "Sensor_ID={0}", sensor_id ) );

                if( Rows != null && Rows.Length > 0 )
                    title = string.Format( "{0}...", Rows.Length );
                else
                    title = "X";
            }
            catch( Exception )
            {
                title = "!";
            }
            
            return title;
        }

        public int GetCountOldSetting(int mobitel)
        {
            int count = -1;

            int itemFound = mobitelsBindingSource.Find( "Mobitel_ID", mobitel );
            mobitelsBindingSource.Position = itemFound;
            count = sensorsBindingSource.Count;
            return count;
        }

        public void GetSituationData(int idRow)
        {
            DataRowView rvView = null;

            for( int i = 0; i < sensorsBindingSource.Count; i++ )
            {
                rvView = ( DataRowView )sensorsBindingSource[i];

                if( ( int )rvView.Row["id"] == idRow )
                    break;
            }

            atlantaDataSet.sensorsRow sensor = ( atlantaDataSet.sensorsRow )( rvView ).Row;
            atlantaDataSet.StateRow[] states = sensor.GetStateRows();

            if( states.Length > 1 )
            {
                SensorTransitionFormDvx sensorTransitionForm = new SensorTransitionFormDvx( _transitionTableAdapter,
                    atlantaDataSet, sensor );
                sensorTransitionForm.ShowDialog( this );
            }
            else
            {
                XtraMessageBox.Show( Resources.Err_NotEnoughStatesForTransition, "Info", MessageBoxButtons.OK);
            }
        }

        public string GetAlgorithmData(int idRow)
        {
            DataRowView rvView = null;

            for( int i = 0; i < sensorsBindingSource.Count; i++ )
            {
                rvView = ( DataRowView )sensorsBindingSource[i];

                if( ( int )rvView.Row["id"] == idRow )
                    break;
            }

            atlantaDataSet.sensorsRow sensor = ( atlantaDataSet.sensorsRow )( rvView ).Row;

            foreach( atlantaDataSet.relationalgorithmsRow rel_algRow in 
                ( atlantaDataSet.relationalgorithmsDataTable )atlantaDataSet.relationalgorithms )
            {
                if( rel_algRow.SensorID == sensor.id )
                {
                    int alg_ID = rel_algRow.AlgorithmID;

                    if (alg_ID == -1)
                        return "-- Алгоритм не назначен --";

                    atlantaDataSet.sensoralgorithmsDataTable sensDT = atlantaDataSet.sensoralgorithms;
                    atlantaDataSet.sensoralgorithmsRow[] salg_row = ( atlantaDataSet.sensoralgorithmsRow[] )sensDT.Select( "AlgorithmID = " + alg_ID );
                    if (salg_row.Length > 0)
                    {
                        return (string) (salg_row[0]["Name"]);
                    }
                    return "-- Алгоритм не назначен --";
                }
            }

            return "-- Алгоритм не назначен --";
        }

        public void AddNewSetting( int idmobitel, string typedata, double kK, double bB, bool sign, int strtBt, int lngthBt, string nameval, int numalgorithm)
        {
            try
            {
                sensorsBindingSource.SuspendBinding();
                atlantaDataSet.sensorsRow newSens_row = (atlantaDataSet.sensorsRow)atlantaDataSet.sensors.NewRow();
                newSens_row.Description = "";
                newSens_row.K = kK;
                newSens_row.B = bB;
                newSens_row.S = sign;
                newSens_row.Length = lngthBt;
                newSens_row.mobitel_id = idmobitel;
                newSens_row.NameUnit = nameval;
                newSens_row.StartBit = strtBt;
                newSens_row.Name = typedata;
                newSens_row.MobitelName = "";
                newSens_row.MobitelDescr = "";

                atlantaDataSet.sensors.AddsensorsRow( newSens_row );
                sensorsBindingSource.ResumeBinding();
                sensorsBindingSource.EndEdit();
                _sensorAdapter.Update( atlantaDataSet.sensors );

                atlantaDataSet.relationalgorithmsRow relalg_row =
                        (atlantaDataSet.relationalgorithmsRow)atlantaDataSet.relationalgorithms.NewRow();
                relalg_row.AlgorithmID = numalgorithm;
                relalg_row.SensorID = newSens_row.id;
                atlantaDataSet.relationalgorithms.AddrelationalgorithmsRow( relalg_row );
                relationalgorithmsBindingSource.ResumeBinding();
                relationalgorithmsBindingSource.EndEdit();
                _relationalgorithmAdapter.Update( atlantaDataSet.relationalgorithms );
                _relationalgorithmAdapter.Fill( atlantaDataSet.relationalgorithms );

                if ( vehicleBindingSource.Current != null )
                {
                    //mob_id = ( (atlantaDataSet.vehicleRow)( ( (DataRowView)vehicleBindingSource.Current ).Row ) ).Mobitel_id;
                    int itemFound = mobitelsBindingSource.Find( "Mobitel_ID", idmobitel );
                    mobitelsBindingSource.Position = itemFound;

                    if(itemFound == -1)
                        return;
                }
            }
            catch ( Exception ex )
            {
                XtraMessageBox.Show( ex.Message + " " + ex.InnerException, "AddNewRow Error" );
            }

            atlantaDataSet.AcceptChanges();
        }

        public void bindingNavigatorAddNewItem(int idmobitel)
        {
            try
            {
                sensorsBindingSource.SuspendBinding();
                atlantaDataSet.sensorsRow newSens_row = ( atlantaDataSet.sensorsRow )atlantaDataSet.sensors.NewRow();
                newSens_row.mobitel_id = idmobitel;
                newSens_row.Name = "XXXXX";
                newSens_row.Description = "Новая";
                newSens_row.StartBit = 0;
                newSens_row.Length = 0;
                newSens_row.NameUnit = "";
                newSens_row.K = 1;
                newSens_row.B = 0;
                newSens_row.S = false; // 0 = False, 0 < True
                //newSens_row.MobitelName = "";
                //newSens_row.MobitelDescr = "";

                atlantaDataSet.sensors.AddsensorsRow( newSens_row );
                sensorsBindingSource.ResumeBinding();
                sensorsBindingSource.EndEdit();
                _sensorAdapter.Update( atlantaDataSet.sensors );

                atlantaDataSet.relationalgorithmsRow relalg_row =
                        ( atlantaDataSet.relationalgorithmsRow )atlantaDataSet.relationalgorithms.NewRow();
                relalg_row.AlgorithmID = 1;
                relalg_row.SensorID = newSens_row.id;
                atlantaDataSet.relationalgorithms.AddrelationalgorithmsRow( relalg_row );
                relationalgorithmsBindingSource.ResumeBinding();
                relationalgorithmsBindingSource.EndEdit();
                _relationalgorithmAdapter.Update( atlantaDataSet.relationalgorithms );
                _relationalgorithmAdapter.Fill( atlantaDataSet.relationalgorithms );
            }
            catch( Exception ex )
            {
                XtraMessageBox.Show( ex.Message + " " + ex.InnerException, "AddNewRow Error" );
            }

            atlantaDataSet.AcceptChanges();
        }

        public void bindingNavigatorDeleteItem( int idRow )
        {
            try
            {
                DataRowView rvView = null;
                DriverDb db = new DriverDb();
                db.ConnectDb();
                
                DataRow[] delDataRow = atlantaDataSet.relationalgorithms.Select( "SensorID = " + idRow );

                if( delDataRow.Length > 0 )
                {
                    string sql = "DELETE FROM relationalgorithms WHERE SensorID = " + idRow;
                    db.ExecuteNonQueryCommand( sql );
                    _relationalgorithmAdapter.Fill( atlantaDataSet.relationalgorithms );
                }

                delDataRow = atlantaDataSet.State.Select( "SensorId = " + idRow );

                if( delDataRow.Length > 0 )
                {
                    string sql = "DELETE FROM state WHERE SensorId = " + idRow;
                    db.ExecuteNonQueryCommand( sql );
                    _stateTableAdapter.Fill( atlantaDataSet.State );
                }

                delDataRow = atlantaDataSet.sensorcoefficient.Select( "Sensor_id = " + idRow );

                if( delDataRow.Length > 0 )
                {
                    string sql = "DELETE FROM sensorcoefficient WHERE Sensor_id = " + idRow;
                    db.ExecuteNonQueryCommand( sql );
                    _sensorcoefficientAdapter.Fill( atlantaDataSet.sensorcoefficient );
                }

                delDataRow = atlantaDataSet.sensors.Select( "id = " + idRow );
                
                if( delDataRow.Length > 0 )
                {
                    string sqlsns = "DELETE FROM sensors WHERE id = " + idRow;
                   
                    db.ExecuteNonQueryCommand(sqlsns);
                    // перезагрузить таблицы кеша
                    _sensorAdapter.Fill( atlantaDataSet.sensors );
                }

                db.CloseDbConnection();
            }
            catch( Exception ex )
            {
                XtraMessageBox.Show( ex.Message + "\n" + ex.StackTrace, "DeleteRow Error" );
            }
        }

        public void GetStateData(int idRow)
        {
            DataRowView rvView = null;

            for( int i = 0; i < sensorsBindingSource.Count; i++ )
            {
                rvView = ( DataRowView )sensorsBindingSource[i];

                if( ( int )rvView.Row["id"] == idRow )
                    break;
            }

            atlantaDataSet.sensorsRow sensor = ( atlantaDataSet.sensorsRow )( rvView ).Row;
            SensorStateFormDvx sensorStatesForm = new SensorStateFormDvx( _stateTableAdapter, _transitionTableAdapter,
                    atlantaDataSet, sensor );
            sensorStatesForm.ShowDialog( this );
        }

        public void sensorsAlgorithmSave( string ValueName, int idRow )
        {
            DataRowView rvView = null;

            for( int i = 0; i < sensorsBindingSource.Count; i++ )
            {
                rvView = ( DataRowView )sensorsBindingSource[i];

                if( ( int )rvView.Row["id"] == idRow )
                    break;
            }

            atlantaDataSet.sensorsRow sensor = ( atlantaDataSet.sensorsRow )( rvView ).Row;

            changeRelateAlgoritm(ValueName, sensor.id);
        }

        public void SuspendBindingSensor()
        {
            sensoralgorithmsBindingSource.SuspendBinding();
        }

        public void AcsseptChange()
        {
            _sensorAdapter.Update( atlantaDataSet.sensors );
            _sensorAdapter.Fill(atlantaDataSet.sensors);
        }

        private void changeRelateAlgoritm(string ValueName, int sensorId)
        {
            int new_algorithmID = -1;

            foreach( atlantaDataSet.sensoralgorithmsRow sa_row in
                ( ( atlantaDataSet.sensoralgorithmsDataTable )sensoralgorithmsBindingSource.DataSource ).Rows )
            {
                if( sa_row.Name == ValueName )
                {
                    new_algorithmID = sa_row.AlgorithmID;
                    break;
                }
            }
            try
            {
                relationalgorithmsBindingSource.SuspendBinding();
                DataRow[] data_row = ( ( atlantaDataSet.relationalgorithmsDataTable )
                    relationalgorithmsBindingSource.DataSource ).
                    Select( "SensorID = " + sensorId );

                if( data_row.Length > 0 )
                {
                    ( ( atlantaDataSet.relationalgorithmsRow )data_row[0] ).AlgorithmID = new_algorithmID;
                }
                else
                {
                    //если у нас еще небыло связаного алгоритма - то создаем новую запись в таблице связей
                    atlantaDataSet.relationalgorithmsRow relalg_row =
                        ( atlantaDataSet.relationalgorithmsRow )atlantaDataSet.relationalgorithms.NewRow();
                    relalg_row.AlgorithmID = new_algorithmID;
                    relalg_row.SensorID = sensorId;
                    atlantaDataSet.relationalgorithms.AddrelationalgorithmsRow( relalg_row );
                }
            }
            finally
            {
                relationalgorithmsBindingSource.ResumeBinding();
                relationalgorithmsBindingSource.EndEdit();
                _relationalgorithmAdapter.Update( atlantaDataSet.relationalgorithms );
                _relationalgorithmAdapter.Fill( atlantaDataSet.relationalgorithms );
            }
        }
    }
}
