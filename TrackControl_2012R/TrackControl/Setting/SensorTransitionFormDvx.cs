﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraReports.Serialization;
using LocalCache;
using LocalCache.atlantaDataSetTableAdapters;
using TrackControl.Properties;
using TrackControl.Setting.Controls;

namespace TrackControl.Setting
{
    /// <summary>
    /// Форма для настроек событий, определяемых состояниями датчиков.
    /// </summary>
    public partial class SensorTransitionFormDvx : DevExpress.XtraEditors.XtraForm
    {
        /// <summary>
        /// Адаптер для таблицы переходов (состояний).
        /// </summary>
        private TransitionTableAdapter _transitionAdapter;
        /// <summary>
        /// Датасет приложения.
        /// </summary>
        private atlantaDataSet _dataset;
        /// <summary>
        /// Датчик, для которого настраиваются события.
        /// </summary>
        private atlantaDataSet.sensorsRow _sensor;

        private TrackControl.Setting.Controls.AddButtonControl addPanel;

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="transitionAdapter">Адаптер для таблицы переходов</param>
        /// <param name="dataset">Датасет приложения</param>
        /// <param name="sensor">Датчик, для которого настраиваются события</param>
        public SensorTransitionFormDvx(TransitionTableAdapter transitionAdapter, atlantaDataSet dataset,
            atlantaDataSet.sensorsRow sensor)
        {
            InitializeComponent();
            // addPanel
            this.addPanel = new TrackControl.Setting.Controls.AddButtonControl();
            this.addPanel.BackColor = System.Drawing.Color.Transparent;
            this.addPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.addPanel.Location = new System.Drawing.Point( 0, 0 );
            this.addPanel.Name = "addPanel";
            this.addPanel.Size = new System.Drawing.Size( 430, 100 );
            this.addPanel.TabIndex = 0;
            this.addPanel.AddPressed += new System.EventHandler( this.addPanel_AddPressed );
            this.addPanel.BackPressed += new System.EventHandler( this.closeBtn_Click );

            transitionGridView.CustomDrawCell += transitionGridView_CustomDrawCell;
            transitionGridView.RowCellClick += stateGrid_CellContentClick;
            
            clearEditGroupBox();

            _transitionAdapter = transitionAdapter;
            _dataset = dataset;
            _sensor = sensor;

            Load += SensorTransitionForm_Load;
            transitionGridView.CustomUnboundColumnData += stateGridView_CustomUnboundColumnData;

            init();
        }

        private void stateGridView_CustomUnboundColumnData( object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDataEventArgs e )
        {
            if( e.Column.VisibleIndex == 3 )
            {
                e.Value = imageList1.Images[0];
            }

            if( e.Column.VisibleIndex == 4 )
            {
                e.Value = imageList1.Images[1];
            }
        }

        /// <summary>
        /// Возвращает панель редактирования в начальное состояние.
        /// Отображает панель с кнопкой "Добавить событие".
        /// </summary>
        void clearEditGroupBox()
        {
            editGroupBox.Controls.Clear();
            editGroupBox.Controls.Add( addPanel );
            editGroupBox.Text = Resources.TransitionAdd;
            transControlGrid.Enabled = true;
            coltitle.Caption = Resources.EventDescription;
            coltitle.ToolTip = Resources.EventDescription;
            colenabled.Caption = Resources.Alarm;
            colenabled.ToolTip = Resources.Alarm;
        }

        /// <summary>
        /// Отображает панель удаления события.
        /// </summary>
        /// <param name="transition">Событие, которое намереваются удалить</param>
        void showDeletePanel( atlantaDataSet.TransitionRow transition )
        {
            editGroupBox.Controls.Clear();
            DeleteSensorTransitionControl deletePanel = new DeleteSensorTransitionControl( _transitionAdapter, _dataset, transition );
            deletePanel.EndDelete += deletePanel_EndDelete;
            deletePanel.Dock = DockStyle.Fill;
            editGroupBox.Controls.Add( deletePanel );
            editGroupBox.Text = Resources.TransitionDeleting;
            transControlGrid.Enabled = false;
            coltitle.Caption = Resources.EventDescription;
            coltitle.ToolTip = Resources.EventDescription;
            colenabled.Caption = Resources.Alarm;
            colenabled.ToolTip = Resources.Alarm;
        }

        /// <summary>
        /// Отображает панель редактирования события.
        /// </summary>
        /// <param name="state">Событие, которое будет редактироваться</param>
        void showEditPanel( atlantaDataSet.TransitionRow transition, bool added )
        {
            editGroupBox.Controls.Clear();
            EditSensorTransitionControl editPanel = new EditSensorTransitionControl( _transitionAdapter, _dataset, transition, added );
            editPanel.EndEdit += editPanel_EndEdit;
            editPanel.Dock = DockStyle.Fill;
            editGroupBox.Controls.Add( editPanel );
            editGroupBox.Text = Resources.TransitionSetting;
            transControlGrid.Enabled = false;
            coltitle.Caption = Resources.EventDescription;
            coltitle.ToolTip = Resources.EventDescription;
            colenabled.Caption = Resources.Alarm;
            colenabled.ToolTip = Resources.Alarm;}

        /// <summary>
        /// Обрабатывает клик на кнопке "Закрыть".
        /// </summary>
        void closeBtn_Click( object sender, EventArgs e )
        {
            Close();
        }

        /// <summary>
        /// Обрабатывает событие Load данной формы.
        /// </summary>
        void SensorTransitionForm_Load( object sender, EventArgs e )
        {
            VehicleInfo info = new VehicleInfo( _sensor.mobitelsRow );
            carNameText.Text = info.Info;
            sensorNameText.Text = _sensor.Name;
            sensorTransitionsBindingSource.DataSource = _dataset;
            sensorTransitionsBindingSource.DataMember = "Transition";
            sensorTransitionsBindingSource.Filter = String.Format( "SensorId = {0}", _sensor.id );
        }

        /// <summary>
        /// Обрабатывает клик на кнопке "Добавить".
        /// </summary>
        void addPanel_AddPressed( object sender, EventArgs e )
        {
            atlantaDataSet.TransitionRow transition = _dataset.Transition.NewTransitionRow();
            transition.sensorsRow = _sensor;
            atlantaDataSet.StateRow[] states = _sensor.GetStateRows();
            transition.StateRowByState_Transition_Initial = states[0];
            transition.StateRowByState_Transition_Final = states[1];
            transition.IconName = "Default";
            transition.SoundName = "Default";
            _dataset.Transition.AddTransitionRow( transition );
            showEditPanel( transition, true );
        }

        /// <summary>
        /// Добавляет всплывающие подсказки к кнопкам грида ("Редактировать" и "Удалить").
        /// </summary>
        void transitionGridView_CustomDrawCell( object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e )
        {
            if( e.Column.VisibleIndex == transitionGridView.Columns[3].VisibleIndex )
            {
                e.Column.Caption = Resources.TransitionEdit;
            }

            if( e.Column.VisibleIndex == transitionGridView.Columns[4].VisibleIndex )
            {
                e.Column.Caption = Resources.TransitionDelete;
            }
        }

        /// <summary>
        /// Обрабатывает клик на кнопках грида ("Редактировать" и "Удалить").
        /// </summary>
        void stateGrid_CellContentClick( object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e )
        {
            if( sensorTransitionsBindingSource.Current == null )
                return;

            atlantaDataSet.TransitionRow transition = ( atlantaDataSet.TransitionRow )( ( DataRowView )sensorTransitionsBindingSource.Current ).Row;

            if( e.Column.VisibleIndex == 3)
            {
                showEditPanel( transition, false );
            }

            if( e.Column.VisibleIndex == 4 )
            {
                showDeletePanel( transition );
            }
        } // stateGrid_CellContentClick

        /// <summary>
        /// Обрабатывает событие окончания удаления события для датчика.
        /// </summary>
        void deletePanel_EndDelete( object sender, EventArgs e )
        {
            clearEditGroupBox();
        }

        /// <summary>
        /// Обрабатывает окончание редактирования события.
        /// </summary>
        void editPanel_EndEdit( object sender, EventArgs e )
        {
            clearEditGroupBox();
        }

        void init()
        {
            carLbl.Text = String.Format( "{0}:", Resources.Car );
            sensorLbl.Text = String.Format( "{0}:", Resources.Sensor );
            coltitle.Caption = Resources.EventDescription;
            coltitle.ToolTip = Resources.EventDescription;
            colenabled.Caption = Resources.Alarm;
            colenabled.ToolTip = Resources.Alarm;
            colview.Caption = Resources.Report;
            colview.ToolTip = Resources.Report;
            Text = Resources.SensorTransitions;
            editGroupBox.Text = Resources.EditGroupBox;
            Editing.ToolTip = Resources.EditingState;
            Deleting.ToolTip = Resources.DeletingState;
        }
        
        public SensorTransitionFormDvx()
        {
            InitializeComponent();
        }
    }
}