using LocalCache;

namespace TrackControl.Setting
{
    partial class SettingSensorsControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingSensorsControl));
            this.sensorcoefficientBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sensorsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mobitelsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mobitelsDataGridView = new System.Windows.Forms.DataGridView();
            this.mobitelidDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnMarkAndModel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnModel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vehicleBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.vehicleBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.teletrekLabel = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.sensorsDataGridView = new System.Windows.Forms.DataGridView();
            this.columnID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnMobitel_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnStartBit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnLength = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnNameUnit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnK = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnCalibration = new System.Windows.Forms.DataGridViewButtonColumn();
            this.States = new System.Windows.Forms.DataGridViewButtonColumn();
            this.Transitions = new System.Windows.Forms.DataGridViewButtonColumn();
            this.columnDescription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.columnAlgoritmComboBox = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.sensoralgorithmsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sensorBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorCountItem1 = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorMoveFirstItem1 = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem1 = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem1 = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem1 = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem1 = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.buttonRefresh = new System.Windows.Forms.Button();
            this.relationalgorithmsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.sensorcoefficientBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sensorsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mobitelsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mobitelsDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vehicleBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vehicleBindingNavigator)).BeginInit();
            this.vehicleBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sensorsDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sensoralgorithmsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sensorBindingNavigator)).BeginInit();
            this.sensorBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.relationalgorithmsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // sensorcoefficientBindingSource
            // 
            this.sensorcoefficientBindingSource.DataMember = "sensors_sensorcoefficient";
            this.sensorcoefficientBindingSource.DataSource = this.sensorsBindingSource;
            // 
            // sensorsBindingSource
            // 
            this.sensorsBindingSource.DataMember = "mobitels_sensors";
            this.sensorsBindingSource.DataSource = this.mobitelsBindingSource;
            this.sensorsBindingSource.Filter = "Length <> 1";
            this.sensorsBindingSource.Sort = "StartBit ASC, Name ASC";
            // 
            // mobitelsBindingSource
            // 
            this.mobitelsBindingSource.DataMember = "mobitels";
            this.mobitelsBindingSource.DataSource = typeof(LocalCache.atlantaDataSet);
            this.mobitelsBindingSource.Sort = "Mobitel_ID";
            this.mobitelsBindingSource.CurrentChanged += new System.EventHandler(this.mobitelsBindingSource_CurrentChanged);
            // 
            // mobitelsDataGridView
            // 
            this.mobitelsDataGridView.AllowUserToAddRows = false;
            this.mobitelsDataGridView.AllowUserToDeleteRows = false;
            this.mobitelsDataGridView.AllowUserToResizeRows = false;
            this.mobitelsDataGridView.AutoGenerateColumns = false;
            this.mobitelsDataGridView.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            this.mobitelsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.mobitelsDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.mobitelidDataGridViewTextBoxColumn1,
            this.ColumnNumber,
            this.ColumnMarkAndModel,
            this.ColumnModel});
            this.mobitelsDataGridView.DataSource = this.vehicleBindingSource;
            this.mobitelsDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mobitelsDataGridView.Location = new System.Drawing.Point(0, 25);
            this.mobitelsDataGridView.MultiSelect = false;
            this.mobitelsDataGridView.Name = "mobitelsDataGridView";
            this.mobitelsDataGridView.ReadOnly = true;
            this.mobitelsDataGridView.RowHeadersVisible = false;
            this.mobitelsDataGridView.RowHeadersWidth = 28;
            this.mobitelsDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.mobitelsDataGridView.Size = new System.Drawing.Size(280, 488);
            this.mobitelsDataGridView.TabIndex = 1;
            this.mobitelsDataGridView.SelectionChanged += new System.EventHandler(this.mobitelsDataGridView_SelectionChanged);
            // 
            // mobitelidDataGridViewTextBoxColumn1
            // 
            this.mobitelidDataGridViewTextBoxColumn1.DataPropertyName = "Mobitel_id";
            this.mobitelidDataGridViewTextBoxColumn1.HeaderText = "Mobitel_id";
            this.mobitelidDataGridViewTextBoxColumn1.Name = "mobitelidDataGridViewTextBoxColumn1";
            this.mobitelidDataGridViewTextBoxColumn1.ReadOnly = true;
            this.mobitelidDataGridViewTextBoxColumn1.Visible = false;
            // 
            // ColumnNumber
            // 
            this.ColumnNumber.DataPropertyName = "NumberPlate";
            this.ColumnNumber.HeaderText = "�����";
            this.ColumnNumber.MinimumWidth = 60;
            this.ColumnNumber.Name = "ColumnNumber";
            this.ColumnNumber.ReadOnly = true;
            this.ColumnNumber.Width = 60;
            // 
            // ColumnMarkAndModel
            // 
            this.ColumnMarkAndModel.DataPropertyName = "MakeCar";
            this.ColumnMarkAndModel.HeaderText = "�����";
            this.ColumnMarkAndModel.MinimumWidth = 60;
            this.ColumnMarkAndModel.Name = "ColumnMarkAndModel";
            this.ColumnMarkAndModel.ReadOnly = true;
            this.ColumnMarkAndModel.Width = 117;
            // 
            // ColumnModel
            // 
            this.ColumnModel.DataPropertyName = "CarModel";
            this.ColumnModel.HeaderText = "������";
            this.ColumnModel.Name = "ColumnModel";
            this.ColumnModel.ReadOnly = true;
            // 
            // vehicleBindingSource
            // 
            this.vehicleBindingSource.DataMember = "vehicle";
            this.vehicleBindingSource.DataSource = typeof(LocalCache.atlantaDataSet);
            this.vehicleBindingSource.Sort = "Mobitel_id";
            // 
            // vehicleBindingNavigator
            // 
            this.vehicleBindingNavigator.AddNewItem = null;
            this.vehicleBindingNavigator.BindingSource = this.vehicleBindingSource;
            this.vehicleBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.vehicleBindingNavigator.DeleteItem = null;
            this.vehicleBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.teletrekLabel,
            this.toolStripSeparator1,
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2});
            this.vehicleBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.vehicleBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.vehicleBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.vehicleBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.vehicleBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.vehicleBindingNavigator.Name = "vehicleBindingNavigator";
            this.vehicleBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.vehicleBindingNavigator.Size = new System.Drawing.Size(280, 25);
            this.vehicleBindingNavigator.TabIndex = 0;
            this.vehicleBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(43, 22);
            this.bindingNavigatorCountItem.Text = "��� {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // teletrekLabel
            // 
            this.teletrekLabel.Name = "teletrekLabel";
            this.teletrekLabel.Size = new System.Drawing.Size(64, 22);
            this.teletrekLabel.Text = "���������";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 21);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 20);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // sensorsDataGridView
            // 
            this.sensorsDataGridView.AllowUserToAddRows = false;
            this.sensorsDataGridView.AllowUserToResizeRows = false;
            this.sensorsDataGridView.AutoGenerateColumns = false;
            this.sensorsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.sensorsDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.columnID,
            this.nameDataGridViewTextBoxColumn,
            this.columnMobitel_id,
            this.columnStartBit,
            this.columnLength,
            this.columnNameUnit,
            this.columnK,
            this.columnCalibration,
            this.States,
            this.Transitions,
            this.columnDescription,
            this.columnAlgoritmComboBox});
            this.sensorsDataGridView.DataSource = this.sensorsBindingSource;
            this.sensorsDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sensorsDataGridView.Location = new System.Drawing.Point(0, 25);
            this.sensorsDataGridView.Name = "sensorsDataGridView";
            this.sensorsDataGridView.RowHeadersVisible = false;
            this.sensorsDataGridView.RowHeadersWidth = 28;
            this.sensorsDataGridView.Size = new System.Drawing.Size(667, 464);
            this.sensorsDataGridView.TabIndex = 1;
            this.sensorsDataGridView.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.sensorsDataGridView_CellClick);
            this.sensorsDataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.sensorsDataGridView_CellContentClick);
            this.sensorsDataGridView.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.sensorsDataGridView_CellPainting);
            this.sensorsDataGridView.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.sensorsDataGridView_CellValueChanged);
            this.sensorsDataGridView.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.sensorsDataGridView_DataError);
            // 
            // columnID
            // 
            this.columnID.DataPropertyName = "id";
            this.columnID.HeaderText = "id";
            this.columnID.Name = "columnID";
            this.columnID.Visible = false;
            // 
            // nameDataGridViewTextBoxColumn
            // 
            this.nameDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.nameDataGridViewTextBoxColumn.DataPropertyName = "Name";
            this.nameDataGridViewTextBoxColumn.HeaderText = "��������";
            this.nameDataGridViewTextBoxColumn.Name = "nameDataGridViewTextBoxColumn";
            this.nameDataGridViewTextBoxColumn.Width = 82;
            // 
            // columnMobitel_id
            // 
            this.columnMobitel_id.DataPropertyName = "mobitel_id";
            this.columnMobitel_id.HeaderText = "mobitel_id";
            this.columnMobitel_id.Name = "columnMobitel_id";
            this.columnMobitel_id.Visible = false;
            // 
            // columnStartBit
            // 
            this.columnStartBit.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.columnStartBit.DataPropertyName = "StartBit";
            this.columnStartBit.HeaderText = "��������� ���";
            this.columnStartBit.MinimumWidth = 70;
            this.columnStartBit.Name = "columnStartBit";
            this.columnStartBit.Width = 70;
            // 
            // columnLength
            // 
            this.columnLength.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.columnLength.DataPropertyName = "Length";
            this.columnLength.HeaderText = "�����";
            this.columnLength.MinimumWidth = 50;
            this.columnLength.Name = "columnLength";
            this.columnLength.Width = 50;
            // 
            // columnNameUnit
            // 
            this.columnNameUnit.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.columnNameUnit.DataPropertyName = "NameUnit";
            this.columnNameUnit.HeaderText = "�������� ��������";
            this.columnNameUnit.MinimumWidth = 60;
            this.columnNameUnit.Name = "columnNameUnit";
            this.columnNameUnit.Width = 60;
            // 
            // columnK
            // 
            this.columnK.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.columnK.DataPropertyName = "K";
            this.columnK.HeaderText = "K";
            this.columnK.Name = "columnK";
            this.columnK.Width = 39;
            // 
            // columnCalibration
            // 
            this.columnCalibration.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.columnCalibration.DataPropertyName = "string";
            this.columnCalibration.HeaderText = "���������";
            this.columnCalibration.Name = "columnCalibration";
            this.columnCalibration.Text = "...";
            this.columnCalibration.ToolTipText = "Calibration table";
            this.columnCalibration.Width = 70;
            // 
            // States
            // 
            this.States.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.States.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.States.HeaderText = "���������";
            this.States.MinimumWidth = 70;
            this.States.Name = "States";
            this.States.Text = "��������";
            this.States.ToolTipText = "������������� ��������� ��� ������� �������";
            this.States.UseColumnTextForButtonValue = true;
            this.States.Width = 70;
            // 
            // Transitions
            // 
            this.Transitions.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Transitions.HeaderText = "�������";
            this.Transitions.MinimumWidth = 70;
            this.Transitions.Name = "Transitions";
            this.Transitions.Text = "��������";
            this.Transitions.ToolTipText = "������������� ������� ��� ������� �������";
            this.Transitions.UseColumnTextForButtonValue = true;
            this.Transitions.Width = 70;
            // 
            // columnDescription
            // 
            this.columnDescription.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.columnDescription.DataPropertyName = "Description";
            this.columnDescription.HeaderText = "��������";
            this.columnDescription.MinimumWidth = 80;
            this.columnDescription.Name = "columnDescription";
            this.columnDescription.Visible = false;
            // 
            // columnAlgoritmComboBox
            // 
            this.columnAlgoritmComboBox.HeaderText = "����������";
            this.columnAlgoritmComboBox.MinimumWidth = 150;
            this.columnAlgoritmComboBox.Name = "columnAlgoritmComboBox";
            this.columnAlgoritmComboBox.Width = 150;
            // 
            // sensoralgorithmsBindingSource
            // 
            this.sensoralgorithmsBindingSource.DataMember = "sensoralgorithms";
            this.sensoralgorithmsBindingSource.DataSource = typeof(LocalCache.atlantaDataSet);
            // 
            // sensorBindingNavigator
            // 
            this.sensorBindingNavigator.AddNewItem = null;
            this.sensorBindingNavigator.AutoSize = false;
            this.sensorBindingNavigator.BindingSource = this.sensorsBindingSource;
            this.sensorBindingNavigator.CountItem = this.bindingNavigatorCountItem1;
            this.sensorBindingNavigator.DeleteItem = null;
            this.sensorBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem1,
            this.bindingNavigatorMovePreviousItem1,
            this.bindingNavigatorSeparator3,
            this.bindingNavigatorPositionItem1,
            this.bindingNavigatorCountItem1,
            this.bindingNavigatorSeparator4,
            this.bindingNavigatorMoveNextItem1,
            this.bindingNavigatorMoveLastItem1,
            this.bindingNavigatorSeparator5,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem});
            this.sensorBindingNavigator.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.Flow;
            this.sensorBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.sensorBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem1;
            this.sensorBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem1;
            this.sensorBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem1;
            this.sensorBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem1;
            this.sensorBindingNavigator.Name = "sensorBindingNavigator";
            this.sensorBindingNavigator.PositionItem = this.bindingNavigatorPositionItem1;
            this.sensorBindingNavigator.Size = new System.Drawing.Size(667, 25);
            this.sensorBindingNavigator.TabIndex = 0;
            this.sensorBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorCountItem1
            // 
            this.bindingNavigatorCountItem1.Name = "bindingNavigatorCountItem1";
            this.bindingNavigatorCountItem1.Size = new System.Drawing.Size(43, 15);
            this.bindingNavigatorCountItem1.Text = "��� {0}";
            this.bindingNavigatorCountItem1.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorMoveFirstItem1
            // 
            this.bindingNavigatorMoveFirstItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem1.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem1.Image")));
            this.bindingNavigatorMoveFirstItem1.Name = "bindingNavigatorMoveFirstItem1";
            this.bindingNavigatorMoveFirstItem1.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem1.Size = new System.Drawing.Size(23, 20);
            this.bindingNavigatorMoveFirstItem1.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem1
            // 
            this.bindingNavigatorMovePreviousItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem1.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem1.Image")));
            this.bindingNavigatorMovePreviousItem1.Name = "bindingNavigatorMovePreviousItem1";
            this.bindingNavigatorMovePreviousItem1.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem1.Size = new System.Drawing.Size(23, 20);
            this.bindingNavigatorMovePreviousItem1.Text = "Move previous";
            // 
            // bindingNavigatorSeparator3
            // 
            this.bindingNavigatorSeparator3.Name = "bindingNavigatorSeparator3";
            this.bindingNavigatorSeparator3.Size = new System.Drawing.Size(6, 23);
            // 
            // bindingNavigatorPositionItem1
            // 
            this.bindingNavigatorPositionItem1.AccessibleName = "Position";
            this.bindingNavigatorPositionItem1.AutoSize = false;
            this.bindingNavigatorPositionItem1.Name = "bindingNavigatorPositionItem1";
            this.bindingNavigatorPositionItem1.Size = new System.Drawing.Size(50, 21);
            this.bindingNavigatorPositionItem1.Text = "0";
            this.bindingNavigatorPositionItem1.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator4
            // 
            this.bindingNavigatorSeparator4.Name = "bindingNavigatorSeparator4";
            this.bindingNavigatorSeparator4.Size = new System.Drawing.Size(6, 23);
            // 
            // bindingNavigatorMoveNextItem1
            // 
            this.bindingNavigatorMoveNextItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem1.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem1.Image")));
            this.bindingNavigatorMoveNextItem1.Name = "bindingNavigatorMoveNextItem1";
            this.bindingNavigatorMoveNextItem1.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem1.Size = new System.Drawing.Size(23, 20);
            this.bindingNavigatorMoveNextItem1.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem1
            // 
            this.bindingNavigatorMoveLastItem1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem1.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem1.Image")));
            this.bindingNavigatorMoveLastItem1.Name = "bindingNavigatorMoveLastItem1";
            this.bindingNavigatorMoveLastItem1.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem1.Size = new System.Drawing.Size(23, 20);
            this.bindingNavigatorMoveLastItem1.Text = "Move last";
            // 
            // bindingNavigatorSeparator5
            // 
            this.bindingNavigatorSeparator5.Name = "bindingNavigatorSeparator5";
            this.bindingNavigatorSeparator5.Size = new System.Drawing.Size(6, 23);
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 20);
            this.bindingNavigatorAddNewItem.Text = "Add new";
            this.bindingNavigatorAddNewItem.Click += new System.EventHandler(this.bindingNavigatorAddNewItem_Click);
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 20);
            this.bindingNavigatorDeleteItem.Text = "Delete";
            this.bindingNavigatorDeleteItem.Click += new System.EventHandler(this.bindingNavigatorDeleteItem_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.mobitelsDataGridView);
            this.splitContainer1.Panel1.Controls.Add(this.vehicleBindingNavigator);
            this.splitContainer1.Panel1MinSize = 100;
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.AutoScroll = true;
            this.splitContainer1.Panel2.Controls.Add(this.panel2);
            this.splitContainer1.Panel2.Controls.Add(this.panel1);
            this.splitContainer1.Size = new System.Drawing.Size(951, 513);
            this.splitContainer1.SplitterDistance = 280;
            this.splitContainer1.TabIndex = 6;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.sensorsDataGridView);
            this.panel2.Controls.Add(this.sensorBindingNavigator);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(667, 489);
            this.panel2.TabIndex = 4;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 489);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(667, 24);
            this.panel1.TabIndex = 3;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.buttonRefresh);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel3.Location = new System.Drawing.Point(432, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(235, 24);
            this.panel3.TabIndex = 3;
            // 
            // buttonRefresh
            // 
            this.buttonRefresh.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonRefresh.Location = new System.Drawing.Point(0, 0);
            this.buttonRefresh.Name = "buttonRefresh";
            this.buttonRefresh.Size = new System.Drawing.Size(75, 24);
            this.buttonRefresh.TabIndex = 3;
            this.buttonRefresh.Text = "��������";
            this.buttonRefresh.UseVisualStyleBackColor = true;
            this.buttonRefresh.Click += new System.EventHandler(this.buttonRefresh_Click);
            // 
            // relationalgorithmsBindingSource
            // 
            this.relationalgorithmsBindingSource.DataMember = "sensors_relationalgorithms";
            this.relationalgorithmsBindingSource.DataSource = this.sensorsBindingSource;
            // 
            // SettingSensorsControl
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.Controls.Add(this.splitContainer1);
            this.Name = "SettingSensorsControl";
            this.Size = new System.Drawing.Size(951, 513);
            this.Load += new System.EventHandler(this.SettingsSensorControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.sensorcoefficientBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sensorsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mobitelsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mobitelsDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vehicleBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vehicleBindingNavigator)).EndInit();
            this.vehicleBindingNavigator.ResumeLayout(false);
            this.vehicleBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sensorsDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sensoralgorithmsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sensorBindingNavigator)).EndInit();
            this.sensorBindingNavigator.ResumeLayout(false);
            this.sensorBindingNavigator.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.relationalgorithmsBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private LocalCache.atlantaDataSet atlantaDataSet;
        private System.Windows.Forms.BindingSource mobitelsBindingSource;
        private System.Windows.Forms.BindingSource sensorsBindingSource;
        private System.Windows.Forms.BindingSource sensoralgorithmsBindingSource;
        private System.Windows.Forms.BindingSource sensorcoefficientBindingSource;
        private System.Windows.Forms.BindingSource relationalgorithmsBindingSource;
        private System.Windows.Forms.DataGridView mobitelsDataGridView;
        private System.Windows.Forms.BindingNavigator vehicleBindingNavigator;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.DataGridView sensorsDataGridView;
        private System.Windows.Forms.BindingNavigator sensorBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem1;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator3;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem1;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator4;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem1;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator5;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.ToolStripLabel teletrekLabel;
      private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
      private System.Windows.Forms.Button buttonRefresh;
      private System.Windows.Forms.DataGridViewTextBoxColumn columnID;
      private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn;
      private System.Windows.Forms.DataGridViewTextBoxColumn columnMobitel_id;
      private System.Windows.Forms.DataGridViewTextBoxColumn columnStartBit;
      private System.Windows.Forms.DataGridViewTextBoxColumn columnLength;
      private System.Windows.Forms.DataGridViewTextBoxColumn columnNameUnit;
      private System.Windows.Forms.DataGridViewTextBoxColumn columnK;
      private System.Windows.Forms.DataGridViewButtonColumn columnCalibration;
      private System.Windows.Forms.DataGridViewButtonColumn States;
      private System.Windows.Forms.DataGridViewButtonColumn Transitions;
      private System.Windows.Forms.DataGridViewTextBoxColumn columnDescription;
      private System.Windows.Forms.DataGridViewComboBoxColumn columnAlgoritmComboBox;
      private System.Windows.Forms.BindingSource vehicleBindingSource;
      private System.Windows.Forms.DataGridViewTextBoxColumn mobitelidDataGridViewTextBoxColumn1;
      private System.Windows.Forms.DataGridViewTextBoxColumn ColumnNumber;
      private System.Windows.Forms.DataGridViewTextBoxColumn ColumnMarkAndModel;
      private System.Windows.Forms.DataGridViewTextBoxColumn ColumnModel;

#pragma warning disable

      
#pragma warning restore 
    }
}
