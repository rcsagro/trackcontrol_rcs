namespace TrackControl.Setting
{
    partial class UsersList
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UsersList));
            this.splitContainerControl = new DevExpress.XtraEditors.SplitContainerControl();
            this.gcUsers = new DevExpress.XtraGrid.GridControl();
            this.gvUsers = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPassword = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAdmin = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWinLogin = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserRole = new DevExpress.XtraGrid.Columns.GridColumn();
            this.leRoles = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.rteWinLogin = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.sbSaveObjectValues = new DevExpress.XtraEditors.SimpleButton();
            this.sbEdit = new DevExpress.XtraEditors.SimpleButton();
            this.pnViews = new DevExpress.XtraEditors.PanelControl();
            this.lbObjects = new DevExpress.XtraEditors.LabelControl();
            this.lbRoles = new DevExpress.XtraEditors.LabelControl();
            this.leUserRoles = new DevExpress.XtraEditors.LookUpEdit();
            this.rgObjectTypes = new DevExpress.XtraEditors.RadioGroup();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl)).BeginInit();
            this.splitContainerControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcUsers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvUsers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leRoles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rteWinLogin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnViews)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leUserRoles.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgObjectTypes.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainerControl
            // 
            this.splitContainerControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl.Name = "splitContainerControl";
            this.splitContainerControl.Panel1.Controls.Add(this.gcUsers);
            this.splitContainerControl.Panel1.Text = "Panel1";
            this.splitContainerControl.Panel2.Controls.Add(this.sbSaveObjectValues);
            this.splitContainerControl.Panel2.Controls.Add(this.sbEdit);
            this.splitContainerControl.Panel2.Controls.Add(this.pnViews);
            this.splitContainerControl.Panel2.Controls.Add(this.lbObjects);
            this.splitContainerControl.Panel2.Controls.Add(this.lbRoles);
            this.splitContainerControl.Panel2.Controls.Add(this.leUserRoles);
            this.splitContainerControl.Panel2.Controls.Add(this.rgObjectTypes);
            this.splitContainerControl.Panel2.Text = "Panel2";
            this.splitContainerControl.Size = new System.Drawing.Size(1247, 528);
            this.splitContainerControl.SplitterPosition = 761;
            this.splitContainerControl.TabIndex = 0;
            this.splitContainerControl.Text = "splitContainerControl1";
            // 
            // gcUsers
            // 
            this.gcUsers.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcUsers.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gcUsers_EmbeddedNavigator_ButtonClick);
            this.gcUsers.Location = new System.Drawing.Point(0, 0);
            this.gcUsers.MainView = this.gvUsers;
            this.gcUsers.Name = "gcUsers";
            this.gcUsers.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.rteWinLogin,
            this.leRoles});
            this.gcUsers.Size = new System.Drawing.Size(761, 528);
            this.gcUsers.TabIndex = 2;
            this.gcUsers.UseEmbeddedNavigator = true;
            this.gcUsers.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvUsers});
            // 
            // gvUsers
            // 
            this.gvUsers.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvUsers.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvUsers.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gvUsers.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvUsers.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvUsers.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvUsers.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvUsers.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvUsers.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gvUsers.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvUsers.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvUsers.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvUsers.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvUsers.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gvUsers.Appearance.Empty.Options.UseBackColor = true;
            this.gvUsers.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvUsers.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvUsers.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gvUsers.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvUsers.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gvUsers.Appearance.EvenRow.Options.UseForeColor = true;
            this.gvUsers.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvUsers.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvUsers.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gvUsers.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvUsers.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvUsers.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gvUsers.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvUsers.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gvUsers.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvUsers.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvUsers.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvUsers.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.gvUsers.Appearance.FixedLine.Options.UseBackColor = true;
            this.gvUsers.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gvUsers.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gvUsers.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gvUsers.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gvUsers.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(192)))), ((int)(((byte)(157)))));
            this.gvUsers.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(219)))), ((int)(((byte)(188)))));
            this.gvUsers.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gvUsers.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvUsers.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gvUsers.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvUsers.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvUsers.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvUsers.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvUsers.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvUsers.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvUsers.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gvUsers.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvUsers.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvUsers.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvUsers.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvUsers.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvUsers.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvUsers.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gvUsers.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvUsers.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvUsers.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gvUsers.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(242)))), ((int)(((byte)(213)))));
            this.gvUsers.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gvUsers.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gvUsers.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvUsers.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvUsers.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvUsers.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvUsers.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gvUsers.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvUsers.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gvUsers.Appearance.GroupRow.Options.UseForeColor = true;
            this.gvUsers.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvUsers.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvUsers.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gvUsers.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvUsers.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvUsers.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gvUsers.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvUsers.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvUsers.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gvUsers.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvUsers.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.gvUsers.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gvUsers.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvUsers.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvUsers.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvUsers.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvUsers.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gvUsers.Appearance.OddRow.Options.UseBackColor = true;
            this.gvUsers.Appearance.OddRow.Options.UseBorderColor = true;
            this.gvUsers.Appearance.OddRow.Options.UseForeColor = true;
            this.gvUsers.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(247)))));
            this.gvUsers.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gvUsers.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(148)))), ((int)(((byte)(148)))));
            this.gvUsers.Appearance.Preview.Options.UseBackColor = true;
            this.gvUsers.Appearance.Preview.Options.UseFont = true;
            this.gvUsers.Appearance.Preview.Options.UseForeColor = true;
            this.gvUsers.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvUsers.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gvUsers.Appearance.Row.Options.UseBackColor = true;
            this.gvUsers.Appearance.Row.Options.UseForeColor = true;
            this.gvUsers.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvUsers.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gvUsers.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvUsers.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.gvUsers.Appearance.SelectedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvUsers.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.gvUsers.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvUsers.Appearance.SelectedRow.Options.UseBorderColor = true;
            this.gvUsers.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gvUsers.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gvUsers.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gvUsers.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvUsers.Appearance.VertLine.Options.UseBackColor = true;
            this.gvUsers.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colId,
            this.colName,
            this.colPassword,
            this.colAdmin,
            this.colWinLogin,
            this.colUserRole});
            this.gvUsers.GridControl = this.gcUsers;
            this.gvUsers.Name = "gvUsers";
            this.gvUsers.OptionsNavigation.AutoFocusNewRow = true;
            this.gvUsers.OptionsView.EnableAppearanceEvenRow = true;
            this.gvUsers.OptionsView.EnableAppearanceOddRow = true;
            this.gvUsers.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gvUsers_FocusedRowChanged);
            this.gvUsers.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gvUsers_CellValueChanged);
            this.gvUsers.CellValueChanging += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gvUsers_CellValueChanging);
            this.gvUsers.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.gvUsers_ValidateRow);
            // 
            // colId
            // 
            this.colId.AppearanceHeader.Options.UseTextOptions = true;
            this.colId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colId.Caption = "Id";
            this.colId.FieldName = "Id";
            this.colId.Name = "colId";
            // 
            // colName
            // 
            this.colName.AppearanceHeader.Options.UseTextOptions = true;
            this.colName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colName.Caption = "������������";
            this.colName.FieldName = "Name";
            this.colName.Name = "colName";
            this.colName.Visible = true;
            this.colName.VisibleIndex = 0;
            this.colName.Width = 470;
            // 
            // colPassword
            // 
            this.colPassword.AppearanceCell.Options.UseTextOptions = true;
            this.colPassword.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPassword.AppearanceHeader.Options.UseTextOptions = true;
            this.colPassword.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPassword.Caption = "������";
            this.colPassword.FieldName = "PasswordGrid";
            this.colPassword.Name = "colPassword";
            this.colPassword.Visible = true;
            this.colPassword.VisibleIndex = 1;
            this.colPassword.Width = 186;
            // 
            // colAdmin
            // 
            this.colAdmin.AppearanceCell.Options.UseTextOptions = true;
            this.colAdmin.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colAdmin.AppearanceHeader.Options.UseTextOptions = true;
            this.colAdmin.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colAdmin.Caption = "�����";
            this.colAdmin.FieldName = "Admin";
            this.colAdmin.Name = "colAdmin";
            this.colAdmin.Visible = true;
            this.colAdmin.VisibleIndex = 2;
            this.colAdmin.Width = 135;
            // 
            // colWinLogin
            // 
            this.colWinLogin.AppearanceCell.Options.UseTextOptions = true;
            this.colWinLogin.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colWinLogin.AppearanceHeader.Options.UseTextOptions = true;
            this.colWinLogin.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colWinLogin.Caption = "Win Login";
            this.colWinLogin.FieldName = "WinLogin";
            this.colWinLogin.Name = "colWinLogin";
            this.colWinLogin.Visible = true;
            this.colWinLogin.VisibleIndex = 3;
            this.colWinLogin.Width = 203;
            // 
            // colUserRole
            // 
            this.colUserRole.AppearanceHeader.Options.UseTextOptions = true;
            this.colUserRole.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colUserRole.Caption = "����";
            this.colUserRole.ColumnEdit = this.leRoles;
            this.colUserRole.FieldName = "Id_role";
            this.colUserRole.Name = "colUserRole";
            this.colUserRole.Visible = true;
            this.colUserRole.VisibleIndex = 4;
            this.colUserRole.Width = 256;
            // 
            // leRoles
            // 
            this.leRoles.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leRoles.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name")});
            this.leRoles.DisplayMember = "Name";
            this.leRoles.Name = "leRoles";
            this.leRoles.NullText = "";
            this.leRoles.ValueMember = "Id";
            // 
            // rteWinLogin
            // 
            this.rteWinLogin.AutoHeight = false;
            this.rteWinLogin.Name = "rteWinLogin";
            this.rteWinLogin.PasswordChar = '*';
            // 
            // sbSaveObjectValues
            // 
            this.sbSaveObjectValues.Image = ((System.Drawing.Image)(resources.GetObject("sbSaveObjectValues.Image")));
            this.sbSaveObjectValues.Location = new System.Drawing.Point(341, 41);
            this.sbSaveObjectValues.Name = "sbSaveObjectValues";
            this.sbSaveObjectValues.Size = new System.Drawing.Size(24, 23);
            this.sbSaveObjectValues.TabIndex = 11;
            this.sbSaveObjectValues.Click += new System.EventHandler(this.sbSaveObjectValues_Click);
            // 
            // sbEdit
            // 
            this.sbEdit.Image = ((System.Drawing.Image)(resources.GetObject("sbEdit.Image")));
            this.sbEdit.Location = new System.Drawing.Point(341, 12);
            this.sbEdit.Name = "sbEdit";
            this.sbEdit.Size = new System.Drawing.Size(24, 23);
            this.sbEdit.TabIndex = 10;
            this.sbEdit.Click += new System.EventHandler(this.sbEdit_Click);
            // 
            // pnViews
            // 
            this.pnViews.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnViews.Location = new System.Drawing.Point(0, 142);
            this.pnViews.Name = "pnViews";
            this.pnViews.Size = new System.Drawing.Size(477, 383);
            this.pnViews.TabIndex = 9;
            // 
            // lbObjects
            // 
            this.lbObjects.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbObjects.Location = new System.Drawing.Point(17, 38);
            this.lbObjects.Name = "lbObjects";
            this.lbObjects.Size = new System.Drawing.Size(101, 16);
            this.lbObjects.TabIndex = 8;
            this.lbObjects.Text = "������� �������";
            // 
            // lbRoles
            // 
            this.lbRoles.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbRoles.Location = new System.Drawing.Point(90, 16);
            this.lbRoles.Name = "lbRoles";
            this.lbRoles.Size = new System.Drawing.Size(28, 16);
            this.lbRoles.TabIndex = 7;
            this.lbRoles.Text = "����";
            // 
            // leUserRoles
            // 
            this.leUserRoles.Location = new System.Drawing.Point(126, 12);
            this.leUserRoles.Name = "leUserRoles";
            this.leUserRoles.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.leUserRoles.Properties.Appearance.Options.UseFont = true;
            this.leUserRoles.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leUserRoles.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name")});
            this.leUserRoles.Properties.DisplayMember = "Name";
            this.leUserRoles.Properties.NullText = "�������� ���� ";
            this.leUserRoles.Properties.ValueMember = "Id";
            this.leUserRoles.Size = new System.Drawing.Size(209, 22);
            this.leUserRoles.TabIndex = 6;
            this.leUserRoles.EditValueChanged += new System.EventHandler(this.leUserRoles_EditValueChanged);
            // 
            // rgObjectTypes
            // 
            this.rgObjectTypes.EditValue = 3;
            this.rgObjectTypes.Location = new System.Drawing.Point(126, 38);
            this.rgObjectTypes.Name = "rgObjectTypes";
            this.rgObjectTypes.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.rgObjectTypes.Properties.Appearance.Options.UseBackColor = true;
            this.rgObjectTypes.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(2, "����������� ����"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(3, "������"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(1, "������������ ��������"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(6, "������")});
            this.rgObjectTypes.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.rgObjectTypes.Size = new System.Drawing.Size(209, 98);
            this.rgObjectTypes.TabIndex = 5;
            this.rgObjectTypes.SelectedIndexChanged += new System.EventHandler(this.rgObjectTypes_SelectedIndexChanged);
            // 
            // UsersList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainerControl);
            this.Name = "UsersList";
            this.Size = new System.Drawing.Size(1247, 528);
            this.Load += new System.EventHandler(this.UsersList_Load);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl)).EndInit();
            this.splitContainerControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcUsers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvUsers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leRoles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rteWinLogin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnViews)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leUserRoles.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgObjectTypes.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl;
        private DevExpress.XtraGrid.GridControl gcUsers;
        private DevExpress.XtraGrid.Views.Grid.GridView gvUsers;
        private DevExpress.XtraGrid.Columns.GridColumn colId;
        private DevExpress.XtraGrid.Columns.GridColumn colName;
        private DevExpress.XtraGrid.Columns.GridColumn colPassword;
        private DevExpress.XtraGrid.Columns.GridColumn colAdmin;
        private DevExpress.XtraGrid.Columns.GridColumn colWinLogin;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit rteWinLogin;
        private DevExpress.XtraGrid.Columns.GridColumn colUserRole;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit leRoles;
        private DevExpress.XtraEditors.RadioGroup rgObjectTypes;
        private DevExpress.XtraEditors.LookUpEdit leUserRoles;
        private DevExpress.XtraEditors.LabelControl lbRoles;
        private DevExpress.XtraEditors.PanelControl pnViews;
        private DevExpress.XtraEditors.LabelControl lbObjects;
        private DevExpress.XtraEditors.SimpleButton sbSaveObjectValues;
        private DevExpress.XtraEditors.SimpleButton sbEdit;
    }
}
