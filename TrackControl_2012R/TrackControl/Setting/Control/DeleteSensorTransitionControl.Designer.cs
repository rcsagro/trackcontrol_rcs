namespace TrackControl.Setting.Controls
{
  partial class DeleteSensorTransitionControl
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DeleteSensorTransitionControl));
            this.mainTable = new System.Windows.Forms.TableLayoutPanel();
            this._deleteBtn = new DevExpress.XtraEditors.SimpleButton();
            this._cancelBtn = new DevExpress.XtraEditors.SimpleButton();
            this.iconImg = new DevExpress.XtraEditors.PanelControl();
            this.titleLbl = new DevExpress.XtraEditors.LabelControl();
            this.notifyLbl = new DevExpress.XtraEditors.LabelControl();
            this.askingLbl = new DevExpress.XtraEditors.LabelControl();
            this.mainTable.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.iconImg)).BeginInit();
            this.SuspendLayout();
            // 
            // mainTable
            // 
            this.mainTable.BackColor = System.Drawing.Color.Transparent;
            this.mainTable.ColumnCount = 4;
            this.mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 90F));
            this.mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 153F));
            this.mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.mainTable.Controls.Add(this._deleteBtn, 2, 4);
            this.mainTable.Controls.Add(this._cancelBtn, 3, 4);
            this.mainTable.Controls.Add(this.iconImg, 0, 1);
            this.mainTable.Controls.Add(this.titleLbl, 1, 1);
            this.mainTable.Controls.Add(this.notifyLbl, 2, 3);
            this.mainTable.Controls.Add(this.askingLbl, 0, 0);
            this.mainTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainTable.Location = new System.Drawing.Point(10, 10);
            this.mainTable.Margin = new System.Windows.Forms.Padding(0);
            this.mainTable.Name = "mainTable";
            this.mainTable.RowCount = 5;
            this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.mainTable.Size = new System.Drawing.Size(420, 160);
            this.mainTable.TabIndex = 0;
            // 
            // _deleteBtn
            // 
            this._deleteBtn.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this._deleteBtn.Appearance.Options.UseFont = true;
            this._deleteBtn.Dock = System.Windows.Forms.DockStyle.Fill;
            this._deleteBtn.Image = ((System.Drawing.Image)(resources.GetObject("_deleteBtn.Image")));
            this._deleteBtn.Location = new System.Drawing.Point(230, 130);
            this._deleteBtn.Name = "_deleteBtn";
            this._deleteBtn.Size = new System.Drawing.Size(147, 27);
            this._deleteBtn.TabIndex = 6;
            this._deleteBtn.Text = "������� �������";
            this._deleteBtn.Click += new System.EventHandler(this.deleteBtn_Click);
            this._deleteBtn.MouseEnter += new System.EventHandler(this.deleteBtn_MouseEnter);
            this._deleteBtn.MouseLeave += new System.EventHandler(this.btn_MouseLeave);
            // 
            // _cancelBtn
            // 
            this._cancelBtn.Dock = System.Windows.Forms.DockStyle.Fill;
            this._cancelBtn.Image = ((System.Drawing.Image)(resources.GetObject("_cancelBtn.Image")));
            this._cancelBtn.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this._cancelBtn.Location = new System.Drawing.Point(383, 130);
            this._cancelBtn.Name = "_cancelBtn";
            this._cancelBtn.Size = new System.Drawing.Size(34, 27);
            this._cancelBtn.TabIndex = 7;
            this._cancelBtn.Click += new System.EventHandler(this.cancelBtn_Click);
            this._cancelBtn.MouseEnter += new System.EventHandler(this.cancelBtn_MouseEnter);
            this._cancelBtn.MouseLeave += new System.EventHandler(this.btn_MouseLeave);
            // 
            // iconImg
            // 
            this.iconImg.Dock = System.Windows.Forms.DockStyle.Fill;
            this.iconImg.Location = new System.Drawing.Point(3, 33);
            this.iconImg.Name = "iconImg";
            this.iconImg.Size = new System.Drawing.Size(84, 64);
            this.iconImg.TabIndex = 8;
            // 
            // titleLbl
            // 
            this.titleLbl.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.mainTable.SetColumnSpan(this.titleLbl, 3);
            this.titleLbl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.titleLbl.Location = new System.Drawing.Point(93, 33);
            this.titleLbl.Name = "titleLbl";
            this.mainTable.SetRowSpan(this.titleLbl, 2);
            this.titleLbl.Size = new System.Drawing.Size(75, 15);
            this.titleLbl.TabIndex = 9;
            this.titleLbl.Text = "labelControl1";
            // 
            // notifyLbl
            // 
            this.notifyLbl.Appearance.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
            this.notifyLbl.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.notifyLbl.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.notifyLbl.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.mainTable.SetColumnSpan(this.notifyLbl, 2);
            this.notifyLbl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.notifyLbl.Location = new System.Drawing.Point(230, 110);
            this.notifyLbl.Name = "notifyLbl";
            this.notifyLbl.Size = new System.Drawing.Size(73, 14);
            this.notifyLbl.TabIndex = 10;
            this.notifyLbl.Text = "labelControl1";
            // 
            // askingLbl
            // 
            this.askingLbl.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.askingLbl.Appearance.ForeColor = System.Drawing.Color.Brown;
            this.mainTable.SetColumnSpan(this.askingLbl, 3);
            this.askingLbl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.askingLbl.Location = new System.Drawing.Point(3, 3);
            this.askingLbl.Name = "askingLbl";
            this.askingLbl.Size = new System.Drawing.Size(295, 15);
            this.askingLbl.TabIndex = 11;
            this.askingLbl.Text = "�� �������, ��� ������ ������� ������ �������?";
            // 
            // DeleteSensorTransitionControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.mainTable);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "DeleteSensorTransitionControl";
            this.Padding = new System.Windows.Forms.Padding(10);
            this.Size = new System.Drawing.Size(440, 180);
            this.Load += new System.EventHandler(this.DeleteSensorStateControl_Load);
            this.mainTable.ResumeLayout(false);
            this.mainTable.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.iconImg)).EndInit();
            this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.TableLayoutPanel mainTable;
    private DevExpress.XtraEditors.SimpleButton _deleteBtn;
    private DevExpress.XtraEditors.SimpleButton _cancelBtn;
    private DevExpress.XtraEditors.PanelControl iconImg;
    private DevExpress.XtraEditors.LabelControl titleLbl;
    private DevExpress.XtraEditors.LabelControl notifyLbl;
    private DevExpress.XtraEditors.LabelControl askingLbl;
  }
}
