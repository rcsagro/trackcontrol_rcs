using System;
using DevExpress.XtraEditors;
using TrackControl.Properties;

namespace TrackControl.Setting.Control
{
  public partial class LayoutReset : XtraUserControl
  {
    public LayoutReset()
    {
      InitializeComponent();
      init();
    }

    void init()
    {
      _groupPanel.Text = Resources.DefaultTemplates;
      _onlineLbl.Text = String.Format("  {0}", Resources.Mode_Monitoring);
      _reportsLbl.Text = String.Format(" {0}", Resources.Mode_Reports);
      _reportsBtn.Text = Resources.Reset;
      _onlineBtn.Text = Resources.Reset;
    }

    void _onlineBtn_Click(object sender, EventArgs e)
    {
      AppModel.Instance.OnlineMode.ResetLayout();
    }

    void _reportsBtn_Click(object sender, EventArgs e)
    {
      AppModel.Instance.ReportsMode.ResetLayout();
    }
  }
}
