using System;
using System.ComponentModel;
using System.Windows.Forms;
using TrackControl.Properties;

namespace TrackControl.Setting.Controls
{
    [ToolboxItem(false)]
    public partial class AddButtonControl : UserControl
    {
        /// <summary>
        /// �������, ����������� ��� ����� �� ������ "��������".
        /// </summary>
        public event EventHandler AddPressed;

        /// <summary>
        /// �������, ����������� ��� ����� �� ������ "�����".
        /// </summary>
        public event EventHandler BackPressed;

        /// <summary>
        /// �����������.
        /// </summary>
        public AddButtonControl()
        {
            InitializeComponent();
            init();
        }

        /// <summary>
        /// ���������� ������� AddPressed.
        /// </summary>
        private void onAddPressed()
        {
            if (AddPressed != null)
                AddPressed(this, EventArgs.Empty);
        }

        /// <summary>
        /// ���������� ������� BackPressed.
        /// </summary>
        private void onBackPressed()
        {
            if (BackPressed != null)
                BackPressed(this, EventArgs.Empty);
        }

        /// <summary>
        /// ������������ ���� �� ������ "��������".
        /// </summary>
        private void addBtn_Click(object sender, EventArgs e)
        {
            onAddPressed();
        }

        /// <summary>
        /// ������������ ���� �� ������ "�����".
        /// </summary>
        private void backBtn_Click(object sender, EventArgs e)
        {
            onBackPressed();
        }

        private void init()
        {
            _addBtn.Text = Resources.Add;
            _backBtn.Text = Resources.Back;
        }
    }
}
