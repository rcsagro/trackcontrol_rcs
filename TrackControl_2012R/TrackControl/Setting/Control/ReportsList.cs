using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using BaseReports.ReportListSetting;
using TrackControl.Properties;
using TrackControl.General;

namespace TrackControl.Setting
{
    public partial class ReportsList : DevExpress.XtraEditors.XtraUserControl
    {
        ReportListCtrl _rl_ctrl;

        public ReportsList()
        {
            InitializeComponent();
            Localization();
        }


        private void Localization()
        {
            colCaption.Caption = Resources.ReportListFormColTitleText;
            colVisible.Caption = Resources.ReportListFormColVisibilityText;
        }

        private void gvRList_CustomDrawRowIndicator(object sender, DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventArgs e)
        {
            if (e.Info.IsRowIndicator)
            {
                e.Info.DisplayText = (e.RowHandle + 1).ToString();
            }
        }

        private void ReportsList_Load(object sender, EventArgs e)
        {
            _rl_ctrl = new ReportListCtrl();
            gcRList.DataSource = _rl_ctrl.GetReportsList();
        }


        public IList<IEntity> UnChecked
        {
            get
            {
                List<ReportSetting> settings = (List<ReportSetting>)gcRList.DataSource;
                List<IEntity> uncheck = new List<IEntity>();
                if (settings != null)
                {
                    foreach (ReportSetting setting in settings)
                    {
                        if (!setting.Visible)
                        {
                            uncheck.Add((IEntity)setting);
                        }
                    }
                }
                return uncheck;
            }
        }

        public void UncheckUnvisible(List<ReportSetting> unvisibleItems)
        {
            List<ReportSetting> settings = _rl_ctrl.GetReportsList();
            foreach (ReportSetting setting in settings)
           {
               setting.Visible = true;
               foreach (ReportSetting unvisible in unvisibleItems)
               {
                   if (setting.Id == unvisible.Id)
                   {
                       setting.Visible = false;
                       break;
                   }
               }
           }
            gcRList.DataSource = settings;
        }
    }
}
