namespace TrackControl.Setting
{
    partial class ModulesList
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gcMList = new DevExpress.XtraGrid.GridControl();
            this.gvMList = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colVisible = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCaption = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gcMList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvMList)).BeginInit();
            this.SuspendLayout();
            // 
            // gcMList
            // 
            this.gcMList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcMList.Location = new System.Drawing.Point(0, 0);
            this.gcMList.MainView = this.gvMList;
            this.gcMList.Name = "gcMList";
            this.gcMList.Size = new System.Drawing.Size(449, 446);
            this.gcMList.TabIndex = 0;
            this.gcMList.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvMList});
            // 
            // gvMList
            // 
            this.gvMList.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvMList.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvMList.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gvMList.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvMList.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvMList.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvMList.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvMList.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvMList.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gvMList.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvMList.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvMList.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvMList.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvMList.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gvMList.Appearance.Empty.Options.UseBackColor = true;
            this.gvMList.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvMList.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvMList.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gvMList.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvMList.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gvMList.Appearance.EvenRow.Options.UseForeColor = true;
            this.gvMList.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvMList.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvMList.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gvMList.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvMList.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvMList.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gvMList.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvMList.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gvMList.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvMList.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvMList.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvMList.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.gvMList.Appearance.FixedLine.Options.UseBackColor = true;
            this.gvMList.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gvMList.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gvMList.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gvMList.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gvMList.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(192)))), ((int)(((byte)(157)))));
            this.gvMList.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(219)))), ((int)(((byte)(188)))));
            this.gvMList.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gvMList.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvMList.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gvMList.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvMList.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvMList.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvMList.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvMList.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvMList.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvMList.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gvMList.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvMList.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvMList.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvMList.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvMList.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvMList.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvMList.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gvMList.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvMList.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvMList.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gvMList.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(242)))), ((int)(((byte)(213)))));
            this.gvMList.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gvMList.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gvMList.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvMList.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvMList.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvMList.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvMList.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gvMList.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvMList.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gvMList.Appearance.GroupRow.Options.UseForeColor = true;
            this.gvMList.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvMList.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvMList.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gvMList.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvMList.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvMList.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gvMList.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvMList.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvMList.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gvMList.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvMList.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.gvMList.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gvMList.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvMList.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvMList.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvMList.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvMList.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gvMList.Appearance.OddRow.Options.UseBackColor = true;
            this.gvMList.Appearance.OddRow.Options.UseBorderColor = true;
            this.gvMList.Appearance.OddRow.Options.UseForeColor = true;
            this.gvMList.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(247)))));
            this.gvMList.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gvMList.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(148)))), ((int)(((byte)(148)))));
            this.gvMList.Appearance.Preview.Options.UseBackColor = true;
            this.gvMList.Appearance.Preview.Options.UseFont = true;
            this.gvMList.Appearance.Preview.Options.UseForeColor = true;
            this.gvMList.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvMList.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gvMList.Appearance.Row.Options.UseBackColor = true;
            this.gvMList.Appearance.Row.Options.UseForeColor = true;
            this.gvMList.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvMList.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gvMList.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvMList.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.gvMList.Appearance.SelectedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvMList.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.gvMList.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvMList.Appearance.SelectedRow.Options.UseBorderColor = true;
            this.gvMList.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gvMList.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gvMList.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gvMList.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvMList.Appearance.VertLine.Options.UseBackColor = true;
            this.gvMList.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colId,
            this.colVisible,
            this.colCaption});
            this.gvMList.GridControl = this.gcMList;
            this.gvMList.IndicatorWidth = 30;
            this.gvMList.Name = "gvMList";
            this.gvMList.OptionsView.EnableAppearanceEvenRow = true;
            this.gvMList.OptionsView.EnableAppearanceOddRow = true;
            this.gvMList.OptionsView.ShowGroupPanel = false;
            this.gvMList.CustomDrawRowIndicator += new DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventHandler(this.gvRList_CustomDrawRowIndicator);
            // 
            // colId
            // 
            this.colId.Caption = "Id";
            this.colId.FieldName = "Id";
            this.colId.Name = "colId";
            this.colId.OptionsColumn.AllowEdit = false;
            // 
            // colVisible
            // 
            this.colVisible.AppearanceHeader.Options.UseTextOptions = true;
            this.colVisible.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colVisible.Caption = "���������";
            this.colVisible.FieldName = "Visible";
            this.colVisible.Name = "colVisible";
            this.colVisible.Visible = true;
            this.colVisible.VisibleIndex = 0;
            this.colVisible.Width = 255;
            // 
            // colCaption
            // 
            this.colCaption.AppearanceHeader.Options.UseTextOptions = true;
            this.colCaption.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCaption.Caption = "�������� ������";
            this.colCaption.FieldName = "Name";
            this.colCaption.Name = "colCaption";
            this.colCaption.OptionsColumn.AllowEdit = false;
            this.colCaption.OptionsColumn.ReadOnly = true;
            this.colCaption.Visible = true;
            this.colCaption.VisibleIndex = 1;
            this.colCaption.Width = 673;
            // 
            // ModulesList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gcMList);
            this.Name = "ModulesList";
            this.Size = new System.Drawing.Size(449, 446);
            ((System.ComponentModel.ISupportInitialize)(this.gcMList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvMList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gcMList;
        private DevExpress.XtraGrid.Views.Grid.GridView gvMList;
        private DevExpress.XtraGrid.Columns.GridColumn colVisible;
        private DevExpress.XtraGrid.Columns.GridColumn colCaption;
        private DevExpress.XtraGrid.Columns.GridColumn colId;
    }
}
