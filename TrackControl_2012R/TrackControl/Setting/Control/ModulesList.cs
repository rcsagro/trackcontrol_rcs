using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using BaseReports.ReportListSetting;
using TrackControl.Properties;
using TrackControl.General;
using TrackControl.AccessObjects;
using System.Linq;

namespace TrackControl.Setting
{
    public partial class ModulesList : DevExpress.XtraEditors.XtraUserControl
    {

        public ModulesList(BindingList<TCModule> modules)
        {
            InitializeComponent();
            Localization();
            gcMList.DataSource = modules;
        }


        private void Localization()
        {
            colCaption.Caption = Resources.Title;
            colVisible.Caption = Resources.ReportListFormColVisibilityText;
        }

        private void gvRList_CustomDrawRowIndicator(object sender, DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventArgs e)
        {
            if (e.Info.IsRowIndicator)
            {
                e.Info.DisplayText = (e.RowHandle + 1).ToString();
            }
        }



        public IList<IEntity> UnChecked
        {
            get
            {
                List<IEntity> uncheck = new List<IEntity>();
                if ((BindingList<TCModule>)gcMList.DataSource != null)
                {
                    foreach (TCModule module in (BindingList<TCModule>)gcMList.DataSource)
                    {
                        if (!module.Visible)
                        {
                            uncheck.Add((IEntity)module);
                        }
                    }
                }
                return uncheck;
            }
        }

        public void UncheckUnvisible(List<TCModule> unvisibleItems)
        {
            foreach (TCModule module in (BindingList<TCModule>)gcMList.DataSource)
            {
                module.Visible = true;
                foreach (TCModule unvisible in unvisibleItems)
                {
                    if (module.Id == unvisible.Id)
                    {
                        module.Visible = false;
                    }
                }
            }
            gcMList.RefreshDataSource(); 
            //gcMList.DataSource = _modules;
        }

    }
}
