using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using LocalCache;
using TrackControl.General;
using TrackControl.Properties;

namespace TrackControl.Setting.Controls
{
    [ToolboxItem(false)]
    public partial class LogicSensorEventsEditPanel : UserControl
    {
        #region Fields

        /// <summary>
        /// ���������� ������, ��� �������� ������������� �������.
        /// </summary>
        private atlantaDataSet.sensorsRow _sensor;

        /// <summary>
        /// ������� ��� �������� �� "0" � "1".
        /// </summary>
        private atlantaDataSet.TransitionRow _transition01;

        /// <summary>
        /// ������� ��� �������� �� "1" � "0".
        /// </summary>
        private atlantaDataSet.TransitionRow _transition10;

        /// <summary>
        /// ��������� ��� ���������� "0".
        /// </summary>
        private atlantaDataSet.StateRow _zeroState;

        /// <summary>
        /// ��������� ��� ���������� "1".
        /// </summary>
        private atlantaDataSet.StateRow _unityState;

        /// <summary>
        /// ������� ����������.
        /// </summary>
        private atlantaDataSet _dataset;

        /// <summary>
        /// ������� ��� ������� "Transition", ������� ������ ��������� ���
        /// �������, ����������� ��� ��������� ��������� �������.
        /// </summary>
        private LocalCache.atlantaDataSetTableAdapters.TransitionTableAdapter _adapter;

        /// <summary>
        /// �������� �� ��������������.
        /// </summary>
        private bool _editBegin = false;

        #endregion

        /// <summary>
        /// ��������� ����� ��������� �������������� ��������� ����������� �������.
        /// �.�. ����� ������� ������ "���������" ���� "������".
        /// </summary>
        public event EventHandler<EndEditEventArgs> EndEdit;
        public event EventHandler<EndEditEventArgs> EndEditEvent;

        /// <summary>
        /// �����������.
        /// </summary>
        public LogicSensorEventsEditPanel(
            LocalCache.atlantaDataSet dataset,
            LocalCache.atlantaDataSetTableAdapters.TransitionTableAdapter adapter)
        {
            _dataset = dataset;
            _adapter = adapter;

            InitializeComponent();
            init();

            icon01Combo.SelectedIndexChanged += icon01Combo_SelectedIndexChanged;
            sound01Combo.SelectedIndexChanged += sound01Combo_SelectedIndexChanged;
            icon10Combo.SelectedIndexChanged += icon10Combo_SelectedIndexChanged;
            sound10Combo.SelectedIndexChanged +=sound10Combo_SelectedIndexChanged;
        }

        /// <summary>
        /// ����������� ��������� ������� ��� ����������� �������.
        /// </summary>
        /// <param name="sensor">�� ����, ���������� ������, ��� �������� ������������� �������.</param>
        public void EditEventsForSensor(atlantaDataSet.sensorsRow sensor)
        {
            foreach (atlantaDataSet.StateRow state in sensor.GetStateRows())
            {
                if (state.MinValue < 0.1)
                    _zeroState = state;
                if (state.MinValue >= 1)
                    _unityState = state;
            }

          //  if (_zeroState == null || _unityState == null)
          //      onEndEditEvent(true);

            foreach (atlantaDataSet.TransitionRow transition in sensor.GetTransitionRows())
            {
                if( transition.StateRowByState_Transition_Initial != null)
                    if (transition.StateRowByState_Transition_Initial.MinValue < 0.1)
                        _transition01 = transition;

                if( transition.StateRowByState_Transition_Initial != null )
                    if (transition.StateRowByState_Transition_Initial.MinValue >= 1)
                        _transition10 = transition;
            }

            _sensor = sensor;

            transition01Box.Text = (_transition01 != null) ? _transition01.Title : "";
            notify01FlagCheck.Checked = (_transition01 != null) ? _transition01.Enabled : false;
            report01FlagCheck.Checked = (_transition01 != null) ? _transition01.ViewInReport : false;
            string icon01Name = (_transition01 != null) ? _transition01.IconName : "Default";
            icon01Combo.Properties.Items.AddRange(EventTracker.Helper.GetImageNames());
            icon01Combo.Text = icon01Name;
            string sound01Name = (_transition01 != null) ? _transition01.SoundName : "Default";
            sound01Combo.Properties.Items.AddRange(EventTracker.Helper.GetSoundNames());
            sound01Combo.Text = sound01Name;

            transition10Box.Text = (_transition10 != null) ? _transition10.Title : "";
            notify10FlagCheck.Checked = (_transition10 != null) ? _transition10.Enabled : false;
            report10FlagCheck.Checked = (_transition10 != null) ? _transition10.ViewInReport : false;
            string icon10Name = (_transition10 != null) ? _transition10.IconName : "Default";
            icon10Combo.Properties.Items.AddRange(EventTracker.Helper.GetImageNames());
            icon10Combo.Text = icon10Name;
            string sound10Name = (_transition10 != null) ? _transition10.SoundName : "Default";
            sound10Combo.Properties.Items.AddRange(EventTracker.Helper.GetSoundNames());
            sound10Combo.Text = sound10Name;

            _editBegin = true;
        }

        private void init()
        {
            notify01FlagCheck.Text = Resources.Alarm;
            report01FlagCheck.Text = Resources.IncludeInReport;
            notify10FlagCheck.Text = Resources.Alarm;
            report10FlagCheck.Text = Resources.IncludeInReport;
            gp01Box.Text = Resources.From0to1;
            head01Lbl.Text = Resources.Description;
            icon01Lbl.Text = Resources.Icon;
            sound01Lbl.Text = Resources.Beep;
            gp10Box.Text = Resources.From1to0;
            head10Lbl.Text = Resources.Description;
            icon10Lbl.Text = Resources.Icon;
            sound10Lbl.Text = Resources.Beep;
            _saveBtn.Text = Resources.Save;
        }

        /// <summary>
        /// ���������� ����������� � ������������� ������� EndEdit
        /// </summary>
        /// <param name="canceled">����, ����������� ���� ������ ������ "������" ��� ������ "���������".</param>
        private void onEndEdit(bool canceled)
        {
            _sensor = null;
            _transition01 = null;
            _transition10 = null;
            _unityState = null;
            _zeroState = null;

            _editBegin = false;
            EventTracker.Helper.StopPlaying();

            if (EndEdit != null)
                EndEdit(this, new EndEditEventArgs(canceled));
        }

        /// <summary>
        /// ���������� ����������� � ������������� ������� EndEdit
        /// </summary>
        /// <param name="canceled">����, ����������� ���� ������ ������ "������" ��� ������ "���������".</param>
        private void onEndEditEvent( bool canceled )
        {
            _sensor = null;
            _transition01 = null;
            _transition10 = null;
            _unityState = null;
            _zeroState = null;

            _editBegin = false;
            EventTracker.Helper.StopPlaying();

            if( EndEditEvent != null )
                EndEditEvent( this, new EndEditEventArgs( canceled ) );
        }

        /// <summary>
        /// ��������� ������������ ��������� ������������� ������.
        /// </summary>
        private bool validateEventSettings()
        {
            if (_unityState == null || _zeroState == null || _unityState.Id <= 0 || _zeroState.Id <= 0)
            {
                MessageBox.Show("State may be not null!","Error");
                return false;
            }
            return true;
        }

        #region GUI Event Handlers

        /// <summary>
        /// ������������ ���� �� ������ "������".
        /// </summary>
        private void cancelBtn_Click(object sender, EventArgs e)
        {
            onEndEditEvent(true);
        }

        /// <summary>
        /// ������������ ���� �� ������ "���������".
        /// </summary>
        private void saveBtn_Click(object sender, EventArgs e)
        {
            if (!validateEventSettings()) 
                return;

            if (_transition01 != null)
            {
                _transition01.Title = transition01Box.Text;
                _transition01.IconName = icon01Combo.Text;
                _transition01.SoundName = sound01Combo.Text;
                _transition01.Enabled = notify01FlagCheck.Checked;
                _transition01.ViewInReport = report01FlagCheck.Checked;
            }
            else
            {
                atlantaDataSet.TransitionRow transition01 = (atlantaDataSet.TransitionRow) _dataset.Transition.NewRow();
                transition01.StateRowByState_Transition_Initial = _zeroState;
                transition01.StateRowByState_Transition_Final = _unityState;
                transition01.Title = transition01Box.Text;
                transition01.sensorsRow = _sensor;
                transition01.IconName = icon01Combo.Text;
                transition01.SoundName = sound01Combo.Text;
                transition01.Enabled = notify01FlagCheck.Checked;
                transition01.ViewInReport = report01FlagCheck.Checked;
                _dataset.Transition.AddTransitionRow(transition01);
            }
            _adapter.Update(_dataset.Transition);

            if (_transition10 != null)
            {
                _transition10.Title = transition10Box.Text;
                _transition10.IconName = icon10Combo.Text;
                _transition10.SoundName = sound10Combo.Text;
                _transition10.Enabled = notify10FlagCheck.Checked;
                _transition10.ViewInReport = report10FlagCheck.Checked;
            }
            else
            {

                    atlantaDataSet.TransitionRow transition10 = (atlantaDataSet.TransitionRow)_dataset.Transition.NewRow();
                    transition10.StateRowByState_Transition_Initial = _unityState;
                    transition10.StateRowByState_Transition_Final = _zeroState;
                    transition10.Title = transition10Box.Text;
                    transition10.sensorsRow = _sensor;
                    transition10.IconName = icon10Combo.Text;
                    transition10.SoundName = sound10Combo.Text;
                    transition10.Enabled = notify10FlagCheck.Checked;
                    transition10.ViewInReport = report10FlagCheck.Checked;
                    _dataset.Transition.AddTransitionRow(transition10);
            }

            _adapter.Update(_dataset.Transition);
            onEndEditEvent(false);
        }

        /// <summary>
        /// ������������ ��������� ��������� ���� �� ������ "���������".
        /// </summary>
        private void saveBtn_MouseEnter(object sender, EventArgs e)
        {
            notifyLbl.ForeColor = Color.DarkGreen;
            notifyLbl.Text = Resources.SaveChanges;
        }

        /// <summary>
        /// ������������ ��������� ��������� ���� �� ������ "������".
        /// </summary>
        private void cancelBtn_MouseEnter(object sender, EventArgs e)
        {
            notifyLbl.ForeColor = Color.DarkBlue;
            notifyLbl.Text = Resources.CancelChanges;
        }

        /// <summary>
        /// ������������ �������� ��������� ���� � ������.
        /// </summary>
        private void btn_MouseLeave(object sender, EventArgs e)
        {
            notifyLbl.Text = "";
        }

        /// <summary>
        /// ������������ ��������� ������ ������ ��� �������� �� "0" � "1".
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void icon01Combo_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.icon01Img.ContentImage = EventTracker.Helper.GetImage(this.icon01Combo.Text);
            Application.DoEvents();
        }

        /// <summary>
        /// ������������ ��������� ������ ��������� ������� ��� �������� �� "0" � "1".
        /// </summary>
        private void sound01Combo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_editBegin)
                EventTracker.Helper.PlayWavFile(Globals.APP_DATA, sound01Combo.Text);
        }

        /// <summary>
        /// ������������ ��������� ������ ������ ��� �������� �� "0" � "1".
        /// </summary>
        private void icon10Combo_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.icon10Img.ContentImage = EventTracker.Helper.GetImage(icon10Combo.Text);
            Application.DoEvents();
        }

        /// <summary>
        /// ������������ ��������� ������ ��������� ������� ��� �������� �� "0" � "1".
        /// </summary>
        private void sound10Combo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_editBegin)
                EventTracker.Helper.PlayWavFile(Globals.APP_DATA, sound10Combo.Text);
        }

        #endregion
    }
}
