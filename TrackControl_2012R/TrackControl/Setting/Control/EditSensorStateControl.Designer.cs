namespace TrackControl.Setting.Controls
{
  partial class EditSensorStateControl
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditSensorStateControl));
            this.mainTable = new System.Windows.Forms.TableLayoutPanel();
            this._saveBtn = new DevExpress.XtraEditors.SimpleButton();
            this._cancelBtn = new DevExpress.XtraEditors.SimpleButton();
            this.sensorLbl = new DevExpress.XtraEditors.LabelControl();
            this.minValueLbl = new DevExpress.XtraEditors.LabelControl();
            this.MaxValueLbl = new DevExpress.XtraEditors.LabelControl();
            this.sensorTbx = new DevExpress.XtraEditors.TextEdit();
            this.errorLbl = new DevExpress.XtraEditors.LabelControl();
            this.minValueTbx = new DevExpress.XtraEditors.TextEdit();
            this.maxValueTbx = new DevExpress.XtraEditors.TextEdit();
            this.notifyLbl = new DevExpress.XtraEditors.LabelControl();
            this.mainTable.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sensorTbx.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.minValueTbx.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.maxValueTbx.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // mainTable
            // 
            this.mainTable.ColumnCount = 8;
            this.mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 130F));
            this.mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.mainTable.Controls.Add(this._saveBtn, 5, 3);
            this.mainTable.Controls.Add(this._cancelBtn, 6, 3);
            this.mainTable.Controls.Add(this.sensorLbl, 1, 1);
            this.mainTable.Controls.Add(this.minValueLbl, 1, 3);
            this.mainTable.Controls.Add(this.MaxValueLbl, 3, 3);
            this.mainTable.Controls.Add(this.sensorTbx, 1, 2);
            this.mainTable.Controls.Add(this.errorLbl, 1, 5);
            this.mainTable.Controls.Add(this.minValueTbx, 1, 4);
            this.mainTable.Controls.Add(this.maxValueTbx, 3, 4);
            this.mainTable.Controls.Add(this.notifyLbl, 5, 2);
            this.mainTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainTable.Location = new System.Drawing.Point(0, 0);
            this.mainTable.Margin = new System.Windows.Forms.Padding(0);
            this.mainTable.Name = "mainTable";
            this.mainTable.RowCount = 7;
            this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 1F));
            this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 19F));
            this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 18F));
            this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.mainTable.Size = new System.Drawing.Size(440, 120);
            this.mainTable.TabIndex = 0;
            // 
            // _saveBtn
            // 
            this._saveBtn.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this._saveBtn.Appearance.Options.UseFont = true;
            this._saveBtn.Dock = System.Windows.Forms.DockStyle.Fill;
            this._saveBtn.Image = ((System.Drawing.Image)(resources.GetObject("_saveBtn.Image")));
            this._saveBtn.Location = new System.Drawing.Point(258, 58);
            this._saveBtn.Margin = new System.Windows.Forms.Padding(3, 10, 12, 3);
            this._saveBtn.Name = "_saveBtn";
            this.mainTable.SetRowSpan(this._saveBtn, 2);
            this._saveBtn.Size = new System.Drawing.Size(115, 33);
            this._saveBtn.TabIndex = 10;
            this._saveBtn.Text = "���������";
            this._saveBtn.Click += new System.EventHandler(this.saveBtn_Click);
            this._saveBtn.MouseEnter += new System.EventHandler(this.saveBtn_MouseEnter);
            this._saveBtn.MouseLeave += new System.EventHandler(this.saveBtn_MouseLeave);
            // 
            // _cancelBtn
            // 
            this._cancelBtn.Dock = System.Windows.Forms.DockStyle.Fill;
            this._cancelBtn.Image = ((System.Drawing.Image)(resources.GetObject("_cancelBtn.Image")));
            this._cancelBtn.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this._cancelBtn.Location = new System.Drawing.Point(392, 58);
            this._cancelBtn.Margin = new System.Windows.Forms.Padding(7, 10, 3, 3);
            this._cancelBtn.Name = "_cancelBtn";
            this.mainTable.SetRowSpan(this._cancelBtn, 2);
            this._cancelBtn.Size = new System.Drawing.Size(30, 33);
            this._cancelBtn.TabIndex = 11;
            this._cancelBtn.Click += new System.EventHandler(this.cancelBtn_Click);
            this._cancelBtn.MouseEnter += new System.EventHandler(this.cancelBtn_MouseEnter);
            this._cancelBtn.MouseLeave += new System.EventHandler(this.cancelBtn_MouseLeave);
            // 
            // sensorLbl
            // 
            this.sensorLbl.Appearance.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.mainTable.SetColumnSpan(this.sensorLbl, 3);
            this.sensorLbl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sensorLbl.Location = new System.Drawing.Point(18, 4);
            this.sensorLbl.Name = "sensorLbl";
            this.sensorLbl.Size = new System.Drawing.Size(115, 14);
            this.sensorLbl.TabIndex = 12;
            this.sensorLbl.Text = "�������� ���������";
            // 
            // minValueLbl
            // 
            this.minValueLbl.Appearance.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
            this.minValueLbl.Location = new System.Drawing.Point(18, 51);
            this.minValueLbl.Name = "minValueLbl";
            this.minValueLbl.Size = new System.Drawing.Size(80, 14);
            this.minValueLbl.TabIndex = 13;
            this.minValueLbl.Text = "������ �����";
            // 
            // MaxValueLbl
            // 
            this.MaxValueLbl.Appearance.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
            this.MaxValueLbl.Location = new System.Drawing.Point(138, 51);
            this.MaxValueLbl.Name = "MaxValueLbl";
            this.MaxValueLbl.Size = new System.Drawing.Size(93, 14);
            this.MaxValueLbl.TabIndex = 14;
            this.MaxValueLbl.Text = "������� ������";
            // 
            // sensorTbx
            // 
            this.mainTable.SetColumnSpan(this.sensorTbx, 3);
            this.sensorTbx.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sensorTbx.EditValue = "���������� �������";
            this.sensorTbx.Location = new System.Drawing.Point(18, 24);
            this.sensorTbx.Name = "sensorTbx";
            this.sensorTbx.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F);
            this.sensorTbx.Properties.Appearance.Options.UseFont = true;
            this.sensorTbx.Size = new System.Drawing.Size(214, 22);
            this.sensorTbx.TabIndex = 15;
            // 
            // errorLbl
            // 
            this.errorLbl.Appearance.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
            this.mainTable.SetColumnSpan(this.errorLbl, 6);
            this.errorLbl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.errorLbl.Location = new System.Drawing.Point(18, 97);
            this.errorLbl.Name = "errorLbl";
            this.errorLbl.Size = new System.Drawing.Size(3, 14);
            this.errorLbl.TabIndex = 16;
            this.errorLbl.Text = " ";
            // 
            // minValueTbx
            // 
            this.minValueTbx.Dock = System.Windows.Forms.DockStyle.Fill;
            this.minValueTbx.EditValue = "100.00";
            this.minValueTbx.Location = new System.Drawing.Point(18, 70);
            this.minValueTbx.Name = "minValueTbx";
            this.minValueTbx.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F);
            this.minValueTbx.Properties.Appearance.Options.UseFont = true;
            this.minValueTbx.Size = new System.Drawing.Size(94, 22);
            this.minValueTbx.TabIndex = 17;
            // 
            // maxValueTbx
            // 
            this.maxValueTbx.Dock = System.Windows.Forms.DockStyle.Fill;
            this.maxValueTbx.EditValue = "2400.00";
            this.maxValueTbx.Location = new System.Drawing.Point(138, 70);
            this.maxValueTbx.Name = "maxValueTbx";
            this.maxValueTbx.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F);
            this.maxValueTbx.Properties.Appearance.Options.UseFont = true;
            this.maxValueTbx.Size = new System.Drawing.Size(94, 22);
            this.maxValueTbx.TabIndex = 18;
            // 
            // notifyLbl
            // 
            this.notifyLbl.Appearance.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
            this.mainTable.SetColumnSpan(this.notifyLbl, 2);
            this.notifyLbl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.notifyLbl.Location = new System.Drawing.Point(258, 24);
            this.notifyLbl.Name = "notifyLbl";
            this.notifyLbl.Size = new System.Drawing.Size(3, 14);
            this.notifyLbl.TabIndex = 19;
            this.notifyLbl.Text = " ";
            // 
            // EditSensorStateControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.mainTable);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "EditSensorStateControl";
            this.Size = new System.Drawing.Size(440, 120);
            this.Load += new System.EventHandler(this.EditSensorStateControl_Load);
            this.mainTable.ResumeLayout(false);
            this.mainTable.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sensorTbx.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.minValueTbx.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.maxValueTbx.Properties)).EndInit();
            this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.TableLayoutPanel mainTable;
    private DevExpress.XtraEditors.SimpleButton _saveBtn;
    private DevExpress.XtraEditors.SimpleButton _cancelBtn;
    private DevExpress.XtraEditors.LabelControl sensorLbl;
    private DevExpress.XtraEditors.LabelControl minValueLbl;
    private DevExpress.XtraEditors.LabelControl MaxValueLbl;
    private DevExpress.XtraEditors.TextEdit sensorTbx;
    private DevExpress.XtraEditors.LabelControl errorLbl;
    private DevExpress.XtraEditors.TextEdit minValueTbx;
    private DevExpress.XtraEditors.TextEdit maxValueTbx;
    private DevExpress.XtraEditors.LabelControl notifyLbl;
  }
}
