using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraReports.Serialization;
using LocalCache;
using LocalCache.atlantaDataSetTableAdapters;
using TrackControl.Properties;

namespace TrackControl.Setting.Controls
{
    [ToolboxItem(false)]
    public partial class EditSensorStateControl : UserControl
    {
        /// <summary>
        /// ������� ��� ������� ���������.
        /// </summary>
        private StateTableAdapter _stateAdapter;

        /// <summary>
        /// ������� ����������.
        /// </summary>
        private atlantaDataSet _dataset;

        /// <summary>
        /// ���������, ������� ������������� � ������ ������.
        /// </summary>
        private atlantaDataSet.StateRow _state;

        /// <summary>
        /// ����, ����������� ������������� ������������ ��������� ���
        /// ������ ��� �����������.
        /// </summary>
        private bool _added;

        /// <summary>
        /// �������, ����������� ����� ��������� ��������������.
        /// ��� ���������� ���� ��� ����� �� ������ "������", ���� ��� ���������� ���������.
        /// </summary>
        public event EventHandler EndEdit;

        /// <summary>
        /// �����������.
        /// </summary>
        /// <param name="stateAdapter">������� ��� ������� ���������</param>
        /// <param name="dataset">������� ����������</param>
        /// <param name="state">���������, ������� ���������� ���������</param>
        /// <param name="added">����������� ��� �������������</param>
        public EditSensorStateControl(StateTableAdapter stateAdapter, atlantaDataSet dataset,
            atlantaDataSet.StateRow state, bool added)
        {
            _stateAdapter = stateAdapter;
            _dataset = dataset;
            _state = state;
            _added = added;

            InitializeComponent();
            init();
        }

        private void init()
        {
            sensorLbl.Text = Resources.TransitionDescription;
            minValueLbl.Text = Resources.LimitMin;
            MaxValueLbl.Text = Resources.LimitMax;
            _saveBtn.Text = Resources.Save;

        }

        /// <summary>
        /// ���������� ������� EndEdit.
        /// </summary>
        private void onEndEdit()
        {
            if (EndEdit != null)
                EndEdit(this, EventArgs.Empty);
        }

        /// <summary>
        /// ��������� ������������ ��������� ������.
        /// </summary>
        /// <returns>True - ���� ��������� ������ ���������</returns>
        private bool validate()
        {
            bool result = true;
            // ���������, ������� �� �������� ���������
            if (sensorTbx.Text.Trim().Length == 0 || sensorTbx.BackColor != Color.White)
            {
                markTextbox(sensorTbx);
                sensorTbx.Text = Resources.Err_MustBeFilled;
                result = false;
            }
            // ���������, �������� �� ��������� �������� �������
            bool parseOK = true;
            double min, max;
            if (!Double.TryParse(minValueTbx.Text, out min))
            {
                markTextbox(minValueTbx);
                errorLbl.Text = Resources.Err_LimitMinIsNotNumber;
                parseOK = false;
                result = false;
            }
            if (!Double.TryParse(maxValueTbx.Text, out max))
            {
                markTextbox(maxValueTbx);
                errorLbl.Text += Resources.Err_LimitMaxIsNotNumber;
                parseOK = false;
                result = false;
            }
            // ���������, ������ �� MaxValue ��� MinValue
            if (parseOK && min >= max)
            {
                markTextbox(minValueTbx);
                markTextbox(maxValueTbx);
                errorLbl.Text = Resources.Err_LimitMaxIsLessThanMin;
                result = false;
            }
            // ��������� ����������� ��������
            if (parseOK && checkIntersection(min, max))
            {
                markTextbox(minValueTbx);
                markTextbox(maxValueTbx);
                errorLbl.Text = Resources.Err_IntervalsAreCrossed;
                result = false;
            }
            if (!result) _saveBtn.Enabled = true;

            return result;
        }

        /// <summary>
        /// ������������ ���� ��� �����, �������� ����� �������
        /// �� �������������� ��������� ������.
        /// </summary>
        /// <param name="box">���� ��� �����</param>
        private void markTextbox(DevExpress.XtraEditors.TextEdit box)
        {
            box.BackColor = Color.FromArgb(255, 242, 232);
            box.ForeColor = Color.DarkRed;
        }

        /// <summary>
        /// ������� ��������� � ���� ��� �����.
        /// </summary>
        /// <param name="box">���� ��� �����</param>
        private void unmarkTextbox(DevExpress.XtraEditors.TextEdit box)
        {
            box.BackColor = Color.White;
            box.ForeColor = Color.Black;
        }

        /// <summary>
        /// ��������� ����������� ����������� �������� �������� ������� ��� ������� ���������.
        /// </summary>
        /// <param name="minValue">������ ������� ���������</param>
        /// <param name="maxValue">������� ������� ���������</param>
        /// <returns>True - ���� ��������� ������������</returns>
        private bool checkIntersection(double minValue, double maxValue)
        {
            foreach (atlantaDataSet.StateRow state in _state.sensorsRow.GetStateRows())
            {
                if (state.Id == _state.Id)
                    continue;
                if (minValue >= state.MinValue && minValue <= state.MaxValue)
                    return true;
                if (maxValue >= state.MinValue && maxValue <= state.MaxValue)
                    return true;
            }
            return false;
        }

        /// <summary>
        /// ������������ ������� �������� ��������.
        /// </summary>
        private void EditSensorStateControl_Load(object sender, EventArgs e)
        {
            sensorTbx.Text = _state.Title;
            minValueTbx.Text = _state.MinValue.ToString("N2");
            maxValueTbx.Text = _state.MaxValue.ToString("N2");
        }

        /// <summary>
        /// ������������ ���� �� ������ "������"
        /// </summary>
        private void cancelBtn_Click(object sender, EventArgs e)
        {
            if (_added)
                _state.Delete();

            onEndEdit();
        }

        /// <summary>
        /// ������������ ��������� ������� �� ������ "���������".
        /// </summary>
        private void saveBtn_MouseEnter(object sender, EventArgs e)
        {
            notifyLbl.ForeColor = Color.DarkGreen;
            notifyLbl.Text = Resources.SaveChangesData;
        }

        /// <summary>
        /// ������������ �������� ������� � ������ "���������".
        /// </summary>
        private void saveBtn_MouseLeave(object sender, EventArgs e)
        {
            notifyLbl.Text = "";
        }

        /// <summary>
        /// ������������ ��������� ������� �� ������ "������".
        /// </summary>
        private void cancelBtn_MouseEnter(object sender, EventArgs e)
        {
            notifyLbl.ForeColor = Color.DarkBlue;
            notifyLbl.Text = Resources.UndoChangesData;
        }

        /// <summary>
        /// ������������ �������� ������� � ������ "������".
        /// </summary>
        private void cancelBtn_MouseLeave(object sender, EventArgs e)
        {
            notifyLbl.Text = "";
        }

        /// <summary>
        /// ������������ ���� �� ������ "���������".
        /// </summary>
        private void saveBtn_Click(object sender, EventArgs e)
        {
            _saveBtn.Enabled = false;
            // ��������� ������������ ��������� ������
            if (!validate()) return;

            _state.Title = sensorTbx.Text;
            _state.MinValue = Double.Parse(minValueTbx.Text);
            _state.MaxValue = Double.Parse(maxValueTbx.Text);
            _stateAdapter.Update(_dataset.State);

            onEndEdit();
        }

        /// <summary>
        /// ������������ ��������� ������ ����� ��� ����� �������� ���������.
        /// </summary>
        private void sensorTbx_Enter(object sender, EventArgs e)
        {
            if (sensorTbx.BackColor != Color.White)
            {
                unmarkTextbox(sensorTbx);
                sensorTbx.Text = "";
            }
        }

        /// <summary>
        /// ������������ ��������� ������ ����� ��� ����� ������ ������� ��������� �������
        /// </summary>
        private void minValueTbx_Enter(object sender, EventArgs e)
        {
            if (minValueTbx.BackColor != Color.White)
            {
                unmarkTextbox(minValueTbx);
                errorLbl.Text = "";
            }
        }

        /// <summary>
        /// ������������ ��������� ������ ����� ��� ����� ������� ������� ��������� �������
        /// </summary>
        private void maxValueTbx_Enter(object sender, EventArgs e)
        {
            if (maxValueTbx.BackColor != Color.White)
            {
                unmarkTextbox(maxValueTbx);
                errorLbl.Text = "";
            }
        }
    }
}
