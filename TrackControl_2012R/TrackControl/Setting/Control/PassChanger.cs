using System;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using System.ComponentModel;
using DevExpress.XtraEditors;
using TrackControl.General.Services;
using TrackControl.General;
using TrackControl.Properties;

namespace TrackControl.Setting.Control
{
  [ToolboxItem(false)]
  public partial class PassChanger : XtraUserControl
  {
    private readonly Color TEXT_BOX_BACKCOLOR = Color.White;
    private readonly Color TEXT_BOX_FORECOLOR = Color.FromArgb(64, 64, 64);
    private readonly Color TEXT_BOX_ERROR_BACKCOLOR = Color.FromArgb(255, 248, 234);
    private readonly Color TEXT_BOX_ERROR_FORECOLOR = Color.Brown;

    /// <summary>
    /// �����������
    /// </summary>
    public PassChanger()
    {
      InitializeComponent();
      init();
      showWelcome();
    }

    void init()
    {
      _groupPanel.Text = Resources.PassChanging;
      mainTextLbl.Text = Resources.PassChangingText;
      _mainBtn.Text = Resources.Edit;
      oldPassLbl.Text = Resources.OldPass;
      newPassLbl.Text = Resources.NewPass;
      confirmPassLbl.Text = Resources.Confirmation;
      _saveBtn.Text = Resources.Save;
      _saveBtn.ToolTip = Resources.SaveChanges;
      _cancelBtn.ToolTip = Resources.CancelChanges;
    }

    #region --GUI Handlers --
    /// <summary>
    /// ������������ ���� �� ������ "������"
    /// </summary>
    void cancelBtn_Click(object sender, EventArgs e)
    {
      clearAllBoxes();
      showWelcome();
    }
    /// <summary>
    /// ������������ ���� �� ������ "��������"
    /// </summary>
    void saveBtn_Click(object sender, EventArgs e)
    {
      if (!validate())
        return;

      try
      {
        if (PassService.ChangePassword(oldPassBox.Text, newPassBox.Text))
        {
          notificationIcon.Image = Shared.TickLittle;
          notificationLbl.ForeColor = Color.DarkGreen;
          notificationLbl.Text = Resources.PassWasChanged;
          Application.DoEvents();

          clearAllBoxes();
          Thread.Sleep(2000);

          notificationIcon.Image = null;
          notificationLbl.Text = "";
          Application.DoEvents();
          showWelcome();
        }
        else
        {
          oldPassBox.BackColor = TEXT_BOX_ERROR_BACKCOLOR;
          oldPassBox.ForeColor = TEXT_BOX_ERROR_FORECOLOR;
          oldPassBox.UseSystemPasswordChar = false;
          oldPassBox.Text = Resources.WrongPass;
        }
      }
      catch (Exception)
      {
        showUnknownError();
      }
    }
    /// <summary>
    /// ������������ ��������� ������ ����� ��� ����� �������� ������
    /// </summary>
    void oldPassBox_Enter(object sender, EventArgs e)
    {
      oldPassBox.BackColor = TEXT_BOX_BACKCOLOR;
      oldPassBox.ForeColor = TEXT_BOX_FORECOLOR;
      oldPassBox.Text = "";
      oldPassBox.UseSystemPasswordChar = true;
    }
    /// <summary>
    /// ������������ ��������� ������ ����� ��� ����� ������ ������
    /// </summary>
    void newPassBox_Enter(object sender, EventArgs e)
    {
      if (newPassBox.UseSystemPasswordChar && confirmPassBox.UseSystemPasswordChar)
        return;

      clearNewPassAndConfirmBoxes();
    }
    /// <summary>
    /// ������������ ��������� ������ ����� ��� ������������� ������ ������
    /// </summary>
    void confirmPassBox_Enter(object sender, EventArgs e)
    {
      if (newPassBox.UseSystemPasswordChar && confirmPassBox.UseSystemPasswordChar)
        return;

      clearNewPassAndConfirmBoxes();
    }
    /// <summary>
    /// ������������ ���� �� ������� ������, ���������� ������ ���������.
    /// </summary>
    void mainButton_Click(object sender, EventArgs e)
    {
      showMain();
    }
    #endregion

    /// <summary>
    /// ������� ���� ��� ����� � ������������� ������ ������
    /// </summary>
    void clearNewPassAndConfirmBoxes()
    {
      newPassBox.Text = "";
      newPassBox.ForeColor = TEXT_BOX_FORECOLOR;
      newPassBox.BackColor = TEXT_BOX_BACKCOLOR;
      newPassBox.UseSystemPasswordChar = true;

      confirmPassBox.Text = "";
      confirmPassBox.ForeColor = TEXT_BOX_FORECOLOR;
      confirmPassBox.BackColor = TEXT_BOX_BACKCOLOR;
      confirmPassBox.UseSystemPasswordChar = true;
    }
    /// <summary>
    /// ������� ��� ����
    /// </summary>
    void clearAllBoxes()
    {
      oldPassBox.Text = "";
      oldPassBox.BackColor = TEXT_BOX_BACKCOLOR;
      oldPassBox.ForeColor = TEXT_BOX_FORECOLOR;
      oldPassBox.UseSystemPasswordChar = true;

      clearNewPassAndConfirmBoxes();
    }
    /// <summary>
    /// ��������� � ����������� ������
    /// </summary>
    void showUnknownError()
    {
      notificationIcon.Image = Shared.WarningLittle;
      notificationLbl.ForeColor = Color.DarkRed;
      notificationLbl.Text = Resources.UnknownError;
      Application.DoEvents();

      Thread.Sleep(3000);

      notificationIcon.Image = null;
      notificationLbl.Text = "";
      Application.DoEvents();
    }
    /// <summary>
    /// ��������� ���������� ������ ������
    /// </summary>
    /// <returns>True - ���� ������ �������� � �����������</returns>
    bool validate()
    {
      if (oldPassBox.BackColor != TEXT_BOX_BACKCOLOR || newPassBox.BackColor != TEXT_BOX_BACKCOLOR || confirmPassBox.BackColor != TEXT_BOX_BACKCOLOR)
      {
        return false;
      }

      if (newPassBox.Text.Length < 3)
      {
        newPassBox.BackColor = TEXT_BOX_ERROR_BACKCOLOR;
        newPassBox.ForeColor = TEXT_BOX_ERROR_FORECOLOR;
        newPassBox.Text = Resources.PassIsTooShort;
        newPassBox.UseSystemPasswordChar = false;

        return false;
      }

      if (newPassBox.Text != confirmPassBox.Text)
      {
        newPassBox.BackColor = TEXT_BOX_ERROR_BACKCOLOR;
        newPassBox.ForeColor = TEXT_BOX_ERROR_FORECOLOR;
        newPassBox.Text = Resources.PassAndConfirm;
        newPassBox.UseSystemPasswordChar = false;

        confirmPassBox.BackColor = TEXT_BOX_ERROR_BACKCOLOR;
        confirmPassBox.ForeColor = TEXT_BOX_ERROR_FORECOLOR;
        confirmPassBox.Text = Resources.NotTheSame;
        confirmPassBox.UseSystemPasswordChar = false;

        return false;
      }

      return true;
    }
    /// <summary>
    /// ���������� ������ � ������� �������.
    /// </summary>
    void showWelcome()
    {
      mainTable.Visible = false;
      welcomeTable.Dock = DockStyle.Fill;
      welcomeTable.Visible = true;
    }
    /// <summary>
    /// ���������� ������ ��� ��������� ������.
    /// </summary>
    void showMain()
    {
      welcomeTable.Visible = false;
      mainTable.Dock = DockStyle.Fill;
      mainTable.Visible = true;
    }
  }
}
