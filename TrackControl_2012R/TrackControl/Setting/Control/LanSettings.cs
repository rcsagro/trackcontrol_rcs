using System;
using System.Net;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using TrackControl.General.Services;
using TrackControl.Properties;

namespace TrackControl.Setting.Control
{
    public partial class LanSettings : XtraUserControl
    {
        public LanSettings()
        {
            InitializeComponent();
            init();
        }

        void init()
        {
            _groupPanel.Text = Resources.LanSetting;
            nopeProxyRadio.Text = Resources.DontUseProxy;
            defaultProxyRadio.Text = Resources.ProxyAutoDetect;
            customProxyRadio.Text = Resources.ProxyHandSetup;
            portLbl.Text = Resources.Port;
            proxyLbl.Text = Resources.HttpProxy;
            descriptionLbl.Text = Resources.LanSettingMessage;
            _editBtn.Text = Resources.Setup;
            _saveBtn.Text = Resources.Save;
            _saveBtn.ToolTip = Resources.SaveChanges;
            _cancelBtn.ToolTip = Resources.CancelChanges;
        }

        #region -- Private Methods --

        /// <summary>
        /// ���������� ��������� ������.
        /// </summary>
        private void showWelcome()
        {
            settingTable.Visible = false;
            welcomeTable.Visible = true;
            welcomeTable.Dock = DockStyle.Fill;
            proxyBox.Text = "";
            portBox.Text = "";
        }

        /// <summary>
        /// ���������� ������ ��������.
        /// </summary>
        private void showSettings()
        {
            proxyTable.Enabled = false;
            tableLogin.Enabled = false;
            ProxySettings settings = ProxyService.FetchSettings();
            switch (settings.Mode)
            {
                case ProxyMode.Custom:
                {
                    customProxyRadio.Checked = true;
                    proxyBox.Text = settings.Proxy.Address.DnsSafeHost;
                    portBox.Text = settings.Proxy.Address.Port.ToString();
                    proxyTable.Enabled = true;
                    txLogin.Text = settings.Login;
                    txPassword.Text = settings.Password;
                    tableLogin.Enabled = true;
                    break;
                }
                case ProxyMode.Default:
                {
                    defaultProxyRadio.Checked = true;
                    break;
                }
                case ProxyMode.Nope:
                default:
                {
                    nopeProxyRadio.Checked = true;
                    break;
                }
            }

            welcomeTable.Visible = false;
            settingTable.Visible = true;
            settingTable.Dock = DockStyle.Fill;
        }

        #endregion

        #region -- GUI Handlers --

        /// <summary>
        /// ������������ ��� �������� ��������.
        /// </summary>
        private void LanSettings_Load(object sender, EventArgs e)
        {
            showWelcome();
        }

        /// <summary>
        /// ������������ ���� �� ������ "���������".
        /// </summary>
        private void editBtn_Click(object sender, EventArgs e)
        {
            showSettings();
        }

        /// <summary>
        /// ������������ ���� �� ������ "������".
        /// </summary>
        private void cancelBtn_Click(object sender, EventArgs e)
        {
            showWelcome();
        }

        /// <summary>
        /// ������������ ��������� ���������� �����-������ � ������.
        /// </summary>
        private void customProxyRadio_CheckedChanged(object sender, EventArgs e)
        {
            if (customProxyRadio.Checked)
            {
                proxyTable.Enabled = true;
                tableLogin.Enabled = true;
            }
            else
            {
                proxyTable.Enabled = false;
                tableLogin.Enabled = false;
            }
        }

        /// <summary>
        /// ����������� ����� �� ���� ����� �����.
        /// </summary>
        private void portBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar >= '0' && e.KeyChar <= '9') return; // ��������� ������ �����
            if (e.KeyChar == '\b') return; // ��� �������� 'Backspace'
            e.Handled = true;
        }

        /// <summary>
        /// ������������ ��������� ������ ����� ��� ����� ����� ������.
        /// </summary>
        private void proxyBox_Enter(object sender, EventArgs e)
        {
            if (proxyBox.BackColor == Color.LightSalmon)
            {
                proxyBox.BackColor = Color.White;
                proxyBox.Text = "";
            }
        }

        /// <summary>
        /// ������������ ���� �� ������ "���������".
        /// </summary>
        private void saveBtn_Click(object sender, EventArgs e)
        {
            ProxySettings settings = new ProxySettings(ProxyMode.Nope, null);
            WebProxy proxy = null;
            if (defaultProxyRadio.Checked)
            {
                settings.Mode = ProxyMode.Default;
                proxy = (WebProxy) GlobalProxySelection.Select;
                settings.Password = "";
                settings.Login = "";
            }
            else if (customProxyRadio.Checked)
            {
                settings.Mode = ProxyMode.Custom;
                if (proxyBox.Text.Trim().Length == 0 || proxyBox.BackColor == Color.LightSalmon)
                {
                    proxyBox.Text = Resources.Err_MustBeFilled;
                    proxyBox.BackColor = Color.LightSalmon;
                    return;
                }

                if (portBox.Text.Length > 0)
                {
                    proxy = new WebProxy(proxyBox.Text, Int32.Parse(portBox.Text));
                }
                else
                {
                    proxy = new WebProxy(proxyBox.Text);
                }

                settings.Password = txPassword.Text;
                settings.Login = txLogin.Text;
            }

            settings.Proxy = proxy;
            showWelcome();
            ProxyService.SaveSettings(settings);
        }

        #endregion
    }
}
