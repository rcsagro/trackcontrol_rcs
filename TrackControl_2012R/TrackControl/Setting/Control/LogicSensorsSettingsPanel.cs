using System;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using LocalCache;
using MySql.Data.MySqlClient;
using TrackControl.General.DatabaseDriver;
using TrackControl.Properties;

namespace TrackControl.Setting.Controls
{
  [ToolboxItem(false)]
  public partial class LogicSensorsSettingsPanel : UserControl
  {
    #region Fields
    /// <summary>
    /// ������� �������������� �������� ����������� �������
    /// </summary>
    private LogicSensorEditPanel editControl;
    /// <summary>
    /// ������� ��� ��������� �������, ����������� ���������� ��������.
    /// </summary>
    private LogicSensorEventsEditPanel eventEditControl;
    /// <summary>
    /// ������� ��� ���������� ��������� � �� �������� ���������� ��������.
    /// </summary>
    private LocalCache.atlantaDataSetTableAdapters.SensorsTableAdapter _sensorAdapter =
      new LocalCache.atlantaDataSetTableAdapters.SensorsTableAdapter();
    /// <summary>
    /// ������� ��� ���������� ��������� � �� �������� ���������, �������������
    /// �� ���������� �������.
    /// </summary>
    private LocalCache.atlantaDataSetTableAdapters.StateTableAdapter _stateAdapter =
      new LocalCache.atlantaDataSetTableAdapters.StateTableAdapter();
    /// <summary>
    /// ������� ��� ���������� ��������� � �� �������� �������, �����������
    /// ��� ��������� ��������� �������.
    /// </summary>
    private LocalCache.atlantaDataSetTableAdapters.TransitionTableAdapter _transitionAdapter =
      new LocalCache.atlantaDataSetTableAdapters.TransitionTableAdapter();
    /// <summary>
    /// ������� ��� ���������� ��������� � ������� relationalgorithms
    /// </summary>
    private LocalCache.atlantaDataSetTableAdapters.RelationalgorithmsTableAdapter _relAlgAdapter =
      new LocalCache.atlantaDataSetTableAdapters.RelationalgorithmsTableAdapter();
    /// <summary>
    /// ��������� ����������� �������, ������� ������ ����������� � ���������.
    /// � ������ �������������� ������������ ��������, ������ ���� ���� NULL.
    /// ���� �������, ����� � ������ ������ ���������� �������� � ��, ������� �� �� ��������.
    /// </summary>
    private atlantaDataSet.sensorsRow sensorWhenAdding;

      /// <summary>
      /// ������ ����������� � ���� ������ MySQL.
      /// </summary>
      private object _connection;

    #endregion

    /// <summary>
    /// �����������
    /// </summary>
    public LogicSensorsSettingsPanel()
    {
      InitializeComponent();
      init();
      if (DesignMode)
        return;
    }

    void init()
    {
      ColumnNumber.HeaderText = Resources.CarNumber;
      ColumnMarka.HeaderText = Resources.CarMark;
      ColumnModel.HeaderText = Resources.CarModel;
      label1.Text = Resources.Vehicles;
      nameDataGridViewTextBoxColumn1.HeaderText = Resources.LogicSensorName;
      nameDataGridViewTextBoxColumn1.ToolTipText = Resources.LogicSensorName;
      startBitDataGridViewTextBoxColumn.HeaderText = Resources.StartBit;
      startBitDataGridViewTextBoxColumn.ToolTipText = Resources.StartBit;
      SpecSettingColumn.HeaderText = Resources.Assignment;
      SpecSettingColumn.ToolTipText = Resources.SpecialAssignment;
      sensorHeadLbl.Text = Resources.LogicSensors;
      removeBtn.Text = Resources.Delete;
      addBtn.Text = Resources.Add;
      editBtn.Text = Resources.Edit;
      stateFalseLbl.Text = Resources.StateZero;
      stateTrueLbl.Text = Resources.StateOne;
      _editSensorBtn.Text = Resources.Edit;
      stateHeadLbl.Text = Resources.States;
      _eventEditBtn.Text = Resources.Setup;
      label4.Text = Resources.Transitions;
    }

    /// <summary>
    /// ������������ ������� �������� ��������
    /// </summary>
    void this_onLoad(object sender, EventArgs e)
    {
      if (DesignMode)
        return;
      string CONNECTION_STRING = Crypto.GetDecryptConnectionString(System.Configuration.ConfigurationManager.ConnectionStrings["CS"].ConnectionString);

        DriverDb db = new DriverDb();
        _connection = db.NewSqlConnection(CONNECTION_STRING); //new MySqlConnection(CONNECTION_STRING);

      atlantaDataSet = AppModel.Instance.DataSet;
      mobitelsBindingSource.DataSource = atlantaDataSet;
      vehicleBindingSource.DataSource = atlantaDataSet.vehicle;

      _sensorAdapter.Connection = _connection;
      _stateAdapter.Connection = _connection;
      _transitionAdapter.Connection = _connection;
      _relAlgAdapter.Connection = _connection;

      editControl = new LogicSensorEditPanel(_sensorAdapter, _stateAdapter, _relAlgAdapter, atlantaDataSet);
      editControl.Dock = DockStyle.Fill;
      editControl.Visible = false;
      editControl.EndEdit += new EventHandler<EndEditEventArgs>(onEndEdit);
      editPanel.Controls.Add(editControl);

      eventEditControl = new LogicSensorEventsEditPanel(atlantaDataSet, _transitionAdapter);
      eventEditControl.Dock = DockStyle.Fill;
      eventEditControl.Visible = false;
      eventEditControl.EndEditEvent += new EventHandler<EndEditEventArgs>(onEndEdit);
      editPanel.Controls.Add(eventEditControl);
    }

    /// <summary>
    /// ������������ ������� ��������� �������������� ��������
    /// ����������� �������
    void onEndEdit(object sender, EndEditEventArgs e)
    {
      if (e.Canceled && sensorWhenAdding != null)
      {
        sensorWhenAdding.Delete();
      }
      sensorWhenAdding = null;

      if (!e.Canceled)
      {
        editBtn.Visible = true;
        removeBtn.Visible = true;
      }

      editControl.Visible = false;
      eventEditControl.Visible = false;
      sensorInfoPanel.Visible = true;

      sensorsGrid.Enabled = true;
      sensorsGrid.DefaultCellStyle.BackColor = Color.WhiteSmoke;
      sensorsGrid.DefaultCellStyle.ForeColor = Color.Black;

      mobitelsGrid.Enabled = true;
      mobitelsGrid.DefaultCellStyle.BackColor = Color.WhiteSmoke;
      mobitelsGrid.DefaultCellStyle.ForeColor = Color.Black;

      addBtn.Enabled = true;
      removeBtn.Enabled = true;
      editBtn.Enabled = true;
    }

    /// <summary>
    /// ����������� ��������� ����������� �������
    /// </summary>
    void editSensor(atlantaDataSet.sensorsRow sensor)
    {
      mobitelsGrid.Enabled = false;
      mobitelsGrid.DefaultCellStyle.BackColor = Color.Gainsboro;
      mobitelsGrid.DefaultCellStyle.ForeColor = Color.DimGray;

      sensorsGrid.Enabled = false;
      sensorsGrid.DefaultCellStyle.BackColor = Color.Gainsboro;
      sensorsGrid.DefaultCellStyle.ForeColor = Color.DimGray;

      sensorInfoPanel.Visible = false;
      editControl.Visible = true;
      addBtn.Enabled = false;
      removeBtn.Enabled = false;
      editBtn.Enabled = false;

      editControl.EditSensor(sensor);
    }

    /// <summary>
    /// ����������� ��������� ������� ��� ����������� �������.
    /// </summary>
    /// <param name="sensor">���������� ������, ��� �������� ������������� �������.</param>
    void editEventForSensor(atlantaDataSet.sensorsRow sensor)
    {
      mobitelsGrid.Enabled = false;
      mobitelsGrid.DefaultCellStyle.BackColor = Color.Gainsboro;
      mobitelsGrid.DefaultCellStyle.ForeColor = Color.DimGray;

      sensorsGrid.Enabled = false;
      sensorsGrid.DefaultCellStyle.BackColor = Color.Gainsboro;
      sensorsGrid.DefaultCellStyle.ForeColor = Color.DimGray;

      sensorInfoPanel.Visible = false;
      eventEditControl.Visible = true;
      addBtn.Enabled = false;
      removeBtn.Enabled = false;
      editBtn.Enabled = false;

      eventEditControl.EditEventsForSensor(sensor);
    }


    atlantaDataSet.sensorsRow getCurrentSensor()
    {
      if (sensorsGrid.CurrentRow == null)
        return null;

      return (atlantaDataSet.sensorsRow)(
          (DataRowView)mobitelsSensorsBindingSource.List[sensorsGrid.CurrentRow.Index]).Row;
    }

    #region GUI Event Handlers
    /// <summary>
    /// ������������ ������� �������� ����� ����� �� ������ ����� ���������� ��������.
    /// ��������� ������ �������������� �������� ���������� ����������� �������.
    /// </summary>
    void sensorsGrid_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
    {
      atlantaDataSet.sensorsRow sensor = (atlantaDataSet.sensorsRow)(
          (DataRowView)mobitelsSensorsBindingSource.List[sensorsGrid.CurrentRow.Index]).Row;

      editSensor(sensor);
    }

    /// <summary>
    /// ����������� ������� �������� ���������� �������� � ���������� ���������. ��� ����������
    /// �������� ������ ������ "�������".
    /// </summary>
    void mobitelsGrid_SelectionChanged(object sender, EventArgs e)
    {
        int mob_id = -1;
        if (vehicleBindingSource.Current != null)
        {
            mob_id = ((atlantaDataSet.vehicleRow)(((DataRowView)vehicleBindingSource.Current).Row)).Mobitel_id;
            int itemFound = mobitelsBindingSource.Find("Mobitel_ID", mob_id);
            mobitelsBindingSource.Position = itemFound;
        }

        if (sensorsGrid.Rows.Count > 0)
        {
            removeBtn.Visible = true;
            editBtn.Visible = true;
        }
        else
        {
            removeBtn.Visible = false;
            editBtn.Visible = false;
        }
    }

    /// <summary>
    /// ������������ ������� �� ������ "�������". ������� ��������� ���������� �����������
    /// ������� � �������� ���������.
    /// </summary>
    void removeBtn_Click(object sender, EventArgs e)
    {
      atlantaDataSet.sensorsRow sensor = getCurrentSensor();
      if (sensor == null)
        return;

      string message = String.Format("{0}: \"{1}\"?", Resources.LogicSensorDeletingConfirm, sensor.Name);
      DialogResult result = MessageBox.Show(
          message,
          Resources.SensorDeleting,
          MessageBoxButtons.YesNo,
          MessageBoxIcon.Question,
          MessageBoxDefaultButton.Button2);
      if (DialogResult.Yes == result)
      {
        foreach (atlantaDataSet.TransitionRow transition in sensor.GetTransitionRows())
        {
          transition.Delete();
        }

        foreach (atlantaDataSet.StateRow state in sensor.GetStateRows())
        {
          state.Delete();
        }

        foreach (atlantaDataSet.relationalgorithmsRow relAlg in sensor.GetrelationalgorithmsRows())
        {
          relAlg.Delete();
        }

        sensor.Delete();

        _transitionAdapter.Update(atlantaDataSet.Transition);
        _stateAdapter.Update(atlantaDataSet.State);
        _relAlgAdapter.Update(atlantaDataSet.relationalgorithms);
        _sensorAdapter.Update(atlantaDataSet.sensors);
      }

      if (sensorsGrid.Rows.Count == 0)
      {
        removeBtn.Visible = false;
        editBtn.Visible = false;
      }
    }

    /// <summary>
    /// ������������ ������� �� ������ "��������". ��������� ��������� ��� ����������� �������
    /// �������� ���������.
    /// </summary>
    void addBtn_Click(object sender, EventArgs e)
    {
      atlantaDataSet.sensorsRow sensor = (atlantaDataSet.sensorsRow)atlantaDataSet.sensors.NewRow();
      sensor.Name = "";
      sensor.Description = "";
      sensor.StartBit = 0;
      sensor.Length = 1;
      sensor.NameUnit = "";
      sensor.K = 1.0;
      sensor.Check = false;
      sensor.mobitel_id = ((atlantaDataSet.mobitelsRow)((DataRowView)mobitelsBindingSource.Current).Row).Mobitel_ID;
      sensor.MobitelName = "";
      sensor.MobitelDescr = "";
      atlantaDataSet.sensors.AddsensorsRow(sensor);
      sensorWhenAdding = sensor;

      editSensor(sensor);
    }

    /// <summary>
    /// ������������ ������� �� ������ "��������".
    /// </summary>
    void editBtn_Click(object sender, EventArgs e)
    {
      atlantaDataSet.sensorsRow sensor = getCurrentSensor();
      if (sensor != null)
        editSensor(sensor);
    }
    #endregion

    void eventEditBtn_Click(object sender, EventArgs e)
    {
      atlantaDataSet.sensorsRow sensor = getCurrentSensor();
      if (sensor != null)
        editEventForSensor(sensor);
    }

    /// <summary>
    /// ���������� ���������� �����.
    /// </summary>
    void sensorsGrid_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
    {
      // ����� ���������� � ������� "����������"
      if ((sensorsGrid.Columns["SpecSettingColumn"].Index == e.ColumnIndex) &&
        (e.RowIndex >= 0))
      {
        int? algorithmID = GetAlgorithmId((int)sensorsGrid["id", e.RowIndex].Value);
        sensorsGrid[e.ColumnIndex, e.RowIndex].Value = algorithmID.HasValue ?
          GetAlgorithmName(algorithmID.Value) : String.Empty;
      }
    }

    /// <summary>
    /// ����� �������������� ���������
    /// </summary>
    /// <param name="sensorId">Id �������</param>
    /// <returns>AlgorithmId</returns>
    int? GetAlgorithmId(int sensorId)
    {
      foreach (atlantaDataSet.relationalgorithmsRow row in atlantaDataSet.relationalgorithms)
      {
        if (row.SensorID == sensorId)
        {
          return row.AlgorithmID;
        }
      }
      return null;
    }

    /// <summary>
    /// ����� ������������ ��������� �� ��� ��������������
    /// </summary>
    /// <param name="algorithmID">������������� ���������</param>
    /// <returns>������������ ���������</returns>
    string GetAlgorithmName(int algorithmID)
    {
      foreach (atlantaDataSet.sensoralgorithmsRow row in atlantaDataSet.sensoralgorithms)
      {
        if (row.AlgorithmID == algorithmID)
        {
          return row.Name;
        }
      }
      return String.Empty;
    }












  }
}
