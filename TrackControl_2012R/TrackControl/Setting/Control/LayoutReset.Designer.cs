namespace TrackControl.Setting.Control
{
  partial class LayoutReset
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this._groupPanel = new DevExpress.XtraEditors.GroupControl();
      this._table = new System.Windows.Forms.TableLayoutPanel();
      this._onlineLbl = new DevExpress.XtraEditors.LabelControl();
      this._onlineBtn = new DevExpress.XtraEditors.SimpleButton();
      this._reportsLbl = new DevExpress.XtraEditors.LabelControl();
      this._reportsBtn = new DevExpress.XtraEditors.SimpleButton();
      ((System.ComponentModel.ISupportInitialize)(this._groupPanel)).BeginInit();
      this._groupPanel.SuspendLayout();
      this._table.SuspendLayout();
      this.SuspendLayout();
      // 
      // _groupPanel
      // 
      this._groupPanel.Controls.Add(this._table);
      this._groupPanel.Dock = System.Windows.Forms.DockStyle.Fill;
      this._groupPanel.Location = new System.Drawing.Point(0, 0);
      this._groupPanel.Margin = new System.Windows.Forms.Padding(0);
      this._groupPanel.Name = "_groupPanel";
      this._groupPanel.Padding = new System.Windows.Forms.Padding(10);
      this._groupPanel.Size = new System.Drawing.Size(302, 170);
      this._groupPanel.TabIndex = 0;
      this._groupPanel.Text = "������� �� ���������";
      // 
      // _table
      // 
      this._table.BackColor = System.Drawing.Color.Transparent;
      this._table.ColumnCount = 2;
      this._table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this._table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 72F));
      this._table.Controls.Add(this._onlineLbl, 0, 1);
      this._table.Controls.Add(this._onlineBtn, 1, 1);
      this._table.Controls.Add(this._reportsLbl, 0, 0);
      this._table.Controls.Add(this._reportsBtn, 1, 0);
      this._table.Dock = System.Windows.Forms.DockStyle.Fill;
      this._table.Location = new System.Drawing.Point(12, 32);
      this._table.Margin = new System.Windows.Forms.Padding(0);
      this._table.Name = "_table";
      this._table.RowCount = 4;
      this._table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
      this._table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
      this._table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
      this._table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this._table.Size = new System.Drawing.Size(278, 126);
      this._table.TabIndex = 0;
      // 
      // _onlineLbl
      // 
      this._onlineLbl.Appearance.Image = TrackControl.General.Shared.Eye;
      this._onlineLbl.Appearance.Options.UseImage = true;
      this._onlineLbl.Appearance.Options.UseTextOptions = true;
      this._onlineLbl.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
      this._onlineLbl.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
      this._onlineLbl.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
      this._onlineLbl.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
      this._onlineLbl.Dock = System.Windows.Forms.DockStyle.Fill;
      this._onlineLbl.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
      this._onlineLbl.LineLocation = DevExpress.XtraEditors.LineLocation.Center;
      this._onlineLbl.Location = new System.Drawing.Point(0, 25);
      this._onlineLbl.Margin = new System.Windows.Forms.Padding(0);
      this._onlineLbl.Name = "_onlineLbl";
      this._onlineLbl.Size = new System.Drawing.Size(206, 25);
      this._onlineLbl.TabIndex = 0;
      this._onlineLbl.Text = "  ����� \"��������\"";
      // 
      // _onlineBtn
      // 
      this._onlineBtn.Dock = System.Windows.Forms.DockStyle.Fill;
      this._onlineBtn.Location = new System.Drawing.Point(209, 28);
      this._onlineBtn.Name = "_onlineBtn";
      this._onlineBtn.Size = new System.Drawing.Size(66, 19);
      this._onlineBtn.TabIndex = 1;
      this._onlineBtn.Text = "��������";
      this._onlineBtn.Click += new System.EventHandler(this._onlineBtn_Click);
      // 
      // _reportsLbl
      // 
      this._reportsLbl.Appearance.Image = TrackControl.General.Shared.Table;
      this._reportsLbl.Appearance.Options.UseImage = true;
      this._reportsLbl.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
      this._reportsLbl.Dock = System.Windows.Forms.DockStyle.Fill;
      this._reportsLbl.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
      this._reportsLbl.Location = new System.Drawing.Point(3, 3);
      this._reportsLbl.Name = "_reportsLbl";
      this._reportsLbl.Size = new System.Drawing.Size(200, 19);
      this._reportsLbl.TabIndex = 4;
      this._reportsLbl.Text = " ����� \"������\"";
      // 
      // _reportsBtn
      // 
      this._reportsBtn.Dock = System.Windows.Forms.DockStyle.Fill;
      this._reportsBtn.Location = new System.Drawing.Point(209, 3);
      this._reportsBtn.Name = "_reportsBtn";
      this._reportsBtn.Size = new System.Drawing.Size(66, 19);
      this._reportsBtn.TabIndex = 5;
      this._reportsBtn.Text = "��������";
      this._reportsBtn.Click += new System.EventHandler(this._reportsBtn_Click);
      // 
      // LayoutReset
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this._groupPanel);
      this.Name = "LayoutReset";
      this.Size = new System.Drawing.Size(302, 170);
      ((System.ComponentModel.ISupportInitialize)(this._groupPanel)).EndInit();
      this._groupPanel.ResumeLayout(false);
      this._table.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private DevExpress.XtraEditors.GroupControl _groupPanel;
    private System.Windows.Forms.TableLayoutPanel _table;
    private DevExpress.XtraEditors.LabelControl _onlineLbl;
    private DevExpress.XtraEditors.SimpleButton _onlineBtn;
    private DevExpress.XtraEditors.LabelControl _reportsLbl;
    private DevExpress.XtraEditors.SimpleButton _reportsBtn;
  }
}
