namespace TrackControl.Setting.Controls
{
    partial class LogicSensorEditPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LogicSensorEditPanel));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.stateFalseBox = new System.Windows.Forms.TextBox();
            this.stateTrueBox = new System.Windows.Forms.TextBox();
            this.nameBox = new System.Windows.Forms.TextBox();
            this.specSettingStateLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.specSettingStateInnerLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.specSettingStateLbl = new DevExpress.XtraEditors.LabelControl();
            this.specSettingState = new DevExpress.XtraEditors.RadioGroup();
            this.specSettingBox = new DevExpress.XtraEditors.LookUpEdit();
            this.buttonsLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this._cancelBtn = new DevExpress.XtraEditors.SimpleButton();
            this._saveBtn = new DevExpress.XtraEditors.SimpleButton();
            this.nameLbl = new DevExpress.XtraEditors.LabelControl();
            this.stateFalseLbl = new DevExpress.XtraEditors.LabelControl();
            this.stateTrueLbl = new DevExpress.XtraEditors.LabelControl();
            this.specSettingLbl = new DevExpress.XtraEditors.LabelControl();
            this.startBitLbl = new DevExpress.XtraEditors.LabelControl();
            this.specSettingCheckBox = new DevExpress.XtraEditors.CheckEdit();
            this.startBitBox = new DevExpress.XtraEditors.SpinEdit();
            this.errorStatusLbl = new DevExpress.XtraEditors.LabelControl();
            this.swapBtn = new DevExpress.XtraEditors.SimpleButton();
            this.labelInfo = new DevExpress.XtraEditors.LabelControl();
            this.labelMsg = new DevExpress.XtraEditors.LabelControl();
            this.tableLayoutPanel1.SuspendLayout();
            this.specSettingStateLayoutPanel.SuspendLayout();
            this.specSettingStateInnerLayoutPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.specSettingState.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.specSettingBox.Properties)).BeginInit();
            this.buttonsLayoutPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.specSettingCheckBox.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.startBitBox.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tableLayoutPanel1.ColumnCount = 9;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 11F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 121F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 106F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 113F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.stateFalseBox, 2, 5);
            this.tableLayoutPanel1.Controls.Add(this.stateTrueBox, 2, 7);
            this.tableLayoutPanel1.Controls.Add(this.nameBox, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.specSettingStateLayoutPanel, 3, 9);
            this.tableLayoutPanel1.Controls.Add(this.buttonsLayoutPanel, 2, 14);
            this.tableLayoutPanel1.Controls.Add(this.nameLbl, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.stateFalseLbl, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.stateTrueLbl, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.specSettingLbl, 1, 9);
            this.tableLayoutPanel1.Controls.Add(this.startBitLbl, 3, 3);
            this.tableLayoutPanel1.Controls.Add(this.specSettingCheckBox, 2, 9);
            this.tableLayoutPanel1.Controls.Add(this.startBitBox, 4, 3);
            this.tableLayoutPanel1.Controls.Add(this.errorStatusLbl, 6, 1);
            this.tableLayoutPanel1.Controls.Add(this.swapBtn, 6, 5);
            this.tableLayoutPanel1.Controls.Add(this.labelInfo, 1, 14);
            this.tableLayoutPanel1.Controls.Add(this.labelMsg, 0, 14);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tableLayoutPanel1.ForeColor = System.Drawing.Color.Black;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 16;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 5F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 5F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 5F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 5F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 5F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 43F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 8F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(541, 318);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // stateFalseBox
            // 
            this.stateFalseBox.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel1.SetColumnSpan(this.stateFalseBox, 4);
            this.stateFalseBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.stateFalseBox.ForeColor = System.Drawing.Color.Black;
            this.stateFalseBox.Location = new System.Drawing.Point(132, 71);
            this.stateFalseBox.Margin = new System.Windows.Forms.Padding(0);
            this.stateFalseBox.MaxLength = 100;
            this.stateFalseBox.Name = "stateFalseBox";
            this.stateFalseBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.stateFalseBox.Size = new System.Drawing.Size(271, 21);
            this.stateFalseBox.TabIndex = 7;
            // 
            // stateTrueBox
            // 
            this.stateTrueBox.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel1.SetColumnSpan(this.stateTrueBox, 4);
            this.stateTrueBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.stateTrueBox.Location = new System.Drawing.Point(132, 104);
            this.stateTrueBox.Margin = new System.Windows.Forms.Padding(0);
            this.stateTrueBox.MaxLength = 100;
            this.stateTrueBox.Name = "stateTrueBox";
            this.stateTrueBox.Size = new System.Drawing.Size(271, 21);
            this.stateTrueBox.TabIndex = 9;
            // 
            // nameBox
            // 
            this.nameBox.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel1.SetColumnSpan(this.nameBox, 4);
            this.nameBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nameBox.ForeColor = System.Drawing.Color.Black;
            this.nameBox.Location = new System.Drawing.Point(132, 5);
            this.nameBox.Margin = new System.Windows.Forms.Padding(0);
            this.nameBox.MaxLength = 44;
            this.nameBox.Name = "nameBox";
            this.nameBox.Size = new System.Drawing.Size(271, 21);
            this.nameBox.TabIndex = 2;
            this.nameBox.Enter += new System.EventHandler(this.nameBox_Enter);
            // 
            // specSettingStateLayoutPanel
            // 
            this.specSettingStateLayoutPanel.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.InsetDouble;
            this.specSettingStateLayoutPanel.ColumnCount = 1;
            this.tableLayoutPanel1.SetColumnSpan(this.specSettingStateLayoutPanel, 3);
            this.specSettingStateLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 443F));
            this.specSettingStateLayoutPanel.Controls.Add(this.specSettingStateInnerLayoutPanel, 0, 1);
            this.specSettingStateLayoutPanel.Controls.Add(this.specSettingBox, 0, 0);
            this.specSettingStateLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.specSettingStateLayoutPanel.Location = new System.Drawing.Point(155, 137);
            this.specSettingStateLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
            this.specSettingStateLayoutPanel.Name = "specSettingStateLayoutPanel";
            this.specSettingStateLayoutPanel.RowCount = 2;
            this.tableLayoutPanel1.SetRowSpan(this.specSettingStateLayoutPanel, 4);
            this.specSettingStateLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.specSettingStateLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.specSettingStateLayoutPanel.Size = new System.Drawing.Size(248, 132);
            this.specSettingStateLayoutPanel.TabIndex = 17;
            this.specSettingStateLayoutPanel.Visible = false;
            // 
            // specSettingStateInnerLayoutPanel
            // 
            this.specSettingStateInnerLayoutPanel.ColumnCount = 1;
            this.specSettingStateInnerLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.specSettingStateInnerLayoutPanel.Controls.Add(this.specSettingStateLbl, 0, 1);
            this.specSettingStateInnerLayoutPanel.Controls.Add(this.specSettingState, 0, 2);
            this.specSettingStateInnerLayoutPanel.Location = new System.Drawing.Point(6, 37);
            this.specSettingStateInnerLayoutPanel.Name = "specSettingStateInnerLayoutPanel";
            this.specSettingStateInnerLayoutPanel.RowCount = 4;
            this.specSettingStateInnerLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.specSettingStateInnerLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.specSettingStateInnerLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 58F));
            this.specSettingStateInnerLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 8F));
            this.specSettingStateInnerLayoutPanel.Size = new System.Drawing.Size(234, 89);
            this.specSettingStateInnerLayoutPanel.TabIndex = 14;
            this.specSettingStateInnerLayoutPanel.Visible = false;
            // 
            // specSettingStateLbl
            // 
            this.specSettingStateLbl.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.specSettingStateLbl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.specSettingStateLbl.Location = new System.Drawing.Point(3, 3);
            this.specSettingStateLbl.Name = "specSettingStateLbl";
            this.specSettingStateLbl.Size = new System.Drawing.Size(220, 15);
            this.specSettingStateLbl.TabIndex = 3;
            this.specSettingStateLbl.Text = "�������� ��������� (����� �������)";
            this.specSettingStateLbl.Visible = false;
            // 
            // specSettingState
            // 
            this.specSettingState.Dock = System.Windows.Forms.DockStyle.Fill;
            this.specSettingState.Location = new System.Drawing.Point(3, 26);
            this.specSettingState.Name = "specSettingState";
            this.specSettingState.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.specSettingState.Properties.Appearance.Options.UseFont = true;
            this.specSettingState.Properties.Columns = 1;
            this.specSettingState.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "��������� \"0\""),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "��������� \"1\"")});
            this.specSettingState.Size = new System.Drawing.Size(228, 52);
            this.specSettingState.TabIndex = 4;
            this.specSettingState.Visible = false;
            // 
            // specSettingBox
            // 
            this.specSettingBox.Location = new System.Drawing.Point(6, 6);
            this.specSettingBox.Name = "specSettingBox";
            this.specSettingBox.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.specSettingBox.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.specSettingBox.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.specSettingBox.Size = new System.Drawing.Size(234, 20);
            this.specSettingBox.TabIndex = 15;
            // 
            // buttonsLayoutPanel
            // 
            this.buttonsLayoutPanel.ColumnCount = 2;
            this.tableLayoutPanel1.SetColumnSpan(this.buttonsLayoutPanel, 4);
            this.buttonsLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.buttonsLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.buttonsLayoutPanel.Controls.Add(this._cancelBtn, 1, 0);
            this.buttonsLayoutPanel.Controls.Add(this._saveBtn, 0, 0);
            this.buttonsLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonsLayoutPanel.Location = new System.Drawing.Point(132, 277);
            this.buttonsLayoutPanel.Margin = new System.Windows.Forms.Padding(0);
            this.buttonsLayoutPanel.Name = "buttonsLayoutPanel";
            this.buttonsLayoutPanel.RowCount = 1;
            this.buttonsLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.buttonsLayoutPanel.Size = new System.Drawing.Size(271, 25);
            this.buttonsLayoutPanel.TabIndex = 10;
            // 
            // _cancelBtn
            // 
            this._cancelBtn.Dock = System.Windows.Forms.DockStyle.Fill;
            this._cancelBtn.Image = ((System.Drawing.Image)(resources.GetObject("_cancelBtn.Image")));
            this._cancelBtn.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this._cancelBtn.Location = new System.Drawing.Point(243, 0);
            this._cancelBtn.Margin = new System.Windows.Forms.Padding(0);
            this._cancelBtn.Name = "_cancelBtn";
            this._cancelBtn.Size = new System.Drawing.Size(28, 25);
            this._cancelBtn.TabIndex = 0;
            this._cancelBtn.Click += new System.EventHandler(this.cancelBtn_Click);
            // 
            // _saveBtn
            // 
            this._saveBtn.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this._saveBtn.Appearance.Options.UseFont = true;
            this._saveBtn.Dock = System.Windows.Forms.DockStyle.Right;
            this._saveBtn.Image = ((System.Drawing.Image)(resources.GetObject("_saveBtn.Image")));
            this._saveBtn.Location = new System.Drawing.Point(110, 0);
            this._saveBtn.Margin = new System.Windows.Forms.Padding(0, 0, 15, 0);
            this._saveBtn.Name = "_saveBtn";
            this._saveBtn.Size = new System.Drawing.Size(118, 25);
            this._saveBtn.TabIndex = 1;
            this._saveBtn.Text = "���������";
            this._saveBtn.Click += new System.EventHandler(this.saveBtn_Click);
            // 
            // nameLbl
            // 
            this.nameLbl.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.nameLbl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nameLbl.Location = new System.Drawing.Point(14, 8);
            this.nameLbl.Name = "nameLbl";
            this.nameLbl.Size = new System.Drawing.Size(106, 15);
            this.nameLbl.TabIndex = 18;
            this.nameLbl.Text = "�������� �������";
            // 
            // stateFalseLbl
            // 
            this.stateFalseLbl.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.stateFalseLbl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.stateFalseLbl.Location = new System.Drawing.Point(14, 74);
            this.stateFalseLbl.Name = "stateFalseLbl";
            this.stateFalseLbl.Size = new System.Drawing.Size(108, 15);
            this.stateFalseLbl.TabIndex = 19;
            this.stateFalseLbl.Text = "��������� ��� \"0\"";
            // 
            // stateTrueLbl
            // 
            this.stateTrueLbl.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.stateTrueLbl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.stateTrueLbl.Location = new System.Drawing.Point(14, 107);
            this.stateTrueLbl.Name = "stateTrueLbl";
            this.stateTrueLbl.Size = new System.Drawing.Size(108, 15);
            this.stateTrueLbl.TabIndex = 20;
            this.stateTrueLbl.Text = "��������� ��� \"1\"";
            // 
            // specSettingLbl
            // 
            this.specSettingLbl.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.specSettingLbl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.specSettingLbl.Location = new System.Drawing.Point(14, 140);
            this.specSettingLbl.Name = "specSettingLbl";
            this.specSettingLbl.Size = new System.Drawing.Size(90, 15);
            this.specSettingLbl.TabIndex = 21;
            this.specSettingLbl.Text = "����. ��������";
            // 
            // startBitLbl
            // 
            this.startBitLbl.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.startBitLbl.Location = new System.Drawing.Point(158, 41);
            this.startBitLbl.Name = "startBitLbl";
            this.startBitLbl.Size = new System.Drawing.Size(91, 15);
            this.startBitLbl.TabIndex = 22;
            this.startBitLbl.Text = "��������� ���";
            // 
            // specSettingCheckBox
            // 
            this.specSettingCheckBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.specSettingCheckBox.Location = new System.Drawing.Point(135, 140);
            this.specSettingCheckBox.Name = "specSettingCheckBox";
            this.specSettingCheckBox.Properties.Caption = "";
            this.specSettingCheckBox.Size = new System.Drawing.Size(17, 19);
            this.specSettingCheckBox.TabIndex = 23;
            this.specSettingCheckBox.CheckedChanged += new System.EventHandler(this.specSettingCheckBox_CheckedChanged_1);
            // 
            // startBitBox
            // 
            this.startBitBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.startBitBox.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.startBitBox.Location = new System.Drawing.Point(264, 41);
            this.startBitBox.Name = "startBitBox";
            this.startBitBox.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.startBitBox.Properties.Appearance.Options.UseFont = true;
            this.startBitBox.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.startBitBox.Size = new System.Drawing.Size(107, 22);
            this.startBitBox.TabIndex = 24;
            // 
            // errorStatusLbl
            // 
            this.errorStatusLbl.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.errorStatusLbl.Appearance.ForeColor = System.Drawing.Color.Red;
            this.errorStatusLbl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.errorStatusLbl.Location = new System.Drawing.Point(406, 8);
            this.errorStatusLbl.Name = "errorStatusLbl";
            this.errorStatusLbl.Size = new System.Drawing.Size(137, 15);
            this.errorStatusLbl.TabIndex = 25;
            this.errorStatusLbl.Text = "���������� ���������";
            // 
            // swapBtn
            // 
            this.swapBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.swapBtn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.swapBtn.Image = ((System.Drawing.Image)(resources.GetObject("swapBtn.Image")));
            this.swapBtn.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.swapBtn.Location = new System.Drawing.Point(406, 74);
            this.swapBtn.Name = "swapBtn";
            this.tableLayoutPanel1.SetRowSpan(this.swapBtn, 3);
            this.swapBtn.Size = new System.Drawing.Size(23, 55);
            this.swapBtn.TabIndex = 26;
            this.swapBtn.ToolTip = "����������� �������� �����";
            this.swapBtn.Click += new System.EventHandler(this.swapBtn_Click_1);
            // 
            // labelInfo
            // 
            this.labelInfo.Appearance.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelInfo.Appearance.ForeColor = System.Drawing.Color.MediumBlue;
            this.labelInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelInfo.Location = new System.Drawing.Point(14, 280);
            this.labelInfo.Name = "labelInfo";
            this.labelInfo.Size = new System.Drawing.Size(0, 14);
            this.labelInfo.TabIndex = 27;
            // 
            // labelMsg
            // 
            this.labelMsg.Appearance.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelMsg.Appearance.ForeColor = System.Drawing.Color.Green;
            this.labelMsg.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelMsg.Location = new System.Drawing.Point(3, 280);
            this.labelMsg.Name = "labelMsg";
            this.labelMsg.Size = new System.Drawing.Size(0, 14);
            this.labelMsg.TabIndex = 28;
            // 
            // LogicSensorEditPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "LogicSensorEditPanel";
            this.Size = new System.Drawing.Size(541, 318);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.specSettingStateLayoutPanel.ResumeLayout(false);
            this.specSettingStateInnerLayoutPanel.ResumeLayout(false);
            this.specSettingStateInnerLayoutPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.specSettingState.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.specSettingBox.Properties)).EndInit();
            this.buttonsLayoutPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.specSettingCheckBox.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.startBitBox.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TextBox nameBox;
        private System.Windows.Forms.TextBox stateFalseBox;
        private System.Windows.Forms.TextBox stateTrueBox;
        private System.Windows.Forms.TableLayoutPanel buttonsLayoutPanel;
      private DevExpress.XtraEditors.SimpleButton _cancelBtn;
      private DevExpress.XtraEditors.SimpleButton _saveBtn;
      private DevExpress.XtraEditors.LabelControl nameLbl;
      private DevExpress.XtraEditors.LabelControl stateFalseLbl;
      private DevExpress.XtraEditors.LabelControl stateTrueLbl;
      private DevExpress.XtraEditors.LabelControl specSettingLbl;
      private DevExpress.XtraEditors.LabelControl startBitLbl;
      private DevExpress.XtraEditors.CheckEdit specSettingCheckBox;
      private DevExpress.XtraEditors.SpinEdit startBitBox;
      private DevExpress.XtraEditors.LabelControl errorStatusLbl;
      private System.Windows.Forms.TableLayoutPanel specSettingStateLayoutPanel;
      private System.Windows.Forms.TableLayoutPanel specSettingStateInnerLayoutPanel;
      private DevExpress.XtraEditors.LabelControl specSettingStateLbl;
      private DevExpress.XtraEditors.LookUpEdit specSettingBox;
      private DevExpress.XtraEditors.SimpleButton swapBtn;
      private DevExpress.XtraEditors.RadioGroup specSettingState;
      private DevExpress.XtraEditors.LabelControl labelInfo;
      private DevExpress.XtraEditors.LabelControl labelMsg;
    }
}
