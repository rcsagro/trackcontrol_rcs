using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.Utils;
using LocalCache;
using LocalCache.atlantaDataSetTableAdapters;
using EventTracker;
using TrackControl.Properties;

namespace TrackControl.Setting.Controls
{
    /// <summary>
    /// ������� ��� �������� ���������� �������.
    /// </summary>
    [ToolboxItem(false)]
    public partial class DeleteSensorTransitionControl : UserControl
    {
        /// <summary>
        /// ������� ��� ������� ��������� (�������).
        /// </summary>
        private TransitionTableAdapter _transitionAdapter;

        /// <summary>
        /// ������� ����������.
        /// </summary>
        private atlantaDataSet _dataset;

        /// <summary>
        /// �������, ������� ������������ �������.
        /// </summary>
        private atlantaDataSet.TransitionRow _transition;

        /// <summary>
        /// ��������� ����� �������� ���������� �������.
        /// </summary>
        public event EventHandler EndDelete;

        /// <summary>
        /// �����������.
        /// </summary>
        /// <param name="transitionAdapter">������� ��� ������� ���������</param>
        /// <param name="dataset">������� ����������</param>
        /// <param name="transition">�������, ������� ������������ �������</param>
        public DeleteSensorTransitionControl(
            TransitionTableAdapter transitionAdapter,
            atlantaDataSet dataset,
            atlantaDataSet.TransitionRow transition)
        {
            _transitionAdapter = transitionAdapter;
            _dataset = dataset;
            _transition = transition;

            InitializeComponent();
            init();
        }

        private void init()
        {
            askingLbl.Text = Resources.TransitionDeleteConfirm;
            _deleteBtn.Text = Resources.TransitionDelete;

        }

        /// <summary>
        /// ���������� ������� EndDelete.
        /// </summary>
        private void onEndDelete()
        {
            if (EndDelete != null)
                EndDelete(this, EventArgs.Empty);
        }

        /// <summary>
        /// ���������� �������� ��������.
        /// </summary>
        private void DeleteSensorStateControl_Load(object sender, EventArgs e)
        {
            titleLbl.Text = _transition.Title;
            iconImg.ContentImage = Helper.GetImage(_transition.IconName);
            ActiveControl = _deleteBtn;
        }

        /// <summary>
        /// ������������ ��������� ������� �� ������ "�������".
        /// </summary>
        private void deleteBtn_MouseEnter(object sender, EventArgs e)
        {
            notifyLbl.ForeColor = Color.DarkRed;
            notifyLbl.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            notifyLbl.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center; 
            notifyLbl.Appearance.TextOptions.WordWrap = WordWrap.Wrap;
            notifyLbl.Text = Resources.TransitionDelete;
        }

        /// <summary>
        /// ������������ ��������� ������� �� ������ "������".
        /// </summary>
        private void cancelBtn_MouseEnter(object sender, EventArgs e)
        {
            notifyLbl.ForeColor = Color.DarkBlue;
            notifyLbl.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            notifyLbl.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            notifyLbl.Appearance.TextOptions.WordWrap = WordWrap.Wrap;
            notifyLbl.Text = Resources.DeleteCancel;
        }

        /// <summary>
        /// ������������ �������� ������� � ������ ("�������" ���� "������").
        /// </summary>
        private void btn_MouseLeave(object sender, EventArgs e)
        {
            notifyLbl.Text = "";
        }

        /// <summary>
        /// ������������ ���� �� ������ "������".
        /// </summary>
        private void cancelBtn_Click(object sender, EventArgs e)
        {
            onEndDelete();
        }

        /// <summary>
        /// ������������ ���� �� ������ "�������".
        /// ������� ��������� ������� ������������.
        /// </summary>
        private void deleteBtn_Click(object sender, EventArgs e)
        {
            _deleteBtn.Enabled = false;
            _transition.Delete();
            _transitionAdapter.Update(_dataset.Transition);

            onEndDelete();
        }
    }
}
