using System.Data.SqlClient;
using LocalCache;
using System;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using MySql.Data.MySqlClient;
using TrackControl.General.DatabaseDriver;
using TrackControl.Properties;

namespace TrackControl.Setting
{
  [ToolboxItem(false)]
  public partial class SettingTTControl : UserControl
  {
    public SettingTTControl()
    {
      InitializeComponent();
      init();
      vehicleDataGridView.Dock = DockStyle.Fill;
    }

    void init()
    {
      dataGridViewTextBoxColumn13.HeaderText = Resources.Title;
      dataGridViewTextBoxColumn13.ToolTipText = Resources.SettingTitle;
      dataGridViewTextBoxColumn14.HeaderText = Resources.Description;
      dataGridViewTextBoxColumn14.ToolTipText = Resources.SettingDescription;
      dataGridViewTextBoxColumn2.HeaderText = Resources.ParkingTime;
      dataGridViewTextBoxColumn2.ToolTipText = Resources.ParkingTimeToolTip;
      dataGridViewTextBoxColumn3.HeaderText = Resources.EngineSpeed;
      dataGridViewTextBoxColumn3.ToolTipText = Resources.EngineSpeedToolTip;
      //dataGridViewTextBoxColumn11.HeaderText = Resources.EngineSpeedXX;
      //dataGridViewTextBoxColumn11.ToolTipText = Resources.EngineSpeedXX;
      //dataGridViewTextBoxColumn4.HeaderText = Resources.EquipmentSpeed;
      //dataGridViewTextBoxColumn4.ToolTipText = Resources.EquipmentSpeed;
      //dataGridViewTextBoxColumn12.HeaderText = Resources.EquipmentSpeedXX;
      //dataGridViewTextBoxColumn12.ToolTipText = Resources.EquipmentSpeedXX;
      dataGridViewTextBoxColumn5.HeaderText = Resources.FuelingLimit;
      dataGridViewTextBoxColumn5.ToolTipText = Resources.FuelingLimitToolTip;
      dataGridViewTextBoxColumn6.HeaderText = Resources.BandwidthSmoothing;
      dataGridViewTextBoxColumn6.ToolTipText = Resources.BandwidthSmoothingToolTip;
      dataGridViewTextBoxColumn7.HeaderText = Resources.DischargeLimit;
      dataGridViewTextBoxColumn7.ToolTipText = Resources.DischargeLimitToolTip;
      dataGridViewTextBoxColumn8.HeaderText = Resources.Acceleration;
      dataGridViewTextBoxColumn8.ToolTipText = Resources.AccelerationToolTip;
      dataGridViewTextBoxColumn9.HeaderText = Resources.Deceleration;
      dataGridViewTextBoxColumn9.ToolTipText = Resources.DecelerationToolTip;
      dataGridViewTextBoxColumn10.HeaderText = Resources.Speed;
      dataGridViewTextBoxColumn10.ToolTipText = Resources.SpeedToolTip;
      AvgFuelRatePerHour.HeaderText = Resources.FuelConsumptionAvg;
      AvgFuelRatePerHour.ToolTipText = Resources.FuelConsumptionAvgToolTip;
      FuelApproximationTime.HeaderText = Resources.LevelDeterminationTime;
      FuelApproximationTime.ToolTipText = Resources.LevelDeterminationTimeToolTip;
      TimeGetFuelAfterStop.HeaderText = Resources.LevelStabilizationTime;
      TimeGetFuelAfterStop.ToolTipText = Resources.LevelStabilizationTimeToolTip;
      TimeGetFuelBeforeMotion.HeaderText = Resources.InaccuracyTime;
      TimeGetFuelBeforeMotion.ToolTipText = Resources.InaccuracyTimeToolTip;
      CzMinCrossingPairTime.HeaderText = Resources.MinTimeBetweenTwoZonesCrossing;
      CzMinCrossingPairTime.ToolTipText = Resources.MinTimeBetweenZoneBoundsCrossingToolTip;
      FuelerRadiusFinds.HeaderText = Resources.SearchRadius;
      FuelerRadiusFinds.ToolTipText = Resources.FuelerSearchRadius;
      FuelerMaxTimeStop.HeaderText = Resources.MinIntervalBetweenFueling;
      FuelerMaxTimeStop.ToolTipText = Resources.MinTimeBeforeNextFueling;
      FuelerMinFuelrate.HeaderText = Resources.MinFuelingToReport;
      FuelerMinFuelrate.ToolTipText = Resources.MinFuelingToReportHint;
      settingBindingNavigator.Text = Resources.Assign;
      assignToolStripButton.Text = Resources.Assign;
      refreshToolStripButton.Text = Resources.Refresh;
      refreshToolStripButton.ToolTipText = Resources.RefreshData;
      dataGridViewTextBoxColumn17.HeaderText = Resources.CarNumber;
      dataGridViewTextBoxColumn17.ToolTipText = Resources.VehicleNumber;
      dataGridViewTextBoxColumn16.HeaderText = Resources.CarMark;
      dataGridViewTextBoxColumn16.ToolTipText = Resources.VehicleMark;
      dataGridViewTextBoxColumn21.HeaderText = Resources.CarModel;
      dataGridViewTextBoxColumn21.ToolTipText = Resources.VehicleModel;

      FuelerCountInMotion.HeaderText = Resources.FuelerCountInMotion;
      FuelerCountInMotion.ToolTipText = Resources.FuelerCountInMotionToolTip;
      FuelrateWithDischarge.ToolTipText = Resources.FuelrateWithDischargeToolTip;
      FuelrateWithDischarge.HeaderText = Resources.FuelrateWithDischarge;
      FuelingMinMaxAlgorithm.HeaderText = Resources.FuelingMinMaxAlgorithmHeader;
      FuelingMinMaxAlgorithm.ToolTipText = Resources.FuelingMinMaxAlgorithmToolTip;

      colTimeBreakMaxPermitted.HeaderText = Resources.TimeBreakMaxPermitted;
      colTimeBreakMaxPermitted.ToolTipText = Resources.TimeBreakMaxPermittedToolTip; 
    }

    void settingBindingNavigatorSaveItem_Click(object sender, EventArgs e)
    {
      Validate();
        //=============================================================
      settingBindingSource.EndEdit();
      int entry = settingTableAdapter.Update(atlantaDataSet.setting);
      int count = 0;
      count += entry;
      vehicleBindingSource.EndEdit();
      entry = vehicleTableAdapter.Update(atlantaDataSet.vehicle);
      count += entry;
      toolStripStatusLabel1.Text = String.Format("{0}: {1}", Resources.RowsSaved, count);
    }

    void SettingsTTControl_Load(object sender, EventArgs e)
    {
      if (DesignMode)
        return;

      string cs = Crypto.GetDecryptConnectionString(ConfigurationManager.ConnectionStrings["CS"].ConnectionString);
      //vehicleTableAdapter.Connection.ConnectionString = cs;
      if( DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse )
      {
          MySqlConnection connection = (MySqlConnection)vehicleTableAdapter.Connection;
          connection.ConnectionString = cs;
      }
      else if( DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse )
      {
          SqlConnection connection = (SqlConnection)vehicleTableAdapter.Connection;
          connection.ConnectionString = cs;
      }
      //settingTableAdapter.Connection.ConnectionString = cs;

      if( DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse )
      {
          MySqlConnection connection = (MySqlConnection)settingTableAdapter.Connection;
          connection.ConnectionString = cs;
      }
      else if( DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse )
      {
          SqlConnection connection = (SqlConnection)settingTableAdapter.Connection;
          connection.ConnectionString = cs;
      }
      

      atlantaDataSet = AppModel.Instance.DataSet;

      vehicleBindingSource.DataSource = atlantaDataSet.vehicle;
      settingBindingSource.DataSource = atlantaDataSet.setting;
    }

    /// <summary>
    /// setting_SelectionChanged
    /// </summary>
    void setting_SelectionChanged(int s_id)
    {
        int itemFound = settingBindingSource.Find("id", s_id);
        settingBindingSource.Position = itemFound;
    }

    void vehicleDataGridView_SelectionChanged(object sender, EventArgs e)
    {
      int s_id = -1;
      if (vehicleBindingSource.Current != null)
      {
        s_id = ((atlantaDataSet.vehicleRow)(((DataRowView)vehicleBindingSource.Current).Row)).setting_id;
        setting_SelectionChanged(s_id);
      }
    }

      // ����� ��������� ���������� �������� �� ������
    void assignToolStripButton_Click(object sender, EventArgs e)
    {
      atlantaDataSet.settingRow cur_row = ((atlantaDataSet.settingRow)((DataRowView)settingBindingSource.Current).Row);
      ((atlantaDataSet.vehicleRow)((DataRowView)vehicleBindingSource.Current).Row).setting_id = cur_row.id;
      int count = vehicleTableAdapter.Update(atlantaDataSet.vehicle);
      toolStripStatusLabel1.Text = String.Format("{0}: {1}", Resources.RowsUpdated, count);
    } // ============================

    void bindingNavigatorDeleteItem_Click(object sender, EventArgs e)
    {
      settingTableAdapter.Update(atlantaDataSet.setting);

      if (settingDataGridView.SelectedRows.Count > 0)
      {
        int s_id = (int)settingDataGridView.SelectedRows[0].Cells["dataGridViewTextBoxColumn1"].Value;
        if (s_id == -1)
        {
          return;
        }
        foreach (atlantaDataSet.vehicleRow v_row in atlantaDataSet.vehicle)
        {
          if (v_row.setting_id == s_id)
          {
            v_row.setting_id = 1;

          }
        }
        vehicleTableAdapter.Update(atlantaDataSet.vehicle);
      }
    }

    void bindingNavigatorAddNewItem_Click(object sender, EventArgs e)
    {
      try
      {
        settingBindingSource.SuspendBinding();
        atlantaDataSet.settingRow setting_row = GetDefaultSettingRow();
        atlantaDataSet.setting.AddsettingRow(setting_row);
        settingBindingSource.ResumeBinding();
        settingBindingSource.EndEdit();
        settingTableAdapter.Update(atlantaDataSet.setting);
      }
      catch (Exception ex)
      {
        XtraMessageBox.Show(ex.StackTrace, "AddNewRow Error");
      }
      atlantaDataSet.AcceptChanges();
    }

    void refreshToolStripButton_Click(object sender, EventArgs e)
    {
      SettingsTTControl_Load(this, null);
    }

    atlantaDataSet.settingRow GetDefaultSettingRow()
    {
      atlantaDataSet.settingRow setting_row = atlantaDataSet.setting.NewsettingRow();
      setting_row.accelMax = 1;
      setting_row.AvgFuelRatePerHour = 30;
      setting_row.band = 5; //������ �����������
      setting_row.breakMax = 1;
      setting_row.CzMinCrossingPairTime = new TimeSpan(0, 1, 0);
      setting_row.Desc = Resources.Description;
      setting_row.FuelApproximationTime = new TimeSpan(0, 2, 0);
      setting_row.FuelerMaxTimeStop = new TimeSpan(0, 2, 0);
      setting_row.FuelerMinFuelrate = 1;
      setting_row.FuelerRadiusFinds = 10;
      setting_row.FuelingDischarge = 10;
      setting_row.FuelingEdge = 20;
      setting_row.Name = Resources.SettingXX;
      setting_row.RotationAdd = 100;
      setting_row.RotationMain = 100;
      setting_row.speedMax = 70;
      setting_row.TimeBreak = new TimeSpan(0, 1, 0);
      setting_row.TimeGetFuelAfterStop = new TimeSpan(0);
      setting_row.TimeGetFuelBeforeMotion = new TimeSpan(0);
      setting_row.idleRunningAdd = 0;
      setting_row.idleRunningMain = 0;
      setting_row.FuelerCountInMotion = 0;
      setting_row.FuelrateWithDischarge = 0;
      setting_row.FuelingMinMaxAlgorithm = 0;
      setting_row.TimeBreakMaxPermitted = new TimeSpan(3, 0, 0);
      return setting_row;
    }
  }
}