namespace TrackControl.Setting.Controls
{
  partial class LogicSensorsSettingsPanel
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.components = new System.ComponentModel.Container();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
        System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LogicSensorsSettingsPanel));
        this.splitContainer1 = new System.Windows.Forms.SplitContainer();
        this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
        this.mobitelsGrid = new System.Windows.Forms.DataGridView();
        this.mobitel = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.ColumnNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.ColumnMarka = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.ColumnModel = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.vehicleBindingSource = new System.Windows.Forms.BindingSource(this.components);
        this.label1 = new System.Windows.Forms.Label();
        this.ls_LayoutPanel = new System.Windows.Forms.TableLayoutPanel();
        this.label2 = new System.Windows.Forms.Label();
        this.sensorsGrid = new System.Windows.Forms.DataGridView();
        this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.SpecSettingColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.mobitelsSensorsBindingSource = new System.Windows.Forms.BindingSource(this.components);
        this.sensorsHead = new System.Windows.Forms.Panel();
        this.sensorsHeadTablePanel = new System.Windows.Forms.TableLayoutPanel();
        this.sensorHeadLbl = new System.Windows.Forms.Label();
        this.removeBtn = new System.Windows.Forms.Label();
        this.addBtn = new System.Windows.Forms.Label();
        this.editBtn = new System.Windows.Forms.Label();
        this.editPanel = new System.Windows.Forms.Panel();
        this.sensorInfoPanel = new System.Windows.Forms.TableLayoutPanel();
        this.statePanel = new System.Windows.Forms.Panel();
        this.stateTable = new System.Windows.Forms.TableLayoutPanel();
        this.stateFalseLbl = new System.Windows.Forms.Label();
        this.stateFalseBox = new System.Windows.Forms.TextBox();
        this.sensorStateFalseBindingSource = new System.Windows.Forms.BindingSource(this.components);
        this.stateTrueBox = new System.Windows.Forms.TextBox();
        this.sensorStateTrueBindingSource = new System.Windows.Forms.BindingSource(this.components);
        this.stateTrueLbl = new System.Windows.Forms.Label();
        this.tableForMainBtn = new System.Windows.Forms.TableLayoutPanel();
        this._editSensorBtn = new DevExpress.XtraEditors.SimpleButton();
        this.stateHeadLbl = new System.Windows.Forms.Label();
        this.transitionPanel = new System.Windows.Forms.Panel();
        this.transitionTable = new System.Windows.Forms.TableLayoutPanel();
        this.tableForEventBtn = new System.Windows.Forms.TableLayoutPanel();
        this._eventEditBtn = new DevExpress.XtraEditors.SimpleButton();
        this.label4 = new System.Windows.Forms.Label();
        this.nameDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.startBitDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.mobitelsBindingSource = new System.Windows.Forms.BindingSource(this.components);
        this.splitContainer1.Panel1.SuspendLayout();
        this.splitContainer1.Panel2.SuspendLayout();
        this.splitContainer1.SuspendLayout();
        this.tableLayoutPanel1.SuspendLayout();
        ((System.ComponentModel.ISupportInitialize)(this.mobitelsGrid)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.vehicleBindingSource)).BeginInit();
        this.ls_LayoutPanel.SuspendLayout();
        ((System.ComponentModel.ISupportInitialize)(this.sensorsGrid)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.mobitelsSensorsBindingSource)).BeginInit();
        this.sensorsHead.SuspendLayout();
        this.sensorsHeadTablePanel.SuspendLayout();
        this.editPanel.SuspendLayout();
        this.sensorInfoPanel.SuspendLayout();
        this.statePanel.SuspendLayout();
        this.stateTable.SuspendLayout();
        ((System.ComponentModel.ISupportInitialize)(this.sensorStateFalseBindingSource)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.sensorStateTrueBindingSource)).BeginInit();
        this.tableForMainBtn.SuspendLayout();
        this.transitionPanel.SuspendLayout();
        this.transitionTable.SuspendLayout();
        this.tableForEventBtn.SuspendLayout();
        ((System.ComponentModel.ISupportInitialize)(this.mobitelsBindingSource)).BeginInit();
        this.SuspendLayout();
        // 
        // splitContainer1
        // 
        this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
        this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
        this.splitContainer1.IsSplitterFixed = true;
        this.splitContainer1.Location = new System.Drawing.Point(2, 2);
        this.splitContainer1.Margin = new System.Windows.Forms.Padding(0);
        this.splitContainer1.Name = "splitContainer1";
        // 
        // splitContainer1.Panel1
        // 
        this.splitContainer1.Panel1.Controls.Add(this.tableLayoutPanel1);
        // 
        // splitContainer1.Panel2
        // 
        this.splitContainer1.Panel2.Controls.Add(this.ls_LayoutPanel);
        this.splitContainer1.Size = new System.Drawing.Size(814, 470);
        this.splitContainer1.SplitterDistance = 250;
        this.splitContainer1.SplitterWidth = 3;
        this.splitContainer1.TabIndex = 0;
        // 
        // tableLayoutPanel1
        // 
        this.tableLayoutPanel1.ColumnCount = 1;
        this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
        this.tableLayoutPanel1.Controls.Add(this.mobitelsGrid, 0, 1);
        this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
        this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
        this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
        this.tableLayoutPanel1.Name = "tableLayoutPanel1";
        this.tableLayoutPanel1.RowCount = 2;
        this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
        this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
        this.tableLayoutPanel1.Size = new System.Drawing.Size(248, 468);
        this.tableLayoutPanel1.TabIndex = 0;
        // 
        // mobitelsGrid
        // 
        this.mobitelsGrid.AllowUserToAddRows = false;
        this.mobitelsGrid.AllowUserToDeleteRows = false;
        this.mobitelsGrid.AllowUserToResizeRows = false;
        this.mobitelsGrid.AutoGenerateColumns = false;
        this.mobitelsGrid.BorderStyle = System.Windows.Forms.BorderStyle.None;
        this.mobitelsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
        this.mobitelsGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.mobitel,
            this.ColumnNumber,
            this.ColumnMarka,
            this.ColumnModel});
        this.mobitelsGrid.DataSource = this.vehicleBindingSource;
        dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
        dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
        dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
        dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
        dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
        dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
        dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
        this.mobitelsGrid.DefaultCellStyle = dataGridViewCellStyle1;
        this.mobitelsGrid.Dock = System.Windows.Forms.DockStyle.Fill;
        this.mobitelsGrid.Location = new System.Drawing.Point(0, 27);
        this.mobitelsGrid.Margin = new System.Windows.Forms.Padding(0, 3, 0, 0);
        this.mobitelsGrid.MultiSelect = false;
        this.mobitelsGrid.Name = "mobitelsGrid";
        this.mobitelsGrid.ReadOnly = true;
        this.mobitelsGrid.RowHeadersVisible = false;
        this.mobitelsGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
        this.mobitelsGrid.Size = new System.Drawing.Size(248, 441);
        this.mobitelsGrid.TabIndex = 0;
        this.mobitelsGrid.SelectionChanged += new System.EventHandler(this.mobitelsGrid_SelectionChanged);
        // 
        // mobitel
        // 
        this.mobitel.DataPropertyName = "Mobitel_id";
        this.mobitel.HeaderText = "mobitel";
        this.mobitel.Name = "mobitel";
        this.mobitel.ReadOnly = true;
        this.mobitel.Visible = false;
        // 
        // ColumnNumber
        // 
        this.ColumnNumber.DataPropertyName = "NumberPlate";
        this.ColumnNumber.HeaderText = "�����";
        this.ColumnNumber.MinimumWidth = 50;
        this.ColumnNumber.Name = "ColumnNumber";
        this.ColumnNumber.ReadOnly = true;
        this.ColumnNumber.Width = 55;
        // 
        // ColumnMarka
        // 
        this.ColumnMarka.DataPropertyName = "MakeCar";
        this.ColumnMarka.HeaderText = "�����";
        this.ColumnMarka.MinimumWidth = 50;
        this.ColumnMarka.Name = "ColumnMarka";
        this.ColumnMarka.ReadOnly = true;
        this.ColumnMarka.Width = 120;
        // 
        // ColumnModel
        // 
        this.ColumnModel.DataPropertyName = "CarModel";
        this.ColumnModel.HeaderText = "������";
        this.ColumnModel.MinimumWidth = 50;
        this.ColumnModel.Name = "ColumnModel";
        this.ColumnModel.ReadOnly = true;
        this.ColumnModel.Width = 80;
        // 
        // label1
        // 
        this.label1.AutoSize = true;
        this.label1.BackColor = System.Drawing.Color.LightBlue;
        this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
        this.label1.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
        this.label1.ForeColor = System.Drawing.Color.Black;
        this.label1.Location = new System.Drawing.Point(0, 0);
        this.label1.Margin = new System.Windows.Forms.Padding(0);
        this.label1.Name = "label1";
        this.label1.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
        this.label1.Size = new System.Drawing.Size(248, 24);
        this.label1.TabIndex = 1;
        this.label1.Text = "������������ ��������";
        this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
        // 
        // ls_LayoutPanel
        // 
        this.ls_LayoutPanel.ColumnCount = 1;
        this.ls_LayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
        this.ls_LayoutPanel.Controls.Add(this.label2, 0, 2);
        this.ls_LayoutPanel.Controls.Add(this.sensorsGrid, 0, 1);
        this.ls_LayoutPanel.Controls.Add(this.sensorsHead, 0, 0);
        this.ls_LayoutPanel.Controls.Add(this.editPanel, 0, 3);
        this.ls_LayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
        this.ls_LayoutPanel.Location = new System.Drawing.Point(0, 0);
        this.ls_LayoutPanel.Margin = new System.Windows.Forms.Padding(0);
        this.ls_LayoutPanel.Name = "ls_LayoutPanel";
        this.ls_LayoutPanel.RowCount = 4;
        this.ls_LayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
        this.ls_LayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
        this.ls_LayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 4F));
        this.ls_LayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 305F));
        this.ls_LayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
        this.ls_LayoutPanel.Size = new System.Drawing.Size(559, 468);
        this.ls_LayoutPanel.TabIndex = 0;
        // 
        // label2
        // 
        this.label2.AutoSize = true;
        this.label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
        this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
        this.label2.Location = new System.Drawing.Point(0, 159);
        this.label2.Margin = new System.Windows.Forms.Padding(0);
        this.label2.Name = "label2";
        this.label2.Size = new System.Drawing.Size(559, 4);
        this.label2.TabIndex = 1;
        // 
        // sensorsGrid
        // 
        this.sensorsGrid.AllowUserToAddRows = false;
        this.sensorsGrid.AllowUserToDeleteRows = false;
        this.sensorsGrid.AllowUserToResizeColumns = false;
        this.sensorsGrid.AllowUserToResizeRows = false;
        this.sensorsGrid.AutoGenerateColumns = false;
        this.sensorsGrid.BorderStyle = System.Windows.Forms.BorderStyle.None;
        this.sensorsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
        this.sensorsGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this.nameDataGridViewTextBoxColumn1,
            this.startBitDataGridViewTextBoxColumn,
            this.SpecSettingColumn});
        this.sensorsGrid.DataSource = this.mobitelsSensorsBindingSource;
        dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
        dataGridViewCellStyle2.BackColor = System.Drawing.Color.WhiteSmoke;
        dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
        dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
        dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
        dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
        dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
        this.sensorsGrid.DefaultCellStyle = dataGridViewCellStyle2;
        this.sensorsGrid.Dock = System.Windows.Forms.DockStyle.Fill;
        this.sensorsGrid.Location = new System.Drawing.Point(0, 27);
        this.sensorsGrid.Margin = new System.Windows.Forms.Padding(0, 3, 0, 0);
        this.sensorsGrid.MultiSelect = false;
        this.sensorsGrid.Name = "sensorsGrid";
        this.sensorsGrid.ReadOnly = true;
        this.sensorsGrid.RowHeadersVisible = false;
        this.sensorsGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
        this.sensorsGrid.Size = new System.Drawing.Size(559, 132);
        this.sensorsGrid.TabIndex = 1;
        this.sensorsGrid.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.sensorsGrid_CellPainting);
        this.sensorsGrid.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.sensorsGrid_CellMouseDoubleClick);
        // 
        // id
        // 
        this.id.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
        this.id.DataPropertyName = "id";
        this.id.Frozen = true;
        this.id.HeaderText = "id";
        this.id.MinimumWidth = 40;
        this.id.Name = "id";
        this.id.ReadOnly = true;
        this.id.Visible = false;
        this.id.Width = 40;
        // 
        // SpecSettingColumn
        // 
        this.SpecSettingColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
        this.SpecSettingColumn.HeaderText = "����������";
        this.SpecSettingColumn.MinimumWidth = 50;
        this.SpecSettingColumn.Name = "SpecSettingColumn";
        this.SpecSettingColumn.ReadOnly = true;
        this.SpecSettingColumn.ToolTipText = "����������� ����������";
        this.SpecSettingColumn.Width = 150;
        // 
        // mobitelsSensorsBindingSource
        // 
        this.mobitelsSensorsBindingSource.DataMember = "mobitels_sensors";
        this.mobitelsSensorsBindingSource.DataSource = this.mobitelsBindingSource;
        this.mobitelsSensorsBindingSource.Filter = "Length=1";
        this.mobitelsSensorsBindingSource.Sort = "Id ASC";
        // 
        // sensorsHead
        // 
        this.sensorsHead.BackColor = System.Drawing.Color.LightBlue;
        this.sensorsHead.Controls.Add(this.sensorsHeadTablePanel);
        this.sensorsHead.Dock = System.Windows.Forms.DockStyle.Fill;
        this.sensorsHead.Location = new System.Drawing.Point(0, 0);
        this.sensorsHead.Margin = new System.Windows.Forms.Padding(0);
        this.sensorsHead.Name = "sensorsHead";
        this.sensorsHead.Size = new System.Drawing.Size(559, 24);
        this.sensorsHead.TabIndex = 3;
        // 
        // sensorsHeadTablePanel
        // 
        this.sensorsHeadTablePanel.ColumnCount = 8;
        this.sensorsHeadTablePanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
        this.sensorsHeadTablePanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 5F));
        this.sensorsHeadTablePanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 92F));
        this.sensorsHeadTablePanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 5F));
        this.sensorsHeadTablePanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 81F));
        this.sensorsHeadTablePanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 5F));
        this.sensorsHeadTablePanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 90F));
        this.sensorsHeadTablePanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 5F));
        this.sensorsHeadTablePanel.Controls.Add(this.sensorHeadLbl, 0, 0);
        this.sensorsHeadTablePanel.Controls.Add(this.removeBtn, 4, 0);
        this.sensorsHeadTablePanel.Controls.Add(this.addBtn, 6, 0);
        this.sensorsHeadTablePanel.Controls.Add(this.editBtn, 2, 0);
        this.sensorsHeadTablePanel.Dock = System.Windows.Forms.DockStyle.Fill;
        this.sensorsHeadTablePanel.Location = new System.Drawing.Point(0, 0);
        this.sensorsHeadTablePanel.Margin = new System.Windows.Forms.Padding(0);
        this.sensorsHeadTablePanel.Name = "sensorsHeadTablePanel";
        this.sensorsHeadTablePanel.RowCount = 1;
        this.sensorsHeadTablePanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
        this.sensorsHeadTablePanel.Size = new System.Drawing.Size(559, 24);
        this.sensorsHeadTablePanel.TabIndex = 0;
        // 
        // sensorHeadLbl
        // 
        this.sensorHeadLbl.AutoSize = true;
        this.sensorHeadLbl.BackColor = System.Drawing.Color.LightBlue;
        this.sensorHeadLbl.Dock = System.Windows.Forms.DockStyle.Fill;
        this.sensorHeadLbl.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
        this.sensorHeadLbl.ForeColor = System.Drawing.Color.Black;
        this.sensorHeadLbl.Location = new System.Drawing.Point(0, 0);
        this.sensorHeadLbl.Margin = new System.Windows.Forms.Padding(0);
        this.sensorHeadLbl.Name = "sensorHeadLbl";
        this.sensorHeadLbl.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
        this.sensorHeadLbl.Size = new System.Drawing.Size(276, 24);
        this.sensorHeadLbl.TabIndex = 0;
        this.sensorHeadLbl.Text = "���������� �������";
        this.sensorHeadLbl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
        // 
        // removeBtn
        // 
        this.removeBtn.AutoSize = true;
        this.removeBtn.BackColor = System.Drawing.Color.AliceBlue;
        this.removeBtn.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
        this.removeBtn.Cursor = System.Windows.Forms.Cursors.Hand;
        this.removeBtn.Dock = System.Windows.Forms.DockStyle.Fill;
        this.removeBtn.Font = new System.Drawing.Font("Arial", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
        this.removeBtn.ForeColor = System.Drawing.Color.DarkRed;
        this.removeBtn.Image = ((System.Drawing.Image)(resources.GetObject("removeBtn.Image")));
        this.removeBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
        this.removeBtn.Location = new System.Drawing.Point(379, 1);
        this.removeBtn.Margin = new System.Windows.Forms.Padding(1);
        this.removeBtn.Name = "removeBtn";
        this.removeBtn.Size = new System.Drawing.Size(79, 22);
        this.removeBtn.TabIndex = 1;
        this.removeBtn.Text = "�������";
        this.removeBtn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
        this.removeBtn.Click += new System.EventHandler(this.removeBtn_Click);
        // 
        // addBtn
        // 
        this.addBtn.AutoSize = true;
        this.addBtn.BackColor = System.Drawing.Color.AliceBlue;
        this.addBtn.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
        this.addBtn.Cursor = System.Windows.Forms.Cursors.Hand;
        this.addBtn.Dock = System.Windows.Forms.DockStyle.Fill;
        this.addBtn.Font = new System.Drawing.Font("Arial", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
        this.addBtn.ForeColor = System.Drawing.Color.DarkGreen;
        this.addBtn.Image = ((System.Drawing.Image)(resources.GetObject("addBtn.Image")));
        this.addBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
        this.addBtn.Location = new System.Drawing.Point(465, 1);
        this.addBtn.Margin = new System.Windows.Forms.Padding(1);
        this.addBtn.Name = "addBtn";
        this.addBtn.Size = new System.Drawing.Size(88, 22);
        this.addBtn.TabIndex = 2;
        this.addBtn.Text = "��������";
        this.addBtn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
        this.addBtn.Click += new System.EventHandler(this.addBtn_Click);
        // 
        // editBtn
        // 
        this.editBtn.AutoSize = true;
        this.editBtn.BackColor = System.Drawing.Color.AliceBlue;
        this.editBtn.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
        this.editBtn.Cursor = System.Windows.Forms.Cursors.Hand;
        this.editBtn.Dock = System.Windows.Forms.DockStyle.Fill;
        this.editBtn.Font = new System.Drawing.Font("Arial", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
        this.editBtn.ForeColor = System.Drawing.Color.MidnightBlue;
        this.editBtn.Image = ((System.Drawing.Image)(resources.GetObject("editBtn.Image")));
        this.editBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
        this.editBtn.Location = new System.Drawing.Point(282, 1);
        this.editBtn.Margin = new System.Windows.Forms.Padding(1);
        this.editBtn.Name = "editBtn";
        this.editBtn.Size = new System.Drawing.Size(90, 22);
        this.editBtn.TabIndex = 3;
        this.editBtn.Text = "��������";
        this.editBtn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
        this.editBtn.Click += new System.EventHandler(this.editBtn_Click);
        // 
        // editPanel
        // 
        this.editPanel.Controls.Add(this.sensorInfoPanel);
        this.editPanel.Dock = System.Windows.Forms.DockStyle.Fill;
        this.editPanel.Location = new System.Drawing.Point(0, 163);
        this.editPanel.Margin = new System.Windows.Forms.Padding(0);
        this.editPanel.Name = "editPanel";
        this.editPanel.Size = new System.Drawing.Size(559, 305);
        this.editPanel.TabIndex = 2;
        // 
        // sensorInfoPanel
        // 
        this.sensorInfoPanel.BackColor = System.Drawing.Color.LightGray;
        this.sensorInfoPanel.ColumnCount = 5;
        this.sensorInfoPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
        this.sensorInfoPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
        this.sensorInfoPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
        this.sensorInfoPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
        this.sensorInfoPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 11F));
        this.sensorInfoPanel.Controls.Add(this.statePanel, 1, 1);
        this.sensorInfoPanel.Controls.Add(this.transitionPanel, 3, 1);
        this.sensorInfoPanel.Dock = System.Windows.Forms.DockStyle.Fill;
        this.sensorInfoPanel.Location = new System.Drawing.Point(0, 0);
        this.sensorInfoPanel.Margin = new System.Windows.Forms.Padding(0);
        this.sensorInfoPanel.Name = "sensorInfoPanel";
        this.sensorInfoPanel.RowCount = 3;
        this.sensorInfoPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
        this.sensorInfoPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
        this.sensorInfoPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
        this.sensorInfoPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
        this.sensorInfoPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
        this.sensorInfoPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
        this.sensorInfoPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
        this.sensorInfoPanel.Size = new System.Drawing.Size(559, 305);
        this.sensorInfoPanel.TabIndex = 0;
        // 
        // statePanel
        // 
        this.statePanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
        this.statePanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
        this.statePanel.Controls.Add(this.stateTable);
        this.statePanel.Dock = System.Windows.Forms.DockStyle.Fill;
        this.statePanel.Location = new System.Drawing.Point(10, 10);
        this.statePanel.Margin = new System.Windows.Forms.Padding(0);
        this.statePanel.Name = "statePanel";
        this.statePanel.Size = new System.Drawing.Size(264, 285);
        this.statePanel.TabIndex = 1;
        // 
        // stateTable
        // 
        this.stateTable.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
        this.stateTable.ColumnCount = 1;
        this.stateTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
        this.stateTable.Controls.Add(this.stateFalseLbl, 0, 1);
        this.stateTable.Controls.Add(this.stateFalseBox, 0, 2);
        this.stateTable.Controls.Add(this.stateTrueBox, 0, 4);
        this.stateTable.Controls.Add(this.stateTrueLbl, 0, 3);
        this.stateTable.Controls.Add(this.tableForMainBtn, 0, 6);
        this.stateTable.Controls.Add(this.stateHeadLbl, 0, 0);
        this.stateTable.Dock = System.Windows.Forms.DockStyle.Fill;
        this.stateTable.Location = new System.Drawing.Point(0, 0);
        this.stateTable.Margin = new System.Windows.Forms.Padding(0);
        this.stateTable.Name = "stateTable";
        this.stateTable.RowCount = 8;
        this.stateTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
        this.stateTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
        this.stateTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
        this.stateTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
        this.stateTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
        this.stateTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
        this.stateTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
        this.stateTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
        this.stateTable.Size = new System.Drawing.Size(262, 283);
        this.stateTable.TabIndex = 0;
        // 
        // stateFalseLbl
        // 
        this.stateFalseLbl.AutoSize = true;
        this.stateFalseLbl.Dock = System.Windows.Forms.DockStyle.Fill;
        this.stateFalseLbl.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
        this.stateFalseLbl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
        this.stateFalseLbl.Location = new System.Drawing.Point(10, 24);
        this.stateFalseLbl.Margin = new System.Windows.Forms.Padding(10, 0, 10, 0);
        this.stateFalseLbl.Name = "stateFalseLbl";
        this.stateFalseLbl.Padding = new System.Windows.Forms.Padding(0, 0, 0, 2);
        this.stateFalseLbl.Size = new System.Drawing.Size(242, 30);
        this.stateFalseLbl.TabIndex = 0;
        this.stateFalseLbl.Text = "��������� ��� \"0\"";
        this.stateFalseLbl.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
        // 
        // stateFalseBox
        // 
        this.stateFalseBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(250)))), ((int)(((byte)(211)))));
        this.stateFalseBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.sensorStateFalseBindingSource, "Title", true));
        this.stateFalseBox.Dock = System.Windows.Forms.DockStyle.Fill;
        this.stateFalseBox.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
        this.stateFalseBox.ForeColor = System.Drawing.Color.Black;
        this.stateFalseBox.Location = new System.Drawing.Point(10, 54);
        this.stateFalseBox.Margin = new System.Windows.Forms.Padding(10, 0, 10, 0);
        this.stateFalseBox.MaxLength = 49;
        this.stateFalseBox.Multiline = true;
        this.stateFalseBox.Name = "stateFalseBox";
        this.stateFalseBox.ReadOnly = true;
        this.stateFalseBox.Size = new System.Drawing.Size(242, 24);
        this.stateFalseBox.TabIndex = 1;
        // 
        // sensorStateFalseBindingSource
        // 
        this.sensorStateFalseBindingSource.DataMember = "sensors_State";
        this.sensorStateFalseBindingSource.DataSource = this.mobitelsSensorsBindingSource;
        this.sensorStateFalseBindingSource.Filter = "MinValue=0";
        // 
        // stateTrueBox
        // 
        this.stateTrueBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(252)))), ((int)(((byte)(250)))), ((int)(((byte)(211)))));
        this.stateTrueBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.sensorStateTrueBindingSource, "Title", true));
        this.stateTrueBox.Dock = System.Windows.Forms.DockStyle.Fill;
        this.stateTrueBox.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
        this.stateTrueBox.ForeColor = System.Drawing.Color.Black;
        this.stateTrueBox.Location = new System.Drawing.Point(10, 108);
        this.stateTrueBox.Margin = new System.Windows.Forms.Padding(10, 0, 10, 0);
        this.stateTrueBox.MaxLength = 49;
        this.stateTrueBox.Multiline = true;
        this.stateTrueBox.Name = "stateTrueBox";
        this.stateTrueBox.ReadOnly = true;
        this.stateTrueBox.Size = new System.Drawing.Size(242, 24);
        this.stateTrueBox.TabIndex = 2;
        // 
        // sensorStateTrueBindingSource
        // 
        this.sensorStateTrueBindingSource.DataMember = "sensors_State";
        this.sensorStateTrueBindingSource.DataSource = this.mobitelsSensorsBindingSource;
        this.sensorStateTrueBindingSource.Filter = "MinValue=1";
        // 
        // stateTrueLbl
        // 
        this.stateTrueLbl.AutoSize = true;
        this.stateTrueLbl.Dock = System.Windows.Forms.DockStyle.Fill;
        this.stateTrueLbl.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
        this.stateTrueLbl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
        this.stateTrueLbl.Location = new System.Drawing.Point(10, 78);
        this.stateTrueLbl.Margin = new System.Windows.Forms.Padding(10, 0, 10, 0);
        this.stateTrueLbl.Name = "stateTrueLbl";
        this.stateTrueLbl.Padding = new System.Windows.Forms.Padding(0, 0, 0, 2);
        this.stateTrueLbl.Size = new System.Drawing.Size(242, 30);
        this.stateTrueLbl.TabIndex = 3;
        this.stateTrueLbl.Text = "��������� ��� \"1\"";
        this.stateTrueLbl.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
        // 
        // tableForMainBtn
        // 
        this.tableForMainBtn.ColumnCount = 2;
        this.tableForMainBtn.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
        this.tableForMainBtn.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 243F));
        this.tableForMainBtn.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
        this.tableForMainBtn.Controls.Add(this._editSensorBtn, 1, 0);
        this.tableForMainBtn.Dock = System.Windows.Forms.DockStyle.Fill;
        this.tableForMainBtn.Location = new System.Drawing.Point(0, 152);
        this.tableForMainBtn.Margin = new System.Windows.Forms.Padding(0);
        this.tableForMainBtn.Name = "tableForMainBtn";
        this.tableForMainBtn.RowCount = 1;
        this.tableForMainBtn.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
        this.tableForMainBtn.Size = new System.Drawing.Size(262, 27);
        this.tableForMainBtn.TabIndex = 4;
        // 
        // _editSensorBtn
        // 
        this._editSensorBtn.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
        this._editSensorBtn.Appearance.Options.UseFont = true;
        this._editSensorBtn.Dock = System.Windows.Forms.DockStyle.Right;
        this._editSensorBtn.Image = ((System.Drawing.Image)(resources.GetObject("_editSensorBtn.Image")));
        this._editSensorBtn.Location = new System.Drawing.Point(110, 0);
        this._editSensorBtn.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
        this._editSensorBtn.Name = "_editSensorBtn";
        this._editSensorBtn.Size = new System.Drawing.Size(142, 27);
        this._editSensorBtn.TabIndex = 0;
        this._editSensorBtn.Text = "�������������";
        this._editSensorBtn.Click += new System.EventHandler(this.editBtn_Click);
        // 
        // stateHeadLbl
        // 
        this.stateHeadLbl.AutoSize = true;
        this.stateHeadLbl.BackColor = System.Drawing.Color.LightBlue;
        this.stateHeadLbl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
        this.stateHeadLbl.Dock = System.Windows.Forms.DockStyle.Fill;
        this.stateHeadLbl.Font = new System.Drawing.Font("Arial", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
        this.stateHeadLbl.ForeColor = System.Drawing.Color.Black;
        this.stateHeadLbl.Location = new System.Drawing.Point(1, 1);
        this.stateHeadLbl.Margin = new System.Windows.Forms.Padding(1);
        this.stateHeadLbl.Name = "stateHeadLbl";
        this.stateHeadLbl.Size = new System.Drawing.Size(260, 22);
        this.stateHeadLbl.TabIndex = 5;
        this.stateHeadLbl.Text = "���������";
        this.stateHeadLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
        // 
        // transitionPanel
        // 
        this.transitionPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
        this.transitionPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
        this.transitionPanel.Controls.Add(this.transitionTable);
        this.transitionPanel.Dock = System.Windows.Forms.DockStyle.Fill;
        this.transitionPanel.Location = new System.Drawing.Point(284, 10);
        this.transitionPanel.Margin = new System.Windows.Forms.Padding(0);
        this.transitionPanel.Name = "transitionPanel";
        this.transitionPanel.Size = new System.Drawing.Size(264, 285);
        this.transitionPanel.TabIndex = 2;
        // 
        // transitionTable
        // 
        this.transitionTable.ColumnCount = 1;
        this.transitionTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
        this.transitionTable.Controls.Add(this.tableForEventBtn, 0, 3);
        this.transitionTable.Controls.Add(this.label4, 0, 0);
        this.transitionTable.Dock = System.Windows.Forms.DockStyle.Fill;
        this.transitionTable.Location = new System.Drawing.Point(0, 0);
        this.transitionTable.Margin = new System.Windows.Forms.Padding(0);
        this.transitionTable.Name = "transitionTable";
        this.transitionTable.RowCount = 5;
        this.transitionTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
        this.transitionTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
        this.transitionTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 119F));
        this.transitionTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
        this.transitionTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
        this.transitionTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
        this.transitionTable.Size = new System.Drawing.Size(262, 283);
        this.transitionTable.TabIndex = 0;
        // 
        // tableForEventBtn
        // 
        this.tableForEventBtn.ColumnCount = 2;
        this.tableForEventBtn.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
        this.tableForEventBtn.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 239F));
        this.tableForEventBtn.Controls.Add(this._eventEditBtn, 1, 0);
        this.tableForEventBtn.Dock = System.Windows.Forms.DockStyle.Fill;
        this.tableForEventBtn.Location = new System.Drawing.Point(0, 153);
        this.tableForEventBtn.Margin = new System.Windows.Forms.Padding(0);
        this.tableForEventBtn.Name = "tableForEventBtn";
        this.tableForEventBtn.RowCount = 1;
        this.tableForEventBtn.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
        this.tableForEventBtn.Size = new System.Drawing.Size(262, 27);
        this.tableForEventBtn.TabIndex = 4;
        // 
        // _eventEditBtn
        // 
        this._eventEditBtn.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
        this._eventEditBtn.Appearance.Options.UseFont = true;
        this._eventEditBtn.Dock = System.Windows.Forms.DockStyle.Right;
        this._eventEditBtn.Image = ((System.Drawing.Image)(resources.GetObject("_eventEditBtn.Image")));
        this._eventEditBtn.Location = new System.Drawing.Point(137, 0);
        this._eventEditBtn.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
        this._eventEditBtn.Name = "_eventEditBtn";
        this._eventEditBtn.Size = new System.Drawing.Size(115, 27);
        this._eventEditBtn.TabIndex = 0;
        this._eventEditBtn.Text = "���������";
        this._eventEditBtn.Click += new System.EventHandler(this.eventEditBtn_Click);
        // 
        // label4
        // 
        this.label4.AutoSize = true;
        this.label4.BackColor = System.Drawing.Color.LightBlue;
        this.label4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
        this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
        this.label4.Font = new System.Drawing.Font("Arial", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
        this.label4.ForeColor = System.Drawing.Color.Black;
        this.label4.Location = new System.Drawing.Point(1, 1);
        this.label4.Margin = new System.Windows.Forms.Padding(1);
        this.label4.Name = "label4";
        this.label4.Size = new System.Drawing.Size(260, 22);
        this.label4.TabIndex = 6;
        this.label4.Text = "�������";
        this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
        // 
        // nameDataGridViewTextBoxColumn1
        // 
        this.nameDataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
        this.nameDataGridViewTextBoxColumn1.DataPropertyName = "Name";
        this.nameDataGridViewTextBoxColumn1.HeaderText = "�������� ����������� �������";
        this.nameDataGridViewTextBoxColumn1.MinimumWidth = 350;
        this.nameDataGridViewTextBoxColumn1.Name = "nameDataGridViewTextBoxColumn1";
        this.nameDataGridViewTextBoxColumn1.ReadOnly = true;
        this.nameDataGridViewTextBoxColumn1.ToolTipText = "�������� ����������� �������";
        this.nameDataGridViewTextBoxColumn1.Width = 350;
        // 
        // startBitDataGridViewTextBoxColumn
        // 
        this.startBitDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
        this.startBitDataGridViewTextBoxColumn.DataPropertyName = "StartBit";
        this.startBitDataGridViewTextBoxColumn.HeaderText = "��������� ���";
        this.startBitDataGridViewTextBoxColumn.MinimumWidth = 105;
        this.startBitDataGridViewTextBoxColumn.Name = "startBitDataGridViewTextBoxColumn";
        this.startBitDataGridViewTextBoxColumn.ReadOnly = true;
        this.startBitDataGridViewTextBoxColumn.ToolTipText = "��������� ���";
        this.startBitDataGridViewTextBoxColumn.Width = 105;
        // 
        // mobitelsBindingSource
        // 
        this.mobitelsBindingSource.DataMember = "mobitels";
        this.mobitelsBindingSource.DataSource = typeof(LocalCache.atlantaDataSet);
        // 
        // LogicSensorsSettingsPanel
        // 
        this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
        this.Controls.Add(this.splitContainer1);
        this.Margin = new System.Windows.Forms.Padding(0);
        this.Name = "LogicSensorsSettingsPanel";
        this.Padding = new System.Windows.Forms.Padding(2);
        this.Size = new System.Drawing.Size(818, 474);
        this.Load += new System.EventHandler(this.this_onLoad);
        this.splitContainer1.Panel1.ResumeLayout(false);
        this.splitContainer1.Panel2.ResumeLayout(false);
        this.splitContainer1.ResumeLayout(false);
        this.tableLayoutPanel1.ResumeLayout(false);
        this.tableLayoutPanel1.PerformLayout();
        ((System.ComponentModel.ISupportInitialize)(this.mobitelsGrid)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.vehicleBindingSource)).EndInit();
        this.ls_LayoutPanel.ResumeLayout(false);
        this.ls_LayoutPanel.PerformLayout();
        ((System.ComponentModel.ISupportInitialize)(this.sensorsGrid)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.mobitelsSensorsBindingSource)).EndInit();
        this.sensorsHead.ResumeLayout(false);
        this.sensorsHeadTablePanel.ResumeLayout(false);
        this.sensorsHeadTablePanel.PerformLayout();
        this.editPanel.ResumeLayout(false);
        this.sensorInfoPanel.ResumeLayout(false);
        this.statePanel.ResumeLayout(false);
        this.stateTable.ResumeLayout(false);
        this.stateTable.PerformLayout();
        ((System.ComponentModel.ISupportInitialize)(this.sensorStateFalseBindingSource)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.sensorStateTrueBindingSource)).EndInit();
        this.tableForMainBtn.ResumeLayout(false);
        this.transitionPanel.ResumeLayout(false);
        this.transitionTable.ResumeLayout(false);
        this.transitionTable.PerformLayout();
        this.tableForEventBtn.ResumeLayout(false);
        ((System.ComponentModel.ISupportInitialize)(this.mobitelsBindingSource)).EndInit();
        this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.SplitContainer splitContainer1;
    private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
    private System.Windows.Forms.DataGridView mobitelsGrid;
    private LocalCache.atlantaDataSet atlantaDataSet;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.TableLayoutPanel ls_LayoutPanel;
    private System.Windows.Forms.Label sensorHeadLbl;
    private System.Windows.Forms.DataGridView sensorsGrid;
    private System.Windows.Forms.BindingSource mobitelsSensorsBindingSource;
    private System.Windows.Forms.BindingSource mobitelsBindingSource;
    private System.Windows.Forms.Panel editPanel;
    private System.Windows.Forms.TableLayoutPanel sensorInfoPanel;
    private System.Windows.Forms.Panel sensorsHead;
    private System.Windows.Forms.TableLayoutPanel sensorsHeadTablePanel;
    private System.Windows.Forms.Label removeBtn;
    private System.Windows.Forms.Label addBtn;
    private System.Windows.Forms.Label editBtn;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.TableLayoutPanel stateTable;
    private System.Windows.Forms.Label stateFalseLbl;
    private System.Windows.Forms.TextBox stateFalseBox;
    private System.Windows.Forms.TextBox stateTrueBox;
    private System.Windows.Forms.Label stateTrueLbl;
    private System.Windows.Forms.TableLayoutPanel tableForMainBtn;
    private System.Windows.Forms.BindingSource sensorStateFalseBindingSource;
    private System.Windows.Forms.BindingSource sensorStateTrueBindingSource;
    private System.Windows.Forms.Panel statePanel;
    private System.Windows.Forms.Panel transitionPanel;
    private System.Windows.Forms.TableLayoutPanel transitionTable;
    private System.Windows.Forms.TableLayoutPanel tableForEventBtn;
    private System.Windows.Forms.Label stateHeadLbl;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.DataGridViewTextBoxColumn id;
    private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn1;
    private System.Windows.Forms.DataGridViewTextBoxColumn startBitDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn SpecSettingColumn;
    private System.Windows.Forms.BindingSource vehicleBindingSource;
    private System.Windows.Forms.DataGridViewTextBoxColumn mobitel;
    private System.Windows.Forms.DataGridViewTextBoxColumn ColumnNumber;
    private System.Windows.Forms.DataGridViewTextBoxColumn ColumnMarka;
    private System.Windows.Forms.DataGridViewTextBoxColumn ColumnModel;
    private DevExpress.XtraEditors.SimpleButton _editSensorBtn;
    private DevExpress.XtraEditors.SimpleButton _eventEditBtn;
  }
}
