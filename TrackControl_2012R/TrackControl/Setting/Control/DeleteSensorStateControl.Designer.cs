namespace TrackControl.Setting.Controls
{
  partial class DeleteSensorStateControl
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DeleteSensorStateControl));
            this.mainTable = new System.Windows.Forms.TableLayoutPanel();
            this.infoTable = new System.Windows.Forms.TableLayoutPanel();
            this.stateBox = new System.Windows.Forms.TextBox();
            this.minBox = new System.Windows.Forms.TextBox();
            this.maxBox = new System.Windows.Forms.TextBox();
            this._deleteBtn = new DevExpress.XtraEditors.SimpleButton();
            this._cancelBtn = new DevExpress.XtraEditors.SimpleButton();
            this.askingLbl = new DevExpress.XtraEditors.LabelControl();
            this.warningLbl = new DevExpress.XtraEditors.LabelControl();
            this.notifyLbl = new DevExpress.XtraEditors.LabelControl();
            this.mainTable.SuspendLayout();
            this.infoTable.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainTable
            // 
            this.mainTable.ColumnCount = 8;
            this.mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 130F));
            this.mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.mainTable.Controls.Add(this.infoTable, 1, 1);
            this.mainTable.Controls.Add(this._deleteBtn, 5, 3);
            this.mainTable.Controls.Add(this._cancelBtn, 6, 3);
            this.mainTable.Controls.Add(this.askingLbl, 1, 0);
            this.mainTable.Controls.Add(this.warningLbl, 1, 2);
            this.mainTable.Controls.Add(this.notifyLbl, 5, 2);
            this.mainTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainTable.Location = new System.Drawing.Point(0, 0);
            this.mainTable.Margin = new System.Windows.Forms.Padding(0);
            this.mainTable.Name = "mainTable";
            this.mainTable.RowCount = 5;
            this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.mainTable.Size = new System.Drawing.Size(430, 100);
            this.mainTable.TabIndex = 0;
            // 
            // infoTable
            // 
            this.infoTable.ColumnCount = 3;
            this.mainTable.SetColumnSpan(this.infoTable, 6);
            this.infoTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.infoTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.infoTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.infoTable.Controls.Add(this.stateBox, 0, 0);
            this.infoTable.Controls.Add(this.minBox, 1, 0);
            this.infoTable.Controls.Add(this.maxBox, 2, 0);
            this.infoTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.infoTable.Location = new System.Drawing.Point(5, 20);
            this.infoTable.Margin = new System.Windows.Forms.Padding(0);
            this.infoTable.Name = "infoTable";
            this.infoTable.RowCount = 1;
            this.infoTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.infoTable.Size = new System.Drawing.Size(420, 22);
            this.infoTable.TabIndex = 6;
            // 
            // stateBox
            // 
            this.stateBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(247)))), ((int)(((byte)(247)))));
            this.stateBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.stateBox.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.stateBox.ForeColor = System.Drawing.Color.Black;
            this.stateBox.Location = new System.Drawing.Point(0, 0);
            this.stateBox.Margin = new System.Windows.Forms.Padding(0);
            this.stateBox.Name = "stateBox";
            this.stateBox.ReadOnly = true;
            this.stateBox.Size = new System.Drawing.Size(260, 21);
            this.stateBox.TabIndex = 4;
            // 
            // minBox
            // 
            this.minBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(247)))), ((int)(((byte)(247)))));
            this.minBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.minBox.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.minBox.ForeColor = System.Drawing.Color.Black;
            this.minBox.Location = new System.Drawing.Point(260, 0);
            this.minBox.Margin = new System.Windows.Forms.Padding(0);
            this.minBox.Name = "minBox";
            this.minBox.ReadOnly = true;
            this.minBox.Size = new System.Drawing.Size(80, 21);
            this.minBox.TabIndex = 5;
            this.minBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // maxBox
            // 
            this.maxBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(247)))), ((int)(((byte)(247)))));
            this.maxBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.maxBox.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.maxBox.ForeColor = System.Drawing.Color.Black;
            this.maxBox.Location = new System.Drawing.Point(340, 0);
            this.maxBox.Margin = new System.Windows.Forms.Padding(0);
            this.maxBox.Name = "maxBox";
            this.maxBox.ReadOnly = true;
            this.maxBox.Size = new System.Drawing.Size(80, 21);
            this.maxBox.TabIndex = 6;
            this.maxBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // _deleteBtn
            // 
            this._deleteBtn.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this._deleteBtn.Appearance.Options.UseFont = true;
            this._deleteBtn.Dock = System.Windows.Forms.DockStyle.Fill;
            this._deleteBtn.Image = ((System.Drawing.Image)(resources.GetObject("_deleteBtn.Image")));
            this._deleteBtn.Location = new System.Drawing.Point(258, 65);
            this._deleteBtn.Name = "_deleteBtn";
            this._deleteBtn.Size = new System.Drawing.Size(124, 27);
            this._deleteBtn.TabIndex = 7;
            this._deleteBtn.Text = "�������";
            this._deleteBtn.Click += new System.EventHandler(this.deleteBtn_Click);
            this._deleteBtn.MouseEnter += new System.EventHandler(this.deleteBtn_MouseEnter);
            this._deleteBtn.MouseLeave += new System.EventHandler(this.btn_MouseLeave);
            // 
            // _cancelBtn
            // 
            this._cancelBtn.Dock = System.Windows.Forms.DockStyle.Fill;
            this._cancelBtn.Image = ((System.Drawing.Image)(resources.GetObject("_cancelBtn.Image")));
            this._cancelBtn.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this._cancelBtn.Location = new System.Drawing.Point(388, 65);
            this._cancelBtn.Name = "_cancelBtn";
            this._cancelBtn.Size = new System.Drawing.Size(34, 27);
            this._cancelBtn.TabIndex = 8;
            this._cancelBtn.Click += new System.EventHandler(this.cancelBtn_Click);
            this._cancelBtn.MouseEnter += new System.EventHandler(this.cancelBtn_MouseEnter);
            this._cancelBtn.MouseLeave += new System.EventHandler(this.btn_MouseLeave);
            // 
            // askingLbl
            // 
            this.askingLbl.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.askingLbl.Appearance.ForeColor = System.Drawing.Color.Firebrick;
            this.mainTable.SetColumnSpan(this.askingLbl, 3);
            this.askingLbl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.askingLbl.Location = new System.Drawing.Point(8, 3);
            this.askingLbl.Name = "askingLbl";
            this.askingLbl.Size = new System.Drawing.Size(306, 15);
            this.askingLbl.TabIndex = 9;
            this.askingLbl.Text = "�� �������, ��� ������ ������� ������ ���������?";
            // 
            // warningLbl
            // 
            this.warningLbl.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold);
            this.warningLbl.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.warningLbl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.warningLbl.Location = new System.Drawing.Point(8, 45);
            this.warningLbl.Name = "warningLbl";
            this.warningLbl.Size = new System.Drawing.Size(212, 45);
            this.warningLbl.TabIndex = 10;
            this.warningLbl.Text = "����� ����� ������� ��� �������, \r\n� ������� ������������ ��������� \r\n���������.";
            // 
            // notifyLbl
            // 
            this.notifyLbl.Appearance.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
            this.notifyLbl.Location = new System.Drawing.Point(258, 45);
            this.notifyLbl.Name = "notifyLbl";
            this.notifyLbl.Size = new System.Drawing.Size(3, 14);
            this.notifyLbl.TabIndex = 11;
            this.notifyLbl.Text = " ";
            // 
            // DeleteSensorStateControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.mainTable);
            this.Name = "DeleteSensorStateControl";
            this.Size = new System.Drawing.Size(430, 100);
            this.Load += new System.EventHandler(this.DeleteSensorStateControl_Load);
            this.mainTable.ResumeLayout(false);
            this.mainTable.PerformLayout();
            this.infoTable.ResumeLayout(false);
            this.infoTable.PerformLayout();
            this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.TableLayoutPanel mainTable;
    private System.Windows.Forms.TextBox stateBox;
    private System.Windows.Forms.TableLayoutPanel infoTable;
    private System.Windows.Forms.TextBox minBox;
    private System.Windows.Forms.TextBox maxBox;
    private DevExpress.XtraEditors.SimpleButton _deleteBtn;
    private DevExpress.XtraEditors.SimpleButton _cancelBtn;
    private DevExpress.XtraEditors.LabelControl askingLbl;
    private DevExpress.XtraEditors.LabelControl warningLbl;
    private DevExpress.XtraEditors.LabelControl notifyLbl;
  }
}
