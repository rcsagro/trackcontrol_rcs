namespace TrackControl.Setting.Control
{
  partial class LanSettings
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LanSettings));
            this._groupPanel = new DevExpress.XtraEditors.GroupControl();
            this.settingTable = new System.Windows.Forms.TableLayoutPanel();
            this.nopeProxyRadio = new System.Windows.Forms.RadioButton();
            this.defaultProxyRadio = new System.Windows.Forms.RadioButton();
            this.customProxyRadio = new System.Windows.Forms.RadioButton();
            this.proxyTable = new System.Windows.Forms.TableLayoutPanel();
            this.portBox = new System.Windows.Forms.TextBox();
            this.portLbl = new System.Windows.Forms.Label();
            this.proxyLbl = new System.Windows.Forms.Label();
            this.proxyBox = new System.Windows.Forms.TextBox();
            this.buttonTable = new System.Windows.Forms.TableLayoutPanel();
            this._saveBtn = new DevExpress.XtraEditors.SimpleButton();
            this._cancelBtn = new DevExpress.XtraEditors.SimpleButton();
            this.tableLogin = new System.Windows.Forms.TableLayoutPanel();
            this.txPassword = new System.Windows.Forms.TextBox();
            this.txLogin = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.welcomeTable = new System.Windows.Forms.TableLayoutPanel();
            this.iconLbl = new System.Windows.Forms.Label();
            this.descriptionLbl = new System.Windows.Forms.Label();
            this._editBtn = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this._groupPanel)).BeginInit();
            this._groupPanel.SuspendLayout();
            this.settingTable.SuspendLayout();
            this.proxyTable.SuspendLayout();
            this.buttonTable.SuspendLayout();
            this.tableLogin.SuspendLayout();
            this.welcomeTable.SuspendLayout();
            this.SuspendLayout();
            // 
            // _groupPanel
            // 
            this._groupPanel.Controls.Add(this.settingTable);
            this._groupPanel.Controls.Add(this.welcomeTable);
            this._groupPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this._groupPanel.Location = new System.Drawing.Point(0, 0);
            this._groupPanel.Name = "_groupPanel";
            this._groupPanel.Size = new System.Drawing.Size(347, 357);
            this._groupPanel.TabIndex = 0;
            this._groupPanel.Text = "��������� ����";
            // 
            // settingTable
            // 
            this.settingTable.ColumnCount = 3;
            this.settingTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.settingTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.settingTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.settingTable.Controls.Add(this.nopeProxyRadio, 1, 1);
            this.settingTable.Controls.Add(this.defaultProxyRadio, 1, 2);
            this.settingTable.Controls.Add(this.customProxyRadio, 1, 3);
            this.settingTable.Controls.Add(this.proxyTable, 1, 5);
            this.settingTable.Controls.Add(this.buttonTable, 1, 7);
            this.settingTable.Controls.Add(this.tableLogin, 1, 6);
            this.settingTable.Location = new System.Drawing.Point(6, 182);
            this.settingTable.Name = "settingTable";
            this.settingTable.RowCount = 9;
            this.settingTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 5F));
            this.settingTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.settingTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.settingTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.settingTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 5F));
            this.settingTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.settingTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.settingTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.settingTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.settingTable.Size = new System.Drawing.Size(336, 170);
            this.settingTable.TabIndex = 1;
            // 
            // nopeProxyRadio
            // 
            this.nopeProxyRadio.AutoSize = true;
            this.nopeProxyRadio.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nopeProxyRadio.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.nopeProxyRadio.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.nopeProxyRadio.Location = new System.Drawing.Point(10, 5);
            this.nopeProxyRadio.Margin = new System.Windows.Forms.Padding(0);
            this.nopeProxyRadio.Name = "nopeProxyRadio";
            this.nopeProxyRadio.Size = new System.Drawing.Size(316, 20);
            this.nopeProxyRadio.TabIndex = 0;
            this.nopeProxyRadio.TabStop = true;
            this.nopeProxyRadio.Text = "�� ������������ ������";
            this.nopeProxyRadio.UseVisualStyleBackColor = true;
            // 
            // defaultProxyRadio
            // 
            this.defaultProxyRadio.AutoSize = true;
            this.defaultProxyRadio.Dock = System.Windows.Forms.DockStyle.Fill;
            this.defaultProxyRadio.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.defaultProxyRadio.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.defaultProxyRadio.Location = new System.Drawing.Point(10, 25);
            this.defaultProxyRadio.Margin = new System.Windows.Forms.Padding(0);
            this.defaultProxyRadio.Name = "defaultProxyRadio";
            this.defaultProxyRadio.Size = new System.Drawing.Size(316, 20);
            this.defaultProxyRadio.TabIndex = 1;
            this.defaultProxyRadio.TabStop = true;
            this.defaultProxyRadio.Text = "�������������� ����������� ��� ������ ����";
            this.defaultProxyRadio.UseVisualStyleBackColor = true;
            // 
            // customProxyRadio
            // 
            this.customProxyRadio.AutoSize = true;
            this.customProxyRadio.Dock = System.Windows.Forms.DockStyle.Fill;
            this.customProxyRadio.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.customProxyRadio.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.customProxyRadio.Location = new System.Drawing.Point(10, 45);
            this.customProxyRadio.Margin = new System.Windows.Forms.Padding(0);
            this.customProxyRadio.Name = "customProxyRadio";
            this.customProxyRadio.Size = new System.Drawing.Size(316, 20);
            this.customProxyRadio.TabIndex = 2;
            this.customProxyRadio.TabStop = true;
            this.customProxyRadio.Text = "������ ��������� ������� ������";
            this.customProxyRadio.UseVisualStyleBackColor = true;
            this.customProxyRadio.CheckedChanged += new System.EventHandler(this.customProxyRadio_CheckedChanged);
            // 
            // proxyTable
            // 
            this.proxyTable.ColumnCount = 6;
            this.proxyTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 13F));
            this.proxyTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 77F));
            this.proxyTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 140F));
            this.proxyTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.proxyTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.proxyTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.proxyTable.Controls.Add(this.portBox, 5, 0);
            this.proxyTable.Controls.Add(this.portLbl, 4, 0);
            this.proxyTable.Controls.Add(this.proxyLbl, 1, 0);
            this.proxyTable.Controls.Add(this.proxyBox, 2, 0);
            this.proxyTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.proxyTable.Location = new System.Drawing.Point(10, 70);
            this.proxyTable.Margin = new System.Windows.Forms.Padding(0);
            this.proxyTable.Name = "proxyTable";
            this.proxyTable.RowCount = 1;
            this.proxyTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.proxyTable.Size = new System.Drawing.Size(316, 20);
            this.proxyTable.TabIndex = 3;
            // 
            // portBox
            // 
            this.portBox.BackColor = System.Drawing.Color.White;
            this.portBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.portBox.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.portBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.portBox.Location = new System.Drawing.Point(276, 0);
            this.portBox.Margin = new System.Windows.Forms.Padding(0);
            this.portBox.MaxLength = 5;
            this.portBox.Name = "portBox";
            this.portBox.Size = new System.Drawing.Size(40, 20);
            this.portBox.TabIndex = 0;
            this.portBox.Text = "80805";
            this.portBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.portBox_KeyPress);
            // 
            // portLbl
            // 
            this.portLbl.AutoSize = true;
            this.portLbl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.portLbl.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.portLbl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.portLbl.Location = new System.Drawing.Point(236, 0);
            this.portLbl.Margin = new System.Windows.Forms.Padding(0);
            this.portLbl.Name = "portLbl";
            this.portLbl.Padding = new System.Windows.Forms.Padding(0, 0, 2, 0);
            this.portLbl.Size = new System.Drawing.Size(40, 20);
            this.portLbl.TabIndex = 1;
            this.portLbl.Text = "����";
            this.portLbl.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // proxyLbl
            // 
            this.proxyLbl.AutoSize = true;
            this.proxyLbl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.proxyLbl.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.proxyLbl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.proxyLbl.Location = new System.Drawing.Point(13, 0);
            this.proxyLbl.Margin = new System.Windows.Forms.Padding(0);
            this.proxyLbl.Name = "proxyLbl";
            this.proxyLbl.Padding = new System.Windows.Forms.Padding(0, 0, 2, 0);
            this.proxyLbl.Size = new System.Drawing.Size(77, 20);
            this.proxyLbl.TabIndex = 2;
            this.proxyLbl.Text = "HTTP ������";
            this.proxyLbl.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // proxyBox
            // 
            this.proxyBox.BackColor = System.Drawing.Color.White;
            this.proxyBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.proxyBox.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.proxyBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.proxyBox.Location = new System.Drawing.Point(90, 0);
            this.proxyBox.Margin = new System.Windows.Forms.Padding(0);
            this.proxyBox.Name = "proxyBox";
            this.proxyBox.Size = new System.Drawing.Size(140, 20);
            this.proxyBox.TabIndex = 3;
            this.proxyBox.Text = "255.255.255.255";
            this.proxyBox.Enter += new System.EventHandler(this.proxyBox_Enter);
            // 
            // buttonTable
            // 
            this.buttonTable.ColumnCount = 4;
            this.buttonTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.buttonTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 130F));
            this.buttonTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.buttonTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.buttonTable.Controls.Add(this._saveBtn, 1, 0);
            this.buttonTable.Controls.Add(this._cancelBtn, 3, 0);
            this.buttonTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonTable.Location = new System.Drawing.Point(10, 120);
            this.buttonTable.Margin = new System.Windows.Forms.Padding(0);
            this.buttonTable.Name = "buttonTable";
            this.buttonTable.RowCount = 1;
            this.buttonTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.buttonTable.Size = new System.Drawing.Size(316, 30);
            this.buttonTable.TabIndex = 4;
            // 
            // _saveBtn
            // 
            this._saveBtn.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._saveBtn.Appearance.Options.UseFont = true;
            this._saveBtn.Dock = System.Windows.Forms.DockStyle.Fill;
            this._saveBtn.Image = ((System.Drawing.Image)(resources.GetObject("_saveBtn.Image")));
            this._saveBtn.Location = new System.Drawing.Point(139, 3);
            this._saveBtn.Name = "_saveBtn";
            this._saveBtn.Size = new System.Drawing.Size(124, 24);
            this._saveBtn.TabIndex = 0;
            this._saveBtn.Text = "���������";
            this._saveBtn.ToolTip = "��������� ���������";
            this._saveBtn.Click += new System.EventHandler(this.saveBtn_Click);
            // 
            // _cancelBtn
            // 
            this._cancelBtn.AllowHtmlTextInToolTip = DevExpress.Utils.DefaultBoolean.True;
            this._cancelBtn.Dock = System.Windows.Forms.DockStyle.Fill;
            this._cancelBtn.Image = ((System.Drawing.Image)(resources.GetObject("_cancelBtn.Image")));
            this._cancelBtn.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this._cancelBtn.Location = new System.Drawing.Point(279, 3);
            this._cancelBtn.Name = "_cancelBtn";
            this._cancelBtn.Size = new System.Drawing.Size(34, 24);
            this._cancelBtn.TabIndex = 1;
            this._cancelBtn.ToolTip = "�������� ���������";
            this._cancelBtn.Click += new System.EventHandler(this.cancelBtn_Click);
            // 
            // tableLogin
            // 
            this.tableLogin.ColumnCount = 4;
            this.tableLogin.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 31.63841F));
            this.tableLogin.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 68.36159F));
            this.tableLogin.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLogin.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 118F));
            this.tableLogin.Controls.Add(this.txPassword, 3, 0);
            this.tableLogin.Controls.Add(this.txLogin, 1, 0);
            this.tableLogin.Controls.Add(this.label2, 0, 0);
            this.tableLogin.Controls.Add(this.label1, 2, 0);
            this.tableLogin.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLogin.Location = new System.Drawing.Point(13, 93);
            this.tableLogin.Name = "tableLogin";
            this.tableLogin.RowCount = 1;
            this.tableLogin.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLogin.Size = new System.Drawing.Size(310, 24);
            this.tableLogin.TabIndex = 5;
            // 
            // txPassword
            // 
            this.txPassword.BackColor = System.Drawing.Color.White;
            this.txPassword.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txPassword.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txPassword.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txPassword.Location = new System.Drawing.Point(191, 0);
            this.txPassword.Margin = new System.Windows.Forms.Padding(0);
            this.txPassword.Name = "txPassword";
            this.txPassword.PasswordChar = '*';
            this.txPassword.Size = new System.Drawing.Size(119, 20);
            this.txPassword.TabIndex = 7;
            // 
            // txLogin
            // 
            this.txLogin.AcceptsTab = true;
            this.txLogin.BackColor = System.Drawing.Color.White;
            this.txLogin.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txLogin.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txLogin.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txLogin.Location = new System.Drawing.Point(41, 0);
            this.txLogin.Margin = new System.Windows.Forms.Padding(0);
            this.txLogin.Name = "txLogin";
            this.txLogin.Size = new System.Drawing.Size(90, 20);
            this.txLogin.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Margin = new System.Windows.Forms.Padding(0);
            this.label2.Name = "label2";
            this.label2.Padding = new System.Windows.Forms.Padding(0, 0, 2, 0);
            this.label2.Size = new System.Drawing.Size(41, 24);
            this.label2.TabIndex = 4;
            this.label2.Text = "�����";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label1.Location = new System.Drawing.Point(131, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(0);
            this.label1.Name = "label1";
            this.label1.Padding = new System.Windows.Forms.Padding(0, 0, 2, 0);
            this.label1.Size = new System.Drawing.Size(60, 24);
            this.label1.TabIndex = 3;
            this.label1.Text = "������";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // welcomeTable
            // 
            this.welcomeTable.ColumnCount = 5;
            this.welcomeTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.welcomeTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.welcomeTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 141F));
            this.welcomeTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.welcomeTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 21F));
            this.welcomeTable.Controls.Add(this.iconLbl, 1, 1);
            this.welcomeTable.Controls.Add(this.descriptionLbl, 2, 1);
            this.welcomeTable.Controls.Add(this._editBtn, 2, 3);
            this.welcomeTable.Location = new System.Drawing.Point(6, 16);
            this.welcomeTable.Margin = new System.Windows.Forms.Padding(0);
            this.welcomeTable.Name = "welcomeTable";
            this.welcomeTable.RowCount = 5;
            this.welcomeTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.welcomeTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 64F));
            this.welcomeTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.welcomeTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.welcomeTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.welcomeTable.Size = new System.Drawing.Size(336, 163);
            this.welcomeTable.TabIndex = 0;
            // 
            // iconLbl
            // 
            this.iconLbl.AutoSize = true;
            this.iconLbl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.iconLbl.Image = ((System.Drawing.Image)(resources.GetObject("iconLbl.Image")));
            this.iconLbl.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.iconLbl.Location = new System.Drawing.Point(20, 15);
            this.iconLbl.Margin = new System.Windows.Forms.Padding(0);
            this.iconLbl.Name = "iconLbl";
            this.iconLbl.Size = new System.Drawing.Size(77, 64);
            this.iconLbl.TabIndex = 1;
            // 
            // descriptionLbl
            // 
            this.descriptionLbl.AutoSize = true;
            this.welcomeTable.SetColumnSpan(this.descriptionLbl, 2);
            this.descriptionLbl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.descriptionLbl.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.descriptionLbl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.descriptionLbl.Location = new System.Drawing.Point(97, 15);
            this.descriptionLbl.Margin = new System.Windows.Forms.Padding(0);
            this.descriptionLbl.Name = "descriptionLbl";
            this.descriptionLbl.Size = new System.Drawing.Size(218, 64);
            this.descriptionLbl.TabIndex = 2;
            this.descriptionLbl.Text = "��������� ����������. ���������� ��� ���������� ������ ����� Google � ������ ����" +
    "��.";
            this.descriptionLbl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // _editBtn
            // 
            this._editBtn.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._editBtn.Appearance.Options.UseFont = true;
            this._editBtn.Dock = System.Windows.Forms.DockStyle.Fill;
            this._editBtn.Image = ((System.Drawing.Image)(resources.GetObject("_editBtn.Image")));
            this._editBtn.Location = new System.Drawing.Point(97, 110);
            this._editBtn.Margin = new System.Windows.Forms.Padding(0);
            this._editBtn.Name = "_editBtn";
            this._editBtn.Size = new System.Drawing.Size(141, 33);
            this._editBtn.TabIndex = 3;
            this._editBtn.Text = "���������";
            this._editBtn.Click += new System.EventHandler(this.editBtn_Click);
            // 
            // LanSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._groupPanel);
            this.Name = "LanSettings";
            this.Size = new System.Drawing.Size(347, 357);
            this.Load += new System.EventHandler(this.LanSettings_Load);
            ((System.ComponentModel.ISupportInitialize)(this._groupPanel)).EndInit();
            this._groupPanel.ResumeLayout(false);
            this.settingTable.ResumeLayout(false);
            this.settingTable.PerformLayout();
            this.proxyTable.ResumeLayout(false);
            this.proxyTable.PerformLayout();
            this.buttonTable.ResumeLayout(false);
            this.tableLogin.ResumeLayout(false);
            this.tableLogin.PerformLayout();
            this.welcomeTable.ResumeLayout(false);
            this.welcomeTable.PerformLayout();
            this.ResumeLayout(false);

    }

    #endregion

    private DevExpress.XtraEditors.GroupControl _groupPanel;
    private System.Windows.Forms.TableLayoutPanel welcomeTable;
    private System.Windows.Forms.Label iconLbl;
    private System.Windows.Forms.Label descriptionLbl;
    private System.Windows.Forms.TableLayoutPanel settingTable;
    private System.Windows.Forms.RadioButton nopeProxyRadio;
    private System.Windows.Forms.RadioButton defaultProxyRadio;
    private System.Windows.Forms.RadioButton customProxyRadio;
    private System.Windows.Forms.TableLayoutPanel proxyTable;
    private System.Windows.Forms.TextBox portBox;
    private System.Windows.Forms.Label portLbl;
    private System.Windows.Forms.Label proxyLbl;
    private System.Windows.Forms.TableLayoutPanel buttonTable;
    private DevExpress.XtraEditors.SimpleButton _editBtn;
    private DevExpress.XtraEditors.SimpleButton _saveBtn;
    private DevExpress.XtraEditors.SimpleButton _cancelBtn;
    private System.Windows.Forms.TableLayoutPanel tableLogin;
    private System.Windows.Forms.TextBox proxyBox;
    private System.Windows.Forms.TextBox txPassword;
    private System.Windows.Forms.TextBox txLogin;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label label1;
  }
}
