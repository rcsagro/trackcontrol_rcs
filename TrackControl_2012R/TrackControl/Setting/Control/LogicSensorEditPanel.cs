using System;
using System.Text;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.ComponentModel;
using System.Windows.Forms.VisualStyles;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using LocalCache;
using TrackControl.Reports;
using TrackControl.Properties;

namespace TrackControl.Setting.Controls
{
    [ToolboxItem(false)]
    public partial class LogicSensorEditPanel : UserControl
    {
        #region Fields

        /// <summary>
        /// ������� ��� ���������� ��������� � �� �������� ����������� �������.
        /// </summary>
        private LocalCache.atlantaDataSetTableAdapters.SensorsTableAdapter _sensorAdapter;

        /// <summary>
        /// ������� ��� ���������� ��������� � �� �������� ���������, �������������
        /// �� ���������� �������.
        /// </summary>
        private LocalCache.atlantaDataSetTableAdapters.StateTableAdapter _stateAdapter;

        /// <summary>
        /// ������� ��� ���������� ��������� � ������� relationalgorithms
        /// </summary>
        private LocalCache.atlantaDataSetTableAdapters.RelationalgorithmsTableAdapter _relAlgAdapter;

        /// <summary>
        /// ��������� ����������� �������
        /// </summary>
        private atlantaDataSet.sensorsRow _sensor;

        /// <summary>
        /// ��������� ��� ���������� ������� "0".
        /// </summary>
        private atlantaDataSet.StateRow _falseState;

        /// <summary>
        /// ��������� ��� ���������� ������� "1".
        /// </summary>
        private atlantaDataSet.StateRow _trueState;

        /// <summary>
        /// ������� ����������
        /// </summary>
        private atlantaDataSet _dataset;

        #endregion

        /// <summary>
        /// ��������� ����� ��������� �������������� ��������� ����������� �������.
        /// �.�. ����� ������� ������ "���������" ���� "������".
        /// </summary>
        public event EventHandler<EndEditEventArgs> EndEdit;

        /// <summary>
        /// �����������
        /// </summary>
        public LogicSensorEditPanel(
            LocalCache.atlantaDataSetTableAdapters.SensorsTableAdapter sensorAdapter,
            LocalCache.atlantaDataSetTableAdapters.StateTableAdapter stateAdapter,
            LocalCache.atlantaDataSetTableAdapters.RelationalgorithmsTableAdapter relAlgAdapter,
            LocalCache.atlantaDataSet dataset)
        {
            InitializeComponent();
            initialize();

            _sensorAdapter = sensorAdapter;
            _stateAdapter = stateAdapter;
            _relAlgAdapter = relAlgAdapter;
            _dataset = dataset;

            specSettingBox.CloseUp += specSettingBox_CloseUp;
            _saveBtn.MouseHover +=_saveBtn_MouseHover;
            _saveBtn.MouseLeave +=_saveBtn_MouseLeave;
            _cancelBtn.MouseHover +=_cancelBtn_MouseHover;
            _cancelBtn.MouseLeave +=_cancelBtn_MouseLeave;

            // ���������� ���������� � �����������
            specSettingBox.Properties.DataSource = _dataset.sensoralgorithms;
            specSettingBox.Properties.ValueMember = _dataset.sensoralgorithms.AlgorithmIDColumn.ColumnName;
            specSettingBox.Properties.DisplayMember = _dataset.sensoralgorithms.NameColumn.ColumnName;
            LookUpColumnInfoCollection coll = specSettingBox.Properties.Columns;
            coll.Add( new LookUpColumnInfo( "Name", 0 ) );
        }

        private void _cancelBtn_MouseLeave(object sender, EventArgs e)
        {
            labelInfo.Text = "";
        }

        private void _saveBtn_MouseLeave(object sender, EventArgs e)
        {
            labelMsg.Text = "";
        }

        private void _cancelBtn_MouseHover(object sender, EventArgs e)
        {
            labelInfo.Text = Resources.CancelBtn;
        }

        private void _saveBtn_MouseHover(object sender, EventArgs e)
        {
            labelMsg.Text = Resources.SaveBtn;
        }

        /// <summary>
        /// ����������� ��������� ����������� �������
        /// </summary>
        /// <param name="sensor">������ �� ������� �������� � ��������. �� ����, ���������� ������</param>
        public void EditSensor(atlantaDataSet.sensorsRow sensor)
        {
            init(sensor);

            nameBox.Text = _sensor.Name;
            startBitBox.Value = (decimal) _sensor.StartBit;

            stateFalseBox.Text = (_falseState != null) ? _falseState.Title : "";
            stateTrueBox.Text = (_trueState != null) ? _trueState.Title : "";

            InitSpecSettingControls();
            hideWarning();
        }

        private void initialize()
        {
            nameLbl.Text = Resources.SensorName;
            stateFalseLbl.Text = Resources.StateZero;
            stateTrueLbl.Text = Resources.StateOne;
            errorStatusLbl.Text = Resources.Err_MustBeFilled;
            startBitLbl.Text = Resources.StartBit;
            specSettingLbl.Text = Resources.SpecialAssignment;
            specSettingState.Properties.Items[1].Description = Resources.StateOne;
            specSettingStateLbl.Text = Resources.DoorIsOpened;
            specSettingState.Properties.Items[0].Description = Resources.StateZero;
            _saveBtn.Text = Resources.Save;
            swapBtn.ToolTip = Resources.SwapingBtn;
        }

        /// <summary>
        /// ������������� ��������� ����� ����. ����������
        /// </summary>
        private void InitSpecSettingControls()
        {
            atlantaDataSet.relationalgorithmsRow[] relAlgRows = _sensor.GetrelationalgorithmsRows();

            if (relAlgRows.Length > 0)
            {
                try
                {
                    SetSpecSettingState();

                    specSettingBox.EditValue = relAlgRows[0].AlgorithmID;

                    specSettingCheckBox.Checked = true;
                }
                catch (Exception ex)
                {
                    DataTable table = (DataTable)specSettingBox.Properties.DataSource;
                    if( table.Rows.Count == 0 )
                    {
                        throw ex;
                    }

                    specSettingBox.ItemIndex = 0;
                    specSettingBox.EditValue = 0;
                    // �������� �� �������� �� ��������� �����������
                    DataRow[] dr = _dataset.sensoralgorithms.Select(
                        String.Format("AlgorithmId = {0}", relAlgRows[0].AlgorithmID));
                    if (dr.Length == 0)
                    {
                        StringBuilder sb = new StringBuilder();
                        sb.AppendLine(Resources.LogicSensors_Err1);
                        sb.AppendFormat("{0}: ID={1} AlgorithmId={2} SensorID={3}", Resources.LogicSensors_Err2,
                            relAlgRows[0].ID, relAlgRows[0].AlgorithmID, relAlgRows[0].SensorID);
                        sb.AppendFormat("{0}{0}", Environment.NewLine, Environment.NewLine);
                        sb.AppendLine(Resources.LogicSensors_Err3);
                        sb.AppendLine(Resources.LogicSensors_Err4);
                        XtraMessageBox.Show(sb.ToString(), "LogicSensorEditPanel");
                    }
                    else
                    {
                        throw ex;
                    }
                }
            }
            else
            {
                specSettingCheckBox.Checked = false;
            }
        }

        #region GUI Event Handlers

        /// <summary>
        /// ������������ ������� ������ "������".
        /// </summary>
        private void cancelBtn_Click(object sender, EventArgs e)
        {
            onEndEdit(true);
        }

        /// <summary>
        /// ������������ ������� ������ "���������".
        /// </summary>
        private void saveBtn_Click(object sender, EventArgs e)
        {
            try
            {
                if (!validateSensorSettings())
                    return;

                SaveSensor();
                SaveStates();
                SaveSpecSetting();

                onEndEdit(false);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.StackTrace, ex.Message, MessageBoxButtons.OK);
            }
        }

        /// <summary>
        /// ������������ ��������� ������ ����� ��� ����� �������� �������.
        /// </summary>
        private void nameBox_Enter(object sender, EventArgs e)
        {
            hideWarning();
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// ���������� ���������� ������� 
        /// </summary>
        private void SaveSensor()
        {
            _sensor.Name = nameBox.Text;
            _sensor.StartBit = Convert.ToInt32(startBitBox.Value);

            _sensor.K = specSettingCheckBox.Checked ? ((bool)specSettingState.Properties.Items[specSettingState.SelectedIndex].Value ? 1 : 0) : 0;

            _sensorAdapter.Update(_dataset.sensors);
        }

        /// <summary>
        /// ���������� �������� ���������
        /// </summary>
        private void SaveStates()
        {
            if (_falseState != null)
            {
                _falseState.Title = stateFalseBox.Text;
            }
            else
            {
                atlantaDataSet.StateRow falseState = _dataset.State.NewStateRow();
                falseState.MinValue = 0.0;
                falseState.MaxValue = 0.1;
                falseState.Title = stateFalseBox.Text;
                falseState.sensorsRow = _sensor;
                _dataset.State.AddStateRow(falseState);
            }
            _stateAdapter.Update(_dataset.State);


            if (_trueState != null)
            {
                _trueState.Title = stateTrueBox.Text;
            }
            else
            {
                atlantaDataSet.StateRow trueState = _dataset.State.NewStateRow();
                trueState.MinValue = 1.0;
                trueState.MaxValue = 1.1;
                trueState.Title = stateTrueBox.Text;
                trueState.sensorsRow = _sensor;
                _dataset.State.AddStateRow(trueState);
            }
            _stateAdapter.Update(_dataset.State);
        }

        /// <summary>
        /// ���������� ��������� ������������ ����������
        /// </summary>
        private void SaveSpecSetting()
        {
            atlantaDataSet.relationalgorithmsRow[] relAlgRows =
                _sensor.GetrelationalgorithmsRows();

            if (specSettingCheckBox.Checked)
            {
                int currentAlgorithmID = Convert.ToInt32(specSettingBox.EditValue);
                if (relAlgRows.Length == 0)
                {
                    // ���� ������� ������� � ��� ������ � ������� relationalgorithms - 
                    // ���������� ������� ����� ������-��������.
                    atlantaDataSet.relationalgorithmsRow row =
                        _dataset.relationalgorithms.NewrelationalgorithmsRow();
                    row.SensorID = _sensor.id;
                    row.AlgorithmID = currentAlgorithmID;
                    _dataset.relationalgorithms.AddrelationalgorithmsRow(row);
                }
                else if (relAlgRows[0].AlgorithmID != currentAlgorithmID)
                {
                    relAlgRows[0].AlgorithmID = currentAlgorithmID;
                }
                relAlgRows = _sensor.GetrelationalgorithmsRows();
            }
            else
            {
                // ���� ������� ��� �����, � ����� ���� ��������� ����������
                // ��������� ��� ������� �������, ����� ���������� �������
                // ��� �����.
                foreach (atlantaDataSet.relationalgorithmsRow row in relAlgRows)
                {
                    row.Delete();
                }
               
            }
            //04.09.2014 Concurrency violation: the UpdateCommand affected 0 of the expected 1 records
            //_relAlgAdapter.Update(_dataset.relationalgorithms);
            _relAlgAdapter.Update(relAlgRows);
        }

        /// <summary>
        /// �������������� ��������� ��������.
        /// </summary>
        /// <param name="sensor">���������� ������, ��������� �������� ������������� ���������.</param>
        private void init(atlantaDataSet.sensorsRow sensor)
        {
            _sensor = sensor;
            foreach (atlantaDataSet.StateRow state in sensor.GetStateRows())
            {
                if (state.MinValue < 0.1)
                    _falseState = state;
                else if (state.MinValue >= 1)
                    _trueState = state;
            }
        }

        /// <summary>
        /// ���������� ����������� � ������������� ������� EndEdit
        /// </summary>
        /// <param name="canceled">����, ����������� ���� ������ ������ "������" ��� ������ "���������".</param>
        private void onEndEdit(bool canceled)
        {
            _sensor = null;
            _falseState = null;
            _trueState = null;

            if (EndEdit != null)
                EndEdit(this, new EndEditEventArgs(canceled));
        }

        /// <summary>
        /// ��������� ������������ ��������� ������������� ��������
        /// </summary>
        private bool validateSensorSettings()
        {
            bool result = true;

            string sensorName = nameBox.Text;
            sensorName = sensorName.Trim();
            if (sensorName.Length == 0)
            {
                result = false;
                showWarning();
            }
            return result;
        }

        /// <summary>
        /// ������������� ������������, ��� �������� ������� �����������.
        /// </summary>
        private void showWarning()
        {
            nameBox.BackColor = Color.PeachPuff;
            errorStatusLbl.Visible = true;
        }

        /// <summary>
        /// ������ ��������������.
        /// </summary>
        private void hideWarning()
        {
            nameBox.BackColor = Color.White;
            errorStatusLbl.Visible = false;
        }

        /// <summary>
        /// ������������� ������ ��������� � ����. ���������
        /// � ����������� �� �������� ���� K � ������ _sensor
        /// </summary>
        private void SetSpecSettingState()
        {
            if (_sensor.K == 0)
            {
                specSettingState.SelectedIndex = 0;
            }
            else
            {
                specSettingState.SelectedIndex = 1;
            }
        }

        #endregion

        /// <summary>
        /// ��������� ������� ��������� �������� ���������� ����������� ��������
        /// </summary>
        private void specSettingBox_CloseUp( object sender, EventArgs e )
        {
           DataRowView rowItem = (DataRowView) specSettingBox.Properties.GetDataSourceRowByKeyValue( specSettingBox.EditValue );

            if(rowItem == null)
                return;

           atlantaDataSet.sensoralgorithmsRow algoRow = ( atlantaDataSet.sensoralgorithmsRow )( rowItem ).Row;
           int algorithm = ( algoRow ).AlgorithmID;

            switch( algorithm )
            {
                case ( int )AlgorithmType.DOOR1:
                case ( int )AlgorithmType.DOOR2:
                    specSettingStateLbl.Text = String.Format( "{0}:", Resources.DoorOpened );
                    break;
                default:
                    specSettingStateLbl.Text = String.Format( "{0}:", Resources.ValuedValue );
                    break;
            }
        }

        private void ValidateSpecSettingUsage()
        {
            if (specSettingCheckBox.Checked)
            {
                if ((int)specSettingBox.ItemIndex < 0)
                {
                    specSettingBox.ItemIndex = 0;
                }
                SetSpecSettingState();
                specSettingStateLayoutPanel.Visible = true;
            }
            else
            {
                specSettingStateLayoutPanel.Visible = false;
            }
        }

        private void specSettingCheckBox_CheckedChanged_1( object sender, EventArgs e )
        {
            specSettingStateLayoutPanel.Visible = !specSettingStateLayoutPanel.Visible;
            specSettingStateInnerLayoutPanel.Visible = true;
            specSettingStateLbl.Visible = true;
            specSettingState.Visible = true;

            ValidateSpecSettingUsage();
        }

        private void swapBtn_Click_1( object sender, EventArgs e )
        {
            string tmp = stateFalseBox.Text;
            stateFalseBox.Text = stateTrueBox.Text;
            stateTrueBox.Text = tmp;
        }
    }

    /// <summary>
    /// �����, ��������������� ���������� � ���, ������� �� ������������
    /// ���������� ����������������� ��������.
    /// </summary>
    public class EndEditEventArgs : EventArgs
    {
        private bool _canceled = false;

        public EndEditEventArgs(bool canceled)
        {
            _canceled = canceled;
        }

        public bool Canceled
        {
            get { return _canceled; }
        }
    }
}
