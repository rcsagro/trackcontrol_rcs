namespace TrackControl.Setting.Control
{
  partial class PassChanger
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this._groupPanel = new DevExpress.XtraEditors.GroupControl();
      this.welcomeTable = new System.Windows.Forms.TableLayoutPanel();
      this.mainIconLbl = new System.Windows.Forms.Label();
      this.mainTextLbl = new System.Windows.Forms.Label();
      this._mainBtn = new DevExpress.XtraEditors.SimpleButton();
      this.mainTable = new System.Windows.Forms.TableLayoutPanel();
      this.oldPassLbl = new System.Windows.Forms.Label();
      this.newPassLbl = new System.Windows.Forms.Label();
      this.confirmPassLbl = new System.Windows.Forms.Label();
      this.oldPassBox = new System.Windows.Forms.TextBox();
      this.newPassBox = new System.Windows.Forms.TextBox();
      this.confirmPassBox = new System.Windows.Forms.TextBox();
      this.notificationTable = new System.Windows.Forms.TableLayoutPanel();
      this.notificationIcon = new System.Windows.Forms.Label();
      this.notificationLbl = new System.Windows.Forms.Label();
      this.btnTable = new System.Windows.Forms.TableLayoutPanel();
      this._saveBtn = new DevExpress.XtraEditors.SimpleButton();
      this._cancelBtn = new DevExpress.XtraEditors.SimpleButton();
      ((System.ComponentModel.ISupportInitialize)(this._groupPanel)).BeginInit();
      this._groupPanel.SuspendLayout();
      this.welcomeTable.SuspendLayout();
      this.mainTable.SuspendLayout();
      this.notificationTable.SuspendLayout();
      this.btnTable.SuspendLayout();
      this.SuspendLayout();
      // 
      // _groupPanel
      // 
      this._groupPanel.Controls.Add(this.welcomeTable);
      this._groupPanel.Controls.Add(this.mainTable);
      this._groupPanel.Dock = System.Windows.Forms.DockStyle.Fill;
      this._groupPanel.Location = new System.Drawing.Point(0, 0);
      this._groupPanel.Margin = new System.Windows.Forms.Padding(0);
      this._groupPanel.Name = "_groupPanel";
      this._groupPanel.Size = new System.Drawing.Size(347, 340);
      this._groupPanel.TabIndex = 0;
      this._groupPanel.Text = "��������� ������";
      // 
      // welcomeTable
      // 
      this.welcomeTable.ColumnCount = 5;
      this.welcomeTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
      this.welcomeTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
      this.welcomeTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 140F));
      this.welcomeTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
      this.welcomeTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
      this.welcomeTable.Controls.Add(this.mainIconLbl, 1, 1);
      this.welcomeTable.Controls.Add(this.mainTextLbl, 2, 1);
      this.welcomeTable.Controls.Add(this._mainBtn, 2, 3);
      this.welcomeTable.Location = new System.Drawing.Point(6, 19);
      this.welcomeTable.Name = "welcomeTable";
      this.welcomeTable.RowCount = 5;
      this.welcomeTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
      this.welcomeTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 64F));
      this.welcomeTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.welcomeTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
      this.welcomeTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
      this.welcomeTable.Size = new System.Drawing.Size(338, 155);
      this.welcomeTable.TabIndex = 1;
      // 
      // mainIconLbl
      // 
      this.mainIconLbl.AutoSize = true;
      this.mainIconLbl.Dock = System.Windows.Forms.DockStyle.Fill;
      this.mainIconLbl.Image = TrackControl.General.Shared.PassChange;
      this.mainIconLbl.Location = new System.Drawing.Point(20, 15);
      this.mainIconLbl.Margin = new System.Windows.Forms.Padding(0);
      this.mainIconLbl.Name = "mainIconLbl";
      this.mainIconLbl.Size = new System.Drawing.Size(79, 64);
      this.mainIconLbl.TabIndex = 0;
      // 
      // mainTextLbl
      // 
      this.mainTextLbl.AutoSize = true;
      this.mainTextLbl.BackColor = System.Drawing.Color.Transparent;
      this.welcomeTable.SetColumnSpan(this.mainTextLbl, 2);
      this.mainTextLbl.Dock = System.Windows.Forms.DockStyle.Fill;
      this.mainTextLbl.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.mainTextLbl.ForeColor = System.Drawing.SystemColors.ControlText;
      this.mainTextLbl.Location = new System.Drawing.Point(99, 15);
      this.mainTextLbl.Margin = new System.Windows.Forms.Padding(0);
      this.mainTextLbl.Name = "mainTextLbl";
      this.mainTextLbl.Size = new System.Drawing.Size(219, 64);
      this.mainTextLbl.TabIndex = 1;
      this.mainTextLbl.Text = "��������� ������ ��� ������� � ���������������� ����������. ����� ������ �� �����" +
          " ���� ����� ���� ��������.";
      // 
      // _mainBtn
      // 
      this._mainBtn.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this._mainBtn.Appearance.Options.UseFont = true;
      this._mainBtn.Dock = System.Windows.Forms.DockStyle.Fill;
      this._mainBtn.Image = TrackControl.General.Shared.EditSensor;
      this._mainBtn.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
      this._mainBtn.Location = new System.Drawing.Point(99, 102);
      this._mainBtn.Margin = new System.Windows.Forms.Padding(0);
      this._mainBtn.Name = "_mainBtn";
      this._mainBtn.Size = new System.Drawing.Size(140, 33);
      this._mainBtn.TabIndex = 2;
      this._mainBtn.Text = "��������";
      this._mainBtn.Click += new System.EventHandler(this.mainButton_Click);
      // 
      // mainTable
      // 
      this.mainTable.ColumnCount = 5;
      this.mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
      this.mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
      this.mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
      this.mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 11F));
      this.mainTable.Controls.Add(this.oldPassLbl, 1, 1);
      this.mainTable.Controls.Add(this.newPassLbl, 1, 3);
      this.mainTable.Controls.Add(this.confirmPassLbl, 1, 5);
      this.mainTable.Controls.Add(this.oldPassBox, 3, 1);
      this.mainTable.Controls.Add(this.newPassBox, 3, 3);
      this.mainTable.Controls.Add(this.confirmPassBox, 3, 5);
      this.mainTable.Controls.Add(this.notificationTable, 1, 7);
      this.mainTable.Controls.Add(this.btnTable, 1, 9);
      this.mainTable.Location = new System.Drawing.Point(6, 177);
      this.mainTable.Margin = new System.Windows.Forms.Padding(0);
      this.mainTable.Name = "mainTable";
      this.mainTable.RowCount = 11;
      this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 5F));
      this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
      this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 4F));
      this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
      this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 4F));
      this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
      this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 5F));
      this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
      this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
      this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
      this.mainTable.Size = new System.Drawing.Size(338, 156);
      this.mainTable.TabIndex = 0;
      // 
      // oldPassLbl
      // 
      this.oldPassLbl.AutoSize = true;
      this.oldPassLbl.Dock = System.Windows.Forms.DockStyle.Fill;
      this.oldPassLbl.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.oldPassLbl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
      this.oldPassLbl.Location = new System.Drawing.Point(10, 5);
      this.oldPassLbl.Margin = new System.Windows.Forms.Padding(0);
      this.oldPassLbl.Name = "oldPassLbl";
      this.oldPassLbl.Size = new System.Drawing.Size(100, 20);
      this.oldPassLbl.TabIndex = 0;
      this.oldPassLbl.Text = "������ ������";
      this.oldPassLbl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // newPassLbl
      // 
      this.newPassLbl.AutoSize = true;
      this.newPassLbl.Dock = System.Windows.Forms.DockStyle.Fill;
      this.newPassLbl.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.newPassLbl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
      this.newPassLbl.Location = new System.Drawing.Point(10, 29);
      this.newPassLbl.Margin = new System.Windows.Forms.Padding(0);
      this.newPassLbl.Name = "newPassLbl";
      this.newPassLbl.Size = new System.Drawing.Size(100, 20);
      this.newPassLbl.TabIndex = 1;
      this.newPassLbl.Text = "����� ������";
      this.newPassLbl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // confirmPassLbl
      // 
      this.confirmPassLbl.AutoSize = true;
      this.confirmPassLbl.Dock = System.Windows.Forms.DockStyle.Fill;
      this.confirmPassLbl.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.confirmPassLbl.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
      this.confirmPassLbl.Location = new System.Drawing.Point(10, 53);
      this.confirmPassLbl.Margin = new System.Windows.Forms.Padding(0);
      this.confirmPassLbl.Name = "confirmPassLbl";
      this.confirmPassLbl.Size = new System.Drawing.Size(100, 20);
      this.confirmPassLbl.TabIndex = 2;
      this.confirmPassLbl.Text = "�������������";
      this.confirmPassLbl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // oldPassBox
      // 
      this.oldPassBox.BackColor = System.Drawing.Color.White;
      this.oldPassBox.Dock = System.Windows.Forms.DockStyle.Fill;
      this.oldPassBox.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.oldPassBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(32)))), ((int)(((byte)(74)))));
      this.oldPassBox.Location = new System.Drawing.Point(120, 5);
      this.oldPassBox.Margin = new System.Windows.Forms.Padding(0);
      this.oldPassBox.MaxLength = 20;
      this.oldPassBox.Name = "oldPassBox";
      this.oldPassBox.Size = new System.Drawing.Size(207, 20);
      this.oldPassBox.TabIndex = 5;
      this.oldPassBox.UseSystemPasswordChar = true;
      this.oldPassBox.Enter += new System.EventHandler(this.oldPassBox_Enter);
      // 
      // newPassBox
      // 
      this.newPassBox.BackColor = System.Drawing.Color.White;
      this.newPassBox.Dock = System.Windows.Forms.DockStyle.Fill;
      this.newPassBox.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.newPassBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(32)))), ((int)(((byte)(74)))));
      this.newPassBox.Location = new System.Drawing.Point(120, 29);
      this.newPassBox.Margin = new System.Windows.Forms.Padding(0);
      this.newPassBox.MaxLength = 20;
      this.newPassBox.Name = "newPassBox";
      this.newPassBox.Size = new System.Drawing.Size(207, 20);
      this.newPassBox.TabIndex = 6;
      this.newPassBox.UseSystemPasswordChar = true;
      this.newPassBox.Enter += new System.EventHandler(this.newPassBox_Enter);
      // 
      // confirmPassBox
      // 
      this.confirmPassBox.BackColor = System.Drawing.Color.White;
      this.confirmPassBox.Dock = System.Windows.Forms.DockStyle.Fill;
      this.confirmPassBox.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.confirmPassBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(32)))), ((int)(((byte)(74)))));
      this.confirmPassBox.Location = new System.Drawing.Point(120, 53);
      this.confirmPassBox.Margin = new System.Windows.Forms.Padding(0);
      this.confirmPassBox.MaxLength = 20;
      this.confirmPassBox.Name = "confirmPassBox";
      this.confirmPassBox.Size = new System.Drawing.Size(207, 20);
      this.confirmPassBox.TabIndex = 7;
      this.confirmPassBox.UseSystemPasswordChar = true;
      this.confirmPassBox.Enter += new System.EventHandler(this.confirmPassBox_Enter);
      // 
      // notificationTable
      // 
      this.notificationTable.ColumnCount = 4;
      this.mainTable.SetColumnSpan(this.notificationTable, 3);
      this.notificationTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.notificationTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
      this.notificationTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
      this.notificationTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
      this.notificationTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
      this.notificationTable.Controls.Add(this.notificationIcon, 1, 0);
      this.notificationTable.Controls.Add(this.notificationLbl, 3, 0);
      this.notificationTable.Dock = System.Windows.Forms.DockStyle.Fill;
      this.notificationTable.Location = new System.Drawing.Point(10, 78);
      this.notificationTable.Margin = new System.Windows.Forms.Padding(0);
      this.notificationTable.Name = "notificationTable";
      this.notificationTable.RowCount = 1;
      this.notificationTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.notificationTable.Size = new System.Drawing.Size(317, 20);
      this.notificationTable.TabIndex = 8;
      // 
      // notificationIcon
      // 
      this.notificationIcon.AutoSize = true;
      this.notificationIcon.BackColor = System.Drawing.Color.Transparent;
      this.notificationIcon.Dock = System.Windows.Forms.DockStyle.Fill;
      this.notificationIcon.Location = new System.Drawing.Point(87, 0);
      this.notificationIcon.Margin = new System.Windows.Forms.Padding(0);
      this.notificationIcon.Name = "notificationIcon";
      this.notificationIcon.Size = new System.Drawing.Size(20, 20);
      this.notificationIcon.TabIndex = 0;
      // 
      // notificationLbl
      // 
      this.notificationLbl.AutoSize = true;
      this.notificationLbl.BackColor = System.Drawing.Color.Transparent;
      this.notificationLbl.Dock = System.Windows.Forms.DockStyle.Fill;
      this.notificationLbl.Font = new System.Drawing.Font("Arial", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.notificationLbl.ForeColor = System.Drawing.Color.DarkRed;
      this.notificationLbl.Location = new System.Drawing.Point(117, 0);
      this.notificationLbl.Margin = new System.Windows.Forms.Padding(0);
      this.notificationLbl.Name = "notificationLbl";
      this.notificationLbl.Size = new System.Drawing.Size(200, 20);
      this.notificationLbl.TabIndex = 1;
      this.notificationLbl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // btnTable
      // 
      this.btnTable.ColumnCount = 4;
      this.mainTable.SetColumnSpan(this.btnTable, 3);
      this.btnTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.btnTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 139F));
      this.btnTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
      this.btnTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
      this.btnTable.Controls.Add(this._saveBtn, 1, 0);
      this.btnTable.Controls.Add(this._cancelBtn, 3, 0);
      this.btnTable.Dock = System.Windows.Forms.DockStyle.Fill;
      this.btnTable.Location = new System.Drawing.Point(10, 106);
      this.btnTable.Margin = new System.Windows.Forms.Padding(0);
      this.btnTable.Name = "btnTable";
      this.btnTable.RowCount = 1;
      this.btnTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.btnTable.Size = new System.Drawing.Size(317, 35);
      this.btnTable.TabIndex = 9;
      // 
      // _saveBtn
      // 
      this._saveBtn.AllowHtmlTextInToolTip = DevExpress.Utils.DefaultBoolean.True;
      this._saveBtn.Appearance.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this._saveBtn.Appearance.Options.UseFont = true;
      this._saveBtn.Dock = System.Windows.Forms.DockStyle.Fill;
      this._saveBtn.Image = TrackControl.General.Shared.Save;
      this._saveBtn.Location = new System.Drawing.Point(131, 3);
      this._saveBtn.Name = "_saveBtn";
      this._saveBtn.Size = new System.Drawing.Size(133, 29);
      this._saveBtn.TabIndex = 0;
      this._saveBtn.Text = "���������";
      this._saveBtn.ToolTip = "��������� ���������";
      this._saveBtn.Click += new System.EventHandler(this.saveBtn_Click);
      // 
      // _cancelBtn
      // 
      this._cancelBtn.Dock = System.Windows.Forms.DockStyle.Fill;
      this._cancelBtn.Image = TrackControl.General.Shared.Cancel;
      this._cancelBtn.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
      this._cancelBtn.Location = new System.Drawing.Point(280, 3);
      this._cancelBtn.Name = "_cancelBtn";
      this._cancelBtn.Size = new System.Drawing.Size(34, 29);
      this._cancelBtn.TabIndex = 1;
      this._cancelBtn.ToolTip = "�������� ���������";
      this._cancelBtn.Click += new System.EventHandler(this.cancelBtn_Click);
      // 
      // PassChanger
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this._groupPanel);
      this.Name = "PassChanger";
      this.Size = new System.Drawing.Size(347, 340);
      ((System.ComponentModel.ISupportInitialize)(this._groupPanel)).EndInit();
      this._groupPanel.ResumeLayout(false);
      this.welcomeTable.ResumeLayout(false);
      this.welcomeTable.PerformLayout();
      this.mainTable.ResumeLayout(false);
      this.mainTable.PerformLayout();
      this.notificationTable.ResumeLayout(false);
      this.notificationTable.PerformLayout();
      this.btnTable.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private DevExpress.XtraEditors.GroupControl _groupPanel;
    private System.Windows.Forms.TableLayoutPanel mainTable;
    private System.Windows.Forms.Label oldPassLbl;
    private System.Windows.Forms.Label newPassLbl;
    private System.Windows.Forms.Label confirmPassLbl;
    private System.Windows.Forms.TextBox oldPassBox;
    private System.Windows.Forms.TextBox newPassBox;
    private System.Windows.Forms.TextBox confirmPassBox;
    private System.Windows.Forms.TableLayoutPanel notificationTable;
    private System.Windows.Forms.Label notificationIcon;
    private System.Windows.Forms.Label notificationLbl;
    private System.Windows.Forms.TableLayoutPanel btnTable;
    private System.Windows.Forms.TableLayoutPanel welcomeTable;
    private System.Windows.Forms.Label mainIconLbl;
    private System.Windows.Forms.Label mainTextLbl;
    private DevExpress.XtraEditors.SimpleButton _mainBtn;
    private DevExpress.XtraEditors.SimpleButton _saveBtn;
    private DevExpress.XtraEditors.SimpleButton _cancelBtn;
  }
}
