namespace TrackControl.Setting.Controls
{
    partial class LogicSensorEventsEditPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LogicSensorEventsEditPanel));
            this.mainTable = new System.Windows.Forms.TableLayoutPanel();
            this.btnTable = new System.Windows.Forms.TableLayoutPanel();
            this._saveBtn = new DevExpress.XtraEditors.SimpleButton();
            this._cancelBtn = new DevExpress.XtraEditors.SimpleButton();
            this.notifyLbl = new DevExpress.XtraEditors.LabelControl();
            this.gp10Box = new DevExpress.XtraEditors.GroupControl();
            this.tb10Panel = new System.Windows.Forms.TableLayoutPanel();
            this.report10FlagCheck = new DevExpress.XtraEditors.CheckEdit();
            this.icon10Combo = new DevExpress.XtraEditors.ComboBoxEdit();
            this.icon10Lbl = new DevExpress.XtraEditors.LabelControl();
            this.transition10Box = new DevExpress.XtraEditors.TextEdit();
            this.head10Lbl = new DevExpress.XtraEditors.LabelControl();
            this.notify10FlagCheck = new DevExpress.XtraEditors.CheckEdit();
            this.sound10Combo = new DevExpress.XtraEditors.ComboBoxEdit();
            this.sound10Lbl = new DevExpress.XtraEditors.LabelControl();
            this.icon10Img = new DevExpress.XtraEditors.PanelControl();
            this.gp01Box = new DevExpress.XtraEditors.GroupControl();
            this.tb01Panel = new System.Windows.Forms.TableLayoutPanel();
            this.head01Lbl = new DevExpress.XtraEditors.LabelControl();
            this.transition01Box = new DevExpress.XtraEditors.TextEdit();
            this.icon01Lbl = new DevExpress.XtraEditors.LabelControl();
            this.icon01Combo = new DevExpress.XtraEditors.ComboBoxEdit();
            this.report01FlagCheck = new DevExpress.XtraEditors.CheckEdit();
            this.sound01Lbl = new DevExpress.XtraEditors.LabelControl();
            this.notify01FlagCheck = new DevExpress.XtraEditors.CheckEdit();
            this.sound01Combo = new DevExpress.XtraEditors.ComboBoxEdit();
            this.icon01Img = new DevExpress.XtraEditors.PanelControl();
            this.mainTable.SuspendLayout();
            this.btnTable.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gp10Box)).BeginInit();
            this.gp10Box.SuspendLayout();
            this.tb10Panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.report10FlagCheck.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.icon10Combo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.transition10Box.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.notify10FlagCheck.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sound10Combo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.icon10Img)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gp01Box)).BeginInit();
            this.gp01Box.SuspendLayout();
            this.tb01Panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.transition01Box.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.icon01Combo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.report01FlagCheck.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.notify01FlagCheck.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sound01Combo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.icon01Img)).BeginInit();
            this.SuspendLayout();
            // 
            // mainTable
            // 
            this.mainTable.BackColor = System.Drawing.Color.Transparent;
            this.mainTable.ColumnCount = 3;
            this.mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 470F));
            this.mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.mainTable.Controls.Add(this.btnTable, 1, 4);
            this.mainTable.Controls.Add(this.gp10Box, 1, 3);
            this.mainTable.Controls.Add(this.gp01Box, 1, 1);
            this.mainTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainTable.Location = new System.Drawing.Point(0, 0);
            this.mainTable.Margin = new System.Windows.Forms.Padding(0);
            this.mainTable.Name = "mainTable";
            this.mainTable.RowCount = 6;
            this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 5F));
            this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 124F));
            this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 5F));
            this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 125F));
            this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 39F));
            this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.mainTable.Size = new System.Drawing.Size(494, 307);
            this.mainTable.TabIndex = 1;
            // 
            // btnTable
            // 
            this.btnTable.ColumnCount = 4;
            this.btnTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.btnTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 130F));
            this.btnTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.btnTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.btnTable.Controls.Add(this._saveBtn, 1, 0);
            this.btnTable.Controls.Add(this._cancelBtn, 3, 0);
            this.btnTable.Controls.Add(this.notifyLbl, 0, 0);
            this.btnTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnTable.Location = new System.Drawing.Point(15, 269);
            this.btnTable.Margin = new System.Windows.Forms.Padding(0, 10, 0, 0);
            this.btnTable.Name = "btnTable";
            this.btnTable.RowCount = 1;
            this.btnTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.btnTable.Size = new System.Drawing.Size(470, 29);
            this.btnTable.TabIndex = 2;
            // 
            // _saveBtn
            // 
            this._saveBtn.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this._saveBtn.Appearance.Options.UseFont = true;
            this._saveBtn.Dock = System.Windows.Forms.DockStyle.Fill;
            this._saveBtn.Image = ((System.Drawing.Image)(resources.GetObject("_saveBtn.Image")));
            this._saveBtn.Location = new System.Drawing.Point(300, 3);
            this._saveBtn.Name = "_saveBtn";
            this._saveBtn.Size = new System.Drawing.Size(124, 23);
            this._saveBtn.TabIndex = 3;
            this._saveBtn.Text = "���������";
            this._saveBtn.Click += new System.EventHandler(this.saveBtn_Click);
            this._saveBtn.MouseEnter += new System.EventHandler(this.saveBtn_MouseEnter);
            this._saveBtn.MouseLeave += new System.EventHandler(this.btn_MouseLeave);
            // 
            // _cancelBtn
            // 
            this._cancelBtn.Dock = System.Windows.Forms.DockStyle.Fill;
            this._cancelBtn.Image = ((System.Drawing.Image)(resources.GetObject("_cancelBtn.Image")));
            this._cancelBtn.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this._cancelBtn.Location = new System.Drawing.Point(440, 3);
            this._cancelBtn.Name = "_cancelBtn";
            this._cancelBtn.Size = new System.Drawing.Size(27, 23);
            this._cancelBtn.TabIndex = 4;
            this._cancelBtn.Click += new System.EventHandler(this.cancelBtn_Click);
            this._cancelBtn.MouseEnter += new System.EventHandler(this.cancelBtn_MouseEnter);
            this._cancelBtn.MouseLeave += new System.EventHandler(this.btn_MouseLeave);
            // 
            // notifyLbl
            // 
            this.notifyLbl.Appearance.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
            this.notifyLbl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.notifyLbl.Location = new System.Drawing.Point(3, 3);
            this.notifyLbl.Name = "notifyLbl";
            this.notifyLbl.Size = new System.Drawing.Size(0, 14);
            this.notifyLbl.TabIndex = 5;
            // 
            // gp10Box
            // 
            this.gp10Box.Appearance.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
            this.gp10Box.Appearance.Options.UseFont = true;
            this.gp10Box.Controls.Add(this.tb10Panel);
            this.gp10Box.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gp10Box.Location = new System.Drawing.Point(18, 137);
            this.gp10Box.Name = "gp10Box";
            this.gp10Box.Size = new System.Drawing.Size(464, 119);
            this.gp10Box.TabIndex = 3;
            this.gp10Box.Text = "������� �� \'1\' � \'0\'";
            // 
            // tb10Panel
            // 
            this.tb10Panel.ColumnCount = 5;
            this.tb10Panel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.tb10Panel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tb10Panel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tb10Panel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tb10Panel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tb10Panel.Controls.Add(this.report10FlagCheck, 2, 4);
            this.tb10Panel.Controls.Add(this.icon10Combo, 2, 3);
            this.tb10Panel.Controls.Add(this.icon10Lbl, 2, 2);
            this.tb10Panel.Controls.Add(this.transition10Box, 2, 1);
            this.tb10Panel.Controls.Add(this.head10Lbl, 2, 0);
            this.tb10Panel.Controls.Add(this.notify10FlagCheck, 4, 4);
            this.tb10Panel.Controls.Add(this.sound10Combo, 4, 3);
            this.tb10Panel.Controls.Add(this.sound10Lbl, 4, 2);
            this.tb10Panel.Controls.Add(this.icon10Img, 0, 0);
            this.tb10Panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tb10Panel.Location = new System.Drawing.Point(2, 21);
            this.tb10Panel.Name = "tb10Panel";
            this.tb10Panel.RowCount = 6;
            this.tb10Panel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tb10Panel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tb10Panel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 17F));
            this.tb10Panel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 21F));
            this.tb10Panel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 19F));
            this.tb10Panel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tb10Panel.Size = new System.Drawing.Size(460, 96);
            this.tb10Panel.TabIndex = 1;
            // 
            // report10FlagCheck
            // 
            this.report10FlagCheck.Cursor = System.Windows.Forms.Cursors.Hand;
            this.report10FlagCheck.Dock = System.Windows.Forms.DockStyle.Fill;
            this.report10FlagCheck.Location = new System.Drawing.Point(93, 78);
            this.report10FlagCheck.Name = "report10FlagCheck";
            this.report10FlagCheck.Properties.Appearance.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
            this.report10FlagCheck.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.report10FlagCheck.Properties.Appearance.Options.UseFont = true;
            this.report10FlagCheck.Properties.Appearance.Options.UseForeColor = true;
            this.report10FlagCheck.Properties.Caption = "��������� � ������";
            this.report10FlagCheck.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.report10FlagCheck.Size = new System.Drawing.Size(169, 19);
            this.report10FlagCheck.TabIndex = 12;
            // 
            // icon10Combo
            // 
            this.icon10Combo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.icon10Combo.Location = new System.Drawing.Point(93, 57);
            this.icon10Combo.Name = "icon10Combo";
            this.icon10Combo.Properties.Appearance.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.icon10Combo.Properties.Appearance.Options.UseFont = true;
            this.icon10Combo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.icon10Combo.Size = new System.Drawing.Size(169, 20);
            this.icon10Combo.TabIndex = 13;
            // 
            // icon10Lbl
            // 
            this.icon10Lbl.Appearance.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.icon10Lbl.Appearance.ForeColor = System.Drawing.Color.Black;
            this.icon10Lbl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.icon10Lbl.Location = new System.Drawing.Point(93, 40);
            this.icon10Lbl.Name = "icon10Lbl";
            this.icon10Lbl.Size = new System.Drawing.Size(39, 14);
            this.icon10Lbl.TabIndex = 14;
            this.icon10Lbl.Text = "������";
            // 
            // transition10Box
            // 
            this.tb10Panel.SetColumnSpan(this.transition10Box, 3);
            this.transition10Box.Dock = System.Windows.Forms.DockStyle.Fill;
            this.transition10Box.Location = new System.Drawing.Point(93, 18);
            this.transition10Box.Name = "transition10Box";
            this.transition10Box.Properties.Appearance.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.transition10Box.Properties.Appearance.Options.UseFont = true;
            this.transition10Box.Size = new System.Drawing.Size(364, 22);
            this.transition10Box.TabIndex = 15;
            // 
            // head10Lbl
            // 
            this.head10Lbl.Appearance.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.head10Lbl.Appearance.ForeColor = System.Drawing.Color.Black;
            this.head10Lbl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.head10Lbl.Location = new System.Drawing.Point(93, 3);
            this.head10Lbl.Name = "head10Lbl";
            this.head10Lbl.Size = new System.Drawing.Size(55, 14);
            this.head10Lbl.TabIndex = 16;
            this.head10Lbl.Text = "��������";
            // 
            // notify10FlagCheck
            // 
            this.notify10FlagCheck.Dock = System.Windows.Forms.DockStyle.Fill;
            this.notify10FlagCheck.Location = new System.Drawing.Point(288, 78);
            this.notify10FlagCheck.Name = "notify10FlagCheck";
            this.notify10FlagCheck.Properties.Appearance.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
            this.notify10FlagCheck.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.notify10FlagCheck.Properties.Appearance.Options.UseFont = true;
            this.notify10FlagCheck.Properties.Appearance.Options.UseForeColor = true;
            this.notify10FlagCheck.Properties.Caption = "�������";
            this.notify10FlagCheck.Size = new System.Drawing.Size(169, 19);
            this.notify10FlagCheck.TabIndex = 17;
            // 
            // sound10Combo
            // 
            this.sound10Combo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sound10Combo.Location = new System.Drawing.Point(288, 57);
            this.sound10Combo.Name = "sound10Combo";
            this.sound10Combo.Properties.Appearance.Font = new System.Drawing.Font("Arial", 8.25F);
            this.sound10Combo.Properties.Appearance.Options.UseFont = true;
            this.sound10Combo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.sound10Combo.Size = new System.Drawing.Size(169, 20);
            this.sound10Combo.TabIndex = 18;
            // 
            // sound10Lbl
            // 
            this.sound10Lbl.Appearance.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
            this.sound10Lbl.Appearance.ForeColor = System.Drawing.Color.Black;
            this.sound10Lbl.Location = new System.Drawing.Point(288, 40);
            this.sound10Lbl.Name = "sound10Lbl";
            this.sound10Lbl.Size = new System.Drawing.Size(95, 14);
            this.sound10Lbl.TabIndex = 19;
            this.sound10Lbl.Text = "�������� ������";
            // 
            // icon10Img
            // 
            this.tb10Panel.SetColumnSpan(this.icon10Img, 2);
            this.icon10Img.Location = new System.Drawing.Point(3, 3);
            this.icon10Img.Name = "icon10Img";
            this.tb10Panel.SetRowSpan(this.icon10Img, 5);
            this.icon10Img.Size = new System.Drawing.Size(84, 88);
            this.icon10Img.TabIndex = 20;
            // 
            // gp01Box
            // 
            this.gp01Box.Appearance.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
            this.gp01Box.Appearance.Options.UseFont = true;
            this.gp01Box.Controls.Add(this.tb01Panel);
            this.gp01Box.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gp01Box.Location = new System.Drawing.Point(18, 8);
            this.gp01Box.Name = "gp01Box";
            this.gp01Box.Size = new System.Drawing.Size(464, 118);
            this.gp01Box.TabIndex = 4;
            this.gp01Box.Text = "������� �� \'0\' � \'1\'";
            // 
            // tb01Panel
            // 
            this.tb01Panel.ColumnCount = 5;
            this.tb01Panel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.tb01Panel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tb01Panel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tb01Panel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tb01Panel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tb01Panel.Controls.Add(this.head01Lbl, 2, 0);
            this.tb01Panel.Controls.Add(this.transition01Box, 2, 1);
            this.tb01Panel.Controls.Add(this.icon01Lbl, 2, 2);
            this.tb01Panel.Controls.Add(this.icon01Combo, 2, 3);
            this.tb01Panel.Controls.Add(this.report01FlagCheck, 2, 4);
            this.tb01Panel.Controls.Add(this.sound01Lbl, 4, 2);
            this.tb01Panel.Controls.Add(this.notify01FlagCheck, 4, 4);
            this.tb01Panel.Controls.Add(this.sound01Combo, 4, 3);
            this.tb01Panel.Controls.Add(this.icon01Img, 0, 0);
            this.tb01Panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tb01Panel.Location = new System.Drawing.Point(2, 21);
            this.tb01Panel.Margin = new System.Windows.Forms.Padding(0);
            this.tb01Panel.Name = "tb01Panel";
            this.tb01Panel.RowCount = 6;
            this.tb01Panel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 14F));
            this.tb01Panel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 21F));
            this.tb01Panel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tb01Panel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tb01Panel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tb01Panel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tb01Panel.Size = new System.Drawing.Size(460, 95);
            this.tb01Panel.TabIndex = 1;
            // 
            // head01Lbl
            // 
            this.head01Lbl.Appearance.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
            this.head01Lbl.Appearance.ForeColor = System.Drawing.Color.Black;
            this.head01Lbl.Location = new System.Drawing.Point(93, 3);
            this.head01Lbl.Name = "head01Lbl";
            this.head01Lbl.Size = new System.Drawing.Size(55, 14);
            this.head01Lbl.TabIndex = 8;
            this.head01Lbl.Text = "��������";
            // 
            // transition01Box
            // 
            this.tb01Panel.SetColumnSpan(this.transition01Box, 3);
            this.transition01Box.Dock = System.Windows.Forms.DockStyle.Fill;
            this.transition01Box.Location = new System.Drawing.Point(93, 17);
            this.transition01Box.Name = "transition01Box";
            this.transition01Box.Properties.Appearance.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
            this.transition01Box.Properties.Appearance.Options.UseFont = true;
            this.transition01Box.Size = new System.Drawing.Size(364, 20);
            this.transition01Box.TabIndex = 9;
            // 
            // icon01Lbl
            // 
            this.icon01Lbl.Appearance.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
            this.icon01Lbl.Appearance.ForeColor = System.Drawing.Color.Black;
            this.icon01Lbl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.icon01Lbl.Location = new System.Drawing.Point(93, 38);
            this.icon01Lbl.Name = "icon01Lbl";
            this.icon01Lbl.Size = new System.Drawing.Size(39, 14);
            this.icon01Lbl.TabIndex = 10;
            this.icon01Lbl.Text = "������";
            // 
            // icon01Combo
            // 
            this.icon01Combo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.icon01Combo.Location = new System.Drawing.Point(93, 53);
            this.icon01Combo.Name = "icon01Combo";
            this.icon01Combo.Properties.Appearance.Font = new System.Drawing.Font("Arial", 8.25F);
            this.icon01Combo.Properties.Appearance.Options.UseFont = true;
            this.icon01Combo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.icon01Combo.Size = new System.Drawing.Size(169, 20);
            this.icon01Combo.TabIndex = 11;
            // 
            // report01FlagCheck
            // 
            this.report01FlagCheck.Dock = System.Windows.Forms.DockStyle.Fill;
            this.report01FlagCheck.Location = new System.Drawing.Point(93, 77);
            this.report01FlagCheck.Name = "report01FlagCheck";
            this.report01FlagCheck.Properties.Appearance.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
            this.report01FlagCheck.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.report01FlagCheck.Properties.Appearance.Options.UseFont = true;
            this.report01FlagCheck.Properties.Appearance.Options.UseForeColor = true;
            this.report01FlagCheck.Properties.Caption = "��������� � ������";
            this.report01FlagCheck.Size = new System.Drawing.Size(169, 19);
            this.report01FlagCheck.TabIndex = 12;
            // 
            // sound01Lbl
            // 
            this.sound01Lbl.Appearance.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
            this.sound01Lbl.Appearance.ForeColor = System.Drawing.Color.Black;
            this.sound01Lbl.Location = new System.Drawing.Point(288, 38);
            this.sound01Lbl.Name = "sound01Lbl";
            this.sound01Lbl.Size = new System.Drawing.Size(95, 14);
            this.sound01Lbl.TabIndex = 13;
            this.sound01Lbl.Text = "�������� ������";
            // 
            // notify01FlagCheck
            // 
            this.notify01FlagCheck.Dock = System.Windows.Forms.DockStyle.Fill;
            this.notify01FlagCheck.Location = new System.Drawing.Point(288, 77);
            this.notify01FlagCheck.Name = "notify01FlagCheck";
            this.notify01FlagCheck.Properties.Appearance.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
            this.notify01FlagCheck.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.notify01FlagCheck.Properties.Appearance.Options.UseFont = true;
            this.notify01FlagCheck.Properties.Appearance.Options.UseForeColor = true;
            this.notify01FlagCheck.Properties.Caption = "�������";
            this.notify01FlagCheck.Size = new System.Drawing.Size(169, 19);
            this.notify01FlagCheck.TabIndex = 14;
            // 
            // sound01Combo
            // 
            this.sound01Combo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sound01Combo.Location = new System.Drawing.Point(288, 53);
            this.sound01Combo.Name = "sound01Combo";
            this.sound01Combo.Properties.Appearance.Font = new System.Drawing.Font("Arial", 8.25F);
            this.sound01Combo.Properties.Appearance.Options.UseFont = true;
            this.sound01Combo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.sound01Combo.Size = new System.Drawing.Size(169, 20);
            this.sound01Combo.TabIndex = 15;
            // 
            // icon01Img
            // 
            this.tb01Panel.SetColumnSpan(this.icon01Img, 2);
            this.icon01Img.Location = new System.Drawing.Point(3, 3);
            this.icon01Img.Name = "icon01Img";
            this.tb01Panel.SetRowSpan(this.icon01Img, 5);
            this.icon01Img.Size = new System.Drawing.Size(84, 88);
            this.icon01Img.TabIndex = 16;
            // 
            // LogicSensorEventsEditPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(244)))), ((int)(((byte)(244)))));
            this.Controls.Add(this.mainTable);
            this.Name = "LogicSensorEventsEditPanel";
            this.Size = new System.Drawing.Size(494, 307);
            this.mainTable.ResumeLayout(false);
            this.btnTable.ResumeLayout(false);
            this.btnTable.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gp10Box)).EndInit();
            this.gp10Box.ResumeLayout(false);
            this.tb10Panel.ResumeLayout(false);
            this.tb10Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.report10FlagCheck.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.icon10Combo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.transition10Box.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.notify10FlagCheck.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sound10Combo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.icon10Img)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gp01Box)).EndInit();
            this.gp01Box.ResumeLayout(false);
            this.tb01Panel.ResumeLayout(false);
            this.tb01Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.transition01Box.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.icon01Combo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.report01FlagCheck.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.notify01FlagCheck.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sound01Combo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.icon01Img)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel mainTable;
        private System.Windows.Forms.TableLayoutPanel btnTable;
      private DevExpress.XtraEditors.SimpleButton _saveBtn;
      private DevExpress.XtraEditors.SimpleButton _cancelBtn;
      private DevExpress.XtraEditors.GroupControl gp10Box;
      private System.Windows.Forms.TableLayoutPanel tb10Panel;
      private DevExpress.XtraEditors.CheckEdit report10FlagCheck;
      private DevExpress.XtraEditors.ComboBoxEdit icon10Combo;
      private DevExpress.XtraEditors.LabelControl icon10Lbl;
      private DevExpress.XtraEditors.TextEdit transition10Box;
      private DevExpress.XtraEditors.LabelControl head10Lbl;
      private DevExpress.XtraEditors.CheckEdit notify10FlagCheck;
      private DevExpress.XtraEditors.ComboBoxEdit sound10Combo;
      private DevExpress.XtraEditors.LabelControl sound10Lbl;
      private DevExpress.XtraEditors.GroupControl gp01Box;
      private System.Windows.Forms.TableLayoutPanel tb01Panel;
      private DevExpress.XtraEditors.LabelControl head01Lbl;
      private DevExpress.XtraEditors.TextEdit transition01Box;
      private DevExpress.XtraEditors.LabelControl icon01Lbl;
      private DevExpress.XtraEditors.ComboBoxEdit icon01Combo;
      private DevExpress.XtraEditors.CheckEdit report01FlagCheck;
      private DevExpress.XtraEditors.LabelControl sound01Lbl;
      private DevExpress.XtraEditors.CheckEdit notify01FlagCheck;
      private DevExpress.XtraEditors.ComboBoxEdit sound01Combo;
      private DevExpress.XtraEditors.PanelControl icon01Img;
      private DevExpress.XtraEditors.PanelControl icon10Img;
      private DevExpress.XtraEditors.LabelControl notifyLbl;
    }
}
