namespace TrackControl.Setting.Controls
{
  partial class AddButtonControl
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.mainTable = new System.Windows.Forms.TableLayoutPanel();
      this._addBtn = new DevExpress.XtraEditors.SimpleButton();
      this._backBtn = new DevExpress.XtraEditors.SimpleButton();
      this.mainTable.SuspendLayout();
      this.SuspendLayout();
      // 
      // mainTable
      // 
      this.mainTable.BackColor = System.Drawing.Color.Transparent;
      this.mainTable.ColumnCount = 2;
      this.mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 85F));
      this.mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
      this.mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
      this.mainTable.Controls.Add(this._addBtn, 0, 1);
      this.mainTable.Controls.Add(this._backBtn, 1, 1);
      this.mainTable.Dock = System.Windows.Forms.DockStyle.Fill;
      this.mainTable.Location = new System.Drawing.Point(0, 0);
      this.mainTable.Margin = new System.Windows.Forms.Padding(0);
      this.mainTable.Name = "mainTable";
      this.mainTable.RowCount = 2;
      this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 43F));
      this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
      this.mainTable.Size = new System.Drawing.Size(430, 100);
      this.mainTable.TabIndex = 0;
      // 
      // _addBtn
      // 
      this._addBtn.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
      this._addBtn.Appearance.Options.UseFont = true;
      this._addBtn.Dock = System.Windows.Forms.DockStyle.Right;
      this._addBtn.Image = TrackControl.General.Shared.PlusLittle;
      this._addBtn.Location = new System.Drawing.Point(219, 57);
      this._addBtn.Margin = new System.Windows.Forms.Padding(15, 0, 15, 15);
      this._addBtn.Name = "_addBtn";
      this._addBtn.Size = new System.Drawing.Size(111, 28);
      this._addBtn.TabIndex = 0;
      this._addBtn.Text = "��������";
      this._addBtn.Click += new System.EventHandler(this.addBtn_Click);
      // 
      // _backBtn
      // 
      this._backBtn.Dock = System.Windows.Forms.DockStyle.Fill;
      this._backBtn.Image = TrackControl.General.Shared.Cancel;
      this._backBtn.Location = new System.Drawing.Point(345, 57);
      this._backBtn.Margin = new System.Windows.Forms.Padding(0, 0, 15, 15);
      this._backBtn.Name = "_backBtn";
      this._backBtn.Size = new System.Drawing.Size(70, 28);
      this._backBtn.TabIndex = 1;
      this._backBtn.Text = "�����";
      this._backBtn.Click += new System.EventHandler(this.backBtn_Click);
      // 
      // AddButtonControl
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.Color.Transparent;
      this.Controls.Add(this.mainTable);
      this.Name = "AddButtonControl";
      this.Size = new System.Drawing.Size(430, 100);
      this.mainTable.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.TableLayoutPanel mainTable;
    private DevExpress.XtraEditors.SimpleButton _addBtn;
    private DevExpress.XtraEditors.SimpleButton _backBtn;
  }
}
