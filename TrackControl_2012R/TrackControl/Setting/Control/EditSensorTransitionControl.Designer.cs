namespace TrackControl.Setting.Controls
{
  partial class EditSensorTransitionControl
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      this.initialStateCombo.Dispose();
      this.finalStateCombo.Dispose();
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditSensorTransitionControl));
            this.mainTable = new System.Windows.Forms.TableLayoutPanel();
            this.iconTable = new System.Windows.Forms.TableLayoutPanel();
            this.iconLbl = new DevExpress.XtraEditors.LabelControl();
            this.soundLbl = new DevExpress.XtraEditors.LabelControl();
            this.notifyCheckBox = new DevExpress.XtraEditors.CheckEdit();
            this.soundTable = new System.Windows.Forms.TableLayoutPanel();
            this.reportCheckBox = new DevExpress.XtraEditors.CheckEdit();
            this.iconCombo = new DevExpress.XtraEditors.ComboBoxEdit();
            this.soundCombo = new DevExpress.XtraEditors.ComboBoxEdit();
            this.buttonsTable = new System.Windows.Forms.TableLayoutPanel();
            this._saveBtn = new DevExpress.XtraEditors.SimpleButton();
            this._cancelBtn = new DevExpress.XtraEditors.SimpleButton();
            this._soundBtn = new DevExpress.XtraEditors.SimpleButton();
            this.notifyLbl = new DevExpress.XtraEditors.LabelControl();
            this.statesTable = new System.Windows.Forms.TableLayoutPanel();
            this.initStateLbl = new DevExpress.XtraEditors.LabelControl();
            this.finalStateLbl = new DevExpress.XtraEditors.LabelControl();
            this.initialStateCombo = new DevExpress.XtraEditors.LookUpEdit();
            this.initialStateBinding = new System.Windows.Forms.BindingSource(this.components);
            this.finalStateCombo = new DevExpress.XtraEditors.LookUpEdit();
            this.finalStateBinding = new System.Windows.Forms.BindingSource(this.components);
            this.titleLbl = new DevExpress.XtraEditors.LabelControl();
            this.titleTextBox = new DevExpress.XtraEditors.TextEdit();
            this.iconImg = new DevExpress.XtraEditors.PanelControl();
            this.mainTable.SuspendLayout();
            this.iconTable.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.notifyCheckBox.Properties)).BeginInit();
            this.soundTable.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.reportCheckBox.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iconCombo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.soundCombo.Properties)).BeginInit();
            this.buttonsTable.SuspendLayout();
            this.statesTable.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.initialStateCombo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialStateBinding)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.finalStateCombo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.finalStateBinding)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.titleTextBox.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iconImg)).BeginInit();
            this.SuspendLayout();
            // 
            // mainTable
            // 
            this.mainTable.ColumnCount = 5;
            this.mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 78F));
            this.mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 329F));
            this.mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.mainTable.Controls.Add(this.iconTable, 1, 4);
            this.mainTable.Controls.Add(this.soundTable, 1, 5);
            this.mainTable.Controls.Add(this.buttonsTable, 1, 7);
            this.mainTable.Controls.Add(this.statesTable, 3, 2);
            this.mainTable.Controls.Add(this.titleLbl, 3, 0);
            this.mainTable.Controls.Add(this.titleTextBox, 3, 1);
            this.mainTable.Controls.Add(this.iconImg, 1, 0);
            this.mainTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainTable.Location = new System.Drawing.Point(0, 0);
            this.mainTable.Margin = new System.Windows.Forms.Padding(0);
            this.mainTable.Name = "mainTable";
            this.mainTable.RowCount = 9;
            this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 19F));
            this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 8F));
            this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.mainTable.Size = new System.Drawing.Size(440, 180);
            this.mainTable.TabIndex = 0;
            // 
            // iconTable
            // 
            this.iconTable.ColumnCount = 5;
            this.mainTable.SetColumnSpan(this.iconTable, 3);
            this.iconTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.iconTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.iconTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.iconTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.iconTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.iconTable.Controls.Add(this.iconLbl, 2, 0);
            this.iconTable.Controls.Add(this.soundLbl, 4, 0);
            this.iconTable.Controls.Add(this.notifyCheckBox, 0, 0);
            this.iconTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.iconTable.Location = new System.Drawing.Point(11, 87);
            this.iconTable.Margin = new System.Windows.Forms.Padding(0);
            this.iconTable.Name = "iconTable";
            this.iconTable.RowCount = 1;
            this.iconTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.iconTable.Size = new System.Drawing.Size(417, 19);
            this.iconTable.TabIndex = 3;
            // 
            // iconLbl
            // 
            this.iconLbl.Appearance.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
            this.iconLbl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.iconLbl.Location = new System.Drawing.Point(93, 3);
            this.iconLbl.Name = "iconLbl";
            this.iconLbl.Size = new System.Drawing.Size(39, 14);
            this.iconLbl.TabIndex = 5;
            this.iconLbl.Text = "������";
            // 
            // soundLbl
            // 
            this.soundLbl.Appearance.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
            this.soundLbl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.soundLbl.Location = new System.Drawing.Point(270, 3);
            this.soundLbl.Name = "soundLbl";
            this.soundLbl.Size = new System.Drawing.Size(33, 14);
            this.soundLbl.TabIndex = 6;
            this.soundLbl.Text = "�����";
            // 
            // notifyCheckBox
            // 
            this.notifyCheckBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.notifyCheckBox.Location = new System.Drawing.Point(3, 3);
            this.notifyCheckBox.Name = "notifyCheckBox";
            this.notifyCheckBox.Properties.Appearance.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
            this.notifyCheckBox.Properties.Appearance.Options.UseFont = true;
            this.notifyCheckBox.Properties.Caption = "�������";
            this.notifyCheckBox.Size = new System.Drawing.Size(74, 19);
            this.notifyCheckBox.TabIndex = 7;
            // 
            // soundTable
            // 
            this.soundTable.ColumnCount = 5;
            this.mainTable.SetColumnSpan(this.soundTable, 3);
            this.soundTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.soundTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.soundTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.soundTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.soundTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.soundTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.soundTable.Controls.Add(this.reportCheckBox, 0, 0);
            this.soundTable.Controls.Add(this.iconCombo, 2, 0);
            this.soundTable.Controls.Add(this.soundCombo, 4, 0);
            this.soundTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.soundTable.Location = new System.Drawing.Point(11, 106);
            this.soundTable.Margin = new System.Windows.Forms.Padding(0);
            this.soundTable.Name = "soundTable";
            this.soundTable.RowCount = 1;
            this.soundTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.soundTable.Size = new System.Drawing.Size(417, 25);
            this.soundTable.TabIndex = 5;
            // 
            // reportCheckBox
            // 
            this.reportCheckBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.reportCheckBox.Location = new System.Drawing.Point(3, 3);
            this.reportCheckBox.Name = "reportCheckBox";
            this.reportCheckBox.Properties.Appearance.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
            this.reportCheckBox.Properties.Appearance.Options.UseFont = true;
            this.reportCheckBox.Properties.Caption = "�����";
            this.reportCheckBox.Size = new System.Drawing.Size(74, 19);
            this.reportCheckBox.TabIndex = 3;
            // 
            // iconCombo
            // 
            this.iconCombo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.iconCombo.Location = new System.Drawing.Point(93, 3);
            this.iconCombo.Name = "iconCombo";
            this.iconCombo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.iconCombo.Size = new System.Drawing.Size(144, 20);
            this.iconCombo.TabIndex = 4;
            // 
            // soundCombo
            // 
            this.soundCombo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.soundCombo.Location = new System.Drawing.Point(270, 3);
            this.soundCombo.Name = "soundCombo";
            this.soundCombo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.soundCombo.Size = new System.Drawing.Size(144, 20);
            this.soundCombo.TabIndex = 5;
            // 
            // buttonsTable
            // 
            this.buttonsTable.ColumnCount = 4;
            this.mainTable.SetColumnSpan(this.buttonsTable, 3);
            this.buttonsTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.buttonsTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.buttonsTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 130F));
            this.buttonsTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.buttonsTable.Controls.Add(this._saveBtn, 2, 0);
            this.buttonsTable.Controls.Add(this._cancelBtn, 3, 0);
            this.buttonsTable.Controls.Add(this._soundBtn, 0, 0);
            this.buttonsTable.Controls.Add(this.notifyLbl, 1, 0);
            this.buttonsTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonsTable.Location = new System.Drawing.Point(11, 139);
            this.buttonsTable.Margin = new System.Windows.Forms.Padding(0);
            this.buttonsTable.Name = "buttonsTable";
            this.buttonsTable.RowCount = 1;
            this.buttonsTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.buttonsTable.Size = new System.Drawing.Size(417, 31);
            this.buttonsTable.TabIndex = 6;
            // 
            // _saveBtn
            // 
            this._saveBtn.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this._saveBtn.Appearance.Options.UseFont = true;
            this._saveBtn.Dock = System.Windows.Forms.DockStyle.Fill;
            this._saveBtn.Image = ((System.Drawing.Image)(resources.GetObject("_saveBtn.Image")));
            this._saveBtn.Location = new System.Drawing.Point(257, 3);
            this._saveBtn.Margin = new System.Windows.Forms.Padding(3, 3, 12, 3);
            this._saveBtn.Name = "_saveBtn";
            this._saveBtn.Size = new System.Drawing.Size(115, 25);
            this._saveBtn.TabIndex = 4;
            this._saveBtn.Text = "���������";
            this._saveBtn.Click += new System.EventHandler(this.saveBtn_Click);
            this._saveBtn.MouseEnter += new System.EventHandler(this.saveBtn_MouseEnter);
            this._saveBtn.MouseLeave += new System.EventHandler(this.btn_MouseLeave);
            // 
            // _cancelBtn
            // 
            this._cancelBtn.Dock = System.Windows.Forms.DockStyle.Fill;
            this._cancelBtn.Image = ((System.Drawing.Image)(resources.GetObject("_cancelBtn.Image")));
            this._cancelBtn.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this._cancelBtn.Location = new System.Drawing.Point(387, 3);
            this._cancelBtn.Name = "_cancelBtn";
            this._cancelBtn.Size = new System.Drawing.Size(27, 25);
            this._cancelBtn.TabIndex = 5;
            this._cancelBtn.Click += new System.EventHandler(this.cancelBtn_Click);
            this._cancelBtn.MouseEnter += new System.EventHandler(this.cancelBtn_MouseEnter);
            this._cancelBtn.MouseLeave += new System.EventHandler(this.btn_MouseLeave);
            // 
            // _soundBtn
            // 
            this._soundBtn.Dock = System.Windows.Forms.DockStyle.Fill;
            this._soundBtn.Image = ((System.Drawing.Image)(resources.GetObject("_soundBtn.Image")));
            this._soundBtn.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this._soundBtn.Location = new System.Drawing.Point(3, 3);
            this._soundBtn.Name = "_soundBtn";
            this._soundBtn.Size = new System.Drawing.Size(27, 25);
            this._soundBtn.TabIndex = 6;
            this._soundBtn.Click += new System.EventHandler(this.soundBtn_Click);
            this._soundBtn.MouseEnter += new System.EventHandler(this.soundBtn_MouseEnter);
            this._soundBtn.MouseLeave += new System.EventHandler(this.btn_MouseLeave);
            // 
            // notifyLbl
            // 
            this.notifyLbl.Appearance.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
            this.notifyLbl.Appearance.ForeColor = System.Drawing.Color.Red;
            this.notifyLbl.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.notifyLbl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.notifyLbl.Location = new System.Drawing.Point(36, 3);
            this.notifyLbl.Name = "notifyLbl";
            this.notifyLbl.Size = new System.Drawing.Size(0, 14);
            this.notifyLbl.TabIndex = 7;
            this.notifyLbl.Text = "  ";
            // 
            // statesTable
            // 
            this.statesTable.ColumnCount = 3;
            this.statesTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.statesTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.statesTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.statesTable.Controls.Add(this.initStateLbl, 0, 0);
            this.statesTable.Controls.Add(this.finalStateLbl, 2, 0);
            this.statesTable.Controls.Add(this.initialStateCombo, 0, 1);
            this.statesTable.Controls.Add(this.finalStateCombo, 2, 1);
            this.statesTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.statesTable.Location = new System.Drawing.Point(99, 40);
            this.statesTable.Margin = new System.Windows.Forms.Padding(0);
            this.statesTable.Name = "statesTable";
            this.statesTable.RowCount = 2;
            this.mainTable.SetRowSpan(this.statesTable, 2);
            this.statesTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 35.29412F));
            this.statesTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 64.70588F));
            this.statesTable.Size = new System.Drawing.Size(329, 47);
            this.statesTable.TabIndex = 7;
            // 
            // initStateLbl
            // 
            this.initStateLbl.Appearance.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
            this.initStateLbl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.initStateLbl.Location = new System.Drawing.Point(3, 3);
            this.initStateLbl.Name = "initStateLbl";
            this.initStateLbl.Size = new System.Drawing.Size(120, 14);
            this.initStateLbl.TabIndex = 4;
            this.initStateLbl.Text = "��������� ���������";
            // 
            // finalStateLbl
            // 
            this.finalStateLbl.Appearance.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
            this.finalStateLbl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.finalStateLbl.Location = new System.Drawing.Point(182, 3);
            this.finalStateLbl.Name = "finalStateLbl";
            this.finalStateLbl.Size = new System.Drawing.Size(116, 14);
            this.finalStateLbl.TabIndex = 5;
            this.finalStateLbl.Text = "�������� ���������";
            // 
            // initialStateCombo
            // 
            this.initialStateCombo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.initialStateCombo.Location = new System.Drawing.Point(3, 19);
            this.initialStateCombo.Name = "initialStateCombo";
            this.initialStateCombo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.initialStateCombo.Properties.DataSource = this.initialStateBinding;
            this.initialStateCombo.Properties.DisplayMember = "Title";
            this.initialStateCombo.Size = new System.Drawing.Size(144, 20);
            this.initialStateCombo.TabIndex = 6;
            // 
            // initialStateBinding
            // 
            this.initialStateBinding.DataMember = "State";
            this.initialStateBinding.DataSource = typeof(LocalCache.atlantaDataSet);
            // 
            // finalStateCombo
            // 
            this.finalStateCombo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.finalStateCombo.Location = new System.Drawing.Point(182, 19);
            this.finalStateCombo.Name = "finalStateCombo";
            this.finalStateCombo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.finalStateCombo.Properties.DataSource = this.finalStateBinding;
            this.finalStateCombo.Properties.DisplayMember = "Title";
            this.finalStateCombo.Size = new System.Drawing.Size(144, 20);
            this.finalStateCombo.TabIndex = 7;
            // 
            // finalStateBinding
            // 
            this.finalStateBinding.DataMember = "State";
            this.finalStateBinding.DataSource = typeof(LocalCache.atlantaDataSet);
            // 
            // titleLbl
            // 
            this.titleLbl.Appearance.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
            this.titleLbl.Location = new System.Drawing.Point(102, 3);
            this.titleLbl.Name = "titleLbl";
            this.titleLbl.Size = new System.Drawing.Size(105, 14);
            this.titleLbl.TabIndex = 10;
            this.titleLbl.Text = "�������� �������";
            // 
            // titleTextBox
            // 
            this.titleTextBox.Location = new System.Drawing.Point(102, 23);
            this.titleTextBox.Name = "titleTextBox";
            this.titleTextBox.Properties.Appearance.Font = new System.Drawing.Font("Arial", 8.25F);
            this.titleTextBox.Properties.Appearance.Options.UseFont = true;
            this.titleTextBox.Size = new System.Drawing.Size(323, 20);
            this.titleTextBox.TabIndex = 11;
            // 
            // iconImg
            // 
            this.iconImg.Dock = System.Windows.Forms.DockStyle.Fill;
            this.iconImg.Location = new System.Drawing.Point(14, 3);
            this.iconImg.Name = "iconImg";
            this.mainTable.SetRowSpan(this.iconImg, 4);
            this.iconImg.Size = new System.Drawing.Size(72, 81);
            this.iconImg.TabIndex = 12;
            // 
            // EditSensorTransitionControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.mainTable);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "EditSensorTransitionControl";
            this.Size = new System.Drawing.Size(440, 180);
            this.Load += new System.EventHandler(this.EditSensorTransitionControl_Load);
            this.mainTable.ResumeLayout(false);
            this.mainTable.PerformLayout();
            this.iconTable.ResumeLayout(false);
            this.iconTable.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.notifyCheckBox.Properties)).EndInit();
            this.soundTable.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.reportCheckBox.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iconCombo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.soundCombo.Properties)).EndInit();
            this.buttonsTable.ResumeLayout(false);
            this.buttonsTable.PerformLayout();
            this.statesTable.ResumeLayout(false);
            this.statesTable.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.initialStateCombo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.initialStateBinding)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.finalStateCombo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.finalStateBinding)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.titleTextBox.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iconImg)).EndInit();
            this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.TableLayoutPanel mainTable;
    private System.Windows.Forms.TableLayoutPanel iconTable;
    private System.Windows.Forms.TableLayoutPanel soundTable;
    private System.Windows.Forms.TableLayoutPanel buttonsTable;
    private System.Windows.Forms.TableLayoutPanel statesTable;
    private System.Windows.Forms.BindingSource initialStateBinding;
    private System.Windows.Forms.BindingSource finalStateBinding;
    private DevExpress.XtraEditors.SimpleButton _saveBtn;
    private DevExpress.XtraEditors.SimpleButton _cancelBtn;
    private DevExpress.XtraEditors.SimpleButton _soundBtn;
    private DevExpress.XtraEditors.LabelControl notifyLbl;
    private DevExpress.XtraEditors.LabelControl initStateLbl;
    private DevExpress.XtraEditors.LabelControl finalStateLbl;
    private DevExpress.XtraEditors.LabelControl titleLbl;
    private DevExpress.XtraEditors.LabelControl iconLbl;
    private DevExpress.XtraEditors.LabelControl soundLbl;
    private DevExpress.XtraEditors.CheckEdit notifyCheckBox;
    private DevExpress.XtraEditors.CheckEdit reportCheckBox;
    private DevExpress.XtraEditors.TextEdit titleTextBox;
    private DevExpress.XtraEditors.LookUpEdit initialStateCombo;
    private DevExpress.XtraEditors.LookUpEdit finalStateCombo;
    private DevExpress.XtraEditors.ComboBoxEdit iconCombo;
    private DevExpress.XtraEditors.ComboBoxEdit soundCombo;
    private DevExpress.XtraEditors.PanelControl iconImg;
  }
}
