using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using MySql.Data.MySqlClient;
using TrackControl.General;
using TrackControl.General.DAL;
using TrackControl.General.DatabaseDriver;
using TrackControl.Reports;
using System.Data;
using TrackControl.Properties;

namespace TrackControl.Setting.Control
{

    #region enumerations

    internal enum AnalizAlgoritms : int
    {
        GPS_ABSENT = 1,
        POWER_ABSENT = 2,
        DATA_OMIT = 3,
        DATA_DOUBLES = 4,
        DATA_STATISTIC = 5
    }

    #endregion

    #region structures

    internal struct MobitelInfo
    {
        public string Name;
        public DateTime dtBegin;
        public DateTime dtEnd;
        public int CNT;
    }

    internal struct DataRowAnaliz
    {
        public DateTime DataGPS;
        public long LogID;
        public long SrvPacketID;
        public int Mobitel_ID;
        public int Valid;
    }

    #endregion

    /// <summary>
    /// ������ ������(����������, ������ ������).
    /// </summary>
    public partial class DataAnalysis : UserControl
    {
        private LocalCache.atlantaDataSet dataset;
        private MobitelInfo _MobitelInfo = new MobitelInfo();
        private Dictionary<int, MobitelInfo> MobitelAcum = new Dictionary<int, MobitelInfo>();
        //private Dictionary<int, QueryLostData> MobitelLostData = new Dictionary<int, QueryLostData>();
        private List<QueryLostData> MobitelLostData = new List<QueryLostData>();
        private Dictionary<int, string> dictMobitels;

        public DataAnalysis()
        {
            InitializeComponent();
            init();
        }

        private void DataAnalysis_Load(object sender, EventArgs e)
        {
            if (DesignMode)
            {
                return;
            }
            dataset = AppModel.Instance.DataSet;
            dpEnd.Value = DateTime.Now;
            dpBegin.Value = DateTime.Now.AddMonths(-1);
            //using (ConnectMySQL cnMySQL = new ConnectMySQL())
            DriverDb db = new DriverDb();
            db.ConnectDb();
            {
                //string sSQLselect = "SELECT  '0' as CH, vehicle.Mobitel_id as Mobitel_Id, CONCAT(vehicle.MakeCar,' | ' , vehicle.CarModel) AS Name FROM   vehicle ORDER BY Mobitel_id";
                string sSQLselect = TrackControlQuery.DataAnalysis.SelectVehicleMobitel;

                //DataTable dt = cnMySQL.GetDataTable(sSQLselect);
                DataTable dt = null;
                dt = db.GetDataTable(sSQLselect);

                dictMobitels = new Dictionary<int, string>();
                DataColumn col = new DataColumn("CH", System.Type.GetType("System.Boolean"));
                col.DefaultValue = false;
                dt.Columns.Add(col);

                foreach (DataRow dr in dt.Rows)
                {
                    if (!dictMobitels.ContainsKey((int) dr["Mobitel_id"]))
                    {
                        dictMobitels.Add((int) dr["Mobitel_id"], dr["Name"].ToString());
                        //dr["CH"] = Convert.ToBoolean(0);
                    }
                }

                dgvMobitels.DataSource = dt;
            }
            //dgvMobitels.Sort(dgvMobitels.Columns[2], ListSortDirection.Ascending);
            db.CloseDbConnection();
        }

        private void btAnal_GPS_Abs_Click(object sender, EventArgs e)
        {
            Analiz((int) AnalizAlgoritms.GPS_ABSENT);
        }

        private bool AnalizAddRow(ref DataRowAnaliz dra, DateTime dtBegin, long cnt)
        {
            if (dra.Mobitel_ID <= 0) return false;
            DateTime dtEnd = dra.DataGPS;
            DataGridViewRow newRow = new DataGridViewRow();
            if (dtEnd.Subtract(dtBegin).Minutes >= Convert.ToInt16(txMin.Text))
            {
                for (int i = 0; i <= 4; i++)
                {
                    newRow.Cells.Add(new DataGridViewTextBoxCell());
                }

                newRow.Cells[0].Value = GetMobitelName(dra.Mobitel_ID);
                newRow.Cells[1].Value = dtBegin;
                newRow.Cells[2].Value = dtEnd;
                newRow.Cells[3].Value = dtEnd.Subtract(dtBegin);
                newRow.Cells[4].Value = cnt;
                dgvAnaliz.Rows.Add(newRow);
                Application.DoEvents();
            }
            return true;
        }

        private bool AnalizAddRowNoValid(ref DataRowAnaliz dra, DateTime dtBegin, int cnt)
        {
            if (dra.Mobitel_ID <= 0) return false;
            DataGridViewRow newRow = new DataGridViewRow();
            for (int i = 0; i <= 4; i++)
            {
                newRow.Cells.Add(new DataGridViewTextBoxCell());
            }

            newRow.Cells[0].Value = GetMobitelName(dra.Mobitel_ID);
            newRow.Cells[1].Value = dtBegin;
            newRow.Cells[2].Value = "";
            newRow.Cells[3].Value = "";
            newRow.Cells[4].Value = cnt;
            dgvAnaliz.Rows.Add(newRow);
            Application.DoEvents();
            return true;
        }

        private bool AnalizAddDictionary()
        {
            DataGridViewRow newRow = new DataGridViewRow();
            foreach (MobitelInfo mi in MobitelAcum.Values)
            {
                newRow = new DataGridViewRow();
                for (int i = 0; i <= 4; i++)
                {
                    newRow.Cells.Add(new DataGridViewTextBoxCell());
                }

                newRow.Cells[0].Value = mi.Name;
                newRow.Cells[1].Value = mi.dtBegin;
                newRow.Cells[2].Value = mi.dtEnd;
                newRow.Cells[3].Value = mi.dtEnd.Subtract(mi.dtBegin);
                newRow.Cells[4].Value = mi.CNT;
                dgvAnaliz.Rows.Add(newRow);
                Application.DoEvents();
            }
            return true;
        }

        private void AnalizLoadMobitels()
        {
            //ConnectMySQL cnMySQL = new ConnectMySQL();
            DriverDb db = new DriverDb();
            db.ConnectDb();

            string sSQL = TrackControlQuery.DataAnalysis.SelectFromMobitels;

            //MySqlDataReader drWork = cnMySQL.GetDataReader(sSQL);
            db.GetDataReader(sSQL);
            //while (drWork.Read())
            while (db.Read())
            {
                DataGridViewRow newRow = new DataGridViewRow();
                // ������� ������ ���� CheckBox
                DataGridViewCheckBoxCell Cell_check = new DataGridViewCheckBoxCell();
                Cell_check.Value = false;
                Cell_check.ReadOnly = false;
                // ��������� � �������� ������ ������ ����� ������ ������ ���� CheckBox
                newRow.Cells.Add(Cell_check);
                // ��������� ������ ��������� �������� ���� TextBox
                newRow.Cells.Add(new DataGridViewTextBoxCell());
                newRow.Cells.Add(new DataGridViewTextBoxCell());
                //newRow.Cells[1].Value = drWork.GetInt32(drWork.GetOrdinal("Mobitel_ID"));
                newRow.Cells[1].Value = db.GetInt32(db.GetOrdinal("Mobitel_ID"));
                newRow.Cells[2].Value = db.GetString(db.GetOrdinal("Mobitel_Name"));
                newRow.Cells[2].ReadOnly = false;
                // ��� ������� ����� � �������������� � ������ �������
                dgvMobitels.Rows.Add(newRow);
            }
            //drWork.Close();
            db.CloseDataReader();
            //cnMySQL.CloseMySQLConnection();
            db.CloseDbConnection();
            return;
        }

        private void btAnal_Data_Abs_Click(object sender, EventArgs e)
        {
            Analiz((int) AnalizAlgoritms.DATA_OMIT);
        }

        private void Analiz(int iTypeAnaliz)
        {
            if (!GridChecked())
            {
                MessageBox.Show(Resources.SelectVehicleForAnalysis, Resources.DataAnalysis);
                return;
            }
            DateTime dtEnd = dpEnd.Value;
            //dtEnd = dtEnd.AddDays(1);
            DateTime dtBegin = dpBegin.Value;
            dgvAnaliz.RowCount = 0;
            lbInfor.Text = Resources.LoadingFromDB;
            Application.DoEvents();
            double days = dtEnd.Subtract(dtBegin).TotalDays;
            pbAnaliz.Value = 0;
            pbAnaliz.Visible = true;
            pbAnaliz.Maximum = (int) days;
            pbAnaliz.Minimum = 0;

            MobitelLostData.Clear();

            foreach (DataGridViewRow row in dgvMobitels.Rows)
            {
                bool checkedCell;
                if (bool.TryParse(row.Cells["CH"].FormattedValue.ToString(), out checkedCell))
                {
                    if (checkedCell)
                    {
                        lbInfor.Text = row.Cells["Mobitel_Name"].Value.ToString();
                        pbAnaliz.Value = 0;
                        //DriverDb db = new DriverDb();
                        for (int i = 0; i < days; i++)
                        {
                            DateTime dayBegin = dtBegin.AddDays(i);
                            DateTime dayEnd = dtBegin.AddDays(i + 1);
                            DataTable dtWork;

                            string sSQL = TrackControlQuery.DataAnalysis.SelectDatagpsData;

                            switch (iTypeAnaliz)
                            {
                                case (int) AnalizAlgoritms.DATA_DOUBLES:
                                    sSQL = sSQL + " ORDER BY SrvPacketID, LogID";
                                    break;
                                default:
                                    sSQL = sSQL + " ORDER BY Mobitel_ID, LogID";
                                    break;
                            }

                            using (DriverDb db = new DriverDb())
                            {
                                db.ConnectDb();
                                db.NewSqlParameterArray(3);
                                db.SetNewSqlParameter(db.ParamPrefics + "Begin", dayBegin);
                                db.SetNewSqlParameter(db.ParamPrefics + "End", dayEnd);
                                db.SetNewSqlParameter(db.ParamPrefics + "Mobitel_id", row.Cells["Mobitel_id"].Value);
                                //db.GetDataReader(sSQL, db.GetSqlParameterArray);
                                dtWork = null;
                                dtWork = db.GetDataTable(sSQL, db.GetSqlParameterArray);
                            }

                            AnalizData(iTypeAnaliz, dayEnd, ref dayBegin, ref sSQL, dtWork);

                            if (pbAnaliz.Value < pbAnaliz.Maximum)
                                pbAnaliz.Value++;
                        } // for
                    }
                }
            }
            dgvAnaliz.Sort(dgvAnaliz.Columns[1], ListSortDirection.Ascending);
            System.GC.Collect();
            pbAnaliz.Visible = false;
            lbInfor.Text = Resources.Done;
        }

        private void AnalizData(int iTypeAnaliz, DateTime dtEnd, ref DateTime dtBegin, ref string sSQL, DataTable dtWork)
        {
            int max = dtWork.Rows.Count;
            int cnt = 0;
            bool bStartAnaliz = false;
            bool bStartZone = false;
            //lbInfor.Text = Resources.DataAnalysis;
            //pbAnaliz.Value = 0;
            //pbAnaliz.Visible = true;
            //pbAnaliz.Maximum = max;
            //pbAnaliz.Minimum = 0;
            long LogIDprev = 0;
            long SrvPacketIDprev = 0;
            int Mobitel_IDprev = 0;
            int Mob_Id = 0;
            Application.DoEvents();
            //MobitelLostData.Clear();
            //while (dr.Read())
            DataRowAnaliz dra = new DataRowAnaliz();
            for (int i = 0; i < dtWork.Rows.Count; i++)
            {
                DataRow dr = dtWork.Rows[i];
                dra.DataGPS = Convert.ToDateTime(dr["DataGPS"]);
                dra.LogID = Convert.ToInt64(dr["LogID"]);
                dra.Mobitel_ID = (int) (dr["Mobitel_ID"]);
                dra.SrvPacketID = Convert.ToInt64(dr["SrvPacketID"]);
                dra.Valid = Convert.ToInt32(dr["Valid"]);
                //DataColumn dc = new DataColumn(
                //if (pbAnaliz.Value < max - 1) pbAnaliz.Value++;
                switch (iTypeAnaliz)
                {
                    case (int) AnalizAlgoritms.DATA_DOUBLES:
                        bStartAnaliz = true;
                        break;
                    default:
                        if (Mob_Id != dra.Mobitel_ID)
                        {
                            bStartAnaliz = false;
                            Mob_Id = dra.Mobitel_ID;
                        }
                        break;
                }

                if (bStartAnaliz)
                {
                    try
                    {
                        switch (iTypeAnaliz)
                        {
                                //--------------------------------------------------------------------------------
                            case (int) AnalizAlgoritms.DATA_OMIT:
                                if (((dra.LogID - LogIDprev) > 1) && (SrvPacketIDprev <= dra.SrvPacketID))
                                {
                                    //������ ��� ����� ����  - ���� ��� �� �������� � � ������������  UNIXTIME
                                    // �������������� ������ ���������

                                    sSQL = string.Format(TrackControlQuery.DataAnalysis.SelectCountDatagps, Mob_Id,
                                        LogIDprev, dra.LogID);

                                    int cntRec = 0;
                                    //using (ConnectMySQL cnMySQLR = new ConnectMySQL())
                                    {
                                        //cntRec = Convert.ToInt32(cnMySQLR.GetScalarValue(sSQL));
                                        using (DriverDb db = new DriverDb())
                                        {
                                            db.ConnectDb();
                                            cntRec = Convert.ToInt32(db.GetScalarValue(sSQL));
                                        }
                                    }
                                    if (cntRec <= (dra.LogID - LogIDprev - 1))
                                    {
                                        #region ���������� ������ � �������� �� ����������� ������

                                        QueryLostData _QueryLostData = new QueryLostData();
                                        _QueryLostData.Mobitel_ID = Mob_Id;
                                        _QueryLostData.Begin_LogID = LogIDprev;
                                        _QueryLostData.End_LogID = dra.LogID;
                                        _QueryLostData.Begin_SrvPacketID = SrvPacketIDprev;
                                        _QueryLostData.End_SrvPacketID = dra.SrvPacketID;
                                        MobitelLostData.Add(_QueryLostData);

                                        #endregion

                                        AnalizAddRow(ref dra, dtBegin, dra.LogID - LogIDprev - 1);
                                    }
                                }
                                LogIDprev = dra.LogID;
                                SrvPacketIDprev = dra.SrvPacketID;
                                if (!bStartZone) dtBegin = dra.DataGPS;
                                break;

                                //--------------------------------------------------------------------------------
                            case (int) AnalizAlgoritms.GPS_ABSENT:
                                switch (dra.Valid)
                                {
                                    case 0:
                                        if (!bStartZone)
                                        {
                                            bStartZone = true;
                                            dtBegin = dra.DataGPS;
                                            cnt = 0;
                                        }
                                        cnt++;
                                        break;
                                    case 1:
                                        if (bStartZone)
                                        {
                                            AnalizAddRow(ref dra, dtBegin, cnt);
                                            bStartZone = false;
                                        }
                                        break;
                                }
                                LogIDprev = dra.LogID;
                                break;
                                //--------------------------------------------------------------------------------
                            case (int) AnalizAlgoritms.DATA_DOUBLES:
                                if ((SrvPacketIDprev == dra.SrvPacketID && (LogIDprev == dra.LogID) &&
                                     (Mobitel_IDprev != dra.Mobitel_ID)))
                                {
                                    if (MobitelAcum.ContainsKey(dra.Mobitel_ID))
                                    {
                                        _MobitelInfo.CNT = MobitelAcum[dra.Mobitel_ID].CNT + 1;
                                        _MobitelInfo.dtBegin = MobitelAcum[dra.Mobitel_ID].dtBegin;
                                        _MobitelInfo.dtEnd = dra.DataGPS;
                                        _MobitelInfo.Name = MobitelAcum[dra.Mobitel_ID].Name;
                                        MobitelAcum.Remove(dra.Mobitel_ID);
                                        MobitelAcum.Add(dra.Mobitel_ID, _MobitelInfo);
                                    }
                                    else
                                    {
                                        _MobitelInfo.CNT = 1;
                                        _MobitelInfo.dtBegin = dra.DataGPS;
                                        _MobitelInfo.dtEnd = dra.DataGPS;
                                       MobitelAcum.Add(dra.Mobitel_ID, _MobitelInfo);
                                    }
                                }
                                LogIDprev = dra.LogID;
                                SrvPacketIDprev = dra.SrvPacketID;
                                Mobitel_IDprev = dra.Mobitel_ID;
                                break;
                                //--------------------------------------------------------------------------------
                            case (int) AnalizAlgoritms.DATA_STATISTIC:
                                if (MobitelAcum.ContainsKey(dra.Mobitel_ID))
                                {
                                    _MobitelInfo.CNT = MobitelAcum[dra.Mobitel_ID].CNT + 1;
                                    _MobitelInfo.dtBegin = MobitelAcum[dra.Mobitel_ID].dtBegin;
                                    if (_MobitelInfo.dtEnd.Subtract(dra.DataGPS).Minutes < 0)
                                        _MobitelInfo.dtEnd = dra.DataGPS;
                                    _MobitelInfo.Name = MobitelAcum[dra.Mobitel_ID].Name;

                                    MobitelAcum.Remove(dra.Mobitel_ID);
                                    MobitelAcum.Add(dra.Mobitel_ID, _MobitelInfo);
                                }
                                else
                                {
                                    _MobitelInfo.CNT = 1;
                                    _MobitelInfo.dtBegin = dra.DataGPS;
                                    _MobitelInfo.dtEnd = dra.DataGPS;
                                    _MobitelInfo.Name = GetMobitelName(dra.Mobitel_ID);
                                    MobitelAcum.Add(dra.Mobitel_ID, _MobitelInfo);
                                }
                                break;
                        }
                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show(ex.StackTrace, String.Format(" Error LogID: {0}", LogIDprev));
                    }
                    //finally
                    //{
                    //    lbInfor.Text = "LogID:" + Convert.ToString(LogIDprev);  
                    //}

                    Application.DoEvents();
                }

                else
                    //��������� ��������� ������ � ������������ UNIX ��������
                    if (dra.Valid == 1)
                    {
                        bStartAnaliz = true;
                        bStartZone = false;
                        LogIDprev = dra.LogID;
                        SrvPacketIDprev = dra.SrvPacketID;
                        Mobitel_IDprev = dra.Mobitel_ID;
                    }
            }
            //��������� ���
            switch (iTypeAnaliz)
            {
                case (int) AnalizAlgoritms.DATA_DOUBLES:
                    AnalizAddDictionary();
                    break;
                case (int) AnalizAlgoritms.DATA_STATISTIC:
                    AnalizAddDictionary();
                    break;
                case (int) AnalizAlgoritms.GPS_ABSENT:
                    if (bStartZone) AnalizAddRow(ref dra, dtBegin, cnt);
                    // � ����� �������� ���������� ������
                    if (dtEnd.Subtract(dra.DataGPS).TotalMinutes > 0)
                    {
                        dtBegin = dra.DataGPS;
                        //using (ConnectMySQL cnMySQL = new ConnectMySQL())
                        using (DriverDb dbDriver = new DriverDb())
                        {
                            dbDriver.ConnectDb();

                            //MySqlDataReader dr = cnMySQL.GetDataReader(sSQL);
                            sSQL = string.Format(TrackControlQuery.DataAnalysis.SelectDataGpsId, dra.LogID,
                                dra.Mobitel_ID);

                            dbDriver.GetDataReader(sSQL);

                            cnt = 0;
                            //while (dr.Read())
                            while (dbDriver.Read())
                            {
                                //if (!dr.IsDBNull(dr.GetOrdinal("Valid")))
                                if (!dbDriver.IsDbNull(dbDriver.GetOrdinal("Valid")))
                                {
                                    //if (dr.GetInt16("Valid") == 1) 
                                    if (dbDriver.GetInt16("Valid") == 1)
                                        break;
                                }
                                //if (!dr.IsDBNull(dr.GetOrdinal("DataGPS")))
                                if (!dbDriver.IsDbNull(dbDriver.GetOrdinal("DataGPS")))
                                {
                                    //if (dtEnd.Subtract(dr.GetDateTime("DataGPS")).TotalMinutes < 0) 
                                    if (dtEnd.Subtract(dbDriver.GetDateTime("DataGPS")).TotalMinutes < 0)
                                        break;
                                }
                                cnt++;
                            }
                        }
                        if (cnt > 0) AnalizAddRowNoValid(ref dra, dtBegin, cnt);
                    }
                    break;
                default:
                    if (bStartZone) AnalizAddRow(ref dra, dtBegin, cnt);
                    break;
            }

            ////dr.Close();
            //dgvAnaliz.Sort(dgvAnaliz.Columns[1], ListSortDirection.Ascending);
            ////cnMySQL = null;
            //System.GC.Collect();
            //pbAnaliz.Visible = false;
            //lbInfor.Text = Resources.Done;
        }

        private void btAnal_Data_Double_Click(object sender, EventArgs e)
        {
            MobitelAcum.Clear();

            Analiz((int) AnalizAlgoritms.DATA_DOUBLES);
        }

        private void btAnal_Statistic_Click(object sender, EventArgs e)
        {
            MobitelAcum.Clear();
            Analiz((int) AnalizAlgoritms.DATA_STATISTIC);
        }

        private bool GridChecked()
        {
            bool bCheckPresent = false;
            foreach (DataGridViewRow row in dgvMobitels.Rows)
            {
                bool checkedCell;
                if (bool.TryParse(row.Cells["CH"].FormattedValue.ToString(), out checkedCell))
                {
                    if (checkedCell)
                    {
                        bCheckPresent = true;
                        break;
                    }
                }
            }
            return bCheckPresent;
        }

        private int GridCheckedCount()
        {
            int counter = 0;
            foreach (DataGridViewRow row in dgvMobitels.Rows)
            {
                bool checkedCell;
                if (bool.TryParse(row.Cells["CH"].FormattedValue.ToString(), out checkedCell))
                {
                    if (checkedCell)
                    {
                        counter++;
                    }
                }
            }
            return counter;
        }

        private bool GridAllChecked()
        {
            bool bCheckNoPresent = false;
            foreach (DataGridViewRow row in dgvMobitels.Rows)
            {
                bool checkedCell;
                if (bool.TryParse(row.Cells["CH"].FormattedValue.ToString(), out checkedCell))
                {
                    if (!checkedCell)
                    {
                        bCheckNoPresent = true;
                    }
                }
            }
            return !bCheckNoPresent;
        }

        private void btAnal_Data_Add_Click( object sender, EventArgs e )
        {
            if( !GridChecked() )
            {
                MessageBox.Show( Resources.SelectVehicleForReQuery, Resources.DataAnalysis );
                return;
            }
            if( DialogResult.Yes ==
                MessageBox.Show( this, Resources.ReQueryConfirm, Resources.DataAnalysis, MessageBoxButtons.YesNo ) )
            {
                Int64 NewConfirmedID = 0;
                string sSQL;
                //ConnectMySQL cnMySQL = new ConnectMySQL();
                DriverDb db = new DriverDb();
                db.ConnectDb();

                if( MobitelLostData.Count > 0 )
                {
                    sSQL = TrackControlQuery.PointsAnalyzer.TruncateDatagpsbuffer_on;
                    //cnMySQL.ExecuteNonQueryCommand(sSQL);
                    db.ExecuteNonQueryCommand( sSQL );

                    sSQL = TrackControlQuery.PointsAnalyzer.TruncateDatagpslost_on;
                    //cnMySQL.ExecuteNonQueryCommand(sSQL);
                    db.ExecuteNonQueryCommand( sSQL );

                    sSQL = TrackControlQuery.PointsAnalyzer.TruncateDatagpslost_ontmp;
                    //cnMySQL.ExecuteNonQueryCommand(sSQL);
                    db.ExecuteNonQueryCommand( sSQL );
                    pbAnaliz.Maximum = MobitelLostData.Count;
                    pbAnaliz.Minimum = 0;
                    pbAnaliz.Value = 0;
                    pbAnaliz.Visible = true;
                    lbInfor.Text = Resources.ReQuery;
                    int m_id = 0;

                    //foreach( QueryLostData qld in MobitelLostData )
                    for(int k = 0; k < MobitelLostData.Count;)
                    {
                        QueryLostData qld = MobitelLostData[k];
                        if( m_id != qld.Mobitel_ID )
                        {
                            if( m_id > 0 )
                            {
                                sSQL = string.Format( TrackControlQuery.DataAnalysis.UpdateMobitels, NewConfirmedID, m_id );

                                //cnMySQL.ExecuteNonQueryCommand(sSQL);
                                db.ExecuteNonQueryCommand( sSQL );
                            }

                            m_id = qld.Mobitel_ID;
                            NewConfirmedID = qld.Begin_LogID;
                        }
                        //************** Exception Text **************
                        // MySql.Data.MySqlClient.MySqlException: Duplicate entry '100-0' for key 3
                        //------------------------------------------------------------
                        //INDEX IDX_MobitelID USING BTREE (Mobitel_ID),
                        //UNIQUE INDEX IDX_MobitelID_BeginLogID USING BTREE (Mobitel_ID, Begin_LogID),
                        //UNIQUE INDEX IDX_MobitelID_BeginSrvPacketID USING BTREE (Mobitel_ID, Begin_SrvPacketID)
                        //ConnectMySQL cnMySQLR = new ConnectMySQL();
                        DriverDb driverDb = new DriverDb();
                        driverDb.ConnectDb();

                        sSQL = string.Format( TrackControlQuery.DataAnalysis.SelectCountDatagpslost, qld.Mobitel_ID,
                            qld.Begin_SrvPacketID, qld.Mobitel_ID, qld.Begin_LogID );

                        //int cntUNIQUE = Convert.ToInt32(cnMySQLR.GetScalarValue(sSQL));
                        int cntUNIQUE = Convert.ToInt32( driverDb.GetScalarValue( sSQL ) );
                        //cnMySQLR.CloseMySQLConnection();
                        driverDb.CloseDbConnection();
                        //******************************************
                        if( ( cntUNIQUE == 0 ) )
                        {
                            sSQL = string.Format( TrackControlQuery.DataAnalysis.InsertIntoDatagpslost, qld.Mobitel_ID,
                                qld.Begin_LogID, qld.End_LogID, qld.Begin_SrvPacketID,
                                qld.End_SrvPacketID );

                            //cnMySQL.ExecuteNonQueryCommand(sSQL);
                            db.ExecuteNonQueryCommand( sSQL );

                            if( NewConfirmedID > qld.Begin_LogID ) 
                                NewConfirmedID = qld.Begin_LogID;

                            pbAnaliz.Value += 1;
                            Application.DoEvents();
                        } // if

                        MobitelLostData.Remove(qld);
                    }

                    if( m_id > 0 )
                    {
                        sSQL = string.Format( TrackControlQuery.DataAnalysis.UpdateMobitelsConfirmedId, NewConfirmedID,
                            m_id );

                        //cnMySQL.ExecuteNonQueryCommand(sSQL);
                        db.ExecuteNonQueryCommand( sSQL );
                    }
                    pbAnaliz.Visible = false;

                } // If(MobitelLostData.Count >0)
                else
                //     � ���� ������ �� ��� ��� �������.
                //���� ���� ������������� �������� LogID, �� ��� �����������
                //������ �������� ConfirmedID ��������� �������������� ������:
                //��������� ����� �������� LogID ������������� ��� �������������
                //����� ����� (�� DataGpsId). 
                {
                    foreach( DataGridViewRow row in dgvMobitels.Rows )
                    {
                        bool checkedCell;
                        if( bool.TryParse( row.Cells["CH"].FormattedValue.ToString(), out checkedCell ) )
                        {
                            if( checkedCell )
                            {
                                lbInfor.Text = ( string )row.Cells[1].Value;
                                Application.DoEvents();

                                sSQL = TrackControlQuery.DataAnalysis.SelectDatagpsLost_on +
                                       row.Cells["Mobitel_id"].Value;

                                //if (cnMySQL.GetDataReaderRecordsCount(sSQL) == 0)
                                if( db.GetDataReaderRecordsCount( sSQL ) == 0 )
                                {
                                    sSQL = string.Format( TrackControlQuery.DataAnalysis.Select1FromDataGps,
                                        row.Cells["Mobitel_id"].Value );

                                    //if (cnMySQL.GetDataReaderRecordsCount(sSQL) == 1)//���� �������������
                                    if( db.GetDataReaderRecordsCount( sSQL ) == 1 ) //���� �������������
                                    {

                                        sSQL = string.Format( TrackControlQuery.DataAnalysis.SelectMaxDataGpsId,
                                            row.Cells["Mobitel_id"].Value );

                                        //Int64 MaxNegativeDataGpsId = Convert.ToInt64(cnMySQL.GetScalarValue(sSQL));
                                        Int64 MaxNegativeDataGpsId = Convert.ToInt64( db.GetScalarValue( sSQL ) );
                                        Int64 MaxPositiveDataGpsId = 0;

                                        sSQL = string.Format( TrackControlQuery.DataAnalysis.Select1DataGps,
                                            row.Cells["Mobitel_id"].Value );

                                        //if (cnMySQL.GetDataReaderRecordsCount(sSQL) == 1)//���� �������������
                                        if( db.GetDataReaderRecordsCount( sSQL ) == 1 ) //���� �������������
                                        {
                                            sSQL =
                                                string.Format(
                                                    TrackControlQuery.DataAnalysis.SelectMaxDataGpsIdFromDataGps,
                                                    row.Cells["Mobitel_id"].Value );

                                            //MaxPositiveDataGpsId = Convert.ToInt64(cnMySQL.GetScalarValue(sSQL));
                                            MaxPositiveDataGpsId = Convert.ToInt64( db.GetScalarValue( sSQL ) );
                                        }
                                        if( MaxNegativeDataGpsId > MaxPositiveDataGpsId )
                                        {
                                            sSQL = string.Format( TrackControlQuery.DataAnalysis.SelectCoalesceMaxLogID,
                                                row.Cells["Mobitel_id"].Value );

                                            //NewConfirmedID = Convert.ToInt64(cnMySQL.GetScalarValue(sSQL));
                                            NewConfirmedID = Convert.ToInt64( db.GetScalarValue( sSQL ) );
                                        }
                                        else
                                        {
                                            sSQL = string.Format( TrackControlQuery.DataAnalysis.SelectCoalesceMaxLog_id,
                                                row.Cells["Mobitel_id"].Value );

                                            //NewConfirmedID = Convert.ToInt64(cnMySQL.GetScalarValue(sSQL));
                                            NewConfirmedID = Convert.ToInt64( db.GetScalarValue( sSQL ) );
                                        }
                                    }
                                    else //������ �������������
                                    {
                                        sSQL = string.Format( TrackControlQuery.DataAnalysis.SelectCoalesceMaxDataGps,
                                            row.Cells["Mobitel_id"].Value );

                                        //NewConfirmedID = Convert.ToInt64(cnMySQL.GetScalarValue(sSQL));
                                        NewConfirmedID = Convert.ToInt64( db.GetScalarValue( sSQL ) );
                                    }

                                    sSQL = string.Format( TrackControlQuery.DataAnalysis.UpdateMobitelsConfirmed_ID, NewConfirmedID, row.Cells["Mobitel_id"].Value );

                                    //cnMySQL.ExecuteNonQueryCommand(sSQL);
                                    db.ExecuteNonQueryCommand( sSQL );
                                }
                            }
                        }
                    } // foreach
                } // cnMySQL.CloseMySQLConnection();
                db.CloseDbConnection();
                // MessageBox.Show("�������� �����������!","������ ������");
                lbInfor.Text = Resources.Done;
            } // if (DialogResult.Yes
        } // btAnal_Data_Add_Click

        private string GetMobitelName(int Mobitel_Id)
        {
            if (dictMobitels.ContainsKey(Mobitel_Id))
                return dictMobitels[Mobitel_Id];
            else
                return Mobitel_Id.ToString();
        }

        private void chSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            bool SetValue = false;
            if (chSelectAll.Checked) SetValue = true;
            DataTable dt = (DataTable) dgvMobitels.DataSource;
            foreach (DataRow drow in dt.Rows)
            {
                drow["CH"] = SetValue;
            }
        }

        private void init()
        {
            Mobitel_Name.HeaderText = Resources.CarModel;
            Car.HeaderText = Resources.Transport;
            DateB.HeaderText = Resources.DateFrom;
            DateE.HeaderText = Resources.DateTo;
            Interval.HeaderText = Resources.Interval;
            QTYpoints.HeaderText = Resources.PointCount;
            btAnal_Statistic.Text = Resources.Statistics;
            lbMin.Text = Resources.MinutesShort;
            lbInterval.Text = Resources.IntervalMoreThan;
            lbEnd.Text = Resources.Before;
            lbBegin.Text = Resources.IntervalFrom;
            btAnal_Data_Double.Text = Resources.DuplicatedData;
            btAnal_Data_Abs.Text = Resources.LostData;
            btAnal_GPS_Abs.Text = Resources.NoGPS;
            lbInfor.Text = String.Format("{0}          ", Resources.DataAnalysis);
            btDeleteDates.Text = Resources.DeletingInCorrectDates;
            ToolTip forButton = new ToolTip();
            forButton.SetToolTip(btDeleteDates, Resources.DeletingInCorrectDatesDetail);

        }

        private void btDeleteDates_Click(object sender, EventArgs e)
        {
            DeleteRecordsWithDatesInFuture();
        }

        private void DeleteRecordsWithDatesInFuture()
        {
            int countChecked = GridCheckedCount();
            if (countChecked == 0)
            {
                MessageBox.Show(Resources.SelectVehicleForAnalysis, Resources.DataAnalysis);
                return;
            }
            if (DialogResult.OK ==
                XtraMessageBox.Show(Resources.ConfimDateDeleting, Resources.DataAnalysis, MessageBoxButtons.OKCancel))
            {
                pbAnaliz.Value = 0;
                pbAnaliz.Visible = true;
                pbAnaliz.Maximum = countChecked;
                pbAnaliz.Minimum = 0;

                foreach (DataGridViewRow row in dgvMobitels.Rows)
                {
                    bool checkedCell;
                    if (bool.TryParse(row.Cells["CH"].FormattedValue.ToString(), out checkedCell))
                    {
                        if (checkedCell)
                        {
                            lbInfor.Text = GetMobitelName((int) row.Cells["Mobitel_id"].Value);
                            pbAnaliz.Value++;
                            //using (ConnectMySQL cnn = new ConnectMySQL())
                            DriverDb db = new DriverDb();
                            db.ConnectDb();
                            {
                                string sql = string.Format(TrackControlQuery.DataAnalysis.UpdateDatagps,
                                            row.Cells["Mobitel_id"].Value);
                               
                                //cnn.ExecuteNonQueryCommand(sql);
                                db.ExecuteNonQueryCommand(sql);

                                sql = string.Format(TrackControlQuery.DataAnalysis.DeleteFromOnline, row.Cells["Mobitel_id"].Value);

                                //cnn.ExecuteNonQueryCommand(sql);
                                db.ExecuteNonQueryCommand(sql);
                            }

                            db.CloseDbConnection();
                        }
                    }
                    Application.DoEvents();
                }

                pbAnaliz.Visible = false;
                lbInfor.Text = Resources.Done;
                XtraMessageBox.Show(Resources.DeletingComplite, Resources.DataAnalysis);
            }
        }
    }
}
