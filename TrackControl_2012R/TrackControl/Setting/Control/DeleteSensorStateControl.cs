using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using LocalCache;
using LocalCache.atlantaDataSetTableAdapters;
using TrackControl.Properties;

namespace TrackControl.Setting.Controls
{
    /// <summary>
    /// ������� ��� �������� ���������� ���������.
    /// </summary>
    [ToolboxItem(false)]
    public partial class DeleteSensorStateControl : UserControl
    {
        /// <summary>
        /// ������� ��� ������� ��������� (�������).
        /// </summary>
        private TransitionTableAdapter _transitionAdapter;

        /// <summary>
        /// ������� ��� ������� ���������.
        /// </summary>
        private StateTableAdapter _stateAdapter;

        /// <summary>
        /// ������� ����������.
        /// </summary>
        private atlantaDataSet _dataset;

        /// <summary>
        /// ���������, ������� ������������ �������.
        /// </summary>
        private atlantaDataSet.StateRow _state;

        /// <summary>
        /// ��������� ����� �������� ���������� ���������.
        /// </summary>
        public event EventHandler EndDelete;

        /// <summary>
        /// �����������.
        /// </summary>
        /// <param name="stateAdapter">������� ��� ������� ���������</param>
        /// <param name="transitionAdapter">������� ��� ������� ���������</param>
        /// <param name="dataset">������� ����������</param>
        /// <param name="state">���������, ������� ������������ �������</param>
        public DeleteSensorStateControl(
            StateTableAdapter stateAdapter,
            TransitionTableAdapter transitionAdapter,
            atlantaDataSet dataset,
            atlantaDataSet.StateRow state)
        {
            _stateAdapter = stateAdapter;
            _transitionAdapter = transitionAdapter;
            _dataset = dataset;
            _state = state;

            InitializeComponent();
            init();
        }

        private void init()
        {
            askingLbl.Text = Resources.StateDeleteConfirm;
            warningLbl.Text = Resources.StateDeleteWarning;
            _deleteBtn.Text = Resources.Delete;

        }

        /// <summary>
        /// ���������� �������� ��������.
        /// </summary>
        private void DeleteSensorStateControl_Load(object sender, EventArgs e)
        {
            stateBox.Text = _state.Title;
            minBox.Text = _state.MinValue.ToString("N2");
            maxBox.Text = _state.MaxValue.ToString("N2");
            ActiveControl = _deleteBtn;
        }

        /// <summary>
        /// ������������ ��������� ������� �� ������ "�������".
        /// </summary>
        private void deleteBtn_MouseEnter(object sender, EventArgs e)
        {
            notifyLbl.ForeColor = Color.DarkRed;
            notifyLbl.Text = Resources.StateDelete;
        }

        /// <summary>
        /// ������������ ��������� ������� �� ������ "������".
        /// </summary>
        private void cancelBtn_MouseEnter(object sender, EventArgs e)
        {
            notifyLbl.ForeColor = Color.DarkBlue;
            notifyLbl.Text = Resources.DeleteCancel;
        }

        /// <summary>
        /// ������������ �������� ������� � ������ ("�������" ���� "������").
        /// </summary>
        private void btn_MouseLeave(object sender, EventArgs e)
        {
            notifyLbl.Text = "";
        }

        /// <summary>
        /// ������������ ���� �� ������ "������".
        /// </summary>
        private void cancelBtn_Click(object sender, EventArgs e)
        {
            onEndDelete();
        }

        /// <summary>
        /// ������������ ���� �� ������ "�������".
        /// ������� ��������� ��������� � ��������� � ��� �������.
        /// </summary>
        private void deleteBtn_Click(object sender, EventArgs e)
        {
            _deleteBtn.Enabled = false;
            _state.Delete();

            _transitionAdapter.Update(_dataset.Transition);
            _stateAdapter.Update(_dataset.State);

            onEndDelete();
        }

        /// <summary>
        /// ���������� ������� EndDelete.
        /// </summary>
        private void onEndDelete()
        {
            if (EndDelete != null)
                EndDelete(this, EventArgs.Empty);
        }
    }
}
