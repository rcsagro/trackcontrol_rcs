using System;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using LocalCache;
using LocalCache.atlantaDataSetTableAdapters;
using TrackControl.General;
using EventTracker;
using TrackControl.Properties;

namespace TrackControl.Setting.Controls
{
    [ToolboxItem(false)]
    public partial class EditSensorTransitionControl : UserControl
    {
        /// <summary>
        /// ������� ��� ������� �������.
        /// </summary>
        private TransitionTableAdapter _transitionAdapter;

        /// <summary>
        /// ������� ����������.
        /// </summary>
        private atlantaDataSet _dataset;

        /// <summary>
        /// �������, ������� ������������� � ������ ������.
        /// </summary>
        private atlantaDataSet.TransitionRow _transition;

        /// <summary>
        /// ����, ����������� ������������� ������������ ������� ���
        /// ������ ��� �����������.
        /// </summary>
        private bool _added;

        /// <summary>
        /// �������, ����������� ����� ��������� ��������������.
        /// ��� ���������� ���� ��� ����� �� ������ "������", ���� ��� ���������� ���������.
        /// </summary>
        public event EventHandler EndEdit;

        /// <summary>
        /// �����������.
        /// </summary>
        /// <param name="transitionAdapter">������� ��� ������� �������</param>
        /// <param name="dataset">������� ����������</param>
        /// <param name="transition">�������, ������� �������������</param>
        /// <param name="added">����������� �� (����� - �������������)</param>
        public EditSensorTransitionControl(TransitionTableAdapter transitionAdapter, atlantaDataSet dataset,
            atlantaDataSet.TransitionRow transition, bool added)
        {
            _transitionAdapter = transitionAdapter;
            _dataset = dataset;
            _transition = transition;
            _added = added;

            InitializeComponent();
            init();

            titleTextBox.TextChanged += titleTextBox_Enter;
            notifyCheckBox.MouseEnter += notifyCheckBox_MouseEnter;
            reportCheckBox.MouseEnter += reportCheckBox_MouseEnter;
            iconCombo.SelectedIndexChanged += iconCombo_SelectedIndexChanged;
            soundCombo.SelectedIndexChanged += soundCombo_SelectedIndexChanged;
            initialStateCombo.TextChanged += initialStateCombo_CloseUp;
            //finalStateCombo.TextChanged += FinalStateComboOnTextChanged; 
        }

        private void FinalStateComboOnTextChanged(object sender, EventArgs eventArgs)
        {
            initialStateBinding.Filter = String.Format( "SensorId = {0}", _transition.SensorId );
        }

        private void init()
        {
            soundLbl.Text = Resources.AlarmSound;
            iconLbl.Text = Resources.Icon;
            notifyCheckBox.Text = Resources.Alarm;
            reportCheckBox.Text = Resources.Report;
            initStateLbl.Text = Resources.InitialState;
            finalStateLbl.Text = Resources.FinalState;
            titleLbl.Text = Resources.EventDescription;
            _saveBtn.Text = Resources.Save;
        }

        /// <summary>
        /// ���������� ������� EndEdit.
        /// </summary>
        private void onEndEdit()
        {
            EventTracker.Helper.StopPlaying();

            if (EndEdit != null)
                EndEdit(this, EventArgs.Empty);

            Dispose();
        }

        /// <summary>
        /// ��������� ������������ ��������� ������.
        /// </summary>
        /// <returns>True - ���� ��������� ������ ���������</returns>
        private bool validate()
        {
            bool result = true;
            // ���������, ������� �� �������� �������
            if (titleTextBox.Text.Trim().Length == 0 || titleTextBox.BackColor != Color.White)
            {
                titleTextBox.BackColor = Color.FromArgb(255, 242, 232);
                titleTextBox.ForeColor = Color.DarkRed;
                titleTextBox.Text = Resources.Err_MustBeFilled;
                result = false;
            }

            if (!result) 
                _saveBtn.Enabled = true;

            return result;
        }

        /// <summary>
        /// ��������� ������� �������� ��������.
        /// </summary>
        private void EditSensorTransitionControl_Load(object sender, EventArgs e)
        {
            // �������� �������
            titleTextBox.Text = _transition.Title;

            // ��������� ���������
            initialStateBinding.DataSource = _dataset;
            initialStateBinding.DataMember = "State";

            initialStateBinding.Filter = String.Format("SensorId = {0}", _transition.SensorId);

            initialStateCombo.EditValue = _transition.StateRowByState_Transition_Initial.Title;
            initialStateCombo.Properties.DataSource = initialStateBinding;
            initialStateCombo.Properties.ValueMember = "Title";
            
            // �������� ���������
            finalStateBinding.DataSource = _dataset;
            finalStateBinding.DataMember = "State";
            finalStateBinding.Filter = String.Format("SensorId = {0} AND Id <> {1}", _transition.SensorId,
                _transition.InitialStateId);

            finalStateCombo.EditValue = _transition.StateRowByState_Transition_Final.Title;
            finalStateCombo.Properties.DataSource = finalStateBinding;
            finalStateCombo.Properties.ValueMember = "Title";

            iconImg.ContentImage = Helper.GetImage(_transition.IconName);
            iconCombo.Properties.Items.AddRange(EventTracker.Helper.GetImageNames());
            iconCombo.Text = _transition.IconName;
            // �������
            notifyCheckBox.Checked = _transition.Enabled;
            // ������
            reportCheckBox.Checked = _transition.ViewInReport;
            // �������� ������
            soundCombo.Properties.Items.AddRange(EventTracker.Helper.GetSoundNames());
            soundCombo.Text = _transition.SoundName;
        }

        /// <summary>
        /// ������������ ��������� ������� �� ������ "���������".
        /// </summary>
        private void saveBtn_MouseEnter(object sender, EventArgs e)
        {
            notifyLbl.ForeColor = Color.DarkGreen;
            notifyLbl.Text = Resources.SaveChanges;
        }

        /// <summary>
        /// ������������ ��������� ������� �� ������ "������".
        /// </summary>
        private void cancelBtn_MouseEnter(object sender, EventArgs e)
        {
            notifyLbl.ForeColor = Color.DarkBlue;
            notifyLbl.Text = Resources.CancelChanges;
        }

        /// <summary>
        /// ������������ ���� �� ������ "������".
        /// </summary>
        private void cancelBtn_Click(object sender, EventArgs e)
        {
            if (_added)
                _transition.Delete();

            onEndEdit();
        }

        /// <summary>
        /// ������������ ��������� ������� �� ������ "����������".
        /// </summary>
        private void soundBtn_MouseEnter(object sender, EventArgs e)
        {
            notifyLbl.ForeColor = Color.SaddleBrown;
           notifyLbl.Text = Resources.AlarmSoundPlay;
        }

        /// <summary>
        /// ������������ ��������� ������� �� ������� "�������".
        /// </summary>
        private void notifyCheckBox_MouseEnter(object sender, EventArgs e)
        {
            notifyLbl.ForeColor = Color.Black;
            notifyLbl.Text = Resources.NotifyWhenHappen;
        }

        /// <summary>
        /// ������������ ��������� ������� �� ������� "�����".
        /// </summary>
        private void reportCheckBox_MouseEnter(object sender, EventArgs e)
        {
            notifyLbl.ForeColor = Color.Black;
            notifyLbl.Text = Resources.ShowInReports;
        }

        /// <summary>
        /// ������������ �������� ������� � ������ ("���������", "������" ��� "����������").
        /// </summary>
        private void btn_MouseLeave(object sender, EventArgs e)
        {
            notifyLbl.Text = "";
        }

        /// <summary>
        /// ������������ ��������� ������ ��� �������.
        /// </summary>
        private void iconCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            iconImg.ContentImage = Helper.GetImage(iconCombo.Text);
            Application.DoEvents();
        }

        /// <summary>
        /// ������������ ��������� ��������� ������� ��� �������.
        /// </summary>
        private void soundCombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            EventTracker.Helper.PlayWavFile(Globals.APP_DATA, soundCombo.Text);
        }

        /// <summary>
        /// ������������ ���� �� ������ "���������� ������".
        /// </summary>
        private void soundBtn_Click(object sender, EventArgs e)
        {
            EventTracker.Helper.PlayWavFile(Globals.APP_DATA, soundCombo.Text);
        }

        /// <summary>
        /// ������������ ��������� ���������� ���������. �� ����, ������� �� ������
        /// ��������� ��������� ��������� ���������.
        /// </summary>
        private void initialStateCombo_CloseUp( object sender, EventArgs e )
        {
            finalStateBinding.Filter = String.Format("SensorId = {0} AND Title <> '{1}'", 
                _transition.SensorId, initialStateCombo.EditValue);

            if( finalStateBinding.Count > 0 )
            {
                DataRowView drw = ( DataRowView )finalStateBinding[0];
                _transition.StateRowByState_Transition_Final = ( atlantaDataSet.StateRow )( drw ).Row;
                finalStateCombo.EditValue = _transition.StateRowByState_Transition_Final.Title;
            }
        }

        /// <summary>
        /// ������������ ���� �� ������ "���������".
        /// </summary>
        private void saveBtn_Click(object sender, EventArgs e)
        {
            _saveBtn.Enabled = false;
            // ��������� ������������ ��������� ������
            if (!validate()) return;

            _transition.Title = titleTextBox.Text;
            _transition.Enabled = notifyCheckBox.Checked;
            _transition.ViewInReport = reportCheckBox.Checked;
            _transition.IconName = iconCombo.Text;
            _transition.SoundName = soundCombo.Text;

            DataRowView drw = (DataRowView) initialStateCombo.Properties.GetDataSourceRowByKeyValue(initialStateCombo.EditValue);

            _transition.StateRowByState_Transition_Initial = (atlantaDataSet.StateRow) (drw).Row;

            drw = ( DataRowView )finalStateCombo.Properties.GetDataSourceRowByKeyValue( finalStateCombo.EditValue );

            _transition.StateRowByState_Transition_Final = (atlantaDataSet.StateRow) (drw).Row;

            _transitionAdapter.Update(_dataset.Transition);
            onEndEdit();
        }

        /// <summary>
        /// ������������ ��������� ������ ����� ��� ����� �������� �������.
        /// </summary>
        private void titleTextBox_Enter(object sender, EventArgs e)
        {
            if (titleTextBox.BackColor != Color.White)
            {
                titleTextBox.BackColor = Color.White;
                titleTextBox.ForeColor = Color.Black;
                titleTextBox.Text = "";
            }
        }
    }
}
