using System;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using TrackControl.General;
using TrackControl.General.Users;
using TrackControl.Properties;
using TrackControl.Reports;
using TrackControl.AccessObjects;
using System.Collections.Generic;
using System.Data;  

namespace TrackControl.Setting
{
    public partial class UsersList : DevExpress.XtraEditors.XtraUserControl
    {
        List<IUserAccessObject> _objects;
        UserRole _activeRole;
        IUserAccessObject _activeObject;

        public UsersList()
        {
            InitializeComponent();
            Localization();

            sbSaveObjectValues.MouseEnter += SbSaveObjectValuesOnMouseEnter;
            sbSaveObjectValues.MouseLeave += SbSaveObjectValuesOnMouseLeave;
            sbEdit.ToolTip = "������������� ����";

        }

        private void SbSaveObjectValuesOnMouseLeave(object sender, EventArgs eventArgs)
        {
            sbSaveObjectValues.ToolTip = "��������� ���������";
        }

        private void SbSaveObjectValuesOnMouseEnter(object sender, EventArgs eventArgs)
        {
            sbSaveObjectValues.ToolTip = "��������� ���������";
        }

        #region GridUsersList

        private void gvUsers_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            UserBase ub = GetCurrentUserBase();

            if (ub == null) 
                return;

            if (e.Column.Name != "colPassword")
            {
                ub.Save();
            }
        }

        private void gvUsers_CellValueChanging(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            UserBase ub = GetCurrentUserBase();

            if (ub == null) 
                return;

            if (e.Column.Name == "colPassword")
            {
                ub.PasswordGrid = e.Value.ToString();
                ub.Save();
            }
        }

        private void gcUsers_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            if (e.Button.ButtonType == NavigatorButtonType.Remove)
            {
                e.Handled = !DeleteRowUsers();
            }
        }

        bool DeleteRowUsers()
        {
            UserBase ub = GetCurrentUserBase();
            if (ub != null)
            {
                if (XtraMessageBox.Show(Resources.ConfirmDeleteQuestion,
               Resources.ConfirmDeletion,
               MessageBoxButtons.YesNoCancel,
               MessageBoxIcon.Question) == DialogResult.Yes)
                {

                    return ub.Delete();
                }
                else
                    return false;
            }
            else
                return false;
        }

        private UserBase GetCurrentUserBase()
        {
            CurrencyManager cm = (CurrencyManager)this.BindingContext[gcUsers.DataSource];

            if (cm.Position == -1) 
                return null; 

            UserBase ub = (UserBase)cm.Current;
            return ub;
        }

        private void gvUsers_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {
            UserBase ub = GetCurrentUserBase();
            if (!ub.IsPasswordSet())
            {
                e.Valid = false;
                gvUsers.SetColumnError(colPassword, Resources.PasswordNeeded);
                return;
            }
            if (gvUsers.GetRowCellValue(e.RowHandle, colWinLogin) == null) return;
            string winLogin = gvUsers.GetRowCellValue(e.RowHandle, colWinLogin).ToString();
            if (ub.IsWinLoginExist(winLogin))
            {
                e.Valid = false;
                gvUsers.SetColumnError(colWinLogin, Resources.ValuesNeedUnicum);
            }
        }

        private void gvUsers_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            UserBase ub = GetCurrentUserBase();
            if (ub == null) return;
            if (ub.Role != null)
            {
                leUserRoles.EditValue = ub.Role.Id;
                SetCurrentUserRole();
            }
        }

        #endregion

        #region Roles

        private void SetCurrentUserRole()
        {
            _activeRole = (UserRole)leUserRoles.Properties.GetDataSourceRowByKeyValue(leUserRoles.EditValue);
        }

        private void gvRoles_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            SetRoleForTypesObjects();
        }

        void SetRoleForTypesObjects()
        {
            if (_objects == null) return;
            SetCurrentUserRole();

            if (_activeObject != null) 
                _activeObject.AccessRole = _activeRole; 
         }

        private void leUserRoles_EditValueChanged(object sender, EventArgs e)
        {

            ViewObjectTypeControl();
        }
        #endregion

        #region TypesObjects

        private void InitObjectTypes()
        {
            _objects = new List<IUserAccessObject>();
            UserAccessZones azones = new UserAccessZones();
            _objects.Add(azones);
            UserAccessVehicles aveh = new UserAccessVehicles();
            _objects.Add(aveh);
            UserAccessReports arep = new UserAccessReports();
            _objects.Add(arep);
            UserAccessModules amod = new UserAccessModules();
            _objects.Add(amod);
        }

        private void gvObjectTypes_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            ViewObjectTypeControl();
        }

        void ViewObjectTypeControl()
        {
            pnViews.Controls.Clear();
            SetCurrentObjectType();
            SetCurrentUserRole();
            if (_activeObject == null) return;
            if (_activeRole == null) return;
            _activeObject.ParentDataViewer = pnViews;
            _activeObject.AccessRole = _activeRole;
        }

        private void SetCurrentObjectType()
        {
            foreach (IUserAccessObject uao in _objects)
            {
                if (uao.ObjectType == (int)rgObjectTypes.EditValue)
                {
                    _activeObject = uao;
                    return;
                }
            }
        }

        private void rgObjectTypes_SelectedIndexChanged(object sender, EventArgs e)
        {
            ViewObjectTypeControl();
        }

        private void sbSaveObjectValues_Click(object sender, EventArgs e)
        {
            if (_activeObject!=null) 
                _activeObject.SaveUnvisibleRoleItems();
        }

        #endregion

        private void UsersList_Load(object sender, EventArgs e)
        {
            InitObjectTypes();
            OnRefreshRolesList();
            SetFirstItemInRolesList();
            gcUsers.DataSource = UserBaseProvider.GetList(); 
        }

        private void SetFirstItemInRolesList()
        {
            int position = -1;

            if (leUserRoles.EditValue != null)
            {
                position = leUserRoles.Properties.GetDataSourceRowIndex(leUserRoles.Properties.ValueMember,
                    leUserRoles.EditValue);
            }

            if (position == -1)
            {
                leUserRoles.EditValue = leUserRoles.Properties.GetDataSourceValue(leUserRoles.Properties.ValueMember, 0);
            }
        }

        private void sbEdit_Click(object sender, EventArgs e)
        {
            UserRolesEditor formEditor = new UserRolesEditor(new Point(pnViews.Left + splitContainerControl.Panel2.Left, pnViews.Top + splitContainerControl.Panel2.Top)); 
            formEditor.RefreshRolesList += OnRefreshRolesList;
            formEditor.ShowDialog();  
        }

        void OnRefreshRolesList()
        {
            leRoles.DataSource = UserBaseProvider.GetRolesList();
            leUserRoles.Properties.DataSource = UserBaseProvider.GetRolesList();
        }

        void Localization()
        {
            colName.Caption = Resources.User;
            colPassword.Caption = Resources.Password;
            colAdmin.Caption = Resources.AdminBrief;
            sbSaveObjectValues.Image = Shared.Save ;
            lbRoles.Text = Resources.Roles;
            lbObjects.Text = Resources.AccessObjects;
            leUserRoles.Properties.NullText = Resources.SelectRole;
            rgObjectTypes.Properties.Items[0].Description = Resources.CheckZones;
            rgObjectTypes.Properties.Items[1].Description = Resources.Reports;
            rgObjectTypes.Properties.Items[2].Description = Resources.Vehicles;
        }
    }
}
