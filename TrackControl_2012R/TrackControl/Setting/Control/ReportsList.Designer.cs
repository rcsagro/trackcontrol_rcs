namespace TrackControl.Setting
{
    partial class ReportsList
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gcRList = new DevExpress.XtraGrid.GridControl();
            this.gvRList = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colVisible = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCaption = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colType = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.gcRList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvRList)).BeginInit();
            this.SuspendLayout();
            // 
            // gcRList
            // 
            this.gcRList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcRList.Location = new System.Drawing.Point(0, 0);
            this.gcRList.MainView = this.gvRList;
            this.gcRList.Name = "gcRList";
            this.gcRList.Size = new System.Drawing.Size(449, 446);
            this.gcRList.TabIndex = 0;
            this.gcRList.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvRList});
            // 
            // gvRList
            // 
            this.gvRList.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvRList.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvRList.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gvRList.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvRList.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvRList.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvRList.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvRList.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvRList.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gvRList.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvRList.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvRList.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvRList.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvRList.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gvRList.Appearance.Empty.Options.UseBackColor = true;
            this.gvRList.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvRList.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvRList.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gvRList.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvRList.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gvRList.Appearance.EvenRow.Options.UseForeColor = true;
            this.gvRList.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvRList.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvRList.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gvRList.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvRList.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvRList.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gvRList.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvRList.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gvRList.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvRList.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvRList.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvRList.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.gvRList.Appearance.FixedLine.Options.UseBackColor = true;
            this.gvRList.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gvRList.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gvRList.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gvRList.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gvRList.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(192)))), ((int)(((byte)(157)))));
            this.gvRList.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(219)))), ((int)(((byte)(188)))));
            this.gvRList.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gvRList.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvRList.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gvRList.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvRList.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvRList.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvRList.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvRList.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvRList.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvRList.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gvRList.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvRList.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvRList.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvRList.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvRList.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvRList.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvRList.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gvRList.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvRList.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvRList.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gvRList.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(242)))), ((int)(((byte)(213)))));
            this.gvRList.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gvRList.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gvRList.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvRList.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvRList.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvRList.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvRList.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gvRList.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvRList.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gvRList.Appearance.GroupRow.Options.UseForeColor = true;
            this.gvRList.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvRList.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvRList.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gvRList.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvRList.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvRList.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gvRList.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvRList.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvRList.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gvRList.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvRList.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.gvRList.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gvRList.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvRList.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvRList.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvRList.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvRList.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gvRList.Appearance.OddRow.Options.UseBackColor = true;
            this.gvRList.Appearance.OddRow.Options.UseBorderColor = true;
            this.gvRList.Appearance.OddRow.Options.UseForeColor = true;
            this.gvRList.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(247)))));
            this.gvRList.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gvRList.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(148)))), ((int)(((byte)(148)))));
            this.gvRList.Appearance.Preview.Options.UseBackColor = true;
            this.gvRList.Appearance.Preview.Options.UseFont = true;
            this.gvRList.Appearance.Preview.Options.UseForeColor = true;
            this.gvRList.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvRList.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gvRList.Appearance.Row.Options.UseBackColor = true;
            this.gvRList.Appearance.Row.Options.UseForeColor = true;
            this.gvRList.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvRList.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gvRList.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvRList.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.gvRList.Appearance.SelectedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvRList.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.gvRList.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvRList.Appearance.SelectedRow.Options.UseBorderColor = true;
            this.gvRList.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gvRList.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gvRList.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gvRList.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvRList.Appearance.VertLine.Options.UseBackColor = true;
            this.gvRList.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colVisible,
            this.colCaption,
            this.colType});
            this.gvRList.GridControl = this.gcRList;
            this.gvRList.IndicatorWidth = 30;
            this.gvRList.Name = "gvRList";
            this.gvRList.OptionsView.EnableAppearanceEvenRow = true;
            this.gvRList.OptionsView.EnableAppearanceOddRow = true;
            this.gvRList.OptionsView.ShowGroupPanel = false;
            this.gvRList.CustomDrawRowIndicator += new DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventHandler(this.gvRList_CustomDrawRowIndicator);
            // 
            // colVisible
            // 
            this.colVisible.AppearanceHeader.Options.UseTextOptions = true;
            this.colVisible.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colVisible.Caption = "���������";
            this.colVisible.FieldName = "Visible";
            this.colVisible.Name = "colVisible";
            this.colVisible.Visible = true;
            this.colVisible.VisibleIndex = 0;
            this.colVisible.Width = 255;
            // 
            // colCaption
            // 
            this.colCaption.AppearanceHeader.Options.UseTextOptions = true;
            this.colCaption.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCaption.Caption = "�������� ������";
            this.colCaption.FieldName = "Caption";
            this.colCaption.Name = "colCaption";
            this.colCaption.OptionsColumn.AllowEdit = false;
            this.colCaption.OptionsColumn.ReadOnly = true;
            this.colCaption.Visible = true;
            this.colCaption.VisibleIndex = 1;
            this.colCaption.Width = 673;
            // 
            // colType
            // 
            this.colType.Caption = "���";
            this.colType.FieldName = "TypeName";
            this.colType.Name = "colType";
            this.colType.Width = 322;
            // 
            // ReportsList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gcRList);
            this.Name = "ReportsList";
            this.Size = new System.Drawing.Size(449, 446);
            this.Load += new System.EventHandler(this.ReportsList_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gcRList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvRList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gcRList;
        private DevExpress.XtraGrid.Views.Grid.GridView gvRList;
        private DevExpress.XtraGrid.Columns.GridColumn colVisible;
        private DevExpress.XtraGrid.Columns.GridColumn colCaption;
        private DevExpress.XtraGrid.Columns.GridColumn colType;
    }
}
