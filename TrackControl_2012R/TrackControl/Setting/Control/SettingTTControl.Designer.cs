using LocalCache;
namespace TrackControl.Setting
{
  partial class SettingTTControl
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.components = new System.ComponentModel.Container();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
        System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingTTControl));
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
        this.splitContainer1 = new System.Windows.Forms.SplitContainer();
        this.splitContainer2 = new System.Windows.Forms.SplitContainer();
        this.settingDataGridView = new System.Windows.Forms.DataGridView();
        this.settingBindingSource = new System.Windows.Forms.BindingSource(this.components);
        this.atlantaDataSet = new LocalCache.atlantaDataSet();
        this.settingBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
        this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
        this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
        this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
        this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
        this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
        this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
        this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
        this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
        this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
        this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
        this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
        this.settingBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
        this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
        this.assignToolStripButton = new System.Windows.Forms.ToolStripButton();
        this.refreshToolStripButton = new System.Windows.Forms.ToolStripButton();
        this.vehicleDataGridView = new System.Windows.Forms.DataGridView();
        this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.dataGridViewTextBoxColumn21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.vehicleBindingSource = new System.Windows.Forms.BindingSource(this.components);
        this.vehicleBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
        this.bindingNavigatorCountItem2 = new System.Windows.Forms.ToolStripLabel();
        this.bindingNavigatorMoveFirstItem2 = new System.Windows.Forms.ToolStripButton();
        this.bindingNavigatorMovePreviousItem2 = new System.Windows.Forms.ToolStripButton();
        this.bindingNavigatorSeparator6 = new System.Windows.Forms.ToolStripSeparator();
        this.bindingNavigatorPositionItem2 = new System.Windows.Forms.ToolStripTextBox();
        this.bindingNavigatorSeparator7 = new System.Windows.Forms.ToolStripSeparator();
        this.bindingNavigatorMoveNextItem2 = new System.Windows.Forms.ToolStripButton();
        this.bindingNavigatorMoveLastItem2 = new System.Windows.Forms.ToolStripButton();
        this.settingTableAdapter = new LocalCache.atlantaDataSetTableAdapters.SettingTableAdapter();
        this.vehicleTableAdapter = new LocalCache.atlantaDataSetTableAdapters.VehicleTableAdapter();
        this.statusStrip1 = new System.Windows.Forms.StatusStrip();
        this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
        this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.colTimeBreakMaxPermitted = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.FuelApproximationTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.AvgFuelRatePerHour = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.FuelrateWithDischarge = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.TimeGetFuelAfterStop = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.TimeGetFuelBeforeMotion = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.FuelingMinMaxAlgorithm = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.CzMinCrossingPairTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.FuelerRadiusFinds = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.FuelerMaxTimeStop = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.FuelerMinFuelrate = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.FuelerCountInMotion = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.splitContainer1.Panel1.SuspendLayout();
        this.splitContainer1.Panel2.SuspendLayout();
        this.splitContainer1.SuspendLayout();
        this.splitContainer2.Panel1.SuspendLayout();
        this.splitContainer2.SuspendLayout();
        ((System.ComponentModel.ISupportInitialize)(this.settingDataGridView)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.settingBindingSource)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.atlantaDataSet)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.settingBindingNavigator)).BeginInit();
        this.settingBindingNavigator.SuspendLayout();
        ((System.ComponentModel.ISupportInitialize)(this.vehicleDataGridView)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.vehicleBindingSource)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.vehicleBindingNavigator)).BeginInit();
        this.vehicleBindingNavigator.SuspendLayout();
        this.statusStrip1.SuspendLayout();
        this.SuspendLayout();
        // 
        // splitContainer1
        // 
        this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
        this.splitContainer1.Location = new System.Drawing.Point(0, 0);
        this.splitContainer1.Name = "splitContainer1";
        // 
        // splitContainer1.Panel1
        // 
        this.splitContainer1.Panel1.AutoScroll = true;
        this.splitContainer1.Panel1.Controls.Add(this.splitContainer2);
        // 
        // splitContainer1.Panel2
        // 
        this.splitContainer1.Panel2.AutoScroll = true;
        this.splitContainer1.Panel2.Controls.Add(this.vehicleDataGridView);
        this.splitContainer1.Panel2.Controls.Add(this.vehicleBindingNavigator);
        this.splitContainer1.Size = new System.Drawing.Size(830, 436);
        this.splitContainer1.SplitterDistance = 568;
        this.splitContainer1.TabIndex = 0;
        // 
        // splitContainer2
        // 
        this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
        this.splitContainer2.Location = new System.Drawing.Point(0, 0);
        this.splitContainer2.Name = "splitContainer2";
        this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
        // 
        // splitContainer2.Panel1
        // 
        this.splitContainer2.Panel1.Controls.Add(this.settingDataGridView);
        this.splitContainer2.Panel1.Controls.Add(this.settingBindingNavigator);
        // 
        // splitContainer2.Panel2
        // 
        this.splitContainer2.Panel2.AutoScroll = true;
        this.splitContainer2.Size = new System.Drawing.Size(568, 436);
        this.splitContainer2.SplitterDistance = 407;
        this.splitContainer2.TabIndex = 2;
        // 
        // settingDataGridView
        // 
        this.settingDataGridView.AllowUserToAddRows = false;
        this.settingDataGridView.AutoGenerateColumns = false;
        dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
        dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
        dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
        dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
        dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
        dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
        dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
        this.settingDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
        this.settingDataGridView.ColumnHeadersHeight = 46;
        this.settingDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn13,
            this.dataGridViewTextBoxColumn14,
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn2,
            this.colTimeBreakMaxPermitted,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn6,
            this.FuelApproximationTime,
            this.AvgFuelRatePerHour,
            this.FuelrateWithDischarge,
            this.TimeGetFuelAfterStop,
            this.TimeGetFuelBeforeMotion,
            this.FuelingMinMaxAlgorithm,
            this.CzMinCrossingPairTime,
            this.FuelerRadiusFinds,
            this.FuelerMaxTimeStop,
            this.FuelerMinFuelrate,
            this.FuelerCountInMotion});
        this.settingDataGridView.DataSource = this.settingBindingSource;
        dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
        dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
        dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
        dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
        dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
        dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
        dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
        this.settingDataGridView.DefaultCellStyle = dataGridViewCellStyle4;
        this.settingDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
        this.settingDataGridView.Location = new System.Drawing.Point(0, 25);
        this.settingDataGridView.Name = "settingDataGridView";
        dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
        dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
        dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
        dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
        dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
        dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
        dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
        this.settingDataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle5;
        this.settingDataGridView.RowHeadersVisible = false;
        this.settingDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
        this.settingDataGridView.Size = new System.Drawing.Size(568, 382);
        this.settingDataGridView.TabIndex = 0;
        // 
        // settingBindingSource
        // 
        this.settingBindingSource.DataMember = "setting";
        this.settingBindingSource.DataSource = this.atlantaDataSet;
        // 
        // atlantaDataSet
        // 
        this.atlantaDataSet.DataSetName = "atlantaDataSet";
        this.atlantaDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
        // 
        // settingBindingNavigator
        // 
        this.settingBindingNavigator.AddNewItem = null;
        this.settingBindingNavigator.BindingSource = this.settingBindingSource;
        this.settingBindingNavigator.CountItem = this.bindingNavigatorCountItem;
        this.settingBindingNavigator.DeleteItem = this.bindingNavigatorDeleteItem;
        this.settingBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.settingBindingNavigatorSaveItem,
            this.toolStripSeparator1,
            this.assignToolStripButton,
            this.refreshToolStripButton});
        this.settingBindingNavigator.Location = new System.Drawing.Point(0, 0);
        this.settingBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
        this.settingBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
        this.settingBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
        this.settingBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
        this.settingBindingNavigator.Name = "settingBindingNavigator";
        this.settingBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
        this.settingBindingNavigator.Size = new System.Drawing.Size(568, 25);
        this.settingBindingNavigator.TabIndex = 1;
        this.settingBindingNavigator.Text = "���������";
        // 
        // bindingNavigatorCountItem
        // 
        this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
        this.bindingNavigatorCountItem.Size = new System.Drawing.Size(36, 22);
        this.bindingNavigatorCountItem.Text = "of {0}";
        this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
        // 
        // bindingNavigatorDeleteItem
        // 
        this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
        this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
        this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
        this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
        this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
        this.bindingNavigatorDeleteItem.Text = "Delete";
        this.bindingNavigatorDeleteItem.Click += new System.EventHandler(this.bindingNavigatorDeleteItem_Click);
        // 
        // bindingNavigatorMoveFirstItem
        // 
        this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
        this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
        this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
        this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
        this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
        this.bindingNavigatorMoveFirstItem.Text = "Move first";
        // 
        // bindingNavigatorMovePreviousItem
        // 
        this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
        this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
        this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
        this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
        this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
        this.bindingNavigatorMovePreviousItem.Text = "Move previous";
        // 
        // bindingNavigatorSeparator
        // 
        this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
        this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
        // 
        // bindingNavigatorPositionItem
        // 
        this.bindingNavigatorPositionItem.AccessibleName = "Position";
        this.bindingNavigatorPositionItem.AutoSize = false;
        this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
        this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 21);
        this.bindingNavigatorPositionItem.Text = "0";
        this.bindingNavigatorPositionItem.ToolTipText = "Current position";
        // 
        // bindingNavigatorSeparator1
        // 
        this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
        this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
        // 
        // bindingNavigatorMoveNextItem
        // 
        this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
        this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
        this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
        this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
        this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
        this.bindingNavigatorMoveNextItem.Text = "Move next";
        // 
        // bindingNavigatorMoveLastItem
        // 
        this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
        this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
        this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
        this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
        this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
        this.bindingNavigatorMoveLastItem.Text = "Move last";
        // 
        // bindingNavigatorSeparator2
        // 
        this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
        this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
        // 
        // bindingNavigatorAddNewItem
        // 
        this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
        this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
        this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
        this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
        this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
        this.bindingNavigatorAddNewItem.Text = "Add new";
        this.bindingNavigatorAddNewItem.Click += new System.EventHandler(this.bindingNavigatorAddNewItem_Click);
        // 
        // settingBindingNavigatorSaveItem
        // 
        this.settingBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
        this.settingBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("settingBindingNavigatorSaveItem.Image")));
        this.settingBindingNavigatorSaveItem.Name = "settingBindingNavigatorSaveItem";
        this.settingBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
        this.settingBindingNavigatorSaveItem.Text = "Save Data";
        this.settingBindingNavigatorSaveItem.Click += new System.EventHandler(this.settingBindingNavigatorSaveItem_Click);
        // 
        // toolStripSeparator1
        // 
        this.toolStripSeparator1.Name = "toolStripSeparator1";
        this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
        // 
        // assignToolStripButton
        // 
        this.assignToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
        this.assignToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("assignToolStripButton.Image")));
        this.assignToolStripButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
        this.assignToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
        this.assignToolStripButton.Name = "assignToolStripButton";
        this.assignToolStripButton.Size = new System.Drawing.Size(23, 22);
        this.assignToolStripButton.Text = "���������";
        this.assignToolStripButton.Click += new System.EventHandler(this.assignToolStripButton_Click);
        // 
        // refreshToolStripButton
        // 
        this.refreshToolStripButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
        this.refreshToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
        this.refreshToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("refreshToolStripButton.Image")));
        this.refreshToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
        this.refreshToolStripButton.Name = "refreshToolStripButton";
        this.refreshToolStripButton.Size = new System.Drawing.Size(23, 22);
        this.refreshToolStripButton.Text = "��������";
        this.refreshToolStripButton.ToolTipText = "�������� ������";
        this.refreshToolStripButton.Click += new System.EventHandler(this.refreshToolStripButton_Click);
        // 
        // vehicleDataGridView
        // 
        this.vehicleDataGridView.AllowUserToAddRows = false;
        this.vehicleDataGridView.AutoGenerateColumns = false;
        dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
        dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
        dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
        dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
        dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
        dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
        dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
        this.vehicleDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle6;
        this.vehicleDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn17,
            this.dataGridViewTextBoxColumn16,
            this.dataGridViewTextBoxColumn21});
        this.vehicleDataGridView.DataSource = this.vehicleBindingSource;
        dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
        dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Window;
        dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
        dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.ControlText;
        dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
        dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
        dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
        this.vehicleDataGridView.DefaultCellStyle = dataGridViewCellStyle7;
        this.vehicleDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
        this.vehicleDataGridView.Location = new System.Drawing.Point(0, 25);
        this.vehicleDataGridView.MultiSelect = false;
        this.vehicleDataGridView.Name = "vehicleDataGridView";
        this.vehicleDataGridView.ReadOnly = true;
        dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
        dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control;
        dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
        dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText;
        dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
        dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
        dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
        this.vehicleDataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle8;
        this.vehicleDataGridView.RowHeadersVisible = false;
        this.vehicleDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
        this.vehicleDataGridView.Size = new System.Drawing.Size(258, 411);
        this.vehicleDataGridView.TabIndex = 5;
        this.vehicleDataGridView.SelectionChanged += new System.EventHandler(this.vehicleDataGridView_SelectionChanged);
        // 
        // dataGridViewTextBoxColumn17
        // 
        this.dataGridViewTextBoxColumn17.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
        this.dataGridViewTextBoxColumn17.DataPropertyName = "NumberPlate";
        this.dataGridViewTextBoxColumn17.FillWeight = 60F;
        this.dataGridViewTextBoxColumn17.HeaderText = "�����";
        this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
        this.dataGridViewTextBoxColumn17.ReadOnly = true;
        this.dataGridViewTextBoxColumn17.ToolTipText = "����� ������������� ��������";
        this.dataGridViewTextBoxColumn17.Width = 60;
        // 
        // dataGridViewTextBoxColumn16
        // 
        this.dataGridViewTextBoxColumn16.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
        this.dataGridViewTextBoxColumn16.DataPropertyName = "MakeCar";
        this.dataGridViewTextBoxColumn16.HeaderText = "�����";
        this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
        this.dataGridViewTextBoxColumn16.ReadOnly = true;
        this.dataGridViewTextBoxColumn16.ToolTipText = "����� ������������� ��������";
        // 
        // dataGridViewTextBoxColumn21
        // 
        this.dataGridViewTextBoxColumn21.DataPropertyName = "CarModel";
        this.dataGridViewTextBoxColumn21.HeaderText = "������";
        this.dataGridViewTextBoxColumn21.Name = "dataGridViewTextBoxColumn21";
        this.dataGridViewTextBoxColumn21.ReadOnly = true;
        this.dataGridViewTextBoxColumn21.ToolTipText = "������ ������������� ��������";
        // 
        // vehicleBindingSource
        // 
        this.vehicleBindingSource.DataMember = "vehicle";
        this.vehicleBindingSource.DataSource = this.atlantaDataSet;
        // 
        // vehicleBindingNavigator
        // 
        this.vehicleBindingNavigator.AddNewItem = null;
        this.vehicleBindingNavigator.BindingSource = this.vehicleBindingSource;
        this.vehicleBindingNavigator.CountItem = this.bindingNavigatorCountItem2;
        this.vehicleBindingNavigator.DeleteItem = null;
        this.vehicleBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem2,
            this.bindingNavigatorMovePreviousItem2,
            this.bindingNavigatorSeparator6,
            this.bindingNavigatorPositionItem2,
            this.bindingNavigatorCountItem2,
            this.bindingNavigatorSeparator7,
            this.bindingNavigatorMoveNextItem2,
            this.bindingNavigatorMoveLastItem2});
        this.vehicleBindingNavigator.Location = new System.Drawing.Point(0, 0);
        this.vehicleBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem2;
        this.vehicleBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem2;
        this.vehicleBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem2;
        this.vehicleBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem2;
        this.vehicleBindingNavigator.Name = "vehicleBindingNavigator";
        this.vehicleBindingNavigator.PositionItem = this.bindingNavigatorPositionItem2;
        this.vehicleBindingNavigator.Size = new System.Drawing.Size(258, 25);
        this.vehicleBindingNavigator.TabIndex = 3;
        this.vehicleBindingNavigator.Text = "bindingNavigator1";
        // 
        // bindingNavigatorCountItem2
        // 
        this.bindingNavigatorCountItem2.Name = "bindingNavigatorCountItem2";
        this.bindingNavigatorCountItem2.Size = new System.Drawing.Size(36, 22);
        this.bindingNavigatorCountItem2.Text = "of {0}";
        this.bindingNavigatorCountItem2.ToolTipText = "Total number of items";
        // 
        // bindingNavigatorMoveFirstItem2
        // 
        this.bindingNavigatorMoveFirstItem2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
        this.bindingNavigatorMoveFirstItem2.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem2.Image")));
        this.bindingNavigatorMoveFirstItem2.Name = "bindingNavigatorMoveFirstItem2";
        this.bindingNavigatorMoveFirstItem2.RightToLeftAutoMirrorImage = true;
        this.bindingNavigatorMoveFirstItem2.Size = new System.Drawing.Size(23, 22);
        this.bindingNavigatorMoveFirstItem2.Text = "Move first";
        // 
        // bindingNavigatorMovePreviousItem2
        // 
        this.bindingNavigatorMovePreviousItem2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
        this.bindingNavigatorMovePreviousItem2.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem2.Image")));
        this.bindingNavigatorMovePreviousItem2.Name = "bindingNavigatorMovePreviousItem2";
        this.bindingNavigatorMovePreviousItem2.RightToLeftAutoMirrorImage = true;
        this.bindingNavigatorMovePreviousItem2.Size = new System.Drawing.Size(23, 22);
        this.bindingNavigatorMovePreviousItem2.Text = "Move previous";
        // 
        // bindingNavigatorSeparator6
        // 
        this.bindingNavigatorSeparator6.Name = "bindingNavigatorSeparator6";
        this.bindingNavigatorSeparator6.Size = new System.Drawing.Size(6, 25);
        // 
        // bindingNavigatorPositionItem2
        // 
        this.bindingNavigatorPositionItem2.AccessibleName = "Position";
        this.bindingNavigatorPositionItem2.AutoSize = false;
        this.bindingNavigatorPositionItem2.Name = "bindingNavigatorPositionItem2";
        this.bindingNavigatorPositionItem2.Size = new System.Drawing.Size(50, 21);
        this.bindingNavigatorPositionItem2.Text = "0";
        this.bindingNavigatorPositionItem2.ToolTipText = "Current position";
        // 
        // bindingNavigatorSeparator7
        // 
        this.bindingNavigatorSeparator7.Name = "bindingNavigatorSeparator7";
        this.bindingNavigatorSeparator7.Size = new System.Drawing.Size(6, 25);
        // 
        // bindingNavigatorMoveNextItem2
        // 
        this.bindingNavigatorMoveNextItem2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
        this.bindingNavigatorMoveNextItem2.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem2.Image")));
        this.bindingNavigatorMoveNextItem2.Name = "bindingNavigatorMoveNextItem2";
        this.bindingNavigatorMoveNextItem2.RightToLeftAutoMirrorImage = true;
        this.bindingNavigatorMoveNextItem2.Size = new System.Drawing.Size(23, 22);
        this.bindingNavigatorMoveNextItem2.Text = "Move next";
        // 
        // bindingNavigatorMoveLastItem2
        // 
        this.bindingNavigatorMoveLastItem2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
        this.bindingNavigatorMoveLastItem2.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem2.Image")));
        this.bindingNavigatorMoveLastItem2.Name = "bindingNavigatorMoveLastItem2";
        this.bindingNavigatorMoveLastItem2.RightToLeftAutoMirrorImage = true;
        this.bindingNavigatorMoveLastItem2.Size = new System.Drawing.Size(23, 22);
        this.bindingNavigatorMoveLastItem2.Text = "Move last";
        // 
        // settingTableAdapter
        // 
        this.settingTableAdapter.ClearBeforeFill = true;
        // 
        // vehicleTableAdapter
        // 
        this.vehicleTableAdapter.ClearBeforeFill = true;
        // 
        // statusStrip1
        // 
        this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
        this.statusStrip1.Location = new System.Drawing.Point(0, 436);
        this.statusStrip1.Name = "statusStrip1";
        this.statusStrip1.Size = new System.Drawing.Size(830, 22);
        this.statusStrip1.TabIndex = 1;
        this.statusStrip1.Text = "statusStrip1";
        // 
        // toolStripStatusLabel1
        // 
        this.toolStripStatusLabel1.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Right;
        this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
        this.toolStripStatusLabel1.Size = new System.Drawing.Size(146, 17);
        this.toolStripStatusLabel1.Text = "                                             ";
        // 
        // dataGridViewTextBoxColumn13
        // 
        this.dataGridViewTextBoxColumn13.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
        this.dataGridViewTextBoxColumn13.DataPropertyName = "Name";
        this.dataGridViewTextBoxColumn13.HeaderText = "��������";
        this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
        this.dataGridViewTextBoxColumn13.ToolTipText = "�������� ���������";
        this.dataGridViewTextBoxColumn13.Width = 82;
        // 
        // dataGridViewTextBoxColumn14
        // 
        this.dataGridViewTextBoxColumn14.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
        this.dataGridViewTextBoxColumn14.DataPropertyName = "Desc";
        this.dataGridViewTextBoxColumn14.HeaderText = "��������";
        this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
        this.dataGridViewTextBoxColumn14.ToolTipText = "�������� ���������";
        this.dataGridViewTextBoxColumn14.Width = 82;
        // 
        // dataGridViewTextBoxColumn10
        // 
        this.dataGridViewTextBoxColumn10.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
        this.dataGridViewTextBoxColumn10.DataPropertyName = "speedMax";
        this.dataGridViewTextBoxColumn10.HeaderText = "��������";
        this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
        this.dataGridViewTextBoxColumn10.ToolTipText = "��������";
        this.dataGridViewTextBoxColumn10.Width = 80;
        // 
        // dataGridViewTextBoxColumn8
        // 
        this.dataGridViewTextBoxColumn8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
        this.dataGridViewTextBoxColumn8.DataPropertyName = "accelMax";
        this.dataGridViewTextBoxColumn8.HeaderText = "���������";
        this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
        this.dataGridViewTextBoxColumn8.ToolTipText = "���������";
        this.dataGridViewTextBoxColumn8.Width = 88;
        // 
        // dataGridViewTextBoxColumn9
        // 
        this.dataGridViewTextBoxColumn9.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
        this.dataGridViewTextBoxColumn9.DataPropertyName = "breakMax";
        this.dataGridViewTextBoxColumn9.HeaderText = "����������";
        this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
        this.dataGridViewTextBoxColumn9.ToolTipText = "����������";
        this.dataGridViewTextBoxColumn9.Width = 97;
        // 
        // dataGridViewTextBoxColumn2
        // 
        this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
        this.dataGridViewTextBoxColumn2.DataPropertyName = "TimeBreak";
        dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
        dataGridViewCellStyle2.Format = "T";
        dataGridViewCellStyle2.NullValue = null;
        this.dataGridViewTextBoxColumn2.DefaultCellStyle = dataGridViewCellStyle2;
        this.dataGridViewTextBoxColumn2.HeaderText = "����� �������";
        this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
        this.dataGridViewTextBoxColumn2.ToolTipText = "����� �������";
        // 
        // colTimeBreakMaxPermitted
        // 
        this.colTimeBreakMaxPermitted.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
        this.colTimeBreakMaxPermitted.DataPropertyName = "TimeBreakMaxPermitted";
        dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
        dataGridViewCellStyle3.Format = "T";
        this.colTimeBreakMaxPermitted.DefaultCellStyle = dataGridViewCellStyle3;
        this.colTimeBreakMaxPermitted.HeaderText = "����. ����� ����������� �������";
        this.colTimeBreakMaxPermitted.Name = "colTimeBreakMaxPermitted";
        this.colTimeBreakMaxPermitted.ToolTipText = "����. ����� ����������� �������";
        this.colTimeBreakMaxPermitted.Width = 132;
        // 
        // dataGridViewTextBoxColumn3
        // 
        this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
        this.dataGridViewTextBoxColumn3.DataPropertyName = "RotationMain";
        this.dataGridViewTextBoxColumn3.HeaderText = "������� ���������";
        this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
        this.dataGridViewTextBoxColumn3.ToolTipText = "������� ���������";
        this.dataGridViewTextBoxColumn3.Width = 121;
        // 
        // dataGridViewTextBoxColumn5
        // 
        this.dataGridViewTextBoxColumn5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
        this.dataGridViewTextBoxColumn5.DataPropertyName = "FuelingEdge";
        this.dataGridViewTextBoxColumn5.HeaderText = "����� ��������";
        this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
        this.dataGridViewTextBoxColumn5.ToolTipText = "����� ��������";
        this.dataGridViewTextBoxColumn5.Width = 105;
        // 
        // dataGridViewTextBoxColumn7
        // 
        this.dataGridViewTextBoxColumn7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
        this.dataGridViewTextBoxColumn7.DataPropertyName = "FuelingDischarge";
        this.dataGridViewTextBoxColumn7.HeaderText = "����� �����";
        this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
        this.dataGridViewTextBoxColumn7.ToolTipText = "����� �����";
        this.dataGridViewTextBoxColumn7.Width = 88;
        // 
        // dataGridViewTextBoxColumn6
        // 
        this.dataGridViewTextBoxColumn6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
        this.dataGridViewTextBoxColumn6.DataPropertyName = "band";
        this.dataGridViewTextBoxColumn6.HeaderText = "������ �����������";
        this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
        this.dataGridViewTextBoxColumn6.ToolTipText = "������ �����������";
        this.dataGridViewTextBoxColumn6.Width = 128;
        // 
        // FuelApproximationTime
        // 
        this.FuelApproximationTime.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
        this.FuelApproximationTime.DataPropertyName = "FuelApproximationTime";
        this.FuelApproximationTime.HeaderText = "����� ����������� ������ �������";
        this.FuelApproximationTime.Name = "FuelApproximationTime";
        this.FuelApproximationTime.ToolTipText = "����� ����������� ������ �������";
        this.FuelApproximationTime.Width = 125;
        // 
        // AvgFuelRatePerHour
        // 
        this.AvgFuelRatePerHour.DataPropertyName = "AvgFuelRatePerHour";
        this.AvgFuelRatePerHour.HeaderText = "������� ����������� �������, �/�";
        this.AvgFuelRatePerHour.Name = "AvgFuelRatePerHour";
        this.AvgFuelRatePerHour.ToolTipText = "������� ����������� �������, �/�";
        // 
        // FuelrateWithDischarge
        // 
        this.FuelrateWithDischarge.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
        this.FuelrateWithDischarge.DataPropertyName = "FuelrateWithDischarge";
        this.FuelrateWithDischarge.HeaderText = "�� ��������� ����� � �������";
        this.FuelrateWithDischarge.Name = "FuelrateWithDischarge";
        this.FuelrateWithDischarge.ToolTipText = "�� ��������� ����� � �������";
        this.FuelrateWithDischarge.Width = 107;
        // 
        // TimeGetFuelAfterStop
        // 
        this.TimeGetFuelAfterStop.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
        this.TimeGetFuelAfterStop.DataPropertyName = "TimeGetFuelAfterStop";
        this.TimeGetFuelAfterStop.HeaderText = "����� ������������ ������� ����� ���������";
        this.TimeGetFuelAfterStop.Name = "TimeGetFuelAfterStop";
        this.TimeGetFuelAfterStop.ToolTipText = "����� ������������ ������� ����� ���������";
        this.TimeGetFuelAfterStop.Width = 148;
        // 
        // TimeGetFuelBeforeMotion
        // 
        this.TimeGetFuelBeforeMotion.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
        this.TimeGetFuelBeforeMotion.DataPropertyName = "TimeGetFuelBeforeMotion";
        this.TimeGetFuelBeforeMotion.HeaderText = "����� ���������� �������, ����� ������� ��������";
        this.TimeGetFuelBeforeMotion.Name = "TimeGetFuelBeforeMotion";
        this.TimeGetFuelBeforeMotion.ToolTipText = "����� ���������� �������, ����� ������� ��������";
        this.TimeGetFuelBeforeMotion.Width = 144;
        // 
        // FuelingMinMaxAlgorithm
        // 
        this.FuelingMinMaxAlgorithm.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
        this.FuelingMinMaxAlgorithm.DataPropertyName = "FuelingMinMaxAlgorithm";
        this.FuelingMinMaxAlgorithm.HeaderText = "�������� ������� �� MM";
        this.FuelingMinMaxAlgorithm.Name = "FuelingMinMaxAlgorithm";
        this.FuelingMinMaxAlgorithm.ToolTipText = "��������� ��� ���������� �������� ������������ ��������� �������";
        this.FuelingMinMaxAlgorithm.Width = 82;
        // 
        // CzMinCrossingPairTime
        // 
        this.CzMinCrossingPairTime.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
        this.CzMinCrossingPairTime.DataPropertyName = "CzMinCrossingPairTime";
        this.CzMinCrossingPairTime.HeaderText = "Min ����� ����� ����� ������������� ��";
        this.CzMinCrossingPairTime.Name = "CzMinCrossingPairTime";
        this.CzMinCrossingPairTime.ToolTipText = "Min ����� ����� ����� ����������������� ������������� ����� ��";
        this.CzMinCrossingPairTime.Width = 143;
        // 
        // FuelerRadiusFinds
        // 
        this.FuelerRadiusFinds.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
        this.FuelerRadiusFinds.DataPropertyName = "FuelerRadiusFinds";
        this.FuelerRadiusFinds.HeaderText = "������ ������ ��, �";
        this.FuelerRadiusFinds.Name = "FuelerRadiusFinds";
        this.FuelerRadiusFinds.ToolTipText = "������ ������ ������������ �� ������ �����������������";
        this.FuelerRadiusFinds.Width = 84;
        // 
        // FuelerMaxTimeStop
        // 
        this.FuelerMaxTimeStop.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
        this.FuelerMaxTimeStop.DataPropertyName = "FuelerMaxTimeStop";
        this.FuelerMaxTimeStop.HeaderText = "����������� ����� ����� ����������, �";
        this.FuelerMaxTimeStop.Name = "FuelerMaxTimeStop";
        this.FuelerMaxTimeStop.ToolTipText = "����������� ����� �� �������� ���������� ��";
        this.FuelerMaxTimeStop.Width = 130;
        // 
        // FuelerMinFuelrate
        // 
        this.FuelerMinFuelrate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
        this.FuelerMinFuelrate.DataPropertyName = "FuelerMinFuelrate";
        this.FuelerMinFuelrate.HeaderText = "����������� �������� �������� � ������, �";
        this.FuelerMinFuelrate.Name = "FuelerMinFuelrate";
        this.FuelerMinFuelrate.ToolTipText = "����������� �������� ��������, ��� ����� �������� � �����";
        this.FuelerMinFuelrate.Width = 130;
        // 
        // FuelerCountInMotion
        // 
        this.FuelerCountInMotion.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
        this.FuelerCountInMotion.DataPropertyName = "FuelerCountInMotion";
        this.FuelerCountInMotion.HeaderText = "������� �������� � ��������";
        this.FuelerCountInMotion.Name = "FuelerCountInMotion";
        this.FuelerCountInMotion.ToolTipText = "������� �������� � ��������";
        this.FuelerCountInMotion.Width = 113;
        // 
        // SettingTTControl
        // 
        this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        this.Controls.Add(this.splitContainer1);
        this.Controls.Add(this.statusStrip1);
        this.Name = "SettingTTControl";
        this.Size = new System.Drawing.Size(830, 458);
        this.Load += new System.EventHandler(this.SettingsTTControl_Load);
        this.splitContainer1.Panel1.ResumeLayout(false);
        this.splitContainer1.Panel2.ResumeLayout(false);
        this.splitContainer1.Panel2.PerformLayout();
        this.splitContainer1.ResumeLayout(false);
        this.splitContainer2.Panel1.ResumeLayout(false);
        this.splitContainer2.Panel1.PerformLayout();
        this.splitContainer2.ResumeLayout(false);
        ((System.ComponentModel.ISupportInitialize)(this.settingDataGridView)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.settingBindingSource)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.atlantaDataSet)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.settingBindingNavigator)).EndInit();
        this.settingBindingNavigator.ResumeLayout(false);
        this.settingBindingNavigator.PerformLayout();
        ((System.ComponentModel.ISupportInitialize)(this.vehicleDataGridView)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.vehicleBindingSource)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.vehicleBindingNavigator)).EndInit();
        this.vehicleBindingNavigator.ResumeLayout(false);
        this.vehicleBindingNavigator.PerformLayout();
        this.statusStrip1.ResumeLayout(false);
        this.statusStrip1.PerformLayout();
        this.ResumeLayout(false);
        this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.SplitContainer splitContainer1;
    private atlantaDataSet atlantaDataSet;
    private System.Windows.Forms.BindingSource settingBindingSource;
    private LocalCache.atlantaDataSetTableAdapters.SettingTableAdapter settingTableAdapter;
    private System.Windows.Forms.BindingNavigator settingBindingNavigator;
    private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
    private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
    private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
    private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
    private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
    private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
    private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
    private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
    private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
    private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
    private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
    private System.Windows.Forms.ToolStripButton settingBindingNavigatorSaveItem;
    private System.Windows.Forms.DataGridView settingDataGridView;
    private System.Windows.Forms.BindingSource vehicleBindingSource;
    private LocalCache.atlantaDataSetTableAdapters.VehicleTableAdapter vehicleTableAdapter;
    private System.Windows.Forms.ToolStripButton assignToolStripButton;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
    private System.Windows.Forms.ToolStripButton refreshToolStripButton;
    private System.Windows.Forms.SplitContainer splitContainer2;
    private System.Windows.Forms.StatusStrip statusStrip1;
    private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
    private System.Windows.Forms.BindingNavigator vehicleBindingNavigator;
    private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem2;
    private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem2;
    private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem2;
    private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator6;
    private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem2;
    private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator7;
    private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem2;
    private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem2;
    private System.Windows.Forms.DataGridView vehicleDataGridView;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn21;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
    private System.Windows.Forms.DataGridViewTextBoxColumn colTimeBreakMaxPermitted;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
    private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
    private System.Windows.Forms.DataGridViewTextBoxColumn FuelApproximationTime;
    private System.Windows.Forms.DataGridViewTextBoxColumn AvgFuelRatePerHour;
    private System.Windows.Forms.DataGridViewTextBoxColumn FuelrateWithDischarge;
    private System.Windows.Forms.DataGridViewTextBoxColumn TimeGetFuelAfterStop;
    private System.Windows.Forms.DataGridViewTextBoxColumn TimeGetFuelBeforeMotion;
    private System.Windows.Forms.DataGridViewTextBoxColumn FuelingMinMaxAlgorithm;
    private System.Windows.Forms.DataGridViewTextBoxColumn CzMinCrossingPairTime;
    private System.Windows.Forms.DataGridViewTextBoxColumn FuelerRadiusFinds;
    private System.Windows.Forms.DataGridViewTextBoxColumn FuelerMaxTimeStop;
    private System.Windows.Forms.DataGridViewTextBoxColumn FuelerMinFuelrate;
    private System.Windows.Forms.DataGridViewTextBoxColumn FuelerCountInMotion;
  }
}