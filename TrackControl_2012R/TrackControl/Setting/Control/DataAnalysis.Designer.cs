namespace TrackControl.Setting.Control
{
  partial class DataAnalysis
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DataAnalysis));
            this.splitContainer = new System.Windows.Forms.SplitContainer();
            this.chSelectAll = new DevExpress.XtraEditors.CheckEdit();
            this.dgvMobitels = new System.Windows.Forms.DataGridView();
            this.CH = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Mobitel_Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Mobitel_Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.layoutPnlMain = new System.Windows.Forms.TableLayoutPanel();
            this.dgvAnaliz = new System.Windows.Forms.DataGridView();
            this.Car = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DateB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DateE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Interval = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QTYpoints = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.layoutPnlHeader = new System.Windows.Forms.TableLayoutPanel();
            this.btAnal_Statistic = new System.Windows.Forms.Button();
            this.lbMin = new System.Windows.Forms.Label();
            this.txMin = new System.Windows.Forms.TextBox();
            this.lbInterval = new System.Windows.Forms.Label();
            this.lbEnd = new System.Windows.Forms.Label();
            this.lbBegin = new System.Windows.Forms.Label();
            this.dpBegin = new System.Windows.Forms.DateTimePicker();
            this.dpEnd = new System.Windows.Forms.DateTimePicker();
            this.layoutPnlButtons = new System.Windows.Forms.TableLayoutPanel();
            this.btAnal_Data_Double = new System.Windows.Forms.Button();
            this.btAnal_Data_Add = new System.Windows.Forms.Button();
            this.btAnal_Data_Abs = new System.Windows.Forms.Button();
            this.btDeleteDates = new System.Windows.Forms.Button();
            this.btAnal_GPS_Abs = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.pbAnaliz = new System.Windows.Forms.ProgressBar();
            this.lbInfor = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).BeginInit();
            this.splitContainer.Panel1.SuspendLayout();
            this.splitContainer.Panel2.SuspendLayout();
            this.splitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chSelectAll.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMobitels)).BeginInit();
            this.layoutPnlMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAnaliz)).BeginInit();
            this.layoutPnlHeader.SuspendLayout();
            this.layoutPnlButtons.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer
            // 
            this.splitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer.Location = new System.Drawing.Point(0, 0);
            this.splitContainer.Name = "splitContainer";
            // 
            // splitContainer.Panel1
            // 
            this.splitContainer.Panel1.Controls.Add(this.chSelectAll);
            this.splitContainer.Panel1.Controls.Add(this.dgvMobitels);
            // 
            // splitContainer.Panel2
            // 
            this.splitContainer.Panel2.Controls.Add(this.layoutPnlMain);
            this.splitContainer.Size = new System.Drawing.Size(868, 411);
            this.splitContainer.SplitterDistance = 193;
            this.splitContainer.TabIndex = 0;
            // 
            // chSelectAll
            // 
            this.chSelectAll.Location = new System.Drawing.Point(0, 0);
            this.chSelectAll.Name = "chSelectAll";
            this.chSelectAll.Properties.Caption = "";
            this.chSelectAll.Size = new System.Drawing.Size(23, 19);
            this.chSelectAll.TabIndex = 19;
            this.chSelectAll.CheckedChanged += new System.EventHandler(this.chSelectAll_CheckedChanged);
            // 
            // dgvMobitels
            // 
            this.dgvMobitels.AllowUserToAddRows = false;
            this.dgvMobitels.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dgvMobitels.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvMobitels.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMobitels.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CH,
            this.Mobitel_Id,
            this.Mobitel_Name});
            this.dgvMobitels.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvMobitels.Location = new System.Drawing.Point(0, 0);
            this.dgvMobitels.Name = "dgvMobitels";
            this.dgvMobitels.RowHeadersVisible = false;
            this.dgvMobitels.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvMobitels.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvMobitels.Size = new System.Drawing.Size(193, 411);
            this.dgvMobitels.TabIndex = 17;
            // 
            // CH
            // 
            this.CH.DataPropertyName = "CH";
            this.CH.Frozen = true;
            this.CH.HeaderText = "";
            this.CH.Name = "CH";
            this.CH.Width = 20;
            // 
            // Mobitel_Id
            // 
            this.Mobitel_Id.DataPropertyName = "Mobitel_Id";
            this.Mobitel_Id.HeaderText = "Mobitel_ID";
            this.Mobitel_Id.Name = "Mobitel_Id";
            this.Mobitel_Id.ReadOnly = true;
            this.Mobitel_Id.Visible = false;
            this.Mobitel_Id.Width = 5;
            // 
            // Mobitel_Name
            // 
            this.Mobitel_Name.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Mobitel_Name.DataPropertyName = "Name";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.Mobitel_Name.DefaultCellStyle = dataGridViewCellStyle2;
            this.Mobitel_Name.HeaderText = "������";
            this.Mobitel_Name.Name = "Mobitel_Name";
            // 
            // layoutPnlMain
            // 
            this.layoutPnlMain.ColumnCount = 1;
            this.layoutPnlMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.layoutPnlMain.Controls.Add(this.dgvAnaliz, 0, 2);
            this.layoutPnlMain.Controls.Add(this.layoutPnlHeader, 0, 0);
            this.layoutPnlMain.Controls.Add(this.layoutPnlButtons, 0, 1);
            this.layoutPnlMain.Controls.Add(this.tableLayoutPanel1, 0, 3);
            this.layoutPnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutPnlMain.Location = new System.Drawing.Point(0, 0);
            this.layoutPnlMain.Name = "layoutPnlMain";
            this.layoutPnlMain.RowCount = 4;
            this.layoutPnlMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.layoutPnlMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.layoutPnlMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.layoutPnlMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.layoutPnlMain.Size = new System.Drawing.Size(671, 411);
            this.layoutPnlMain.TabIndex = 0;
            // 
            // dgvAnaliz
            // 
            this.dgvAnaliz.AllowUserToAddRows = false;
            this.dgvAnaliz.AllowUserToDeleteRows = false;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dgvAnaliz.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvAnaliz.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAnaliz.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Car,
            this.DateB,
            this.DateE,
            this.Interval,
            this.QTYpoints});
            this.dgvAnaliz.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvAnaliz.Location = new System.Drawing.Point(3, 83);
            this.dgvAnaliz.Name = "dgvAnaliz";
            this.dgvAnaliz.ReadOnly = true;
            this.dgvAnaliz.RowHeadersVisible = false;
            this.dgvAnaliz.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAnaliz.Size = new System.Drawing.Size(665, 295);
            this.dgvAnaliz.TabIndex = 10;
            // 
            // Car
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.Car.DefaultCellStyle = dataGridViewCellStyle4;
            this.Car.HeaderText = "������";
            this.Car.Name = "Car";
            this.Car.ReadOnly = true;
            this.Car.Width = 120;
            // 
            // DateB
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.DateB.DefaultCellStyle = dataGridViewCellStyle5;
            this.DateB.HeaderText = "���� ������";
            this.DateB.Name = "DateB";
            this.DateB.ReadOnly = true;
            this.DateB.Width = 120;
            // 
            // DateE
            // 
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.DateE.DefaultCellStyle = dataGridViewCellStyle6;
            this.DateE.HeaderText = "���� ���������";
            this.DateE.Name = "DateE";
            this.DateE.ReadOnly = true;
            this.DateE.Width = 120;
            // 
            // Interval
            // 
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Interval.DefaultCellStyle = dataGridViewCellStyle7;
            this.Interval.HeaderText = "��������";
            this.Interval.Name = "Interval";
            this.Interval.ReadOnly = true;
            // 
            // QTYpoints
            // 
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.QTYpoints.DefaultCellStyle = dataGridViewCellStyle8;
            this.QTYpoints.HeaderText = "���������� �����";
            this.QTYpoints.Name = "QTYpoints";
            this.QTYpoints.ReadOnly = true;
            // 
            // layoutPnlHeader
            // 
            this.layoutPnlHeader.ColumnCount = 8;
            this.layoutPnlHeader.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.layoutPnlHeader.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 140F));
            this.layoutPnlHeader.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.layoutPnlHeader.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 140F));
            this.layoutPnlHeader.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.layoutPnlHeader.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.layoutPnlHeader.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.layoutPnlHeader.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.layoutPnlHeader.Controls.Add(this.btAnal_Statistic, 7, 0);
            this.layoutPnlHeader.Controls.Add(this.lbMin, 6, 0);
            this.layoutPnlHeader.Controls.Add(this.txMin, 5, 0);
            this.layoutPnlHeader.Controls.Add(this.lbInterval, 4, 0);
            this.layoutPnlHeader.Controls.Add(this.lbEnd, 2, 0);
            this.layoutPnlHeader.Controls.Add(this.lbBegin, 0, 0);
            this.layoutPnlHeader.Controls.Add(this.dpBegin, 1, 0);
            this.layoutPnlHeader.Controls.Add(this.dpEnd, 3, 0);
            this.layoutPnlHeader.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutPnlHeader.Location = new System.Drawing.Point(3, 3);
            this.layoutPnlHeader.Name = "layoutPnlHeader";
            this.layoutPnlHeader.RowCount = 1;
            this.layoutPnlHeader.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.layoutPnlHeader.Size = new System.Drawing.Size(665, 34);
            this.layoutPnlHeader.TabIndex = 0;
            // 
            // btAnal_Statistic
            // 
            this.btAnal_Statistic.Location = new System.Drawing.Point(573, 3);
            this.btAnal_Statistic.Name = "btAnal_Statistic";
            this.btAnal_Statistic.Size = new System.Drawing.Size(75, 23);
            this.btAnal_Statistic.TabIndex = 19;
            this.btAnal_Statistic.Text = "����������";
            this.btAnal_Statistic.UseVisualStyleBackColor = true;
            this.btAnal_Statistic.Click += new System.EventHandler(this.btAnal_Statistic_Click);
            // 
            // lbMin
            // 
            this.lbMin.AutoSize = true;
            this.lbMin.Dock = System.Windows.Forms.DockStyle.Left;
            this.lbMin.Location = new System.Drawing.Point(523, 0);
            this.lbMin.Name = "lbMin";
            this.lbMin.Size = new System.Drawing.Size(27, 34);
            this.lbMin.TabIndex = 13;
            this.lbMin.Text = "���";
            this.lbMin.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txMin
            // 
            this.txMin.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txMin.Location = new System.Drawing.Point(473, 3);
            this.txMin.Name = "txMin";
            this.txMin.Size = new System.Drawing.Size(44, 20);
            this.txMin.TabIndex = 12;
            this.txMin.Text = "5";
            this.txMin.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lbInterval
            // 
            this.lbInterval.AutoSize = true;
            this.lbInterval.Dock = System.Windows.Forms.DockStyle.Right;
            this.lbInterval.Location = new System.Drawing.Point(411, 0);
            this.lbInterval.Name = "lbInterval";
            this.lbInterval.Size = new System.Drawing.Size(56, 34);
            this.lbInterval.TabIndex = 11;
            this.lbInterval.Text = "�������� �����";
            this.lbInterval.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbEnd
            // 
            this.lbEnd.AutoSize = true;
            this.lbEnd.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbEnd.Location = new System.Drawing.Point(223, 0);
            this.lbEnd.Name = "lbEnd";
            this.lbEnd.Size = new System.Drawing.Size(24, 34);
            this.lbEnd.TabIndex = 5;
            this.lbEnd.Text = "��";
            this.lbEnd.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbBegin
            // 
            this.lbBegin.AutoSize = true;
            this.lbBegin.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbBegin.Location = new System.Drawing.Point(3, 0);
            this.lbBegin.Name = "lbBegin";
            this.lbBegin.Size = new System.Drawing.Size(74, 34);
            this.lbBegin.TabIndex = 3;
            this.lbBegin.Text = "���������� ��";
            this.lbBegin.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dpBegin
            // 
            this.dpBegin.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dpBegin.Location = new System.Drawing.Point(83, 3);
            this.dpBegin.Name = "dpBegin";
            this.dpBegin.Size = new System.Drawing.Size(134, 20);
            this.dpBegin.TabIndex = 4;
            // 
            // dpEnd
            // 
            this.dpEnd.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dpEnd.Location = new System.Drawing.Point(253, 3);
            this.dpEnd.Name = "dpEnd";
            this.dpEnd.Size = new System.Drawing.Size(134, 20);
            this.dpEnd.TabIndex = 6;
            // 
            // layoutPnlButtons
            // 
            this.layoutPnlButtons.ColumnCount = 5;
            this.layoutPnlButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 125F));
            this.layoutPnlButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 198F));
            this.layoutPnlButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.layoutPnlButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 44F));
            this.layoutPnlButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.layoutPnlButtons.Controls.Add(this.btAnal_Data_Double, 4, 0);
            this.layoutPnlButtons.Controls.Add(this.btAnal_Data_Add, 3, 0);
            this.layoutPnlButtons.Controls.Add(this.btAnal_Data_Abs, 2, 0);
            this.layoutPnlButtons.Controls.Add(this.btDeleteDates, 1, 0);
            this.layoutPnlButtons.Controls.Add(this.btAnal_GPS_Abs, 0, 0);
            this.layoutPnlButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutPnlButtons.Location = new System.Drawing.Point(3, 43);
            this.layoutPnlButtons.Name = "layoutPnlButtons";
            this.layoutPnlButtons.RowCount = 1;
            this.layoutPnlButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.layoutPnlButtons.Size = new System.Drawing.Size(665, 34);
            this.layoutPnlButtons.TabIndex = 1;
            // 
            // btAnal_Data_Double
            // 
            this.btAnal_Data_Double.Location = new System.Drawing.Point(520, 3);
            this.btAnal_Data_Double.Name = "btAnal_Data_Double";
            this.btAnal_Data_Double.Size = new System.Drawing.Size(120, 25);
            this.btAnal_Data_Double.TabIndex = 21;
            this.btAnal_Data_Double.Text = "���������� ������";
            this.btAnal_Data_Double.UseVisualStyleBackColor = true;
            this.btAnal_Data_Double.Click += new System.EventHandler(this.btAnal_Data_Double_Click);
            // 
            // btAnal_Data_Add
            // 
            this.btAnal_Data_Add.AutoSize = true;
            this.btAnal_Data_Add.Image = ((System.Drawing.Image)(resources.GetObject("btAnal_Data_Add.Image")));
            this.btAnal_Data_Add.Location = new System.Drawing.Point(476, 3);
            this.btAnal_Data_Add.Name = "btAnal_Data_Add";
            this.btAnal_Data_Add.Size = new System.Drawing.Size(27, 25);
            this.btAnal_Data_Add.TabIndex = 20;
            this.btAnal_Data_Add.UseVisualStyleBackColor = true;
            this.btAnal_Data_Add.Click += new System.EventHandler(this.btAnal_Data_Add_Click);
            // 
            // btAnal_Data_Abs
            // 
            this.btAnal_Data_Abs.Location = new System.Drawing.Point(326, 3);
            this.btAnal_Data_Abs.Name = "btAnal_Data_Abs";
            this.btAnal_Data_Abs.Size = new System.Drawing.Size(129, 25);
            this.btAnal_Data_Abs.TabIndex = 8;
            this.btAnal_Data_Abs.Text = "����������� ������";
            this.btAnal_Data_Abs.UseVisualStyleBackColor = true;
            this.btAnal_Data_Abs.Click += new System.EventHandler(this.btAnal_Data_Abs_Click);
            // 
            // btDeleteDates
            // 
            this.btDeleteDates.Location = new System.Drawing.Point(128, 3);
            this.btDeleteDates.Name = "btDeleteDates";
            this.btDeleteDates.Size = new System.Drawing.Size(192, 25);
            this.btDeleteDates.TabIndex = 7;
            this.btDeleteDates.Text = "�������� ������������ ���";
            this.btDeleteDates.UseVisualStyleBackColor = true;
            this.btDeleteDates.Click += new System.EventHandler(this.btDeleteDates_Click);
            // 
            // btAnal_GPS_Abs
            // 
            this.btAnal_GPS_Abs.Location = new System.Drawing.Point(3, 3);
            this.btAnal_GPS_Abs.Name = "btAnal_GPS_Abs";
            this.btAnal_GPS_Abs.Size = new System.Drawing.Size(102, 25);
            this.btAnal_GPS_Abs.TabIndex = 6;
            this.btAnal_GPS_Abs.Text = "���������� GPS";
            this.btAnal_GPS_Abs.UseVisualStyleBackColor = true;
            this.btAnal_GPS_Abs.Click += new System.EventHandler(this.btAnal_GPS_Abs_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 129F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.pbAnaliz, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.lbInfor, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 384);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(665, 24);
            this.tableLayoutPanel1.TabIndex = 11;
            // 
            // pbAnaliz
            // 
            this.pbAnaliz.Location = new System.Drawing.Point(132, 3);
            this.pbAnaliz.Name = "pbAnaliz";
            this.pbAnaliz.Size = new System.Drawing.Size(472, 18);
            this.pbAnaliz.TabIndex = 18;
            this.pbAnaliz.Visible = false;
            // 
            // lbInfor
            // 
            this.lbInfor.AutoSize = true;
            this.lbInfor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbInfor.Location = new System.Drawing.Point(3, 0);
            this.lbInfor.Name = "lbInfor";
            this.lbInfor.Size = new System.Drawing.Size(123, 24);
            this.lbInfor.TabIndex = 15;
            this.lbInfor.Text = "������ ������          ";
            this.lbInfor.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // DataAnalysis
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainer);
            this.Name = "DataAnalysis";
            this.Size = new System.Drawing.Size(868, 411);
            this.Load += new System.EventHandler(this.DataAnalysis_Load);
            this.splitContainer.Panel1.ResumeLayout(false);
            this.splitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).EndInit();
            this.splitContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chSelectAll.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMobitels)).EndInit();
            this.layoutPnlMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAnaliz)).EndInit();
            this.layoutPnlHeader.ResumeLayout(false);
            this.layoutPnlHeader.PerformLayout();
            this.layoutPnlButtons.ResumeLayout(false);
            this.layoutPnlButtons.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.SplitContainer splitContainer;
    private System.Windows.Forms.DataGridView dgvMobitels;
    private System.Windows.Forms.TableLayoutPanel layoutPnlMain;
    private System.Windows.Forms.TableLayoutPanel layoutPnlHeader;
    private System.Windows.Forms.Label lbBegin;
    private System.Windows.Forms.DateTimePicker dpBegin;
    private System.Windows.Forms.Label lbEnd;
    private System.Windows.Forms.DateTimePicker dpEnd;
    private System.Windows.Forms.Label lbInterval;
    private System.Windows.Forms.TextBox txMin;
    private System.Windows.Forms.Label lbMin;
    private System.Windows.Forms.Button btAnal_Statistic;
    private System.Windows.Forms.TableLayoutPanel layoutPnlButtons;
    private System.Windows.Forms.Button btAnal_GPS_Abs;
    private System.Windows.Forms.Button btAnal_Data_Abs;
    private System.Windows.Forms.Button btAnal_Data_Add;
    private System.Windows.Forms.Button btAnal_Data_Double;
    private System.Windows.Forms.DataGridView dgvAnaliz;
    private System.Windows.Forms.DataGridViewTextBoxColumn Car;
    private System.Windows.Forms.DataGridViewTextBoxColumn DateB;
    private System.Windows.Forms.DataGridViewTextBoxColumn DateE;
    private System.Windows.Forms.DataGridViewTextBoxColumn Interval;
    private System.Windows.Forms.DataGridViewTextBoxColumn QTYpoints;
    private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
    private System.Windows.Forms.Label lbInfor;
      private System.Windows.Forms.ProgressBar pbAnaliz;
      private DevExpress.XtraEditors.CheckEdit chSelectAll;
      private System.Windows.Forms.DataGridViewCheckBoxColumn CH;
      private System.Windows.Forms.DataGridViewTextBoxColumn Mobitel_Id;
      private System.Windows.Forms.DataGridViewTextBoxColumn Mobitel_Name;
      private System.Windows.Forms.Button btDeleteDates;
  }
}
