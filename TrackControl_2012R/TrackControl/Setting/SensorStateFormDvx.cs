﻿using System;
using System.Data;
using System.Windows.Forms;
using TrackControl.Setting.Controls;
using LocalCache;
using LocalCache.atlantaDataSetTableAdapters;
using TrackControl.Properties;

/// <summary>
/// Форма для настроек состояний, зависящих от показаний датчиков.
/// </summary>
namespace TrackControl.Setting
{
    public partial class SensorStateFormDvx : DevExpress.XtraEditors.XtraForm
    {
        /// <summary>
        /// Адаптер для таблицы состояний.
        /// </summary>
        private StateTableAdapter _stateAdapter;
        /// <summary>
        /// Адаптер для таблицы переходов (состояний).
        /// </summary>
        private TransitionTableAdapter _transitionAdapter;
        /// <summary>
        /// Датасет приложения.
        /// </summary>
        private atlantaDataSet _dataset;
        /// <summary>
        /// Датчик, для которого настраиваются состояния.
        /// </summary>
        private atlantaDataSet.sensorsRow _sensor;

        private TrackControl.Setting.Controls.AddButtonControl addPanel;

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="stateAdapter">Адаптер для таблицы состояний</param>
        /// <param name="transitionAdapter">Адаптер для таблицы переходов</param>
        /// <param name="dataset">Датасет приложения</param>
        /// <param name="sensor">Датчик, для которого настраиваются состояния</param>
        public SensorStateFormDvx(StateTableAdapter stateAdapter, TransitionTableAdapter transitionAdapter, atlantaDataSet dataset,
            atlantaDataSet.sensorsRow sensor)
        {
            InitializeComponent();

            // 
            // addPanel begin
            // 
            this.addPanel = new AddButtonControl();
            this.addPanel.BackColor = System.Drawing.Color.Transparent;
            this.addPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.addPanel.Location = new System.Drawing.Point( 0, 0 );
            this.addPanel.Name = "addPanel";
            this.addPanel.Size = new System.Drawing.Size( 430, 100 );
            this.addPanel.TabIndex = 0;
            this.addPanel.AddPressed += new System.EventHandler( this.addPanel_AddPressed );
            this.addPanel.BackPressed += new System.EventHandler( this.closeBtn_Click );
            //============================ add panel end====================================

            this.stateGridView.RowCellClick += stateGrid_CellContentClick;
            this.stateGridView.CustomDrawCell += this.stateGrid_CellFormatting;

            init();
            clearEditGroupBox();

            _stateAdapter = stateAdapter;
            _transitionAdapter = transitionAdapter;
            _dataset = dataset;
            _sensor = sensor;

            Load += this_Load;
            stateGridView.CustomUnboundColumnData += stateGridView_CustomUnboundColumnData;
        }

        private void stateGridView_CustomUnboundColumnData( object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDataEventArgs e )
        {
            if( e.Column.VisibleIndex == 3 )
            {
                e.Value = imageList1.Images[0];
            }

            if( e.Column.VisibleIndex == 4 )
            {
                e.Value = imageList1.Images[1];
            }
        }

        /// <summary>
        /// Обрабатывает клик на кнопках грида ("Редактировать" и "Удалить").
        /// </summary>
        private void stateGrid_CellContentClick( object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e )
        {
            if( sensorStatesbindingSource.Current == null )
                return;

            atlantaDataSet.StateRow state =
                ( atlantaDataSet.StateRow )( ( DataRowView )sensorStatesbindingSource.Current ).Row;

            if( e.Column.VisibleIndex == stateGridView.Columns[3].VisibleIndex )
            {
                showEditPanel( state, false );
            }
            if( e.Column.VisibleIndex == stateGridView.Columns[4].VisibleIndex )
            {
                showDeletePanel( state );
            }
        }

        /// <summary>
        /// Добавляет всплывающие подсказки к кнопкам грида ("Редактировать" и "Удалить").
        /// </summary>
        private void stateGrid_CellFormatting( object sender, DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventArgs e )
        {
            if( e.Column.VisibleIndex == stateGridView.Columns[3].VisibleIndex )
            {
                e.Column.ToolTip = Resources.StateEdit;
            }
            if( e.Column.VisibleIndex == stateGridView.Columns[4].VisibleIndex )
            {
                e.Column.ToolTip = Resources.StateDelete;
            }
        } // SensorStatesFormDvx

        public SensorStateFormDvx()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Возвращает панель редактирования в начальное состояние.
        /// Отображает панель с кнопкой "Добавить состояние".
        /// </summary>
        private void clearEditGroupBox()
        {
            editGroupBox.Controls.Clear();
            editGroupBox.Controls.Add( addPanel );
            editGroupBox.Text = Resources.StateAdd;
            stateGrid.Enabled = true;
        }

        /// <summary>
        /// Отображает панель удаления состояния.
        /// </summary>
        /// <param name="state">Состояние, которое намереваются удалить</param>
        void showDeletePanel( atlantaDataSet.StateRow state )
        {
            editGroupBox.Controls.Clear();

            DeleteSensorStateControl deletePanel = new DeleteSensorStateControl( _stateAdapter, _transitionAdapter, _dataset, state );
            deletePanel.EndDelete += deletePanel_EndDelete;
            deletePanel.Dock = DockStyle.Fill;
            editGroupBox.Controls.Add( deletePanel );
            editGroupBox.Text = Resources.StateDeleting;
            stateGrid.Enabled = false;
        }

        /// <summary>
        /// Отображает панель редактирования состояния.
        /// </summary>
        /// <param name="state">Состояние, которое будет редактироваться</param>
        void showEditPanel( atlantaDataSet.StateRow state, bool added )
        {
            editGroupBox.Controls.Clear();
            EditSensorStateControl editPanel = new EditSensorStateControl( _stateAdapter, _dataset, state, added );
            editPanel.EndEdit += editPanel_EndEdit;
            editPanel.Dock = DockStyle.Fill;
            editGroupBox.Controls.Add( editPanel );
            editGroupBox.Text = Resources.StateSetting;
            stateGrid.Enabled = false;
        }

        /// <summary>
        /// Обрабатывает событие Load данной формы
        /// </summary>
        void this_Load( object sender, EventArgs e )
        {
            VehicleInfo info = new VehicleInfo( _sensor.mobitelsRow );
            carNameText.Text = info.Info;
            sensorNameText.Text = _sensor.Name;

            sensorStatesbindingSource.DataSource = _dataset;
            sensorStatesbindingSource.DataMember = "State";
            sensorStatesbindingSource.Filter = String.Format( "SensorId = {0}", _sensor.id );
            //stateGrid.DataSource = sensorStatesbindingSource;
        }

        /// <summary>
        /// Обрабатывает клик на кнопке "Добавить".
        /// </summary>
        void addPanel_AddPressed( object sender, EventArgs e )
        {
            atlantaDataSet.StateRow state = _dataset.State.NewStateRow();
            state.MinValue = 0.0;
            state.MaxValue = 0.0;
            state.Title = "";
            state.sensorsRow = _sensor;
            _dataset.State.AddStateRow( state );
            showEditPanel( state, true );
        }

        /// <summary>
        /// Обрабатывает клик на кнопке "Закрыть".
        /// </summary>
        void closeBtn_Click( object sender, EventArgs e )
        {
            Close();
        }

        /// <summary>
        /// Обрабатывает окончание редактирования состояния.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void editPanel_EndEdit( object sender, EventArgs e )
        {
            clearEditGroupBox();
        }

        /// <summary>
        /// Обрабатывает событие окончания удаления состояния датчика.
        /// </summary>
        void deletePanel_EndDelete( object sender, EventArgs e )
        {
            clearEditGroupBox();
        }

        private void init()
        {
            carLbl.Text = String.Format( "{0}:", Resources.Car );
            sensorLbl.Text = String.Format( "{0}:", Resources.Sensor );

            titleDataGridViewTextBoxColumn.Caption = Resources.Description;

            titleDataGridViewTextBoxColumn.ToolTip = Resources.DescrState;
            minValueDataGridViewTextBoxColumn.ToolTip = Resources.MinValueTip;
            maxValueDataGridViewTextBoxColumn.ToolTip = Resources.MaxValueTip;
            Edit.ToolTip = Resources.EditTip;
            Delete.ToolTip = Resources.DeleteTip;

            minValueDataGridViewTextBoxColumn.Caption = Resources.LimitMin;
            maxValueDataGridViewTextBoxColumn.Caption = Resources.LimitMax;

            Text = Resources.SensorStateForm;
            editGroupBox.Text = Resources.GroupeBoxText;
        }
    }
}