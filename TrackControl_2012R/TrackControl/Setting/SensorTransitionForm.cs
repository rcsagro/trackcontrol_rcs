using System;
using System.Data;
using System.Windows.Forms;
using TrackControl.Setting.Controls;
using LocalCache;
using LocalCache.atlantaDataSetTableAdapters;
using DevExpress.XtraEditors;
using TrackControl.Properties;

namespace TrackControl.Setting
{
  /// <summary>
  /// ����� ��� �������� �������, ������������ ����������� ��������.
  /// </summary>
  public partial class SensorTransitionForm : XtraForm
  {
    /// <summary>
    /// ������� ��� ������� ��������� (���������).
    /// </summary>
    private TransitionTableAdapter _transitionAdapter;
    /// <summary>
    /// ������� ����������.
    /// </summary>
    private atlantaDataSet _dataset;
    /// <summary>
    /// ������, ��� �������� ������������� �������.
    /// </summary>
    private atlantaDataSet.sensorsRow _sensor;

    /// <summary>
    /// �����������.
    /// </summary>
    /// <param name="transitionAdapter">������� ��� ������� ���������</param>
    /// <param name="dataset">������� ����������</param>
    /// <param name="sensor">������, ��� �������� ������������� �������</param>
    public SensorTransitionForm(
      TransitionTableAdapter transitionAdapter,
      atlantaDataSet dataset,
      atlantaDataSet.sensorsRow sensor)
    {
      InitializeComponent();
      init();
      clearEditGroupBox();

      _transitionAdapter = transitionAdapter;
      _dataset = dataset;
      _sensor = sensor;
    }

    /// <summary>
    /// ���������� ������ �������������� � ��������� ���������.
    /// ���������� ������ � ������� "�������� �������".
    /// </summary>
    void clearEditGroupBox()
    {
      editGroupBox.Controls.Clear();
      editGroupBox.Controls.Add(addPanel);
      editGroupBox.Text = Resources.TransitionAdd;
      transitionGrid.Enabled = true;
    }
    /// <summary>
    /// ���������� ������ �������� �������.
    /// </summary>
    /// <param name="transition">�������, ������� ������������ �������</param>
    void showDeletePanel(atlantaDataSet.TransitionRow transition)
    {
      editGroupBox.Controls.Clear();
      DeleteSensorTransitionControl deletePanel = new DeleteSensorTransitionControl(_transitionAdapter, _dataset, transition);
      deletePanel.EndDelete += deletePanel_EndDelete;
      deletePanel.Dock = DockStyle.Fill;
      editGroupBox.Controls.Add(deletePanel);
      editGroupBox.Text = Resources.TransitionDeleting;
      transitionGrid.Enabled = false;
    }
    /// <summary>
    /// ���������� ������ �������������� �������.
    /// </summary>
    /// <param name="state">�������, ������� ����� ���������������</param>
    void showEditPanel(atlantaDataSet.TransitionRow transition, bool added)
    {
      editGroupBox.Controls.Clear();
      EditSensorTransitionControl editPanel = new EditSensorTransitionControl(_transitionAdapter, _dataset, transition, added);
      editPanel.EndEdit += editPanel_EndEdit;
      editPanel.Dock = DockStyle.Fill;
      editGroupBox.Controls.Add(editPanel);
      editGroupBox.Text = Resources.TransitionSetting;
      transitionGrid.Enabled = false;
    }

    /// <summary>
    /// ������������ ������� Load ������ �����.
    /// </summary>
    void SensorTransitionForm_Load(object sender, EventArgs e)
    {
      VehicleInfo info = new VehicleInfo(_sensor.mobitelsRow);
      carNameText.Text = info.Info;
      sensorNameText.Text = _sensor.Name;
      sensorTransitionsBindingSource.DataSource = _dataset;
      sensorTransitionsBindingSource.DataMember = "Transition";
      sensorTransitionsBindingSource.Filter = String.Format("SensorId = {0}", _sensor.id);
    }
    /// <summary>
    /// ������������ ���� �� ������ "��������".
    /// </summary>
    void addPanel_AddPressed(object sender, EventArgs e)
    {
      atlantaDataSet.TransitionRow transition = _dataset.Transition.NewTransitionRow();
      transition.sensorsRow = _sensor;
      atlantaDataSet.StateRow[] states = _sensor.GetStateRows();
      transition.StateRowByState_Transition_Initial = states[0];
      transition.StateRowByState_Transition_Final = states[1];
      transition.IconName = "Default";
      transition.SoundName = "Default";
      _dataset.Transition.AddTransitionRow(transition);
      showEditPanel(transition, true);
    }
    /// <summary>
    /// ������������ ���� �� ������ "�������".
    /// </summary>
    void closeBtn_Click(object sender, EventArgs e)
    {
      Close();
    }
    /// <summary>
    /// ��������� ����������� ��������� � ������� ����� ("�������������" � "�������").
    /// </summary>
    void transitionGrid_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
    {
      if (e.ColumnIndex == transitionGrid.Columns["Edit"].Index)
      {
        DataGridViewCell cell = transitionGrid.Rows[e.RowIndex].Cells[e.ColumnIndex];
        cell.ToolTipText = Resources.TransitionEdit;
      }
      if (e.ColumnIndex == transitionGrid.Columns["Delete"].Index)
      {
        DataGridViewCell cell = transitionGrid.Rows[e.RowIndex].Cells[e.ColumnIndex];
        cell.ToolTipText = Resources.TransitionDelete;
      }
    }

    /// <summary>
    /// ������������ ���� �� ������� ����� ("�������������" � "�������").
    /// </summary>
    void transitionGrid_CellContentClick(object sender, DataGridViewCellEventArgs e)
    {
      if (sensorTransitionsBindingSource.Current == null)
        return;

      atlantaDataSet.TransitionRow transition = (atlantaDataSet.TransitionRow)((DataRowView)sensorTransitionsBindingSource.Current).Row;
      if (e.ColumnIndex == transitionGrid.Columns["Edit"].Index)
      {
        showEditPanel(transition, false);
      }
      if (e.ColumnIndex == transitionGrid.Columns["Delete"].Index)
      {
        showDeletePanel(transition);
      }
    }
    /// <summary>
    /// ������������ ������� ��������� �������� ������� ��� �������.
    /// </summary>
    void deletePanel_EndDelete(object sender, EventArgs e)
    {
      clearEditGroupBox();
    }

    /// <summary>
    /// ������������ ��������� �������������� �������.
    /// </summary>
    void editPanel_EndEdit(object sender, EventArgs e)
    {
      clearEditGroupBox();
    }

    void init()
    {
      carLbl.Text = String.Format("{0}:", Resources.Car);
      sensorLbl.Text = String.Format("{0}:", Resources.Sensor);
      titleDataGridViewTextBoxColumn.HeaderText = Resources.EventDescription;
      enabledDataGridViewCheckBoxColumn.HeaderText = Resources.Alarm;
      viewInReportDataGridViewCheckBoxColumn.HeaderText = Resources.Report;
      Text = Resources.SensorTransitions;
    }
  }
}