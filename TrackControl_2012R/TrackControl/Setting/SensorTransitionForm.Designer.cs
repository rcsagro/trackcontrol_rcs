namespace TrackControl.Setting
{
  partial class SensorTransitionForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SensorTransitionForm));
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
      this._mainTable = new System.Windows.Forms.TableLayoutPanel();
      this.editGroupBox = new System.Windows.Forms.GroupBox();
      this.carSensorTable = new System.Windows.Forms.TableLayoutPanel();
      this.carIcon = new System.Windows.Forms.Label();
      this.sensorIcon = new System.Windows.Forms.Label();
      this.carLbl = new System.Windows.Forms.Label();
      this.sensorLbl = new System.Windows.Forms.Label();
      this.carNameText = new System.Windows.Forms.Label();
      this.sensorNameText = new System.Windows.Forms.Label();
      this.transitionGrid = new System.Windows.Forms.DataGridView();
      this.titleDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.enabledDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
      this.viewInReportDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
      this.Edit = new System.Windows.Forms.DataGridViewImageColumn();
      this.Delete = new System.Windows.Forms.DataGridViewImageColumn();
      this.sensorTransitionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
      this.addPanel = new TrackControl.Setting.Controls.AddButtonControl();
      this._mainTable.SuspendLayout();
      this.carSensorTable.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.transitionGrid)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.sensorTransitionsBindingSource)).BeginInit();
      this.SuspendLayout();
      // 
      // _mainTable
      // 
      this._mainTable.BackColor = System.Drawing.Color.Snow;
      this._mainTable.ColumnCount = 3;
      this._mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
      this._mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this._mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
      this._mainTable.Controls.Add(this.editGroupBox, 1, 3);
      this._mainTable.Controls.Add(this.carSensorTable, 1, 1);
      this._mainTable.Controls.Add(this.transitionGrid, 1, 2);
      this._mainTable.Dock = System.Windows.Forms.DockStyle.Fill;
      this._mainTable.Location = new System.Drawing.Point(0, 0);
      this._mainTable.Margin = new System.Windows.Forms.Padding(10, 5, 10, 10);
      this._mainTable.Name = "_mainTable";
      this._mainTable.RowCount = 5;
      this._mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
      this._mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
      this._mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this._mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 199F));
      this._mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 11F));
      this._mainTable.Size = new System.Drawing.Size(476, 365);
      this._mainTable.TabIndex = 1;
      // 
      // editGroupBox
      // 
      this.editGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
      this.editGroupBox.Location = new System.Drawing.Point(10, 155);
      this.editGroupBox.Margin = new System.Windows.Forms.Padding(0);
      this.editGroupBox.Name = "editGroupBox";
      this.editGroupBox.Padding = new System.Windows.Forms.Padding(5, 0, 5, 5);
      this.editGroupBox.Size = new System.Drawing.Size(456, 199);
      this.editGroupBox.TabIndex = 0;
      this.editGroupBox.TabStop = false;
      // 
      // carSensorTable
      // 
      this.carSensorTable.ColumnCount = 3;
      this.carSensorTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 21F));
      this.carSensorTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 86F));
      this.carSensorTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.carSensorTable.Controls.Add(this.carIcon, 0, 0);
      this.carSensorTable.Controls.Add(this.sensorIcon, 0, 1);
      this.carSensorTable.Controls.Add(this.carLbl, 1, 0);
      this.carSensorTable.Controls.Add(this.sensorLbl, 1, 1);
      this.carSensorTable.Controls.Add(this.carNameText, 2, 0);
      this.carSensorTable.Controls.Add(this.sensorNameText, 2, 1);
      this.carSensorTable.Dock = System.Windows.Forms.DockStyle.Fill;
      this.carSensorTable.Location = new System.Drawing.Point(10, 10);
      this.carSensorTable.Margin = new System.Windows.Forms.Padding(0);
      this.carSensorTable.Name = "carSensorTable";
      this.carSensorTable.RowCount = 2;
      this.carSensorTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
      this.carSensorTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
      this.carSensorTable.Size = new System.Drawing.Size(456, 50);
      this.carSensorTable.TabIndex = 1;
      // 
      // carIcon
      // 
      this.carIcon.AutoSize = true;
      this.carIcon.Dock = System.Windows.Forms.DockStyle.Fill;
      this.carIcon.Image = ((System.Drawing.Image)(resources.GetObject("carIcon.Image")));
      this.carIcon.Location = new System.Drawing.Point(0, 0);
      this.carIcon.Margin = new System.Windows.Forms.Padding(0);
      this.carIcon.Name = "carIcon";
      this.carIcon.Size = new System.Drawing.Size(21, 25);
      this.carIcon.TabIndex = 0;
      // 
      // sensorIcon
      // 
      this.sensorIcon.AutoSize = true;
      this.sensorIcon.Dock = System.Windows.Forms.DockStyle.Fill;
      this.sensorIcon.Image = ((System.Drawing.Image)(resources.GetObject("sensorIcon.Image")));
      this.sensorIcon.Location = new System.Drawing.Point(0, 25);
      this.sensorIcon.Margin = new System.Windows.Forms.Padding(0);
      this.sensorIcon.Name = "sensorIcon";
      this.sensorIcon.Size = new System.Drawing.Size(21, 25);
      this.sensorIcon.TabIndex = 1;
      // 
      // carLbl
      // 
      this.carLbl.AutoSize = true;
      this.carLbl.Dock = System.Windows.Forms.DockStyle.Fill;
      this.carLbl.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.carLbl.ForeColor = System.Drawing.Color.DarkOliveGreen;
      this.carLbl.Location = new System.Drawing.Point(21, 0);
      this.carLbl.Margin = new System.Windows.Forms.Padding(0);
      this.carLbl.Name = "carLbl";
      this.carLbl.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
      this.carLbl.Size = new System.Drawing.Size(86, 25);
      this.carLbl.TabIndex = 2;
      this.carLbl.Text = "����������:";
      this.carLbl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // sensorLbl
      // 
      this.sensorLbl.AutoSize = true;
      this.sensorLbl.Dock = System.Windows.Forms.DockStyle.Fill;
      this.sensorLbl.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.sensorLbl.Location = new System.Drawing.Point(21, 25);
      this.sensorLbl.Margin = new System.Windows.Forms.Padding(0);
      this.sensorLbl.Name = "sensorLbl";
      this.sensorLbl.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
      this.sensorLbl.Size = new System.Drawing.Size(86, 25);
      this.sensorLbl.TabIndex = 3;
      this.sensorLbl.Text = "������:";
      this.sensorLbl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // carNameText
      // 
      this.carNameText.AutoSize = true;
      this.carNameText.Dock = System.Windows.Forms.DockStyle.Fill;
      this.carNameText.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.carNameText.ForeColor = System.Drawing.Color.Black;
      this.carNameText.Location = new System.Drawing.Point(107, 0);
      this.carNameText.Margin = new System.Windows.Forms.Padding(0);
      this.carNameText.Name = "carNameText";
      this.carNameText.Size = new System.Drawing.Size(349, 25);
      this.carNameText.TabIndex = 4;
      this.carNameText.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // sensorNameText
      // 
      this.sensorNameText.AutoSize = true;
      this.sensorNameText.Dock = System.Windows.Forms.DockStyle.Fill;
      this.sensorNameText.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.sensorNameText.ForeColor = System.Drawing.Color.Black;
      this.sensorNameText.Location = new System.Drawing.Point(107, 25);
      this.sensorNameText.Margin = new System.Windows.Forms.Padding(0);
      this.sensorNameText.Name = "sensorNameText";
      this.sensorNameText.Size = new System.Drawing.Size(349, 25);
      this.sensorNameText.TabIndex = 5;
      this.sensorNameText.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // transitionGrid
      // 
      this.transitionGrid.AllowUserToAddRows = false;
      this.transitionGrid.AllowUserToDeleteRows = false;
      this.transitionGrid.AllowUserToResizeColumns = false;
      this.transitionGrid.AllowUserToResizeRows = false;
      this.transitionGrid.AutoGenerateColumns = false;
      this.transitionGrid.BackgroundColor = System.Drawing.Color.Gray;
      this.transitionGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.transitionGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.titleDataGridViewTextBoxColumn,
            this.enabledDataGridViewCheckBoxColumn,
            this.viewInReportDataGridViewCheckBoxColumn,
            this.Edit,
            this.Delete});
      this.transitionGrid.DataSource = this.sensorTransitionsBindingSource;
      dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
      dataGridViewCellStyle2.Font = new System.Drawing.Font("Tahoma", 8.25F);
      dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
      dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.White;
      dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
      dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
      this.transitionGrid.DefaultCellStyle = dataGridViewCellStyle2;
      this.transitionGrid.Dock = System.Windows.Forms.DockStyle.Fill;
      this.transitionGrid.Location = new System.Drawing.Point(10, 65);
      this.transitionGrid.Margin = new System.Windows.Forms.Padding(0, 5, 0, 5);
      this.transitionGrid.MultiSelect = false;
      this.transitionGrid.Name = "transitionGrid";
      this.transitionGrid.ReadOnly = true;
      this.transitionGrid.RowHeadersVisible = false;
      this.transitionGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
      this.transitionGrid.Size = new System.Drawing.Size(456, 85);
      this.transitionGrid.TabIndex = 2;
      this.transitionGrid.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.transitionGrid_CellFormatting);
      this.transitionGrid.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.transitionGrid_CellContentClick);
      // 
      // titleDataGridViewTextBoxColumn
      // 
      this.titleDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
      this.titleDataGridViewTextBoxColumn.DataPropertyName = "Title";
      dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      this.titleDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle1;
      this.titleDataGridViewTextBoxColumn.HeaderText = "�������� �������";
      this.titleDataGridViewTextBoxColumn.MinimumWidth = 100;
      this.titleDataGridViewTextBoxColumn.Name = "titleDataGridViewTextBoxColumn";
      this.titleDataGridViewTextBoxColumn.ReadOnly = true;
      // 
      // enabledDataGridViewCheckBoxColumn
      // 
      this.enabledDataGridViewCheckBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
      this.enabledDataGridViewCheckBoxColumn.DataPropertyName = "Enabled";
      this.enabledDataGridViewCheckBoxColumn.HeaderText = "�������";
      this.enabledDataGridViewCheckBoxColumn.MinimumWidth = 55;
      this.enabledDataGridViewCheckBoxColumn.Name = "enabledDataGridViewCheckBoxColumn";
      this.enabledDataGridViewCheckBoxColumn.ReadOnly = true;
      this.enabledDataGridViewCheckBoxColumn.Width = 55;
      // 
      // viewInReportDataGridViewCheckBoxColumn
      // 
      this.viewInReportDataGridViewCheckBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
      this.viewInReportDataGridViewCheckBoxColumn.DataPropertyName = "ViewInReport";
      this.viewInReportDataGridViewCheckBoxColumn.HeaderText = "�����";
      this.viewInReportDataGridViewCheckBoxColumn.MinimumWidth = 40;
      this.viewInReportDataGridViewCheckBoxColumn.Name = "viewInReportDataGridViewCheckBoxColumn";
      this.viewInReportDataGridViewCheckBoxColumn.ReadOnly = true;
      this.viewInReportDataGridViewCheckBoxColumn.Width = 40;
      // 
      // Edit
      // 
      this.Edit.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
      this.Edit.HeaderText = "";
      this.Edit.Image = TrackControl.General.Shared.EditSensor;
      this.Edit.MinimumWidth = 20;
      this.Edit.Name = "Edit";
      this.Edit.ReadOnly = true;
      this.Edit.Width = 20;
      // 
      // Delete
      // 
      this.Delete.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
      this.Delete.HeaderText = "";
      this.Delete.Image = TrackControl.General.Shared.DeleteSensor;
      this.Delete.MinimumWidth = 20;
      this.Delete.Name = "Delete";
      this.Delete.ReadOnly = true;
      this.Delete.Width = 20;
      // 
      // sensorTransitionsBindingSource
      // 
      this.sensorTransitionsBindingSource.DataMember = "Transition";
      this.sensorTransitionsBindingSource.DataSource = typeof(LocalCache.atlantaDataSet);
      // 
      // addPanel
      // 
      this.addPanel.BackColor = System.Drawing.Color.Transparent;
      this.addPanel.Dock = System.Windows.Forms.DockStyle.Fill;
      this.addPanel.Location = new System.Drawing.Point(0, 0);
      this.addPanel.Name = "addPanel";
      this.addPanel.Size = new System.Drawing.Size(430, 100);
      this.addPanel.TabIndex = 0;
      this.addPanel.AddPressed += new System.EventHandler(this.addPanel_AddPressed);
      this.addPanel.BackPressed += new System.EventHandler(this.closeBtn_Click);
      // 
      // SensorTransitionForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(476, 365);
      this.Controls.Add(this._mainTable);
      this.MaximizeBox = false;
      this.MaximumSize = new System.Drawing.Size(484, 400);
      this.MinimizeBox = false;
      this.MinimumSize = new System.Drawing.Size(484, 400);
      this.Name = "SensorTransitionForm";
      this.ShowIcon = false;
      this.ShowInTaskbar = false;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "������� ��� �������";
      this.Load += new System.EventHandler(this.SensorTransitionForm_Load);
      this._mainTable.ResumeLayout(false);
      this.carSensorTable.ResumeLayout(false);
      this.carSensorTable.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.transitionGrid)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.sensorTransitionsBindingSource)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.TableLayoutPanel _mainTable;
    private System.Windows.Forms.GroupBox editGroupBox;
    private TrackControl.Setting.Controls.AddButtonControl addPanel;
    private System.Windows.Forms.TableLayoutPanel carSensorTable;
    private System.Windows.Forms.Label carIcon;
    private System.Windows.Forms.Label sensorIcon;
    private System.Windows.Forms.Label carLbl;
    private System.Windows.Forms.Label sensorLbl;
    private System.Windows.Forms.Label carNameText;
    private System.Windows.Forms.Label sensorNameText;
    private System.Windows.Forms.BindingSource sensorTransitionsBindingSource;
    private System.Windows.Forms.DataGridView transitionGrid;
    private System.Windows.Forms.DataGridViewTextBoxColumn titleDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewCheckBoxColumn enabledDataGridViewCheckBoxColumn;
    private System.Windows.Forms.DataGridViewCheckBoxColumn viewInReportDataGridViewCheckBoxColumn;
    private System.Windows.Forms.DataGridViewImageColumn Edit;
    private System.Windows.Forms.DataGridViewImageColumn Delete;
  }
}