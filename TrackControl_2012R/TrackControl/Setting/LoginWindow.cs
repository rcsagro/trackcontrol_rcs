using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace TrackControl.Setting.Controls
{
    /// <summary>
    /// ���� �����������. ����������� ��� ����������� ������� � ���������� ��������.
    /// </summary>
    public partial class LoginWindow : UserControl
    {
        const string ACCESS_BOX_WELCOME_TEXT = "������� ������";
        const string ACCESS_BOX_WRONG_PASSWORD_TEXT = "�������!";

        #region Fields
        /// <summary>
        /// ��� ������
        /// </summary>
        private string _passHash;
        #endregion

        #region .ctor
        /// <summary>
        /// �����������
        /// </summary>
        public LoginWindow()
        {
            InitializeComponent();
            Reset();
        }

        /// <summary>
        /// �����������
        /// </summary>
        /// <param name="passHash">��� ������</param>
        public LoginWindow(string passHash)
            : this()
        {
            _passHash = passHash;
        }
        #endregion

        #region Events
        /// <summary>
        /// ��������� ��� �������� ����� ����������� ������
        /// </summary>
        public event EventHandler Authorize;
        #endregion

        #region Public Methods
        /// <summary>
        /// �������� ��������� ���� ����������� � ��������� ���������
        /// </summary>
        public void Reset()
        {
            this.accessBox.BackColor = Color.WhiteSmoke;
            this.accessBox.ForeColor = Color.DimGray;
            this.accessBox.UseSystemPasswordChar = false;
            this.accessBox.Text = ACCESS_BOX_WELCOME_TEXT;
            this.ActiveControl = tableLayoutPanel1;
            this.accessBox.Enabled = true;
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// ���������� ������� �� �������� �����������
        /// </summary>
        private void onAuthorize()
        {
            if (Authorize != null)
                Authorize(this, EventArgs.Empty);
        }

        /// <summary>
        /// ��������� ������������ ���������� ������
        /// </summary>
        /// <param name="password">������</param>
        private bool checkPassword(string password)
        {
            if (getPassHash(password) == _passHash)
                return true;

            return false;
        }

        /// <summary>
        /// ���������� ��� ������
        /// </summary>
        /// <param name="password">������</param>
        /// <returns>��� ������</returns>
        private string getPassHash(string password)
        {
            // ��� �����
            return password;
        }
        #endregion
        
        #region GUI Event Handlers
        /// <summary>
        /// ������������ ������� ��������� ������ ����� ��� ����� ������
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void accessBox_Enter(object sender, EventArgs e)
        {
            this.accessBox.Text = "";
            this.accessBox.UseSystemPasswordChar = true;
            this.accessBox.ForeColor = Color.Black;
            this.accessBox.BackColor = Color.WhiteSmoke;
        }

        /// <summary>
        /// ������������ ������� ����� ������� � ���� ������
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void accessBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar.ToString() != "\r")
                return;

            this.accessBox.Enabled = false;

            // ������ �������� �� 0.5 ���. ����� ��������� ������ ������.
            Thread.Sleep(500);

            if (checkPassword(this.accessBox.Text))
            {
                onAuthorize();
            }
            else
            {
                this.accessBox.BackColor = Color.PeachPuff;
                this.accessBox.ForeColor = Color.Red;
                this.accessBox.UseSystemPasswordChar = false;
                this.accessBox.Text = ACCESS_BOX_WRONG_PASSWORD_TEXT;
                this.accessBox.Enabled = true;
                this.ActiveControl = tableLayoutPanel1;
            }
        }
        #endregion

        
    }
}
