﻿namespace TrackControl.Setting
{
    partial class PcbSensors
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose( bool disposing )
        {
            if( disposing && ( components != null ) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PcbSensors));
            this.lblInfo = new DevExpress.XtraEditors.LabelControl();
            this.lblName = new DevExpress.XtraEditors.LabelControl();
            this.gridControlPcb = new DevExpress.XtraGrid.GridControl();
            this.gridViewPcb = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.IDPcb = new DevExpress.XtraGrid.Columns.GridColumn();
            this.nameTypeData = new DevExpress.XtraGrid.Columns.GridColumn();
            this.koeffK = new DevExpress.XtraGrid.Columns.GridColumn();
            this.strtBit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.lnghtBit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.nameValueMeasure = new DevExpress.XtraGrid.Columns.GridColumn();
            this.nameAlgorithm = new DevExpress.XtraGrid.Columns.GridColumn();
            this.algorithmItemComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.Commt = new DevExpress.XtraGrid.Columns.GridColumn();
            this.PcbId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControlPcbList = new DevExpress.XtraGrid.GridControl();
            this.gridViewPcbList = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.namePcbs = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Comment = new DevExpress.XtraGrid.Columns.GridColumn();
            this.lblListPcb = new DevExpress.XtraEditors.LabelControl();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.btnExportesXml = new DevExpress.XtraEditors.SimpleButton();
            this.btnImportesXml = new DevExpress.XtraEditors.SimpleButton();
            this.importSCAD = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlPcb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPcb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.algorithmItemComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlPcbList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPcbList)).BeginInit();
            this.SuspendLayout();
            // 
            // lblInfo
            // 
            this.lblInfo.Location = new System.Drawing.Point(458, 17);
            this.lblInfo.Name = "lblInfo";
            this.lblInfo.Size = new System.Drawing.Size(325, 13);
            this.lblInfo.TabIndex = 2;
            this.lblInfo.Text = "Распределение данных для версии исполнения плат датчиков:";
            // 
            // lblName
            // 
            this.lblName.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblName.Location = new System.Drawing.Point(458, 36);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(111, 13);
            this.lblName.TabIndex = 3;
            this.lblName.Text = "SCAD_MR vers_01e";
            // 
            // gridControlPcb
            // 
            this.gridControlPcb.Location = new System.Drawing.Point(439, 55);
            this.gridControlPcb.MainView = this.gridViewPcb;
            this.gridControlPcb.Name = "gridControlPcb";
            this.gridControlPcb.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.algorithmItemComboBox1});
            this.gridControlPcb.Size = new System.Drawing.Size(626, 315);
            this.gridControlPcb.TabIndex = 4;
            this.gridControlPcb.UseEmbeddedNavigator = true;
            this.gridControlPcb.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewPcb});
            // 
            // gridViewPcb
            // 
            this.gridViewPcb.ColumnPanelRowHeight = 40;
            this.gridViewPcb.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.IDPcb,
            this.nameTypeData,
            this.koeffK,
            this.strtBit,
            this.lnghtBit,
            this.nameValueMeasure,
            this.nameAlgorithm,
            this.Commt,
            this.PcbId});
            this.gridViewPcb.GridControl = this.gridControlPcb;
            this.gridViewPcb.Name = "gridViewPcb";
            this.gridViewPcb.OptionsView.ColumnAutoWidth = false;
            this.gridViewPcb.OptionsView.ShowGroupPanel = false;
            // 
            // IDPcb
            // 
            this.IDPcb.AppearanceCell.Options.UseTextOptions = true;
            this.IDPcb.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.IDPcb.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.IDPcb.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.IDPcb.AppearanceHeader.Options.UseTextOptions = true;
            this.IDPcb.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.IDPcb.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.IDPcb.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.IDPcb.Caption = "ID";
            this.IDPcb.FieldName = "ID";
            this.IDPcb.Name = "IDPcb";
            this.IDPcb.OptionsColumn.AllowEdit = false;
            this.IDPcb.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.IDPcb.OptionsColumn.ReadOnly = true;
            this.IDPcb.ToolTip = "Идентификатор";
            this.IDPcb.Width = 60;
            // 
            // nameTypeData
            // 
            this.nameTypeData.AppearanceCell.Options.UseTextOptions = true;
            this.nameTypeData.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.nameTypeData.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.nameTypeData.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.nameTypeData.AppearanceHeader.Options.UseTextOptions = true;
            this.nameTypeData.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.nameTypeData.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.nameTypeData.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.nameTypeData.Caption = "Название поля";
            this.nameTypeData.FieldName = "TypeDataPcb";
            this.nameTypeData.Name = "nameTypeData";
            this.nameTypeData.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.nameTypeData.ToolTip = "Название поля/Тип данных";
            this.nameTypeData.Visible = true;
            this.nameTypeData.VisibleIndex = 0;
            this.nameTypeData.Width = 138;
            // 
            // koeffK
            // 
            this.koeffK.AppearanceCell.Options.UseTextOptions = true;
            this.koeffK.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.koeffK.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.koeffK.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.koeffK.AppearanceHeader.Options.UseTextOptions = true;
            this.koeffK.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.koeffK.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.koeffK.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.koeffK.Caption = "Коеффициент К";
            this.koeffK.FieldName = "KoefficK";
            this.koeffK.Name = "koeffK";
            this.koeffK.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.koeffK.ToolTip = "Коеффициент К";
            this.koeffK.Visible = true;
            this.koeffK.VisibleIndex = 1;
            this.koeffK.Width = 93;
            // 
            // strtBit
            // 
            this.strtBit.AppearanceCell.Options.UseTextOptions = true;
            this.strtBit.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.strtBit.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.strtBit.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.strtBit.AppearanceHeader.Options.UseTextOptions = true;
            this.strtBit.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.strtBit.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.strtBit.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.strtBit.Caption = "Стартовый бит";
            this.strtBit.FieldName = "StrtBit";
            this.strtBit.Name = "strtBit";
            this.strtBit.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.strtBit.ToolTip = "Номер стартового бита";
            this.strtBit.Visible = true;
            this.strtBit.VisibleIndex = 2;
            this.strtBit.Width = 60;
            // 
            // lnghtBit
            // 
            this.lnghtBit.AppearanceCell.Options.UseTextOptions = true;
            this.lnghtBit.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lnghtBit.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.lnghtBit.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.lnghtBit.AppearanceHeader.Options.UseTextOptions = true;
            this.lnghtBit.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lnghtBit.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.lnghtBit.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lnghtBit.Caption = "Длина поля в битах";
            this.lnghtBit.FieldName = "LngthBit";
            this.lnghtBit.Name = "lnghtBit";
            this.lnghtBit.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.lnghtBit.ToolTip = "Длина поля в битах";
            this.lnghtBit.Visible = true;
            this.lnghtBit.VisibleIndex = 3;
            this.lnghtBit.Width = 57;
            // 
            // nameValueMeasure
            // 
            this.nameValueMeasure.AppearanceCell.Options.UseTextOptions = true;
            this.nameValueMeasure.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.nameValueMeasure.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.nameValueMeasure.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.nameValueMeasure.AppearanceHeader.Options.UseTextOptions = true;
            this.nameValueMeasure.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.nameValueMeasure.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.nameValueMeasure.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.nameValueMeasure.Caption = "Величина измерения";
            this.nameValueMeasure.FieldName = "NameValueMeasure";
            this.nameValueMeasure.Name = "nameValueMeasure";
            this.nameValueMeasure.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.nameValueMeasure.ToolTip = "Название величины измерения";
            this.nameValueMeasure.Visible = true;
            this.nameValueMeasure.VisibleIndex = 4;
            // 
            // nameAlgorithm
            // 
            this.nameAlgorithm.AppearanceCell.Options.UseTextOptions = true;
            this.nameAlgorithm.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.nameAlgorithm.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.nameAlgorithm.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.nameAlgorithm.AppearanceHeader.Options.UseTextOptions = true;
            this.nameAlgorithm.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.nameAlgorithm.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.nameAlgorithm.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.nameAlgorithm.Caption = "Название алгоритма";
            this.nameAlgorithm.ColumnEdit = this.algorithmItemComboBox1;
            this.nameAlgorithm.FieldName = "Algorithm";
            this.nameAlgorithm.Name = "nameAlgorithm";
            this.nameAlgorithm.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.nameAlgorithm.ToolTip = "Название алгоритма";
            this.nameAlgorithm.Visible = true;
            this.nameAlgorithm.VisibleIndex = 5;
            // 
            // algorithmItemComboBox1
            // 
            this.algorithmItemComboBox1.Appearance.Options.UseTextOptions = true;
            this.algorithmItemComboBox1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.algorithmItemComboBox1.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.algorithmItemComboBox1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.algorithmItemComboBox1.AppearanceDisabled.Options.UseTextOptions = true;
            this.algorithmItemComboBox1.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.algorithmItemComboBox1.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.algorithmItemComboBox1.AppearanceDisabled.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.algorithmItemComboBox1.AppearanceDropDown.Options.UseTextOptions = true;
            this.algorithmItemComboBox1.AppearanceDropDown.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.algorithmItemComboBox1.AppearanceDropDown.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.algorithmItemComboBox1.AppearanceDropDown.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.algorithmItemComboBox1.AppearanceFocused.Options.UseTextOptions = true;
            this.algorithmItemComboBox1.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.algorithmItemComboBox1.AppearanceFocused.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.algorithmItemComboBox1.AppearanceFocused.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.algorithmItemComboBox1.AppearanceReadOnly.Options.UseTextOptions = true;
            this.algorithmItemComboBox1.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.algorithmItemComboBox1.AppearanceReadOnly.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.algorithmItemComboBox1.AppearanceReadOnly.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.algorithmItemComboBox1.AutoHeight = false;
            this.algorithmItemComboBox1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            serializableAppearanceObject1.Options.UseTextOptions = true;
            serializableAppearanceObject1.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            serializableAppearanceObject1.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            serializableAppearanceObject1.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.algorithmItemComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "Алгоритм", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "Выбор алгоритма", null, null, true)});
            this.algorithmItemComboBox1.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.algorithmItemComboBox1.DropDownRows = 10;
            this.algorithmItemComboBox1.Items.AddRange(new object[] {
            "-- Алгоритм не выбран --"});
            this.algorithmItemComboBox1.Name = "algorithmItemComboBox1";
            this.algorithmItemComboBox1.Sorted = true;
            this.algorithmItemComboBox1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // Commt
            // 
            this.Commt.AppearanceCell.Options.UseTextOptions = true;
            this.Commt.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.Commt.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.Commt.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Commt.AppearanceHeader.Options.UseTextOptions = true;
            this.Commt.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Commt.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.Commt.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Commt.Caption = "Примечание";
            this.Commt.FieldName = "Commentary";
            this.Commt.Name = "Commt";
            this.Commt.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.Commt.ToolTip = "Примечание";
            this.Commt.Visible = true;
            this.Commt.VisibleIndex = 6;
            this.Commt.Width = 120;
            // 
            // PcbId
            // 
            this.PcbId.AppearanceCell.Options.UseTextOptions = true;
            this.PcbId.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.PcbId.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.PcbId.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.PcbId.AppearanceHeader.Options.UseTextOptions = true;
            this.PcbId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.PcbId.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.PcbId.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.PcbId.Caption = "Идентификатор версии платы";
            this.PcbId.FieldName = "ID_Pcb";
            this.PcbId.Name = "PcbId";
            this.PcbId.OptionsColumn.AllowEdit = false;
            this.PcbId.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.PcbId.OptionsColumn.ReadOnly = true;
            this.PcbId.Width = 20;
            // 
            // gridControlPcbList
            // 
            this.gridControlPcbList.Location = new System.Drawing.Point(16, 55);
            this.gridControlPcbList.MainView = this.gridViewPcbList;
            this.gridControlPcbList.Name = "gridControlPcbList";
            this.gridControlPcbList.Size = new System.Drawing.Size(417, 317);
            this.gridControlPcbList.TabIndex = 5;
            this.gridControlPcbList.UseEmbeddedNavigator = true;
            this.gridControlPcbList.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewPcbList});
            // 
            // gridViewPcbList
            // 
            this.gridViewPcbList.ColumnPanelRowHeight = 40;
            this.gridViewPcbList.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.ID,
            this.namePcbs,
            this.Comment});
            this.gridViewPcbList.GridControl = this.gridControlPcbList;
            this.gridViewPcbList.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always;
            this.gridViewPcbList.Name = "gridViewPcbList";
            this.gridViewPcbList.OptionsDetail.AllowZoomDetail = false;
            this.gridViewPcbList.OptionsDetail.EnableMasterViewMode = false;
            this.gridViewPcbList.OptionsDetail.ShowDetailTabs = false;
            this.gridViewPcbList.OptionsDetail.SmartDetailExpand = false;
            this.gridViewPcbList.OptionsView.ShowGroupPanel = false;
            // 
            // ID
            // 
            this.ID.AppearanceCell.Options.UseTextOptions = true;
            this.ID.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ID.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.ID.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.ID.AppearanceHeader.Options.UseTextOptions = true;
            this.ID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ID.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.ID.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.ID.Caption = "ID";
            this.ID.FieldName = "IDPcb";
            this.ID.Name = "ID";
            this.ID.ToolTip = "ID";
            this.ID.Width = 20;
            // 
            // namePcbs
            // 
            this.namePcbs.AppearanceCell.Options.UseTextOptions = true;
            this.namePcbs.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.namePcbs.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.namePcbs.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.namePcbs.AppearanceHeader.Options.UseTextOptions = true;
            this.namePcbs.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.namePcbs.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.namePcbs.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.namePcbs.Caption = "Название версий плат датчиков";
            this.namePcbs.FieldName = "nmPcbs";
            this.namePcbs.Name = "namePcbs";
            this.namePcbs.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.namePcbs.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count)});
            this.namePcbs.ToolTip = "Название версий плат датчиков";
            this.namePcbs.Visible = true;
            this.namePcbs.VisibleIndex = 0;
            this.namePcbs.Width = 141;
            // 
            // Comment
            // 
            this.Comment.AppearanceCell.Options.UseTextOptions = true;
            this.Comment.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.Comment.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.Comment.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Comment.AppearanceHeader.Options.UseTextOptions = true;
            this.Comment.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Comment.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.Comment.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Comment.Caption = "Комментарий";
            this.Comment.FieldName = "Comms";
            this.Comment.Name = "Comment";
            this.Comment.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.Comment.ToolTip = "Комментарий";
            this.Comment.Visible = true;
            this.Comment.VisibleIndex = 1;
            this.Comment.Width = 157;
            // 
            // lblListPcb
            // 
            this.lblListPcb.Location = new System.Drawing.Point(16, 36);
            this.lblListPcb.Name = "lblListPcb";
            this.lblListPcb.Size = new System.Drawing.Size(206, 13);
            this.lblListPcb.TabIndex = 6;
            this.lblListPcb.Text = "Список версий печатных плат датчиков";
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "add.png");
            this.imageList1.Images.SetKeyName(1, "bullet_add_1.png");
            this.imageList1.Images.SetKeyName(2, "bullet_add_2.png");
            this.imageList1.Images.SetKeyName(3, "arrow_undo.png");
            this.imageList1.Images.SetKeyName(4, "arrow_left.png");
            this.imageList1.Images.SetKeyName(5, "arrow_right.png");
            // 
            // btnExportesXml
            // 
            this.btnExportesXml.ImageIndex = 4;
            this.btnExportesXml.ImageList = this.imageList1;
            this.btnExportesXml.Location = new System.Drawing.Point(693, 393);
            this.btnExportesXml.Name = "btnExportesXml";
            this.btnExportesXml.Size = new System.Drawing.Size(128, 23);
            this.btnExportesXml.TabIndex = 7;
            this.btnExportesXml.Text = "Импорт из XML";
            this.btnExportesXml.ToolTip = "Импорт данных из XML";
            // 
            // btnImportesXml
            // 
            this.btnImportesXml.ImageIndex = 5;
            this.btnImportesXml.ImageList = this.imageList1;
            this.btnImportesXml.Location = new System.Drawing.Point(853, 393);
            this.btnImportesXml.Name = "btnImportesXml";
            this.btnImportesXml.Size = new System.Drawing.Size(129, 23);
            this.btnImportesXml.TabIndex = 8;
            this.btnImportesXml.Text = "Экспорт в XML";
            this.btnImportesXml.ToolTip = "Экспорт данных в XML";
            // 
            // importSCAD
            // 
            this.importSCAD.Image = ((System.Drawing.Image)(resources.GetObject("importSCAD.Image")));
            this.importSCAD.Location = new System.Drawing.Point(511, 393);
            this.importSCAD.Name = "importSCAD";
            this.importSCAD.Size = new System.Drawing.Size(96, 23);
            this.importSCAD.TabIndex = 9;
            this.importSCAD.Text = "Импорт SCAD";
            this.importSCAD.ToolTip = "Импорт данных из SCAD файлов";
            this.importSCAD.Click += new System.EventHandler(this.importSCAD_Click);
            // 
            // PcbSensors
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.importSCAD);
            this.Controls.Add(this.btnImportesXml);
            this.Controls.Add(this.btnExportesXml);
            this.Controls.Add(this.lblListPcb);
            this.Controls.Add(this.gridControlPcbList);
            this.Controls.Add(this.gridControlPcb);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.lblInfo);
            this.Name = "PcbSensors";
            this.Size = new System.Drawing.Size(1027, 419);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlPcb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPcb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.algorithmItemComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlPcbList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPcbList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl lblInfo;
        private DevExpress.XtraEditors.LabelControl lblName;
        private DevExpress.XtraGrid.GridControl gridControlPcb;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewPcb;
        private DevExpress.XtraGrid.Columns.GridColumn nameTypeData;
        private DevExpress.XtraGrid.Columns.GridColumn koeffK;
        private DevExpress.XtraGrid.Columns.GridColumn strtBit;
        private DevExpress.XtraGrid.Columns.GridColumn lnghtBit;
        private DevExpress.XtraGrid.GridControl gridControlPcbList;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewPcbList;
        private DevExpress.XtraGrid.Columns.GridColumn namePcbs;
        private DevExpress.XtraEditors.LabelControl lblListPcb;
        private DevExpress.XtraGrid.Columns.GridColumn Comment;
        private DevExpress.XtraGrid.Columns.GridColumn ID;
        private System.Windows.Forms.ImageList imageList1;
        private DevExpress.XtraGrid.Columns.GridColumn Commt;
        private DevExpress.XtraGrid.Columns.GridColumn IDPcb;
        private DevExpress.XtraGrid.Columns.GridColumn PcbId;
        private DevExpress.XtraEditors.SimpleButton btnExportesXml;
        private DevExpress.XtraEditors.SimpleButton btnImportesXml;
        private DevExpress.XtraEditors.SimpleButton importSCAD;
        private DevExpress.XtraGrid.Columns.GridColumn nameValueMeasure;
        private DevExpress.XtraGrid.Columns.GridColumn nameAlgorithm;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox algorithmItemComboBox1;
    }
}
