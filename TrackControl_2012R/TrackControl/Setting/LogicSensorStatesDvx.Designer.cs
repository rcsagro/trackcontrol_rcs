﻿namespace TrackControl.Setting
{
    partial class LogicSensorStatesDvx
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && ( components != null ) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.editPanel = new DevExpress.XtraEditors.GroupControl();
            this.stateTable = new System.Windows.Forms.TableLayoutPanel();
            this.tableForMainBtn = new System.Windows.Forms.TableLayoutPanel();
            this.eventPanel = new DevExpress.XtraEditors.GroupControl();
            this.transitionTable = new System.Windows.Forms.TableLayoutPanel();
            this.tableForEventBtn = new System.Windows.Forms.TableLayoutPanel();
            this.vehicleBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mobitelsSensorsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mobitelsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.editPanel)).BeginInit();
            this.editPanel.SuspendLayout();
            this.stateTable.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.eventPanel)).BeginInit();
            this.eventPanel.SuspendLayout();
            this.transitionTable.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vehicleBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mobitelsSensorsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mobitelsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.IsSplitterFixed = true;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.editPanel);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.eventPanel);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(976, 336);
            this.splitContainerControl1.SplitterPosition = 460;
            this.splitContainerControl1.TabIndex = 0;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // editPanel
            // 
            this.editPanel.Appearance.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold);
            this.editPanel.Appearance.Options.UseFont = true;
            this.editPanel.AutoSize = true;
            this.editPanel.Controls.Add(this.stateTable);
            this.editPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.editPanel.Location = new System.Drawing.Point(0, 0);
            this.editPanel.Name = "editPanel";
            this.editPanel.Size = new System.Drawing.Size(460, 336);
            this.editPanel.TabIndex = 0;
            this.editPanel.Text = "Состояния";
            // 
            // stateTable
            // 
            this.stateTable.AutoSize = true;
            this.stateTable.BackColor = System.Drawing.Color.WhiteSmoke;
            this.stateTable.ColumnCount = 1;
            this.stateTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.stateTable.Controls.Add(this.tableForMainBtn, 0, 6);
            this.stateTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.stateTable.Location = new System.Drawing.Point(2, 21);
            this.stateTable.Margin = new System.Windows.Forms.Padding(0);
            this.stateTable.Name = "stateTable";
            this.stateTable.RowCount = 8;
            this.stateTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.stateTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.stateTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.stateTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.stateTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.stateTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.stateTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.stateTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.stateTable.Size = new System.Drawing.Size(456, 313);
            this.stateTable.TabIndex = 1;
            // 
            // tableForMainBtn
            // 
            this.tableForMainBtn.ColumnCount = 2;
            this.tableForMainBtn.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableForMainBtn.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 243F));
            this.tableForMainBtn.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableForMainBtn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableForMainBtn.Location = new System.Drawing.Point(0, 152);
            this.tableForMainBtn.Margin = new System.Windows.Forms.Padding(0);
            this.tableForMainBtn.Name = "tableForMainBtn";
            this.tableForMainBtn.RowCount = 1;
            this.tableForMainBtn.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableForMainBtn.Size = new System.Drawing.Size(456, 27);
            this.tableForMainBtn.TabIndex = 4;
            // 
            // eventPanel
            // 
            this.eventPanel.AutoSize = true;
            this.eventPanel.Controls.Add(this.transitionTable);
            this.eventPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.eventPanel.Location = new System.Drawing.Point(0, 0);
            this.eventPanel.Name = "eventPanel";
            this.eventPanel.Size = new System.Drawing.Size(511, 336);
            this.eventPanel.TabIndex = 0;
            this.eventPanel.Text = "События";
            // 
            // transitionTable
            // 
            this.transitionTable.AutoSize = true;
            this.transitionTable.BackColor = System.Drawing.Color.WhiteSmoke;
            this.transitionTable.ColumnCount = 1;
            this.transitionTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.transitionTable.Controls.Add(this.tableForEventBtn, 0, 3);
            this.transitionTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.transitionTable.Location = new System.Drawing.Point(2, 21);
            this.transitionTable.Margin = new System.Windows.Forms.Padding(0);
            this.transitionTable.Name = "transitionTable";
            this.transitionTable.RowCount = 5;
            this.transitionTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.transitionTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.transitionTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 119F));
            this.transitionTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.transitionTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.transitionTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.transitionTable.Size = new System.Drawing.Size(507, 313);
            this.transitionTable.TabIndex = 1;
            // 
            // tableForEventBtn
            // 
            this.tableForEventBtn.ColumnCount = 2;
            this.tableForEventBtn.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableForEventBtn.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 239F));
            this.tableForEventBtn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableForEventBtn.Location = new System.Drawing.Point(0, 153);
            this.tableForEventBtn.Margin = new System.Windows.Forms.Padding(0);
            this.tableForEventBtn.Name = "tableForEventBtn";
            this.tableForEventBtn.RowCount = 1;
            this.tableForEventBtn.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableForEventBtn.Size = new System.Drawing.Size(507, 27);
            this.tableForEventBtn.TabIndex = 4;
            // 
            // mobitelsSensorsBindingSource
            // 
            this.mobitelsSensorsBindingSource.DataMember = "mobitels_sensors";
            this.mobitelsSensorsBindingSource.DataSource = this.mobitelsBindingSource;
            this.mobitelsSensorsBindingSource.Filter = "Length=1";
            this.mobitelsSensorsBindingSource.Sort = "Id ASC";
            // 
            // mobitelsBindingSource
            // 
            this.mobitelsBindingSource.DataMember = "mobitels";
            this.mobitelsBindingSource.DataSource = typeof(LocalCache.atlantaDataSet);
            // 
            // LogicSensorStatesDvx
            // 
            this.ActiveGlowColor = System.Drawing.Color.Empty;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(976, 336);
            this.Controls.Add(this.splitContainerControl1);
            this.FormBorderEffect = DevExpress.XtraEditors.FormBorderEffect.Shadow;
            this.InactiveGlowColor = System.Drawing.Color.Empty;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "LogicSensorStatesDvx";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Логические датчики";
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.editPanel)).EndInit();
            this.editPanel.ResumeLayout(false);
            this.editPanel.PerformLayout();
            this.stateTable.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.eventPanel)).EndInit();
            this.eventPanel.ResumeLayout(false);
            this.eventPanel.PerformLayout();
            this.transitionTable.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vehicleBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mobitelsSensorsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mobitelsBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraEditors.GroupControl editPanel;
        private DevExpress.XtraEditors.GroupControl eventPanel;
        private System.Windows.Forms.TableLayoutPanel stateTable;
        private System.Windows.Forms.TableLayoutPanel tableForMainBtn;
        private System.Windows.Forms.TableLayoutPanel transitionTable;
        private System.Windows.Forms.TableLayoutPanel tableForEventBtn;
        private System.Windows.Forms.BindingSource mobitelsBindingSource;
        private System.Windows.Forms.BindingSource vehicleBindingSource;
        private System.Windows.Forms.BindingSource mobitelsSensorsBindingSource;
    }
}