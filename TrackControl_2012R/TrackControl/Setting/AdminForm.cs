using BaseReports.Procedure;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using LocalCache;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using TrackControl.General.DAL;
using TrackControl.General.DatabaseDriver;
using TrackControl.General.Patameters;
using TrackControl.GMap.MapProviders;
using TrackControl.GMap.UI;
using TrackControl.Properties;
using TrackControl.Reports;
using TrackControl.Vehicles;

namespace TrackControl.Setting
{
    /// <summary>
    /// ����� �������� ����������
    /// </summary>
    public partial class AdminForm : XtraUserControl
    {
        public class AnalisData
        {
            private string xcar;
            private DateTime xdateb;
            private DateTime xdatee;
            private TimeSpan xinterval;
            private long xqtypoints;

            public AnalisData(string car, DateTime dateb, DateTime datee, TimeSpan interval, long numpoints)
            {
                xcar = car;
                xdateb = dateb;
                xdatee = datee;
                xinterval = interval;
                xqtypoints = numpoints;
            }

            public string xCar
            {
                get
                {
                    return xcar;
                }
            }

            public DateTime xDateB
            {
                get
                {
                    return xdateb;
                }
            }

            public DateTime xDateE
            {
                get
                {
                    return xdatee;
                }
            }

            public TimeSpan xInterval
            {
                get
                {
                    return xinterval;
                }
            }

            public long xQTYpoints
            {
                get
                {
                    return xqtypoints;
                }
            }
        } // AnalisData

        public class PcbVersion
        {
            private int ID;
            private string Name;
            private string Comment;
            private string Setting;

            public PcbVersion( int id, string name, string comment )
            {
                this.ID = id;
                this.Name = name;
                this.Comment = comment;
            }

            public int IDPcb
            {
                get
                {
                    return this.ID;
                }

                set
                {
                    this.ID = value;
                }
            }

            public string nmPcbs
            {
                get
                {
                    return this.Name;
                }

                set
                {
                    if( value != null )
                    {
                        if( value.Length > 50 )
                        {
                            XtraMessageBox.Show( Resources.ErrorLengthType, Resources.ErrorPcb,
                                MessageBoxButtons.OK );
                            return;
                        }

                        this.Name = value;
                    }
                    else
                        this.Name = "****";
                }
            }

            public string Comms
            {
                get
                {
                    return this.Comment;
                }

                set
                {
                    if( value != null )
                    {
                        if( value.Length > 50 )
                        {
                            XtraMessageBox.Show( Resources.ErrorLengthComment, Resources.ErrorPcb,
                                MessageBoxButtons.OK );
                            return;
                        }

                        this.Comment = value;
                    }
                    else
                        this.Comment = "";
                }
            }

            public string settVehic 
            {
                get { return this.Setting; }
                set { this.Setting = value; }
            }
        } // PcbVersion

        public class PcbDataVersion
        {
            private int Id;
            private string TypeData;
            private double KoeffK;
            private int StartBit;
            private int LengthBit;
            private string Comment;
            private int IDPcb;
            private string tarirov;
            private bool states;
            private bool situati;
            private string algorithm;
            private string nameUnit;
            private bool Is64bitPackets;
            private double KoeffB;
            private bool Signed;

            public PcbDataVersion( int id, string typedata, double koeffK, double koeffB, bool signed, int startBit, int lengthBit, string comment, int idPcb,
                string nmUnit, bool is64bitPackets)
            {
                this.Id = id;
                this.TypeData = typedata;
                this.KoeffK = koeffK;
                KoeffB = koeffB;
                Signed = signed;
                this.StartBit = startBit;
                this.LengthBit = lengthBit;
                this.Comment = comment;
                this.IDPcb = idPcb;
                this.tarirov = "----";
                this.states = false;
                this.situati = false;
                this.algorithm = Resources.NoAlgorithm;
                this.nameUnit = nmUnit;
                this.Is64bitPackets = is64bitPackets;
            }

            public string NameUnit
            {
                get
                {
                    return nameUnit;
                }

                set
                {
                    this.nameUnit = value;
                }
            }

            public int ID
            {
                get { return this.Id; }

                set { this.Id = value; }
            }

            public string TypeDataPcb
            {
                get { return this.TypeData; }

                set
                {
                    if( value == null )
                        this.TypeData = "*";
                    else
                    {
                        if( value.Length > 255 )
                        {
                            XtraMessageBox.Show( Resources.ErrorLengthName, Resources.ErrorPcb,
                                MessageBoxButtons.OK );
                            return;
                        }

                        this.TypeData = value;
                    }
                }
            }

            public string KoefficK
            {
                get { return this.KoeffK.ToString(); }

                set
                {
                    this.KoeffK = Math.Round(Convert.ToDouble( value ), 6);
                }
            }

            public string Shifter
            {
                get { return this.KoeffB.ToString(); }

                set
                {
                    this.KoeffB = Math.Round(Convert.ToDouble(value), 6);
                }
            }

            public bool Signing
            {
                get { return Signed; }

                set
                {
                    Signed = value;
                }
            }

            public int StrtBit
            {
                get { return this.StartBit; }

                set
                {
                    if (Is64bitPackets)
                    {
                        if ((value > 200) || (value < 0))
                        {
                            // to do
                        }
                        //throw new Exception("Start bits may be in the range from 0 to 200 bit!");
                    }
                    else if ((value > 63) || (value < 0))
                    {
                        // to do
                    }
                    //throw new Exception("Start bits may be in the range from 0 to 63 bit!");
                    this.StartBit = value;
                }
            }

            public int LngthBit
            {
                get { return this.LengthBit; }

                set
                {
                    if (Is64bitPackets)
                    {
                        if ((value + (this.StrtBit - 1)) > 200)
                        {
                            // to do
                        }
                        //throw new Exception("Field width bits under the border 200 bit!");
                    }
                    else if ((value + (this.StrtBit - 1)) > 63)
                    {
                        // to do
                    }
                    //throw new Exception("Field width bits under the border 63 bit!");
                    this.LengthBit = value;
                }
            }

            public string Commentary
            {
                get { return this.Comment; }

                set
                {
                    if( value == null )
                    {
                        this.Comment = "";
                    }
                    else
                    {
                        if( value.Length > 255 )
                        {
                            XtraMessageBox.Show( Resources.ErrorLenthEcrise, Resources.ErrorPcb,
                                MessageBoxButtons.OK );
                            return;
                        }

                        this.Comment = value;
                    }
                }
            }

            public int ID_Pcb
            {
                get { return this.IDPcb; }

                set
                {
                    this.IDPcb = value;
                }
            }

            public string Tarirovich
            {
                get { return this.tarirov; }

                set
                {
                    this.tarirov = value;
                }
            }

            public bool State
            {
                get { return this.states; }

                set
                {
                    this.states = value;
                }
            }

            public bool Situation
            {
                get { return this.situati; }

                set
                {
                    this.situati = value;
                }
            }

            public string Algorithm
            {
                get { return this.algorithm; }

                set
                {
                    this.algorithm = value;
                }
            }
        } // PcbDataVersion

        public class PcbDataLogic
        {
            private int Id;
            private string name;
            private int strtbt;
            private string spec;
            private string stateFalseBox1Text;
            private string stateTrueBox1Text;
            private bool conditions;
            private bool events;
            private bool Is64bitPackets;

            public PcbDataLogic( int id, string name, int strtbt, string spec, string stateFalseBox1Text, string stateTrueBox1Text , bool is64bitPackets)
            {
                this.Id = id;
                this.name = name;
                this.strtbt = strtbt;
                this.spec = spec;
                this.stateFalseBox1Text = stateFalseBox1Text;
                this.stateTrueBox1Text = stateTrueBox1Text;
                this.conditions = true;
                this.events = true;
                this.Is64bitPackets = is64bitPackets;
            }

            public string Name
            {
                get { return name; }
                set { name = value; }
            }

            public int StartBit
            {
                get { return strtbt; }
                set
                {
                    if (Is64bitPackets)
                    {
                        if ((value > 200) || (value < 0))
                        {
                            // to do
                        }
                        //throw new Exception("Start bits may be in the range from 0 to 200 bit!");
                    }
                    else if ((value > 63) || (value < 0))
                    {
                        // to do
                    }
                    //throw new Exception("Start bits may be in the range from 0 to 63 bit!");
                    strtbt = value;
                }
            }

            public int id
            {
                get { return Id; }
                set { Id = value; }
            }

            public string Spec
            {
                get { return spec; }
                set { spec = value; }
            }

            public string State0
            {
                get { return stateFalseBox1Text; }
                set { stateFalseBox1Text = value; }
            }

            public string State1
            {
                get { return stateTrueBox1Text; }
                set { stateTrueBox1Text = value; }
            }

            public bool rpConditions
            {
                get { return conditions; }
            }

            public bool rpEvents
            {
                get { return events; }
            }
        } // PcbDataLogic

        BindingList<PcbVersion> pcbVersionLst = new BindingList<PcbVersion>();
        BindingList<PcbDataVersion> pcbData = new BindingList<PcbDataVersion>();
        BindingList<PcbDataLogic> pcbDataLogics = new BindingList<PcbDataLogic>();

        private const bool CONDITIONS = true;
        private const bool EVENTS = false;
        private string DisplayBarText = "";

        private static VehiclesModel modelVeh;
        private static DevVehicleControl vehiclesControl;
        private static DevVehicleControl vehiclesPanel;
        private static int currentMobitelId = -1;
        private int currentPcbVersion = -1;
        private int currentPcbId = -1;
        private int currentClickRowState = -1;
        private int currentClickRowSituation = -1;
        private int currentClickRowTarirovich = -1;
        private SettingSensorUser settingSensorsUser = null;
        private int currentPcbIdShow = -1;
        private static int currentLogicRowIndex = -1;
        private static int currentLogicRowIndexPrev = -1;
        private static bool flagStop = false;

        /// <summary>
        /// ������� � ������� ����������
        /// </summary>
        private LocalCache.atlantaDataSet _dataset;
        private LocalCache.atlantaDataSet atlantaDataSet;
        /// <summary>
        /// ������� ��� ���������� ��������� � �� �������� ���������� ��������.
        /// </summary>
        private LocalCache.atlantaDataSetTableAdapters.SensorsTableAdapter _sensorAdapter =
            new LocalCache.atlantaDataSetTableAdapters.SensorsTableAdapter();
        /// <summary>
        /// ������� ��� ���������� ��������� � �� �������� ���������, �������������
        /// �� ���������� �������.
        /// </summary>
        private LocalCache.atlantaDataSetTableAdapters.StateTableAdapter _stateAdapter =
            new LocalCache.atlantaDataSetTableAdapters.StateTableAdapter();
        /// <summary>
        /// ������� ��� ���������� ��������� � �� �������� �������, �����������
        /// ��� ��������� ��������� �������.
        /// </summary>
        private LocalCache.atlantaDataSetTableAdapters.TransitionTableAdapter _transitionAdapter =
            new LocalCache.atlantaDataSetTableAdapters.TransitionTableAdapter();
        /// <summary>
        /// ������� ��� ���������� ��������� � ������� relationalgorithms
        /// </summary>
        private LocalCache.atlantaDataSetTableAdapters.RelationalgorithmsTableAdapter _relAlgAdapter =
            new LocalCache.atlantaDataSetTableAdapters.RelationalgorithmsTableAdapter();
        /// <summary>
        /// ��������� ��� ���������� ������� "0".
        /// </summary>
        private atlantaDataSet.StateRow _falseState;

        /// <summary>
        /// ��������� ��� ���������� ������� "1".
        /// </summary>
        private atlantaDataSet.StateRow _trueState;
        /// <summary>
        /// ��������� ����������� �������, ������� ������ ����������� � ���������.
        /// � ������ �������������� ������������ ��������, ������ ���� ���� NULL.
        /// ���� �������, ����� � ������ ������ ���������� �������� � ��, ������� �� �� ��������.
        /// </summary>
        private atlantaDataSet.sensorsRow sensorWhenAdding;
        private LogicSensorStatesDvx lgcSensorStatesDvx = null;
        private DevAdminForm devSettingControl;
        private PcbSensors pcbPageControl = null;
        //private string[] columnField = null;

        /// <summary>
        /// ������ ����������� � ���� ������ MySQL.
        /// </summary>
        private object _connection;

        /// <summary>
        /// �����������
        /// </summary>
        public AdminForm()
        {
            _dataset = AppModel.Instance.DataSet;

            InitializeComponent();
            this.settingSensorsControl1 = new TrackControl.Setting.SettingSensorsControl();
            this.settingSensorsControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.settingSensorsControl1.Location = new System.Drawing.Point(0, 0);
            this.settingSensorsControl1.Margin = new System.Windows.Forms.Padding(0);
            this.settingSensorsControl1.Name = "SettingSensorControl";
            this.settingSensorsControl1.Size = new System.Drawing.Size(1183, 514);
            this.settingSensorsControl1.TabIndex = 0;
           
            this.pcbPageControl = new TrackControl.Setting.PcbSensors();
            this.pcbPageControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pcbPageControl.Location = new System.Drawing.Point(0, 0);
            this.pcbPageControl.Margin = new System.Windows.Forms.Padding(0);
            this.pcbPageControl.Name = "PcbSensors";
            this.pcbPageControl.Size = new System.Drawing.Size(1183, 514);
            this.pcbPageTab.Controls.Add(this.pcbPageControl);
            this.pcbPageTab.Name = "pcbPageTab";
            this.pcbPageTab.Size = new System.Drawing.Size(1183, 514);
            this.pcbPageTab.Text = Resources.PCBSensor;
            this.pcbPageTab.Visible = false;
            this.pcbPageControl.GetPcbData();
            this.pcbPageControl.Visible = true;

            NavigatorCustomButton button = gridControlSetup.EmbeddedNavigator.Buttons.CustomButtons.Add();
            button.ImageIndex = 0;
            button.Tag = "Adding";
            button.Hint = Resources.AddNewData;
            gridControlSetup.EmbeddedNavigator.Buttons.ImageList = imageList1;

            NavigatorCustomButton butt = gridControlLogic.EmbeddedNavigator.Buttons.CustomButtons.Add();
            butt.ImageIndex = 0;
            butt.Tag = "Adding";
            butt.Hint = Resources.AddNewData;
            gridControlLogic.EmbeddedNavigator.Buttons.ImageList = imageList1;
            gridControlLogic.EmbeddedNavigator.ButtonClick += EmbeddedNavigatorLogic_ButtonClick;
            
            gridPcbVers.RowCellStyle += gridPcbVers_RowCellStyle;
            gridPcbVers.RowClick += gridPcbVers_RowClick;
            _tabsPcbPage.Selected +=_tabsPcbPage_Selected;
            gridControlSetup.EmbeddedNavigator.ButtonClick += EmbeddedNavigator_ButtonClick;
            tpAdjustingSensors.Text = Resources.TextSens;
            tpAdjustingSensors.Tooltip = Resources.TextSensTip;

            RepositoryItemButtonEdit buttonEditTarirovich =
                gridSetups.Columns["Tarirovich"].ColumnEdit as RepositoryItemButtonEdit;
            if(buttonEditTarirovich != null)
                buttonEditTarirovich.ButtonClick += buttonEditTarirovich_ButtonClick;

            RepositoryItemButtonEdit buttonEditState =
               gridSetups.Columns["State"].ColumnEdit as RepositoryItemButtonEdit;
            if (buttonEditState != null)
            {
                buttonEditState.Buttons[0].ToolTip = Resources.EditState;
                buttonEditState.ButtonClick += buttonEditState_ButtonClick;
            }

            RepositoryItemButtonEdit buttonEditSituation =
               gridSetups.Columns["Situation"].ColumnEdit as RepositoryItemButtonEdit;
            if (buttonEditSituation != null)
            {
                buttonEditSituation.Buttons[0].ToolTip = Resources.EditSituation;
                buttonEditSituation.ButtonClick += buttonEditSituation_ButtonClick;
            }

            RepositoryItemComboBox comboAlgorithmEdit = 
               gridSetups.Columns["Algorithm"].ColumnEdit as RepositoryItemComboBox;

            RepositoryItemButtonEdit btnRpCondition =
                sensorsGrid.Columns["rpConditions"].ColumnEdit as RepositoryItemButtonEdit;
            if (btnRpCondition != null)
            {
                btnRpCondition.Buttons[0].ToolTip = Resources.rpCondition;
                btnRpCondition.ButtonClick += btnRpCondition_ButtonClick;
            }

            RepositoryItemButtonEdit btnRpEvent =
                sensorsGrid.Columns["rpEvents"].ColumnEdit as RepositoryItemButtonEdit;
            if (btnRpEvent != null)
            {
                btnRpEvent.Buttons[0].ToolTip = Resources.rpEvent;
                btnRpEvent.ButtonClick += btnRpEvent_ButtonClick;
            }

            gridSetups.CustomDrawCell += gridSetups_CustomDrawCell;
            
            settingSensorsUser = new SettingSensorUser();
            settingSensorsUser.InitialComboBoxAlgorithm(comboAlgorithmEdit);

            atlantaDataSet = AppModel.Instance.DataSet;
            mobitelsBindingSource.DataSource = atlantaDataSet;
            vehicleBindingSource.DataSource = atlantaDataSet.vehicle;
            sensorsGrid.RowClick += sensorsGrid_RowClick;
            sensorsGrid.FocusedRowChanged += sensorsGrid_FocusedRowChanged;
            
            gridControlLogic.EmbeddedNavigator.Buttons.Append.Visible = false;
            gridControlLogic.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            gridControlLogic.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            gridControlLogic.EmbeddedNavigator.Buttons.Edit.Visible = false;

            init();
            LoadUsersList();
            LoadTotalParams();

            string CONNECTION_STRING = Crypto.GetDecryptConnectionString( System.Configuration.ConfigurationManager.ConnectionStrings["CS"].ConnectionString );

            DriverDb db = new DriverDb();
            _connection = db.NewSqlConnection( CONNECTION_STRING );
            _sensorAdapter.Connection = _connection;
            _stateAdapter.Connection = _connection;
            _transitionAdapter.Connection = _connection;
            _relAlgAdapter.Connection = _connection;

            gridSetups.CustomDrawColumnHeader += CustomDrawColumnHeader;
            sensorsGrid.CustomDrawColumnHeader += sensorsGrid_CustomDrawColumnHeader;
            btnSettingPcb.Enabled = false;
            Load += DataAnalysis_Load;
            txMine.EditValue = 5;
            
            splitContainerControl6.Panel2.Height = 150;
            panelControl2.Height = 150;
            pbAnalize.Height = 150;
        }

        private bool flagStateHeaderSensors = false;

        void sensorsGrid_CustomDrawColumnHeader( object sender, ColumnHeaderCustomDrawEventArgs e )
        {
            if( e.Column == null )
                return;

            if( flagStateHeaderSensors )
            {
                e.Column.AppearanceHeader.BackColor = Color.PapayaWhip;
                e.Column.AppearanceHeader.BackColor2 = Color.Silver;
            }

            Rectangle rect = e.Bounds;
            ControlPaint.DrawBorder3D( e.Graphics, e.Bounds );
            Brush brush = e.Cache.GetGradientBrush( rect, e.Column.AppearanceHeader.BackColor,
                e.Column.AppearanceHeader.BackColor2, e.Column.AppearanceHeader.GradientMode );

            rect.Inflate( -1, -1 );
            // Fill column headers with the specified colors
            e.Graphics.FillRectangle( brush, rect );
            e.Appearance.DrawString( e.Cache, e.Info.Caption, e.Info.CaptionRect );

            // Draw the filter and sort buttons.
           foreach( DevExpress.Utils.Drawing.DrawElementInfo info in e.Info.InnerElements )
            {
                if( info.Visible )
                {
                    DevExpress.Utils.Drawing.ObjectPainter.DrawObject( e.Cache, info.ElementPainter, info.ElementInfo );
                }
                else
                {
                    flagStateHeaderSensors = true;

                    //if( e.Column.FieldName == "rpEvents" || e.Column.FieldName == "rpConditions" ||
                    //    e.Column.FieldName == "State0" || e.Column.FieldName == "State1" )
                    //{
                    //    e.Column.AppearanceHeader.BackColor = Color.Tan;
                    //    e.Column.AppearanceHeader.BackColor2 = Color.Tan;
                    //}
                    //else
                    //{
                    //    e.Column.AppearanceHeader.BackColor = Color.Gainsboro;
                    //    e.Column.AppearanceHeader.BackColor2 = Color.Gainsboro;
                    //}

                    rect = e.Bounds;
                    ControlPaint.DrawBorder3D( e.Graphics, e.Bounds );
                    brush = e.Cache.GetGradientBrush( rect, e.Column.AppearanceHeader.BackColor,
                        e.Column.AppearanceHeader.BackColor2, e.Column.AppearanceHeader.GradientMode );

                    rect.Inflate( -1, -1 );
                    // Fill column headers with the specified colors.
                    e.Graphics.FillRectangle( brush, rect );
                    e.Appearance.DrawString( e.Cache, e.Info.Caption, e.Info.CaptionRect );
                }
            }
            
            e.Handled = true;
        }

        private bool flagStateHeader = false;

        private void CustomDrawColumnHeader( object sender, ColumnHeaderCustomDrawEventArgs e )
        {
            if( e.Column == null ) 
                return;

            if( flagStateHeader )
            {
                e.Column.AppearanceHeader.BackColor = Color.PapayaWhip;
                e.Column.AppearanceHeader.BackColor2 = Color.Silver;
            }
           
            Rectangle rect = e.Bounds;
            ControlPaint.DrawBorder3D( e.Graphics, e.Bounds );
            Brush brush = e.Cache.GetGradientBrush( rect, e.Column.AppearanceHeader.BackColor,
                e.Column.AppearanceHeader.BackColor2, e.Column.AppearanceHeader.GradientMode );

            //rect.Inflate( -1, -1 );
            // Fill column headers with the specified colors
            e.Graphics.FillRectangle( brush, rect );
            e.Appearance.DrawString( e.Cache, e.Info.Caption, e.Info.CaptionRect );

            // Draw the filter and sort buttons.
            foreach( DevExpress.Utils.Drawing.DrawElementInfo info in e.Info.InnerElements )
            {
                if (info.Visible)
                {
                    DevExpress.Utils.Drawing.ObjectPainter.DrawObject(e.Cache, info.ElementPainter, info.ElementInfo);
                }
                else
                {
                    flagStateHeader = true;

                    //if (e.Column.FieldName == "Tarirovich" || e.Column.FieldName == "State" ||
                    //    e.Column.FieldName == "Situation" || e.Column.FieldName == "Algorithm")
                    //{
                    //    e.Column.AppearanceHeader.BackColor = Color.Tan;
                    //    e.Column.AppearanceHeader.BackColor2 = Color.Tan;
                    //}
                    //else
                    //{
                    //    e.Column.AppearanceHeader.BackColor = Color.Gainsboro;
                    //    e.Column.AppearanceHeader.BackColor2 = Color.Gainsboro;
                    //}

                    rect = e.Bounds;
                    ControlPaint.DrawBorder3D( e.Graphics, e.Bounds );
                    brush = e.Cache.GetGradientBrush( rect, e.Column.AppearanceHeader.BackColor,
                        e.Column.AppearanceHeader.BackColor2, e.Column.AppearanceHeader.GradientMode );

                    rect.Inflate( -1, -1 );
                    // Fill column headers with the specified colors.
                    e.Graphics.FillRectangle( brush, rect );
                    e.Appearance.DrawString( e.Cache, e.Info.Caption, e.Info.CaptionRect );
                }
            }
            
            e.Handled = true;
        } // CustomDrawColumnHeader
        
        private void sensorsGrid_FocusedRowChanged(object sender, FocusedRowChangedEventArgs e)
        {
            currentLogicRowIndex = e.FocusedRowHandle;
        }

        private void btnRpEvent_ButtonClick( object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e )
        {
            if( e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Up )
            {
                currentLogicRowIndexPrev = currentLogicRowIndex;
                lgcSensorStatesDvx = new LogicSensorStatesDvx();
                lgcSensorStatesDvx.EditableSensor( currentMobitelId, EVENTS );
                lgcSensorStatesDvx.ShowDialog();

                GetLogicSensor( currentMobitelId, currentPcbId );
                sensorsGrid.SelectRow( currentLogicRowIndexPrev );
                sensorsGrid.FocusedRowHandle = currentLogicRowIndexPrev;
            }
        }

        private void btnRpCondition_ButtonClick( object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e )
        {
            if( e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Down )
            {
                currentLogicRowIndexPrev = currentLogicRowIndex;
                lgcSensorStatesDvx = new LogicSensorStatesDvx();
                lgcSensorStatesDvx.EditableSensor( currentMobitelId, CONDITIONS );
                lgcSensorStatesDvx.ShowDialog();

                GetLogicSensor( currentMobitelId, currentPcbId );
                sensorsGrid.SelectRow(currentLogicRowIndexPrev);
                sensorsGrid.FocusedRowHandle = currentLogicRowIndexPrev;
            }
        }

        public static int getCurrentMobitelId
        {
            get { return currentMobitelId; }
        }

        /// <summary>
        /// ����� �������������� ���������
        /// </summary>
        /// <param name="sensorId">Id �������</param>
        /// <returns>AlgorithmId</returns>
        int? GetAlgorithmId( int sensorId )
        {
            foreach( atlantaDataSet.relationalgorithmsRow row in atlantaDataSet.relationalgorithms )
            {
                if( row.SensorID == sensorId )
                {
                    return row.AlgorithmID;
                }
            }

            return null;
        }

        /// <summary>
        /// ����� ������������ ��������� �� ��� ��������������
        /// </summary>
        /// <param name="algorithmID">������������� ���������</param>
        /// <returns>������������ ���������</returns>
        string GetAlgorithmName( int algorithmID )
        {
            foreach( atlantaDataSet.sensoralgorithmsRow row in atlantaDataSet.sensoralgorithms )
            {
                if( row.AlgorithmID == algorithmID )
                {
                    return row.Name;
                }
            }

            return String.Empty;
        }

        int GetAlgorithmID(string algorithmName)
        {
            foreach (atlantaDataSet.sensoralgorithmsRow row in atlantaDataSet.sensoralgorithms)
            {
                if (row.Name == algorithmName)
                {
                    return row.AlgorithmID;
                }
            }

            return 0;
        }

        private void sensorsGrid_RowClick( object sender, RowClickEventArgs e )
        {
            currentLogicRowIndex = e.RowHandle;
        }

        private void EmbeddedNavigatorLogic_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            if ("Adding".Equals(e.Button.Tag))
            {
                if (gridControlLogic.FocusedView != null)
                {
                    if( sensorsGrid.RowCount <= 0 )
                        AdminForm.gridViewLogicCurrentRow = 0;

                    atlantaDataSet.sensorsRow sensor = (atlantaDataSet.sensorsRow) atlantaDataSet.sensors.NewRow();
                    sensor.Name = "New";
                    sensor.Description = "Default";
                    sensor.StartBit = 60;
                    sensor.Length = 1;
                    sensor.NameUnit = "--";
                    sensor.K = 1.0;
                    sensor.Check = false;
                    sensor.mobitel_id = currentMobitelId;
                    sensor.MobitelName = "";
                    sensor.MobitelDescr = "";
                    atlantaDataSet.sensors.AddsensorsRow(sensor);
                    sensorWhenAdding = sensor;

                    _sensorAdapter.Update( atlantaDataSet.sensors );

                    if( sensorsGrid.RowCount > 0 )
                        AdminForm.gridViewLogicCurrentRow = mobitelsSensorsBindingSource.List.Count - 1;

                    GetLogicSensorNew( currentMobitelId, currentPcbId );
                    editSensor(sensor);
                    
                    sensorsGrid.FocusedRowHandle = sensorsGrid.RowCount - 1;
                    gridSetups.SelectRow( sensorsGrid.FocusedRowHandle );

                    e.Handled = true;
                }
            }
            else if (e.Button.ButtonType == NavigatorButtonType.Remove)
            {
                atlantaDataSet.sensorsRow sensor = getCurrentSensor(sensorsGrid.FocusedRowHandle);

                string message = String.Format("{0}: \"{1}\"?", Resources.LogicSensorDeletingConfirm, sensor.Name);

                DialogResult result = XtraMessageBox.Show(message, Resources.SensorDeleting, MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);

                if (sensor == null)
                {
                    e.Handled = true;
                    return;
                }

                if (DialogResult.No == result)
                {
                    e.Handled = true;
                    return;
                }

                if (DialogResult.Yes == result)
                {
                    settingSensorsUser.bindingNavigatorDeleteItem( sensor.id );
                    GetLogicSensor( currentMobitelId, currentPcbId );
                    sensorsGrid.RefreshData();
                    e.Handled = true;
                }
            } // else
            else if (e.Button.ButtonType == NavigatorButtonType.Edit)
            {
                atlantaDataSet.sensorsRow sensor = getCurrentSensor(sensorsGrid.FocusedRowHandle);

                if (sensor != null)
                {
                    editSensor(sensor);
                    GetLogicSensor( currentMobitelId, currentPcbId );
                }

                e.Handled = true;
            }
            else if (e.Button.ButtonType == NavigatorButtonType.EndEdit)
            {
                e.Handled = true;
            }
        }

        atlantaDataSet.sensorsRow getCurrentSensor(int numSensor)
        {
           return ( atlantaDataSet.sensorsRow )(
                ( DataRowView )mobitelsSensorsBindingSource.List[numSensor] ).Row;
        }

        /// <summary>
        /// ����������� ��������� ����������� �������
        /// </summary>
        void editSensor( atlantaDataSet.sensorsRow sensor )
        {
            lgcSensorStatesDvx = new LogicSensorStatesDvx();
            lgcSensorStatesDvx.EditSensor(sensor);
        }

        private void gridSetups_CustomDrawCell(object sender, RowCellCustomDrawEventArgs e)
        {
            int numRow = e.RowHandle;
            string clName = e.Column.Name;

            if (clName == "colTarirovka")
            {
                int idRow = pcbData[numRow].ID;
                string title = settingSensorsUser.GetTextNumberTarirovok( idRow );
                pcbData[numRow].Tarirovich = title;
            }
        }

        private void buttonEditSituation_ButtonClick( object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e )
        {
            ButtonEdit ed = gridSetups.ActiveEditor as ButtonEdit;

            if( ed == null ) 
                return;

            if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Up)
            {
                currentClickRowSituation = gridSetups.FocusedRowHandle;
                int idRow = pcbData[currentClickRowSituation].ID;
                settingSensorsUser.GetSituationData(idRow);
            }
        }

        private void buttonEditState_ButtonClick( object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e )
        {
            ButtonEdit ed = gridSetups.ActiveEditor as ButtonEdit;

            if( ed == null )
                return;

            if( e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Down )
            {
                currentClickRowState = gridSetups.FocusedRowHandle;
                int idRow = pcbData[currentClickRowState].ID;
                settingSensorsUser.GetStateData(idRow);
            }
        }

        private void SetNumberTarirovka()
        {
            for (int i = 0; i < gridSetups.RowCount; i++)
            {
                currentClickRowTarirovich = i;
                int idRow = pcbData[currentClickRowTarirovich].ID;
                string title = settingSensorsUser.GetTarirovkaNumber(idRow);
                pcbData[currentClickRowTarirovich].Tarirovich = title;
            }
        }

        private void buttonEditTarirovich_ButtonClick( object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e )
        {
            try
            {
                ButtonEdit ed = gridSetups.ActiveEditor as ButtonEdit;

                if (ed == null)
                    return;

                if (e.Button.Kind == DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis)
                {
                    currentClickRowTarirovich = gridSetups.FocusedRowHandle;
                    int idRow = pcbData[currentClickRowTarirovich].ID;
                    string title = settingSensorsUser.GetTarirovkaTable(idRow);
                    pcbData[currentClickRowTarirovich].Tarirovich = title;
                    gridSetups.RefreshData();
                }
            } // try
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Error Edit Tarirovka", MessageBoxButtons.OK);
            }
        }

        private void EmbeddedNavigator_ButtonClick( object sender, NavigatorButtonClickEventArgs e )
        {
            if (e.Button.ButtonType == NavigatorButtonType.CancelEdit)
            {
                PcbDataGetFromDb( currentMobitelId, currentPcbId );
                gridSetups.RefreshData();
                e.Handled = true;
            }
            else if ("Adding".Equals(e.Button.Tag))
            {
                if (gridControlSetup.FocusedView != null)
                {
                    if (currentMobitelId == -1)
                    {
                        e.Handled = true;
                        return;
                    }

                    settingSensorsUser.bindingNavigatorAddNewItem(currentMobitelId);
                    PcbDataGetFromDb(currentMobitelId, currentPcbId);
                    gridSetups.RefreshData();

                    e.Handled = true;
                }
            }
            else if (e.Button.ButtonType == NavigatorButtonType.EndEdit)
            {
                if (XtraMessageBox.Show(Resources.ChangedYes, Resources.Infos, MessageBoxButtons.YesNo) ==
                    DialogResult.No)
                {
                    PcbDataGetFromDb( currentMobitelId, currentPcbId );
                    gridSetups.RefreshData();

                    e.Handled = true;
                    return;
                }

                PcbDataSaveOldSettingIntoDb(currentMobitelId);
                settingSensorsUser.FillSensorBindingSource();
                PcbDataGetFromDb( currentMobitelId, currentPcbId );
                gridSetups.RefreshData();
                e.Handled = true;
            }
            else if (e.Button.ButtonType == NavigatorButtonType.Remove)
            {
                if (XtraMessageBox.Show(Resources.DeleteRowQuest, Resources.WarningPcb,
                    MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No)
                {
                    e.Handled = true;
                    return;
                }

                currentClickRowState = gridSetups.FocusedRowHandle;
                int idRow = pcbData[currentClickRowState].ID;
                settingSensorsUser.bindingNavigatorDeleteItem(idRow);
                PcbDataGetFromDb(currentMobitelId, currentPcbId);
                gridSetups.RefreshData();
                e.Handled = true;
            } // else
        }

        private void _tabsPcbPage_Selected( object sender, DevExpress.XtraTab.TabPageEventArgs e )
        {
            if (e.PageIndex == 3)
            {
                GetPcbVersions();

                if( currentMobitelId != -1 )
                    GettingDataPcbs( currentMobitelId );
            }
        }

        private void gridPcbVers_RowClick( object sender, RowClickEventArgs e )
        {
            currentPcbVersion = e.RowHandle;
            currentPcbId = Convert.ToInt32( gridPcbVers.GetRowCellValue( currentPcbVersion, "IDPcb" ).ToString() );
            btnSettingPcb.Enabled = true;
        }

        private void gridPcbVers_RowCellStyle( object sender, RowCellStyleEventArgs e )
        {
            if( e.Column.FieldName == "settVehic" )
            {
                var data = gridPcbVers.GetRowCellValue(e.RowHandle, "settVehic") as String;

                if( data == null )
                    return;

                if (data == Resources.Assigning)
                {
                    Font = new Font(e.Appearance.Font, FontStyle.Bold);
                    e.Appearance.Font = Font;
                }
            }
        }

        private void GettingDataPcbs(int mobitelid)
        {
            DriverDb db = new DriverDb();
            db.ConnectDb();

            try
            {
                if( pcbVersionLst.Count > 0 )
                {
                    PcbVersion pcbvr = pcbVersionLst[pcbVersionLst.Count - 1];

                    if (pcbvr.IDPcb == -2)
                    {
                        pcbVersionLst.RemoveAt(pcbVersionLst.Count - 1);
                    }
                }

                currentMobitelId = mobitelid;

                string sql = TrackControlQuery.AdminForm.SelectVehiclePcbVersionId + currentMobitelId;

                DataTable tbTable = db.GetDataTable( sql );

                //PcbDataGetFromDb(mobitelid, currentPcbId);

                if( tbTable.Rows.Count > 0 )
                {
                    string tmp = tbTable.Rows[0]["PcbVersionId"].ToString();

                    db.CloseDbConnection();

                    if( tmp == "" || tmp == "-1" )
                        currentPcbId = -2;
                    else
                        currentPcbId = Convert.ToInt32( tmp );

                    if( currentPcbId == -2 )
                    {
                        PcbVersion pcbver = new PcbVersion( currentPcbId, Resources.NotScheme, Resources.SettingWithoutPattern );
                        pcbver.settVehic = Resources.NotAssign;
                        pcbver.IDPcb = currentPcbId;
                        pcbVersionLst.Add( pcbver );
                    }

                    gridControlPcbVers.DataSource = pcbVersionLst;
                    GetPcbVersion( currentMobitelId, currentPcbId );
                }
                else
                {
                    db.CloseDbConnection();
                }
            }
            catch (Exception e)
            {
                db.CloseDbConnection();
                XtraMessageBox.Show(e.Message, Resources.ErrorPcb, MessageBoxButtons.OK);
            }
        }

        private void getIdVehicle(int mobitel_id)
        {
            currentMobitelId = -1;
            currentPcbId = -1;

            GettingDataPcbs(mobitel_id);
        }

        private void GetPcbVersion(int mobid, int pcbid)
        {
            try
            {
                if( mobid == -1 )
                    return;

                if (pcbid == -1)
                    return;

                pcbData.Clear();

                for (int j = 0; j < gridPcbVers.RowCount; j++)
                {
                    gridPcbVers.UnselectRow(j);
                    pcbVersionLst[j].settVehic = Resources.NotAssign;
                }

                gridPcbVers.RefreshData();

                for (int i = 0; i < gridPcbVers.RowCount; i++)
                {
                    if (pcbid == pcbVersionLst[i].IDPcb)
                    {
                        pcbVersionLst[i].settVehic = Resources.Assigning;
                        gridPcbVers.FocusedRowHandle = i;
                        gridPcbVers.SelectRow( i );

                        PcbDataGetFromDb(mobid, pcbid);
                        GetLogicSensor(mobid, pcbid);
                        break;
                    }
                }   
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, Resources.ErrorPcb, MessageBoxButtons.OK);
            }
        } // GetPcbVersion

        public static void SetVehicleMode(VehiclesModel model)
        {
            if (null == model)
                throw new ArgumentNullException("model vehicle");

            modelVeh = model;

            vehiclesControl = new DevVehicleControl(modelVeh);
            vehiclesControl.CheckBoxesShow = false;
            vehiclesControl.DeleteButtonShow = BarItemVisibility.Never;
            vehiclesControl.setShowVisible = BarItemVisibility.Never;

            vehiclesPanel = new DevVehicleControl(modelVeh);
            vehiclesPanel.CheckBoxesShow = true;
            vehiclesPanel.DeleteButtonShow = BarItemVisibility.Never;
            vehiclesPanel.Dock = DockStyle.Fill;
        }

        public void ShowDevAdminForm(DevAdminForm admForm)
        {
            try
            {
                if (admForm == null)
                    throw new ArgumentNullException("admForm");

                devSettingControl = admForm;
                devSettingControl.Dock = DockStyle.Fill;
                _settingsTab.Controls.Add(devSettingControl);

                // ��� ��� ��������� ��������� ��������
                if (vehiclesControl == null)
                    throw new ArgumentNullException("vehicleControl");
                
                SetButtonsNavigators();
                vehiclesControl._gettMobitel = new DevVehicleControl.GetMobitel(getIdVehicle);
                vehiclesControl.Dock = DockStyle.Fill;
                controlTransport.Controls.Add(vehiclesControl);

                if( vehiclesPanel == null )
                    throw new ArgumentNullException( "vehiclesPanel" );

                panelVehicles.Dock = DockStyle.Fill;
                panelVehicles.Controls.Add(vehiclesPanel);
            }
            catch (Exception e)
            {
                XtraMessageBox.Show(e.Message, "Error", MessageBoxButtons.OK);
                return;
            }
        }

        private void SetButtonsNavigators()
        {
            gridControlPcbVers.EmbeddedNavigator.Buttons.ButtonCollection[6].Visible = false;
            gridControlPcbVers.EmbeddedNavigator.Buttons.ButtonCollection[7].Visible = false;
            gridControlPcbVers.EmbeddedNavigator.Buttons.ButtonCollection[8].Visible = false;
            gridControlPcbVers.EmbeddedNavigator.Buttons.ButtonCollection[9].Visible = false;
            gridControlPcbVers.EmbeddedNavigator.Buttons.ButtonCollection[10].Visible = false;

            gridControlSetup.EmbeddedNavigator.Buttons.ButtonCollection[6].Visible = false;
            gridControlSetup.EmbeddedNavigator.Buttons.ButtonCollection[7].Visible = true;
            gridControlSetup.EmbeddedNavigator.Buttons.ButtonCollection[8].Visible = true;
            gridControlSetup.EmbeddedNavigator.Buttons.ButtonCollection[10].Visible = true;
            gridControlSetup.EmbeddedNavigator.Buttons.CustomButtons[0].Visible = true;
        } // SetButtonsNavigators

        private void GetPcbVersions()
        {
            DriverDb db = null;
            gridControlPcbVers.DataSource = null;
            pcbVersionLst.Clear();

            try
            {
                db = new DriverDb();
                db.ConnectDb();
                DataTable tblpcb = db.GetDataTable(TrackControlQuery.AdminForm.SelectPcbVersionId);
                db.CloseDbConnection();

                for (int i = 0; i < tblpcb.Rows.Count; i++)
                {
                    PcbVersion pcbVer = new PcbVersion(Convert.ToInt32(tblpcb.Rows[i]["Id"].ToString()), 
                        tblpcb.Rows[i]["Name"].ToString(), tblpcb.Rows[i]["Comment"].ToString());
                    pcbVersionLst.Add(pcbVer);
                }

                for( int j = 0; j < pcbVersionLst.Count; j++ )
                {
                    pcbVersionLst[j].settVehic = Resources.NotAssign;
                }

                gridControlPcbVers.DataSource = pcbVersionLst;

            }
            catch (Exception e)
            {
                if (db != null)
                    db.CloseDbConnection();

                XtraMessageBox.Show(e.Message, Resources.ErrorPcb, MessageBoxButtons.OK);
            }
        } // GetPcbVersions

        private void PcbDataSaveOldSettingIntoDb(int mobitel)
        {
            DriverDb db = null;
            try
            {
                db = new DriverDb();
                db.ConnectDb();
                string sql = "";

                for (int i = 0; i < gridSetups.RowCount; i++)
                {
                    string typedatapcb = gridSetups.GetRowCellValue( i, "TypeDataPcb" ).ToString();
                    string koeff = gridSetups.GetRowCellValue( i, "KoefficK" ).ToString();
                    string koeffB = gridSetups.GetRowCellValue(i, "Shifter").ToString();

                    bool signed = (bool)gridSetups.GetRowCellValue(i, "Signing");

                    int strtbit = (int) gridSetups.GetRowCellValue( i, "StrtBit" );
                    int lgth = (int) gridSetups.GetRowCellValue(i, "LngthBit");
                    string algorithm = gridSetups.GetRowCellValue( i, "Algorithm" ).ToString();
                    int Id = (int)gridSetups.GetRowCellValue(i, "ID");
                    string uni = ( string )gridSetups.GetRowCellValue( i, "NameUnit" );

                    settingSensorsUser.sensorsAlgorithmSave( algorithm,  Id);
                                        
                    int index = koeff.IndexOf( "," );
                    string temp = "";

                    if( index >= 0 )
                    {
                        temp = koeff.Substring( 0, index );
                        temp = temp + ".";
                        temp = temp + koeff.Substring( index + 1, koeff.Length - index - 1 );
                        koeff = temp;
                    }

                    index = koeffB.IndexOf(",");
                    temp = "";

                    if (index >= 0)
                    {
                        temp = koeffB.Substring(0, index);
                        temp = temp + ".";
                        temp = temp + koeffB.Substring(index + 1, koeffB.Length - index - 1);
                        koeffB = temp;
                    }

                    byte bsigned = (byte)(signed ? 1 : 0);
                    sql = string.Format( TrackControlQuery.AdminForm.UpdateSensors,
                            typedatapcb, strtbit, lgth, koeff, koeffB, bsigned, mobitel, Id, uni );

                    db.ExecuteNonQueryCommand(sql);
                } // for

                db.CloseDbConnection();
            }
            catch (Exception e)
            {
                if (db != null)
                    db.CloseDbConnection();

                XtraMessageBox.Show(e.Message, Resources.ErrorPcb, MessageBoxButtons.OK);
            }
        }

        private void GetLogicSensor(int mobitelid, int idpcb)
        {
            try
            {
                if( mobitelid == -1 )
                    return;

                if( idpcb == -1 )
                    return;

                pcbDataLogics.Clear();

                mobitelsBindingSource.Filter = string.Format("Mobitel_id = {0}", mobitelid);
                int itemFound = mobitelsBindingSource.Find( "Mobitel_ID", mobitelid );
                mobitelsBindingSource.Position = itemFound;

                if ( itemFound == -1 )
                    return;

                Mobitel mobitel = VehicleProvider2.GetMobitel(mobitelid);
                bool is64BitPackets = mobitel.Is64BitPackets;

                for( int i = 0; i < mobitelsSensorsBindingSource.List.Count; i++ )
                {
                    atlantaDataSet.sensorsRow sensor = getCurrentSensor( i );

                    string name = sensor.Name;
                    int id = sensor.id;
                    int strtbt = sensor.StartBit;
                    string stateFalseBox1Text = "";
                    string stateTrueBox1Text = "";
                    _falseState = null;
                    _trueState = null;
                    
                    foreach (atlantaDataSet.StateRow state in sensor.GetStateRows())
                    {
                        if (state.MinValue < 0.1)
                            _falseState = state;

                        else if (state.MinValue >= 1)
                            _trueState = state;
                    }

                    stateFalseBox1Text = ( _falseState != null ) ? _falseState.Title : "";
                    stateTrueBox1Text = ( _trueState != null ) ? _trueState.Title : "";

                    int? algorithmID = GetAlgorithmId( id );
                    string Value = algorithmID.HasValue ? GetAlgorithmName( algorithmID.Value ) : String.Empty;

                    PcbDataLogic dataLogic = new PcbDataLogic(id, name, strtbt, Value, stateFalseBox1Text, stateTrueBox1Text, is64BitPackets);
                    pcbDataLogics.Add( dataLogic );
                } // for

                gridControlLogic.DataSource = pcbDataLogics;
            }
            catch (Exception e)
            {
                XtraMessageBox.Show(e.Message, Resources.ErrorPcb, MessageBoxButtons.OK);
            }
        }

        private void GetLogicSensorNew( int mobitelid, int idpcb )
        {
            try
            {
                if( mobitelid == -1 )
                    return;

                if( idpcb == -1 )
                    return;

                pcbDataLogics.Clear();

                mobitelsBindingSource.Filter = string.Format( "Mobitel_id = {0}", mobitelid );
                int itemFound = mobitelsBindingSource.Find( "Mobitel_ID", mobitelid );
                mobitelsBindingSource.Position = itemFound;

                if( itemFound == -1 )
                    return;

                Mobitel mobitel = VehicleProvider2.GetMobitel(mobitelid);
                bool is64BitPackets = mobitel.Is64BitPackets;

                for( int i = 0; i < mobitelsSensorsBindingSource.List.Count; i++ )
                {
                    atlantaDataSet.sensorsRow sensor = getCurrentSensor( i );

                    string name = sensor.Name;
                    int id = sensor.id;
                    int strtbt = sensor.StartBit;
                    bool isState = false;

                    foreach( atlantaDataSet.StateRow state in sensor.GetStateRows() )
                    {
                        isState = true;
                        if( state.MinValue < 0.1 )
                            _falseState = state;
                        else if( state.MinValue >= 1 )
                            _trueState = state;
                    }

                    // ������������ ��������������� ������ ��� ������� ����� �� ������ ���������� ��������
                    if( !isState )
                    {
                        atlantaDataSet.StateRow stRow = atlantaDataSet.State.NewStateRow(); // ��� ��������� 0
                        stRow.MinValue = 0.0;
                        stRow.MaxValue = 0.1;
                        stRow.Title = "0";
                        stRow.SensorId = id;
                        _falseState = stRow;
                        atlantaDataSet.State.AddStateRow( stRow );

                        stRow = atlantaDataSet.State.NewStateRow(); // ��� ��������� 1
                        stRow.MinValue = 1.0;
                        stRow.MaxValue = 1.1;
                        stRow.Title = "1";
                        stRow.SensorId = id;
                        _trueState = stRow;
                        atlantaDataSet.State.AddStateRow( stRow );
                    }

                    string stateFalseBox1Text = ( _falseState != null ) ? _falseState.Title : "";
                    string stateTrueBox1Text = ( _trueState != null ) ? _trueState.Title : "";

                    int? algorithmID = GetAlgorithmId( id );
                    string Value = algorithmID.HasValue ? GetAlgorithmName( algorithmID.Value ) : String.Empty;

                    PcbDataLogic dataLogic = new PcbDataLogic(id, name, strtbt, Value, stateFalseBox1Text, stateTrueBox1Text, is64BitPackets);
                    pcbDataLogics.Add( dataLogic );
                } // for

                gridControlLogic.DataSource = pcbDataLogics;
                _stateAdapter.Update( atlantaDataSet.State ); // ��������� ������ � ����
            }
            catch( Exception e )
            {
                XtraMessageBox.Show( e.Message, Resources.ErrorPcb, MessageBoxButtons.OK );
            }
        }

        private void PcbDataGetFromDb(int mobitelid, int idpcb)
        {
            pcbData.Clear();

            try
            {
                if (mobitelid == -1)
                    return;

                if ( idpcb == -1 )
                    return;

                DataRowView rvView = null;
                BindingSource bdsSensors = settingSensorsUser.GetBindingSensors(mobitelid);

                //bdsSensors.RemoveSort(); -- �� ������ ������, ��� ���������� ���������� � �������

                int itemFound = bdsSensors.Find( "Mobitel_ID", mobitelid );
                bdsSensors.Position = itemFound;

                if ( itemFound == -1 )
                    return;

                Mobitel mobitel = VehicleProvider2.GetMobitel(mobitelid);
                bool is64BitPackets = mobitel.Is64BitPackets;

                for( int i = 0; i < bdsSensors.Count; i++ )
                {
                    rvView = ( DataRowView ) bdsSensors[i];
                    int id = (int) rvView.Row["id"];
                    
                    
                    string typedata = ( string )rvView.Row["Name"];
                    double koeffk = Math.Round((double) rvView.Row["K"], 6);
                    double koeffb = Math.Round((double)rvView.Row["B"], 6);
                    bool signed = (bool)rvView.Row["S"];
                    int strtbt = ( int )rvView.Row["StartBit"];
                    int lgthbt = ( int )rvView.Row["Length"];
                    string nameUnit = (string)rvView.Row["NameUnit"];

                    string nameAlg = settingSensorsUser.GetAlgorithmData( id );

                    PcbDataVersion dataVersion = new PcbDataVersion(id, typedata, koeffk, koeffb, signed, strtbt, lgthbt, "��� �����������", idpcb, nameUnit, is64BitPackets);
                    dataVersion.Algorithm = nameAlg;
                    pcbData.Add( dataVersion );
                } // for

                gridControlSetup.DataSource = pcbData;

                SetNumberTarirovka();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, Resources.ErrorPcb, MessageBoxButtons.OK);
            }
        } // PcbDataGetFromDb

        private void LoadUsersList()
        {
            UsersList ulist = new UsersList();
            usersTab.Controls.Add(ulist);
            ulist.Dock = DockStyle.Fill;
        }

        private void LoadTotalParams()
        {
            TotalParamsProvider provParams = new TotalParamsProvider();
            rleLang.DataSource = provParams.GetLangList;
            osmLang.DataSource = provParams.GetLangListOSM;
            pgParams.SelectedObject = provParams;
        }

        /// <summary>
        /// ������� � ������� ����������. ������ �������� ������������ ����������
        /// ������ �����.
        /// </summary>
        public LocalCache.atlantaDataSet Dataset
        {
            get { return _dataset; }
        }

        public static int gridViewLogicCurrentRow
        {
            get { return currentLogicRowIndex; }
            set { currentLogicRowIndex = value; }
        }

        private void init()
        {
            _adminTab.Text = Resources.Admin;
            _settingsTab.Text = Resources.Params;
           // _logicSensorsTab.Text = Resources.LogicSensors;
           //_gageSensorsTab.Text = Resources.OtherSensors;
           // _reportsTab.Text = Resources.Reports;
           //_analysisTab.Text = Resources.DataAnalysis;

            analysisTab.Text = Resources.DataAnalysisNew;
            analysisTab.Tooltip = Resources.DataAnalysisTooltip;
            barBeginPeriod.Caption = Resources.BegingPeriod;
            barBeginPeriod.Hint = Resources.BegingPeriodHint;
            barEndPeriod.Caption = Resources.EndPeriod;
            barEndPeriod.Hint = Resources.EndPeriodHint;
            barStaticItem1.Caption = Resources.SizeTimePeriod;
            barStaticItem1.Hint = Resources.SizeTimePeriodHint;
            DisplayBarText = Resources.ProgressBarCaption;
            pbAnalize.ToolTip = Resources.ProgressBarHint;
            Car.Caption = Resources.carCaption;
            Car.ToolTip = Resources.carToolTip;
            DateB.Caption = Resources.datebCaption;
            DateB.ToolTip = Resources.datebToolTip;
            DateE.Caption = Resources.dateeCaption;
            DateE.ToolTip = Resources.dateeToolTip;
            Interval.Caption = Resources.intervalCaption;
            Interval.ToolTip = Resources.intervalToolTip;
            QTYpoints.Caption = Resources.qtyCaption;
            QTYpoints.ToolTip = Resources.qtyToolTip;
            btnDataAnalysis.Caption = Resources.dataAnalysisCaption;
            btnDataAnalysis.Hint = Resources.dataAnalysisHint;
            btnNoGps.Caption = Resources.NoGPSCaption;
            btnNoGps.Hint = Resources.NoGPSHint;
            btnDeleteNoCorrect.Caption = Resources.deleteNoCorrectCaption;
            btnDeleteNoCorrect.Hint = Resources.deleteNoCorrectHint;
            btnMissingData.Caption = Resources.missingDataCaption;
            btnMissingData.Hint = Resources.missingDataHint;
            btnAdding.Caption = Resources.btnAddingCaption;
            btnAdding.Hint = Resources.btnAddingHint;
            groupControl2.Text = Resources.groupControl2;
            barBeginDate.Caption = Resources.BeginDateCaption;
            barBeginDate.Hint = Resources.BeginDateHint;
            barEndDate.Caption = Resources.EndDateCaption;
            barEndDate.Hint = Resources.EndDateHint;
            txMine.Caption = Resources.mineCaption;
            txMine.Hint = Resources.mineHint;
            btnDuplicatedData.Caption = Resources.DuplicatedData;
            btnDuplicatedData.Hint = Resources.DuplicatedData;

            usersTab.Text = Resources.Users;
            crTotalSettings.Properties.Caption = Resources.GeneralSettings;
            erIsDataAnalizOn.Properties.Caption = Resources.GeneralSettingsIsDataAnalizOn;
            //erIsReceiveAdressOn.Properties.Caption = Resources.GeneralSettingsIsReceiveAdressOn;
            erIsGraphFuelShow.Properties.Caption = Resources.GraphFuelShow;
            crLuxena.Properties.Caption = Resources.LuxenaLayer;
            erIsLuxenaUse.Properties.Caption = Resources.LuxenaUse;
            erLuxenaLang.Properties.Caption = Resources.LuxenaLang;
            erIsOpenStreetMapUse.Properties.Caption = Resources.AddressLayUse;
            erIsOpenStreetMapUse.Properties.ToolTip = Resources.AddressLayUseTip;
            erOpenStreetMapServer.Properties.Caption = Resources.OSMServer;
            erOpenStreetMapServer.Properties.ToolTip = Resources.OSMServerTip;
            erOpenStreetMapPort.Properties.Caption = Resources.OSMPort;
            erOpenStreetMapPort.Properties.ToolTip = Resources.OSMPortTip;
            erOpenStreetMapKey.Properties.Caption = Resources.UrlKey;
            erOpenStreetMapKey.Properties.ToolTip = Resources.UrlKeyTip;
            erMapLang.Properties.Caption = Resources.OSMLang;
            erMapLang.Properties.ToolTip = Resources.OSMLangTip;
            crOpenStreet.Properties.Caption = Resources.OSMName;
            crOpenStreet.Properties.ToolTip = Resources.OSMName;

            crRcsNominatim.Properties.Caption = Resources.AddressLayRcsNominatim;
            crRcsNominatim.Properties.ToolTip = Resources.AddressLayRcsNominatimTip ;

            erIsRcsNominatimUse.Properties.Caption = Resources.AddressLayUse;
            erIsRcsNominatimUse.Properties.ToolTip = Resources.AddressLayUseTip;

            crGoogleMap.Properties.Caption = Resources.AddressLayGoogleMap;
            crGoogleMap.Properties.ToolTip = Resources.AddressLayGoogleMapTip;

            erIsGoogleMapUse.Properties.Caption = Resources.AddressLayUse;
            erIsGoogleMapUse.Properties.ToolTip = Resources.AddressLayUseTip;

            erGoogleMapKey.Properties.Caption = Resources.UrlKey;
            erGoogleMapKey.Properties.ToolTip = Resources.UrlKeyTip;

            splitContainerControl3.Panel1.Text = Resources.PCB_Sensor;
            colNamePcb.Caption = Resources.colCaptionPcb;
            colNamePcb.ToolTip = Resources.colTipPCB;
            colComment.Caption = Resources.CommentCaption;
            colComment.ToolTip = Resources.CommentToolTip;
            colSetting.Caption = Resources.SettingsCaption;
            colSetting.ToolTip = Resources.SettingToolTip;
            colID.Caption = Resources.colId;
            colID.ToolTip = Resources.colIdToolTip;
            btnSettingPcb.Text = Resources.ButtonSetting;
            btnSettingPcb.ToolTip = Resources.ButtonSettingToolTip;

            splitContainerControl3.Panel2.Text = Resources.StandartSettings;
            colTypeData.Caption = Resources.TypeData;
            colTypeData.ToolTip = Resources.TypeDataTip;
            colStartBit.Caption = Resources.StartBitRec;
            colStartBit.ToolTip = Resources.StartBitRecTip;
            colLengthRecord.Caption = Resources.LngthRec;
            colLengthRecord.ToolTip = Resources.LngthRecTip;
            colKoeffK.Caption = Resources.KoeffK;
            colKoeffK.ToolTip = Resources.KoeffKTip;
            colShifter.Caption = Resources.KoeffB;
            colShifter.ToolTip = Resources.KoeffBTip;
            colSigning.Caption = Resources.Signed;
            colSigning.ToolTip = Resources.SignedTip;
            colComms.Caption = Resources.Comms;
            colComms.ToolTip = Resources.CommsTip;
            colTarirovka.Caption = Resources.Tarirovka;
            colTarirovka.ToolTip = Resources.TarirovkaTip;
            colState.Caption = Resources.StateCaption;
            colState.ToolTip = Resources.StateCaptionTip;
            colSituation.Caption = Resources.Situation;
            colSituation.ToolTip = Resources.SituationTip;
            colAlgorithm.Caption = Resources.Algorithm;
            colAlgorithm.ToolTip = Resources.AlgorithmTip;
            colIdData.Caption = Resources.IdData;
            colIdData.ToolTip = Resources.IdDataTip;
            colIdPcbs.Caption = Resources.IdPcbs;
            colIdPcbs.ToolTip = Resources.IdPcbsTip;

            groupControl1.Text = Resources.LogicSens;
            nameDataGridViewTextBoxColumn1.Caption = Resources.NameLogic;
            nameDataGridViewTextBoxColumn1.ToolTip = Resources.NameLogicTip;
            startBitDataGridViewTextBoxColumn.Caption = Resources.NumberBit;
            startBitDataGridViewTextBoxColumn.ToolTip = Resources.NumberBitTip;
            SpecSettingColumn.Caption = Resources.SpecSetting;
            SpecSettingColumn.ToolTip = Resources.SpecSettingTip;
            id.Caption = Resources.IdCaption;
            id.ToolTip = Resources.IdCaptionTip;
            colState0.Caption = Resources.State0;
            colState0.ToolTip = Resources.State0Tip;
            colState1.Caption = Resources.State1;
            colState1.ToolTip = Resources.State1Tip;
            colCondition.Caption = Resources.Condition;
            colCondition.ToolTip = Resources.ConditionTip;
            colEvents.Caption = Resources.Events;
            colEvents.ToolTip = Resources.EventsTip;
            columnNameUnit.Caption = Resources.nameUnit;
            columnNameUnit.ToolTip = Resources.nameUnit;

            tpAdjustingSensors.Text = Resources.TextSens;
            tpAdjustingSensors.Tooltip = Resources.TextSensTip;
            pbAnalize.CustomDisplayText += pbAnalize_CustomDisplayText;
        }

        void pbAnalize_CustomDisplayText( object sender, DevExpress.XtraEditors.Controls.CustomDisplayTextEventArgs e )
        {
            string val = e.Value.ToString();
            e.DisplayText = DisplayBarText + ": " + val;
        }

        private void pgParams_CellValueChanged(object sender,
            DevExpress.XtraVerticalGrid.Events.CellValueChangedEventArgs e)
        {
            if (e.Row == erIsOpenStreetMapUse) // ��� �������� ������ ����� �� �������� (OpenStreetMap)
            {
                GMapOverlay.IsOpenStreetMapUse = (bool) e.Value;
                GMapOverlay.IsRcsNominatimUse = false;
                BuildGraphs.IsGraphFuelShow = false;
                GMapProviders.getOpenStreetMap.SetStartParams();
            }

            if (e.Row == erIsRcsNominatimUse) // ��� �������� ������ ����� �� �������� (Nominatim in RCS)
            {
                GMapOverlay.IsRcsNominatimUse = (bool) e.Value;
                GMapOverlay.IsOpenStreetMapUse = false;
                BuildGraphs.IsGraphFuelShow = false;
                GMapProviders.getRcsNominatim.SetStartParams();
            }

            if (e.Row == erIsGraphFuelShow)
            {
                BuildGraphs.IsGraphFuelShow = (bool) e.Value;
                GMapOverlay.IsRcsNominatimUse = false;
                GMapOverlay.IsOpenStreetMapUse = false;
            }
        }

        // ��������� ����� �������� �� ������ � ������ ���������� � ������� ������ ���������
        private void btnSettingPcb_Click( object sender, EventArgs e )
        {
            // mobitel_id ������
            if(currentMobitelId == -1)
                return;

            if(currentPcbVersion == -1)
                return;

            if (XtraMessageBox.Show(Resources.DeleteQuestion, Resources.Infos, MessageBoxButtons.YesNo) ==
                DialogResult.No)
            {
                return;
            }

            currentPcbVersion = gridPcbVers.FocusedRowHandle;
            currentPcbId = Convert.ToInt32(gridPcbVers.GetRowCellValue( currentPcbVersion, "IDPcb" ).ToString());

            if (currentPcbId == -2)
            {
                XtraMessageBox.Show(Resources.IsNotAssing, Resources.Infos, MessageBoxButtons.OK, MessageBoxIcon.Question);
                return;
            }

            string sqlget = string.Format("SELECT * FROM vehicle WHERE PcbVersionId = {0} and Mobitel_id = {1}", currentPcbId,
                    currentMobitelId);

            string sql = string.Format( TrackControlQuery.AdminForm.UpdateVehicle, currentPcbId,
                    currentMobitelId);

            DriverDb dbs = new DriverDb();
            dbs.ConnectDb();
            DataTable tbls = dbs.GetDataTable(sqlget);

            int idpcb = 0;
            if (tbls.Rows.Count == 1)
            {
                idpcb = Convert.ToInt32(tbls.Rows[0]["PcbVErsionId"].ToString());
                if (idpcb == currentPcbId)
                    if (DialogResult.Yes !=
                        XtraMessageBox.Show(Resources.Excisting, "Warning", MessageBoxButtons.YesNo,
                            MessageBoxIcon.Warning))
                    {
                        return;
                    }
            }
            else if (tbls.Rows.Count > 1)
            {
                XtraMessageBox.Show(Resources.Begiding, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            DriverDb db = new DriverDb();
            db.ConnectDb();
            db.ExecuteNonQueryCommand(sql);
            
            // ������� �������� ������� � ������� ��������
            sql = string.Format( TrackControlQuery.AdminForm.SelectFromPcbData, currentPcbId );
            DataTable tblsTable = db.GetDataTable(sql);

            settingSensorsUser.FillSensorBindingSource();
            DataTable bdsSensors = settingSensorsUser.GetSensorsID(currentMobitelId);
            int iCount = 0;

            // ������� ��� ���������� ��������� ��������� ������� � ���������� �� �� � ��������
            while( iCount < bdsSensors.Rows.Count )
            {
                int idRow = Convert.ToInt32(bdsSensors.Rows[iCount++]["id"].ToString());
                settingSensorsUser.bindingNavigatorDeleteItem( idRow );
            }

            // ������� ��� ���������� ��������� �� ������ ��������
            mobitelsBindingSource.Filter = string.Format("Mobitel_id = {0}", currentMobitelId);
            int itemFound = mobitelsBindingSource.Find( "Mobitel_ID", currentMobitelId );
            mobitelsBindingSource.Position = itemFound;
            
            for (; mobitelsSensorsBindingSource.List.Count > 0;)
            {
                mobitelsSensorsBindingSource.Remove( mobitelsSensorsBindingSource.List[0]);
            }

            // ��������� � ���������� � ������� � ����� � �� ����� ��������� ��������
            for ( int i = 0; i < tblsTable.Rows.Count; i++ )
            {
                bool signed = false;
                string result = tblsTable.Rows[i]["S"].ToString();
                if (result == "0" || result == "False")
                {
                    signed = Convert.ToBoolean(false);
                }
                else if (result != "0" || result == "True")
                {
                    signed = Convert.ToBoolean(true);
                }

                settingSensorsUser.AddNewSetting( currentMobitelId, ( string )tblsTable.Rows[i]["Typedata"], ( double )tblsTable.Rows[i]["koeffK"], ( double )tblsTable.Rows[i]["Shifter"], signed,
                    (int)tblsTable.Rows[i]["Startbit"], (int)tblsTable.Rows[i]["Lengthbit"], (string)tblsTable.Rows[i]["NameVal"], (int)tblsTable.Rows[i]["NumAlg"]);
            } // for

            GetLogicSensorNew( currentMobitelId, currentPcbId ); // ���������� ����� ��������� � ������� ���������� ���������
            PcbDataGetFromDb( currentMobitelId, currentPcbId ); // ���������� � ������� ������� ���������
            
            db.CloseDbConnection();
        }

        //=================================== ������ ������ ====================================================================
        #region enumerations

        internal enum AnalizAlgoritms : int
        {
            GPS_ABSENT = 1,
            POWER_ABSENT = 2,
            DATA_OMIT = 3,
            DATA_DOUBLES = 4,
            DATA_STATISTIC = 5
        }

        #endregion

        #region structures

        internal struct MobitelInfo
        {
            public string Name;
            public DateTime dtBegin;
            public DateTime dtEnd;
            public int CNT;
        }

        internal struct DataRowAnaliz
        {
            public DateTime DataGPS;
            public long LogID;
            public long SrvPacketID;
            public int Mobitel_ID;
            public int Valid;
        }

        #endregion

        private LocalCache.atlantaDataSet dataset;
        private MobitelInfo _MobitelInfo = new MobitelInfo();
        private Dictionary<int, MobitelInfo> MobitelAcum = new Dictionary<int, MobitelInfo>();
        //private Dictionary<int, QueryLostData> MobitelLostData = new Dictionary<int, QueryLostData>();
        private List<QueryLostData> MobitelLostData = new List<QueryLostData>();
        private Dictionary<int, string> dictMobitels;
        private BindingList<AnalisData> listAnalisData = new BindingList<AnalisData>();
        private List<IVehicle> Vehicles = new List<IVehicle>();
        private List<IVehicle> VehiclesList = new List<IVehicle>();
        private string textFild = "";
        
        private void btnDataAnalysis_ItemClick( object sender, DevExpress.XtraBars.ItemClickEventArgs e )
        {
            flagStop = false;
            textFild = btnDataAnalysis.Caption;
            MobitelAcum.Clear();
            AnalizByDays( ( int )AnalizAlgoritms.DATA_STATISTIC );
        }
        
        private bool GridChecked()
        {
            bool bCheckPresent = false;

            Vehicles = vehiclesPanel.GetCheckedNode;
            
            if(Vehicles.Count > 0)
                bCheckPresent = true;
            
            return bCheckPresent;
        } // GridChecked

        private void AnalizAllPeriod( int iTypeAnaliz )
        {
            if  (!IsInputParamsValid()) return;


            ctrlAnalisis.DataSource = listAnalisData;
            listAnalisData.Clear();
            gridAnalisis.RefreshData();
            DisplayBarText = Resources.LoadingFromDB;
            MobitelLostData.Clear();

            pbAnalize.EditValue = 0;
            pbAnalize.Properties.Minimum = 0;
            pbAnalize.Properties.Maximum = Vehicles.Count;

            for( int j = 0; j < Vehicles.Count; j++ )
            {
                DateTime dtEnd = (DateTime)barEndDate.EditValue;
                DateTime dtBegin = (DateTime)barBeginDate.EditValue;
                if (flagStop)
                {
                    pbAnalize.EditValue = 0;
                    DisplayBarText = textFild + " - " + Resources.StopWithUser;
                    pbAnalize.Refresh();
                    Application.DoEvents();
                    System.GC.Collect();

                    return;
                }


                IVehicle vehicle = Vehicles[j];

                DisplayBarText = vehicle.CarMaker + "|" + vehicle.CarModel + "|" + vehicle.RegNumber;
                    if (flagStop)
                    {
                        pbAnalize.EditValue = 0;
                        DisplayBarText = textFild + " - " + Resources.StopWithUser;
                        pbAnalize.Refresh();
                        Application.DoEvents();
                        System.GC.Collect();

                        return;
                    }

                    DataTable dtWork;

                    string sSQL = vehicle.Mobitel.Is64BitPackets ? TrackControlQuery.DataAnalysis.SelectDatagpsData64 : TrackControlQuery.DataAnalysis.SelectDatagpsData;

                    switch( iTypeAnaliz )
                    {
                        case ( int )AnalizAlgoritms.DATA_DOUBLES:
                            sSQL = sSQL + " ORDER BY SrvPacketID, LogID";
                            break;
                        default:
                            sSQL = sSQL + " ORDER BY Mobitel_ID, LogID";
                            break;
                    }

                    using( DriverDb db = new DriverDb() )
                    {
                        db.ConnectDb();
                        db.NewSqlParameterArray( 3 );
                        db.SetNewSqlParameter(db.ParamPrefics + "Begin", dtBegin);
                        db.SetNewSqlParameter( db.ParamPrefics + "End", dtEnd );
                        db.SetNewSqlParameter( db.ParamPrefics + "Mobitel_id", vehicle.Mobitel.Id );
                        dtWork = db.GetDataTable( sSQL, db.GetSqlParameterArray );
                    }
                    AnalizData(iTypeAnaliz, dtEnd, ref dtBegin, ref sSQL, dtWork);

                    pbAnalize.EditValue = j ; // ��-�� ���� ��� �� ���������� �� �����
            } // for

            pbAnalize.EditValue = 0;
            DisplayBarText = textFild + " - " + Resources.Done;
            pbAnalize.Refresh();
            Application.DoEvents();
            System.GC.Collect();
        }

        private bool IsInputParamsValid()
        {
            if (!GridChecked())
            {
                XtraMessageBox.Show(Resources.SelectVehicleForAnalysis, Resources.DataAnalysis, MessageBoxButtons.OK);
                return false;
            }

            if (barBeginDate.EditValue == null)
            {
                XtraMessageBox.Show("������� ���� ������ �������", "������ �������", MessageBoxButtons.OK);
                return false;
            }

            if (barEndDate.EditValue == null)
            {
                XtraMessageBox.Show("������� ���� ��������� �������", "������ �������", MessageBoxButtons.OK);
                return false;
            }
            return true;
        }

// Analiz

        private void AnalizByDays(int iTypeAnaliz)
        {
            if (!IsInputParamsValid()) return;
            DateTime dtEnd = (DateTime)barEndDate.EditValue;
            DateTime dtBegin = (DateTime)barBeginDate.EditValue;
            ctrlAnalisis.DataSource = listAnalisData;
            listAnalisData.Clear();
            gridAnalisis.RefreshData();
            DisplayBarText = Resources.LoadingFromDB;
            double days = dtEnd.Subtract(dtBegin).TotalDays;
            MobitelLostData.Clear();

            for (int j = 0; j < Vehicles.Count; j++)
            {
                if (flagStop)
                {
                    pbAnalize.EditValue = 0;
                    DisplayBarText = textFild + " - " + Resources.StopWithUser;
                    pbAnalize.Refresh();
                    Application.DoEvents();
                    System.GC.Collect();

                    return;
                }

                pbAnalize.EditValue = 0;
                pbAnalize.Properties.Minimum = 0;
                pbAnalize.Properties.Maximum = (int)days;
                int iCount = 0;

                IVehicle vehicle = Vehicles[j];

                DisplayBarText = vehicle.CarMaker + "|" + vehicle.CarModel + "|" + vehicle.RegNumber;

                for (int i = 0; i < days; i++)
                {
                    if (flagStop)
                    {
                        pbAnalize.EditValue = 0;
                        DisplayBarText = textFild + " - " + Resources.StopWithUser;
                        pbAnalize.Refresh();
                        Application.DoEvents();
                        System.GC.Collect();

                        return;
                    }

                    DateTime dayBegin = dtBegin.AddDays(i);
                    DateTime dayEnd = dtBegin.AddDays(i + 1);
                    DataTable dtWork;

                    string sSQL = vehicle.Mobitel.Is64BitPackets ? TrackControlQuery.DataAnalysis.SelectDatagpsData64 : TrackControlQuery.DataAnalysis.SelectDatagpsData;

                    switch (iTypeAnaliz)
                    {
                        case (int)AnalizAlgoritms.DATA_DOUBLES:
                            sSQL = sSQL + " ORDER BY SrvPacketID, LogID";
                            break;
                        default:
                            sSQL = sSQL + " ORDER BY Mobitel_ID, LogID";
                            break;
                    }

                    using (DriverDb db = new DriverDb())
                    {
                        db.ConnectDb();
                        db.NewSqlParameterArray(3);
                        db.SetNewSqlParameter(db.ParamPrefics + "Begin", dayBegin);
                        db.SetNewSqlParameter(db.ParamPrefics + "End", dayEnd);
                        db.SetNewSqlParameter(db.ParamPrefics + "Mobitel_id", vehicle.Mobitel.Id);
                        dtWork = db.GetDataTable(sSQL, db.GetSqlParameterArray);
                    }

                    AnalizData(iTypeAnaliz, dayEnd, ref dayBegin, ref sSQL, dtWork);

                    if (iCount <= days)
                    {
                        iCount++;
                        pbAnalize.EditValue = iCount;
                    }
                } // for

                iCount += 3;
                pbAnalize.EditValue = iCount; // ��-�� ���� ��� �� ���������� �� �����
            } // for

            pbAnalize.EditValue = 0;
            DisplayBarText = textFild + " - " + Resources.Done;
            pbAnalize.Refresh();
            Application.DoEvents();
            System.GC.Collect();
        } // Analiz

        private void AnalizData(int iTypeAnaliz, DateTime dtEnd, ref DateTime dtBegin, ref string sSQL, DataTable dtWork)
        {
            int cnt = 0;
            bool bStartAnaliz = false;
            bool bStartZone = false;
            long LogIDprev = 0;
            DateTime DataGPSprev  = DateTime.MinValue;
            long SrvPacketIDprev = 0;
            int Mobitel_IDprev = 0;
            int Mob_Id = 0;
            Application.DoEvents();
            DataRowAnaliz dra = new DataRowAnaliz();

            for (int i = 0; i < dtWork.Rows.Count; i++)
            {
                if (flagStop)
                {
                    return;
                }

                DataRow dr = dtWork.Rows[i];
                dra.DataGPS = Convert.ToDateTime(dr["DataGPS"]);
                dra.LogID = Convert.ToInt64(dr["LogID"]);
                dra.Mobitel_ID = (int) (dr["Mobitel_ID"]);
                dra.SrvPacketID = Convert.ToInt64(dr["SrvPacketID"]);
                dra.Valid = Convert.ToInt32(dr["Valid"]);

                switch (iTypeAnaliz)
                {
                    case (int) AnalizAlgoritms.DATA_DOUBLES:
                        bStartAnaliz = true;
                        break;
                    default:
                        if (Mob_Id != dra.Mobitel_ID)
                        {
                            bStartAnaliz = false;
                            Mob_Id = dra.Mobitel_ID;
                        }
                        break;
                }

                if (bStartAnaliz)
                {
                    try
                    {
                        switch (iTypeAnaliz)
                        {
                            case (int) AnalizAlgoritms.DATA_OMIT:
                                if (((dra.LogID - LogIDprev) > 1) && (SrvPacketIDprev <= dra.SrvPacketID))
                                {
                                    //������ ��� ����� ����  - ���� ��� �� �������� � � ������������  UNIXTIME
                                    // �������������� ������ ���������

                                    sSQL = string.Format(TrackControlQuery.DataAnalysis.SelectCountDatagps, Mob_Id,
                                        LogIDprev, dra.LogID);

                                    int cntRec = 0;

                                    using (DriverDb db = new DriverDb())
                                    {
                                        db.ConnectDb();
                                        cntRec = Convert.ToInt32(db.GetScalarValue(sSQL));
                                    }

                                    if (cntRec <= (dra.LogID - LogIDprev - 1))
                                    {
                                        #region ���������� ������ � �������� �� ����������� ������
                                        if (IsIntervalValid(LogIDprev,dra.LogID,DataGPSprev,dra.DataGPS))
                                        {
                                            QueryLostData _QueryLostData = new QueryLostData();
                                            _QueryLostData.Mobitel_ID = Mob_Id;
                                            _QueryLostData.Begin_LogID = LogIDprev;
                                            _QueryLostData.End_LogID = dra.LogID;
                                            _QueryLostData.Begin_SrvPacketID = SrvPacketIDprev;
                                            _QueryLostData.End_SrvPacketID = dra.SrvPacketID;
                                            MobitelLostData.Add(_QueryLostData);
                                            AnalizAddRow(ref dra, dtBegin, dra.LogID - LogIDprev - 1);
                                        }
                                        #endregion

                                        
                                    } // if
                                } // if

                                LogIDprev = dra.LogID;
                                SrvPacketIDprev = dra.SrvPacketID;
                                DataGPSprev = dra.DataGPS;
                                if (!bStartZone)
                                    dtBegin = dra.DataGPS;

                                break;

                            case (int) AnalizAlgoritms.GPS_ABSENT:
                                switch (dra.Valid)
                                {
                                    case 0:
                                        if (!bStartZone)
                                        {
                                            bStartZone = true;
                                            dtBegin = dra.DataGPS;
                                            cnt = 0;
                                        }
                                        cnt++;
                                        break;
                                    case 1:
                                        if (bStartZone)
                                        {
                                            AnalizAddRow(ref dra, dtBegin, cnt);
                                            bStartZone = false;
                                        }
                                        break;
                                }
                                LogIDprev = dra.LogID;
                                DataGPSprev = dra.DataGPS;
                                break;
                                
                            case (int) AnalizAlgoritms.DATA_DOUBLES:
                                if ((SrvPacketIDprev == dra.SrvPacketID && (LogIDprev == dra.LogID) &&
                                     (Mobitel_IDprev != dra.Mobitel_ID)))
                                {
                                    if (MobitelAcum.ContainsKey(dra.Mobitel_ID))
                                    {
                                        _MobitelInfo.CNT = MobitelAcum[dra.Mobitel_ID].CNT + 1;
                                        _MobitelInfo.dtBegin = MobitelAcum[dra.Mobitel_ID].dtBegin;
                                        _MobitelInfo.dtEnd = dra.DataGPS;
                                        _MobitelInfo.Name = MobitelAcum[dra.Mobitel_ID].Name;
                                        MobitelAcum.Remove(dra.Mobitel_ID);
                                        MobitelAcum.Add(dra.Mobitel_ID, _MobitelInfo);
                                    }
                                    else
                                    {
                                        _MobitelInfo.CNT = 1;
                                        _MobitelInfo.dtBegin = dra.DataGPS;
                                        _MobitelInfo.dtEnd = dra.DataGPS;
                                        _MobitelInfo.Name = GetMobitelName(dra.Mobitel_ID);
                                        MobitelAcum.Add(dra.Mobitel_ID, _MobitelInfo);
                                    }
                                }

                                LogIDprev = dra.LogID;
                                DataGPSprev = dra.DataGPS;
                                SrvPacketIDprev = dra.SrvPacketID;
                                Mobitel_IDprev = dra.Mobitel_ID;

                                break;
                                
                            case (int) AnalizAlgoritms.DATA_STATISTIC:
                                if (MobitelAcum.ContainsKey(dra.Mobitel_ID))
                                {
                                    _MobitelInfo.CNT = MobitelAcum[dra.Mobitel_ID].CNT + 1;
                                    _MobitelInfo.dtBegin = MobitelAcum[dra.Mobitel_ID].dtBegin;
                                    if (_MobitelInfo.dtEnd.Subtract(dra.DataGPS).Minutes < 0)
                                        _MobitelInfo.dtEnd = dra.DataGPS;
                                    _MobitelInfo.Name = MobitelAcum[dra.Mobitel_ID].Name;

                                    MobitelAcum.Remove(dra.Mobitel_ID);
                                    MobitelAcum.Add(dra.Mobitel_ID, _MobitelInfo);
                                }
                                else
                                {
                                    _MobitelInfo.CNT = 1;
                                    _MobitelInfo.dtBegin = dra.DataGPS;
                                    _MobitelInfo.dtEnd = dra.DataGPS;
                                    _MobitelInfo.Name = GetMobitelName(dra.Mobitel_ID);
                                    MobitelAcum.Add(dra.Mobitel_ID, _MobitelInfo);
                                }
                                break;
                        }
                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show(ex.StackTrace, String.Format(" Error LogID: {0}", LogIDprev));
                    }
                }
                else
                    //��������� ��������� ������ � ������������ UNIX ��������
                    if (dra.Valid == 1)
                    {
                        bStartAnaliz = true;
                        bStartZone = false;
                        LogIDprev = dra.LogID;
                        DataGPSprev = dra.DataGPS;
                        SrvPacketIDprev = dra.SrvPacketID;
                        Mobitel_IDprev = dra.Mobitel_ID;
                    }
            } // for

            //��������� ���
            switch (iTypeAnaliz)
            {
                case (int) AnalizAlgoritms.DATA_DOUBLES:
                    AnalizAddDictionary();
                    break;
                case (int) AnalizAlgoritms.DATA_STATISTIC:
                    AnalizAddDictionary();
                    break;
                case (int) AnalizAlgoritms.GPS_ABSENT:
                    if (bStartZone) AnalizAddRow(ref dra, dtBegin, cnt);
                    // � ����� �������� ���������� ������
                    if (dtEnd.Subtract(dra.DataGPS).TotalMinutes > 0)
                    {
                        dtBegin = dra.DataGPS;

                        using (DriverDb dbDriver = new DriverDb())
                        {
                            dbDriver.ConnectDb();

                            sSQL = string.Format(TrackControlQuery.DataAnalysis.SelectDataGpsId, dra.LogID,
                                dra.Mobitel_ID);

                            dbDriver.GetDataReader(sSQL);

                            cnt = 0;

                            while (dbDriver.Read())
                            {
                                if (!dbDriver.IsDbNull(dbDriver.GetOrdinal("Valid")))
                                {
                                    if (dbDriver.GetInt16("Valid") == 1)
                                        break;
                                }

                                if (!dbDriver.IsDbNull(dbDriver.GetOrdinal("DataGPS")))
                                {
                                    if (dtEnd.Subtract(dbDriver.GetDateTime("DataGPS")).TotalMinutes < 0)
                                        break;
                                }
                                cnt++;
                            }
                        }
                        if (cnt > 0) AnalizAddRowNoValid(ref dra, dtBegin, cnt);
                    }
                    break;
                default:
                    if (bStartZone) AnalizAddRow(ref dra, dtBegin, cnt);
                    break;
            }
        }


        private bool IsIntervalValid(long LogIDprev, long LogID,DateTime dtBegin, DateTime dtEnd)
        {
            double diffLogId = LogID - LogIDprev;
            if (diffLogId > 0)
            {
                TimeSpan tsWork =dtEnd.Subtract(dtBegin);
                double diffSeconds = Math.Abs(tsWork.TotalSeconds);
                if (diffSeconds > diffLogId) return true;
            }
            return false;
        }


        private bool AnalizAddDictionary()
        {
            foreach (MobitelInfo mi in MobitelAcum.Values)
            {
                AnalisData analisData = new AnalisData(mi.Name, mi.dtBegin, mi.dtEnd, mi.dtEnd.Subtract(mi.dtBegin),
                    mi.CNT);
                listAnalisData.Add(analisData);
                gridAnalisis.RefreshData();
            }

            return true;
        }

        private bool AnalizAddRowNoValid( ref DataRowAnaliz dra, DateTime dtBegin, int cnt )
        {
            if( dra.Mobitel_ID <= 0 ) 
                return false;

            AnalisData analisData = new AnalisData( GetMobitelName( dra.Mobitel_ID ), dtBegin, new DateTime(), new TimeSpan(), cnt );
            listAnalisData.Add( analisData );gridAnalisis.RefreshData();
            return true;
        }

        private bool AnalizAddRow( ref DataRowAnaliz dra, DateTime dtBegin, long cnt )
        {
            if( dra.Mobitel_ID <= 0 ) 
                return false;

            DateTime dtEnd = dra.DataGPS;
           
            if( dtEnd.Subtract( dtBegin ).Minutes >= Convert.ToInt16( txMine.EditValue ) )
            {
                AnalisData analisData = new AnalisData( GetMobitelName( dra.Mobitel_ID ), dtBegin, dtEnd, dtEnd.Subtract( dtBegin ), cnt );
                listAnalisData.Add( analisData );
                gridAnalisis.RefreshData();
            }

            return true;
        }

        private string GetMobitelName( int Mobitel_Id )
        {
            if (dictMobitels.ContainsKey(Mobitel_Id))
            {
                return dictMobitels[Mobitel_Id];
            }
            else
            {
                return Mobitel_Id.ToString();
            }
        }

        private void DataAnalysis_Load( object sender, EventArgs e )
        {
            if( DesignMode )
            {
                return;
            }

            dataset = AppModel.Instance.DataSet;
            barEndDate.EditValue = DateTime.Now;
            barBeginDate.EditValue = DateTime.Now.AddMonths( -1 );
            
            DriverDb db = new DriverDb();
            db.ConnectDb();
            {
                string sSQLselect = TrackControlQuery.DataAnalysis.SelectVehicleMobitel;

                DataTable dt = null;
                dt = db.GetDataTable( sSQLselect );

                dictMobitels = new Dictionary<int, string>();
                DataColumn col = new DataColumn( "CH", System.Type.GetType( "System.Boolean" ) );
                col.DefaultValue = false;
                dt.Columns.Add( col );

                foreach( DataRow dr in dt.Rows )
                {
                    if( !dictMobitels.ContainsKey( ( int )dr["Mobitel_id"] ) )
                    {
                        dictMobitels.Add( ( int )dr["Mobitel_id"], dr["Name"].ToString() );
                    }
                }
            }
            
            db.CloseDbConnection();
        }

        private void btnNoGps_ItemClick( object sender, DevExpress.XtraBars.ItemClickEventArgs e )
        {
            flagStop = false;
            textFild = btnNoGps.Caption;
            AnalizByDays( ( int )AnalizAlgoritms.GPS_ABSENT );
        }

        private void btnDeleteNoCorrect_ItemClick( object sender, DevExpress.XtraBars.ItemClickEventArgs e )
        {
            flagStop = false;
            textFild = btnDeleteNoCorrect.Caption;
            DeleteRecordsWithDatesInFuture();
        }

        private int GridCheckedCount()
        {
            int counter = 0;
            
            VehiclesList = vehiclesPanel.GetCheckedNode;

            if( VehiclesList.Count > 0 )
                counter = VehiclesList.Count;

            return counter;
        }

        private void DeleteRecordsWithDatesInFuture()
        {
            int countChecked = GridCheckedCount();

            if( countChecked == 0 )
            {
                XtraMessageBox.Show( Resources.SelectVehicleForAnalysis, Resources.DataAnalysis, MessageBoxButtons.OK );
                return;
            }

            if( DialogResult.OK ==
                XtraMessageBox.Show( Resources.ConfimDateDeleting, Resources.DataAnalysis, MessageBoxButtons.OKCancel ) )
            {
                int iCount = 0;
                pbAnalize.EditValue = 0;
                pbAnalize.Properties.Minimum = 0;
                pbAnalize.Properties.Maximum = countChecked;
                
                for(int j = 0; j < VehiclesList.Count; j++)
                {
                    if (flagStop)
                    {
                        pbAnalize.EditValue = 0;
                        DisplayBarText = textFild + " - " + Resources.StopWithUser;
                        pbAnalize.Refresh();
                        Application.DoEvents();

                        return;
                    }

                    IVehicle vehicle = VehiclesList[j];
                    bool checkedCell = true;
                    
                        if( checkedCell )
                        {
                            DisplayBarText = vehicle.CarMaker + "|" + vehicle.CarModel + "|" + vehicle.RegNumber;
                            //GetMobitelName( vehicle.Mobitel.Id );
                            
                            var db = new DriverDb();
                            db.ConnectDb();
                            if (vehicle.Is64BitPackets)
                            {
                                string sql = string.Format(TrackControlQuery.DataAnalysis.UpdateDatagps64,
                                                vehicle.Mobitel.Id);
                                db.ExecuteNonQueryCommand(sql);
                                sql = string.Format(TrackControlQuery.DataAnalysis.DeleteFromOnline64, vehicle.Mobitel.Id);
                                db.ExecuteNonQueryCommand(sql);
                            }
                            else
                            {
                                string sql = string.Format(TrackControlQuery.DataAnalysis.UpdateDatagps,
                                                vehicle.Mobitel.Id);
                                db.ExecuteNonQueryCommand(sql);
                                sql = string.Format(TrackControlQuery.DataAnalysis.DeleteFromOnline, vehicle.Mobitel.Id);
                                db.ExecuteNonQueryCommand(sql);
                                
                            }
                            db.CloseDbConnection();
                            iCount++;
                            pbAnalize.EditValue = iCount;
                            Application.DoEvents();
                        } // if
                } // for

                pbAnalize.EditValue = 0;
                DisplayBarText = textFild + " - " + Resources.Done;
                pbAnalize.Refresh();
                Application.DoEvents();
                XtraMessageBox.Show( Resources.DeletingComplite, Resources.DataAnalysis, MessageBoxButtons.OK );
            }
        }

        private void btnMissingData_ItemClick( object sender, DevExpress.XtraBars.ItemClickEventArgs e )
        {
            flagStop = false;
            textFild = btnMissingData.Caption;
            AnalizAllPeriod( ( int )AnalizAlgoritms.DATA_OMIT );
        }

        private void btnAdding_ItemClick( object sender, DevExpress.XtraBars.ItemClickEventArgs e )
        {
            flagStop = false;
            textFild = btnAdding.Caption;

            if( !GridChecked() )
            {
                MessageBox.Show( Resources.SelectVehicleForReQuery, Resources.DataAnalysis );
                return;
            }

            if( DialogResult.Yes == XtraMessageBox.Show( this, Resources.ReQueryConfirm, Resources.DataAnalysis, MessageBoxButtons.YesNo ) )
            {
                Int64 NewConfirmedID = 0;
                string sSQL;
                int iCount = 0;

                DriverDb db = new DriverDb();
                db.ConnectDb();

                if( MobitelLostData.Count > 0 )
                {
                    sSQL = TrackControlQuery.PointsAnalyzer.TruncateDatagpsbuffer_on;
                
                    db.ExecuteNonQueryCommand( sSQL );

                    sSQL = TrackControlQuery.PointsAnalyzer.TruncateDatagpsbuffer_on64;

                    db.ExecuteNonQueryCommand(sSQL);

                    sSQL = TrackControlQuery.PointsAnalyzer.TruncateDatagpslost_on;
                  
                    db.ExecuteNonQueryCommand( sSQL );

                    sSQL = TrackControlQuery.PointsAnalyzer.TruncateDatagpslost_ontmp;
                  
                    db.ExecuteNonQueryCommand( sSQL );
                    pbAnalize.Properties.Maximum = MobitelLostData.Count;
                    pbAnalize.Properties.Minimum = 0;
                    pbAnalize.EditValue = 0;
                    iCount = 0;

                    DisplayBarText = textFild + " - " + Resources.ReQuery;
                    int m_id = 0;

                    for( int k = 0; k < MobitelLostData.Count; )
                    {
                        if (flagStop)
                        {
                            return;
                        }

                        QueryLostData qld = MobitelLostData[k];

                        if( m_id != qld.Mobitel_ID )
                        {
                            if( m_id > 0 )
                            {
                                sSQL = string.Format( TrackControlQuery.DataAnalysis.UpdateMobitels, NewConfirmedID, m_id );

                                db.ExecuteNonQueryCommand( sSQL );
                            }

                            m_id = qld.Mobitel_ID;
                            NewConfirmedID = qld.Begin_LogID;
                        }
                        DriverDb driverDb = new DriverDb();
                        driverDb.ConnectDb();

                        sSQL = string.Format( TrackControlQuery.DataAnalysis.SelectCountDatagpslost, qld.Mobitel_ID,
                            qld.Begin_SrvPacketID, qld.Mobitel_ID, qld.Begin_LogID );

                        int cntUNIQUE = Convert.ToInt32( driverDb.GetScalarValue( sSQL ) );
                        
                        driverDb.CloseDbConnection();
                        
                        if( ( cntUNIQUE == 0 ) )
                        {
                            sSQL = string.Format( TrackControlQuery.DataAnalysis.InsertIntoDatagpslost, qld.Mobitel_ID,
                                qld.Begin_LogID, qld.End_LogID, qld.Begin_SrvPacketID,
                                qld.End_SrvPacketID );

                            db.ExecuteNonQueryCommand( sSQL );

                            if( NewConfirmedID > qld.Begin_LogID )
                                NewConfirmedID = qld.Begin_LogID;

                            iCount++;
                            pbAnalize.EditValue = iCount;
                            pbAnalize.Refresh();
                        } // if

                        MobitelLostData.Remove( qld );
                    } // for

                    if( m_id > 0 )
                    {
                        sSQL = string.Format( TrackControlQuery.DataAnalysis.UpdateMobitelsConfirmedId, NewConfirmedID,
                            m_id );

                        db.ExecuteNonQueryCommand( sSQL );
                    }
                } // If(MobitelLostData.Count > 0)
                else
                // � ���� ������ �� ��� ��� �������.
                // ���� ���� ������������� �������� LogID, �� ��� �����������
                // ������ �������� ConfirmedID ��������� �������������� ������:
                // ��������� ����� �������� LogID ������������� ��� �������������
                // ����� ����� (�� DataGpsId). 
                {
                    for(int k = 0; k < Vehicles.Count; k++)
                    {
                        if (flagStop)
                        {
                            pbAnalize.EditValue = 0;
                            DisplayBarText = textFild + " - " + Resources.Done;
                            pbAnalize.Refresh();
                            return;
                        }

                        IVehicle vehicle = Vehicles[k];

                        DisplayBarText = vehicle.CarMaker + "|" + vehicle.CarModel + "|" + vehicle.RegNumber;

                            sSQL = TrackControlQuery.DataAnalysis.SelectDatagpsLost_on +
                                   vehicle.Mobitel.Id;

                            if (db.GetDataReaderRecordsCount(sSQL) == 0)
                            {
                                sSQL = string.Format(TrackControlQuery.DataAnalysis.Select1FromDataGps,
                                    vehicle.Mobitel.Id );

                                if (db.GetDataReaderRecordsCount(sSQL) == 1) //���� �������������
                                {

                                    sSQL = string.Format(TrackControlQuery.DataAnalysis.SelectMaxDataGpsId,
                                        vehicle.Mobitel.Id );

                                    Int64 MaxNegativeDataGpsId = Convert.ToInt64(db.GetScalarValue(sSQL));
                                    Int64 MaxPositiveDataGpsId = 0;

                                    sSQL = string.Format(TrackControlQuery.DataAnalysis.Select1DataGps,
                                        vehicle.Mobitel.Id );

                                    if (db.GetDataReaderRecordsCount(sSQL) == 1) //���� �������������
                                    {
                                        sSQL =
                                            string.Format(TrackControlQuery.DataAnalysis.SelectMaxDataGpsIdFromDataGps,
                                                vehicle.Mobitel.Id );

                                        MaxPositiveDataGpsId = Convert.ToInt64(db.GetScalarValue(sSQL));
                                    }
                                    if (MaxNegativeDataGpsId > MaxPositiveDataGpsId)
                                    {
                                        sSQL = string.Format(TrackControlQuery.DataAnalysis.SelectCoalesceMaxLogID,
                                            vehicle.Mobitel.Id );

                                        NewConfirmedID = Convert.ToInt64(db.GetScalarValue(sSQL));
                                    }
                                    else
                                    {
                                        sSQL = string.Format(TrackControlQuery.DataAnalysis.SelectCoalesceMaxLog_id,
                                            vehicle.Mobitel.Id );

                                        NewConfirmedID = Convert.ToInt64(db.GetScalarValue(sSQL));
                                    }
                                }
                                else //������ �������������
                                {
                                    sSQL = string.Format(TrackControlQuery.DataAnalysis.SelectCoalesceMaxDataGps,
                                        vehicle.Mobitel.Id );

                                    NewConfirmedID = Convert.ToInt64(db.GetScalarValue(sSQL));
                                }

                                sSQL = string.Format(TrackControlQuery.DataAnalysis.UpdateMobitelsConfirmed_ID,
                                    NewConfirmedID, vehicle.Mobitel.Id );

                                db.ExecuteNonQueryCommand(sSQL);
                            } // if
                        } // for
                    } // else

                db.CloseDbConnection();
            } // if

            //pbAnalize.EditValue = 0;
            //DisplayBarText = textFild + " - " + Resources.Done;
            //pbAnalize.Refresh();
            //Application.DoEvents();
        } 

        private void btnDuplicatedData_ItemClick( object sender, DevExpress.XtraBars.ItemClickEventArgs e )
        {
            flagStop = false;
            textFild = btnDuplicatedData.Caption;
            MobitelAcum.Clear();
            AnalizByDays( ( int )AnalizAlgoritms.DATA_DOUBLES );
        }

        private void barBtnCancel_ItemClick( object sender, ItemClickEventArgs e )
        {
            flagStop = flagStop ? false : true;
        }

        private void pgParams_Click(object sender, EventArgs e)
        {
            // to do this
        }
    }
}