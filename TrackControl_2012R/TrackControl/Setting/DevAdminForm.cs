﻿using System.Reflection;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraTreeList;
using DevExpress.XtraVerticalGrid;
using LocalCache;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data.SqlClient;
using System.Resources;
using System.Windows.Forms;
using TrackControl.General;
using TrackControl.General.DatabaseDriver;
using TrackControl.Properties;
using TrackControl.Vehicles;

namespace TrackControl.Setting
{
    public partial class DevAdminForm : UserControl
    {
        private DevVehicleControl vehicleControl;
        private PropertyObject _properObj;
        private bool flagPropertyGridChanged = false;
        //private RowChangedEventArgs RowChanged = null;
        private static string codeParameter;
        private static RepositoryItemComboBox GetRepItemCombo;
        private static BarEditItem GetBarEditItem;
        private static PropertyGridControl gsSettingProperty;
        private static PropertyObject GetPropertyObject;
        private static int SelectedIndex = 0;
        //private string cultureRu;
        public DevAdminForm()
        {
            //cultureRu = Resources.Cultura;
            //Thread.CurrentThread.CurrentUICulture = new CultureInfo( cultureRu, false );

            InitializeComponent();
            gcSettings.SelectedObject = null;
            _properObj = new PropertyObject();
            Load += new EventHandler(DevAdminForm_Load);

            repositoryItemComboBox1.SelectedIndexChanged += RepositoryItemComboBox1OnSelectedIndexChanged;
            // disabling the text editor of the combo box 
            repositoryItemComboBox1.TextEditStyle = TextEditStyles.DisableTextEditor;

            bbiAddNewRow.Hint = Resources.AddNewParam;
            //barDeleteSetting.Hint = Resources.DelParam;
            settingBindingNavigatorSaveItem.Hint = Resources.SaveParam;
            refreshToolStripButton.Hint = Resources.RefreshTable;
            assignToolStripButton.Hint = Resources.AssignParam;
            barStaticItem2.Caption = Resources.CodeParam + ":";
            bbiLog.Hint = Resources.EventLogViewer;


            // static record
            GetRepItemCombo = repositoryItemComboBox1;
            GetBarEditItem = barEditItemName;
            gsSettingProperty = gcSettings;
            GetPropertyObject = _properObj;
            settingBindingNavigatorSaveItem.Enabled = false;
            gcSettings.CellValueChanged += gcSettings_CellValueChanged;
        }

        void gcSettings_CellValueChanged( object sender, DevExpress.XtraVerticalGrid.Events.CellValueChangedEventArgs e )
        {
            settingBindingNavigatorSaveItem.Enabled = true;
        }
        
        // DevAdminForm
        private void RepositoryItemComboBox1OnSelectedIndexChanged(object sender, EventArgs eventArgs)
        {
            try
            {
                gcSettings.SelectedObject = null;
                ComboBoxEdit comboBoxEdit = sender as ComboBoxEdit;

                if (comboBoxEdit == null)
                {
                    return;
                }

                SelectedIndex = comboBoxEdit.SelectedIndex;
                _properObj.Index(SelectedIndex);
                codeParameter = repositoryItemComboBox1.Items[SelectedIndex].ToString();
                gcSettings.SelectedObject = _properObj;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.StackTrace, Resources.Parameter);
                return;
            }
        }
        // RepositoryItemComboBox1OnSelectedIndexChanged
        public static string GetCodeParameter()
        {
            return codeParameter;
        }

        public void ShowTree(DevVehicleControl tree)
        {
            vehicleControl = tree;
            panelTreeControl.Controls.Add(tree);
            tree.Dock = DockStyle.Fill;
        }

        //// грузим данные из базы
        protected void DevAdminForm_Load(object o, EventArgs e)
        {
            try
            {
                string cs =
                    Crypto.GetDecryptConnectionString(ConfigurationManager.ConnectionStrings["CS"].ConnectionString);

                //vehicleTableAdapter.Connection.ConnectionString = cs;

                if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                {
                    MySqlConnection connection = (MySqlConnection)vehicleTableAdapter.Connection;
                    connection.ConnectionString = cs;
                }
                else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                {
                    SqlConnection connection = (SqlConnection)vehicleTableAdapter.Connection;
                    connection.ConnectionString = cs;
                }

                //settingTableAdapter.Connection.ConnectionString = cs;

                if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                {
                    MySqlConnection connection = (MySqlConnection)settingTableAdapter.Connection;
                    connection.ConnectionString = cs;
                }
                else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                {
                    SqlConnection connection = (SqlConnection)settingTableAdapter.Connection;
                    connection.ConnectionString = cs;
                }

                atlantaDataSet = AppModel.Instance.DataSet;

                vehicleBindingSource.DataSource = atlantaDataSet.vehicle;
                settingBindingSource.DataSource = atlantaDataSet.setting;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.StackTrace, Resources.Parameter);
                return;
            }

            gcSettings.SelectedObject = null;
            LoadSettings();

            if (repositoryItemComboBox1.Items.Count <= 0)
                return;

            object val = repositoryItemComboBox1.Items[0];
            barEditItemName.EditValue = val;
            _properObj.Index(0);
            gcSettings.SelectedObject = _properObj;
            codeParameter = repositoryItemComboBox1.Items[0].ToString();
        }
        // DevAdminForm_Load
        public static void FitSelectItem(string valustr)
        {
            int index = -1;

            for (int i = 0; i < GetRepItemCombo.Items.Count; i++)
            {
                string str = GetRepItemCombo.Items[i].ToString();

                if (str.Equals(valustr))
                {
                    index = i;
                    break;
                } // if
            } // for

            if (index != -1)
            {
                gsSettingProperty.SelectedObject = null;
                object val = GetRepItemCombo.Items[index];
                GetBarEditItem.EditValue = val;
                GetPropertyObject.Index(index);
                gsSettingProperty.SelectedObject = GetPropertyObject;
                SelectedIndex = index;
            } // if
        } // FitSelectItem

        // загружаем данные из базы в PropertyGrid
        protected void LoadSettings()
        {
            try
            {
                _properObj.ClearData();
                _properObj.Index(0);
                repositoryItemComboBox1.Items.Clear();

                for (int i = 0; i < atlantaDataSet.setting.Rows.Count; i++)
                {
                    atlantaDataSet.settingRow row = (atlantaDataSet.settingRow)(atlantaDataSet.setting.Rows[i]);

                    _properObj._totalDut.addAvgFuelRatePerHour(row.AvgFuelRatePerHour);
                    _properObj.addCzMinCrossingPairTime(row.CzMinCrossingPairTime);
                    _properObj._AlgorithmOne.addFuelApproximationTime(row.FuelApproximationTime);
                    _properObj.addFuelerCountInMotion(row.FuelerCountInMotion);
                    _properObj.addFuelerMaxTimeStop(row.FuelerMaxTimeStop);
                    _properObj.addFuelerMinFuelrate(row.FuelerMinFuelrate);
                    _properObj.addDesc(row.Desc);
                    _properObj.addName(row.Name);
                    _properObj.addFuelerRadiusFinds(row.FuelerRadiusFinds);
                    _properObj._AlgorithmOne.addFuelingDischarge(row.FuelingDischarge);
                    _properObj._AlgorithmOne.addFuelingEdger(row.FuelingEdge);
                    _properObj._AlgorithmThird.addFuelingMinMaxAlgorithm(row.FuelingMinMaxAlgorithm);
                    _properObj._Inclinometer.addMaxAngleAxisX(row.InclinometerMaxAngleAxisX);
                    _properObj._Inclinometer.addMaxAngleAxisY(row.InclinometerMaxAngleAxisY);
                    _properObj._totalDut.addFuelrateWithDischarge(row.FuelrateWithDischarge);
                    _properObj.addRotationMain(row.RotationMain);
                    _properObj.addTimeBreak(row.TimeBreak);
                    _properObj.addTimeBreakMaxPermitted(row.TimeBreakMaxPermitted);
                    _properObj.addTimeMinimMovingSize(row.TimeMinimMovingSize);
                    _properObj._AlgorithmTwo.addTimeGetFuelAfterStop(row.TimeGetFuelAfterStop);
                    _properObj._AlgorithmTwo.addTimeGetFuelBeforeMotion(row.TimeGetFuelBeforeMotion);
                    _properObj.addAccelMax(row.accelMax);
                    _properObj._AlgorithmOne.addBand(row.band);
                    _properObj.addSpeedMax(row.speedMax);
                    _properObj.addSpeedMin(row.speedMin);
                    _properObj.addBreakMax(row.breakMax);
                    _properObj.addCzMinTimeAboutToSwitch(row.MinTimeSwitch);

                    repositoryItemComboBox1.Items.Add(row.Name);
                } // for
            } // try
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message + " " + ex.InnerException, Resources.Parameter);
                return;
            }
        }
        // LoadSettings
        // перегружаем-обновляем данные из базы
        private void refreshToolStripButton_ItemClick_1(object sender, ItemClickEventArgs e)
        {
            try
            {
                settingTableAdapter.Update(atlantaDataSet.setting);
                gcSettings.SelectedObject = null;

                LoadSettings();

                object val = repositoryItemComboBox1.Items[SelectedIndex];
                barEditItemName.EditValue = val;
                _properObj.Index(SelectedIndex);
                gcSettings.SelectedObject = _properObj;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.StackTrace, Resources.Parameter);
                return;
            }
        }
        // refreshToolStripButton_ItemClick_1
        // сохранить изменённые настройки в БД
        private void settingBindingNavigatorSaveItem_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                var properObjBuff = gcSettings.SelectedObject as PropertyObject;

                if (properObjBuff != null)
                {
                    if (properObjBuff.Name.Length >= 20)
                    {
                        XtraMessageBox.Show("Длина имени настройки не более 20 символов!", "Ошибка имени настройки",
                            MessageBoxButtons.OK);
                        return;
                    }

                    if (properObjBuff.Desc.Length >= 80)
                    {
                        XtraMessageBox.Show("Длина описания настройки не более 80 символов!",
                            "Ошибка описания настройки",
                            MessageBoxButtons.OK);
                        return;
                    }

                    // заполняем таблицу согласно полученных данных
                    settingBindingSource.SuspendBinding();

                    atlantaDataSet.settingRow row =
                        (atlantaDataSet.settingRow)(atlantaDataSet.setting.Rows[SelectedIndex]);

                    properObjBuff.Index(SelectedIndex);
                    LogChanging(row.accelMax.ToString(), properObjBuff.accelMax.ToString(), properObjBuff, "accelMax");
                    row.accelMax = properObjBuff.accelMax;
                    LogChanging(row.AvgFuelRatePerHour.ToString(), properObjBuff._totalDut.AvgFuelRatePerHour.ToString(), properObjBuff._totalDut, "AvgFuelRatePerHour");
                    row.AvgFuelRatePerHour = properObjBuff._totalDut.AvgFuelRatePerHour;
                    LogChanging(row.band.ToString(), properObjBuff._AlgorithmOne.band.ToString(), properObjBuff._AlgorithmOne, "band");
                    row.band = properObjBuff._AlgorithmOne.band;
                    LogChanging(row.breakMax.ToString(), properObjBuff.breakMax.ToString(), properObjBuff, "breakMax");
                    row.breakMax = properObjBuff.breakMax;
                    LogChanging(row.CzMinCrossingPairTime.ToString(), properObjBuff.CzMinCrossingPairTime.ToString(), properObjBuff, "CzMinCrossingPairTime");
                    row.CzMinCrossingPairTime = properObjBuff.CzMinCrossingPairTime;
                    LogChanging(row.Name, properObjBuff.Name, properObjBuff, "Name");
                    row.Name = properObjBuff.Name;
                    LogChanging(row.Desc, properObjBuff.Desc, properObjBuff, "Desc");
                    row.Desc = properObjBuff.Desc;
                    LogChanging(row.FuelApproximationTime.ToString(), properObjBuff._AlgorithmOne.FuelApproximationTime.ToString(), properObjBuff._AlgorithmOne, "FuelApproximationTime");
                    row.FuelApproximationTime = properObjBuff._AlgorithmOne.FuelApproximationTime;
                    LogChanging(row.FuelerMaxTimeStop.ToString(), properObjBuff.FuelerMaxTimeStop.ToString(), properObjBuff, "FuelerMaxTimeStop");
                    row.FuelerMaxTimeStop = properObjBuff.FuelerMaxTimeStop;
                    LogChanging(row.FuelerMinFuelrate.ToString(), properObjBuff.FuelerMinFuelrate.ToString(), properObjBuff, "FuelerMinFuelrate");
                    row.FuelerMinFuelrate = properObjBuff.FuelerMinFuelrate;
                    LogChanging(row.FuelerRadiusFinds.ToString(), properObjBuff.FuelerRadiusFinds.ToString(), properObjBuff, "FuelerRadiusFinds");
                    row.FuelerRadiusFinds = properObjBuff.FuelerRadiusFinds;
                    LogChanging(row.FuelingDischarge.ToString(), properObjBuff._AlgorithmOne.FuelingDischarge.ToString(), properObjBuff._AlgorithmOne, "FuelingDischarge");
                    row.FuelingDischarge = properObjBuff._AlgorithmOne.FuelingDischarge;
                    LogChanging(row.FuelingEdge.ToString(), properObjBuff._AlgorithmOne.FuelingEdge.ToString(), properObjBuff._AlgorithmOne, "FuelingEdge");
                    row.FuelingEdge = properObjBuff._AlgorithmOne.FuelingEdge;
                    LogChanging(row.speedMax.ToString(), properObjBuff.SpeedMax.ToString(), properObjBuff, "SpeedMax");
                    row.speedMax = properObjBuff.SpeedMax;
                    LogChanging(row.speedMin.ToString(), properObjBuff.SpeedMin.ToString(), properObjBuff, "SpeedMin");
                    row.speedMin = properObjBuff.SpeedMin;
                    LogChanging(row.TimeBreak.ToString(), properObjBuff.TimeBreak.ToString(), properObjBuff, "TimeBreak");
                    row.TimeBreak = properObjBuff.TimeBreak;
                    LogChanging(row.TimeGetFuelAfterStop.ToString(), properObjBuff._AlgorithmTwo.TimeGetFuelAfterStop.ToString(), properObjBuff._AlgorithmTwo, "TimeGetFuelAfterStop");
                    row.TimeGetFuelAfterStop = properObjBuff._AlgorithmTwo.TimeGetFuelAfterStop;
                    LogChanging(row.TimeGetFuelBeforeMotion.ToString(), properObjBuff._AlgorithmTwo.TimeGetFuelBeforeMotion.ToString(), properObjBuff._AlgorithmTwo, "TimeGetFuelBeforeMotion");
                    row.TimeGetFuelBeforeMotion = properObjBuff._AlgorithmTwo.TimeGetFuelBeforeMotion;
                    LogChanging(row.FuelerCountInMotion.ToString(), properObjBuff.FuelerCountInMotion.ToString(), properObjBuff, "FuelerCountInMotion");
                    row.FuelerCountInMotion = properObjBuff.FuelerCountInMotion;
                    LogChanging(row.FuelrateWithDischarge.ToString(), properObjBuff._totalDut.FuelrateWithDischarge.ToString(), properObjBuff._totalDut, "FuelrateWithDischarge");
                    row.FuelrateWithDischarge = properObjBuff._totalDut.FuelrateWithDischarge;
                    LogChanging(row.FuelingMinMaxAlgorithm.ToString(), properObjBuff._AlgorithmThird.FuelingMinMaxAlgorithm.ToString(), properObjBuff._AlgorithmThird, "FuelingMinMaxAlgorithm");
                    row.FuelingMinMaxAlgorithm = properObjBuff._AlgorithmThird.FuelingMinMaxAlgorithm;
                    LogChanging(row.TimeBreakMaxPermitted.ToString(), properObjBuff.TimeBreakMaxPermitted.ToString(), properObjBuff, "TimeBreakMaxPermitted");
                    row.TimeBreakMaxPermitted = properObjBuff.TimeBreakMaxPermitted;
                    LogChanging(row.InclinometerMaxAngleAxisX.ToString(), properObjBuff._Inclinometer.MaxAngleAxisX.ToString(), properObjBuff._Inclinometer, "MaxAngleAxisX");
                    row.InclinometerMaxAngleAxisX = properObjBuff._Inclinometer.MaxAngleAxisX;
                    LogChanging(row.InclinometerMaxAngleAxisY.ToString(), properObjBuff._Inclinometer.MaxAngleAxisY.ToString(), properObjBuff._Inclinometer, "MaxAngleAxisY");
                    row.InclinometerMaxAngleAxisY = properObjBuff._Inclinometer.MaxAngleAxisY;
                    LogChanging(row.TimeMinimMovingSize.ToString(), properObjBuff.TimeMinimMovingSize.ToString(), properObjBuff, "TimeMinimMovingSize");
                    row.TimeMinimMovingSize = properObjBuff.TimeMinimMovingSize;
                    LogChanging(row.MinTimeSwitch.ToString(), properObjBuff.CzMinTimeAboutToSwitch.ToString(), properObjBuff, "CzMinTimeAboutToSwitch");
                    row.MinTimeSwitch = properObjBuff.CzMinTimeAboutToSwitch;

                    row.RotationAdd = 100;
                    row.idleRunningAdd = 0;
                    row.idleRunningMain = 0;

                    // записываем обновленную таблицу в базу
                    settingBindingSource.ResumeBinding();
                    settingBindingSource.EndEdit();
                    settingTableAdapter.Update(atlantaDataSet.setting);
                    atlantaDataSet.AcceptChanges();
                } // if

                gcSettings.SelectedObject = null;

                LoadSettings();

                object val = repositoryItemComboBox1.Items[SelectedIndex];
                barEditItemName.EditValue = val;
                _properObj.Index(SelectedIndex);
                gcSettings.SelectedObject = _properObj;
                settingBindingNavigatorSaveItem.Enabled = false;
            } // try
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.StackTrace + " " + ex.Message + " " + ex.InnerException, Resources.Parameter);
            }
        }

        private static void LogChanging(string oldValue, string newValue, GlobalizedObject properObj, string nameProperty)
        {
            string dispName = properObj.GetProperties()[nameProperty].DisplayName;
            if (oldValue != newValue)
            {
                string logValue = string.Format("{0}->{1} {2}", oldValue, newValue, dispName);
                UserLog.InsertLog(UserLogTypes.Settings, logValue, SelectedIndex);
            }
        }

        // settingBindingNavigatorSaveItem_ItemClick
        // добавить новую настройку
        private void bbiAddNewRow_ItemClick(object sender, ItemClickEventArgs e)
        {
            int count = 0;

            try
            {
                settingBindingSource.SuspendBinding();
                count = atlantaDataSet.setting.Rows.Count;
                atlantaDataSet.settingRow new_row = GetDefaultSettingRow(count);
                atlantaDataSet.setting.AddsettingRow(new_row);
                settingBindingSource.ResumeBinding();
                settingBindingSource.EndEdit();
                settingTableAdapter.Update(atlantaDataSet.setting);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message + " " + ex.InnerException, Resources.Parameter);
                return;
            }

            atlantaDataSet.AcceptChanges();
            gcSettings.SelectedObject = null;

            LoadSettings();

            object val = repositoryItemComboBox1.Items[count];
            barEditItemName.EditValue = val;
            _properObj.Index(count);
            gcSettings.SelectedObject = _properObj;
            SelectedIndex = count;
        }
        // CreatePropertyGrid
        private atlantaDataSet.settingRow GetDefaultSettingRow(int count)
        {
            atlantaDataSet.settingRow setting_row = atlantaDataSet.setting.NewsettingRow();

            setting_row.accelMax = 1;
            setting_row.AvgFuelRatePerHour = 30;
            setting_row.band = 5;
            setting_row.breakMax = 1;
            setting_row.CzMinCrossingPairTime = new TimeSpan(0, 1, 0);
            setting_row.Desc = Resources.NewParameter + " " + count;
            setting_row.FuelApproximationTime = new TimeSpan(0, 2, 0);
            setting_row.FuelerMaxTimeStop = new TimeSpan(0, 2, 0);
            setting_row.FuelerMinFuelrate = 1;
            setting_row.FuelerRadiusFinds = 10;
            setting_row.FuelingDischarge = 10;
            setting_row.FuelingEdge = 20;
            setting_row.Name = Resources.NewParameter + " " + count;
            setting_row.RotationMain = 100;
            setting_row.speedMax = 70;
            setting_row.speedMin = 0;
            setting_row.TimeBreak = new TimeSpan(0, 1, 0);
            setting_row.TimeGetFuelAfterStop = new TimeSpan(0);
            setting_row.TimeGetFuelBeforeMotion = new TimeSpan(0);
            setting_row.FuelerCountInMotion = 0;
            setting_row.FuelrateWithDischarge = 0;
            setting_row.FuelingMinMaxAlgorithm = 0;
            setting_row.TimeBreakMaxPermitted = new TimeSpan(3, 0, 0);
            setting_row.TimeMinimMovingSize = new TimeSpan(0, 0, 0);

            setting_row.RotationAdd = 100; 
            setting_row.idleRunningAdd = 0;
            setting_row.idleRunningMain = 0;
            setting_row.InclinometerMaxAngleAxisX = 5;
            setting_row.InclinometerMaxAngleAxisY = 5;

            return setting_row;
        }
        // GetDefaultSettingRow
        // назначает выбранную настройку на машину
        private void assignToolStripButton_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                // читаем выбранную настройку
                atlantaDataSet.settingRow rowSet =
                    (atlantaDataSet.settingRow)(atlantaDataSet.setting.Rows[SelectedIndex]);
                int idSetting = rowSet.id;

                // определяем список выбранных машин
                DevVehicleControl.GetCheckObjectFromNodeOperation operation =
                    new DevVehicleControl.GetCheckObjectFromNodeOperation();

                TreeList _tree = DevVehicleControl.GetTreeList();
                _tree.NodesIterator.DoOperation(operation);
                List<IVehicle> vehicleList = operation.getCheckVehicles();

                if (vehicleList.Count == 0)
                {
                    XtraMessageBox.Show(Resources.ChoosParameterError, Resources.Parameter);
                    return;
                }

                // ищем выбранные машины в базе
                for (int i = 0; i < vehicleList.Count; i++)
                {
                    IVehicle vehicle = vehicleList[i];
                    int mobitelId = vehicle.Mobitel.Id;

                    foreach (atlantaDataSet.vehicleRow v_row in atlantaDataSet.vehicle)
                    {
                        int mobId = v_row.Mobitel_id;
                        if (mobitelId == mobId)
                        {
                            v_row.setting_id = idSetting; // ставим настройку на машину в базе
                            vehicle.Settings.Id = idSetting; // ставим настройку на машину в дереве
                        } // if
                    } // foreach
                } // for

                vehicleTableAdapter.Update(atlantaDataSet.vehicle);
                atlantaDataSet.AcceptChanges();
                DevVehicleControl devVehicle = DevVehicleControl.GetDevVehicle();
                devVehicle.RefreshTree();
                XtraMessageBox.Show(Resources.SetParameter, Resources.Parameter);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message + " " + ex.InnerException, Resources.Parameter);
                return;
            }
        }
        // assignToolStripButton_ItemClick
        private void barDeleteSetting_ItemClick(object sender, ItemClickEventArgs e)
        {
            //    // сдесь проверяем, не назначена ли удаляемая настройка на какую-нибуть машину
            //    settingTableAdapter.Update(atlantaDataSet.setting);
            //    vehicleTableAdapter.Update(atlantaDataSet.vehicle);
            //    bool flagAssign = false;
            //    atlantaDataSet.settingRow rowSet = (atlantaDataSet.settingRow) (atlantaDataSet.setting.Rows[SelectedIndex]);
            //    int idSetting = rowSet.id;

            //    foreach (atlantaDataSet.vehicleRow v_row in atlantaDataSet.vehicle)
            //    {
            //        if (v_row.setting_id == rowSet.id)
            //        {
            //            flagAssign = true;
            //            break;
            //        } // if
            //    } // foreach

            //    if (flagAssign)
            //    {
            //        XtraMessageBox.Show(Resources.DellParameterError, Resources.Parameter);
            //        return;
            //    }

            //    // удаляем настройку из базы, если она не назначена ни на одну машину
            //    try
            //    {
            //        atlantaDataSet.settingRow row = (atlantaDataSet.settingRow) (atlantaDataSet.setting.Rows[SelectedIndex]);
            //        atlantaDataSet.setting.Rows.Remove(row);
            //        settingTableAdapter.Update(atlantaDataSet.setting);
            //        atlantaDataSet.AcceptChanges();
            //    }
            //    catch (Exception ex)
            //    {
            //        return;
            //    }

            //    gcSettings.SelectedObject = null;
            //    LoadSettings();
            //    object val = repositoryItemComboBox1.Items[0];
            //    barEditItemName.EditValue = val;
            //    _properObj.Index(0);
            //    gcSettings.SelectedObject = _properObj;
        }
        // удаляем настройку из базы
        private void barButtonItem1_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                if (XtraMessageBox.Show("Действительно хотите удалить настройку?", "Удаление настройки",
                        MessageBoxButtons.OKCancel) == DialogResult.OK)
                {
                    // удаляем настройку
                    // сдесь проверяем, не назначена ли удаляемая настройка на какую-нибуть машину
                    settingTableAdapter.Update(atlantaDataSet.setting);
                    vehicleTableAdapter.Update(atlantaDataSet.vehicle);
                    bool flagAssign = false;
                    atlantaDataSet.settingRow rowSet = (atlantaDataSet.settingRow)(atlantaDataSet.setting.Rows[SelectedIndex]);
                    
                    foreach (atlantaDataSet.vehicleRow v_row in atlantaDataSet.vehicle)
                    {
                        if (v_row.setting_id == rowSet.id)
                        {
                            flagAssign = true;
                            break;
                        } // if
                    } // foreach

                    if (flagAssign)
                    {
                        XtraMessageBox.Show(Resources.DellParameterError, Resources.Parameter, MessageBoxButtons.OK);
                        return;
                    }

                    // удаляем настройку из базы, если она не назначена ни на одну машину
                    try
                    {
                        atlantaDataSet.settingRow row = (atlantaDataSet.settingRow)(atlantaDataSet.setting.Rows[SelectedIndex]);
                        int id = row.id;
                        atlantaDataSet.setting.Rows.Remove(row);
                        settingTableAdapter.Update(atlantaDataSet.setting);
                        atlantaDataSet.AcceptChanges();
                        settingTableAdapter.Delete(id);
                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show(ex.Message + "\n" + ex.StackTrace, "Error DevAdmin Form", MessageBoxButtons.OK);
                        return;
                    }

                    gcSettings.SelectedObject = null;
                    LoadSettings();
                    object val = repositoryItemComboBox1.Items[0];
                    barEditItemName.EditValue = val;
                    _properObj.Index(0);
                    gcSettings.SelectedObject = _properObj;
                } // if
            } // try
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message + "\n" + ex.StackTrace, "Error DevAdminForm", MessageBoxButtons.OK);
                return;
            }
        }

        private void bbiLog_ItemClick(object sender, ItemClickEventArgs e)
        {
            UserLog.ViewLog(UserLogTypes.Settings, SelectedIndex);
        }
        // barButtonItem1_ItemClick
    }
    // DevAdminForm
    //=============================================== Nested classes ============================================================
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class Total : GlobalizedObject
    {
        private List<double> _avgFuelRatePerHour = new List<double>();
        private List<int> _fuelrateWithDischarge = new List<int>();
        private int index = 0;
        private int CountRow = 0;

        public Total()
        {
            //TODO:
        }

        public void ClearData()
        {
            _avgFuelRatePerHour.Clear();
            _fuelrateWithDischarge.Clear();
            CountRow = 0;
            index = 0;
        }

        public void Index(int indx)
        {
            CountRow = _avgFuelRatePerHour.Count;
            if ((index > -1) && (index < CountRow))
            {
                index = indx;
            }
            else
            {
                index = 0;
            }
        }
        // Index
        public int Count()
        {
            return _avgFuelRatePerHour.Count;
        }

        public void addAvgFuelRatePerHour(double AvgFuelRatePerHour)
        {
            _avgFuelRatePerHour.Add(AvgFuelRatePerHour);
        }

        public void addFuelrateWithDischarge(int FuelrateWithDischarge)
        {
            _fuelrateWithDischarge.Add(FuelrateWithDischarge);
        }
        //        [DisplayName("Средний расход топлива, л/ч")]
        //        [Category("Параметры алгоритмов ДУТ")]
        //        [Description("Средний расход топлива, л/ч")]
        public double AvgFuelRatePerHour
        {
            get { return _avgFuelRatePerHour[index]; }
            set { _avgFuelRatePerHour[index] = value; }
        }
        //        [DisplayName("Не учитывать сливы в расходе")]
        //       [Category("Параметры алгоритмов ДУТ")]
        //        [Description("Не учитывать сливы в расходе")]
        public int FuelrateWithDischarge
        {
            get { return _fuelrateWithDischarge[index]; }
            set { _fuelrateWithDischarge[index] = value; }
        }

        /// <summary>
        /// Представление в виде строки
        /// </summary>
        public override string ToString()
        {
            return _avgFuelRatePerHour[index].ToString() + "; " + _fuelrateWithDischarge[index].ToString();
        }
    }
    // Total
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class AlgorithmOne : GlobalizedObject
    {
        private List<int> _fuelingEdge = new List<int>();
        private List<int> _fuelingDischarge = new List<int>();
        private List<int> _band = new List<int>();
        private List<TimeSpan> _fuelApproximationTime = new List<TimeSpan>();
        private int index = 0;
        private int CountRow = 0;

        public AlgorithmOne()
        {
            //TODO:
        }

        public void ClearData()
        {
            _fuelingEdge.Clear();
            _fuelingDischarge.Clear();
            _band.Clear();
            _fuelApproximationTime.Clear();
            index = 0;
            CountRow = 0;
        }

        public void Index(int indx)
        {
            CountRow = _band.Count;
            if ((index > -1) && (index < CountRow))
            {
                index = indx;
            }
            else
            {
                index = 0;
            }
        }
        // Index
        public int Count()
        {
            return _band.Count;
        }

        public void addFuelApproximationTime(TimeSpan FuelApproximationTime)
        {
            _fuelApproximationTime.Add(FuelApproximationTime);
        }

        public void addFuelingDischarge(int FuelingDischarge)
        {
            _fuelingDischarge.Add(FuelingDischarge);
        }

        public void addFuelingEdger(int FuelingEdge)
        {
            _fuelingEdge.Add(FuelingEdge);
        }

        public void addBand(int band)
        {
            _band.Add(band);
        }
        // [DisplayName("Порог заправки")]
        //        [Category("Параметры алгоритмов ДУТ")]
        // [Description("Порог заправки")]
        public int FuelingEdge
        {
            get { return _fuelingEdge[index]; }
            set { _fuelingEdge[index] = value; }
        }
        // [DisplayName("Порог слива")]
        //        [Category("Параметры алгоритмов ДУТ")]
        // [Description("Порог слива")]
        public int FuelingDischarge
        {
            get { return _fuelingDischarge[index]; }
            set { _fuelingDischarge[index] = value; }
        }
        // [DisplayName("Полоса сглаживания")]
        //        [Category("Параметры алгоритмов ДУТ")]
        // [Description("Полоса сглаживания")]
        public int band
        {
            get { return _band[index]; }
            set { _band[index] = value; }
        }
        //  [DisplayName("Время определения уровня топлива")]
        //        [Category("Параметры алгоритмов ДУТ")]
        //  [Description("Время определения уровня топлива")]
        public TimeSpan FuelApproximationTime
        {
            get { return _fuelApproximationTime[index]; }
            set { _fuelApproximationTime[index] = value; }
        }

        /// <summary>
        /// Представление в виде строки
        /// </summary>
        public override string ToString()
        {
            return _fuelingEdge[index] + "; " + _fuelingDischarge[index] + "; " + _band[index].ToString() + "; " +
            _fuelApproximationTime[index];
        }
    }
    // AlgorithmOne
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class AlgorithmTwo : GlobalizedObject
    {
        private List<TimeSpan> _timeGetFuelAfterStop = new List<TimeSpan>();
        private List<TimeSpan> _timeGetFuelBeforeMotion = new List<TimeSpan>();
        private int index = 0;
        private int CountRow = 0;

        public AlgorithmTwo()
        {
            //TODO:
        }

        public void ClearData()
        {
            _timeGetFuelAfterStop.Clear();
            _timeGetFuelBeforeMotion.Clear();
            CountRow = 0;
            index = 0;
        }

        public void Index(int indx)
        {
            CountRow = _timeGetFuelAfterStop.Count;
            if ((index > -1) && (index <= CountRow))
            {
                index = indx;
            }
            else
            {
                index = 0;
            }
        }
        // Index
        public int Count()
        {
            return _timeGetFuelAfterStop.Count;
        }

        public void addTimeGetFuelAfterStop(TimeSpan TimeGetFuelAfterStop)
        {
            _timeGetFuelAfterStop.Add(TimeGetFuelAfterStop);
        }

        public void addTimeGetFuelBeforeMotion(TimeSpan TimeGetFuelBeforeMotion)
        {
            _timeGetFuelBeforeMotion.Add(TimeGetFuelBeforeMotion);
        }
        //  [DisplayName("Время стабилизации топлива после остановки")]
        //        [Category("Параметры алгоритмов ДУТ")]
        //  [Description("Время стабилизации топлива после остановки")]
        public TimeSpan TimeGetFuelAfterStop
        {
            get { return _timeGetFuelAfterStop[index]; }
            set { _timeGetFuelAfterStop[index] = value; }
        }
        //  [DisplayName("Время неточности топлива перед началом движения")]
        //        [Category("Параметры алгоритмов ДУТ")]
        //  [Description("Время неточности топлива перед началом движения")]
        public TimeSpan TimeGetFuelBeforeMotion
        {
            get { return _timeGetFuelBeforeMotion[index]; }
            set { _timeGetFuelBeforeMotion[index] = value; }
        }

        /// <summary>
        /// Представление в виде строки
        /// </summary>
        public override string ToString()
        {
            return _timeGetFuelAfterStop[index].ToString() + "; " + _timeGetFuelBeforeMotion[index].ToString();
        }
    }
    // AlgorithmTwo
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class AlgorithmThird : GlobalizedObject
    {
        private List<int> _fuelingMinMaxAlgorithm = new List<int>();
        private int index = 0;
        private int CountRow = 0;

        public AlgorithmThird()
        {
            //TODO:
        }

        public void Index(int indx)
        {
            CountRow = _fuelingMinMaxAlgorithm.Count;
            if ((index > -1) && (index < CountRow))
            {
                index = indx;
            }
            else
            {
                index = 0;
            }
        }
        // Index
        public int Count()
        {
            return _fuelingMinMaxAlgorithm.Count;
        }

        public void ClearData()
        {
            _fuelingMinMaxAlgorithm.Clear();
            CountRow = 0;
            index = 0;
        }

        public void addFuelingMinMaxAlgorithm(int FuelingMinMaxAlgorithm)
        {
            _fuelingMinMaxAlgorithm.Add(FuelingMinMaxAlgorithm);
        }
        //  [DisplayName("Заправки считать по Min/Max")]
        //        [Category("Параметры алгоритмов ДУТ")]
        //  [Description("Применять для вычисления заправок максимальное изменение топлива")]
        public int FuelingMinMaxAlgorithm
        {
            get { return _fuelingMinMaxAlgorithm[index]; }
            set { _fuelingMinMaxAlgorithm[index] = value; }
        }

        /// <summary>
        /// Представление в виде строки
        /// </summary>
        public override string ToString()
        {
            return _fuelingMinMaxAlgorithm[index].ToString();
        }
    }
    // AlgorithmThird
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class Inclinometer : GlobalizedObject
    {
        private List<double> _MaxAngleAxisX = new List<double>();
        private List<double> _MaxAngleAxisY = new List<double>();
        private int index = 0;
        private int CountRow = 0;

        public void Index(int indx)
        {
            CountRow = _MaxAngleAxisX.Count;
            if ((index > -1) && (index < CountRow))
            {
                index = indx;
            }
            else
            {
                index = 0;
            }
        }
        // Index
        public int Count()
        {
            return _MaxAngleAxisX.Count;
        }

        public void ClearData()
        {
            _MaxAngleAxisX.Clear();
            _MaxAngleAxisY.Clear();
            CountRow = 0;
            index = 0;
        }

        public void addMaxAngleAxisX(double newMaxAngleAxisX)
        {
            _MaxAngleAxisX.Add(newMaxAngleAxisX);
        }

        public void addMaxAngleAxisY(double newMaxAngleAxisY)
        {
            _MaxAngleAxisY.Add(newMaxAngleAxisY);
        }

        public double MaxAngleAxisX
        {
            get { return _MaxAngleAxisX[index]; }
            set { _MaxAngleAxisX[index] = value; }
        }

        public double MaxAngleAxisY
        {
            get { return _MaxAngleAxisY[index]; }
            set { _MaxAngleAxisY[index] = value; }
        }

        /// <summary>
        /// Представление в виде строки
        /// </summary>
        public override string ToString()
        {
            return string.Format("X:{0} ; Y:{1}", _MaxAngleAxisX[index], _MaxAngleAxisY[index]);
        }
    }

    public class PropertyObject : GlobalizedObject
    {
        private List<string> _name = new List<string>();
        private List<string> _desc = new List<string>();
        private List<double> _speedMax = new List<double>();
        private List<double> _speedMin = new List<double>();
        private List<double> _accelMax = new List<double>();
        private List<double> _breakMax = new List<double>();
        private List<TimeSpan> _timeBreak = new List<TimeSpan>();
        private List<TimeSpan> _timeBreakMaxPermitted = new List<TimeSpan>();
        private List <TimeSpan> _timeMinimMovingSize = new List<TimeSpan>();
        private List<int> _rotationMain = new List<int>();
        private List<TimeSpan> _czMinCrossingPairTime = new List<TimeSpan>();
        private List<int> _fuelerRadiusFinds = new List<int>();
        private List<TimeSpan> _fuelerMaxTimeStop = new List<TimeSpan>();
        private List<int> _fuelerMinFuelrate = new List<int>();
        private List<int> _fuelerCountInMotion = new List<int>();
        private List<int> _czMinTimeAboutToSwitch = new List<int>();
        public Total _totalDut = new Total();
        public AlgorithmOne _AlgorithmOne = new AlgorithmOne();
        public AlgorithmTwo _AlgorithmTwo = new AlgorithmTwo();
        public AlgorithmThird _AlgorithmThird = new AlgorithmThird();
        public Inclinometer _Inclinometer = new Inclinometer();
        private int index = 0;
        private int CountRow = 0;
        
        public void Index(int indx)
        {
            CountRow = _name.Count;
            if ((index > -1) && (index < CountRow))
            {
                index = indx;
            }
            else
            {
                index = 0;
            }

            _AlgorithmOne.Index(index);
            _AlgorithmTwo.Index(index);
            _AlgorithmThird.Index(index);
            _totalDut.Index(index);
            _Inclinometer.Index(index);
        }
        // Index
        public int Count()
        {
            return _name.Count;
        }

        public PropertyObject()
        {
            //TODO:
        }

        public void ClearData()
        {
            _name.Clear();
            _desc.Clear();
            _speedMax.Clear();
            _speedMin.Clear();
            _accelMax.Clear();
            _breakMax.Clear();
            _timeBreak.Clear();
            _timeBreakMaxPermitted.Clear();
            _timeMinimMovingSize.Clear();
            _rotationMain.Clear();
            _czMinCrossingPairTime.Clear();
            _fuelerRadiusFinds.Clear();
            _fuelerMaxTimeStop.Clear();
            _fuelerMinFuelrate.Clear();
            _fuelerCountInMotion.Clear();
            _totalDut.ClearData();
            _AlgorithmOne.ClearData();
            _AlgorithmTwo.ClearData();
            _AlgorithmThird.ClearData();
            _Inclinometer.ClearData();
            _czMinTimeAboutToSwitch.Clear();
            CountRow = 0;
            index = 0;
        }
        // ClearData
        public void addCzMinCrossingPairTime(TimeSpan CzMinCrossingPairTime)
        {
            _czMinCrossingPairTime.Add(CzMinCrossingPairTime);
        }

        public void addCzMinTimeAboutToSwitch(int CzMinTimeToSwitch)
        {
            _czMinTimeAboutToSwitch.Add(CzMinTimeToSwitch);
        }

        public void addFuelerCountInMotion(int FuelerCountInMotion)
        {
            _fuelerCountInMotion.Add(FuelerCountInMotion);
        }

        public void addFuelerMaxTimeStop(TimeSpan FuelerMaxTimeStop)
        {
            _fuelerMaxTimeStop.Add(FuelerMaxTimeStop);
        }

        public void addFuelerMinFuelrate(int FuelerMinFuelrate)
        {
            _fuelerMinFuelrate.Add(FuelerMinFuelrate);
        }

        public void addDesc(string Desc)
        {
            _desc.Add(Desc);
        }

        public void addName(string Name)
        {
            _name.Add(Name);
        }

        public void addFuelerRadiusFinds(int FuelerRadiusFinds)
        {
            _fuelerRadiusFinds.Add(FuelerRadiusFinds);
        }

        public void addRotationMain(int RotationMain)
        {
            _rotationMain.Add(RotationMain);
        }

        public void addTimeBreak(TimeSpan TimeBreak)
        {
            _timeBreak.Add(TimeBreak);
        }

        public void addTimeBreakMaxPermitted(TimeSpan TimeBreakMaxPermitted)
        {
            _timeBreakMaxPermitted.Add(TimeBreakMaxPermitted);
        }

        public void addTimeMinimMovingSize(TimeSpan TimeMinimMovingSize)
        {
            _timeMinimMovingSize.Add(TimeMinimMovingSize);
        }

        public void addAccelMax(double accelMax)
        {
            _accelMax.Add(accelMax);
        }

        public void addSpeedMax(double speedMax)
        {
            _speedMax.Add(speedMax);
        }

        public void addSpeedMin(double speedMin)
        {
            _speedMin.Add(speedMin);
        }

        public void addBreakMax(double breakMax)
        {
            _breakMax.Add(breakMax);
        }
        // [DisplayName("Название настройки")]
        // [Category("Общие параметры")]
        // [Description("Уникальное название настройки")]
        public string Name
        {
            get { return _name[index]; }
            set { _name[index] = value; }
        }
        //  [DisplayName("Описание настройки")]
        //  [Category("Общие параметры")]
        //  [Description("Описание настройки")]
        public string Desc
        {
            get { return _desc[index]; }
            set { _desc[index] = value; }
        }
        // [DisplayName("Максимальная скорость движения")]
        // [Category("Параметры движения")]
        // [Description("Максимальная допустимая скорость движения, км/ч")]
        public double SpeedMax
        {
            get { return _speedMax[index]; }
            set { _speedMax[index] = value; }
        }
        // [DisplayName("Минимальная скорость движения")]
        // [Category("Параметры движения")]
        // [Description("Минимальная допустимая скорость движения, км/ч")]
        public double SpeedMin
        {
            get { return _speedMin[index]; }
            set { _speedMin[index] = value; }
        }
        // [DisplayName("Предельное ускорение")]
        // [Category("Параметры движения")]
        // [Description("Предельное допустимое ускорение, м/с^2")]
        public double accelMax
        {
            get { return _accelMax[index]; }
            set { _accelMax[index] = value; }
        }
        //  [DisplayName("Предельное торможение")]
        //  [Category("Параметры движения")]
        //  [Description("Предельное допустимое торможение, м/с^2")]
        public double breakMax
        {
            get { return _breakMax[index]; }
            set { _breakMax[index] = value; }
        }
        //  [DisplayName("Минимальное время стоянки")]
        //  [Category("Параметры движения")]
        //  [Description("Минимальное время разрешённой стоянки")]
        public TimeSpan TimeBreak
        {
            get { return _timeBreak[index]; }
            set { _timeBreak[index] = value; }
        }
        // [DisplayName("Максимальное время стоянки")]
        // [Category("Параметры движения")]
        // [Description("Максимальное время разрешённой стоянки")]
        public TimeSpan TimeBreakMaxPermitted
        {
            get { return _timeBreakMaxPermitted[index]; }
            set { _timeBreakMaxPermitted[index] = value; }
        }

        // [DisplayName("Минимальное время движения в начале смены")]
        // [Category("Параметры движения")]
        // [Description("Минимальное время движения в начале смены")]
        public TimeSpan TimeMinimMovingSize
        {
            get { return _timeMinimMovingSize[index]; }
            set { _timeMinimMovingSize[index] = value; }
        }

        // [DisplayName("Минимальное время между двумя пересечениями границ КЗ")]
        // [Category("Параметры отчетов по контрольным зонам")]
        // [Description("Минимальное время между двумя пересечениями границ одной КЗ")]
        public TimeSpan CzMinCrossingPairTime
        {
            get { return _czMinCrossingPairTime[index]; }
            set { _czMinCrossingPairTime[index] = value; }
        }

        // [DisplayName("Параметры отчета по событиям датчиков")]
        // [Category("Параметры отчета по событиям датчиков")]
        // [Description("Минимальное время между двумя переключениями")]
        public int CzMinTimeAboutToSwitch
        {
            get { return _czMinTimeAboutToSwitch[index]; }
            set { _czMinTimeAboutToSwitch[index] = value; }
        }

        //  [DisplayName("Радиус поиска ТС")]
        //  [Category("Параметры отчета топливозаправщика")]
        //  [Description("Радиус поиска заправляемых ТС вокруг топливозаправщика")]
        public int FuelerRadiusFinds
        {
            get { return _fuelerRadiusFinds[index]; }
            set { _fuelerRadiusFinds[index] = value; }
        }
        // [DisplayName("Минимальное время между заправками")]
        // [Category("Параметры отчета топливозаправщика")]
        // [Description("Минимальное время между заправками следующего ТС")]
        public TimeSpan FuelerMaxTimeStop
        {
            get { return _fuelerMaxTimeStop[index]; }
            set { _fuelerMaxTimeStop[index] = value; }
        }
        // [DisplayName("Минимальная величина заправки в отчёте")]
        // [Category("Параметры отчета топливозаправщика")]
        // [Description("Минимальная величина заправки, которая которая будет попадать в отчет")]
        public int FuelerMinFuelrate
        {
            get { return _fuelerMinFuelrate[index]; }
            set { _fuelerMinFuelrate[index] = value; }
        }
        // [DisplayName("Считать заправки в движении")]
        // [Category("Параметры отчета топливозаправщика")]
        // [Description("Подсчет заправок в движении")]
        public int FuelerCountInMotion
        {
            get { return _fuelerCountInMotion[index]; }
            set { _fuelerCountInMotion[index] = value; }
        }
        //  [DisplayName("Общие")]
        //  [Description("Общие параметры алгоритмов ДУТ")]
        //  [Category("Параметры алгоритмов ДУТ")]
        [ReadOnly(true)]
        public Total total
        {
            get { return _totalDut; }
            set { _totalDut = value; }
        }
        //  [DisplayName("Алгоритм 1")]
        //  [Description("Общие параметры алгоритмов ДУТ. Алгоритм 1")]
        //  [Category("Параметры алгоритмов ДУТ")]
        [ReadOnly(true)]
        public AlgorithmOne algorithm1
        {
            get { return _AlgorithmOne; }
            set { _AlgorithmOne = value; }
        }
        // [DisplayName("Алгоритм 2")]
        // [Description("Общие параметры алгоритмов ДУТ. Алгоритм 2")]
        // [Category("Параметры алгоритмов ДУТ")]
        [ReadOnly(true)]
        public AlgorithmTwo algorithm2
        {
            get { return _AlgorithmTwo; }
            set { _AlgorithmTwo = value; }
        }
        // [DisplayName("Алгоритм 3")]
        // [Description("Общие параметры алгоритмов ДУТ. Алгоритм 3")]
        // [Category("Параметры алгоритмов ДУТ")]
        [ReadOnly(true)]
        public AlgorithmThird algorithm3
        {
            get { return _AlgorithmThird; }
            set { _AlgorithmThird = value; }
        }
        //[ReadOnly(true)]
        public Inclinometer Inclinometer
        {
            get { return _Inclinometer; }
            set { _Inclinometer = value; }
        }
    }
    // PropertyObject
    // ==========================Классы локализации Property Grid==========================================================
    public class GlobalizedObject : ICustomTypeDescriptor
    {
        private PropertyDescriptorCollection globalizedProps;

        public string GetClassName()
        {
            return TypeDescriptor.GetClassName(this, true);
        }

        public AttributeCollection GetAttributes()
        {
            return TypeDescriptor.GetAttributes(this, true);
        }

        public String GetComponentName()
        {
            return TypeDescriptor.GetComponentName(this, true);
        }

        public TypeConverter GetConverter()
        {
            return TypeDescriptor.GetConverter(this, true);
        }

        public EventDescriptor GetDefaultEvent()
        {
            return TypeDescriptor.GetDefaultEvent(this, true);
        }

        public PropertyDescriptor GetDefaultProperty()
        {
            return TypeDescriptor.GetDefaultProperty(this, true);
        }

        public object GetEditor(Type editorBaseType)
        {
            return TypeDescriptor.GetEditor(this, editorBaseType, true);
        }

        public EventDescriptorCollection GetEvents(Attribute[] attributes)
        {
            return TypeDescriptor.GetEvents(this, attributes, true);
        }

        public EventDescriptorCollection GetEvents()
        {
            return TypeDescriptor.GetEvents(this, true);
        }
        // Called to get the properties of a type
        public PropertyDescriptorCollection GetProperties(Attribute[] attributes)
        {
            if (globalizedProps == null)
            {
                // Get the collection of properties
                PropertyDescriptorCollection baseProps = TypeDescriptor.GetProperties(this, attributes, true);
                globalizedProps = new PropertyDescriptorCollection(null);

                // For each property use a property descriptor of our own that is able to be globalized
                foreach (PropertyDescriptor oProp in baseProps)
                {
                    globalizedProps.Add(new GlobalizedPropertyDescriptor(oProp));
                }
            } // if

            return globalizedProps;
        }
        // GetProperties
        public PropertyDescriptorCollection GetProperties()
        {
            // Only do once
            if (globalizedProps == null)
            {
                // Get the collection of properties
                PropertyDescriptorCollection baseProps = TypeDescriptor.GetProperties(this, true);
                globalizedProps = new PropertyDescriptorCollection(null);

                // For each property use a property descriptor of our own that is able to be globalized
                foreach (PropertyDescriptor oProp in baseProps)
                {
                    globalizedProps.Add(new GlobalizedPropertyDescriptor(oProp));
                }
            } // if
            return globalizedProps;
        }
        // GetProperties
        public object GetPropertyOwner(PropertyDescriptor pd)
        {
            return this;
        }
    }
    // GlobalizedObject
    // GlobalizedPropertyDescriptor enhances the base class bay obtaining the display name for a property
    // from the resource
    public class GlobalizedPropertyDescriptor : PropertyDescriptor
    {
        private PropertyDescriptor basePropertyDescriptor;
        private String localizedName = "";
        private String localizedDescription = "";
        private String localizedCategory = "";
        // путь к ресурсам проекта (приложения)
        private string pathToResources = "TrackControl.Properties.Resources";

        public GlobalizedPropertyDescriptor(PropertyDescriptor basePropertyDescriptor)
            : base(basePropertyDescriptor)
        {
            this.basePropertyDescriptor = basePropertyDescriptor;
        }

        public override bool CanResetValue(object component)
        {
            return basePropertyDescriptor.CanResetValue(component);
        }

        public override Type ComponentType
        {
            get { return basePropertyDescriptor.ComponentType; }
        }

        public override string DisplayName
        {
            get
            {
                // First lookup the property if GlobalizedPropertyAttribute instances are available
                // If yes, then try to get resource table name and display name id from that attribute
                string tableName = "";
                string displayName = "";

                foreach (Attribute oAttrib in this.basePropertyDescriptor.Attributes)
                {
                    if (oAttrib.GetType().Equals(typeof(GlobalizedPropertyAttribute)))
                    {
                        displayName = ((GlobalizedPropertyAttribute)oAttrib).Name;
                        tableName = ((GlobalizedPropertyAttribute)oAttrib).Table;
                    }
                } // foreach

                // If no resource table specified by attribute, then build it itself by using namespace and class name
                if (tableName.Length == 0)
                    // tableName = basePropertyDescriptor.ComponentType.Namespace + "." +
                    //             basePropertyDescriptor.ComponentType.Name;
                    tableName = pathToResources;

                // If no display name id is specified by attribute, then construct it by using default display name (usually the property name)
                if (displayName.Length == 0)
                    displayName = this.basePropertyDescriptor.DisplayName;

                // Now use table name and display name id to access the resources.  
                ResourceManager rm = new ResourceManager(tableName, basePropertyDescriptor.ComponentType.Assembly);

                // Get the string from the resources. 
                // If this fails, then use default display name (usually the property name) 
                string s = rm.GetString(displayName);
                this.localizedName = (s != null) ? s : this.basePropertyDescriptor.DisplayName;

                return this.localizedName;
            } // get
        }
        // DisplayName
        public override string Description
        {
            get
            {
                // First lookup the property if there are GlobalizedPropertyAttribute instances are available
                // If yes, try to get resource table name and display name id from that attribute
                string tableName = "";
                string displayName = "";

                foreach (Attribute oAttrib in this.basePropertyDescriptor.Attributes)
                {
                    if (oAttrib.GetType().Equals(typeof(GlobalizedPropertyAttribute)))
                    {
                        displayName = ((GlobalizedPropertyAttribute)oAttrib).Description;
                        tableName = ((GlobalizedPropertyAttribute)oAttrib).Table;
                    }
                } // foreach

                // If no resource table specified by attribute, then build it itself by using namespace and class name.
                if (tableName.Length == 0)
                    //tableName = basePropertyDescriptor.ComponentType.Namespace + "." + basePropertyDescriptor.ComponentType.Name;
                    tableName = pathToResources;

                // If no display name id is specified by attribute, then construct it by using default display name (usually the property name) 
                if (displayName.Length == 0)
                    displayName = this.basePropertyDescriptor.DisplayName + "Description";

                // Now use table name and display name id to access the resources.  
                ResourceManager rm = new ResourceManager(tableName, basePropertyDescriptor.ComponentType.Assembly);

                // Get the string from the resources. 
                // If this fails, then use default empty string indictating 'no description' 
                string s = rm.GetString(displayName);
                this.localizedDescription = (s != null) ? s : "";

                return this.localizedDescription;
            } // get
        }
        // Description
        public override string Category
        {
            get
            {
                // First lookup the property if there are GlobalizedPropertyAttribute instances are available
                // If yes, try to get resource table name and display name id from that attribute
                string tableName = "";
                string displayName = "";

                foreach (Attribute oAttrib in this.basePropertyDescriptor.Attributes)
                {
                    if (oAttrib.GetType().Equals(typeof(GlobalizedPropertyAttribute)))
                    {
                        displayName = ((GlobalizedPropertyAttribute)oAttrib).Description;
                        tableName = ((GlobalizedPropertyAttribute)oAttrib).Table;
                    }
                } // foreach

                // If no resource table specified by attribute, then build it itself by using namespace and class name.
                if (tableName.Length == 0)
                    //tableName = basePropertyDescriptor.ComponentType.Namespace + "." + basePropertyDescriptor.ComponentType.Name;
                    tableName = pathToResources;

                // If no display name id is specified by attribute, then construct it by using default display name (usually the property name) 
                if (displayName.Length == 0)
                    displayName = this.basePropertyDescriptor.DisplayName + "Category";

                // Now use table name and display name id to access the resources.  
                ResourceManager rm = new ResourceManager(tableName, basePropertyDescriptor.ComponentType.Assembly);

                // Get the string from the resources. 
                // If this fails, then use default empty string indictating 'no description' 
                string s = rm.GetString(displayName);
                this.localizedCategory = (s != null) ? s : "";

                return this.localizedCategory;
            } // get
        }
        // Category
        public override object GetValue(object component)
        {
            return this.basePropertyDescriptor.GetValue(component);
        }

        public override bool IsReadOnly
        {
            get { return this.basePropertyDescriptor.IsReadOnly; }
        }

        public override string Name
        {
            get { return this.basePropertyDescriptor.Name; }
        }

        public override Type PropertyType
        {
            get { return this.basePropertyDescriptor.PropertyType; }
        }

        public override void ResetValue(object component)
        {
            this.basePropertyDescriptor.ResetValue(component);
        }

        public override bool ShouldSerializeValue(object component)
        {
            return this.basePropertyDescriptor.ShouldSerializeValue(component);
        }

        public override void SetValue(object component, object value)
        {
            this.basePropertyDescriptor.SetValue(component, value);
        }
    }
    // GlobalizedPropertyDescriptor
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public class GlobalizedPropertyAttribute : Attribute
    {
        private String resourceName = "";
        private String resourceDescription = "";
        private String resourceCategory = "";
        private String resourceTable = "";

        public GlobalizedPropertyAttribute(String name)
        {
            resourceName = name;
        }

        public String Name
        {
            get { return resourceName; }
            set { resourceName = value; }
        }

        public String Description
        {
            get { return resourceDescription; }
            set { resourceDescription = value; }
        }

        public String Category
        {
            get { return resourceCategory; }
            set { resourceCategory = value; }
        }

        public String Table
        {
            get { return resourceTable; }
            set { resourceTable = value; }
        }
    }
    // GlobalizedPropertyAttribute
}
// TrackControl.Setting
