﻿namespace TrackControl.Setting
{
    partial class SettingSensorUser
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose( bool disposing )
        {
            if( disposing && ( components != null ) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.mobitelsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sensorsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sensoralgorithmsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sensorcoefficientBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.relationalgorithmsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.vehicleBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.mobitelsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sensorsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sensoralgorithmsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sensorcoefficientBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.relationalgorithmsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vehicleBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // mobitelsBindingSource
            // 
            this.mobitelsBindingSource.DataMember = "mobitels";
            this.mobitelsBindingSource.DataSource = typeof(LocalCache.atlantaDataSet);
            this.mobitelsBindingSource.Sort = "Mobitel_ID";
            // 
            // sensorsBindingSource
            // 
            this.sensorsBindingSource.DataMember = "mobitels_sensors";
            this.sensorsBindingSource.DataSource = this.mobitelsBindingSource;
            this.sensorsBindingSource.Filter = "Length <> 1";
            this.sensorsBindingSource.Sort = "StartBit ASC, Name ASC";
            // 
            // sensoralgorithmsBindingSource
            // 
            this.sensoralgorithmsBindingSource.DataMember = "sensoralgorithms";
            this.sensoralgorithmsBindingSource.DataSource = typeof(LocalCache.atlantaDataSet);
            // 
            // sensorcoefficientBindingSource
            // 
            this.sensorcoefficientBindingSource.DataMember = "sensors_sensorcoefficient";
            this.sensorcoefficientBindingSource.DataSource = this.sensorsBindingSource;
            // 
            // relationalgorithmsBindingSource
            // 
            this.relationalgorithmsBindingSource.DataMember = "sensors_relationalgorithms";
            this.relationalgorithmsBindingSource.DataSource = this.sensorsBindingSource;
            // 
            // vehicleBindingSource
            // 
            this.vehicleBindingSource.DataMember = "vehicle";
            this.vehicleBindingSource.DataSource = typeof(LocalCache.atlantaDataSet);
            this.vehicleBindingSource.Sort = "Mobitel_id";
            // 
            // SettingSensorUser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "SettingSensorUser";
            this.Size = new System.Drawing.Size(268, 142);
            ((System.ComponentModel.ISupportInitialize)(this.mobitelsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sensorsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sensoralgorithmsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sensorcoefficientBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.relationalgorithmsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vehicleBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.BindingSource mobitelsBindingSource;
        private System.Windows.Forms.BindingSource sensorsBindingSource;
        private System.Windows.Forms.BindingSource sensoralgorithmsBindingSource;
        private System.Windows.Forms.BindingSource sensorcoefficientBindingSource;
        private System.Windows.Forms.BindingSource relationalgorithmsBindingSource;
        private System.Windows.Forms.BindingSource vehicleBindingSource;
    }
}
