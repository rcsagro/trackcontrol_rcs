using System;
using System.Drawing;
using System.Diagnostics;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using System.Collections;
using LocalCache;
using TrackControl.General.Core;
using DevExpress.XtraEditors;
using TrackControl.Properties;
using DevExpress.XtraPrinting;
using BaseReports;
using BaseReports.Procedure;
using BaseReports.Properties;
using TrackControl.General;
using TrackControl.Reports;
using BaseReports.ReportsDE;
using BaseReports.RFID;
using TrackControl.Vehicles;
using TrackControl.Reports.Graph;
using DevExpress.XtraBars;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraPrinting.Drawing;
using DevExpress.XtraExport;
using DevExpress.XtraReports;
using DevExpress.XtraReports.UI;
using DevExpress.XtraPrinting.Native;
using DevExpress.XtraPrinting.Control;
using DevExpress.XtraPrinting.Preview;
using DevExpress.XtraPrintingLinks;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views;
using DevExpress.XtraGrid.Views.Grid;
using System.ComponentModel;
using System.Text;
using Report;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Data.OleDb;
using Agro;

namespace TrackControl.Setting
{
    public partial class ExcelForm : Form
    {
        public class reportExls
        {
            private double uservalue;
            private double sensorvalue;

            public reportExls(double uservalue, double sensorvalue)
            {
                this.uservalue = uservalue;
                this.sensorvalue = sensorvalue;
            } // reportExls

            public double UserValue
            {
                get { return uservalue; }
            }

            public double SensorValue
            {
                get { return sensorvalue; }
            }
        } // reportExls

        class StartIE
        {
            public StartIE( string msg )
            {
                XtraMessageBox.Show( msg + "\n" + Resources.DriverOfficeFileError, Resources.ErrorCalibration, MessageBoxButtons.OK );
                ProcessStartInfo startInfo = new ProcessStartInfo( "IExplore.exe" );
                startInfo.WindowStyle = ProcessWindowStyle.Normal;
                startInfo.Arguments = "http://www.microsoft.com/en-us/download/details.aspx?id=23734";
                Process.Start( startInfo );
            }
        }

        private ReportBase<reportExls, int> ReportingExcel;
        private List<string> listNumCol;
        private BindingSource bs;
        private int sens_id = -5;
        private int isTypeExcel = -1;
        private const int MICROSOFT_EXCEL_PRESENT = 0; // Microsoft Office Excel program
        private const int DRIVER_EXCEL_PRESENT = -1; // 2007 Office System Driver: Data Connectivity Components
        private string strExcelProvider = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"{1};HDR=YES\";";

        public ExcelForm()
        {
            InitializeComponent();
            init();
            listNumCol = new List<string>();
            ReportingExcel =
                new ReportBase<reportExls, int>(Controls, compositeLink1, printingSystem1, dgv, listNumCol);

            isExcelExist();
        }

        private void ExcelForm_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'sensorsSet.sensorcoefficient' table. You can move, or remove it, as needed.
            // this.sensorcoefficientTableAdapter.Fill( this.sensorsSet.sensorcoefficient );
            // TODO: This line of code loads data into the 'sensorsSet.sensorcoefficient' table. You can move, or remove it, as needed.
            // this.sensorcoefficientTableAdapter.Fill( this.sensorsSet.sensorcoefficient );
        }

        private void isExcelExist()
        {
            isTypeExcel = -1;

            try
            {
                Excel ex = null;
                ex = new Excel();

                if( ex != null )
                {
                    isTypeExcel = 0; // Microsoft Office Excel program ������������ �� ��������� ����������
                    ex.Dispose();
                    return;
                }
            }
            catch(Exception e)
            {
                // to do
            }
        } // isExcelExist

        public BindingSource bindingSource
        {
            set
            {
                if (value == null) return;
                bs = value;
                dgv.DataSource = bs;
                bindingNavigator.BindingSource = bs;
            }
        }

        public int sensorIdentificator
        {
            set { sens_id = value; }
        }

        public void DraftWithMicrosoftExsel()
        {
            // C1XLBook book = new C1XLBook();

            SaveFileDialog dlg = new SaveFileDialog();
            dlg.OverwritePrompt = true;
            dlg.CreatePrompt = true;
            dlg.RestoreDirectory = false;
            dlg.CheckFileExists = false;
            dlg.CheckPathExists = false;
            dlg.Filter = "Microsoft Excel document (*.xls)|*.xls";
            dlg.Title = Resources.SaveCoeffTable; // Resources.Save;
            dlg.FileName = "Calibration.xls";

            if( dlg.ShowDialog() == DialogResult.OK )
            {
                // book.Save(dlg.FileName);

                // XLSheet sheet = book.Sheets[0];
                // sheet.Name = "Calibration";

                // sheet[0, 0].Value = Resources.ExcelForm_Custom;
                // sheet[0, 1].Value = Resources.ExcelForm_Measured;

                // book.Save(dlg.FileName);
                // System.Diagnostics.Process.Start(dlg.FileName);

                listNumCol.Add( "dataGridViewTextBoxColumn2" );
                listNumCol.Add( "dataGridViewTextBoxColumn3" );
                ReportingExcel.CreateBindDataList();
                ReportingExcel.AddDataToBindList( new reportExls( 0, 0 ) );
                ReportingExcel.CreateElementOtherReport();
                ReportingExcel.ExportDataToExcel( dlg.FileName );
                ReportingExcel.DeleteData();
            } // if
        } // DraftWithMicrosoftExsel

        public void DraftWithDriverExcel()
        {
            try
            {
                bool b2007 = false; // Microsoft Office 2007 ��� 2003
                DataTable dtData = null;
                SaveFileDialog dlg = new SaveFileDialog();
                dlg.RestoreDirectory = false;
                dlg.CheckFileExists = false;
                dlg.CheckPathExists = false;
                dlg.Filter = "Office Excel file(*.xls)|*.xls";
                dlg.Title = Resources.SaveCoeffTable;
                dlg.DefaultExt = "xls";
                dlg.FileName = "Calibration0";

                if( dlg.ShowDialog() == DialogResult.OK )
                {
                    if( File.Exists( dlg.FileName ) )
                        File.Delete( dlg.FileName );

                    dtData = new DataTable();
                    Random rndm = new Random();
                    int num = rndm.Next( 50 );
                    dtData.TableName = "Calibration" + Convert.ToString( num );
                    DataColumn dCol0 = new DataColumn( Resources.UsersColumn );
                    dtData.Columns.Add( dCol0 );
                    DataColumn dCol1 = new DataColumn( Resources.MeasuredColumn );
                    dtData.Columns.Add( dCol1 );

                    listNumCol.Add( "dataGridViewTextBoxColumn2" );
                    listNumCol.Add( "dataGridViewTextBoxColumn3" );

                    for( int i = 1; i < 11; i++ )
                    {
                        double uservalue = i;
                        double sensorvalue = i;

                        DataRow rw = dtData.NewRow();
                        rw[Resources.UsersColumn] = uservalue;
                        rw[Resources.MeasuredColumn] = sensorvalue;
                        dtData.Rows.Add( rw );
                    } // for

                    string sConnStr = String.Format( strExcelProvider, dlg.FileName, b2007 ? "Excel 12.0 Xml" : "Excel 8.0" );

                    using( OleDbConnection odcConn = new OleDbConnection( sConnStr ) )
                    {
                        odcConn.Open();
                        using( OleDbCommand odcComm = new OleDbCommand() { Connection = odcConn } )
                        {
                            // �������� �������
                            odcComm.CommandText = GenerateSqlStatementCreateTable( dtData );
                            odcComm.ExecuteNonQuery();

                            DataRow dr;
                            OleDbParameter odpParam;

                            // ���������� ������ �������� ����� �� ���������� (� �������� ����������)
                            string sColumns, sParameters;
                            GenerateColumnsString( dtData, out sColumns, out sParameters );

                            for( int i = 0; i < dtData.Rows.Count; i++ )
                            {
                                dr = dtData.Rows[i];
                                // ������������� �������� ��� INSERT
                                odcComm.Parameters.Clear();

                                for( int j = 0; j < dtData.Columns.Count; j++ )
                                {
                                    odpParam = new OleDbParameter();
                                    odpParam.ParameterName = "@p" + j;

                                    odpParam.Value = dr.IsNull( j ) ? DBNull.Value : dr[j];

                                    odcComm.Parameters.Add( odpParam );
                                } // for

                                odcComm.CommandText = string.Format( "INSERT INTO {0} ({1}) VALUES ({2})", dtData.TableName, sColumns, sParameters );
                                odcComm.ExecuteNonQuery();
                            } // for

                            odcConn.Close();
                        } // using 
                    } // using
                } // if
            } // try
            catch( InvalidOperationException e )
            {
                StartIE ie = new StartIE( e.Message );
            }
            catch( OleDbException e )
            {
                XtraMessageBox.Show( e.Message, Resources.ErrorCalibration, MessageBoxButtons.OK );
            }
        } // DraftWithDriverExcel

        private void bDraft_Click(object sender, EventArgs e)
        {
            if( isTypeExcel == MICROSOFT_EXCEL_PRESENT )
            {
                DraftWithMicrosoftExsel();
            }
            else if( isTypeExcel == DRIVER_EXCEL_PRESENT )
            {
                DraftWithDriverExcel();
            }
        }

        /// <summary>
        /// ����������� �� ���������� �������� ��� ������� �����������
        /// </summary>
        public virtual int ExcelColumnsDictionary
        {
            get { return 2; }
        }

        /// <summary>
        ///  ����������� Excel-���� � DataTable
        ///  ������� ��� �������� ������
        ///  ������ ���� � Excel-�����
        ///  SQL - ������. ���������� $SHEETS$ ��� ������� �� ���� ������, ���� �����
        /// </summary>
        public void OpenFileWithExcelDriver()
        {
            bs.EndEdit();
            try
            {
                OpenFileDialog dlg = new OpenFileDialog();
                dlg.RestoreDirectory = false;
                dlg.CheckFileExists = false;
                dlg.CheckPathExists = false;
                dlg.Filter = "Office Excel document(*.xls;*.xlsx)|*.xls;*.xlsx|All files (*.*)|*.*";
                dlg.Title = Resources.OpenCoeffTable;

                if( dlg.ShowDialog() == DialogResult.OK )
                {
                    DataSet dataSet = new DataSet();
                    DataTable tableData = new DataTable();

                    // ��� Excel 2007 ��� ��� Excel 2003
                    string sConnStrExcls = String.Format( strExcelProvider, dlg.FileName, dlg.FileName.EndsWith( ".xlsx" ) ? "Excel 12.0 Xml" : "Excel 8.0" );

                    using( OleDbConnection odcConn = new OleDbConnection( sConnStrExcls ) )
                    {
                        odcConn.Open();
                        DataTable schemaTable = odcConn.GetOleDbSchemaTable( OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" } );
                        string sheet1 = ( string )schemaTable.Rows[0].ItemArray[2];
                                              
                        string sRequest = String.Format( "SELECT * FROM [{0}]", sheet1 );
                        OleDbCommand odcComm = new OleDbCommand( sRequest, odcConn );
                        using( OleDbDataAdapter oddaAdapter = new OleDbDataAdapter( odcComm ) )
                            oddaAdapter.Fill( dataSet );

                        odcConn.Close();
                    } // using

                    tableData = dataSet.Tables[0];

                    if( tableData.Rows.Count <= 0 )
                    {
                        XtraMessageBox.Show( Resources.ErrorDocCalibration, Resources.ErrorCalibration, MessageBoxButtons.OK );
                        return;
                    }

                    Dictionary<double, double> excel = new Dictionary<double, double>( tableData.Rows.Count );

                    double uval_prev = 0, sval_prev = 0;

                    for( int i = 0; i < tableData.Rows.Count; i++ )
                    {
                        DataRow row = tableData.Rows[i];
                        double uval = Convert.ToDouble( row[0] );
                        double sval = Convert.ToDouble( row[1] );

                        if( uval_prev > uval )
                            throw new Exception( Resources.NotCorrectValue + " [" + uval + "] " + Resources.NumStr + " " + Convert.ToString( i + 2 ) );
                        else
                            uval_prev = uval;

                        if( sval_prev > sval )
                            throw new Exception( Resources.NotCorrectValue + " [" + sval + "] " + Resources.NumStr + " " + Convert.ToString( i + 2 ) );
                        else
                            sval_prev = sval;
                    }

                    for( int i = 0; i < tableData.Rows.Count; i++ )
                    {
                        DataRow row = tableData.Rows[i];
                        double uval = Convert.ToDouble( row[0] );
                        double sval = Convert.ToDouble( row[1] );

                        if( !excel.ContainsKey( uval ) )
                        {
                            excel.Add( uval, sval );
                        }
                    }

                    PrepareContext( bs, "UserValue", excel.Keys );

                    foreach( DataRowView rowV in bs.List )
                    {
                        double uval = Convert.ToDouble( rowV["UserValue"] );
                        object sval = excel[uval];

                        if( !rowV["SensorValue"].Equals( sval ) )
                        {
                            rowV["SensorValue"] = sval;
                        }
                    } // foreach
                } // if
            } // try
            catch( InvalidOperationException e )
            {
                StartIE ie = new StartIE( e.Message );
            }
            catch( OleDbException e )
            {
                XtraMessageBox.Show( e.Message, Resources.ErrorCalibration, MessageBoxButtons.OK );
            }
        } // OpenFileWithExcelDriver

        public void OpenWithMisrosoftExcel()
        {
            bs.EndEdit();
            try
            {
                OpenFileDialog dlg = new OpenFileDialog();
                dlg.RestoreDirectory = false;
                dlg.CheckFileExists = false;
                dlg.CheckPathExists = false;
                dlg.Filter = "Office Excel document(*.xls;*.xlsx)|*.xls;*.xlsx|All files (*.*)|*.*";
                dlg.Title = Resources.OpenCoeffTable;

                if( dlg.ShowDialog() == DialogResult.OK )
                {
                    // --- ��� �������� �� DevExpress ---
                    // C1XLBook book = new C1XLBook();
                    // book.Load(dlg.FileName, true);
                    // if (book.Sheets.Count == 0)
                    //      throw new Exception("Wrong file format.");

                    // XLSheet sheet = book.Sheets[0]; // ����� ���������� ���������� excel - ����

                    // if (sheet.Rows.Count < 1 || sheet.Columns.Count < 2 
                    //      || !sheet[0, 0].Value.ToString().ToUpper().Trim().Equals(Resources.ExcelForm_Custom.ToUpper())
                    //      || !sheet[0, 1].Value.ToString().ToUpper().Trim().Equals(Resources.ExcelForm_Measured.ToUpper()))
                    //          throw new Exception("Wrong file format.");

                    string[,] cellValues = null;
                    int cols = 0;
                    int rows = 0;
                    bool flagWorDocument = false;

                    using( Excel ex = new Excel() )
                    {
                        if( ex.OpenDocument( dlg.FileName ) )
                        {
                            cols = Math.Min( ex.ColumnMax(), ExcelColumnsDictionary );
                            rows = (int)ex.RowMax() - 1;
                            cellValues = new string[rows, cols];
                            int k = 0;
                            int m = 0;
                            flagWorDocument = true;
                            for( int i = 2; i <= ( rows + 1 ); i++ )
                            {
                                for( int j = 0; j < cols; j++ )
                                {
                                    string range = ex.ParseColNum( j ) + i.ToString();
                                    cellValues[k, m] = ex.GetValue( range );
                                    m++;
                                } // for
                                k++;
                                m = 0;
                            } // for
                            ex.CloseDocument();
                        } // if
                    } // using

                    if( !flagWorDocument )
                    {
                        XtraMessageBox.Show( Resources.ErrorDocCalibration, Resources.ErrorCalibration, MessageBoxButtons.OK );
                        return;
                    }

                    Dictionary<double, double> excel = new Dictionary<double, double>( rows );

                    double uval_prev = 0, sval_prev = 0;

                    for( int i = 0; i < rows; i++ )
                    {
                        double uval = Convert.ToDouble( cellValues[i, 0] );
                        double sval = Convert.ToDouble( cellValues[i, 1] );

                        if( uval_prev > uval )
                            throw new Exception( Resources.NotCorrectValue + " [" + uval + "] " + Resources.NumStr + " " + Convert.ToString( i + 2 ) );
                        else
                            uval_prev = uval;

                        if( sval_prev > sval )
                            throw new Exception( Resources.NotCorrectValue + " [" + sval + "] " + Resources.NumStr + " " + Convert.ToString( i + 2 ) );
                        else
                            sval_prev = sval;
                    }

                    for( int i = 0; i < rows; i++ )
                    {
                        double uval = Convert.ToDouble( cellValues[i, 0] );
                        double sval = Convert.ToDouble( cellValues[i, 1] );

                        if( !excel.ContainsKey( uval ) )
                        {
                            excel.Add( uval, sval );
                        }
                    }

                    PrepareContext( bs, "UserValue", excel.Keys );

                    foreach( DataRowView rowV in bs.List )
                    {
                        double uval = Convert.ToDouble( rowV["UserValue"] );
                        object sval = excel[uval];
                        if( !rowV["SensorValue"].Equals( sval ) )
                        {
                            rowV["SensorValue"] = sval;
                        }
                    }
                }
            }
            catch( Exception ex )
            {
                XtraMessageBox.Show( ex.Message, Resources.ErrorExcelTable, MessageBoxButtons.OK );
            }
        } // OpenWithMisrosoftExcel

        private void bLoad_Click(object sender, EventArgs e)
        {
            if( isTypeExcel == MICROSOFT_EXCEL_PRESENT )
            {
                OpenWithMisrosoftExcel();
            }
            else if( isTypeExcel == DRIVER_EXCEL_PRESENT )
            {
                OpenFileWithExcelDriver();
            }
        } // bLoad_Click

        /// <summary>
        /// ������ �� Excel � ���������� ����� ������
        /// </summary>
        /// <param name="CellValues"></param>
        public virtual void AddFromExcel(params string[] cell_values)
        {
            // to do
        }

        public void SaveAsWithExcel()
        {
            bs.EndEdit();
            try
            {
                // C1XLBook book = new C1XLBook();

                SaveFileDialog dlg = new SaveFileDialog();
                dlg.RestoreDirectory = false;
                dlg.CheckFileExists = false;
                dlg.CheckPathExists = false;
                dlg.Filter = "Office Excel Book(*.xls)|*.xls";
                dlg.Title = Resources.SaveAsTableToExcel;
                dlg.FileName = "Calibration.xls";

                if( dlg.ShowDialog() == DialogResult.OK )
                {
                    // book.Save(dlg.FileName);

                    // XLSheet sheet = book.Sheets[0];
                    // sheet.Name = "Calibration";
                    // sheet[0, 0].Value = Resources.ExcelForm_Custom;
                    // sheet[0, 1].Value = Resources.ExcelForm_Measured;

                    // int i = 1;

                    listNumCol.Add( "dataGridViewTextBoxColumn2" );
                    listNumCol.Add( "dataGridViewTextBoxColumn3" );
                    ReportingExcel.CreateBindDataList();

                    foreach( DataRowView rowV in bs.List )
                    {
                        //  sheet[i, 0].Value = Convert.ToDouble(rowV["UserValue"]);
                        //  sheet[i, 1].Value = Convert.ToDouble(rowV["Sensorvalue"]);
                        //  i++;
                        double uservalue = Convert.ToDouble( rowV["UserValue"] );
                        double sensorvalue = Convert.ToDouble( rowV["Sensorvalue"] );

                        ReportingExcel.AddDataToBindList( new reportExls( uservalue, sensorvalue ) );
                    } // foreach

                    ReportingExcel.CreateElementOtherReport();
                    ReportingExcel.ExportDataToExcel( dlg.FileName );
                    ReportingExcel.DeleteData();

                    // book.Save(dlg.FileName);
                    // aketner - 09.11.2012
                } // if
            } // try
            catch( Exception ex )
            {
                XtraMessageBox.Show( ex.StackTrace, Resources.ErrorSaveExcel );
            }
        } // SaveAsWithExcel

        /// ����������� dataTable � Excel-����
        /// Excel 2003 ��� �����
        public void SaveAsWithDriverExcel()
        {
            bs.EndEdit();

            try
            {
                bool b2007 = false; // Microsoft Office 2007 ��� 2003
                DataTable dtData = null;
                SaveFileDialog dlg = new SaveFileDialog();
                dlg.OverwritePrompt = false;
                dlg.CreatePrompt = false;
                dlg.RestoreDirectory = false;
                dlg.CheckFileExists = false;
                dlg.CheckPathExists = false;
                dlg.DefaultExt = "xls";
                dlg.Filter = "Office Excel file(*.xls)|*.xls";
                dlg.Title = Resources.SaveAsTableToExcel;
                dlg.FileName = "Calibration";

                if( dlg.ShowDialog() == DialogResult.OK )
                {
                    if(File.Exists(dlg.FileName))
                        File.Delete(dlg.FileName);

                    dtData = new DataTable();
                    dtData.TableName = "Calibration";
                    DataColumn dCol0 = new DataColumn( Resources.UsersColumn );
                    dtData.Columns.Add( dCol0 );
                    DataColumn dCol1 = new DataColumn( Resources.MeasuredColumn );
                    dtData.Columns.Add( dCol1 );

                    listNumCol.Add( "dataGridViewTextBoxColumn2" );
                    listNumCol.Add( "dataGridViewTextBoxColumn3" );

                    foreach( DataRowView rowV in bs.List )
                    {
                        double uservalue = Convert.ToDouble( rowV["UserValue"] );
                        double sensorvalue = Convert.ToDouble( rowV["Sensorvalue"] );

                        DataRow rw = dtData.NewRow();
                        rw[Resources.UsersColumn] = uservalue;
                        rw[Resources.MeasuredColumn] = sensorvalue;
                        dtData.Rows.Add( rw );
                    } // foreach

                    string sConnStr = String.Format( strExcelProvider, dlg.FileName, b2007 ? "Excel 12.0 Xml" : "Excel 8.0" );

                    using( OleDbConnection odcConn = new OleDbConnection( sConnStr ) )
                    {
                        odcConn.Open();
                        using( OleDbCommand odcComm = new OleDbCommand() { Connection = odcConn } )
                        {
                            // �������� �������
                            odcComm.CommandText = GenerateSqlStatementCreateTable( dtData );
                            odcComm.ExecuteNonQuery();

                            DataRow dr;
                            OleDbParameter odpParam;

                            // ���������� ������ �������� ����� �� ���������� (� �������� ����������)
                            string sColumns, sParameters;
                            GenerateColumnsString( dtData, out sColumns, out sParameters );

                            for( int i = 0; i < dtData.Rows.Count; i++ )
                            {
                                dr = dtData.Rows[i];
                                // ������������� �������� ��� INSERT
                                odcComm.Parameters.Clear();

                                for( int j = 0; j < dtData.Columns.Count; j++ )
                                {
                                    odpParam = new OleDbParameter();
                                    odpParam.ParameterName = "@p" + j;

                                    odpParam.Value = dr.IsNull( j ) ? DBNull.Value : dr[j];

                                    odcComm.Parameters.Add( odpParam );
                                } // for

                                odcComm.CommandText = string.Format( "INSERT INTO {0} ({1}) VALUES ({2})", dtData.TableName, sColumns, sParameters );
                                odcComm.ExecuteNonQuery();
                            } // for

                            odcConn.Close();
                        } // using 
                    } // using
                } // if
            } // try
            catch( InvalidOperationException e )
            {
                StartIE ie = new StartIE( e.Message );
            }
            catch( OleDbException e )
            {
                XtraMessageBox.Show( e.Message, Resources.ErrorSaveExcel, MessageBoxButtons.OK );
            }
        } // SaveAsWithDriverExcel

    // ������� ������ �������� ([columnname0],[columnname1],[columnname2])
    // � ��������������� �� ���������� (@p0,@p1,@p2)
    // � �������� ����������� ������������ �������
    private static void GenerateColumnsString(DataTable dtData, out string sColumns, out string sParams)
    {
        StringBuilder sbColumns = new StringBuilder();
        StringBuilder sbParams = new StringBuilder();

        for (int i = 0; i < dtData.Columns.Count; i++)
        {
            if (i != 0)
            {
                sbColumns.Append(',');
                sbParams.Append(',');
            }

            sbColumns.AppendFormat("[{0}]", dtData.Columns[i].ColumnName);
            sbParams.AppendFormat("@p{0}", i);
        }
 
        sColumns = sbColumns.ToString();
        sParams = sbParams.ToString();
    } // GenerateColumnsString
 
    // ������� SQL-������ ��� �������� �������, � ������������ � DataTable
    // ���������� ������ 'CREATE TABLE...'
    private static string GenerateSqlStatementCreateTable(DataTable dtData)
    {
        StringBuilder sbCreateTable = new StringBuilder();
 
        DataColumn dc;
 
        sbCreateTable.AppendFormat("CREATE TABLE {0} (", dtData.TableName);

        for (int i = 0; i < dtData.Columns.Count; i++)
        {
            dc = dtData.Columns[i];
 
            if (i != 0) 
                sbCreateTable.Append(",");
 
            string dataType = dc.DataType.Equals(typeof(double)) ? "DOUBLE" : "NVARCHAR";
 
            sbCreateTable.AppendFormat("[{0}] {1}", dc.ColumnName, dataType);
        }

        sbCreateTable.Append(")");
 
        return sbCreateTable.ToString();
    } // GenerateSqlStatementCreateTable

        private void bSaveAs_Click(object sender, EventArgs e)
        {
            if( isTypeExcel == MICROSOFT_EXCEL_PRESENT )
            {
                SaveAsWithExcel();
            }
            else if( isTypeExcel == DRIVER_EXCEL_PRESENT )
            {
                SaveAsWithDriverExcel();
            }
        } // bSave_Click

        private void bClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void PrepareContext(BindingSource bs, string parameter_name, ICollection parameter_collection)
        {
            // ���������, � ������� ����� �������� BindingSource 
            ArrayList collection = new ArrayList(parameter_collection);
            // ����������� ������
            ArrayList coincides = new ArrayList();

            foreach (DataRowView rowV in bs.List)
            {
                object pval = rowV[parameter_name];

                if (collection.Contains(pval))
                {
                    collection.Remove(pval);

                    if (!coincides.Contains(pval)) 
                        coincides.Add(pval);
                }
            }

            List<DataRowView> Del = new List<DataRowView>();

            foreach (DataRowView rowV in bs.List)
            {
                object pval = rowV[parameter_name];

                if (coincides.Contains(pval))
                {
                    coincides.Remove(pval);
                }
                else
                {
                    if (collection.Count > 0)
                    {
                        IEnumerator ie = collection.GetEnumerator(0, 1);
                        ie.MoveNext();
                        pval = ie.Current;
                        collection.Remove(pval);
                        rowV[parameter_name] = pval;
                    }
                    else
                    {
                        Del.Add(rowV);
                    }
                }
            }

            for (int i = Del.Count - 1; i >= 0; i--)
            {
                Del[i].Delete();
            }

            foreach (object pval in collection)
            {
                // DataRowView rowV = ( DataRowView ) bs.AddNew();
                // rowV[ parameter_name ] = pval;
                atlantaDataSet.sensorcoefficientRow newSensCoef_row =
                    (atlantaDataSet.sensorcoefficientRow) atlDataSet.sensorcoefficient.NewRow();
                newSensCoef_row.b = 0;
                newSensCoef_row.K = 0;
                newSensCoef_row.Sensor_id = sens_id;
                newSensCoef_row.SensorValue = 0;
                newSensCoef_row.UserValue = (double) pval; // 0;
                atlDataSet.sensorcoefficient.AddsensorcoefficientRow(newSensCoef_row);
                sensorcoefficientBindingSource.EndEdit();
            }
        }

        private void ExcelForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                LinearApproximation appr = new LinearApproximation();
               
                foreach (DataRowView rowV in bs.List)
                {
                    double x = Convert.ToDouble(rowV["SensorValue"]);
                    double y = Convert.ToDouble(rowV["UserValue"]);
                   
                    appr.AddPoint( x, y );
                }

                List<double> dat = new List<double>(appr.Keys);
                PrepareContext(bs, "UserValue", dat);

                foreach (DataRowView rowV in bs.List)
                {
                    double x = Convert.ToDouble(rowV["SensorValue"]);
                    if (appr.Keys.Contains(x))
                    {
                        LinearApproximation.ValuePoint v = appr[x];

                        if (!rowV["UserValue"].Equals(v.Y))
                            rowV["UserValue"] = v.Y;

                        if (!rowV["K"].Equals(v.K)) 
                            rowV["K"] = v.K;
                        if (!rowV["b"].Equals(v.b)) 
                            rowV["b"] = v.b;
                    }
                }
            }
            catch (LinearApproximationException ex)
            {
                XtraMessageBox.Show(ex.GetText, "Exception");
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.StackTrace, "Updating error.");
            }
            bs.EndEdit();
            bs.Filter = "";
        }

        private void dgv_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            AppController.ShowException(e.Exception);
        }

        private void bindingNavigatorAddNewItem_Click(object sender, EventArgs e)
        {
            atlantaDataSet.sensorcoefficientRow newSensCoef_row =
                (atlantaDataSet.sensorcoefficientRow) atlDataSet.sensorcoefficient.NewRow();
            newSensCoef_row.b = 0;
            //newSensCoef_row.id;
            newSensCoef_row.K = 0;
            newSensCoef_row.Sensor_id = sens_id;
            newSensCoef_row.SensorValue = 0;
            newSensCoef_row.UserValue = 0;
            atlDataSet.sensorcoefficient.AddsensorcoefficientRow(newSensCoef_row);
            sensorcoefficientBindingSource.EndEdit();
        }

        private void init()
        {
            bindingNavigator.CountItemFormat = Resources.ExcelForm_From;
            bindingNavigatorMoveFirstItem.Text = Resources.ExcelForm_toBegin;
            bindingNavigatorMovePreviousItem.Text = Resources.ExcelForm_toPrev;
            bindingNavigatorPositionItem.ToolTipText = Resources.ExcelForm_Current;
            bindingNavigatorCountItem.ToolTipText = Resources.ExcelForm_TotalCount;
            bindingNavigatorMoveNextItem.ToolTipText = Resources.ExcelForm_toNext;
            bindingNavigatorMoveLastItem.ToolTipText = Resources.ExcelForm_toLast;
            bindingNavigatorAddNewItem.ToolTipText = Resources.ExcelForm_addNew;
            bindingNavigatorDeleteItem.ToolTipText = Resources.Delete;
            bClose.Text = Resources.Close;
            bDraft.Text = Resources.ExcelForm_Default;
            bLoad.Text = Resources.Load;
            bSaveAs.Text = Resources.SaveAs;
            dataGridViewTextBoxColumn3.HeaderText = Resources.ExcelForm_Measured;
            dataGridViewTextBoxColumn2.HeaderText = Resources.ExcelForm_Custom;
        }
    }
}