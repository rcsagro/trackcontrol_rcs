using LocalCache;
namespace TrackControl.Setting
{
    partial class ExcelForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && ( components != null ) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager( typeof( ExcelForm ) );
            this.atlDataSet = new LocalCache.atlantaDataSet();
            this.imageList1 = new System.Windows.Forms.ImageList( this.components );
            this.sensorcoefficientTableAdapter = new LocalCache.atlantaDataSetTableAdapters.SensorcoefficientTableAdapter();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bindingNavigator = new System.Windows.Forms.BindingNavigator( this.components );
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.bSaveAs = new System.Windows.Forms.Button();
            this.bLoad = new System.Windows.Forms.Button();
            this.bDraft = new System.Windows.Forms.Button();
            this.bClose = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.sensorcoefficientBindingSource = new System.Windows.Forms.BindingSource( this.components );
            this.printingSystem1 = new DevExpress.XtraPrinting.PrintingSystem( this.components );
            this.compositeLink1 = new DevExpress.XtraPrintingLinks.CompositeLink( this.components );
            ( ( System.ComponentModel.ISupportInitialize )( this.atlDataSet ) ).BeginInit();
            this.panel2.SuspendLayout();
            ( ( System.ComponentModel.ISupportInitialize )( this.dgv ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize )( this.bindingNavigator ) ).BeginInit();
            this.bindingNavigator.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ( ( System.ComponentModel.ISupportInitialize )( this.sensorcoefficientBindingSource ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize )( this.printingSystem1 ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize )( this.compositeLink1.ImageCollection ) ).BeginInit();
            this.SuspendLayout();
            // 
            // atlDataSet
            // 
            this.atlDataSet.DataSetName = "atlantaDataSet";
            this.atlDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // imageList1
            // 
            this.imageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imageList1.ImageSize = new System.Drawing.Size( 16, 16 );
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // sensorcoefficientTableAdapter
            // 
            this.sensorcoefficientTableAdapter.ClearBeforeFill = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add( this.dgv );
            this.panel2.Controls.Add( this.bindingNavigator );
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point( 3, 3 );
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size( 403, 231 );
            this.panel2.TabIndex = 2;
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeRows = false;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange( new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6} );
            this.dgv.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv.Location = new System.Drawing.Point( 0, 25 );
            this.dgv.Name = "dgv";
            this.dgv.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            this.dgv.RowHeadersVisible = false;
            this.dgv.Size = new System.Drawing.Size( 403, 206 );
            this.dgv.TabIndex = 1;
            this.dgv.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler( this.dgv_DataError );
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "id";
            this.dataGridViewTextBoxColumn1.HeaderText = "id";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn2.DataPropertyName = "UserValue";
            this.dataGridViewTextBoxColumn2.HeaderText = "����������������";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn3.DataPropertyName = "SensorValue";
            this.dataGridViewTextBoxColumn3.HeaderText = "���������";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "K";
            this.dataGridViewTextBoxColumn4.HeaderText = "K";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.Visible = false;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "b";
            this.dataGridViewTextBoxColumn5.HeaderText = "b";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.Visible = false;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "Sensor_id";
            this.dataGridViewTextBoxColumn6.HeaderText = "Sensor_id";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.Visible = false;
            // 
            // bindingNavigator
            // 
            this.bindingNavigator.AddNewItem = null;
            this.bindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.bindingNavigator.CountItemFormat = "�� {0}";
            this.bindingNavigator.DeleteItem = this.bindingNavigatorDeleteItem;
            this.bindingNavigator.Items.AddRange( new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem} );
            this.bindingNavigator.Location = new System.Drawing.Point( 0, 0 );
            this.bindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.bindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.bindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.bindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.bindingNavigator.Name = "bindingNavigator";
            this.bindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.bindingNavigator.Size = new System.Drawing.Size( 403, 25 );
            this.bindingNavigator.TabIndex = 2;
            this.bindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size( 36, 22 );
            this.bindingNavigatorCountItem.Text = "�� {0}";
            this.bindingNavigatorCountItem.ToolTipText = "����� ���������� �����";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ( ( System.Drawing.Image )( resources.GetObject( "bindingNavigatorDeleteItem.Image" ) ) );
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size( 23, 22 );
            this.bindingNavigatorDeleteItem.Text = "Delete";
            this.bindingNavigatorDeleteItem.ToolTipText = "�������";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ( ( System.Drawing.Image )( resources.GetObject( "bindingNavigatorMoveFirstItem.Image" ) ) );
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size( 23, 22 );
            this.bindingNavigatorMoveFirstItem.Text = "��������� � ������";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ( ( System.Drawing.Image )( resources.GetObject( "bindingNavigatorMovePreviousItem.Image" ) ) );
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size( 23, 22 );
            this.bindingNavigatorMovePreviousItem.Text = "��������� � �����������";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size( 6, 25 );
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size( 50, 21 );
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "������� �������";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size( 6, 25 );
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ( ( System.Drawing.Image )( resources.GetObject( "bindingNavigatorMoveNextItem.Image" ) ) );
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size( 23, 22 );
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            this.bindingNavigatorMoveNextItem.ToolTipText = "��������� � ����������";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ( ( System.Drawing.Image )( resources.GetObject( "bindingNavigatorMoveLastItem.Image" ) ) );
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size( 23, 22 );
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            this.bindingNavigatorMoveLastItem.ToolTipText = "��������� � ����������";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size( 6, 25 );
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ( ( System.Drawing.Image )( resources.GetObject( "bindingNavigatorAddNewItem.Image" ) ) );
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size( 23, 22 );
            this.bindingNavigatorAddNewItem.Text = "Add new";
            this.bindingNavigatorAddNewItem.ToolTipText = "�������� �����";
            this.bindingNavigatorAddNewItem.Click += new System.EventHandler( this.bindingNavigatorAddNewItem_Click );
            // 
            // panel1
            // 
            this.panel1.Controls.Add( this.bSaveAs );
            this.panel1.Controls.Add( this.bLoad );
            this.panel1.Controls.Add( this.bDraft );
            this.panel1.Controls.Add( this.bClose );
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point( 3, 240 );
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size( 403, 23 );
            this.panel1.TabIndex = 0;
            // 
            // bSaveAs
            // 
            this.bSaveAs.Location = new System.Drawing.Point( 303, 0 );
            this.bSaveAs.Name = "bSaveAs";
            this.bSaveAs.Size = new System.Drawing.Size( 100, 23 );
            this.bSaveAs.TabIndex = 3;
            this.bSaveAs.Text = "��������� ���";
            this.bSaveAs.UseVisualStyleBackColor = true;
            this.bSaveAs.Click += new System.EventHandler( this.bSaveAs_Click );
            // 
            // bLoad
            // 
            this.bLoad.Location = new System.Drawing.Point( 202, 0 );
            this.bLoad.Name = "bLoad";
            this.bLoad.Size = new System.Drawing.Size( 100, 23 );
            this.bLoad.TabIndex = 2;
            this.bLoad.Text = "���������";
            this.bLoad.UseVisualStyleBackColor = true;
            this.bLoad.Click += new System.EventHandler( this.bLoad_Click );
            // 
            // bDraft
            // 
            this.bDraft.Location = new System.Drawing.Point( 101, 0 );
            this.bDraft.Name = "bDraft";
            this.bDraft.Size = new System.Drawing.Size( 100, 23 );
            this.bDraft.TabIndex = 1;
            this.bDraft.Text = "�������������";
            this.bDraft.UseVisualStyleBackColor = true;
            this.bDraft.Click += new System.EventHandler( this.bDraft_Click );
            // 
            // bClose
            // 
            this.bClose.Location = new System.Drawing.Point( 0, 0 );
            this.bClose.Name = "bClose";
            this.bClose.Size = new System.Drawing.Size( 100, 23 );
            this.bClose.TabIndex = 0;
            this.bClose.Text = "�������";
            this.bClose.UseVisualStyleBackColor = true;
            this.bClose.Click += new System.EventHandler( this.bClose_Click );
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add( new System.Windows.Forms.ColumnStyle( System.Windows.Forms.SizeType.Percent, 100F ) );
            this.tableLayoutPanel1.Controls.Add( this.panel1, 0, 1 );
            this.tableLayoutPanel1.Controls.Add( this.panel2, 0, 0 );
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point( 0, 0 );
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add( new System.Windows.Forms.RowStyle( System.Windows.Forms.SizeType.Percent, 100F ) );
            this.tableLayoutPanel1.RowStyles.Add( new System.Windows.Forms.RowStyle() );
            this.tableLayoutPanel1.Size = new System.Drawing.Size( 409, 266 );
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // sensorcoefficientBindingSource
            // 
            this.sensorcoefficientBindingSource.DataMember = "sensorcoefficient";
            this.sensorcoefficientBindingSource.DataSource = this.atlDataSet;
            // 
            // printingSystem1
            // 
            this.printingSystem1.ExportOptions.Xls.ShowGridLines = true;
            this.printingSystem1.ExportOptions.Xlsx.ShowGridLines = true;
            this.printingSystem1.Links.AddRange( new object[] {
            this.compositeLink1} );
            // 
            // compositeLink1
            // 
            // 
            // 
            // 
            this.compositeLink1.ImageCollection.ImageStream = ( ( DevExpress.Utils.ImageCollectionStreamer )( resources.GetObject( "compositeLink1.ImageCollection.ImageStream" ) ) );
            this.compositeLink1.Landscape = true;
            this.compositeLink1.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.compositeLink1.PrintingSystem = this.printingSystem1;
            this.compositeLink1.PrintingSystemBase = this.printingSystem1;
            // 
            // ExcelForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size( 409, 266 );
            this.Controls.Add( this.tableLayoutPanel1 );
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "ExcelForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "ExcelForm";
            this.Load += new System.EventHandler( this.ExcelForm_Load );
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler( this.ExcelForm_FormClosing );
            ( ( System.ComponentModel.ISupportInitialize )( this.atlDataSet ) ).EndInit();
            this.panel2.ResumeLayout( false );
            this.panel2.PerformLayout();
            ( ( System.ComponentModel.ISupportInitialize )( this.dgv ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize )( this.bindingNavigator ) ).EndInit();
            this.bindingNavigator.ResumeLayout( false );
            this.bindingNavigator.PerformLayout();
            this.panel1.ResumeLayout( false );
            this.tableLayoutPanel1.ResumeLayout( false );
            ( ( System.ComponentModel.ISupportInitialize )( this.sensorcoefficientBindingSource ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize )( this.printingSystem1 ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize )( this.compositeLink1.ImageCollection ) ).EndInit();
            this.ResumeLayout( false );

        }

        #endregion

        internal LocalCache.atlantaDataSet atlDataSet;
        private System.Windows.Forms.ImageList imageList1;
        private LocalCache.atlantaDataSetTableAdapters.SensorcoefficientTableAdapter sensorcoefficientTableAdapter;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView dgv;
        private System.Windows.Forms.BindingNavigator bindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button bSaveAs;
        private System.Windows.Forms.Button bLoad;
        private System.Windows.Forms.Button bDraft;
        private System.Windows.Forms.Button bClose;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.BindingSource sensorcoefficientBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private DevExpress.XtraPrinting.PrintingSystem printingSystem1;
        private DevExpress.XtraPrintingLinks.CompositeLink compositeLink1;
    }
}