﻿namespace TrackControl.Setting
{
    partial class SensorStateFormDvx
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if( disposing && ( components != null ) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SensorStateFormDvx));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            this._mainTable = new System.Windows.Forms.TableLayoutPanel();
            this.carSensorTable = new System.Windows.Forms.TableLayoutPanel();
            this.carIcon = new System.Windows.Forms.Label();
            this.sensorIcon = new System.Windows.Forms.Label();
            this.carLbl = new System.Windows.Forms.Label();
            this.sensorLbl = new System.Windows.Forms.Label();
            this.carNameText = new System.Windows.Forms.Label();
            this.sensorNameText = new System.Windows.Forms.Label();
            this.stateGrid = new DevExpress.XtraGrid.GridControl();
            this.stateGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.titleDataGridViewTextBoxColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this.minValueDataGridViewTextBoxColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this.maxValueDataGridViewTextBoxColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Edit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rpItemPictureEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.Delete = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colsensorId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rpItemImageEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemImageEdit();
            this.editGroupBox = new DevExpress.XtraEditors.GroupControl();
            this.sensorStatesbindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this._mainTable.SuspendLayout();
            this.carSensorTable.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.stateGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stateGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rpItemPictureEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rpItemImageEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.editGroupBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sensorStatesbindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // _mainTable
            // 
            this._mainTable.BackColor = System.Drawing.Color.WhiteSmoke;
            this._mainTable.ColumnCount = 3;
            this._mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this._mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this._mainTable.Controls.Add(this.carSensorTable, 1, 1);
            this._mainTable.Controls.Add(this.stateGrid, 1, 3);
            this._mainTable.Controls.Add(this.editGroupBox, 1, 5);
            this._mainTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this._mainTable.Location = new System.Drawing.Point(0, 0);
            this._mainTable.Margin = new System.Windows.Forms.Padding(0);
            this._mainTable.Name = "_mainTable";
            this._mainTable.RowCount = 7;
            this._mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 5F));
            this._mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 39F));
            this._mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 5F));
            this._mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 5F));
            this._mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 154F));
            this._mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 8F));
            this._mainTable.Size = new System.Drawing.Size(637, 337);
            this._mainTable.TabIndex = 3;
            // 
            // carSensorTable
            // 
            this.carSensorTable.ColumnCount = 3;
            this.carSensorTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 21F));
            this.carSensorTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 86F));
            this.carSensorTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.carSensorTable.Controls.Add(this.carIcon, 0, 0);
            this.carSensorTable.Controls.Add(this.sensorIcon, 0, 1);
            this.carSensorTable.Controls.Add(this.carLbl, 1, 0);
            this.carSensorTable.Controls.Add(this.sensorLbl, 1, 1);
            this.carSensorTable.Controls.Add(this.carNameText, 2, 0);
            this.carSensorTable.Controls.Add(this.sensorNameText, 2, 1);
            this.carSensorTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.carSensorTable.Location = new System.Drawing.Point(10, 5);
            this.carSensorTable.Margin = new System.Windows.Forms.Padding(0);
            this.carSensorTable.Name = "carSensorTable";
            this.carSensorTable.RowCount = 2;
            this.carSensorTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.carSensorTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.carSensorTable.Size = new System.Drawing.Size(617, 39);
            this.carSensorTable.TabIndex = 0;
            // 
            // carIcon
            // 
            this.carIcon.AutoSize = true;
            this.carIcon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.carIcon.Image = ((System.Drawing.Image)(resources.GetObject("carIcon.Image")));
            this.carIcon.Location = new System.Drawing.Point(0, 0);
            this.carIcon.Margin = new System.Windows.Forms.Padding(0);
            this.carIcon.Name = "carIcon";
            this.carIcon.Size = new System.Drawing.Size(21, 19);
            this.carIcon.TabIndex = 0;
            // 
            // sensorIcon
            // 
            this.sensorIcon.AutoSize = true;
            this.sensorIcon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sensorIcon.Image = ((System.Drawing.Image)(resources.GetObject("sensorIcon.Image")));
            this.sensorIcon.Location = new System.Drawing.Point(0, 19);
            this.sensorIcon.Margin = new System.Windows.Forms.Padding(0);
            this.sensorIcon.Name = "sensorIcon";
            this.sensorIcon.Size = new System.Drawing.Size(21, 20);
            this.sensorIcon.TabIndex = 1;
            // 
            // carLbl
            // 
            this.carLbl.AutoSize = true;
            this.carLbl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.carLbl.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.carLbl.ForeColor = System.Drawing.Color.DarkOliveGreen;
            this.carLbl.Location = new System.Drawing.Point(21, 0);
            this.carLbl.Margin = new System.Windows.Forms.Padding(0);
            this.carLbl.Name = "carLbl";
            this.carLbl.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.carLbl.Size = new System.Drawing.Size(86, 19);
            this.carLbl.TabIndex = 2;
            this.carLbl.Text = "Автомобиль:";
            this.carLbl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // sensorLbl
            // 
            this.sensorLbl.AutoSize = true;
            this.sensorLbl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sensorLbl.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.sensorLbl.ForeColor = System.Drawing.Color.DarkSlateGray;
            this.sensorLbl.Location = new System.Drawing.Point(21, 19);
            this.sensorLbl.Margin = new System.Windows.Forms.Padding(0);
            this.sensorLbl.Name = "sensorLbl";
            this.sensorLbl.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.sensorLbl.Size = new System.Drawing.Size(86, 20);
            this.sensorLbl.TabIndex = 3;
            this.sensorLbl.Text = "Датчик:";
            this.sensorLbl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // carNameText
            // 
            this.carNameText.AutoSize = true;
            this.carNameText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.carNameText.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.carNameText.ForeColor = System.Drawing.Color.Black;
            this.carNameText.Location = new System.Drawing.Point(107, 0);
            this.carNameText.Margin = new System.Windows.Forms.Padding(0);
            this.carNameText.Name = "carNameText";
            this.carNameText.Size = new System.Drawing.Size(510, 19);
            this.carNameText.TabIndex = 4;
            this.carNameText.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // sensorNameText
            // 
            this.sensorNameText.AutoSize = true;
            this.sensorNameText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sensorNameText.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.sensorNameText.ForeColor = System.Drawing.Color.Black;
            this.sensorNameText.Location = new System.Drawing.Point(107, 19);
            this.sensorNameText.Margin = new System.Windows.Forms.Padding(0);
            this.sensorNameText.Name = "sensorNameText";
            this.sensorNameText.Size = new System.Drawing.Size(510, 20);
            this.sensorNameText.TabIndex = 5;
            this.sensorNameText.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // stateGrid
            // 
            this.stateGrid.DataSource = this.sensorStatesbindingSource;
            this.stateGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.stateGrid.Location = new System.Drawing.Point(13, 52);
            this.stateGrid.MainView = this.stateGridView;
            this.stateGrid.Name = "stateGrid";
            this.stateGrid.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.rpItemImageEdit1,
            this.rpItemPictureEdit1});
            this.stateGrid.Size = new System.Drawing.Size(611, 115);
            this.stateGrid.TabIndex = 4;
            this.stateGrid.UseDisabledStatePainter = false;
            this.stateGrid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.stateGridView});
            // 
            // stateGridView
            // 
            this.stateGridView.Appearance.GroupRow.Image = ((System.Drawing.Image)(resources.GetObject("stateGridView.Appearance.GroupRow.Image")));
            this.stateGridView.Appearance.GroupRow.Options.UseImage = true;
            this.stateGridView.ColumnPanelRowHeight = 40;
            this.stateGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.titleDataGridViewTextBoxColumn,
            this.minValueDataGridViewTextBoxColumn,
            this.maxValueDataGridViewTextBoxColumn,
            this.Edit,
            this.Delete,
            this.colId,
            this.colsensorId});
            this.stateGridView.GridControl = this.stateGrid;
            this.stateGridView.Name = "stateGridView";
            this.stateGridView.OptionsView.ShowGroupPanel = false;
            // 
            // titleDataGridViewTextBoxColumn
            // 
            this.titleDataGridViewTextBoxColumn.AppearanceCell.Options.UseTextOptions = true;
            this.titleDataGridViewTextBoxColumn.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.titleDataGridViewTextBoxColumn.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.titleDataGridViewTextBoxColumn.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.titleDataGridViewTextBoxColumn.AppearanceHeader.Options.UseTextOptions = true;
            this.titleDataGridViewTextBoxColumn.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.titleDataGridViewTextBoxColumn.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.titleDataGridViewTextBoxColumn.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.titleDataGridViewTextBoxColumn.Caption = "Описание";
            this.titleDataGridViewTextBoxColumn.FieldName = "Title";
            this.titleDataGridViewTextBoxColumn.Name = "titleDataGridViewTextBoxColumn";
            this.titleDataGridViewTextBoxColumn.OptionsColumn.AllowEdit = false;
            this.titleDataGridViewTextBoxColumn.OptionsColumn.AllowFocus = false;
            this.titleDataGridViewTextBoxColumn.OptionsColumn.ReadOnly = true;
            this.titleDataGridViewTextBoxColumn.ToolTip = "Описание состояния";
            this.titleDataGridViewTextBoxColumn.Visible = true;
            this.titleDataGridViewTextBoxColumn.VisibleIndex = 0;
            // 
            // minValueDataGridViewTextBoxColumn
            // 
            this.minValueDataGridViewTextBoxColumn.AppearanceCell.Options.UseTextOptions = true;
            this.minValueDataGridViewTextBoxColumn.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.minValueDataGridViewTextBoxColumn.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.minValueDataGridViewTextBoxColumn.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.minValueDataGridViewTextBoxColumn.AppearanceHeader.Options.UseTextOptions = true;
            this.minValueDataGridViewTextBoxColumn.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.minValueDataGridViewTextBoxColumn.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.minValueDataGridViewTextBoxColumn.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.minValueDataGridViewTextBoxColumn.Caption = "Нижний порог";
            this.minValueDataGridViewTextBoxColumn.FieldName = "MinValue";
            this.minValueDataGridViewTextBoxColumn.Name = "minValueDataGridViewTextBoxColumn";
            this.minValueDataGridViewTextBoxColumn.OptionsColumn.AllowEdit = false;
            this.minValueDataGridViewTextBoxColumn.OptionsColumn.AllowFocus = false;
            this.minValueDataGridViewTextBoxColumn.OptionsColumn.ReadOnly = true;
            this.minValueDataGridViewTextBoxColumn.ToolTip = "Значение нижнего порога";
            this.minValueDataGridViewTextBoxColumn.Visible = true;
            this.minValueDataGridViewTextBoxColumn.VisibleIndex = 1;
            // 
            // maxValueDataGridViewTextBoxColumn
            // 
            this.maxValueDataGridViewTextBoxColumn.AppearanceCell.Options.UseTextOptions = true;
            this.maxValueDataGridViewTextBoxColumn.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.maxValueDataGridViewTextBoxColumn.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.maxValueDataGridViewTextBoxColumn.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.maxValueDataGridViewTextBoxColumn.AppearanceHeader.Options.UseTextOptions = true;
            this.maxValueDataGridViewTextBoxColumn.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.maxValueDataGridViewTextBoxColumn.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.maxValueDataGridViewTextBoxColumn.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.maxValueDataGridViewTextBoxColumn.Caption = "Верхний порог";
            this.maxValueDataGridViewTextBoxColumn.FieldName = "MaxValue";
            this.maxValueDataGridViewTextBoxColumn.Name = "maxValueDataGridViewTextBoxColumn";
            this.maxValueDataGridViewTextBoxColumn.OptionsColumn.AllowEdit = false;
            this.maxValueDataGridViewTextBoxColumn.OptionsColumn.AllowFocus = false;
            this.maxValueDataGridViewTextBoxColumn.OptionsColumn.ReadOnly = true;
            this.maxValueDataGridViewTextBoxColumn.ToolTip = "Значение верхнего порога";
            this.maxValueDataGridViewTextBoxColumn.Visible = true;
            this.maxValueDataGridViewTextBoxColumn.VisibleIndex = 2;
            // 
            // Edit
            // 
            this.Edit.AppearanceCell.Image = ((System.Drawing.Image)(resources.GetObject("Edit.AppearanceCell.Image")));
            this.Edit.AppearanceCell.Options.UseImage = true;
            this.Edit.AppearanceCell.Options.UseTextOptions = true;
            this.Edit.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Edit.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.Edit.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.Edit.AppearanceHeader.Image = ((System.Drawing.Image)(resources.GetObject("Edit.AppearanceHeader.Image")));
            this.Edit.AppearanceHeader.Options.UseImage = true;
            this.Edit.AppearanceHeader.Options.UseTextOptions = true;
            this.Edit.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Edit.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.Edit.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Edit.ColumnEdit = this.rpItemPictureEdit1;
            this.Edit.FieldName = "Edit";
            this.Edit.Image = ((System.Drawing.Image)(resources.GetObject("Edit.Image")));
            this.Edit.ImageAlignment = System.Drawing.StringAlignment.Center;
            this.Edit.ImageIndex = 0;
            this.Edit.Name = "Edit";
            this.Edit.OptionsColumn.AllowEdit = false;
            this.Edit.OptionsColumn.AllowFocus = false;
            this.Edit.OptionsColumn.ReadOnly = true;
            this.Edit.ToolTip = "Редактировать";
            this.Edit.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.Edit.Visible = true;
            this.Edit.VisibleIndex = 3;
            this.Edit.Width = 24;
            // 
            // rpItemPictureEdit1
            // 
            this.rpItemPictureEdit1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.rpItemPictureEdit1.InitialImage = ((System.Drawing.Image)(resources.GetObject("rpItemPictureEdit1.InitialImage")));
            this.rpItemPictureEdit1.Name = "rpItemPictureEdit1";
            this.rpItemPictureEdit1.ReadOnly = true;
            // 
            // Delete
            // 
            this.Delete.AppearanceCell.Options.UseTextOptions = true;
            this.Delete.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Delete.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.Delete.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.Delete.AppearanceHeader.Options.UseTextOptions = true;
            this.Delete.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Delete.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.Delete.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Delete.ColumnEdit = this.rpItemPictureEdit1;
            this.Delete.FieldName = "Delete";
            this.Delete.Image = ((System.Drawing.Image)(resources.GetObject("Delete.Image")));
            this.Delete.ImageAlignment = System.Drawing.StringAlignment.Center;
            this.Delete.ImageIndex = 0;
            this.Delete.Name = "Delete";
            this.Delete.OptionsColumn.AllowEdit = false;
            this.Delete.OptionsColumn.AllowFocus = false;
            this.Delete.OptionsColumn.ReadOnly = true;
            this.Delete.ToolTip = "Удалить";
            this.Delete.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.Delete.Visible = true;
            this.Delete.VisibleIndex = 4;
            this.Delete.Width = 24;
            // 
            // colId
            // 
            this.colId.FieldName = "Id";
            this.colId.Name = "colId";
            // 
            // colsensorId
            // 
            this.colsensorId.FieldName = "SensorId";
            this.colsensorId.Name = "colsensorId";
            // 
            // rpItemImageEdit1
            // 
            this.rpItemImageEdit1.AutoHeight = false;
            this.rpItemImageEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("rpItemImageEdit1.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "Редактировать событие", null, null, true)});
            this.rpItemImageEdit1.Name = "rpItemImageEdit1";
            // 
            // editGroupBox
            // 
            this.editGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.editGroupBox.Location = new System.Drawing.Point(13, 178);
            this.editGroupBox.Name = "editGroupBox";
            this.editGroupBox.Size = new System.Drawing.Size(611, 148);
            this.editGroupBox.TabIndex = 5;
            this.editGroupBox.Text = "Добавить состояние";
            // 
            // sensorStatesbindingSource
            // 
            this.sensorStatesbindingSource.DataMember = "State";
            this.sensorStatesbindingSource.DataSource = typeof(LocalCache.atlantaDataSet);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "pencil.png");
            this.imageList1.Images.SetKeyName(1, "cross.png");
            // 
            // SensorStateFormDvx
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(637, 337);
            this.Controls.Add(this._mainTable);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SensorStateFormDvx";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Состояния для датчика";
            this._mainTable.ResumeLayout(false);
            this.carSensorTable.ResumeLayout(false);
            this.carSensorTable.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.stateGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stateGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rpItemPictureEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rpItemImageEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.editGroupBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sensorStatesbindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel _mainTable;
        private System.Windows.Forms.TableLayoutPanel carSensorTable;
        private System.Windows.Forms.Label carIcon;
        private System.Windows.Forms.Label sensorIcon;
        private System.Windows.Forms.Label carLbl;
        private System.Windows.Forms.Label sensorLbl;
        private System.Windows.Forms.Label carNameText;
        private System.Windows.Forms.Label sensorNameText;
        private DevExpress.XtraGrid.GridControl stateGrid;
        private DevExpress.XtraGrid.Views.Grid.GridView stateGridView;
        private DevExpress.XtraGrid.Columns.GridColumn titleDataGridViewTextBoxColumn;
        private DevExpress.XtraGrid.Columns.GridColumn minValueDataGridViewTextBoxColumn;
        private DevExpress.XtraGrid.Columns.GridColumn maxValueDataGridViewTextBoxColumn;
        private DevExpress.XtraGrid.Columns.GridColumn Edit;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit rpItemPictureEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn Delete;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageEdit rpItemImageEdit1;
        private DevExpress.XtraEditors.GroupControl editGroupBox;
        private System.Windows.Forms.BindingSource sensorStatesbindingSource;
        private System.Windows.Forms.ImageList imageList1;
        private DevExpress.XtraGrid.Columns.GridColumn colId;
        private DevExpress.XtraGrid.Columns.GridColumn colsensorId;
    }
}