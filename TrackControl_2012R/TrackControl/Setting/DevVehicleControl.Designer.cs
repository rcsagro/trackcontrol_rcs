﻿namespace TrackControl.Setting
{
    partial class DevVehicleControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && ( components != null ) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DevVehicleControl));
            this._barManager = new DevExpress.XtraBars.BarManager(this.components);
            this._tools = new DevExpress.XtraBars.Bar();
            this.bbiExpand = new DevExpress.XtraBars.BarButtonItem();
            this.bbiCollapce = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSynhronize = new DevExpress.XtraBars.BarButtonItem();
            this.bbiRefreshing = new DevExpress.XtraBars.BarButtonItem();
            this.bbiDeleteParam = new DevExpress.XtraBars.BarButtonItem();
            this._funnelBox = new DevExpress.XtraBars.BarEditItem();
            this._funnelRepo = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this._eraseBtn = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.timeImages = new DevExpress.Utils.ImageCollection(this.components);
            this._popup = new DevExpress.XtraBars.PopupMenu(this.components);
            this.ttcTree = new DevExpress.Utils.ToolTipController(this.components);
            this._images = new DevExpress.Utils.ImageCollection(this.components);
            this.atlantaDataSet = new LocalCache.atlantaDataSet();
            this.settingBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.settingTableAdapter = new LocalCache.atlantaDataSetTableAdapters.SettingTableAdapter();
            this._tree = new DevExpress.XtraTreeList.TreeList();
            this._titleCol = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this._descriptionCol = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.timeCol = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.rcbTime = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this._loginCol = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.colNameParameter = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.vehicleBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.vehicleTableAdapter = new LocalCache.atlantaDataSetTableAdapters.VehicleTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this._barManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._funnelRepo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeImages)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._popup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._images)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.atlantaDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.settingBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._tree)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rcbTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vehicleBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // _barManager
            // 
            this._barManager.AllowCustomization = false;
            this._barManager.AllowMoveBarOnToolbar = false;
            this._barManager.AllowQuickCustomization = false;
            this._barManager.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this._tools});
            this._barManager.DockControls.Add(this.barDockControlTop);
            this._barManager.DockControls.Add(this.barDockControlBottom);
            this._barManager.DockControls.Add(this.barDockControlLeft);
            this._barManager.DockControls.Add(this.barDockControlRight);
            this._barManager.Form = this;
            this._barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this._funnelBox,
            this._eraseBtn,
            this.bbiExpand,
            this.bbiCollapce,
            this.bbiSynhronize,
            this.bbiRefreshing,
            this.bbiDeleteParam});
            this._barManager.MaxItemId = 7;
            this._barManager.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this._funnelRepo});
            // 
            // _tools
            // 
            this._tools.BarName = "Tools";
            this._tools.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Top;
            this._tools.DockCol = 0;
            this._tools.DockRow = 0;
            this._tools.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this._tools.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bbiExpand, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bbiCollapce, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bbiSynhronize, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bbiRefreshing, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bbiDeleteParam, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.Width, this._funnelBox, "", false, true, true, 129),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this._eraseBtn, DevExpress.XtraBars.BarItemPaintStyle.Standard)});
            this._tools.OptionsBar.AllowQuickCustomization = false;
            this._tools.OptionsBar.DisableClose = true;
            this._tools.OptionsBar.DisableCustomization = true;
            this._tools.OptionsBar.DrawDragBorder = false;
            this._tools.OptionsBar.UseWholeRow = true;
            this._tools.Text = "Tools";
            // 
            // bbiExpand
            // 
            this.bbiExpand.Border = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.bbiExpand.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiExpand.Glyph")));
            this.bbiExpand.Hint = "Развернуть узлы дерева";
            this.bbiExpand.Id = 2;
            this.bbiExpand.Name = "bbiExpand";
            this.bbiExpand.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiExpand_ItemClick);
            // 
            // bbiCollapce
            // 
            this.bbiCollapce.Border = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.bbiCollapce.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiCollapce.Glyph")));
            this.bbiCollapce.Hint = "Свернуть узлы дерева";
            this.bbiCollapce.Id = 3;
            this.bbiCollapce.Name = "bbiCollapce";
            this.bbiCollapce.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiCollapce_ItemClick);
            // 
            // bbiSynhronize
            // 
            this.bbiSynhronize.Border = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.bbiSynhronize.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiSynhronize.Glyph")));
            this.bbiSynhronize.Hint = "Синхронизировать содержимое дерева";
            this.bbiSynhronize.Id = 4;
            this.bbiSynhronize.Name = "bbiSynhronize";
            this.bbiSynhronize.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.bbiSynhronize.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSynhronize_ItemClick);
            // 
            // bbiRefreshing
            // 
            this.bbiRefreshing.Border = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.bbiRefreshing.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiRefreshing.Glyph")));
            this.bbiRefreshing.Hint = "Обновить содержимое дерева";
            this.bbiRefreshing.Id = 5;
            this.bbiRefreshing.Name = "bbiRefreshing";
            this.bbiRefreshing.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRefreshing_ItemClick);
            // 
            // bbiDeleteParam
            // 
            this.bbiDeleteParam.Border = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.bbiDeleteParam.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiDeleteParam.Glyph")));
            this.bbiDeleteParam.Hint = "Удалить настройку с машины";
            this.bbiDeleteParam.Id = 6;
            this.bbiDeleteParam.Name = "bbiDeleteParam";
            this.bbiDeleteParam.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_ItemClick);
            // 
            // _funnelBox
            // 
            this._funnelBox.Edit = this._funnelRepo;
            this._funnelBox.Id = 0;
            this._funnelBox.Name = "_funnelBox";
            this._funnelBox.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // _funnelRepo
            // 
            this._funnelRepo.AutoHeight = false;
            this._funnelRepo.MaxLength = 20;
            this._funnelRepo.Name = "_funnelRepo";
            // 
            // _eraseBtn
            // 
            this._eraseBtn.Border = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this._eraseBtn.Glyph = ((System.Drawing.Image)(resources.GetObject("_eraseBtn.Glyph")));
            this._eraseBtn.Hint = "Очистить фильтр";
            this._eraseBtn.Id = 1;
            this._eraseBtn.Name = "_eraseBtn";
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(825, 33);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 441);
            this.barDockControlBottom.Size = new System.Drawing.Size(825, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 33);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 408);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(825, 33);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 408);
            // 
            // timeImages
            // 
            this.timeImages.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("timeImages.ImageStream")));
            this.timeImages.Images.SetKeyName(0, "status.png");
            this.timeImages.Images.SetKeyName(1, "status-away.png");
            this.timeImages.Images.SetKeyName(2, "status-busy.png");
            this.timeImages.Images.SetKeyName(3, "status-offline.png");
            // 
            // _popup
            // 
            this._popup.Manager = this._barManager;
            this._popup.Name = "_popup";
            // 
            // _images
            // 
            this._images.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("_images.ImageStream")));
            // 
            // atlantaDataSet
            // 
            this.atlantaDataSet.DataSetName = "atlantaDataSet";
            this.atlantaDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // settingBindingSource
            // 
            this.settingBindingSource.DataMember = "setting";
            this.settingBindingSource.DataSource = this.atlantaDataSet;
            // 
            // settingTableAdapter
            // 
            this.settingTableAdapter.ClearBeforeFill = true;
            // 
            // _tree
            // 
            this._tree.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this._tree.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this._titleCol,
            this._descriptionCol,
            this.timeCol,
            this._loginCol,
            this.colNameParameter});
            this._tree.Dock = System.Windows.Forms.DockStyle.Fill;
            this._tree.ImageIndexFieldName = "IconIndex";
            this._tree.Location = new System.Drawing.Point(0, 33);
            this._tree.Margin = new System.Windows.Forms.Padding(0);
            this._tree.Name = "_tree";
            this._tree.OptionsBehavior.AllowIndeterminateCheckState = true;
            this._tree.OptionsBehavior.Editable = false;
            this._tree.OptionsBehavior.EnableFiltering = true;
            this._tree.OptionsMenu.EnableColumnMenu = false;
            this._tree.OptionsMenu.EnableFooterMenu = false;
            this._tree.OptionsView.ShowCheckBoxes = true;
            this._tree.OptionsView.ShowIndicator = false;
            this._tree.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.rcbTime});
            this._tree.SelectImageList = this._images;
            this._tree.Size = new System.Drawing.Size(825, 408);
            this._tree.TabIndex = 5;
            this._tree.ToolTipController = this.ttcTree;
            this._tree.VisibleChanged += new System.EventHandler(this._tree_VisibleChanged);
            // 
            // _titleCol
            // 
            this._titleCol.Caption = "Название, номер";
            this._titleCol.FieldName = "Title";
            this._titleCol.MinWidth = 150;
            this._titleCol.Name = "_titleCol";
            this._titleCol.OptionsColumn.AllowEdit = false;
            this._titleCol.OptionsColumn.AllowFocus = false;
            this._titleCol.OptionsColumn.AllowMove = false;
            this._titleCol.OptionsColumn.AllowMoveToCustomizationForm = false;
            this._titleCol.OptionsColumn.ShowInCustomizationForm = false;
            this._titleCol.SortMode = DevExpress.XtraGrid.ColumnSortMode.DisplayText;
            this._titleCol.SortOrder = System.Windows.Forms.SortOrder.Ascending;
            this._titleCol.UnboundType = DevExpress.XtraTreeList.Data.UnboundColumnType.String;
            this._titleCol.Visible = true;
            this._titleCol.VisibleIndex = 0;
            this._titleCol.Width = 159;
            // 
            // _descriptionCol
            // 
            this._descriptionCol.Caption = "Описание";
            this._descriptionCol.FieldName = "Description";
            this._descriptionCol.MinWidth = 100;
            this._descriptionCol.Name = "_descriptionCol";
            this._descriptionCol.OptionsColumn.AllowEdit = false;
            this._descriptionCol.OptionsColumn.AllowFocus = false;
            this._descriptionCol.OptionsColumn.AllowMove = false;
            this._descriptionCol.OptionsColumn.AllowMoveToCustomizationForm = false;
            this._descriptionCol.OptionsColumn.ShowInCustomizationForm = false;
            this._descriptionCol.Visible = true;
            this._descriptionCol.VisibleIndex = 1;
            this._descriptionCol.Width = 278;
            // 
            // timeCol
            // 
            this.timeCol.ColumnEdit = this.rcbTime;
            this.timeCol.Name = "timeCol";
            this.timeCol.OptionsColumn.AllowEdit = false;
            this.timeCol.OptionsColumn.AllowFocus = false;
            this.timeCol.OptionsColumn.AllowMove = false;
            this.timeCol.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.timeCol.OptionsColumn.AllowSize = false;
            this.timeCol.OptionsColumn.FixedWidth = true;
            this.timeCol.OptionsColumn.ReadOnly = true;
            this.timeCol.OptionsColumn.ShowInCustomizationForm = false;
            this.timeCol.Visible = true;
            this.timeCol.VisibleIndex = 2;
            this.timeCol.Width = 60;
            // 
            // rcbTime
            // 
            this.rcbTime.AutoHeight = false;
            this.rcbTime.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rcbTime.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", 0, 0),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", 1, 1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", 2, 2),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", 3, 3)});
            this.rcbTime.Name = "rcbTime";
            this.rcbTime.SmallImages = this.timeImages;
            // 
            // _loginCol
            // 
            this._loginCol.AppearanceCell.Options.UseTextOptions = true;
            this._loginCol.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._loginCol.AppearanceHeader.Options.UseTextOptions = true;
            this._loginCol.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._loginCol.Caption = "Логин";
            this._loginCol.FieldName = "Login";
            this._loginCol.MinWidth = 40;
            this._loginCol.Name = "_loginCol";
            this._loginCol.OptionsColumn.AllowEdit = false;
            this._loginCol.OptionsColumn.AllowFocus = false;
            this._loginCol.OptionsColumn.AllowMove = false;
            this._loginCol.OptionsColumn.AllowMoveToCustomizationForm = false;
            this._loginCol.OptionsColumn.FixedWidth = true;
            this._loginCol.OptionsColumn.ShowInCustomizationForm = false;
            this._loginCol.UnboundType = DevExpress.XtraTreeList.Data.UnboundColumnType.String;
            this._loginCol.Visible = true;
            this._loginCol.VisibleIndex = 3;
            this._loginCol.Width = 97;
            // 
            // colNameParameter
            // 
            this.colNameParameter.AppearanceCell.Options.UseTextOptions = true;
            this.colNameParameter.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNameParameter.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colNameParameter.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colNameParameter.AppearanceHeader.Options.UseTextOptions = true;
            this.colNameParameter.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNameParameter.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colNameParameter.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colNameParameter.Caption = "Код настройки";
            this.colNameParameter.FieldName = "Имя настройки";
            this.colNameParameter.Name = "colNameParameter";
            this.colNameParameter.OptionsColumn.AllowEdit = false;
            this.colNameParameter.OptionsColumn.AllowFocus = false;
            this.colNameParameter.OptionsColumn.AllowMove = false;
            this.colNameParameter.OptionsColumn.AllowMoveToCustomizationForm = false;
            this.colNameParameter.OptionsColumn.ReadOnly = true;
            this.colNameParameter.OptionsColumn.ShowInCustomizationForm = false;
            this.colNameParameter.Visible = true;
            this.colNameParameter.VisibleIndex = 4;
            this.colNameParameter.Width = 227;
            // 
            // vehicleBindingSource
            // 
            this.vehicleBindingSource.DataMember = "vehicle";
            this.vehicleBindingSource.DataSource = this.atlantaDataSet;
            // 
            // vehicleTableAdapter
            // 
            this.vehicleTableAdapter.ClearBeforeFill = true;
            // 
            // DevVehicleControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._tree);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "DevVehicleControl";
            this.Size = new System.Drawing.Size(825, 441);
            ((System.ComponentModel.ISupportInitialize)(this._barManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._funnelRepo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeImages)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._popup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._images)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.atlantaDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.settingBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._tree)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rcbTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vehicleBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.BarManager _barManager;
        private DevExpress.XtraBars.Bar _tools;
        private DevExpress.XtraBars.BarButtonItem bbiExpand;
        private DevExpress.XtraBars.BarButtonItem bbiCollapce;
        private DevExpress.XtraBars.BarEditItem _funnelBox;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit _funnelRepo;
        private DevExpress.XtraBars.BarButtonItem _eraseBtn;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.Utils.ImageCollection timeImages;
        private DevExpress.XtraBars.PopupMenu _popup;
        private DevExpress.Utils.ToolTipController ttcTree;
        private DevExpress.XtraBars.BarButtonItem bbiSynhronize;
        private DevExpress.Utils.ImageCollection _images;
        private LocalCache.atlantaDataSet atlantaDataSet;
        private System.Windows.Forms.BindingSource settingBindingSource;
        private LocalCache.atlantaDataSetTableAdapters.SettingTableAdapter settingTableAdapter;
        private DevExpress.XtraTreeList.TreeList _tree;
        private DevExpress.XtraTreeList.Columns.TreeListColumn _titleCol;
        private DevExpress.XtraTreeList.Columns.TreeListColumn _descriptionCol;
        private DevExpress.XtraTreeList.Columns.TreeListColumn timeCol;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox rcbTime;
        private DevExpress.XtraTreeList.Columns.TreeListColumn _loginCol;
        private DevExpress.XtraTreeList.Columns.TreeListColumn colNameParameter;
        private DevExpress.XtraBars.BarButtonItem bbiRefreshing;
        private DevExpress.XtraBars.BarButtonItem bbiDeleteParam;
        private System.Windows.Forms.BindingSource vehicleBindingSource;
        private LocalCache.atlantaDataSetTableAdapters.VehicleTableAdapter vehicleTableAdapter;
    }
}
