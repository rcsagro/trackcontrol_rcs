
namespace TrackControl.Setting.ReportsCfg
{
  /// <summary>
  /// ��������� ������������� �������.
  /// ������������� ������ ����������� �� 
  /// <see cref="TrackControl.Setting.ReportsCfg.BaseReportCfg"/>.
  /// </summary>
  interface IReportCfg
  {
    /// <summary>
    /// ��������� �� ��������� � ������ ������.
    /// </summary>
    /// <returns>������ - ��������� ���������.</returns>
    bool ChangesOccurred();
  }
}
