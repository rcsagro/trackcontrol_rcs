namespace TrackControl.Setting.ReportsCfg
{
  partial class BaseReportCfg
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.pnlButtons = new System.Windows.Forms.Panel();
      this.lblChangesWarning = new System.Windows.Forms.Label();
      this.btnCancel = new System.Windows.Forms.Button();
      this.btnSave = new System.Windows.Forms.Button();
      this.pnlMain = new System.Windows.Forms.Panel();
      this.providerChangesWarning = new System.Windows.Forms.ErrorProvider(this.components);
      this.pnlButtons.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.providerChangesWarning)).BeginInit();
      this.SuspendLayout();
      // 
      // pnlButtons
      // 
      this.pnlButtons.Controls.Add(this.lblChangesWarning);
      this.pnlButtons.Controls.Add(this.btnCancel);
      this.pnlButtons.Controls.Add(this.btnSave);
      this.pnlButtons.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.pnlButtons.Location = new System.Drawing.Point(0, 261);
      this.pnlButtons.Name = "pnlButtons";
      this.pnlButtons.Size = new System.Drawing.Size(507, 46);
      this.pnlButtons.TabIndex = 1;
      // 
      // lblChangesWarning
      // 
      this.lblChangesWarning.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.lblChangesWarning.AutoSize = true;
      this.lblChangesWarning.ForeColor = System.Drawing.Color.Red;
      this.lblChangesWarning.Image = TrackControl.General.Shared.WarningLittle;
      this.lblChangesWarning.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.lblChangesWarning.Location = new System.Drawing.Point(3, 15);
      this.lblChangesWarning.Name = "lblChangesWarning";
      this.lblChangesWarning.Size = new System.Drawing.Size(247, 13);
      this.lblChangesWarning.TabIndex = 2;
      this.lblChangesWarning.Text = "      ��������� ��������� ��� �������� ������";
      this.lblChangesWarning.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      this.lblChangesWarning.Visible = false;
      // 
      // btnCancel
      // 
      this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.btnCancel.Location = new System.Drawing.Point(395, 10);
      this.btnCancel.Name = "btnCancel";
      this.btnCancel.Size = new System.Drawing.Size(75, 23);
      this.btnCancel.TabIndex = 1;
      this.btnCancel.Text = "������";
      this.btnCancel.UseVisualStyleBackColor = true;
      this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
      // 
      // btnSave
      // 
      this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.btnSave.Location = new System.Drawing.Point(290, 10);
      this.btnSave.Name = "btnSave";
      this.btnSave.Size = new System.Drawing.Size(75, 23);
      this.btnSave.TabIndex = 0;
      this.btnSave.Text = "���������";
      this.btnSave.UseVisualStyleBackColor = true;
      this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
      // 
      // pnlMain
      // 
      this.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pnlMain.Location = new System.Drawing.Point(0, 0);
      this.pnlMain.Name = "pnlMain";
      this.pnlMain.Size = new System.Drawing.Size(507, 261);
      this.pnlMain.TabIndex = 0;
      // 
      // providerChangesWarning
      // 
      this.providerChangesWarning.ContainerControl = this;
      // 
      // BaseReportCfg
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.pnlMain);
      this.Controls.Add(this.pnlButtons);
      this.MinimumSize = new System.Drawing.Size(300, 200);
      this.Name = "BaseReportCfg";
      this.Size = new System.Drawing.Size(507, 307);
      this.Load += new System.EventHandler(this.BaseReportCfg_Load);
      this.pnlButtons.ResumeLayout(false);
      this.pnlButtons.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.providerChangesWarning)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Button btnCancel;
    private System.Windows.Forms.Button btnSave;
    private System.Windows.Forms.Label lblChangesWarning;
    private System.Windows.Forms.Panel pnlButtons;
    private System.Windows.Forms.Panel pnlMain;
    protected System.Windows.Forms.ErrorProvider providerChangesWarning;

  }
}
