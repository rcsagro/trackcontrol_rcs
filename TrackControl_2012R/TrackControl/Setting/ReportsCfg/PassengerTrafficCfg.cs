using ReportsOnPassengerTraffic;
using TrackControl.Properties;

namespace TrackControl.Setting.ReportsCfg
{
    /// <summary>
    /// ������������ ������ "��������������".
    /// </summary>
    public partial class PassengerTrafficCfg : BaseReportCfg
    {
        private ReportPassZonesModel reportPassZonesModel;

        public PassengerTrafficCfg()
        {
            InitializeComponent();
            init();

            if (IsRealRuntime)
            {
                reportPassZonesModel = AppModel.Instance.ReportPassZonesModel;
            }
        }

        /// <summary>
        /// �������� ����� � ������ ������.
        /// </summary>
        protected override void BindData()
        {
            zonesDataTableBindingSource.DataSource = reportPassZonesModel.BindingData;
        }

        /// <summary>
        /// ��������� �� ��������� � ������ ������.
        /// </summary>
        /// <returns>������ - ��������� ���������.</returns>
        protected override bool InnerChangesOccurred()
        {
            return reportPassZonesModel.ChangesOccurred;
        }

        /// <summary>
        /// ��������� ���������.
        /// </summary>
        protected override void AcceptChanges()
        {
            reportPassZonesModel.AcceptChanges();
        }

        /// <summary>
        /// ������ ���������.
        /// </summary>
        protected override void RejectChanges()
        {
            reportPassZonesModel.RejectChanges();
            zonesDataTableBindingSource.ResetBindings(false);
        }

        private void init()
        {
            lblInfo.Text = Resources.PassengerTraffic_SelectZones;
            SelectedCheckBoxColumn.HeaderText = Resources.Mark;
            SelectedCheckBoxColumn.ToolTipText = Resources.ZoneSelecting;
            NameTextBoxColumn.HeaderText = Resources.Title;
            NameTextBoxColumn.ToolTipText = Resources.ZoneTitle;
            DescrTextBoxColumn.HeaderText = Resources.Description;
            DescrTextBoxColumn.ToolTipText = Resources.ZoneDescription;
        }
    }
}
