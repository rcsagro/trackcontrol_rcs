namespace TrackControl.Setting.ReportsCfg
{
  partial class FuelerCfg
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FuelerCfg));
            this.buttonSave = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.mobitelsDataGridView = new System.Windows.Forms.DataGridView();
            this.ColumnNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnMarkAndModel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnModel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.makeCarDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.numberPlateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.teamidDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mobitelidDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.carModelDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.settingidDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.driveridDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.outLinkIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.categoryidDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fuelWayLiterDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fuelMotorLiterDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.identifierDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.categoryid2DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pcbVersionIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vehicleBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.vehicleBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.teletrekLabel = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.dataGridViewFuelerSetting = new System.Windows.Forms.DataGridView();
            this.ColumnRadiusFindTC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnMinDeltaTimeFuelAdd = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnMinFlowmeterInFueler = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.mobitelsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mobitelsDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vehicleBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vehicleBindingNavigator)).BeginInit();
            this.vehicleBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewFuelerSetting)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mobitelsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonSave
            // 
            this.buttonSave.Location = new System.Drawing.Point(26, 9);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(75, 23);
            this.buttonSave.TabIndex = 6;
            this.buttonSave.Text = "���������";
            this.buttonSave.UseVisualStyleBackColor = true;
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(121, 9);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 7;
            this.buttonCancel.Text = "��������";
            this.buttonCancel.UseVisualStyleBackColor = true;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.splitContainer2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.panel1);
            this.splitContainer1.Size = new System.Drawing.Size(919, 441);
            this.splitContainer1.SplitterDistance = 396;
            this.splitContainer1.TabIndex = 8;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.mobitelsDataGridView);
            this.splitContainer2.Panel1.Controls.Add(this.vehicleBindingNavigator);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.dataGridViewFuelerSetting);
            this.splitContainer2.Size = new System.Drawing.Size(919, 396);
            this.splitContainer2.SplitterDistance = 405;
            this.splitContainer2.TabIndex = 6;
            // 
            // mobitelsDataGridView
            // 
            this.mobitelsDataGridView.AllowUserToAddRows = false;
            this.mobitelsDataGridView.AllowUserToDeleteRows = false;
            this.mobitelsDataGridView.AllowUserToResizeRows = false;
            this.mobitelsDataGridView.AutoGenerateColumns = false;
            this.mobitelsDataGridView.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            this.mobitelsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.mobitelsDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnNumber,
            this.ColumnMarkAndModel,
            this.ColumnModel,
            this.idDataGridViewTextBoxColumn,
            this.makeCarDataGridViewTextBoxColumn,
            this.numberPlateDataGridViewTextBoxColumn,
            this.teamidDataGridViewTextBoxColumn,
            this.mobitelidDataGridViewTextBoxColumn,
            this.carModelDataGridViewTextBoxColumn,
            this.settingidDataGridViewTextBoxColumn,
            this.driveridDataGridViewTextBoxColumn,
            this.outLinkIdDataGridViewTextBoxColumn,
            this.categoryidDataGridViewTextBoxColumn,
            this.fuelWayLiterDataGridViewTextBoxColumn,
            this.fuelMotorLiterDataGridViewTextBoxColumn,
            this.identifierDataGridViewTextBoxColumn,
            this.categoryid2DataGridViewTextBoxColumn,
            this.pcbVersionIdDataGridViewTextBoxColumn});
            this.mobitelsDataGridView.DataSource = this.vehicleBindingSource;
            this.mobitelsDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mobitelsDataGridView.Location = new System.Drawing.Point(0, 25);
            this.mobitelsDataGridView.MultiSelect = false;
            this.mobitelsDataGridView.Name = "mobitelsDataGridView";
            this.mobitelsDataGridView.ReadOnly = true;
            this.mobitelsDataGridView.RowHeadersVisible = false;
            this.mobitelsDataGridView.RowHeadersWidth = 28;
            this.mobitelsDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.mobitelsDataGridView.Size = new System.Drawing.Size(405, 371);
            this.mobitelsDataGridView.TabIndex = 2;
            // 
            // ColumnNumber
            // 
            this.ColumnNumber.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.ColumnNumber.DataPropertyName = "NumberPlate";
            this.ColumnNumber.FillWeight = 60F;
            this.ColumnNumber.HeaderText = "�����";
            this.ColumnNumber.Name = "ColumnNumber";
            this.ColumnNumber.ReadOnly = true;
            this.ColumnNumber.Width = 60;
            // 
            // ColumnMarkAndModel
            // 
            this.ColumnMarkAndModel.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ColumnMarkAndModel.DataPropertyName = "MakeCar";
            this.ColumnMarkAndModel.HeaderText = "�����";
            this.ColumnMarkAndModel.Name = "ColumnMarkAndModel";
            this.ColumnMarkAndModel.ReadOnly = true;
            // 
            // ColumnModel
            // 
            this.ColumnModel.DataPropertyName = "CarModel";
            this.ColumnModel.HeaderText = "������";
            this.ColumnModel.Name = "ColumnModel";
            this.ColumnModel.ReadOnly = true;
            // 
            // idDataGridViewTextBoxColumn
            // 
            this.idDataGridViewTextBoxColumn.DataPropertyName = "id";
            this.idDataGridViewTextBoxColumn.HeaderText = "id";
            this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
            this.idDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // makeCarDataGridViewTextBoxColumn
            // 
            this.makeCarDataGridViewTextBoxColumn.DataPropertyName = "MakeCar";
            this.makeCarDataGridViewTextBoxColumn.HeaderText = "MakeCar";
            this.makeCarDataGridViewTextBoxColumn.Name = "makeCarDataGridViewTextBoxColumn";
            this.makeCarDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // numberPlateDataGridViewTextBoxColumn
            // 
            this.numberPlateDataGridViewTextBoxColumn.DataPropertyName = "NumberPlate";
            this.numberPlateDataGridViewTextBoxColumn.HeaderText = "NumberPlate";
            this.numberPlateDataGridViewTextBoxColumn.Name = "numberPlateDataGridViewTextBoxColumn";
            this.numberPlateDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // teamidDataGridViewTextBoxColumn
            // 
            this.teamidDataGridViewTextBoxColumn.DataPropertyName = "Team_id";
            this.teamidDataGridViewTextBoxColumn.HeaderText = "Team_id";
            this.teamidDataGridViewTextBoxColumn.Name = "teamidDataGridViewTextBoxColumn";
            this.teamidDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // mobitelidDataGridViewTextBoxColumn
            // 
            this.mobitelidDataGridViewTextBoxColumn.DataPropertyName = "Mobitel_id";
            this.mobitelidDataGridViewTextBoxColumn.HeaderText = "Mobitel_id";
            this.mobitelidDataGridViewTextBoxColumn.Name = "mobitelidDataGridViewTextBoxColumn";
            this.mobitelidDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // carModelDataGridViewTextBoxColumn
            // 
            this.carModelDataGridViewTextBoxColumn.DataPropertyName = "CarModel";
            this.carModelDataGridViewTextBoxColumn.HeaderText = "CarModel";
            this.carModelDataGridViewTextBoxColumn.Name = "carModelDataGridViewTextBoxColumn";
            this.carModelDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // settingidDataGridViewTextBoxColumn
            // 
            this.settingidDataGridViewTextBoxColumn.DataPropertyName = "setting_id";
            this.settingidDataGridViewTextBoxColumn.HeaderText = "setting_id";
            this.settingidDataGridViewTextBoxColumn.Name = "settingidDataGridViewTextBoxColumn";
            this.settingidDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // driveridDataGridViewTextBoxColumn
            // 
            this.driveridDataGridViewTextBoxColumn.DataPropertyName = "driver_id";
            this.driveridDataGridViewTextBoxColumn.HeaderText = "driver_id";
            this.driveridDataGridViewTextBoxColumn.Name = "driveridDataGridViewTextBoxColumn";
            this.driveridDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // outLinkIdDataGridViewTextBoxColumn
            // 
            this.outLinkIdDataGridViewTextBoxColumn.DataPropertyName = "OutLinkId";
            this.outLinkIdDataGridViewTextBoxColumn.HeaderText = "OutLinkId";
            this.outLinkIdDataGridViewTextBoxColumn.Name = "outLinkIdDataGridViewTextBoxColumn";
            this.outLinkIdDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // categoryidDataGridViewTextBoxColumn
            // 
            this.categoryidDataGridViewTextBoxColumn.DataPropertyName = "Category_id";
            this.categoryidDataGridViewTextBoxColumn.HeaderText = "Category_id";
            this.categoryidDataGridViewTextBoxColumn.Name = "categoryidDataGridViewTextBoxColumn";
            this.categoryidDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // fuelWayLiterDataGridViewTextBoxColumn
            // 
            this.fuelWayLiterDataGridViewTextBoxColumn.DataPropertyName = "FuelWayLiter";
            this.fuelWayLiterDataGridViewTextBoxColumn.HeaderText = "FuelWayLiter";
            this.fuelWayLiterDataGridViewTextBoxColumn.Name = "fuelWayLiterDataGridViewTextBoxColumn";
            this.fuelWayLiterDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // fuelMotorLiterDataGridViewTextBoxColumn
            // 
            this.fuelMotorLiterDataGridViewTextBoxColumn.DataPropertyName = "FuelMotorLiter";
            this.fuelMotorLiterDataGridViewTextBoxColumn.HeaderText = "FuelMotorLiter";
            this.fuelMotorLiterDataGridViewTextBoxColumn.Name = "fuelMotorLiterDataGridViewTextBoxColumn";
            this.fuelMotorLiterDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // identifierDataGridViewTextBoxColumn
            // 
            this.identifierDataGridViewTextBoxColumn.DataPropertyName = "Identifier";
            this.identifierDataGridViewTextBoxColumn.HeaderText = "Identifier";
            this.identifierDataGridViewTextBoxColumn.Name = "identifierDataGridViewTextBoxColumn";
            this.identifierDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // categoryid2DataGridViewTextBoxColumn
            // 
            this.categoryid2DataGridViewTextBoxColumn.DataPropertyName = "Category_id2";
            this.categoryid2DataGridViewTextBoxColumn.HeaderText = "Category_id2";
            this.categoryid2DataGridViewTextBoxColumn.Name = "categoryid2DataGridViewTextBoxColumn";
            this.categoryid2DataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // pcbVersionIdDataGridViewTextBoxColumn
            // 
            this.pcbVersionIdDataGridViewTextBoxColumn.DataPropertyName = "PcbVersionId";
            this.pcbVersionIdDataGridViewTextBoxColumn.HeaderText = "PcbVersionId";
            this.pcbVersionIdDataGridViewTextBoxColumn.Name = "pcbVersionIdDataGridViewTextBoxColumn";
            this.pcbVersionIdDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // vehicleBindingSource
            // 
            this.vehicleBindingSource.DataMember = "vehicle";
            this.vehicleBindingSource.DataSource = typeof(LocalCache.atlantaDataSet);
            this.vehicleBindingSource.Sort = "Mobitel_id";
            // 
            // vehicleBindingNavigator
            // 
            this.vehicleBindingNavigator.AddNewItem = null;
            this.vehicleBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.vehicleBindingNavigator.DeleteItem = null;
            this.vehicleBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.teletrekLabel,
            this.toolStripSeparator1,
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2});
            this.vehicleBindingNavigator.Location = new System.Drawing.Point(0, 0);
            this.vehicleBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.vehicleBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.vehicleBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.vehicleBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.vehicleBindingNavigator.Name = "vehicleBindingNavigator";
            this.vehicleBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.vehicleBindingNavigator.Size = new System.Drawing.Size(405, 25);
            this.vehicleBindingNavigator.TabIndex = 3;
            this.vehicleBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(43, 22);
            this.bindingNavigatorCountItem.Text = "��� {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // teletrekLabel
            // 
            this.teletrekLabel.Name = "teletrekLabel";
            this.teletrekLabel.Size = new System.Drawing.Size(64, 22);
            this.teletrekLabel.Text = "���������";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 21);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // dataGridViewFuelerSetting
            // 
            this.dataGridViewFuelerSetting.AllowUserToAddRows = false;
            this.dataGridViewFuelerSetting.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewFuelerSetting.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnRadiusFindTC,
            this.ColumnMinDeltaTimeFuelAdd,
            this.ColumnMinFlowmeterInFueler});
            this.dataGridViewFuelerSetting.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewFuelerSetting.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewFuelerSetting.Name = "dataGridViewFuelerSetting";
            this.dataGridViewFuelerSetting.RowHeadersVisible = false;
            this.dataGridViewFuelerSetting.Size = new System.Drawing.Size(510, 396);
            this.dataGridViewFuelerSetting.TabIndex = 0;
            // 
            // ColumnRadiusFindTC
            // 
            this.ColumnRadiusFindTC.FillWeight = 120F;
            this.ColumnRadiusFindTC.HeaderText = "������ ������ ������������ ��, �";
            this.ColumnRadiusFindTC.Name = "ColumnRadiusFindTC";
            this.ColumnRadiusFindTC.Width = 120;
            // 
            // ColumnMinDeltaTimeFuelAdd
            // 
            this.ColumnMinDeltaTimeFuelAdd.FillWeight = 120F;
            this.ColumnMinDeltaTimeFuelAdd.HeaderText = "����������� ���� ����� ����������, ������";
            this.ColumnMinDeltaTimeFuelAdd.Name = "ColumnMinDeltaTimeFuelAdd";
            this.ColumnMinDeltaTimeFuelAdd.Width = 120;
            // 
            // ColumnMinFlowmeterInFueler
            // 
            this.ColumnMinFlowmeterInFueler.FillWeight = 120F;
            this.ColumnMinFlowmeterInFueler.HeaderText = "����������� ������ ��� ��������, �";
            this.ColumnMinFlowmeterInFueler.Name = "ColumnMinFlowmeterInFueler";
            this.ColumnMinFlowmeterInFueler.Width = 120;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.buttonSave);
            this.panel1.Controls.Add(this.buttonCancel);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(708, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(211, 41);
            this.panel1.TabIndex = 8;
            // 
            // mobitelsBindingSource
            // 
            this.mobitelsBindingSource.DataMember = "mobitels";
            this.mobitelsBindingSource.DataSource = typeof(LocalCache.atlantaDataSet);
            this.mobitelsBindingSource.Sort = "Mobitel_ID";
            // 
            // FuelerCfg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainer1);
            this.Name = "FuelerCfg";
            this.Size = new System.Drawing.Size(919, 441);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel1.PerformLayout();
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mobitelsDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vehicleBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vehicleBindingNavigator)).EndInit();
            this.vehicleBindingNavigator.ResumeLayout(false);
            this.vehicleBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewFuelerSetting)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mobitelsBindingSource)).EndInit();
            this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Button buttonSave;
    private System.Windows.Forms.Button buttonCancel;
    private System.Windows.Forms.SplitContainer splitContainer1;
    private System.Windows.Forms.Panel panel1;
    private System.Windows.Forms.SplitContainer splitContainer2;
    private System.Windows.Forms.DataGridView dataGridViewFuelerSetting;
    private System.Windows.Forms.BindingNavigator vehicleBindingNavigator;
    private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
    private System.Windows.Forms.ToolStripLabel teletrekLabel;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
    private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
    private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
    private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
    private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
    private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
    private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
    private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
    private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
    private System.Windows.Forms.DataGridView mobitelsDataGridView;
    private System.Windows.Forms.BindingSource vehicleBindingSource;
    private System.Windows.Forms.BindingSource mobitelsBindingSource;
    private System.Windows.Forms.DataGridViewTextBoxColumn ColumnRadiusFindTC;
    private System.Windows.Forms.DataGridViewTextBoxColumn ColumnMinDeltaTimeFuelAdd;
    private System.Windows.Forms.DataGridViewTextBoxColumn ColumnMinFlowmeterInFueler;
    private System.Windows.Forms.DataGridViewTextBoxColumn ColumnNumber;
    private System.Windows.Forms.DataGridViewTextBoxColumn ColumnMarkAndModel;
    private System.Windows.Forms.DataGridViewTextBoxColumn ColumnModel;
    private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn makeCarDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn numberPlateDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn teamidDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn mobitelidDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn carModelDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn settingidDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn driveridDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn outLinkIdDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn categoryidDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn fuelWayLiterDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn fuelMotorLiterDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn identifierDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn categoryid2DataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn pcbVersionIdDataGridViewTextBoxColumn;
  }
}
