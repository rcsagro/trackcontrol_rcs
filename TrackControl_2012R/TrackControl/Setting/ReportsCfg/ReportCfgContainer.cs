using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.ComponentModel;
using TrackControl.Properties;

namespace TrackControl.Setting.ReportsCfg
{
    /// <summary>
    /// ��������� ��� �������������� �������. 
    /// ��������������� ������� �������� ������������ UserControl, ������� �������� ���������� 
    /// <see cref="TrackControl.Setting.ReportsCfg.BaseReportCfg"/>
    /// <para>������ ������������ ������, ����������, �� �� �����������, 
    /// �������� ���� �������� � tabControl.</para>
    /// <para>������������ ������ ��� ReportCfgContainer ������ ��������� AdminForm.</para> 
    /// </summary>
    public partial class ReportCfgContainer : UserControl
    {
        public ReportCfgContainer()
        {
            InitializeComponent();
            init();
            SubscribeTabPageValidating();
        }

        /// <summary>
        /// ����� �������� ��������� ����������� ��������� IReportCfg. 
        /// ������������ ��������.
        /// </summary>
        /// <param name="parent">�������-��������� �����, ������� � �������� 
        /// ����� ���������� �������� ��������.</param>
        /// <returns>IEnumerable &lt; IReportCfg &gt;</returns>
        private IEnumerable<IReportCfg> GetReportConfigurators(System.Windows.Forms.Control parent)
        {
            foreach (System.Windows.Forms.Control ctrl in parent.Controls)
            {
                if (ctrl is IReportCfg)
                {
                    yield return (IReportCfg) ctrl;
                }
                else
                {
                    foreach (IReportCfg reportCfg in GetReportConfigurators(ctrl))
                    {
                        yield return reportCfg;
                    }
                }
            }
        }

        /// <summary>
        /// �������� �� ������� Validating � ���� ������� tabControl-�
        /// </summary>
        private void SubscribeTabPageValidating()
        {
            foreach (TabPage page in tabControl.TabPages)
            {
                page.Validating += new CancelEventHandler(TabPageValidating);
                page.CausesValidation = true;
            }
        }

        /// <summary>
        /// ��������� ������� Validating �������� �������� tabControl-� 
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">CancelEventArgs</param>
        private void TabPageValidating(object sender, CancelEventArgs e)
        {
            foreach (IReportCfg cfg in GetReportConfigurators(tabControl.SelectedTab))
            {
                if (cfg.ChangesOccurred())
                {
                    e.Cancel = true;
                    break;
                }
            }
        }

        /// <summary>
        /// ��������� ������� ��������� �������� ������� �������� tabControl-�.
        /// ������������� ������� �������� ������������� �����, ����� ���������
        /// Validating ��� ����� ��������.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        private void tabControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            tabControl.SelectedTab.Focus();
        }

        /// <summary>
        /// ��������� ������� Enter.
        /// ������������� ������� �������� ������������� �����, ����� ���������
        /// Validating ��� ����� ��������.
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        private void tabControl_Enter(object sender, EventArgs e)
        {
            tabControl.SelectedTab.Focus();
        }

        private void init()
        {
            tabpPassengers.Text = Resources.PassengerTraffic;
            tabpFueler.Text = Resources.Fueler;
        }
    }
}
