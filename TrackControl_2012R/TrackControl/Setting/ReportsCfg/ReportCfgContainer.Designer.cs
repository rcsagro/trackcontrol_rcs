namespace TrackControl.Setting.ReportsCfg
{
  partial class ReportCfgContainer
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabpPassengers = new System.Windows.Forms.TabPage();
            this.passengerTrafficCfg = new TrackControl.Setting.ReportsCfg.PassengerTrafficCfg();
            this.tabpFueler = new System.Windows.Forms.TabPage();
            this.fuelerCfg = new TrackControl.Setting.ReportsCfg.FuelerCfg();
            this.tabControl.SuspendLayout();
            this.tabpPassengers.SuspendLayout();
            this.tabpFueler.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabpPassengers);
            this.tabControl.Controls.Add(this.tabpFueler);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(716, 426);
            this.tabControl.TabIndex = 1;
            this.tabControl.SelectedIndexChanged += new System.EventHandler(this.tabControl_SelectedIndexChanged);
            this.tabControl.Enter += new System.EventHandler(this.tabControl_Enter);
            // 
            // tabpPassengers
            // 
            this.tabpPassengers.Controls.Add(this.passengerTrafficCfg);
            this.tabpPassengers.Location = new System.Drawing.Point(4, 22);
            this.tabpPassengers.Name = "tabpPassengers";
            this.tabpPassengers.Padding = new System.Windows.Forms.Padding(3);
            this.tabpPassengers.Size = new System.Drawing.Size(708, 400);
            this.tabpPassengers.TabIndex = 0;
            this.tabpPassengers.Text = "��������������";
            this.tabpPassengers.UseVisualStyleBackColor = true;
            // 
            // passengerTrafficCfg
            // 
            this.passengerTrafficCfg.Dock = System.Windows.Forms.DockStyle.Fill;
            this.passengerTrafficCfg.Location = new System.Drawing.Point(3, 3);
            this.passengerTrafficCfg.MinimumSize = new System.Drawing.Size(300, 200);
            this.passengerTrafficCfg.Name = "passengerTrafficCfg";
            this.passengerTrafficCfg.Size = new System.Drawing.Size(702, 394);
            this.passengerTrafficCfg.TabIndex = 1;
            // 
            // tabpFueler
            // 
            this.tabpFueler.Controls.Add(this.fuelerCfg);
            this.tabpFueler.Location = new System.Drawing.Point(4, 22);
            this.tabpFueler.Name = "tabpFueler";
            this.tabpFueler.Padding = new System.Windows.Forms.Padding(3);
            this.tabpFueler.Size = new System.Drawing.Size(708, 400);
            this.tabpFueler.TabIndex = 1;
            this.tabpFueler.Text = "����������������";
            this.tabpFueler.UseVisualStyleBackColor = true;
            // 
            // fuelerCfg
            // 
            this.fuelerCfg.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fuelerCfg.Location = new System.Drawing.Point(3, 3);
            this.fuelerCfg.Name = "fuelerCfg";
            this.fuelerCfg.Size = new System.Drawing.Size(702, 394);
            this.fuelerCfg.TabIndex = 0;
            // 
            // ReportCfgContainer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tabControl);
            this.Name = "ReportCfgContainer";
            this.Size = new System.Drawing.Size(716, 426);
            this.tabControl.ResumeLayout(false);
            this.tabpPassengers.ResumeLayout(false);
            this.tabpFueler.ResumeLayout(false);
            this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.TabControl tabControl;
    private System.Windows.Forms.TabPage tabpPassengers;
    internal PassengerTrafficCfg passengerTrafficCfg;
    internal FuelerCfg fuelerCfg;
    internal System.Windows.Forms.TabPage tabpFueler;

  }
}
