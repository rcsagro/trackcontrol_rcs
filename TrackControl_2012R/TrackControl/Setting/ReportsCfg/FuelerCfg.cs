using System;
using System.Windows.Forms;
using TrackControl.Properties;

namespace TrackControl.Setting.ReportsCfg
{
    public partial class FuelerCfg : UserControl
    {
        public FuelerCfg()
        {
            InitializeComponent();
            init();
        }

        private void init()
        {
            buttonSave.Text = Resources.Save;
            buttonCancel.Text = Resources.Cancel;
            teletrekLabel.Text = Resources.Mobitels;
            ColumnRadiusFindTC.HeaderText = Resources.FuelerCfg_Radius;
            ColumnMinDeltaTimeFuelAdd.HeaderText = Resources.FuelerCfg_MinInterval;
            ColumnMinFlowmeterInFueler.HeaderText = Resources.FuelerCfg_MinExpense;
            ColumnNumber.HeaderText = Resources.CarNumber;
            ColumnMarkAndModel.HeaderText = Resources.CarMark;
            ColumnModel.HeaderText = Resources.CarModel;

        }
    }
}
