using System;
using System.ComponentModel;
using System.Windows.Forms;
using LocalCache;
using TrackControl.Properties;

namespace TrackControl.Setting.ReportsCfg
{
    /// <summary>
    /// ������� ����� �������������� �������.
    /// <para>������������ ������ ������ ��������� AdminForm.</para>
    /// <para>���������� ���������� ������ InnerChangesOccurred, AcceptChanges, 
    /// RejectChanges, BindDataset.</para>
    /// </summary>
    public partial class BaseReportCfg : UserControl, IReportCfg
    {
        private atlantaDataSet dataset;

        /// <summary>
        /// �������.
        /// </summary>
        protected atlantaDataSet Dataset
        {
            get { return dataset; }
        }

        /// <summary>
        /// ���������� �������� �������� Runtime.
        /// ��� ������ ��������� ����������� ����������
        /// � ������������ ��������� LicenseManager.
        /// </summary>
        protected readonly bool IsRealRuntime;

        public BaseReportCfg()
        {
            InitializeComponent();
            init();
            IsRealRuntime = LicenseManager.UsageMode == LicenseUsageMode.Runtime;
        }

        #region IReportCfg Members

        /// <summary>
        /// ��������� �� ��������� � ������ ������.
        /// </summary>
        /// <returns>������ - ��������� ���������.</returns>
        public bool ChangesOccurred()
        {
            bool result = InnerChangesOccurred();
            if (result)
            {
                ShowChangesWarning();
            }
            return result;
        }

        #endregion

        /// <summary>
        /// The method {0} is not implemented.
        /// </summary>
        private const string NOT_IMPLEMENTED_EXCEPTION = "The method {0} is not implemented.";

        /// <summary>
        /// ����������� ��������� �� ��������� � ������ ������.
        /// ������������� ������������.
        /// </summary>
        /// <exception cref="NotImplementedException">The method is not implemented</exception>
        /// <returns>������ - ��������� ���������.</returns>
        protected virtual bool InnerChangesOccurred()
        {
            throw new NotImplementedException(String.Format(
                NOT_IMPLEMENTED_EXCEPTION, "InnerChangesOccurred"));
        }

        /// <summary>
        /// ��������� ���������.
        /// ������������� ������������.
        /// </summary>
        /// <exception cref="NotImplementedException">The method is not implemented</exception>
        protected virtual void AcceptChanges()
        {
            throw new NotImplementedException(String.Format(
                NOT_IMPLEMENTED_EXCEPTION, "AcceptChanges"));
        }

        /// <summary>
        /// ������ ���������.
        /// ������������� ������������.
        /// </summary>
        /// <exception cref="NotImplementedException">The method is not implemented</exception>
        protected virtual void RejectChanges()
        {
            throw new NotImplementedException(String.Format(
                NOT_IMPLEMENTED_EXCEPTION, "RejectChanges"));
        }

        /// <summary>
        /// ������� Load.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BaseReportCfg_Load(object sender, EventArgs e)
        {
            OnLoad();
        }

        /// <summary>
        /// ���������� ������� Load.
        /// </summary>
        protected virtual void OnLoad()
        {
            if (IsRealRuntime)
            {
                FindDataset();
                BindData();
            }
        }

        /// <summary>
        /// ����� ��������. �� ���� ����� ����� AdminForm.
        /// </summary>
        /// <exception cref="ApplicationException">
        /// ���������� �������� ������ ���� ������ ����� AdminForm</exception>
        private void FindDataset()
        {
            dataset = AppModel.Instance.DataSet;
        }

        /// <summary>
        /// ������� � ������ ����� ������ ������� ����� �������� � ���������.
        /// <para>�������� �������� ����������� � ���� ������ - 
        /// �������� ����� � ������ ������.</para>
        /// ������������� ������������.
        /// </summary>
        protected virtual void BindData()
        {
        }

        
        /// <summary>
        /// ���������� ������� ������� �� ������ "���������". 
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            AcceptChanges();
            HideChangesWarning();
        }

        /// <summary>
        /// ���������� ������� ������� �� ������ "������". 
        /// </summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        private void btnCancel_Click(object sender, EventArgs e)
        {
            RejectChanges();
            HideChangesWarning();
        }

        /// <summary>
        /// ������ �������������� � ��������� ����������
        /// </summary>
        private void HideChangesWarning()
        {
            lblChangesWarning.Visible = false;
            providerChangesWarning.Clear();
        }

        /// <summary>
        /// �������� �������������� � ��������� ����������
        /// </summary>
        private void ShowChangesWarning()
        {
            lblChangesWarning.Visible = true;
            providerChangesWarning.SetError(lblChangesWarning, Resources.BrConfig_PessSaveOrCancel);
        }

        private void init()
        {
            lblChangesWarning.Text = String.Format("      {0}", Resources.BrConfig_DoSaveOrCancel);
            btnCancel.Text = Resources.Cancel;
            btnSave.Text = Resources.Save;

        }
    }
}
