namespace TrackControl.Setting
{
  partial class SensorStatesForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SensorStatesForm));
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
      this._mainTable = new System.Windows.Forms.TableLayoutPanel();
      this.carSensorTable = new System.Windows.Forms.TableLayoutPanel();
      this.carIcon = new System.Windows.Forms.Label();
      this.sensorIcon = new System.Windows.Forms.Label();
      this.carLbl = new System.Windows.Forms.Label();
      this.sensorLbl = new System.Windows.Forms.Label();
      this.carNameText = new System.Windows.Forms.Label();
      this.sensorNameText = new System.Windows.Forms.Label();
      this.stateGrid = new System.Windows.Forms.DataGridView();
      this.titleDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.minValueDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.maxValueDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.Edit = new System.Windows.Forms.DataGridViewImageColumn();
      this.Delete = new System.Windows.Forms.DataGridViewImageColumn();
      this.sensorStatesbindingSource = new System.Windows.Forms.BindingSource(this.components);
      this.editGroupBox = new System.Windows.Forms.GroupBox();
      this.addPanel = new TrackControl.Setting.Controls.AddButtonControl();
      this._mainTable.SuspendLayout();
      this.carSensorTable.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.stateGrid)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.sensorStatesbindingSource)).BeginInit();
      this.SuspendLayout();
      // 
      // _mainTable
      // 
      this._mainTable.BackColor = System.Drawing.Color.Snow;
      this._mainTable.ColumnCount = 3;
      this._mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
      this._mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this._mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
      this._mainTable.Controls.Add(this.carSensorTable, 1, 1);
      this._mainTable.Controls.Add(this.stateGrid, 1, 3);
      this._mainTable.Controls.Add(this.editGroupBox, 1, 5);
      this._mainTable.Dock = System.Windows.Forms.DockStyle.Fill;
      this._mainTable.Location = new System.Drawing.Point(0, 0);
      this._mainTable.Margin = new System.Windows.Forms.Padding(0);
      this._mainTable.Name = "_mainTable";
      this._mainTable.RowCount = 7;
      this._mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 5F));
      this._mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
      this._mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 5F));
      this._mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this._mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 5F));
      this._mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 145F));
      this._mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
      this._mainTable.Size = new System.Drawing.Size(476, 340);
      this._mainTable.TabIndex = 1;
      // 
      // carSensorTable
      // 
      this.carSensorTable.ColumnCount = 3;
      this.carSensorTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 21F));
      this.carSensorTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 86F));
      this.carSensorTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
      this.carSensorTable.Controls.Add(this.carIcon, 0, 0);
      this.carSensorTable.Controls.Add(this.sensorIcon, 0, 1);
      this.carSensorTable.Controls.Add(this.carLbl, 1, 0);
      this.carSensorTable.Controls.Add(this.sensorLbl, 1, 1);
      this.carSensorTable.Controls.Add(this.carNameText, 2, 0);
      this.carSensorTable.Controls.Add(this.sensorNameText, 2, 1);
      this.carSensorTable.Dock = System.Windows.Forms.DockStyle.Fill;
      this.carSensorTable.Location = new System.Drawing.Point(10, 5);
      this.carSensorTable.Margin = new System.Windows.Forms.Padding(0);
      this.carSensorTable.Name = "carSensorTable";
      this.carSensorTable.RowCount = 2;
      this.carSensorTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
      this.carSensorTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
      this.carSensorTable.Size = new System.Drawing.Size(456, 40);
      this.carSensorTable.TabIndex = 0;
      // 
      // carIcon
      // 
      this.carIcon.AutoSize = true;
      this.carIcon.Dock = System.Windows.Forms.DockStyle.Fill;
      this.carIcon.Image = ((System.Drawing.Image)(resources.GetObject("carIcon.Image")));
      this.carIcon.Location = new System.Drawing.Point(0, 0);
      this.carIcon.Margin = new System.Windows.Forms.Padding(0);
      this.carIcon.Name = "carIcon";
      this.carIcon.Size = new System.Drawing.Size(21, 20);
      this.carIcon.TabIndex = 0;
      // 
      // sensorIcon
      // 
      this.sensorIcon.AutoSize = true;
      this.sensorIcon.Dock = System.Windows.Forms.DockStyle.Fill;
      this.sensorIcon.Image = ((System.Drawing.Image)(resources.GetObject("sensorIcon.Image")));
      this.sensorIcon.Location = new System.Drawing.Point(0, 20);
      this.sensorIcon.Margin = new System.Windows.Forms.Padding(0);
      this.sensorIcon.Name = "sensorIcon";
      this.sensorIcon.Size = new System.Drawing.Size(21, 20);
      this.sensorIcon.TabIndex = 1;
      // 
      // carLbl
      // 
      this.carLbl.AutoSize = true;
      this.carLbl.Dock = System.Windows.Forms.DockStyle.Fill;
      this.carLbl.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.carLbl.ForeColor = System.Drawing.Color.DarkOliveGreen;
      this.carLbl.Location = new System.Drawing.Point(21, 0);
      this.carLbl.Margin = new System.Windows.Forms.Padding(0);
      this.carLbl.Name = "carLbl";
      this.carLbl.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
      this.carLbl.Size = new System.Drawing.Size(86, 20);
      this.carLbl.TabIndex = 2;
      this.carLbl.Text = "����������:";
      this.carLbl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // sensorLbl
      // 
      this.sensorLbl.AutoSize = true;
      this.sensorLbl.Dock = System.Windows.Forms.DockStyle.Fill;
      this.sensorLbl.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.sensorLbl.ForeColor = System.Drawing.Color.DarkSlateGray;
      this.sensorLbl.Location = new System.Drawing.Point(21, 20);
      this.sensorLbl.Margin = new System.Windows.Forms.Padding(0);
      this.sensorLbl.Name = "sensorLbl";
      this.sensorLbl.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
      this.sensorLbl.Size = new System.Drawing.Size(86, 20);
      this.sensorLbl.TabIndex = 3;
      this.sensorLbl.Text = "������:";
      this.sensorLbl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // carNameText
      // 
      this.carNameText.AutoSize = true;
      this.carNameText.Dock = System.Windows.Forms.DockStyle.Fill;
      this.carNameText.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.carNameText.ForeColor = System.Drawing.Color.Black;
      this.carNameText.Location = new System.Drawing.Point(107, 0);
      this.carNameText.Margin = new System.Windows.Forms.Padding(0);
      this.carNameText.Name = "carNameText";
      this.carNameText.Size = new System.Drawing.Size(349, 20);
      this.carNameText.TabIndex = 4;
      this.carNameText.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // sensorNameText
      // 
      this.sensorNameText.AutoSize = true;
      this.sensorNameText.Dock = System.Windows.Forms.DockStyle.Fill;
      this.sensorNameText.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.sensorNameText.ForeColor = System.Drawing.Color.Black;
      this.sensorNameText.Location = new System.Drawing.Point(107, 20);
      this.sensorNameText.Margin = new System.Windows.Forms.Padding(0);
      this.sensorNameText.Name = "sensorNameText";
      this.sensorNameText.Size = new System.Drawing.Size(349, 20);
      this.sensorNameText.TabIndex = 5;
      this.sensorNameText.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // stateGrid
      // 
      this.stateGrid.AllowUserToAddRows = false;
      this.stateGrid.AllowUserToDeleteRows = false;
      this.stateGrid.AllowUserToResizeColumns = false;
      this.stateGrid.AllowUserToResizeRows = false;
      this.stateGrid.AutoGenerateColumns = false;
      this.stateGrid.BackgroundColor = System.Drawing.Color.Gray;
      this.stateGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.stateGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.titleDataGridViewTextBoxColumn,
            this.minValueDataGridViewTextBoxColumn,
            this.maxValueDataGridViewTextBoxColumn,
            this.Edit,
            this.Delete});
      this.stateGrid.DataSource = this.sensorStatesbindingSource;
      this.stateGrid.Dock = System.Windows.Forms.DockStyle.Fill;
      this.stateGrid.Location = new System.Drawing.Point(10, 50);
      this.stateGrid.Margin = new System.Windows.Forms.Padding(0);
      this.stateGrid.MultiSelect = false;
      this.stateGrid.Name = "stateGrid";
      this.stateGrid.ReadOnly = true;
      this.stateGrid.RowHeadersVisible = false;
      this.stateGrid.RowTemplate.DefaultCellStyle.BackColor = System.Drawing.Color.White;
      this.stateGrid.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Black;
      this.stateGrid.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.White;
      this.stateGrid.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black;
      this.stateGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
      this.stateGrid.Size = new System.Drawing.Size(456, 130);
      this.stateGrid.TabIndex = 1;
      this.stateGrid.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.stateGrid_CellFormatting);
      this.stateGrid.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.stateGrid_CellContentClick);
      // 
      // titleDataGridViewTextBoxColumn
      // 
      this.titleDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
      this.titleDataGridViewTextBoxColumn.DataPropertyName = "Title";
      this.titleDataGridViewTextBoxColumn.HeaderText = "��������";
      this.titleDataGridViewTextBoxColumn.MinimumWidth = 150;
      this.titleDataGridViewTextBoxColumn.Name = "titleDataGridViewTextBoxColumn";
      this.titleDataGridViewTextBoxColumn.ReadOnly = true;
      // 
      // minValueDataGridViewTextBoxColumn
      // 
      this.minValueDataGridViewTextBoxColumn.DataPropertyName = "MinValue";
      dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
      dataGridViewCellStyle3.Format = "N2";
      dataGridViewCellStyle3.NullValue = null;
      this.minValueDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle3;
      this.minValueDataGridViewTextBoxColumn.HeaderText = "������ �����";
      this.minValueDataGridViewTextBoxColumn.MinimumWidth = 60;
      this.minValueDataGridViewTextBoxColumn.Name = "minValueDataGridViewTextBoxColumn";
      this.minValueDataGridViewTextBoxColumn.ReadOnly = true;
      this.minValueDataGridViewTextBoxColumn.Width = 60;
      // 
      // maxValueDataGridViewTextBoxColumn
      // 
      this.maxValueDataGridViewTextBoxColumn.DataPropertyName = "MaxValue";
      dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
      dataGridViewCellStyle4.Format = "N2";
      dataGridViewCellStyle4.NullValue = null;
      this.maxValueDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle4;
      this.maxValueDataGridViewTextBoxColumn.HeaderText = "������� �����";
      this.maxValueDataGridViewTextBoxColumn.MinimumWidth = 60;
      this.maxValueDataGridViewTextBoxColumn.Name = "maxValueDataGridViewTextBoxColumn";
      this.maxValueDataGridViewTextBoxColumn.ReadOnly = true;
      this.maxValueDataGridViewTextBoxColumn.Width = 60;
      // 
      // Edit
      // 
      this.Edit.HeaderText = "";
      this.Edit.Image = TrackControl.General.Shared.EditSensor;
      this.Edit.MinimumWidth = 24;
      this.Edit.Name = "Edit";
      this.Edit.ReadOnly = true;
      this.Edit.Width = 24;
      // 
      // Delete
      // 
      this.Delete.HeaderText = "";
      this.Delete.Image = TrackControl.General.Shared.DeleteSensor;
      this.Delete.MinimumWidth = 24;
      this.Delete.Name = "Delete";
      this.Delete.ReadOnly = true;
      this.Delete.Width = 24;
      // 
      // sensorStatesbindingSource
      // 
      this.sensorStatesbindingSource.DataMember = "State";
      this.sensorStatesbindingSource.DataSource = typeof(LocalCache.atlantaDataSet);
      // 
      // editGroupBox
      // 
      this.editGroupBox.BackColor = System.Drawing.Color.Transparent;
      this.editGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
      this.editGroupBox.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
      this.editGroupBox.ForeColor = System.Drawing.Color.Navy;
      this.editGroupBox.Location = new System.Drawing.Point(10, 185);
      this.editGroupBox.Margin = new System.Windows.Forms.Padding(0);
      this.editGroupBox.Name = "editGroupBox";
      this.editGroupBox.Padding = new System.Windows.Forms.Padding(5, 0, 5, 5);
      this.editGroupBox.Size = new System.Drawing.Size(456, 145);
      this.editGroupBox.TabIndex = 3;
      this.editGroupBox.TabStop = false;
      // 
      // addPanel
      // 
      this.addPanel.BackColor = System.Drawing.Color.Transparent;
      this.addPanel.Dock = System.Windows.Forms.DockStyle.Fill;
      this.addPanel.Location = new System.Drawing.Point(0, 0);
      this.addPanel.Name = "addPanel";
      this.addPanel.Size = new System.Drawing.Size(430, 100);
      this.addPanel.TabIndex = 0;
      this.addPanel.AddPressed += new System.EventHandler(this.addPanel_AddPressed);
      this.addPanel.BackPressed += new System.EventHandler(this.closeBtn_Click);
      // 
      // SensorStatesForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(476, 340);
      this.Controls.Add(this._mainTable);
      this.DoubleBuffered = true;
      this.MaximizeBox = false;
      this.MaximumSize = new System.Drawing.Size(484, 375);
      this.MinimizeBox = false;
      this.MinimumSize = new System.Drawing.Size(484, 375);
      this.Name = "SensorStatesForm";
      this.ShowIcon = false;
      this.ShowInTaskbar = false;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "��������� ��� �������";
      this.Load += new System.EventHandler(this.this_Load);
      this._mainTable.ResumeLayout(false);
      this.carSensorTable.ResumeLayout(false);
      this.carSensorTable.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.stateGrid)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.sensorStatesbindingSource)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.TableLayoutPanel _mainTable;
    private System.Windows.Forms.TableLayoutPanel carSensorTable;
    private System.Windows.Forms.Label carIcon;
    private System.Windows.Forms.Label sensorIcon;
    private System.Windows.Forms.Label carLbl;
    private System.Windows.Forms.Label sensorLbl;
    private System.Windows.Forms.Label carNameText;
    private System.Windows.Forms.Label sensorNameText;
    private System.Windows.Forms.BindingSource sensorStatesbindingSource;
    private System.Windows.Forms.DataGridView stateGrid;
    private System.Windows.Forms.GroupBox editGroupBox;
    private TrackControl.Setting.Controls.AddButtonControl addPanel;
    private System.Windows.Forms.DataGridViewTextBoxColumn titleDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn minValueDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn maxValueDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewImageColumn Edit;
    private System.Windows.Forms.DataGridViewImageColumn Delete;
  }
}