using System;
using System.Collections.Generic;
using System.Text;

namespace SensorAdministratorCAN
{
    public enum KOD_CMD
    {
        REQ = 0x51, //cmd request
        REP = 0x61, //cmd reply
        CMD_GET = 0xC0,
        CMD_SET = 0x90,
        OK_CMD_SET = 0x03,
        NO_CMD_SET = 0x60,
        ERROR_CMD = 0xFE,
        CMD_BASE = 0x01, //can & rs485 speeds, execution +...??
        CMD_SPI = 0x02,
        CMD_CAN = 0x03,
        CMD_RS485 = 0x04,
        CMD_AIN = 0x05,
        CMD_DIN = 0x06,
        CMD_DOUT = 0x07
    };

    #region common_setting

    public class MDAC
    {
        public MDAC()
        {
            multiply = 1;
            divider = 1;
        }

        public byte multiply;
        public byte divider;

        public static int GetSize()
        {
            return 2; //multiply+divider
        }
    };

    public class sPorog
    {
        public sPorog()
        {
            level = 0;
            direct = 0;
        }

        public UInt32 level; //�����. ������������
        public byte direct; //����������� ����������� ������ 0-���� 1-�����

        public static int GetSize()
        {
            return 5 + 3; //level(4)+direct //+3 ��� ���������� �� ����� �������� 4Bt
        }
    };

    #endregion

    public class Protocols
    {
        public Protocols(string str, byte value)
        {
            name = str;
            cod = value;
        }

        public string name;
        public byte cod;
    };

    public class CanSpeeds
    {
        public CanSpeeds(string str, UInt32 value)
        {
            name = str;
            codeSpeed = value;
        }

        public string name;
        public UInt32 codeSpeed;
    };

    public class CodeData
    {
        public string name;
        public byte cod;

        public CodeData(string str, byte value_cod)
        {
            name = str;
            cod = value_cod;
        }
    };

    public class CodeDataSource
    {
        private static List<CodeData> listCodeData = new List<CodeData>();

        public CodeDataSource()
        {
            listCodeData.Add(new CodeData("--------", 0xF0));
            listCodeData.Add(new CodeData("DigitalIN", 0x10));
            listCodeData.Add(new CodeData("AnalogIN", 0x20));
            listCodeData.Add(new CodeData("RS485", 0x30));
            listCodeData.Add(new CodeData("CAN", 0x40));
            listCodeData.Add(new CodeData("CNT", 0x60));
        }

        public static string GetDataName(byte value)
        {
            //if (listCodeData.Count == 0)
            //{
            //  listCodeData.Add(new CodeData("--------", 0xF0));
            //  listCodeData.Add(new CodeData("DigitalIN", 0x10));
            //  listCodeData.Add(new CodeData("AnalogIN", 0x20));
            //  listCodeData.Add(new CodeData("RS485", 0x30));
            //  listCodeData.Add(new CodeData("CAN", 0x40));
            //  listCodeData.Add(new CodeData("CNT", 0x60));
            //}

            string ret = "";
            foreach (CodeData cod_data in listCodeData)
            {
                if (cod_data.cod == (value & 0xF0))
                {
                    ret = cod_data.name;
                    break;
                }
            }
            return ret;
        }

        public static byte GetDataCodAddress(byte value)
        {
            return (byte) (value & 0x0F);
        }

        public static byte GetDataCod(string str)
        {
            //if (listCodeData.Count == 0)
            //{
            //  listCodeData.Add(new CodeData("--------", 0xF0));
            //  listCodeData.Add(new CodeData("DigitalIN", 0x10));
            //  listCodeData.Add(new CodeData("AnalogIN", 0x20));
            //  listCodeData.Add(new CodeData("RS485", 0x30));
            //  listCodeData.Add(new CodeData("CAN", 0x40));
            //  listCodeData.Add(new CodeData("CNT", 0x60));
            //}
            byte ret = 0xF0;
            foreach (CodeData cod_data in listCodeData)
            {
                if (cod_data.name == str)
                {
                    ret = cod_data.cod;
                    break;
                }
            }
            return ret;
        }
    };

    public static class CRC
    {
        public static byte CountCRC8(byte[] data, int offset, int len)
        {
            byte crc = 0;
            for (int i = offset; i < len + offset - 1; i++)
                crc ^= data[i];
            return crc;
        }
    }

    [Serializable]
    public class ForSerrialize
    {
        public DeviceTuning devTun = new DeviceTuning();
        public SpiTuning spiTun = new SpiTuning();
        public CanTuning canTun = new CanTuning();
    }

    #region BASE_setting

    public class sSetRs485rx
    {
        public byte protocolCod; //��� ���������
        public byte address;
        public byte startBit;
        public byte lenghtBit;
        public MDAC sMdac;
        public byte timeAverage; //����� ���������� �������� ������
        public sPorog[] porog = new sPorog[2];

        public static int GetSize()
        {
            int ret = 5; //protocolCod+address+startBit+lenghtBit+timeAverage
            ret += MDAC.GetSize();
            ret += (sPorog.GetSize()*2);
            return ret + 1 + 1; //+1 ��� ���������� MDAC+timeAverage �� ����� 4Bt +1 cmd_BASE_SETTING
        }

        public sSetRs485rx()
        {
            sMdac = new MDAC();
            porog[0] = new sPorog();
            porog[1] = new sPorog();
        }
    }

    public class sSetRs485tx
    {
        public UInt16 timeSend; //u16
        public byte lenghtPack;
        public byte[] packet = new byte[16];

        public static int GetSize()
        {
            return 4 + 16 + 1; //+1 cmd_BASE_SETTING
        }
    };

    public class sSetCount
    {
        public byte codeType = 0xff; //��� ���� ������. �� ������ ���� baseCodeType
        public byte regime = 0xff; //����� ������ �������/����������

        public ushort levEvent = 0xffff;
            //������� ������������ ������� ��� ������� - ������� ������ ���������� �������, �������� - ������� ������������� �����

        public static int GetSize()
        {
            return 4 + 1; //+1 cmd_BASE_SETTING
        }
    };

    public class sSetDin
    {
        //u8 regime;           //����� ������ ���������� ����/�������/���������� - ��������� � ������ �����
        public ushort timeToOne = 0xffff; //����� ����������� ������� ��� �������� ��� 1
        public ushort timeToNull = 0xffff; //����� ����������� ������� ��� �������� ��� 0

        public static int GetSize()
        {
            return 4*STR_DEFINE.MAX_DIN + 1; // +1;//
        }
    };

    public class sSetAin
    {
        public byte regime = 0xff; //����� ������ ���/����������
        public byte numAinCanals = 0xff; //����� ����������� ����� ��� ��������� ��������
        public MDAC sMdac;
        public sPorog[] porog = new sPorog[2];

        public static int GetSize()
        {
            return 2 + MDAC.GetSize() + sPorog.GetSize()*2 + 1; //
        }

        public sSetAin()
        {
            sMdac = new MDAC();
            porog[0] = new sPorog();
            porog[1] = new sPorog();
        }
    };

    public class sSetDout
    {
        public byte codeType; //��� ���� ������. �� ������ ���� baseCodeType
        public UInt32 porogStart = 0xffffffff; //������ ����� ������ ������������
        public UInt32 porogEnd = 0xffffffff; //������� ����� ��������� ������������

        public static int GetSize()
        {
            return 12*STR_DEFINE.MAX_DOUT + 1; // 5;//
        }
    };

    public struct STR_DEFINE
    {
        public const int CNT_REGIM_count = 0x1;
        public const int CNT_REGIM_frequency = 0x2;

        public const int SPEED_EXECUT = 0;
        public const int MAX_UART_TX = 8;
        public const int START_UART_TX = (START_UART_RX + MAX_UART);
        public const int MAX_UART = 8;
        public const int START_UART_RX = 1;
        public const int MAX_CNT = 4;
        public const int START_CNT = (START_UART_TX + MAX_UART_TX);
        public const int MAX_DIN = 4;
        public const int START_DIN = (START_CNT + MAX_CNT);
        public const int MAX_AIN = 8;
        public const int START_AIN = (START_DIN + MAX_DIN);
        public const int MAX_DOUT = 2;
        public const int START_DOUT = (START_AIN + MAX_AIN);

        public const int MAX_CAN_CELL = 16; //32;
        public const int MAX_CAN_TX = 8;
        public const int START_CAN_CELL = 0;
        public const int START_CAN_TX = 32;

        public const int LEN_BASE = 29;
    };

    [Serializable]
    public class DeviceTuning
    {
        public const int LEN_EXECUTION = 20;
        public const int AIN_REGIM_adc = 0x01;
        public const int AIN_REGIM_compatator = 0x02;

        public UInt32 can_speedUint32;
            //�������� ������ ����. ���� �������� �FFFFFFFF �� �������� ���������� ��������� ���������

        public string can_speedStr;
        public UInt32 rs485_speed; // �������� ����� 485-�� ����������
        public char[] execution = new char[LEN_EXECUTION]; //20-��������������� �������� ����� ������
        public sSetRs485rx[] ssRs485rx = new sSetRs485rx[STR_DEFINE.MAX_UART];
        public sSetRs485tx[] ssRs485tx = new sSetRs485tx[STR_DEFINE.MAX_UART_TX];
        public sSetCount[] ssCnt = new sSetCount[STR_DEFINE.MAX_CNT];
        public sSetDin[] ssDin = new sSetDin[STR_DEFINE.MAX_DIN];
        public sSetAin[] ssAin = new sSetAin[STR_DEFINE.MAX_AIN];
        public sSetDout[] ssDout = new sSetDout[STR_DEFINE.MAX_DOUT];

        private List<CanSpeeds> list_can_speed;

        public DeviceTuning()
        {
            for (int i = 0; i < STR_DEFINE.MAX_UART; i++)
                ssRs485rx[i] = new sSetRs485rx();
            for (int i = 0; i < STR_DEFINE.MAX_AIN; i++)
                ssAin[i] = new sSetAin();

            list_can_speed = new List<CanSpeeds>();
            list_can_speed.Add(new CanSpeeds("500 kBs", 0x060506));
            list_can_speed.Add(new CanSpeeds("250 kBs", 0x06050C));
            list_can_speed.Add(new CanSpeeds("125 kBs", 0x060518));
            list_can_speed.Add(new CanSpeeds("100 kBs", 0x06051E));
            list_can_speed.Add(new CanSpeeds("factory", 0xFFFFFFFF));
        }

        // private UInt32[] CanSpeedHEX = { 0x060506, 0x06050C, 0x060518, 0x06051E };//500k,250k,125k,100k - �� �������� ��� � ComboboxCanSpeed

        public UInt32 GetCanSpeed(string strSpeed)
        {
            UInt32 ret = 0;
            foreach (CanSpeeds can_s in list_can_speed)
            {
                if (can_s.name == strSpeed)
                {
                    ret = can_s.codeSpeed;
                    break;
                }
            }
            return ret;
        }

        public string GetCanSpeed(UInt32 uintSpeed)
        {
            string retStr = "";
            foreach (CanSpeeds can_s in list_can_speed)
            {
                if (can_s.codeSpeed == uintSpeed)
                {
                    retStr = can_s.name;
                    break;
                }
            }
            return retStr;
        }

        public bool GetSetting(byte[] data)
        {
            bool ret = false;
            if (data[4] == STR_DEFINE.SPEED_EXECUT)
            {
                can_speedUint32 = BitConverter.ToUInt32(data, 5);
                can_speedStr = GetCanSpeed(can_speedUint32);
                rs485_speed = BitConverter.ToUInt32(data, 9);
                Array.Copy(data, 13, execution, 0, LEN_EXECUTION);
            }
            else if (data[4] >= STR_DEFINE.START_UART_RX && data[4] < (STR_DEFINE.MAX_UART + STR_DEFINE.START_UART_RX))
            {
                //int tttt = sSetRs485rx.GetSize();
                int numberCell = data[4] - STR_DEFINE.START_UART_RX;
                ssRs485rx[numberCell] = new sSetRs485rx();
                ssRs485rx[numberCell].protocolCod = data[5];
                ssRs485rx[numberCell].address = data[6];
                ssRs485rx[numberCell].startBit = data[7];
                ssRs485rx[numberCell].lenghtBit = data[8];
                ssRs485rx[numberCell].sMdac.multiply = data[9];
                ssRs485rx[numberCell].sMdac.divider = data[10];
                ssRs485rx[numberCell].timeAverage = data[11];
                ssRs485rx[numberCell].porog[0].level = BitConverter.ToUInt32(data, 13);
                ssRs485rx[numberCell].porog[0].direct = data[17];
                ssRs485rx[numberCell].porog[1].level = BitConverter.ToUInt32(data, 21);
                ssRs485rx[numberCell].porog[1].direct = data[25];
            }
            else if (data[4] >= STR_DEFINE.START_UART_TX && data[4] < (STR_DEFINE.MAX_UART_TX + STR_DEFINE.START_UART_TX))
            {
                int numberCell = data[4] - STR_DEFINE.START_UART_TX;
                ssRs485tx[numberCell] = new sSetRs485tx();
                ssRs485tx[numberCell].timeSend = BitConverter.ToUInt16(data, 5);
                ssRs485tx[numberCell].lenghtPack = data[7];
                for (int i = 0; i < 16; i++)
                    ssRs485tx[numberCell].packet[i] = data[i + 8];
            }
            else if (data[4] >= STR_DEFINE.START_CNT && data[4] < (STR_DEFINE.MAX_CNT + STR_DEFINE.START_CNT))
            {
                int numberCell = data[4] - STR_DEFINE.START_CNT;
                ssCnt[numberCell] = new sSetCount();
                ssCnt[numberCell].codeType = data[5];
                ssCnt[numberCell].regime = data[6];
                ssCnt[numberCell].levEvent = BitConverter.ToUInt16(data, 7);
            }
            else if (data[4] >= STR_DEFINE.START_DIN && data[4] < (STR_DEFINE.START_DIN + STR_DEFINE.MAX_DIN))
            {
                for (int i = 0; i < STR_DEFINE.MAX_DIN; i++)
                {
                    ssDin[i] = new sSetDin();
                    ssDin[i].timeToOne = BitConverter.ToUInt16(data, 5 + i*4);
                    ssDin[i].timeToNull = BitConverter.ToUInt16(data, 7 + i*4);
                }
            }
            else if (data[4] >= STR_DEFINE.START_AIN && data[4] < (STR_DEFINE.START_AIN + STR_DEFINE.MAX_AIN))
            {
                int numberCell = data[4] - STR_DEFINE.START_AIN;
                ssAin[numberCell] = new sSetAin();
                ssAin[numberCell].regime = data[5];
                ssAin[numberCell].numAinCanals = data[6];
                ssAin[numberCell].sMdac.multiply = data[7];
                ssAin[numberCell].sMdac.divider = data[8];
                ssAin[numberCell].porog[0].level = BitConverter.ToUInt32(data, 9);
                ssAin[numberCell].porog[0].direct = data[13];
                ssAin[numberCell].porog[1].level = BitConverter.ToUInt32(data, 17);
                ssAin[numberCell].porog[1].direct = data[21];
            }
            else if (data[4] >= STR_DEFINE.START_DOUT && data[4] < (STR_DEFINE.START_DOUT + STR_DEFINE.MAX_DOUT))
            {
                for (int i = 0; i < STR_DEFINE.MAX_DOUT; i++)
                {
                    ssDout[i] = new sSetDout();
                    ssDout[i].codeType = data[5 + i*12];
                    ssDout[i].porogStart = BitConverter.ToUInt32(data, 9 + i*12);
                    ssDout[i].porogEnd = BitConverter.ToUInt32(data, 13 + i*12);
                }
            }
            return ret;
        }

        public byte[] SetSetting(byte code_data)
        {
            byte[] buff = null;
            if (code_data == STR_DEFINE.SPEED_EXECUT)
            {
                buff = new byte[STR_DEFINE.LEN_BASE + 4 + 1]; // len_data];
                buff[0] = (byte) KOD_CMD.REQ;
                buff[1] = (byte) KOD_CMD.CMD_BASE;
                buff[2] = (byte) KOD_CMD.CMD_SET;
                buff[3] = STR_DEFINE.LEN_BASE; //Len data
                buff[4] = code_data; //STR_DEFINE.SPEED_EXECUT;//Number parammetra in structure

                Array.Copy(BitConverter.GetBytes(can_speedUint32), 0, buff, 5, 4);
                Array.Copy(BitConverter.GetBytes(rs485_speed), 0, buff, 9, 4);
                Array.Copy(Array.ConvertAll<char, byte>(execution, Convert.ToByte), 0, buff, 13, LEN_EXECUTION);

                buff[STR_DEFINE.LEN_BASE + 4] = CRC.CountCRC8(buff, 1, STR_DEFINE.LEN_BASE + 4);
                    //����� ��� ����� ������� ����� - ���� ������
            }
            else if (code_data >= STR_DEFINE.START_UART_RX &&
                     code_data < (STR_DEFINE.MAX_UART + STR_DEFINE.START_UART_RX))
            {
                buff = new byte[sSetRs485rx.GetSize() + 4 + 1]; // len_data];
                buff[0] = (byte) KOD_CMD.REQ;
                buff[1] = (byte) KOD_CMD.CMD_BASE;
                buff[2] = (byte) KOD_CMD.CMD_SET;
                buff[3] = (byte) (sSetRs485rx.GetSize()); //Len data
                buff[4] = code_data;

                int numberCell = code_data - STR_DEFINE.START_UART_RX;
                buff[5] = ssRs485rx[numberCell].protocolCod;
                buff[6] = ssRs485rx[numberCell].address;
                buff[7] = ssRs485rx[numberCell].startBit;
                buff[8] = ssRs485rx[numberCell].lenghtBit;
                buff[9] = ssRs485rx[numberCell].sMdac.multiply;
                buff[10] = ssRs485rx[numberCell].sMdac.divider;
                buff[11] = ssRs485rx[numberCell].timeAverage;
                Array.Copy(BitConverter.GetBytes(ssRs485rx[numberCell].porog[0].level), 0, buff, 13, 4);
                buff[17] = ssRs485rx[numberCell].porog[0].direct;
                Array.Copy(BitConverter.GetBytes(ssRs485rx[numberCell].porog[1].level), 0, buff, 21, 4);
                buff[25] = ssRs485rx[numberCell].porog[1].direct;

                buff[sSetRs485rx.GetSize() + 4] = CRC.CountCRC8(buff, 1, sSetRs485rx.GetSize() + 4); //
            }
            else if (code_data >= STR_DEFINE.START_UART_TX &&
                     code_data < (STR_DEFINE.MAX_UART_TX + STR_DEFINE.START_UART_TX))
            {
                buff = new byte[sSetRs485tx.GetSize() + 4 + 1]; // len_data];
                buff[0] = (byte) KOD_CMD.REQ;
                buff[1] = (byte) KOD_CMD.CMD_BASE;
                buff[2] = (byte) KOD_CMD.CMD_SET;
                buff[3] = (byte) (sSetRs485tx.GetSize()); //Len data
                buff[4] = code_data;

                int numberCell = code_data - STR_DEFINE.START_UART_TX;

                Array.Copy(BitConverter.GetBytes(ssRs485tx[numberCell].timeSend), 0, buff, 5, 2);
                buff[7] = ssRs485tx[numberCell].lenghtPack;
                Array.Copy(ssRs485tx[numberCell].packet, 0, buff, 8, 16);

                buff[sSetRs485tx.GetSize() + 4] = CRC.CountCRC8(buff, 1, sSetRs485tx.GetSize() + 4);
                    //����� ��� ����� ������� ����� - ���� ������
            }
            else if (code_data >= STR_DEFINE.START_CNT && code_data < (STR_DEFINE.MAX_CNT + STR_DEFINE.START_CNT))
            {
                buff = new byte[sSetCount.GetSize() + 4 + 1];
                buff[0] = (byte) KOD_CMD.REQ;
                buff[1] = (byte) KOD_CMD.CMD_BASE;
                buff[2] = (byte) KOD_CMD.CMD_SET;
                buff[3] = (byte) (sSetCount.GetSize());
                buff[4] = code_data;

                int numberCell = code_data - STR_DEFINE.START_CNT;
                buff[5] = ssCnt[numberCell].codeType;
                buff[6] = ssCnt[numberCell].regime;
                Array.Copy(BitConverter.GetBytes(ssCnt[numberCell].levEvent), 0, buff, 7, 2);

                buff[sSetCount.GetSize() + 4] = CRC.CountCRC8(buff, 1, sSetCount.GetSize() + 4);
                    //����� ��� ����� ������� ����� - ���� ������
            }
            else if (code_data >= STR_DEFINE.START_DIN &&
                     code_data < (STR_DEFINE.START_DIN + STR_DEFINE.MAX_DIN))
            {
                buff = new byte[sSetDin.GetSize() + 4 + 1];
                buff[0] = (byte) KOD_CMD.REQ;
                buff[1] = (byte) KOD_CMD.CMD_BASE;
                buff[2] = (byte) KOD_CMD.CMD_SET;
                buff[3] = (byte) (sSetDin.GetSize());
                buff[4] = code_data;

                for (int i = 0; i < STR_DEFINE.MAX_DIN; i++)
                {
                    Array.Copy(BitConverter.GetBytes(ssDin[i].timeToOne), 0, buff, 5 + i*4, 2);
                    Array.Copy(BitConverter.GetBytes(ssDin[i].timeToNull), 0, buff, 7 + i*4, 2);
                }

                buff[sSetDin.GetSize() + 4] = CRC.CountCRC8(buff, 1, sSetDin.GetSize() + 4); //
            }
            else if (code_data >= STR_DEFINE.START_AIN &&
                     code_data < (STR_DEFINE.START_AIN + STR_DEFINE.MAX_AIN))
            {
                buff = new byte[sSetAin.GetSize() + 4 + 1];
                buff[0] = (byte) KOD_CMD.REQ;
                buff[1] = (byte) KOD_CMD.CMD_BASE;
                buff[2] = (byte) KOD_CMD.CMD_SET;
                buff[3] = (byte) (sSetAin.GetSize());
                buff[4] = code_data;

                int numberCell = code_data - STR_DEFINE.START_AIN;
                buff[5] = ssAin[numberCell].regime;
                buff[6] = ssAin[numberCell].numAinCanals;
                buff[7] = ssAin[numberCell].sMdac.multiply;
                buff[8] = ssAin[numberCell].sMdac.divider;
                Array.Copy(BitConverter.GetBytes(ssAin[numberCell].porog[0].level), 0, buff, 9, 4);
                buff[13] = ssAin[numberCell].porog[0].direct;
                Array.Copy(BitConverter.GetBytes(ssAin[numberCell].porog[1].level), 0, buff, 17, 4);
                buff[21] = ssAin[numberCell].porog[1].direct;

                buff[sSetAin.GetSize() + 4] = CRC.CountCRC8(buff, 1, sSetAin.GetSize() + 4); //
            }
            else if (code_data >= STR_DEFINE.START_DOUT &&
                     code_data < (STR_DEFINE.START_DOUT + STR_DEFINE.MAX_DOUT))
            {
                buff = new byte[sSetDout.GetSize() + 4 + 1];
                buff[0] = (byte) KOD_CMD.REQ;
                buff[1] = (byte) KOD_CMD.CMD_BASE;
                buff[2] = (byte) KOD_CMD.CMD_SET;
                buff[3] = (byte) (sSetDout.GetSize());
                buff[4] = code_data;

                for (int i = 0; i < STR_DEFINE.MAX_DOUT; i++)
                {
                    buff[5 + i*12] = ssDout[i].codeType;
                    Array.Copy(BitConverter.GetBytes(ssDout[i].porogStart), 0, buff, 9 + i*12, 4);
                    Array.Copy(BitConverter.GetBytes(ssDout[i].porogEnd), 0, buff, 13 + i*12, 4);
                }
                buff[sSetDout.GetSize() + 4] = CRC.CountCRC8(buff, 1, sSetDout.GetSize() + 4);
                    //
            }
            return buff;
        }
    }

    #endregion

    #region SpiTuning

    public class SpiDataUnit
    {
//��������� ���������� ������
        public byte codeType; //��� ���� ������. �� ������ ���� baseCodeType
        public byte startBit; //��������� ��� ������ � ����
        public byte lenghtBit; //������������ ����� ������ � ����
        public string nameVal; // ��� �������� ��������
        public int numAlgo; // ����� ���������

        public static int GetSize()
        {
            return 3; //codeType+startBit+lenghtBit
        }
    };

    /*#define MAX_SPI_ADDR    4//16
struct SpiTuning
{
    sSetSPI[] ssSpi = new sSetSPI[4];//MAX_SPI_ADDR];
};*/
    [Serializable]
    public class SpiTuning
    {
        public const int numberDataUnit = 15;
        public UInt16 timeSend; //(u16) ������ �������� ������� ������ � ��������
        public byte address; //����� ������
        public SpiDataUnit[] dataUnit = new SpiDataUnit[15];

        public UInt64 mask_evt;
            //u32 mask_evt[2];    //�����, � ������� ��� ��������� ������ ����������� ������� // ��� �������

        public static int GetSize()
        {
            int ret = 12; //timeSend(2)+address+1_����������+mask_evt(8)
            ret += (SpiDataUnit.GetSize()*numberDataUnit);
            return ret;
        }

        public bool GetSetting(byte[] data)
        {
            bool ret = false;
            if (data[4] == 0) //address SPI structure = 0
            {
                timeSend = BitConverter.ToUInt16(data, 5);
                address = data[7];
                for (int i = 0; i < numberDataUnit; i++)
                {
                    dataUnit[i] = new SpiDataUnit();
                    dataUnit[i].codeType = data[3*i + 8];
                    dataUnit[i].startBit = data[3*i + 8 + 1];
                    dataUnit[i].lenghtBit = data[3*i + 8 + 2];
                }
                mask_evt = BitConverter.ToUInt64(data, GetSize() - 8 + 4); //+4 len_cmd
            }
            else
            {
            } //������ ������� ���� �� �����
            return ret;
        }

        public byte[] SetSetting()
        {
            byte[] buff = new byte[SpiTuning.GetSize() + 4 + 1];
            buff[0] = (byte) KOD_CMD.REQ;
            buff[1] = (byte) KOD_CMD.CMD_SPI;
            buff[2] = (byte) KOD_CMD.CMD_SET;
            buff[3] = (byte) SpiTuning.GetSize();
            buff[4] = address; //address SPI structure

            Array.Copy(BitConverter.GetBytes(timeSend), 0, buff, 5, 2);
            buff[7] = address; //address SPI packets

            for (int i = 0; i < numberDataUnit; i++)
            {
                if (dataUnit[i] == null)
                    continue;
                buff[3*i + 8] = dataUnit[i].codeType;
                buff[3*i + 8 + 1] = dataUnit[i].startBit;
                buff[3*i + 8 + 2] = dataUnit[i].lenghtBit;
            }

            Array.Copy(BitConverter.GetBytes(mask_evt), 0, buff, GetSize() - 8 + 4, 8);

            buff[SpiTuning.GetSize() + 4] = CRC.CountCRC8(buff, 1, SpiTuning.GetSize() + 4);
                //����� ��� ����� ������� ����� - ���� ������
            return buff;
        }
    }

    #endregion

    #region CAN_region

    public class sSetCANrx
    {
        public UInt32 address; //����� ��� ������
        public UInt32 mask_address; //����� ��� ��������� ������
        public byte startBit; //��������� ��� ������
        public byte lenghtBit; //����� ������
        public MDAC sMdac; //����� �������������� ������
        public sPorog[] porog = new sPorog[2]; //��� ������ ��� ������������ �������

        public static int GetSize()
        {
            int ret = 10;
            ret += MDAC.GetSize();
            ret += (sPorog.GetSize()*2);
            return ret + 1;
        }

        public sSetCANrx()
        {
            sMdac = new MDAC();
            porog[0] = new sPorog();
            porog[1] = new sPorog();
        }
    }

    [Serializable]
    public class CanTuning
    {
        public sSetCANrx[] ssSetCanRx = new sSetCANrx[STR_DEFINE.MAX_CAN_CELL];

        public CanTuning()
        {
            for (int i = 0; i < STR_DEFINE.MAX_CAN_CELL; i++)
                ssSetCanRx[i] = new sSetCANrx();
        }

        public bool GetSetting(byte[] data)
        {
            bool ret = false;
            if (data[4] >= STR_DEFINE.START_CAN_CELL && data[4] < (STR_DEFINE.MAX_CAN_CELL + STR_DEFINE.START_CAN_CELL))
            {
                int numberCell = data[4] - STR_DEFINE.START_CAN_CELL;
                ssSetCanRx[numberCell] = new sSetCANrx();
                ssSetCanRx[numberCell].address = BitConverter.ToUInt32(data, 5);
                ssSetCanRx[numberCell].mask_address = BitConverter.ToUInt32(data, 9);
                ssSetCanRx[numberCell].startBit = data[13];
                ssSetCanRx[numberCell].lenghtBit = data[14];
                ssSetCanRx[numberCell].sMdac.multiply = data[15];
                ssSetCanRx[numberCell].sMdac.divider = data[16];
                ssSetCanRx[numberCell].porog[0].level = BitConverter.ToUInt32(data, 17);
                ssSetCanRx[numberCell].porog[0].direct = data[21];
                ssSetCanRx[numberCell].porog[1].level = BitConverter.ToUInt32(data, 25);
                ssSetCanRx[numberCell].porog[1].direct = data[29];
            }
            return ret;
        }

        public byte[] SetSetting(int number)
        {
            byte[] buff = new byte[sSetCANrx.GetSize() + 4 + 1];
            buff[0] = (byte) KOD_CMD.REQ;
            buff[1] = (byte) KOD_CMD.CMD_CAN;
            buff[2] = (byte) KOD_CMD.CMD_SET;
            buff[3] = (byte) sSetCANrx.GetSize();
            buff[4] = (byte) (number + STR_DEFINE.START_CAN_CELL); //address structure

            Array.Copy(BitConverter.GetBytes(ssSetCanRx[number].address), 0, buff, 5, 4);
            Array.Copy(BitConverter.GetBytes(ssSetCanRx[number].mask_address), 0, buff, 9, 4);
            buff[13] = ssSetCanRx[number].startBit;
            buff[14] = ssSetCanRx[number].lenghtBit;
            buff[15] = ssSetCanRx[number].sMdac.multiply;
            buff[16] = ssSetCanRx[number].sMdac.divider;
            Array.Copy(BitConverter.GetBytes(ssSetCanRx[number].porog[0].level), 0, buff, 17, 4);
            buff[21] = ssSetCanRx[number].porog[0].direct;
            Array.Copy(BitConverter.GetBytes(ssSetCanRx[number].porog[1].level), 0, buff, 25, 4);
            buff[29] = ssSetCanRx[number].porog[1].direct;

            buff[sSetCANrx.GetSize() + 4] = CRC.CountCRC8(buff, 1, sSetCANrx.GetSize() + 4);
                //����� ��� ����� ������� ����� - ���� ������
            return buff;
        }
    }

    #endregion
}
