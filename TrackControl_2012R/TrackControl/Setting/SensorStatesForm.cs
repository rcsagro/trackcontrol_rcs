using System;
using System.Data;
using System.Windows.Forms;
using TrackControl.Setting.Controls;
using LocalCache;
using LocalCache.atlantaDataSetTableAdapters;
using DevExpress.XtraEditors;
using TrackControl.Properties;

namespace TrackControl.Setting
{
  /// <summary>
  /// ����� ��� �������� ���������, ��������� �� ��������� ��������.
  /// </summary>
  public partial class SensorStatesForm : XtraForm
  {
    /// <summary>
    /// ������� ��� ������� ���������.
    /// </summary>
    private StateTableAdapter _stateAdapter;
    /// <summary>
    /// ������� ��� ������� ��������� (���������).
    /// </summary>
    private TransitionTableAdapter _transitionAdapter;
    /// <summary>
    /// ������� ����������.
    /// </summary>
    private atlantaDataSet _dataset;
    /// <summary>
    /// ������, ��� �������� ������������� ���������.
    /// </summary>
    private atlantaDataSet.sensorsRow _sensor;

    #region .ctor
    /// <summary>
    /// �����������.
    /// </summary>
    /// <param name="stateAdapter">������� ��� ������� ���������</param>
    /// <param name="transitionAdapter">������� ��� ������� ���������</param>
    /// <param name="dataset">������� ����������</param>
    /// <param name="sensor">������, ��� �������� ������������� ���������</param>
    public SensorStatesForm(
      StateTableAdapter stateAdapter,
      TransitionTableAdapter transitionAdapter,
      atlantaDataSet dataset,
      atlantaDataSet.sensorsRow sensor)
    {
      InitializeComponent();
      init();
      clearEditGroupBox();

      _stateAdapter = stateAdapter;
      _transitionAdapter = transitionAdapter;
      _dataset = dataset;
      _sensor = sensor;
    }
    #endregion

    #region Private Methods
    /// <summary>
    /// ���������� ������ �������������� � ��������� ���������.
    /// ���������� ������ � ������� "�������� ���������".
    /// </summary>
    void clearEditGroupBox()
    {
      editGroupBox.Controls.Clear();
      editGroupBox.Controls.Add(addPanel);
      editGroupBox.Text = Resources.StateAdd;
      stateGrid.Enabled = true;
      
    }
    /// <summary>
    /// ���������� ������ �������� ���������.
    /// </summary>
    /// <param name="state">���������, ������� ������������ �������</param>
    void showDeletePanel(atlantaDataSet.StateRow state)
    {
      editGroupBox.Controls.Clear();

      DeleteSensorStateControl deletePanel = new DeleteSensorStateControl(_stateAdapter, _transitionAdapter, _dataset, state);
      deletePanel.EndDelete += deletePanel_EndDelete;
      deletePanel.Dock = DockStyle.Fill;
      editGroupBox.Controls.Add(deletePanel);
      editGroupBox.Text = Resources.StateDeleting;
      stateGrid.Enabled = false;
    }
    /// <summary>
    /// ���������� ������ �������������� ���������.
    /// </summary>
    /// <param name="state">���������, ������� ����� ���������������</param>
    void showEditPanel(atlantaDataSet.StateRow state, bool added)
    {
      editGroupBox.Controls.Clear();
      EditSensorStateControl editPanel = new EditSensorStateControl(_stateAdapter, _dataset, state, added);
      editPanel.EndEdit += editPanel_EndEdit;
      editPanel.Dock = DockStyle.Fill;
      editGroupBox.Controls.Add(editPanel);
      editGroupBox.Text = Resources.StateSetting;
      stateGrid.Enabled = false;
    }
    #endregion

    #region GUI Handlers
    /// <summary>
    /// ������������ ������� Load ������ �����.
    /// </summary>
    void this_Load(object sender, EventArgs e)
    {
      VehicleInfo info = new VehicleInfo(_sensor.mobitelsRow);
      carNameText.Text = info.Info;
      sensorNameText.Text = _sensor.Name;
      sensorStatesbindingSource.DataSource = _dataset;
      sensorStatesbindingSource.DataMember = "State";
      sensorStatesbindingSource.Filter = String.Format("SensorId = {0}", _sensor.id);
    }
    /// <summary>
    /// ������������ ���� �� ������ "��������".
    /// </summary>
    void addPanel_AddPressed(object sender, EventArgs e)
    {
      atlantaDataSet.StateRow state = _dataset.State.NewStateRow();
      state.MinValue = 0.0;
      state.MaxValue = 0.0;
      state.Title = "";
      state.sensorsRow = _sensor;
      _dataset.State.AddStateRow(state);
      showEditPanel(state, true);
    }
    /// <summary>
    /// ������������ ���� �� ������ "�������".
    /// </summary>
    void closeBtn_Click(object sender, EventArgs e)
    {
      Close();
    }
    /// <summary>
    /// ������������ ��������� �������������� ���������.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    void editPanel_EndEdit(object sender, EventArgs e)
    {
      clearEditGroupBox();
    }
    /// <summary>
    /// ��������� ����������� ��������� � ������� ����� ("�������������" � "�������").
    /// </summary>
    void stateGrid_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
    {
      if (e.ColumnIndex == stateGrid.Columns["Edit"].Index)
      {
        DataGridViewCell cell = stateGrid.Rows[e.RowIndex].Cells[e.ColumnIndex];
        cell.ToolTipText = Resources.StateEdit;
      }
      if (e.ColumnIndex == stateGrid.Columns["Delete"].Index)
      {
        DataGridViewCell cell = stateGrid.Rows[e.RowIndex].Cells[e.ColumnIndex];
        cell.ToolTipText = Resources.StateDelete;
      }
    }
    /// <summary>
    /// ������������ ���� �� ������� ����� ("�������������" � "�������").
    /// </summary>
    void stateGrid_CellContentClick(object sender, DataGridViewCellEventArgs e)
    {
      if (sensorStatesbindingSource.Current == null)
        return;

      atlantaDataSet.StateRow state = (atlantaDataSet.StateRow)((DataRowView)sensorStatesbindingSource.Current).Row;
      if (e.ColumnIndex == stateGrid.Columns["Edit"].Index)
      {
        showEditPanel(state, false);
      }
      if (e.ColumnIndex == stateGrid.Columns["Delete"].Index)
      {
        showDeletePanel(state);
      }
    }
    /// <summary>
    /// ������������ ������� ��������� �������� ��������� �������.
    /// </summary>
    void deletePanel_EndDelete(object sender, EventArgs e)
    {
      clearEditGroupBox();
    }
    #endregion

    void init()
    {
      carLbl.Text = String.Format("{0}:", Resources.Car);
      sensorLbl.Text = String.Format("{0}:", Resources.Sensor);
      titleDataGridViewTextBoxColumn.HeaderText = Resources.Description;
      minValueDataGridViewTextBoxColumn.HeaderText = Resources.LimitMin;
      maxValueDataGridViewTextBoxColumn.HeaderText = Resources.LimitMax;
      Text = Resources.SensorStateForm;
    }
  }
}