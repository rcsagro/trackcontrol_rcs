using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using LocalCache;

namespace GPSControl.Setting
{
  public partial class SettingForm : Form
  {
    public SettingForm(int s_id, atlantaDataSet dataset)
    {
      InitializeComponent();
      atlantaDataSet = dataset;
      settingBindingSource.DataSource = atlantaDataSet.setting;
      settingBindingSource.Filter = "id=" + s_id.ToString();
      
    }

    private void settingBindingNavigatorSaveItem_Click(object sender, EventArgs e)
    {
      this.Validate();
      this.settingBindingSource.EndEdit();
      this.settingTableAdapter.Update(this.atlantaDataSet.setting);

    }

    private void SettingForm_Load(object sender, EventArgs e)
    {
      // TODO: This line of code loads data into the 'atlantaDataSet.setting' table. You can move, or remove it, as needed.
      //this.settingTableAdapter.Fill(this.atlantaDataSet.setting);

    }
  }
}