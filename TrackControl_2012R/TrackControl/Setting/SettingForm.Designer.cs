namespace GPSControl.Setting
{
  partial class SettingForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      System.Windows.Forms.Label timeBreakLabel;
      System.Windows.Forms.Label accelMaxLabel;
      System.Windows.Forms.Label breakMaxLabel;
      System.Windows.Forms.Label speedMaxLabel;
      System.Windows.Forms.Label rotationMainLabel;
      System.Windows.Forms.Label rotationAddLabel;
      System.Windows.Forms.Label fuelingEdgeLabel;
      System.Windows.Forms.Label fuelingDischargeLabel;
      System.Windows.Forms.Label bandLabel;
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingForm));
      this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
      this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
      this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
      this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
      this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
      this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
      this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
      this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
      this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
      this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
      this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
      this.settingBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
      this.settingBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
      this.settingBindingSource = new System.Windows.Forms.BindingSource(this.components);
      this.atlantaDataSet = new GPSControl.atlantaDataSet();
      this.tabControl1 = new System.Windows.Forms.TabControl();
      this.tabPage1 = new System.Windows.Forms.TabPage();
      this.speedMaxNumericUpDown = new System.Windows.Forms.NumericUpDown();
      this.breakMaxNumericUpDown = new System.Windows.Forms.NumericUpDown();
      this.accelMaxNumericUpDown = new System.Windows.Forms.NumericUpDown();
      this.timeBreakDateTimePicker = new System.Windows.Forms.DateTimePicker();
      this.tabPage2 = new System.Windows.Forms.TabPage();
      this.rotationAddNumericUpDown = new System.Windows.Forms.NumericUpDown();
      this.rotationMainNumericUpDown = new System.Windows.Forms.NumericUpDown();
      this.tabPage3 = new System.Windows.Forms.TabPage();
      this.bandNumericUpDown = new System.Windows.Forms.NumericUpDown();
      this.fuelingDischargeNumericUpDown = new System.Windows.Forms.NumericUpDown();
      this.fuelingEdgeNumericUpDown = new System.Windows.Forms.NumericUpDown();
      this.settingTableAdapter = new GPSControl.atlantaDataSetTableAdapters.settingTableAdapter();
      timeBreakLabel = new System.Windows.Forms.Label();
      accelMaxLabel = new System.Windows.Forms.Label();
      breakMaxLabel = new System.Windows.Forms.Label();
      speedMaxLabel = new System.Windows.Forms.Label();
      rotationMainLabel = new System.Windows.Forms.Label();
      rotationAddLabel = new System.Windows.Forms.Label();
      fuelingEdgeLabel = new System.Windows.Forms.Label();
      fuelingDischargeLabel = new System.Windows.Forms.Label();
      bandLabel = new System.Windows.Forms.Label();
      ((System.ComponentModel.ISupportInitialize)(this.settingBindingNavigator)).BeginInit();
      this.settingBindingNavigator.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.settingBindingSource)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.atlantaDataSet)).BeginInit();
      this.tabControl1.SuspendLayout();
      this.tabPage1.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.speedMaxNumericUpDown)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.breakMaxNumericUpDown)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.accelMaxNumericUpDown)).BeginInit();
      this.tabPage2.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.rotationAddNumericUpDown)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.rotationMainNumericUpDown)).BeginInit();
      this.tabPage3.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.bandNumericUpDown)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.fuelingDischargeNumericUpDown)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.fuelingEdgeNumericUpDown)).BeginInit();
      this.SuspendLayout();
      // 
      // timeBreakLabel
      // 
      timeBreakLabel.AutoSize = true;
      timeBreakLabel.Location = new System.Drawing.Point(10, 22);
      timeBreakLabel.Name = "timeBreakLabel";
      timeBreakLabel.Size = new System.Drawing.Size(64, 13);
      timeBreakLabel.TabIndex = 0;
      timeBreakLabel.Text = "Time Break:";
      // 
      // accelMaxLabel
      // 
      accelMaxLabel.AutoSize = true;
      accelMaxLabel.Location = new System.Drawing.Point(10, 70);
      accelMaxLabel.Name = "accelMaxLabel";
      accelMaxLabel.Size = new System.Drawing.Size(59, 13);
      accelMaxLabel.TabIndex = 2;
      accelMaxLabel.Text = "accel Max:";
      // 
      // breakMaxLabel
      // 
      breakMaxLabel.AutoSize = true;
      breakMaxLabel.Location = new System.Drawing.Point(10, 94);
      breakMaxLabel.Name = "breakMaxLabel";
      breakMaxLabel.Size = new System.Drawing.Size(60, 13);
      breakMaxLabel.TabIndex = 4;
      breakMaxLabel.Text = "break Max:";
      // 
      // speedMaxLabel
      // 
      speedMaxLabel.AutoSize = true;
      speedMaxLabel.Location = new System.Drawing.Point(10, 46);
      speedMaxLabel.Name = "speedMaxLabel";
      speedMaxLabel.Size = new System.Drawing.Size(62, 13);
      speedMaxLabel.TabIndex = 6;
      speedMaxLabel.Text = "speed Max:";
      // 
      // rotationMainLabel
      // 
      rotationMainLabel.AutoSize = true;
      rotationMainLabel.Location = new System.Drawing.Point(45, 30);
      rotationMainLabel.Name = "rotationMainLabel";
      rotationMainLabel.Size = new System.Drawing.Size(76, 13);
      rotationMainLabel.TabIndex = 0;
      rotationMainLabel.Text = "Rotation Main:";
      // 
      // rotationAddLabel
      // 
      rotationAddLabel.AutoSize = true;
      rotationAddLabel.Location = new System.Drawing.Point(49, 69);
      rotationAddLabel.Name = "rotationAddLabel";
      rotationAddLabel.Size = new System.Drawing.Size(72, 13);
      rotationAddLabel.TabIndex = 2;
      rotationAddLabel.Text = "Rotation Add:";
      // 
      // fuelingEdgeLabel
      // 
      fuelingEdgeLabel.AutoSize = true;
      fuelingEdgeLabel.Location = new System.Drawing.Point(32, 12);
      fuelingEdgeLabel.Name = "fuelingEdgeLabel";
      fuelingEdgeLabel.Size = new System.Drawing.Size(72, 13);
      fuelingEdgeLabel.TabIndex = 0;
      fuelingEdgeLabel.Text = "Fueling Edge:";
      // 
      // fuelingDischargeLabel
      // 
      fuelingDischargeLabel.AutoSize = true;
      fuelingDischargeLabel.Location = new System.Drawing.Point(9, 38);
      fuelingDischargeLabel.Name = "fuelingDischargeLabel";
      fuelingDischargeLabel.Size = new System.Drawing.Size(95, 13);
      fuelingDischargeLabel.TabIndex = 2;
      fuelingDischargeLabel.Text = "Fueling Discharge:";
      // 
      // bandLabel
      // 
      bandLabel.AutoSize = true;
      bandLabel.Location = new System.Drawing.Point(70, 64);
      bandLabel.Name = "bandLabel";
      bandLabel.Size = new System.Drawing.Size(34, 13);
      bandLabel.TabIndex = 4;
      bandLabel.Text = "band:";
      // 
      // bindingNavigatorMoveFirstItem
      // 
      this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
      this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
      this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
      this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
      this.bindingNavigatorMoveFirstItem.Text = "Move first";
      this.bindingNavigatorMoveFirstItem.Visible = false;
      // 
      // bindingNavigatorMovePreviousItem
      // 
      this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
      this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
      this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
      this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
      this.bindingNavigatorMovePreviousItem.Text = "Move previous";
      this.bindingNavigatorMovePreviousItem.Visible = false;
      // 
      // bindingNavigatorSeparator
      // 
      this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
      this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
      this.bindingNavigatorSeparator.Visible = false;
      // 
      // bindingNavigatorPositionItem
      // 
      this.bindingNavigatorPositionItem.AccessibleName = "Position";
      this.bindingNavigatorPositionItem.AutoSize = false;
      this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
      this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 21);
      this.bindingNavigatorPositionItem.Text = "0";
      this.bindingNavigatorPositionItem.ToolTipText = "Current position";
      this.bindingNavigatorPositionItem.Visible = false;
      // 
      // bindingNavigatorCountItem
      // 
      this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
      this.bindingNavigatorCountItem.Size = new System.Drawing.Size(36, 22);
      this.bindingNavigatorCountItem.Text = "of {0}";
      this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
      this.bindingNavigatorCountItem.Visible = false;
      // 
      // bindingNavigatorSeparator1
      // 
      this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator1";
      this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
      this.bindingNavigatorSeparator1.Visible = false;
      // 
      // bindingNavigatorMoveNextItem
      // 
      this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
      this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
      this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
      this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
      this.bindingNavigatorMoveNextItem.Text = "Move next";
      this.bindingNavigatorMoveNextItem.Visible = false;
      // 
      // bindingNavigatorMoveLastItem
      // 
      this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
      this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
      this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
      this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
      this.bindingNavigatorMoveLastItem.Text = "Move last";
      this.bindingNavigatorMoveLastItem.Visible = false;
      // 
      // bindingNavigatorSeparator2
      // 
      this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator2";
      this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
      this.bindingNavigatorSeparator2.Visible = false;
      // 
      // bindingNavigatorAddNewItem
      // 
      this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
      this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
      this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
      this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
      this.bindingNavigatorAddNewItem.Text = "Add new";
      // 
      // bindingNavigatorDeleteItem
      // 
      this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
      this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
      this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
      this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
      this.bindingNavigatorDeleteItem.Text = "Delete";
      // 
      // settingBindingNavigatorSaveItem
      // 
      this.settingBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.settingBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("settingBindingNavigatorSaveItem.Image")));
      this.settingBindingNavigatorSaveItem.Name = "settingBindingNavigatorSaveItem";
      this.settingBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 22);
      this.settingBindingNavigatorSaveItem.Text = "Save Data";
      this.settingBindingNavigatorSaveItem.Click += new System.EventHandler(this.settingBindingNavigatorSaveItem_Click);
      // 
      // settingBindingNavigator
      // 
      this.settingBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
      this.settingBindingNavigator.BindingSource = this.settingBindingSource;
      this.settingBindingNavigator.CountItem = this.bindingNavigatorCountItem;
      this.settingBindingNavigator.DeleteItem = this.bindingNavigatorDeleteItem;
      this.settingBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.settingBindingNavigatorSaveItem});
      this.settingBindingNavigator.Location = new System.Drawing.Point(0, 0);
      this.settingBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
      this.settingBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
      this.settingBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
      this.settingBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
      this.settingBindingNavigator.Name = "settingBindingNavigator";
      this.settingBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
      this.settingBindingNavigator.Size = new System.Drawing.Size(388, 25);
      this.settingBindingNavigator.TabIndex = 0;
      this.settingBindingNavigator.Text = "bindingNavigator1";
      // 
      // settingBindingSource
      // 
      this.settingBindingSource.DataMember = "setting";
      this.settingBindingSource.DataSource = this.atlantaDataSet;
      // 
      // atlantaDataSet
      // 
      this.atlantaDataSet.DataSetName = "atlantaDataSet";
      this.atlantaDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
      // 
      // tabControl1
      // 
      this.tabControl1.Controls.Add(this.tabPage1);
      this.tabControl1.Controls.Add(this.tabPage2);
      this.tabControl1.Controls.Add(this.tabPage3);
      this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tabControl1.Location = new System.Drawing.Point(0, 25);
      this.tabControl1.Name = "tabControl1";
      this.tabControl1.SelectedIndex = 0;
      this.tabControl1.Size = new System.Drawing.Size(388, 186);
      this.tabControl1.TabIndex = 1;
      // 
      // tabPage1
      // 
      this.tabPage1.AutoScroll = true;
      this.tabPage1.Controls.Add(speedMaxLabel);
      this.tabPage1.Controls.Add(this.speedMaxNumericUpDown);
      this.tabPage1.Controls.Add(breakMaxLabel);
      this.tabPage1.Controls.Add(this.breakMaxNumericUpDown);
      this.tabPage1.Controls.Add(accelMaxLabel);
      this.tabPage1.Controls.Add(this.accelMaxNumericUpDown);
      this.tabPage1.Controls.Add(timeBreakLabel);
      this.tabPage1.Controls.Add(this.timeBreakDateTimePicker);
      this.tabPage1.Location = new System.Drawing.Point(4, 22);
      this.tabPage1.Name = "tabPage1";
      this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
      this.tabPage1.Size = new System.Drawing.Size(380, 160);
      this.tabPage1.TabIndex = 0;
      this.tabPage1.Text = "��������";
      this.tabPage1.UseVisualStyleBackColor = true;
      // 
      // speedMaxNumericUpDown
      // 
      this.speedMaxNumericUpDown.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.settingBindingSource, "speedMax", true));
      this.speedMaxNumericUpDown.Location = new System.Drawing.Point(204, 44);
      this.speedMaxNumericUpDown.Name = "speedMaxNumericUpDown";
      this.speedMaxNumericUpDown.Size = new System.Drawing.Size(68, 20);
      this.speedMaxNumericUpDown.TabIndex = 7;
      // 
      // breakMaxNumericUpDown
      // 
      this.breakMaxNumericUpDown.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.settingBindingSource, "breakMax", true));
      this.breakMaxNumericUpDown.Location = new System.Drawing.Point(204, 96);
      this.breakMaxNumericUpDown.Name = "breakMaxNumericUpDown";
      this.breakMaxNumericUpDown.Size = new System.Drawing.Size(68, 20);
      this.breakMaxNumericUpDown.TabIndex = 5;
      // 
      // accelMaxNumericUpDown
      // 
      this.accelMaxNumericUpDown.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.settingBindingSource, "accelMax", true));
      this.accelMaxNumericUpDown.Location = new System.Drawing.Point(204, 70);
      this.accelMaxNumericUpDown.Name = "accelMaxNumericUpDown";
      this.accelMaxNumericUpDown.Size = new System.Drawing.Size(68, 20);
      this.accelMaxNumericUpDown.TabIndex = 3;
      // 
      // timeBreakDateTimePicker
      // 
      this.timeBreakDateTimePicker.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.settingBindingSource, "TimeBreak", true));
      this.timeBreakDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Time;
      this.timeBreakDateTimePicker.Location = new System.Drawing.Point(204, 18);
      this.timeBreakDateTimePicker.Name = "timeBreakDateTimePicker";
      this.timeBreakDateTimePicker.ShowUpDown = true;
      this.timeBreakDateTimePicker.Size = new System.Drawing.Size(68, 20);
      this.timeBreakDateTimePicker.TabIndex = 1;
      // 
      // tabPage2
      // 
      this.tabPage2.Controls.Add(rotationAddLabel);
      this.tabPage2.Controls.Add(this.rotationAddNumericUpDown);
      this.tabPage2.Controls.Add(rotationMainLabel);
      this.tabPage2.Controls.Add(this.rotationMainNumericUpDown);
      this.tabPage2.Location = new System.Drawing.Point(4, 22);
      this.tabPage2.Name = "tabPage2";
      this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
      this.tabPage2.Size = new System.Drawing.Size(380, 160);
      this.tabPage2.TabIndex = 1;
      this.tabPage2.Text = "�������";
      this.tabPage2.UseVisualStyleBackColor = true;
      // 
      // rotationAddNumericUpDown
      // 
      this.rotationAddNumericUpDown.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.settingBindingSource, "RotationAdd", true));
      this.rotationAddNumericUpDown.Location = new System.Drawing.Point(127, 69);
      this.rotationAddNumericUpDown.Name = "rotationAddNumericUpDown";
      this.rotationAddNumericUpDown.Size = new System.Drawing.Size(120, 20);
      this.rotationAddNumericUpDown.TabIndex = 3;
      // 
      // rotationMainNumericUpDown
      // 
      this.rotationMainNumericUpDown.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.settingBindingSource, "RotationMain", true));
      this.rotationMainNumericUpDown.Location = new System.Drawing.Point(127, 30);
      this.rotationMainNumericUpDown.Name = "rotationMainNumericUpDown";
      this.rotationMainNumericUpDown.Size = new System.Drawing.Size(120, 20);
      this.rotationMainNumericUpDown.TabIndex = 1;
      // 
      // tabPage3
      // 
      this.tabPage3.AutoScroll = true;
      this.tabPage3.Controls.Add(bandLabel);
      this.tabPage3.Controls.Add(this.bandNumericUpDown);
      this.tabPage3.Controls.Add(fuelingDischargeLabel);
      this.tabPage3.Controls.Add(this.fuelingDischargeNumericUpDown);
      this.tabPage3.Controls.Add(fuelingEdgeLabel);
      this.tabPage3.Controls.Add(this.fuelingEdgeNumericUpDown);
      this.tabPage3.Location = new System.Drawing.Point(4, 22);
      this.tabPage3.Name = "tabPage3";
      this.tabPage3.Size = new System.Drawing.Size(380, 160);
      this.tabPage3.TabIndex = 2;
      this.tabPage3.Text = "�������";
      this.tabPage3.UseVisualStyleBackColor = true;
      // 
      // bandNumericUpDown
      // 
      this.bandNumericUpDown.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.settingBindingSource, "band", true));
      this.bandNumericUpDown.Location = new System.Drawing.Point(110, 64);
      this.bandNumericUpDown.Name = "bandNumericUpDown";
      this.bandNumericUpDown.Size = new System.Drawing.Size(120, 20);
      this.bandNumericUpDown.TabIndex = 5;
      // 
      // fuelingDischargeNumericUpDown
      // 
      this.fuelingDischargeNumericUpDown.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.settingBindingSource, "FuelingDischarge", true));
      this.fuelingDischargeNumericUpDown.Location = new System.Drawing.Point(110, 38);
      this.fuelingDischargeNumericUpDown.Name = "fuelingDischargeNumericUpDown";
      this.fuelingDischargeNumericUpDown.Size = new System.Drawing.Size(120, 20);
      this.fuelingDischargeNumericUpDown.TabIndex = 3;
      // 
      // fuelingEdgeNumericUpDown
      // 
      this.fuelingEdgeNumericUpDown.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.settingBindingSource, "FuelingEdge", true));
      this.fuelingEdgeNumericUpDown.Location = new System.Drawing.Point(110, 12);
      this.fuelingEdgeNumericUpDown.Name = "fuelingEdgeNumericUpDown";
      this.fuelingEdgeNumericUpDown.Size = new System.Drawing.Size(120, 20);
      this.fuelingEdgeNumericUpDown.TabIndex = 1;
      // 
      // settingTableAdapter
      // 
      this.settingTableAdapter.ClearBeforeFill = true;
      // 
      // SettingForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(388, 211);
      this.Controls.Add(this.tabControl1);
      this.Controls.Add(this.settingBindingNavigator);
      this.Name = "SettingForm";
      this.Text = "��������� ����������";
      this.Load += new System.EventHandler(this.SettingForm_Load);
      ((System.ComponentModel.ISupportInitialize)(this.settingBindingNavigator)).EndInit();
      this.settingBindingNavigator.ResumeLayout(false);
      this.settingBindingNavigator.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.settingBindingSource)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.atlantaDataSet)).EndInit();
      this.tabControl1.ResumeLayout(false);
      this.tabPage1.ResumeLayout(false);
      this.tabPage1.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.speedMaxNumericUpDown)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.breakMaxNumericUpDown)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.accelMaxNumericUpDown)).EndInit();
      this.tabPage2.ResumeLayout(false);
      this.tabPage2.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.rotationAddNumericUpDown)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.rotationMainNumericUpDown)).EndInit();
      this.tabPage3.ResumeLayout(false);
      this.tabPage3.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.bandNumericUpDown)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.fuelingDischargeNumericUpDown)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.fuelingEdgeNumericUpDown)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private atlantaDataSet atlantaDataSet;
    private System.Windows.Forms.BindingSource settingBindingSource;
    private LocalCache.atlantaDataSetTableAdapters.settingTableAdapter settingTableAdapter;
    private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
    private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
    private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
    private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
    private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
    private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
    private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
    private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
    private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
    private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
    private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
    private System.Windows.Forms.ToolStripButton settingBindingNavigatorSaveItem;
    private System.Windows.Forms.BindingNavigator settingBindingNavigator;
    private System.Windows.Forms.TabControl tabControl1;
    private System.Windows.Forms.TabPage tabPage1;
    private System.Windows.Forms.DateTimePicker timeBreakDateTimePicker;
    private System.Windows.Forms.TabPage tabPage2;
    private System.Windows.Forms.TabPage tabPage3;
    private System.Windows.Forms.NumericUpDown speedMaxNumericUpDown;
    private System.Windows.Forms.NumericUpDown breakMaxNumericUpDown;
    private System.Windows.Forms.NumericUpDown accelMaxNumericUpDown;
    private System.Windows.Forms.NumericUpDown rotationAddNumericUpDown;
    private System.Windows.Forms.NumericUpDown rotationMainNumericUpDown;
    private System.Windows.Forms.NumericUpDown bandNumericUpDown;
    private System.Windows.Forms.NumericUpDown fuelingDischargeNumericUpDown;
    private System.Windows.Forms.NumericUpDown fuelingEdgeNumericUpDown;
  }
}