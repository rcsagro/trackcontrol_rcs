﻿namespace TrackControl.Setting
{
    partial class DevAdminForm
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && ( components != null ) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DevAdminForm));
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.gcSettings = new DevExpress.XtraVerticalGrid.PropertyGridControl();
            this.atlantaDataSet1 = new LocalCache.atlantaDataSet();
            this.standaloneBarDockControl1 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.panelTreeControl = new DevExpress.XtraEditors.PanelControl();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.bbiAddNewRow = new DevExpress.XtraBars.BarButtonItem();
            this.settingBindingNavigatorSaveItem = new DevExpress.XtraBars.BarButtonItem();
            this.refreshToolStripButton = new DevExpress.XtraBars.BarButtonItem();
            this.assignToolStripButton = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.bbiLog = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticItem2 = new DevExpress.XtraBars.BarStaticItem();
            this.barEditItemName = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.bbiExpand = new DevExpress.XtraBars.BarButtonItem();
            this.bbiCollapce = new DevExpress.XtraBars.BarButtonItem();
            this.bbiClear = new DevExpress.XtraBars.BarButtonItem();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.barEditItem1 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemTextEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this._eraseBtn = new DevExpress.XtraBars.BarButtonItem();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.settingBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.atlantaDataSet = new LocalCache.atlantaDataSet();
            this.bar4 = new DevExpress.XtraBars.Bar();
            this.bar5 = new DevExpress.XtraBars.Bar();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.vehicleBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.settingTableAdapter = new LocalCache.atlantaDataSetTableAdapters.SettingTableAdapter();
            this.vehicleTableAdapter = new LocalCache.atlantaDataSetTableAdapters.VehicleTableAdapter();
            this.settingvehicleBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcSettings)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.atlantaDataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelTreeControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.settingBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.atlantaDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vehicleBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.settingvehicleBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.panelControl1);
            this.splitContainerControl1.Panel1.Controls.Add(this.standaloneBarDockControl1);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.panelTreeControl);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(985, 494);
            this.splitContainerControl1.SplitterPosition = 541;
            this.splitContainerControl1.TabIndex = 0;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.gcSettings);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(0, 57);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(541, 437);
            this.panelControl1.TabIndex = 1;
            // 
            // gcSettings
            // 
            this.gcSettings.Appearance.Category.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.gcSettings.Appearance.Category.ForeColor = System.Drawing.Color.Black;
            this.gcSettings.Appearance.Category.Options.UseFont = true;
            this.gcSettings.Appearance.Category.Options.UseForeColor = true;
            this.gcSettings.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcSettings.Location = new System.Drawing.Point(2, 2);
            this.gcSettings.Name = "gcSettings";
            this.gcSettings.SelectedObject = this.atlantaDataSet1.setting;
            this.gcSettings.Size = new System.Drawing.Size(537, 433);
            this.gcSettings.TabIndex = 0;
            // 
            // atlantaDataSet1
            // 
            this.atlantaDataSet1.DataSetName = "atlantaDataSet";
            this.atlantaDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // standaloneBarDockControl1
            // 
            this.standaloneBarDockControl1.CausesValidation = false;
            this.standaloneBarDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.standaloneBarDockControl1.Location = new System.Drawing.Point(0, 0);
            this.standaloneBarDockControl1.Name = "standaloneBarDockControl1";
            this.standaloneBarDockControl1.Size = new System.Drawing.Size(541, 57);
            this.standaloneBarDockControl1.Text = "standaloneBarDockControl1";
            // 
            // panelTreeControl
            // 
            this.panelTreeControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelTreeControl.Location = new System.Drawing.Point(0, 0);
            this.panelTreeControl.Name = "panelTreeControl";
            this.panelTreeControl.Size = new System.Drawing.Size(439, 494);
            this.panelTreeControl.TabIndex = 0;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2,
            this.bar3});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.DockControls.Add(this.standaloneBarDockControl1);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.settingBindingNavigatorSaveItem,
            this.assignToolStripButton,
            this.bbiExpand,
            this.bbiCollapce,
            this.bbiClear,
            this.barStaticItem1,
            this.barEditItem1,
            this.refreshToolStripButton,
            this._eraseBtn,
            this.bbiAddNewRow,
            this.barStaticItem2,
            this.barEditItemName,
            this.barButtonItem1,
            this.bbiLog});
            this.barManager1.MaxItemId = 23;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit1,
            this.repositoryItemTextEdit3,
            this.repositoryItemTextEdit4,
            this.repositoryItemTextEdit2,
            this.repositoryItemComboBox1});
            // 
            // bar2
            // 
            this.bar2.BarName = "Custom 3";
            this.bar2.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Standalone;
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar2.FloatLocation = new System.Drawing.Point(318, 165);
            this.bar2.FloatSize = new System.Drawing.Size(46, 24);
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bbiAddNewRow, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.settingBindingNavigatorSaveItem, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.refreshToolStripButton, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.assignToolStripButton, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiLog)});
            this.bar2.OptionsBar.AllowQuickCustomization = false;
            this.bar2.OptionsBar.AllowRename = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.StandaloneBarDockControl = this.standaloneBarDockControl1;
            this.bar2.Text = "Custom 3";
            // 
            // bbiAddNewRow
            // 
            this.bbiAddNewRow.Border = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.bbiAddNewRow.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiAddNewRow.Glyph")));
            this.bbiAddNewRow.Hint = "Добавить новую настройку";
            this.bbiAddNewRow.Id = 16;
            this.bbiAddNewRow.Name = "bbiAddNewRow";
            this.bbiAddNewRow.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAddNewRow_ItemClick);
            // 
            // settingBindingNavigatorSaveItem
            // 
            this.settingBindingNavigatorSaveItem.Border = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.settingBindingNavigatorSaveItem.Glyph = ((System.Drawing.Image)(resources.GetObject("settingBindingNavigatorSaveItem.Glyph")));
            this.settingBindingNavigatorSaveItem.Hint = "Сохранить данные";
            this.settingBindingNavigatorSaveItem.Id = 0;
            this.settingBindingNavigatorSaveItem.Name = "settingBindingNavigatorSaveItem";
            this.settingBindingNavigatorSaveItem.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.settingBindingNavigatorSaveItem_ItemClick);
            // 
            // refreshToolStripButton
            // 
            this.refreshToolStripButton.Border = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.refreshToolStripButton.Glyph = ((System.Drawing.Image)(resources.GetObject("refreshToolStripButton.Glyph")));
            this.refreshToolStripButton.Hint = "Обновить данные таблицы";
            this.refreshToolStripButton.Id = 14;
            this.refreshToolStripButton.Name = "refreshToolStripButton";
            this.refreshToolStripButton.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.refreshToolStripButton_ItemClick_1);
            // 
            // assignToolStripButton
            // 
            this.assignToolStripButton.Border = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.assignToolStripButton.Glyph = ((System.Drawing.Image)(resources.GetObject("assignToolStripButton.Glyph")));
            this.assignToolStripButton.Hint = "Назначить выбранную настройку на машину";
            this.assignToolStripButton.Id = 1;
            this.assignToolStripButton.Name = "assignToolStripButton";
            this.assignToolStripButton.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.assignToolStripButton_ItemClick);
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Border = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.barButtonItem1.Caption = "Delete Setting";
            this.barButtonItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem1.Glyph")));
            this.barButtonItem1.Hint = "Удалить настройку";
            this.barButtonItem1.Id = 21;
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_ItemClick);
            // 
            // bbiLog
            // 
            this.bbiLog.Border = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.bbiLog.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiLog.Glyph")));
            this.bbiLog.Hint = "Просмотр журнала событий";
            this.bbiLog.Id = 22;
            this.bbiLog.Name = "bbiLog";
            this.bbiLog.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiLog_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Custom 4";
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 1;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar3.FloatLocation = new System.Drawing.Point(321, 139);
            this.bar3.FloatSize = new System.Drawing.Size(46, 24);
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem2),
            new DevExpress.XtraBars.LinkPersistInfo(this.barEditItemName)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.AllowRename = true;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.StandaloneBarDockControl = this.standaloneBarDockControl1;
            this.bar3.Text = "Custom 4";
            // 
            // barStaticItem2
            // 
            this.barStaticItem2.Caption = "Код настройки:";
            this.barStaticItem2.Id = 17;
            this.barStaticItem2.Name = "barStaticItem2";
            this.barStaticItem2.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barEditItemName
            // 
            this.barEditItemName.AutoFillWidth = true;
            this.barEditItemName.Border = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.barEditItemName.Edit = this.repositoryItemComboBox1;
            this.barEditItemName.Hint = "Выбрать имя настройки";
            this.barEditItemName.Id = 19;
            this.barEditItemName.Name = "barEditItemName";
            this.barEditItemName.Width = 250;
            // 
            // repositoryItemComboBox1
            // 
            this.repositoryItemComboBox1.AutoHeight = false;
            this.repositoryItemComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox1.Name = "repositoryItemComboBox1";
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(985, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 494);
            this.barDockControlBottom.Size = new System.Drawing.Size(985, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 494);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(985, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 494);
            // 
            // bbiExpand
            // 
            this.bbiExpand.Border = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.bbiExpand.Caption = "Expand";
            this.bbiExpand.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiExpand.Glyph")));
            this.bbiExpand.Hint = "Развернуть узлы дерева";
            this.bbiExpand.Id = 4;
            this.bbiExpand.Name = "bbiExpand";
            // 
            // bbiCollapce
            // 
            this.bbiCollapce.Border = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.bbiCollapce.Caption = "Collapce";
            this.bbiCollapce.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiCollapce.Glyph")));
            this.bbiCollapce.Hint = "Свернуть узлы дерева";
            this.bbiCollapce.Id = 5;
            this.bbiCollapce.Name = "bbiCollapce";
            // 
            // bbiClear
            // 
            this.bbiClear.Border = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.bbiClear.Caption = "Очистить";
            this.bbiClear.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiClear.Glyph")));
            this.bbiClear.Hint = "Очистить";
            this.bbiClear.Id = 8;
            this.bbiClear.Name = "bbiClear";
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Border = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.barStaticItem1.Caption = "Фильтр";
            this.barStaticItem1.Id = 12;
            this.barStaticItem1.Name = "barStaticItem1";
            this.barStaticItem1.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barEditItem1
            // 
            this.barEditItem1.Edit = this.repositoryItemTextEdit4;
            this.barEditItem1.Id = 13;
            this.barEditItem1.Name = "barEditItem1";
            this.barEditItem1.Width = 100;
            // 
            // repositoryItemTextEdit4
            // 
            this.repositoryItemTextEdit4.AutoHeight = false;
            this.repositoryItemTextEdit4.Name = "repositoryItemTextEdit4";
            // 
            // _eraseBtn
            // 
            this._eraseBtn.Border = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this._eraseBtn.Caption = "Обновить";
            this._eraseBtn.Glyph = ((System.Drawing.Image)(resources.GetObject("_eraseBtn.Glyph")));
            this._eraseBtn.Hint = "Обновить дерева";
            this._eraseBtn.Id = 15;
            this._eraseBtn.Name = "_eraseBtn";
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // settingBindingSource
            // 
            this.settingBindingSource.DataMember = "setting";
            this.settingBindingSource.DataSource = this.atlantaDataSet;
            // 
            // atlantaDataSet
            // 
            this.atlantaDataSet.DataSetName = "atlantaDataSet";
            this.atlantaDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // bar4
            // 
            this.bar4.BarName = "Custom 5";
            this.bar4.DockCol = 0;
            this.bar4.DockRow = 2;
            this.bar4.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar4.Text = "Custom 5";
            // 
            // bar5
            // 
            this.bar5.BarName = "Custom 5";
            this.bar5.DockCol = 0;
            this.bar5.DockRow = 2;
            this.bar5.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar5.Text = "Custom 5";
            // 
            // bar1
            // 
            this.bar1.BarName = "Custom 3";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.Text = "Custom 3";
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            // 
            // bindingSource1
            // 
            this.bindingSource1.DataMember = "mobitels";
            this.bindingSource1.DataSource = this.atlantaDataSet1;
            // 
            // vehicleBindingSource
            // 
            this.vehicleBindingSource.DataMember = "vehicle";
            this.vehicleBindingSource.DataSource = this.atlantaDataSet;
            // 
            // settingTableAdapter
            // 
            this.settingTableAdapter.ClearBeforeFill = true;
            // 
            // vehicleTableAdapter
            // 
            this.vehicleTableAdapter.ClearBeforeFill = true;
            // 
            // settingvehicleBindingSource
            // 
            this.settingvehicleBindingSource.DataMember = "setting_vehicle";
            this.settingvehicleBindingSource.DataSource = this.settingBindingSource;
            // 
            // DevAdminForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Controls.Add(this.splitContainerControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "DevAdminForm";
            this.Size = new System.Drawing.Size(985, 494);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcSettings)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.atlantaDataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelTreeControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.settingBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.atlantaDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vehicleBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.settingvehicleBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.Bar bar4;
        private DevExpress.XtraBars.Bar bar5;
        private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl1;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem settingBindingNavigatorSaveItem;
        private DevExpress.XtraBars.BarButtonItem assignToolStripButton;
        private DevExpress.XtraBars.BarButtonItem bbiExpand;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraBars.BarButtonItem bbiCollapce;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraBars.BarButtonItem bbiClear;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
        private LocalCache.atlantaDataSet atlantaDataSet1;
        private System.Windows.Forms.BindingSource bindingSource1;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraBars.BarEditItem barEditItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit4;
        private DevExpress.XtraBars.BarButtonItem refreshToolStripButton;
        private DevExpress.XtraBars.BarButtonItem _eraseBtn;
        private LocalCache.atlantaDataSet atlantaDataSet;
        private System.Windows.Forms.BindingSource settingBindingSource;
        private System.Windows.Forms.BindingSource vehicleBindingSource;
        private LocalCache.atlantaDataSetTableAdapters.SettingTableAdapter settingTableAdapter;
        private LocalCache.atlantaDataSetTableAdapters.VehicleTableAdapter vehicleTableAdapter;
        private System.Windows.Forms.BindingSource settingvehicleBindingSource;
        private DevExpress.XtraVerticalGrid.PropertyGridControl gcSettings;
        private DevExpress.XtraEditors.PanelControl panelTreeControl;
        private DevExpress.XtraBars.BarButtonItem bbiAddNewRow;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticItem2;
        private DevExpress.XtraBars.BarEditItem barEditItemName;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem bbiLog;
    }
}
