﻿namespace TrackControl.Setting
{
    partial class SensorTransitionFormDvx
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if( disposing && ( components != null ) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SensorTransitionFormDvx));
            this._mainTable = new System.Windows.Forms.TableLayoutPanel();
            this.carSensorTable = new System.Windows.Forms.TableLayoutPanel();
            this.carIcon = new System.Windows.Forms.Label();
            this.sensorIcon = new System.Windows.Forms.Label();
            this.carLbl = new System.Windows.Forms.Label();
            this.sensorLbl = new System.Windows.Forms.Label();
            this.carNameText = new System.Windows.Forms.Label();
            this.sensorNameText = new System.Windows.Forms.Label();
            this.editGroupBox = new DevExpress.XtraEditors.GroupControl();
            this.transControlGrid = new DevExpress.XtraGrid.GridControl();
            this.sensorTransitionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.transitionGridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInitialStateId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFinalStateId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.coltitle = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colenabled = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSensorId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIconName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSoundName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colview = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Editing = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rpItemPictureEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.Deleting = new DevExpress.XtraGrid.Columns.GridColumn();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this._mainTable.SuspendLayout();
            this.carSensorTable.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.editGroupBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.transControlGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sensorTransitionsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.transitionGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rpItemPictureEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // _mainTable
            // 
            this._mainTable.BackColor = System.Drawing.Color.WhiteSmoke;
            this._mainTable.ColumnCount = 3;
            this._mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 8F));
            this._mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._mainTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 8F));
            this._mainTable.Controls.Add(this.carSensorTable, 1, 1);
            this._mainTable.Controls.Add(this.editGroupBox, 1, 3);
            this._mainTable.Controls.Add(this.transControlGrid, 1, 2);
            this._mainTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this._mainTable.Location = new System.Drawing.Point(0, 0);
            this._mainTable.Margin = new System.Windows.Forms.Padding(10, 5, 10, 10);
            this._mainTable.Name = "_mainTable";
            this._mainTable.RowCount = 5;
            this._mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this._mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this._mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 214F));
            this._mainTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 11F));
            this._mainTable.Size = new System.Drawing.Size(684, 427);
            this._mainTable.TabIndex = 2;
            // 
            // carSensorTable
            // 
            this.carSensorTable.ColumnCount = 3;
            this.carSensorTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 21F));
            this.carSensorTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 86F));
            this.carSensorTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.carSensorTable.Controls.Add(this.carIcon, 0, 0);
            this.carSensorTable.Controls.Add(this.sensorIcon, 0, 1);
            this.carSensorTable.Controls.Add(this.carLbl, 1, 0);
            this.carSensorTable.Controls.Add(this.sensorLbl, 1, 1);
            this.carSensorTable.Controls.Add(this.carNameText, 2, 0);
            this.carSensorTable.Controls.Add(this.sensorNameText, 2, 1);
            this.carSensorTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.carSensorTable.Location = new System.Drawing.Point(8, 10);
            this.carSensorTable.Margin = new System.Windows.Forms.Padding(0);
            this.carSensorTable.Name = "carSensorTable";
            this.carSensorTable.RowCount = 2;
            this.carSensorTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.carSensorTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.carSensorTable.Size = new System.Drawing.Size(668, 50);
            this.carSensorTable.TabIndex = 1;
            // 
            // carIcon
            // 
            this.carIcon.AutoSize = true;
            this.carIcon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.carIcon.Image = ((System.Drawing.Image)(resources.GetObject("carIcon.Image")));
            this.carIcon.Location = new System.Drawing.Point(0, 0);
            this.carIcon.Margin = new System.Windows.Forms.Padding(0);
            this.carIcon.Name = "carIcon";
            this.carIcon.Size = new System.Drawing.Size(21, 25);
            this.carIcon.TabIndex = 0;
            // 
            // sensorIcon
            // 
            this.sensorIcon.AutoSize = true;
            this.sensorIcon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sensorIcon.Image = ((System.Drawing.Image)(resources.GetObject("sensorIcon.Image")));
            this.sensorIcon.Location = new System.Drawing.Point(0, 25);
            this.sensorIcon.Margin = new System.Windows.Forms.Padding(0);
            this.sensorIcon.Name = "sensorIcon";
            this.sensorIcon.Size = new System.Drawing.Size(21, 25);
            this.sensorIcon.TabIndex = 1;
            // 
            // carLbl
            // 
            this.carLbl.AutoSize = true;
            this.carLbl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.carLbl.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.carLbl.ForeColor = System.Drawing.Color.DarkOliveGreen;
            this.carLbl.Location = new System.Drawing.Point(21, 0);
            this.carLbl.Margin = new System.Windows.Forms.Padding(0);
            this.carLbl.Name = "carLbl";
            this.carLbl.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.carLbl.Size = new System.Drawing.Size(86, 25);
            this.carLbl.TabIndex = 2;
            this.carLbl.Text = "Автомобиль:";
            this.carLbl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // sensorLbl
            // 
            this.sensorLbl.AutoSize = true;
            this.sensorLbl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sensorLbl.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.sensorLbl.Location = new System.Drawing.Point(21, 25);
            this.sensorLbl.Margin = new System.Windows.Forms.Padding(0);
            this.sensorLbl.Name = "sensorLbl";
            this.sensorLbl.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.sensorLbl.Size = new System.Drawing.Size(86, 25);
            this.sensorLbl.TabIndex = 3;
            this.sensorLbl.Text = "Датчик:";
            this.sensorLbl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // carNameText
            // 
            this.carNameText.AutoSize = true;
            this.carNameText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.carNameText.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.carNameText.ForeColor = System.Drawing.Color.Black;
            this.carNameText.Location = new System.Drawing.Point(107, 0);
            this.carNameText.Margin = new System.Windows.Forms.Padding(0);
            this.carNameText.Name = "carNameText";
            this.carNameText.Size = new System.Drawing.Size(561, 25);
            this.carNameText.TabIndex = 4;
            this.carNameText.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // sensorNameText
            // 
            this.sensorNameText.AutoSize = true;
            this.sensorNameText.BackColor = System.Drawing.Color.WhiteSmoke;
            this.sensorNameText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sensorNameText.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.sensorNameText.ForeColor = System.Drawing.Color.Black;
            this.sensorNameText.Location = new System.Drawing.Point(107, 25);
            this.sensorNameText.Margin = new System.Windows.Forms.Padding(0);
            this.sensorNameText.Name = "sensorNameText";
            this.sensorNameText.Size = new System.Drawing.Size(561, 25);
            this.sensorNameText.TabIndex = 5;
            this.sensorNameText.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // editGroupBox
            // 
            this.editGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.editGroupBox.Location = new System.Drawing.Point(11, 205);
            this.editGroupBox.Name = "editGroupBox";
            this.editGroupBox.Size = new System.Drawing.Size(662, 208);
            this.editGroupBox.TabIndex = 3;
            this.editGroupBox.Text = "Добавить событие";
            // 
            // transControlGrid
            // 
            this.transControlGrid.DataSource = this.sensorTransitionsBindingSource;
            this.transControlGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.transControlGrid.Location = new System.Drawing.Point(11, 63);
            this.transControlGrid.MainView = this.transitionGridView;
            this.transControlGrid.Name = "transControlGrid";
            this.transControlGrid.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.rpItemPictureEdit1});
            this.transControlGrid.Size = new System.Drawing.Size(662, 136);
            this.transControlGrid.TabIndex = 4;
            this.transControlGrid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.transitionGridView});
            // 
            // sensorTransitionsBindingSource
            // 
            this.sensorTransitionsBindingSource.DataMember = "Transition";
            this.sensorTransitionsBindingSource.DataSource = typeof(LocalCache.atlantaDataSet);
            // 
            // transitionGridView
            // 
            this.transitionGridView.ColumnPanelRowHeight = 40;
            this.transitionGridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colId,
            this.colInitialStateId,
            this.colFinalStateId,
            this.coltitle,
            this.colenabled,
            this.colSensorId,
            this.colIconName,
            this.colSoundName,
            this.colview,
            this.Editing,
            this.Deleting});
            this.transitionGridView.GridControl = this.transControlGrid;
            this.transitionGridView.Name = "transitionGridView";
            this.transitionGridView.OptionsView.ShowGroupPanel = false;
            // 
            // colId
            // 
            this.colId.AppearanceCell.Options.UseTextOptions = true;
            this.colId.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colId.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colId.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.colId.AppearanceHeader.Options.UseTextOptions = true;
            this.colId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colId.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colId.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colId.FieldName = "Id";
            this.colId.Name = "colId";
            this.colId.OptionsColumn.AllowEdit = false;
            this.colId.OptionsColumn.AllowFocus = false;
            this.colId.OptionsColumn.ReadOnly = true;
            // 
            // colInitialStateId
            // 
            this.colInitialStateId.AppearanceCell.Options.UseTextOptions = true;
            this.colInitialStateId.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colInitialStateId.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colInitialStateId.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.colInitialStateId.AppearanceHeader.Options.UseTextOptions = true;
            this.colInitialStateId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colInitialStateId.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colInitialStateId.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colInitialStateId.FieldName = "InitialStateId";
            this.colInitialStateId.Name = "colInitialStateId";
            this.colInitialStateId.OptionsColumn.AllowEdit = false;
            this.colInitialStateId.OptionsColumn.AllowFocus = false;
            this.colInitialStateId.OptionsColumn.ReadOnly = true;
            // 
            // colFinalStateId
            // 
            this.colFinalStateId.AppearanceCell.Options.UseTextOptions = true;
            this.colFinalStateId.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFinalStateId.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colFinalStateId.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.colFinalStateId.AppearanceHeader.Options.UseTextOptions = true;
            this.colFinalStateId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFinalStateId.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colFinalStateId.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFinalStateId.FieldName = "FinalStateId";
            this.colFinalStateId.Name = "colFinalStateId";
            this.colFinalStateId.OptionsColumn.AllowEdit = false;
            this.colFinalStateId.OptionsColumn.AllowFocus = false;
            this.colFinalStateId.OptionsColumn.ReadOnly = true;
            // 
            // coltitle
            // 
            this.coltitle.AppearanceCell.Options.UseTextOptions = true;
            this.coltitle.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.coltitle.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.coltitle.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.coltitle.AppearanceHeader.Options.UseTextOptions = true;
            this.coltitle.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.coltitle.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.coltitle.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.coltitle.Caption = "Описание события";
            this.coltitle.FieldName = "Title";
            this.coltitle.Name = "coltitle";
            this.coltitle.OptionsColumn.AllowEdit = false;
            this.coltitle.OptionsColumn.AllowFocus = false;
            this.coltitle.OptionsColumn.ReadOnly = true;
            this.coltitle.ToolTip = "Описание события";
            this.coltitle.Visible = true;
            this.coltitle.VisibleIndex = 0;
            this.coltitle.Width = 119;
            // 
            // colenabled
            // 
            this.colenabled.AppearanceCell.Options.UseTextOptions = true;
            this.colenabled.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colenabled.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colenabled.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.colenabled.AppearanceHeader.Options.UseTextOptions = true;
            this.colenabled.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colenabled.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colenabled.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colenabled.Caption = "Тревога";
            this.colenabled.FieldName = "Enabled";
            this.colenabled.Name = "colenabled";
            this.colenabled.OptionsColumn.AllowEdit = false;
            this.colenabled.OptionsColumn.AllowFocus = false;
            this.colenabled.OptionsColumn.ReadOnly = true;
            this.colenabled.ToolTip = "Тревога";
            this.colenabled.Visible = true;
            this.colenabled.VisibleIndex = 1;
            this.colenabled.Width = 119;
            // 
            // colSensorId
            // 
            this.colSensorId.AppearanceCell.Options.UseTextOptions = true;
            this.colSensorId.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSensorId.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colSensorId.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.colSensorId.AppearanceHeader.Options.UseTextOptions = true;
            this.colSensorId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSensorId.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colSensorId.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colSensorId.FieldName = "SensorId";
            this.colSensorId.Name = "colSensorId";
            this.colSensorId.OptionsColumn.AllowEdit = false;
            this.colSensorId.OptionsColumn.AllowFocus = false;
            this.colSensorId.OptionsColumn.ReadOnly = true;
            // 
            // colIconName
            // 
            this.colIconName.AppearanceCell.Options.UseTextOptions = true;
            this.colIconName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIconName.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colIconName.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colIconName.AppearanceHeader.Options.UseTextOptions = true;
            this.colIconName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIconName.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colIconName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colIconName.FieldName = "IconName";
            this.colIconName.Name = "colIconName";
            this.colIconName.OptionsColumn.AllowEdit = false;
            this.colIconName.OptionsColumn.AllowFocus = false;
            this.colIconName.OptionsColumn.ReadOnly = true;
            // 
            // colSoundName
            // 
            this.colSoundName.AppearanceCell.Options.UseTextOptions = true;
            this.colSoundName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSoundName.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colSoundName.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.colSoundName.AppearanceHeader.Options.UseTextOptions = true;
            this.colSoundName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSoundName.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colSoundName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colSoundName.FieldName = "SoundName";
            this.colSoundName.Name = "colSoundName";
            this.colSoundName.OptionsColumn.AllowEdit = false;
            this.colSoundName.OptionsColumn.AllowFocus = false;
            this.colSoundName.OptionsColumn.ReadOnly = true;
            // 
            // colview
            // 
            this.colview.AppearanceCell.Options.UseTextOptions = true;
            this.colview.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colview.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colview.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.colview.AppearanceHeader.Options.UseTextOptions = true;
            this.colview.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colview.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colview.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colview.Caption = "Отчет";
            this.colview.FieldName = "ViewInReport";
            this.colview.Name = "colview";
            this.colview.OptionsColumn.AllowEdit = false;
            this.colview.OptionsColumn.AllowFocus = false;
            this.colview.OptionsColumn.ReadOnly = true;
            this.colview.ToolTip = "Отчет";
            this.colview.Visible = true;
            this.colview.VisibleIndex = 2;
            this.colview.Width = 119;
            // 
            // Editing
            // 
            this.Editing.ColumnEdit = this.rpItemPictureEdit1;
            this.Editing.FieldName = "Edit";
            this.Editing.Image = ((System.Drawing.Image)(resources.GetObject("Editing.Image")));
            this.Editing.ImageAlignment = System.Drawing.StringAlignment.Center;
            this.Editing.ImageIndex = 0;
            this.Editing.Name = "Editing";
            this.Editing.OptionsColumn.AllowEdit = false;
            this.Editing.OptionsColumn.AllowFocus = false;
            this.Editing.OptionsColumn.ReadOnly = true;
            this.Editing.ToolTip = "Редактирование события";
            this.Editing.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.Editing.Visible = true;
            this.Editing.VisibleIndex = 3;
            this.Editing.Width = 20;
            // 
            // rpItemPictureEdit1
            // 
            this.rpItemPictureEdit1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.rpItemPictureEdit1.InitialImage = ((System.Drawing.Image)(resources.GetObject("rpItemPictureEdit1.InitialImage")));
            this.rpItemPictureEdit1.Name = "rpItemPictureEdit1";
            this.rpItemPictureEdit1.ReadOnly = true;
            // 
            // Deleting
            // 
            this.Deleting.ColumnEdit = this.rpItemPictureEdit1;
            this.Deleting.FieldName = "Delete";
            this.Deleting.Image = ((System.Drawing.Image)(resources.GetObject("Deleting.Image")));
            this.Deleting.ImageAlignment = System.Drawing.StringAlignment.Center;
            this.Deleting.ImageIndex = 1;
            this.Deleting.Name = "Deleting";
            this.Deleting.OptionsColumn.AllowEdit = false;
            this.Deleting.OptionsColumn.AllowFocus = false;
            this.Deleting.OptionsColumn.ReadOnly = true;
            this.Deleting.ToolTip = "Удаление события";
            this.Deleting.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.Deleting.Visible = true;
            this.Deleting.VisibleIndex = 4;
            this.Deleting.Width = 20;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "pencil.png");
            this.imageList1.Images.SetKeyName(1, "cross.png");
            // 
            // SensorTransitionFormDvx
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 427);
            this.Controls.Add(this._mainTable);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SensorTransitionFormDvx";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "События для датчика";
            this._mainTable.ResumeLayout(false);
            this.carSensorTable.ResumeLayout(false);
            this.carSensorTable.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.editGroupBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.transControlGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sensorTransitionsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.transitionGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rpItemPictureEdit1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel _mainTable;
        private System.Windows.Forms.TableLayoutPanel carSensorTable;
        private System.Windows.Forms.Label carIcon;
        private System.Windows.Forms.Label sensorIcon;
        private System.Windows.Forms.Label carLbl;
        private System.Windows.Forms.Label sensorLbl;
        private System.Windows.Forms.Label carNameText;
        private System.Windows.Forms.Label sensorNameText;
        private DevExpress.XtraEditors.GroupControl editGroupBox;
        private DevExpress.XtraGrid.GridControl transControlGrid;
        private DevExpress.XtraGrid.Views.Grid.GridView transitionGridView;
        private System.Windows.Forms.BindingSource sensorTransitionsBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colId;
        private DevExpress.XtraGrid.Columns.GridColumn colInitialStateId;
        private DevExpress.XtraGrid.Columns.GridColumn colFinalStateId;
        private DevExpress.XtraGrid.Columns.GridColumn coltitle;
        private DevExpress.XtraGrid.Columns.GridColumn colenabled;
        private DevExpress.XtraGrid.Columns.GridColumn colSensorId;
        private DevExpress.XtraGrid.Columns.GridColumn colIconName;
        private DevExpress.XtraGrid.Columns.GridColumn colSoundName;
        private DevExpress.XtraGrid.Columns.GridColumn colview;
        private DevExpress.XtraGrid.Columns.GridColumn Editing;
        private DevExpress.XtraGrid.Columns.GridColumn Deleting;
        private System.Windows.Forms.ImageList imageList1;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit rpItemPictureEdit1;
    }
}