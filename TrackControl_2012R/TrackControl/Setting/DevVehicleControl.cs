﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;
using LocalCache;
using DevExpress.XtraEditors;
using System.Collections;
using DevExpress.XtraBars;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Columns;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.XtraTreeList.Nodes.Operations;
using DevExpress.XtraTreeList.ViewInfo;
using DevExpress.Utils;
using MySql.Data.MySqlClient;
using TrackControl.General;
using TrackControl.General.DatabaseDriver;
using TrackControl.Online;using System.Configuration;
using TrackControl.Vehicles;
using TrackControl.Properties;

namespace TrackControl.Setting
{
    public partial class DevVehicleControl : XtraUserControl, ITreeModel<OnlineVehicle>
    {
        protected static TreeList thisTree;
        protected static DevVehicleControl DevVehicle;
        private bool _isLoaded;
        VehiclesModel _model;
        string _condition;
        protected static atlantaDataSet dataset;

        public delegate void GetMobitel(int mobitel);

        public GetMobitel _gettMobitel = null;

        public event Action<Entity> SelectionChanged;
    	public event Action<VehiclesGroup> RemoveGroupClicked;
        public event Action<OnlineVehicle> VehicleSelected = delegate { };
        public event MethodInvoker CheckedChanged = delegate { };
        public event MethodInvoker SynhroCheckedVehicles;

        public DevVehicleControl( VehiclesModel model )
        {
            _model = model;
            InitializeComponent();
            init();
            this.Load += new System.EventHandler(this.this_Load);
            _model.RefreshTreeView += RefreshLastTime;
            this._tree.VirtualTreeGetChildNodes += new DevExpress.XtraTreeList.VirtualTreeGetChildNodesEventHandler(this.tree_VirtualTreeGetChildNodes);
            this._tree.VirtualTreeGetCellValue += new DevExpress.XtraTreeList.VirtualTreeGetCellValueEventHandler(this.tree_VirtualTreeGetCellValue);
            this._tree.BeforeCheckNode += new DevExpress.XtraTreeList.CheckNodeEventHandler(this.tree_BeforeCheckNode);
            this._tree.AfterCheckNode += new DevExpress.XtraTreeList.NodeEventHandler(this.tree_AfterCheckNode);
            this._tree.AfterFocusNode += new DevExpress.XtraTreeList.NodeEventHandler(this.tree_AfterFocusNode);
            this._tree.CustomDrawNodeImages += new DevExpress.XtraTreeList.CustomDrawNodeImagesEventHandler(this.tree_CustomDrawNodeImages);
            this._tree.CustomDrawNodeCell += new DevExpress.XtraTreeList.CustomDrawNodeCellEventHandler(this.tree_CustomDrawNodeCell);
            this._tree.FilterNode += new DevExpress.XtraTreeList.FilterNodeEventHandler(this.tree_FilterNode);
            this._funnelRepo.KeyUp += new System.Windows.Forms.KeyEventHandler(this.funnelRepo_KeyUp);
            this._eraseBtn.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.eraseBtn_ItemClick);
            this.ttcTree.GetActiveObjectInfo += new DevExpress.Utils.ToolTipControllerGetActiveObjectInfoEventHandler(this.ttcTree_GetActiveObjectInfo);
            this._tree.CustomDrawNodeCell += TreeOnCustomDrawNodeCell;
            this._tree.Click += TreeOnClick;
            this._tree.KeyUp += TreeOnKeyUp;
            this._tree.KeyDown += TreeOnDown;
            this._tree.FocusedNodeChanged += _tree_FocusedNodeChanged;
            thisTree = _tree;
            DevVehicle = this;
        }

        void _tree_FocusedNodeChanged( object sender, FocusedNodeChangedEventArgs e )
        {
            TreeListNode node = _tree.Selection[0];

            if( node != null )
            {
                if( node.Focused )
                {
                    IVehicle vehicle = _tree.GetDataRecordByNode( node ) as IVehicle;

                    if (null != vehicle)
                    {
                        int mobitel_id = vehicle.Mobitel.Id;

                        if( _gettMobitel != null )
                            _gettMobitel( mobitel_id );
                    }
                    
                    TreeListColumn[] columns = new TreeListColumn[_tree.VisibleColumnCount];

                    for( int i = 0; i < _tree.VisibleColumnCount; i++ )
                    {
                        columns[i] = _tree.GetColumnByVisibleIndex( i );
                    } // for

                    TreeListColumn col = columns[4];
                    string text = node.GetDisplayText( col );
                    DevAdminForm.FitSelectItem( text );
                } // if
            } // if
        } // _tree_FocusedNodeChanged

        private void TreeOnKeyUp(object sender, KeyEventArgs e)
        {
            //TODO: to do there code
        }

        private void TreeOnDown(object sender, KeyEventArgs e)
        {
            //TreeListNode node = _tree.Selection[0];

            //if( node != null )
            //{
            //    if( node.Focused )
            //    {
            //        TreeListColumn[] columns = new TreeListColumn[_tree.VisibleColumnCount];

            //        for( int i = 0; i < _tree.VisibleColumnCount; i++ )
            //        {
            //            columns[i] = _tree.GetColumnByVisibleIndex( i );
            //        } // for

            //        TreeListColumn col = columns[4];
            //        string text = node.GetDisplayText( col );
            //        DevAdminForm.FitSelectItem( text );

            //        TreeList list = sender as TreeList;

            //        if( list != null )
            //        {
            //            col = list.Columns[0];
            //            string numberPlate = node.GetDisplayText( col );

            //            if( _gettMobitel != null )
            //                _gettMobitel( numberPlate );
            //        }
            //    } // if
            //} // if
        } // TreeOnDown

        private void TreeOnClick(object sender, EventArgs eventArgs)
        {
            //TreeListNode node = _tree.Selection[0];

            //if (node != null)
            //{
            //    if (node.Focused)
            //    {
            //        TreeListColumn[] columns = new TreeListColumn[_tree.VisibleColumnCount];

            //        for(int i = 0; i < _tree.VisibleColumnCount; i++) 
            //        {
            //            columns[i] = _tree.GetColumnByVisibleIndex(i);
            //        } // for

            //        TreeListColumn col = columns[4];
            //        string text = node.GetDisplayText( col );
            //        DevAdminForm.FitSelectItem(text);
                    
            //        TreeList list = sender as TreeList;

            //        if (list != null)
            //        {
            //            col = list.Columns[0];
            //            string numberPlate = node.GetDisplayText(col);
                       
            //            if (_gettMobitel != null)
            //                _gettMobitel( numberPlate );
            //        }
            //    } // if
            //} // if
        } // TreeOnClick

        private void TreeOnCustomDrawNodeCell(object sender, CustomDrawNodeCellEventArgs e)
        {
            string text = e.CellText;
            string codeParameter = DevAdminForm.GetCodeParameter();

            if (text.Equals(codeParameter))
            {
                Brush foreBrush = new SolidBrush( Color.RoyalBlue );
                Brush backBrush = new SolidBrush( Color.RoyalBlue );

                e.Graphics.FillRectangle( backBrush, e.Bounds );

                Font font = new Font("Tahoma", 8, FontStyle.Bold);
                e.Appearance.Font = font;
                e.Graphics.DrawString( e.CellText, e.Appearance.Font, foreBrush, e.Bounds,
                    e.Appearance.GetStringFormat() );
            } // if
        } // TreeOnCustomDrawNodeCell

        public BarItemVisibility setShowVisible
        {
            set { bbiSynhronize.Visibility = value; }
        }

        private void init()
        {
            _eraseBtn.Glyph = Shared.FunnelClear;
            _funnelBox.Caption = Resources.Funnel;
           // _eraseBtn.Caption = Resources.Clear;
            _eraseBtn.Hint = Resources.ClearFilter;
            _titleCol.Caption = Resources.TitleCar; // Resources.TitleNumber;
            _descriptionCol.Caption = Resources.Descript;  // Resources.Desctiption;
            _loginCol.Caption = Resources.Login;
            bbiExpand.Glyph = Shared.Expand;
            bbiCollapce.Glyph = Shared.Collapce;
            bbiSynhronize.Glyph = Shared.Synchro;
            colNameParameter.Caption = Resources.CodeParameter;
            bbiExpand.Hint = Resources.Expande;
            bbiCollapce.Hint = Resources.Collapse;
            bbiSynhronize.Hint = Resources.Synhronize;
            bbiRefreshing.Hint = Resources.Refreshing;
            bbiDeleteParam.Hint = Resources.DeleteParamCar;
        }

        #region --   Члены интерфейса ITreeModel<OnlineVehicle>   --

        public OnlineVehicle Current
        {
            get
            { // Не удалось привести тип объекта "TrackControl.Reports.ReportVehicle" к типу "TrackControl.Online.OnlineVehicle"
                OnlineVehicle ov = null;
                IVehicle vehicle = _tree.GetDataRecordByNode( _tree.FocusedNode ) as IVehicle;

                if( null != vehicle )
                    ov = (OnlineVehicle)vehicle.Tag;

                return ov;
            } // get
        } // Current

        public IList<OnlineVehicle> Checked
        {
            get
            {
                CheckedVehiclesOperation operation = new CheckedVehiclesOperation();
                _tree.NodesIterator.DoOperation(operation);
                return operation.Vehicles;
            }
        } // Checked

        public IList<OnlineVehicle> GetAll()
        {
            List<OnlineVehicle> all = new List<OnlineVehicle>();
            foreach (IVehicle vehicle in _model.Vehicles)
            {
                all.Add((OnlineVehicle)vehicle.Tag);
            }
            
            return all.AsReadOnly();
        } // GetAll

        public OnlineVehicle GetById(int id)
        {
            return null;
        }

    #endregion

        public void RefreshTree()
        {
            _isLoaded = false;
            _tree.RefreshDataSource();
            //SetChecked();
        }

        void RefreshLastTime()
        {
            MethodInvoker action = RefreshLastTimeInvoke;

            if (_tree.InvokeRequired)
            {
                _tree.Invoke(action);
            }
            else
            {
                action();
            }
        } // RefreshLastTime

        void RefreshLastTimeInvoke()
        {
            UpdateTimeStatus operation = new UpdateTimeStatus(timeCol);
            _tree.NodesIterator.DoLocalOperation( operation, _tree.Nodes );
        }

        void this_Load(object sender, EventArgs e)
        {
            // читаем настройки из БД
            DevAdminForm_Load( this, null );

            _tree.DataSource = new object();
            TreeViewService.TreeListInitialView(_tree);
            setCondition(String.Empty);
            //RefreshTree();
        } // this_Load

        void tree_VirtualTreeGetChildNodes(object sender, VirtualTreeGetChildNodesInfo e)
        {
            if (!_isLoaded)
            {
                e.Children = new VehiclesGroup[] { _model.Root };
                _isLoaded = true;
            }
            else
            {
                VehiclesGroup group = e.Node as VehiclesGroup;
                if (null != group)
                {
                    IList children = new List<object>();
                    foreach (VehiclesGroup vg in group.OwnGroups)
                        if (true /*vg.AllItems.Count > 0*/)
                            children.Add(vg);

                    foreach (IVehicle vehicle in group.OwnItems)
                        children.Add(vehicle);

                    e.Children = children;
                } // if
                else // Если узел не является группой, значит не имеет дочерних узлов
                {
                    e.Children = new object[] { };
                } // else
            } // else
        } // tree_VirtualTreeGetChildNodes

        void tree_VirtualTreeGetCellValue(object sender, VirtualTreeGetCellValueInfo e)
        {
            if (e.Node is VehiclesGroup)
            {
                VehiclesGroup group = (VehiclesGroup)e.Node;
                    if (e.Column == _titleCol)
                        e.CellData = group.NameWithCounter;
            } // if
            else if (e.Node is IVehicle)
            {
                IVehicle vehicle = (IVehicle)e.Node;
                if ( e.Column == _titleCol )
                    e.CellData = vehicle.RegNumber;
                else if ( e.Column == _descriptionCol )
                    e.CellData = String.Format( "{0} {1}", vehicle.CarMaker, vehicle.CarModel );
                else if ( e.Column == timeCol )
                    e.CellData = vehicle.TimeStatus();
                else if ( e.Column == _loginCol )
                    e.CellData = vehicle.Mobitel.Login;
                else if (e.Column == colNameParameter)
                {
                    e.CellData = getNameParameter(vehicle.Settings.Id);
                }
            } // else if
        } // tree_VirtualTreeGetCellValue

        protected string getNameParameter(int id)
        {
            for ( int i = 0; i < atlantaDataSet.setting.Rows.Count; i++ )
            {
                atlantaDataSet.settingRow row = ( atlantaDataSet.settingRow )( atlantaDataSet.setting.Rows[i] );
                if (id == row.id)
                    return row.Name;
            } // for

            return "";
        } // getNameParameter

        //// грузим данные из базы
        protected void DevAdminForm_Load(object o, EventArgs e)
        {
            try
            {
                string cs =
                    Crypto.GetDecryptConnectionString(ConfigurationManager.ConnectionStrings["CS"].ConnectionString);

                //vehicleTableAdapter.Connection.ConnectionString = cs;

                if( DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse )
                {
                    MySqlConnection connection = (MySqlConnection)vehicleTableAdapter.Connection;
                    connection.ConnectionString = cs;
                }
                else if( DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse )
                {
                    SqlConnection connection = (SqlConnection)vehicleTableAdapter.Connection;
                    connection.ConnectionString = cs;
                }

                //settingTableAdapter.Connection.ConnectionString = cs;

                if( DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse )
                {
                    MySqlConnection connection = (MySqlConnection)settingTableAdapter.Connection;
                    connection.ConnectionString = cs;
                }
                else if( DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse )
                {
                    SqlConnection connection = (SqlConnection)settingTableAdapter.Connection;
                    connection.ConnectionString = cs;
                }

                atlantaDataSet = AppModel.Instance.DataSet;

                vehicleBindingSource.DataSource = atlantaDataSet.vehicle;
                settingBindingSource.DataSource = atlantaDataSet.setting;
            }
            catch (Exception ex)
            {
                return;
            }
        } // DevAdminForm_Load

        private void tree_BeforeCheckNode(object sender, CheckNodeEventArgs e)
        {
            TreeViewService.FixNodeState(e);
        }

        void tree_AfterCheckNode(object sender, NodeEventArgs e)
        {
            TreeViewService.SetCheckedChildNodes(e.Node);
            TreeViewService.SetCheckedParentNodes(e.Node);
            if (e.Node.TreeList.GetDataRecordByNode(e.Node) is VehiclesGroup)
            {
                GetChecked();
            }
            else
            {
                IVehicle vehicle = e.Node.TreeList.GetDataRecordByNode(e.Node) as IVehicle;
                if (vehicle != null)
                    vehicle.Checked = e.Node.Checked;
            }
            _tree.FocusedNode = e.Node;
            CheckedChanged();
        } // tree_AfterCheckNode

        void tree_AfterFocusNode(object sender, NodeEventArgs e)
        {
            OnlineVehicle vehicle = e.Node.Tag as OnlineVehicle;
            if (null != vehicle)
                VehicleSelected(vehicle);
        }

        void tree_CustomDrawNodeImages(object sender, CustomDrawNodeImagesEventArgs e)
        {
            Rectangle rect = e.SelectRect;
            rect.X += (rect.Width - 16) / 2;
            rect.Y += (rect.Height - 16) / 2;
            rect.Width = 16;
            rect.Height = 16;

            object obj = _tree.GetDataRecordByNode(e.Node);
            if (obj is IVehicle)
            {
                IVehicle vehicle = (IVehicle)obj;
                e.Graphics.DrawImage(vehicle.Style.Icon, rect);
            }
            else if (obj is VehiclesGroup)
            {
                VehiclesGroup group = (VehiclesGroup)obj;
                e.Graphics.DrawImage(group.Style.Icon, rect);
            }

            e.Handled = true;
        } // tree_CustomDrawNodeImages

        void tree_CustomDrawNodeCell(object sender, CustomDrawNodeCellEventArgs e)
        {
            object obj = _tree.GetDataRecordByNode(e.Node);
            if (obj is IVehicle) return;
            e.Appearance.Font = new Font(AppearanceObject.DefaultFont, FontStyle.Bold);

            if (e.Node != _tree.FocusedNode)
            e.Appearance.BackColor = AppearanceObject.ControlAppearance.BorderColor;
        } // tree_CustomDrawNodeCell 

        void tree_FilterNode(object sender, FilterNodeEventArgs e)
        {
            IVehicle vehicle = _tree.GetDataRecordByNode(e.Node) as IVehicle;

            if (null != vehicle)
            {
                if( _condition != null )
                {
                    e.Node.Visible = vehicle.RegNumber.Contains( _condition );
                }
                else
                {
                    e.Node.Visible = vehicle.RegNumber.Contains( "" );
                }

                e.Handled = true;
            }
        } // tree_FilterNode

        void funnelRepo_KeyUp(object sender, KeyEventArgs e)
        {
            TextEdit edit = (TextEdit)sender;
            setCondition(edit.Text);
        } // funnelRepo_KeyUp 

        void eraseBtn_ItemClick(object sender, ItemClickEventArgs e)
        {
            _funnelBox.EditValue = String.Empty;
            setCondition(String.Empty);
        }

        void setCondition(string condition)
        {
            _condition = condition;
            if (_condition.Length > 0)
            {
                _eraseBtn.Enabled = true;
                _tree.FilterNodes();
                hideEmpty();
            }
            else
            {
                 _eraseBtn.Enabled = false;
                 TreeListNode root = _tree.Nodes[0];
                TreeViewService.ShowAll(root);
            }
        } // setCondition

        void hideEmpty()
        {
            TreeListNode root = _tree.Nodes[0];
            foreach (TreeListNode node in root.Nodes)
            {
                VehiclesGroup group = _tree.GetDataRecordByNode(node) as VehiclesGroup;
                if (null != group)
                {
                    if (TreeViewService.GetVisibleCount(node) > 0)
                        node.Visible = true;
                    else
                        node.Visible = false;
                } // if
            } // foreach
        } // hideEmpty 

        #region --   Nested Classes   --

        private class CheckedVehiclesOperation : TreeListOperation
        {
            public List<OnlineVehicle> Vehicles
            {
              get { return _vehicles; }
            }

            List<OnlineVehicle> _vehicles = new List<OnlineVehicle>();
            VehiclesGroup group;
            public override bool NeedsVisitChildren(TreeListNode node)
            {
                group = node.TreeList.GetDataRecordByNode(node) as VehiclesGroup;
                return group != null;
            }

            public override void Execute(TreeListNode node)
            {
                IVehicle vehicle = node.TreeList.GetDataRecordByNode(node) as IVehicle;
                if (null != vehicle && node.Checked)
                {
                    OnlineVehicle ov = vehicle.Tag as OnlineVehicle;
                    ov.Group = group;
                    if (null != ov)
                        _vehicles.Add(ov);
                } // if
            } // Execute
        } // CheckedVehiclesOperation

        private class CheckNodeFromObjectOperation : TreeListOperation
        {
            public override bool NeedsVisitChildren(TreeListNode node)
            {
                VehiclesGroup group = node.TreeList.GetDataRecordByNode(node) as VehiclesGroup;
                return group != null;
            }

            public override void Execute(TreeListNode node)
            {
                IVehicle vehicle = node.TreeList.GetDataRecordByNode(node) as IVehicle;
                if (null != vehicle)
                {
                    node.Checked = vehicle.Checked;
                    TreeViewService.SetCheckedParentNodes(node);
                }
            }
        } // CheckNodeFromObjectOperation

        private class CheckObjectFromNodeOperation : TreeListOperation
        {
            public override bool NeedsVisitChildren(TreeListNode node)
            {
                VehiclesGroup group = node.TreeList.GetDataRecordByNode(node) as VehiclesGroup;
                return group != null;
            }

            public override void Execute(TreeListNode node)
            {
                IVehicle vehicle = node.TreeList.GetDataRecordByNode(node) as IVehicle;
                if (null != vehicle)
                {
                    vehicle.Checked = node.Checked;
                }
            }
        } // CheckObjectFromNodeOperation

        public class GetCheckObjectFromNodeOperation : TreeListOperation
        {
            List<IVehicle> vehicles = new List<IVehicle>();

            public List<IVehicle> getCheckVehicles()
            {
                return vehicles;
            }

            public override bool NeedsVisitChildren( TreeListNode node )
            {
                VehiclesGroup group = node.TreeList.GetDataRecordByNode( node ) as VehiclesGroup;
                return group != null;
            }

            public override void Execute( TreeListNode node )
            {
                IVehicle vehicle = node.TreeList.GetDataRecordByNode( node ) as IVehicle;
                if ( null != vehicle )
                {
                    vehicle.Checked = node.Checked;

                    if ( vehicle.Checked )
                    {
                        vehicles.Add( vehicle );
                    } // if
                } // if
            } // Execute
        } // GetCheckObjectFromNodeOperation

        private class UpdateTimeStatus : TreeListOperation
        {
            TreeListColumn _colTime;
            public UpdateTimeStatus(TreeListColumn colTime)
                : base()
            {
                _colTime = colTime;
            }

            public override void Execute(TreeListNode node)
            {
                object obj = node.TreeList.GetDataRecordByNode(node);

                if (obj is IVehicle)
                {
                    IVehicle vehicle = (IVehicle)obj;
                    node.SetValue(_colTime.VisibleIndex, vehicle.TimeStatus());
                }
            }
        } // UpdateTimeStatus

    #endregion

        private void bbiExpand_ItemClick(object sender, ItemClickEventArgs e)
        {
            _tree.ExpandAll();  
        }

        private void bbiCollapce_ItemClick(object sender, ItemClickEventArgs e)
        {
            _tree.CollapseAll();
            TreeViewService.TreeListInitialView(_tree);
        }

        private void ttcTree_GetActiveObjectInfo(object sender, ToolTipControllerGetActiveObjectInfoEventArgs e)
        {
            if (e.SelectedControl is DevExpress.XtraTreeList.TreeList)
            {
                TreeList tree = (TreeList)e.SelectedControl;
                TreeListHitInfo hit = tree.CalcHitInfo(e.ControlMousePosition);
                if (hit.Column == timeCol)
                {
                    if (hit.HitInfoType == HitInfoType.Cell)
                    {
                        object cellInfo = new TreeListCellToolTipInfo(hit.Node, hit.Column, null);
                        IVehicle vehicle = tree.GetDataRecordByNode(hit.Node) as IVehicle;
                        if (null != vehicle)
                        {
                            string toolTip = string.Format("{1}: {0}", vehicle.Mobitel.LastTimeGps.ToString(), Resources.LastData);
                            e.Info = new DevExpress.Utils.ToolTipControlInfo(cellInfo, toolTip);
                        }

                    }
                    else if (hit.HitInfoType == HitInfoType.Column)
                    {
                        e.Info = new DevExpress.Utils.ToolTipControlInfo(hit.Column, TreeViewService.GetToolTip());
                    }
                }
                else if (hit.Column == _titleCol)
                {
                    object cellInfo = new TreeListCellToolTipInfo( hit.Node, hit.Column, null );
                    IVehicle vehicle = tree.GetDataRecordByNode( hit.Node ) as IVehicle;
                    if( null != vehicle )
                    {
                        string toolTip = string.Format( "{0}", vehicle.VehicleComment );
                        e.Info = new DevExpress.Utils.ToolTipControlInfo( cellInfo, toolTip );
                    }
                }
            }
        } // ttcTree_GetActiveObjectInfo

        public void SetChecked()
        {
            CheckNodeFromObjectOperation operation = new CheckNodeFromObjectOperation();
            _tree.NodesIterator.DoOperation(operation);
        }

        public void GetChecked()
        {
            CheckObjectFromNodeOperation operation = new CheckObjectFromNodeOperation();
            _tree.NodesIterator.DoOperation(operation);
        }

        public void SetCheckedFromModel()
        {
            _tree.ExpandAll();
            RefreshTree();
        }

        void onSelectionChanged(Entity entity)
    	{
      		Action<Entity> handler = SelectionChanged;
      		if (null != handler)
        		handler(entity);
    	}
        
        void onRemoveGroupClicked(VehiclesGroup group)
    	{
      		Action<VehiclesGroup> handler = RemoveGroupClicked;
      		if (null != handler)
        		handler(group);
    	}

        private void bbiSynhronize_ItemClick( object sender, ItemClickEventArgs e )
        {
            if ( DialogResult.No == XtraMessageBox.Show( Resources.ConfirmSynhro, StaticMethods.GetTotalMessageCaption(), MessageBoxButtons.YesNo, MessageBoxIcon.Question ) ) return;
            GetChecked();
            if ( SynhroCheckedVehicles != null ) SynhroCheckedVehicles();
        }

        private void bbiRefreshing_ItemClick( object sender, ItemClickEventArgs e )
        {
            DevAdminForm_Load( null, null );
            RefreshTree();
        }

        public static TreeList GetTreeList()
        {
            return thisTree;
        }

        public static DevVehicleControl GetDevVehicle()
        {
            return DevVehicle;
        }

        // Удалить текущую настройку с машины и оставить настроку по умолчанию
        private void barButtonItem1_ItemClick(object sender, ItemClickEventArgs e)
        {
            settingTableAdapter.Update(atlantaDataSet.setting);
            vehicleTableAdapter.Update(atlantaDataSet.vehicle);

            // Получим список выделенных машин
            GetCheckObjectFromNodeOperation operation = new GetCheckObjectFromNodeOperation();
            _tree.NodesIterator.DoOperation( operation );
            List<IVehicle> vehicleList = operation.getCheckVehicles();

            for (int i = 0; i < vehicleList.Count; i++)
            {
                IVehicle vehicle = vehicleList[i];
                int mobitelId = vehicle.Mobitel.Id;

                foreach ( atlantaDataSet.vehicleRow v_row in atlantaDataSet.vehicle )
                {
                    int mobId = v_row.Mobitel_id;

                    if ( mobitelId == mobId )
                    {
                        v_row.setting_id = 1; // ставим настройку по умолчанию в базу данных
                        vehicle.Settings.Id = 1; // ставим настройку по умолчанию в лист дерева
                    } // if
                } // foreach
            } // for

            if (vehicleList.Count != 0)
            {
                vehicleTableAdapter.Update(atlantaDataSet.vehicle);
                atlantaDataSet.AcceptChanges();
                RefreshTree();
            } // if
        } // barButtonItem1_ItemClick

        public bool CheckBoxesShow
        {
            get { return _tree.OptionsView.ShowCheckBoxes; }
            set { _tree.OptionsView.ShowCheckBoxes = value; }
        }

        public BarItemVisibility DeleteButtonShow
        {
            set
            {
                bbiDeleteParam.Visibility = value;
            }
        }

        public List<IVehicle> GetCheckedNode
        {
            get
            {
                GetCheckObjectFromNodeOperation operation = new GetCheckObjectFromNodeOperation();
                _tree.NodesIterator.DoOperation( operation );
                return operation.getCheckVehicles();
            }
        }

        bool flagVisible = true;
        private void _tree_VisibleChanged(object sender, EventArgs e)
        {
             RefreshTree();
             flagVisible = false;
        }
    } // DevVehicleControl
} // TrackControl.Setting
