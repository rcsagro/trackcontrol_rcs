﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using LocalCache;
using TrackControl.General.DatabaseDriver;
using TrackControl.Setting.Controls;
using MySql.Data.MySqlClient;
using TrackControl.General.DatabaseDriver;
using TrackControl.Properties;
using TrackControl.Vehicles;

namespace TrackControl.Setting
{
    public partial class LogicSensorStatesDvx : DevExpress.XtraEditors.XtraForm
    {
        /// <summary>
        /// Контрол редактирования настроек логического датчика
        /// </summary>
        private LogicSensorEditPanel editControl;

        /// <summary>
        /// Контрол для настройки событий, порождаемых логическим датчиком.
        /// </summary>
        private LogicSensorEventsEditPanel eventEditControl;

        /// <summary>
        /// Адаптер для сохранения изменений в БД настроек логических датчиков.
        /// </summary>
        private LocalCache.atlantaDataSetTableAdapters.SensorsTableAdapter _sensorAdapter =
            new LocalCache.atlantaDataSetTableAdapters.SensorsTableAdapter();

        /// <summary>
        /// Адаптер для сохранения изменений в БД настроек состояния, определяемого
        /// по показаниям датчика.
        /// </summary>
        private LocalCache.atlantaDataSetTableAdapters.StateTableAdapter _stateAdapter =
            new LocalCache.atlantaDataSetTableAdapters.StateTableAdapter();

        /// <summary>
        /// Адаптер для сохранения изменений в БД настроек событий, возникающих
        /// при изменении состояния объекта.
        /// </summary>
        private LocalCache.atlantaDataSetTableAdapters.TransitionTableAdapter _transitionAdapter =
            new LocalCache.atlantaDataSetTableAdapters.TransitionTableAdapter();

        /// <summary>
        /// Адаптер для сохранения изменений в таблице relationalgorithms
        /// </summary>
        private LocalCache.atlantaDataSetTableAdapters.RelationalgorithmsTableAdapter _relAlgAdapter =
            new LocalCache.atlantaDataSetTableAdapters.RelationalgorithmsTableAdapter();

        /// <summary>
        /// Настройки логического датчика, которые сейчас добавляются к телетреку.
        /// В режиме редактирования существующих настроек, данное поле есть NULL.
        /// Поле введено, чтобы в случае отмены добавления настроек в БД, удалить их из датасета.
        /// </summary>
        private atlantaDataSet.sensorsRow sensorWhenAdding;

        private LocalCache.atlantaDataSet atlantaDataSet;

        /// <summary>
        /// Настройки логического датчика
        /// </summary>
        private atlantaDataSet.sensorsRow _sensor;

        /// <summary>
        /// Состояние при показаниях датчика "0".
        /// </summary>
        private atlantaDataSet.StateRow _falseState;

        /// <summary>
        /// Состояние при показаниях датчика "1".
        /// </summary>
        private atlantaDataSet.StateRow _trueState;

        /// <summary>
        /// Объект подключения к базе данных MySQL.
        /// </summary>
        private object _connection;

        private bool TypeDialogWindows = true;

        public LogicSensorStatesDvx()
        {
            InitializeComponent();

            Localization();

            LogicSensorStatesDvx_Load();
        }

        private void Localization()
        {
            Text = Resources.LogicSensorStatesDvx;
            editPanel.Text = Resources.StatesPanel;
            eventPanel.Text = Resources.EventPanel;
        }

        /// <summary>
        /// Обрабатывает событие загрузки контрола
        /// </summary>
        private void LogicSensorStatesDvx_Load()
        {
            string CONNECTION_STRING =
                Crypto.GetDecryptConnectionString(
                    System.Configuration.ConfigurationManager.ConnectionStrings["CS"].ConnectionString);

            DriverDb db = new DriverDb();
            _connection = db.NewSqlConnection(CONNECTION_STRING);

            atlantaDataSet = AppModel.Instance.DataSet;
            mobitelsBindingSource.DataSource = atlantaDataSet;
            vehicleBindingSource.DataSource = atlantaDataSet.vehicle;

            _sensorAdapter.Connection = _connection;
            _stateAdapter.Connection = _connection;
            _transitionAdapter.Connection = _connection;
            _relAlgAdapter.Connection = _connection;

            editControl = new LogicSensorEditPanel(_sensorAdapter, _stateAdapter, _relAlgAdapter, atlantaDataSet);
            editControl.Dock = DockStyle.Fill;
            editControl.Visible = false;
            editControl.EndEdit += new EventHandler<EndEditEventArgs>(onEndEdit);
            editPanel.Controls.Add(editControl);

            eventEditControl = new LogicSensorEventsEditPanel(atlantaDataSet, _transitionAdapter);
            eventEditControl.Dock = DockStyle.Fill;
            eventEditControl.Visible = false;
            eventEditControl.EndEditEvent += new EventHandler<EndEditEventArgs>(onEndEditEvent);
            eventPanel.Controls.Add(eventEditControl);
        }

        /// <summary>
        /// Обрабатывает событие окончания редактирования настроек
        /// логического датчика
        private void onEndEdit(object sender, EndEditEventArgs e)
        {
            if (e.Canceled && sensorWhenAdding != null)
            {
                sensorWhenAdding.Delete();
            }

            sensorWhenAdding = null;
            editPanel.Controls[0].Visible = true;
            editPanel.Controls[1].Visible = false;
            Close();
        }

        /// <summary>
        /// Обрабатывает событие окончания редактирования настроек
        /// логического датчика
        private void onEndEditEvent(object sender, EndEditEventArgs e)
        {
            if (e.Canceled && sensorWhenAdding != null)
            {
                sensorWhenAdding.Delete();
            }
            sensorWhenAdding = null;
            eventPanel.Controls[0].Visible = true;
            eventPanel.Controls[1].Visible = false;
            Close();
        }

        /// <summary>
        /// Редактирует настройки логического датчика
        /// </summary>
        /// <param name="sensor">строка из таблицы датчиков в датасете. По сути, логический датчик</param>
        public void ShowLogicState(atlantaDataSet.sensorsRow sensor)
        {
            _sensor = sensor;

            foreach (atlantaDataSet.StateRow state in sensor.GetStateRows())
            {
                if (state.MinValue < 0.1)
                    _falseState = state;
                else if (state.MinValue >= 1)
                    _trueState = state;
            }
        }

        /// <summary>
        /// Редактирует настройки логического датчика
        /// </summary>
        /// <param name="sensor">строка из таблицы датчиков в датасете. По сути, логический датчик</param>
        public void EditSensor(atlantaDataSet.sensorsRow sensor)
        {
            editControl.Visible = true;
            editControl.EditSensor(sensor);
            ShowLogicState(sensor);
        }

        public void EditableSensor(int mobitelsid, bool typecondition)
        {
            TypeDialogWindows = typecondition;

            if (TypeDialogWindows)
            {
                splitContainerControl1.Panel1.Visible = true;
                splitContainerControl1.Panel2.Visible = false;
                eventPanel.Visible = false;
                editPanel.Visible = true;
                Width = editPanel.Width + 13;
                FormBorderStyle = FormBorderStyle.FixedDialog;
                splitContainerControl1.SplitterPosition = editPanel.Width;
             
                atlantaDataSet.sensorsRow sensor = getCurrentSensor(mobitelsid);

                if (sensor != null)
                {
                    editPanel.Controls[0].Visible = false;
                    editPanel.Controls[1].Visible = true;

                    editSensor(sensor);
                }
            }
            else
            {
                splitContainerControl1.Panel1.Visible = false;
                splitContainerControl1.Panel2.Visible = true;
                editPanel.Visible = false;
                eventPanel.Visible = true;
                Width = editPanel.Width + 40;
                FormBorderStyle = FormBorderStyle.FixedDialog;
                splitContainerControl1.SplitterPosition = 0;
              
                atlantaDataSet.sensorsRow sensor = getCurrentSensor(AdminForm.getCurrentMobitelId);

                if (sensor != null)
                {
                    eventPanel.Controls[0].Visible = false;
                    eventPanel.Controls[1].Visible = true;

                    editEventForSensor(sensor);
                }
            }
        }

        private atlantaDataSet.sensorsRow getCurrentSensor(int thisMobitel)
        {
            if (AdminForm.gridViewLogicCurrentRow == -1)
                return null;

            mobitelsBindingSource.Filter = "Mobitel_id = " + thisMobitel;
            int itemFound = mobitelsBindingSource.Find("Mobitel_id", thisMobitel);

            if (itemFound == -1)
                return null;

            return
                (atlantaDataSet.sensorsRow)
                    ((DataRowView) mobitelsSensorsBindingSource.List[AdminForm.gridViewLogicCurrentRow]).Row;
        }

        private void editSensor(LocalCache.atlantaDataSet.sensorsRow sensor)
        {
            editControl.EditSensor(sensor);
        }

        /// <summary>
        /// Редактирует настройки событий для логического датчика.
        /// </summary>
        /// <param name="sensor">Логический датчик, для которого настраиваются события.</param>
        private void editEventForSensor(atlantaDataSet.sensorsRow sensor)
        {
            eventEditControl.Visible = true;
            eventEditControl.EditEventsForSensor(sensor);
        }
    }
}