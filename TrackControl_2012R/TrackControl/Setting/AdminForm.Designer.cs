namespace TrackControl.Setting
{
  partial class AdminForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
            this.components = new System.ComponentModel.Container();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode2 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode3 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode4 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode5 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode6 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode7 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode8 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode9 = new DevExpress.XtraGrid.GridLevelNode();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AdminForm));
            this._tabsPcbPage = new DevExpress.XtraTab.XtraTabControl();
            this._adminTab = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.adminTable = new System.Windows.Forms.TableLayoutPanel();
            this.lanSettings = new TrackControl.Setting.Control.LanSettings();
            this._layoutReseter = new TrackControl.Setting.Control.LayoutReset();
            this.passChanger1 = new TrackControl.Setting.Control.PassChanger();
            this.pgParams = new DevExpress.XtraVerticalGrid.PropertyGridControl();
            this.chDataAnaliz = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.rleLang = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.chOpenStreetMapUse = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.osmLang = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.chGraphFuelShow = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.chMinTimeMove = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.textEditCode = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.crTotalSettings = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.erIsDataAnalizOn = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erIsGraphFuelShow = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erMapLang = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erGoogleSatelliteKey = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.crLuxena = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.erIsLuxenaUse = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erLuxenaLang = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erLuxenaServer = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.crOpenStreet = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.erIsOpenStreetMapUse = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erOpenStreetMapServer = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erOpenStreetMapPort = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erOpenStreetMapKey = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.crRcsNominatim = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.erIsRcsNominatimUse = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.crGoogleMap = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.erIsGoogleMapUse = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erGoogleMapKey = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this._settingsTab = new DevExpress.XtraTab.XtraTabPage();
            this.pcbPageTab = new DevExpress.XtraTab.XtraTabPage();
            this.tpAdjustingSensors = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.controlTransport = new DevExpress.XtraEditors.XtraUserControl();
            this.splitContainerControl3 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridControlPcbVers = new DevExpress.XtraGrid.GridControl();
            this.gridPcbVers = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colNamePcb = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colComment = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSetting = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.btnSettingPcb = new DevExpress.XtraEditors.SimpleButton();
            this.splitContainerControl4 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridControlSetup = new DevExpress.XtraGrid.GridControl();
            this.gridSetups = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTypeData = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStartBit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLengthRecord = new DevExpress.XtraGrid.Columns.GridColumn();
            this.columnNameUnit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colKoeffK = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colShifter = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSigning = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colComms = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTarirovka = new DevExpress.XtraGrid.Columns.GridColumn();
            this.taririvkaItemButtonEdit = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colState = new DevExpress.XtraGrid.Columns.GridColumn();
            this.stateItemButtonEdit = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colSituation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.situationItemButtonEdit = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colAlgorithm = new DevExpress.XtraGrid.Columns.GridColumn();
            this.algorithmComboBox = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.colIdData = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIdPcbs = new DevExpress.XtraGrid.Columns.GridColumn();
            this.itemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.gridControlLogic = new DevExpress.XtraGrid.GridControl();
            this.sensorsGrid = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.nameDataGridViewTextBoxColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.startBitDataGridViewTextBoxColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SpecSettingColumn = new DevExpress.XtraGrid.Columns.GridColumn();
            this.id = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colState0 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colState1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCondition = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rpstrItemButtonEdit = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colEvents = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rpsItemButtonEdit = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.rpItemButtonEdit = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.analysisTab = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl5 = new DevExpress.XtraEditors.SplitContainerControl();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.panelVehicles = new DevExpress.XtraEditors.XtraUserControl();
            this.splitContainerControl6 = new DevExpress.XtraEditors.SplitContainerControl();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.ctrlAnalisis = new DevExpress.XtraGrid.GridControl();
            this.gridAnalisis = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.Car = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DateB = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DateE = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Interval = new DevExpress.XtraGrid.Columns.GridColumn();
            this.QTYpoints = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.barBeginPeriod = new DevExpress.XtraBars.BarStaticItem();
            this.barBeginDate = new DevExpress.XtraBars.BarEditItem();
            this.dateBeginPeriod = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.barEndPeriod = new DevExpress.XtraBars.BarStaticItem();
            this.barEndDate = new DevExpress.XtraBars.BarEditItem();
            this.dateEndPeriod = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.txMine = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.standaloneBarDockControl1 = new DevExpress.XtraBars.StandaloneBarDockControl();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btnDataAnalysis = new DevExpress.XtraBars.BarButtonItem();
            this.btnNoGps = new DevExpress.XtraBars.BarButtonItem();
            this.btnDeleteNoCorrect = new DevExpress.XtraBars.BarButtonItem();
            this.btnMissingData = new DevExpress.XtraBars.BarButtonItem();
            this.btnAdding = new DevExpress.XtraBars.BarButtonItem();
            this.btnDuplicatedData = new DevExpress.XtraBars.BarButtonItem();
            this.barBtnCancel = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.lbInfor = new DevExpress.XtraBars.BarEditItem();
            this.textInfo = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.pbAnaliz = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemProgressBar2 = new DevExpress.XtraEditors.Repository.RepositoryItemProgressBar();
            this.textEditPeriod = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.textEndPeriod = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dateLengthInterval = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemProgressBar1 = new DevExpress.XtraEditors.Repository.RepositoryItemProgressBar();
            this.repositoryItemProgressBar3 = new DevExpress.XtraEditors.Repository.RepositoryItemProgressBar();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.pbAnalize = new DevExpress.XtraEditors.ProgressBarControl();
            this.usersTab = new DevExpress.XtraTab.XtraTabPage();
            this.mobitelsSensorsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mobitelsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.atlantaDataSet1 = new LocalCache.atlantaDataSet();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.sensorStateFalseBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.sensorStateTrueBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.vehicleBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            ((System.ComponentModel.ISupportInitialize)(this._tabsPcbPage)).BeginInit();
            this._tabsPcbPage.SuspendLayout();
            this._adminTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            this.adminTable.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pgParams)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chDataAnaliz)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleLang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chOpenStreetMapUse)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.osmLang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chGraphFuelShow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chMinTimeMove)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditCode)).BeginInit();
            this.tpAdjustingSensors.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl3)).BeginInit();
            this.splitContainerControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlPcbVers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridPcbVers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl4)).BeginInit();
            this.splitContainerControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlSetup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSetups)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.taririvkaItemButtonEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stateItemButtonEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.situationItemButtonEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.algorithmComboBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.itemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlLogic)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sensorsGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rpstrItemButtonEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rpsItemButtonEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rpItemButtonEdit)).BeginInit();
            this.analysisTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl5)).BeginInit();
            this.splitContainerControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl6)).BeginInit();
            this.splitContainerControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ctrlAnalisis)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridAnalisis)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateBeginPeriod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateBeginPeriod.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEndPeriod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEndPeriod.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBar2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPeriod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEndPeriod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateLengthInterval)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBar3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbAnalize.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mobitelsSensorsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mobitelsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.atlantaDataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sensorStateFalseBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sensorStateTrueBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vehicleBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            this.SuspendLayout();
            // 
            // _tabsPcbPage
            // 
            this._tabsPcbPage.Dock = System.Windows.Forms.DockStyle.Fill;
            this._tabsPcbPage.Location = new System.Drawing.Point(0, 0);
            this._tabsPcbPage.Name = "_tabsPcbPage";
            this._tabsPcbPage.SelectedTabPage = this._adminTab;
            this._tabsPcbPage.ShowToolTips = DevExpress.Utils.DefaultBoolean.True;
            this._tabsPcbPage.Size = new System.Drawing.Size(1189, 542);
            this._tabsPcbPage.TabIndex = 0;
            this._tabsPcbPage.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this._adminTab,
            this._settingsTab,
            this.pcbPageTab,
            this.tpAdjustingSensors,
            this.analysisTab,
            this.usersTab});
            // 
            // _adminTab
            // 
            this._adminTab.Controls.Add(this.splitContainerControl1);
            this._adminTab.Name = "_adminTab";
            this._adminTab.Padding = new System.Windows.Forms.Padding(15);
            this._adminTab.Size = new System.Drawing.Size(1183, 514);
            this._adminTab.Text = "�������������";
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Location = new System.Drawing.Point(15, 15);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.adminTable);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.pgParams);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(1153, 484);
            this.splitContainerControl1.SplitterPosition = 777;
            this.splitContainerControl1.TabIndex = 2;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // adminTable
            // 
            this.adminTable.ColumnCount = 5;
            this.adminTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45F));
            this.adminTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 350F));
            this.adminTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.adminTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 354F));
            this.adminTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45F));
            this.adminTable.Controls.Add(this.lanSettings, 3, 0);
            this.adminTable.Controls.Add(this._layoutReseter, 1, 1);
            this.adminTable.Controls.Add(this.passChanger1, 1, 0);
            this.adminTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.adminTable.Location = new System.Drawing.Point(0, 0);
            this.adminTable.Margin = new System.Windows.Forms.Padding(0);
            this.adminTable.Name = "adminTable";
            this.adminTable.RowCount = 3;
            this.adminTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 181F));
            this.adminTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 181F));
            this.adminTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.adminTable.Size = new System.Drawing.Size(777, 484);
            this.adminTable.TabIndex = 2;
            // 
            // lanSettings
            // 
            this.lanSettings.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lanSettings.Location = new System.Drawing.Point(392, 3);
            this.lanSettings.Name = "lanSettings";
            this.lanSettings.Size = new System.Drawing.Size(348, 175);
            this.lanSettings.TabIndex = 1;
            // 
            // _layoutReseter
            // 
            this._layoutReseter.Dock = System.Windows.Forms.DockStyle.Fill;
            this._layoutReseter.Location = new System.Drawing.Point(35, 184);
            this._layoutReseter.Name = "_layoutReseter";
            this._layoutReseter.Size = new System.Drawing.Size(344, 175);
            this._layoutReseter.TabIndex = 2;
            // 
            // passChanger1
            // 
            this.passChanger1.Location = new System.Drawing.Point(35, 3);
            this.passChanger1.Name = "passChanger1";
            this.passChanger1.Size = new System.Drawing.Size(344, 174);
            this.passChanger1.TabIndex = 0;
            // 
            // pgParams
            // 
            this.pgParams.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pgParams.Location = new System.Drawing.Point(0, 0);
            this.pgParams.Name = "pgParams";
            this.pgParams.OptionsBehavior.PropertySort = DevExpress.XtraVerticalGrid.PropertySort.NoSort;
            this.pgParams.RecordWidth = 51;
            this.pgParams.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.chDataAnaliz,
            this.rleLang,
            this.chOpenStreetMapUse,
            this.osmLang,
            this.chGraphFuelShow,
            this.chMinTimeMove,
            this.textEditCode});
            this.pgParams.RowHeaderWidth = 149;
            this.pgParams.Rows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.crTotalSettings,
            this.crLuxena,
            this.crOpenStreet,
            this.crRcsNominatim,
            this.crGoogleMap});
            this.pgParams.Size = new System.Drawing.Size(371, 484);
            this.pgParams.TabIndex = 0;
            this.pgParams.CellValueChanged += new DevExpress.XtraVerticalGrid.Events.CellValueChangedEventHandler(this.pgParams_CellValueChanged);
            // 
            // chDataAnaliz
            // 
            this.chDataAnaliz.AutoHeight = false;
            this.chDataAnaliz.Caption = "Check";
            this.chDataAnaliz.Name = "chDataAnaliz";
            // 
            // rleLang
            // 
            this.rleLang.AutoHeight = false;
            this.rleLang.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rleLang.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "����")});
            this.rleLang.DisplayMember = "Name";
            this.rleLang.Name = "rleLang";
            this.rleLang.NullText = "";
            this.rleLang.ValueMember = "Name";
            // 
            // chOpenStreetMapUse
            // 
            this.chOpenStreetMapUse.AutoHeight = false;
            this.chOpenStreetMapUse.Caption = "Check";
            this.chOpenStreetMapUse.Name = "chOpenStreetMapUse";
            // 
            // osmLang
            // 
            this.osmLang.AutoHeight = false;
            this.osmLang.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.osmLang.DisplayMember = "Name";
            this.osmLang.Name = "osmLang";
            this.osmLang.NullText = "";
            this.osmLang.ValueMember = "Name";
            // 
            // chGraphFuelShow
            // 
            this.chGraphFuelShow.AutoHeight = false;
            this.chGraphFuelShow.Caption = "Check";
            this.chGraphFuelShow.Name = "chGraphFuelShow";
            // 
            // chMinTimeMove
            // 
            this.chMinTimeMove.AutoHeight = false;
            this.chMinTimeMove.Name = "chMinTimeMove";
            // 
            // textEditCode
            // 
            this.textEditCode.AutoHeight = false;
            this.textEditCode.Name = "textEditCode";
            // 
            // crTotalSettings
            // 
            this.crTotalSettings.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.erIsDataAnalizOn,
            this.erIsGraphFuelShow,
            this.erMapLang,
            this.erGoogleSatelliteKey});
            this.crTotalSettings.Height = 15;
            this.crTotalSettings.Name = "crTotalSettings";
            this.crTotalSettings.Properties.Caption = "����� ���������";
            // 
            // erIsDataAnalizOn
            // 
            this.erIsDataAnalizOn.Name = "erIsDataAnalizOn";
            this.erIsDataAnalizOn.Properties.Caption = "�������� ������ ���� � ������� ��� ��������";
            this.erIsDataAnalizOn.Properties.FieldName = "IsDataAnalizOn";
            this.erIsDataAnalizOn.Properties.RowEdit = this.chDataAnaliz;
            // 
            // erIsGraphFuelShow
            // 
            this.erIsGraphFuelShow.Name = "erIsGraphFuelShow";
            this.erIsGraphFuelShow.Properties.Caption = "����������� �������� �������";
            this.erIsGraphFuelShow.Properties.FieldName = "IsGraphFuelShow";
            this.erIsGraphFuelShow.Properties.RowEdit = this.chGraphFuelShow;
            this.erIsGraphFuelShow.Properties.ToolTip = "����������� �������� �������";
            // 
            // erMapLang
            // 
            this.erMapLang.Name = "erMapLang";
            this.erMapLang.Properties.Caption = "���� �������� ����������";
            this.erMapLang.Properties.FieldName = "MapsLang";
            this.erMapLang.Properties.RowEdit = this.osmLang;
            this.erMapLang.Properties.ToolTip = "���� ��������� ���� ";
            // 
            // erGoogleSatelliteKey
            // 
            this.erGoogleSatelliteKey.Name = "erGoogleSatelliteKey";
            this.erGoogleSatelliteKey.Properties.Caption = "������ ����������� ���� Google Map";
            this.erGoogleSatelliteKey.Properties.FieldName = "GoogleSatteliteKey";
            this.erGoogleSatelliteKey.Properties.RowEdit = this.textEditCode;
            this.erGoogleSatelliteKey.Properties.ToolTip = "������ ����������� ���� Google Map";
            // 
            // crLuxena
            // 
            this.crLuxena.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.erIsLuxenaUse,
            this.erLuxenaLang,
            this.erLuxenaServer});
            this.crLuxena.Name = "crLuxena";
            this.crLuxena.Properties.Caption = "�������� ���� Luxena";
            this.crLuxena.Visible = false;
            // 
            // erIsLuxenaUse
            // 
            this.erIsLuxenaUse.Name = "erIsLuxenaUse";
            this.erIsLuxenaUse.Properties.Caption = "������������ �������� ����";
            this.erIsLuxenaUse.Properties.FieldName = "IsLuxenaUse";
            this.erIsLuxenaUse.Properties.RowEdit = this.chDataAnaliz;
            // 
            // erLuxenaLang
            // 
            this.erLuxenaLang.Name = "erLuxenaLang";
            this.erLuxenaLang.Properties.Caption = "���� �������� ����������";
            this.erLuxenaLang.Properties.FieldName = "LuxenaLang";
            this.erLuxenaLang.Properties.RowEdit = this.rleLang;
            // 
            // erLuxenaServer
            // 
            this.erLuxenaServer.Name = "erLuxenaServer";
            this.erLuxenaServer.Properties.Caption = "������";
            this.erLuxenaServer.Properties.FieldName = "LuxenaServer";
            // 
            // crOpenStreet
            // 
            this.crOpenStreet.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.erIsOpenStreetMapUse,
            this.erOpenStreetMapServer,
            this.erOpenStreetMapPort,
            this.erOpenStreetMapKey});
            this.crOpenStreet.Name = "crOpenStreet";
            this.crOpenStreet.Properties.Caption = "�������� ���� OpenStreetMap";
            this.crOpenStreet.Properties.ToolTip = "�������� ���� OpenStrretMap";
            // 
            // erIsOpenStreetMapUse
            // 
            this.erIsOpenStreetMapUse.Name = "erIsOpenStreetMapUse";
            this.erIsOpenStreetMapUse.Properties.Caption = "������������ �������� ����";
            this.erIsOpenStreetMapUse.Properties.FieldName = "IsOpenStreetMapUse";
            this.erIsOpenStreetMapUse.Properties.RowEdit = this.chOpenStreetMapUse;
            this.erIsOpenStreetMapUse.Properties.ToolTip = "������������ �������� ����";
            // 
            // erOpenStreetMapServer
            // 
            this.erOpenStreetMapServer.Name = "erOpenStreetMapServer";
            this.erOpenStreetMapServer.Properties.Caption = "������";
            this.erOpenStreetMapServer.Properties.FieldName = "OpenStreetMapServer";
            this.erOpenStreetMapServer.Properties.ToolTip = "������ OpenStreetMap";
            // 
            // erOpenStreetMapPort
            // 
            this.erOpenStreetMapPort.Name = "erOpenStreetMapPort";
            this.erOpenStreetMapPort.Properties.Caption = "����";
            this.erOpenStreetMapPort.Properties.FieldName = "OpenStreetMapPort";
            this.erOpenStreetMapPort.Properties.ToolTip = "���� OpenStreetMap";
            this.erOpenStreetMapPort.Visible = false;
            // 
            // erOpenStreetMapKey
            // 
            this.erOpenStreetMapKey.Name = "erOpenStreetMapKey";
            this.erOpenStreetMapKey.Properties.Caption = "����";
            this.erOpenStreetMapKey.Properties.FieldName = "OpenStreetMapKey";
            this.erOpenStreetMapKey.Properties.ToolTip = "���� ������� � �������� OpenStreetMap";
            // 
            // crRcsNominatim
            // 
            this.crRcsNominatim.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.erIsRcsNominatimUse});
            this.crRcsNominatim.Height = 15;
            this.crRcsNominatim.Name = "crRcsNominatim";
            this.crRcsNominatim.Properties.Caption = "�������� ���� RcsNominatim";
            this.crRcsNominatim.Properties.ToolTip = "�������� ���� RcsNominatim";
            // 
            // erIsRcsNominatimUse
            // 
            this.erIsRcsNominatimUse.Height = 17;
            this.erIsRcsNominatimUse.Name = "erIsRcsNominatimUse";
            this.erIsRcsNominatimUse.Properties.Caption = "������������ �������� ����";
            this.erIsRcsNominatimUse.Properties.FieldName = "IsRcsNominatimUse";
            this.erIsRcsNominatimUse.Properties.RowEdit = this.chOpenStreetMapUse;
            this.erIsRcsNominatimUse.Properties.ToolTip = "������������ �������� ����";
            // 
            // crGoogleMap
            // 
            this.crGoogleMap.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.erIsGoogleMapUse,
            this.erGoogleMapKey});
            this.crGoogleMap.Name = "crGoogleMap";
            this.crGoogleMap.Properties.Caption = "�������� ���� GoogleMap";
            this.crGoogleMap.Properties.ToolTip = "�������� ���� GoogleMap";
            this.crGoogleMap.Visible = false;
            // 
            // erIsGoogleMapUse
            // 
            this.erIsGoogleMapUse.Name = "erIsGoogleMapUse";
            this.erIsGoogleMapUse.Properties.Caption = "������������ �������� ����";
            this.erIsGoogleMapUse.Properties.FieldName = "IsGoogleMapUse";
            this.erIsGoogleMapUse.Properties.RowEdit = this.chOpenStreetMapUse;
            this.erIsGoogleMapUse.Properties.ToolTip = "������������ �������� ����";
            // 
            // erGoogleMapKey
            // 
            this.erGoogleMapKey.Name = "erGoogleMapKey";
            this.erGoogleMapKey.Properties.Caption = "����";
            this.erGoogleMapKey.Properties.FieldName = "GoogleMapKey";
            this.erGoogleMapKey.Properties.ToolTip = "���� GoogleMap";
            // 
            // _settingsTab
            // 
            this._settingsTab.Name = "_settingsTab";
            this._settingsTab.Size = new System.Drawing.Size(1183, 514);
            this._settingsTab.Text = "���������";
            // 
            // pcbPageTab
            // 
            this.pcbPageTab.Name = "pcbPageTab";
            this.pcbPageTab.Size = new System.Drawing.Size(1183, 514);
            this.pcbPageTab.Text = "����� ��������";
            // 
            // tpAdjustingSensors
            // 
            this.tpAdjustingSensors.Controls.Add(this.splitContainerControl2);
            this.tpAdjustingSensors.Name = "tpAdjustingSensors";
            this.tpAdjustingSensors.Size = new System.Drawing.Size(1183, 514);
            this.tpAdjustingSensors.Text = "��������� ��������";
            this.tpAdjustingSensors.Tooltip = "��������� ��������";
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl2.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.splitContainerControl2.Panel1.Controls.Add(this.controlTransport);
            this.splitContainerControl2.Panel1.Text = "Panel1";
            this.splitContainerControl2.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.splitContainerControl2.Panel2.Controls.Add(this.splitContainerControl3);
            this.splitContainerControl2.Panel2.Text = "Panel2";
            this.splitContainerControl2.Size = new System.Drawing.Size(1183, 514);
            this.splitContainerControl2.SplitterPosition = 213;
            this.splitContainerControl2.TabIndex = 2;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // controlTransport
            // 
            this.controlTransport.Appearance.BackColor = System.Drawing.Color.Gainsboro;
            this.controlTransport.Appearance.Options.UseBackColor = true;
            this.controlTransport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.controlTransport.Location = new System.Drawing.Point(0, 0);
            this.controlTransport.Name = "controlTransport";
            this.controlTransport.Size = new System.Drawing.Size(209, 510);
            this.controlTransport.TabIndex = 1;
            // 
            // splitContainerControl3
            // 
            this.splitContainerControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl3.Horizontal = false;
            this.splitContainerControl3.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl3.Name = "splitContainerControl3";
            this.splitContainerControl3.Panel1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.splitContainerControl3.Panel1.Controls.Add(this.gridControlPcbVers);
            this.splitContainerControl3.Panel1.Controls.Add(this.panelControl1);
            this.splitContainerControl3.Panel1.ShowCaption = true;
            this.splitContainerControl3.Panel1.Text = "������� ���� ��������";
            this.splitContainerControl3.Panel2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.splitContainerControl3.Panel2.Controls.Add(this.splitContainerControl4);
            this.splitContainerControl3.Panel2.ShowCaption = true;
            this.splitContainerControl3.Panel2.Text = "����������� ���������";
            this.splitContainerControl3.Size = new System.Drawing.Size(961, 510);
            this.splitContainerControl3.SplitterPosition = 320;
            this.splitContainerControl3.TabIndex = 0;
            this.splitContainerControl3.Text = "splitContainerControl3";
            // 
            // gridControlPcbVers
            // 
            this.gridControlPcbVers.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlPcbVers.Location = new System.Drawing.Point(0, 0);
            this.gridControlPcbVers.MainView = this.gridPcbVers;
            this.gridControlPcbVers.Name = "gridControlPcbVers";
            this.gridControlPcbVers.Size = new System.Drawing.Size(957, 257);
            this.gridControlPcbVers.TabIndex = 4;
            this.gridControlPcbVers.UseEmbeddedNavigator = true;
            this.gridControlPcbVers.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridPcbVers});
            // 
            // gridPcbVers
            // 
            this.gridPcbVers.ColumnPanelRowHeight = 40;
            this.gridPcbVers.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colNamePcb,
            this.colComment,
            this.colSetting,
            this.colID});
            this.gridPcbVers.GridControl = this.gridControlPcbVers;
            this.gridPcbVers.Name = "gridPcbVers";
            this.gridPcbVers.OptionsView.ColumnAutoWidth = false;
            this.gridPcbVers.OptionsView.ShowGroupPanel = false;
            // 
            // colNamePcb
            // 
            this.colNamePcb.AppearanceCell.Options.UseTextOptions = true;
            this.colNamePcb.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colNamePcb.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colNamePcb.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colNamePcb.AppearanceHeader.Options.UseTextOptions = true;
            this.colNamePcb.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNamePcb.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colNamePcb.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colNamePcb.Caption = "�������� ����� ��������";
            this.colNamePcb.FieldName = "nmPcbs";
            this.colNamePcb.Name = "colNamePcb";
            this.colNamePcb.OptionsColumn.AllowEdit = false;
            this.colNamePcb.OptionsColumn.AllowFocus = false;
            this.colNamePcb.OptionsColumn.ReadOnly = true;
            this.colNamePcb.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count)});
            this.colNamePcb.ToolTip = "�������� ���� ��������";
            this.colNamePcb.Visible = true;
            this.colNamePcb.VisibleIndex = 0;
            this.colNamePcb.Width = 362;
            // 
            // colComment
            // 
            this.colComment.AppearanceCell.Options.UseTextOptions = true;
            this.colComment.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colComment.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colComment.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colComment.AppearanceHeader.Options.UseTextOptions = true;
            this.colComment.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colComment.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colComment.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colComment.Caption = "�����������";
            this.colComment.FieldName = "Comms";
            this.colComment.Name = "colComment";
            this.colComment.OptionsColumn.AllowEdit = false;
            this.colComment.OptionsColumn.AllowFocus = false;
            this.colComment.OptionsColumn.ReadOnly = true;
            this.colComment.ToolTip = "����������� � ������ ��������";
            this.colComment.Visible = true;
            this.colComment.VisibleIndex = 1;
            this.colComment.Width = 333;
            // 
            // colSetting
            // 
            this.colSetting.AppearanceCell.Options.UseTextOptions = true;
            this.colSetting.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSetting.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colSetting.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.colSetting.AppearanceHeader.Options.UseTextOptions = true;
            this.colSetting.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSetting.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colSetting.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colSetting.Caption = "���������� ����� �������� �� ������";
            this.colSetting.FieldName = "settVehic";
            this.colSetting.Name = "colSetting";
            this.colSetting.OptionsColumn.AllowFocus = false;
            this.colSetting.ToolTip = "���������� ����� �������� �� ������";
            this.colSetting.Visible = true;
            this.colSetting.VisibleIndex = 2;
            this.colSetting.Width = 179;
            // 
            // colID
            // 
            this.colID.AppearanceCell.Options.UseTextOptions = true;
            this.colID.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colID.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colID.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.colID.AppearanceHeader.Options.UseTextOptions = true;
            this.colID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colID.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colID.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colID.Caption = "ID";
            this.colID.FieldName = "IDPcb";
            this.colID.Name = "colID";
            this.colID.OptionsColumn.AllowEdit = false;
            this.colID.OptionsColumn.AllowFocus = false;
            this.colID.OptionsColumn.ReadOnly = true;
            this.colID.ToolTip = "������������� ������ ����� ��������";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.btnSettingPcb);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControl1.Location = new System.Drawing.Point(0, 257);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(957, 40);
            this.panelControl1.TabIndex = 3;
            // 
            // btnSettingPcb
            // 
            this.btnSettingPcb.Location = new System.Drawing.Point(388, 9);
            this.btnSettingPcb.Name = "btnSettingPcb";
            this.btnSettingPcb.Size = new System.Drawing.Size(163, 26);
            this.btnSettingPcb.TabIndex = 0;
            this.btnSettingPcb.Text = "��������� ����� �������";
            this.btnSettingPcb.ToolTip = "��������� ��������� ����� �������� �� ��������� ������";
            this.btnSettingPcb.Click += new System.EventHandler(this.btnSettingPcb_Click);
            // 
            // splitContainerControl4
            // 
            this.splitContainerControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl4.Horizontal = false;
            this.splitContainerControl4.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl4.Name = "splitContainerControl4";
            this.splitContainerControl4.Panel1.CaptionLocation = DevExpress.Utils.Locations.Left;
            this.splitContainerControl4.Panel1.Controls.Add(this.gridControlSetup);
            this.splitContainerControl4.Panel1.ShowCaption = true;
            this.splitContainerControl4.Panel1.Text = "Panel1";
            this.splitContainerControl4.Panel2.Controls.Add(this.groupControl1);
            this.splitContainerControl4.Panel2.ShowCaption = true;
            this.splitContainerControl4.Panel2.Text = "Panel2";
            this.splitContainerControl4.Size = new System.Drawing.Size(957, 162);
            this.splitContainerControl4.SplitterPosition = 195;
            this.splitContainerControl4.TabIndex = 0;
            this.splitContainerControl4.Text = "splitContainerControl4";
            // 
            // gridControlSetup
            // 
            this.gridControlSetup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlSetup.Location = new System.Drawing.Point(0, 0);
            this.gridControlSetup.MainView = this.gridSetups;
            this.gridControlSetup.Name = "gridControlSetup";
            this.gridControlSetup.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.algorithmComboBox,
            this.situationItemButtonEdit,
            this.stateItemButtonEdit,
            this.taririvkaItemButtonEdit,
            this.itemCheckEdit1,
            this.repItemCheckEdit1});
            this.gridControlSetup.Size = new System.Drawing.Size(957, 157);
            this.gridControlSetup.TabIndex = 6;
            this.gridControlSetup.UseEmbeddedNavigator = true;
            this.gridControlSetup.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridSetups});
            // 
            // gridSetups
            // 
            this.gridSetups.Appearance.HeaderPanel.BackColor = System.Drawing.Color.Transparent;
            this.gridSetups.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gridSetups.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.gridSetups.Appearance.HeaderPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridSetups.Appearance.HeaderPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridSetups.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridSetups.ColumnPanelRowHeight = 40;
            this.gridSetups.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTypeData,
            this.colStartBit,
            this.colLengthRecord,
            this.columnNameUnit,
            this.colKoeffK,
            this.colShifter,
            this.colSigning,
            this.colComms,
            this.colTarirovka,
            this.colState,
            this.colSituation,
            this.colAlgorithm,
            this.colIdData,
            this.colIdPcbs});
            this.gridSetups.GridControl = this.gridControlSetup;
            this.gridSetups.Name = "gridSetups";
            this.gridSetups.OptionsView.ColumnAutoWidth = false;
            this.gridSetups.OptionsView.ShowGroupPanel = false;
            // 
            // colTypeData
            // 
            this.colTypeData.AppearanceCell.Options.UseTextOptions = true;
            this.colTypeData.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTypeData.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTypeData.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTypeData.AppearanceHeader.BackColor = System.Drawing.Color.PapayaWhip;
            this.colTypeData.AppearanceHeader.BackColor2 = System.Drawing.Color.Silver;
            this.colTypeData.AppearanceHeader.BorderColor = System.Drawing.Color.Black;
            this.colTypeData.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.colTypeData.AppearanceHeader.Options.UseBackColor = true;
            this.colTypeData.AppearanceHeader.Options.UseBorderColor = true;
            this.colTypeData.AppearanceHeader.Options.UseForeColor = true;
            this.colTypeData.AppearanceHeader.Options.UseTextOptions = true;
            this.colTypeData.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTypeData.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTypeData.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTypeData.Caption = "�������� ����";
            this.colTypeData.FieldName = "TypeDataPcb";
            this.colTypeData.Name = "colTypeData";
            this.colTypeData.ToolTip = "�������� ���� ���� ������";
            this.colTypeData.Visible = true;
            this.colTypeData.VisibleIndex = 0;
            this.colTypeData.Width = 99;
            // 
            // colStartBit
            // 
            this.colStartBit.AppearanceCell.Options.UseTextOptions = true;
            this.colStartBit.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colStartBit.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colStartBit.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.colStartBit.AppearanceHeader.BackColor = System.Drawing.Color.PapayaWhip;
            this.colStartBit.AppearanceHeader.BackColor2 = System.Drawing.Color.Silver;
            this.colStartBit.AppearanceHeader.BorderColor = System.Drawing.Color.Black;
            this.colStartBit.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.colStartBit.AppearanceHeader.Options.UseBackColor = true;
            this.colStartBit.AppearanceHeader.Options.UseBorderColor = true;
            this.colStartBit.AppearanceHeader.Options.UseForeColor = true;
            this.colStartBit.AppearanceHeader.Options.UseTextOptions = true;
            this.colStartBit.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colStartBit.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colStartBit.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colStartBit.Caption = "��������� ��� ����";
            this.colStartBit.FieldName = "StrtBit";
            this.colStartBit.Name = "colStartBit";
            this.colStartBit.ToolTip = "��������� ��� ���� ������";
            this.colStartBit.Visible = true;
            this.colStartBit.VisibleIndex = 1;
            this.colStartBit.Width = 104;
            // 
            // colLengthRecord
            // 
            this.colLengthRecord.AppearanceCell.Options.UseTextOptions = true;
            this.colLengthRecord.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLengthRecord.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colLengthRecord.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.colLengthRecord.AppearanceHeader.BackColor = System.Drawing.Color.PapayaWhip;
            this.colLengthRecord.AppearanceHeader.BackColor2 = System.Drawing.Color.Silver;
            this.colLengthRecord.AppearanceHeader.BorderColor = System.Drawing.Color.Black;
            this.colLengthRecord.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.colLengthRecord.AppearanceHeader.Options.UseBackColor = true;
            this.colLengthRecord.AppearanceHeader.Options.UseBorderColor = true;
            this.colLengthRecord.AppearanceHeader.Options.UseForeColor = true;
            this.colLengthRecord.AppearanceHeader.Options.UseTextOptions = true;
            this.colLengthRecord.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLengthRecord.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colLengthRecord.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colLengthRecord.Caption = "����� ����";
            this.colLengthRecord.FieldName = "LngthBit";
            this.colLengthRecord.Name = "colLengthRecord";
            this.colLengthRecord.ToolTip = "����� ���� � �����";
            this.colLengthRecord.Visible = true;
            this.colLengthRecord.VisibleIndex = 2;
            this.colLengthRecord.Width = 107;
            // 
            // columnNameUnit
            // 
            this.columnNameUnit.AppearanceCell.Options.UseTextOptions = true;
            this.columnNameUnit.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnNameUnit.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnNameUnit.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.columnNameUnit.AppearanceHeader.BackColor = System.Drawing.Color.PapayaWhip;
            this.columnNameUnit.AppearanceHeader.BackColor2 = System.Drawing.Color.Silver;
            this.columnNameUnit.AppearanceHeader.BorderColor = System.Drawing.Color.Black;
            this.columnNameUnit.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.columnNameUnit.AppearanceHeader.Options.UseBackColor = true;
            this.columnNameUnit.AppearanceHeader.Options.UseBorderColor = true;
            this.columnNameUnit.AppearanceHeader.Options.UseForeColor = true;
            this.columnNameUnit.AppearanceHeader.Options.UseTextOptions = true;
            this.columnNameUnit.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.columnNameUnit.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.columnNameUnit.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.columnNameUnit.Caption = "�������� ��������";
            this.columnNameUnit.FieldName = "NameUnit";
            this.columnNameUnit.Name = "columnNameUnit";
            this.columnNameUnit.ToolTip = "�������� ��������";
            this.columnNameUnit.Visible = true;
            this.columnNameUnit.VisibleIndex = 3;
            // 
            // colKoeffK
            // 
            this.colKoeffK.AppearanceCell.Options.UseTextOptions = true;
            this.colKoeffK.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colKoeffK.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colKoeffK.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.colKoeffK.AppearanceHeader.BackColor = System.Drawing.Color.PapayaWhip;
            this.colKoeffK.AppearanceHeader.BackColor2 = System.Drawing.Color.Silver;
            this.colKoeffK.AppearanceHeader.BorderColor = System.Drawing.Color.Black;
            this.colKoeffK.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.colKoeffK.AppearanceHeader.Options.UseBackColor = true;
            this.colKoeffK.AppearanceHeader.Options.UseBorderColor = true;
            this.colKoeffK.AppearanceHeader.Options.UseForeColor = true;
            this.colKoeffK.AppearanceHeader.Options.UseTextOptions = true;
            this.colKoeffK.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colKoeffK.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colKoeffK.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colKoeffK.Caption = "����������� �";
            this.colKoeffK.FieldName = "KoefficK";
            this.colKoeffK.Name = "colKoeffK";
            this.colKoeffK.ToolTip = "����������� �";
            this.colKoeffK.Visible = true;
            this.colKoeffK.VisibleIndex = 4;
            this.colKoeffK.Width = 99;
            // 
            // colShifter
            // 
            this.colShifter.AppearanceCell.Options.UseTextOptions = true;
            this.colShifter.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colShifter.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colShifter.AppearanceHeader.Options.UseTextOptions = true;
            this.colShifter.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colShifter.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colShifter.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colShifter.Caption = "�����, b";
            this.colShifter.FieldName = "Shifter";
            this.colShifter.Name = "colShifter";
            this.colShifter.ToolTip = "�������� ������";
            this.colShifter.Visible = true;
            this.colShifter.VisibleIndex = 5;
            // 
            // colSigning
            // 
            this.colSigning.AppearanceCell.Options.UseTextOptions = true;
            this.colSigning.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSigning.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colSigning.AppearanceHeader.Options.UseTextOptions = true;
            this.colSigning.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSigning.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colSigning.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colSigning.Caption = "����";
            this.colSigning.ColumnEdit = this.repItemCheckEdit1;
            this.colSigning.FieldName = "Signing";
            this.colSigning.Name = "colSigning";
            this.colSigning.ToolTip = "��������/�����������";
            this.colSigning.Visible = true;
            this.colSigning.VisibleIndex = 6;
            // 
            // repItemCheckEdit1
            // 
            this.repItemCheckEdit1.AutoHeight = false;
            this.repItemCheckEdit1.Caption = "Check";
            this.repItemCheckEdit1.Name = "repItemCheckEdit1";
            // 
            // colComms
            // 
            this.colComms.AppearanceCell.Options.UseTextOptions = true;
            this.colComms.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colComms.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colComms.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colComms.AppearanceHeader.Options.UseTextOptions = true;
            this.colComms.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colComms.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colComms.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.colComms.Caption = "����������";
            this.colComms.FieldName = "Commentary";
            this.colComms.Name = "colComms";
            this.colComms.ToolTip = "���������� �� ������ ������";
            // 
            // colTarirovka
            // 
            this.colTarirovka.AppearanceCell.Options.UseTextOptions = true;
            this.colTarirovka.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTarirovka.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTarirovka.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.colTarirovka.AppearanceHeader.BackColor = System.Drawing.Color.PapayaWhip;
            this.colTarirovka.AppearanceHeader.BackColor2 = System.Drawing.Color.Silver;
            this.colTarirovka.AppearanceHeader.BorderColor = System.Drawing.Color.Black;
            this.colTarirovka.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.colTarirovka.AppearanceHeader.Options.UseBackColor = true;
            this.colTarirovka.AppearanceHeader.Options.UseBorderColor = true;
            this.colTarirovka.AppearanceHeader.Options.UseForeColor = true;
            this.colTarirovka.AppearanceHeader.Options.UseTextOptions = true;
            this.colTarirovka.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTarirovka.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTarirovka.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTarirovka.Caption = "������������ �������";
            this.colTarirovka.ColumnEdit = this.taririvkaItemButtonEdit;
            this.colTarirovka.FieldName = "Tarirovich";
            this.colTarirovka.Name = "colTarirovka";
            this.colTarirovka.OptionsColumn.ReadOnly = true;
            this.colTarirovka.ToolTip = "����� ������������ �������";
            this.colTarirovka.Visible = true;
            this.colTarirovka.VisibleIndex = 7;
            this.colTarirovka.Width = 117;
            // 
            // taririvkaItemButtonEdit
            // 
            this.taririvkaItemButtonEdit.Appearance.Options.UseTextOptions = true;
            this.taririvkaItemButtonEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.taririvkaItemButtonEdit.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.taririvkaItemButtonEdit.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.taririvkaItemButtonEdit.AppearanceDisabled.Options.UseTextOptions = true;
            this.taririvkaItemButtonEdit.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.taririvkaItemButtonEdit.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.taririvkaItemButtonEdit.AppearanceDisabled.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.taririvkaItemButtonEdit.AppearanceFocused.Options.UseTextOptions = true;
            this.taririvkaItemButtonEdit.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.taririvkaItemButtonEdit.AppearanceFocused.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.taririvkaItemButtonEdit.AppearanceFocused.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.taririvkaItemButtonEdit.AppearanceReadOnly.Options.UseTextOptions = true;
            this.taririvkaItemButtonEdit.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.taririvkaItemButtonEdit.AppearanceReadOnly.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.taririvkaItemButtonEdit.AppearanceReadOnly.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.taririvkaItemButtonEdit.AutoHeight = false;
            this.taririvkaItemButtonEdit.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            serializableAppearanceObject1.Options.UseTextOptions = true;
            serializableAppearanceObject1.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            serializableAppearanceObject1.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            serializableAppearanceObject1.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.taririvkaItemButtonEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Ellipsis, "���������", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "����� ������������ �������", null, null, true)});
            this.taririvkaItemButtonEdit.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.taririvkaItemButtonEdit.Name = "taririvkaItemButtonEdit";
            this.taririvkaItemButtonEdit.ReadOnly = true;
            // 
            // colState
            // 
            this.colState.AppearanceCell.Options.UseTextOptions = true;
            this.colState.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colState.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colState.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colState.AppearanceHeader.BackColor = System.Drawing.Color.PapayaWhip;
            this.colState.AppearanceHeader.BackColor2 = System.Drawing.Color.Silver;
            this.colState.AppearanceHeader.BorderColor = System.Drawing.Color.Black;
            this.colState.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.colState.AppearanceHeader.Options.UseBackColor = true;
            this.colState.AppearanceHeader.Options.UseBorderColor = true;
            this.colState.AppearanceHeader.Options.UseForeColor = true;
            this.colState.AppearanceHeader.Options.UseTextOptions = true;
            this.colState.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colState.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colState.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colState.Caption = "���������";
            this.colState.ColumnEdit = this.stateItemButtonEdit;
            this.colState.FieldName = "State";
            this.colState.Name = "colState";
            this.colState.OptionsColumn.ReadOnly = true;
            this.colState.ToolTip = "���������";
            this.colState.Visible = true;
            this.colState.VisibleIndex = 8;
            this.colState.Width = 108;
            // 
            // stateItemButtonEdit
            // 
            this.stateItemButtonEdit.Appearance.Options.UseTextOptions = true;
            this.stateItemButtonEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.stateItemButtonEdit.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.stateItemButtonEdit.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.stateItemButtonEdit.AppearanceDisabled.Options.UseTextOptions = true;
            this.stateItemButtonEdit.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.stateItemButtonEdit.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.stateItemButtonEdit.AppearanceDisabled.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.stateItemButtonEdit.AppearanceFocused.Options.UseTextOptions = true;
            this.stateItemButtonEdit.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.stateItemButtonEdit.AppearanceFocused.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.stateItemButtonEdit.AppearanceFocused.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.stateItemButtonEdit.AppearanceReadOnly.Options.UseTextOptions = true;
            this.stateItemButtonEdit.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.stateItemButtonEdit.AppearanceReadOnly.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.stateItemButtonEdit.AppearanceReadOnly.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.stateItemButtonEdit.AutoHeight = false;
            serializableAppearanceObject2.Options.UseTextOptions = true;
            serializableAppearanceObject2.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            serializableAppearanceObject2.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            serializableAppearanceObject2.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.stateItemButtonEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Down, "���������", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, "����������� ���������", null, null, true)});
            this.stateItemButtonEdit.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.stateItemButtonEdit.Name = "stateItemButtonEdit";
            this.stateItemButtonEdit.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // colSituation
            // 
            this.colSituation.AppearanceCell.Options.UseTextOptions = true;
            this.colSituation.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSituation.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colSituation.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.colSituation.AppearanceHeader.BackColor = System.Drawing.Color.PapayaWhip;
            this.colSituation.AppearanceHeader.BackColor2 = System.Drawing.Color.Silver;
            this.colSituation.AppearanceHeader.BorderColor = System.Drawing.Color.Black;
            this.colSituation.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.colSituation.AppearanceHeader.Options.UseBackColor = true;
            this.colSituation.AppearanceHeader.Options.UseBorderColor = true;
            this.colSituation.AppearanceHeader.Options.UseForeColor = true;
            this.colSituation.AppearanceHeader.Options.UseTextOptions = true;
            this.colSituation.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSituation.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colSituation.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colSituation.Caption = "�������";
            this.colSituation.ColumnEdit = this.situationItemButtonEdit;
            this.colSituation.FieldName = "Situation";
            this.colSituation.Name = "colSituation";
            this.colSituation.OptionsColumn.ReadOnly = true;
            this.colSituation.ToolTip = "�������";
            this.colSituation.Visible = true;
            this.colSituation.VisibleIndex = 9;
            this.colSituation.Width = 119;
            // 
            // situationItemButtonEdit
            // 
            this.situationItemButtonEdit.Appearance.Options.UseTextOptions = true;
            this.situationItemButtonEdit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.situationItemButtonEdit.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.situationItemButtonEdit.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.situationItemButtonEdit.AppearanceDisabled.Options.UseTextOptions = true;
            this.situationItemButtonEdit.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.situationItemButtonEdit.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.situationItemButtonEdit.AppearanceDisabled.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.situationItemButtonEdit.AppearanceFocused.Options.UseTextOptions = true;
            this.situationItemButtonEdit.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.situationItemButtonEdit.AppearanceFocused.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.situationItemButtonEdit.AppearanceFocused.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.situationItemButtonEdit.AppearanceReadOnly.Options.UseTextOptions = true;
            this.situationItemButtonEdit.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.situationItemButtonEdit.AppearanceReadOnly.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.situationItemButtonEdit.AppearanceReadOnly.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.situationItemButtonEdit.AutoHeight = false;
            this.situationItemButtonEdit.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            serializableAppearanceObject3.Options.UseTextOptions = true;
            serializableAppearanceObject3.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            serializableAppearanceObject3.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            serializableAppearanceObject3.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.situationItemButtonEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Up, "�������", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject3, "����������� �������", null, null, true)});
            this.situationItemButtonEdit.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.situationItemButtonEdit.Name = "situationItemButtonEdit";
            this.situationItemButtonEdit.ReadOnly = true;
            this.situationItemButtonEdit.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // colAlgorithm
            // 
            this.colAlgorithm.AppearanceCell.Options.UseTextOptions = true;
            this.colAlgorithm.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colAlgorithm.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colAlgorithm.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colAlgorithm.AppearanceHeader.BackColor = System.Drawing.Color.PapayaWhip;
            this.colAlgorithm.AppearanceHeader.BackColor2 = System.Drawing.Color.Silver;
            this.colAlgorithm.AppearanceHeader.BorderColor = System.Drawing.Color.Black;
            this.colAlgorithm.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.colAlgorithm.AppearanceHeader.Options.UseBackColor = true;
            this.colAlgorithm.AppearanceHeader.Options.UseBorderColor = true;
            this.colAlgorithm.AppearanceHeader.Options.UseForeColor = true;
            this.colAlgorithm.AppearanceHeader.Options.UseTextOptions = true;
            this.colAlgorithm.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colAlgorithm.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colAlgorithm.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colAlgorithm.Caption = "�������� ���������";
            this.colAlgorithm.ColumnEdit = this.algorithmComboBox;
            this.colAlgorithm.FieldName = "Algorithm";
            this.colAlgorithm.Name = "colAlgorithm";
            this.colAlgorithm.ToolTip = "�������� ���������";
            this.colAlgorithm.Visible = true;
            this.colAlgorithm.VisibleIndex = 10;
            this.colAlgorithm.Width = 136;
            // 
            // algorithmComboBox
            // 
            this.algorithmComboBox.Appearance.Options.UseTextOptions = true;
            this.algorithmComboBox.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.algorithmComboBox.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.algorithmComboBox.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.algorithmComboBox.AppearanceDisabled.Options.UseTextOptions = true;
            this.algorithmComboBox.AppearanceDisabled.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.algorithmComboBox.AppearanceDisabled.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.algorithmComboBox.AppearanceDisabled.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.algorithmComboBox.AppearanceDropDown.Options.UseTextOptions = true;
            this.algorithmComboBox.AppearanceDropDown.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.algorithmComboBox.AppearanceDropDown.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.algorithmComboBox.AppearanceDropDown.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.algorithmComboBox.AppearanceFocused.Options.UseTextOptions = true;
            this.algorithmComboBox.AppearanceFocused.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.algorithmComboBox.AppearanceFocused.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.algorithmComboBox.AppearanceFocused.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.algorithmComboBox.AppearanceReadOnly.Options.UseTextOptions = true;
            this.algorithmComboBox.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.algorithmComboBox.AppearanceReadOnly.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.algorithmComboBox.AppearanceReadOnly.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.algorithmComboBox.AutoHeight = false;
            serializableAppearanceObject4.Options.UseTextOptions = true;
            serializableAppearanceObject4.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            serializableAppearanceObject4.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            serializableAppearanceObject4.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.algorithmComboBox.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "��������", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject4, "����� ���������", null, null, true)});
            this.algorithmComboBox.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.algorithmComboBox.Items.AddRange(new object[] {
            "-- �������� �� ������ --"});
            this.algorithmComboBox.Name = "algorithmComboBox";
            this.algorithmComboBox.Sorted = true;
            // 
            // colIdData
            // 
            this.colIdData.AppearanceCell.Options.UseTextOptions = true;
            this.colIdData.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIdData.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colIdData.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.colIdData.AppearanceHeader.Options.UseTextOptions = true;
            this.colIdData.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIdData.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colIdData.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colIdData.Caption = "�������������";
            this.colIdData.FieldName = "Id";
            this.colIdData.Name = "colIdData";
            this.colIdData.OptionsColumn.AllowEdit = false;
            this.colIdData.OptionsColumn.AllowFocus = false;
            this.colIdData.OptionsColumn.ReadOnly = true;
            this.colIdData.ToolTip = "�������������";
            // 
            // colIdPcbs
            // 
            this.colIdPcbs.AppearanceCell.Options.UseTextOptions = true;
            this.colIdPcbs.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIdPcbs.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colIdPcbs.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.colIdPcbs.AppearanceHeader.Options.UseTextOptions = true;
            this.colIdPcbs.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIdPcbs.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colIdPcbs.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colIdPcbs.Caption = "������������� ������";
            this.colIdPcbs.FieldName = "ID_Pcb";
            this.colIdPcbs.Name = "colIdPcbs";
            this.colIdPcbs.OptionsColumn.AllowEdit = false;
            this.colIdPcbs.OptionsColumn.AllowFocus = false;
            this.colIdPcbs.OptionsColumn.ReadOnly = true;
            this.colIdPcbs.ToolTip = "������������� ������ � ������� Pcb Version";
            // 
            // itemCheckEdit1
            // 
            this.itemCheckEdit1.AutoHeight = false;
            this.itemCheckEdit1.Caption = "Check";
            this.itemCheckEdit1.Name = "itemCheckEdit1";
            this.itemCheckEdit1.ValueChecked = ((byte)(1));
            this.itemCheckEdit1.ValueUnchecked = ((byte)(0));
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.gridControlLogic);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(0, 0);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Text = "���������� �������";
            // 
            // gridControlLogic
            // 
            this.gridControlLogic.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlLogic.Location = new System.Drawing.Point(0, 20);
            this.gridControlLogic.MainView = this.sensorsGrid;
            this.gridControlLogic.Name = "gridControlLogic";
            this.gridControlLogic.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.rpItemButtonEdit,
            this.rpstrItemButtonEdit,
            this.rpsItemButtonEdit});
            this.gridControlLogic.Size = new System.Drawing.Size(0, 0);
            this.gridControlLogic.TabIndex = 1;
            this.gridControlLogic.UseEmbeddedNavigator = true;
            this.gridControlLogic.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.sensorsGrid});
            // 
            // sensorsGrid
            // 
            this.sensorsGrid.ColumnPanelRowHeight = 40;
            this.sensorsGrid.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.nameDataGridViewTextBoxColumn1,
            this.startBitDataGridViewTextBoxColumn,
            this.SpecSettingColumn,
            this.id,
            this.colState0,
            this.colState1,
            this.colCondition,
            this.colEvents});
            this.sensorsGrid.GridControl = this.gridControlLogic;
            this.sensorsGrid.Name = "sensorsGrid";
            this.sensorsGrid.OptionsView.ShowGroupPanel = false;
            // 
            // nameDataGridViewTextBoxColumn1
            // 
            this.nameDataGridViewTextBoxColumn1.AppearanceCell.Options.UseTextOptions = true;
            this.nameDataGridViewTextBoxColumn1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.nameDataGridViewTextBoxColumn1.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.nameDataGridViewTextBoxColumn1.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.nameDataGridViewTextBoxColumn1.AppearanceHeader.BackColor = System.Drawing.Color.PapayaWhip;
            this.nameDataGridViewTextBoxColumn1.AppearanceHeader.BackColor2 = System.Drawing.Color.Silver;
            this.nameDataGridViewTextBoxColumn1.AppearanceHeader.BorderColor = System.Drawing.Color.Black;
            this.nameDataGridViewTextBoxColumn1.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.nameDataGridViewTextBoxColumn1.AppearanceHeader.Options.UseBackColor = true;
            this.nameDataGridViewTextBoxColumn1.AppearanceHeader.Options.UseBorderColor = true;
            this.nameDataGridViewTextBoxColumn1.AppearanceHeader.Options.UseForeColor = true;
            this.nameDataGridViewTextBoxColumn1.AppearanceHeader.Options.UseTextOptions = true;
            this.nameDataGridViewTextBoxColumn1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.nameDataGridViewTextBoxColumn1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.nameDataGridViewTextBoxColumn1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.nameDataGridViewTextBoxColumn1.Caption = "��������";
            this.nameDataGridViewTextBoxColumn1.FieldName = "Name";
            this.nameDataGridViewTextBoxColumn1.Name = "nameDataGridViewTextBoxColumn1";
            this.nameDataGridViewTextBoxColumn1.OptionsColumn.AllowEdit = false;
            this.nameDataGridViewTextBoxColumn1.OptionsColumn.AllowFocus = false;
            this.nameDataGridViewTextBoxColumn1.OptionsColumn.ReadOnly = true;
            this.nameDataGridViewTextBoxColumn1.ToolTip = "�������� ����������� �������";
            this.nameDataGridViewTextBoxColumn1.Visible = true;
            this.nameDataGridViewTextBoxColumn1.VisibleIndex = 0;
            // 
            // startBitDataGridViewTextBoxColumn
            // 
            this.startBitDataGridViewTextBoxColumn.AppearanceCell.Options.UseTextOptions = true;
            this.startBitDataGridViewTextBoxColumn.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.startBitDataGridViewTextBoxColumn.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.startBitDataGridViewTextBoxColumn.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.startBitDataGridViewTextBoxColumn.AppearanceHeader.BackColor = System.Drawing.Color.PapayaWhip;
            this.startBitDataGridViewTextBoxColumn.AppearanceHeader.BackColor2 = System.Drawing.Color.Silver;
            this.startBitDataGridViewTextBoxColumn.AppearanceHeader.BorderColor = System.Drawing.Color.Black;
            this.startBitDataGridViewTextBoxColumn.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.startBitDataGridViewTextBoxColumn.AppearanceHeader.Options.UseBackColor = true;
            this.startBitDataGridViewTextBoxColumn.AppearanceHeader.Options.UseBorderColor = true;
            this.startBitDataGridViewTextBoxColumn.AppearanceHeader.Options.UseForeColor = true;
            this.startBitDataGridViewTextBoxColumn.AppearanceHeader.Options.UseTextOptions = true;
            this.startBitDataGridViewTextBoxColumn.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.startBitDataGridViewTextBoxColumn.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.startBitDataGridViewTextBoxColumn.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.startBitDataGridViewTextBoxColumn.Caption = "����� ����";
            this.startBitDataGridViewTextBoxColumn.FieldName = "StartBit";
            this.startBitDataGridViewTextBoxColumn.Name = "startBitDataGridViewTextBoxColumn";
            this.startBitDataGridViewTextBoxColumn.OptionsColumn.AllowEdit = false;
            this.startBitDataGridViewTextBoxColumn.OptionsColumn.AllowFocus = false;
            this.startBitDataGridViewTextBoxColumn.OptionsColumn.ReadOnly = true;
            this.startBitDataGridViewTextBoxColumn.ToolTip = "����� ���� ����������� �������";
            this.startBitDataGridViewTextBoxColumn.Visible = true;
            this.startBitDataGridViewTextBoxColumn.VisibleIndex = 1;
            // 
            // SpecSettingColumn
            // 
            this.SpecSettingColumn.AppearanceCell.Options.UseTextOptions = true;
            this.SpecSettingColumn.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SpecSettingColumn.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SpecSettingColumn.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.SpecSettingColumn.AppearanceHeader.BackColor = System.Drawing.Color.PapayaWhip;
            this.SpecSettingColumn.AppearanceHeader.BackColor2 = System.Drawing.Color.Silver;
            this.SpecSettingColumn.AppearanceHeader.BorderColor = System.Drawing.Color.Black;
            this.SpecSettingColumn.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.SpecSettingColumn.AppearanceHeader.Options.UseBackColor = true;
            this.SpecSettingColumn.AppearanceHeader.Options.UseBorderColor = true;
            this.SpecSettingColumn.AppearanceHeader.Options.UseForeColor = true;
            this.SpecSettingColumn.AppearanceHeader.Options.UseTextOptions = true;
            this.SpecSettingColumn.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SpecSettingColumn.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.SpecSettingColumn.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.SpecSettingColumn.Caption = "���������� �������";
            this.SpecSettingColumn.FieldName = "Spec";
            this.SpecSettingColumn.Name = "SpecSettingColumn";
            this.SpecSettingColumn.OptionsColumn.AllowEdit = false;
            this.SpecSettingColumn.OptionsColumn.AllowFocus = false;
            this.SpecSettingColumn.OptionsColumn.ReadOnly = true;
            this.SpecSettingColumn.ToolTip = "����������� ���������� ����������� �������";
            this.SpecSettingColumn.Visible = true;
            this.SpecSettingColumn.VisibleIndex = 2;
            // 
            // id
            // 
            this.id.Caption = "Id";
            this.id.FieldName = "id";
            this.id.Name = "id";
            this.id.OptionsColumn.AllowEdit = false;
            this.id.OptionsColumn.AllowFocus = false;
            this.id.OptionsColumn.ReadOnly = true;
            this.id.ToolTip = "�������������";
            // 
            // colState0
            // 
            this.colState0.AppearanceCell.Options.UseTextOptions = true;
            this.colState0.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colState0.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colState0.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.colState0.AppearanceHeader.BackColor = System.Drawing.Color.PapayaWhip;
            this.colState0.AppearanceHeader.BackColor2 = System.Drawing.Color.Silver;
            this.colState0.AppearanceHeader.BorderColor = System.Drawing.Color.Black;
            this.colState0.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.colState0.AppearanceHeader.Options.UseBackColor = true;
            this.colState0.AppearanceHeader.Options.UseBorderColor = true;
            this.colState0.AppearanceHeader.Options.UseForeColor = true;
            this.colState0.AppearanceHeader.Options.UseTextOptions = true;
            this.colState0.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colState0.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colState0.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colState0.Caption = "��������� ��� \"0\"";
            this.colState0.FieldName = "State0";
            this.colState0.Name = "colState0";
            this.colState0.OptionsColumn.AllowEdit = false;
            this.colState0.OptionsColumn.AllowFocus = false;
            this.colState0.OptionsColumn.ReadOnly = true;
            this.colState0.ToolTip = "��������� ��� ������� �������� ����";
            this.colState0.Visible = true;
            this.colState0.VisibleIndex = 3;
            // 
            // colState1
            // 
            this.colState1.AppearanceCell.Options.UseTextOptions = true;
            this.colState1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colState1.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colState1.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.colState1.AppearanceHeader.BackColor = System.Drawing.Color.PapayaWhip;
            this.colState1.AppearanceHeader.BackColor2 = System.Drawing.Color.Silver;
            this.colState1.AppearanceHeader.BorderColor = System.Drawing.Color.Black;
            this.colState1.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.colState1.AppearanceHeader.Options.UseBackColor = true;
            this.colState1.AppearanceHeader.Options.UseBorderColor = true;
            this.colState1.AppearanceHeader.Options.UseForeColor = true;
            this.colState1.AppearanceHeader.Options.UseTextOptions = true;
            this.colState1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colState1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colState1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colState1.Caption = "��������� ��� \"1\"";
            this.colState1.FieldName = "State1";
            this.colState1.Name = "colState1";
            this.colState1.OptionsColumn.AllowEdit = false;
            this.colState1.OptionsColumn.AllowFocus = false;
            this.colState1.OptionsColumn.ReadOnly = true;
            this.colState1.ToolTip = "��������� ��� ��������� �������� ����";
            this.colState1.Visible = true;
            this.colState1.VisibleIndex = 4;
            // 
            // colCondition
            // 
            this.colCondition.AppearanceCell.Options.UseTextOptions = true;
            this.colCondition.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCondition.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colCondition.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.colCondition.AppearanceHeader.BackColor = System.Drawing.Color.PapayaWhip;
            this.colCondition.AppearanceHeader.BackColor2 = System.Drawing.Color.Silver;
            this.colCondition.AppearanceHeader.BorderColor = System.Drawing.Color.Black;
            this.colCondition.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.colCondition.AppearanceHeader.Options.UseBackColor = true;
            this.colCondition.AppearanceHeader.Options.UseBorderColor = true;
            this.colCondition.AppearanceHeader.Options.UseForeColor = true;
            this.colCondition.AppearanceHeader.Options.UseTextOptions = true;
            this.colCondition.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCondition.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colCondition.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCondition.Caption = "���������";
            this.colCondition.ColumnEdit = this.rpstrItemButtonEdit;
            this.colCondition.FieldName = "rpConditions";
            this.colCondition.Name = "colCondition";
            this.colCondition.ToolTip = "����� ��������� ���������";
            this.colCondition.Visible = true;
            this.colCondition.VisibleIndex = 5;
            // 
            // rpstrItemButtonEdit
            // 
            this.rpstrItemButtonEdit.AutoHeight = false;
            this.rpstrItemButtonEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Down, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, "����� ��������� ���������", null, null, true)});
            this.rpstrItemButtonEdit.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.rpstrItemButtonEdit.Name = "rpstrItemButtonEdit";
            this.rpstrItemButtonEdit.ReadOnly = true;
            this.rpstrItemButtonEdit.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // colEvents
            // 
            this.colEvents.AppearanceCell.Options.UseTextOptions = true;
            this.colEvents.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colEvents.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colEvents.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.colEvents.AppearanceHeader.BackColor = System.Drawing.Color.PapayaWhip;
            this.colEvents.AppearanceHeader.BackColor2 = System.Drawing.Color.Silver;
            this.colEvents.AppearanceHeader.BorderColor = System.Drawing.Color.Black;
            this.colEvents.AppearanceHeader.ForeColor = System.Drawing.Color.Black;
            this.colEvents.AppearanceHeader.Options.UseBackColor = true;
            this.colEvents.AppearanceHeader.Options.UseBorderColor = true;
            this.colEvents.AppearanceHeader.Options.UseForeColor = true;
            this.colEvents.AppearanceHeader.Options.UseTextOptions = true;
            this.colEvents.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colEvents.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colEvents.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colEvents.Caption = "�������";
            this.colEvents.ColumnEdit = this.rpsItemButtonEdit;
            this.colEvents.FieldName = "rpEvents";
            this.colEvents.Name = "colEvents";
            this.colEvents.ToolTip = "����� ��������� �������";
            this.colEvents.Visible = true;
            this.colEvents.VisibleIndex = 6;
            // 
            // rpsItemButtonEdit
            // 
            this.rpsItemButtonEdit.AutoHeight = false;
            this.rpsItemButtonEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Up, "�������", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject6, "����� ��������� �������", null, null, true)});
            this.rpsItemButtonEdit.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.rpsItemButtonEdit.Name = "rpsItemButtonEdit";
            this.rpsItemButtonEdit.ReadOnly = true;
            this.rpsItemButtonEdit.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // rpItemButtonEdit
            // 
            this.rpItemButtonEdit.AutoHeight = false;
            this.rpItemButtonEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.SpinUp, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject7, "�������������� ��������� � ������� �������", null, null, true)});
            this.rpItemButtonEdit.Name = "rpItemButtonEdit";
            // 
            // analysisTab
            // 
            this.analysisTab.Controls.Add(this.splitContainerControl5);
            this.analysisTab.Name = "analysisTab";
            this.analysisTab.Size = new System.Drawing.Size(1183, 514);
            this.analysisTab.Text = "������ ������";
            this.analysisTab.Tooltip = "������� ������ ��������";
            // 
            // splitContainerControl5
            // 
            this.splitContainerControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl5.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl5.Name = "splitContainerControl5";
            this.splitContainerControl5.Panel1.Controls.Add(this.groupControl2);
            this.splitContainerControl5.Panel1.Text = "Panel1";
            this.splitContainerControl5.Panel2.Controls.Add(this.splitContainerControl6);
            this.splitContainerControl5.Panel2.Controls.Add(this.standaloneBarDockControl1);
            this.splitContainerControl5.Panel2.Text = "Panel2";
            this.splitContainerControl5.Size = new System.Drawing.Size(1183, 514);
            this.splitContainerControl5.SplitterPosition = 238;
            this.splitContainerControl5.TabIndex = 0;
            this.splitContainerControl5.Text = "splitContainerControl5";
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.panelVehicles);
            this.groupControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl2.Location = new System.Drawing.Point(0, 0);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(238, 514);
            this.groupControl2.TabIndex = 0;
            this.groupControl2.Text = "������������� ������";
            // 
            // panelVehicles
            // 
            this.panelVehicles.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelVehicles.Location = new System.Drawing.Point(2, 21);
            this.panelVehicles.Name = "panelVehicles";
            this.panelVehicles.Size = new System.Drawing.Size(234, 491);
            this.panelVehicles.TabIndex = 0;
            // 
            // splitContainerControl6
            // 
            this.splitContainerControl6.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.splitContainerControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl6.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.Panel2;
            this.splitContainerControl6.Horizontal = false;
            this.splitContainerControl6.Location = new System.Drawing.Point(0, 61);
            this.splitContainerControl6.Name = "splitContainerControl6";
            this.splitContainerControl6.Panel1.Controls.Add(this.panelControl3);
            this.splitContainerControl6.Panel1.Text = "Panel1";
            this.splitContainerControl6.Panel2.Controls.Add(this.panelControl2);
            this.splitContainerControl6.Panel2.Text = "Panel2";
            this.splitContainerControl6.Size = new System.Drawing.Size(940, 453);
            this.splitContainerControl6.SplitterPosition = 33;
            this.splitContainerControl6.TabIndex = 3;
            this.splitContainerControl6.Text = "splitContainerControl6";
            // 
            // panelControl3
            // 
            this.panelControl3.Controls.Add(this.ctrlAnalisis);
            this.panelControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl3.Location = new System.Drawing.Point(0, 0);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(936, 411);
            this.panelControl3.TabIndex = 0;
            // 
            // ctrlAnalisis
            // 
            this.ctrlAnalisis.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ctrlAnalisis.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.ctrlAnalisis.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.ctrlAnalisis.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.ctrlAnalisis.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.ctrlAnalisis.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.ctrlAnalisis.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.ctrlAnalisis.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.ctrlAnalisis.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.ctrlAnalisis.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.ctrlAnalisis.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.ctrlAnalisis.EmbeddedNavigator.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            gridLevelNode1.RelationName = "Level1";
            gridLevelNode2.RelationName = "Level2";
            gridLevelNode3.RelationName = "Level3";
            gridLevelNode4.RelationName = "Level4";
            gridLevelNode5.RelationName = "Level5";
            gridLevelNode6.RelationName = "Level6";
            gridLevelNode7.RelationName = "Level7";
            gridLevelNode8.RelationName = "Level8";
            gridLevelNode9.RelationName = "Level9";
            this.ctrlAnalisis.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1,
            gridLevelNode2,
            gridLevelNode3,
            gridLevelNode4,
            gridLevelNode5,
            gridLevelNode6,
            gridLevelNode7,
            gridLevelNode8,
            gridLevelNode9});
            this.ctrlAnalisis.Location = new System.Drawing.Point(2, 2);
            this.ctrlAnalisis.MainView = this.gridAnalisis;
            this.ctrlAnalisis.MenuManager = this.barManager1;
            this.ctrlAnalisis.Name = "ctrlAnalisis";
            this.ctrlAnalisis.Size = new System.Drawing.Size(932, 407);
            this.ctrlAnalisis.TabIndex = 2;
            this.ctrlAnalisis.UseEmbeddedNavigator = true;
            this.ctrlAnalisis.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridAnalisis});
            // 
            // gridAnalisis
            // 
            this.gridAnalisis.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.gridAnalisis.ColumnPanelRowHeight = 40;
            this.gridAnalisis.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.Car,
            this.DateB,
            this.DateE,
            this.Interval,
            this.QTYpoints});
            this.gridAnalisis.GridControl = this.ctrlAnalisis;
            this.gridAnalisis.Name = "gridAnalisis";
            this.gridAnalisis.OptionsView.ShowGroupPanel = false;
            // 
            // Car
            // 
            this.Car.AppearanceCell.Options.UseTextOptions = true;
            this.Car.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Car.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.Car.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.Car.AppearanceHeader.Options.UseTextOptions = true;
            this.Car.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Car.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.Car.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Car.Caption = "������";
            this.Car.FieldName = "xCar";
            this.Car.Name = "Car";
            this.Car.OptionsColumn.AllowEdit = false;
            this.Car.OptionsColumn.AllowFocus = false;
            this.Car.OptionsColumn.ReadOnly = true;
            this.Car.ToolTip = "������";
            this.Car.Visible = true;
            this.Car.VisibleIndex = 0;
            this.Car.Width = 120;
            // 
            // DateB
            // 
            this.DateB.AppearanceCell.Options.UseTextOptions = true;
            this.DateB.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DateB.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.DateB.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.DateB.AppearanceHeader.Options.UseTextOptions = true;
            this.DateB.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DateB.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.DateB.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.DateB.Caption = "���� ������";
            this.DateB.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DateB.FieldName = "xDateB";
            this.DateB.Name = "DateB";
            this.DateB.OptionsColumn.AllowEdit = false;
            this.DateB.OptionsColumn.AllowFocus = false;
            this.DateB.OptionsColumn.ReadOnly = true;
            this.DateB.ToolTip = "���� ������";
            this.DateB.Visible = true;
            this.DateB.VisibleIndex = 1;
            this.DateB.Width = 120;
            // 
            // DateE
            // 
            this.DateE.AppearanceCell.Options.UseTextOptions = true;
            this.DateE.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DateE.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.DateE.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.DateE.AppearanceHeader.Options.UseTextOptions = true;
            this.DateE.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DateE.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.DateE.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.DateE.Caption = "���� ���������";
            this.DateE.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DateE.FieldName = "xDateE";
            this.DateE.Name = "DateE";
            this.DateE.OptionsColumn.AllowEdit = false;
            this.DateE.OptionsColumn.AllowFocus = false;
            this.DateE.OptionsColumn.ReadOnly = true;
            this.DateE.ToolTip = "���� ���������";
            this.DateE.Visible = true;
            this.DateE.VisibleIndex = 2;
            this.DateE.Width = 120;
            // 
            // Interval
            // 
            this.Interval.AppearanceCell.Options.UseTextOptions = true;
            this.Interval.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Interval.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.Interval.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.Interval.AppearanceHeader.Options.UseTextOptions = true;
            this.Interval.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Interval.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.Interval.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.Interval.Caption = "��������";
            this.Interval.FieldName = "xInterval";
            this.Interval.Name = "Interval";
            this.Interval.OptionsColumn.AllowEdit = false;
            this.Interval.OptionsColumn.AllowFocus = false;
            this.Interval.OptionsColumn.ReadOnly = true;
            this.Interval.ToolTip = "��������";
            this.Interval.Visible = true;
            this.Interval.VisibleIndex = 3;
            this.Interval.Width = 120;
            // 
            // QTYpoints
            // 
            this.QTYpoints.AppearanceCell.Options.UseTextOptions = true;
            this.QTYpoints.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.QTYpoints.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.QTYpoints.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.QTYpoints.AppearanceHeader.Options.UseTextOptions = true;
            this.QTYpoints.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.QTYpoints.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.QTYpoints.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.QTYpoints.Caption = "���������� �����";
            this.QTYpoints.FieldName = "xQTYpoints";
            this.QTYpoints.Name = "QTYpoints";
            this.QTYpoints.OptionsColumn.AllowEdit = false;
            this.QTYpoints.OptionsColumn.AllowFocus = false;
            this.QTYpoints.OptionsColumn.ReadOnly = true;
            this.QTYpoints.ToolTip = "���������� �����";
            this.QTYpoints.Visible = true;
            this.QTYpoints.VisibleIndex = 4;
            this.QTYpoints.Width = 120;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1,
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.DockControls.Add(this.standaloneBarDockControl1);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btnDataAnalysis,
            this.btnNoGps,
            this.btnDeleteNoCorrect,
            this.btnMissingData,
            this.btnAdding,
            this.btnDuplicatedData,
            this.barBeginDate,
            this.barEndDate,
            this.lbInfor,
            this.pbAnaliz,
            this.barBeginPeriod,
            this.barEndPeriod,
            this.barStaticItem1,
            this.txMine,
            this.barBtnCancel});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 24;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.textEditPeriod,
            this.dateBeginPeriod,
            this.textEndPeriod,
            this.dateEndPeriod,
            this.dateLengthInterval,
            this.repositoryItemProgressBar1,
            this.textInfo,
            this.repositoryItemProgressBar2,
            this.repositoryItemProgressBar3,
            this.repositoryItemTextEdit1});
            // 
            // bar1
            // 
            this.bar1.BarName = "������";
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar1.FloatLocation = new System.Drawing.Point(498, 277);
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barBeginPeriod),
            new DevExpress.XtraBars.LinkPersistInfo(this.barBeginDate),
            new DevExpress.XtraBars.LinkPersistInfo(this.barEndPeriod),
            new DevExpress.XtraBars.LinkPersistInfo(this.barEndDate),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.txMine)});
            this.bar1.OptionsBar.AllowQuickCustomization = false;
            this.bar1.StandaloneBarDockControl = this.standaloneBarDockControl1;
            this.bar1.Text = "������";
            // 
            // barBeginPeriod
            // 
            this.barBeginPeriod.Border = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.barBeginPeriod.Caption = "������ �������";
            this.barBeginPeriod.Hint = "������ ������� �������";
            this.barBeginPeriod.Id = 18;
            this.barBeginPeriod.Name = "barBeginPeriod";
            this.barBeginPeriod.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barBeginDate
            // 
            this.barBeginDate.Border = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.barBeginDate.Caption = "���� ������";
            this.barBeginDate.Edit = this.dateBeginPeriod;
            this.barBeginDate.Hint = "���� ������ �������";
            this.barBeginDate.Id = 7;
            this.barBeginDate.Name = "barBeginDate";
            this.barBeginDate.Width = 100;
            // 
            // dateBeginPeriod
            // 
            this.dateBeginPeriod.AutoHeight = false;
            this.dateBeginPeriod.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateBeginPeriod.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateBeginPeriod.CalendarTimeProperties.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F4);
            this.dateBeginPeriod.CalendarTimeProperties.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Default;
            this.dateBeginPeriod.Name = "dateBeginPeriod";
            // 
            // barEndPeriod
            // 
            this.barEndPeriod.Border = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.barEndPeriod.Caption = "����� �������";
            this.barEndPeriod.Hint = "����� ������� �������";
            this.barEndPeriod.Id = 19;
            this.barEndPeriod.Name = "barEndPeriod";
            this.barEndPeriod.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barEndDate
            // 
            this.barEndDate.Border = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.barEndDate.Caption = "���� �����";
            this.barEndDate.Edit = this.dateEndPeriod;
            this.barEndDate.Hint = "���� ��������� �������";
            this.barEndDate.Id = 9;
            this.barEndDate.Name = "barEndDate";
            this.barEndDate.Width = 100;
            // 
            // dateEndPeriod
            // 
            this.dateEndPeriod.AutoHeight = false;
            this.dateEndPeriod.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEndPeriod.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEndPeriod.CalendarTimeProperties.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F4);
            this.dateEndPeriod.CalendarTimeProperties.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Default;
            this.dateEndPeriod.Name = "dateEndPeriod";
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Border = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.barStaticItem1.Caption = "����� ������� � ���:";
            this.barStaticItem1.Hint = "����� ������� ������� � �������";
            this.barStaticItem1.Id = 20;
            this.barStaticItem1.Name = "barStaticItem1";
            this.barStaticItem1.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // txMine
            // 
            this.txMine.Border = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.txMine.Caption = "����� �������";
            this.txMine.Edit = this.repositoryItemTextEdit1;
            this.txMine.Hint = "����� ������� � ���";
            this.txMine.Id = 22;
            this.txMine.Name = "txMine";
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // standaloneBarDockControl1
            // 
            this.standaloneBarDockControl1.CausesValidation = false;
            this.standaloneBarDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.standaloneBarDockControl1.Location = new System.Drawing.Point(0, 0);
            this.standaloneBarDockControl1.Name = "standaloneBarDockControl1";
            this.standaloneBarDockControl1.Size = new System.Drawing.Size(940, 61);
            this.standaloneBarDockControl1.Text = "standaloneBarDockControl1";
            // 
            // bar2
            // 
            this.bar2.BarName = "������� ����";
            this.bar2.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Standalone;
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 1;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Standalone;
            this.bar2.FloatLocation = new System.Drawing.Point(657, 244);
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btnDataAnalysis),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnNoGps),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnDeleteNoCorrect),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnMissingData),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnAdding),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnDuplicatedData),
            new DevExpress.XtraBars.LinkPersistInfo(this.barBtnCancel)});
            this.bar2.Offset = 1;
            this.bar2.OptionsBar.AllowQuickCustomization = false;
            this.bar2.StandaloneBarDockControl = this.standaloneBarDockControl1;
            this.bar2.Text = "������� ����";
            // 
            // btnDataAnalysis
            // 
            this.btnDataAnalysis.Border = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.btnDataAnalysis.Caption = "����������";
            this.btnDataAnalysis.Glyph = ((System.Drawing.Image)(resources.GetObject("btnDataAnalysis.Glyph")));
            this.btnDataAnalysis.Hint = "���������� GPS ������";
            this.btnDataAnalysis.Id = 0;
            this.btnDataAnalysis.Name = "btnDataAnalysis";
            this.btnDataAnalysis.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnDataAnalysis_ItemClick);
            // 
            // btnNoGps
            // 
            this.btnNoGps.Border = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.btnNoGps.Caption = "��� GPS";
            this.btnNoGps.Glyph = ((System.Drawing.Image)(resources.GetObject("btnNoGps.Glyph")));
            this.btnNoGps.Hint = "���������� GPS";
            this.btnNoGps.Id = 1;
            this.btnNoGps.Name = "btnNoGps";
            this.btnNoGps.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnNoGps_ItemClick);
            // 
            // btnDeleteNoCorrect
            // 
            this.btnDeleteNoCorrect.Border = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.btnDeleteNoCorrect.Caption = "�������� ���";
            this.btnDeleteNoCorrect.Glyph = ((System.Drawing.Image)(resources.GetObject("btnDeleteNoCorrect.Glyph")));
            this.btnDeleteNoCorrect.Hint = "�������� ������������ ���";
            this.btnDeleteNoCorrect.Id = 2;
            this.btnDeleteNoCorrect.Name = "btnDeleteNoCorrect";
            this.btnDeleteNoCorrect.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnDeleteNoCorrect_ItemClick);
            // 
            // btnMissingData
            // 
            this.btnMissingData.Border = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.btnMissingData.Caption = "��������";
            this.btnMissingData.Glyph = ((System.Drawing.Image)(resources.GetObject("btnMissingData.Glyph")));
            this.btnMissingData.Hint = "����������� ������";
            this.btnMissingData.Id = 3;
            this.btnMissingData.Name = "btnMissingData";
            this.btnMissingData.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnMissingData_ItemClick);
            // 
            // btnAdding
            // 
            this.btnAdding.Border = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.btnAdding.Caption = "��������";
            this.btnAdding.Glyph = ((System.Drawing.Image)(resources.GetObject("btnAdding.Glyph")));
            this.btnAdding.Hint = "�������� ������";
            this.btnAdding.Id = 4;
            this.btnAdding.Name = "btnAdding";
            this.btnAdding.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnAdding_ItemClick);
            // 
            // btnDuplicatedData
            // 
            this.btnDuplicatedData.Border = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.btnDuplicatedData.Caption = "���������";
            this.btnDuplicatedData.Glyph = ((System.Drawing.Image)(resources.GetObject("btnDuplicatedData.Glyph")));
            this.btnDuplicatedData.Hint = "���������� ������";
            this.btnDuplicatedData.Id = 5;
            this.btnDuplicatedData.Name = "btnDuplicatedData";
            this.btnDuplicatedData.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnDuplicatedData_ItemClick);
            // 
            // barBtnCancel
            // 
            this.barBtnCancel.Border = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.barBtnCancel.Caption = "������";
            this.barBtnCancel.Glyph = ((System.Drawing.Image)(resources.GetObject("barBtnCancel.Glyph")));
            this.barBtnCancel.Hint = "������ �������� ��������";
            this.barBtnCancel.Id = 23;
            this.barBtnCancel.Name = "barBtnCancel";
            this.barBtnCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barBtnCancel_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(1189, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 542);
            this.barDockControlBottom.Size = new System.Drawing.Size(1189, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 542);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1189, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 542);
            // 
            // lbInfor
            // 
            this.lbInfor.Border = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.lbInfor.Caption = "����������";
            this.lbInfor.Edit = this.textInfo;
            this.lbInfor.Hint = "���������� � ������";
            this.lbInfor.Id = 14;
            this.lbInfor.Name = "lbInfor";
            this.lbInfor.Width = 22;
            // 
            // textInfo
            // 
            this.textInfo.AutoHeight = false;
            this.textInfo.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.textInfo.Name = "textInfo";
            this.textInfo.ReadOnly = true;
            // 
            // pbAnaliz
            // 
            this.pbAnaliz.Border = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.pbAnaliz.Caption = "��������";
            this.pbAnaliz.Edit = this.repositoryItemProgressBar2;
            this.pbAnaliz.Hint = "������� ���������";
            this.pbAnaliz.Id = 15;
            this.pbAnaliz.Name = "pbAnaliz";
            this.pbAnaliz.Width = 400;
            // 
            // repositoryItemProgressBar2
            // 
            this.repositoryItemProgressBar2.Name = "repositoryItemProgressBar2";
            // 
            // textEditPeriod
            // 
            this.textEditPeriod.Appearance.Options.UseTextOptions = true;
            this.textEditPeriod.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.textEditPeriod.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEditPeriod.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.textEditPeriod.AutoHeight = false;
            this.textEditPeriod.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.textEditPeriod.MaxLength = 50;
            this.textEditPeriod.Name = "textEditPeriod";
            this.textEditPeriod.ReadOnly = true;
            // 
            // textEndPeriod
            // 
            this.textEndPeriod.Appearance.Options.UseTextOptions = true;
            this.textEndPeriod.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.textEndPeriod.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.textEndPeriod.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.textEndPeriod.AutoHeight = false;
            this.textEndPeriod.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.textEndPeriod.MaxLength = 50;
            this.textEndPeriod.Name = "textEndPeriod";
            this.textEndPeriod.ReadOnly = true;
            // 
            // dateLengthInterval
            // 
            this.dateLengthInterval.Appearance.Options.UseTextOptions = true;
            this.dateLengthInterval.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.dateLengthInterval.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.dateLengthInterval.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.dateLengthInterval.AutoHeight = false;
            this.dateLengthInterval.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.dateLengthInterval.Name = "dateLengthInterval";
            this.dateLengthInterval.ReadOnly = true;
            // 
            // repositoryItemProgressBar1
            // 
            this.repositoryItemProgressBar1.Name = "repositoryItemProgressBar1";
            // 
            // repositoryItemProgressBar3
            // 
            this.repositoryItemProgressBar3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.repositoryItemProgressBar3.EndColor = System.Drawing.SystemColors.HotTrack;
            this.repositoryItemProgressBar3.Name = "repositoryItemProgressBar3";
            this.repositoryItemProgressBar3.ReadOnly = true;
            this.repositoryItemProgressBar3.ShowTitle = true;
            this.repositoryItemProgressBar3.TextOrientation = DevExpress.Utils.Drawing.TextOrientation.Horizontal;
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.pbAnalize);
            this.panelControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl2.Location = new System.Drawing.Point(0, 0);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(936, 33);
            this.panelControl2.TabIndex = 0;
            // 
            // pbAnalize
            // 
            this.pbAnalize.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbAnalize.Location = new System.Drawing.Point(2, 2);
            this.pbAnalize.MenuManager = this.barManager1;
            this.pbAnalize.Name = "pbAnalize";
            this.pbAnalize.Properties.Appearance.BackColor = System.Drawing.Color.WhiteSmoke;
            this.pbAnalize.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.pbAnalize.Properties.EndColor = System.Drawing.SystemColors.HotTrack;
            this.pbAnalize.Properties.ShowTitle = true;
            this.pbAnalize.Properties.StartColor = System.Drawing.SystemColors.HotTrack;
            this.pbAnalize.Properties.Step = 1;
            this.pbAnalize.Properties.TextOrientation = DevExpress.Utils.Drawing.TextOrientation.Horizontal;
            this.pbAnalize.Size = new System.Drawing.Size(932, 29);
            this.pbAnalize.TabIndex = 0;
            // 
            // usersTab
            // 
            this.usersTab.Name = "usersTab";
            this.usersTab.Size = new System.Drawing.Size(1183, 514);
            this.usersTab.Text = "������������";
            // 
            // mobitelsSensorsBindingSource
            // 
            this.mobitelsSensorsBindingSource.DataMember = "mobitels_sensors";
            this.mobitelsSensorsBindingSource.DataSource = this.mobitelsBindingSource;
            this.mobitelsSensorsBindingSource.Filter = "Length=1";
            this.mobitelsSensorsBindingSource.Sort = "Id ASC";
            // 
            // mobitelsBindingSource
            // 
            this.mobitelsBindingSource.DataMember = "mobitels";
            this.mobitelsBindingSource.DataSource = typeof(LocalCache.atlantaDataSet);
            // 
            // bindingSource1
            // 
            this.bindingSource1.DataMember = "mobitels";
            this.bindingSource1.DataSource = this.atlantaDataSet1;
            // 
            // atlantaDataSet1
            // 
            this.atlantaDataSet1.DataSetName = "atlantaDataSet";
            this.atlantaDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "add.png");
            // 
            // sensorStateFalseBindingSource
            // 
            this.sensorStateFalseBindingSource.DataMember = "sensors_State";
            this.sensorStateFalseBindingSource.DataSource = this.mobitelsSensorsBindingSource;
            this.sensorStateFalseBindingSource.Filter = "MinValue=0";
            // 
            // sensorStateTrueBindingSource
            // 
            this.sensorStateTrueBindingSource.DataMember = "sensors_State";
            this.sensorStateTrueBindingSource.DataSource = this.mobitelsSensorsBindingSource;
            this.sensorStateTrueBindingSource.Filter = "MinValue=1";
            // 
            // barManager2
            // 
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.MaxItemId = 0;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Size = new System.Drawing.Size(1189, 0);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 542);
            this.barDockControl2.Size = new System.Drawing.Size(1189, 0);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 0);
            this.barDockControl3.Size = new System.Drawing.Size(0, 542);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(1189, 0);
            this.barDockControl4.Size = new System.Drawing.Size(0, 542);
            // 
            // AdminForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._tabsPcbPage);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.Name = "AdminForm";
            this.Size = new System.Drawing.Size(1189, 542);
            ((System.ComponentModel.ISupportInitialize)(this._tabsPcbPage)).EndInit();
            this._tabsPcbPage.ResumeLayout(false);
            this._adminTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            this.adminTable.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pgParams)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chDataAnaliz)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleLang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chOpenStreetMapUse)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.osmLang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chGraphFuelShow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chMinTimeMove)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditCode)).EndInit();
            this.tpAdjustingSensors.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl3)).EndInit();
            this.splitContainerControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlPcbVers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridPcbVers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl4)).EndInit();
            this.splitContainerControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlSetup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSetups)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.taririvkaItemButtonEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stateItemButtonEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.situationItemButtonEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.algorithmComboBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.itemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlLogic)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sensorsGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rpstrItemButtonEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rpsItemButtonEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rpItemButtonEdit)).EndInit();
            this.analysisTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl5)).EndInit();
            this.splitContainerControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl6)).EndInit();
            this.splitContainerControl6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ctrlAnalisis)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridAnalisis)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateBeginPeriod.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateBeginPeriod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEndPeriod.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEndPeriod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBar2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEditPeriod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEndPeriod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateLengthInterval)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemProgressBar3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbAnalize.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mobitelsSensorsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mobitelsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.atlantaDataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sensorStateFalseBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sensorStateTrueBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vehicleBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            this.ResumeLayout(false);

    }

    #endregion

    private DevExpress.XtraTab.XtraTabControl _tabsPcbPage;
    private DevExpress.XtraTab.XtraTabPage _adminTab;
    private DevExpress.XtraTab.XtraTabPage _settingsTab;
    private SettingSensorsControl settingSensorsControl1;
    private LocalCache.atlantaDataSet atlantaDataSet1;
    private System.Windows.Forms.BindingSource bindingSource1;
    private DevExpress.XtraTab.XtraTabPage usersTab;
    private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
    private System.Windows.Forms.TableLayoutPanel adminTable;
    private TrackControl.Setting.Control.PassChanger passChanger1;
    private TrackControl.Setting.Control.LanSettings lanSettings;
    private TrackControl.Setting.Control.LayoutReset _layoutReseter;
    private DevExpress.XtraVerticalGrid.PropertyGridControl pgParams;
    private DevExpress.XtraVerticalGrid.Rows.CategoryRow crTotalSettings;
    private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chDataAnaliz;
    private DevExpress.XtraVerticalGrid.Rows.EditorRow erIsDataAnalizOn;
    private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit rleLang;
    private DevExpress.XtraVerticalGrid.Rows.CategoryRow crLuxena;
    private DevExpress.XtraVerticalGrid.Rows.EditorRow erIsLuxenaUse;
    private DevExpress.XtraVerticalGrid.Rows.EditorRow erLuxenaLang;
    private DevExpress.XtraVerticalGrid.Rows.EditorRow erLuxenaServer;
    private DevExpress.XtraTab.XtraTabPage pcbPageTab;
    private DevExpress.XtraTab.XtraTabPage tpAdjustingSensors;
    private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
    private DevExpress.XtraEditors.XtraUserControl controlTransport;
    private DevExpress.XtraEditors.SplitContainerControl splitContainerControl3;
    private DevExpress.XtraEditors.PanelControl panelControl1;
    private DevExpress.XtraEditors.SimpleButton btnSettingPcb;
    private DevExpress.XtraGrid.GridControl gridControlPcbVers;
    private DevExpress.XtraGrid.Views.Grid.GridView gridPcbVers;
    private DevExpress.XtraGrid.Columns.GridColumn colNamePcb;
    private DevExpress.XtraGrid.Columns.GridColumn colComment;
    private DevExpress.XtraGrid.Columns.GridColumn colSetting;
    private DevExpress.XtraGrid.Columns.GridColumn colID;
    private System.Windows.Forms.ImageList imageList1;
    private DevExpress.XtraEditors.SplitContainerControl splitContainerControl4;
    private DevExpress.XtraGrid.GridControl gridControlSetup;
    private DevExpress.XtraGrid.Views.Grid.GridView gridSetups;
    private DevExpress.XtraGrid.Columns.GridColumn colTypeData;
    private DevExpress.XtraGrid.Columns.GridColumn colStartBit;
    private DevExpress.XtraGrid.Columns.GridColumn colLengthRecord;
    private DevExpress.XtraGrid.Columns.GridColumn colKoeffK;
    private DevExpress.XtraGrid.Columns.GridColumn colComms;
    private DevExpress.XtraGrid.Columns.GridColumn colTarirovka;
    private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit taririvkaItemButtonEdit;
    private DevExpress.XtraGrid.Columns.GridColumn colState;
    private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit stateItemButtonEdit;
    private DevExpress.XtraGrid.Columns.GridColumn colSituation;
    private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit situationItemButtonEdit;
    private DevExpress.XtraGrid.Columns.GridColumn colAlgorithm;
    private DevExpress.XtraEditors.Repository.RepositoryItemComboBox algorithmComboBox;
    private DevExpress.XtraGrid.Columns.GridColumn colIdData;
    private DevExpress.XtraGrid.Columns.GridColumn colIdPcbs;
    private DevExpress.XtraEditors.GroupControl groupControl1;
    private DevExpress.XtraGrid.GridControl gridControlLogic;
    private DevExpress.XtraGrid.Views.Grid.GridView sensorsGrid;
    private DevExpress.XtraGrid.Columns.GridColumn nameDataGridViewTextBoxColumn1;
    private DevExpress.XtraGrid.Columns.GridColumn startBitDataGridViewTextBoxColumn;
    private DevExpress.XtraGrid.Columns.GridColumn SpecSettingColumn;
    private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit rpItemButtonEdit;
    private System.Windows.Forms.BindingSource mobitelsBindingSource;
    private System.Windows.Forms.BindingSource mobitelsSensorsBindingSource;
    private System.Windows.Forms.BindingSource sensorStateFalseBindingSource;
    private System.Windows.Forms.BindingSource sensorStateTrueBindingSource;
    private System.Windows.Forms.BindingSource vehicleBindingSource;
    private DevExpress.XtraGrid.Columns.GridColumn id;
    private DevExpress.XtraGrid.Columns.GridColumn colState0;
    private DevExpress.XtraGrid.Columns.GridColumn colState1;
    private DevExpress.XtraGrid.Columns.GridColumn colCondition;
    private DevExpress.XtraGrid.Columns.GridColumn colEvents;
    private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit rpstrItemButtonEdit;
    private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit rpsItemButtonEdit;
    private DevExpress.XtraGrid.Columns.GridColumn columnNameUnit;
    private DevExpress.XtraTab.XtraTabPage analysisTab;
    private DevExpress.XtraEditors.SplitContainerControl splitContainerControl5;
    private DevExpress.XtraEditors.GroupControl groupControl2;
    private DevExpress.XtraBars.StandaloneBarDockControl standaloneBarDockControl1;
    private DevExpress.XtraBars.BarManager barManager1;
    private DevExpress.XtraBars.Bar bar1;
    private DevExpress.XtraBars.Bar bar2;
    private DevExpress.XtraBars.BarButtonItem btnDataAnalysis;
    private DevExpress.XtraBars.BarButtonItem btnNoGps;
    private DevExpress.XtraBars.BarButtonItem btnDeleteNoCorrect;
    private DevExpress.XtraBars.BarButtonItem btnMissingData;
    private DevExpress.XtraBars.BarButtonItem btnAdding;
    private DevExpress.XtraBars.BarButtonItem btnDuplicatedData;
    private DevExpress.XtraBars.BarDockControl barDockControlTop;
    private DevExpress.XtraBars.BarDockControl barDockControlBottom;
    private DevExpress.XtraBars.BarDockControl barDockControlLeft;
    private DevExpress.XtraBars.BarDockControl barDockControlRight;
    private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit textEditPeriod;
    private DevExpress.XtraBars.BarEditItem barBeginDate;
    private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit dateBeginPeriod;
    private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit textEndPeriod;
    private DevExpress.XtraBars.BarEditItem barEndDate;
    private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit dateEndPeriod;
    private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit dateLengthInterval;
    private DevExpress.XtraBars.BarEditItem lbInfor;
    private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit textInfo;
    private DevExpress.XtraEditors.Repository.RepositoryItemProgressBar repositoryItemProgressBar1;
    private DevExpress.XtraBars.BarEditItem pbAnaliz;
    private DevExpress.XtraEditors.Repository.RepositoryItemProgressBar repositoryItemProgressBar2;
    private DevExpress.XtraEditors.Repository.RepositoryItemProgressBar repositoryItemProgressBar3;
    private DevExpress.XtraBars.BarStaticItem barBeginPeriod;
    private DevExpress.XtraBars.BarStaticItem barEndPeriod;
    private DevExpress.XtraBars.BarStaticItem barStaticItem1;
    private DevExpress.XtraEditors.XtraUserControl panelVehicles;
    private DevExpress.XtraBars.BarEditItem txMine;
    private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
    private DevExpress.XtraEditors.SplitContainerControl splitContainerControl6;
    private DevExpress.XtraEditors.PanelControl panelControl3;
    private DevExpress.XtraGrid.GridControl ctrlAnalisis;
    private DevExpress.XtraGrid.Views.Grid.GridView gridAnalisis;
    private DevExpress.XtraGrid.Columns.GridColumn Car;
    private DevExpress.XtraGrid.Columns.GridColumn DateB;
    private DevExpress.XtraGrid.Columns.GridColumn DateE;
    private DevExpress.XtraGrid.Columns.GridColumn Interval;
    private DevExpress.XtraGrid.Columns.GridColumn QTYpoints;
    private DevExpress.XtraBars.BarDockControl barDockControl3;
    private DevExpress.XtraBars.BarDockControl barDockControl4;
    private DevExpress.XtraBars.BarDockControl barDockControl2;
    private DevExpress.XtraBars.BarDockControl barDockControl1;
    private DevExpress.XtraBars.BarManager barManager2;
    private DevExpress.XtraEditors.PanelControl panelControl2;
    private DevExpress.XtraEditors.ProgressBarControl pbAnalize;
    private DevExpress.XtraBars.BarButtonItem barBtnCancel;
    private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chOpenStreetMapUse;
    private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit osmLang;
    private DevExpress.XtraVerticalGrid.Rows.CategoryRow crOpenStreet;
    private DevExpress.XtraVerticalGrid.Rows.EditorRow erIsOpenStreetMapUse;
    private DevExpress.XtraVerticalGrid.Rows.EditorRow erOpenStreetMapServer;
    private DevExpress.XtraVerticalGrid.Rows.EditorRow erOpenStreetMapPort;
    private DevExpress.XtraVerticalGrid.Rows.EditorRow erMapLang;
    private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chGraphFuelShow;
    private DevExpress.XtraVerticalGrid.Rows.EditorRow erIsGraphFuelShow;
    private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit chMinTimeMove;
    private DevExpress.XtraVerticalGrid.Rows.EditorRow erOpenStreetMapKey;
    private DevExpress.XtraVerticalGrid.Rows.CategoryRow crRcsNominatim;
    private DevExpress.XtraVerticalGrid.Rows.EditorRow erIsRcsNominatimUse;
    private DevExpress.XtraVerticalGrid.Rows.CategoryRow crGoogleMap;
    private DevExpress.XtraVerticalGrid.Rows.EditorRow erIsGoogleMapUse;
    private DevExpress.XtraVerticalGrid.Rows.EditorRow erGoogleMapKey;
    private DevExpress.XtraGrid.Columns.GridColumn colShifter;
    private DevExpress.XtraGrid.Columns.GridColumn colSigning;
    private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit itemCheckEdit1;
    private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repItemCheckEdit1;
    private DevExpress.XtraVerticalGrid.Rows.EditorRow erGoogleSatelliteKey;
    private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit textEditCode;
  }
}