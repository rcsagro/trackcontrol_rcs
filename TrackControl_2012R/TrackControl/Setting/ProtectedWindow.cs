using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Collections;

namespace TrackControl.Setting.Controls
{
    /// <summary>
    /// ������� ������� ��� ���������� ��������. ���� ���������� �� ����� ����
    /// �� ������ ��� ������������ ���� �����������
    /// </summary>
    [Serializable]
    public partial class ProtectedWindow : UserControl
    {
        #region Fields
        /// <summary>
        /// ����, ����������� ����� �� ������������ ����� ��� ������ � ������ ���������
        /// </summary>
        private bool _accessGranted = false;
        /// <summary>
        /// ������� ���� �����������
        /// </summary>
        private LoginWindow _loginWindow;
        #endregion

        #region .ctor
        /// <summary>
        /// �����������
        /// </summary>
        protected ProtectedWindow()
        {
            InitializeComponent();
            customInitialization();
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// ������������� �����������, ����������� ��� ������ VS-���������
        /// </summary>
        private void customInitialization()
        {
            //if (String.IsNullOrEmpty(passHash))
            //    throw new Exception("�� ����� ��� ������");

            _loginWindow = new LoginWindow(passHash);
            _loginWindow.Dock = DockStyle.Fill;
            _loginWindow.Authorize += new EventHandler(loginWindow_Authorize);
            this.Controls.Add(_loginWindow);

            showLoginWindow();
        }

        /// <summary>
        /// ���������� ������� ����������� ������������.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void loginWindow_Authorize(object sender, EventArgs e)
        {
            hideLoginWindow();
        }

        /// <summary>
        /// ���������� ����� ����������� � �������� ����� ��������
        /// </summary>
        private void showLoginWindow()
        {
            this.mainTableLayoutPanel.Visible = false;
            this._loginWindow.Reset();
            this._loginWindow.Visible = true;
        }

        /// <summary>
        /// �������� ����� ����������� � ���������� ����� ��������
        /// </summary>
        private void hideLoginWindow()
        {
            this.mainTableLayoutPanel.Visible = true;
            this._loginWindow.Visible = false;
        }
        #endregion

        #region Protected Members
        /// <summary>
        /// ���������� ��� ������. ������ ���� �������������� � ����������� �������.
        /// </summary>
        /// <returns></returns>
        protected virtual string passHash
        {
            get { return ""; }
        }
        #endregion

        #region GUI Event Handlers
        private void logOut(object sender, EventArgs e)
        {
            showLoginWindow();
        }
        #endregion
    }
}
