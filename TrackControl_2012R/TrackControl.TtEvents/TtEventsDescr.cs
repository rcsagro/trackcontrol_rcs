#region Using directives
using System;
#endregion

namespace TrackControl.TtEvents
{
    /// <summary>
    /// �������� ������� ���������
    /// </summary>
    public static class TtEventsDescr
    {
        /// <summary>
        /// ����� 0x00000080 - �������� ���� ���.
        /// </summary>
        public const uint MAIN_POWER_ON = 0x00000080;

        /// <summary>
        /// ����� 0x00000100 - ��������� ����������� ���.//������� �� ��������� �������� �������
        /// </summary>
        public const uint RESERVE_POWER_ON = 0x00000100;

        /// <summary>
        /// ����� 0x00080000 - ����� ���������
        /// </summary>
        public const uint PROGRAM_START = 0x00080000;

        /// <summary>
        /// ����� 0x00100000 - ������������ ���������
        /// </summary>
        public const uint PROGRAM_RESTART = 0x00100000;

        /// <summary>
        /// ����� 0x00040000 - ��������� ��������� ��������
        /// </summary>
        public const uint SENSOR_BOARD = 0x00040000;

        /// <summary>
        /// ����� 0x00800000 - ��������� ��������
        /// </summary>
        public const uint ACCELERATION_CHANGING = 0x00800000;

        /// <summary>
        /// ����� 0x00000001 - ����������� ��������
        /// </summary>
        public const uint LOG_MIN_SPEED = 0x00000001;

        /// <summary>
        /// ����� 0x00000008 - ���������� �����
        /// </summary>
        public const uint LOG_COURSE = 0x00000008;

        /// <summary>
        /// ����� 0x00000002 - ������ 1
        /// </summary>
        public const uint TIMER_1 = 0x00000002;

        /// <summary>
        /// ����� 0x00000004 - ������ 2
        /// </summary>
        public const uint TIMER_2 = 0x00000004;

        /// <summary>
        /// ����� 0x00000200 - ����������� � ���� GSM
        /// </summary>
        public const uint GSM_APPEARANCE = 0x00000200;

        /// <summary>
        /// ����� 0x00000400 - ������ ���� GSM
        /// </summary>
        public const uint GSM_FAILURE = 0x00000400;

        /// <summary>
        /// ����� 0x00000010 - ��������� 1
        /// </summary>
        public const uint DISTANCE_1 = 0x00000010;

        /// <summary>
        /// ����� 0x00000020 - ��������� 2
        /// </summary>
        public const uint DISTANCE_2 = 0x00000020;

        /// <summary>
        /// ����� 0x00008000 - ���������� �������� ������� � ������ (�� �������� �� 10)
        /// </summary>
        public const uint PRECIZE_GPS = 0x00008000;

        /// <summary>
        /// ����� 0x00200000 - ���������� ������� (Reserve 1)
        /// </summary>
        public const uint POWER_KEY = 0x00200000;
    }
}
