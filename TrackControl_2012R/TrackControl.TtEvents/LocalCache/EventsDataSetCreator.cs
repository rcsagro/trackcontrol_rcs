using System;

namespace TrackControl.TtEvents.LocalCache
{
    /// <summary>
    /// ������� EventsDataSet.
    /// ��� ������, ������� �������� � ������ ���������,
    /// ������� �������� ��������� � ������� ������, 
    /// � �� �������������� ��������������.
    /// </summary>
    internal static class EventsDataSetCreator
    {
        /// <summary>
        /// ������ �������������
        /// </summary>
        private static volatile object syncObject = new object();

        /// <summary>
        /// ��������� ��������
        /// </summary>
        private static volatile EventsDataSet dataSet;

        /// <summary>
        /// ���������� ��������� ��������
        /// </summary>
        /// <returns>EventsDataSet</returns>
        internal static EventsDataSet GetDataSet()
        {
            if (dataSet == null)
            {
                lock (syncObject)
                {
                    if (dataSet == null)
                        dataSet = new EventsDataSet();
                }
            }

            return dataSet;
        } // GetDataSet()
    }
}
