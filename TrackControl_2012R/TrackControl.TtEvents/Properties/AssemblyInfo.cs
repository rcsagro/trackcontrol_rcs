﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("TrackControl.TtEvents")]
[assembly: AssemblyDescription("Teletrack's events")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("RCS Ltd")]
[assembly: AssemblyProduct("TrackControl")]
[assembly: AssemblyCopyright("Copyright © RCS 2008-2010")]
[assembly: AssemblyTrademark("AVS")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("851c5ed1-ff12-4ebf-b3be-7c5fb12173ef")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
