using LocalCache;
using System;
using System.IO;
using System.Windows.Forms;
using TrackControl.TtEvents.LocalCache;
using TrackControl.General.Log;
using TrackControl.TtEvents.Properties;

namespace TrackControl.TtEvents.Settings
{
    /// <summary>
    /// ���������� �������� ����������� ������� ���������.
    /// <para>������� ������� Event ���������� ��������� ���������
    /// � ����������������� ��������� ���� AnalysisEnable
    /// (���������� � ���������� ������� ������� ��� ���)</para>
    /// <para>�� ������ ����� ����������� ����������� �����
    /// ���� �������� ������� � �������� ���� AnalysisEnable
    /// ���������� � �����������, � ��� ��� ���� � �����.</para>
    /// </summary>
    internal class EventCtrl
    {
        /// <summary>
        /// ���� � ����� � ���������� ������ ������������ �������
        /// </summary>
        private static readonly string _settingFilePath = Path.Combine(
            TrackControl.General.Globals.APP_DATA, "TtEventList.xml");

        private atlantaDataSet dsAtlanta;
        private EventsDataSet dsEvents;
        
        public EventCtrl(atlantaDataSet dataSet)
        {
            dsAtlanta = dataSet;
            dsEvents = EventsDataSetCreator.GetDataSet();
            SetDefaultValues();
            LoadSetting();
        }

        /// <summary>
        /// ���������� ������� Event ���������� �� ���������
        /// </summary>
        private void SetDefaultValues()
        {
            if (dsEvents.Event.Count > 0)
                return;

            dsEvents.Event.AddEventRow(
                TtEventsDescr.MAIN_POWER_ON, Resources.BoardPowerOn, true);
            dsEvents.Event.AddEventRow(
                TtEventsDescr.RESERVE_POWER_ON, Resources.BatteryOn, true);
            dsEvents.Event.AddEventRow(
                TtEventsDescr.PROGRAM_START, Resources.AppStarted, false);
            dsEvents.Event.AddEventRow(
                TtEventsDescr.PROGRAM_RESTART, Resources.AppRestarted, false);
            dsEvents.Event.AddEventRow(
                TtEventsDescr.SENSOR_BOARD, Resources.SensorsStateChanged, true);
            dsEvents.Event.AddEventRow(
                TtEventsDescr.ACCELERATION_CHANGING, Resources.SpeedChanged, false);
            dsEvents.Event.AddEventRow(
                TtEventsDescr.LOG_MIN_SPEED, Resources.SpeedLowLimit, false);
            dsEvents.Event.AddEventRow(
                TtEventsDescr.LOG_COURSE, Resources.CourseShift, false);
            dsEvents.Event.AddEventRow(
                TtEventsDescr.TIMER_1, Resources.Timer1, false);
            dsEvents.Event.AddEventRow(
                TtEventsDescr.TIMER_2, Resources.Timer2, false);
            dsEvents.Event.AddEventRow(
                TtEventsDescr.GSM_APPEARANCE, Resources.GsmRegistration, false);
            dsEvents.Event.AddEventRow(
                TtEventsDescr.GSM_FAILURE, Resources.GsmLost, false);
            dsEvents.Event.AddEventRow(
                TtEventsDescr.DISTANCE_1, Resources.Distance1, false);
            dsEvents.Event.AddEventRow(
                TtEventsDescr.DISTANCE_2, Resources.Distance2, false);
            dsEvents.Event.AddEventRow(TtEventsDescr.POWER_KEY, Resources.PowerKey, false);

            dsEvents.Event.AcceptChanges();
        }

        /// <summary>
        /// �������� ��������� �� �����
        /// </summary>
        private void LoadSetting()
        {
            if (File.Exists(_settingFilePath))
            {
                try
                {
                    EventsDataSet.EventDataTable tmpTable = new EventsDataSet.EventDataTable();
                    tmpTable.ReadXml(_settingFilePath);
                    SetSettingListToRights(tmpTable);
                }
                catch (Exception ex)
                {
                    ExecuteLogging.Log("App", String.Format(
                        "The Accident was happen while eventsetting xml loading. XML path: {0}{2}{1}",
                        _settingFilePath, ex, Environment.NewLine));
                }
            }
        }

        /// <summary>
        /// �������� ������� �������� Event � �������. 
        /// <para>�������� ���� AnalysisEnable, � ������������ 
        /// � ������������ ����� �����������.</para>
        /// </summary>
        private void SetSettingListToRights(EventsDataSet.EventDataTable tableFromFile)
        {
            foreach (EventsDataSet.EventRow tmpRow in tableFromFile)
            {
                EventsDataSet.EventRow eventRow = dsEvents.Event.FindByMask(tmpRow.Mask);

                if (eventRow == null)
                    continue;

                if (tmpRow.AnalysisEnable != eventRow.AnalysisEnable)
                {
                    eventRow.AnalysisEnable = tmpRow.AnalysisEnable;
                    eventRow.AcceptChanges();
                }
            }
        }

        /// <summary>
        /// ���������� ��������� � ����
        /// </summary>
        private void SaveSetting()
        {
            try
            {
                // ��������� �� ������� �������, � �����, ����� ������
                // ��� ���������� � ��������. ����� ��� ������(LoadSetting)
                // XML �������� ��� ����� ����������� �� ��������� �������.
                dsEvents.Event.Copy().WriteXml(_settingFilePath);
            }
            catch (Exception ex)
            {
                ExecuteLogging.Log("App", String.Format(
                    "The accident was happen during eventsetting xml saving. XML path: {0}{2}{1}",
                    _settingFilePath, ex, Environment.NewLine));
            }
        }

        /// <summary>
        /// ���������� �����
        /// </summary>
        /// <returns>DialogResult: OK - ������ ������ ����������</returns>
        internal DialogResult ShowForm()
        {
            DialogResult result = new EventListForm(dsAtlanta).ShowDialog();

            if (result == DialogResult.OK)
                SaveSetting();

            return result;
        }
    }
}
