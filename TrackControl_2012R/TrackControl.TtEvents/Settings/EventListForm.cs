#region Using directives
using System.Windows.Forms;
using LocalCache;
using TrackControl.TtEvents.LocalCache;
using TrackControl.TtEvents.Properties;
#endregion

namespace TrackControl.TtEvents.Settings
{
    /// <summary>
    /// ����� ������ ������������� ������� ������� ���������
    /// </summary>
    partial class EventListForm : Form
    {
        private atlantaDataSet dsAtlanta;
        private EventsDataSet dsEvents;

        private EventListForm()
        {
            InitializeComponent();
            init();
        }

        /// <summary>
        /// Create report selector form
        /// </summary>
        /// <param name="reports">atlantaDataSet</param>
        public EventListForm(atlantaDataSet dataSet)
            : this()
        {
            dsAtlanta = dataSet;
            dsEvents = EventsDataSetCreator.GetDataSet();
            bindingSource.DataSource = dsEvents;
        }

        private void init()
        {
            btnOk.Text = Resources.Apply;
            btnCancel.Text = Resources.Cancel;
            dataGridViewCheckBoxColumn1.HeaderText = Resources.Analyse;
            dataGridViewCheckBoxColumn1.ToolTipText = Resources.PlaceInReports;
            dataGridViewTextBoxColumn2.HeaderText = Resources.EventDescription;
            dataGridViewTextBoxColumn2.ToolTipText = Resources.EventDescription;
            Text = Resources.EventTypeRangeSettings;
        }

        private void grid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if ((e.ColumnIndex == 0) && (!btnOk.Enabled))
                btnOk.Enabled = true;
        }

        private void ReportListForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.DialogResult == DialogResult.Cancel)
                dsEvents.Event.RejectChanges();
            else
                dsEvents.Event.AcceptChanges();
        }

        private void btnOk_Click(object sender, System.EventArgs e)
        {

        }
    }
}