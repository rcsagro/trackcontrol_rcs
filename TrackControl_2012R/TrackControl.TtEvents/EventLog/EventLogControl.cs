using System;
using System.Windows.Forms;
using LocalCache;
using TrackControl.TtEvents.LocalCache;
using TrackControl.TtEvents.Settings;
using TrackControl.General;
using TrackControl.TtEvents.Properties;

namespace TrackControl.TtEvents.EventLog
{
    /// <summary>
    /// ������� ��� ������ � �������� �������
    /// </summary>
    public partial class EventLogControl : UserControl
    {
        private atlantaDataSet dsAtlanta;
        private EventsDataSet dsEvents;
        private EventCtrl eventCfgCtrl;
        private EventLogGenerator eventLogGenerator;

        public EventLogControl()
        {
            InitializeComponent();
            init();
            btnFill.Image = Shared.Right;
            btnSettings.Image = Shared.Settings;
        }

        private void init()
        {
            btnFill.Text = Resources.BuildEventLog;
            btnSettings.Text = Resources.SetupEventTypeRange;
            dataGridViewTextBoxColumn4.ToolTipText = Resources.EventDateTime;
            dataGridViewTextBoxColumn5.HeaderText = Resources.EventDescription;
            dataGridViewTextBoxColumn5.ToolTipText = Resources.EventDescription;
        }

        /// <summary>
        /// �������������
        /// </summary>
        /// <param name="dataSet">atlantaDataSet</param>
        public void Init(atlantaDataSet dataSet)
        {
            dsAtlanta = dataSet;
            dsEvents = EventsDataSetCreator.GetDataSet();
            eventCfgCtrl = new EventCtrl(dataSet);
            eventLogGenerator = new EventLogGenerator(dataSet);
            bindingSource.DataSource = dsEvents;
        }

        /// <summary>
        /// ���������� ������� ������� ������ ������������
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSettings_Click(object sender, EventArgs e)
        {
            eventCfgCtrl.ShowForm();
        }

        /// <summary>
        /// ��������� ���������� �� �������������� ���������
        /// </summary>
        /// <param name="mobitelId">Mobitel id</param>
        public void SetFilter(int mobitelId)
        {
            bindingSource.Filter = String.Format("MobitelId = {0}", mobitelId);
        }

        /// <summary>
        /// ������� ������� �������
        /// </summary>
        public void ClearEventLog()
        {
            dsEvents.EventLog.Clear();
        } // ClearEventLog()

        /// <summary>
        /// ���������� ������� ������� ������ ������������ ������
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnFill_Click(object sender, EventArgs e)
        {
            Cursor originalCursor = Cursor;
            Cursor = Cursors.WaitCursor;
            bindingNavigator.Enabled = false;
            bindingSource.SuspendBinding();

            try
            {
                eventLogGenerator.FillLog();
            }
            finally
            {
                bindingSource.ResumeBinding();
                bindingNavigator.Enabled = true;
                Cursor = originalCursor;
            }
        }
    }
}
