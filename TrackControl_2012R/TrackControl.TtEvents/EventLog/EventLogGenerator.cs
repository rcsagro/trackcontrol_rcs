#region Using directives
using LocalCache;
using System.Collections.Generic;
using TrackControl.TtEvents.LocalCache;
using System;
using TrackControl.TtEvents.Properties;
using TrackControl.Vehicles;

#endregion

namespace TrackControl.TtEvents.EventLog
{
    /// <summary>
    /// ���������� ���� �������.
    /// <para>������������ ���������� �� ��������� ������
    /// ��������� (������� <see cref="LocalCache.atlantaDataSet.dataviewDataTable"/>) �
    /// �������� ������� (������� 
    /// <see cref="TrackControl.TtEvents.LocalCache.EventsDataSet.EventDataTable"/>)</para>
    /// </summary>
    internal class EventLogGenerator
    {
        private atlantaDataSet dsAtlanta;
        private EventsDataSet dsEvents;

        private List<EventsDataSet.EventRow> _enabledEvents;

        /// <summary>
        /// �������, ������� ���������� ���������
        /// </summary>
        private List<EventsDataSet.EventRow> enabledEvents
        {
            get
            {
                if (_enabledEvents.Count == 0)
                {
                    foreach (EventsDataSet.EventRow row in dsEvents.Event)
                    {
                        if (row.AnalysisEnable)
                            _enabledEvents.Add(row);
                    }
                }
                return _enabledEvents;
            }
        }

        public EventLogGenerator(atlantaDataSet dataSet)
        {
            _enabledEvents = new List<EventsDataSet.EventRow>();
            dsAtlanta = dataSet;
            dsEvents = EventsDataSetCreator.GetDataSet();
        }

        /// <summary>
        /// ��������� ��� ������� atlantaDataSet.EventLog
        /// </summary>
        public void FillLog()
        {
            _enabledEvents.Clear();
            dsEvents.EventLog.Clear();

            if (enabledEvents.Count == 0)
                return;

            try
            {
                FillEventLogTable();
                FillEventLogTable64();
            }
            catch (OutOfMemoryException ex)
            {
                dsEvents.EventLog.Clear();
                throw new OutOfMemoryException(
                    String.Format(
                        "Not enough memory for Event Log building.{0}Reduce Time Interval or change Event Type Range{0}Press {1}",
                        Environment.NewLine, Resources.Continue), ex);
            }
            finally
            {
                dsEvents.AcceptChanges();
            }
        }

        /// <summary>
        /// ���������� ������� EventLog
        /// </summary>
        private void FillEventLogTable()
        {
            foreach (atlantaDataSet.dataviewRow dataViewRow in dsAtlanta.dataview)
            {
                foreach (EventsDataSet.EventRow eventRow in enabledEvents)
                {
                    if ((eventRow.Mask & dataViewRow.Events) > 0)
                    {
                        EventsDataSet.EventLogRow logRow = dsEvents.EventLog.NewEventLogRow();
                        logRow.DataGpsId = dataViewRow.DataGps_ID;
                        logRow.MobitelId = dataViewRow.Mobitel_ID;
                        logRow.Time = dataViewRow.time;
                        logRow.EventMask = eventRow.Mask;
                        logRow.Description = eventRow.Description;
                        dsEvents.EventLog.AddEventLogRow(logRow);
                    } // if (eventRow.Mask)
                } // foreach (eventRow)
                //System.Windows.Forms.Application.DoEvents();
            }
        }

        /// <summary>
        /// ���������� ������� EventLog
        /// </summary>
        private void FillEventLogTable64()
        {
            
            foreach (var pair in DataSetManager.GpsDatas64)
	        {
	            List<GpsData> gpsDatas =  pair.Value;
	            foreach (var gpsData in gpsDatas)
	            {
                    foreach (EventsDataSet.EventRow eventRow in enabledEvents)
                    {
                        if ((eventRow.Mask & gpsData.Events) > 0)
                        {
                            EventsDataSet.EventLogRow logRow = dsEvents.EventLog.NewEventLogRow();
                            logRow.DataGpsId = gpsData.Id;
                            logRow.MobitelId = gpsData.Mobitel;
                            logRow.Time = gpsData.Time;
                            logRow.EventMask = eventRow.Mask;
                            logRow.Description = eventRow.Description;
                            dsEvents.EventLog.AddEventLogRow(logRow);
                        } // if (eventRow.Mask)
                    } // foreach (eventRow)
	            }

	        }
        }
    }
}
