using System;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using TrackControl.General.DAL;
using TrackControl.General.DatabaseDriver;

namespace LocalCache.atlantaDataSetTableAdapters
{
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute( "System.Data.Design.TypedDataSetGenerator", "2.0.0.0" )]
    [global::System.ComponentModel.DesignerCategoryAttribute( "code" )]
    [global::System.ComponentModel.ToolboxItem( true )]
    [global::System.ComponentModel.DataObjectAttribute( true )]
    [global::System.ComponentModel.DesignerAttribute( "Microsoft.VSDesigner.DataSource.Design.TableAdapterDesigner, Microsoft.VSDesigner" +
        ", Version=8.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" )]
    [global::System.ComponentModel.Design.HelpKeywordAttribute( "vs.data.TableAdapter" )]
    public class DriverTableAdapter : global::System.ComponentModel.Component
    {
        //private global::MySql.Data.MySqlClient.MySqlDataAdapter _adapter;
        //private global::MySql.Data.MySqlClient.MySqlConnection _connection;
        //private global::MySql.Data.MySqlClient.MySqlCommand[] _commandCollection;

        DriverDb db = new DriverDb();

        private bool _clearBeforeFill;

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public DriverTableAdapter()
        {
            this.ClearBeforeFill = true;
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        //private global::MySql.Data.MySqlClient.MySqlDataAdapter Adapter
        private object Adapter
        {
            get
            {
                //if( ( this._adapter == null ) )
                if( ( this.db.Adapter == null ) )
                {
                    this.InitAdapter();
                }
                //return this._adapter;
                return this.db.Adapter;
            }
        } // Adapter

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        //public global::MySql.Data.MySqlClient.MySqlConnection Connection
        public object Connection
        {
            get
            {
                if( ( this.db.Connection == null ) )
                {
                    this.InitConnection();
                }

                return this.db.Connection;
            }
            set
            {
                if( ( this.db.Connection == null ) )
                {
                    this.InitConnection();
                }

                this.db.Connection = value;

                if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                {
                    MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;

                    if ((adapter.InsertCommand != null))
                    {
                        this.db.AdapterInsertCommandConnection(value);
                    }

                    if ((adapter.DeleteCommand != null))
                    {
                        this.db.AdapterDeleteCommandConnection(value);
                    }

                    if ((adapter.UpdateCommand != null))
                    {
                        this.db.AdapterUpdateCommandConnection(value);
                    }
                } // if
                else if( DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse )
                {
                    SqlDataAdapter adapter = (SqlDataAdapter)this.Adapter;

                    if( ( adapter.InsertCommand != null ) )
                    {
                        this.db.AdapterInsertCommandConnection( value );
                    }

                    if( ( adapter.DeleteCommand != null ) )
                    {
                        this.db.AdapterDeleteCommandConnection( value );
                    }

                    if( ( adapter.UpdateCommand != null ) )
                    {
                        this.db.AdapterUpdateCommandConnection( value );
                    }
                } // else if

                if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                {
                    MySqlCommand[] collect = (MySqlCommand[]) this.CommandCollection;

                    for (int i = 0; (i < this.CommandCollection.Length); i = (i + 1))
                    {
                        if ((this.CommandCollection[i] != null))
                        {
                            collect[i].Connection = (MySqlConnection) value;
                        } // if
                    } // for
                } // if
                else if(DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                {
                    SqlCommand[] collect = (SqlCommand[]) this.CommandCollection;

                    for (int i = 0; (i < this.CommandCollection.Length); i = (i + 1))
                    {
                        if ((this.CommandCollection[i] != null))
                        {
                            collect[i].Connection = (SqlConnection) value;
                        } // if
                    } // for
                }
            }
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        //protected global::MySql.Data.MySqlClient.MySqlCommand[] CommandCollection
        protected object[] CommandCollection
        {
            get
            {
                //if( ( this._commandCollection == null ) )
                if( ( this.db.CommandCollection == null ) )
                {
                    this.InitCommandCollection();
                }
                return this.db.CommandCollection;
            }
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public bool ClearBeforeFill
        {
            get
            {
                return this._clearBeforeFill;
            }
            set
            {
                this._clearBeforeFill = value;
            }
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        private void InitAdapter()
        {
            //this._adapter = new global::MySql.Data.MySqlClient.MySqlDataAdapter();
            this.db.NewDataAdapter();
            global::System.Data.Common.DataTableMapping tableMapping = new global::System.Data.Common.DataTableMapping();
            tableMapping.SourceTable = "Table";
            tableMapping.DataSetTable = "driver";
            tableMapping.ColumnMappings.Add( "id", "id" );
            tableMapping.ColumnMappings.Add( "Family", "Family" );
            tableMapping.ColumnMappings.Add( "Name", "Name" );
            tableMapping.ColumnMappings.Add( "ByrdDay", "ByrdDay" );
            tableMapping.ColumnMappings.Add( "Category", "Category" );
            tableMapping.ColumnMappings.Add( "Permis", "Permis" );
            tableMapping.ColumnMappings.Add( "foto", "foto" );
            tableMapping.ColumnMappings.Add( "Identifier", "Identifier" );
			tableMapping.ColumnMappings.Add( "numTelephone", "numTelephone" );
            //this._adapter.TableMappings.Add( tableMapping );
            this.db.AdapterTableMappingsAdd(tableMapping);

            //this._adapter.InsertCommand = new global::MySql.Data.MySqlClient.MySqlCommand();
            this.db.AdapterInsertCommand();
            //this._adapter.InsertCommand.Connection = this.Connection;
            this.db.AdapterInsertCommandConnection(this.Connection);
            //this._adapter.InsertCommand.UpdatedRowSource = System.Data.UpdateRowSource.FirstReturnedRecord;
            this.db.AdapterInsertCommandUpdatedRowSource( System.Data.UpdateRowSource.FirstReturnedRecord );
            //this._adapter.InsertCommand.CommandText =
            //  "INSERT INTO driver (Family, Name, ByrdDay, Category, Permis, foto, Identifier) \r\n" +
            //  "VALUES (?family, ?name, ?byrdDay, ?category, ?permis, ?foto, ?Identifier); " +
            //  "SELECT * FROM driver WHERE Id = LAST_INSERT_ID()";

            string sqlstr = TrackControlQuery.DriverTableAdapter.InsertIntoDriver;
           
            this.db.AdapterInsertCommandText(sqlstr);

            //this._adapter.InsertCommand.CommandType = global::System.Data.CommandType.Text;
            this.db.AdapterInsertCommandType( global::System.Data.CommandType.Text );
            //global::MySql.Data.MySqlClient.MySqlParameter param;// = new global::MySql.Data.MySqlClient.MySqlParameter();

            this.db.NewSqlParameter();
            //param.ParameterName = "?family";
            this.db.ParameterName( db.ParamPrefics + "family" );
            //param.DbType = global::System.Data.DbType.String;
            this.db.ParameterDbType( global::System.Data.DbType.String );
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.String;
            this.db.ParameterSqlDbType( db.GettingString() );
            //param.Size = 40;
            this.db.ParameterSize(40);
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "Family";
            this.db.ParameterSourceColumn( "Family" );
            //this._adapter.InsertCommand.Parameters.Add( param );
            this.db.AdapterInsertCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?name";
            this.db.ParameterName( db.ParamPrefics + "name" );
            //param.DbType = global::System.Data.DbType.String;
            this.db.ParameterDbType( global::System.Data.DbType.String );
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.String;
            this.db.ParameterSqlDbType( db.GettingString() );
            //param.Size = 40;
            this.db.ParameterSize(40);
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "Name";
            this.db.ParameterSourceColumn( "Name" );
            //this._adapter.InsertCommand.Parameters.Add( param );
            this.db.AdapterInsertCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?byrdDay";
            this.db.ParameterName( db.ParamPrefics + "byrdDay" );
            //param.DbType = global::System.Data.DbType.DateTime;
            this.db.ParameterDbType( global::System.Data.DbType.DateTime );
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Date;
            this.db.ParameterSqlDbType( db.GettingDate() );
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "ByrdDay";
            this.db.ParameterSourceColumn( "ByrdDay" );
            //this._adapter.InsertCommand.Parameters.Add( param );
            this.db.AdapterInsertCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?category";
            this.db.ParameterName( db.ParamPrefics + "category" );
            //param.DbType = global::System.Data.DbType.String;
            this.db.ParameterDbType( global::System.Data.DbType.String );
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.String;
            this.db.ParameterSqlDbType( db.GettingString() );
            //param.Size = 5;
            this.db.ParameterSize(5);
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "Category";
            this.db.ParameterSourceColumn( "Category" );
            //this._adapter.InsertCommand.Parameters.Add( param );
            this.db.AdapterInsertCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?permis";
            this.db.ParameterName( db.ParamPrefics + "permis" );
            //param.DbType = global::System.Data.DbType.String;
            this.db.ParameterDbType( global::System.Data.DbType.String );
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.String;
            this.db.ParameterSqlDbType( db.GettingString() );
            //param.Size = 20;
            this.db.ParameterSize(20);
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "Permis";
            this.db.ParameterSourceColumn( "Permis" );
            //this._adapter.InsertCommand.Parameters.Add( param );
            this.db.AdapterInsertCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?foto";
            this.db.ParameterName( db.ParamPrefics + "foto" );
            //param.DbType = global::System.Data.DbType.Binary;
            this.db.ParameterDbType( global::System.Data.DbType.Binary );
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Blob;
            this.db.ParameterSqlDbType( db.GettingBlob() );
            //param.Size = 2147483647;
            this.db.ParameterSize( 2147483647 );
            //param.IsNullable = true;
            this.db.ParameterIsNullable( true );
            //param.SourceColumn = "foto";
            this.db.ParameterSourceColumn( "foto" );
            //this._adapter.InsertCommand.Parameters.Add( param );
            this.db.AdapterInsertCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?Identifier";
            this.db.ParameterName( db.ParamPrefics + "Identifier" );
            //param.DbType = global::System.Data.DbType.UInt16;
            this.db.ParameterDbType( global::System.Data.DbType.Int16 );
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.UInt16;
            this.db.ParameterSqlDbType( db.GettingUInt16() );
            //param.IsNullable = true;
            this.db.ParameterIsNullable( true );
            //param.SourceColumn = "Identifier";
            this.db.ParameterSourceColumn( "Identifier" );
            //this._adapter.InsertCommand.Parameters.Add( param );
            this.db.AdapterInsertCommandParametersAdd();
 
            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?numTelephone";
            this.db.ParameterName( db.ParamPrefics + "numTelephone" );
            //param.DbType = global::System.Data.DbType.UInt16;
            this.db.ParameterDbType( global::System.Data.DbType.String );
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.UInt16;
            this.db.ParameterSqlDbType( db.GettingString() );
            this.db.ParameterSize(32);
            //param.IsNullable = true;
            this.db.ParameterIsNullable( false );
            //param.SourceColumn = "Identifier";
            this.db.ParameterSourceColumn( "numTelephone" );
            //this._adapter.InsertCommand.Parameters.Add( param );
            this.db.AdapterInsertCommandParametersAdd();

            //this._adapter.UpdateCommand = new global::MySql.Data.MySqlClient.MySqlCommand();
            this.db.AdapterUpdateCommand();
            //this._adapter.UpdateCommand.Connection = this.Connection;
            this.db.AdapterUpdateCommandConnection(this.Connection);
            //this._adapter.UpdateCommand.CommandText =
            //  "UPDATE driver SET Family = ?Family, Name = ?Name, ByrdDay = ?ByrdDay," +
            //  " Category = ?Category, Permis = ?Permis, foto = ?foto, Identifier = ?Identifier " +
            //  "WHERE id = ?id";
            string sqlstr1 = TrackControlQuery.DriverTableAdapter.UpdateDriverFamily;
          
            this.db.AdapterUpdateCommandText(sqlstr1);
            //this._adapter.UpdateCommand.CommandType = global::System.Data.CommandType.Text;
            this.db.AdapterUpdateCommandType( global::System.Data.CommandType.Text );

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?Family";
            this.db.ParameterName( db.ParamPrefics + "Family" );
            //param.DbType = global::System.Data.DbType.String;
            this.db.ParameterDbType( global::System.Data.DbType.String );
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.String;
            this.db.ParameterSqlDbType( db.GettingString() );
            //param.Size = 40;
            this.db.ParameterSize(40);
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "Family";
            this.db.ParameterSourceColumn( "Family" );
            //this._adapter.UpdateCommand.Parameters.Add( param );
            this.db.AdapterUpdateCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?Name";
            this.db.ParameterName( db.ParamPrefics + "Name" );
            //param.DbType = global::System.Data.DbType.String;
            this.db.ParameterDbType( global::System.Data.DbType.String );
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.String;
            this.db.ParameterSqlDbType( db.GettingString() );
            //param.Size = 40;
            this.db.ParameterSize(40);
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "Name";
            this.db.ParameterSourceColumn("Name");
            //this._adapter.UpdateCommand.Parameters.Add( param );
            this.db.AdapterUpdateCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?ByrdDay";
            this.db.ParameterName( db.ParamPrefics + "ByrdDay" );
            //param.DbType = global::System.Data.DbType.DateTime;
            this.db.ParameterDbType( global::System.Data.DbType.DateTime );
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Date;
            this.db.ParameterSqlDbType( db.GettingDate() );
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "ByrdDay";
            this.db.ParameterSourceColumn( "ByrdDay" );
            //this._adapter.UpdateCommand.Parameters.Add( param );
            this.db.AdapterUpdateCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?Category";
            this.db.ParameterName( db.ParamPrefics + "Category" );
            //param.DbType = global::System.Data.DbType.String;
            this.db.ParameterDbType( global::System.Data.DbType.String );
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.String;
            this.db.ParameterSqlDbType( db.GettingString() );
            //param.Size = 5;
            this.db.ParameterSize(5);
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "Category";
            this.db.ParameterSourceColumn( "Category" );
            //this._adapter.UpdateCommand.Parameters.Add( param );
            this.db.AdapterUpdateCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?Permis";
            this.db.ParameterName( db.ParamPrefics + "Permis" );
            //param.DbType = global::System.Data.DbType.String;
            this.db.ParameterDbType( global::System.Data.DbType.String );
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.String;
            this.db.ParameterSqlDbType( db.GettingString() );
            //param.Size = 20;
            this.db.ParameterSize(20);
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "Permis";
            this.db.ParameterSourceColumn( "Permis" );
            //this._adapter.UpdateCommand.Parameters.Add( param );
            this.db.AdapterUpdateCommandParametersAdd();

           //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?foto";
            this.db.ParameterName( db.ParamPrefics + "foto" );
            //param.DbType = global::System.Data.DbType.Binary;
            this.db.ParameterDbType( global::System.Data.DbType.Binary );
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Blob;
            this.db.ParameterSqlDbType( db.GettingBlob() );
            //param.Size = 2147483647;
            this.db.ParameterSize( 2147483647 );
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "foto";
            this.db.ParameterSourceColumn( "foto" );
            //this._adapter.UpdateCommand.Parameters.Add( param );
            this.db.AdapterUpdateCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?Identifier";
            this.db.ParameterName( db.ParamPrefics + "Identifier" );
            //param.DbType = global::System.Data.DbType.UInt16;
            this.db.ParameterDbType( global::System.Data.DbType.Int16 );
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.UInt16;
            this.db.ParameterSqlDbType( db.GettingUInt16() );
            //param.IsNullable = true;
            this.db.ParameterIsNullable( true );
            //param.SourceColumn = "Identifier";
            this.db.ParameterSourceColumn( "Identifier" );
            //this._adapter.UpdateCommand.Parameters.Add( param );
            this.db.AdapterUpdateCommandParametersAdd();

             //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?numTelephone";
            this.db.ParameterName( db.ParamPrefics + "numTelephone" );
            //param.DbType = global::System.Data.DbType.UInt16;
            this.db.ParameterDbType( global::System.Data.DbType.String );
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.UInt16;
            this.db.ParameterSqlDbType( db.GettingString() );
            this.db.ParameterSize(32);
            //param.IsNullable = true;
            this.db.ParameterIsNullable( false );
            //param.SourceColumn = "Identifier";
            this.db.ParameterSourceColumn( "numTelephone" );
            //this._adapter.UpdateCommand.Parameters.Add( param );
            this.db.AdapterUpdateCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?id";
            this.db.ParameterName( db.ParamPrefics + "id" );
            //param.DbType = global::System.Data.DbType.Int32;
            this.db.ParameterDbType( global::System.Data.DbType.Int32 );
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType( db.GettingInt32() );
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "id";
            this.db.ParameterSourceColumn( "id" );
            //param.SourceVersion = global::System.Data.DataRowVersion.Original;
            this.db.ParameterSourceVersion( global::System.Data.DataRowVersion.Original );
            //this._adapter.UpdateCommand.Parameters.Add( param );
            this.db.AdapterUpdateCommandParametersAdd();

            //this._adapter.DeleteCommand = new global::MySql.Data.MySqlClient.MySqlCommand();
            this.db.AdapterDeleteCommand();
            //this._adapter.DeleteCommand.Connection = this.Connection;
            this.db.AdapterDeleteCommandConnection(this.Connection);
            //this._adapter.DeleteCommand.CommandText = "DELETE FROM driver WHERE id = @Original_Id";
            this.db.AdapterDeleteCommandText( "DELETE FROM driver WHERE id = @Original_Id" );
            //this._adapter.DeleteCommand.CommandType = global::System.Data.CommandType.Text;
            this.db.AdapterDeleteCommandType( global::System.Data.CommandType.Text );
         //   param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "@Original_Id";
            this.db.ParameterName( "@Original_Id" );
            //param.DbType = global::System.Data.DbType.Int32;
            this.db.ParameterDbType( global::System.Data.DbType.Int32 );
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType( db.GettingInt32() );
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "id";
            this.db.ParameterSourceColumn( "id" );
            //param.SourceVersion = global::System.Data.DataRowVersion.Original;
            this.db.ParameterSourceVersion( global::System.Data.DataRowVersion.Original );
            //this._adapter.DeleteCommand.Parameters.Add( param );
            this.db.AdapterDeleteCommandParametersAdd();
        } // InitAdapter

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        private void InitConnection()
        {
            //this._connection = new global::MySql.Data.MySqlClient.MySqlConnection();
            this.db.AdapNewConnection();
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        private void InitCommandCollection()
        {
            //this._commandCollection = new global::MySql.Data.MySqlClient.MySqlCommand[1];
            this.db.NewCommandCollection(1);
            //this._commandCollection[0] = new global::MySql.Data.MySqlClient.MySqlCommand();
            this.db.SetNewCommandCollection(0);
            //this._commandCollection[0].Connection = this.Connection;
            this.db.SetCommandCollectionConnection(this.Connection, 0);
            //this._commandCollection[0].CommandText = "SELECT * FROM  driver";
            this.db.SetCommandCollectionText( "SELECT * FROM  driver", 0);
            //this._commandCollection[0].CommandType = global::System.Data.CommandType.Text;
            this.db.SetCommandCollectionType( global::System.Data.CommandType.Text, 0);
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.ComponentModel.Design.HelpKeywordAttribute( "vs.data.TableAdapter" )]
        [global::System.ComponentModel.DataObjectMethodAttribute( global::System.ComponentModel.DataObjectMethodType.Fill, true )]
        public virtual int Fill( atlantaDataSet.driverDataTable dataTable )
        {
            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;
                adapter.SelectCommand = (MySqlCommand) this.CommandCollection[0];
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                SqlDataAdapter adapter = (SqlDataAdapter) this.Adapter;
                adapter.SelectCommand = (SqlCommand) this.CommandCollection[0];
            }

            if( ( this.ClearBeforeFill == true ) )
            {
                dataTable.Clear();
            }

            int returnValue = 0;

            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;
                returnValue = adapter.Fill(dataTable);
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                SqlDataAdapter adapter = (SqlDataAdapter)this.Adapter;
                returnValue = adapter.Fill( dataTable );
            }

            return returnValue;
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.ComponentModel.Design.HelpKeywordAttribute( "vs.data.TableAdapter" )]
        [global::System.ComponentModel.DataObjectMethodAttribute( global::System.ComponentModel.DataObjectMethodType.Select, true )]
        public virtual atlantaDataSet.driverDataTable GetData()
        {
            if( DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse )
            {
                MySqlDataAdapter adapter = (MySqlDataAdapter)this.Adapter;
                adapter.SelectCommand = (MySqlCommand)this.CommandCollection[0];
            }
            else if( DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse )
            {
                SqlDataAdapter adapter = (SqlDataAdapter)this.Adapter;
                adapter.SelectCommand = (SqlCommand)this.CommandCollection[0];
            }

            atlantaDataSet.driverDataTable dataTable = new atlantaDataSet.driverDataTable();

            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                MySqlDataAdapter adapter = (MySqlDataAdapter)this.Adapter;
                adapter.Fill(dataTable);
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                SqlDataAdapter adapter = (SqlDataAdapter)this.Adapter;
                adapter.Fill( dataTable );
            }
            return dataTable;
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.ComponentModel.Design.HelpKeywordAttribute( "vs.data.TableAdapter" )]
        public virtual int Update( atlantaDataSet.driverDataTable dataTable )
        {
            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                MySqlDataAdapter adapter = (MySqlDataAdapter)this.Adapter;
                return adapter.Update(dataTable);
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                SqlDataAdapter adapter = (SqlDataAdapter) this.Adapter;
                return adapter.Update( dataTable );
            }

            return 0;
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.ComponentModel.Design.HelpKeywordAttribute( "vs.data.TableAdapter" )]
        public virtual int Update( atlantaDataSet dataSet )
        {
            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;
                return adapter.Update(dataSet, "driver");
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                SqlDataAdapter adapter = (SqlDataAdapter) this.Adapter;
                return adapter.Update( dataSet, "driver" );
            }

            return 0;
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.ComponentModel.Design.HelpKeywordAttribute( "vs.data.TableAdapter" )]
        public virtual int Update( global::System.Data.DataRow dataRow )
        {
            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;
                return adapter.Update(new global::System.Data.DataRow[] {dataRow});
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                SqlDataAdapter adapter = (SqlDataAdapter) this.Adapter;
                return adapter.Update( new global::System.Data.DataRow[] { dataRow } );
            }

            return 0;
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.ComponentModel.Design.HelpKeywordAttribute( "vs.data.TableAdapter" )]
        public virtual int Update( global::System.Data.DataRow[] dataRows )
        {
            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;
                return adapter.Update(dataRows);
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                SqlDataAdapter adapter = (SqlDataAdapter) this.Adapter;
                return adapter.Update(dataRows);
            }

            return 0;
        }
    }
}
