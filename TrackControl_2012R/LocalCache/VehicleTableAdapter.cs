using System;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using TrackControl.General.DAL;
using TrackControl.General.DatabaseDriver;

namespace LocalCache.atlantaDataSetTableAdapters
{
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Design.TypedDataSetGenerator", "2.0.0.0")]
    [global::System.ComponentModel.DesignerCategoryAttribute("code")]
    [global::System.ComponentModel.ToolboxItem(true)]
    [global::System.ComponentModel.DataObjectAttribute(true)]
    [global::System.ComponentModel.DesignerAttribute(
        "Microsoft.VSDesigner.DataSource.Design.TableAdapterDesigner, Microsoft.VSDesigner" +
        ", Version=8.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")]
    [global::System.ComponentModel.Design.HelpKeywordAttribute("vs.data.TableAdapter")]
    public class VehicleTableAdapter : global::System.ComponentModel.Component
    {
        //private global::MySql.Data.MySqlClient.MySqlDataAdapter _adapter;
        //private global::MySql.Data.MySqlClient.MySqlConnection _connection;
        //private global::MySql.Data.MySqlClient.MySqlCommand[] _commandCollection;

        private DriverDb db = new DriverDb();

        private bool _clearBeforeFill;

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public VehicleTableAdapter()
        {
            this.ClearBeforeFill = true;
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        //private global::MySql.Data.MySqlClient.MySqlDataAdapter Adapter
        private object Adapter
        {
            get
            {
                if ((this.db.Adapter == null))
                {
                    this.InitAdapter();
                }

                return this.db.Adapter;
            }
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        //public global::MySql.Data.MySqlClient.MySqlConnection Connection
        public object Connection
        {
            get
            {
                if ((this.db.Connection == null))
                {
                    this.InitConnection();
                }

                return this.db.Connection;
            }
            set
            {
                this.db.Connection = value;

                if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                {
                    MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;

                    if ((adapter.InsertCommand != null))
                    {
                        adapter.InsertCommand.Connection = (MySqlConnection) value;
                    }

                    if ((adapter.DeleteCommand != null))
                    {
                        adapter.DeleteCommand.Connection = (MySqlConnection) value;
                    }

                    if ((adapter.UpdateCommand != null))
                    {
                        adapter.UpdateCommand.Connection = (MySqlConnection) value;
                    }

                    MySqlCommand[] collection = (MySqlCommand[]) this.CommandCollection;

                    for (int i = 0; (i < this.CommandCollection.Length); i = (i + 1))
                    {
                        if ((this.CommandCollection[i] != null))
                        {
                            collection[i].Connection = (MySqlConnection) value;
                        }
                    } // for
                } // if
                if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                {
                    SqlDataAdapter adapter = (SqlDataAdapter) this.Adapter;

                    if ((adapter.InsertCommand != null))
                    {
                        adapter.InsertCommand.Connection = (SqlConnection) value;
                    }

                    if ((adapter.DeleteCommand != null))
                    {
                        adapter.DeleteCommand.Connection = (SqlConnection) value;
                    }

                    if ((adapter.UpdateCommand != null))
                    {
                        adapter.UpdateCommand.Connection = (SqlConnection) value;
                    }

                    SqlCommand[] collection = (SqlCommand[]) this.CommandCollection;

                    for (int i = 0; (i < this.CommandCollection.Length); i = (i + 1))
                    {
                        if ((this.CommandCollection[i] != null))
                        {
                            collection[i].Connection = (SqlConnection) value;
                        }
                    } // for
                } // else if
            } // set
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        //protected global::MySql.Data.MySqlClient.MySqlCommand[] CommandCollection
        protected object[] CommandCollection
        {
            get
            {
                if ((this.db.CommandCollection == null))
                {
                    this.InitCommandCollection();
                }

                return this.db.CommandCollection;
            }
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public bool ClearBeforeFill
        {
            get { return this._clearBeforeFill; }
            set { this._clearBeforeFill = value; }
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        private void InitAdapter()
        {
            //this._adapter = new global::MySql.Data.MySqlClient.MySqlDataAdapter();
            this.db.NewDataAdapter();
            global::System.Data.Common.DataTableMapping tableMapping = new global::System.Data.Common.DataTableMapping();
            tableMapping.SourceTable = "Table";
            tableMapping.DataSetTable = "vehicle";
            tableMapping.ColumnMappings.Add("id", "id");
            tableMapping.ColumnMappings.Add("MakeCar", "MakeCar");
            tableMapping.ColumnMappings.Add("NumberPlate", "NumberPlate");
            tableMapping.ColumnMappings.Add("Team_id", "Team_id");
            tableMapping.ColumnMappings.Add("Mobitel_id", "Mobitel_id");
            tableMapping.ColumnMappings.Add("CarModel", "CarModel");
            tableMapping.ColumnMappings.Add("setting_id", "setting_id");
            tableMapping.ColumnMappings.Add("driver_id", "driver_id");
            tableMapping.ColumnMappings.Add("OutLinkId", "OutLinkId");

            tableMapping.ColumnMappings.Add("FuelWayLiter", "FuelWayLiter");
            tableMapping.ColumnMappings.Add("FuelMotorLiter", "FuelMotorLiter");
            tableMapping.ColumnMappings.Add("Identifier", "Identifier");
            tableMapping.ColumnMappings.Add("Category_id", "Category_id");
            tableMapping.ColumnMappings.Add("Category_id2", "Category_id2");
            tableMapping.ColumnMappings.Add( "PcbVersionId", "PcbVersionId" );
            
            //this._adapter.TableMappings.Add(tableMapping);
            this.db.AdapterTableMappingsAdd(tableMapping);

            //this._adapter.InsertCommand = new global::MySql.Data.MySqlClient.MySqlCommand();
            this.db.AdapterInsertCommand();
            //this._adapter.InsertCommand.Connection = this.Connection;
            this.db.AdapterInsertCommandConnection(this.Connection);
            //this._adapter.InsertCommand.UpdatedRowSource = System.Data.UpdateRowSource.FirstReturnedRecord;
            this.db.AdapterInsertCommandUpdatedRowSource(System.Data.UpdateRowSource.FirstReturnedRecord);
            //this._adapter.InsertCommand.CommandText = 
            //  "INSERT INTO vehicle (MakeCar, NumberPlate, Team_id, Mobitel_id, " +
            //  " CarModel, setting_id, driver_id,OutLinkId) " +
            //  "VALUES (?MakeCar, ?NumberPlate, ?Team_id, ?Mobitel_id, ?CarModel," +
            //  " ?setting_id, ?driver_id,?OutLinkId); " +
            //  "SELECT * FROM vehicle WHERE Id = LAST_INSERT_ID()";

            string sqlstr = TrackControlQuery.VehicleTableAdapter.InsertIntoVehicle;

            this.db.AdapterInsertCommandText(sqlstr);

            //this._adapter.InsertCommand.CommandType = global::System.Data.CommandType.Text;
            this.db.AdapterInsertCommandType(global::System.Data.CommandType.Text);

            //global::MySql.Data.MySqlClient.MySqlParameter param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?MakeCar";
            this.db.ParameterName(db.ParamPrefics + "MakeCar");
            //param.DbType = global::System.Data.DbType.String;
            this.db.ParameterDbType(global::System.Data.DbType.String);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.String;
            this.db.ParameterSqlDbType(db.GettingString());
            //param.Size = 40;
            this.db.ParameterSize(128);
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "MakeCar";
            this.db.ParameterSourceColumn("MakeCar");
            //this._adapter.InsertCommand.Parameters.Add(param);
            this.db.AdapterInsertCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?NumberPlate";
            this.db.ParameterName(db.ParamPrefics + "NumberPlate");
            //param.DbType = global::System.Data.DbType.String;
            this.db.ParameterDbType(global::System.Data.DbType.String);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.String;
            this.db.ParameterSqlDbType(db.GettingString());
            //param.Size = 20;
            this.db.ParameterSize(128);
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "NumberPlate";
            this.db.ParameterSourceColumn("NumberPlate");
            //this._adapter.InsertCommand.Parameters.Add(param);
            this.db.AdapterInsertCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?Team_id";
            this.db.ParameterName(db.ParamPrefics + "Team_id");
            //param.DbType = global::System.Data.DbType.Int32;
            this.db.ParameterDbType(global::System.Data.DbType.Int32);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType(db.GettingInt32());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "Team_id";
            this.db.ParameterSourceColumn("Team_id");
            //this._adapter.InsertCommand.Parameters.Add(param);
            this.db.AdapterInsertCommandParametersAdd();

            // param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?Mobitel_id";
            this.db.ParameterName(db.ParamPrefics + "Mobitel_id");
            //param.DbType = global::System.Data.DbType.Int32;
            this.db.ParameterDbType(global::System.Data.DbType.Int32);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType(db.GettingInt32());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "Mobitel_id";
            this.db.ParameterSourceColumn("Mobitel_id");
            //this._adapter.InsertCommand.Parameters.Add(param);
            this.db.AdapterInsertCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?CarModel";
            this.db.ParameterName(db.ParamPrefics + "CarModel");
            //param.DbType = global::System.Data.DbType.String;
            this.db.ParameterDbType(global::System.Data.DbType.String);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.String;
            this.db.ParameterSqlDbType(db.GettingString());
            //param.Size = 20;
            this.db.ParameterSize(128);
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "CarModel";
            this.db.ParameterSourceColumn("CarModel");
            //this._adapter.InsertCommand.Parameters.Add(param);
            this.db.AdapterInsertCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?setting_id";
            this.db.ParameterName(db.ParamPrefics + "setting_id");
            //param.DbType = global::System.Data.DbType.Int32;
            this.db.ParameterDbType(global::System.Data.DbType.Int32);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType(db.GettingInt32());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "setting_id";
            this.db.ParameterSourceColumn("setting_id");
            //this._adapter.InsertCommand.Parameters.Add(param);
            this.db.AdapterInsertCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?driver_id";
            this.db.ParameterName(db.ParamPrefics + "driver_id");
            //param.DbType = global::System.Data.DbType.Int32;
            this.db.ParameterDbType(global::System.Data.DbType.Int32);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType(db.GettingInt32());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "driver_id";
            this.db.ParameterSourceColumn("driver_id");
            //this._adapter.InsertCommand.Parameters.Add(param);
            this.db.AdapterInsertCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?OutLinkId";
            this.db.ParameterName(db.ParamPrefics + "OutLinkId");
            //param.DbType = global::System.Data.DbType.String;
            this.db.ParameterDbType(global::System.Data.DbType.String);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.String;
            this.db.ParameterSqlDbType(db.GettingString());
            //param.Size = 20;
            this.db.ParameterSize(128);
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "OutLinkId";
            this.db.ParameterSourceColumn("OutLinkId");
            //this._adapter.InsertCommand.Parameters.Add(param);
            this.db.AdapterInsertCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?FuelWayLiter";
            this.db.ParameterName(db.ParamPrefics + "FuelWayLiter");
            //param.DbType = global::System.Data.DbType.String;
            this.db.ParameterDbType(global::System.Data.DbType.Double);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.String;
            this.db.ParameterSqlDbType(db.GettingDouble());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "FuelWayLiter";
            this.db.ParameterSourceColumn("FuelWayLiter");
            //this._adapter.InsertCommand.Parameters.Add(param);
            this.db.AdapterInsertCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?FuelMotorLiter";
            this.db.ParameterName(db.ParamPrefics + "FuelMotorLiter");
            //param.DbType = global::System.Data.DbType.String;
            this.db.ParameterDbType(global::System.Data.DbType.Double);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.String;
            this.db.ParameterSqlDbType(db.GettingDouble());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "FuelWayLiter";
            this.db.ParameterSourceColumn("FuelMotorLiter");
            //this._adapter.InsertCommand.Parameters.Add(param);
            this.db.AdapterInsertCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?Team_id";
            this.db.ParameterName(db.ParamPrefics + "Category_id");
            //param.DbType = global::System.Data.DbType.Int32;
            this.db.ParameterDbType(global::System.Data.DbType.Int32);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType(db.GettingInt32());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "Team_id";
            this.db.ParameterSourceColumn("Category_id");
            //this._adapter.InsertCommand.Parameters.Add(param);
            this.db.AdapterInsertCommandParametersAdd();

            this.db.NewSqlParameter();
            this.db.ParameterName(db.ParamPrefics + "Identifier");
            this.db.ParameterDbType(global::System.Data.DbType.Int32);
            this.db.ParameterSqlDbType(db.GettingInt32());
            this.db.ParameterIsNullable(true);
            this.db.ParameterSourceColumn("Identifier");
            this.db.AdapterInsertCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?Team_id";
            this.db.ParameterName(db.ParamPrefics + "Category_id2");
            //param.DbType = global::System.Data.DbType.Int32;
            this.db.ParameterDbType(global::System.Data.DbType.Int32);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType(db.GettingInt32());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "Team_id";
            this.db.ParameterSourceColumn("Category_id2");
            //this._adapter.InsertCommand.Parameters.Add(param);
            this.db.AdapterInsertCommandParametersAdd();

            this.db.NewSqlParameter();
            this.db.ParameterName( db.ParamPrefics + "Category_id3" );
            this.db.ParameterDbType( global::System.Data.DbType.Int32 );
            this.db.ParameterSqlDbType( db.GettingInt32() );
            this.db.ParameterIsNullable( true );
            this.db.ParameterSourceColumn( "Category_id3" );
            this.db.AdapterInsertCommandParametersAdd();

            this.db.NewSqlParameter();
            this.db.ParameterName( db.ParamPrefics + "Category_id4" );
            this.db.ParameterDbType( global::System.Data.DbType.Int32 );
            this.db.ParameterSqlDbType( db.GettingInt32() );
            this.db.ParameterIsNullable( true );
            this.db.ParameterSourceColumn( "Category_id4" );
            this.db.AdapterInsertCommandParametersAdd();
            
            this.db.NewSqlParameter();
            this.db.ParameterName( db.ParamPrefics + "PcbVersionId" );
            this.db.ParameterDbType( global::System.Data.DbType.Int32 );
            this.db.ParameterSqlDbType( db.GettingInt32() );
            this.db.ParameterIsNullable( true );
            this.db.ParameterSourceColumn( "PcbVersionId" );
            this.db.AdapterInsertCommandParametersAdd();

            //this._adapter.UpdateCommand = new global::MySql.Data.MySqlClient.MySqlCommand();
            this.db.AdapterUpdateCommand();
            //this._adapter.UpdateCommand.Connection = this.Connection;
            this.db.AdapterUpdateCommandConnection(this.Connection);
            //this._adapter.UpdateCommand.CommandText = 
            //  "UPDATE vehicle SET MakeCar = ?MakeCar, NumberPlate = ?NumberPlate," +
            //  " Team_id = ?Team_id, Mobitel_id = ?Mobitel_id," +
            //  " CarModel = ?CarModel, setting_id = ?setting_id, driver_id = ?driver_id,OutLinkId = ?OutLinkId " +
            //  "WHERE id = ?id";

            string sqlstr1 = TrackControlQuery.VehicleTableAdapter.UpdateVehicle;

            this.db.AdapterUpdateCommandText(sqlstr1);

            //this._adapter.UpdateCommand.CommandType = global::System.Data.CommandType.Text;
            this.db.AdapterUpdateCommandType(global::System.Data.CommandType.Text);

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?MakeCar";
            this.db.ParameterName(db.ParamPrefics + "MakeCar");
            //param.DbType = global::System.Data.DbType.String;
            this.db.ParameterDbType(global::System.Data.DbType.String);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.String;
            this.db.ParameterSqlDbType(db.GettingString());
            //param.Size = 40;
            this.db.ParameterSize(128);
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "MakeCar";
            this.db.ParameterSourceColumn("MakeCar");
            //this._adapter.UpdateCommand.Parameters.Add(param);
            this.db.AdapterUpdateCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?NumberPlate";
            this.db.ParameterName(db.ParamPrefics + "NumberPlate");
            //param.DbType = global::System.Data.DbType.String;
            this.db.ParameterDbType(global::System.Data.DbType.String);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.String;
            this.db.ParameterSqlDbType(db.GettingString());
            //param.Size = 20;
            this.db.ParameterSize(128);
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "NumberPlate";
            this.db.ParameterSourceColumn("NumberPlate");
            //this._adapter.UpdateCommand.Parameters.Add(param);
            this.db.AdapterUpdateCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?Team_id";
            this.db.ParameterName(db.ParamPrefics + "Team_id");
            //param.DbType = global::System.Data.DbType.Int32;
            this.db.ParameterDbType(global::System.Data.DbType.Int32);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType(db.GettingInt32());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "Team_id";
            this.db.ParameterSourceColumn("Team_id");
            //this._adapter.UpdateCommand.Parameters.Add(param);
            this.db.AdapterUpdateCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?Mobitel_id";
            this.db.ParameterName(db.ParamPrefics + "Mobitel_id");
            //param.DbType = global::System.Data.DbType.Int32;
            this.db.ParameterDbType(global::System.Data.DbType.Int32);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType(db.GettingInt32());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "Mobitel_id";
            this.db.ParameterSourceColumn("Mobitel_id");
            //this._adapter.UpdateCommand.Parameters.Add(param);
            this.db.AdapterUpdateCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?CarModel";
            this.db.ParameterName(db.ParamPrefics + "CarModel");
            //param.DbType = global::System.Data.DbType.String;
            this.db.ParameterDbType(global::System.Data.DbType.String);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.String;
            this.db.ParameterSqlDbType(db.GettingString());
            //param.Size = 20;
            this.db.ParameterSize(128);
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "CarModel";
            this.db.ParameterSourceColumn("CarModel");
            //this._adapter.UpdateCommand.Parameters.Add(param);
            this.db.AdapterUpdateCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?setting_id";
            this.db.ParameterName(db.ParamPrefics + "setting_id");
            //param.DbType = global::System.Data.DbType.Int32;
            this.db.ParameterDbType(global::System.Data.DbType.Int32);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType(db.GettingInt32());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "setting_id";
            this.db.ParameterSourceColumn("setting_id");
            //this._adapter.UpdateCommand.Parameters.Add(param);
            this.db.AdapterUpdateCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?driver_id";
            this.db.ParameterName(db.ParamPrefics + "driver_id");
            //param.DbType = global::System.Data.DbType.Int32;
            this.db.ParameterDbType(global::System.Data.DbType.Int32);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType(db.GettingInt32());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "driver_id";
            this.db.ParameterSourceColumn("driver_id");
            //this._adapter.UpdateCommand.Parameters.Add(param);
            this.db.AdapterUpdateCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?OutLinkId";
            this.db.ParameterName(db.ParamPrefics + "OutLinkId");
            //param.DbType = global::System.Data.DbType.String;
            this.db.ParameterDbType(global::System.Data.DbType.String);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.String;
            this.db.ParameterSqlDbType(db.GettingString());
            //param.Size = 20;
            this.db.ParameterSize(128);
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "OutLinkId";
            this.db.ParameterSourceColumn("OutLinkId");
            //this._adapter.UpdateCommand.Parameters.Add(param);
            this.db.AdapterUpdateCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?FuelWayLiter";
            this.db.ParameterName(db.ParamPrefics + "FuelWayLiter");
            //param.DbType = global::System.Data.DbType.Double;
            this.db.ParameterDbType(global::System.Data.DbType.Double);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Double;
            this.db.ParameterSqlDbType(db.GettingDouble());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "FuelWayLiter";
            this.db.ParameterSourceColumn("FuelWayLiter");
            //this._adapter.UpdateCommand.Parameters.Add(param);
            this.db.AdapterUpdateCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?FuelMotorLiter";
            this.db.ParameterName(db.ParamPrefics + "FuelMotorLiter");
            //param.DbType = global::System.Data.DbType.Double;
            this.db.ParameterDbType(global::System.Data.DbType.Double);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Double;
            this.db.ParameterSqlDbType(db.GettingDouble());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "FuelMotorLiter";
            this.db.ParameterSourceColumn("FuelMotorLiter");
            //this._adapter.UpdateCommand.Parameters.Add(param);
            this.db.AdapterUpdateCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?Team_id";
            this.db.ParameterName(db.ParamPrefics + "Category_id");
            //param.DbType = global::System.Data.DbType.Int32;
            this.db.ParameterDbType(global::System.Data.DbType.Int32);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType(db.GettingInt32());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "Team_id";
            this.db.ParameterSourceColumn("Category_id");
            //this._adapter.UpdateCommand.Parameters.Add(param);
            this.db.AdapterUpdateCommandParametersAdd();

            this.db.NewSqlParameter();
            this.db.ParameterName(db.ParamPrefics + "Identifier");
            this.db.ParameterDbType(global::System.Data.DbType.Int32);
            this.db.ParameterSqlDbType(db.GettingInt32());
            this.db.ParameterIsNullable(true);
            this.db.ParameterSourceColumn("Identifier");
            this.db.AdapterUpdateCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?Team_id";
            this.db.ParameterName(db.ParamPrefics + "Category_id2");
            //param.DbType = global::System.Data.DbType.Int32;
            this.db.ParameterDbType(global::System.Data.DbType.Int32);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType(db.GettingInt32());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "Team_id";
            this.db.ParameterSourceColumn("Category_id2");
            //this._adapter.UpdateCommand.Parameters.Add(param);
            this.db.AdapterUpdateCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?id";
            this.db.ParameterName(db.ParamPrefics + "id");
            //param.DbType = global::System.Data.DbType.Int32;
            this.db.ParameterDbType(global::System.Data.DbType.Int32);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType(db.GettingInt32());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "id";
            this.db.ParameterSourceColumn("id");
            //param.SourceVersion = global::System.Data.DataRowVersion.Original;
            this.db.ParameterSourceVersion(global::System.Data.DataRowVersion.Original);
            //this._adapter.UpdateCommand.Parameters.Add(param);
            this.db.AdapterUpdateCommandParametersAdd();

            this.db.NewSqlParameter();
            this.db.ParameterName( db.ParamPrefics + "PcbVersionId" );
            this.db.ParameterDbType( global::System.Data.DbType.Int32 );
            this.db.ParameterSqlDbType( db.GettingInt32() );
            this.db.ParameterIsNullable( true );
            this.db.ParameterSourceColumn( "PcbVersionId" );
            this.db.ParameterSourceVersion( global::System.Data.DataRowVersion.Original );
            this.db.AdapterUpdateCommandParametersAdd();

            this.db.NewSqlParameter();
            this.db.ParameterName( db.ParamPrefics + "Category_id3" );
            this.db.ParameterDbType( global::System.Data.DbType.Int32 );
            this.db.ParameterSqlDbType( db.GettingInt32() );
            this.db.ParameterIsNullable( true );
            this.db.ParameterSourceColumn( "Category_id3" );
            this.db.AdapterUpdateCommandParametersAdd();

            this.db.NewSqlParameter();
            this.db.ParameterName( db.ParamPrefics + "Category_id4" );
            this.db.ParameterDbType( global::System.Data.DbType.Int32 );
            this.db.ParameterSqlDbType( db.GettingInt32() );
            this.db.ParameterIsNullable( true );
            this.db.ParameterSourceColumn( "Category_id4" );
            this.db.AdapterUpdateCommandParametersAdd();
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        private void InitConnection()
        {
            //this._connection = new global::MySql.Data.MySqlClient.MySqlConnection();
            this.db.AdapNewConnection();
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        private void InitCommandCollection()
        {
            //this._commandCollection = new global::MySql.Data.MySqlClient.MySqlCommand[1];
            this.db.NewCommandCollection(1);
            //this._commandCollection[0] = new global::MySql.Data.MySqlClient.MySqlCommand();
            this.db.SetNewCommandCollection(0);
            //this._commandCollection[0].Connection = this.Connection;
            this.db.SetCommandCollectionConnection(this.Connection, 0);
            //this._commandCollection[0].CommandText = "SELECT id, MakeCar, NumberPlate, Team_id, Mobitel_id, CarModel, setting_id, driver_id,OutLinkId FROM vehicle";
            this.db.SetCommandCollectionText(TrackControlQuery.VehicleTableAdapter.SelectFromVehicle, 0);
            //this._commandCollection[0].CommandType = global::System.Data.CommandType.Text;
            this.db.SetCommandCollectionType(global::System.Data.CommandType.Text, 0);
        }

        //[global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.ComponentModel.Design.HelpKeywordAttribute("vs.data.TableAdapter")]
        [global::System.ComponentModel.DataObjectMethodAttribute(global::System.ComponentModel.DataObjectMethodType.Fill,
            true)]
        public virtual int Fill(atlantaDataSet.vehicleDataTable dataTable)
        {
            try
            {
                if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                {
                    MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;

                    adapter.SelectCommand = (MySqlCommand) this.CommandCollection[0];
                }
                else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                {
                    SqlDataAdapter adapter = (SqlDataAdapter) this.Adapter;

                    adapter.SelectCommand = (SqlCommand) this.CommandCollection[0];
                }

                if ((this.ClearBeforeFill == true))
                {
                    dataTable.Clear();
                }
                int returnValue = 0; // adapter.Fill( dataTable );

                if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                {
                    MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;
                    dataTable.DataSet.EnforceConstraints = false;

                    returnValue = adapter.Fill(dataTable);
                }
                else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                {
                    SqlDataAdapter adapter = (SqlDataAdapter) this.Adapter;
                    dataTable.DataSet.EnforceConstraints = false;

                    returnValue = adapter.Fill(dataTable);
                }
                return returnValue;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message + " " + e.InnerException);
            }
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.ComponentModel.Design.HelpKeywordAttribute("vs.data.TableAdapter")]
        [global::System.ComponentModel.DataObjectMethodAttribute(global::System.ComponentModel.DataObjectMethodType.Select, true)]
        public virtual atlantaDataSet.vehicleDataTable GetData()
        {
            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;
                adapter.SelectCommand = (MySqlCommand) this.CommandCollection[0];
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                SqlDataAdapter adapter = (SqlDataAdapter) this.Adapter;
                adapter.SelectCommand = (SqlCommand) this.CommandCollection[0];
            }

            atlantaDataSet.vehicleDataTable dataTable = new atlantaDataSet.vehicleDataTable();

            //adapter.Fill(dataTable);
            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;

                adapter.Fill(dataTable);
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                SqlDataAdapter adapter = (SqlDataAdapter) this.Adapter;

                adapter.Fill(dataTable);
            }
            return dataTable;
        }

        //[global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.ComponentModel.Design.HelpKeywordAttribute("vs.data.TableAdapter")]
        public virtual int Update(atlantaDataSet.vehicleDataTable dataTable)
        {
            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;

                return adapter.Update(dataTable);
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                SqlDataAdapter adapter = (SqlDataAdapter) this.Adapter;

                return adapter.Update(dataTable);
            }

            return 0;
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.ComponentModel.Design.HelpKeywordAttribute("vs.data.TableAdapter")]
        public virtual int Update(atlantaDataSet dataSet)
        {
            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;

                return adapter.Update(dataSet, "vehicle");
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                SqlDataAdapter adapter = (SqlDataAdapter) this.Adapter;

                return adapter.Update(dataSet, "vehicle");
            }

            return 0;
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.ComponentModel.Design.HelpKeywordAttribute("vs.data.TableAdapter")]
        public virtual int Update(global::System.Data.DataRow dataRow)
        {
            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;

                return adapter.Update(new global::System.Data.DataRow[] {dataRow});
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                SqlDataAdapter adapter = (SqlDataAdapter) this.Adapter;

                return adapter.Update(new global::System.Data.DataRow[] {dataRow});
            }

            return 0;
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.ComponentModel.Design.HelpKeywordAttribute("vs.data.TableAdapter")]
        public virtual int Update(global::System.Data.DataRow[] dataRows)
        {
            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;

                return adapter.Update(dataRows);
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                SqlDataAdapter adapter = (SqlDataAdapter) this.Adapter;

                return adapter.Update(dataRows);
            }

            return 0;
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.ComponentModel.Design.HelpKeywordAttribute("vs.data.TableAdapter")]
        [global::System.ComponentModel.DataObjectMethodAttribute(
            global::System.ComponentModel.DataObjectMethodType.Insert, true)]
        public virtual int Insert(string MakeCar, string NumberPlate, global::System.Nullable<int> Team_id,
            global::System.Nullable<int> Mobitel_id, string CarModel,
            global::System.Nullable<int> setting_id, global::System.Nullable<int> driver_id,
            string IdOutLink, double FuelWayLiter, double FuelMotorLiter,
            global::System.Nullable<int> Category_id, int Identifier, global::System.Nullable<int> Category_id2, int PcbVersionId, global::System.Nullable<int> Category_id3, global::System.Nullable<int> Category_id4 )
        {
            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;

                if ((MakeCar == null))
                {
                    adapter.InsertCommand.Parameters[0].Value = global::System.DBNull.Value;
                }
                else
                {
                    adapter.InsertCommand.Parameters[0].Value = ((string) (MakeCar));
                }
                if ((NumberPlate == null))
                {
                    adapter.InsertCommand.Parameters[1].Value = global::System.DBNull.Value;
                }
                else
                {
                    adapter.InsertCommand.Parameters[1].Value = ((string) (NumberPlate));
                }
                if ((Team_id.HasValue == true))
                {
                    adapter.InsertCommand.Parameters[2].Value = ((int) (Team_id.Value));
                }
                else
                {
                    adapter.InsertCommand.Parameters[2].Value = global::System.DBNull.Value;
                }
                if ((Mobitel_id.HasValue == true))
                {
                    adapter.InsertCommand.Parameters[3].Value = ((int) (Mobitel_id.Value));
                }
                else
                {
                    adapter.InsertCommand.Parameters[3].Value = global::System.DBNull.Value;
                }
                if ((CarModel == null))
                {
                    adapter.InsertCommand.Parameters[4].Value = global::System.DBNull.Value;
                }
                else
                {
                    adapter.InsertCommand.Parameters[4].Value = ((string) (CarModel));
                }
                if ((setting_id.HasValue == true))
                {
                    adapter.InsertCommand.Parameters[5].Value = ((int) (setting_id.Value));
                }
                else
                {
                    adapter.InsertCommand.Parameters[5].Value = global::System.DBNull.Value;
                }
                if ((driver_id.HasValue == true))
                {
                    adapter.InsertCommand.Parameters[6].Value = ((int) (driver_id.Value));
                }
                else
                {
                    adapter.InsertCommand.Parameters[6].Value = global::System.DBNull.Value;
                }
                if ((IdOutLink == null))
                {
                    adapter.InsertCommand.Parameters[7].Value = global::System.DBNull.Value;
                }
                else
                {
                    adapter.InsertCommand.Parameters[7].Value = ((string) (IdOutLink));
                }
                if ((FuelWayLiter == 0.0))
                {
                    adapter.InsertCommand.Parameters[8].Value = 0.0;
                }
                else
                {
                    adapter.InsertCommand.Parameters[8].Value = ((double) (FuelWayLiter));
                }
                if ((FuelMotorLiter == 0.0))
                {
                    adapter.InsertCommand.Parameters[9].Value = 0.0;
                }
                else
                {
                    adapter.InsertCommand.Parameters[9].Value = ((double) (FuelMotorLiter));
                }

                if ((Category_id.HasValue == true))
                {
                    adapter.InsertCommand.Parameters[10].Value = ((int) (Category_id.Value));
                }
                else
                {
                    adapter.InsertCommand.Parameters[10].Value = global::System.DBNull.Value;
                }

                if ((Identifier == 0))
                {
                    adapter.InsertCommand.Parameters[11].Value = 0;
                }
                else
                {
                    adapter.InsertCommand.Parameters[11].Value = ((int) (Identifier));
                }

                if ((Category_id2.HasValue == true))
                {
                    adapter.InsertCommand.Parameters[12].Value = ((int) (Category_id2.Value));
                }
                else
                {
                    adapter.InsertCommand.Parameters[12].Value = global::System.DBNull.Value;
                }

                if( PcbVersionId <= -1 )
                {
                    adapter.InsertCommand.Parameters[13].Value = -1;
                }
                else
                {
                    adapter.InsertCommand.Parameters[13].Value = ( int )PcbVersionId;
                }

                if( ( Category_id3.HasValue == true ) )
                {
                    adapter.InsertCommand.Parameters[14].Value = ( ( int )( Category_id3.Value ) );
                }
                else
                {
                    adapter.InsertCommand.Parameters[14].Value = global::System.DBNull.Value;
                }

                if( ( Category_id4.HasValue == true ) )
                {
                    adapter.InsertCommand.Parameters[15].Value = ( ( int )( Category_id4.Value ) );
                }
                else
                {
                    adapter.InsertCommand.Parameters[15].Value = global::System.DBNull.Value;
                }

                global::System.Data.ConnectionState previousConnectionState = adapter.InsertCommand.Connection.State;
                if (((adapter.InsertCommand.Connection.State & global::System.Data.ConnectionState.Open)
                     != global::System.Data.ConnectionState.Open))
                {
                    adapter.InsertCommand.Connection.Open();
                }
                try
                {
                    int returnValue = adapter.InsertCommand.ExecuteNonQuery();
                    return returnValue;
                }
                finally
                {
                    if ((previousConnectionState == global::System.Data.ConnectionState.Closed))
                    {
                        adapter.InsertCommand.Connection.Close();
                    }
                }
            } // if
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                SqlDataAdapter adapter = (SqlDataAdapter) this.Adapter;

                if ((MakeCar == null))
                {
                    adapter.InsertCommand.Parameters[0].Value = global::System.DBNull.Value;
                }
                else
                {
                    adapter.InsertCommand.Parameters[0].Value = ((string) (MakeCar));
                }
                if ((NumberPlate == null))
                {
                    adapter.InsertCommand.Parameters[1].Value = global::System.DBNull.Value;
                }
                else
                {
                    adapter.InsertCommand.Parameters[1].Value = ((string) (NumberPlate));
                }
                if ((Team_id.HasValue == true))
                {
                    adapter.InsertCommand.Parameters[2].Value = ((int) (Team_id.Value));
                }
                else
                {
                    adapter.InsertCommand.Parameters[2].Value = global::System.DBNull.Value;
                }
                if ((Mobitel_id.HasValue == true))
                {
                    adapter.InsertCommand.Parameters[3].Value = ((int) (Mobitel_id.Value));
                }
                else
                {
                    adapter.InsertCommand.Parameters[3].Value = global::System.DBNull.Value;
                }
                if ((CarModel == null))
                {
                    adapter.InsertCommand.Parameters[4].Value = global::System.DBNull.Value;
                }
                else
                {
                    adapter.InsertCommand.Parameters[4].Value = ((string) (CarModel));
                }
                if ((setting_id.HasValue == true))
                {
                    adapter.InsertCommand.Parameters[5].Value = ((int) (setting_id.Value));
                }
                else
                {
                    adapter.InsertCommand.Parameters[5].Value = global::System.DBNull.Value;
                }
                if ((driver_id.HasValue == true))
                {
                    adapter.InsertCommand.Parameters[6].Value = ((int) (driver_id.Value));
                }
                else
                {
                    adapter.InsertCommand.Parameters[6].Value = global::System.DBNull.Value;
                }
                if ((IdOutLink == null))
                {
                    adapter.InsertCommand.Parameters[7].Value = global::System.DBNull.Value;
                }
                else
                {
                    adapter.InsertCommand.Parameters[7].Value = ((string) (IdOutLink));
                }

                if( ( FuelWayLiter == 0.0 ) )
                {
                    adapter.InsertCommand.Parameters[8].Value = 0.0;
                }
                else
                {
                    adapter.InsertCommand.Parameters[8].Value = ( ( double )( FuelWayLiter ) );
                }
                if( ( FuelMotorLiter == 0.0 ) )
                {
                    adapter.InsertCommand.Parameters[9].Value = 0.0;
                }
                else
                {
                    adapter.InsertCommand.Parameters[9].Value = ( ( double )( FuelMotorLiter ) );
                }

                if( ( Category_id.HasValue == true ) )
                {
                    adapter.InsertCommand.Parameters[10].Value = ( ( int )( Category_id.Value ) );
                }
                else
                {
                    adapter.InsertCommand.Parameters[10].Value = global::System.DBNull.Value;
                }

                if( ( Identifier == 0 ) )
                {
                    adapter.InsertCommand.Parameters[11].Value = 0;
                }
                else
                {
                    adapter.InsertCommand.Parameters[11].Value = ( ( int )( Identifier ) );
                }

                if( ( Category_id2.HasValue == true ) )
                {
                    adapter.InsertCommand.Parameters[12].Value = ( ( int )( Category_id2.Value ) );
                }
                else
                {
                    adapter.InsertCommand.Parameters[12].Value = global::System.DBNull.Value;
                }

                if( PcbVersionId <= -1 )
                {
                    adapter.InsertCommand.Parameters[13].Value = -1;
                }
                else
                {
                    adapter.InsertCommand.Parameters[13].Value = ( int )PcbVersionId;
                }

                if( ( Category_id3.HasValue == true ) )
                {
                    adapter.InsertCommand.Parameters[14].Value = ( ( int )( Category_id3.Value ) );
                }
                else
                {
                    adapter.InsertCommand.Parameters[14].Value = global::System.DBNull.Value;
                }

                if( ( Category_id4.HasValue == true ) )
                {
                    adapter.InsertCommand.Parameters[15].Value = ( ( int )( Category_id4.Value ) );
                }
                else
                {
                    adapter.InsertCommand.Parameters[15].Value = global::System.DBNull.Value;
                }

                global::System.Data.ConnectionState previousConnectionState = adapter.InsertCommand.Connection.State;
                if (((adapter.InsertCommand.Connection.State & global::System.Data.ConnectionState.Open)
                     != global::System.Data.ConnectionState.Open))
                {
                    adapter.InsertCommand.Connection.Open();
                }
                try
                {
                    int returnValue = adapter.InsertCommand.ExecuteNonQuery();
                    return returnValue;
                }
                finally
                {
                    if ((previousConnectionState == global::System.Data.ConnectionState.Closed))
                    {
                        adapter.InsertCommand.Connection.Close();
                    }
                }
            } // else if

            return 0;
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.ComponentModel.Design.HelpKeywordAttribute("vs.data.TableAdapter")]
        [global::System.ComponentModel.DataObjectMethodAttribute(global::System.ComponentModel.DataObjectMethodType.Update, true)]
        public virtual int Update(string MakeCar, string NumberPlate, global::System.Nullable<int> Team_id,
            global::System.Nullable<int> Mobitel_id, string CarModel,
            global::System.Nullable<int> setting_id, global::System.Nullable<int> driver_id, string IdOutLink, double FuelWayLiter, double FuelMotorLiter,
            global::System.Nullable<int> Category_id, int Identifier, global::System.Nullable<int> Category_id2, int PcbVersionId,
            int id, global::System.Nullable<int> Category_id3, global::System.Nullable<int> Category_id4 )
        {
            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;


                if ((MakeCar == null))
                {
                    adapter.UpdateCommand.Parameters[0].Value = global::System.DBNull.Value;
                }
                else
                {
                    adapter.UpdateCommand.Parameters[0].Value = ((string) (MakeCar));
                }
                if ((NumberPlate == null))
                {
                    adapter.UpdateCommand.Parameters[1].Value = global::System.DBNull.Value;
                }
                else
                {
                    adapter.UpdateCommand.Parameters[1].Value = ((string) (NumberPlate));
                }
                if ((Team_id.HasValue == true))
                {
                    adapter.UpdateCommand.Parameters[2].Value = ((int) (Team_id.Value));
                }
                else
                {
                    adapter.UpdateCommand.Parameters[2].Value = global::System.DBNull.Value;
                }
                if ((Mobitel_id.HasValue == true))
                {
                    adapter.UpdateCommand.Parameters[3].Value = ((int) (Mobitel_id.Value));
                }
                else
                {
                    adapter.UpdateCommand.Parameters[3].Value = global::System.DBNull.Value;
                }
                if ((CarModel == null))
                {
                    adapter.UpdateCommand.Parameters[4].Value = global::System.DBNull.Value;
                }
                else
                {
                    adapter.UpdateCommand.Parameters[4].Value = ((string) (CarModel));
                }
                if ((setting_id.HasValue == true))
                {
                    adapter.UpdateCommand.Parameters[5].Value = ((int) (setting_id.Value));
                }
                else
                {
                    adapter.UpdateCommand.Parameters[5].Value = global::System.DBNull.Value;
                }
                if ((driver_id.HasValue == true))
                {
                    adapter.UpdateCommand.Parameters[6].Value = ((int) (driver_id.Value));
                }
                else
                {
                    adapter.UpdateCommand.Parameters[6].Value = global::System.DBNull.Value;
                }

                if( ( IdOutLink == null ) )
                {
                    adapter.InsertCommand.Parameters[7].Value = global::System.DBNull.Value;
                }
                else
                {
                    adapter.InsertCommand.Parameters[7].Value = ( ( string )( IdOutLink ) );
                }

                if( ( FuelWayLiter == 0.0 ) )
                {
                    adapter.InsertCommand.Parameters[8].Value = 0.0;
                }
                else
                {
                    adapter.InsertCommand.Parameters[8].Value = ( ( double )( FuelWayLiter ) );
                }
                if( ( FuelMotorLiter == 0.0 ) )
                {
                    adapter.InsertCommand.Parameters[9].Value = 0.0;
                }
                else
                {
                    adapter.InsertCommand.Parameters[9].Value = ( ( double )( FuelMotorLiter ) );
                }

                if( ( Category_id.HasValue == true ) )
                {
                    adapter.InsertCommand.Parameters[10].Value = ( ( int )( Category_id.Value ) );
                }
                else
                {
                    adapter.InsertCommand.Parameters[10].Value = global::System.DBNull.Value;
                }

                if( ( Identifier == 0 ) )
                {
                    adapter.InsertCommand.Parameters[11].Value = 0;
                }
                else
                {
                    adapter.InsertCommand.Parameters[11].Value = ( ( int )( Identifier ) );
                }

                if( ( Category_id2.HasValue == true ) )
                {
                    adapter.InsertCommand.Parameters[12].Value = ( ( int )( Category_id2.Value ) );
                }
                else
                {
                    adapter.InsertCommand.Parameters[12].Value = global::System.DBNull.Value;
                }

                if( PcbVersionId <= -1 )
                {
                    adapter.InsertCommand.Parameters[13].Value = -1;
                }
                else
                {
                    adapter.InsertCommand.Parameters[13].Value = ( int )PcbVersionId;
                }

                adapter.UpdateCommand.Parameters[14].Value = ((int) (id));

                if( ( Category_id3.HasValue == true ) )
                {
                    adapter.InsertCommand.Parameters[15].Value = ( ( int )( Category_id3.Value ) );
                }
                else
                {
                    adapter.InsertCommand.Parameters[15].Value = global::System.DBNull.Value;
                }

                if( ( Category_id4.HasValue == true ) )
                {
                    adapter.InsertCommand.Parameters[16].Value = ( ( int )( Category_id4.Value ) );
                }
                else
                {
                    adapter.InsertCommand.Parameters[16].Value = global::System.DBNull.Value;
                }

                global::System.Data.ConnectionState previousConnectionState = adapter.UpdateCommand.Connection.State;
                if (((adapter.UpdateCommand.Connection.State & global::System.Data.ConnectionState.Open)
                     != global::System.Data.ConnectionState.Open))
                {
                    adapter.UpdateCommand.Connection.Open();
                }
                try
                {
                    int returnValue = adapter.UpdateCommand.ExecuteNonQuery();
                    return returnValue;
                }
                finally
                {
                    if ((previousConnectionState == global::System.Data.ConnectionState.Closed))
                    {
                        adapter.UpdateCommand.Connection.Close();
                    }
                }
            } // if
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                SqlDataAdapter adapter = (SqlDataAdapter) this.Adapter;
                
                if( ( MakeCar == null ) )
                {
                    adapter.UpdateCommand.Parameters[0].Value = global::System.DBNull.Value;
                }
                else
                {
                    adapter.UpdateCommand.Parameters[0].Value = ( ( string )( MakeCar ) );
                }
                if( ( NumberPlate == null ) )
                {
                    adapter.UpdateCommand.Parameters[1].Value = global::System.DBNull.Value;
                }
                else
                {
                    adapter.UpdateCommand.Parameters[1].Value = ( ( string )( NumberPlate ) );
                }
                if( ( Team_id.HasValue == true ) )
                {
                    adapter.UpdateCommand.Parameters[2].Value = ( ( int )( Team_id.Value ) );
                }
                else
                {
                    adapter.UpdateCommand.Parameters[2].Value = global::System.DBNull.Value;
                }
                if( ( Mobitel_id.HasValue == true ) )
                {
                    adapter.UpdateCommand.Parameters[3].Value = ( ( int )( Mobitel_id.Value ) );
                }
                else
                {
                    adapter.UpdateCommand.Parameters[3].Value = global::System.DBNull.Value;
                }
                if( ( CarModel == null ) )
                {
                    adapter.UpdateCommand.Parameters[4].Value = global::System.DBNull.Value;
                }
                else
                {
                    adapter.UpdateCommand.Parameters[4].Value = ( ( string )( CarModel ) );
                }
                if( ( setting_id.HasValue == true ) )
                {
                    adapter.UpdateCommand.Parameters[5].Value = ( ( int )( setting_id.Value ) );
                }
                else
                {
                    adapter.UpdateCommand.Parameters[5].Value = global::System.DBNull.Value;
                }
                if( ( driver_id.HasValue == true ) )
                {
                    adapter.UpdateCommand.Parameters[6].Value = ( ( int )( driver_id.Value ) );
                }
                else
                {
                    adapter.UpdateCommand.Parameters[6].Value = global::System.DBNull.Value;
                }

                if( ( IdOutLink == null ) )
                {
                    adapter.InsertCommand.Parameters[7].Value = global::System.DBNull.Value;
                }
                else
                {
                    adapter.InsertCommand.Parameters[7].Value = ( ( string )( IdOutLink ) );
                }

                if( ( FuelWayLiter == 0.0 ) )
                {
                    adapter.InsertCommand.Parameters[8].Value = 0.0;
                }
                else
                {
                    adapter.InsertCommand.Parameters[8].Value = ( ( double )( FuelWayLiter ) );
                }
                if( ( FuelMotorLiter == 0.0 ) )
                {
                    adapter.InsertCommand.Parameters[9].Value = 0.0;
                }
                else
                {
                    adapter.InsertCommand.Parameters[9].Value = ( ( double )( FuelMotorLiter ) );
                }

                if( ( Category_id.HasValue == true ) )
                {
                    adapter.InsertCommand.Parameters[10].Value = ( ( int )( Category_id.Value ) );
                }
                else
                {
                    adapter.InsertCommand.Parameters[10].Value = global::System.DBNull.Value;
                }

                if( ( Identifier == 0 ) )
                {
                    adapter.InsertCommand.Parameters[11].Value = 0;
                }
                else
                {
                    adapter.InsertCommand.Parameters[11].Value = ( ( int )( Identifier ) );
                }

                if( ( Category_id2.HasValue == true ) )
                {
                    adapter.InsertCommand.Parameters[12].Value = ( ( int )( Category_id2.Value ) );
                }
                else
                {
                    adapter.InsertCommand.Parameters[12].Value = global::System.DBNull.Value;
                }

                if( PcbVersionId <= -1 )
                {
                    adapter.InsertCommand.Parameters[13].Value = -1;
                }
                else
                {
                    adapter.InsertCommand.Parameters[13].Value = ( int )PcbVersionId;
                }

                adapter.UpdateCommand.Parameters[14].Value = ( ( int )( id ) );

                if( ( Category_id3.HasValue == true ) )
                {
                    adapter.InsertCommand.Parameters[15].Value = ( ( int )( Category_id3.Value ) );
                }
                else
                {
                    adapter.InsertCommand.Parameters[15].Value = global::System.DBNull.Value;
                }

                if( ( Category_id4.HasValue == true ) )
                {
                    adapter.InsertCommand.Parameters[16].Value = ( ( int )( Category_id4.Value ) );
                }
                else
                {
                    adapter.InsertCommand.Parameters[16].Value = global::System.DBNull.Value;
                }
                global::System.Data.ConnectionState previousConnectionState = adapter.UpdateCommand.Connection.State;
                if (((adapter.UpdateCommand.Connection.State & global::System.Data.ConnectionState.Open)
                     != global::System.Data.ConnectionState.Open))
                {
                    adapter.UpdateCommand.Connection.Open();
                }
                try
                {
                    int returnValue = adapter.UpdateCommand.ExecuteNonQuery();
                    return returnValue;
                }
                finally
                {
                    if ((previousConnectionState == global::System.Data.ConnectionState.Closed))
                    {
                        adapter.UpdateCommand.Connection.Close();
                    }
                }
            } // else if

            return 0;
        }

        public int update(atlantaDataSet dataset)
        {
            int ret = 0;
            foreach (atlantaDataSet.vehicleRow v_rows in dataset.vehicle.Rows)
            {
                if (v_rows.RowState == System.Data.DataRowState.Modified)
                {
                    Update(v_rows.MakeCar, v_rows.NumberPlate, v_rows.Team_id,
                        v_rows.Mobitel_id, v_rows.CarModel, v_rows.setting_id,
                        v_rows.driver_id, v_rows.OutLinkId, v_rows.FuelWayLiter,
                        v_rows.FuelMotorLiter, v_rows.Category_id, v_rows.Identifier, v_rows.Category_id2, v_rows.PcbVersionId , - 1, v_rows.Category_id3, v_rows.Category_id4 );
                    ret++;
                }
                if (v_rows.RowState == System.Data.DataRowState.Added)
                {
                    Insert(v_rows.MakeCar, v_rows.NumberPlate, v_rows.Team_id, v_rows.Mobitel_id,
                        v_rows.CarModel, v_rows.setting_id, v_rows.driver_id, v_rows.OutLinkId, v_rows.FuelWayLiter,
                        v_rows.FuelMotorLiter, v_rows.Category_id, v_rows.Identifier, v_rows.Category_id2, v_rows.PcbVersionId, v_rows.Category_id3, v_rows.Category_id4);
                    ret++;
                }

            }

            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                MySqlConnection connection = (MySqlConnection) this.Connection;
                connection.Close();
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                SqlConnection connection = (SqlConnection) this.Connection;
                connection.Close();
            }

            return ret;
        }

        public int insert(atlantaDataSet dataset)
        {
            int ret = 0;
            foreach (atlantaDataSet.vehicleRow v in dataset.vehicle.Rows)
            {
                if (v.RowState == System.Data.DataRowState.Added)
                {
                    Insert(v.MakeCar, v.NumberPlate, v.Team_id, v.Mobitel_id, v.CarModel, v.setting_id, v.driver_id,
                        v.OutLinkId, v.FuelWayLiter, v.FuelMotorLiter, v.Category_id, v.Identifier, v.Category_id2, v.PcbVersionId, v.Category_id3, v.Category_id4 );
                    ret++;
                }
            }

            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                MySqlConnection connection = (MySqlConnection) this.Connection;
                connection.Close();
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                SqlConnection connection = (SqlConnection) this.Connection;
                connection.Close();
            }

            return ret;
        }
    }
}