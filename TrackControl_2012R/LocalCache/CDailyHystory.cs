using System;
using System.Collections.Generic;

namespace LocalCache
{
  public class CDailyHystory
  {
    /// <summary>
    /// SDaily
    /// </summary>
    private struct SDaily
    {
      public DateTime data;
      public int m_id;
    }

    public CDailyHystory()
    {
      listDaily = new List<SDaily>();
    }

    /// <summary>
    /// Clear
    /// </summary>
    public void Clear()
    {
      lock (syncObject)
      {
        listDaily.Clear();
      }
    }

    public void Add(int mobitelId, DateTime date)
    {
      lock (syncObject)
      {
        SDaily daily = new SDaily();
        daily.m_id = mobitelId;
        daily.data = date;
        listDaily.Add(daily);
      }
    }


    private int[] m_id;
    public int[] M_id
    {
      get { return m_id; }
      set { m_id = value; }
    }

    private DateTime[] data;
    public DateTime[] Data
    {
      get { return data; }
      set { data = value; }
    }
    private volatile List<SDaily> listDaily;
    private static volatile object syncObject = new object(); 


    /// <summary>
    /// Get data
    /// </summary>
    /// <param name="m_id">M _id</param>
    /// <returns>Date time</returns>
    public DateTime[] GetData(int m_id)
    {
      lock (syncObject)
      {
        List<DateTime> listDate = new List<DateTime>();
        foreach (SDaily d in listDaily)
        {
          if (d.m_id == m_id)
            listDate.Add(d.data);
        }
        return Data = listDate.ToArray();
      } 
    }

    /// <summary>
    /// Get mobitel
    /// </summary>
    /// <param name="date">Date</param>
    /// <returns>Int</returns>
    public int[] GetMobitel(DateTime date)
    {
      lock (syncObject)
      {
        List<int> listMid = new List<int>();
        foreach (SDaily d in listDaily)
        {
          TimeSpan ts = d.data - date;
          if (ts.TotalDays < 1)
            listMid.Add(d.m_id);
        } 
        return M_id = listMid.ToArray();
      }
    }  	
  }
}
