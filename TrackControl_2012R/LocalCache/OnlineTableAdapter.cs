using System;
using System.Data;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using TrackControl.General.DatabaseDriver;

namespace LocalCache.atlantaDataSetTableAdapters
{
  public class OnlineTableAdapter : Component
  {
    Int64 _lastId;

    public string CS
    {
      get { return _cs; }
      set { _cs = value; }
    }
    string _cs;

    public atlantaDataSet.onlineDataTable GetLastData()
    {
      atlantaDataSet.onlineDataTable result = getHistoryData(_lastId);
      if (result.Count > 0)
      {
        _lastId = (Int64)result.Compute("MAX(Datagps_Id)", "");
      }

      return result;
    }

    atlantaDataSet.onlineDataTable getHistoryData(Int64 lastId)
    {
      atlantaDataSet.onlineDataTable tab = new atlantaDataSet.onlineDataTable();

      //using (MySqlCommand command = new MySqlCommand("hystoryOnline"))
        //using()
        DriverDb db = new DriverDb();
      {
          db.NewSqlCommand( "hystoryOnline" );
        //command.CommandType = CommandType.StoredProcedure;
          db.CommandType( CommandType.StoredProcedure );
        //command.Parameters.Add("?id", MySqlDbType.Int32).Value = lastId;
            db.CommandParametersAdd(db.ParamPrefics + "id", db.GettingInt64(), lastId);
        //using (MySqlConnection connection = new MySqlConnection(_cs))
          {
              db.NewSqlConnection(_cs);
          atlantaDataSet.onlineRow row;
          object[] data;
          //command.Connection = connection;
              db.CommandSqlConnection();
          //connection.Open();
              db.SqlConnectionOpen();
          //using (MySqlDataReader reader = command.ExecuteReader())
              {
                  db.SqlDataReader = db.CommandExecuteReader();
            //while (reader.Read())
                  while( db.Read() )
            {
              row = tab.NewonlineRow();
              //data = new object[reader.VisibleFieldCount];
              data = new object[db.VisibleFieldCount];
              //reader.GetValues(data);
              db.ReaderGetValues( data );
              row.ItemArray = data;
              tab.AddonlineRow(row);
            }
          }
              db.CloseDataReader();
        }
          db.SqlConnectionClose();
      }
        db.CloseSqlCommand();
      return tab;
    }
  }
}
