using System;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using TrackControl.General.DAL;
using TrackControl.General.DatabaseDriver;

namespace LocalCache.atlantaDataSetTableAdapters
{
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Design.TypedDataSetGenerator", "2.0.0.0")]
    [global::System.ComponentModel.DesignerCategoryAttribute("code")]
    [global::System.ComponentModel.ToolboxItem(true)]
    [global::System.ComponentModel.DataObjectAttribute(true)]
    [global::System.ComponentModel.DesignerAttribute(
        "Microsoft.VSDesigner.DataSource.Design.TableAdapterDesigner, Microsoft.VSDesigner" +
        ", Version=8.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")]
    [global::System.ComponentModel.Design.HelpKeywordAttribute("vs.data.TableAdapter")]
    public class TeamTableAdapter : global::System.ComponentModel.Component
    {

        //private global::MySql.Data.MySqlClient.MySqlDataAdapter _adapter;
        //private global::MySql.Data.MySqlClient.MySqlConnection _connection;
        //private global::MySql.Data.MySqlClient.MySqlCommand[] _commandCollection;

        private DriverDb db = new DriverDb();

        private bool _clearBeforeFill;

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public TeamTableAdapter()
        {
            this.ClearBeforeFill = true;
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        //private global::MySql.Data.MySqlClient.MySqlDataAdapter Adapter
        private object Adapter
        {
            get
            {
                if ((this.db.Adapter == null))
                {
                    this.InitAdapter();
                }

                return this.db.Adapter;
            }
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        //public global::MySql.Data.MySqlClient.MySqlConnection Connection
        public object Connection
        {
            get
            {
                if ((this.db.Connection == null))
                {
                    this.InitConnection();
                }

                return this.db.Connection;
            }
            set
            {
                this.db.Connection = value;

                if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                {
                    MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;

                    if ((adapter.InsertCommand != null))
                    {
                        adapter.InsertCommand.Connection = (MySqlConnection) value;
                    }

                    if ((adapter.DeleteCommand != null))
                    {
                        adapter.DeleteCommand.Connection = (MySqlConnection) value;
                    }

                    if ((adapter.UpdateCommand != null))
                    {
                        adapter.UpdateCommand.Connection = (MySqlConnection) value;
                    }

                    MySqlCommand[] collection = (MySqlCommand[]) this.CommandCollection;

                    for (int i = 0; (i < this.CommandCollection.Length); i = (i + 1))
                    {
                        if ((this.CommandCollection[i] != null))
                        {
                            collection[i].Connection = (MySqlConnection) value;
                        }
                    } // for
                } // if
                else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                {
                    SqlDataAdapter adapter = (SqlDataAdapter) this.Adapter;

                    if ((adapter.InsertCommand != null))
                    {
                        adapter.InsertCommand.Connection = (SqlConnection) value;
                    }

                    if ((adapter.DeleteCommand != null))
                    {
                        adapter.DeleteCommand.Connection = (SqlConnection) value;
                    }

                    if ((adapter.UpdateCommand != null))
                    {
                        adapter.UpdateCommand.Connection = (SqlConnection) value;
                    }

                    SqlCommand[] collection = (SqlCommand[]) this.CommandCollection;

                    for (int i = 0; (i < this.CommandCollection.Length); i = (i + 1))
                    {
                        if ((this.CommandCollection[i] != null))
                        {
                            collection[i].Connection = (SqlConnection) value;
                        }
                    } // for
                } // if
            } // set
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        //protected global::MySql.Data.MySqlClient.MySqlCommand[] CommandCollection
        protected object[] CommandCollection
        {
            get
            {
                if ((this.db.CommandCollection == null))
                {
                    this.InitCommandCollection();
                }

                return this.db.CommandCollection;
            }
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public bool ClearBeforeFill
        {
            get
            {
                return this._clearBeforeFill;
            }
            set
            {
                this._clearBeforeFill = value;
            }
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        private void InitAdapter()
        {
            //this._adapter = new global::MySql.Data.MySqlClient.MySqlDataAdapter();
            this.db.NewDataAdapter();
            global::System.Data.Common.DataTableMapping tableMapping = new global::System.Data.Common.DataTableMapping();
            tableMapping.SourceTable = "Table";
            tableMapping.DataSetTable = "team";
            tableMapping.ColumnMappings.Add("id", "id");
            tableMapping.ColumnMappings.Add("Name", "Name");
            tableMapping.ColumnMappings.Add("Descr", "Descr");
            tableMapping.ColumnMappings.Add("Setting_id", "Setting_id");
            tableMapping.ColumnMappings.Add("FuelWayKmGrp", "FuelWayKmGrp");
            tableMapping.ColumnMappings.Add("FuelMotoHrGrp", "FuelMotoHrGrp");
            //this._adapter.TableMappings.Add(tableMapping);
            this.db.AdapterTableMappingsAdd(tableMapping);

            //this._adapter.InsertCommand = new global::MySql.Data.MySqlClient.MySqlCommand();
            this.db.AdapterInsertCommand();
            //this._adapter.InsertCommand.Connection = this.Connection;
            this.db.AdapterInsertCommandConnection(this.Connection);
            //this._adapter.InsertCommand.UpdatedRowSource = System.Data.UpdateRowSource.FirstReturnedRecord;
            this.db.AdapterInsertCommandUpdatedRowSource(System.Data.UpdateRowSource.FirstReturnedRecord);
            //this._adapter.InsertCommand.CommandText = 
            //  "INSERT INTO team (`Name`, `Descr`, `Setting_id`) " +
            //  "VALUES (?Name, ?Descr, ?Setting_id); " +
            //  "SELECT * FROM team WHERE Id = LAST_INSERT_ID()";

            this.db.AdapterInsertCommandText(TrackControlQuery.TeamTableAdapter.InsertIntoTeam);

            //this._adapter.InsertCommand.CommandType = global::System.Data.CommandType.Text;
            this.db.AdapterInsertCommandType(global::System.Data.CommandType.Text);
            // global::MySql.Data.MySqlClient.MySqlParameter param = new global::MySql.Data.MySqlClient.MySqlParameter();

            this.db.NewSqlParameter();
            //param.ParameterName = "?Name";
            this.db.ParameterName(db.ParamPrefics + "Name");
            //param.DbType = global::System.Data.DbType.StringFixedLength;
            this.db.ParameterDbType(global::System.Data.DbType.StringFixedLength);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.String;
            this.db.ParameterSqlDbType(db.GettingString());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "Name";
            this.db.ParameterSourceColumn("Name");
            //this._adapter.InsertCommand.Parameters.Add(param);
            this.db.AdapterInsertCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?Descr";
            this.db.ParameterName(db.ParamPrefics + "Descr");
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Text;
            this.db.ParameterSqlDbType(db.GettingText());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "Descr";
            this.db.ParameterSourceColumn("Descr");
            //this._adapter.InsertCommand.Parameters.Add(param);
            this.db.AdapterInsertCommandParametersAdd();

            // param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?Setting_id";
            this.db.ParameterName(db.ParamPrefics + "Setting_id");
            //param.DbType = global::System.Data.DbType.Int32;
            this.db.ParameterDbType(global::System.Data.DbType.Int32);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType(db.GettingInt32());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "Setting_id";
            this.db.ParameterSourceColumn("Setting_id");
            //this._adapter.InsertCommand.Parameters.Add(param);
            this.db.AdapterInsertCommandParametersAdd();

            // param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?Setting_id";
            this.db.ParameterName(db.ParamPrefics + "FuelWayKmGrp");
            //param.DbType = global::System.Data.DbType.Int32;
            this.db.ParameterDbType(global::System.Data.DbType.Double);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType(db.GettingDouble());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "Setting_id";
            this.db.ParameterSourceColumn("FuelWayKmGrp");
            //this._adapter.InsertCommand.Parameters.Add(param);
            this.db.AdapterInsertCommandParametersAdd();

            // param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?Setting_id";
            this.db.ParameterName(db.ParamPrefics + "FuelMotoHrGrp");
            //param.DbType = global::System.Data.DbType.Int32;
            this.db.ParameterDbType(global::System.Data.DbType.Double);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType(db.GettingDouble());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "Setting_id";
            this.db.ParameterSourceColumn("FuelMotoHrGrp");
            //this._adapter.InsertCommand.Parameters.Add(param);
            this.db.AdapterInsertCommandParametersAdd();

            //this._adapter.UpdateCommand = new global::MySql.Data.MySqlClient.MySqlCommand();
            this.db.AdapterUpdateCommand();
            //this._adapter.UpdateCommand.Connection = this.Connection;
            this.db.AdapterUpdateCommandConnection(this.Connection);
            //this._adapter.UpdateCommand.CommandText = 
            //  "UPDATE team SET Name = ?p1, Descr = ?p2, Setting_id = ?p3 " +
            //  "WHERE id = ?id";
            //this._adapter.UpdateCommand.CommandType = global::System.Data.CommandType.Text;

            this.db.AdapterUpdateCommandText(TrackControlQuery.TeamTableAdapter.UpdateTeam);

            this.db.AdapterUpdateCommandType(global::System.Data.CommandType.Text);

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?p1";
            this.db.ParameterName(db.ParamPrefics + "p1");
            //param.DbType = global::System.Data.DbType.String;
            this.db.ParameterDbType(global::System.Data.DbType.String);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.String;
            this.db.ParameterSqlDbType(db.GettingString());
            //param.Size = 20;
            this.db.ParameterSize(20);
            // param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "Name";
            this.db.ParameterSourceColumn("Name");
            //this._adapter.UpdateCommand.Parameters.Add(param);
            this.db.AdapterUpdateCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?p2";
            this.db.ParameterName(db.ParamPrefics + "p2");
            //param.DbType = global::System.Data.DbType.String;
            this.db.ParameterDbType(global::System.Data.DbType.String);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Text;
            this.db.ParameterSqlDbType(db.GettingText());
            //param.Size = 2147483647;
            this.db.ParameterSize(2147483647);
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "Descr";
            this.db.ParameterSourceColumn("Descr");
            //this._adapter.UpdateCommand.Parameters.Add(param);
            this.db.AdapterUpdateCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?p3";
            this.db.ParameterName(db.ParamPrefics + "p3");
            //param.DbType = global::System.Data.DbType.Int32;
            this.db.ParameterDbType(global::System.Data.DbType.Int32);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType(db.GettingInt32());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "Setting_id";
            this.db.ParameterSourceColumn("Setting_id");
            //this._adapter.UpdateCommand.Parameters.Add(param);
            this.db.AdapterUpdateCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?p3";
            this.db.ParameterName(db.ParamPrefics + "FuelWayKmGrp");
            //param.DbType = global::System.Data.DbType.Int32;
            this.db.ParameterDbType(global::System.Data.DbType.Double);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType(db.GettingDouble());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "Setting_id";
            this.db.ParameterSourceColumn("FuelWayKmGrp");
            //this._adapter.UpdateCommand.Parameters.Add(param);
            this.db.AdapterUpdateCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?p3";
            this.db.ParameterName(db.ParamPrefics + "FuelMotoHrGrp");
            //param.DbType = global::System.Data.DbType.Int32;
            this.db.ParameterDbType(global::System.Data.DbType.Double);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType(db.GettingDouble());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "Setting_id";
            this.db.ParameterSourceColumn("FuelMotoHrGrp");
            //this._adapter.UpdateCommand.Parameters.Add(param);
            this.db.AdapterUpdateCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?id";
            this.db.ParameterName(db.ParamPrefics + "id");
            //param.DbType = global::System.Data.DbType.Int32;
            this.db.ParameterDbType(global::System.Data.DbType.Int32);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType(db.GettingInt32());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "id";
            this.db.ParameterSourceColumn("id");
            // param.SourceVersion = global::System.Data.DataRowVersion.Original;
            this.db.ParameterSourceVersion(global::System.Data.DataRowVersion.Original);
            //this._adapter.UpdateCommand.Parameters.Add(param);
            this.db.AdapterUpdateCommandParametersAdd();

            //this._adapter.DeleteCommand = new global::MySql.Data.MySqlClient.MySqlCommand();
            this.db.AdapterDeleteCommand();
            // this._adapter.DeleteCommand.Connection = this.Connection;
            this.db.AdapterDeleteCommandConnection(this.Connection);
            //this._adapter.DeleteCommand.CommandText = "DELETE FROM team WHERE id = @Original_Id";
            this.db.AdapterInsertCommandText("DELETE FROM team WHERE id = @Original_Id");
            //this._adapter.DeleteCommand.CommandType = global::System.Data.CommandType.Text;
            this.db.AdapterDeleteCommandType(global::System.Data.CommandType.Text);

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "@Original_Id";
            this.db.ParameterName("@Original_Id");
            //param.DbType = global::System.Data.DbType.Int32;
            this.db.ParameterDbType(global::System.Data.DbType.Int32);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType(db.GettingInt32());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "id";
            this.db.ParameterSourceColumn("id");
            //param.SourceVersion = global::System.Data.DataRowVersion.Original;
            this.db.ParameterSourceVersion(global::System.Data.DataRowVersion.Original);
            //this._adapter.DeleteCommand.Parameters.Add(param);
            this.db.AdapterDeleteCommandParametersAdd();
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        private void InitConnection()
        {
            //this._connection = new global::MySql.Data.MySqlClient.MySqlConnection();
            this.db.AdapNewConnection();
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        private void InitCommandCollection()
        {
            //this._commandCollection = new global::MySql.Data.MySqlClient.MySqlCommand[1];
            this.db.NewCommandCollection(1);
            //this._commandCollection[0] = new global::MySql.Data.MySqlClient.MySqlCommand();
            this.db.SetNewCommandCollection(0);
            //this._commandCollection[0].Connection = this.Connection;
            this.db.SetCommandCollectionConnection(this.Connection, 0);
            //this._commandCollection[0].CommandText = "SELECT * FROM team";
            this.db.SetCommandCollectionText("SELECT * FROM team", 0);
            //this._commandCollection[0].CommandType = global::System.Data.CommandType.Text;
            this.db.SetCommandCollectionType(global::System.Data.CommandType.Text, 0);
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.ComponentModel.Design.HelpKeywordAttribute("vs.data.TableAdapter")]
        [global::System.ComponentModel.DataObjectMethodAttribute(global::System.ComponentModel.DataObjectMethodType.Fill,
            true)]
        public virtual int Fill(atlantaDataSet.teamDataTable dataTable)
        {
            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;
                adapter.SelectCommand = (MySqlCommand) this.CommandCollection[0];
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                SqlDataAdapter adapter = (SqlDataAdapter) this.Adapter;
                adapter.SelectCommand = (SqlCommand) this.CommandCollection[0];
            }

            if ((this.ClearBeforeFill == true))
            {
                dataTable.Clear();
            }

            int returnValue = 0; // this.Adapter.Fill( dataTable );
            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;
                returnValue = adapter.Fill(dataTable);
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                SqlDataAdapter adapter = (SqlDataAdapter) this.Adapter;
                returnValue = adapter.Fill(dataTable);
            }

            return returnValue;
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.ComponentModel.Design.HelpKeywordAttribute("vs.data.TableAdapter")]
        [global::System.ComponentModel.DataObjectMethodAttribute(
            global::System.ComponentModel.DataObjectMethodType.Select, true)]
        public virtual atlantaDataSet.teamDataTable GetData()
        {
            //this.Adapter.SelectCommand = this.CommandCollection[0];
            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;
                adapter.SelectCommand = (MySqlCommand) this.CommandCollection[0];
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                SqlDataAdapter adapter = (SqlDataAdapter) this.Adapter;
                adapter.SelectCommand = (SqlCommand) this.CommandCollection[0];
            }

            atlantaDataSet.teamDataTable dataTable = new atlantaDataSet.teamDataTable();


            //this.Adapter.Fill(dataTable);
            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;
                adapter.Fill(dataTable);
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                SqlDataAdapter adapter = (SqlDataAdapter) this.Adapter;
                adapter.Fill(dataTable);
            }

            return dataTable;
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.ComponentModel.Design.HelpKeywordAttribute("vs.data.TableAdapter")]
        public virtual int Update(atlantaDataSet.teamDataTable dataTable)
        {

            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;
                return adapter.Update(dataTable);
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                SqlDataAdapter adapter = (SqlDataAdapter) this.Adapter;
                return adapter.Update(dataTable);
            }

            return 0;
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.ComponentModel.Design.HelpKeywordAttribute("vs.data.TableAdapter")]
        public virtual int Update(atlantaDataSet dataSet)
        {
            //return this.Adapter.Update(dataSet, "team");
            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;
                return adapter.Update(dataSet, "team");
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                SqlDataAdapter adapter = (SqlDataAdapter) this.Adapter;
                return adapter.Update(dataSet, "team");
            }

            return 0;
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.ComponentModel.Design.HelpKeywordAttribute("vs.data.TableAdapter")]
        public virtual int Update(global::System.Data.DataRow dataRow)
        {
            //return this.Adapter.Update(new global::System.Data.DataRow[] {dataRow});

            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;
                return adapter.Update(new global::System.Data.DataRow[] {dataRow});
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                SqlDataAdapter adapter = (SqlDataAdapter) this.Adapter;
                return adapter.Update(new global::System.Data.DataRow[] {dataRow});
            }
            return 0;
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.ComponentModel.Design.HelpKeywordAttribute("vs.data.TableAdapter")]
        public virtual int Update(global::System.Data.DataRow[] dataRows)
        {
            // return this.Adapter.Update(dataRows);
            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;
                return adapter.Update(dataRows);
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                SqlDataAdapter adapter = (SqlDataAdapter) this.Adapter;
                return adapter.Update(dataRows);
            }

            return 0;
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.ComponentModel.Design.HelpKeywordAttribute("vs.data.TableAdapter")]
        [global::System.ComponentModel.DataObjectMethodAttribute(
            global::System.ComponentModel.DataObjectMethodType.Insert, true)]
        public virtual int Insert(string Name, string Descr, global::System.Nullable<int> Setting_id,
            double FuelWayKmGrp,
            double FuelMotoHrGrp)
        {
            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;

                if ((Name == null))
                {
                    adapter.InsertCommand.Parameters[0].Value = global::System.DBNull.Value;
                }
                else
                {
                    adapter.InsertCommand.Parameters[0].Value = ((string) (Name));
                }
                if ((Descr == null))
                {
                    adapter.InsertCommand.Parameters[1].Value = global::System.DBNull.Value;
                }
                else
                {
                    adapter.InsertCommand.Parameters[1].Value = ((string) (Descr));
                }
                if ((Setting_id.HasValue == true))
                {
                    adapter.InsertCommand.Parameters[2].Value = ((int) (Setting_id.Value));
                }
                else
                {
                    adapter.InsertCommand.Parameters[2].Value = global::System.DBNull.Value;
                }

                if ((FuelWayKmGrp != 0.0))
                {
                    adapter.InsertCommand.Parameters[3].Value = ((double) (FuelWayKmGrp));
                }
                else
                {
                    adapter.InsertCommand.Parameters[3].Value = 0.0;
                }

                if ((FuelMotoHrGrp != 0.0))
                {
                    adapter.InsertCommand.Parameters[4].Value = ((double) (FuelMotoHrGrp));
                }
                else
                {
                    adapter.InsertCommand.Parameters[4].Value = 0.0;
                }

                global::System.Data.ConnectionState previousConnectionState = adapter.InsertCommand.Connection.State;
                if (((adapter.InsertCommand.Connection.State & global::System.Data.ConnectionState.Open)
                     != global::System.Data.ConnectionState.Open))
                {
                    adapter.InsertCommand.Connection.Open();
                }
                try
                {
                    int returnValue = adapter.InsertCommand.ExecuteNonQuery();
                    return returnValue;
                }
                finally
                {
                    if ((previousConnectionState == global::System.Data.ConnectionState.Closed))
                    {
                        adapter.InsertCommand.Connection.Close();
                    }
                }
            } // if
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                SqlDataAdapter adapter = (SqlDataAdapter) this.Adapter;

                if ((Name == null))
                {
                    adapter.InsertCommand.Parameters[0].Value = global::System.DBNull.Value;
                }
                else
                {
                    adapter.InsertCommand.Parameters[0].Value = ((string) (Name));
                }
                if ((Descr == null))
                {
                    adapter.InsertCommand.Parameters[1].Value = global::System.DBNull.Value;
                }
                else
                {
                    adapter.InsertCommand.Parameters[1].Value = ((string) (Descr));
                }
                if ((Setting_id.HasValue == true))
                {
                    adapter.InsertCommand.Parameters[2].Value = ((int) (Setting_id.Value));
                }
                else
                {
                    adapter.InsertCommand.Parameters[2].Value = global::System.DBNull.Value;
                }

                if ((FuelWayKmGrp != 0.0))
                {
                    adapter.InsertCommand.Parameters[3].Value = ((double) (FuelWayKmGrp));
                }
                else
                {
                    adapter.InsertCommand.Parameters[3].Value = 0.0;
                }

                if ((FuelMotoHrGrp != 0.0))
                {
                    adapter.InsertCommand.Parameters[4].Value = ((double) (FuelMotoHrGrp));
                }
                else
                {
                    adapter.InsertCommand.Parameters[4].Value = 0.0;
                }

                global::System.Data.ConnectionState previousConnectionState = adapter.InsertCommand.Connection.State;
                if (((adapter.InsertCommand.Connection.State & global::System.Data.ConnectionState.Open)
                     != global::System.Data.ConnectionState.Open))
                {
                    adapter.InsertCommand.Connection.Open();
                }
                try
                {
                    int returnValue = adapter.InsertCommand.ExecuteNonQuery();
                    return returnValue;
                }
                finally
                {
                    if ((previousConnectionState == global::System.Data.ConnectionState.Closed))
                    {
                        adapter.InsertCommand.Connection.Close();
                    }
                }
            } // if

            return 0;
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.ComponentModel.Design.HelpKeywordAttribute("vs.data.TableAdapter")]
        [global::System.ComponentModel.DataObjectMethodAttribute(
            global::System.ComponentModel.DataObjectMethodType.Update, true)]
        public virtual int Update(string p1, string p2, global::System.Nullable<int> p3, int id, double FuelWayKmGrp,
            double FuelMotoHrGrp)
        {
            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;

                if ((p1 == null))
                {
                    adapter.UpdateCommand.Parameters[0].Value = global::System.DBNull.Value;
                }
                else
                {
                    adapter.UpdateCommand.Parameters[0].Value = ((string) (p1));
                }
                if ((p2 == null))
                {
                    adapter.UpdateCommand.Parameters[1].Value = global::System.DBNull.Value;
                }
                else
                {
                    adapter.UpdateCommand.Parameters[1].Value = ((string) (p2));
                }
                if ((p3.HasValue == true))
                {
                    adapter.UpdateCommand.Parameters[2].Value = ((int) (p3.Value));
                }
                else
                {
                    adapter.UpdateCommand.Parameters[2].Value = global::System.DBNull.Value;
                }

                if ((FuelWayKmGrp != 0))
                {
                    adapter.UpdateCommand.Parameters[3].Value = ((double) (FuelWayKmGrp));
                }
                else
                {
                    adapter.UpdateCommand.Parameters[3].Value = 0.0;
                }

                if ((FuelMotoHrGrp != 0.0))
                {
                    adapter.UpdateCommand.Parameters[4].Value = ((double) (FuelMotoHrGrp));
                }
                else
                {
                    adapter.UpdateCommand.Parameters[4].Value = 0.0;
                }

                adapter.UpdateCommand.Parameters[5].Value = ((int) (id));
                global::System.Data.ConnectionState previousConnectionState = adapter.UpdateCommand.Connection.State;
                if (((adapter.UpdateCommand.Connection.State & global::System.Data.ConnectionState.Open)
                     != global::System.Data.ConnectionState.Open))
                {
                    adapter.UpdateCommand.Connection.Open();
                }
                try
                {
                    int returnValue = adapter.UpdateCommand.ExecuteNonQuery();
                    return returnValue;
                }
                finally
                {
                    if ((previousConnectionState == global::System.Data.ConnectionState.Closed))
                    {
                        adapter.UpdateCommand.Connection.Close();
                    }
                }
            } // if
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                SqlDataAdapter adapter = (SqlDataAdapter) this.Adapter;

                if ((p1 == null))
                {
                    adapter.UpdateCommand.Parameters[0].Value = global::System.DBNull.Value;
                }
                else
                {
                    adapter.UpdateCommand.Parameters[0].Value = ((string) (p1));
                }
                if ((p2 == null))
                {
                    adapter.UpdateCommand.Parameters[1].Value = global::System.DBNull.Value;
                }
                else
                {
                    adapter.UpdateCommand.Parameters[1].Value = ((string) (p2));
                }
                if ((p3.HasValue == true))
                {
                    adapter.UpdateCommand.Parameters[2].Value = ((int) (p3.Value));
                }
                else
                {
                    adapter.UpdateCommand.Parameters[2].Value = global::System.DBNull.Value;
                }

                if ((FuelWayKmGrp != 0))
                {
                    adapter.UpdateCommand.Parameters[3].Value = ((double) (FuelWayKmGrp));
                }
                else
                {
                    adapter.UpdateCommand.Parameters[3].Value = 0.0;
                }

                if ((FuelMotoHrGrp != 0.0))
                {
                    adapter.UpdateCommand.Parameters[4].Value = ((double) (FuelMotoHrGrp));
                }
                else
                {
                    adapter.UpdateCommand.Parameters[4].Value = 0.0;
                }

                adapter.UpdateCommand.Parameters[5].Value = ((int) (id));
                global::System.Data.ConnectionState previousConnectionState = adapter.UpdateCommand.Connection.State;
                if (((adapter.UpdateCommand.Connection.State & global::System.Data.ConnectionState.Open)
                     != global::System.Data.ConnectionState.Open))
                {
                    adapter.UpdateCommand.Connection.Open();
                }
                try
                {
                    int returnValue = adapter.UpdateCommand.ExecuteNonQuery();
                    return returnValue;
                }
                finally
                {
                    if ((previousConnectionState == global::System.Data.ConnectionState.Closed))
                    {
                        adapter.UpdateCommand.Connection.Close();
                    }
                }
            } //else  if

            return 0;
        }
    }
}
