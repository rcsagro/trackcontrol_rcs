using LocalCache.atlantaDataSetTableAdapters;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Windows.Forms;
using TrackControl.General;
using TrackControl.General.DAL;
using TrackControl.General.DatabaseDriver;
using TrackControl.Vehicles;

namespace LocalCache
{
    public static class DataSetManager
    {
        public static string ConnectionString = String.Empty;
        private static  atlantaDataSet.settingDataTable settingData = null;
        private static atlantaDataSet.vehicleDataTable vehicleData = null;

        //public static List<GpsData> GpsDatasForDemoTest64Packet = new List<GpsData>();

        public static Dictionary<int, List<GpsData>> GpsDatas64 = new Dictionary<int, List<GpsData>>();

        /// <summary>
        /// ��������� ������� ��������� �������
        /// </summary>
        /// <param name="ds">�������, ������� ���������� ���������</param>
        public static void Fill(atlantaDataSet ds)
        {
            if (String.IsNullOrEmpty(ConnectionString))
                throw new Exception("Not specify a connection string to the database");

            #region -- Sensors --

            using (SensorsTableAdapter sensorsTableAdapter = new SensorsTableAdapter())
            {
                //sensorsTableAdapter.SqlConnection.ConnectionString = ConnectionString;

                if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                {
                    MySqlConnection connection = (MySqlConnection) sensorsTableAdapter.Connection;
                    connection.ConnectionString = ConnectionString;
                }
                else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                {
                    SqlConnection connection = (SqlConnection) sensorsTableAdapter.Connection;
                    connection.ConnectionString = ConnectionString;
                }

                sensorsTableAdapter.Fill(ds.sensors);
            }

            #endregion

            #region -- Sensoralgorithms --

            using (SensoralgorithmsTableAdapter sensoralgorithmsTableAdapter = new SensoralgorithmsTableAdapter())
            {
                //sensoralgorithmsTableAdapter.Connection.ConnectionString = ConnectionString;

                if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                {
                    MySqlConnection connection = (MySqlConnection) sensoralgorithmsTableAdapter.Connection;
                    connection.ConnectionString = ConnectionString;
                }
                else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                {
                    SqlConnection connection = (SqlConnection) sensoralgorithmsTableAdapter.Connection;
                    connection.ConnectionString = ConnectionString;
                }
                sensoralgorithmsTableAdapter.Fill(ds.sensoralgorithms);
            }

            #endregion

            #region -- Relationalgorithms --

            using (RelationalgorithmsTableAdapter relationalgorithmsTableAdapter = new RelationalgorithmsTableAdapter())
            {
                if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                {
                    MySqlConnection connection = (MySqlConnection) relationalgorithmsTableAdapter.Connection;
                    connection.ConnectionString = ConnectionString;
                }
                else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                {
                    SqlConnection connection = (SqlConnection) relationalgorithmsTableAdapter.Connection;
                    connection.ConnectionString = ConnectionString;
                }

                relationalgorithmsTableAdapter.Fill(ds.relationalgorithms);
            }

            #endregion

            #region -- Sensorcoefficient --

            using (SensorcoefficientTableAdapter sensorcoefficientTableAdapter = new SensorcoefficientTableAdapter())
            {
                //sensorcoefficientTableAdapter.SqlConnection.ConnectionString = ConnectionString;

                if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                {
                    MySqlConnection connection = (MySqlConnection) sensorcoefficientTableAdapter.Connection;
                    connection.ConnectionString = ConnectionString;
                }
                else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                {
                    SqlConnection connection = (SqlConnection) sensorcoefficientTableAdapter.Connection;
                    connection.ConnectionString = ConnectionString;
                }

                sensorcoefficientTableAdapter.Fill(ds.sensorcoefficient);
            }

            #endregion

            #region -- Mobitels --

            MobitelsProvider mobitelsProvider = new MobitelsProvider(ConnectionString);
            mobitelsProvider.FillMobitelsTable(ds.mobitels);

            #endregion

            #region -- Setting --

            using (SettingTableAdapter settingTableAdapter = new SettingTableAdapter())
            {
                //settingTableAdapter.Connection.ConnectionString = ConnectionString;

                if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                {
                    MySqlConnection connection = (MySqlConnection) settingTableAdapter.Connection;
                    connection.ConnectionString = ConnectionString;
                }
                else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                {
                    SqlConnection connection = (SqlConnection) settingTableAdapter.Connection;
                    connection.ConnectionString = ConnectionString;
                }

                settingTableAdapter.Fill(ds.setting);

                settingData = ds.setting; // ��� ���������� �������������
            }

            #endregion

            #region -- State --

            using (StateTableAdapter stateTableAdapter = new StateTableAdapter())
            {
                //stateTableAdapter.Connection.ConnectionString = ConnectionString;
                if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                {
                    MySqlConnection connection = (MySqlConnection) stateTableAdapter.Connection;
                    connection.ConnectionString = ConnectionString;
                }
                else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                {
                    SqlConnection connection = (SqlConnection) stateTableAdapter.Connection;
                    connection.ConnectionString = ConnectionString;
                }

                try
                {
                    stateTableAdapter.Fill(ds.State);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message + "\n" + ex.StackTrace, "StateTableAdapter", MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                }
            }

            #endregion

            #region -- Transition --

            using (TransitionTableAdapter transitionTableAdapter = new TransitionTableAdapter())
            {
                //transitionTableAdapter.Connection.ConnectionString = ConnectionString;
                if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                {
                    MySqlConnection connection = (MySqlConnection) transitionTableAdapter.Connection;
                    connection.ConnectionString = ConnectionString;
                }
                else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                {
                    SqlConnection connection = (SqlConnection) transitionTableAdapter.Connection;
                    connection.ConnectionString = ConnectionString;
                }
                ds.EnforceConstraints = false;
                transitionTableAdapter.Fill(ds.Transition);
            }

            #endregion

            #region -- Driver --

            using (DriverTableAdapter driverTableAdapter = new DriverTableAdapter())
            {
                //driverTableAdapter.Connection.ConnectionString = ConnectionString;
                if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                {
                    MySqlConnection connection = (MySqlConnection) driverTableAdapter.Connection;
                    connection.ConnectionString = ConnectionString;
                }
                else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                {
                    SqlConnection connection = (SqlConnection) driverTableAdapter.Connection;
                    connection.ConnectionString = ConnectionString;
                }

                driverTableAdapter.Fill(ds.driver);
            }

            #endregion

            #region -- Team --

            using (TeamTableAdapter teamTableAdapter = new TeamTableAdapter())
            {
                //teamTableAdapter.Connection.ConnectionString = ConnectionString;

                if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                {
                    MySqlConnection connection = (MySqlConnection) teamTableAdapter.Connection;
                    connection.ConnectionString = ConnectionString;
                }
                else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                {
                    SqlConnection connection = (SqlConnection) teamTableAdapter.Connection;
                    connection.ConnectionString = ConnectionString;
                }

                teamTableAdapter.Fill(ds.team);
            }

            #endregion

            #region -- Vehicle --

            using (VehicleTableAdapter vehicleTableAdapter = new VehicleTableAdapter())
            {
                //vehicleTableAdapter.Connection.ConnectionString = ConnectionString;
                if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                {
                    MySqlConnection connection = (MySqlConnection) vehicleTableAdapter.Connection;
                    connection.ConnectionString = ConnectionString;
                }
                else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                {
                    SqlConnection connection = (SqlConnection) vehicleTableAdapter.Connection;
                    connection.ConnectionString = ConnectionString;
                }
                vehicleTableAdapter.Fill(ds.vehicle);

                vehicleData = ds.vehicle; // ��� ���������� �������������
            }

            #endregion

            #region -- ReferenceVehical --

            //int vehicleICount = 0;

            foreach (atlantaDataSet.mobitelsRow m_row in ds.mobitels.Rows)
            {
                try
                {
                    atlantaDataSet.ReferenceVehicalRow vehicle = ds.ReferenceVehical.NewReferenceVehicalRow();
                    atlantaDataSet.vehicleRow[] v_rows = m_row.GetvehicleRows();

                    if (v_rows.Length > 0)
                    {
                        try
                        {
                            vehicle.Model = v_rows[0].CarModel;
                        }
                        catch (Exception e)
                        {
                            vehicle.Model = " ";
                        }

                        vehicle.Make = v_rows[0].MakeCar;

                        vehicle.vehicle_id = v_rows[0].id;

                        vehicle.NumberPlate = v_rows[0].NumberPlate;
                        vehicle.Driver_id = v_rows[0].driver_id;

                        if (v_rows[0].driverRow != null)
                            vehicle.DriverName = v_rows[0].driverRow.Family;
                        else
                            vehicle.DriverName = "";

                        vehicle.OutLinkId = v_rows[0].OutLinkId;
                        vehicle.FuelWayLiter = v_rows[0].FuelWayLiter;
                        vehicle.FuelMotorLiter = v_rows[0].FuelMotorLiter;
                        vehicle.Identifier = v_rows[0].Identifier;

                        foreach (atlantaDataSet.teamRow t_row in ds.team.Rows)
                        {
                            if ((v_rows[0]["Team_id"] != DBNull.Value) &&
                                (t_row["id"] != DBNull.Value) && (v_rows[0].Team_id == t_row.id))
                            {
                                vehicle.Team_id = t_row.id;
                                vehicle.Team = t_row.Name;
                                break;
                            }
                        }
                    }
                    else
                    {
                        vehicle.Model = m_row.Name;
                        vehicle.Make = m_row.Descr;
                        vehicle.Driver_id = 1;
                        vehicle.NumberPlate = m_row.Mobitel_ID.ToString();
                        vehicle.vehicle_id = -m_row.Mobitel_ID;
                    }

                    vehicle.colorTrack = m_row.colorTrack;
                    vehicle.Check = m_row.Check;
                    vehicle.Mobitel_id = m_row.Mobitel_ID;
                    DataRow row = (DataRow) m_row;

                    if (row["NumTT"] != DBNull.Value)
                        vehicle.Num_TT = m_row.NumTT;
                    else
                        vehicle.Num_TT = "";
                    if (m_row["SIM"] != DBNull.Value)
                        vehicle.sim = m_row.SIM;
                    else
                        vehicle.sim = "";

                    vehicle.LastTimeGps = m_row.LastTimeGps;
                    ds.ReferenceVehical.AddReferenceVehicalRow(vehicle);
                }
                catch (Exception e)
                {
                    continue;
                }
            } // foreach

            ds.AcceptChanges();

            #endregion
        }

        public static atlantaDataSet.sensorsDataTable FillSensorTable(atlantaDataSet.sensorsDataTable dssensors)
        {
            try
            {
                using (SensorsTableAdapter sensorsTableAdapter = new SensorsTableAdapter())
                {

                    if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                    {
                        MySqlConnection connection = (MySqlConnection) sensorsTableAdapter.Connection;
                        connection.ConnectionString = ConnectionString;
                    }
                    else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                    {
                        SqlConnection connection = (SqlConnection) sensorsTableAdapter.Connection;
                        connection.ConnectionString = ConnectionString;
                    }

                    sensorsTableAdapter.Fill(dssensors);
                }

                return dssensors;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + '\n' + ex.StackTrace, "Error FillSensorTable", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                return null;
            }
        }

        public static void FillVehicleTable( atlantaDataSet ds )
        {
            using( VehicleTableAdapter vehicleTableAdapter = new VehicleTableAdapter() )
            {
                //vehicleTableAdapter.Connection.ConnectionString = ConnectionString;
                if( DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse )
                {
                    MySqlConnection connection = ( MySqlConnection )vehicleTableAdapter.Connection;
                    connection.ConnectionString = ConnectionString;
                }
                else if( DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse )
                {
                    SqlConnection connection = ( SqlConnection )vehicleTableAdapter.Connection;
                    connection.ConnectionString = ConnectionString;
                }
                vehicleTableAdapter.Fill( ds.vehicle );

                vehicleData = ds.vehicle; // ��� ���������� �������������
            }
        }

        /// <summary>
        /// ������� �� �� ������ ��������� �� �������� ���������� �������.
        /// </summary>
        /// <exception cref="Exception">��������� ����� ������ ���� ������ ���������.</exception> 
        /// <exception cref="ArgumentNullException">
        /// � ����� GetDataViewRowsFromDb �� ������� ������ atlantaDataSet.</exception>
        /// <param name="dataset">������� ����� ������ ��� �������� ����� ������ dataviewRow.</param>
        /// <param name="mobitelId">������������� ���������.</param>
        /// <param name="startTime">��������� �����.</param>
        /// <param name="finishTime">�������� �����.</param>
        /// <returns>������ ����� dataviewRow.</returns>
        public static List<atlantaDataSet.dataviewRow> GetDataViewRowsFromDb(atlantaDataSet dataset,
            int mobitelId, DateTime startTime, DateTime finishTime)
        {
            if (String.IsNullOrEmpty(ConnectionString))
                throw new Exception("Not specify a connection string to the database");

            if (dataset == null)
            {
                throw new ArgumentNullException("dataset",
                    "In GetDataViewRowsFromDb does not give object atlantaDataSet.");
            }
            if (startTime >= finishTime)
            {
                throw new Exception("Start time must be less than the final.");
            }

            if (DriverDb.MySqlUse == DriverDb.TypeDataBaseUsing)
            {
                return new DataviewTableAdapter(ConnectionString).GetDataViewRows(
                    dataset, mobitelId, startTime, finishTime);
            }
            else if (DriverDb.MssqlUse == DriverDb.TypeDataBaseUsing)
            {
                long tmStart = StaticMethods.ConvertToUnixTimestamp(startTime);
                long tmEnd = StaticMethods.ConvertToUnixTimestamp(finishTime);

                return new DataviewTableAdapter(ConnectionString).GetDataViewRows(
                    dataset, mobitelId, tmStart, tmEnd);
            }

            return null;
        }

        public static List<GpsData> GetGpsDataFromDb(atlantaDataSet dataset, int mobitelId, DateTime startTime, DateTime finishTime)
        {
            if (String.IsNullOrEmpty(ConnectionString))
                throw new Exception("Not specify a connection string to the database");

            if (startTime >= finishTime)
            {
                //throw new Exception("Start time must be less than the final.");
                return null;
            }

            var mobitel = VehicleProvider2.GetMobitel(mobitelId);

            if (mobitel != null)
            {
                if (mobitel.Is64BitPackets)
                {
                    return GetGpsData64FromDbMobitelRow(mobitel, startTime, finishTime);
                }
                else
                {
                    List<atlantaDataSet.dataviewRow> dviews = GetDataViewRowsFromDb(dataset, mobitelId, startTime, finishTime);
                    GpsData[] gpsDatas = ConvertDataviewRowsToDataGpsArray(dviews.ToArray());
                    return gpsDatas.ToList();
                }
            }
            return null;
        }


        public static List<GpsData> GetGpsData64FromDbMobitelRow(Mobitel mobitel, DateTime startTime, DateTime finishTime)
        {
            var vRows = (atlantaDataSet.vehicleRow[])vehicleData.Select("Mobitel_id=" + mobitel.Id);
            var rowSet = ( atlantaDataSet.settingRow ) ( settingData.FindByid(vRows[0].setting_id) );
            var speedMinim = (float) rowSet.speedMin;
            return GetGpsData64FromDbMobitelId(mobitel.Id, startTime, finishTime, mobitel.IsNotDrawDgps, speedMinim);
        }

        public static List<GpsData> GetGpsData64FromDbMobitelId(int mobitelId, DateTime startTime, DateTime finishTime, bool isNotDrawDgps = false, float speedMinim = 0)
        {
            var gpsDatas = new List<GpsData>();
            using (var db = new DriverDb())
            {
                db.ConnectDb();
                db.NewSqlParameterArray(3);
                db.SetNewSqlParameter(db.ParamPrefics + "MobitelId", mobitelId);
                db.SetNewSqlParameter(db.ParamPrefics + "Begin", StaticMethods.ConvertToUnixTimestamp(startTime));
                db.SetNewSqlParameter(db.ParamPrefics + "End", StaticMethods.ConvertToUnixTimestamp(finishTime));
                db.GetDataReader(TrackControlQuery.DataviewTableAdapter.SelectDatagps64, db.GetSqlParameterArray);

                while (db.Read())
                {
                    GpsData gpsData = MobitelProvider.GetOneRecord64(db, isNotDrawDgps, speedMinim);
                    gpsDatas.Add(gpsData);
                }
                db.CloseDataReader();
            }
            return gpsDatas;
        }


        /// <summary>
        /// �������������� ������ ������� dataview � ������ DataGps.
        /// </summary>
        /// <exception cref="ArgumentNullException">
        /// � ����� ConvertDataviewRowToDataGps �� ������� ������ dataviewRow.</exception>
        /// <param name="row">dataviewRow.</param>
        /// <returns>DataGps.</returns>
        public static GpsData ConvertDataviewRowToDataGps(atlantaDataSet.dataviewRow row)
        {
            if (row == null)
            {
                throw new ArgumentNullException("row",
                    "In ConvertDataviewRowToDataGps does not give object dataviewRow.");
            }

            var result = new GpsData();
            result.Id = row.DataGps_ID;
            result.Accel = row.accel;
            result.Altitude = row.Altitude;
            result.Dist = row.dist;
            result.Events = row.Events;
            result.LatLng = new PointLatLng(row.Lat, row.Lon);
            result.Sensors = BitConverter.GetBytes(row.sensor);
            result.Speed = Convert.ToSingle(row.speed);
            result.LogId = row.LogID;
            result.Time = row.time;
            result.Valid = row.Valid > 0;
            result.Mobitel = row.Mobitel_ID;
            return result;
        }

        /// <summary>
        /// �������������� ������� ������� ������� dataview � ������ �������� DataGps.
        /// </summary>
        /// <exception cref="ArgumentNullException">
        /// � ����� ConvertDataviewRowsToDataGpsArray �� ������� ������ �������� dataviewRow.</exception>
        /// <param name="array">������ dataviewRow.</param>
        /// <returns>������ DataGps.</returns>
        public static GpsData[] ConvertDataviewRowsToDataGpsArray(atlantaDataSet.dataviewRow[] array)
        {
            if (array == null)
            {
                throw new ArgumentNullException("array",
                    "In ConvertDataviewRowsToDataGpsArray does not give massive objects dataviewRow.");
            }

            List<GpsData> result = new List<GpsData>();
            foreach (atlantaDataSet.dataviewRow row in array)
            {
                result.Add(ConvertDataviewRowToDataGps(row));
            }
            return result.ToArray();
        }

        /// <summary>
        /// ������� ������ �� ������� dataview, ����������� �
        /// ��������� ��������� � ����������� � �������� ��������� ���������.
        /// <para>����� ��������� �������(start) ���������� � �������,
        /// � ������(end) - �� ����������.</para>  
        /// </summary>
        /// <exception cref="ArgumentNullException">
        /// � ����� GetDataViewRowsExceptLastPoint �� �������� mobitelsRow.</exception>
        /// <param name="m_row">mobitelsRow.</param>
        /// <param name="start">����� ������ �������.</param>
        /// <param name="end">����� ��������� �������.</param>
        /// <returns>������ dataviewRow.</returns>
        public static GpsData[] GetGpsDataExceptLastPoint(
            atlantaDataSet.mobitelsRow m_row, DateTime start, DateTime end)
        {
            if (m_row == null)
            {
                throw new ArgumentNullException("m_row",
                    "In GetDataViewRowsExceptLastPoint does not give mobitelsRow.");
            }
            GpsData[] gpsDatas = GetDataGpsArray(m_row);

            return gpsDatas.Where(gps => gps.Time >= start && gps.Time < end).ToArray();
            //List<GpsData> result = new List<GpsData>();
            //foreach (atlantaDataSet.dataviewRow row in m_row.GetdataviewRows())
            //{
            //    if ((row.time >= start) && (row.time < end))
            //    {
            //        result.Add(ConvertDataviewRowToDataGps(row));
            //    }
            //}
            //return result.ToArray();
        }

        /// <summary>
        /// ������� ������ �� ������� dataview, ����������� �
        /// ��������� ��������� � ����������� � �������� ��������� ���������.
        /// <para>�����(start) � ������(end) ��������� ������� ���������� � �������.</para>  
        /// </summary>
        /// <exception cref="ArgumentNullException">
        /// � ����� GetDataViewRows �� �������� mobitelsRow.</exception>
        /// <param name="m_row">mobitelsRow.</param>
        /// <param name="start">����� ������ �������.</param>
        /// <param name="end">����� ��������� �������.</param>
        /// <returns>������ dataviewRow.</returns>
        public static GpsData[] GetDataGpsForPeriod(atlantaDataSet.mobitelsRow m_row, DateTime start, DateTime end)
        {
            if (m_row == null)
            {
                throw new ArgumentNullException("m_row",
                    "In GetDataViewRows does not give mobitelsRow.");
            }
            if (m_row.Is64bitPackets)
            {
                if (GpsDatas64.ContainsKey(m_row.Mobitel_ID))
                    return GpsDatas64[m_row.Mobitel_ID].Where(gps => gps.Time >= start && gps.Time <= end).ToArray();
            }
            else
            {
                List<GpsData> result = new List<GpsData>();
                foreach (atlantaDataSet.dataviewRow row in m_row.GetdataviewRows())
                {
                    if ((row.time >= start) && (row.time <= end))
                    {
                        result.Add(ConvertDataviewRowToDataGps(row));
                    }
                }
                return result.ToArray();
            }
            return null;
        }

        public static void FillTeamTable(atlantaDataSet ds)
        {
            using( var teamTableAdapter = new TeamTableAdapter() )
            {
                if( DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse )
                {
                    var connection = ( MySqlConnection )teamTableAdapter.Connection;
                    connection.ConnectionString = ConnectionString;
                }
                else if( DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse )
                {
                    var connection = ( SqlConnection )teamTableAdapter.Connection;
                    connection.ConnectionString = ConnectionString;
                }

                teamTableAdapter.Fill( ds.team );
            }
        }

        /// <summary>
        /// �������������� ������� ������� ������� dataview � ������ �������� DataGps.
        /// </summary>
        /// <exception cref="ArgumentNullException">
        /// � ����� ConvertDataviewRowsToDataGpsArray �� ������� ������ �������� dataviewRow.</exception>
        /// <param name="array">������ dataviewRow.</param>
        /// <returns>������ DataGps.</returns>
        public static GpsData[] GetDataGpsArray(atlantaDataSet.mobitelsRow mRow)
        {
                GpsData[] result = null;
                    if (mRow != null)
                    {
                        if (mRow.Is64bitPackets)
                        {
                            if (GpsDatas64.ContainsKey(mRow.Mobitel_ID))
                                result = GpsDatas64[mRow.Mobitel_ID].ToArray();
                        }
                        else
                        {
                            atlantaDataSet.dataviewRow[] array = mRow.GetdataviewRows();
                            result = ConvertDataviewRowsToDataGpsArray(array);
                            //UpdateGpsData64Sensors(result);
                        }
                    }
                    return result;
        }

        private static void UpdateGpsData64Sensors(GpsData[] gpsDatas)
        {
            using (var db = new DriverDb())
            {
                db.ConnectDb();
                string sql = "SELECT datagps64.DataGps_ID,datagps64.Sensors FROM datagps64";
                db.GetDataReader(sql);
                int i = 0;
                while (db.Read())
                {
                    using (var dbUp = new DriverDb())
                    {
                        sql = String.Format("UPDATE datagps64 SET Sensors = ?Sensors WHERE DataGps_ID = {0}",
                                            db.GetInt32("DataGps_ID"));
                        dbUp.ConnectDb();
                        dbUp.NewSqlParameterArray(1);
                        dbUp.SetNewSqlParameter("?Sensors", gpsDatas[i].Sensors);
                        dbUp.ExecuteNonQueryCommand(sql, dbUp.GetSqlParameterArray);
                        if (i < gpsDatas.Length) i++;
                    }
                }

            }
        }

        public static bool IsGpsDataExist(atlantaDataSet.mobitelsRow mRow)
        {
            if (mRow == null) return false;
            
            if (mRow.Is64bitPackets)
            {
                if (GpsDatas64.ContainsKey(mRow.Mobitel_ID))
                    return GpsDatas64[mRow.Mobitel_ID].Count > 0;
                else
                    return false;
            }
            else
                return mRow.GetdataviewRows().Length > 0;
        }
    }
}
