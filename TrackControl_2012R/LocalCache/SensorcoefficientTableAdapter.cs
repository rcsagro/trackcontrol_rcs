using System;
using System.Configuration;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using TrackControl.General.DAL;
using TrackControl.General.DatabaseDriver;

namespace LocalCache.atlantaDataSetTableAdapters
{
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Design.TypedDataSetGenerator", "2.0.0.0")]
    [global::System.ComponentModel.DesignerCategoryAttribute("code")]
    [global::System.ComponentModel.ToolboxItem(true)]
    [global::System.ComponentModel.DataObjectAttribute(true)]
    [global::System.ComponentModel.DesignerAttribute("Microsoft.VSDesigner.DataSource.Design.TableAdapterDesigner, Microsoft.VSDesigner" +
            ", Version=8.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")]
    [global::System.ComponentModel.Design.HelpKeywordAttribute("vs.data.TableAdapter")]
    public class SensorcoefficientTableAdapter : global::System.ComponentModel.Component
    {

        //private global::MySql.Data.MySqlClient.MySqlDataAdapter _adapter;
        //private global::MySql.Data.MySqlClient.MySqlConnection _connection;
        //private global::MySql.Data.MySqlClient.MySqlCommand[] _commandCollection;
        private DriverDb db = new DriverDb();

        private bool _clearBeforeFill;

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public SensorcoefficientTableAdapter()
        {
            this.ClearBeforeFill = true;
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        private object Adapter
        {
            get
            {
                if ((this.db.Adapter == null))
                {
                    this.InitAdapter();
                }

                return this.db.Adapter;
            }
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public object Connection
        {
            get
            {
                if ((this.db.Connection == null))
                {
                    this.InitConnection();
                }

                return this.db.Connection;
            }
            set
            {
                this.db.Connection = value;

                if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                {
                    MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;

                    if ((adapter.InsertCommand != null))
                    {
                        adapter.InsertCommand.Connection = (MySqlConnection) value;
                    }

                    if ((adapter.DeleteCommand != null))
                    {
                        adapter.DeleteCommand.Connection = (MySqlConnection) value;
                    }

                    if ((adapter.UpdateCommand != null))
                    {
                        adapter.UpdateCommand.Connection = (MySqlConnection) value;
                    }

                    MySqlCommand[] collection = (MySqlCommand[]) this.CommandCollection;

                    for (int i = 0; (i < this.CommandCollection.Length); i = (i + 1))
                    {
                        if ((this.CommandCollection[i] != null))
                        {
                            collection[i].Connection = (MySqlConnection) value;
                        }
                    } // fot
                } // if
                else if( DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse )
                {
                    SqlDataAdapter adapter = (SqlDataAdapter)this.Adapter;

                    if( ( adapter.InsertCommand != null ) )
                    {
                        adapter.InsertCommand.Connection = (SqlConnection)value;
                    }

                    if( ( adapter.DeleteCommand != null ) )
                    {
                        adapter.DeleteCommand.Connection = (SqlConnection)value;
                    }

                    if( ( adapter.UpdateCommand != null ) )
                    {
                        adapter.UpdateCommand.Connection = (SqlConnection)value;
                    }

                    SqlCommand[] collection = (SqlCommand[])this.CommandCollection;

                    for( int i = 0; ( i < this.CommandCollection.Length ); i = ( i + 1 ) )
                    {
                        if( ( this.CommandCollection[i] != null ) )
                        {
                            collection[i].Connection = (SqlConnection)value;
                        }
                    } // fot
                } // else if
            } // set
        } //  Conenction

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        //protected global::MySql.Data.MySqlClient.MySqlCommand[] CommandCollection
        protected object[] CommandCollection
        {
            get
            {
                if ((this.db.CommandCollection == null))
                {
                    this.InitCommandCollection();
                }

                return this.db.CommandCollection;
            }
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public bool ClearBeforeFill
        {
            get
            {
                return this._clearBeforeFill;
            }
            set
            {
                this._clearBeforeFill = value;
            }
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        private void InitAdapter()
        {
            //this._adapter = new global::MySql.Data.MySqlClient.MySqlDataAdapter();
            this.db.NewDataAdapter();
            global::System.Data.Common.DataTableMapping tableMapping = new global::System.Data.Common.DataTableMapping();
            tableMapping.SourceTable = "Table";
            tableMapping.DataSetTable = "sensorcoefficient";
            tableMapping.ColumnMappings.Add("id", "id");
            tableMapping.ColumnMappings.Add("UserValue", "UserValue");
            tableMapping.ColumnMappings.Add("SensorValue", "SensorValue");
            tableMapping.ColumnMappings.Add("K", "K");
            tableMapping.ColumnMappings.Add("b", "b");
            tableMapping.ColumnMappings.Add("Sensor_id", "Sensor_id");
            //this._adapter.TableMappings.Add(tableMapping);
            this.db.AdapterTableMappingsAdd(tableMapping);

            // DELETE
            //this._adapter.DeleteCommand = new global::MySql.Data.MySqlClient.MySqlCommand();
            this.db.AdapterDeleteCommand();
            //this._adapter.DeleteCommand.Connection = this.Connection;
            this.db.AdapterDeleteCommandConnection(this.Connection);
            //this._adapter.DeleteCommand.CommandText = "DELETE FROM sensorcoefficient WHERE id = ?id";
            this.db.AdapterDeleteCommandText( "DELETE FROM sensorcoefficient WHERE id = " + db.ParamPrefics + "id" );
            //this._adapter.DeleteCommand.CommandType = System.Data.CommandType.Text;
            this.db.AdapterDeleteCommandType( System.Data.CommandType.Text );

            //MySql.Data.MySqlClient.MySqlParameter param = new MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?id";
            this.db.ParameterName( db.ParamPrefics + "id" );
            //param.DbType = System.Data.DbType.Int32;
            this.db.ParameterDbType( System.Data.DbType.Int32 );
            //param.MySqlDbType = MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType( db.GettingInt32() );
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "id";
            this.db.ParameterSourceColumn("id");
            //param.SourceVersion = System.Data.DataRowVersion.Original;
            this.db.ParameterSourceVersion( System.Data.DataRowVersion.Original );
            //this._adapter.DeleteCommand.Parameters.Add(param);
            this.db.AdapterDeleteCommandParametersAdd();

            // INSERT
            //this._adapter.InsertCommand = new MySql.Data.MySqlClient.MySqlCommand();
            this.db.AdapterInsertCommand();
            //this._adapter.InsertCommand.Connection = this.Connection;
            this.db.AdapterInsertCommandConnection(this.Connection);
            //this._adapter.InsertCommand.UpdatedRowSource = System.Data.UpdateRowSource.FirstReturnedRecord;
            this.db.AdapterInsertCommandUpdatedRowSource( System.Data.UpdateRowSource.FirstReturnedRecord );
            //this._adapter.InsertCommand.CommandText = "INSERT INTO sensorcoefficient ( UserValue, SensorValue, K, b, Sensor_id )" +
            //    " VALUES ( ?UserValue, ?SensorValue, ?K, ?b, ?Sensor_id)"; //Select ?id = LAST_INSERT_ID()";

            string sqlstr = TrackControlQuery.SensorcoefficientTableAdapter.InsertIntoSensorcoefficient;
          
            this.db.AdapterInsertCommandText( sqlstr );
            //this._adapter.InsertCommand.CommandType = System.Data.CommandType.Text;
            this.db.AdapterInsertCommandType( System.Data.CommandType.Text );

            //param = new MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?id";
            this.db.ParameterName( db.ParamPrefics + "id" );
            //param.DbType = System.Data.DbType.Int32;
            this.db.ParameterDbType( System.Data.DbType.Int32 );
            //param.MySqlDbType = MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType( db.GettingInt32() );
            //param.Direction = System.Data.ParameterDirection.Output;
            this.db.ParameterDirection(System.Data.ParameterDirection.Output);
            //param.IsNullable = false;
            this.db.ParameterIsNullable(false);
            //param.SourceColumn = "id";
            this.db.ParameterSourceColumn( "id" );
            //this._adapter.InsertCommand.Parameters.Add(param);
            this.db.AdapterInsertCommandParametersAdd();

            //param = new MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?UserValue";
            this.db.ParameterName(db.ParamPrefics + "UserValue");
            //param.DbType = System.Data.DbType.Double;
            this.db.ParameterDbType( System.Data.DbType.Double );
            //param.MySqlDbType = MySql.Data.MySqlClient.MySqlDbType.Double;
            this.db.ParameterSqlDbType( db.GettingDouble() );
            //param.IsNullable = false;
            this.db.ParameterIsNullable(false);
            //param.SourceColumn = "UserValue";
            this.db.ParameterSourceColumn( "UserValue" );
            //this._adapter.InsertCommand.Parameters.Add(param);
            this.db.AdapterInsertCommandParametersAdd();

            //param = new MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?SensorValue";
            this.db.ParameterName( db.ParamPrefics + "SensorValue" );
            //param.DbType = System.Data.DbType.Double;
            this.db.ParameterDbType( System.Data.DbType.Double );
            //param.MySqlDbType = MySql.Data.MySqlClient.MySqlDbType.Double;
            this.db.ParameterSqlDbType( db.GettingDouble() );
            //param.IsNullable = false;
            this.db.ParameterIsNullable(false);
            //param.SourceColumn = "SensorValue";
            this.db.ParameterSourceColumn( "SensorValue" );
            //this._adapter.InsertCommand.Parameters.Add(param);
            this.db.AdapterInsertCommandParametersAdd();

            //param = new MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?K";
            this.db.ParameterName( db.ParamPrefics + "K" );
           // param.DbType = System.Data.DbType.Double;
            this.db.ParameterDbType( System.Data.DbType.Double );
            //param.MySqlDbType = MySql.Data.MySqlClient.MySqlDbType.Double;
            this.db.ParameterSqlDbType( db.GettingDouble() );
            //param.IsNullable = false;
            this.db.ParameterIsNullable(false);
            //param.SourceColumn = "K";
            this.db.ParameterSourceColumn("K");
            //this._adapter.InsertCommand.Parameters.Add(param);
            this.db.AdapterInsertCommandParametersAdd();

            //param = new MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?b";
            this.db.ParameterName( db.ParamPrefics + "b" );
            //param.DbType = System.Data.DbType.Double;
            this.db.ParameterDbType( System.Data.DbType.Double );
            //param.MySqlDbType = MySql.Data.MySqlClient.MySqlDbType.Double;
            this.db.ParameterSqlDbType( db.GettingDouble() );
            //param.IsNullable = false;
            this.db.ParameterIsNullable(false);
            //param.SourceColumn = "b";
            this.db.ParameterSourceColumn( "b" );
            //this._adapter.InsertCommand.Parameters.Add(param);
            this.db.AdapterInsertCommandParametersAdd();

            //param = new MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?Sensor_id";
            this.db.ParameterName( db.ParamPrefics + "Sensor_id" );
            //param.DbType = System.Data.DbType.Int32;
            this.db.ParameterDbType( System.Data.DbType.Int32 );
            //param.MySqlDbType = MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType( db.GettingDouble() );
           // param.IsNullable = false;
            this.db.ParameterIsNullable(false);
            //param.SourceColumn = "Sensor_id";
            this.db.ParameterSourceColumn( "Sensor_id" );
            //this._adapter.InsertCommand.Parameters.Add(param);
            this.db.AdapterInsertCommandParametersAdd();

            // UPDATE
            //this._adapter.UpdateCommand = new global::MySql.Data.MySqlClient.MySqlCommand();
            this.db.AdapterUpdateCommand();
            //this._adapter.UpdateCommand.Connection = this.Connection;
            this.db.AdapterUpdateCommandConnection(this.Connection);
            //this._adapter.UpdateCommand.CommandText =
            ////  "UPDATE sensorcoefficient\r\n " +
            ////  "SET UserValue = ?UserValue, SensorValue = ?SensorValue," +
            ////  "  K = ?K, b = ?b, Sensor_id = ?Sensor_id WHERE id = ?id";
            //"UPDATE sensorcoefficient SET Sensor_id = ?Sensor_id, UserValue = ?Us" +
            //    "erValue, SensorValue = ?SensorValue, K = ?K, b = ?b WHERE id = ?id";
            string sqlstr1 = TrackControlQuery.SensorcoefficientTableAdapter.UpdateSensorcoefficient;
         
            this.db.AdapterUpdateCommandText( sqlstr1 );
            //this._adapter.UpdateCommand.CommandType = global::System.Data.CommandType.Text;
            this.db.AdapterUpdateCommandType( global::System.Data.CommandType.Text );

            //param = new MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?id";
            this.db.ParameterName( db.ParamPrefics + "id" );
            //param.DbType = System.Data.DbType.Int32;
            this.db.ParameterDbType( System.Data.DbType.Int32 );
            //param.MySqlDbType = MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType( db.GettingInt32() );
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "id";
            this.db.ParameterSourceColumn( "id" );
            //param.SourceVersion = global::System.Data.DataRowVersion.Original;
            this.db.ParameterSourceVersion( global::System.Data.DataRowVersion.Original );
            //this._adapter.UpdateCommand.Parameters.Add(param);
            this.db.AdapterUpdateCommandParametersAdd();

            //param = new MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?UserValue";
            this.db.ParameterName( db.ParamPrefics + "UserValue" );
            //param.DbType = System.Data.DbType.Double;
            this.db.ParameterDbType( System.Data.DbType.Double );
            //param.MySqlDbType = MySql.Data.MySqlClient.MySqlDbType.Double;
            this.db.ParameterSqlDbType( db.GettingDouble() );
            //param.IsNullable = false;
            this.db.ParameterIsNullable(false);
            //param.SourceColumn = "UserValue";
            this.db.ParameterSourceColumn( "UserValue" );
            //this._adapter.UpdateCommand.Parameters.Add(param);
            this.db.AdapterUpdateCommandParametersAdd();

            //param = new MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?SensorValue";
            this.db.ParameterName( db.ParamPrefics + "SensorValue" );
            //param.DbType = System.Data.DbType.Double;
            this.db.ParameterDbType( System.Data.DbType.Double );
            //param.MySqlDbType = MySql.Data.MySqlClient.MySqlDbType.Double;
            this.db.ParameterSqlDbType( db.GettingDouble() );
            //param.IsNullable = false;
            this.db.ParameterIsNullable(false);
            //param.SourceColumn = "SensorValue";
            this.db.ParameterSourceColumn( "SensorValue" );
            //this._adapter.UpdateCommand.Parameters.Add(param);
            this.db.AdapterUpdateCommandParametersAdd();

            //param = new MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?K";
            this.db.ParameterName( db.ParamPrefics + "K" );
            //param.DbType = System.Data.DbType.Double;
            this.db.ParameterDbType( System.Data.DbType.Double );
            //param.MySqlDbType = MySql.Data.MySqlClient.MySqlDbType.Double;
            this.db.ParameterSqlDbType( db.GettingDouble() );
            //param.IsNullable = false;
            this.db.ParameterIsNullable(false);
            //param.SourceColumn = "K";
            this.db.ParameterSourceColumn( "K" );
            //this._adapter.UpdateCommand.Parameters.Add(param);
            this.db.AdapterUpdateCommandParametersAdd();

            //param = new MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?b";
            this.db.ParameterName( db.ParamPrefics + "b" );
            //param.DbType = System.Data.DbType.Double;
            this.db.ParameterDbType( System.Data.DbType.Double );
            //param.MySqlDbType = MySql.Data.MySqlClient.MySqlDbType.Double;
            this.db.ParameterSqlDbType( db.GettingDouble() );
            //param.IsNullable = false;
            this.db.ParameterIsNullable( false );
            //param.SourceColumn = "b";
            this.db.ParameterSourceColumn( "b" );
            //this._adapter.UpdateCommand.Parameters.Add(param);
            this.db.AdapterUpdateCommandParametersAdd();

           // param = new MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?Sensor_id";
            this.db.ParameterName( db.ParamPrefics + "Sensor_id" );
            //param.DbType = System.Data.DbType.Int32;
            this.db.ParameterDbType( System.Data.DbType.Int32 );
            //param.MySqlDbType = MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType( this.db.GettingInt32() );
            //param.IsNullable = false;
            this.db.ParameterIsNullable(false);
            //param.SourceColumn = "Sensor_id";
            this.db.ParameterSourceColumn( "Sensor_id" );
            //this._adapter.UpdateCommand.Parameters.Add(param);
            this.db.AdapterUpdateCommandParametersAdd();
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        private void InitConnection()
        {
            //this._connection = new global::MySql.Data.MySqlClient.MySqlConnection();
            this.db.AdapNewConnection();
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        private void InitCommandCollection()
        {
            //this._commandCollection = new global::MySql.Data.MySqlClient.MySqlCommand[1];
            this.db.NewCommandCollection(1);
            //this._commandCollection[0] = new global::MySql.Data.MySqlClient.MySqlCommand();
            this.db.SetNewCommandCollection(0);
            //this._commandCollection[0].Connection = this.Connection;
            this.db.SetCommandCollectionConnection(this.Connection, 0);
            //this._commandCollection[0].CommandText = "SELECT  id, UserValue, SensorValue, K, b, Sensor_id FROM sensorcoefficient";
            this.db.SetCommandCollectionText( "SELECT  id, UserValue, SensorValue, K, b, Sensor_id FROM sensorcoefficient", 0);
            //this._commandCollection[0].CommandType = global::System.Data.CommandType.Text;
            this.db.SetCommandCollectionType( global::System.Data.CommandType.Text, 0);
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.ComponentModel.Design.HelpKeywordAttribute("vs.data.TableAdapter")]
        [global::System.ComponentModel.DataObjectMethodAttribute(global::System.ComponentModel.DataObjectMethodType.Fill, true)]
        public virtual int Fill(atlantaDataSet.sensorcoefficientDataTable dataTable)
        {
            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;
                adapter.SelectCommand = (MySqlCommand) this.CommandCollection[0];
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                SqlDataAdapter adapter = (SqlDataAdapter)this.Adapter;
                adapter.SelectCommand = (SqlCommand) this.CommandCollection[0];
            }

            if ((this.ClearBeforeFill == true))
            {
                dataTable.Clear();
            }

            int returnValue = 0;

            if( DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse )
            {
                MySqlDataAdapter adapter = (MySqlDataAdapter)this.Adapter;
                returnValue = adapter.Fill( dataTable );
            }
            else if( DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse )
            {
                SqlDataAdapter adapter = (SqlDataAdapter)this.Adapter;
                returnValue = adapter.Fill( dataTable );
            }

            return returnValue;
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.ComponentModel.Design.HelpKeywordAttribute("vs.data.TableAdapter")]
        [global::System.ComponentModel.DataObjectMethodAttribute(global::System.ComponentModel.DataObjectMethodType.Select, true)]
        public virtual atlantaDataSet.sensorcoefficientDataTable GetData()
        {
            if( DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse )
            {
                MySqlDataAdapter adapter = (MySqlDataAdapter)this.Adapter;
                adapter.SelectCommand = (MySqlCommand) this.CommandCollection[0];
            }
            else if( DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse )
            {
                SqlDataAdapter adapter = (SqlDataAdapter)this.Adapter;
                adapter.SelectCommand = (SqlCommand)this.CommandCollection[0];
            }

            atlantaDataSet.sensorcoefficientDataTable dataTable = new atlantaDataSet.sensorcoefficientDataTable();

            if( DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse )
            {
                MySqlDataAdapter adapter = (MySqlDataAdapter)this.Adapter;
                adapter.Fill( dataTable );
            }
            else if( DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse )
            {
                SqlDataAdapter adapter = (SqlDataAdapter)this.Adapter;
                adapter.Fill( dataTable );
            }

            return dataTable;
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.ComponentModel.Design.HelpKeywordAttribute("vs.data.TableAdapter")]
        public virtual int Update(atlantaDataSet.sensorcoefficientDataTable dataTable)
        {
            if( DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse )
            {
                MySqlDataAdapter adapter = (MySqlDataAdapter)this.Adapter;
                return adapter.Update( dataTable );
            }
            else if( DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse )
            {
                SqlDataAdapter adapter = (SqlDataAdapter)this.Adapter;
                return adapter.Update( dataTable );
            }

            return 0;
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.ComponentModel.Design.HelpKeywordAttribute("vs.data.TableAdapter")]
        public virtual int Update(global::System.Data.DataRow[] dataRows)
        {
            if( DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse )
            {
                MySqlDataAdapter adapter = (MySqlDataAdapter)this.Adapter;
                return adapter.Update( dataRows );
            }
            else if( DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse )
            {
                SqlDataAdapter adapter = (SqlDataAdapter)this.Adapter;
                return adapter.Update( dataRows );
            }

            return 0;
        }
    }
}
