using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using TrackControl.General.DAL;
using TrackControl.General.DatabaseDriver;

namespace LocalCache.atlantaDataSetTableAdapters
{
    /// <summary>
    ///Represents the connection and commands used to retrieve and save data.
    ///</summary>
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Design.TypedDataSetGenerator", "2.0.0.0")]
    [global::System.ComponentModel.DesignerCategoryAttribute("code")]
    [global::System.ComponentModel.ToolboxItem(true)]
    [global::System.ComponentModel.DataObjectAttribute(true)]
    [global::System.ComponentModel.DesignerAttribute(
        "Microsoft.VSDesigner.DataSource.Design.TableAdapterDesigner, Microsoft.VSDesigner" +
        ", Version=8.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")]
    [global::System.ComponentModel.Design.HelpKeywordAttribute("vs.data.TableAdapter")]
    public class TransitionTableAdapter : global::System.ComponentModel.Component
    {

        //private global::MySql.Data.MySqlClient.MySqlDataAdapter _adapter;
        //private global::MySql.Data.MySqlClient.MySqlConnection _connection;
        //private global::MySql.Data.MySqlClient.MySqlCommand[] _commandCollection;

        private DriverDb db = new DriverDb();

        private bool _clearBeforeFill;

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public TransitionTableAdapter()
        {
            this.ClearBeforeFill = true;
        }

        // [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        private object Adapter
        {
            get
            {
                if ((this.db.Adapter == null))
                {
                    this.InitAdapter();
                }

                return this.db.Adapter;
            }
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public object Connection
        {
            get
            {
                if ((this.db.Connection == null))
                {
                    this.InitConnection();
                }

                return this.db.Connection;
            }
            set
            {
                this.db.Connection = value;

                if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                {
                    MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;

                    if ((adapter.InsertCommand != null))
                    {
                        adapter.InsertCommand.Connection = (MySqlConnection) value;
                    }

                    if ((adapter.DeleteCommand != null))
                    {
                        adapter.DeleteCommand.Connection = (MySqlConnection) value;
                    }

                    if ((adapter.UpdateCommand != null))
                    {
                        adapter.UpdateCommand.Connection = (MySqlConnection) value;
                    }

                    MySqlCommand[] collection = (MySqlCommand[]) this.CommandCollection;

                    for (int i = 0; (i < this.CommandCollection.Length); i = (i + 1))
                    {
                        if ((this.CommandCollection[i] != null))
                        {
                            collection[i].Connection = (MySqlConnection) value;
                        }
                    } // for
                } // if
                else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                {
                    SqlDataAdapter adapter = (SqlDataAdapter) this.Adapter;

                    if ((adapter.InsertCommand != null))
                    {
                        adapter.InsertCommand.Connection = (SqlConnection) value;
                    }

                    if ((adapter.DeleteCommand != null))
                    {
                        adapter.DeleteCommand.Connection = (SqlConnection) value;
                    }

                    if ((adapter.UpdateCommand != null))
                    {
                        adapter.UpdateCommand.Connection = (SqlConnection) value;
                    }

                    SqlCommand[] collection = (SqlCommand[]) this.CommandCollection;

                    for (int i = 0; (i < this.CommandCollection.Length); i = (i + 1))
                    {
                        if ((this.CommandCollection[i] != null))
                        {
                            collection[i].Connection = (SqlConnection) value;
                        }
                    } // for
                } // else if
            } // set
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        //protected global::MySql.Data.MySqlClient.MySqlCommand[] CommandCollection
        protected object[] CommandCollection
        {
            get
            {
                if ((this.db.CommandCollection == null))
                {
                    this.InitCommandCollection();
                }

                return this.db.CommandCollection;
            }
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public bool ClearBeforeFill
        {
            get
            {
                return this._clearBeforeFill;
            }
            set
            {
                this._clearBeforeFill = value;
            }
        }

        // [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        private void InitAdapter()
        {
            this.db.NewDataAdapter(); //_adapter = new global::MySql.Data.MySqlClient.MySqlDataAdapter();
            global::System.Data.Common.DataTableMapping tableMapping = new global::System.Data.Common.DataTableMapping();
            tableMapping.SourceTable = "Table";
            tableMapping.DataSetTable = "Transition";
            tableMapping.ColumnMappings.Add("Id", "Id");
            tableMapping.ColumnMappings.Add("InitialStateId", "InitialStateId");
            tableMapping.ColumnMappings.Add("FinalStateId", "FinalStateId");
            tableMapping.ColumnMappings.Add("Title", "Title");
            tableMapping.ColumnMappings.Add("SensorId", "SensorId");
            tableMapping.ColumnMappings.Add("IconName", "IconName");
            tableMapping.ColumnMappings.Add("SoundName", "SoundName");
            tableMapping.ColumnMappings.Add("Enabled", "Enabled");
            tableMapping.ColumnMappings.Add("ViewInReport", "ViewInReport");
            //this._adapter.TableMappings.Add(tableMapping);
            this.db.AdapterTableMappingsAdd(tableMapping);

            // DELETE .........................................................

            //this._adapter.DeleteCommand = new global::MySql.Data.MySqlClient.MySqlCommand();
            this.db.AdapterDeleteCommand();
            //this._adapter.DeleteCommand.Connection = this.Connection;
            this.db.AdapterDeleteCommandConnection(this.Connection);
            //this._adapter.DeleteCommand.CommandText = @"DELETE FROM `Transition` WHERE ((`Id` = @Original_Id) AND ((@IsNull_InitialStateId = 1 AND `InitialStateId` IS NULL) OR (`InitialStateId` = @Original_InitialStateId)) AND ((@IsNull_FinalStateId = 1 AND `FinalStateId` IS NULL) OR (`FinalStateId` = @Original_FinalStateId)) AND (`Title` = @Original_Title) AND ((@IsNull_SensorId = 1 AND `SensorId` IS NULL) OR (`SensorId` = @Original_SensorId)) AND (`IconName` = @Original_IconName) AND (`SoundName` = @Original_SoundName) AND (`Enabled` = @Original_Enabled) AND (`ViewInReport` = @Original_ViewInReport))";

            this.db.AdapterDeleteCommandText(TrackControlQuery.TransitionTableAdapter.DeleteFromTransition);

            //this._adapter.DeleteCommand.CommandType = global::System.Data.CommandType.Text;
            this.db.AdapterDeleteCommandType(global::System.Data.CommandType.Text);

            //global::MySql.Data.MySqlClient.MySqlParameter param = new global::MySql.Data.MySqlClient.MySqlParameter();
            //param.ParameterName = "@Original_Id";
            this.db.NewSqlParameter();
            this.db.ParameterName("@Original_Id");
            //param.DbType = global::System.Data.DbType.Int32;
            this.db.ParameterDbType(global::System.Data.DbType.Int32);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType(db.GettingInt32());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "Id";
            this.db.ParameterSourceColumn("Id");
            //param.SourceVersion = global::System.Data.DataRowVersion.Original;
            this.db.ParameterSourceVersion(global::System.Data.DataRowVersion.Original);
            //this._adapter.DeleteCommand.Parameters.Add(param);
            this.db.AdapterDeleteCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "@IsNull_InitialStateId";
            this.db.ParameterName("@IsNull_InitialStateId");
            //param.DbType = global::System.Data.DbType.Int32;
            this.db.ParameterDbType(global::System.Data.DbType.Int32);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType(db.GettingInt32());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "InitialStateId";
            this.db.ParameterSourceColumn("InitialStateId");
            //param.SourceVersion = global::System.Data.DataRowVersion.Original;
            this.db.ParameterSourceVersion(global::System.Data.DataRowVersion.Original);
            //param.SourceColumnNullMapping = true;
            this.db.ParameterSourceColumnNullMapping(true);
            //this._adapter.DeleteCommand.Parameters.Add(param);
            this.db.AdapterDeleteCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "@Original_InitialStateId";
            this.db.ParameterName("@Original_InitialStateId");
            //param.DbType = global::System.Data.DbType.Int32;
            this.db.ParameterDbType(global::System.Data.DbType.Int32);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType(db.GettingInt32());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "InitialStateId";
            this.db.ParameterSourceColumn("InitialStateId");
            //param.SourceVersion = global::System.Data.DataRowVersion.Original;
            this.db.ParameterSourceVersion(global::System.Data.DataRowVersion.Original);
            //this._adapter.DeleteCommand.Parameters.Add(param);
            this.db.AdapterDeleteCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "@IsNull_FinalStateId";
            this.db.ParameterName("@IsNull_FinalStateId");
            //param.DbType = global::System.Data.DbType.Int32;
            this.db.ParameterDbType(global::System.Data.DbType.Int32);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType(db.GettingInt32());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "FinalStateId";
            this.db.ParameterSourceColumn("FinalStateId");
            //param.SourceVersion = global::System.Data.DataRowVersion.Original;
            this.db.ParameterSourceVersion(global::System.Data.DataRowVersion.Original);
            //param.SourceColumnNullMapping = true;
            this.db.ParameterSourceColumnNullMapping(true);
            //this._adapter.DeleteCommand.Parameters.Add(param);
            this.db.AdapterDeleteCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "@Original_FinalStateId";
            this.db.ParameterName("@Original_FinalStateId");
            //param.DbType = global::System.Data.DbType.Int32;
            this.db.ParameterDbType(global::System.Data.DbType.Int32);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType(db.GettingInt32());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "FinalStateId";
            this.db.ParameterSourceColumn("FinalStateId");
            //param.SourceVersion = global::System.Data.DataRowVersion.Original;
            this.db.ParameterSourceVersion(global::System.Data.DataRowVersion.Original);
            //this._adapter.DeleteCommand.Parameters.Add(param);
            this.db.AdapterDeleteCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "@Original_Title";
            this.db.ParameterName("@Original_Title");
            //param.DbType = global::System.Data.DbType.String;
            this.db.ParameterDbType(global::System.Data.DbType.String);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.VarChar;
            this.db.ParameterSqlDbType(db.GettingVarChar());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "Title";
            this.db.ParameterSourceColumn("Title");
            //param.SourceVersion = global::System.Data.DataRowVersion.Original;
            this.db.ParameterSourceVersion(global::System.Data.DataRowVersion.Original);
            //this._adapter.DeleteCommand.Parameters.Add(param);
            this.db.AdapterDeleteCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "@IsNull_SensorId";
            this.db.ParameterName("@IsNull_SensorId");
            //param.DbType = global::System.Data.DbType.Int32;
            this.db.ParameterDbType(global::System.Data.DbType.Int32);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType(db.GettingInt32());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "SensorId";
            this.db.ParameterSourceColumn("SensorId");
            //param.SourceVersion = global::System.Data.DataRowVersion.Original;
            this.db.ParameterSourceVersion(global::System.Data.DataRowVersion.Original);
            //param.SourceColumnNullMapping = true;
            this.db.ParameterSourceColumnNullMapping(true);
            //this._adapter.DeleteCommand.Parameters.Add(param);
            this.db.AdapterDeleteCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "@Original_SensorId";
            this.db.ParameterName("@Original_SensorId");
            //param.DbType = global::System.Data.DbType.Int32;
            this.db.ParameterDbType(global::System.Data.DbType.Int32);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType(db.GettingInt32());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "SensorId";
            this.db.ParameterSourceColumn("SensorId");
            //param.SourceVersion = global::System.Data.DataRowVersion.Original;
            this.db.ParameterSourceVersion(global::System.Data.DataRowVersion.Original);
            //this._adapter.DeleteCommand.Parameters.Add(param);
            this.db.AdapterDeleteCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "@Original_IconName";
            this.db.ParameterName("@Original_IconName");
            //param.DbType = global::System.Data.DbType.String;
            this.db.ParameterDbType(global::System.Data.DbType.String);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.VarChar;
            this.db.ParameterSqlDbType(db.GettingVarChar());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "IconName";
            this.db.ParameterSourceColumn("IconName");
            //param.SourceVersion = global::System.Data.DataRowVersion.Original;
            this.db.ParameterSourceVersion(global::System.Data.DataRowVersion.Original);
            //this._adapter.DeleteCommand.Parameters.Add(param);
            this.db.AdapterDeleteCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "@Original_SoundName";
            this.db.ParameterName("@Original_SoundName");
            //param.DbType = global::System.Data.DbType.String;
            this.db.ParameterDbType(global::System.Data.DbType.String);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.VarChar;
            this.db.ParameterSqlDbType(db.GettingVarChar());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "SoundName";
            this.db.ParameterSourceColumn("SoundName");
            //param.SourceVersion = global::System.Data.DataRowVersion.Original;
            this.db.ParameterSourceVersion(global::System.Data.DataRowVersion.Original);
            //this._adapter.DeleteCommand.Parameters.Add(param);
            this.db.AdapterDeleteCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "@Original_Enabled";
            this.db.ParameterName("@Original_Enabled");
            //param.DbType = global::System.Data.DbType.SByte;
            this.db.ParameterDbType(global::System.Data.DbType.Byte);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Byte;
            this.db.ParameterSqlDbType(db.GettingByte());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "Enabled";
            this.db.ParameterSourceColumn("Enabled");
            //param.SourceVersion = global::System.Data.DataRowVersion.Original;
            this.db.ParameterSourceVersion(global::System.Data.DataRowVersion.Original);
            //this._adapter.DeleteCommand.Parameters.Add(param);
            this.db.AdapterDeleteCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "@Original_ViewInReport";
            this.db.ParameterName("@Original_ViewInReport");
            //param.DbType = global::System.Data.DbType.SByte;
            this.db.ParameterDbType(global::System.Data.DbType.Byte);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Byte;
            this.db.ParameterSqlDbType(db.GettingByte());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "ViewInReport";
            this.db.ParameterSourceColumn("ViewInReport");
            //param.SourceVersion = global::System.Data.DataRowVersion.Original;
            this.db.ParameterSourceVersion(global::System.Data.DataRowVersion.Original);
            //this._adapter.DeleteCommand.Parameters.Add(param);
            this.db.AdapterDeleteCommandParametersAdd();

            // INSERT .............................................................

            // this._adapter.InsertCommand = new global::MySql.Data.MySqlClient.MySqlCommand();
            this.db.AdapterInsertCommand();
            //this._adapter.InsertCommand.Connection = this.Connection;
            this.db.AdapterInsertCommandConnection(this.Connection);
            //this._adapter.InsertCommand.CommandText = "INSERT INTO `Transition` (`InitialStateId`, `FinalStateId`, `Title`, `" +
            //    "SensorId`, `IconName`, `SoundName`, `Enabled`, `ViewInReport`) VALUES (@InitialStateId, @FinalSt" +
            //    "ateId, @Title, @SensorId, @IconName, @SoundName, @Enabled, @ViewInReport); SELECT * FROM Transition where Id = LAST_INSERT_ID()";

            this.db.AdapterInsertCommandText(TrackControlQuery.TransitionTableAdapter.InsertIntoTransition);

            //this._adapter.InsertCommand.CommandType = global::System.Data.CommandType.Text;
            this.db.AdapterInsertCommandType(global::System.Data.CommandType.Text);
            //this._adapter.InsertCommand.UpdatedRowSource = System.Data.UpdateRowSource.FirstReturnedRecord;
            this.db.AdapterInsertCommandUpdatedRowSource(System.Data.UpdateRowSource.FirstReturnedRecord);

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "@InitialStateId";
            this.db.ParameterName("@InitialStateId");
            //param.DbType = global::System.Data.DbType.Int32;
            this.db.ParameterDbType(global::System.Data.DbType.Int32);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType(db.GettingInt32());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "InitialStateId";
            this.db.ParameterSourceColumn("InitialStateId");
            //this._adapter.InsertCommand.Parameters.Add(param);
            this.db.AdapterInsertCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "@FinalStateId";
            this.db.ParameterName("@FinalStateId");
            //param.DbType = global::System.Data.DbType.Int32;
            this.db.ParameterDbType(global::System.Data.DbType.Int32);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType(db.GettingInt32());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "FinalStateId";
            this.db.ParameterSourceColumn("FinalStateId");
            //this._adapter.InsertCommand.Parameters.Add(param);
            this.db.AdapterInsertCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "@Title";
            this.db.ParameterName("@Title");
            //param.DbType = global::System.Data.DbType.String;
            this.db.ParameterDbType(global::System.Data.DbType.String);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.VarChar;
            this.db.ParameterSqlDbType(db.GettingVarChar());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "Title";
            this.db.ParameterSourceColumn("Title");
            //this._adapter.InsertCommand.Parameters.Add(param);
            this.db.AdapterInsertCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "@SensorId";
            this.db.ParameterName("@SensorId");
            //param.DbType = global::System.Data.DbType.Int32;
            this.db.ParameterDbType(global::System.Data.DbType.Int32);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType(db.GettingInt32());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "SensorId";
            this.db.ParameterSourceColumn("SensorId");
            //this._adapter.InsertCommand.Parameters.Add(param);
            this.db.AdapterInsertCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "@IconName";
            this.db.ParameterName("@IconName");
            //param.DbType = global::System.Data.DbType.String;
            this.db.ParameterDbType(global::System.Data.DbType.String);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.VarChar;
            this.db.ParameterSqlDbType(db.GettingVarChar());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "IconName";
            this.db.ParameterSourceColumn("IconName");
            //this._adapter.InsertCommand.Parameters.Add(param);
            this.db.AdapterInsertCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "@SoundName";
            this.db.ParameterName("@SoundName");
            //param.DbType = global::System.Data.DbType.String;
            this.db.ParameterDbType(global::System.Data.DbType.String);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.VarChar;
            this.db.ParameterSqlDbType(db.GettingVarChar());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "SoundName";
            this.db.ParameterSourceColumn("SoundName");
            //this._adapter.InsertCommand.Parameters.Add(param);
            this.db.AdapterInsertCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "@Enabled";
            this.db.ParameterName("@Enabled");
            //param.DbType = global::System.Data.DbType.SByte;
            this.db.ParameterDbType(global::System.Data.DbType.Byte);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Byte;
            this.db.ParameterSqlDbType(db.GettingByte());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "Enabled";
            this.db.ParameterSourceColumn("Enabled");
            //this._adapter.InsertCommand.Parameters.Add(param);
            this.db.AdapterInsertCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "@ViewInReport";
            this.db.ParameterName("@ViewInReport");
            //param.DbType = global::System.Data.DbType.SByte;
            this.db.ParameterDbType(global::System.Data.DbType.Byte);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Byte;
            this.db.ParameterSqlDbType(db.GettingByte());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "ViewInReport";
            this.db.ParameterSourceColumn("ViewInReport");
            //param.SourceVersion = global::System.Data.DataRowVersion.Original;
            this.db.ParameterSourceVersion(global::System.Data.DataRowVersion.Original);
            //this._adapter.InsertCommand.Parameters.Add(param);
            this.db.AdapterInsertCommandParametersAdd();

            // UPDATE ................................................

            //this._adapter.UpdateCommand = new global::MySql.Data.MySqlClient.MySqlCommand();
            this.db.AdapterUpdateCommand();
            //this._adapter.UpdateCommand.Connection = this.Connection;
            this.db.AdapterUpdateCommandConnection(this.Connection);
            //this._adapter.UpdateCommand.CommandText = @"UPDATE `Transition` SET `InitialStateId` = @InitialStateId, `FinalStateId` = @FinalStateId, `Title` = @Title, `SensorId` = @SensorId, `IconName` = @IconName, `SoundName` = @SoundName, `Enabled` = @Enabled, `ViewInReport` = @ViewInReport WHERE ((`Id` = @Original_Id) AND ((@IsNull_InitialStateId = 1 AND `InitialStateId` IS NULL) OR (`InitialStateId` = @Original_InitialStateId)) AND ((@IsNull_FinalStateId = 1 AND `FinalStateId` IS NULL) OR (`FinalStateId` = @Original_FinalStateId)) AND (`Title` = @Original_Title) AND ((@IsNull_SensorId = 1 AND `SensorId` IS NULL) OR (`SensorId` = @Original_SensorId)) AND (`IconName` = @Original_IconName) AND (`SoundName` = @Original_SoundName) AND (`Enabled` = @Original_Enabled) AND (`ViewInReport` = @Original_ViewInReport))";

            this.db.AdapterUpdateCommandText(TrackControlQuery.TransitionTableAdapter.UpdateTransition);

            //this._adapter.UpdateCommand.CommandType = global::System.Data.CommandType.Text;
            this.db.AdapterUpdateCommandType(global::System.Data.CommandType.Text);

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "@InitialStateId";
            this.db.ParameterName("@InitialStateId");
            //param.DbType = global::System.Data.DbType.Int32;
            this.db.ParameterDbType(global::System.Data.DbType.Int32);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType(db.GettingInt32());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "InitialStateId";
            this.db.ParameterSourceColumn("InitialStateId");
            //this._adapter.UpdateCommand.Parameters.Add(param);
            this.db.AdapterUpdateCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "@FinalStateId";
            this.db.ParameterName("@FinalStateId");
            //param.DbType = global::System.Data.DbType.Int32;
            this.db.ParameterDbType(global::System.Data.DbType.Int32);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType(db.GettingInt32());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "FinalStateId";
            this.db.ParameterSourceColumn("FinalStateId");
            //this._adapter.UpdateCommand.Parameters.Add(param);
            this.db.AdapterUpdateCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "@Title";
            this.db.ParameterName("@Title");
            //param.DbType = global::System.Data.DbType.String;
            this.db.ParameterDbType(global::System.Data.DbType.String);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.VarChar;
            this.db.ParameterSqlDbType(db.GettingVarChar());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "Title";
            this.db.ParameterSourceColumn("Title");
            //this._adapter.UpdateCommand.Parameters.Add(param);
            this.db.AdapterUpdateCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "@SensorId";
            this.db.ParameterName("@SensorId");
            //param.DbType = global::System.Data.DbType.Int32;
            this.db.ParameterDbType(global::System.Data.DbType.Int32);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType(db.GettingInt32());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "SensorId";
            this.db.ParameterSourceColumn("SensorId");
            //this._adapter.UpdateCommand.Parameters.Add(param);
            this.db.AdapterUpdateCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "@IconName";
            this.db.ParameterName("@IconName");
            //param.DbType = global::System.Data.DbType.String;
            this.db.ParameterDbType(global::System.Data.DbType.String);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.VarChar;
            this.db.ParameterSqlDbType(db.GettingVarChar());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "IconName";
            this.db.ParameterSourceColumn("IconName");
            //this._adapter.UpdateCommand.Parameters.Add(param);
            this.db.AdapterUpdateCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "@SoundName";
            this.db.ParameterName("@SoundName");
            //param.DbType = global::System.Data.DbType.String;
            this.db.ParameterDbType(global::System.Data.DbType.String);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.VarChar;
            this.db.ParameterSqlDbType(db.GettingVarChar());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "SoundName";
            this.db.ParameterSourceColumn("SoundName");
            //this._adapter.UpdateCommand.Parameters.Add(param);
            this.db.AdapterUpdateCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "@Enabled";
            this.db.ParameterName("@Enabled");
            //param.DbType = global::System.Data.DbType.SByte;
            this.db.ParameterDbType(global::System.Data.DbType.Byte);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Byte;
            this.db.ParameterSqlDbType(db.GettingByte());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "Enabled";
            this.db.ParameterSourceColumn("Enabled");
            //this._adapter.UpdateCommand.Parameters.Add(param);
            this.db.AdapterUpdateCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "@ViewInReport";
            this.db.ParameterName("@ViewInReport");
            //param.DbType = global::System.Data.DbType.SByte;
            this.db.ParameterDbType(global::System.Data.DbType.Byte);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Byte;
            this.db.ParameterSqlDbType(db.GettingByte());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "ViewInReport";
            this.db.ParameterSourceColumn("ViewInReport");
            //this._adapter.UpdateCommand.Parameters.Add(param);
            this.db.AdapterUpdateCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "@Original_Id";
            this.db.ParameterName("@Original_Id");
            //param.DbType = global::System.Data.DbType.Int32;
            this.db.ParameterDbType(global::System.Data.DbType.Int32);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType(db.GettingInt32());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "Id";
            this.db.ParameterSourceColumn("Id");
            //param.SourceVersion = global::System.Data.DataRowVersion.Original;
            this.db.ParameterSourceVersion(global::System.Data.DataRowVersion.Original);
            //this._adapter.UpdateCommand.Parameters.Add(param);
            this.db.AdapterUpdateCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "@IsNull_InitialStateId";
            this.db.ParameterName("@IsNull_InitialStateId");
            //param.DbType = global::System.Data.DbType.Int32;
            this.db.ParameterDbType(global::System.Data.DbType.Int32);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType(db.GettingInt32());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "InitialStateId";
            this.db.ParameterSourceColumn("InitialStateId");
            //param.SourceVersion = global::System.Data.DataRowVersion.Original;
            this.db.ParameterSourceVersion(global::System.Data.DataRowVersion.Original);
            //param.SourceColumnNullMapping = true;
            this.db.ParameterSourceColumnNullMapping(true);
            //this._adapter.UpdateCommand.Parameters.Add(param);
            this.db.AdapterUpdateCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "@Original_InitialStateId";
            this.db.ParameterName("@Original_InitialStateId");
            //param.DbType = global::System.Data.DbType.Int32;
            this.db.ParameterDbType(global::System.Data.DbType.Int32);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType(db.GettingInt32());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "InitialStateId";
            this.db.ParameterSourceColumn("InitialStateId");
            //param.SourceVersion = global::System.Data.DataRowVersion.Original;
            this.db.ParameterSourceVersion(global::System.Data.DataRowVersion.Original);
            //this._adapter.UpdateCommand.Parameters.Add(param);
            this.db.AdapterUpdateCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "@IsNull_FinalStateId";
            this.db.ParameterName("@IsNull_FinalStateId");
            //param.DbType = global::System.Data.DbType.Int32;
            this.db.ParameterDbType(global::System.Data.DbType.Int32);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType(db.GettingInt32());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "FinalStateId";
            this.db.ParameterSourceColumn("FinalStateId");
            //param.SourceVersion = global::System.Data.DataRowVersion.Original;
            this.db.ParameterSourceVersion(global::System.Data.DataRowVersion.Original);
            //param.SourceColumnNullMapping = true;
            this.db.ParameterSourceColumnNullMapping(true);
            //this._adapter.UpdateCommand.Parameters.Add(param);
            this.db.AdapterUpdateCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "@Original_FinalStateId";
            this.db.ParameterName("@Original_FinalStateId");
            //param.DbType = global::System.Data.DbType.Int32;
            this.db.ParameterDbType(global::System.Data.DbType.Int32);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType(db.GettingInt32());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "FinalStateId";
            this.db.ParameterSourceColumn("FinalStateId");
            //param.SourceVersion = global::System.Data.DataRowVersion.Original;
            this.db.ParameterSourceVersion(global::System.Data.DataRowVersion.Original);
            //this._adapter.UpdateCommand.Parameters.Add(param);
            this.db.AdapterUpdateCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "@Original_Title";
            this.db.ParameterName("@Original_Title");
            //param.DbType = global::System.Data.DbType.String;
            this.db.ParameterDbType(global::System.Data.DbType.String);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.VarChar;
            this.db.ParameterSqlDbType(db.GettingVarChar());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "Title";
            this.db.ParameterSourceColumn("Title");
            //param.SourceVersion = global::System.Data.DataRowVersion.Original;
            this.db.ParameterSourceVersion(global::System.Data.DataRowVersion.Original);
            //this._adapter.UpdateCommand.Parameters.Add(param);
            this.db.AdapterUpdateCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "@IsNull_SensorId";
            this.db.ParameterName("@IsNull_SensorId");
            //param.DbType = global::System.Data.DbType.Int32;
            this.db.ParameterDbType(global::System.Data.DbType.Int32);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType(db.GettingInt32());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "SensorId";
            this.db.ParameterSourceColumn("SensorId");
            //param.SourceVersion = global::System.Data.DataRowVersion.Original;
            this.db.ParameterSourceVersion(global::System.Data.DataRowVersion.Original);
            //param.SourceColumnNullMapping = true;
            this.db.ParameterSourceColumnNullMapping(true);
            //this._adapter.UpdateCommand.Parameters.Add(param);
            this.db.AdapterUpdateCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "@Original_SensorId";
            this.db.ParameterName("@Original_SensorId");
            //param.DbType = global::System.Data.DbType.Int32;
            this.db.ParameterDbType(global::System.Data.DbType.Int32);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType(db.GettingInt32());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "SensorId";
            this.db.ParameterSourceColumn("SensorId");
            //param.SourceVersion = global::System.Data.DataRowVersion.Original;
            this.db.ParameterSourceVersion(global::System.Data.DataRowVersion.Original);
            //this._adapter.UpdateCommand.Parameters.Add(param);
            this.db.AdapterUpdateCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "@Original_IconName";
            this.db.ParameterName("@Original_IconName");
            //param.DbType = global::System.Data.DbType.String;
            this.db.ParameterDbType(global::System.Data.DbType.String);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.VarChar;
            this.db.ParameterSqlDbType(db.GettingVarChar());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "IconName";
            this.db.ParameterSourceColumn("IconName");
            //param.SourceVersion = global::System.Data.DataRowVersion.Original;
            this.db.ParameterSourceVersion(global::System.Data.DataRowVersion.Original);
            //this._adapter.UpdateCommand.Parameters.Add(param);
            this.db.AdapterUpdateCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "@Original_SoundName";
            this.db.ParameterName("@Original_SoundName");
            //param.DbType = global::System.Data.DbType.String;
            this.db.ParameterDbType(global::System.Data.DbType.String);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.VarChar;
            this.db.ParameterSqlDbType(db.GettingVarChar());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "SoundName";
            this.db.ParameterSourceColumn("SoundName");
            //param.SourceVersion = global::System.Data.DataRowVersion.Original;
            this.db.ParameterSourceVersion(global::System.Data.DataRowVersion.Original);
            //this._adapter.UpdateCommand.Parameters.Add(param);
            this.db.AdapterUpdateCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "@Original_Enabled";
            this.db.ParameterName("@Original_Enabled");
            //param.DbType = global::System.Data.DbType.SByte;
            this.db.ParameterDbType(global::System.Data.DbType.Byte);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Byte;
            this.db.ParameterSqlDbType(db.GettingByte());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "Enabled";
            this.db.ParameterSourceColumn("Enabled");
            //param.SourceVersion = global::System.Data.DataRowVersion.Original;
            this.db.ParameterSourceVersion(global::System.Data.DataRowVersion.Original);
            //this._adapter.UpdateCommand.Parameters.Add(param);
            this.db.AdapterUpdateCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "@Original_ViewInReport";
            this.db.ParameterName("@Original_ViewInReport");
            //param.DbType = global::System.Data.DbType.SByte;
            this.db.ParameterDbType(global::System.Data.DbType.Byte);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Byte;
            this.db.ParameterSqlDbType(db.GettingByte());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "ViewInReport";
            this.db.ParameterSourceColumn("ViewInReport");
            //param.SourceVersion = global::System.Data.DataRowVersion.Original;
            this.db.ParameterSourceVersion(global::System.Data.DataRowVersion.Original);
            //this._adapter.UpdateCommand.Parameters.Add(param);
            this.db.AdapterUpdateCommandParametersAdd();
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        private void InitConnection()
        {
            //this._connection = new global::MySql.Data.MySqlClient.MySqlConnection();
            this.db.AdapNewConnection();
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        private void InitCommandCollection()
        {
            //this._commandCollection = new global::MySql.Data.MySqlClient.MySqlCommand[1];
            this.db.NewCommandCollection(1);
            //this._commandCollection[0] = new global::MySql.Data.MySqlClient.MySqlCommand();
            this.db.SetNewCommandCollection(0);
            //this._commandCollection[0].Connection = this.Connection;
            this.db.SetCommandCollectionConnection(this.Connection, 0);
            //this._commandCollection[0].CommandText = "SELECT `Id`, `InitialStateId`, `FinalStateId`, `Title`, `SensorId`, `IconName`, `" +
            //  "SoundName`, `Enabled`, `ViewInReport` FROM `Transition`";
            //this._commandCollection[0].CommandText = "SELECT `Id`, `InitialStateId`, `FinalStateId`, `Title`, `SensorId`, `IconName`, `" +
            //    "SoundName`, `Enabled`, `ViewInReport` FROM `transition`";

            this.db.SetCommandCollectionText(TrackControlQuery.TransitionTableAdapter.SelectTransition, 0);

            //this._commandCollection[0].CommandType = global::System.Data.CommandType.Text;
            this.db.SetCommandCollectionType(global::System.Data.CommandType.Text, 0);
        }

        // [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.ComponentModel.Design.HelpKeywordAttribute("vs.data.TableAdapter")]
        [global::System.ComponentModel.DataObjectMethodAttribute(global::System.ComponentModel.DataObjectMethodType.Fill,
            true)]
        public virtual int Fill(atlantaDataSet.TransitionDataTable dataTable)
        {
            int returnValue = 0;

            try
            {
                if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                {
                    MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;
                    adapter.SelectCommand = (MySqlCommand) this.CommandCollection[0];
                }
                else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                {
                    SqlDataAdapter adapter = (SqlDataAdapter) this.Adapter;
                    adapter.SelectCommand = (SqlCommand) this.CommandCollection[0];
                }

                if ((this.ClearBeforeFill == true))
                {
                    dataTable.Clear();
                }

                returnValue = 0; // adapter.Fill( dataTable );
                if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                {
                    MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;
                    returnValue = adapter.Fill(dataTable);
                }
                else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                {
                    SqlDataAdapter adapter = (SqlDataAdapter) this.Adapter;
                    returnValue = adapter.Fill(dataTable);
                }
            }
            catch (ConstraintException cEx)
            {
                if (dataTable.Count > 0)
                    return dataTable.Count;

                MessageBox.Show( "" + cEx.Message + "\n" + cEx.StackTrace + "", "Error", MessageBoxButtons.OK );
            }
            catch( Exception e )
            {
                MessageBox.Show( "" + e.Message + "\n" + e.StackTrace + "", "Error", MessageBoxButtons.OK );
                //return returnValue;
            }

            return returnValue;
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.ComponentModel.Design.HelpKeywordAttribute("vs.data.TableAdapter")]
        [global::System.ComponentModel.DataObjectMethodAttribute(
            global::System.ComponentModel.DataObjectMethodType.Select, true)]
        public virtual atlantaDataSet.TransitionDataTable GetData()
        {
            //adapter.SelectCommand = this.CommandCollection[0];

            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;
                adapter.SelectCommand = (MySqlCommand) this.CommandCollection[0];
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                SqlDataAdapter adapter = (SqlDataAdapter) this.Adapter;
                adapter.SelectCommand = (SqlCommand) this.CommandCollection[0];
            }

            atlantaDataSet.TransitionDataTable dataTable = new atlantaDataSet.TransitionDataTable();

            //adapter.Fill(dataTable);

            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;
                adapter.Fill(dataTable);
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                SqlDataAdapter adapter = (SqlDataAdapter) this.Adapter;
                adapter.Fill(dataTable);
            }

            return dataTable;
        }

        //[global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.ComponentModel.Design.HelpKeywordAttribute("vs.data.TableAdapter")]
        public virtual int Update(atlantaDataSet.TransitionDataTable dataTable)
        {
            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;
                return adapter.Update(dataTable);
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                SqlDataAdapter adapter = (SqlDataAdapter) this.Adapter;
                return adapter.Update(dataTable);
            }

            return 0;
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.ComponentModel.Design.HelpKeywordAttribute("vs.data.TableAdapter")]
        public virtual int Update(atlantaDataSet dataSet)
        {
            //return adapter.Update(dataSet, "Transition");

            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;
                return adapter.Update(dataSet, "Transition");
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                SqlDataAdapter adapter = (SqlDataAdapter) this.Adapter;
                return adapter.Update(dataSet, "Transition");
            }

            return 0;
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.ComponentModel.Design.HelpKeywordAttribute("vs.data.TableAdapter")]
        public virtual int Update(global::System.Data.DataRow dataRow)
        {
            //return adapter.Update(new global::System.Data.DataRow[] {dataRow});
            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;
                return adapter.Update(new global::System.Data.DataRow[] {dataRow});
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                SqlDataAdapter adapter = (SqlDataAdapter) this.Adapter;
                return adapter.Update(new global::System.Data.DataRow[] {dataRow});
            }

            return 0;
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.ComponentModel.Design.HelpKeywordAttribute("vs.data.TableAdapter")]
        public virtual int Update(global::System.Data.DataRow[] dataRows)
        {
            //return adapter.Update(dataRows);
            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;
                return adapter.Update(dataRows);
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                SqlDataAdapter adapter = (SqlDataAdapter) this.Adapter;
                return adapter.Update(dataRows);
            }

            return 0;
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.ComponentModel.Design.HelpKeywordAttribute("vs.data.TableAdapter")]
        [global::System.ComponentModel.DataObjectMethodAttribute(
            global::System.ComponentModel.DataObjectMethodType.Delete, true)]
        public virtual int Delete(int Original_Id, global::System.Nullable<int> Original_InitialStateId,
            global::System.Nullable<int> Original_FinalStateId, string Original_Title,
            global::System.Nullable<int> Original_SensorId, string Original_IconName, string Original_SoundName,
            byte Original_Enabled, byte Original_ViewInReport)
        {
            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;

                adapter.DeleteCommand.Parameters[0].Value = ((int) (Original_Id));
                if ((Original_InitialStateId.HasValue == true))
                {
                    adapter.DeleteCommand.Parameters[1].Value = ((object) (0));
                    adapter.DeleteCommand.Parameters[2].Value = ((int) (Original_InitialStateId.Value));
                }
                else
                {
                    adapter.DeleteCommand.Parameters[1].Value = ((object) (1));
                    adapter.DeleteCommand.Parameters[2].Value = global::System.DBNull.Value;
                }
                if ((Original_FinalStateId.HasValue == true))
                {
                    adapter.DeleteCommand.Parameters[3].Value = ((object) (0));
                    adapter.DeleteCommand.Parameters[4].Value = ((int) (Original_FinalStateId.Value));
                }
                else
                {
                    adapter.DeleteCommand.Parameters[3].Value = ((object) (1));
                    adapter.DeleteCommand.Parameters[4].Value = global::System.DBNull.Value;
                }
                if ((Original_Title == null))
                {
                    throw new global::System.ArgumentNullException("Original_Title");
                }
                else
                {
                    adapter.DeleteCommand.Parameters[5].Value = ((string) (Original_Title));
                }
                if ((Original_SensorId.HasValue == true))
                {
                    adapter.DeleteCommand.Parameters[6].Value = ((object) (0));
                    adapter.DeleteCommand.Parameters[7].Value = ((int) (Original_SensorId.Value));
                }
                else
                {
                    adapter.DeleteCommand.Parameters[6].Value = ((object) (1));
                    adapter.DeleteCommand.Parameters[7].Value = global::System.DBNull.Value;
                }
                if ((Original_IconName == null))
                {
                    throw new global::System.ArgumentNullException("Original_IconName");
                }
                else
                {
                    adapter.DeleteCommand.Parameters[8].Value = ((string) (Original_IconName));
                }
                if ((Original_SoundName == null))
                {
                    throw new global::System.ArgumentNullException("Original_SoundName");
                }
                else
                {
                    adapter.DeleteCommand.Parameters[9].Value = ((string) (Original_SoundName));
                }
                adapter.DeleteCommand.Parameters[10].Value = ((byte) (Original_Enabled));
                adapter.DeleteCommand.Parameters[11].Value = ((byte) (Original_ViewInReport));
                global::System.Data.ConnectionState previousConnectionState = adapter.DeleteCommand.Connection.State;
                if (((adapter.DeleteCommand.Connection.State & global::System.Data.ConnectionState.Open)
                     != global::System.Data.ConnectionState.Open))
                {
                    adapter.DeleteCommand.Connection.Open();
                }
                try
                {
                    int returnValue = adapter.DeleteCommand.ExecuteNonQuery();
                    return returnValue;
                }
                finally
                {
                    if ((previousConnectionState == global::System.Data.ConnectionState.Closed))
                    {
                        adapter.DeleteCommand.Connection.Close();
                    }
                }
            } // if
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                SqlDataAdapter adapter = (SqlDataAdapter) this.Adapter;

                adapter.DeleteCommand.Parameters[0].Value = ((int) (Original_Id));
                if ((Original_InitialStateId.HasValue == true))
                {
                    adapter.DeleteCommand.Parameters[1].Value = ((object) (0));
                    adapter.DeleteCommand.Parameters[2].Value = ((int) (Original_InitialStateId.Value));
                }
                else
                {
                    adapter.DeleteCommand.Parameters[1].Value = ((object) (1));
                    adapter.DeleteCommand.Parameters[2].Value = global::System.DBNull.Value;
                }
                if ((Original_FinalStateId.HasValue == true))
                {
                    adapter.DeleteCommand.Parameters[3].Value = ((object) (0));
                    adapter.DeleteCommand.Parameters[4].Value = ((int) (Original_FinalStateId.Value));
                }
                else
                {
                    adapter.DeleteCommand.Parameters[3].Value = ((object) (1));
                    adapter.DeleteCommand.Parameters[4].Value = global::System.DBNull.Value;
                }
                if ((Original_Title == null))
                {
                    throw new global::System.ArgumentNullException("Original_Title");
                }
                else
                {
                    adapter.DeleteCommand.Parameters[5].Value = ((string) (Original_Title));
                }
                if ((Original_SensorId.HasValue == true))
                {
                    adapter.DeleteCommand.Parameters[6].Value = ((object) (0));
                    adapter.DeleteCommand.Parameters[7].Value = ((int) (Original_SensorId.Value));
                }
                else
                {
                    adapter.DeleteCommand.Parameters[6].Value = ((object) (1));
                    adapter.DeleteCommand.Parameters[7].Value = global::System.DBNull.Value;
                }
                if ((Original_IconName == null))
                {
                    throw new global::System.ArgumentNullException("Original_IconName");
                }
                else
                {
                    adapter.DeleteCommand.Parameters[8].Value = ((string) (Original_IconName));
                }
                if ((Original_SoundName == null))
                {
                    throw new global::System.ArgumentNullException("Original_SoundName");
                }
                else
                {
                    adapter.DeleteCommand.Parameters[9].Value = ((string) (Original_SoundName));
                }
                adapter.DeleteCommand.Parameters[10].Value = ((byte) (Original_Enabled));
                adapter.DeleteCommand.Parameters[11].Value = ((byte) (Original_ViewInReport));
                global::System.Data.ConnectionState previousConnectionState = adapter.DeleteCommand.Connection.State;
                if (((adapter.DeleteCommand.Connection.State & global::System.Data.ConnectionState.Open)
                     != global::System.Data.ConnectionState.Open))
                {
                    adapter.DeleteCommand.Connection.Open();
                }
                try
                {
                    int returnValue = adapter.DeleteCommand.ExecuteNonQuery();
                    return returnValue;
                }
                finally
                {
                    if ((previousConnectionState == global::System.Data.ConnectionState.Closed))
                    {
                        adapter.DeleteCommand.Connection.Close();
                    }
                }
            } // else if

            return 0;
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.ComponentModel.Design.HelpKeywordAttribute("vs.data.TableAdapter")]
        [global::System.ComponentModel.DataObjectMethodAttribute(
            global::System.ComponentModel.DataObjectMethodType.Insert, true)]
        public virtual int Insert(global::System.Nullable<int> InitialStateId, global::System.Nullable<int> FinalStateId,
            string Title, global::System.Nullable<int> SensorId, string IconName, string SoundName, byte Enabled,
            byte ViewInReport)
        {
            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;

                if ((InitialStateId.HasValue == true))
                {
                    adapter.InsertCommand.Parameters[0].Value = ((int) (InitialStateId.Value));
                }
                else
                {
                    adapter.InsertCommand.Parameters[0].Value = global::System.DBNull.Value;
                }
                if ((FinalStateId.HasValue == true))
                {
                    adapter.InsertCommand.Parameters[1].Value = ((int) (FinalStateId.Value));
                }
                else
                {
                    adapter.InsertCommand.Parameters[1].Value = global::System.DBNull.Value;
                }
                if ((Title == null))
                {
                    throw new global::System.ArgumentNullException("Title");
                }
                else
                {
                    adapter.InsertCommand.Parameters[2].Value = ((string) (Title));
                }
                if ((SensorId.HasValue == true))
                {
                    adapter.InsertCommand.Parameters[3].Value = ((int) (SensorId.Value));
                }
                else
                {
                    adapter.InsertCommand.Parameters[3].Value = global::System.DBNull.Value;
                }
                if ((IconName == null))
                {
                    throw new global::System.ArgumentNullException("IconName");
                }
                else
                {
                    adapter.InsertCommand.Parameters[4].Value = ((string) (IconName));
                }
                if ((SoundName == null))
                {
                    throw new global::System.ArgumentNullException("SoundName");
                }
                else
                {
                    adapter.InsertCommand.Parameters[5].Value = ((string) (SoundName));
                }
                adapter.InsertCommand.Parameters[6].Value = ((byte) (Enabled));
                adapter.InsertCommand.Parameters[7].Value = ((byte) (ViewInReport));
                global::System.Data.ConnectionState previousConnectionState = adapter.InsertCommand.Connection.State;
                if (((adapter.InsertCommand.Connection.State & global::System.Data.ConnectionState.Open)
                     != global::System.Data.ConnectionState.Open))
                {
                    adapter.InsertCommand.Connection.Open();
                }
                try
                {
                    int returnValue = adapter.InsertCommand.ExecuteNonQuery();
                    return returnValue;
                }
                finally
                {
                    if ((previousConnectionState == global::System.Data.ConnectionState.Closed))
                    {
                        adapter.InsertCommand.Connection.Close();
                    }
                }
            } // if
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                SqlDataAdapter adapter = (SqlDataAdapter) this.Adapter;

                if ((InitialStateId.HasValue == true))
                {
                    adapter.InsertCommand.Parameters[0].Value = ((int) (InitialStateId.Value));
                }
                else
                {
                    adapter.InsertCommand.Parameters[0].Value = global::System.DBNull.Value;
                }
                if ((FinalStateId.HasValue == true))
                {
                    adapter.InsertCommand.Parameters[1].Value = ((int) (FinalStateId.Value));
                }
                else
                {
                    adapter.InsertCommand.Parameters[1].Value = global::System.DBNull.Value;
                }
                if ((Title == null))
                {
                    throw new global::System.ArgumentNullException("Title");
                }
                else
                {
                    adapter.InsertCommand.Parameters[2].Value = ((string) (Title));
                }
                if ((SensorId.HasValue == true))
                {
                    adapter.InsertCommand.Parameters[3].Value = ((int) (SensorId.Value));
                }
                else
                {
                    adapter.InsertCommand.Parameters[3].Value = global::System.DBNull.Value;
                }
                if ((IconName == null))
                {
                    throw new global::System.ArgumentNullException("IconName");
                }
                else
                {
                    adapter.InsertCommand.Parameters[4].Value = ((string) (IconName));
                }
                if ((SoundName == null))
                {
                    throw new global::System.ArgumentNullException("SoundName");
                }
                else
                {
                    adapter.InsertCommand.Parameters[5].Value = ((string) (SoundName));
                }
                adapter.InsertCommand.Parameters[6].Value = ((byte) (Enabled));
                adapter.InsertCommand.Parameters[7].Value = ((byte) (ViewInReport));
                global::System.Data.ConnectionState previousConnectionState = adapter.InsertCommand.Connection.State;
                if (((adapter.InsertCommand.Connection.State & global::System.Data.ConnectionState.Open)
                     != global::System.Data.ConnectionState.Open))
                {
                    adapter.InsertCommand.Connection.Open();
                }
                try
                {
                    int returnValue = adapter.InsertCommand.ExecuteNonQuery();
                    return returnValue;
                }
                finally
                {
                    if ((previousConnectionState == global::System.Data.ConnectionState.Closed))
                    {
                        adapter.InsertCommand.Connection.Close();
                    }
                }
            } // else if

            return 0;
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.ComponentModel.Design.HelpKeywordAttribute("vs.data.TableAdapter")]
        [global::System.ComponentModel.DataObjectMethodAttribute(
            global::System.ComponentModel.DataObjectMethodType.Update, true)]
        public virtual int Update(global::System.Nullable<int> InitialStateId, global::System.Nullable<int> FinalStateId,
            string Title, global::System.Nullable<int> SensorId, string IconName, string SoundName, byte Enabled,
            int Original_Id, global::System.Nullable<int> Original_InitialStateId,
            global::System.Nullable<int> Original_FinalStateId, string Original_Title,
            global::System.Nullable<int> Original_SensorId, string Original_IconName, string Original_SoundName,
            byte Original_Enabled, byte Original_ViewInReport)
        {
            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;

                if ((InitialStateId.HasValue == true))
                {
                    adapter.UpdateCommand.Parameters[0].Value = ((int) (InitialStateId.Value));
                }
                else
                {
                    adapter.UpdateCommand.Parameters[0].Value = global::System.DBNull.Value;
                }
                if ((FinalStateId.HasValue == true))
                {
                    adapter.UpdateCommand.Parameters[1].Value = ((int) (FinalStateId.Value));
                }
                else
                {
                    adapter.UpdateCommand.Parameters[1].Value = global::System.DBNull.Value;
                }
                if ((Title == null))
                {
                    throw new global::System.ArgumentNullException("Title");
                }
                else
                {
                    adapter.UpdateCommand.Parameters[2].Value = ((string) (Title));
                }
                if ((SensorId.HasValue == true))
                {
                    adapter.UpdateCommand.Parameters[3].Value = ((int) (SensorId.Value));
                }
                else
                {
                    adapter.UpdateCommand.Parameters[3].Value = global::System.DBNull.Value;
                }
                if ((IconName == null))
                {
                    throw new global::System.ArgumentNullException("IconName");
                }
                else
                {
                    adapter.UpdateCommand.Parameters[4].Value = ((string) (IconName));
                }
                if ((SoundName == null))
                {
                    throw new global::System.ArgumentNullException("SoundName");
                }
                else
                {
                    adapter.UpdateCommand.Parameters[5].Value = ((string) (SoundName));
                }
                adapter.UpdateCommand.Parameters[6].Value = ((byte) (Enabled));
                adapter.UpdateCommand.Parameters[7].Value = ((int) (Original_Id));
                if ((Original_InitialStateId.HasValue == true))
                {
                    adapter.UpdateCommand.Parameters[8].Value = ((object) (0));
                    adapter.UpdateCommand.Parameters[9].Value = ((int) (Original_InitialStateId.Value));
                }
                else
                {
                    adapter.UpdateCommand.Parameters[8].Value = ((object) (1));
                    adapter.UpdateCommand.Parameters[9].Value = global::System.DBNull.Value;
                }
                if ((Original_FinalStateId.HasValue == true))
                {
                    adapter.UpdateCommand.Parameters[10].Value = ((object) (0));
                    adapter.UpdateCommand.Parameters[11].Value = ((int) (Original_FinalStateId.Value));
                }
                else
                {
                    adapter.UpdateCommand.Parameters[10].Value = ((object) (1));
                    adapter.UpdateCommand.Parameters[11].Value = global::System.DBNull.Value;
                }
                if ((Original_Title == null))
                {
                    throw new global::System.ArgumentNullException("Original_Title");
                }
                else
                {
                    adapter.UpdateCommand.Parameters[12].Value = ((string) (Original_Title));
                }
                if ((Original_SensorId.HasValue == true))
                {
                    adapter.UpdateCommand.Parameters[13].Value = ((object) (0));
                    adapter.UpdateCommand.Parameters[14].Value = ((int) (Original_SensorId.Value));
                }
                else
                {
                    adapter.UpdateCommand.Parameters[13].Value = ((object) (1));
                    adapter.UpdateCommand.Parameters[14].Value = global::System.DBNull.Value;
                }
                if ((Original_IconName == null))
                {
                    throw new global::System.ArgumentNullException("Original_IconName");
                }
                else
                {
                    adapter.UpdateCommand.Parameters[15].Value = ((string) (Original_IconName));
                }
                if ((Original_SoundName == null))
                {
                    throw new global::System.ArgumentNullException("Original_SoundName");
                }
                else
                {
                    adapter.UpdateCommand.Parameters[16].Value = ((string) (Original_SoundName));
                }
                adapter.UpdateCommand.Parameters[17].Value = ((byte) (Original_Enabled));
                adapter.UpdateCommand.Parameters[18].Value = ((byte) (Original_ViewInReport));
                global::System.Data.ConnectionState previousConnectionState = adapter.UpdateCommand.Connection.State;
                if (((adapter.UpdateCommand.Connection.State & global::System.Data.ConnectionState.Open)
                     != global::System.Data.ConnectionState.Open))
                {
                    adapter.UpdateCommand.Connection.Open();
                }
                try
                {
                    int returnValue = adapter.UpdateCommand.ExecuteNonQuery();
                    return returnValue;
                }
                finally
                {
                    if ((previousConnectionState == global::System.Data.ConnectionState.Closed))
                    {
                        adapter.UpdateCommand.Connection.Close();
                    }
                }
            } // if
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                SqlDataAdapter adapter = (SqlDataAdapter) this.Adapter;

                if ((InitialStateId.HasValue == true))
                {
                    adapter.UpdateCommand.Parameters[0].Value = ((int) (InitialStateId.Value));
                }
                else
                {
                    adapter.UpdateCommand.Parameters[0].Value = global::System.DBNull.Value;
                }
                if ((FinalStateId.HasValue == true))
                {
                    adapter.UpdateCommand.Parameters[1].Value = ((int) (FinalStateId.Value));
                }
                else
                {
                    adapter.UpdateCommand.Parameters[1].Value = global::System.DBNull.Value;
                }
                if ((Title == null))
                {
                    throw new global::System.ArgumentNullException("Title");
                }
                else
                {
                    adapter.UpdateCommand.Parameters[2].Value = ((string) (Title));
                }
                if ((SensorId.HasValue == true))
                {
                    adapter.UpdateCommand.Parameters[3].Value = ((int) (SensorId.Value));
                }
                else
                {
                    adapter.UpdateCommand.Parameters[3].Value = global::System.DBNull.Value;
                }
                if ((IconName == null))
                {
                    throw new global::System.ArgumentNullException("IconName");
                }
                else
                {
                    adapter.UpdateCommand.Parameters[4].Value = ((string) (IconName));
                }
                if ((SoundName == null))
                {
                    throw new global::System.ArgumentNullException("SoundName");
                }
                else
                {
                    adapter.UpdateCommand.Parameters[5].Value = ((string) (SoundName));
                }
                adapter.UpdateCommand.Parameters[6].Value = ((byte) (Enabled));
                adapter.UpdateCommand.Parameters[7].Value = ((int) (Original_Id));
                if ((Original_InitialStateId.HasValue == true))
                {
                    adapter.UpdateCommand.Parameters[8].Value = ((object) (0));
                    adapter.UpdateCommand.Parameters[9].Value = ((int) (Original_InitialStateId.Value));
                }
                else
                {
                    adapter.UpdateCommand.Parameters[8].Value = ((object) (1));
                    adapter.UpdateCommand.Parameters[9].Value = global::System.DBNull.Value;
                }
                if ((Original_FinalStateId.HasValue == true))
                {
                    adapter.UpdateCommand.Parameters[10].Value = ((object) (0));
                    adapter.UpdateCommand.Parameters[11].Value = ((int) (Original_FinalStateId.Value));
                }
                else
                {
                    adapter.UpdateCommand.Parameters[10].Value = ((object) (1));
                    adapter.UpdateCommand.Parameters[11].Value = global::System.DBNull.Value;
                }
                if ((Original_Title == null))
                {
                    throw new global::System.ArgumentNullException("Original_Title");
                }
                else
                {
                    adapter.UpdateCommand.Parameters[12].Value = ((string) (Original_Title));
                }
                if ((Original_SensorId.HasValue == true))
                {
                    adapter.UpdateCommand.Parameters[13].Value = ((object) (0));
                    adapter.UpdateCommand.Parameters[14].Value = ((int) (Original_SensorId.Value));
                }
                else
                {
                    adapter.UpdateCommand.Parameters[13].Value = ((object) (1));
                    adapter.UpdateCommand.Parameters[14].Value = global::System.DBNull.Value;
                }
                if ((Original_IconName == null))
                {
                    throw new global::System.ArgumentNullException("Original_IconName");
                }
                else
                {
                    adapter.UpdateCommand.Parameters[15].Value = ((string) (Original_IconName));
                }
                if ((Original_SoundName == null))
                {
                    throw new global::System.ArgumentNullException("Original_SoundName");
                }
                else
                {
                    adapter.UpdateCommand.Parameters[16].Value = ((string) (Original_SoundName));
                }
                adapter.UpdateCommand.Parameters[17].Value = ((byte) (Original_Enabled));
                adapter.UpdateCommand.Parameters[18].Value = ((byte) (Original_ViewInReport));
                global::System.Data.ConnectionState previousConnectionState = adapter.UpdateCommand.Connection.State;
                if (((adapter.UpdateCommand.Connection.State & global::System.Data.ConnectionState.Open)
                     != global::System.Data.ConnectionState.Open))
                {
                    adapter.UpdateCommand.Connection.Open();
                }
                try
                {
                    int returnValue = adapter.UpdateCommand.ExecuteNonQuery();
                    return returnValue;
                }
                finally
                {
                    if ((previousConnectionState == global::System.Data.ConnectionState.Closed))
                    {
                        adapter.UpdateCommand.Connection.Close();
                    }
                }
            } //  else if

            return 0;
        }
    }
}