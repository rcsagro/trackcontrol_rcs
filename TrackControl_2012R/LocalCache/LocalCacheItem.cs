using System.Threading;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Windows.Forms;
using TrackControl.General;
using TrackControl.General.DAL;
using TrackControl.General.DatabaseDriver;
using TrackControl.Vehicles;

namespace LocalCache
{
    /// <summary>
    /// ������� ��������� ���������� atlantaDataSet
    /// </summary>
    public class LocalCacheItem
    {
        private  Dictionary<int, List<GpsData>> GpsDatas64 = new Dictionary<int, List<GpsData>>();
        public GpsData[] GpsDatas;

        readonly int _mobitelId = 0;
        DateTime _dtStart;

        public DateTime DateStart
        {
            get { return _dtStart; }
            set { _dtStart = value; }
        }
        DateTime _dtEnd;

        public DateTime DateEnd
        {
            get { return _dtEnd; }
            set { _dtEnd = value; }
        }

        string CONNECTION_STRING = DriverDb.ConnectionString; // ConnectMySQL.ConnectionString;

        bool bDataSetReady = false;

        atlantaDataSet _dsAtlanta;
        //BackgroundWorker - ������������� � MainForm TrackControl
        System.ComponentModel.BackgroundWorker bwCreateData = new System.ComponentModel.BackgroundWorker();

        public LocalCacheItem(DateTime dtStart, DateTime dtEnd, int Mobitel_Id)
        {
            _dtStart = dtStart;
            _dtEnd = dtEnd;
            _mobitelId = Mobitel_Id;
        }

        public int CreateDsAtlanta(ref atlantaDataSet dsAtlanta)
        {
            try
            {
                _dsAtlanta = dsAtlanta;
                //�������� ������� ������ �� ���� ���� � DataGPS ��� ��������� ������
                //if (RecordsDataGPS() == 0) return 0;
                if (dsAtlanta.dataview.Rows.Count > 0)
                    return dsAtlanta.dataview.Rows.Count;

                if (bwCreateData.IsBusy)
                {
                    MessageBox.Show("Can not start again! Try again later.", "Error run worker async ", MessageBoxButtons.OK);
                    return 0;
                }

                //int k = dsAtlanta.mobitels.Rows.Count;
                if (dsAtlanta.mobitels.Rows.Count == 0)
                {
                    FillMobitelsTable();
                    FillSensorsTable();
                    FillStateTable();
                    //FillTransitionTable();
                    FillRelationAlgorithmsTable();
                    FillSensorCoefficientTable();
                    FillSettingTable();
                    FillTeamTable();
                    FillDriverTable();
                    FillVehicleTable();
                }

                atlantaDataSet.mobitelsRow m_row =
                    (LocalCache.atlantaDataSet.mobitelsRow) dsAtlanta.mobitels.FindByMobitel_ID(_mobitelId);
                if (m_row == null) return 0;
                m_row.Check = true;
                bwCreateData.WorkerReportsProgress = true;
                bwCreateData.DoWork += new System.ComponentModel.DoWorkEventHandler(bwCreateData_DoWork);
                bwCreateData.RunWorkerCompleted +=
                    new System.ComponentModel.RunWorkerCompletedEventHandler(bwCreateData_RunWorkerCompleted);
                bwCreateData.RunWorkerAsync();

                while (!bDataSetReady)
                {
                    Application.DoEvents();
                    continue;
                }


                GpsDatas = GetDataGpsArray(m_row);

                if (GpsDatas == null)
                    return 0;
                else
                    return GpsDatas.Length;
            }
            catch (Exception ex)
            {
                MessageBox.Show( ex.Message, "Error Create DS Atlanta ", MessageBoxButtons.OK );
                return 0;
            }
        }

        public int CreateDsAtlantaWGpsDataProvider(ref atlantaDataSet dsAtlanta, GetGpsData getGpsDataAllProtocols)
        {
            try
            {
                _dsAtlanta = dsAtlanta;
                //int k = dsAtlanta.mobitels.Rows.Count;
                if (dsAtlanta.mobitels.Rows.Count == 0)
                {
                    FillMobitelsTable();
                    FillSensorsTable();
                    FillStateTable();
                    //FillTransitionTable();
                    FillRelationAlgorithmsTable();
                    FillSensorCoefficientTable();
                    FillSettingTable();
                    FillTeamTable();
                    FillDriverTable();
                    FillVehicleTable();
                }

                var mRow = (atlantaDataSet.mobitelsRow)dsAtlanta.mobitels.FindByMobitel_ID(_mobitelId);
                if (mRow == null) return 0;
                GpsDatas = getGpsDataAllProtocols(_mobitelId, _dtStart, _dtEnd).ToArray();
                if (GpsDatas == null)
                    return 0;
                else
                    return GpsDatas.Length;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error Create DS Atlanta ", MessageBoxButtons.OK);
                return 0;
            }
        }

        public void CreateDSAtlantaForReports(ref atlantaDataSet dsAtlanta)
        {
            if (CONNECTION_STRING.Length == 0)
            {
                DriverDb db = new DriverDb();
                db.ConnectDb();
                CONNECTION_STRING = DriverDb.ConnectionString;
            }
            _dsAtlanta = dsAtlanta;
            if (dsAtlanta.mobitels.Rows.Count == 0)
            {
                FillMobitelsTable();
                FillSensorsTable();
                FillStateTable();
                FillTransitionTable();
                FillRelationAlgorithmsTable();
                FillSensorCoefficientTable();
                FillSettingTable();
                //FillTeamTable();
                //FillDriverTable();
                FillVehicleTable();
            }
        }

        private void bwCreateData_RunWorkerCompleted(object sender, EventArgs e)
        {
            bDataSetReady = true;
        }

        private void bwCreateData_DoWork(object sender, EventArgs e)
        {
            LocalCache.atlantaDataSetTableAdapters.DataviewTableAdapter taDataView = 
              new LocalCache.atlantaDataSetTableAdapters.DataviewTableAdapter(CONNECTION_STRING);
            string name = "";
            taDataView.Fill(_dsAtlanta, _dtStart, _dtEnd, bwCreateData, ref name,GpsDatas64);
        }

        private int RecordsDataGPS()
        {
            //ConnectMySQL cnMySQL = new ConnectMySQL();
            DriverDb db = new DriverDb();
            db.ConnectDb();

            string SQLselect = string.Format( TrackControlQuery.LocalCacheItem.SelectDatagps, _mobitelId );
          
            //MySqlParameter[] parDate = new MySqlParameter[2];
            db.NewSqlParameterArray(2);
            //parDate[0] = new MySqlParameter("?TimeStart", _dtStart);
            db.NewSqlParameter( db.ParamPrefics + "TimeStart", _dtStart, 0 );
            //parDate[1] = new MySqlParameter("?TimeLast", _dtEnd);
            db.NewSqlParameter( db.ParamPrefics + "TimeLast", _dtEnd, 1 );
            int iRecords = db.GetDataReaderRecordsCount(SQLselect, db.GetSqlParameterArray);
            //cnMySQL.CloseMySQLConnection();
            db.CloseDbConnection();
            return iRecords;
        }

        private void FillMobitelsTable()
        {
            MobitelsProvider mobitelsTableAdapter = new MobitelsProvider(CONNECTION_STRING);
            mobitelsTableAdapter.FillMobitelsTable(_dsAtlanta.mobitels);
        }

        private void FillStateTable()
        {
            LocalCache.atlantaDataSetTableAdapters.StateTableAdapter stateTableAdapter;
            stateTableAdapter = new LocalCache.atlantaDataSetTableAdapters.StateTableAdapter();
            //stateTableAdapter.SqlConnection.ConnectionString = CONNECTION_STRING;
            if( DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse )
            {
                MySqlConnection connection = (MySqlConnection)stateTableAdapter.Connection;
                connection.ConnectionString = CONNECTION_STRING;
            }
            else if( DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse )
            {
                SqlConnection connection = (SqlConnection)stateTableAdapter.Connection;
                connection.ConnectionString = CONNECTION_STRING;
            }
            stateTableAdapter.Fill(_dsAtlanta.State);
        }

        private void FillTransitionTable()
        {
            LocalCache.atlantaDataSetTableAdapters.TransitionTableAdapter transTableAdapter;
            transTableAdapter = new LocalCache.atlantaDataSetTableAdapters.TransitionTableAdapter();
            //transTableAdapter.Connection.ConnectionString = CONNECTION_STRING;
            if( DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse )
            {
                MySqlConnection connection = (MySqlConnection)transTableAdapter.Connection;
                connection.ConnectionString = CONNECTION_STRING;
            }
            else if( DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse )
            {
                SqlConnection connection = (SqlConnection)transTableAdapter.Connection;
                connection.ConnectionString = CONNECTION_STRING;
            }
            transTableAdapter.Fill(_dsAtlanta.Transition);
        } 

        private void FillSensorsTable()
        {
            LocalCache.atlantaDataSetTableAdapters.SensorsTableAdapter sensorsTableAdapter;
            sensorsTableAdapter = new LocalCache.atlantaDataSetTableAdapters.SensorsTableAdapter();
            //sensorsTableAdapter.SqlConnection.ConnectionString = CONNECTION_STRING;
            if( DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse )
            {
                MySqlConnection connection = (MySqlConnection)sensorsTableAdapter.Connection;
                connection.ConnectionString = CONNECTION_STRING;
            }
            else if( DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse )
            {
                SqlConnection connection = (SqlConnection)sensorsTableAdapter.Connection;
                connection.ConnectionString = CONNECTION_STRING;
            }
            sensorsTableAdapter.Fill(_dsAtlanta.sensors);
        } // FillSensorsTable()

        /// <summary>
        /// ���������� ������� RelationAlgorithms
        /// </summary>
        private void FillRelationAlgorithmsTable()
        {
            LocalCache.atlantaDataSetTableAdapters.RelationalgorithmsTableAdapter relationalgorithmsTableAdapter;
            relationalgorithmsTableAdapter = new LocalCache.atlantaDataSetTableAdapters.RelationalgorithmsTableAdapter();

            if( DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse )
            {
                MySqlConnection connection = (MySqlConnection)relationalgorithmsTableAdapter.Connection;
                connection.ConnectionString = CONNECTION_STRING;
            }
            else if( DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse )
            {
                SqlConnection connection = (SqlConnection)relationalgorithmsTableAdapter.Connection;
                connection.ConnectionString = CONNECTION_STRING;
            }

            relationalgorithmsTableAdapter.Fill(_dsAtlanta.relationalgorithms);
        }

        /// <summary>
        /// ���������� ������� SensorCoefficient
        /// </summary>
        private void FillSensorCoefficientTable()
        {
            LocalCache.atlantaDataSetTableAdapters.SensorcoefficientTableAdapter sensorcoefficientTableAdapter;
            sensorcoefficientTableAdapter = new LocalCache.atlantaDataSetTableAdapters.SensorcoefficientTableAdapter();
            //sensorcoefficientTableAdapter.SqlConnection.ConnectionString = CONNECTION_STRING;
            if( DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse )
            {
                MySqlConnection connection = (MySqlConnection)sensorcoefficientTableAdapter.Connection;
                connection.ConnectionString = CONNECTION_STRING;
            }
            else if( DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse )
            {
                SqlConnection connection = (SqlConnection)sensorcoefficientTableAdapter.Connection;
                connection.ConnectionString = CONNECTION_STRING;
            }

            sensorcoefficientTableAdapter.Fill(_dsAtlanta.sensorcoefficient);
        }

        /// <summary>
        /// ���������� ������� SensorAlgorithms
        /// </summary>
        private void FillSensorAlgorithmsTable()
        {
            LocalCache.atlantaDataSetTableAdapters.SensoralgorithmsTableAdapter sensoralgorithmsTableAdapter;
            sensoralgorithmsTableAdapter = new LocalCache.atlantaDataSetTableAdapters.SensoralgorithmsTableAdapter();
            //sensoralgorithmsTableAdapter.Connection.ConnectionString = CONNECTION_STRING;

            if( DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse )
            {
                MySqlConnection connection = (MySqlConnection)sensoralgorithmsTableAdapter.Connection;
                connection.ConnectionString = CONNECTION_STRING;
            }
            else if( DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse )
            {
                SqlConnection connection = (SqlConnection)sensoralgorithmsTableAdapter.Connection;
                connection.ConnectionString = CONNECTION_STRING;
            }

            sensoralgorithmsTableAdapter.Fill(_dsAtlanta.sensoralgorithms);
        }

        private void FillSettingTable()
        {
            LocalCache.atlantaDataSetTableAdapters.SettingTableAdapter settingTableAdapter;
            settingTableAdapter = new LocalCache.atlantaDataSetTableAdapters.SettingTableAdapter();
            //settingTableAdapter.Connection.ConnectionString = CONNECTION_STRING;

            if( DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse )
            {
                MySqlConnection connection = (MySqlConnection)settingTableAdapter.Connection;
                connection.ConnectionString = CONNECTION_STRING;
            }
            else if( DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse )
            {
                SqlConnection connection = (SqlConnection)settingTableAdapter.Connection;
                connection.ConnectionString = CONNECTION_STRING;
            }

            settingTableAdapter.Fill(_dsAtlanta.setting);
        }

        private void FillDriverTable()
        {
            LocalCache.atlantaDataSetTableAdapters.DriverTableAdapter driverTableAdapter;
            driverTableAdapter = new LocalCache.atlantaDataSetTableAdapters.DriverTableAdapter();

            //driverTableAdapter.Connection.ConnectionString = CONNECTION_STRING;

            if( DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse )
            {
                MySqlConnection connection = (MySqlConnection)driverTableAdapter.Connection;
                connection.ConnectionString = CONNECTION_STRING; 
            }
            else if( DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse )
            {
                SqlConnection connection = (SqlConnection)driverTableAdapter.Connection;
                connection.ConnectionString = CONNECTION_STRING; 
            }

            driverTableAdapter.Fill(_dsAtlanta.driver);
        } // FillDriverTable()

        /// <summary>
        /// ���������� ������� Team
        /// </summary>
        private void FillTeamTable()
        {
            LocalCache.atlantaDataSetTableAdapters.TeamTableAdapter teamTableAdapter;
            teamTableAdapter = new LocalCache.atlantaDataSetTableAdapters.TeamTableAdapter();

            //teamTableAdapter.Connection.ConnectionString = CONNECTION_STRING;

            if( DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse )
            {
                MySqlConnection connection = (MySqlConnection)teamTableAdapter.Connection;
                connection.ConnectionString = CONNECTION_STRING;
            }
            else if( DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse )
            {
                SqlConnection connection = (SqlConnection)teamTableAdapter.Connection;
                connection.ConnectionString = CONNECTION_STRING;
            }

            teamTableAdapter.Fill(_dsAtlanta.team);
        }

        /// <summary>
        /// ���������� ������� Vehicle
        /// </summary>
        private void FillVehicleTable()
        {
            LocalCache.atlantaDataSetTableAdapters.VehicleTableAdapter vehicleTableAdapter;
            vehicleTableAdapter = new LocalCache.atlantaDataSetTableAdapters.VehicleTableAdapter();
            if( DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse )
            {
                MySqlConnection connection = (MySqlConnection)vehicleTableAdapter.Connection;
                connection.ConnectionString = CONNECTION_STRING;
            }
            else if( DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse )
            {
                SqlConnection connection = (SqlConnection)vehicleTableAdapter.Connection;
                connection.ConnectionString = CONNECTION_STRING;
            }
            //vehicleTableAdapter.Connection.ConnectionString = CONNECTION_STRING;
            vehicleTableAdapter.Fill(_dsAtlanta.vehicle);
        }

        private GpsData[] GetDataGpsArray(atlantaDataSet.mobitelsRow m_row)
        {
            GpsData[] result = null;
            if (m_row.Is64bitPackets)
            {
                if (GpsDatas64.ContainsKey(m_row.Mobitel_ID))
                {
                    result = GpsDatas64[m_row.Mobitel_ID].ToArray();
                    GpsDatas64.Clear();
                }
            }
            else
            {
                result = DataSetManager.ConvertDataviewRowsToDataGpsArray(m_row.GetdataviewRows());
                _dsAtlanta.dataview.Clear();
            }
            return result;
        }
    }
}
