using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using LocalCache.Properties;
using TrackControl.General;
using TrackControl.General.DAL;
using TrackControl.General.DatabaseDriver;
using TrackControl.Reports;
using TrackControl.Statistics;
using TrackControl.Vehicles;

namespace LocalCache.atlantaDataSetTableAdapters
{
    public class DataviewTableAdapter
    {
        private static atlantaDataSet datasetData = null;
        private static int mobitelsID = 0;
        private static bool workingStateFlag = false;

//        public static string DATAGPS_SELECT_DIN_PARAMETER = @"
//            SELECT DataGps_ID, Mobitel_ID, ((Latitude * 1.0000) / 600000) AS Lat, ((Longitude * 1.0000) / 600000) AS Lon,FROM_UNIXTIME(UnixTime) AS `time`
//            FROM datagps FORCE INDEX(Index_3) 
//            WHERE (UnixTime BETWEEN UNIX_TIMESTAMP({1}Begin) AND UNIX_TIMESTAMP({1}End)) AND                                                                                                                                
//            (Valid = 1) {0} ORDER BY UnixTime";

//        public static string msDATAGPS_SELECT_DIN_PARAMETER = @"
//            SELECT DataGps_ID, Mobitel_ID, ((Latitude * 1.0000) / 600000) AS Lat, ((Longitude * 1.0000) / 600000) AS Lon,dbo.FROM_UNIXTIME(UnixTime) AS [time]
//            FROM datagps WITH (INDEX (Index_3)) 
//            WHERE (UnixTime BETWEEN dbo.UNIX_TIMESTAMPS({1}Begin) AND dbo.UNIX_TIMESTAMPS({1}End)) AND                                                                                                                                
//            (Valid = 1) {0} ORDER BY UnixTime";

        /// <summary>
        /// ������ ����������� � ���� ������
        /// </summary>
        public string CS
        {
            get { return _cs; }
            set { _cs = value; }
        }

        private string _cs;

        public DataviewTableAdapter()
        {
        }

        /// <summary>
        /// �����������.
        /// </summary>
        /// <param name="cs">������ �����������.</param>
        public DataviewTableAdapter(string cs)
        {
            _cs = cs;
        }

        /// <summary>
        /// Get daily history
        /// </summary>
        public void GetDailyHistory(atlantaDataSet dataset, CDailyHystory dh, int daysCount, BackgroundWorker work)
        {
            try
            {
                DateTime tm = DateTime.Now;
                checkCS();
                dh.Clear();
                //List<QuerySql> listSql = new List<QuerySql>();
                List<DriverDb> listSql = new List<DriverDb>();
                int pointer = 1;
                //QuerySql sql;
                DriverDb db;//MySqlParameter param;

                string cmdText = TrackControlQuery.DataviewTableAdapter.CmdHistory;
                string cmdText64 = TrackControlQuery.DataviewTableAdapter.CmdHistory64;

                if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                {
                    cmdText = String.Format( cmdText, daysCount );
                    cmdText64 = String.Format(cmdText64, daysCount);
                }
                else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                {
                    // cmdText = String.Format( msCMD_HISTORY, daysCount ); // ��� ������� �������� ���������

                    int secondDay = 86400 * daysCount; // ���������� ����� ������ � �������������� ������

                    // ����� �������� ��������� ��� ��� ms sql, aketner - 08.07.2013
                    DriverDb _db = new DriverDb();
                    _db.ConnectDb();

                    // �������� ������ ������� unix time �� ��
                    foreach( atlantaDataSet.mobitelsRow m_row in dataset.mobitels )
                    {
                        string commanda = "";
                        if (m_row.Is64bitPackets)
                            commanda = string.Format(cmdText64, m_row.Mobitel_ID, secondDay);
                        else
                            commanda = string.Format( cmdText, m_row.Mobitel_ID, secondDay );

                        DataTable tblUnixTm = new DataTable();

                        tblUnixTm = _db.GetDataTable( commanda );

                        // ������������ �� object � int
                        int[] listDate = new int[tblUnixTm.Rows.Count];

                        for( int j = 0; j < tblUnixTm.Rows.Count; j++ )
                        {
                            listDate[j] = Convert.ToInt32( tblUnixTm.Rows[j][0].ToString() );
                        } // for

                        // ����, ������������ � �������� ���� ����� ��� ��������� Mobitel_ID
                        List<DateTime> listResDts = new List<DateTime>();

                        DateTime resDate;
                        int promsec = 0;

                        if( listDate.Length > 0 )
                        {
                            //resDate = new DateTime( 1970, 1, 1 ).AddSeconds( listDate[0] );
                            resDate = StaticMethods.UnixTimeStampToDateTime(listDate[0]);
                            promsec = listDate[0] + 86400;
                            listResDts.Add( resDate );
                        }

                        for( int t = 1; t < listDate.Length; t++ )
                        {
                            if( promsec < listDate[t] )
                            {
                                if( Math.Abs( listDate[t - 1] - promsec ) < 86400 )
                                {
                                    //resDate = new DateTime( 1970, 1, 1 ).AddSeconds( listDate[t] );
                                    resDate = StaticMethods.UnixTimeStampToDateTime(listDate[t]);
                                    listResDts.Add( resDate );
                                    promsec = listDate[t] + 86400;
                                } // if
                            } // if
                        } // for

                        if( listDate.Length > 0 )
                        {
                            //resDate = new DateTime( 1970, 1, 1 ).AddSeconds(  );
                            resDate = StaticMethods.UnixTimeStampToDateTime( listDate[listDate.Length - 1] );

                            if( resDate.Day != listResDts[listResDts.Count - 1].Day )
                            {
                                listResDts.Add( resDate );
                            }
                        } // if

                        for( int h = listResDts.Count - 1; h >= 0; h-- )
                        {
                            dh.Add( m_row.Mobitel_ID, listResDts[h] );
                        } // for

                        tblUnixTm.Dispose();
                        listResDts.Clear();

                        work.ReportProgress( pointer );
                        pointer++;

                        if( work.CancellationPending )
                        {
                            break;
                        }
                    } // foreach

                    _db.CloseDbConnection();

                    return;
                } // DriverDb.MssqlUse
                
                foreach (atlantaDataSet.mobitelsRow m_row in dataset.mobitels)
                {
                    if (work.CancellationPending)
                        break;

                    //sql = new QuerySql();
                    db = new DriverDb();
                    //sql.reader = null;
                    db.SqlDataReader = null;
                    //sql.cmd = new MySqlCommand();
                    db.NewSqlCommand();
                    //db.SqlCmd(db.GetCommand);
                    //sql.cmd.Connection = new MySqlConnection(_cs);
                    db.CommandConnection(_cs);
                    //sql.cmd.CommandTimeout = 2400;
                    db.CommandTimeout(2500);
                    //sql.cmd.CommandType = CommandType.Text;
                    db.CommandType(CommandType.Text);
                    //sql.cmd.CommandText = cmdText;
                    if (m_row.Is64bitPackets)
                        db.CommandText(cmdText64);
                    else
                        db.CommandText(cmdText);
                    //param = sql.cmd.CreateParameter();
                    if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                    {
                        MySqlParameter param = null;
                        param = (MySqlParameter) db.CommandCreateParameter();
                        param.DbType = DbType.Int32;
                        param.ParameterName = db.ParamPrefics + "m_id";
                        param.SourceColumn = "Mobitel_ID";
                        //sql.cmd.Parameters.Add(param);
                        db.CommandParametersAdd(param);
                        param.Value = m_row.Mobitel_ID;
                    } // if
                    else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                    {
                        SqlParameter param = null;
                        param = (SqlParameter) db.CommandCreateParameter();
                        param.DbType = DbType.Int32;
                        param.ParameterName = "@m_id";
                        param.SourceColumn = "Mobitel_ID";
                        //sql.cmd.Parameters.Add(param);
                        db.CommandParametersAdd(param);
                        param.Value = m_row.Mobitel_ID;
                    } // if else

                    //sql.cmd.Connection.Open();
                    db.CommandConnectionOpen();
                    //sql.result = sql.cmd.BeginExecuteReader();
                    db.AsyncResult = db.CommandBeginExecuteReader();

                    //listSql.Add(sql);
                    listSql.Add(db);

                    if (!work.CancellationPending && listSql.Count > 3)
                    {
                        GetDailyHistoryExtracted(dh, listSql, work);
                    }

                    work.ReportProgress(pointer);
                    pointer++;
                } // foreach

                if (!work.CancellationPending && listSql.Count > 0)
                {
                   GetDailyHistoryExtracted(dh, listSql, work);
                }
            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error ({0}): {1}", ex.Number, ex.Message);
                throw new Exception(String.Format("MySql Exception: {0}\r\n{1}",
                                                  ex.Message, ex.InnerException == null ? "" : ex.InnerException.Message),
                                    ex.InnerException);
            }
            catch (OutOfMemoryException ex)
            {
                Console.WriteLine("Error: {0}", ex.Message);
                throw new OutOfMemoryException(String.Format(
                    "Not enough memory to complete operation: {0}\r\n{1}",
                    ex.Message, ex.InnerException == null ? "" : ex.InnerException.Message),
                                               ex.InnerException);
            }
            catch (SqlException ex)
            {
                Console.WriteLine("Error ({0}): {1}", ex.Number, ex.Message);
                throw new Exception(String.Format("MS Sql Exception: {0}\r\n{1}",
                                                  ex.Message, ex.InnerException == null ? "" : ex.InnerException.Message),
                                    ex.InnerException);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                //MySqlConnection.ClearAllPools();
                DriverDb.SqlClearAllPools();
            }

            DateTime tms = DateTime.Now;
        } // GetDailHistory

		/// <summary>
		/// Get daily history for mobitel 
		/// </summary>
		public void GetDailyHistory (atlantaDataSet dataset, CDailyHystory dh, int daysCount, BackgroundWorker work, int mobitel)
		{
			try {
				DateTime tm = DateTime.Now;
				checkCS ();
				dh.Clear ();
				//List<QuerySql> listSql = new List<QuerySql>();
				List<DriverDb> listSql = new List<DriverDb> ();
				int pointer = 1;
				//QuerySql sql;
				DriverDb db;
				//MySqlParameter param;

				string cmdText = TrackControlQuery.DataviewTableAdapter.CmdHistory;
				string cmdText64 = TrackControlQuery.DataviewTableAdapter.CmdHistory64;

				if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse) {
					cmdText = String.Format (cmdText, daysCount);
					cmdText64 = String.Format (cmdText64, daysCount);
				} else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse) {
					// cmdText = String.Format( msCMD_HISTORY, daysCount ); // ��� ������� �������� ���������

					int secondDay = 86400 * daysCount; // ���������� ����� ������ � �������������� ������

					// ����� �������� ��������� ��� ��� ms sql, aketner - 08.07.2013
					DriverDb _db = new DriverDb ();
					_db.ConnectDb ();

					// �������� ������ ������� unix time �� ��
					foreach (atlantaDataSet.mobitelsRow m_row in dataset.mobitels.Select("Mobitel_ID=" + mobitel)) {
						string commanda = "";
						if (m_row.Is64bitPackets)
							commanda = string.Format (cmdText64, m_row.Mobitel_ID, secondDay);
						else
							commanda = string.Format (cmdText, m_row.Mobitel_ID, secondDay);

						DataTable tblUnixTm = new DataTable ();

						tblUnixTm = _db.GetDataTable (commanda);

						// ������������ �� object � int
						int [] listDate = new int [tblUnixTm.Rows.Count];

						for (int j = 0; j < tblUnixTm.Rows.Count; j++) {
							listDate [j] = Convert.ToInt32 (tblUnixTm.Rows [j] [0].ToString ());
						} // for

						// ����, ������������ � �������� ���� ����� ��� ��������� Mobitel_ID
						List<DateTime> listResDts = new List<DateTime> ();

						DateTime resDate;
						int promsec = 0;

						if (listDate.Length > 0) {
							//resDate = new DateTime( 1970, 1, 1 ).AddSeconds( listDate[0] );
							resDate = StaticMethods.UnixTimeStampToDateTime (listDate [0]);
							promsec = listDate [0] + 86400;
							listResDts.Add (resDate);
						}

						for (int t = 1; t < listDate.Length; t++) {
							if (promsec < listDate [t]) {
								if (Math.Abs (listDate [t - 1] - promsec) < 86400) {
									//resDate = new DateTime( 1970, 1, 1 ).AddSeconds( listDate[t] );
									resDate = StaticMethods.UnixTimeStampToDateTime (listDate [t]);
									listResDts.Add (resDate);
									promsec = listDate [t] + 86400;
								} // if
							} // if
						} // for

						if (listDate.Length > 0) {
							//resDate = new DateTime( 1970, 1, 1 ).AddSeconds(  );
							resDate = StaticMethods.UnixTimeStampToDateTime (listDate [listDate.Length - 1]);

							if (resDate.Day != listResDts [listResDts.Count - 1].Day) {
								listResDts.Add (resDate);
							}
						} // if

						for (int h = listResDts.Count - 1; h >= 0; h--) {
							dh.Add (m_row.Mobitel_ID, listResDts [h]);
						} // for

						tblUnixTm.Dispose ();
						listResDts.Clear ();

						work.ReportProgress (pointer);
						pointer++;

						if (work.CancellationPending) {
							break;
						}
					} // foreach

					_db.CloseDbConnection ();

					return;
				} // DriverDb.MssqlUse

				foreach (atlantaDataSet.mobitelsRow m_row in dataset.mobitels.Select("Mobitel_ID=" + mobitel)) {
					if (work.CancellationPending)
						break;

					//sql = new QuerySql();
					db = new DriverDb ();
					//sql.reader = null;
					db.SqlDataReader = null;
					//sql.cmd = new MySqlCommand();
					db.NewSqlCommand ();
					//db.SqlCmd(db.GetCommand);
					//sql.cmd.Connection = new MySqlConnection(_cs);
					db.CommandConnection (_cs);
					//sql.cmd.CommandTimeout = 2400;
					db.CommandTimeout (2500);
					//sql.cmd.CommandType = CommandType.Text;
					db.CommandType (CommandType.Text);
					//sql.cmd.CommandText = cmdText;
					if (m_row.Is64bitPackets)
						db.CommandText (cmdText64);
					else
						db.CommandText (cmdText);
					//param = sql.cmd.CreateParameter();
					if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse) {
						MySqlParameter param = null;
						param = (MySqlParameter)db.CommandCreateParameter ();
						param.DbType = DbType.Int32;
						param.ParameterName = db.ParamPrefics + "m_id";
						param.SourceColumn = "Mobitel_ID";
						//sql.cmd.Parameters.Add(param);
						db.CommandParametersAdd (param);
						param.Value = m_row.Mobitel_ID;
					} // if
					else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse) {
						SqlParameter param = null;
						param = (SqlParameter)db.CommandCreateParameter ();
						param.DbType = DbType.Int32;
						param.ParameterName = "@m_id";
						param.SourceColumn = "Mobitel_ID";
						//sql.cmd.Parameters.Add(param);
						db.CommandParametersAdd (param);
						param.Value = m_row.Mobitel_ID;
					} // if else

					//sql.cmd.Connection.Open();
					db.CommandConnectionOpen ();
					//sql.result = sql.cmd.BeginExecuteReader();
					db.AsyncResult = db.CommandBeginExecuteReader ();

					//listSql.Add(sql);
					listSql.Add (db);

					if (!work.CancellationPending && listSql.Count > 3) {
						GetDailyHistoryExtracted (dh, listSql, work);
					}

					work.ReportProgress (pointer);
					pointer++;
				} // foreach

				if (!work.CancellationPending && listSql.Count > 0) {
					GetDailyHistoryExtracted (dh, listSql, work);
				}
			} catch (MySqlException ex) {
				Console.WriteLine ("Error ({0}): {1}", ex.Number, ex.Message);
				throw new Exception (String.Format ("MySql Exception: {0}\r\n{1}",
												  ex.Message, ex.InnerException == null ? "" : ex.InnerException.Message),
									ex.InnerException);
			} catch (OutOfMemoryException ex) {
				Console.WriteLine ("Error: {0}", ex.Message);
				throw new OutOfMemoryException (String.Format (
					"Not enough memory to complete operation: {0}\r\n{1}",
					ex.Message, ex.InnerException == null ? "" : ex.InnerException.Message),
											   ex.InnerException);
			} catch (SqlException ex) {
				Console.WriteLine ("Error ({0}): {1}", ex.Number, ex.Message);
				throw new Exception (String.Format ("MS Sql Exception: {0}\r\n{1}",
												  ex.Message, ex.InnerException == null ? "" : ex.InnerException.Message),
									ex.InnerException);
			} catch (Exception ex) {
				throw ex;
			} finally {
				//MySqlConnection.ClearAllPools();
				DriverDb.SqlClearAllPools ();
			}

			DateTime tms = DateTime.Now;
		}

        /// <summary>
        /// �������� ������� ������� ������ MySqlCommand.
        /// </summary>
        /// <param name="mobitelId">������������� ���������.</param>
        /// <param name="timeStart">��������� �����.</param>
        /// <param name="timeEnd">�������� �����.</param>
        /// <returns>MySqlCommand</returns>
        private object GetSelectCommand(int mobitelId, DateTime timeStart, DateTime timeEnd, bool is64BitPackets)
        {
            if (!FormInterval.isActivate())
            {
                DriverDb db = new DriverDb();
                db.NewSqlCommand();

                if (is64BitPackets)
                    db.CommandText(TrackControlQuery.DataviewTableAdapter.SelectDatagpsId64);
                else
                    db.CommandText(TrackControlQuery.DataviewTableAdapter.SelectDatagpsId);
                
                db.NewSqlConnection(CS);
                db.CommandSqlConnection();
                db.CommandTimeout(800);

                db.CommandParametersAdd(db.ParamPrefics + "MobitelId", db.GettingInt32(), mobitelId);
                db.CommandParametersAdd(db.ParamPrefics + "Begin", db.GettingDateTime(), timeStart);
                db.CommandParametersAdd(db.ParamPrefics + "End", db.GettingDateTime(), timeEnd);

                return db.GetCommand;
                
                //return cmd;
            }
            else
            {
                DriverDb db = new DriverDb();
                db.NewSqlCommand();
                db.NewSqlConnection(CS);
                db.CommandSqlConnection();
                db.CommandTimeout(800);
                db.CommandType(CommandType.StoredProcedure);
                db.CommandText("SelectDataGpsPeriodAll");
                db.CommandParametersAdd(db.ParamPrefics + "mobile", db.GettingInt32(), mobitelId);
                db.CommandParametersAdd(db.ParamPrefics + "beginperiod", db.GettingDateTime(), timeStart);
                db.CommandParametersAdd(db.ParamPrefics + "endperiod", db.GettingDateTime(), timeEnd);
                db.CommandParametersAdd(db.ParamPrefics + "timebegin", db.GettingVarChar(),
                                            FormInterval.getTimePeriodBegin());
                db.CommandParametersAdd(db.ParamPrefics + "timeend", db.GettingVarChar(),
                                            FormInterval.getTimePeriodEnd());
                db.CommandParametersAdd(db.ParamPrefics + "weekdays", db.GettingVarChar(),
                                            FormInterval.getWeekPeriod());
                db.CommandParametersAdd(db.ParamPrefics + "daymonth", db.GettingVarChar(),
                                            FormInterval.getMonthPeriod());
                //TODO  - ��������� ��� ������� 64 ��������� �������
                //db.CommandParametersAdd(db.ParamPrefics + "is64BitPackets", db.GettingBit(),
                //                            is64BitPackets);              

                return db.GetCommand;
            } // else
        } // GetSelectCommand

        private object GetSelectCommand(int mobitelId, long timeStart, long timeEnd, bool is64BitPackets)
        {
            if( !FormInterval.isActivate() )
            {
                DriverDb db = new DriverDb();
                db.NewSqlCommand();
                if (is64BitPackets)
                   db.CommandText(string.Format(TrackControlQuery.DataviewTableAdapter.SelectDatagps64,""));
                else
                    db.CommandText( TrackControlQuery.DataviewTableAdapter.SelectDatagps );
                db.NewSqlConnection( CS );
                db.CommandSqlConnection();
                db.CommandTimeout(800);

                db.CommandParametersAdd( db.ParamPrefics + "MobitelId", db.GettingInt32(), mobitelId );
                db.CommandParametersAdd( db.ParamPrefics + "Begin", db.GettingInt64(), timeStart );
                db.CommandParametersAdd( db.ParamPrefics + "End", db.GettingInt64(), timeEnd );

                return db.GetCommand;
            }
            else
            {
                DriverDb db = new DriverDb();
                db.NewSqlCommand();
                db.NewSqlConnection( CS );
                db.CommandSqlConnection();
                db.CommandTimeout( 800 );
                
                db.CommandType( CommandType.StoredProcedure );
                db.CommandText( "SelectDataGpsPeriodAll" );
                db.CommandParametersAdd( db.ParamPrefics + "mobile", db.GettingInt32(), mobitelId );
                db.CommandParametersAdd( db.ParamPrefics + "beginperiod", db.GettingDateTime(), timeStart );
                db.CommandParametersAdd( db.ParamPrefics + "endperiod", db.GettingDateTime(), timeEnd );
                db.CommandParametersAdd( db.ParamPrefics + "timebegin", db.GettingVarChar(),
                                        FormInterval.getTimePeriodBegin() );
                db.CommandParametersAdd( db.ParamPrefics + "timeend", db.GettingVarChar(),
                                        FormInterval.getTimePeriodEnd() );
                db.CommandParametersAdd( db.ParamPrefics + "weekdays", db.GettingVarChar(),
                                        FormInterval.getWeekPeriod() );
                db.CommandParametersAdd( db.ParamPrefics + "daymonth", db.GettingVarChar(),
                                        FormInterval.getMonthPeriod() );

                return db.GetCommand;
            } // else

            return null;
        } // GetSelectCommand

        public void WorkProcessState(bool state)
        {
            workingStateFlag = state;
        }

        public void Fill(atlantaDataSet dataset, DateTime begin, DateTime end, BackgroundWorker work, ref string name, Dictionary<int, List<GpsData>> GpsDatas64 )
        {
            if (workingStateFlag)
                return;
            datasetData = dataset; // ��� ���������� �������������
            checkCS();
            List<DriverDb> listSql = new List<DriverDb>();
            GpsDatas64.Clear();
            datasetData.dataview.Clear();
            GC.Collect();DriverDb db;

            try
            {
                foreach (atlantaDataSet.mobitelsRow m_row in dataset.mobitels)
                {
                    if( workingStateFlag )
                        return;
                    
                    if (m_row.Check)
                    {
                        db = new DriverDb();
                        db.SqlDataReader = null;

                        if (!FormInterval.isActivate())
                        {
                            long epochBegin = StaticMethods.ConvertToUnixTimestamp(begin);
                            long epochEnd = StaticMethods.ConvertToUnixTimestamp(end);
                            db.GetCommand = GetSelectCommand(m_row.Mobitel_ID, epochBegin, epochEnd, m_row.Is64bitPackets);
                        }
                        else
                        {
                            db.GetCommand = GetSelectCommand(m_row.Mobitel_ID, begin, end, m_row.Is64bitPackets);                            
                        }

                        db.CommandConnectionOpen();
                        db.AsyncResult = db.CommandBeginExecuteReader(); 
                       
                        listSql.Add(db);

                        if (listSql.Count > 3)
                        {
                            FillByIdExtracted(dataset, work, ref name, listSql, GpsDatas64);
                        } // if
                    } // if
                } // foreach

                if (listSql.Count > 0)
                {
                    if( workingStateFlag )
                        return;

                    FillByIdExtracted(dataset, work, ref name, listSql, GpsDatas64);
                }
            } // try
            catch (MySqlException ex)
            {
                Console.WriteLine("Error ({0}): {1}", ex.Number, ex.Message);
                throw new Exception(String.Format("MySql Exception: {0}\r\n{1}",
                                                  ex.Message, ex.InnerException == null ? "" : ex.InnerException.Message),
                                    ex.InnerException);
            }
            catch (OutOfMemoryException ex)
            {
                Console.WriteLine("Error: {0}", ex.Message);
                throw new OutOfMemoryException(String.Format(
                    "Not enough memory to complete operation: {0}\r\n{1}",
                    ex.Message, ex.InnerException == null ? "" : ex.InnerException.Message),
                                               ex.InnerException);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + "\n" + ex.StackTrace);
            }
            finally
            {
                //MySqlConnection.ClearAllPools();
                DriverDb.SqlClearAllPools();
            }
        } // Fill

        private static void ProgresBackgoundWork(BackgroundWorker work, int i)
        {
            if (i > 100000)
            {
                i %= 100000;
            }
            if (i%1000 == 0)
            {
                work.ReportProgress(i/1000);
            }
        }

        private static double dist = 0;

        private static void GetDailyHistoryExtracted(CDailyHystory dh, List<DriverDb> listSql, BackgroundWorker work) // -----------
        {
            if (work.CancellationPending)
              return;

            DriverDb db = new DriverDb();
            
            while (listSql.Count > 0)
            {
                int i;

                for (i = 0; i < listSql.Count; i++)
                {
                    if (work.CancellationPending)
                        break;

                    if (listSql[i].AsyncResult.IsCompleted)
                    {
                        db = listSql[i];
                        break;
                    }
                }

                if (i >= listSql.Count)
                    continue;

                listSql.Remove(listSql[i]);

                if (work.CancellationPending)
                    return;

                db.CommandEndExecuteReader(db.AsyncResult);
                int ttId = (int) db.GetCommandParameters(0);

                while (db.Read())
                {
                    if (work.CancellationPending)
                    {
                        db.CloseDataReader();
                        db.CommandConnectionClose();
                        break;
                    } // if

                    dh.Add(ttId, db.ReaderGetDateTime(0));
                } // while

                db.CloseDataReader();
                db.CommandConnectionClose();
            }
        }

        private static void FillByIdExtracted(atlantaDataSet dataset, BackgroundWorker work, ref string name, 
                                                List<DriverDb> listSql, Dictionary<int, List<GpsData>> gpsDatas64) 
        {
            try
            {
                var db = new DriverDb();
               
                int Count = 0;
              
                while (listSql.Count > 0)
                {
                    int i = 0;

                    if( workingStateFlag )
                        return;

                    for(i = 0; i < listSql.Count; i++)
                    {
                        if(listSql[i].AsyncResult.IsCompleted)
                        {
                            db = listSql[i];
                            break;
                        } // if
                    } // for

                    if( i >= listSql.Count )
                    {
                        Count++;
                        continue;
                    }
                    
                    db.CommandEndExecuteReader(db.AsyncResult);
        
                    atlantaDataSet.mobitelsRow m_row = 
                        dataset.mobitels.FindByMobitel_ID( ( int )db.GetCommandParameters( 0 ) );

                    name = m_row.Name;
                    mobitelsID = m_row.Mobitel_ID;

                    if (m_row.Is64bitPackets)
                    {
                        List<GpsData> trackPoints = GetVehicleTrack(work, listSql, db, i,!m_row.IsNotDrawDgps);
                        TrackRepairService.CheckTrack(trackPoints);

                        if (trackPoints != null && trackPoints.Count > 0)
                        {
                            if (!gpsDatas64.ContainsKey(m_row.Mobitel_ID))
                            {
                                gpsDatas64.Add(m_row.Mobitel_ID, trackPoints); 
                                dist = 0;
                                for(int j = 1; j < trackPoints.Count; j++)
                                {
                                    dist += �GeoDistanceForCache.CalculateFromGrad(trackPoints[j - 1].LatLng.Lat, trackPoints[j - 1].LatLng.Lng, trackPoints[j].LatLng.Lat, trackPoints[j].LatLng.Lng);
                                }
                                m_row.path = dist; // in kilometer
                            }
                        }
                    }
                    else 
                    {
                        SetDataSetDataViewRows(dataset, work, listSql, db, i);
                    }

                    m_row.path = dist;
                } // while
            } // try
            catch (Exception e)
            {
                throw new Exception(e.Message + e.StackTrace);
            }
        } // FilleByIdExtracted

        private static void  SetDataSetDataViewRows(atlantaDataSet dataset, BackgroundWorker work, List<DriverDb> listSql, DriverDb db, int i)
        {
            int n = 0;
            dist = 0;

            List<atlantaDataSet.dataviewRow> rowsList = new List<atlantaDataSet.dataviewRow>();
            atlantaDataSet.dataviewRow row;
            object[] data;

            if (!FormInterval.isActivate())
            {
                while (db.Read())
                {
                    row = dataset.dataview.NewdataviewRow();
                    data = new object[db.ReaderVisibleFieldCount()];
                    db.ReaderGetValues(data);

                    data[2] = Convert.ToDouble(data[2].ToString()) / 600000.0;
                    data[3] = Convert.ToDouble(data[3].ToString()) / 600000.0;
                    data[6] = Convert.ToDouble(data[6].ToString()) * 1.852;
                    data[5] = StaticMethods.UnixTimeStampToDateTime(Convert.ToDouble(data[5].ToString()));
                    
                    row.ItemArray = data;
                    rowsList.Add(row);

                    if ((n % 100) == 0)
                        ProgresBackgoundWork(work, n);

                    ++n;
                } // while
            } // if
            else
            {
                while (db.Read())
                {
                    row = dataset.dataview.NewdataviewRow();
                    data = new object[db.ReaderVisibleFieldCount()];
                    db.ReaderGetValues(data);
                    row.ItemArray = data;
                    rowsList.Add(row);

                    if ((n % 100) == 0)
                        ProgresBackgoundWork(work, n);

                    ++n;
                } // while
            } // else

            db.CloseDataReader();
            db.CommandConnectionClose();
            listSql.Remove(listSql[i]);

            CheckTrack(rowsList);
            ProgresBackgoundWork(work, n += rowsList.Count);

            CalcAcceleration(rowsList);
            ProgresBackgoundWork(work, n += rowsList.Count);

            FillDataView(dataset, rowsList);
            ProgresBackgoundWork(work, n += rowsList.Count);
        }

        private static List<GpsData> GetVehicleTrack(BackgroundWorker work, List<DriverDb> listSql, DriverDb db, int i,bool isDrawDgps)
        {
            atlantaDataSet.vehicleRow[] vehRow = (atlantaDataSet.vehicleRow[]) datasetData.vehicle.Select("Mobitel_id=" + mobitelsID);

            if (vehRow.Length <= 0)
            {
                MessageBox.Show(string.Format(Resources.VehicleEmpty, mobitelsID) , "Error GetVehicleTrack",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            atlantaDataSet.settingRow settingRows =
                (atlantaDataSet.settingRow) datasetData.setting.FindByid(vehRow[0].setting_id);

            float minimSpeed = (float)settingRows.speedMin;

            int n = 0;
            dist = 0;
            List<GpsData> trackPoints = new List<GpsData>();

            while (db.Read())
            {
                GpsData gps = MobitelProvider.GetOneRecord64(db, isDrawDgps, minimSpeed);
                trackPoints.Add(gps);
                //----------------------------------------
                //gps = MobitelProvider.GetOneRecord64(db, isDrawDgps, minimSpeed);
                //DataSetManager.GpsDatasForDemoTest64Packet.Add(gps);
                //----------------------------------------
                if ((n % 100) == 0)
                    ProgresBackgoundWork(work, n);

                ++n;
            } // while

            db.CloseDataReader();
            db.CommandConnectionClose();
            listSql.Remove(listSql[i]);
            ProgresBackgoundWork(work, n += trackPoints.Count);
            //CheckTrack(gps);
            CalcAcceleration(trackPoints);
            return trackPoints;
        }

        [Conditional("DEBUG")]
        private void checkCS()
        {
            if (String.IsNullOrEmpty(_cs))
                throw new Exception("��� DataviewTableAdapter �� ���� ������ ������ ����������� � ��");
        }

        /// <summary>
        /// ������� �� �� ������ ��������� �� ��������
        /// ���������� �������.
        /// </summary>
        /// <param name="dataset">������� ����� ������ ��� �������� ����� ������ dataviewRow.</param>
        /// <param name="mobitelId">������������� ���������.</param>
        /// <param name="timeStart">��������� �����.</param>
        /// <param name="timeEnd">�������� �����.</param>
        /// <returns>������ ����� dataviewRow.</returns>
        internal List<atlantaDataSet.dataviewRow> GetDataViewRows(atlantaDataSet dataset,int mobitelId,
                                                                   DateTime timeStart, DateTime timeEnd)
        {
            List<atlantaDataSet.dataviewRow> result = new List<atlantaDataSet.dataviewRow>();

            DriverDb db = new DriverDb();
            {
                db.GetCommand = GetSelectCommand( mobitelId, timeStart, timeEnd,false );
                db.CommandConnectionOpen();
                try
                {
                    db.SqlDataReader = db.CommandExecuteReader();
                    {
                        atlantaDataSet.dataviewRow row;
                        dist = 0;
                        object[] data;
                       
                        while (db.Read())
                        {
                            row = dataset.dataview.NewdataviewRow();
        
                            data = new object[db.DataReaderVisibleFieldCount];
                            db.DataReaderGetValues(data);
                            data[2] = Convert.ToDouble( data[2].ToString() ) / 600000.0;
                            data[3] = Convert.ToDouble( data[3].ToString() ) / 600000.0;
                            data[6] = Convert.ToDouble( data[6].ToString() ) * 1.852;
                            data[5] = StaticMethods.UnixTimeStampToDateTime( Convert.ToDouble( data[5].ToString() ) );
                            row.ItemArray = data;
                            result.Add(row);
                        }
                    }
                    db.CloseDataReader();
                }
                finally
                {
                    db.CloseDataReader();
                    db.CommandConnectionClose();
                }
            }
            CheckTrack(result);
            CalcAcceleration(result);
            return result;
        }

        /// <summary>
        /// ������� �� �� ������ ��������� �� ��������
        /// ���������� �������.
        /// </summary>
        /// <param name="dataset">������� ����� ������ ��� �������� ����� ������ dataviewRow.</param>
        /// <param name="mobitelId">������������� ���������.</param>
        /// <param name="timeStart">��������� �����.</param>
        /// <param name="timeEnd">�������� �����.</param>
        /// <returns>������ ����� dataviewRow.</returns>
        internal List<atlantaDataSet.dataviewRow> GetDataViewRows( atlantaDataSet dataset, int mobitelId,
                                                                   long timeStart, long timeEnd )
        {
            List<atlantaDataSet.dataviewRow> result = new List<atlantaDataSet.dataviewRow>();

            //  using (MySqlCommand cmd = GetSelectCommand(mobitelId, timeStart, timeEnd))
            DriverDb db = new DriverDb();
            {
                db.GetCommand = GetSelectCommand( mobitelId, timeStart, timeEnd,false );
                //cmd.Connection.Open();
                db.CommandConnectionOpen();
                try
                {
                    //using (MySqlDataReader reader = cmd.ExecuteReader())
                    //MySqlDataReader reader = db.CommandExecuteReader();
                    db.SqlDataReader = db.CommandExecuteReader();
                    {
                        atlantaDataSet.dataviewRow row;
                        dist = 0;
                        object[] data;
                        //while (reader.Read())
                        while( db.Read() )
                        {
                            row = dataset.dataview.NewdataviewRow();
                            //data = new object[reader.VisibleFieldCount];
                            data = new object[db.DataReaderVisibleFieldCount];
                            //reader.GetValues(data);
                            db.DataReaderGetValues( data );
                            data[2] = Convert.ToDouble( data[2].ToString() ) / 600000.0;
                            data[3] = Convert.ToDouble( data[3].ToString() ) / 600000.0;
                            data[6] = Convert.ToDouble( data[6].ToString() ) * 1.852;
                            data[5] = StaticMethods.UnixTimeStampToDateTime( Convert.ToDouble( data[5].ToString() ) );

                            row.ItemArray = data;
                            result.Add( row );
                        }
                    }
                    db.CloseDataReader();
                }
                finally
                {
                    //cmd.Connection.Close();
                    db.CloseDataReader();
                    db.CommandConnectionClose();
                }
            }
            CheckTrack( result );
            CalcAcceleration( result );
            return result;
        }


        /// <summary>
        /// �������� ����� �� ��������� "�������", ��������� ��������� ����� � ������
        /// ������������� � �����������.
        /// ���������� ������ ��� ��������� ����� �� ��������������, � ���������.
        /// </summary>
        /// <param name="rowsList">��������� dataviewRow.</param>
        private static void CheckTrack(List<atlantaDataSet.dataviewRow> rowsList)
        {

            if (rowsList.Count == 0)
            {
                return;
            }

            rowsList[0].dist = 0;

            if (rowsList.Count == 1)
            {
                return;
            }

            if (rowsList.Count == 2)
            {
                CheckTwoPoinsTrack(rowsList);
                return;
            }

            // ������ �����, ����� ���������� ��������
            CheckTrackExceptLastSegment(rowsList);
            // ������� ��������� ������� �����
            CheckLastSegment(rowsList);
        }

        /// <summary>
        /// �������� ���������� �������, ��������� ������ �� ���� �����. 
        /// </summary>
        /// <param name="rowsList">��������� dataviewRow.</param>
        private static void CheckTwoPoinsTrack(List<atlantaDataSet.dataviewRow> rowsList)
        {
            atlantaDataSet.dataviewRow first = rowsList[0];
            atlantaDataSet.dataviewRow second = rowsList[1];
            first.dist = 0;
            second.dist = �GeoDistanceForCache.CalculateFromGrad(
                first.Lat, first.Lon, second.Lat, second.Lon);

            if (IsCurrentRowNotValid(first, second))
            {
                // ������� ��� �����, ������ ��� �� �����, ����� �� ��� ����������.
                rowsList.Clear();
            }
            else
            {
                IncTotalDistance(second.dist);
            }
        }

        /// <summary>
        /// �������� ���������� � ���������, � ������ �������������,
        /// ������� ����� �� ��������� � ����������.
        /// ��������� ������� �� �����������.
        /// </summary>
        /// <param name="rowsList">��������� dataviewRow.</param>
        private static void CheckTrackExceptLastSegment(List<atlantaDataSet.dataviewRow> rowsList)
        {
            atlantaDataSet.dataviewRow prevRow, curRow, nextRow;
            bool deleteFirstRow = false;
            try
            {
                for (int i = 1; i < rowsList.Count - 1; i++)
                {
                    prevRow = rowsList[i - 1];
                    curRow = rowsList[i];
                    nextRow = rowsList[i + 1];

                    nextRow.dist = 0;

                    curRow.dist = �GeoDistanceForCache.CalculateFromGrad(
                        prevRow.Lat, prevRow.Lon, curRow.Lat, curRow.Lon);

                    if (!IsValidLogId(prevRow, curRow, nextRow, true))
                    {
                        curRow.Lon = prevRow.Lon;
                        curRow.Lat = prevRow.Lat;
                        curRow.dist = 0;
                        curRow.Events = prevRow.Events;
                        curRow.speed = prevRow.speed;
                    }

                    if (IsCurrentRowNotValid(prevRow, curRow))
                    {
                        if ((i == 1) && (!IsCurrentRowNotValid(curRow, nextRow)))
                        {
                            // ������ ����� ���������� ��� ���������� �� ��������� � ������, 
                            // � ������ �������� �� ��������� �� ������ - ������ ���������� �������� ������ �����.
                            // ������ ����� ������, �.�. �� ����� ��� ��������� ����������.
                            deleteFirstRow = true;
                            curRow.dist = 0;
                        }
                        else
                        {
                            // ���� ���������� ����� ����� 0 �������� ���� �� ������ ��������� 
                            //(�������� ����������� �������������)
                            bool isRowNotValid = IsPrevRowWithSpeedNotValid(rowsList, prevRow, curRow, i);
                            //isRowNotValid = false;
                            if (isRowNotValid)
                            {
                                if ((curRow.speed == 0.0) && (curRow.LogID - prevRow.LogID == 1))
                                {
                                    // � ����� �� ����������, � ���������� ����� ���, ���������� ��� � ���������� ����� 
                                    curRow.Lat = prevRow.Lat;
                                    curRow.Lon = prevRow.Lon;
                                    curRow.dist = 0;
                                }
                                else
                                {
                                    nextRow.dist = �GeoDistanceForCache.CalculateFromGrad(
                                        prevRow.Lat, prevRow.Lon, nextRow.Lat, nextRow.Lon);
                                    if (!IsCurrentRowNotValid(prevRow, nextRow))
                                    {
                                        AdjustOnePoint(prevRow, curRow, nextRow);
                                    }
                                    else
                                    {
                                        AdjustListPoints(rowsList, prevRow, curRow, ref nextRow, ref i);

                                    }
                                }
                            }
                            else
                                IncTotalDistance(curRow.dist);
                        }
                    }
                    else
                        IncTotalDistance(curRow.dist);
                }

                if (deleteFirstRow)
                {
                    rowsList.RemoveAt(0);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private static bool IsPrevRowWithSpeedNotValid(List<atlantaDataSet.dataviewRow> rowsList,
                                                       atlantaDataSet.dataviewRow prevRow,
                                                       atlantaDataSet.dataviewRow curRow, int i)
        {
            bool isRowNotValid = true;
            if (prevRow.speed == 0 || prevRow.dist == 0)
            {
                //isRowNotValid = true;
                atlantaDataSet.dataviewRow prevRowWithSpeed = null;
                for (int j = i - 2; j > 0; j--)
                {
                    if (rowsList[j].speed > 0 || rowsList[j].dist > 0)
                    {
                        prevRowWithSpeed = rowsList[j];
                        if (!IsCurrentRowNotValid(prevRowWithSpeed, curRow)) isRowNotValid = false;
                        break;
                    }
                }
            }
            return isRowNotValid;
        }

        private static void AdjustListPoints(List<atlantaDataSet.dataviewRow> rowsList,
                                             atlantaDataSet.dataviewRow prevRow, atlantaDataSet.dataviewRow curRow,
                                             ref atlantaDataSet.dataviewRow nextRow, ref int i)
        {
            List<atlantaDataSet.dataviewRow> rowsNoValid = new List<atlantaDataSet.dataviewRow>();
            rowsNoValid.Add(curRow);
            i++;
            //������ ���������� �����
            while (IsCurrentRowNotValid(prevRow, nextRow) && i < rowsList.Count - 2)
            {
                if (!rowsNoValid.Contains(nextRow)) rowsNoValid.Add(nextRow);
                i++;
                nextRow = rowsList[i];
                nextRow.dist = �GeoDistanceForCache.CalculateFromGrad(
                    prevRow.Lat, prevRow.Lon, nextRow.Lat, nextRow.Lon);

            }
            // ��������� ������ ���������� �����
            double deltaLat = (nextRow.Lat - prevRow.Lat)/(rowsNoValid.Count + 1);
            double deltaLon = (nextRow.Lon - prevRow.Lon)/(rowsNoValid.Count + 1);
            double dist = �GeoDistanceForCache.CalculateFromGrad(
                prevRow.Lat, prevRow.Lon, nextRow.Lat, nextRow.Lon)/(rowsNoValid.Count + 1);
            nextRow.dist = dist;
            //double speed = 0;
            //if (nextRow.time.Subtract(prevRow.time).TotalHours > 0) speed = dist * (rowsNoValid.Count + 1) / (nextRow.time.Subtract(prevRow.time).TotalHours);
            for (int j = 0; j < rowsNoValid.Count; j++)
            {
                if (i == rowsList.Count - 2)
                {
                    rowsNoValid[j].Valid = 0;
                }
                else
                {
                    rowsNoValid[j].Lat = prevRow.Lat + (j + 1)*deltaLat;
                    rowsNoValid[j].Lon = prevRow.Lon + (j + 1)*deltaLon;
                    rowsNoValid[j].dist = dist;
                    //rowsNoValid[j].speed = speed;
                    IncTotalDistance(curRow.dist);
                }
            }
        }

        private static void AdjustOnePoint(atlantaDataSet.dataviewRow prevRow, atlantaDataSet.dataviewRow curRow,
                                           atlantaDataSet.dataviewRow nextRow)
        {
            // ���������� ���������
            curRow.Lat = (prevRow.Lat + nextRow.Lat)/2;
            curRow.Lon = (prevRow.Lon + nextRow.Lon)/2;
            curRow.dist = �GeoDistanceForCache.CalculateFromGrad(
                prevRow.Lat, prevRow.Lon, curRow.Lat, curRow.Lon);
            IncTotalDistance(curRow.dist);
        }

        /// <summary>
        /// ��������� �������� �� ����� (��� "����� ����������") �� ������� ����� UnixTime ���������� �� ����� �����
        /// </summary>
        /// <param name="prevRow"></param>
        /// <param name="curRow"></param>
        /// <param name="nextRow"></param>
        /// <param name="OrderByUnixTime"> ������� ������������� ������ ��� ���������� ORDER BY UnixTime</param>
        /// <returns></returns>
        private static bool IsValidLogId(atlantaDataSet.dataviewRow prevRow, atlantaDataSet.dataviewRow curRow,
                                         atlantaDataSet.dataviewRow nextRow, bool OrderByUnixTime)
        {
            if (OrderByUnixTime)
            {
                if ((curRow.LogID > prevRow.LogID) & (curRow.LogID > nextRow.LogID) &
                    ((nextRow.LogID - prevRow.LogID) == 1))
                {
                    return false;
                }
                else
                    return true;
            }
            else
                return true;
        }

        /// <summary>
        /// �������� ���������� ���������� �������� �����. 
        /// </summary>
        /// <param name="rowsList">��������� dataviewRow.</param>
        private static void CheckLastSegment(List<atlantaDataSet.dataviewRow> rowsList)
        {
            atlantaDataSet.dataviewRow prevRow = rowsList[rowsList.Count - 2];
            atlantaDataSet.dataviewRow lastRow = rowsList[rowsList.Count - 1];

            lastRow.dist = �GeoDistanceForCache.CalculateFromGrad(
                prevRow.Lat, prevRow.Lon, lastRow.Lat, lastRow.Lon);

            if (IsCurrentRowNotValid(prevRow, lastRow))
            {
                if ((lastRow.speed == 0.0) && (lastRow.LogID - prevRow.LogID == 1))
                {
                    // � ����� �� ����������, � ���������� ����� ��� ����� prevRow - lastRow,
                    // ������������� ���������� ��� � ���������� ����� 
                    lastRow.Lat = prevRow.Lat;
                    lastRow.Lon = prevRow.Lon;
                    lastRow.dist = 0;
                }
                else
                {
                    // ��������� ����� ������, �.�. �� ����� ��� ��������� ����������.
                    rowsList.RemoveAt(rowsList.Count - 1);
                }
            }
            else
            {
                IncTotalDistance(lastRow.dist);
            }
        }

        /// <summary>
        /// ��������� ����������� ���� �� �������� ��������.
        /// </summary>
        /// <param name="distance">���������� ����, ��.</param>
        private static void IncTotalDistance(double distance)
        {
            dist += distance;
        }

        /// <summary>
        /// �������� ����, ��� ������� ����� ����������, ��������� ��������
        /// ���������� ��������� �������� ������ 200 ��/�. 
        /// </summary>
        /// <param name="prev">���������� ������ dataviewRow.</param>
        /// <param name="current">������� ������ dataviewRow.</param>
        /// <returns>true - ���������� ��������.</returns>
        private static bool IsCurrentRowNotValid(atlantaDataSet.dataviewRow prev,
                                                 atlantaDataSet.dataviewRow current)
        {
            if (current.dist == 0.0)
            {
                return false;
            }
            TimeSpan dT = current.time - prev.time;
            return dT.TotalSeconds == 0
                       ? current.dist >= .1
                       : current.dist*1000/dT.TotalSeconds > 56; // ~200 ��/�
        }

        /// <summary>
        /// ������ ��������� � ������ �����.
        /// </summary>
        /// <param name="rowsList">��������� GpsData. ��� 64-��� ������.</param>
        private static void CalcAcceleration(List<GpsData> rowsList)
        {
            if (rowsList.Count == 0)
            {
                return;
            }

            for (int i = 0; i < rowsList.Count; i++)
            {
                double acc = rowsList[i].Accel > 127 ? rowsList[i].Accel - 255 : rowsList[i].Accel;
                //rowsList[i].Accel = acc / 3.60; // km/h/c^2 --> m/c^2
                rowsList[i].Accel = acc;
            } // for
        }

        /// <summary>
        /// ������ ��������� � ������ �����.
        /// </summary>
        /// <param name="rowsList">��������� dataviewRow.</param>
        private static void CalcAcceleration(List<atlantaDataSet.dataviewRow> rowsList)
        {
            if (rowsList.Count == 0)
            {
                return;
            }

            atlantaDataSet.dataviewRow curRow;
            rowsList[0].accel = 0;

            for (int i = 1; i < rowsList.Count; i++)
            {
                curRow = rowsList[i];
                if ((curRow.Events & 0x800000) != 0)
                {
                    double acc = 0;
                    acc = curRow.Altitude > 127 ? curRow.Altitude - 255 : curRow.Altitude;
                    //curRow.accel = acc*10/36;
                    curRow.accel = acc;
                }
                else
                {
                    atlantaDataSet.dataviewRow prevRow = rowsList[i - 1];
                    TimeSpan dT = curRow.time - prevRow.time;
                    double dV = curRow.speed - prevRow.speed;
                    //dV = dV*1000/3600;
                    curRow.accel = dT.TotalSeconds > 0 ? dV/dT.TotalSeconds : 0;
                }
            }
        }

        /// <summary>
        /// ���������� ������� atlantaDataSet.dataviewRow �������� ��
        /// ���������� ���������.
        /// </summary>
        /// <param name="dataset">atlantaDataSet.</param>
        /// <param name="rowsList">��������� dataviewRow.</param>
        private static void FillDataView(atlantaDataSet dataset, List<atlantaDataSet.dataviewRow> rowsList)
        {
            foreach (atlantaDataSet.dataviewRow row in rowsList)
            {
                dataset.dataview.AdddataviewRow(row);
            }
        }
    }
}
