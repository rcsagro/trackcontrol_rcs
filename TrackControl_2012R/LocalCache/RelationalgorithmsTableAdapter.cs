using System;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using TrackControl.General.DAL;
using TrackControl.General.DatabaseDriver;

namespace LocalCache.atlantaDataSetTableAdapters
{
  [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Design.TypedDataSetGenerator", "2.0.0.0")]
  [global::System.ComponentModel.DesignerCategoryAttribute("code")]
  [global::System.ComponentModel.ToolboxItem(true)]
  [global::System.ComponentModel.DataObjectAttribute(true)]
  [global::System.ComponentModel.DesignerAttribute("Microsoft.VSDesigner.DataSource.Design.TableAdapterDesigner, Microsoft.VSDesigner" +
      ", Version=8.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")]
  [global::System.ComponentModel.Design.HelpKeywordAttribute("vs.data.TableAdapter")]
  public class RelationalgorithmsTableAdapter : global::System.ComponentModel.Component
  {

    //private global::MySql.Data.MySqlClient.MySqlDataAdapter _adapter;
    //private global::MySql.Data.MySqlClient.MySqlConnection _connection;
    //private global::MySql.Data.MySqlClient.MySqlCommand[] _commandCollection;

    private DriverDb db = new DriverDb();

    private bool _clearBeforeFill;

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    public RelationalgorithmsTableAdapter()
    {
      this.ClearBeforeFill = true;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    private object Adapter
    {
      get
      {
        if ((this.db.Adapter == null))
        {
          this.InitAdapter();
        }
        return this.db.Adapter;
      }
    }

     [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
      public object Connection
      {
          get
          {
              if ((this.db.Connection == null))
              {
                  this.InitConnection();
              }

              return this.db.Connection;
          }
          set
          {
              if( ( this.db.Connection == null ) )
              {
                  this.InitConnection();
              }

              this.db.Connection = value;

              if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
              {
                  MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;

                  if ((adapter.InsertCommand != null))
                  {
                      adapter.InsertCommand.Connection = (MySqlConnection) value;
                  }

                  if ((adapter.DeleteCommand != null))
                  {
                      adapter.DeleteCommand.Connection = (MySqlConnection) value;
                  }

                  if ((adapter.UpdateCommand != null))
                  {
                      adapter.UpdateCommand.Connection = (MySqlConnection) value;
                  }
              } // if
              else if( DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse )
              {
                  SqlDataAdapter adapter = (SqlDataAdapter)this.Adapter;

                  if( ( adapter.InsertCommand != null ) )
                  {
                      adapter.InsertCommand.Connection = (SqlConnection)value;
                  }

                  if( ( adapter.DeleteCommand != null ) )
                  {
                      adapter.DeleteCommand.Connection = (SqlConnection)value;
                  }

                  if( ( adapter.UpdateCommand != null ) )
                  {
                      adapter.UpdateCommand.Connection = (SqlConnection)value;
                  }
              } // else if

              if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
              {
                  MySqlCommand[] collection = (MySqlCommand[]) this.CommandCollection;

                  for (int i = 0; (i < this.CommandCollection.Length); i = (i + 1))
                  {
                      if ((this.CommandCollection[i] != null))
                      {
                          collection[i].Connection = (MySqlConnection) value;
                      }
                  } //for
              } // if
              else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
              {
                  SqlCommand[] collection = (SqlCommand[]) this.CommandCollection;

                  for (int i = 0; (i < this.CommandCollection.Length); i = (i + 1))
                  {
                      if ((this.CommandCollection[i] != null))
                      {
                          collection[i].Connection = (SqlConnection) value;
                      }
                  } //for
              } // else if
          } // set
      } // Connection

      [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
      //protected global::MySql.Data.MySqlClient.MySqlCommand[] CommandCollection
      protected object[] CommandCollection
      {
          get
          {
              if ((this.db.CommandCollection == null))
              {
                  this.InitCommandCollection();
              }

              return this.db.CommandCollection;
          }
      } // CommandCollection

      [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    public bool ClearBeforeFill
    {
      get
      {
        return this._clearBeforeFill;
      }
      set
      {
        this._clearBeforeFill = value;
      }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    private void InitAdapter()
    {
      //this._adapter = new global::MySql.Data.MySqlClient.MySqlDataAdapter();
        this.db.NewDataAdapter();
      global::System.Data.Common.DataTableMapping tableMapping = new global::System.Data.Common.DataTableMapping();
      tableMapping.SourceTable = "Table";
      tableMapping.DataSetTable = "relationalgorithms";
      tableMapping.ColumnMappings.Add("ID", "ID");
      tableMapping.ColumnMappings.Add("AlgorithmID", "AlgorithmID");
      tableMapping.ColumnMappings.Add("SensorID", "SensorID");
      //this._adapter.TableMappings.Add(tableMapping);
        this.db.AdapterTableMappingsAdd(tableMapping);

      //DELETE
      //this._adapter.DeleteCommand = new global::MySql.Data.MySqlClient.MySqlCommand();
        this.db.AdapterDeleteCommand();
      //this._adapter.DeleteCommand.Connection = this.Connection;
        this.db.AdapterDeleteCommandConnection(this.Connection);
      //this._adapter.DeleteCommand.CommandText = "DELETE FROM relationalgorithms WHERE ID = ?ID";
        this.db.AdapterDeleteCommandText( TrackControlQuery.RelationalgorithmsTableAdapter.DeleteRelationalAlgorithms );
      //this._adapter.DeleteCommand.CommandType = System.Data.CommandType.Text;
        this.db.AdapterDeleteCommandType( System.Data.CommandType.Text );
      //MySql.Data.MySqlClient.MySqlParameter param = new MySql.Data.MySqlClient.MySqlParameter();
        this.db.NewSqlParameter();
      //param.ParameterName = "?ID";
        this.db.ParameterName( db.ParamPrefics + "ID" );
      //param.DbType = System.Data.DbType.Int32;
        this.db.ParameterDbType( System.Data.DbType.Int32 );
      //param.MySqlDbType = MySql.Data.MySqlClient.MySqlDbType.Int32;
        this.db.ParameterSqlDbType( db.GettingInt32() );
      //param.IsNullable = true;
        this.db.ParameterIsNullable(true);
      //param.SourceColumn = "ID";
        this.db.ParameterSourceColumn( "ID" );
      //param.SourceVersion = System.Data.DataRowVersion.Original;
        this.db.ParameterSourceVersion( System.Data.DataRowVersion.Original );
      //this._adapter.DeleteCommand.Parameters.Add(param);
        this.db.AdapterDeleteCommandParametersAdd();

      //UPDATE
      //this._adapter.UpdateCommand = new global::MySql.Data.MySqlClient.MySqlCommand();
        this.db.AdapterUpdateCommand();
      //this._adapter.UpdateCommand.Connection = this.Connection;
        this.db.AdapterUpdateCommandConnection(this.Connection);
      //this._adapter.UpdateCommand.CommandText =
      //  "UPDATE relationalgorithms\r\n " +
      //  "SET ID = ?ID, AlgorithmID = ?AlgorithmID, SensorID = ?SensorID \r\n" +
      //  "WHERE (ID = ?ID)";

        string sqlstr = TrackControlQuery.RelationalgorithmsTableAdapter.UpdateRelationalgorithms;
        
        this.db.AdapterUpdateCommandText( sqlstr );
      //this._adapter.UpdateCommand.CommandType = global::System.Data.CommandType.Text;
        this.db.AdapterUpdateCommandType( global::System.Data.CommandType.Text );

       //param = new MySql.Data.MySqlClient.MySqlParameter();
       this.db.NewSqlParameter();
       //param.ParameterName = "?ID";
       this.db.ParameterName(db.ParamPrefics + "ID");
       //param.DbType = global::System.Data.DbType.Int32;
       this.db.ParameterDbType(global::System.Data.DbType.Int32);
       //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
       this.db.ParameterSqlDbType(db.GettingInt32());
       //param.IsNullable = false;
       this.db.ParameterIsNullable(false);
       //param.SourceColumn = "ID";
       this.db.ParameterSourceColumn("ID");
       //this._adapter.UpdateCommand.Parameters.Add(param);
       this.db.AdapterUpdateCommandParametersAdd();
       
        //param = new global::MySql.Data.MySqlClient.MySqlParameter();
        this.db.NewSqlParameter();
      //param.ParameterName = "?AlgorithmID";
        this.db.ParameterName( db.ParamPrefics + "AlgorithmID" );
      //param.DbType = global::System.Data.DbType.Int32;
        this.db.ParameterDbType( global::System.Data.DbType.Int32 );
      //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
        this.db.ParameterSqlDbType( db.GettingInt32() );
      //param.IsNullable = false;
        this.db.ParameterIsNullable(false);
      //param.SourceColumn = "AlgorithmID";
        this.db.ParameterSourceColumn( "AlgorithmID" );
      //this._adapter.UpdateCommand.Parameters.Add(param);
      this.db.AdapterUpdateCommandParametersAdd();

      //param = new global::MySql.Data.MySqlClient.MySqlParameter();
        this.db.NewSqlParameter();
      //param.ParameterName = "?SensorID";
        this.db.ParameterName( db.ParamPrefics + "SensorID" );
      //param.DbType = global::System.Data.DbType.Int32;
        this.db.ParameterDbType( global::System.Data.DbType.Int32 );
      //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
        this.db.ParameterSqlDbType( db.GettingInt32() );
      //param.IsNullable = false;
        this.db.ParameterIsNullable(false);
      //param.SourceColumn = "SensorID";
        this.db.ParameterSourceColumn( "SensorID" );
      //this._adapter.UpdateCommand.Parameters.Add(param);
      this.db.AdapterUpdateCommandParametersAdd();

      //INSERT
      //this._adapter.InsertCommand = new MySql.Data.MySqlClient.MySqlCommand();
        this.db.AdapterInsertCommand();
      //this._adapter.InsertCommand.Connection = this.Connection;
        this.db.AdapterInsertCommandConnection(this.Connection);
      //this._adapter.InsertCommand.UpdatedRowSource = System.Data.UpdateRowSource.FirstReturnedRecord;
        this.db.AdapterInsertCommandUpdatedRowSource( System.Data.UpdateRowSource.FirstReturnedRecord );
      //this._adapter.InsertCommand.CommandText = 
      //  "INSERT INTO relationalgorithms ( ID, AlgorithmID, SensorID ) VALUES ( ?ID, ?AlgorithmID, ?SensorID);" +
      //  "Select ?ID = LAST_INSERT_ID()";
        string sqlstr1 = TrackControlQuery.RelationalgorithmsTableAdapter.InsertIntoRelationalgorithms;
       
        this.db.AdapterInsertCommandText( sqlstr1 );
      //this._adapter.InsertCommand.CommandType = System.Data.CommandType.Text;
        this.db.AdapterInsertCommandType( System.Data.CommandType.Text );
        
        //param = new MySql.Data.MySqlClient.MySqlParameter();
        this.db.NewSqlParameter();
        //param.ParameterName = "?ID";
        this.db.ParameterName(db.ParamPrefics + "ID");
        //param.DbType = global::System.Data.DbType.Int32;
        this.db.ParameterDbType(global::System.Data.DbType.Int32);
        //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
        this.db.ParameterSqlDbType(db.GettingInt32());
        //param.IsNullable = false;
        this.db.ParameterIsNullable(false);
        //param.SourceColumn = "ID";
        this.db.ParameterSourceColumn("ID");
        //this._adapter.InsertCommand.Parameters.Add(param);
        this.db.AdapterInsertCommandParametersAdd();
        
        //param = new global::MySql.Data.MySqlClient.MySqlParameter();
        this.db.NewSqlParameter();
      //param.ParameterName = "?AlgorithmID";
        this.db.ParameterName( db.ParamPrefics + "AlgorithmID" );
      //param.DbType = global::System.Data.DbType.Int32;
        this.db.ParameterDbType( global::System.Data.DbType.Int32 );
      //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
        this.db.ParameterSqlDbType( db.GettingInt32() );
      //param.IsNullable = false;
        this.db.ParameterIsNullable(false);
      //param.SourceColumn = "AlgorithmID";
        this.db.ParameterSourceColumn( "AlgorithmID" );
      //this._adapter.InsertCommand.Parameters.Add(param);
      this.db.AdapterInsertCommandParametersAdd();

      //param = new global::MySql.Data.MySqlClient.MySqlParameter();
        this.db.NewSqlParameter();
      //param.ParameterName = "?SensorID";
        this.db.ParameterName( db.ParamPrefics + "SensorID" );
      //param.DbType = global::System.Data.DbType.Int32;
        this.db.ParameterDbType( global::System.Data.DbType.Int32 );
      //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
        this.db.ParameterSqlDbType( db.GettingInt32() );
      //param.IsNullable = false;
        this.db.ParameterIsNullable(false);
      //param.SourceColumn = "SensorID";
        this.db.ParameterSourceColumn( "SensorID" );
      //this._adapter.InsertCommand.Parameters.Add(param);
      this.db.AdapterInsertCommandParametersAdd();
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    private void InitConnection()
    {
      //this._connection = new global::MySql.Data.MySqlClient.MySqlConnection();
        db.AdapNewConnection();
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    private void InitCommandCollection()
    {
      //this._commandCollection = new global::MySql.Data.MySqlClient.MySqlCommand[1];
        this.db.NewCommandCollection(1);
      //this._commandCollection[0] = new global::MySql.Data.MySqlClient.MySqlCommand();
        this.db.SetNewCommandCollection(0);
      //this._commandCollection[0].Connection = this.Connection;
        this.db.SetCommandCollectionConnection(this.Connection, 0);
      //this._commandCollection[0].CommandText = "SELECT `ID`, `AlgorithmID`, `SensorID` FROM `relationalgorithms`";
      this.db.SetCommandCollectionText(TrackControlQuery.RelationalgorithmsTableAdapter.SelectRelationalAlgorithms , 0 );
        
        //this._commandCollection[0].CommandType = global::System.Data.CommandType.Text;
        this.db.SetCommandCollectionType( global::System.Data.CommandType.Text, 0);
    }

      [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
      [global::System.ComponentModel.Design.HelpKeywordAttribute("vs.data.TableAdapter")]
      [global::System.ComponentModel.DataObjectMethodAttribute(global::System.ComponentModel.DataObjectMethodType.Fill, true)]
      public virtual int Fill(atlantaDataSet.relationalgorithmsDataTable dataTable)
      {
          if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
          {
              MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;
              adapter.SelectCommand = (MySqlCommand) this.CommandCollection[0];
          }
          else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
          {
              SqlDataAdapter adapter = (SqlDataAdapter)this.Adapter;
              adapter.SelectCommand = (SqlCommand)this.CommandCollection[0];
          }

          if ((this.ClearBeforeFill == true))
          {
              dataTable.Clear();
          }

          int returnValue = 0;
          if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
          {
              MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;
              returnValue = adapter.Fill(dataTable);
          }
          else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
          {
              SqlDataAdapter adapter = (SqlDataAdapter)this.Adapter;
              returnValue = adapter.Fill( dataTable );
          }

          return returnValue;
      }

      [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.ComponentModel.Design.HelpKeywordAttribute("vs.data.TableAdapter")]
    public virtual int Update(global::System.Data.DataRow[] dataRows)
    {
        if( DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse )
        {
            MySqlDataAdapter adapter = (MySqlDataAdapter)this.Adapter;
            return adapter.Update( dataRows );
        }
        else if( DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse )
        {
            SqlDataAdapter adapter = (SqlDataAdapter)this.Adapter;
            return adapter.Update( dataRows );
        }

          return 0;
    } // Update

      //[global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
      [global::System.ComponentModel.Design.HelpKeywordAttribute("vs.data.TableAdapter")]
      public virtual int Update(atlantaDataSet.relationalgorithmsDataTable dataTable)
      {
          if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
          {
              MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;
              return adapter.Update( dataTable );
          }
          else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
          {
              SqlDataAdapter adapter = (SqlDataAdapter) this.Adapter;
              return adapter.Update( dataTable );
          }

          return 0;
      } // Update
  }
}
