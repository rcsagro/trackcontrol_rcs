using System;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using TrackControl.General.DAL;
using TrackControl.General.DatabaseDriver;

namespace LocalCache.atlantaDataSetTableAdapters
{
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Design.TypedDataSetGenerator", "2.0.0.0")]
    [global::System.ComponentModel.DesignerCategoryAttribute("code")]
    [global::System.ComponentModel.ToolboxItem(true)]
    [global::System.ComponentModel.DataObjectAttribute(true)]
    [global::System.ComponentModel.DesignerAttribute("Microsoft.VSDesigner.DataSource.Design.TableAdapterDesigner, Microsoft.VSDesigner" +
            ", Version=8.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")]
    [global::System.ComponentModel.Design.HelpKeywordAttribute("vs.data.TableAdapter")]
    public class SensorsTableAdapter : global::System.ComponentModel.Component 
  {

    //private global::MySql.Data.MySqlClient.MySqlDataAdapter _adapter;
    //private global::MySql.Data.MySqlClient.MySqlConnection _connection;
    //private global::MySql.Data.MySqlClient.MySqlCommand[] _commandCollection;
    DriverDb db = new DriverDb();

    private bool _clearBeforeFill;

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    public SensorsTableAdapter()
    {
      this.ClearBeforeFill = true;
    }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        //private global::MySql.Data.MySqlClient.MySqlDataAdapter Adapter
        private object Adapter
        {
            get
            {
                if ((this.db.Adapter == null))
                {
                    this.InitAdapter();
                }

                return this.db.Adapter;
            }
        } // Adapter

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    public object Connection
    {
            get
            {
                if ((this.db.Connection == null))
                {
                    this.InitConnection();
                }

                return this.db.Connection;
            }

        set
        {
            this.db.Connection = value;

            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;

                //if ((this.Adapter.InsertCommand != null))
                if ((adapter.InsertCommand != null))
                {
                    //this.Adapter.InsertCommand.Connection = value;
                    adapter.InsertCommand.Connection = (MySqlConnection) value;
                }

                //if ((this.Adapter.DeleteCommand != null))
                if ((adapter.DeleteCommand != null))
                {
                    //this.Adapter.DeleteCommand.Connection = value;
                    adapter.DeleteCommand.Connection = (MySqlConnection) value;
                }

                //if ((this.Adapter.UpdateCommand != null))
                if ((adapter.UpdateCommand != null))
                {
                    //this.Adapter.UpdateCommand.Connection = value;
                    adapter.UpdateCommand.Connection = (MySqlConnection) value;
                }

                MySqlCommand[] collection = (MySqlCommand[]) this.CommandCollection;

                for (int i = 0; (i < this.CommandCollection.Length); i = (i + 1))
                {
                    if ((this.CommandCollection[i] != null))
                    {
                        collection[i].Connection = (MySqlConnection) value;
                    }
                } // for
            } // if
            else if( DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse )
            {
                SqlDataAdapter adapter = (SqlDataAdapter)this.Adapter;

                //if ((this.Adapter.InsertCommand != null))
                if( ( adapter.InsertCommand != null ) )
                {
                    //this.Adapter.InsertCommand.Connection = value;
                    adapter.InsertCommand.Connection = (SqlConnection)value;
                }

                //if ((this.Adapter.DeleteCommand != null))
                if( ( adapter.DeleteCommand != null ) )
                {
                    //this.Adapter.DeleteCommand.Connection = value;
                    adapter.DeleteCommand.Connection = (SqlConnection)value;
                }

                //if ((this.Adapter.UpdateCommand != null))
                if( ( adapter.UpdateCommand != null ) )
                {
                    //this.Adapter.UpdateCommand.Connection = value;
                    adapter.UpdateCommand.Connection = (SqlConnection)value;
                }

                SqlCommand[] collection = (SqlCommand[])this.CommandCollection;

                for( int i = 0; ( i < this.CommandCollection.Length ); i = ( i + 1 ) )
                {
                    if( ( this.CommandCollection[i] != null ) )
                    {
                        collection[i].Connection = (SqlConnection)value;
                    }
                } // for
            } // if
        } // set
    }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        //protected global::MySql.Data.MySqlClient.MySqlCommand[] CommandCollection
        protected object[] CommandCollection
        {
            get
            {
                if ((this.db.CommandCollection == null))
                {
                    this.InitCommandCollection();
                }

                return this.db.CommandCollection;
            }
        } // CommandCollection

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    public bool ClearBeforeFill
    {
      get
      {
        return this._clearBeforeFill;
      }
      set
      {
        this._clearBeforeFill = value;
      }
    }

    //[global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    private void InitAdapter()
    {
      //this._adapter = new global::MySql.Data.MySqlClient.MySqlDataAdapter();
        this.db.NewDataAdapter();
      global::System.Data.Common.DataTableMapping tableMapping = new global::System.Data.Common.DataTableMapping();
      tableMapping.SourceTable = "Table";
      tableMapping.DataSetTable = "sensors";
      tableMapping.ColumnMappings.Add("Name", "Name");
      tableMapping.ColumnMappings.Add("Description", "Description");
      tableMapping.ColumnMappings.Add("StartBit", "StartBit");
      tableMapping.ColumnMappings.Add("Length", "Length");
      tableMapping.ColumnMappings.Add("NameUnit", "NameUnit");
      tableMapping.ColumnMappings.Add("K", "K");
      tableMapping.ColumnMappings.Add("id", "id");
      tableMapping.ColumnMappings.Add("mobitel_id", "mobitel_id");
      tableMapping.ColumnMappings.Add("B", "B");
      tableMapping.ColumnMappings.Add("S", "S");
      //this._adapter.TableMappings.Add(tableMapping);
        this.db.AdapterTableMappingsAdd(tableMapping);

      // DELETE
      //this._adapter.DeleteCommand = new global::MySql.Data.MySqlClient.MySqlCommand();
        this.db.AdapterDeleteCommand();
      //this._adapter.DeleteCommand.Connection = this.Connection;
        this.db.AdapterDeleteCommandConnection(this.Connection);
      //this._adapter.DeleteCommand.CommandText = "DELETE FROM sensors WHERE id = ?id";
        this.db.AdapterDeleteCommandText( "DELETE FROM sensors WHERE id = " + db.ParamPrefics + "id" );
      //this._adapter.DeleteCommand.CommandType = System.Data.CommandType.Text;
        this.db.AdapterDeleteCommandType( System.Data.CommandType.Text );
      //MySql.Data.MySqlClient.MySqlParameter param = new MySql.Data.MySqlClient.MySqlParameter();
        this.db.NewSqlParameter();
      //param.ParameterName = "?id";
        this.db.ParameterName( db.ParamPrefics + "id" );
      //param.DbType = System.Data.DbType.Int32;
        this.db.ParameterDbType( System.Data.DbType.Int32 );
      //param.MySqlDbType = MySql.Data.MySqlClient.MySqlDbType.Int32;
        this.db.ParameterSqlDbType( db.GettingInt32() );
      //param.IsNullable = true;
        this.db.ParameterIsNullable(true);
      //param.SourceColumn = "id";
        this.db.ParameterSourceColumn( "id" );
      //param.SourceVersion = System.Data.DataRowVersion.Original;
        this.db.ParameterSourceVersion( System.Data.DataRowVersion.Original );
      //this._adapter.DeleteCommand.Parameters.Add(param);
        this.db.AdapterDeleteCommandParametersAdd();

        // INSERT
      //this._adapter.InsertCommand = new MySql.Data.MySqlClient.MySqlCommand();
        this.db.AdapterInsertCommand();
      //this._adapter.InsertCommand.Connection = this.Connection;
        this.db.AdapterInsertCommandConnection(this.Connection);
      //this._adapter.InsertCommand.UpdatedRowSource = System.Data.UpdateRowSource.FirstReturnedRecord;
        this.db.AdapterInsertCommandUpdatedRowSource( System.Data.UpdateRowSource.FirstReturnedRecord );
      //this._adapter.InsertCommand.CommandText = "INSERT INTO sensors ( Mobitel_id, Name, Description, StartBit, Length, NameUnit, " +
      //    "K ) VALUES ( ?Mobitel_id, ?Name, ?Description, ?StartBit, ?Length, ?NameUnit, ?K " +
      //    "); SELECT * FROM sensors where Id = LAST_INSERT_ID()";

        string sqlstr = TrackControlQuery.SensorsTableAdapter.InsertIntoSensors;
      
        this.db.AdapterInsertCommandText( sqlstr );
      //this._adapter.InsertCommand.CommandType = System.Data.CommandType.Text;
        this.db.AdapterInsertCommandType( System.Data.CommandType.Text );
      //this._adapter.InsertCommand.UpdatedRowSource = System.Data.UpdateRowSource.FirstReturnedRecord;
        this.db.AdapterInsertCommandUpdatedRowSource( System.Data.UpdateRowSource.FirstReturnedRecord );

      //param = new MySql.Data.MySqlClient.MySqlParameter();
        this.db.NewSqlParameter();
      //param.ParameterName = "?Mobitel_ID";
        this.db.ParameterName( db.ParamPrefics + "mobitel_id" );
      //param.DbType = System.Data.DbType.Int32;
        this.db.ParameterDbType( System.Data.DbType.Int32 );
      //param.MySqlDbType = MySql.Data.MySqlClient.MySqlDbType.Int32;
        this.db.ParameterSqlDbType( db.GettingInt32() );
      //param.IsNullable = false;
        this.db.ParameterIsNullable(false);
      //param.SourceColumn = "mobitel_id";
        this.db.ParameterSourceColumn( "mobitel_id" );
      //this._adapter.InsertCommand.Parameters.Add(param);
        this.db.AdapterInsertCommandParametersAdd();

      //param = new MySql.Data.MySqlClient.MySqlParameter();
        this.db.NewSqlParameter();
      //param.ParameterName = "?Name";
        this.db.ParameterName( db.ParamPrefics + "Name" );
      //param.DbType = System.Data.DbType.String;
        this.db.ParameterDbType( System.Data.DbType.String );
      //param.MySqlDbType = MySql.Data.MySqlClient.MySqlDbType.VarChar;
        this.db.ParameterSqlDbType( db.GettingVarChar() );
      //param.IsNullable = false;
        this.db.ParameterSize(45);
        this.db.ParameterIsNullable(false);
      //param.SourceColumn = "Name";
        this.db.ParameterSourceColumn("Name");
      //this._adapter.InsertCommand.Parameters.Add(param);
        this.db.AdapterInsertCommandParametersAdd();

      //param = new MySql.Data.MySqlClient.MySqlParameter();
        this.db.NewSqlParameter();
      //param.ParameterName = "?Description";
        this.db.ParameterName( db.ParamPrefics + "Description" );
      //param.DbType = System.Data.DbType.String;
        this.db.ParameterDbType( System.Data.DbType.String );
      //param.MySqlDbType = MySql.Data.MySqlClient.MySqlDbType.VarChar;
        this.db.ParameterSqlDbType( db.GettingVarChar() );
      //param.IsNullable = false;
        db.ParameterSize(200);
        this.db.ParameterIsNullable(false);
      //param.SourceColumn = "Description";
        this.db.ParameterSourceColumn( "Description" );
      //this._adapter.InsertCommand.Parameters.Add(param);
        this.db.AdapterInsertCommandParametersAdd();

      //param = new MySql.Data.MySqlClient.MySqlParameter();
        this.db.NewSqlParameter();
      //param.ParameterName = "?StartBit";
        this.db.ParameterName( db.ParamPrefics + "StartBit" );
      //param.DbType = System.Data.DbType.Int32;
        this.db.ParameterDbType( System.Data.DbType.Int64 );
      //param.MySqlDbType = MySql.Data.MySqlClient.MySqlDbType.Int32;
        this.db.ParameterSqlDbType(db.GettingInt64() );
      //param.IsNullable = false;
        this.db.ParameterIsNullable(false);
      //param.SourceColumn = "StartBit";
        this.db.ParameterSourceColumn( "StartBit" );
      //this._adapter.InsertCommand.Parameters.Add(param);
        this.db.AdapterInsertCommandParametersAdd();

      //param = new MySql.Data.MySqlClient.MySqlParameter();
        this.db.NewSqlParameter();
      //param.ParameterName = "?Length";
        this.db.ParameterName( db.ParamPrefics + "Length" );
      //param.DbType = System.Data.DbType.Int32;
        this.db.ParameterDbType( System.Data.DbType.Int64 );
      //param.MySqlDbType = MySql.Data.MySqlClient.MySqlDbType.Int32;
        this.db.ParameterSqlDbType( db.GettingInt64() );
      //param.IsNullable = false;
        this.db.ParameterIsNullable(false);
      //param.SourceColumn = "Length";
        this.db.ParameterSourceColumn( "Length" );
      //this._adapter.InsertCommand.Parameters.Add(param);
        this.db.AdapterInsertCommandParametersAdd();

      //param = new MySql.Data.MySqlClient.MySqlParameter();
        this.db.NewSqlParameter();
      //param.ParameterName = "?NameUnit";
        this.db.ParameterName( db.ParamPrefics + "NameUnit" );
      //param.DbType = System.Data.DbType.String;
        this.db.ParameterDbType( System.Data.DbType.String );
      //param.MySqlDbType = MySql.Data.MySqlClient.MySqlDbType.VarChar;
        this.db.ParameterSqlDbType( db.GettingVarChar() );
      //param.IsNullable = false;
        db.ParameterSize(45);
        this.db.ParameterIsNullable(false);
      //param.SourceColumn = "NameUnit";
        this.db.ParameterSourceColumn( "NameUnit" );
      //this._adapter.InsertCommand.Parameters.Add(param);
        this.db.AdapterInsertCommandParametersAdd();

      //param = new MySql.Data.MySqlClient.MySqlParameter();
        this.db.NewSqlParameter();
      //param.ParameterName = "?K";
        this.db.ParameterName( db.ParamPrefics + "K" );
      //param.DbType = System.Data.DbType.Double;
        this.db.ParameterDbType( System.Data.DbType.Double );
      //param.MySqlDbType = MySql.Data.MySqlClient.MySqlDbType.Double;
        this.db.ParameterSqlDbType( db.GettingDouble() );
      //param.IsNullable = false;
        this.db.ParameterIsNullable(false);
      //param.SourceColumn = "K";
        this.db.ParameterSourceColumn( "K" );
      //this._adapter.InsertCommand.Parameters.Add(param);
        this.db.AdapterInsertCommandParametersAdd();

        //param = new MySql.Data.MySqlClient.MySqlParameter();
        this.db.NewSqlParameter();
        //param.ParameterName = "?K";
        this.db.ParameterName(db.ParamPrefics + "B");
        //param.DbType = System.Data.DbType.Double;
        this.db.ParameterDbType(System.Data.DbType.Double);
        //param.MySqlDbType = MySql.Data.MySqlClient.MySqlDbType.Double;
        this.db.ParameterSqlDbType(db.GettingDouble());
        //param.IsNullable = false;
        this.db.ParameterIsNullable(false);
        //param.SourceColumn = "K";
        this.db.ParameterSourceColumn("B");
        //this._adapter.InsertCommand.Parameters.Add(param);
        this.db.AdapterInsertCommandParametersAdd();

        //param = new MySql.Data.MySqlClient.MySqlParameter();
        this.db.NewSqlParameter();
        //param.ParameterName = "?K";
        this.db.ParameterName(db.ParamPrefics + "S");
        //param.DbType = System.Data.DbType.Double;
        this.db.ParameterDbType(System.Data.DbType.Byte);
        //param.MySqlDbType = MySql.Data.MySqlClient.MySqlDbType.Double;
        this.db.ParameterSqlDbType(db.GettingByte());
        //param.IsNullable = false;
        this.db.ParameterIsNullable(false);
        //param.SourceColumn = "K";
        this.db.ParameterSourceColumn("S");
        //this._adapter.InsertCommand.Parameters.Add(param);
        this.db.AdapterInsertCommandParametersAdd();

      // UPDATE
      //this._adapter.UpdateCommand = new global::MySql.Data.MySqlClient.MySqlCommand();
        this.db.AdapterUpdateCommand();
      //this._adapter.UpdateCommand.Connection = this.Connection;
        this.db.AdapterUpdateCommandConnection(this.Connection);
      //this._adapter.UpdateCommand.CommandText =
      //  "UPDATE sensors\r\n " +
      //  "SET Name = ?Name, Description = ?Description, StartBit = ?StartBit," +
      //  "  Length = ?Length, NameUnit = ?NameUnit, K = ?K WHERE (id = ?id)";

        string sqlstr1 = TrackControlQuery.SensorsTableAdapter.UpdateSensors;
       
        this.db.AdapterUpdateCommandText( sqlstr1 );
      //this._adapter.UpdateCommand.CommandType = global::System.Data.CommandType.Text;
        this.db.AdapterUpdateCommandType( global::System.Data.CommandType.Text );
      
      //param = new global::MySql.Data.MySqlClient.MySqlParameter();
      //param = new global::MySql.Data.MySqlClient.MySqlParameter();
        this.db.NewSqlParameter();
      //param.ParameterName = "?Name";
        this.db.ParameterName( db.ParamPrefics + "Name" );
      //param.DbType = global::System.Data.DbType.String;
        this.db.ParameterDbType( global::System.Data.DbType.String );
      //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.String;
        this.db.ParameterSqlDbType( db.GettingString() );
      //param.Size = 45;
        this.db.ParameterSize(45);
      //param.IsNullable = false;
        this.db.ParameterIsNullable(false);
      //param.SourceColumn = "Name";
        this.db.ParameterSourceColumn( "Name" );
      //this._adapter.UpdateCommand.Parameters.Add(param);
        this.db.AdapterUpdateCommandParametersAdd();

     //param = new global::MySql.Data.MySqlClient.MySqlParameter();
        this.db.NewSqlParameter();
      //param.ParameterName = "?Description";
        this.db.ParameterName( db.ParamPrefics + "Description" );
      //param.DbType = global::System.Data.DbType.String;
        this.db.ParameterDbType( global::System.Data.DbType.String );
      //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.String;
        this.db.ParameterSqlDbType( db.GettingString() );
      //param.Size = 200;
        this.db.ParameterSize(200);
      //param.IsNullable = false;
        this.db.ParameterIsNullable(false);
      //param.SourceColumn = "Description";
        this.db.ParameterSourceColumn( "Description" );
      //this._adapter.UpdateCommand.Parameters.Add(param);
        this.db.AdapterUpdateCommandParametersAdd();

      //param = new global::MySql.Data.MySqlClient.MySqlParameter();
        this.db.NewSqlParameter();
      //param.ParameterName = "?StartBit";
        this.db.ParameterName( db.ParamPrefics + "StartBit" );
      //param.DbType = global::System.Data.DbType.Int32;
        this.db.ParameterDbType( global::System.Data.DbType.Int64 );
      //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
        this.db.ParameterSqlDbType( db.GettingInt64() );
      //param.IsNullable = false;
        this.db.ParameterIsNullable(false);
      //param.SourceColumn = "StartBit";
        this.db.ParameterSourceColumn( "StartBit" );
      //this._adapter.UpdateCommand.Parameters.Add(param);
        this.db.AdapterUpdateCommandParametersAdd();

      //param = new global::MySql.Data.MySqlClient.MySqlParameter();
        this.db.NewSqlParameter();
      //param.ParameterName = "?Length";
        this.db.ParameterName( db.ParamPrefics + "Length" );
      //param.DbType = global::System.Data.DbType.Int32;
        this.db.ParameterDbType( global::System.Data.DbType.Int64 );
      //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
        this.db.ParameterSqlDbType( db.GettingInt64() );
      //param.IsNullable = false;
        this.db.ParameterIsNullable(false);
      //param.SourceColumn = "Length";
        this.db.ParameterSourceColumn("Length");
      //this._adapter.UpdateCommand.Parameters.Add(param);
        this.db.AdapterUpdateCommandParametersAdd();
       
      //param = new global::MySql.Data.MySqlClient.MySqlParameter();
        this.db.NewSqlParameter();
      //param.ParameterName = "?NameUnit";
        this.db.ParameterName( db.ParamPrefics + "NameUnit" );
      //param.DbType = global::System.Data.DbType.String;
        this.db.ParameterDbType( global::System.Data.DbType.String );
      //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.String;
        this.db.ParameterSqlDbType( db.GettingString() );
      //param.Size = 45;
        this.db.ParameterSize(45);
      //param.IsNullable = false;
        this.db.ParameterIsNullable(false);
      //param.SourceColumn = "NameUnit";
        this.db.ParameterSourceColumn( "NameUnit" );
      //this._adapter.UpdateCommand.Parameters.Add(param);
        this.db.AdapterUpdateCommandParametersAdd();

      //param = new global::MySql.Data.MySqlClient.MySqlParameter();
        this.db.NewSqlParameter();
      //param.ParameterName = "?K";
        this.db.ParameterName( db.ParamPrefics + "K" );
      //param.DbType = global::System.Data.DbType.Double;
        this.db.ParameterDbType( global::System.Data.DbType.Double );
      //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Double;
        this.db.ParameterSqlDbType( db.GettingDouble() );
      //param.IsNullable = false;
        this.db.ParameterIsNullable(false);
      //param.SourceColumn = "K";
        this.db.ParameterSourceColumn("K");
      //this._adapter.UpdateCommand.Parameters.Add(param);
        this.db.AdapterUpdateCommandParametersAdd();

      //param = new global::MySql.Data.MySqlClient.MySqlParameter();
        this.db.NewSqlParameter();
      //param.ParameterName = "?id";
        this.db.ParameterName( db.ParamPrefics + "id" );
      //param.DbType = global::System.Data.DbType.Int32;
        this.db.ParameterDbType( global::System.Data.DbType.Int32 );
      //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
        this.db.ParameterSqlDbType( db.GettingInt32() );
      //param.IsNullable = true;
        this.db.ParameterIsNullable(true);
      //param.SourceColumn = "id";
        this.db.ParameterSourceColumn("id");
      //param.SourceVersion = global::System.Data.DataRowVersion.Original;
        this.db.ParameterSourceVersion( global::System.Data.DataRowVersion.Original );
      //this._adapter.UpdateCommand.Parameters.Add(param);
        this.db.AdapterUpdateCommandParametersAdd();

        //param = new global::MySql.Data.MySqlClient.MySqlParameter();
        this.db.NewSqlParameter();
        //param.ParameterName = "?K";
        this.db.ParameterName(db.ParamPrefics + "B");
        //param.DbType = global::System.Data.DbType.Double;
        this.db.ParameterDbType(global::System.Data.DbType.Double);
        //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Double;
        this.db.ParameterSqlDbType(db.GettingDouble());
        //param.IsNullable = false;
        this.db.ParameterIsNullable(false);
        //param.SourceColumn = "K";
        this.db.ParameterSourceColumn("B");
        //this._adapter.UpdateCommand.Parameters.Add(param);
        this.db.AdapterUpdateCommandParametersAdd();

        //param = new global::MySql.Data.MySqlClient.MySqlParameter();
        this.db.NewSqlParameter();
        //param.ParameterName = "?K";
        this.db.ParameterName(db.ParamPrefics + "S");
        //param.DbType = global::System.Data.DbType.Double;
        this.db.ParameterDbType(global::System.Data.DbType.Byte);
        //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Double;
        this.db.ParameterSqlDbType(db.GettingByte());
        //param.IsNullable = false;
        this.db.ParameterIsNullable(false);
        //param.SourceColumn = "K";
        this.db.ParameterSourceColumn("S");
        //this._adapter.UpdateCommand.Parameters.Add(param);
        this.db.AdapterUpdateCommandParametersAdd();
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    private void InitConnection()
    {
      //this._connection = new global::MySql.Data.MySqlClient.MySqlConnection();
        this.db.AdapNewConnection();
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    private void InitCommandCollection()
    {
      //this._commandCollection = new global::MySql.Data.MySqlClient.MySqlCommand[1];
        this.db.NewCommandCollection(1);
      //this._commandCollection[0] = new global::MySql.Data.MySqlClient.MySqlCommand();
        this.db.SetNewCommandCollection(0);
      //this._commandCollection[0].Connection = this.Connection;
        this.db.SetCommandCollectionConnection(this.Connection, 0);
      //this._commandCollection[0].CommandText =
      //  "SELECT id, mobitel_id, Name, Description, StartBit, Length, NameUnit, K FROM sensors";
        this.db.SetCommandCollectionText( "SELECT id, mobitel_id, Name, Description, StartBit, Length, NameUnit, K, B, S FROM sensors", 0);
      //this._commandCollection[0].CommandType = global::System.Data.CommandType.Text;
        this.db.SetCommandCollectionType( global::System.Data.CommandType.Text, 0);
    }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.ComponentModel.Design.HelpKeywordAttribute("vs.data.TableAdapter")]
        [global::System.ComponentModel.DataObjectMethodAttribute(global::System.ComponentModel.DataObjectMethodType.Fill,
            true)]

        public virtual int Fill(atlantaDataSet.sensorsDataTable dataTable)
        {
            //this.Adapter.SelectCommand = this.CommandCollection[0];

            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;
                adapter.SelectCommand = (MySqlCommand) this.CommandCollection[0];
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                SqlDataAdapter adapter = (SqlDataAdapter) this.Adapter;
                adapter.SelectCommand = (SqlCommand) this.CommandCollection[0];
            }

            if ((this.ClearBeforeFill == true))
            {
                dataTable.Clear();
            }

            //int returnValue = this.Adapter.Fill(dataTable);
            int returnValue = 0; // this.DataAdapter.Fill( dataTable );
            if( DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse )
            {
                MySqlDataAdapter adapter = (MySqlDataAdapter)this.Adapter;
                returnValue = adapter.Fill( dataTable );
            }
            else if( DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse )
            {
                SqlDataAdapter adapter = (SqlDataAdapter)this.Adapter;
                returnValue = adapter.Fill( dataTable );
            }

            return returnValue;
        }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.ComponentModel.Design.HelpKeywordAttribute("vs.data.TableAdapter")]
    [global::System.ComponentModel.DataObjectMethodAttribute(global::System.ComponentModel.DataObjectMethodType.Select, true)]
    public virtual atlantaDataSet.sensorsDataTable GetData()
    {
      //this.Adapter.SelectCommand = this.CommandCollection[0];
        //this.DataAdapter.SelectCommand = this.CommandCollection[0];
        if( DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse )
        {
            MySqlDataAdapter adapter = (MySqlDataAdapter)this.Adapter;
            adapter.SelectCommand = (MySqlCommand)this.CommandCollection[0];
        }
        else if( DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse )
        {
            SqlDataAdapter adapter = (SqlDataAdapter)this.Adapter;
            adapter.SelectCommand = (SqlCommand)this.CommandCollection[0];
        }

      atlantaDataSet.sensorsDataTable dataTable = new atlantaDataSet.sensorsDataTable();

      //this.Adapter.Fill(dataTable);
      //this.DataAdapter.Fill( dataTable );

      if( DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse )
      {
          MySqlDataAdapter adapter = (MySqlDataAdapter)this.Adapter;
          adapter.Fill( dataTable );
      }
      else if( DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse )
      {
          SqlDataAdapter adapter = (SqlDataAdapter)this.Adapter;
          adapter.Fill( dataTable );
      }

      return dataTable;
    }

    //[global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.ComponentModel.Design.HelpKeywordAttribute("vs.data.TableAdapter")]
    public virtual int Update(atlantaDataSet.sensorsDataTable dataTable)
    {
      //return this.Adapter.Update(dataTable);
        //return this.DataAdapter.Update( dataTable );
        try
        {
            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;
                return adapter.Update(dataTable);
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                SqlDataAdapter adapter = (SqlDataAdapter) this.Adapter;
                return adapter.Update(dataTable);
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message + " " + ex.InnerException);
        }

        return 0;
    }
  }
}
