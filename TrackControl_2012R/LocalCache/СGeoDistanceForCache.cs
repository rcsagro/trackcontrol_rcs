﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LocalCache
{
    internal class СGeoDistanceForCache
    {
        //Equatorial radius of the earth from WGS 84 
        //in meters, semi major axis = a
        private static int a = 6378137;
        //flattening = 1/298.257223563 = 0.0033528106647474805
        //first eccentricity squared = e = (2-flattening)*flattening
        private static double e = 0.0066943799901413165;
        //Miles to Meters conversion factor (take inverse for opposite)
        //private	static double milesToMeters = 1609.347;
        //Degrees to Radians converstion factor (take inverse for opposite)
        private static double degreesToRadians = System.Math.PI/180;


        /// <summary>
        /// расстояние в километрах Calculate from grad
        /// </summary>
        /// <param name="lat0">Lat 0</param>
        /// <param name="lon0">Lon 0</param>
        /// <param name="lat">Lat</param>
        /// <param name="lon">Lon</param>
        /// <returns>Double</returns>
        public static double CalculateFromGrad(double lat0, double lon0, double lat, double lon)
        {
            lat0 = lat0*degreesToRadians;
            lon0 = lon0*degreesToRadians;
            lat = lat*degreesToRadians;
            lon = lon*degreesToRadians;

            return CalculateFromRadian(lat0, lon0, lat, lon);

        }

        /// <summary>
        /// Calculate from radian
        /// </summary>
        /// <param name="lat0">Lat 0</param>
        /// <param name="lon0">Lon 0</param>
        /// <param name="lat">Lat</param>
        /// <param name="lon">Lon</param>
        /// <returns>Double</returns>
        public static double CalculateFromRadian(double lat0, double lon0, double lat, double lon)
        {
            double Rm = calcMeridionalRadiusOfCurvature(lat0);

            double Rpv = calcRoCinPrimeVertical(lat0);

            double distance = Math.Sqrt(Math.Pow(Rm, 2)*Math.Pow(lat - lat0, 2) +
                                        Math.Pow(Rpv, 2)*Math.Pow(Math.Cos(lat0), 2)*Math.Pow(lon - lon0, 2));

            return distance/1000;
        }

        /// <summary>
        /// Calc ro cin prime vertical
        /// </summary>
        /// <param name="lat0">Lat 0</param>
        /// <returns>Double</returns>
        private static double calcRoCinPrimeVertical(double lat0)
        {
            return a/Math.Sqrt(1 - e*Math.Pow(Math.Sin(lat0), 2));
        }

        /// <summary>
        /// Calc meridional radius of curvature
        /// </summary>
        /// <param name="lat0">Lat 0</param>
        /// <returns>Double</returns>
        private static double calcMeridionalRadiusOfCurvature(double lat0)
        {
            return a*(1 - e)/Math.Pow(1 - e*(Math.Pow(Math.Sin(lat0), 2)), 1.5);
        }
    }
}
