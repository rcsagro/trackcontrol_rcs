using System;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using TrackControl.General.DAL;
using TrackControl.General.DatabaseDriver;

namespace LocalCache.atlantaDataSetTableAdapters
{
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Design.TypedDataSetGenerator", "2.0.0.0")]
    [global::System.ComponentModel.DesignerCategoryAttribute("code")]
    [global::System.ComponentModel.ToolboxItem(true)]
    [global::System.ComponentModel.DataObjectAttribute(true)]
    [global::System.ComponentModel.DesignerAttribute(
        "Microsoft.VSDesigner.DataSource.Design.TableAdapterDesigner, Microsoft.VSDesigner" +
        ", Version=8.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")]
    [global::System.ComponentModel.Design.HelpKeywordAttribute("vs.data.TableAdapter")]
    public class SettingTableAdapter : global::System.ComponentModel.Component
    {

        //private global::MySql.Data.MySqlClient.MySqlDataAdapter _adapter;
        //private global::MySql.Data.MySqlClient.MySqlConnection _connection;
        //private global::MySql.Data.MySqlClient.MySqlCommand[] _commandCollection;
        private DriverDb db = new DriverDb();

        private bool _clearBeforeFill;

        //[global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public SettingTableAdapter()
        {
            this.ClearBeforeFill = true;
        }

        //[global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        //private global::MySql.Data.MySqlClient.MySqlDataAdapter Adapter
        private object Adapter
        {
            get
            {
                if ((this.db.Adapter == null))
                {
                    this.InitAdapter();
                }

                return this.db.Adapter;
            }
        }

        //[global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        //public global::MySql.Data.MySqlClient.MySqlConnection Connection
        public object Connection
        {
            get
            {
                if ((this.db.Connection == null))
                {
                    this.InitConnection();
                }

                return this.db.Connection;
            }
            set
            {
                this.db.Connection = value;

                if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                {
                    MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;

                    if ((adapter.InsertCommand != null))
                    {
                        adapter.InsertCommand.Connection = (MySqlConnection) value;
                    }

                    if ((adapter.DeleteCommand != null))
                    {
                        adapter.DeleteCommand.Connection = (MySqlConnection) value;
                    }

                    if ((adapter.UpdateCommand != null))
                    {
                        adapter.UpdateCommand.Connection = (MySqlConnection) value;
                    }

                    MySqlCommand[] collection = (MySqlCommand[]) this.CommandCollection;

                    for (int i = 0; (i < this.CommandCollection.Length); i = (i + 1))
                    {
                        if ((this.CommandCollection[i] != null))
                        {
                            collection[i].Connection = (MySqlConnection) value;
                        }
                    } // for
                } // if
                else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                {
                    SqlDataAdapter adapter = (SqlDataAdapter) this.Adapter;

                    if ((adapter.InsertCommand != null))
                    {
                        adapter.InsertCommand.Connection = (SqlConnection) value;
                    }

                    if ((adapter.DeleteCommand != null))
                    {
                        adapter.DeleteCommand.Connection = (SqlConnection) value;
                    }

                    if ((adapter.UpdateCommand != null))
                    {
                        adapter.UpdateCommand.Connection = (SqlConnection) value;
                    }

                    SqlCommand[] collection = (SqlCommand[]) this.CommandCollection;

                    for (int i = 0; (i < this.CommandCollection.Length); i = (i + 1))
                    {
                        if ((this.CommandCollection[i] != null))
                        {
                            collection[i].Connection = (SqlConnection) value;
                        }
                    } // for
                } // if
            } // set
        } // Connection

        //[global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        //protected global::MySql.Data.MySqlClient.MySqlCommand[] CommandCollection
        protected object[] CommandCollection
        {
            get
            {
                if ((this.db.CommandCollection == null))
                {
                    this.InitCommandCollection();
                }

                return this.db.CommandCollection;
            }
        }

        //[global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public bool ClearBeforeFill
        {
            get { return this._clearBeforeFill; }
            set { this._clearBeforeFill = value; }
        }

        //[global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        private void InitAdapter()
        {
            //this._adapter = new global::MySql.Data.MySqlClient.MySqlDataAdapter();
            this.db.NewDataAdapter();
            global::System.Data.Common.DataTableMapping tableMapping = new global::System.Data.Common.DataTableMapping();
            tableMapping.SourceTable = "Table";
            tableMapping.DataSetTable = "setting";
            tableMapping.ColumnMappings.Add("id", "id");
            tableMapping.ColumnMappings.Add("TimeBreak", "TimeBreak");
            tableMapping.ColumnMappings.Add("RotationMain", "RotationMain");
            tableMapping.ColumnMappings.Add("RotationAdd", "RotationAdd");
            tableMapping.ColumnMappings.Add("FuelingEdge", "FuelingEdge");
            tableMapping.ColumnMappings.Add("band", "band");
            tableMapping.ColumnMappings.Add("FuelingDischarge", "FuelingDischarge");
            tableMapping.ColumnMappings.Add("idleRunningMain", "idleRunningMain");
            tableMapping.ColumnMappings.Add("idleRunningAdd", "idleRunningAdd");
            tableMapping.ColumnMappings.Add("Name", "Name");
            tableMapping.ColumnMappings.Add("Desc", "Desc");
            tableMapping.ColumnMappings.Add("accelMax", "accelMax");
            tableMapping.ColumnMappings.Add("breakMax", "breakMax");
            tableMapping.ColumnMappings.Add("speedMax", "speedMax");
            tableMapping.ColumnMappings.Add( "speedMin", "speedMin" );
            tableMapping.ColumnMappings.Add("AvgFuelRatePerHour", "AvgFuelRatePerHour");
            tableMapping.ColumnMappings.Add("FuelApproximationTime", "FuelApproximationTime");
            tableMapping.ColumnMappings.Add("TimeGetFuelAfterStop", "TimeGetFuelAfterStop");
            tableMapping.ColumnMappings.Add("TimeGetFuelBeforeMotion", "TimeGetFuelBeforeMotion");
            tableMapping.ColumnMappings.Add("CzMinCrossingPairTime", "CzMinCrossingPairTime");
            tableMapping.ColumnMappings.Add("FuelerRadiusFinds", "FuelerRadiusFinds");
            tableMapping.ColumnMappings.Add("FuelerMaxTimeStop", "FuelerMaxTimeStop");
            tableMapping.ColumnMappings.Add("FuelerMinFuelrate", "FuelerMinFuelrate");
            tableMapping.ColumnMappings.Add("FuelerCountInMotion", "FuelerCountInMotion");
            tableMapping.ColumnMappings.Add("FuelrateWithDischarge", "FuelrateWithDischarge");
            tableMapping.ColumnMappings.Add("FuelingMinMaxAlgorithm", "FuelingMinMaxAlgorithm");
            tableMapping.ColumnMappings.Add("TimeBreakMaxPermitted", "TimeBreakMaxPermitted");
            tableMapping.ColumnMappings.Add("TimeMinimMovingSize", "TimeMinimMovingSize");
            tableMapping.ColumnMappings.Add("InclinometerMaxAngleAxisX", "InclinometerMaxAngleAxisX");
            tableMapping.ColumnMappings.Add("InclinometerMaxAngleAxisY", "InclinometerMaxAngleAxisY");
            tableMapping.ColumnMappings.Add("MinTimeSwitch", "MinTimeSwitch");

            //this._adapter.TableMappings.Add(tableMapping); �� ��� ����
            this.db.AdapterTableMappingsAdd(tableMapping);

            //this._adapter.DeleteCommand = new global::MySql.Data.MySqlClient.MySqlCommand();
            this.db.AdapterDeleteCommand();
            //this._adapter.DeleteCommand.Connection = this.Connection;
            this.db.AdapterDeleteCommandConnection(this.Connection);
            //this._adapter.DeleteCommand.CommandText = "DELETE FROM setting WHERE (id = ?id)";
            this.db.AdapterDeleteCommandText("DELETE FROM setting WHERE (id = " + db.ParamPrefics + "id)");
            //this._adapter.DeleteCommand.CommandType = global::System.Data.CommandType.Text;
            this.db.AdapterDeleteCommandType(global::System.Data.CommandType.Text);
            //global::MySql.Data.MySqlClient.MySqlParameter param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?id";
            this.db.ParameterName(db.ParamPrefics + "id");
            //param.DbType = global::System.Data.DbType.Int32;
            this.db.ParameterDbType(global::System.Data.DbType.Int32);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType(db.GettingInt32());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "id";
            this.db.ParameterSourceColumn("id");
            //param.SourceVersion = global::System.Data.DataRowVersion.Original;
            this.db.ParameterSourceVersion(global::System.Data.DataRowVersion.Original);
            //this._adapter.DeleteCommand.Parameters.Add(param);
            this.db.AdapterDeleteCommandParametersAdd();

            //this._adapter.InsertCommand = new global::MySql.Data.MySqlClient.MySqlCommand();
            this.db.AdapterInsertCommand();
            //this._adapter.InsertCommand.Connection = this.Connection;
            this.db.AdapterInsertCommandConnection(this.Connection);
//      this._adapter.InsertCommand.CommandText =
//        @"INSERT INTO `setting` (`band`, `FuelingDischarge`, `idleRunningMain`, `idleRunningAdd`, `FuelingEdge`, 
//           `RotationAdd`, `RotationMain`, `TimeBreak`, `Name`, `Desc`, `accelMax`, `breakMax`, `speedMax`,
//           `AvgFuelRatePerHour`,`FuelApproximationTime`,`TimeGetFuelAfterStop`,`TimeGetFuelBeforeMotion`, 
//           `CzMinCrossingPairTime`, `FuelerRadiusFinds`, `FuelerMaxTimeStop`, `FuelerMinFuelrate`,
//            `FuelerCountInMotion`, `FuelrateWithDischarge`, `FuelingMinMaxAlgorithm`, `TimeBreakMaxPermitted`) 
//          VALUES (?band, ?FuelingDischarge, ?idleRunningMain, ?idleRunningAdd, ?FuelingEdge, 
//            ?RotationAdd, ?RotationMain, ?TimeBreak, ?Name, ?Desc, ?accelMax, ?breakMax, ?speedMax, 
//            ?AvgFuelRatePerHour, ?FuelApproximationTime, ?TimeGetFuelAfterStop, ?TimeGetFuelBeforeMotion,
//            ?CzMinCrossingPairTime, ?FuelerRadiusFinds, ?FuelerMaxTimeStop, ?FuelerMinFuelrate,
//            ?FuelerCountInMotion, ?FuelrateWithDischarge, ?FuelingMinMaxAlgorithm, ?TimeBreakMaxPermitted)";

            this.db.AdapterInsertCommandText(TrackControlQuery.SettingTableAdapter.InsertIntoSetting);

            //this._adapter.InsertCommand.CommandType = global::System.Data.CommandType.Text;
            this.db.AdapterInsertCommandType(global::System.Data.CommandType.Text);

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?band";
            this.db.ParameterName(db.ParamPrefics + "band");
            //param.DbType = global::System.Data.DbType.Int32;
            this.db.ParameterDbType(global::System.Data.DbType.Int32);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType(db.GettingInt32());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "band";
            this.db.ParameterSourceColumn("band");
            //this._adapter.InsertCommand.Parameters.Add(param);
            this.db.AdapterInsertCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?FuelingDischarge";
            this.db.ParameterName(db.ParamPrefics + "FuelingDischarge");
            //param.DbType = global::System.Data.DbType.Int32;
            this.db.ParameterDbType(global::System.Data.DbType.Int32);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType(db.GettingInt32());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "FuelingDischarge";
            this.db.ParameterSourceColumn("FuelingDischarge");
            //this._adapter.InsertCommand.Parameters.Add(param);
            this.db.AdapterInsertCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?idleRunningMain";
            this.db.ParameterName(db.ParamPrefics + "idleRunningMain");
            //param.DbType = global::System.Data.DbType.Int32;
            this.db.ParameterDbType(global::System.Data.DbType.Int32);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType(db.GettingInt32());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "idleRunningMain";
            this.db.ParameterSourceColumn("idleRunningMain");
            //this._adapter.InsertCommand.Parameters.Add(param);
            this.db.AdapterInsertCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?idleRunningAdd";
            this.db.ParameterName(db.ParamPrefics + "idleRunningAdd");
            //param.DbType = global::System.Data.DbType.Int32;
            this.db.ParameterDbType(global::System.Data.DbType.Int32);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType(db.GettingInt32());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "idleRunningAdd";
            this.db.ParameterSourceColumn("idleRunningAdd");
            //this._adapter.InsertCommand.Parameters.Add(param);
            this.db.AdapterInsertCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?FuelingEdge";
            this.db.ParameterName(db.ParamPrefics + "FuelingEdge");
            //param.DbType = global::System.Data.DbType.Int32;
            this.db.ParameterDbType(global::System.Data.DbType.Int32);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType(db.GettingInt32());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "FuelingEdge";
            this.db.ParameterSourceColumn("FuelingEdge");
            //this._adapter.InsertCommand.Parameters.Add(param);
            this.db.AdapterInsertCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?RotationAdd";
            this.db.ParameterName(db.ParamPrefics + "RotationAdd");
            //param.DbType = global::System.Data.DbType.Int32;
            this.db.ParameterDbType(global::System.Data.DbType.Int32);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType(db.GettingInt32());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "RotationAdd";
            this.db.ParameterSourceColumn("RotationAdd");
            //this._adapter.InsertCommand.Parameters.Add(param);
            this.db.AdapterInsertCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?RotationMain";
            this.db.ParameterName(db.ParamPrefics + "RotationMain");
            //param.DbType = global::System.Data.DbType.Int32;
            this.db.ParameterDbType(global::System.Data.DbType.Int32);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType(db.GettingInt32());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "RotationMain";
            this.db.ParameterSourceColumn("RotationMain");
            //this._adapter.InsertCommand.Parameters.Add(param);
            this.db.AdapterInsertCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?TimeBreak";
            this.db.ParameterName(db.ParamPrefics + "TimeBreak");
            //param.DbType = global::System.Data.DbType.Object;
            this.db.ParameterDbType(global::System.Data.DbType.Object);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Time;
            this.db.ParameterSqlDbType(db.GettingTime());
            //param.Size = 1024;
            this.db.ParameterSize(1024);
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "TimeBreak";
            this.db.ParameterSourceColumn("TimeBreak");
            //this._adapter.InsertCommand.Parameters.Add(param);
            this.db.AdapterInsertCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?Name";
            this.db.ParameterName(db.ParamPrefics + "Name");
            //param.DbType = global::System.Data.DbType.String;
            this.db.ParameterDbType(global::System.Data.DbType.String);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.String;
            this.db.ParameterSqlDbType(db.GettingString());
            //param.Size = 20;
            this.db.ParameterSize(20);
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "Name";
            this.db.ParameterSourceColumn("Name");
            //this._adapter.InsertCommand.Parameters.Add(param);
            this.db.AdapterInsertCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?Desc";
            this.db.ParameterName(db.ParamPrefics + "Desc");
            //param.DbType = global::System.Data.DbType.String;
            this.db.ParameterDbType(global::System.Data.DbType.String);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.String;
            this.db.ParameterSqlDbType(db.GettingString());
            //param.Size = 80;
            this.db.ParameterSize(80);
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "Desc";
            this.db.ParameterSourceColumn("Desc");
            //this._adapter.InsertCommand.Parameters.Add(param);
            this.db.AdapterInsertCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?accelMax";
            this.db.ParameterName(db.ParamPrefics + "accelMax");
            //param.DbType = global::System.Data.DbType.Decimal;
            this.db.ParameterDbType(global::System.Data.DbType.Decimal);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Double;
            this.db.ParameterSqlDbType(db.GettingDouble());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "accelMax";
            this.db.ParameterSourceColumn("accelMax");
            //this._adapter.InsertCommand.Parameters.Add(param);
            this.db.AdapterInsertCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?breakMax";
            this.db.ParameterName(db.ParamPrefics + "breakMax");
            //param.DbType = global::System.Data.DbType.Decimal;
            this.db.ParameterDbType(global::System.Data.DbType.Decimal);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Double;
            this.db.ParameterSqlDbType(db.GettingDouble());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "breakMax";
            this.db.ParameterSourceColumn("breakMax");
            //this._adapter.InsertCommand.Parameters.Add(param);
            this.db.AdapterInsertCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?speedMax";
            this.db.ParameterName(db.ParamPrefics + "speedMax");
            //param.DbType = global::System.Data.DbType.Decimal;
            this.db.ParameterDbType(global::System.Data.DbType.Double);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Double;
            this.db.ParameterSqlDbType(db.GettingDouble());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "speedMax";
            this.db.ParameterSourceColumn("speedMax");
            //this._adapter.InsertCommand.Parameters.Add(param);
            this.db.AdapterInsertCommandParametersAdd();

            this.db.NewSqlParameter();
            this.db.ParameterName( db.ParamPrefics + "speedMin" );
            this.db.ParameterDbType( global::System.Data.DbType.Double );
            this.db.ParameterSqlDbType( db.GettingDouble() );
            this.db.ParameterIsNullable( true );
            this.db.ParameterSourceColumn( "speedMin" );
            this.db.AdapterInsertCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            // param.ParameterName = "?AvgFuelRatePerHour";
            this.db.ParameterName(db.ParamPrefics + "AvgFuelRatePerHour");
            //param.DbType = global::System.Data.DbType.Decimal;
            this.db.ParameterDbType(global::System.Data.DbType.Decimal);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Double;
            this.db.ParameterSqlDbType(db.GettingDouble());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "AvgFuelRatePerHour";
            this.db.ParameterSourceColumn("AvgFuelRatePerHour");
            //this._adapter.InsertCommand.Parameters.Add(param);
            this.db.AdapterInsertCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?FuelApproximationTime";
            this.db.ParameterName(db.ParamPrefics + "FuelApproximationTime");
            //param.DbType = global::System.Data.DbType.Object;
            this.db.ParameterDbType(global::System.Data.DbType.Object);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Time;
            this.db.ParameterSqlDbType(db.GettingTime());
            //param.Size = 1024;
            this.db.ParameterSize(1024);
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "FuelApproximationTime";
            this.db.ParameterSourceColumn("FuelApproximationTime");
            //this._adapter.InsertCommand.Parameters.Add(param);
            this.db.AdapterInsertCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?TimeGetFuelAfterStop";
            this.db.ParameterName(db.ParamPrefics + "TimeGetFuelAfterStop");
            //param.DbType = global::System.Data.DbType.Object;
            this.db.ParameterDbType(global::System.Data.DbType.Object);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Time;
            this.db.ParameterSqlDbType(db.GettingTime());
            //param.Size = 1024;
            this.db.ParameterSize(1024);
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "TimeGetFuelAfterStop";
            this.db.ParameterSourceColumn("TimeGetFuelAfterStop");
            //this._adapter.InsertCommand.Parameters.Add(param);
            this.db.AdapterInsertCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?TimeGetFuelBeforeMotion";
            this.db.ParameterName(db.ParamPrefics + "TimeGetFuelBeforeMotion");
            //param.DbType = global::System.Data.DbType.Object;
            this.db.ParameterDbType(global::System.Data.DbType.Object);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Time;
            this.db.ParameterSqlDbType(db.GettingTime());
            //param.Size = 1024;
            this.db.ParameterSize(1024);
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "TimeGetFuelBeforeMotion";
            this.db.ParameterSourceColumn("TimeGetFuelBeforeMotion");
            //this._adapter.InsertCommand.Parameters.Add(param);
            this.db.AdapterInsertCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?CzMinCrossingPairTime";
            this.db.ParameterName(db.ParamPrefics + "CzMinCrossingPairTime");
            //param.DbType = global::System.Data.DbType.Object;
            this.db.ParameterDbType(global::System.Data.DbType.Object);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Time;
            this.db.ParameterSqlDbType(db.GettingTime());
            //param.Size = 1024;
            this.db.ParameterSize(1024);
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "CzMinCrossingPairTime";
            this.db.ParameterSourceColumn("CzMinCrossingPairTime");
            //this._adapter.InsertCommand.Parameters.Add(param);
            this.db.AdapterInsertCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?FuelerRadiusFinds";
            this.db.ParameterName(db.ParamPrefics + "FuelerRadiusFinds");
            //param.DbType = global::System.Data.DbType.Int32;
            this.db.ParameterDbType(global::System.Data.DbType.Int32);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType(db.GettingInt32());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "FuelerRadiusFinds";
            this.db.ParameterSourceColumn("FuelerRadiusFinds");
            //this._adapter.InsertCommand.Parameters.Add(param);
            this.db.AdapterInsertCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?FuelerMaxTimeStop";
            this.db.ParameterName(db.ParamPrefics + "FuelerMaxTimeStop");
            //param.DbType = global::System.Data.DbType.Object;
            this.db.ParameterDbType(global::System.Data.DbType.Object);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Time;
            this.db.ParameterSqlDbType(db.GettingTime());
            //param.Size = 1024;
            this.db.ParameterSize(1024);
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "FuelerMaxTimeStop";
            this.db.ParameterSourceColumn("FuelerMaxTimeStop");
            //this._adapter.InsertCommand.Parameters.Add(param);
            this.db.AdapterInsertCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?FuelerMinFuelrate";
            this.db.ParameterName(db.ParamPrefics + "FuelerMinFuelrate");
            //param.DbType = global::System.Data.DbType.Int32;
            this.db.ParameterDbType(global::System.Data.DbType.Int32);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType(db.GettingInt32());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "FuelerMinFuelrate";
            this.db.ParameterSourceColumn("FuelerMinFuelrate");
            //this._adapter.InsertCommand.Parameters.Add(param);
            this.db.AdapterInsertCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?FuelerCountInMotion";
            this.db.ParameterName(db.ParamPrefics + "FuelerCountInMotion");
            //param.DbType = global::System.Data.DbType.Int32;
            this.db.ParameterDbType(global::System.Data.DbType.Int32);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType(db.GettingInt32());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "FuelerCountInMotion";
            this.db.ParameterSourceColumn("FuelerCountInMotion");
            //this._adapter.InsertCommand.Parameters.Add(param);
            this.db.AdapterInsertCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();//
            this.db.NewSqlParameter();
            //param.ParameterName = "?FuelrateWithDischarge";
            this.db.ParameterName(db.ParamPrefics + "FuelrateWithDischarge");
            //param.DbType = global::System.Data.DbType.Int32;
            this.db.ParameterDbType(global::System.Data.DbType.Int32);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType(db.GettingInt32());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "FuelrateWithDischarge";
            this.db.ParameterSourceColumn("FuelrateWithDischarge");
            //this._adapter.InsertCommand.Parameters.Add(param);
            this.db.AdapterInsertCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();//
            this.db.NewSqlParameter();
            //param.ParameterName = "?FuelingMinMaxAlgorithm";
            this.db.ParameterName(db.ParamPrefics + "FuelingMinMaxAlgorithm");
            //param.DbType = global::System.Data.DbType.Int32;
            this.db.ParameterDbType(global::System.Data.DbType.Int32);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType(db.GettingInt32());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "FuelingMinMaxAlgorithm";
            this.db.ParameterSourceColumn("FuelingMinMaxAlgorithm");
            //this._adapter.InsertCommand.Parameters.Add(param);
            this.db.AdapterInsertCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?TimeBreakMaxPermitted";
            this.db.ParameterName(db.ParamPrefics + "TimeBreakMaxPermitted");
            //param.DbType = global::System.Data.DbType.Object;
            this.db.ParameterDbType(global::System.Data.DbType.Object);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Time;
            this.db.ParameterSqlDbType(db.GettingTime());
            //param.Size = 1024;
            this.db.ParameterSize(1024);
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "TimeBreakMaxPermitted";
            this.db.ParameterSourceColumn("TimeBreakMaxPermitted");
            //this._adapter.InsertCommand.Parameters.Add(param);
            this.db.AdapterInsertCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?accelMax";
            this.db.ParameterName(db.ParamPrefics + "InclinometerMaxAngleAxisX");
            //param.DbType = global::System.Data.DbType.Decimal;
            this.db.ParameterDbType(global::System.Data.DbType.Double);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Double;
            this.db.ParameterSqlDbType(db.GettingDouble());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "accelMax";
            this.db.ParameterSourceColumn("InclinometerMaxAngleAxisX");
            //this._adapter.InsertCommand.Parameters.Add(param);
            this.db.AdapterInsertCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?accelMax";
            this.db.ParameterName(db.ParamPrefics + "InclinometerMaxAngleAxisY");
            //param.DbType = global::System.Data.DbType.Decimal;
            this.db.ParameterDbType(global::System.Data.DbType.Double);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Double;
            this.db.ParameterSqlDbType(db.GettingDouble());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "accelMax";
            this.db.ParameterSourceColumn("InclinometerMaxAngleAxisY");
            //this._adapter.InsertCommand.Parameters.Add(param);
            this.db.AdapterInsertCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?TimeMinimMovingSize";
            this.db.ParameterName(db.ParamPrefics + "TimeMinimMovingSize");
            //param.DbType = global::System.Data.DbType.Object;
            this.db.ParameterDbType(global::System.Data.DbType.Object);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Time;
            this.db.ParameterSqlDbType(db.GettingTime());
            //param.Size = 1024;
            this.db.ParameterSize(1024);
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "TimeMinimMovingSize";
            this.db.ParameterSourceColumn("TimeMinimMovingSize");
            //this._adapter.InsertCommand.Parameters.Add(param);
            this.db.AdapterInsertCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?FuelerRadiusFinds";
            this.db.ParameterName(db.ParamPrefics + "MinTimeSwitch");
            //param.DbType = global::System.Data.DbType.Int32;
            this.db.ParameterDbType(global::System.Data.DbType.Int32);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType(db.GettingInt32());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(false);
            //param.SourceColumn = "FuelerRadiusFinds";
            this.db.ParameterSourceColumn("MinTimeSwitch");
            //this._adapter.InsertCommand.Parameters.Add(param);
            this.db.AdapterInsertCommandParametersAdd();

            //this._adapter.UpdateCommand = new global::MySql.Data.MySqlClient.MySqlCommand();
            this.db.AdapterUpdateCommand();
            //this._adapter.UpdateCommand.Connection = this.Connection;
            this.db.AdapterUpdateCommandConnection(this.Connection);
//      this._adapter.UpdateCommand.CommandText =
//        @"UPDATE setting
//          SET  TimeBreak = ?p1, RotationMain = ?p2, RotationAdd = ?p3, FuelingEdge = ?p4, band = ?p5, 
//            FuelingDischarge = ?p6, accelMax = ?p7, breakMax = ?p8, speedMax = ?p9, 
//            idleRunningMain = ?p10, idleRunningAdd = ?p11, Name = ?p12, `Desc` = ?p13, 
//            AvgFuelRatePerHour = ?p14, FuelApproximationTime = ?p15, TimeGetFuelAfterStop = ?p16, 
//            TimeGetFuelBeforeMotion = ?p17, CzMinCrossingPairTime = ?p18,
//            FuelerRadiusFinds = ?p19, FuelerMaxTimeStop = ?p20, FuelerMinFuelrate = ?p21,
//            FuelerCountInMotion = ?p22, FuelrateWithDischarge = ?p23, FuelingMinMaxAlgorithm = ?p24, TimeBreakMaxPermitted = ?p25
//          WHERE (id = ?id)";

            this.db.AdapterUpdateCommandText(TrackControlQuery.SettingTableAdapter.UpdateSetting);

            //
            //this._adapter.UpdateCommand.CommandType = global::System.Data.CommandType.Text;
            this.db.AdapterUpdateCommandType(global::System.Data.CommandType.Text);

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?p1";
            this.db.ParameterName(db.ParamPrefics + "p1");
            //param.DbType = global::System.Data.DbType.Object;
            this.db.ParameterDbType(global::System.Data.DbType.Object);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Time;
            this.db.ParameterSqlDbType(db.GettingTime());
            //param.Size = 1024;
            this.db.ParameterSize(1024);
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "TimeBreak";
            this.db.ParameterSourceColumn("TimeBreak");
            //this._adapter.UpdateCommand.Parameters.Add(param);
            this.db.AdapterUpdateCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?p2";
            this.db.ParameterName(db.ParamPrefics + "p2");
            //param.DbType = global::System.Data.DbType.Int32;
            this.db.ParameterDbType(global::System.Data.DbType.Int32);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType(db.GettingInt32());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "RotationMain";
            this.db.ParameterSourceColumn("RotationMain");
            //this._adapter.UpdateCommand.Parameters.Add(param);
            this.db.AdapterUpdateCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?p3";
            this.db.ParameterName(db.ParamPrefics + "p3");
            //param.DbType = global::System.Data.DbType.Int32;
            this.db.ParameterDbType(global::System.Data.DbType.Int32);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType(db.GettingInt32());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "RotationAdd";
            this.db.ParameterSourceColumn("RotationAdd");
            //this._adapter.UpdateCommand.Parameters.Add(param);
            this.db.AdapterUpdateCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?p4";
            this.db.ParameterName(db.ParamPrefics + "p4");
            //param.DbType = global::System.Data.DbType.Int32;
            this.db.ParameterDbType(global::System.Data.DbType.Int32);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType(db.GettingInt32());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "FuelingEdge";
            this.db.ParameterSourceColumn("FuelingEdge");
            //this._adapter.UpdateCommand.Parameters.Add(param);
            this.db.AdapterUpdateCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?p5";
            this.db.ParameterName(db.ParamPrefics + "p5");
            //param.DbType = global::System.Data.DbType.Int32;
            this.db.ParameterDbType(global::System.Data.DbType.Int32);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType(db.GettingInt32());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "band";
            this.db.ParameterSourceColumn("band");
            //this._adapter.UpdateCommand.Parameters.Add(param);
            this.db.AdapterUpdateCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?p6";
            this.db.ParameterName(db.ParamPrefics + "p6");
            //param.DbType = global::System.Data.DbType.Int32;
            this.db.ParameterDbType(global::System.Data.DbType.Int32);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType(db.GettingInt32());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "FuelingDischarge";
            this.db.ParameterSourceColumn("FuelingDischarge");
            //this._adapter.UpdateCommand.Parameters.Add(param);
            this.db.AdapterUpdateCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?p7";
            this.db.ParameterName(db.ParamPrefics + "p7");
            //param.DbType = global::System.Data.DbType.Decimal;
            this.db.ParameterDbType(global::System.Data.DbType.Double);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Double;
            this.db.ParameterSqlDbType(db.GettingDouble());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "accelMax";
            this.db.ParameterSourceColumn("accelMax");
            //this._adapter.UpdateCommand.Parameters.Add(param);
            this.db.AdapterUpdateCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?p8";
            this.db.ParameterName(db.ParamPrefics + "p8");
            //param.DbType = global::System.Data.DbType.Decimal;
            this.db.ParameterDbType(global::System.Data.DbType.Double);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Double;
            this.db.ParameterSqlDbType(db.GettingDouble());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "breakMax";
            this.db.ParameterSourceColumn("breakMax");
            //this._adapter.UpdateCommand.Parameters.Add(param);
            this.db.AdapterUpdateCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?p9";
            this.db.ParameterName(db.ParamPrefics + "p9");
            //param.DbType = global::System.Data.DbType.Decimal;
            this.db.ParameterDbType(global::System.Data.DbType.Double);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Double;
            this.db.ParameterSqlDbType(db.GettingDouble());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "speedMax";
            this.db.ParameterSourceColumn("speedMax");
            //this._adapter.UpdateCommand.Parameters.Add(param);
            this.db.AdapterUpdateCommandParametersAdd();

            this.db.NewSqlParameter();
            this.db.ParameterName( db.ParamPrefics + "p99" );
            this.db.ParameterDbType( global::System.Data.DbType.Double );
            this.db.ParameterSqlDbType( db.GettingDouble() );
            this.db.ParameterIsNullable( true );
            this.db.ParameterSourceColumn( "speedMin" );
            this.db.AdapterUpdateCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?p10";
            this.db.ParameterName(db.ParamPrefics + "p10");
            //param.DbType = global::System.Data.DbType.Int32;
            this.db.ParameterDbType(global::System.Data.DbType.Int32);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType(db.GettingInt32());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "idleRunningMain";
            this.db.ParameterSourceColumn("idleRunningMain");
            //this._adapter.UpdateCommand.Parameters.Add(param);
            this.db.AdapterUpdateCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?p11";
            this.db.ParameterName(db.ParamPrefics + "p11");
            //param.DbType = global::System.Data.DbType.Int32;
            this.db.ParameterDbType(global::System.Data.DbType.Int32);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType(db.GettingInt32());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "idleRunningAdd";
            this.db.ParameterSourceColumn("idleRunningAdd");
            //this._adapter.UpdateCommand.Parameters.Add(param);
            this.db.AdapterUpdateCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?p12";
            this.db.ParameterName(db.ParamPrefics + "p12");
            //param.DbType = global::System.Data.DbType.String;
            this.db.ParameterDbType(global::System.Data.DbType.String);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.String;
            this.db.ParameterSqlDbType(db.GettingString());
            //param.Size = 20;
            this.db.ParameterSize(20);
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "Name";
            this.db.ParameterSourceColumn("Name");
            //this._adapter.UpdateCommand.Parameters.Add(param);
            this.db.AdapterUpdateCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?p13";
            this.db.ParameterName(db.ParamPrefics + "p13");
            //param.DbType = global::System.Data.DbType.String;
            this.db.ParameterDbType(global::System.Data.DbType.String);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.String;
            this.db.ParameterSqlDbType(db.GettingString());
            //param.Size = 80;
            this.db.ParameterSize(80);
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "Desc";
            this.db.ParameterSourceColumn("Desc");
            //this._adapter.UpdateCommand.Parameters.Add(param);
            this.db.AdapterUpdateCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?p14";
            this.db.ParameterName(db.ParamPrefics + "p14");
            //param.DbType = global::System.Data.DbType.Decimal;
            this.db.ParameterDbType(global::System.Data.DbType.Decimal);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Double;
            this.db.ParameterSqlDbType(db.GettingDouble());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "AvgFuelRatePerHour";
            this.db.ParameterSourceColumn("AvgFuelRatePerHour");
            //this._adapter.UpdateCommand.Parameters.Add(param);
            this.db.AdapterUpdateCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?p15";
            this.db.ParameterName(db.ParamPrefics + "p15");
            //param.DbType = global::System.Data.DbType.Object;
            this.db.ParameterDbType(global::System.Data.DbType.Object);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Time;
            this.db.ParameterSqlDbType(db.GettingTime());
            //param.Size = 1024;
            this.db.ParameterSize(1024);
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "FuelApproximationTime";
            this.db.ParameterSourceColumn("FuelApproximationTime");
            //this._adapter.UpdateCommand.Parameters.Add(param);
            this.db.AdapterUpdateCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?p16";
            this.db.ParameterName(db.ParamPrefics + "p16");
            //param.DbType = global::System.Data.DbType.Object;
            this.db.ParameterDbType(global::System.Data.DbType.Object);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Time;
            this.db.ParameterSqlDbType(db.GettingTime());
            //param.Size = 1024;
            this.db.ParameterSize(1024);
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "TimeGetFuelAfterStop";
            this.db.ParameterSourceColumn("TimeGetFuelAfterStop");
            //this._adapter.UpdateCommand.Parameters.Add(param);
            this.db.AdapterUpdateCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?p17";
            this.db.ParameterName(db.ParamPrefics + "p17");
            //param.DbType = global::System.Data.DbType.Object;
            this.db.ParameterDbType(global::System.Data.DbType.Object);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Time;
            this.db.ParameterSqlDbType(db.GettingTime());
            //param.Size = 1024;
            this.db.ParameterSize(1024);
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "TimeGetFuelBeforeMotion";
            this.db.ParameterSourceColumn("TimeGetFuelBeforeMotion");
            //this._adapter.UpdateCommand.Parameters.Add(param);
            this.db.AdapterUpdateCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?p18";
            this.db.ParameterName(db.ParamPrefics + "p18");
            //param.DbType = global::System.Data.DbType.Object;
            this.db.ParameterDbType(global::System.Data.DbType.Object);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Time;
            this.db.ParameterSqlDbType(db.GettingTime());
            //param.Size = 1024;
            this.db.ParameterSize(1024);
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "CzMinCrossingPairTime";
            this.db.ParameterSourceColumn("CzMinCrossingPairTime");
            //this._adapter.UpdateCommand.Parameters.Add(param);
            this.db.AdapterUpdateCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?p19";
            this.db.ParameterName(db.ParamPrefics + "p19");
            //param.DbType = global::System.Data.DbType.Int32;
            this.db.ParameterDbType(global::System.Data.DbType.Int32);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType(db.GettingInt32());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "FuelerRadiusFinds";
            this.db.ParameterSourceColumn("FuelerRadiusFinds");
            //this._adapter.UpdateCommand.Parameters.Add(param);
            this.db.AdapterUpdateCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?p20";
            this.db.ParameterName(db.ParamPrefics + "p20");
            //param.DbType = global::System.Data.DbType.Object;
            this.db.ParameterDbType(global::System.Data.DbType.Object);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Time;
            this.db.ParameterSqlDbType(db.GettingTime());
            //param.Size = 1024;
            this.db.ParameterSize(1024);
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "FuelerMaxTimeStop";
            this.db.ParameterSourceColumn("FuelerMaxTimeStop");
            //this._adapter.UpdateCommand.Parameters.Add(param);
            this.db.AdapterUpdateCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?p21";
            this.db.ParameterName(db.ParamPrefics + "p21");
            //param.DbType = global::System.Data.DbType.Int32;
            this.db.ParameterDbType(global::System.Data.DbType.Int32);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType(db.GettingInt32());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "FuelerMinFuelrate";
            this.db.ParameterSourceColumn("FuelerMinFuelrate");
            //this._adapter.UpdateCommand.Parameters.Add(param);
            this.db.AdapterUpdateCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?p22";
            this.db.ParameterName(db.ParamPrefics + "p22");
            //param.DbType = global::System.Data.DbType.Int32;
            this.db.ParameterDbType(global::System.Data.DbType.Int32);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType(db.GettingInt32());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "FuelerCountInMotion";
            this.db.ParameterSourceColumn("FuelerCountInMotion");
            //this._adapter.UpdateCommand.Parameters.Add(param);
            this.db.AdapterUpdateCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();//
            this.db.NewSqlParameter();
            //param.ParameterName = "?p23";
            this.db.ParameterName(db.ParamPrefics + "p23");
            //param.DbType = global::System.Data.DbType.Int32;
            this.db.ParameterDbType(global::System.Data.DbType.Int32);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType(db.GettingInt32());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "FuelrateWithDischarge";
            this.db.ParameterSourceColumn("FuelrateWithDischarge");
            //this._adapter.UpdateCommand.Parameters.Add(param);
            this.db.AdapterUpdateCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();//
            this.db.NewSqlParameter();
            //param.ParameterName = "?p24";
            this.db.ParameterName(db.ParamPrefics + "p24");
            //param.DbType = global::System.Data.DbType.Int32;
            this.db.ParameterDbType(global::System.Data.DbType.Int32);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType(db.GettingInt32());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "FuelingMinMaxAlgorithm";
            this.db.ParameterSourceColumn("FuelingMinMaxAlgorithm");
            //this._adapter.UpdateCommand.Parameters.Add(param);
            this.db.AdapterUpdateCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?p25";
            this.db.ParameterName(db.ParamPrefics + "p25");
            //param.DbType = global::System.Data.DbType.Object;
            this.db.ParameterDbType(global::System.Data.DbType.Object);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Time;
            this.db.ParameterSqlDbType(db.GettingTime());
            //param.Size = 1024;
            this.db.ParameterSize(1024);
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "TimeBreakMaxPermitted";
            this.db.ParameterSourceColumn("TimeBreakMaxPermitted");
            //this._adapter.UpdateCommand.Parameters.Add(param);
            this.db.AdapterUpdateCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?p25";
            this.db.ParameterName(db.ParamPrefics + "p28");
            //param.DbType = global::System.Data.DbType.Object;
            this.db.ParameterDbType(global::System.Data.DbType.Object);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Time;
            this.db.ParameterSqlDbType(db.GettingTime());
            //param.Size = 1024;
            this.db.ParameterSize(1024);
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "TimeMinimMovingSize";
            this.db.ParameterSourceColumn("TimeMinimMovingSize");
            //this._adapter.UpdateCommand.Parameters.Add(param);
            this.db.AdapterUpdateCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?InclinometerMaxAngleAxisX";
            this.db.ParameterName(db.ParamPrefics + "p26");
            //param.DbType = global::System.Data.DbType.Decimal;
            this.db.ParameterDbType(global::System.Data.DbType.Double);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Double;
            this.db.ParameterSqlDbType(db.GettingDouble());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "InclinometerMaxAngleAxisX";
            this.db.ParameterSourceColumn("InclinometerMaxAngleAxisX");
            //this._adapter.InsertCommand.Parameters.Add(param);
            this.db.AdapterUpdateCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?InclinometerMaxAngleAxisY";
            this.db.ParameterName(db.ParamPrefics + "p27");
            //param.DbType = global::System.Data.DbType.Decimal;
            this.db.ParameterDbType(global::System.Data.DbType.Double);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Double;
            this.db.ParameterSqlDbType(db.GettingDouble());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "InclinometerMaxAngleAxisY";
            this.db.ParameterSourceColumn("InclinometerMaxAngleAxisY");
            //this._adapter.InsertCommand.Parameters.Add(param);
            this.db.AdapterUpdateCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?p19";
            this.db.ParameterName(db.ParamPrefics + "p29");
            //param.DbType = global::System.Data.DbType.Int32;
            this.db.ParameterDbType(global::System.Data.DbType.Int32);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType(db.GettingInt32());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(false);
            //param.SourceColumn = "FuelerRadiusFinds";
            this.db.ParameterSourceColumn("MinTimeSwitch");
            //this._adapter.UpdateCommand.Parameters.Add(param);
            this.db.AdapterUpdateCommandParametersAdd();

            // param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "?id";
            this.db.ParameterName(db.ParamPrefics + "id");
            //param.DbType = global::System.Data.DbType.Int32;
            this.db.ParameterDbType(global::System.Data.DbType.Int32);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType(db.GettingInt32());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "id";
            this.db.ParameterSourceColumn("id");
            //param.SourceVersion = global::System.Data.DataRowVersion.Original;
            this.db.ParameterSourceVersion(global::System.Data.DataRowVersion.Original);
            //this._adapter.UpdateCommand.Parameters.Add(param);
            this.db.AdapterUpdateCommandParametersAdd();
        }

        //[global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        private void InitConnection()
        {
            //this._connection = new global::MySql.Data.MySqlClient.MySqlConnection();
            this.db.AdapNewConnection();
        }

        //[global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        private void InitCommandCollection()
        {
            //this._commandCollection = new global::MySql.Data.MySqlClient.MySqlCommand[1];
            this.db.NewCommandCollection(1);
            //this._commandCollection[0] = new global::MySql.Data.MySqlClient.MySqlCommand();
            this.db.SetNewCommandCollection(0);
            //this._commandCollection[0].Connection = this.Connection;
            this.db.SetCommandCollectionConnection(this.Connection, 0);
            //this._commandCollection[0].CommandText =
            //  "SELECT band, FuelingDischarge, idleRunningMain, idleRunningAdd, FuelingEdge, \r\n" +
            //  "  RotationAdd, RotationMain, TimeBreak, id, Name, `Desc`, accelMax, \r\n" +
            //  "  breakMax, speedMax, AvgFuelRatePerHour, FuelApproximationTime, TimeGetFuelAfterStop, \r\n" +
            //  "  TimeGetFuelBeforeMotion, CzMinCrossingPairTime, \r\n" +
            //  "  FuelerRadiusFinds, FuelerMaxTimeStop, FuelerMinFuelrate, \r\n" +
            //  "  FuelerCountInMotion, FuelrateWithDischarge, FuelingMinMaxAlgorithm,TimeBreakMaxPermitted \r\n" +
            //  "FROM setting";

            this.db.SetCommandCollectionText(TrackControlQuery.SettingTableAdapter.SelectFromSetting, 0);

            //this._commandCollection[0].CommandType = global::System.Data.CommandType.Text;
            this.db.SetCommandCollectionType(global::System.Data.CommandType.Text, 0);
        }

        //[global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.ComponentModel.Design.HelpKeywordAttribute("vs.data.TableAdapter")]
        [global::System.ComponentModel.DataObjectMethodAttribute(global::System.ComponentModel.DataObjectMethodType.Fill,
            true)]
        public virtual int Fill(atlantaDataSet.settingDataTable dataTable)
        {
            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;
                adapter.SelectCommand = (MySqlCommand) this.CommandCollection[0];
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                SqlDataAdapter adapter = (SqlDataAdapter) this.Adapter;
                adapter.SelectCommand = (SqlCommand) this.CommandCollection[0];
            }

            if ((this.ClearBeforeFill == true))
            {
                dataTable.Clear();
            }

            int returnValue = 0;
            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;
                returnValue = adapter.Fill(dataTable);
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                SqlDataAdapter adapter = (SqlDataAdapter) this.Adapter;
                returnValue = adapter.Fill(dataTable);
            }

            //int returnValue = this.Adapter.Fill(dataTable);
            return returnValue;
        }

        //[global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.ComponentModel.Design.HelpKeywordAttribute("vs.data.TableAdapter")]
        [global::System.ComponentModel.DataObjectMethodAttribute(
            global::System.ComponentModel.DataObjectMethodType.Select, true)]
        public virtual atlantaDataSet.settingDataTable GetData()
        {
            //this.Adapter.SelectCommand = this.CommandCollection[0];
            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;
                adapter.SelectCommand = (MySqlCommand) this.CommandCollection[0];
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                SqlDataAdapter adapter = (SqlDataAdapter) this.Adapter;
                adapter.SelectCommand = (SqlCommand) this.CommandCollection[0];
            }
            atlantaDataSet.settingDataTable dataTable = new atlantaDataSet.settingDataTable();

            //this.Adapter.Fill(dataTable);
            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;
                adapter.Fill(dataTable);
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                SqlDataAdapter adapter = (SqlDataAdapter) this.Adapter;
                adapter.Fill(dataTable);
            }

            return dataTable;
        }

        //[global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.ComponentModel.Design.HelpKeywordAttribute("vs.data.TableAdapter")]
        public virtual int Update(atlantaDataSet.settingDataTable dataTable)
        {
            //return this.Adapter.Update(dataTable);

            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;
                return adapter.Update(dataTable);
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                SqlDataAdapter adapter = (SqlDataAdapter) this.Adapter;
                return adapter.Update(dataTable);
            }

            return 0;
        }

        //[global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.ComponentModel.Design.HelpKeywordAttribute("vs.data.TableAdapter")]
        public virtual int Update(atlantaDataSet dataSet)
        {
            // return this.Adapter.Update(dataSet, "setting");
            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;
                return adapter.Update(dataSet, "setting");
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                SqlDataAdapter adapter = (SqlDataAdapter) this.Adapter;
                return adapter.Update(dataSet, "setting");
            }

            return 0;
        } // Update

        //[global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.ComponentModel.Design.HelpKeywordAttribute("vs.data.TableAdapter")]
        public virtual int Update(global::System.Data.DataRow dataRow)
        {
            //return this.Adapter.Update(new global::System.Data.DataRow[] {dataRow});

            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;
                return adapter.Update(new global::System.Data.DataRow[] {dataRow});
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                SqlDataAdapter adapter = (SqlDataAdapter) this.Adapter;
                return adapter.Update(new global::System.Data.DataRow[] {dataRow});
            }

            return 0;
        }

        //[global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.ComponentModel.Design.HelpKeywordAttribute("vs.data.TableAdapter")]
        public virtual int Update(global::System.Data.DataRow[] dataRows)
        {
            //return this.Adapter.Update(dataRows);
            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;
                return adapter.Update(dataRows);
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                SqlDataAdapter adapter = (SqlDataAdapter) this.Adapter;
                return adapter.Update(dataRows);
            }

            return 0;
        }

        //[global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.ComponentModel.Design.HelpKeywordAttribute("vs.data.TableAdapter")]
        [global::System.ComponentModel.DataObjectMethodAttribute(
            global::System.ComponentModel.DataObjectMethodType.Delete, true)]
        public virtual int Delete(int id)
        {
            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;
                adapter.DeleteCommand.Parameters[0].Value = ((int) (id));
                global::System.Data.ConnectionState previousConnectionState = adapter.DeleteCommand.Connection.State;

                if (((adapter.DeleteCommand.Connection.State & global::System.Data.ConnectionState.Open)
                     != global::System.Data.ConnectionState.Open))
                {
                    adapter.DeleteCommand.Connection.Open();
                }

                try
                {
                    int returnValue = adapter.DeleteCommand.ExecuteNonQuery();
                    return returnValue;
                }
                finally
                {
                    if ((previousConnectionState == global::System.Data.ConnectionState.Closed))
                    {
                        adapter.DeleteCommand.Connection.Close();
                    }
                }
            } // if
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                SqlDataAdapter adapter = (SqlDataAdapter) this.Adapter;
                adapter.DeleteCommand.Parameters[0].Value = ((int) (id));
                global::System.Data.ConnectionState previousConnectionState = adapter.DeleteCommand.Connection.State;

                if (((adapter.DeleteCommand.Connection.State & global::System.Data.ConnectionState.Open)
                     != global::System.Data.ConnectionState.Open))
                {
                    adapter.DeleteCommand.Connection.Open();
                }

                try
                {
                    int returnValue = adapter.DeleteCommand.ExecuteNonQuery();
                    return returnValue;
                }
                finally
                {
                    if ((previousConnectionState == global::System.Data.ConnectionState.Closed))
                    {
                        adapter.DeleteCommand.Connection.Close();
                    }
                }
            } // else if

            return 0;
        }

        //[global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.ComponentModel.Design.HelpKeywordAttribute("vs.data.TableAdapter")]
        [global::System.ComponentModel.DataObjectMethodAttribute(
            global::System.ComponentModel.DataObjectMethodType.Insert, true)]
        public virtual int Insert(global::System.Nullable<int> band, global::System.Nullable<int> FuelingDischarge,
            global::System.Nullable<int> idleRunningMain, global::System.Nullable<int> idleRunningAdd,
            global::System.Nullable<int> FuelingEdge, global::System.Nullable<int> RotationAdd,
            global::System.Nullable<int> RotationMain, object TimeBreak, string Name, string Desc,
            global::System.Nullable<decimal> accelMax, global::System.Nullable<decimal> breakMax,
            global::System.Nullable<decimal> speedMax, global::System.Nullable<decimal> speedMin, 
            global::System.Nullable<decimal> AvgFuelRatePerHour,
            object FuelApproximationTime, object TimeGetFuelAfterStop, object TimeGetFuelBeforeMotion,
            object CzMinCrossingPairTime, global::System.Nullable<int> FuelerRadiusFinds,
            object FuelerMaxTimeStop, global::System.Nullable<int> FuelerMinFuelrate,
            global::System.Nullable<int> FuelerCountInMotion, global::System.Nullable<int> FuelrateWithDischarge,
            global::System.Nullable<int> FuelingMinMaxAlgorithm, object TimeBreakMaxPermitted, object TimeMinimMovingSize,
            global::System.Nullable<decimal> InclinometerMaxAngleAxisX, global::System.Nullable<decimal> InclinometerMaxAngleAxisY,
            global::System.Nullable<int> MinTimeSwitch)
        {
            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;

                if ((band.HasValue == true))
                {
                    adapter.InsertCommand.Parameters[0].Value = ((int) (band.Value));
                }
                else
                {
                    adapter.InsertCommand.Parameters[0].Value = global::System.DBNull.Value;
                }

                if ((FuelingDischarge.HasValue == true))
                {
                    adapter.InsertCommand.Parameters[1].Value = ((int) (FuelingDischarge.Value));
                }
                else
                {
                    adapter.InsertCommand.Parameters[1].Value = global::System.DBNull.Value;
                }
                if ((idleRunningMain.HasValue == true))
                {
                    adapter.InsertCommand.Parameters[2].Value = ((int) (idleRunningMain.Value));
                }
                else
                {
                    adapter.InsertCommand.Parameters[2].Value = global::System.DBNull.Value;
                }
                if ((idleRunningAdd.HasValue == true))
                {
                    adapter.InsertCommand.Parameters[3].Value = ((int) (idleRunningAdd.Value));
                }
                else
                {
                    adapter.InsertCommand.Parameters[3].Value = global::System.DBNull.Value;
                }
                if ((FuelingEdge.HasValue == true))
                {
                    adapter.InsertCommand.Parameters[4].Value = ((int) (FuelingEdge.Value));
                }
                else
                {
                    adapter.InsertCommand.Parameters[4].Value = global::System.DBNull.Value;
                }
                if ((RotationAdd.HasValue == true))
                {
                    adapter.InsertCommand.Parameters[5].Value = ((int) (RotationAdd.Value));
                }
                else
                {
                    adapter.InsertCommand.Parameters[5].Value = global::System.DBNull.Value;
                }
                if ((RotationMain.HasValue == true))
                {
                    adapter.InsertCommand.Parameters[6].Value = ((int) (RotationMain.Value));
                }
                else
                {
                    adapter.InsertCommand.Parameters[6].Value = global::System.DBNull.Value;
                }
                if ((TimeBreak == null))
                {
                    adapter.InsertCommand.Parameters[7].Value = global::System.DBNull.Value;
                }
                else
                {
                    adapter.InsertCommand.Parameters[7].Value = ((object) (TimeBreak));
                }
                if ((Name == null))
                {
                    adapter.InsertCommand.Parameters[8].Value = global::System.DBNull.Value;
                }
                else
                {
                    adapter.InsertCommand.Parameters[8].Value = ((string) (Name));
                }
                if ((Desc == null))
                {
                    adapter.InsertCommand.Parameters[9].Value = global::System.DBNull.Value;
                }
                else
                {
                    adapter.InsertCommand.Parameters[9].Value = ((string) (Desc));
                }
                if ((accelMax.HasValue == true))
                {
                    adapter.InsertCommand.Parameters[10].Value = ((decimal) (accelMax.Value));
                }
                else
                {
                    adapter.InsertCommand.Parameters[10].Value = global::System.DBNull.Value;
                }
                if ((breakMax.HasValue == true))
                {
                    adapter.InsertCommand.Parameters[11].Value = ((decimal) (breakMax.Value));
                }
                else
                {
                    adapter.InsertCommand.Parameters[11].Value = global::System.DBNull.Value;
                }
                if ((speedMax.HasValue == true))
                {
                    adapter.InsertCommand.Parameters[12].Value = ((decimal) (speedMax.Value));
                }
                else
                {
                    adapter.InsertCommand.Parameters[12].Value = global::System.DBNull.Value;
                }
                if( ( speedMin.HasValue == true ) )
                {
                    adapter.InsertCommand.Parameters[27].Value = ( ( decimal )( speedMin.Value ) );
                }
                else
                {
                    adapter.InsertCommand.Parameters[27].Value = 0;
                }
                if ((AvgFuelRatePerHour.HasValue == true)) //AvgFuelRatePerHour, FuelApproximationTime
                {
                    adapter.InsertCommand.Parameters[13].Value = ((decimal) (AvgFuelRatePerHour.Value));
                }
                else
                {
                    adapter.InsertCommand.Parameters[13].Value = global::System.DBNull.Value;
                }
                if ((FuelApproximationTime == null))
                {
                    adapter.InsertCommand.Parameters[14].Value = global::System.DBNull.Value;
                }
                else
                {
                    adapter.InsertCommand.Parameters[14].Value = ((object) (FuelApproximationTime));
                }
                if ((TimeGetFuelAfterStop == null))
                {
                    adapter.InsertCommand.Parameters[15].Value = global::System.DBNull.Value;
                }
                else
                {
                    adapter.InsertCommand.Parameters[15].Value = ((object) (TimeGetFuelAfterStop));
                }
                if ((TimeGetFuelBeforeMotion == null))
                {
                    adapter.InsertCommand.Parameters[16].Value = global::System.DBNull.Value;
                }
                else
                {
                    adapter.InsertCommand.Parameters[16].Value = ((object) (TimeGetFuelBeforeMotion));
                }
                if ((CzMinCrossingPairTime == null))
                {
                    adapter.InsertCommand.Parameters[17].Value = global::System.DBNull.Value;
                }
                else
                {
                    adapter.InsertCommand.Parameters[17].Value = CzMinCrossingPairTime;
                }
                if ((FuelerRadiusFinds.HasValue == true))
                {
                    adapter.InsertCommand.Parameters[18].Value = ((int) (FuelerRadiusFinds.Value));
                }
                else
                {
                    adapter.InsertCommand.Parameters[18].Value = global::System.DBNull.Value;
                }
                if ((FuelerMaxTimeStop == null))
                {
                    adapter.InsertCommand.Parameters[19].Value = global::System.DBNull.Value;
                }
                else
                {
                    adapter.InsertCommand.Parameters[19].Value = FuelerMaxTimeStop;
                }
                if ((FuelerMinFuelrate.HasValue == true))
                {
                    adapter.InsertCommand.Parameters[20].Value = ((int) (FuelerMinFuelrate.Value));
                }
                else
                {
                    adapter.InsertCommand.Parameters[20].Value = global::System.DBNull.Value;
                }
                if ((FuelerCountInMotion.HasValue == true))
                {
                    adapter.InsertCommand.Parameters[21].Value = ((int) (FuelerCountInMotion.Value));
                }
                else
                {
                    adapter.InsertCommand.Parameters[21].Value = global::System.DBNull.Value;
                }
                if ((FuelrateWithDischarge.HasValue == true))
                {
                    adapter.InsertCommand.Parameters[22].Value = ((int) (FuelrateWithDischarge.Value));
                }
                else
                {
                    adapter.InsertCommand.Parameters[22].Value = global::System.DBNull.Value;
                }
                if ((FuelingMinMaxAlgorithm.HasValue == true))
                {
                    adapter.InsertCommand.Parameters[23].Value = ((int) (FuelingMinMaxAlgorithm.Value));
                }
                else
                {
                    adapter.InsertCommand.Parameters[23].Value = global::System.DBNull.Value;
                }
                if ((TimeBreakMaxPermitted == null))
                {
                    adapter.InsertCommand.Parameters[24].Value = global::System.DBNull.Value;
                }
                else
                {
                    adapter.InsertCommand.Parameters[24].Value = ((object) (TimeBreakMaxPermitted));
                }
                if ((InclinometerMaxAngleAxisX == null))
                {
                    adapter.InsertCommand.Parameters[25].Value = global::System.DBNull.Value;
                }
                else
                {
                    adapter.InsertCommand.Parameters[25].Value = ((double) (InclinometerMaxAngleAxisX));
                }

                if ((InclinometerMaxAngleAxisY == null))
                {
                    adapter.InsertCommand.Parameters[26].Value = global::System.DBNull.Value;
                }
                else
                {
                    adapter.InsertCommand.Parameters[26].Value = ((double) (InclinometerMaxAngleAxisY));
                }
                if ((TimeMinimMovingSize == null))
                {
                    adapter.InsertCommand.Parameters[28].Value = global::System.DBNull.Value;
                }
                else
                {
                    adapter.InsertCommand.Parameters[28].Value = ((object)(TimeMinimMovingSize));
                }
                if (MinTimeSwitch == null)
                {
                    adapter.InsertCommand.Parameters[29].Value = global::System.DBNull.Value;
                }
                else
                {
                    adapter.InsertCommand.Parameters[29].Value = ((object)(MinTimeSwitch));
                }

                global::System.Data.ConnectionState previousConnectionState = adapter.InsertCommand.Connection.State;
                if (((adapter.InsertCommand.Connection.State & global::System.Data.ConnectionState.Open)
                     != global::System.Data.ConnectionState.Open))
                {
                    adapter.InsertCommand.Connection.Open();
                }
                try
                {
                    int returnValue = adapter.InsertCommand.ExecuteNonQuery();
                    return returnValue;
                }
                finally
                {
                    if ((previousConnectionState == global::System.Data.ConnectionState.Closed))
                    {
                        adapter.InsertCommand.Connection.Close();
                    }
                }
            } // if
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                SqlDataAdapter adapter = (SqlDataAdapter) this.Adapter;

                if ((band.HasValue == true))
                {
                    adapter.InsertCommand.Parameters[0].Value = ((int) (band.Value));
                }
                else
                {
                    adapter.InsertCommand.Parameters[0].Value = global::System.DBNull.Value;
                }
                if ((FuelingDischarge.HasValue == true))
                {
                    adapter.InsertCommand.Parameters[1].Value = ((int) (FuelingDischarge.Value));
                }
                else
                {
                    adapter.InsertCommand.Parameters[1].Value = global::System.DBNull.Value;
                }
                if ((idleRunningMain.HasValue == true))
                {
                    adapter.InsertCommand.Parameters[2].Value = ((int) (idleRunningMain.Value));
                }
                else
                {
                    adapter.InsertCommand.Parameters[2].Value = global::System.DBNull.Value;
                }
                if ((idleRunningAdd.HasValue == true))
                {
                    adapter.InsertCommand.Parameters[3].Value = ((int) (idleRunningAdd.Value));
                }
                else
                {
                    adapter.InsertCommand.Parameters[3].Value = global::System.DBNull.Value;
                }
                if ((FuelingEdge.HasValue == true))
                {
                    adapter.InsertCommand.Parameters[4].Value = ((int) (FuelingEdge.Value));
                }
                else
                {
                    adapter.InsertCommand.Parameters[4].Value = global::System.DBNull.Value;
                }
                if ((RotationAdd.HasValue == true))
                {
                    adapter.InsertCommand.Parameters[5].Value = ((int) (RotationAdd.Value));
                }
                else
                {
                    adapter.InsertCommand.Parameters[5].Value = global::System.DBNull.Value;
                }
                if ((RotationMain.HasValue == true))
                {
                    adapter.InsertCommand.Parameters[6].Value = ((int) (RotationMain.Value));
                }
                else
                {
                    adapter.InsertCommand.Parameters[6].Value = global::System.DBNull.Value;
                }
                if ((TimeBreak == null))
                {
                    adapter.InsertCommand.Parameters[7].Value = global::System.DBNull.Value;
                }
                else
                {
                    adapter.InsertCommand.Parameters[7].Value = ((object) (TimeBreak));
                }
                if ((Name == null))
                {
                    adapter.InsertCommand.Parameters[8].Value = global::System.DBNull.Value;
                }
                else
                {
                    adapter.InsertCommand.Parameters[8].Value = ((string) (Name));
                }
                if ((Desc == null))
                {
                    adapter.InsertCommand.Parameters[9].Value = global::System.DBNull.Value;
                }
                else
                {
                    adapter.InsertCommand.Parameters[9].Value = ((string) (Desc));
                }
                if ((accelMax.HasValue == true))
                {
                    adapter.InsertCommand.Parameters[10].Value = ((decimal) (accelMax.Value));
                }
                else
                {
                    adapter.InsertCommand.Parameters[10].Value = global::System.DBNull.Value;
                }
                if ((breakMax.HasValue == true))
                {
                    adapter.InsertCommand.Parameters[11].Value = ((decimal) (breakMax.Value));
                }
                else
                {
                    adapter.InsertCommand.Parameters[11].Value = global::System.DBNull.Value;
                }
                if ((speedMax.HasValue == true))
                {
                    adapter.InsertCommand.Parameters[12].Value = ((decimal) (speedMax.Value));
                }
                else
                {
                    adapter.InsertCommand.Parameters[12].Value = global::System.DBNull.Value;
                }
                if( ( speedMin.HasValue == true ) )
                {
                    adapter.InsertCommand.Parameters[27].Value = ( ( decimal )( speedMin.Value ) );
                }
                else
                {
                    adapter.InsertCommand.Parameters[27].Value = 0;
                }
                if ((AvgFuelRatePerHour.HasValue == true)) //AvgFuelRatePerHour, FuelApproximationTime
                {
                    adapter.InsertCommand.Parameters[13].Value = ((decimal) (AvgFuelRatePerHour.Value));
                }
                else
                {
                    adapter.InsertCommand.Parameters[13].Value = global::System.DBNull.Value;
                }
                if ((FuelApproximationTime == null))
                {
                    adapter.InsertCommand.Parameters[14].Value = global::System.DBNull.Value;
                }
                else
                {
                    adapter.InsertCommand.Parameters[14].Value = ((object) (FuelApproximationTime));
                }
                if ((TimeGetFuelAfterStop == null))
                {
                    adapter.InsertCommand.Parameters[15].Value = global::System.DBNull.Value;
                }
                else
                {
                    adapter.InsertCommand.Parameters[15].Value = ((object) (TimeGetFuelAfterStop));
                }
                if ((TimeGetFuelBeforeMotion == null))
                {
                    adapter.InsertCommand.Parameters[16].Value = global::System.DBNull.Value;
                }
                else
                {
                    adapter.InsertCommand.Parameters[16].Value = ((object) (TimeGetFuelBeforeMotion));
                }
                if ((CzMinCrossingPairTime == null))
                {
                    adapter.InsertCommand.Parameters[17].Value = global::System.DBNull.Value;
                }
                else
                {
                    adapter.InsertCommand.Parameters[17].Value = CzMinCrossingPairTime;
                }
                if ((FuelerRadiusFinds.HasValue == true))
                {
                    adapter.InsertCommand.Parameters[18].Value = ((int) (FuelerRadiusFinds.Value));
                }
                else
                {
                    adapter.InsertCommand.Parameters[18].Value = global::System.DBNull.Value;
                }
                if ((FuelerMaxTimeStop == null))
                {
                    adapter.InsertCommand.Parameters[19].Value = global::System.DBNull.Value;
                }
                else
                {
                    adapter.InsertCommand.Parameters[19].Value = FuelerMaxTimeStop;
                }
                if ((FuelerMinFuelrate.HasValue == true))
                {
                    adapter.InsertCommand.Parameters[20].Value = ((int) (FuelerMinFuelrate.Value));
                }
                else
                {
                    adapter.InsertCommand.Parameters[20].Value = global::System.DBNull.Value;
                }
                if ((FuelerCountInMotion.HasValue == true))
                {
                    adapter.InsertCommand.Parameters[21].Value = ((int) (FuelerCountInMotion.Value));
                }
                else
                {
                    adapter.InsertCommand.Parameters[21].Value = global::System.DBNull.Value;
                }
                if ((FuelrateWithDischarge.HasValue == true))
                {
                    adapter.InsertCommand.Parameters[22].Value = ((int) (FuelrateWithDischarge.Value));
                }
                else
                {
                    adapter.InsertCommand.Parameters[22].Value = global::System.DBNull.Value;
                }
                if ((FuelingMinMaxAlgorithm.HasValue == true))
                {
                    adapter.InsertCommand.Parameters[23].Value = ((int) (FuelingMinMaxAlgorithm.Value));
                }
                else
                {
                    adapter.InsertCommand.Parameters[23].Value = global::System.DBNull.Value;
                }
                if ((TimeBreakMaxPermitted == null))
                {
                    adapter.InsertCommand.Parameters[24].Value = global::System.DBNull.Value;
                }
                else
                {
                    adapter.InsertCommand.Parameters[24].Value = ((object) (TimeBreakMaxPermitted));
                }

                if ((InclinometerMaxAngleAxisX == null))
                {
                    adapter.InsertCommand.Parameters[25].Value = global::System.DBNull.Value;
                }
                else
                {
                    adapter.InsertCommand.Parameters[25].Value = ((double) (InclinometerMaxAngleAxisX));
                }

                if ((InclinometerMaxAngleAxisY == null))
                {
                    adapter.InsertCommand.Parameters[26].Value = global::System.DBNull.Value;
                }
                else
                {
                    adapter.InsertCommand.Parameters[26].Value = ((double) (InclinometerMaxAngleAxisY));
                }
                if ((TimeMinimMovingSize == null))
                {
                    adapter.InsertCommand.Parameters[28].Value = global::System.DBNull.Value;
                }
                else
                {
                    adapter.InsertCommand.Parameters[28].Value = ((object)(TimeMinimMovingSize));
                }
                if ((MinTimeSwitch == null))
                {
                    adapter.InsertCommand.Parameters[29].Value = global::System.DBNull.Value;
                }
                else
                {
                    adapter.InsertCommand.Parameters[29].Value = ((object)(MinTimeSwitch));
                }

                global::System.Data.ConnectionState previousConnectionState = adapter.InsertCommand.Connection.State;
                if (((adapter.InsertCommand.Connection.State & global::System.Data.ConnectionState.Open)
                     != global::System.Data.ConnectionState.Open))
                {
                    adapter.InsertCommand.Connection.Open();
                }
                try
                {
                    int returnValue = adapter.InsertCommand.ExecuteNonQuery();
                    return returnValue;
                } 
                finally
                {
                    if ((previousConnectionState == global::System.Data.ConnectionState.Closed))
                    {
                        adapter.InsertCommand.Connection.Close();
                    }
                }
            } // else if

            return 0;
        }

        //[global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.ComponentModel.Design.HelpKeywordAttribute("vs.data.TableAdapter")]
        [global::System.ComponentModel.DataObjectMethodAttribute(
            global::System.ComponentModel.DataObjectMethodType.Update, true)]
        public virtual int Update(object p1, global::System.Nullable<int> p2, global::System.Nullable<int> p3,
            global::System.Nullable<int> p4, global::System.Nullable<int> p5, global::System.Nullable<int> p6,
            global::System.Nullable<decimal> p7, global::System.Nullable<decimal> p8,
            global::System.Nullable<decimal> p9, global::System.Nullable<int> p10, global::System.Nullable<int> p11,
            string p12, string p13, int id, global::System.Nullable<int> p14,
            object p15, object p16, object p17, object p18,
            global::System.Nullable<int> p19, object p20, global::System.Nullable<int> p21,
            global::System.Nullable<int> p22, global::System.Nullable<int> p23, global::System.Nullable<int> p24,
            object p25, global::System.Nullable<decimal> p26, global::System.Nullable<decimal> p27, global::System.Nullable<int> p29)
        {
            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;
                if ((p1 == null))
                {
                    adapter.UpdateCommand.Parameters[0].Value = global::System.DBNull.Value;
                }
                else
                {
                    adapter.UpdateCommand.Parameters[0].Value = ((object) (p1));
                }
                if ((p2.HasValue == true))
                {
                    adapter.UpdateCommand.Parameters[1].Value = ((int) (p2.Value));
                }
                else
                {
                    adapter.UpdateCommand.Parameters[1].Value = global::System.DBNull.Value;
                }
                if ((p3.HasValue == true))
                {
                    adapter.UpdateCommand.Parameters[2].Value = ((int) (p3.Value));
                }
                else
                {
                    adapter.UpdateCommand.Parameters[2].Value = global::System.DBNull.Value;
                }
                if ((p4.HasValue == true))
                {
                    adapter.UpdateCommand.Parameters[3].Value = ((int) (p4.Value));
                }
                else
                {
                    adapter.UpdateCommand.Parameters[3].Value = global::System.DBNull.Value;
                }
                if ((p5.HasValue == true))
                {
                    adapter.UpdateCommand.Parameters[4].Value = ((int) (p5.Value));
                }
                else
                {
                    adapter.UpdateCommand.Parameters[4].Value = global::System.DBNull.Value;
                }
                if ((p6.HasValue == true))
                {
                    adapter.UpdateCommand.Parameters[5].Value = ((int) (p6.Value));
                }
                else
                {
                    adapter.UpdateCommand.Parameters[5].Value = global::System.DBNull.Value;
                }
                if ((p7.HasValue == true))
                {
                    adapter.UpdateCommand.Parameters[6].Value = ((decimal) (p7.Value));
                }
                else
                {
                    adapter.UpdateCommand.Parameters[6].Value = global::System.DBNull.Value;
                }
                if ((p8.HasValue == true))
                {
                    adapter.UpdateCommand.Parameters[7].Value = ((decimal) (p8.Value));
                }
                else
                {
                    adapter.UpdateCommand.Parameters[7].Value = global::System.DBNull.Value;
                }
                if ((p9.HasValue == true))
                {
                    adapter.UpdateCommand.Parameters[8].Value = ((decimal) (p9.Value));
                }
                else
                {
                    adapter.UpdateCommand.Parameters[8].Value = global::System.DBNull.Value;
                }
                if ((p10.HasValue == true))
                {
                    adapter.UpdateCommand.Parameters[9].Value = ((int) (p10.Value));
                }
                else
                {
                    adapter.UpdateCommand.Parameters[9].Value = global::System.DBNull.Value;
                }
                if ((p11.HasValue == true))
                {
                    adapter.UpdateCommand.Parameters[10].Value = ((int) (p11.Value));
                }
                else
                {
                    adapter.UpdateCommand.Parameters[10].Value = global::System.DBNull.Value;
                }
                if ((p12 == null))
                {
                    adapter.UpdateCommand.Parameters[11].Value = global::System.DBNull.Value;
                }
                else
                {
                    adapter.UpdateCommand.Parameters[11].Value = ((string) (p12));
                }
                if ((p13 == null))
                {
                    adapter.UpdateCommand.Parameters[12].Value = global::System.DBNull.Value;
                }
                else
                {
                    adapter.UpdateCommand.Parameters[12].Value = ((string) (p13));
                } //FuelApproximationTime
                if (p14.HasValue)
                {
                    adapter.UpdateCommand.Parameters[13].Value = ((int) (p14));
                }
                else
                {
                    adapter.UpdateCommand.Parameters[13].Value = global::System.DBNull.Value;
                }
                if ((p15 == null))
                {
                    adapter.UpdateCommand.Parameters[14].Value = global::System.DBNull.Value;
                }
                else
                {
                    adapter.UpdateCommand.Parameters[14].Value = ((object) (p15));
                }
                if ((p16 == null))
                {
                    adapter.UpdateCommand.Parameters[15].Value = global::System.DBNull.Value;
                }
                else
                {
                    adapter.UpdateCommand.Parameters[15].Value = ((object) (p16));
                }
                if ((p17 == null))
                {
                    adapter.UpdateCommand.Parameters[16].Value = global::System.DBNull.Value;
                }
                else
                {
                    adapter.UpdateCommand.Parameters[16].Value = ((object) (p17));
                }
                if ((p18 == null))
                {
                    adapter.UpdateCommand.Parameters[17].Value = global::System.DBNull.Value;
                }
                else
                {
                    adapter.UpdateCommand.Parameters[17].Value = p18;
                }
                if ((p19.HasValue == true))
                {
                    adapter.UpdateCommand.Parameters[18].Value = ((int) (p19.Value));
                }
                else
                {
                    adapter.UpdateCommand.Parameters[18].Value = global::System.DBNull.Value;
                }
                if ((p20 == null))
                {
                    adapter.UpdateCommand.Parameters[19].Value = global::System.DBNull.Value;
                }
                else
                {
                    adapter.UpdateCommand.Parameters[19].Value = p20;
                }
                if ((p21.HasValue == true))
                {
                    adapter.UpdateCommand.Parameters[20].Value = ((int) (p21.Value));
                }
                else
                {
                    adapter.UpdateCommand.Parameters[20].Value = global::System.DBNull.Value;
                }
                if ((p22.HasValue == true))
                {
                    adapter.UpdateCommand.Parameters[21].Value = ((int) (p22.Value));
                }
                else
                {
                    adapter.UpdateCommand.Parameters[21].Value = global::System.DBNull.Value;
                }
                if ((p23.HasValue == true))
                {
                    adapter.UpdateCommand.Parameters[22].Value = ((int) (p23.Value));
                }
                else
                {
                    adapter.UpdateCommand.Parameters[22].Value = global::System.DBNull.Value;
                }
                if ((p24.HasValue == true))
                {
                    adapter.UpdateCommand.Parameters[23].Value = ((int) (p24.Value));
                }
                else
                {
                    adapter.UpdateCommand.Parameters[23].Value = global::System.DBNull.Value;
                }
                if ((p25 == null))
                {
                    adapter.UpdateCommand.Parameters[24].Value = global::System.DBNull.Value;
                }
                else
                {
                    adapter.UpdateCommand.Parameters[24].Value = ((object) (p25));
                }

                if ((p26 == null))
                {
                    adapter.UpdateCommand.Parameters[25].Value = global::System.DBNull.Value;
                }
                else
                {
                    adapter.UpdateCommand.Parameters[25].Value = ((double) (p26));
                }

                if ((p27 == null))
                {
                    adapter.UpdateCommand.Parameters[26].Value = global::System.DBNull.Value;
                }
                else
                {
                    adapter.UpdateCommand.Parameters[26].Value = ((double) (p27));
                }
                if ((p29 == null))
                {
                    adapter.UpdateCommand.Parameters[27].Value = global::System.DBNull.Value;
                }
                else
                {
                    adapter.UpdateCommand.Parameters[27].Value = ((int)(p29));
                }

                adapter.UpdateCommand.Parameters[13].Value = ((int) (id));
                global::System.Data.ConnectionState previousConnectionState = adapter.UpdateCommand.Connection.State;
                if (((adapter.UpdateCommand.Connection.State & global::System.Data.ConnectionState.Open)
                     != global::System.Data.ConnectionState.Open))
                {
                    adapter.UpdateCommand.Connection.Open();
                }
                try
                {
                    int returnValue = adapter.UpdateCommand.ExecuteNonQuery();
                    return returnValue;
                }
                finally
                {
                    if ((previousConnectionState == global::System.Data.ConnectionState.Closed))
                    {
                        adapter.UpdateCommand.Connection.Close();
                    }
                }
            } // if
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                SqlDataAdapter adapter = (SqlDataAdapter) this.Adapter;

                if ((p1 == null))
                {
                    adapter.UpdateCommand.Parameters[0].Value = global::System.DBNull.Value;
                }
                else
                {
                    adapter.UpdateCommand.Parameters[0].Value = ((object) (p1));
                }
                if ((p2.HasValue == true))
                {
                    adapter.UpdateCommand.Parameters[1].Value = ((int) (p2.Value));
                }
                else
                {
                    adapter.UpdateCommand.Parameters[1].Value = global::System.DBNull.Value;
                }
                if ((p3.HasValue == true))
                {
                    adapter.UpdateCommand.Parameters[2].Value = ((int) (p3.Value));
                }
                else
                {
                    adapter.UpdateCommand.Parameters[2].Value = global::System.DBNull.Value;
                }
                if ((p4.HasValue == true))
                {
                    adapter.UpdateCommand.Parameters[3].Value = ((int) (p4.Value));
                }
                else
                {
                    adapter.UpdateCommand.Parameters[3].Value = global::System.DBNull.Value;
                }
                if ((p5.HasValue == true))
                {
                    adapter.UpdateCommand.Parameters[4].Value = ((int) (p5.Value));
                }
                else
                {
                    adapter.UpdateCommand.Parameters[4].Value = global::System.DBNull.Value;
                }
                if ((p6.HasValue == true))
                {
                    adapter.UpdateCommand.Parameters[5].Value = ((int) (p6.Value));
                }
                else
                {
                    adapter.UpdateCommand.Parameters[5].Value = global::System.DBNull.Value;
                }
                if ((p7.HasValue == true))
                {
                    adapter.UpdateCommand.Parameters[6].Value = ((decimal) (p7.Value));
                }
                else
                {
                    adapter.UpdateCommand.Parameters[6].Value = global::System.DBNull.Value;
                }
                if ((p8.HasValue == true))
                {
                    adapter.UpdateCommand.Parameters[7].Value = ((decimal) (p8.Value));
                }
                else
                {
                    adapter.UpdateCommand.Parameters[7].Value = global::System.DBNull.Value;
                }
                if ((p9.HasValue == true))
                {
                    adapter.UpdateCommand.Parameters[8].Value = ((decimal) (p9.Value));
                }
                else
                {
                    adapter.UpdateCommand.Parameters[8].Value = global::System.DBNull.Value;
                }
                if ((p10.HasValue == true))
                {
                    adapter.UpdateCommand.Parameters[9].Value = ((int) (p10.Value));
                }
                else
                {
                    adapter.UpdateCommand.Parameters[9].Value = global::System.DBNull.Value;
                }
                if ((p11.HasValue == true))
                {
                    adapter.UpdateCommand.Parameters[10].Value = ((int) (p11.Value));
                }
                else
                {
                    adapter.UpdateCommand.Parameters[10].Value = global::System.DBNull.Value;
                }
                if ((p12 == null))
                {
                    adapter.UpdateCommand.Parameters[11].Value = global::System.DBNull.Value;
                }
                else
                {
                    adapter.UpdateCommand.Parameters[11].Value = ((string) (p12));
                }
                if ((p13 == null))
                {
                    adapter.UpdateCommand.Parameters[12].Value = global::System.DBNull.Value;
                }
                else
                {
                    adapter.UpdateCommand.Parameters[12].Value = ((string) (p13));
                } //FuelApproximationTime
                if (p14.HasValue)
                {
                    adapter.UpdateCommand.Parameters[13].Value = ((int) (p14));
                }
                else
                {
                    adapter.UpdateCommand.Parameters[13].Value = global::System.DBNull.Value;
                }
                if ((p15 == null))
                {
                    adapter.UpdateCommand.Parameters[14].Value = global::System.DBNull.Value;
                }
                else
                {
                    adapter.UpdateCommand.Parameters[14].Value = ((object) (p15));
                }
                if ((p16 == null))
                {
                    adapter.UpdateCommand.Parameters[15].Value = global::System.DBNull.Value;
                }
                else
                {
                    adapter.UpdateCommand.Parameters[15].Value = ((object) (p16));
                }
                if ((p17 == null))
                {
                    adapter.UpdateCommand.Parameters[16].Value = global::System.DBNull.Value;
                }
                else
                {
                    adapter.UpdateCommand.Parameters[16].Value = ((object) (p17));
                }
                if ((p18 == null))
                {
                    adapter.UpdateCommand.Parameters[17].Value = global::System.DBNull.Value;
                }
                else
                {
                    adapter.UpdateCommand.Parameters[17].Value = p18;
                }
                if ((p19.HasValue == true))
                {
                    adapter.UpdateCommand.Parameters[18].Value = ((int) (p19.Value));
                }
                else
                {
                    adapter.UpdateCommand.Parameters[18].Value = global::System.DBNull.Value;
                }
                if ((p20 == null))
                {
                    adapter.UpdateCommand.Parameters[19].Value = global::System.DBNull.Value;
                }
                else
                {
                    adapter.UpdateCommand.Parameters[19].Value = p20;
                }
                if ((p21.HasValue == true))
                {
                    adapter.UpdateCommand.Parameters[20].Value = ((int) (p21.Value));
                }
                else
                {
                    adapter.UpdateCommand.Parameters[20].Value = global::System.DBNull.Value;
                }
                if ((p22.HasValue == true))
                {
                    adapter.UpdateCommand.Parameters[21].Value = ((int) (p22.Value));
                }
                else
                {
                    adapter.UpdateCommand.Parameters[21].Value = global::System.DBNull.Value;
                }
                if ((p23.HasValue == true))
                {
                    adapter.UpdateCommand.Parameters[22].Value = ((int) (p23.Value));
                }
                else
                {
                    adapter.UpdateCommand.Parameters[22].Value = global::System.DBNull.Value;
                }
                if ((p24.HasValue == true))
                {
                    adapter.UpdateCommand.Parameters[23].Value = ((int) (p24.Value));
                }
                else
                {
                    adapter.UpdateCommand.Parameters[23].Value = global::System.DBNull.Value;
                }
                if ((p25 == null))
                {
                    adapter.UpdateCommand.Parameters[24].Value = global::System.DBNull.Value;
                }
                else
                {
                    adapter.UpdateCommand.Parameters[24].Value = ((object) (p25));
                }

                if ((p26.HasValue == true))
                {
                    adapter.UpdateCommand.Parameters[25].Value = ((double) (p26.Value));
                }
                else
                {
                    adapter.UpdateCommand.Parameters[25].Value = global::System.DBNull.Value;
                }

                if ((p27.HasValue == true))
                {
                    adapter.UpdateCommand.Parameters[26].Value = ((double) (p27.Value));
                }
                else
                {
                    adapter.UpdateCommand.Parameters[26].Value = global::System.DBNull.Value;
                }

                if ((p29.HasValue == true))
                {
                    adapter.UpdateCommand.Parameters[27].Value = ((double)(p29.Value));
                }
                else
                {
                    adapter.UpdateCommand.Parameters[27].Value = global::System.DBNull.Value;
                }

                adapter.UpdateCommand.Parameters[13].Value = ((int) (id));
                global::System.Data.ConnectionState previousConnectionState = adapter.UpdateCommand.Connection.State;
                if (((adapter.UpdateCommand.Connection.State & global::System.Data.ConnectionState.Open)
                     != global::System.Data.ConnectionState.Open))
                {
                    adapter.UpdateCommand.Connection.Open();
                }
                try
                {
                    int returnValue = adapter.UpdateCommand.ExecuteNonQuery();
                    return returnValue;
                }
                finally
                {
                    if ((previousConnectionState == global::System.Data.ConnectionState.Closed))
                    {
                        adapter.UpdateCommand.Connection.Close();
                    }
                }
            } //  else if

            return 0;
        }
    }
}
