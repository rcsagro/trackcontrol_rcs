using System;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using TrackControl.General.DAL;
using TrackControl.General.DatabaseDriver;

namespace LocalCache.atlantaDataSetTableAdapters
{
    /// <summary>
    ///Represents the connection and commands used to retrieve and save data.
    ///</summary>
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Design.TypedDataSetGenerator", "2.0.0.0")]
    [global::System.ComponentModel.DesignerCategoryAttribute("code")]
    [global::System.ComponentModel.ToolboxItem(true)]
    [global::System.ComponentModel.DataObjectAttribute(true)]
    [global::System.ComponentModel.DesignerAttribute(
        "Microsoft.VSDesigner.DataSource.Design.TableAdapterDesigner, Microsoft.VSDesigner" +
        ", Version=8.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")]
    [global::System.ComponentModel.Design.HelpKeywordAttribute("vs.data.TableAdapter")]
    public partial class StateTableAdapter : global::System.ComponentModel.Component
    {

        //private global::MySql.Data.MySqlClient.MySqlDataAdapter _adapter;
        //private global::MySql.Data.MySqlClient.MySqlConnection _connection;
        //private global::MySql.Data.MySqlClient.MySqlCommand[] _commandCollection;

        private DriverDb db = new DriverDb();

        private bool _clearBeforeFill;

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public StateTableAdapter()
        {
            this.ClearBeforeFill = true;
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        //private global::MySql.Data.MySqlClient.MySqlDataAdapter Adapter
        private object Adapter
        {
            get
            {
                if ((this.db.Adapter == null))
                {
                    this.InitAdapter();
                }

                return this.db.Adapter;
            }
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public object Connection
        {
            get
            {
                if ((this.db.Connection == null))
                {
                    this.InitConnection();
                }

                return this.db.Connection;
            } // get
            set
            {
                this.db.Connection = value;

                if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                {
                    MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;

                    if ((adapter.InsertCommand != null))
                    {
                        adapter.InsertCommand.Connection = (MySqlConnection) value;
                    }

                    if ((adapter.DeleteCommand != null))
                    {
                        adapter.DeleteCommand.Connection = (MySqlConnection) value;
                    }

                    if ((adapter.UpdateCommand != null))
                    {
                        adapter.UpdateCommand.Connection = (MySqlConnection) value;
                    }

                    MySqlCommand[] collection = (MySqlCommand[]) this.CommandCollection;

                    for (int i = 0; (i < this.CommandCollection.Length); i = (i + 1))
                    {
                        if ((this.CommandCollection[i] != null))
                        {
                            collection[i].Connection = (MySqlConnection) value;
                        }
                    } // for
                } // if
                else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                {
                    SqlDataAdapter adapter = (SqlDataAdapter) this.Adapter;

                    if ((adapter.InsertCommand != null))
                    {
                        adapter.InsertCommand.Connection = (SqlConnection) value;
                    }

                    if ((adapter.DeleteCommand != null))
                    {
                        adapter.DeleteCommand.Connection = (SqlConnection) value;
                    }

                    if ((adapter.UpdateCommand != null))
                    {
                        adapter.UpdateCommand.Connection = (SqlConnection) value;
                    }

                    SqlCommand[] collection = (SqlCommand[]) this.CommandCollection;

                    for (int i = 0; (i < this.CommandCollection.Length); i = (i + 1))
                    {
                        if ((this.CommandCollection[i] != null))
                        {
                            collection[i].Connection = (SqlConnection) value;
                        }
                    } // for
                } // else if
            } // set
        } // Conection

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        //protected global::MySql.Data.MySqlClient.MySqlCommand[] CommandCollection
        protected object[] CommandCollection
        {
            get
            {
                if ((this.db.CommandCollection == null))
                {
                    this.InitCommandCollection();
                }

                return this.db.CommandCollection;
            }
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public bool ClearBeforeFill
        {
            get
            {
                return this._clearBeforeFill;
            }
            set
            {
                this._clearBeforeFill = value;
            }
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        private void InitAdapter()
        {
            //this._adapter = new global::MySql.Data.MySqlClient.MySqlDataAdapter();
            this.db.NewDataAdapter();
            global::System.Data.Common.DataTableMapping tableMapping = new global::System.Data.Common.DataTableMapping();
            tableMapping.SourceTable = "Table";
            tableMapping.DataSetTable = "State";
            tableMapping.ColumnMappings.Add("Id", "Id");
            tableMapping.ColumnMappings.Add("MinValue", "MinValue");
            tableMapping.ColumnMappings.Add("MaxValue", "MaxValue");
            tableMapping.ColumnMappings.Add("Title", "Title");
            tableMapping.ColumnMappings.Add("SensorId", "SensorId");
            //this._adapter.TableMappings.Add(tableMapping);
            this.db.AdapterTableMappingsAdd(tableMapping);

            //this._adapter.DeleteCommand = new global::MySql.Data.MySqlClient.MySqlCommand();
            this.db.AdapterDeleteCommand();
            //this._adapter.DeleteCommand.Connection = this.Connection;
            this.db.AdapterDeleteCommandConnection(this.Connection);
            //this._adapter.DeleteCommand.CommandText = @"DELETE FROM `State` WHERE ((`Id` = @Original_Id) AND (`MinValue` = @Original_MinValue) AND (`MaxValue` = @Original_MaxValue) AND (`Title` = @Original_Title) AND ((@IsNull_SensorId = 1 AND `SensorId` IS NULL) OR (`SensorId` = @Original_SensorId)))";

            this.db.AdapterDeleteCommandText(TrackControlQuery.StateTableAdapter.DeleteFromState);

            //this._adapter.DeleteCommand.CommandType = global::System.Data.CommandType.Text;
            this.db.AdapterDeleteCommandType(global::System.Data.CommandType.Text);

            //global::MySql.Data.MySqlClient.MySqlParameter param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "@Original_Id";
            this.db.ParameterName("@Original_Id");
            //param.DbType = global::System.Data.DbType.Int32;
            this.db.ParameterDbType(global::System.Data.DbType.Int32);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType(db.GettingInt32());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "Id";
            this.db.ParameterSourceColumn("Id");
            //param.SourceVersion = global::System.Data.DataRowVersion.Original;
            this.db.ParameterSourceVersion(global::System.Data.DataRowVersion.Original);
            //this._adapter.DeleteCommand.Parameters.Add(param);
            this.db.AdapterDeleteCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "@Original_MinValue";
            this.db.ParameterName("@Original_MinValue");
            //param.DbType = global::System.Data.DbType.Double;
            this.db.ParameterDbType(global::System.Data.DbType.Double);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Double;
            this.db.ParameterSqlDbType(db.GettingDouble());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "MinValue";
            this.db.ParameterSourceColumn("MinValue");
            //param.SourceVersion = global::System.Data.DataRowVersion.Original;
            this.db.ParameterSourceVersion(global::System.Data.DataRowVersion.Original);
            //this._adapter.DeleteCommand.Parameters.Add(param);
            this.db.AdapterDeleteCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "@Original_MaxValue";
            this.db.ParameterName("@Original_MaxValue");
            //param.DbType = global::System.Data.DbType.Double;
            this.db.ParameterDbType(global::System.Data.DbType.Double);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Double;
            this.db.ParameterSqlDbType(db.GettingDouble());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "MaxValue";
            this.db.ParameterSourceColumn("MaxValue");
            //param.SourceVersion = global::System.Data.DataRowVersion.Original;
            this.db.ParameterSourceVersion(global::System.Data.DataRowVersion.Original);
            //this._adapter.DeleteCommand.Parameters.Add(param);
            this.db.AdapterDeleteCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "@Original_Title";
            this.db.ParameterName("@Original_Title");
            //param.DbType = global::System.Data.DbType.String;
            this.db.ParameterDbType(global::System.Data.DbType.String);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.VarChar;
            this.db.ParameterSqlDbType(db.GettingVarChar());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "Title";
            this.db.ParameterSourceColumn("Title");
            //param.SourceVersion = global::System.Data.DataRowVersion.Original;
            this.db.ParameterSourceVersion(global::System.Data.DataRowVersion.Original);
            //this._adapter.DeleteCommand.Parameters.Add(param);
            this.db.AdapterDeleteCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "@IsNull_SensorId";
            this.db.ParameterName("@IsNull_SensorId");
            //param.DbType = global::System.Data.DbType.Int32;
            this.db.ParameterDbType(global::System.Data.DbType.Int32);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType(db.GettingInt32());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "SensorId";
            this.db.ParameterSourceColumn("SensorId");
            //param.SourceVersion = global::System.Data.DataRowVersion.Original;
            this.db.ParameterSourceVersion(global::System.Data.DataRowVersion.Original);
            //param.SourceColumnNullMapping = true;
            this.db.ParameterSourceColumnNullMapping(true);
            //this._adapter.DeleteCommand.Parameters.Add(param);
            this.db.AdapterDeleteCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "@Original_SensorId";
            this.db.ParameterName("@Original_SensorId");
            //param.DbType = global::System.Data.DbType.Int32;
            this.db.ParameterDbType(global::System.Data.DbType.Int32);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType(db.GettingInt32());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "SensorId";
            this.db.ParameterSourceColumn("SensorId");
            //param.SourceVersion = global::System.Data.DataRowVersion.Original;
            this.db.ParameterSourceVersion(global::System.Data.DataRowVersion.Original);
            //this._adapter.DeleteCommand.Parameters.Add(param);
            this.db.AdapterDeleteCommandParametersAdd();

            //this._adapter.InsertCommand = new global::MySql.Data.MySqlClient.MySqlCommand();
            this.db.AdapterInsertCommand();
            //this._adapter.InsertCommand.Connection = this.Connection;
            this.db.AdapterInsertCommandConnection(this.Connection);
            //this._adapter.InsertCommand.CommandText = "INSERT INTO `State` (`MinValue`, `MaxValue`, `Title`, `SensorId`) VALUES (@MinValue, @MaxValue, @Title, @SensorId); SELECT * FROM `State` WHERE Id = LAST_INSERT_ID()";

            this.db.AdapterInsertCommandText(TrackControlQuery.StateTableAdapter.InsertIntoState);

            //this._adapter.InsertCommand.CommandType = global::System.Data.CommandType.Text;
            this.db.AdapterInsertCommandType(global::System.Data.CommandType.Text);
            //this._adapter.InsertCommand.UpdatedRowSource = System.Data.UpdateRowSource.FirstReturnedRecord;
            this.db.AdapterInsertCommandUpdatedRowSource(System.Data.UpdateRowSource.FirstReturnedRecord);

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "@MinValue";
            this.db.ParameterName("@MinValue");
            //param.DbType = global::System.Data.DbType.Double;
            this.db.ParameterDbType(global::System.Data.DbType.Double);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Double;
            this.db.ParameterSqlDbType(db.GettingDouble());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "MinValue";
            this.db.ParameterSourceColumn("MinValue");
            //this._adapter.InsertCommand.Parameters.Add(param);
            this.db.AdapterInsertCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "@MaxValue";
            this.db.ParameterName("@MaxValue");
            //param.DbType = global::System.Data.DbType.Double;
            this.db.ParameterDbType(global::System.Data.DbType.Double);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Double;
            this.db.ParameterSqlDbType(db.GettingDouble());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "MaxValue";
            this.db.ParameterSourceColumn("MaxValue");
            //this._adapter.InsertCommand.Parameters.Add(param);
            this.db.AdapterInsertCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "@Title";
            this.db.ParameterName("@Title");
            //param.DbType = global::System.Data.DbType.String;
            this.db.ParameterDbType(global::System.Data.DbType.String);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.VarChar;
            this.db.ParameterSqlDbType(db.GettingVarChar());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "Title";
            this.db.ParameterSourceColumn("Title");
            //this._adapter.InsertCommand.Parameters.Add(param);
            this.db.AdapterInsertCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "@SensorId";
            this.db.ParameterName("@SensorId");
            //param.DbType = global::System.Data.DbType.Int32;
            this.db.ParameterDbType(global::System.Data.DbType.Int32);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType(db.GettingInt32());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "SensorId";
            this.db.ParameterSourceColumn("SensorId");
            //this._adapter.InsertCommand.Parameters.Add(param);
            this.db.AdapterInsertCommandParametersAdd();

            //this._adapter.UpdateCommand = new global::MySql.Data.MySqlClient.MySqlCommand();
            this.db.AdapterUpdateCommand();
            //this._adapter.UpdateCommand.Connection = this.Connection;
            this.db.AdapterUpdateCommandConnection(this.Connection);
            //this._adapter.UpdateCommand.CommandText = @"UPDATE `State` SET `MinValue` = @MinValue, `MaxValue` = @MaxValue, `Title` = @Title, `SensorId` = @SensorId WHERE ((`Id` = @Original_Id) AND (`MinValue` = @Original_MinValue) AND (`MaxValue` = @Original_MaxValue) AND (`Title` = @Original_Title) AND ((@IsNull_SensorId = 1 AND `SensorId` IS NULL) OR (`SensorId` = @Original_SensorId)))";

            this.db.AdapterUpdateCommandText(TrackControlQuery.StateTableAdapter.UpdateState);

            //this._adapter.UpdateCommand.CommandType = global::System.Data.CommandType.Text;
            this.db.AdapterUpdateCommandType(global::System.Data.CommandType.Text);

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "@MinValue";
            this.db.ParameterName("@MinValue");
            //param.DbType = global::System.Data.DbType.Double;
            this.db.ParameterDbType(global::System.Data.DbType.Double);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Double;
            this.db.ParameterSqlDbType(db.GettingDouble());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "MinValue";
            this.db.ParameterSourceColumn("MinValue");
            //this._adapter.UpdateCommand.Parameters.Add(param);
            this.db.AdapterUpdateCommandParametersAdd();

            // param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "@MaxValue";
            this.db.ParameterName("@MaxValue");
            //param.DbType = global::System.Data.DbType.Double;
            this.db.ParameterDbType(global::System.Data.DbType.Double);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Double;
            this.db.ParameterSqlDbType(db.GettingDouble());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "MaxValue";
            this.db.ParameterSourceColumn("MaxValue");
            //this._adapter.UpdateCommand.Parameters.Add(param);
            this.db.AdapterUpdateCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "@Title";
            this.db.ParameterName("@Title");
            //param.DbType = global::System.Data.DbType.String;
            this.db.ParameterDbType(global::System.Data.DbType.String);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.VarChar;
            this.db.ParameterSqlDbType(db.GettingVarChar());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "Title";
            this.db.ParameterSourceColumn("Title");
            //this._adapter.UpdateCommand.Parameters.Add(param);
            this.db.AdapterUpdateCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "@SensorId";
            this.db.ParameterName("@SensorId");
            //param.DbType = global::System.Data.DbType.Int32;
            this.db.ParameterDbType(global::System.Data.DbType.Int32);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType(db.GettingInt32());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "SensorId";
            this.db.ParameterSourceColumn("SensorId");
            //this._adapter.UpdateCommand.Parameters.Add(param);
            this.db.AdapterUpdateCommandParametersAdd();

            //param = new global::MySql.Data.MySqlClient.MySqlParameter();
            this.db.NewSqlParameter();
            //param.ParameterName = "@Original_Id";
            this.db.ParameterName("@Original_Id");
            //param.DbType = global::System.Data.DbType.Int32;
            this.db.ParameterDbType(global::System.Data.DbType.Int32);
            //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
            this.db.ParameterSqlDbType(db.GettingInt32());
            //param.IsNullable = true;
            this.db.ParameterIsNullable(true);
            //param.SourceColumn = "Id";
            this.db.ParameterSourceColumn("Id");
            //param.SourceVersion = global::System.Data.DataRowVersion.Original;
            this.db.ParameterSourceVersion(global::System.Data.DataRowVersion.Original);
            //this._adapter.UpdateCommand.Parameters.Add(param);
            this.db.AdapterUpdateCommandParametersAdd();

            ////param = new global::MySql.Data.MySqlClient.MySqlParameter();
            //this.db.NewSqlParameter();
            ////param.ParameterName = "@Original_MinValue";
            //this.db.ParameterName("@Original_MinValue");
            ////param.DbType = global::System.Data.DbType.Double;
            //this.db.ParameterDbType(global::System.Data.DbType.Double);
            ////param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Double;
            //this.db.ParameterSqlDbType(db.GettingDouble());
            ////param.IsNullable = true;
            //this.db.ParameterIsNullable(true);
            ////param.SourceColumn = "MinValue";
            //this.db.ParameterSourceColumn("MinValue");
            ////param.SourceVersion = global::System.Data.DataRowVersion.Original;
            //this.db.ParameterSourceVersion(global::System.Data.DataRowVersion.Original);
            ////this._adapter.UpdateCommand.Parameters.Add(param);
            //this.db.AdapterUpdateCommandParametersAdd();

            ////param = new global::MySql.Data.MySqlClient.MySqlParameter();
            //this.db.NewSqlParameter();
            ////param.ParameterName = "@Original_MaxValue";
            //this.db.ParameterName("@Original_MaxValue");
            ////param.DbType = global::System.Data.DbType.Double;
            //this.db.ParameterDbType(global::System.Data.DbType.Double);
            ////param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Double;
            //this.db.ParameterSqlDbType(db.GettingDouble());
            ////param.IsNullable = true;
            //this.db.ParameterIsNullable(true);
            ////param.SourceColumn = "MaxValue";
            //this.db.ParameterSourceColumn("MaxValue");
            ////param.SourceVersion = global::System.Data.DataRowVersion.Original;
            //this.db.ParameterSourceVersion(global::System.Data.DataRowVersion.Original);
            ////this._adapter.UpdateCommand.Parameters.Add(param);
            //this.db.AdapterUpdateCommandParametersAdd();

            ////param = new global::MySql.Data.MySqlClient.MySqlParameter();
            //this.db.NewSqlParameter();
            ////param.ParameterName = "@Original_Title";
            //this.db.ParameterName("@Original_Title");
            ////param.DbType = global::System.Data.DbType.String;
            //this.db.ParameterDbType(global::System.Data.DbType.String);
            ////param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.VarChar;
            //this.db.ParameterSqlDbType(db.GettingVarChar());
            ////param.IsNullable = true;
            //this.db.ParameterIsNullable(true);
            ////param.SourceColumn = "Title";
            //this.db.ParameterSourceColumn("Title");
            ////param.SourceVersion = global::System.Data.DataRowVersion.Original;
            //this.db.ParameterSourceVersion(global::System.Data.DataRowVersion.Original);
            ////this._adapter.UpdateCommand.Parameters.Add(param);
            //this.db.AdapterUpdateCommandParametersAdd();

            ////param = new global::MySql.Data.MySqlClient.MySqlParameter();
            //this.db.NewSqlParameter();
            ////param.ParameterName = "@IsNull_SensorId";
            //this.db.ParameterName("@IsNull_SensorId");
            ////param.DbType = global::System.Data.DbType.Int32;
            //this.db.ParameterDbType(global::System.Data.DbType.Int32);
            ////param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
            //this.db.ParameterSqlDbType(db.GettingInt32());
            ////param.IsNullable = true;
            //this.db.ParameterIsNullable(true);
            ////param.SourceColumn = "SensorId";
            //this.db.ParameterSourceColumn("SensorId");
            ////param.SourceVersion = global::System.Data.DataRowVersion.Original;
            //this.db.ParameterSourceVersion(global::System.Data.DataRowVersion.Original);
            ////param.SourceColumnNullMapping = true;
            //this.db.ParameterSourceColumnNullMapping(true);
            ////this._adapter.UpdateCommand.Parameters.Add(param);
            //this.db.AdapterUpdateCommandParametersAdd();

            ////param = new global::MySql.Data.MySqlClient.MySqlParameter();
            //this.db.NewSqlParameter();
            ////param.ParameterName = "@Original_SensorId";
            //this.db.ParameterName("@Original_SensorId");
            ////param.DbType = global::System.Data.DbType.Int32;
            //this.db.ParameterDbType(global::System.Data.DbType.Int32);
            ////param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.Int32;
            //this.db.ParameterSqlDbType(db.GettingInt32());
            ////param.IsNullable = true;
            //this.db.ParameterIsNullable(true);
            ////param.SourceColumn = "SensorId";
            //this.db.ParameterSourceColumn("SensorId");
            ////param.SourceVersion = global::System.Data.DataRowVersion.Original;
            //this.db.ParameterSourceVersion(global::System.Data.DataRowVersion.Original);
            ////this._adapter.UpdateCommand.Parameters.Add(param);
            //this.db.AdapterUpdateCommandParametersAdd();
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        private void InitConnection()
        {
            //this._connection = new global::MySql.Data.MySqlClient.MySqlConnection();
            this.db.AdapNewConnection();
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        private void InitCommandCollection()
        {
            //this._commandCollection = new global::MySql.Data.MySqlClient.MySqlCommand[1];
            this.db.NewCommandCollection(1);
            //this._commandCollection[0] = new global::MySql.Data.MySqlClient.MySqlCommand();
            this.db.SetNewCommandCollection(0);
            //this._commandCollection[0].Connection = this.Connection;
            this.db.SetCommandCollectionConnection(this.Connection, 0);
            //this._commandCollection[0].CommandText = "SELECT `Id`, `MinValue`, `MaxValue`, `Title`, `SensorId` FROM `State`";
            //this._commandCollection[0].CommandText = "SELECT `Id`, `MinValue`, `MaxValue`, `Title`, `SensorId` FROM `state`";

            this.db.SetCommandCollectionText(TrackControlQuery.StateTableAdapter.SelectState, 0);

            //this._commandCollection[0].CommandType = global::System.Data.CommandType.Text;
            this.db.SetCommandCollectionType(global::System.Data.CommandType.Text, 0);
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.ComponentModel.Design.HelpKeywordAttribute("vs.data.TableAdapter")]
        [global::System.ComponentModel.DataObjectMethodAttribute(global::System.ComponentModel.DataObjectMethodType.Fill,
            true)]

        public virtual int Fill(atlantaDataSet.StateDataTable dataTable)
        {
            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;
                adapter.SelectCommand = (MySqlCommand) this.CommandCollection[0];
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                SqlDataAdapter adapter = (SqlDataAdapter) this.Adapter;
                adapter.SelectCommand = (SqlCommand) this.CommandCollection[0];
            }

            if ((this.ClearBeforeFill == true))
            {
                dataTable.Clear();
            }

            int returnValue = 0; // this.DataAdapter.Fill( dataTable );
            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;
                returnValue = adapter.Fill(dataTable);
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                SqlDataAdapter adapter = (SqlDataAdapter) this.Adapter;
                returnValue = adapter.Fill(dataTable);
            }

            return returnValue;
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.ComponentModel.Design.HelpKeywordAttribute("vs.data.TableAdapter")]
        [global::System.ComponentModel.DataObjectMethodAttribute(
            global::System.ComponentModel.DataObjectMethodType.Select, true)]

        public virtual atlantaDataSet.StateDataTable GetData()
        {
            //this.DataAdapter.SelectCommand = this.CommandCollection[0];
            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;
                adapter.SelectCommand = (MySqlCommand) this.CommandCollection[0];
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                SqlDataAdapter adapter = (SqlDataAdapter) this.Adapter;
                adapter.SelectCommand = (SqlCommand) this.CommandCollection[0];
            }

            atlantaDataSet.StateDataTable dataTable = new atlantaDataSet.StateDataTable();

            //this.DataAdapter.Fill(dataTable);
            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;
                adapter.Fill(dataTable);
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                SqlDataAdapter adapter = (SqlDataAdapter) this.Adapter;
                adapter.Fill(dataTable);
            }

            return dataTable;
        }

        //[global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.ComponentModel.Design.HelpKeywordAttribute("vs.data.TableAdapter")]

        public virtual int Update(atlantaDataSet.StateDataTable dataTable)
        {
            //return this.DataAdapter.Update(dataTable);
            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;
                return adapter.Update(dataTable);
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                SqlDataAdapter adapter = (SqlDataAdapter) this.Adapter;
                return adapter.Update(dataTable);
            }

            return 0;
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.ComponentModel.Design.HelpKeywordAttribute("vs.data.TableAdapter")]

        public virtual int Update(atlantaDataSet dataSet)
        {
            //return this.DataAdapter.Update(dataSet, "State");

            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;
                return adapter.Update(dataSet, "State");
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                SqlDataAdapter adapter = (SqlDataAdapter) this.Adapter;
                return adapter.Update(dataSet, "State");
            }

            return 0;
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.ComponentModel.Design.HelpKeywordAttribute("vs.data.TableAdapter")]

        public virtual int Update(global::System.Data.DataRow dataRow)
        {
            //return this.DataAdapter.Update(new global::System.Data.DataRow[] {dataRow});
            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;
                return adapter.Update(new global::System.Data.DataRow[] {dataRow});
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                SqlDataAdapter adapter = (SqlDataAdapter) this.Adapter;
                return adapter.Update(new global::System.Data.DataRow[] {dataRow});
            }

            return 0;
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.ComponentModel.Design.HelpKeywordAttribute("vs.data.TableAdapter")]

        public virtual int Update(global::System.Data.DataRow[] dataRows)
        {
            //return this.DataAdapter.Update(dataRows);

            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;
                return adapter.Update(dataRows);
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                SqlDataAdapter adapter = (SqlDataAdapter) this.Adapter;
                return adapter.Update(dataRows);
            }

            return 0;
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.ComponentModel.Design.HelpKeywordAttribute("vs.data.TableAdapter")]
        [global::System.ComponentModel.DataObjectMethodAttribute(
            global::System.ComponentModel.DataObjectMethodType.Delete, true)]

        public virtual int Delete(int Original_Id, double Original_MinValue, double Original_MaxValue,
            string Original_Title, global::System.Nullable<int> Original_SensorId)
        {
            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;

                adapter.DeleteCommand.Parameters[0].Value = ((int) (Original_Id));
                adapter.DeleteCommand.Parameters[1].Value = ((double) (Original_MinValue));
                adapter.DeleteCommand.Parameters[2].Value = ((double) (Original_MaxValue));

                if ((Original_Title == null))
                {
                    throw new global::System.ArgumentNullException("Original_Title");
                }
                else
                {
                    adapter.DeleteCommand.Parameters[3].Value = ((string) (Original_Title));
                }

                if ((Original_SensorId.HasValue == true))
                {
                    adapter.DeleteCommand.Parameters[4].Value = ((object) (0));
                    adapter.DeleteCommand.Parameters[5].Value = ((int) (Original_SensorId.Value));
                }
                else
                {
                    adapter.DeleteCommand.Parameters[4].Value = ((object) (1));
                    adapter.DeleteCommand.Parameters[5].Value = global::System.DBNull.Value;
                }

                global::System.Data.ConnectionState previousConnectionState =
                    adapter.DeleteCommand.Connection.State;

                if (((adapter.DeleteCommand.Connection.State & global::System.Data.ConnectionState.Open)
                     != global::System.Data.ConnectionState.Open))
                {
                    adapter.DeleteCommand.Connection.Open();
                }
                try
                {
                    int returnValue = adapter.DeleteCommand.ExecuteNonQuery();
                    return returnValue;
                }
                finally
                {
                    if ((previousConnectionState == global::System.Data.ConnectionState.Closed))
                    {
                        adapter.DeleteCommand.Connection.Close();
                    }
                }
            } // if
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                SqlDataAdapter adapter = (SqlDataAdapter) this.Adapter;

                adapter.DeleteCommand.Parameters[0].Value = ((int) (Original_Id));
                adapter.DeleteCommand.Parameters[1].Value = ((double) (Original_MinValue));
                adapter.DeleteCommand.Parameters[2].Value = ((double) (Original_MaxValue));

                if ((Original_Title == null))
                {
                    throw new global::System.ArgumentNullException("Original_Title");
                }
                else
                {
                    adapter.DeleteCommand.Parameters[3].Value = ((string) (Original_Title));
                }

                if ((Original_SensorId.HasValue == true))
                {
                    adapter.DeleteCommand.Parameters[4].Value = ((object) (0));
                    adapter.DeleteCommand.Parameters[5].Value = ((int) (Original_SensorId.Value));
                }
                else
                {
                    adapter.DeleteCommand.Parameters[4].Value = ((object) (1));
                    adapter.DeleteCommand.Parameters[5].Value = global::System.DBNull.Value;
                }

                global::System.Data.ConnectionState previousConnectionState =
                    adapter.DeleteCommand.Connection.State;

                if (((adapter.DeleteCommand.Connection.State & global::System.Data.ConnectionState.Open)
                     != global::System.Data.ConnectionState.Open))
                {
                    adapter.DeleteCommand.Connection.Open();
                }
                try
                {
                    int returnValue = adapter.DeleteCommand.ExecuteNonQuery();
                    return returnValue;
                }
                finally
                {
                    if ((previousConnectionState == global::System.Data.ConnectionState.Closed))
                    {
                        adapter.DeleteCommand.Connection.Close();
                    }
                }
            } // else if
            return 0;
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.ComponentModel.Design.HelpKeywordAttribute("vs.data.TableAdapter")]
        [global::System.ComponentModel.DataObjectMethodAttribute(
            global::System.ComponentModel.DataObjectMethodType.Insert, true)]

        public virtual int Insert(double MinValue, double MaxValue, string Title, global::System.Nullable<int> SensorId)
        {
            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;

                adapter.InsertCommand.Parameters[0].Value = ((double) (MinValue));
                adapter.InsertCommand.Parameters[1].Value = ((double) (MaxValue));

                if ((Title == null))
                {
                    throw new global::System.ArgumentNullException("Title");
                }
                else
                {
                    adapter.InsertCommand.Parameters[2].Value = ((string) (Title));
                }
                if ((SensorId.HasValue == true))
                {
                    adapter.InsertCommand.Parameters[3].Value = ((int) (SensorId.Value));
                }
                else
                {
                    adapter.InsertCommand.Parameters[3].Value = global::System.DBNull.Value;
                }

                global::System.Data.ConnectionState previousConnectionState = adapter.InsertCommand.Connection.State;

                if (((adapter.InsertCommand.Connection.State & global::System.Data.ConnectionState.Open)
                     != global::System.Data.ConnectionState.Open))
                {
                    adapter.InsertCommand.Connection.Open();
                }
                try
                {
                    int returnValue = adapter.InsertCommand.ExecuteNonQuery();
                    return returnValue;
                }
                finally
                {
                    if ((previousConnectionState == global::System.Data.ConnectionState.Closed))
                    {
                        adapter.InsertCommand.Connection.Close();
                    }
                }
            } // if
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                SqlDataAdapter adapter = (SqlDataAdapter) this.Adapter;

                adapter.InsertCommand.Parameters[0].Value = ((double) (MinValue));
                adapter.InsertCommand.Parameters[1].Value = ((double) (MaxValue));

                if ((Title == null))
                {
                    throw new global::System.ArgumentNullException("Title");
                }
                else
                {
                    adapter.InsertCommand.Parameters[2].Value = ((string) (Title));
                }
                if ((SensorId.HasValue == true))
                {
                    adapter.InsertCommand.Parameters[3].Value = ((int) (SensorId.Value));
                }
                else
                {
                    adapter.InsertCommand.Parameters[3].Value = global::System.DBNull.Value;
                }

                global::System.Data.ConnectionState previousConnectionState = adapter.InsertCommand.Connection.State;

                if (((adapter.InsertCommand.Connection.State & global::System.Data.ConnectionState.Open)
                     != global::System.Data.ConnectionState.Open))
                {
                    adapter.InsertCommand.Connection.Open();
                }
                try
                {
                    int returnValue = adapter.InsertCommand.ExecuteNonQuery();
                    return returnValue;
                }
                finally
                {
                    if ((previousConnectionState == global::System.Data.ConnectionState.Closed))
                    {
                        adapter.InsertCommand.Connection.Close();
                    }
                }
            } // else if
            return 0;
        }

        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.ComponentModel.Design.HelpKeywordAttribute("vs.data.TableAdapter")]
        [global::System.ComponentModel.DataObjectMethodAttribute(
            global::System.ComponentModel.DataObjectMethodType.Update, true)]
        public virtual int Update(double MinValue, double MaxValue, string Title, global::System.Nullable<int> SensorId,
            int Original_Id, double Original_MinValue, double Original_MaxValue, string Original_Title,
            global::System.Nullable<int> Original_SensorId)
        {
            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;

                adapter.UpdateCommand.Parameters[0].Value = ((double) (MinValue));
                adapter.UpdateCommand.Parameters[1].Value = ((double) (MaxValue));
                if ((Title == null))
                {
                    throw new global::System.ArgumentNullException("Title");
                }
                else
                {
                    adapter.UpdateCommand.Parameters[2].Value = ((string) (Title));
                }
                if ((SensorId.HasValue == true))
                {
                    adapter.UpdateCommand.Parameters[3].Value = ((int) (SensorId.Value));
                }
                else
                {
                    adapter.UpdateCommand.Parameters[3].Value = global::System.DBNull.Value;
                }

                adapter.UpdateCommand.Parameters[4].Value = ((int) (Original_Id));
                adapter.UpdateCommand.Parameters[5].Value = ((double) (Original_MinValue));
                adapter.UpdateCommand.Parameters[6].Value = ((double) (Original_MaxValue));

                if ((Original_Title == null))
                {
                    throw new global::System.ArgumentNullException("Original_Title");
                }
                else
                {
                    adapter.UpdateCommand.Parameters[7].Value = ((string) (Original_Title));
                }
                if ((Original_SensorId.HasValue == true))
                {
                    adapter.UpdateCommand.Parameters[8].Value = ((object) (0));
                    adapter.UpdateCommand.Parameters[9].Value = ((int) (Original_SensorId.Value));
                }
                else
                {
                    adapter.UpdateCommand.Parameters[8].Value = ((object) (1));
                    adapter.UpdateCommand.Parameters[9].Value = global::System.DBNull.Value;
                }

                global::System.Data.ConnectionState previousConnectionState = adapter.UpdateCommand.Connection.State;

                if (((adapter.UpdateCommand.Connection.State & global::System.Data.ConnectionState.Open)
                     != global::System.Data.ConnectionState.Open))
                {
                    adapter.UpdateCommand.Connection.Open();
                }
                try
                {
                    int returnValue = adapter.UpdateCommand.ExecuteNonQuery();
                    return returnValue;
                }
                finally
                {
                    if ((previousConnectionState == global::System.Data.ConnectionState.Closed))
                    {
                        adapter.UpdateCommand.Connection.Close();
                    }
                }
            } // if
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                SqlDataAdapter adapter = (SqlDataAdapter) this.Adapter;

                adapter.UpdateCommand.Parameters[0].Value = ((double) (MinValue));
                adapter.UpdateCommand.Parameters[1].Value = ((double) (MaxValue));
                if ((Title == null))
                {
                    throw new global::System.ArgumentNullException("Title");
                }
                else
                {
                    adapter.UpdateCommand.Parameters[2].Value = ((string) (Title));
                }
                if ((SensorId.HasValue == true))
                {
                    adapter.UpdateCommand.Parameters[3].Value = ((int) (SensorId.Value));
                }
                else
                {
                    adapter.UpdateCommand.Parameters[3].Value = global::System.DBNull.Value;
                }

                adapter.UpdateCommand.Parameters[4].Value = ((int) (Original_Id));
                adapter.UpdateCommand.Parameters[5].Value = ((double) (Original_MinValue));
                adapter.UpdateCommand.Parameters[6].Value = ((double) (Original_MaxValue));

                if ((Original_Title == null))
                {
                    throw new global::System.ArgumentNullException("Original_Title");
                }
                else
                {
                    adapter.UpdateCommand.Parameters[7].Value = ((string) (Original_Title));
                }
                if ((Original_SensorId.HasValue == true))
                {
                    adapter.UpdateCommand.Parameters[8].Value = ((object) (0));
                    adapter.UpdateCommand.Parameters[9].Value = ((int) (Original_SensorId.Value));
                }
                else
                {
                    adapter.UpdateCommand.Parameters[8].Value = ((object) (1));
                    adapter.UpdateCommand.Parameters[9].Value = global::System.DBNull.Value;
                }

                global::System.Data.ConnectionState previousConnectionState = adapter.UpdateCommand.Connection.State;

                if (((adapter.UpdateCommand.Connection.State & global::System.Data.ConnectionState.Open)
                     != global::System.Data.ConnectionState.Open))
                {
                    adapter.UpdateCommand.Connection.Open();
                }
                try
                {
                    int returnValue = adapter.UpdateCommand.ExecuteNonQuery();
                    return returnValue;
                }
                finally
                {
                    if ((previousConnectionState == global::System.Data.ConnectionState.Closed))
                    {
                        adapter.UpdateCommand.Connection.Close();
                    }
                }
            } // else if

            return 0;
        }
    }
}
