using System;
using System.Collections;
using System.Windows.Forms;
using LocalCache;
using LocalCache.Properties;
using LocalCache.atlantaDataSetTableAdapters;
using TrackControl.Reports;
using TrackControl.Vehicles;
using TrackControl;

namespace LocalCache
{
	/// <summary>
	/// ���������� � ������������ ��������
	/// </summary>
	public class VehicleInfo
	{
		#region Fields

		/// <summary>
		/// ������� ��������
		/// </summary>
		private string _driverName = Resources.LocalCacheSqlNotData;

		/// <summary>
		/// ������� � ��� ��������
		/// </summary>
		private string _driverFullName = Resources.LocalCacheSqlNotData;

        /// <summary>
        /// �������� �������� ������������� �������
        /// </summary>
        private string _aggregateName = Resources.LocalCacheSqlNotData;

		/// <summary>
		/// ������������� ������������� ��������
		/// </summary>
		private string _carMaker = "";

		/// <summary>
		/// ������ ������������� ��������
		/// </summary>
		private string _carModel = "";

		/// <summary>
		/// ��������������� ����� ������������� ��������
		/// </summary>
		private string _registrationNumber = "";

		/// <summary>
		/// ���������� ���������� � ������������ ��������
		/// </summary>
		private string _info = "";

		/// <summary>
		/// ��� ������� ���� � ������������ ��������
		/// </summary>
		private string _outlinkid = "";

		/// <summary>
		/// ��� ������������� �������
		/// </summary>
		private string _typefuel = "";

		/// <summary>
		/// ��� ������� ���� ��� �������
		/// </summary>
		private string _fueloutlinkid = "";

		/// <summary>
		/// �������� ������, � ������� ������ ������ ������������ ��������
		/// </summary>
		private string _group = "";

	    private int _identification;
	    private string _devidshort;

        #endregion

		#region .ctor

		/// <summary>
		/// ������� � �������������� ����� ��������� ������ LocalCache.VehicleInfo
		/// </summary>
		/// <param name="m_row">������� ��������</param>
		public VehicleInfo(atlantaDataSet.mobitelsRow mobitel)
		{
			if (mobitel == null)
				return;
			_info = mobitel.Name;
			_registrationNumber = mobitel.Name;
			atlantaDataSet.vehicleRow[] vehicle = mobitel.GetvehicleRows();
			if (vehicle != null && vehicle.Length > 0) {
				_carMaker = vehicle[0].MakeCar;
				_carModel = vehicle[0].CarModel;
				_outlinkid = vehicle[0].OutLinkId;

				if (vehicle[0].NumberPlate.Trim().Length > 0)
					_registrationNumber = vehicle[0].NumberPlate;

				if (vehicle[0].driverRow != null) {
					_driverName = vehicle[0].driverRow.Family;
					_driverFullName = String.Format("{0} {1}", vehicle[0].driverRow.Family, vehicle[0].driverRow.Name);
				}

				if (vehicle[0].teamRow != null) 
                {
					_group = vehicle[0].teamRow.Name;
				}
			}
		}

        public void AggregateFromTable(atlantaDataSet.mobitelsRow mobitel, Vehicle vh)
        {
            if (mobitel == null)
                return;
            if (vh == null)
                return;

            vh.VehicleInfoAggregate(mobitel.Mobitel_ID);
            _aggregateName = vh.Aggregate.AggregateName;
        }

        public void DriverFromTable(atlantaDataSet.mobitelsRow mobitel)
        {
            if (mobitel == null)
                return;
            _info = mobitel.Name;
            _registrationNumber = mobitel.Name;
            atlantaDataSet.vehicleRow[] vehicle = mobitel.GetvehicleRows();
            if (vehicle != null && vehicle.Length > 0)
            {
                _carMaker = vehicle[0].MakeCar;
                _carModel = vehicle[0].CarModel;
                _outlinkid = vehicle[0].OutLinkId;

                if (vehicle[0].NumberPlate.Trim().Length > 0)
                    _registrationNumber = vehicle[0].NumberPlate;

                if (vehicle[0].driverRow != null)
                {
                    _driverName = vehicle[0].driverRow.Family;
                    _driverFullName = String.Format("{0} {1}", vehicle[0].driverRow.Family, vehicle[0].driverRow.Name);
                }

                if (vehicle[0].teamRow != null)
                {
                    _group = vehicle[0].teamRow.Name;
                }
            }
        }

        public VehicleInfo(int mobitelId, atlantaDataSet.mobitelsRow mobitel)
        {
            ulong rfid = GetActualRfidNumberDriver(mobitel);
            UInt16? rfidAgregat = GetActualRfidNumberAggregate(mobitel);
            Vehicle veh = new Vehicle(mobitelId, rfid, rfidAgregat);

            _carMaker = veh.CarMaker;
            _carModel = veh.CarModel;
            _outlinkid = veh.IdOutLink;
            _typefuel = veh.TypeFuel;
            _fueloutlinkid = veh.IdOutLinkFuel;
            _registrationNumber = veh.RegNumber;
            _identification = veh.Identifier;
            _devidshort = veh.DevIdShort;

            if (rfidAgregat == 1023)
            {
                _aggregateName = Resources.NotCard1023;
            }
            else if (rfidAgregat == 0)
            {
                AggregateFromTable(mobitel, veh);
            }
            else
            {
                _aggregateName = veh.Aggregate.AggregateName;
            }



            if (rfid == 1023)
            {
                _driverName = Resources.NotCard1023;
                _driverFullName = Resources.NotCard1023;
            }
            else if (rfid == 0)
            {
                DriverFromTable(mobitel);
                if (veh.Driver != null)
                {
                    if (veh.Driver.FullName == " ")
                    {
                        _driverName = Resources.NotData;
                        _driverFullName = Resources.NotData;
                    }
                    else
                    {
                        _driverName = veh.Driver.FullName;
                        _driverFullName = veh.Driver.FullName;
                    }
                }
            }
            else
            {
                _driverName = veh.Driver.FullName;
                _driverFullName = veh.Driver.FullName;
            }

            if (veh.Group != null)
                _group = veh.Group.Name;
            _info = veh.Info;
        }

        public VehicleInfo(int mobitelId)
		{
            Vehicle veh = new Vehicle(mobitelId);

            _carMaker = veh.CarMaker;
            _carModel = veh.CarModel;
            _outlinkid = veh.IdOutLink;
            _typefuel = veh.TypeFuel;
            _fueloutlinkid = veh.IdOutLinkFuel;
            _registrationNumber = veh.RegNumber;

            if (veh.Group != null)
                _group = veh.Group.Name;
            _info = veh.Info;
		}

		#endregion

		#region Properties

	    public string DevIdShort
	    {
            get { return _devidshort; }
	    }

        /// <summary>
        /// ������������� ������������� ��������.
        /// </summary>
        public int Identificator
        {
            get { return _identification; }
        }

		/// <summary>
		/// ������� ��������.
		/// </summary>
		public string DriverName {
			get { return _driverName; }
		}

		/// <summary>
		/// ������� � ��� ��������.
		/// </summary>
		public string DriverFullName {
			get {
				if (_driverFullName != null)
					return _driverFullName;
				else {
					return "";
				}
			}
		}

        /// <summary>
        /// �������� �������� ������������� ��������
        /// </summary>
        public string AggregateName
        {
            get
            {
                if (_aggregateName != null)
                    return _aggregateName;
                else
                {
                    return "";
                }
            }
        }

		/// <summary>
		/// ������������� ������������� ��������.
		/// </summary>
		public string CarMaker {
			get { return _carMaker; }
		}

		/// <summary>
		/// ��� ������� ���� ������������� ��������.
		/// </summary>
		public string OutLinkId {
			get { return _outlinkid; }
		}

		/// <summary>
		/// ��� ������� ���� ���� �������
		/// </summary>
		public string OutLinkIdFuel {
			get { return _fueloutlinkid; }
		}

		/// <summary>
		/// ��� ������������� �������
		/// </summary>
		public string TypeFuel {
			get { return _typefuel; }
		}

		/// <summary>
		/// ������ ������������� ��������.
		/// </summary>
		public string CarModel {
			get { return _carModel; }
		}

		/// <summary>
		/// ��������������� ����� ������������� ��������.
		/// </summary>
		public string RegistrationNumber {
			get { return _registrationNumber; }
		}

		/// <summary>
		/// ���������� ���������� � ������������ ��������.
		/// </summary>
		public string Info {
			get {
				if (_carMaker != null) {
					if (_carMaker.Length > 0) {
						return String.Format("{0} \"{1} {2}\"",
							_registrationNumber,
							_carMaker,
							_carModel);
					}

					return _info;
				}
				return "";
			}
		}

		/// <summary>
		/// �������� ������, � ������� ��������� ������ ������������ ��������
		/// </summary>
		public string Group {
			get { return _group; }
		}

        public static ulong ulongSector(byte[] sensors, long length, long position)
        {
            try
            {
                if (length < 0 || sensors == null)
                {
                    return 0;
                }
            
                if (position < 0)
                {
                    return 0;
                }
              
                ulong res = 0;
                BitArray bits = new BitArray(sensors);
                for (int i = 0; i < length; i++)
                {
                    try
                    {
                        ulong d = Convert.ToUInt64(bits[i + (int)position]);
                        res += d << i;
                    }
                    catch(Exception ex)
                    {
                        ;
                    }
                }

                return res;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message + '\n' + e.StackTrace, "Error calibrate", MessageBoxButtons.OK);
                return 0;
            }
        }

        private ulong GetActualRfidNumberDriver(atlantaDataSet.mobitelsRow mobitel)
        {
            try
            {
                atlantaDataSet.sensorsRow[] sensors = mobitel.GetsensorsRows();

                if (sensors.Length > 0)
                {
                    for (int i = 0; i < sensors.Length; i++)
                    {
                        atlantaDataSet.sensorsRow sensor = sensors[i];
                        if (sensor.Length > 1)
                        {
                            atlantaDataSet.relationalgorithmsRow[] algs = sensor.GetrelationalgorithmsRows();
                            if (null != algs)
                            {
                                if (algs.Length > 0)
                                {
                                    if (algs[0].AlgorithmID == (int) AlgorithmType.DRIVER)
                                    {
                                        atlantaDataSet.dataviewRow[] dataview = mobitel.GetdataviewRows();
                                        if (dataview.Length > 0)
                                        {
                                            ulong val = dataview[dataview.Length - 1].sensor;
                                            byte[] bytes = BitConverter.GetBytes(val);
                                            UInt16 valrfid =
                                                Convert.ToUInt16(ulongSector(bytes, sensor.Length, sensor.StartBit));
                                            return valrfid;
                                        }
                                    } // if
                                } // if
                            } // if
                        } // if
                    } // for
                } // if
            } // try
            catch (Exception e)
            {
                //MessageBox.Show(e.Message + "\n" + e.StackTrace, "Error GetActualRfidNumberDriver",
                //   MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return 0;
		} // GetActualRfidNumberDriver

        private UInt16? GetActualRfidNumberAggregate(atlantaDataSet.mobitelsRow mobitel)
        {
            try
            {
                atlantaDataSet.sensorsRow[] sensors = mobitel.GetsensorsRows();

                if (sensors.Length > 0)
                {
                    for (int i = 0; i < sensors.Length; i++)
                    {
                        atlantaDataSet.sensorsRow sensor = sensors[i];
                        if (sensor.Length > 1)
                        {
                            atlantaDataSet.relationalgorithmsRow[] algs = sensor.GetrelationalgorithmsRows();
                            if (null != algs)
                            {
                                if (algs.Length > 0)
                                {
                                    if (algs[0].AlgorithmID == (int)AlgorithmType.AGREGAT)
                                    {
                                        atlantaDataSet.dataviewRow[] dataview = mobitel.GetdataviewRows();
                                        if (dataview.Length > 0)
                                        {
                                            ulong val = dataview[dataview.Length - 1].sensor;
                                            byte[] bytes = BitConverter.GetBytes(val);
                                            UInt16 valrfid =
                                                Convert.ToUInt16(ulongSector(bytes, sensor.Length, sensor.StartBit));
                                            return valrfid;
                                        }
                                    } // if
                                } // if
                            } // if
                        } // if
                    } // for
                } // if
            } // try
            catch (Exception e)
            {
                //MessageBox.Show(e.Message + "\n" + e.StackTrace, "Error GetActualRfidNumberDriver",
                //   MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return 0;
        } // GetActualRfidNumberAggergate

		#endregion
	}
}
