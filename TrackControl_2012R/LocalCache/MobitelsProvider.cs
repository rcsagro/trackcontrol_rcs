using System;
using System.Data;
using MySql.Data.MySqlClient;
using LocalCache.Properties;
using TrackControl.General;
using TrackControl.General.DAL;
using TrackControl.General.DatabaseDriver;

namespace LocalCache
{
    public class MobitelsProvider
    {
        private const string GET_ALL_MOBITELS = @"
SELECT
  m.mobitel_id,
  m.Name,
  m.Descr,
  COALESCE(imc.devIdShort, 'No Data') AS devIdShort,
  COALESCE(tel.TelNumber, 'No Data') AS TelNumber,ls.Color,From_UnixTime(m.LastInsertTime) as LastTimeGps
FROM mobitels m
  INNER JOIN servicesend srvs ON srvs.ServiceSend_ID = (
    SELECT MAX(ServiceSend_ID)
    FROM servicesend
    WHERE ID = m.servicesend_id)
  LEFT JOIN internalmobitelconfig imc ON imc.InternalMobitelConfig_ID = (
    SELECT MAX(InternalMobitelConfig_ID)
    FROM internalmobitelconfig
    WHERE ID = m.InternalMobitelConfig_ID)
  LEFT JOIN telnumbers tel ON (srvs.telMobitel = tel.TelNumber_ID)
  LEFT OUTER JOIN `lines` ls
   ON (ls.Mobitel_ID = m.Mobitel_ID)";

        private const string msGET_ALL_MOBITELS = @"SELECT m.mobitel_id
     , m.Name
     , m.Descr
     , coalesce(imc.devIdShort, 'No Data') AS devIdShort
     , coalesce(tel.TelNumber, 'No Data') AS TelNumber
     , ls.Color
     , dbo.From_UnixTime(m.LastInsertTime) AS LastTimeGps
FROM
  mobitels m
  INNER JOIN servicesend srvs
    ON srvs.ServiceSend_ID = (SELECT max(ServiceSend_ID)
                              FROM
                                servicesend
                              WHERE
                                ID = m.servicesend_id)
  LEFT JOIN internalmobitelconfig imc
    ON imc.InternalMobitelConfig_ID = (SELECT max(InternalMobitelConfig_ID)
                                       FROM
                                         internalmobitelconfig
                                       WHERE
                                         ID = m.InternalMobitelConfig_ID)
  LEFT JOIN telnumbers tel
    ON (srvs.telMobitel = tel.TelNumber_ID)
  LEFT OUTER JOIN lines ls
    ON (ls.Mobitel_ID = m.Mobitel_ID)";

        private string _cs; // ������ ����������� � ��

        /// <summary>
        /// ������� � �������������� ����� ��������� ������ LocalCache.MobitelsProvider
        /// </summary>
        /// <param name="cs"></param>
        public MobitelsProvider(string cs)
        {
            _cs = cs;
        }

        public void FillMobitelsTable(atlantaDataSet.mobitelsDataTable mobitels)
        {
            mobitels.Clear();
            //using (MySqlCommand command = new MySqlCommand(GET_ALL_MOBITELS))
            {
                DriverDb db = new DriverDb();

               db.NewSqlCommand(TrackControlQuery.MobitelsProvider.SelectMobitel);
                
                //using (MySqlConnection connection = new MySqlConnection(_cs))
                {
                    //command.Connection = connection;
                    db.NewSqlConnection(_cs);
                    db.CommandSqlConnection();
                    //connection.Open();
                    db.SqlConnectionOpen();
                    atlantaDataSet.mobitelsRow row;
                    //using (MySqlDataReader reader = command.ExecuteReader())
                    db.SqlDataReader = db.CommandExecuteReader();
                    {
                        while (db.Read())
                        {
                            row = mobitels.NewmobitelsRow();
                            row.Mobitel_ID = db.GetInt32("mobitel_id");
                            row.Name = db.GetString("Name");
                            row.Descr = db.GetString("Descr");
                            row.NumTT = db.GetString("devIdShort");
                            row.SIM = db.GetString("TelNumber");
                            row.timeStop = new TimeSpan();
                            row.timeTravel = new TimeSpan();
                            row.colorTrack = db.IsDbNull(db.GetOrdinal("Color"))
                                                 ? Globals.TRACK_COLOR_DEFAULT
                                                 : db.GetInt32("Color");
                            
                            row.LastTimeGps = db.GetDateTime("LastTimeGps");
                            row.Is64bitPackets = db.GetBit("Is64bitPackets");
                            row.IsNotDrawDgps = db.GetBit("IsNotDrawDgps");
							mobitels.AddmobitelsRow(row);
                        }
                    }
                    db.CloseDataReader();
                }
                db.SqlConnectionClose();
            }
        }

        public static DataTable GetAllLastTimeGps()
        {
            //using (ConnectMySQL cn = new ConnectMySQL())
            DriverDb db = new DriverDb();
            db.ConnectDb();
            {
                string sql = TrackControlQuery.MobitelsProvider.SelectMobitelId;
               
                return db.GetDataTable(sql);
            }
            //db.CloseDbConnection();
        }

        public static int GetMobitelIdFromNameTT(string nameTT)
        {
            //using (ConnectMySQL cn = new ConnectMySQL())
            using (DriverDb db = new DriverDb())
            {
                db.ConnectDb();

                string sql = string.Format( TrackControlQuery.MobitelsProvider.SelectFromMobitels, db.ParamPrefics );

                //MySqlParameter[] parSQL = new MySqlParameter[1];
                db.NewSqlParameterArray(1);
                //parSQL[0] = new MySqlParameter( "?nameTT", nameTT );
                db.NewSqlParameter(db.ParamPrefics + "nameTT", nameTT, 0);
                //return cn.GetScalarValueNull<int>( sql, parSQL, 0 );
                return db.GetScalarValueNull<int>(sql, db.GetSqlParameterArray, 0);
            }
        }
    }
}
