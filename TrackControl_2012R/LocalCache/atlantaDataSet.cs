﻿using System;
using LocalCache;

namespace LocalCache
{
    partial class atlantaDataSet
    {
        partial class mobitelsDataTable
        {
        }
    
        partial class SensorEventsDataTable
        {
        }

        partial class vehicleDataTable
        {
        }

        partial class KilometrageReportDayDataTable
        {
        }

        partial class settingDataTable
        {
        }

        partial class TachometerValueDataTable
        {
            /// <summary>
            /// Возвращает массив значений оборотов двигателя для заданного телетрека.
            /// </summary>
            /// <param name="mobitel">Телетрек.</param>
            /// <returns></returns>
            public double[] SelectByMobitel(mobitelsRow mobitel)
            {
                TachometerValueRow[] row = mobitel.GetTachometerValueRows();
                double[] value = new double[row.Length];
                for (int i = 0; i < row.Length; i++)
                {
                    value[i] = row[i].Value;
                }

                return value;
            }
        }

        partial class FuelReportDataTable
        {
        }

        /// <summary>
        /// Fuel value row
        /// </summary>
        partial class FuelValueRow : IComparable
        {
            #region IComparable Members

            public int CompareTo(object obj)
            {
                if (obj is atlantaDataSet.FuelValueRow)
                {
                    atlantaDataSet.FuelValueRow id = (atlantaDataSet.FuelValueRow) obj;
                    if (id.DataGPS_ID == this.DataGPS_ID)
                        return 0;
                    if (this.DataGPS_ID > id.DataGPS_ID)
                        return 1;
                    if (this.DataGPS_ID < id.DataGPS_ID)
                        return -1;
                }
                throw new ArgumentException("object не является atlantaDataSet.FuelValueRow");
            }

            #endregion
        }

        partial class FuelValueDataTable
        {
        }

        partial class RotateValueDataTable
        {
        }

        partial class sensorsDataTable
        {
        }

        partial class dataviewDataTable
        {
            public atlantaDataSet.dataviewRow FindByTime(atlantaDataSet.mobitelsRow m_row, DateTime time)
            {
                foreach (atlantaDataSet.dataviewRow d_row in m_row.GetdataviewRows())
                {
                    if (d_row.time == time)
                    {
                        return d_row;
                    }
                }

                throw new Exception(String.Format("В таблице dataview не найдена точка с временем {0}", time));
            }

            public System.DateTime[] SelectTimeByMobitelID(mobitelsRow m_row)
            {
                dataviewRow[] row = m_row.GetdataviewRows();
                System.DateTime[] time = new System.DateTime[row.Length];
                for (int i = 0; i < row.Length; i++)
                {
                    time[i] = row[i].time;
                }
                return time;
            }

            public double[] SelectAccelByMoitelID(mobitelsRow m_row)
            {
                dataviewRow[] row = m_row.GetdataviewRows();
                double[] accel = new double[row.Length];
                for (int i = 0; i < row.Length; i++)
                {
                    accel[i] = (double) row[i].accel;
                }
                return accel;
            }

            public double[] SelectSpeedByMoitelID(mobitelsRow m_row)
            {
                dataviewRow[] row = m_row.GetdataviewRows();
                double[] speed = new double[row.Length];

                for (int i = 0; i < row.Length; i++)
                {
                    speed[i] = (double) row[i].speed;
                }
                return speed;
            }
        }
    }
}