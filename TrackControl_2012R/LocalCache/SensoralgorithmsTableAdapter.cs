using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using MySql.Data.MySqlClient;
using TrackControl.General.DAL;
using TrackControl.General.DatabaseDriver;

namespace LocalCache.atlantaDataSetTableAdapters 
{
	[global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Design.TypedDataSetGenerator", "2.0.0.0")]
	[global::System.ComponentModel.DesignerCategoryAttribute("code")]
	[global::System.ComponentModel.ToolboxItem(true)]
	[global::System.ComponentModel.DataObjectAttribute(true)]
	[global::System.ComponentModel.DesignerAttribute("Microsoft.VSDesigner.DataSource.Design.TableAdapterDesigner, Microsoft.VSDesigner" +
			", Version=8.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")]
	[global::System.ComponentModel.Design.HelpKeywordAttribute("vs.data.TableAdapter")]
	public class SensoralgorithmsTableAdapter : global::System.ComponentModel.Component
  {

    //private global::MySql.Data.MySqlClient.MySqlDataAdapter _adapter;
    //private global::MySql.Data.MySqlClient.MySqlConnection _connection;
    //private global::MySql.Data.MySqlClient.MySqlCommand[] _commandCollection;
    DriverDb db = new DriverDb();

    private bool _clearBeforeFill;

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    public SensoralgorithmsTableAdapter()
    {
      this.ClearBeforeFill = true;
    }

	    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
	    //private global::MySql.Data.MySqlClient.MySqlDataAdapter Adapter
	    private object Adapter
	    {
	        get
	        {
	            if ((this.db.Adapter == null))
	            {
	                this.InitAdapter();
	            }

	            return this.db.Adapter;
	        } // get
	    } // Adapter

	    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
	    //public global::MySql.Data.MySqlClient.MySqlConnection Connection
	    public object Connection
	    {
	        get
	        {
	            if ((this.db.Connection == null))
	            {
	                this.InitConnection();
	            }

	            return this.db.Connection;
	        } // get
	        set
	        {
	            this.db.Connection = value;

	            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
	            {
	                MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;

	                if ((adapter.InsertCommand != null))
	                {
	                    adapter.InsertCommand.Connection = (MySqlConnection) value;
	                }

	                if ((adapter.DeleteCommand != null))
	                {
                        adapter.DeleteCommand.Connection = (MySqlConnection) value;
	                }

	                if ((adapter.UpdateCommand != null))
	                {
                        adapter.UpdateCommand.Connection = (MySqlConnection) value;
	                }

	                MySqlCommand[] collection = (MySqlCommand[]) this.CommandCollection;

	                for (int i = 0; (i < this.CommandCollection.Length); i = (i + 1))
	                {
	                    if ((this.CommandCollection[i] != null))
	                    {
	                        collection[i].Connection = (MySqlConnection) value;
	                    }
	                } // for
	            } // if
                else if( DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse )
                {
                    SqlDataAdapter adapter = (SqlDataAdapter)this.Adapter;

                    if( ( adapter.InsertCommand != null ) )
                    {
                        adapter.InsertCommand.Connection = (SqlConnection)value;
                    }

                    if( ( adapter.DeleteCommand != null ) )
                    {
                        adapter.DeleteCommand.Connection = (SqlConnection)value;
                    }

                    if( ( adapter.UpdateCommand != null ) )
                    {
                        adapter.UpdateCommand.Connection = (SqlConnection)value;
                    }

                    SqlCommand[] collection = (SqlCommand[])this.CommandCollection;

                    for( int i = 0; ( i < this.CommandCollection.Length ); i = ( i + 1 ) )
                    {
                        if( ( this.CommandCollection[i] != null ) )
                        {
                            collection[i].Connection = (SqlConnection)value;
                        }
                    } // for
                } // else if
	        } // set
	    } // Connection

	    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
	    //protected global::MySql.Data.MySqlClient.MySqlCommand[] CommandCollection
	    protected object[] CommandCollection
	    {
	        get
	        {
	            if ((this.db.CommandCollection == null))
	            {
	                this.InitCommandCollection();
	            }

	            return this.db.CommandCollection;
	        }
	    } // CommandCollection

	    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    public bool ClearBeforeFill
    {
      get
      {
        return this._clearBeforeFill;
      }
      set
      {
        this._clearBeforeFill = value;
      }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    private void InitAdapter()
    {
      //this._adapter = new global::MySql.Data.MySqlClient.MySqlDataAdapter();
        this.db.NewDataAdapter();
      global::System.Data.Common.DataTableMapping tableMapping = new global::System.Data.Common.DataTableMapping();
      tableMapping.SourceTable = "Table";
      tableMapping.DataSetTable = "sensoralgorithms";
      tableMapping.ColumnMappings.Add("ID", "ID");
      tableMapping.ColumnMappings.Add("Name", "Name");
      tableMapping.ColumnMappings.Add("Description", "Description");
      tableMapping.ColumnMappings.Add("AlgorithmID", "AlgorithmID");
      this.db.AdapterTableMappingsAdd(tableMapping);
      //this._adapter.InsertCommand = new global::MySql.Data.MySqlClient.MySqlCommand();
        this.db.AdapterInsertCommand();
      //this._adapter.InsertCommand.Connection = this.Connection;
        this.db.AdapterInsertCommandConnection(this.Connection);
      //this._adapter.InsertCommand.CommandText = "INSERT INTO `atlanta`.`sensoralgorithms` (`Name`, `Description`, `AlgorithmID`) V" +
      //    "ALUES (?Name, ?Description, ?AlgorithmID)";

       this.db.AdapterInsertCommandText(TrackControlQuery.SensoralgorithmTableAdapter.InsertIntoAtlanta);
        

        //this._adapter.InsertCommand.CommandType = global::System.Data.CommandType.Text;
        this.db.AdapterInsertCommandType( global::System.Data.CommandType.Text );

      //global::MySql.Data.MySqlClient.MySqlParameter param = new global::MySql.Data.MySqlClient.MySqlParameter();
        this.db.NewSqlParameter();
      //param.ParameterName = "?Name";
        this.db.ParameterName( db.ParamPrefics + "Name" );
      //param.DbType = global::System.Data.DbType.StringFixedLength;
        this.db.ParameterDbType( global::System.Data.DbType.StringFixedLength );
      //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.String;
        this.db.ParameterSqlDbType( db.GettingString() );
      //param.IsNullable = true;
        this.db.ParameterIsNullable( true );
      //param.SourceColumn = "Name";
        this.db.ParameterSourceColumn( "Name" );
      //this._adapter.InsertCommand.Parameters.Add(param);
        this.db.AdapterInsertCommandParametersAdd();

      //param = new global::MySql.Data.MySqlClient.MySqlParameter();
        this.db.NewSqlParameter();
      //param.ParameterName = "?Description";
        this.db.ParameterName( db.ParamPrefics + "Description" );
      //param.DbType = global::System.Data.DbType.StringFixedLength;
        this.db.ParameterDbType( global::System.Data.DbType.StringFixedLength );
      //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.String;
        this.db.ParameterSqlDbType( db.GettingString() );
      //param.IsNullable = true;
        this.db.ParameterIsNullable(true);
      //param.SourceColumn = "Description";
        this.db.ParameterSourceColumn( "Description" );
      //this._adapter.InsertCommand.Parameters.Add(param);
        this.db.AdapterInsertCommandParametersAdd();

      //param = new global::MySql.Data.MySqlClient.MySqlParameter();
        this.db.NewSqlParameter();
      //param.ParameterName = "?AlgorithmID";
        this.db.ParameterName( db.ParamPrefics + "AlgorithmID" );
      //param.DbType = global::System.Data.DbType.UInt32;
        this.db.ParameterDbType( global::System.Data.DbType.Int32 );
      //param.MySqlDbType = global::MySql.Data.MySqlClient.MySqlDbType.UInt32;
        this.db.ParameterSqlDbType( db.GettingUInt32() );
      //param.IsNullable = true;
        this.db.ParameterIsNullable( true );
      //param.SourceColumn = "AlgorithmID";
        this.db.ParameterSourceColumn( "AlgorithmID" );
      //this._adapter.InsertCommand.Parameters.Add(param);
      this.db.AdapterInsertCommandParametersAdd();
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    private void InitConnection()
    {
      //this._connection = new global::MySql.Data.MySqlClient.MySqlConnection();
        this.db.AdapNewConnection();
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    private void InitCommandCollection()
    {
      //this._commandCollection = new global::MySql.Data.MySqlClient.MySqlCommand[1];
        this.db.NewCommandCollection(1);
      //this._commandCollection[0] = new global::MySql.Data.MySqlClient.MySqlCommand();
        this.db.SetNewCommandCollection(0);
      //this._commandCollection[0].Connection = this.Connection;
        this.db.SetCommandCollectionConnection(this.Connection, 0);
      //this._commandCollection[0].CommandText = "SELECT     ID, Name, Description, AlgorithmID\r\nFROM         sensoralgorithms";
        this.db.SetCommandCollectionText( "SELECT ID, Name, Description, AlgorithmID\r\nFROM         sensoralgorithms", 0);
      //this._commandCollection[0].CommandType = global::System.Data.CommandType.Text;
        this.db.SetCommandCollectionType( global::System.Data.CommandType.Text, 0 );
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.ComponentModel.Design.HelpKeywordAttribute("vs.data.TableAdapter")]
    [global::System.ComponentModel.DataObjectMethodAttribute(global::System.ComponentModel.DataObjectMethodType.Fill, true)]
    public virtual int Fill(atlantaDataSet.sensoralgorithmsDataTable dataTable)
    {
        if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
        {
            MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;
            adapter.SelectCommand = (MySqlCommand) this.CommandCollection[0];
        }
        else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
        {
            SqlDataAdapter adapter = (SqlDataAdapter)this.Adapter;
            adapter.SelectCommand = (SqlCommand)this.CommandCollection[0];
        }

        if ((this.ClearBeforeFill == true))
        {
            dataTable.Clear();
        }

        int returnValue = 0;

        if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
        {
            MySqlDataAdapter adapter = (MySqlDataAdapter)this.Adapter;
            returnValue = adapter.Fill(dataTable);
        }
        else if( DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse )
        {
            SqlDataAdapter adapter = (SqlDataAdapter)this.Adapter;
            returnValue = adapter.Fill( dataTable );
        }

        return returnValue;
    }

	    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
	    [global::System.ComponentModel.Design.HelpKeywordAttribute("vs.data.TableAdapter")]
	    [global::System.ComponentModel.DataObjectMethodAttribute(global::System.ComponentModel.DataObjectMethodType.Select,
	        true)]
	    public virtual atlantaDataSet.sensoralgorithmsDataTable GetData()
	    {
	        if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
	        {
	            MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;
	            adapter.SelectCommand = (MySqlCommand) this.CommandCollection[0];
	        }
	        else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
	        {
	            SqlDataAdapter adapter = (SqlDataAdapter) this.Adapter;
	            adapter.SelectCommand = (SqlCommand) this.CommandCollection[0];
	        }

	        atlantaDataSet.sensoralgorithmsDataTable dataTable = new atlantaDataSet.sensoralgorithmsDataTable();

	        if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
	        {
	            MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;
	            adapter.Fill(dataTable);
	        }
	        else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
	        {
	            SqlDataAdapter adapter = (SqlDataAdapter) this.Adapter;
	            adapter.Fill(dataTable);
	        }

	        return dataTable;
	    }

	    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
	    [global::System.ComponentModel.Design.HelpKeywordAttribute("vs.data.TableAdapter")]
	    public virtual int Update(atlantaDataSet.sensoralgorithmsDataTable dataTable)
	    {
	        if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
	        {
	            MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;
                return adapter.Update( dataTable );
	        }
	        else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
	        {
	            SqlDataAdapter adapter = (SqlDataAdapter) this.Adapter;
                return adapter.Update( dataTable );
	        }

	        return 0;
	    }

	    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
	    [global::System.ComponentModel.Design.HelpKeywordAttribute("vs.data.TableAdapter")]
	    public virtual int Update(atlantaDataSet dataSet)
	    {
	        //return this.Adapter.Update(dataSet, "sensoralgorithms");
	        if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
	        {
	            MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;
                return adapter.Update( dataSet, "sensoralgorithms" );
	        }
	        else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
	        {
	            SqlDataAdapter adapter = (SqlDataAdapter) this.Adapter;
                return adapter.Update( dataSet, "sensoralgorithms" );
	        }

	        return 0;
	    }

	    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
	    [global::System.ComponentModel.Design.HelpKeywordAttribute("vs.data.TableAdapter")]
	    public virtual int Update(global::System.Data.DataRow dataRow)
	    {
	        //return this.Adapter.Update(new global::System.Data.DataRow[] {dataRow});
	        if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
	        {
	            MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;
                return adapter.Update( new global::System.Data.DataRow[] { dataRow } );
	        }
	        else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
	        {
	            SqlDataAdapter adapter = (SqlDataAdapter) this.Adapter;
                return adapter.Update( new global::System.Data.DataRow[] { dataRow } );
	        }

	        return 0;
	    } // Update

	    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
	    [global::System.ComponentModel.Design.HelpKeywordAttribute("vs.data.TableAdapter")]
	    public virtual int Update(global::System.Data.DataRow[] dataRows)
	    {
	        //return this.Adapter.Update(dataRows);
	        if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
	        {
	            MySqlDataAdapter adapter = (MySqlDataAdapter) this.Adapter;
	            return adapter.Update(dataRows);
	        }
	        else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
	        {
	            SqlDataAdapter adapter = (SqlDataAdapter) this.Adapter;
	            return adapter.Update(dataRows);
	        }
	        return 0;
	    }
  }
}
