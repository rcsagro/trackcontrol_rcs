using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Data;
using NUnit.Framework;
using Route;
using Route.Dictionaries; 
using System.Configuration;
using Agro.Utilites;
using BaseReports.Procedure;
using TrackControl.General;
using TrackControl;
using TrackControl.General.DatabaseDriver;
using TrackControl.Reports ;
using TrackControl.MySqlDal;
using TrackControl.UkrGIS.SearchEngine;

namespace Route.Test
{
    [TestFixture]
    public class TaskTest
    {
        [SetUp]
        public void ModelInit()
        {
            AppModel _appModel = AppModel.Instance;
            //DbCommon dbCommon = new DbCommon(ConnectMySQL.ConnectionString);
            //DbCommon dbCommon = new DbCommon( DriverDb.ConnectionString );
            DriverDb dbCommon = new DriverDb( DriverDb.ConnectionString );
            AppModel.Instance.DbCommon = dbCommon;
            TaskItem.ZonesModel = _appModel.ZonesManager;
            ReportZonesModel _reportZonesModel = new ReportZonesModel(_appModel.ZonesManager);
            Algorithm.ZonesModel = _reportZonesModel;
            AppController _controller = AppController.Instance;
            AppController.initUkrGIS(); 
            Algorithm.GeoLocator = GeoLocator.Instance;
        }
        [Test, Description("���� �� ������������ �������� �������� ��� ���������")]
        public void CreateTaskWithZeroMobitel()
        {
                TaskItem ti = new TaskItem(0);
                Assert.AreEqual(ti.AddDoc(), 0);
       }
        [Test, Description("���� �� �������� � �������� ����� ��������")]
        public void CreateTaskHeader()
        {
            int newID;
            int mobitelID;
            //using (ConnectMySQL cn = new ConnectMySQL())
            DriverDb db = new DriverDb();
            db.ConnectDb();
            {
                string sSQL = "SELECT rt_route.Id FROM  rt_route ORDER BY  Id DESC LIMIT 1";
                //newID = cn.GetScalarValueIntNull(sSQL);
                newID = db.GetScalarValueIntNull( sSQL );
                sSQL = "SELECT   mobitels.Mobitel_ID FROM   mobitels   LIMIT 1";
                //mobitelID = cn.GetScalarValueIntNull(sSQL);
                mobitelID = db.GetScalarValueIntNull( sSQL );
            }
            db.CloseDbConnection();
            
            if (mobitelID > 0)
            {
                using (TaskItem ti = new TaskItem(DateTime.Today,DateTime.Today,mobitelID,(short)Consts.RouteType.ChackZone ,   "TEST NUnit"))
                {
                    Assert.GreaterOrEqual(ti.AddDoc(), newID + 1);
                    ti.DeleteDoc(false);
                }
            }

        }
        [Test, Description("�������� ������� ��������,�������� �� ��������� �������,���� ���������� ��������� ��������� �������")]
        public void CreateFromSample()
        {
            int Zone_Id;
            int Record_Id;
            int SampleGrp_Id;
            int Sample_Id;
            int mobitel_ID;
            //using (ConnectMySQL cn = new ConnectMySQL())
            DriverDb db = new DriverDb();
            db.ConnectDb();
            {
                string sSQL = "SELECT zones.Zone_ID FROM  zones  LIMIT 1";
                //Zone_Id = cn.GetScalarValueIntNull(sSQL);
                Zone_Id = db.GetScalarValueIntNull( sSQL );
                sSQL = "SELECT   mobitels.Mobitel_ID FROM   mobitels   LIMIT 1";
                //mobitel_ID = cn.GetScalarValueIntNull(sSQL);
                mobitel_ID = db.GetScalarValueIntNull( sSQL );
            }
            db.CloseDbConnection();
            
            DictionaryRouteGrp drg = new DictionaryRouteGrp();
            drg.Name = "Test";
            Assert.AreEqual(drg.Name, "Test"); 
            drg.Name = "Test groupe " + DateTime.Now.Minute + DateTime.Now.Second;
            SampleGrp_Id = drg.AddItem(); 
            DictionaryRoute dr = new DictionaryRoute();
            dr.Name = "Test";
            Assert.AreEqual(dr.Name, "Test"); 
            dr.Name = "Test " + DateTime.Now.Minute + DateTime.Now.Second;
            dr.Id_main = SampleGrp_Id;
            Assert.AreEqual(dr.Id_main, SampleGrp_Id); 
            Sample_Id = dr.AddItem();
            TimeSpan[] ts = { new TimeSpan(0, 20, 0), new TimeSpan(0, 32, 0), new TimeSpan(0, 45, 0), new TimeSpan(0, 24, 0) };
            TimeSpan tsAbs = ts[0];
            Record_Id = dr.AddContent(1, Zone_Id, (int)Consts.RouteEvent.Entry, ts[0].ToString().Substring(0, 5), tsAbs.ToString().Substring(0, 5), "00:20", "00:20");
            Assert.Greater(Record_Id, 0);
            Record_Id = dr.AddContent(2, Zone_Id, (int)Consts.RouteEvent.Exit, ts[1].ToString().Substring(0, 5), tsAbs.ToString().Substring(0, 5), "00:40", "00:20");
            Assert.Greater(Record_Id, 0);
            Record_Id = dr.AddContent(3, Zone_Id, (int)Consts.RouteEvent.Entry, ts[2].ToString().Substring(0, 5), tsAbs.ToString().Substring(0, 5), "10:20", "10:20");
            Assert.Greater(Record_Id, 0);
            Record_Id = dr.AddContent(4, Zone_Id, (int)Consts.RouteEvent.Exit, ts[3].ToString().Substring(0, 5), tsAbs.ToString().Substring(0, 5), "0", "0");
            Assert.Greater(Record_Id, 0);
            TimeSpan ts_total = ts[0];
            for (int i = 1; i < ts.Length ; i++)
            {
                ts_total = ts_total.Add(ts[i]); 
            }
            TaskItem ti = new TaskItem(DateTime.Today ,DateTime.Today.Add(ts_total)  ,Sample_Id,mobitel_ID,0,(short)Consts.RouteType.ChackZone)   ;
            Assert.Greater(ti.AddDoc(), 0);
            Assert.AreEqual(ti.ContentCreateFromSample(), 4);
            // ���� ��������� ��������� ������� ������� ��� ����� ������ ���������� ��������� �������
            ti.Date = ti.Date.Add(new TimeSpan(1, 27, 0));
            Assert.IsTrue(ti.UpdateDoc());
            DataTable dt = ti.GetContent();
            Assert.AreEqual((DateTime)dt.Rows[3]["DatePlan"], ti.Date.Add(ts_total)); 
            //---------------------------------------------------------------------------------------
            Assert.IsTrue(ti.DeleteDoc(false));
            Assert.IsTrue(dr.DeleteItem(false));
            Assert.IsTrue(drg.DeleteItem(false));  

        }
        [Test , Description("�������� � ��������� � �������� �������� ���������� �� �������")]
        public void CompareWithTamplate46()
        {
            // ������� - �� �����
            using (TaskItem ti = new TaskItem(46))
            {
                ti.Remark = "�� �������! ������ ��� �����!";
                Assert.AreEqual(ti.UpdateDoc(),true); 
                ti.ChangeStatusEvent += PrintStstusEvent;
                if (ti.ContentAutoFill())
                {
                    Assert.AreEqual(ti.Sample_Id, 4);
                    Assert.AreEqual(ti.Mobitel_Id , 103);
                    Assert.AreEqual(ti.Driver_Id , 11);
                    Assert.AreEqual(ti.Distance, 755.04);
                    Assert.AreEqual(ti.SpeedAvg, 63.4);
                    Assert.AreEqual(ti.FuelStart, 338.76);
                    Assert.AreEqual(ti.FuelEnd, 85.01);
                    Assert.AreEqual(ti.FuelAdd, 0);
                    Assert.AreEqual(ti.FuelAddQty , 0);
                    Assert.AreEqual(ti.FuelSubQty, 0);
                    Assert.AreEqual(ti.Fuel_ExpensAvg, 0);
                    Assert.AreEqual(ti.Fuel_ExpensTotal, 0);
                    Assert.AreEqual(ti.Fuel_ExpensMove, 0);
                    Assert.AreEqual(ti.Fuel_ExpensStop, 0);
                    Assert.AreEqual(ti.TimeMove, "11:54:31");
                    Assert.AreEqual(ti.TimeStop, "04:54:57");
                    Assert.AreEqual(ti.DateEnd, new DateTime(2010, 06, 09, 16, 49, 28));
                    Assert.AreEqual(ti.TimeFactTotal, "16:49:28");
                    Assert.AreEqual(ti.TimePlanTotal, "17:00:00");
                    Assert.AreEqual(ti.Deviation, "-00:10:32");
                    Assert.AreEqual(ti.DeviationAr, "-00:10:32");
                    Assert.AreEqual(ti.PointsValidity , 100);
                    Assert.AreEqual(ti.PointsFact, 6737);
                    Assert.AreEqual(ti.PointsCalc, 6747);
                    Assert.AreEqual(ti.PointsIntervalMax, "00:03:00");
                    DataTable dt = ti.GetStops();
                    Assert.AreEqual(dt.Rows.Count  , 23);
                    dt = ti.GetSensors();
                    Assert.AreEqual(dt.Rows.Count, 3);
                    dt = ti.GetContent();
                    Assert.AreEqual(dt.Rows.Count, 5);
                    dt = ti.GetFuelDUT();
                    Assert.AreEqual(dt.Rows.Count, 0);
                }
                ti.ChangeStatusEvent -= PrintStstusEvent;
            }
           
        }
        [Test, Description("�������� � ��������� � �������� �������� ���������� ��� �����, � �� ����������� �����")]
        public void CompareWithTamplate119()
        {
            using (TaskItem ti = new TaskItem(119))
            {
                ti.Remark = "�� �������! ������ ��� �����!";
                Assert.AreEqual(ti.UpdateDoc(), true); 
                ti.ChangeStatusEvent += PrintStstusEvent;
                if (ti.ContentAutoFillFact())
                {
                    Assert.AreEqual(ti.Sample_Id, 0);
                    Assert.AreEqual(ti.Mobitel_Id, 130);
                    Assert.AreEqual(ti.Driver_Id, 52);
                    Assert.AreEqual(ti.Distance, 439.07);
                    Assert.AreEqual(ti.SpeedAvg, 60.13);
                    //--------------------------------------
                    Assert.AreEqual(ti.FuelStart, 188.57);
                    Assert.AreEqual(ti.FuelEnd, 95.83);
                    Assert.AreEqual(ti.FuelAdd, 0);
                    Assert.AreEqual(ti.FuelAddQty, 0);
                    Assert.AreEqual(ti.FuelSubQty, 0);
                    ti.ContentRefreshFuelDUT();
                    ti.ContentRecalcFuelDUT();
                    Assert.AreEqual(ti.FuelStart, 188.57);
                    Assert.AreEqual(ti.FuelEnd, 95.83);
                    Assert.AreEqual(ti.FuelAdd, 0);
                    Assert.AreEqual(ti.FuelAddQty, 0);
                    Assert.AreEqual(ti.FuelSubQty, 0);
                    //--------------------------------------
                    Assert.AreEqual(ti.Fuel_ExpensAvg, 0);
                    Assert.AreEqual(ti.Fuel_ExpensTotal, 0);
                    Assert.AreEqual(ti.Fuel_ExpensMove, 0);
                    Assert.AreEqual(ti.Fuel_ExpensStop, 0);
                    Assert.AreEqual(ti.TimeMove, "07:18:06");
                    Assert.AreEqual(ti.TimeStop, "04:24:45");
                    Assert.AreEqual(ti.DateEnd, new DateTime(2010, 05, 28, 18, 32, 59));
                    Assert.AreEqual(ti.TimeFactTotal, "11:42:51");
                    Assert.AreEqual(ti.TimePlanTotal, "");
                    Assert.AreEqual(ti.PointsValidity, 100);
                    Assert.AreEqual(ti.PointsFact, 4158);
                    Assert.AreEqual(ti.PointsCalc, 4158);
                    Assert.AreEqual(ti.PointsIntervalMax, "00:01:00");
                    DataTable dt = ti.GetStops();
                    Assert.AreEqual(dt.Rows.Count, 6);
                    dt = ti.GetSensors();
                    Assert.AreEqual(dt.Rows.Count, 0);
                    dt = ti.GetContent();
                    Assert.AreEqual(dt.Rows.Count, 3);
                    dt = ti.GetFuelDUT();
                    Assert.AreEqual(dt.Rows.Count, 0);
                    
                }
                ti.ChangeStatusEvent -= PrintStstusEvent;
            }
        }
        [Test, Description("�������� � ��������� � �������� �������� �������� ������ �������")]
        public void CompareWithTamplate76()
        {
            using (TaskItem ti = new TaskItem(76))
            {
                ti.Remark = "�� �������! ������ ��� �����!";
                Assert.AreEqual(ti.UpdateDoc(), true); 
                ti.ChangeStatusEvent += PrintStstusEvent;
                if (ti.ContentAutoFillFact())
                {
                    Assert.AreEqual(ti.Sample_Id, 0);
                    Assert.AreEqual(ti.Mobitel_Id, 130);
                    Assert.AreEqual(ti.Driver_Id, 52);
                    Assert.AreEqual(ti.Distance, 1.68);
                    Assert.AreEqual(ti.SpeedAvg, 11.28);
                    //--------------------------------------
                    Assert.AreEqual(ti.FuelStart, 201.11);
                    Assert.AreEqual(ti.FuelEnd, 312.5);
                    Assert.AreEqual(ti.FuelAdd, 117.97);
                    Assert.AreEqual(ti.FuelAddQty, 1);
                    Assert.AreEqual(ti.FuelSubQty, 0);
                    ti.ContentRefreshFuelDUT();
                    ti.ContentRecalcFuelDUT();
                    Assert.AreEqual(ti.FuelStart, 201.11);
                    Assert.AreEqual(ti.FuelEnd, 312.5);
                    Assert.AreEqual(ti.FuelAdd, 117.97);
                    Assert.AreEqual(ti.FuelAddQty, 1);
                    Assert.AreEqual(ti.FuelSubQty, 0);
                    //--------------------------------------
                    Assert.AreEqual(ti.Fuel_ExpensAvg, 0);
                    Assert.AreEqual(ti.Fuel_ExpensTotal, 0);
                    Assert.AreEqual(ti.Fuel_ExpensMove, 0);
                    Assert.AreEqual(ti.Fuel_ExpensStop, 0);
                    Assert.AreEqual(ti.TimeMove, "00:08:56");
                    Assert.AreEqual(ti.TimeStop, "1.05:37:16");
                    Assert.AreEqual(ti.DateEnd, new DateTime(2010, 06, 03, 20, 38, 11));
                    Assert.AreEqual(ti.TimeFactTotal, "1.05:46:12");
                    Assert.AreEqual(ti.TimePlanTotal, "");

                }
                ti.ChangeStatusEvent -= PrintStstusEvent;
            }
        }
        [Test, Description("�������� � ��������� � �������� �������� , ���������� �� ���������� ������� � ������� �����")]
        public void CompareWithTamplate91()
        {
            using (TaskItem ti = new TaskItem(91))
            {
                ti.Remark = "�� �������! ������ ��� �����!";
                Assert.AreEqual(ti.UpdateDoc(), true); 
                ti.ChangeStatusEvent += PrintStstusEvent;
                if (ti.ContentAutoFill())
                {
                    Assert.AreEqual(ti.Sample_Id, 0);
                    Assert.AreEqual(ti.Mobitel_Id, 118);
                    Assert.AreEqual(ti.Driver_Id, 29);
                    Assert.AreEqual(ti.Distance, 387.83);
                    Assert.AreEqual(ti.SpeedAvg, 62.54);
                    //--------------------------------------
                    Assert.AreEqual(ti.FuelStart, 0);
                    Assert.AreEqual(ti.FuelEnd, 0);
                    Assert.AreEqual(ti.FuelAdd, 0);
                    Assert.AreEqual(ti.FuelAddQty, 0);
                    Assert.AreEqual(ti.FuelSubQty, 0);
                    ti.ContentRefreshFuelDUT();
                    ti.ContentRecalcFuelDUT();
                    Assert.AreEqual(ti.FuelStart, 0);
                    Assert.AreEqual(ti.FuelEnd, 0);
                    Assert.AreEqual(ti.FuelAdd, 0);
                    Assert.AreEqual(ti.FuelAddQty, 0);
                    Assert.AreEqual(ti.FuelSubQty, 0);
                    //--------------------------------------
                    Assert.AreEqual(ti.Fuel_ExpensAvg, 0);
                    Assert.AreEqual(ti.Fuel_ExpensTotal, 0);
                    Assert.AreEqual(ti.Fuel_ExpensMove, 0);
                    Assert.AreEqual(ti.Fuel_ExpensStop, 0);
                    //--------------------------------------
                    Assert.AreEqual(ti.TimeMove, "06:12:06");
                    Assert.AreEqual(ti.TimeStop, "09:46:01");
                    Assert.AreEqual(ti.DateEnd, new DateTime(2010, 06, 07, 15, 58, 07));
                    Assert.AreEqual(ti.PointsFact, 4119);
                    Assert.AreEqual(ti.PointsCalc, 4121);
                    Assert.AreEqual(ti.PointsIntervalMax, "00:01:01");
                    Assert.AreEqual(ti.TimeFactTotal, "15:58:07");
                    Assert.AreEqual(ti.TimePlanTotal, "");
                    //--------------------------------------
                    DataTable dt = ti.GetStops();
                    Assert.AreEqual(dt.Rows.Count, 11);
                    dt = ti.GetSensors();
                    Assert.AreEqual(dt.Rows.Count, 8);
                    dt = ti.GetContent();
                    Assert.AreEqual(dt.Rows.Count, 10);
                    dt = ti.GetFuelDUT();
                    Assert.AreEqual(dt.Rows.Count, 0);

                }
                ti.ChangeStatusEvent -= PrintStstusEvent;
            }
        }
        public void PrintStstusEvent(string sMes)
        {
            Debug.Print(sMes);
        }

    }
}
