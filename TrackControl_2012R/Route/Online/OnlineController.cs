﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Route.Documents.Shedule;
using TrackControl.General;
using TrackControl.General.Core;
using TrackControl.General.DAL;
using TrackControl.General.DatabaseDriver;
using TrackControl.Online;
using TrackControl.Vehicles;
using TrackControl.MySqlDal;
using Route.Online; 

namespace Route
{
    public class OnlineController : Singleton<OnlineController>
    {
        public event VoidHandler DrawVehicles = delegate { };
        public event VoidHandler StopOnline = delegate { };
        readonly IOnlineDataProvider _provider;
        readonly System.Threading.Timer _timer;
        bool _isStarted;
        public List<RouteChecker> RoutesCheckers { get; set; }
        private Shedule _sheduleTracking;
        //CreatorTestGpsData _dataCreator;

        public OnlineController()
        {
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                _provider = new OnlineDataProvider(driverDb);
                RoutesCheckers = new List<RouteChecker>();
                _timer = new System.Threading.Timer(Analize, null, Timeout.Infinite, Timeout.Infinite);

            }
        }

        public void Start(Shedule shedule)
        {
            if (_isStarted)
                return;
            Clear();
            _isStarted = true;
            _sheduleTracking = shedule;
#if DEBUG
            //StartGenerateTestOnlineData();
#endif
            ThreadPool.QueueUserWorkItem(Analize);
        }

        public bool IsStarted
        {
            get { return _isStarted; }
            set { _isStarted = value; }
        }

        public void Stop()
        {
            _isStarted = false;
            _timer.Change(Timeout.Infinite, Timeout.Infinite);
        }

        void Analize(object obj)
        {
            _timer.Change(Timeout.Infinite, Timeout.Infinite);
            if (!_isStarted) return;
            SetRouteCheckersList();
            //int newPointsCount = 0;
            if (RoutesCheckers.Count > 0)
            {
                foreach (var routeChecker in RoutesCheckers)
                {
                    OnlineVehicle ov = routeChecker.OVehicle;
                    IList<GpsData> geoData = _provider.GetLastData(ov);
                    //newPointsCount += geoData.Count;
                    foreach (GpsData p in geoData)
                    {
                        ov.CheckGpsData(p);
                        if (p.Valid) routeChecker.CheckDataGps(p); ;
                        //ov.LastPoint = p;
                        //ov.OnlineData.Add(p);
                        //ov.LastLogId = p.LogId;
                        //if (p.Valid)
                        //{
                        //    ov.GpsSignal = GpsSignal.Valid;
                        //    ov.LastTime = p.Time;
                        //    ov.Speed = p.Speed;
                        //    ov.UpdateMotionState();
                        //    routesChecker.CheckDataGps(p);
                        //}
                        //else
                        //    ov.GpsSignal = GpsSignal.NotValid;
                    }
                }
                if (_isStarted)
                    DrawVehicles();
                    _timer.Change(5000, Timeout.Infinite);
            }
            else
            {
                Stop();
                StopOnline();
            }
        }

        
        void SetRouteCheckersList()
        {
            var sheduleRecordsAdded = _sheduleTracking.SheduleRecords.Except(RoutesCheckers.Select(rch => rch.SheduleRecordCheking));
            foreach (var sheduleRecord in sheduleRecordsAdded)
            {
                if (sheduleRecord.IsReadyForTracking()) RoutesCheckers.Add(new RouteChecker(sheduleRecord));
            }
        }

        public void Clear()
        {
            RoutesCheckers.Clear();
            Stop();
        }

        //void StartGenerateTestOnlineData()
        //{
        //    _dataCreator = new CreatorTestGpsData();
        //    _dataCreator.Start();
        //}
    }
}
