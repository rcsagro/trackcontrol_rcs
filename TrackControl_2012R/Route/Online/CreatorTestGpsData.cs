﻿using System;
using System.Diagnostics;
using System.Threading;
using TrackControl.General;
using TrackControl.General.DatabaseDriver;
using Timer = System.Threading.Timer;

namespace Route.Online
{
    public class CreatorTestGpsData
    {
        bool _isStarted;
        Timer _timer;
        double _latStart = 49.2342 * 600000;
        double _lonStart = 25.8917 * 600000;
        private int _latDelta = 15;
        private int _lonDelta = -10;
        private int _logId = 1;
        public static DateTime OnlineTime = DateTime.Now;
        public static DateTime StartTime = DateTime.Now;

        double _latStart1 = 49.2342 * 600000;
        double _lonStart1 = 25.8927 * 600000;
        private int _latDelta1 = 20;
        private int _lonDelta1 = -10;


        int Mobilel_Id = 126;
        int Mobilel_Id1 = 204;

        public CreatorTestGpsData()
        {
            _timer = new Timer(InsertOnlineData, null, Timeout.Infinite, Timeout.Infinite);
            ClearOnlineMobitel();
        }

        public void Start()
        {
            if (_isStarted)
                return;

            _isStarted = true;
            ClearOnlineMobitel();
            ThreadPool.QueueUserWorkItem(InsertOnlineData);
        }

        public void Stop()
        {
            _isStarted = false;
        }



        private void ClearOnlineMobitel()
        {
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                string sql = string.Format("Delete FROM Online Where Mobitel_ID = {0}", Mobilel_Id);
                driverDb.ExecuteNonQueryCommand(sql);
                sql = string.Format("Delete FROM Online Where Mobitel_ID = {0}", Mobilel_Id1);
                driverDb.ExecuteNonQueryCommand(sql);
                 sql = string.Format("Delete FROM datagps Where Mobitel_ID = {0}", Mobilel_Id);
                driverDb.ExecuteNonQueryCommand(sql);
                sql = string.Format("Delete FROM datagps Where Mobitel_ID = {0}", Mobilel_Id1);
                driverDb.ExecuteNonQueryCommand(sql);
            }

        }

        private int TestConvertToTimestamp(DateTime value)
        {
            //TimeSpan span = (value - new DateTime(1970, 1, 1, 0, 0, 0, 0).ToLocalTime());
            //return (int)span.TotalSeconds;

            TimeSpan dt = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond) - DateTime.UtcNow;
            DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0) + dt;
            TimeSpan diff = value - origin;

            return Convert.ToInt32(diff.TotalSeconds);
        }

        private void InsertOnlineData(object obj)
        {
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                int speed = 40;
                string sql = "";
                //if (_logId > 30 && _logId < 35) speed = 0; 
                sql = string.Format(@"Insert into datagps (Mobitel_ID,Latitude,Longitude,Events,UnixTime,Valid,Speed,LogID,Altitude) 
                Values ({0},{1},{2},{3},{4},{5},{6},{7},0)", Mobilel_Id, _latStart, _lonStart, 0, TestConvertToTimestamp(OnlineTime), 1, speed, _logId);
                driverDb.ExecuteNonQueryCommand(sql);
                if (OnlineTime.Subtract(StartTime).TotalMinutes > 5)
                {
                    sql = string.Format(@"Insert into Online (Mobitel_ID,Latitude,Longitude,Events,UnixTime,Valid,Speed,LogID) 
                Values ({0},{1},{2},{3},{4},{5},{6},{7})", Mobilel_Id, _latStart, _lonStart, 0, TestConvertToTimestamp(OnlineTime), 1, speed, _logId);
                    driverDb.ExecuteNonQueryCommand(sql);
                }
                _latStart += _latDelta;
                _lonStart += _lonDelta;
                sql = string.Format(@"Insert into Online (Mobitel_ID,Latitude,Longitude,Events,UnixTime,Valid,Speed,LogID) 
                Values ({0},{1},{2},{3},{4},{5},{6},{7})", Mobilel_Id1, _latStart1, _lonStart1, 0, TestConvertToTimestamp(OnlineTime), 1, speed, _logId);
                driverDb.ExecuteNonQueryCommand(sql);
                _latStart1 += _latDelta1;
                _lonStart1 += _lonDelta1;
                //Debug.Print("Insert {0} ", OnlineTime); 
                OnlineTime = OnlineTime.AddMinutes(0.02);
                _logId++;
            }
            _timer.Change(100, Timeout.Infinite);
        }
    }
}
