﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Route.Documents.Shedule;
using Route.Properties;
using TrackControl.GMap;
using TrackControl.General;
using TrackControl.Online;

namespace Route.Online
{
    public class MapDrawController
    {
        List <RouteChecker>  _routers;
        GoogleMapControl _googleMap;
        bool _isStarted;
        OnlineController _onlineController;
        private OnlineControl _view;
        private List<IZone> _sheduleZones;

        public MapDrawController(OnlineControl view)
        {
            _view = view;
            _googleMap = _view.GoogleMapCtr;
            _view.StartOnline += Start;
            _view.StopOnline += Stop;
            SetOnlineController();
            //_view.SelecteVehicleOnMap += UpdateMapInvoke;
        }

        public void Start(Shedule shedule)
        {
            if (_isStarted)
                return;
            _isStarted = true;
            _onlineController.Clear();
            //_onlineController.CreateContext();
            //_onlineController.OnlineVehicles = ConvertEfVehiclesToOnlineVehicles();
            //_googleMap.ClearZones();
            //AddOpenedOrdersToOnlineController();
            _sheduleZones = new List<IZone>();
            if (SetSheduleZones(shedule))
            {
                _googleMap.ClearZones();
                _googleMap.AddZones(_sheduleZones);
                _onlineController.Start(shedule);
            }
            else
            {
                MessageBox.Show(Resources.CheckZonesAbsence, Resources.Routers1);
            }

        }

        private void SetOnlineController()
        {
            if (_onlineController == null)
            {
                _onlineController = OnlineController.Instance;
                _onlineController.DrawVehicles += UpdateMapInvoke;
            }
           
        }

        public void Stop()
        {
            _isStarted = false;
            _onlineController.Stop();
        }

        public void UpdateMapInvoke()
        {
            if (!_googleMap.IsDisposed)
            {
                if (_googleMap.InvokeRequired)
                {
                    MethodInvoker m = delegate()
                    {
                        UpdateMap();
                    };
                    if (_view != null && _googleMap != null) _view.Invoke(m);
                }
                else
                {
                    UpdateMap();
                }
            }
        }

        void UpdateMap()
        {
            if (_onlineController == null || _onlineController.RoutesCheckers == null) return;
            var markers = new List<Marker>();
            var tracks = new List<Track>();
            Marker topMaprker = null;
            foreach (var routesChecker in _onlineController.RoutesCheckers)
            {
                OnlineVehicle ov = routesChecker.OVehicle;
                if (null != ov.Track)
                {
                    tracks.Add(ov.Track);
                    MarkerType t = GetMarkerType(ov);
                    var m = new Marker(t, ov.Mobitel.Id, ov.Track.LastPoint.LatLng, ov.RegNumber, String.Format("<size=+4>{0} {1} {2}</size>", ov.RegNumber, ov.CarModel, ov.CarMaker));
                    if (ov.IsSelected)
                    {
                        m.IsActive = true;
                        topMaprker = m;
                    }
                    else
                    {
                        markers.Add(m);

                    }
                    //SetToolTipDetaled(ov, m);
                }
            }
            if (null != topMaprker)
            {
                markers.Add(topMaprker);
            }
            Application.DoEvents();
            _googleMap.ClearTracks();
            _googleMap.ClearMarkers();
            _googleMap.AddTracks(tracks);
            _googleMap.AddMarkers(markers);
            _googleMap.Repaint();
            //_vehList.RefreshVehicleStates();

            if (_view.IsAutoFit)
            {
                _googleMap.ZoomAndCenterAll();
            }
            else
            {
                if (null != topMaprker)
                {
                    _googleMap.PanTo(topMaprker.Point);
                }
            }
            RefreshGridDataSource();
        }

        private MarkerType GetMarkerType(OnlineVehicle ov)
        {
            MarkerType t;
            if (ov.Speed.HasValue && ov.Speed.Value > 0)
            {
                t = MarkerType.Moving;
            }
            else
            {
                t = MarkerType.Parking;
            }
            return t;
        }

        void RefreshGridDataSource()
        {
            //_view.gcRoutes.DataSource = _onlineController.RoutesCheckers;
        }

        bool SetSheduleZones(Shedule shedule)
        {
            _sheduleZones.Clear();
            if (shedule.ZoneMain == null) return false;
            _sheduleZones.Add(shedule.ZoneMain);
            foreach (var sheduleRecord in shedule.SheduleRecords)
            {
                _sheduleZones.Add(sheduleRecord.ZoneTo);
            }
            return true;
        }
    }
}
