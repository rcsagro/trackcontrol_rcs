﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using System.Text;
using Route.Documents.Shedule;
using TrackControl.MySqlDal;
using TrackControl.Vehicles;  
using TrackControl.Online;

namespace Route.Online
{
    public class RouteChecker
    {
        private enum RouteStates
        {
            InStartZone,
            BeforMoving,
            Moving,
            Finished
        }


        private bool _isTrackChekingBefor;

        private RouteStates _routeState = RouteStates.InStartZone;

        private double DistFact { get; set; }

        public readonly SheduleRecord SheduleRecordCheking;

        public readonly OnlineVehicle OVehicle;

        public RouteChecker(SheduleRecord sheduleRecord)
        {
            SheduleRecordCheking = sheduleRecord;
            OVehicle = new OnlineVehicle(SheduleRecordCheking.Vehicle);
            SheduleRecordCheking.State  = "Ожидание старта";
            SheduleRecordCheking.Save();
        }

        /// <summary>
        ///  трек маршрута до начала слежения 
        /// </summary>
        /// <param name="date"></param>
        void CheckTrackBeforStarting(DateTime date)
        {
            var gpsDataProvider = new GpsDataProvider();
            IList<GpsData> gpsDatas = gpsDataProvider.GetValidDataForPeriod(OVehicle.MobitelId, new DateTime(date.Year,date.Month,date.Day),
                                                                 date);
            foreach (var gpsData in gpsDatas.Reverse())
            {
                if (SheduleRecordCheking.Shedule.ZoneMain.Contains(gpsData.LatLng))
                {
                    var routeChecker = new RouteChecker(SheduleRecordCheking);
                    DateTime dateStart = gpsData.Time;
                    OVehicle.OnlineData.Clear();  
                    foreach (var p in gpsDatas.Where(gps => gps.Time >= dateStart))
                    {
                        OVehicle.CheckGpsData(p);
                        if (p.Valid) routeChecker.CheckDataGps(p); ;
                    }
                    DistFact = routeChecker.DistFact;
                    _routeState = routeChecker._routeState;
                   break;
                }
                
            }
            _isTrackChekingBefor = true;
        }

        public  void CheckDataGps(GpsData gpsData)
        {
            switch (_routeState)
            {
                case RouteStates.InStartZone:
                    {
                        if (SheduleRecordCheking.Shedule.ZoneMain.Contains(gpsData.LatLng))
                        {
                            SheduleRecordCheking.State = "В зоне старта";
                            _routeState = RouteStates.BeforMoving;
                            SheduleRecordCheking.Save();
                        }
                        else if (!_isTrackChekingBefor)
                        {
                            CheckTrackBeforStarting(gpsData.Time);
                        }
                        break;
                    }
                case RouteStates.BeforMoving:
                    {
                        if (!SheduleRecordCheking.Shedule.ZoneMain.Contains(gpsData.LatLng))
                        {
                            SheduleRecordCheking.State = "Стартовал";
                            SheduleRecordCheking.TimeFactStart = gpsData.Time;
                            _routeState = RouteStates.Moving;
                            SheduleRecordCheking.Save();
                        }
                    break;
                    }
                case RouteStates.Moving:
                    {
                        if (SheduleRecordCheking.ZoneTo.Contains(gpsData.LatLng))
                        {
                            SheduleRecordCheking.State = "Завершен";
                            SheduleRecordCheking.TimeFactEnd = gpsData.Time;
                            _routeState = RouteStates.Finished;
                            SheduleRecordCheking.Save();
                        }
                        else if (SheduleRecordCheking.Shedule.ZoneMain.Contains(gpsData.LatLng))
                        {
                            SheduleRecordCheking.State = "Возврат в зону старта";
                            _routeState = RouteStates.BeforMoving;
                            SheduleRecordCheking.Save();
                        }
                        else
                        {
                            DistFact += gpsData.Dist;
                            SheduleRecordCheking.State = GetMovingState(gpsData);
                            SheduleRecordCheking.Save();
                        }
                        break;
                    }
                case RouteStates.Finished:
                    {

                        break;
                    }
            }

        }

        string GetMovingState(GpsData gpsData)
        {
            double planSpeed = SheduleRecordCheking.Distance /
                                (SheduleRecordCheking.TimePlanEnd - SheduleRecordCheking.TimePlanStart).Value.TotalHours;
            double distPlan = (gpsData.Time - SheduleRecordCheking.TimeFactStart).Value.TotalHours * planSpeed;

            double speedFact = Math.Round(DistFact / (gpsData.Time - SheduleRecordCheking.TimeFactStart).Value.TotalHours,3);

            if (DistFact == distPlan)
            {
                return "По графику";
            }
            else
            {
                double minutesDiff = Math.Round(60 * (DistFact - distPlan) / planSpeed, 0);
                if (minutesDiff == 0)
                {
                    return "По графику";
                }
                else
                {
                    //return string.Format("Отклонение {0} мин ({1} к/ч {2} км)", minutesDiff, speedFact, Math.Round(DistFact,3));
                    //Debug.Print("{0} {1} {2} {3} {4} {5}", OVehicle.RegNumber, Math.Round(DistFact, 2), Math.Round(distPlan, 2), Math.Round(planSpeed, 2), minutesDiff, SheduleRecordCheking.TimeFactStart);
                    return string.Format("Отклонение {0} мин ", minutesDiff);
                    
                }

            }
            
        }
        //public string RouteDescription
        //{
        //    get
        //    {
        //        return string.Format("#{0} {1}",Task.ID,Task.Remark) ;
        //    }
        //}

        //public string VehicleInfo
        //{
        //    get
        //    {
        //        return OVehicle.Info;
        //    }
        //}

        //public DateTime RouteDateFinFact
        //{
        //    get
        //    {
        //        return Task.DateEnd;
        //    }
        //}

    }
}
