﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using TrackControl.GMap;
using TrackControl.General;
using Route.Properties;
using Route.Documents.Shedule;  



namespace Route.Online
{
    public partial class OnlineControl : UserControl
    {
        public readonly GoogleMapControl GoogleMapCtr;
        private readonly SheduleControl SheduleCtr;
        public event Action<Shedule> StartOnline = delegate { };
        public event VoidHandler StopOnline = delegate { };
        private readonly MapDrawController _mapDrawController;
        private bool _isStarted;
        public OnlineControl()
        {
            InitializeComponent();
            Localization();
            GoogleMapCtr = new GoogleMapControl();
            splitContainerControl1.Panel2.Controls.Add(GoogleMapCtr);
            GoogleMapCtr.Dock = DockStyle.Fill;
            GoogleMapCtr.State = new MapState(12, new PointLatLng(ConstsGen.KievLat, ConstsGen.KievLng));
            _mapDrawController = new MapDrawController(this);
            SheduleCtr = new SheduleControl();
            splitContainerControl1.Panel1.Controls.Add(SheduleCtr);
            SheduleCtr.Dock = DockStyle.Fill;
            SheduleCtr.SelecteVehicleOnMap += _mapDrawController.UpdateMapInvoke;
        }

        private void btStart_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            SwitchWorkRegime();
        }

        private void SwitchWorkRegime()
        {
            if (SheduleCtr.SheduleActive == null)
            {
                XtraMessageBox.Show("Выберите график!", Resources.Routers1);
                return;
            }
            if (!_isStarted)
            {
                bbiStart.Enabled = true;
                bbiStart.Caption = Resources.StopBbi;
                bbiStart.Glyph = Shared.StopMain;
                if (StartOnline != null)
                {

                    StartOnline(SheduleCtr.SheduleActive);
                    OnlineController.Instance.StopOnline += StopOnlineFromController;
                }
                _isStarted = true;
            }
            else
            {
                //if (bbiStart.Caption == "Пуск") return;
                bbiStart.Caption = Resources.START;
                bbiStart.Glyph = Shared.StartMain;
                
                if (StopOnline != null)
                {
                    StopOnline();
                    OnlineController.Instance.StopOnline -= StopOnlineFromController;
                }
                _isStarted = false;
            }
            Application.DoEvents();
        }

        /// <summary>
        /// Состояние кнопки автомасштабирования
        /// </summary>
        public bool IsAutoFit
        {
            get { return bchAutoFit.Checked; }
            set { bchAutoFit.Checked = value; }
        }

        void Localization()
        {
            //bchAutoFit.Caption = TrackControl.General.Properties.Resources.AutoZoom;
            bchAutoFit.Glyph = Shared.AutoScale;
            bbiStart.Glyph = Shared.StartMain;
            bbiStart.Caption = Resources.START;
        }


        public void ExcelExport()
        {
            SheduleCtr.ExcelExport();
        }

        public void StopOnlineFromController()
        {
            SheduleCtr.RefreshDataSource();
            if (_isStarted)
            {
                _isStarted = false;
                SwitchWorkRegime();
            }
        }
    }
}
