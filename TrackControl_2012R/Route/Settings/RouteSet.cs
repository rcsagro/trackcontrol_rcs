using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Agro.Utilites;
using Route.Properties;

namespace Route
{
    public partial class RouteSet : DevExpress.XtraEditors.XtraForm
    {
        public RouteSet()
        {
            InitializeComponent();
            Localization();
            RouteSetItem rsi = new RouteSetItem();
            prParams.SelectedObject = rsi;
        }
        private void Localization()
        {
            crSattlement.Properties.Caption = Resources.RouteSettings;
            erRouteRadius.Properties.Caption = Resources.Radius;
            erRouteDelayForIdent.Properties.Caption = Resources.SettingsTime;
            Text = Resources.RouteSettingsModule;
        }
    }
}