namespace Route
{
    partial class RouteSet
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RouteSet));
            this.prParams = new DevExpress.XtraVerticalGrid.PropertyGridControl();
            this.crSattlement = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.erRouteRadius = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erRouteDelayForIdent = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.teRoute = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.prParams)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teRoute)).BeginInit();
            this.SuspendLayout();
            // 
            // prParams
            // 
            this.prParams.Dock = System.Windows.Forms.DockStyle.Fill;
            this.prParams.Location = new System.Drawing.Point(0, 0);
            this.prParams.Name = "prParams";
            this.prParams.RecordWidth = 53;
            this.prParams.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.teRoute});
            this.prParams.RowHeaderWidth = 147;
            this.prParams.Rows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.crSattlement});
            this.prParams.Size = new System.Drawing.Size(492, 323);
            this.prParams.TabIndex = 0;
            // 
            // crSattlement
            // 
            this.crSattlement.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.erRouteRadius,
            this.erRouteDelayForIdent});
            this.crSattlement.Name = "crSattlement";
            this.crSattlement.Properties.Caption = "��������� ����  �������� ���������� ������";
            // 
            // erRouteRadius
            // 
            this.erRouteRadius.Name = "erRouteRadius";
            this.erRouteRadius.Properties.Caption = "������ ������ ���������� �������, �";
            this.erRouteRadius.Properties.FieldName = "RouteRadius";
            this.erRouteRadius.Properties.RowEdit = this.teRoute;
            // 
            // erRouteDelayForIdent
            // 
            this.erRouteDelayForIdent.Name = "erRouteDelayForIdent";
            this.erRouteDelayForIdent.Properties.Caption = "����� ������������ ������ � �����, ���";
            this.erRouteDelayForIdent.Properties.FieldName = "RouteDelayForIdent";
            // 
            // teRoute
            // 
            this.teRoute.AutoHeight = false;
            this.teRoute.Name = "teRoute";
            // 
            // RouteSet
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(492, 323);
            this.Controls.Add(this.prParams);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "RouteSet";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "��������� ������ ��������";
            ((System.ComponentModel.ISupportInitialize)(this.prParams)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teRoute)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraVerticalGrid.PropertyGridControl prParams;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow crSattlement;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erRouteRadius;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erRouteDelayForIdent;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit teRoute;
    }
}