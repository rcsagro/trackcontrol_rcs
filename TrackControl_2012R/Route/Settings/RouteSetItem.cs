using System;
using System.Collections.Generic;
using System.Text;
using Agro; 

namespace Route
{
    /// <summary>
    /// ��������� ������ ��������
    /// </summary>
    public class RouteSetItem
    {
        public  RouteSetItem()
        {

        }
        private  int iNewValue = 0;
        /// <summary>
        /// ��� �������� - ���������� �����: ������ ������ ����������� ������ � ������
        /// </summary>
        public  int RouteRadius
        {
            get
            {
                return Convert.ToInt32(Params.ReadParTotal(Params.NUMPAR.RouteRadius, "20"));
            }
            set
            {
                if (Int32.TryParse(value.ToString(), out iNewValue))
                    Params.SetParTotal(Params.NUMPAR.RouteRadius, iNewValue.ToString());
            }
        }
        /// <summary>
        ///��� �������� - ���������� �����: �������� ������� � ������� �� ����������� �������� ����� � ���������� ���� ��������� ������������
        /// </summary>
        public  int RouteDelayForIdent
        {
            get 
                { return Convert.ToInt32(Params.ReadParTotal(Params.NUMPAR.RouteDelayForIdent, "5")); 
                }
            set 
                {
                if (Int32.TryParse(value.ToString(), out iNewValue))
                    Params.SetParTotal(Params.NUMPAR.RouteDelayForIdent, iNewValue.ToString()); 
                }
        }
    }
}
