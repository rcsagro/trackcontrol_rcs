﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Agro;
using TrackControl.General.Core;

namespace Route
{
    public class RouteController : Singleton<RouteController>, IDisposable 
    {
        private bool _isStarted;
        MainRoute _view;

        public RouteController()
        {

        }

        public void Start()
        {
            if (_isStarted && (_view != null))
            {
                 _view.BringToFront();
                return; 
            }
            _view = new MainRoute();
            _view.Show();
            _isStarted = true;
        }

        public void Dispose()
        {
            _isStarted = false;
            if (_view != null) _view = null;
        }
    }
}
