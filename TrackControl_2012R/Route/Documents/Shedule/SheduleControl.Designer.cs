﻿namespace Route.Documents.Shedule
{
    partial class SheduleControl
    {
        /// <summary> 
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Обязательный метод для поддержки конструктора - не изменяйте 
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SheduleControl));
            this.pgShedule = new DevExpress.XtraVerticalGrid.PropertyGridControl();
            this.rdeDate = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.rcbZones = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.rcbZonesGroups = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.rcbDictionary = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.erId = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erDate = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erDictionary = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erZoneGroup = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erZone = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erOutLinkId = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erRemark = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gcShedule = new DevExpress.XtraGrid.GridControl();
            this.gvShedule = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colZoneToGroup = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rcbZonesToGroups = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.colZone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rcbZonesTo = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.colVehicleGroup = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rcbVehiclesGroups = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.colVehicle = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rcbVehicles = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.colDistance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimePlanStart = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rteTime = new DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit();
            this.colTimePlanEnd = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimeFactStart = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimeFactEnd = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colState = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIsClose = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rchIsClose = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.beRegime = new DevExpress.XtraBars.BarEditItem();
            this.rrgRegime = new DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup();
            this.beDateGraph = new DevExpress.XtraBars.BarEditItem();
            this.rdeDateGraph = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.bbiNew = new DevExpress.XtraBars.BarButtonItem();
            this.chEdit = new DevExpress.XtraBars.BarCheckItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.repositoryItemRadioGroup1 = new DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemToggleSwitch1 = new DevExpress.XtraEditors.Repository.RepositoryItemToggleSwitch();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.pgShedule)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdeDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdeDate.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rcbZones)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rcbZonesGroups)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rcbDictionary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcShedule)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvShedule)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rcbZonesToGroups)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rcbZonesTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rcbVehiclesGroups)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rcbVehicles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rteTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rchIsClose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rrgRegime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdeDateGraph)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdeDateGraph.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemRadioGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemToggleSwitch1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            this.SuspendLayout();
            // 
            // pgShedule
            // 
            this.pgShedule.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pgShedule.Location = new System.Drawing.Point(0, 0);
            this.pgShedule.Name = "pgShedule";
            this.pgShedule.OptionsBehavior.PropertySort = DevExpress.XtraVerticalGrid.PropertySort.NoSort;
            this.pgShedule.RecordWidth = 144;
            this.pgShedule.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.rdeDate,
            this.rcbZones,
            this.rcbZonesGroups,
            this.rcbDictionary});
            this.pgShedule.RowHeaderWidth = 56;
            this.pgShedule.Rows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.erId,
            this.erDate,
            this.erDictionary,
            this.erZoneGroup,
            this.erZone,
            this.erOutLinkId,
            this.erRemark});
            this.pgShedule.Size = new System.Drawing.Size(708, 136);
            this.pgShedule.TabIndex = 0;
            this.pgShedule.CellValueChanged += new DevExpress.XtraVerticalGrid.Events.CellValueChangedEventHandler(this.pgShedule_CellValueChanged);
            this.pgShedule.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.pgShedule_ValidatingEditor);
            this.pgShedule.Enter += new System.EventHandler(this.pgShedule_Enter);
            // 
            // rdeDate
            // 
            this.rdeDate.AutoHeight = false;
            this.rdeDate.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rdeDate.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rdeDate.CalendarTimeProperties.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F4);
            this.rdeDate.CalendarTimeProperties.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Default;
            this.rdeDate.Name = "rdeDate";
            // 
            // rcbZones
            // 
            this.rcbZones.AutoHeight = false;
            this.rcbZones.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rcbZones.CaseSensitiveSearch = true;
            this.rcbZones.Name = "rcbZones";
            // 
            // rcbZonesGroups
            // 
            this.rcbZonesGroups.AutoHeight = false;
            this.rcbZonesGroups.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rcbZonesGroups.Name = "rcbZonesGroups";
            // 
            // rcbDictionary
            // 
            this.rcbDictionary.AutoHeight = false;
            this.rcbDictionary.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rcbDictionary.Name = "rcbDictionary";
            // 
            // erId
            // 
            this.erId.Name = "erId";
            this.erId.Properties.Caption = "Номер";
            this.erId.Properties.FieldName = "Id";
            this.erId.Properties.ReadOnly = true;
            // 
            // erDate
            // 
            this.erDate.Name = "erDate";
            this.erDate.Properties.Caption = "Дата";
            this.erDate.Properties.FieldName = "DateShedule";
            this.erDate.Properties.RowEdit = this.rdeDate;
            // 
            // erDictionary
            // 
            this.erDictionary.Name = "erDictionary";
            this.erDictionary.Properties.Caption = "Шаблон графика";
            this.erDictionary.Properties.FieldName = "DictionaryShedule";
            this.erDictionary.Properties.RowEdit = this.rcbDictionary;
            // 
            // erZoneGroup
            // 
            this.erZoneGroup.Name = "erZoneGroup";
            this.erZoneGroup.Properties.Caption = "Группа зон";
            this.erZoneGroup.Properties.FieldName = "ZoneMainGroup";
            this.erZoneGroup.Properties.RowEdit = this.rcbZonesGroups;
            // 
            // erZone
            // 
            this.erZone.Name = "erZone";
            this.erZone.Properties.Caption = "Зона выезда";
            this.erZone.Properties.FieldName = "ZoneMain";
            this.erZone.Properties.RowEdit = this.rcbZones;
            // 
            // erOutLinkId
            // 
            this.erOutLinkId.Name = "erOutLinkId";
            this.erOutLinkId.Properties.Caption = "Номер документа";
            this.erOutLinkId.Properties.FieldName = "OutLinkId";
            // 
            // erRemark
            // 
            this.erRemark.Name = "erRemark";
            this.erRemark.Properties.Caption = "Примечание";
            this.erRemark.Properties.FieldName = "Remark";
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 26);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.pgShedule);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.gcShedule);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(708, 504);
            this.splitContainerControl1.SplitterPosition = 136;
            this.splitContainerControl1.TabIndex = 1;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // gcShedule
            // 
            this.gcShedule.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcShedule.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gcShedule_EmbeddedNavigator_ButtonClick);
            this.gcShedule.Location = new System.Drawing.Point(0, 0);
            this.gcShedule.MainView = this.gvShedule;
            this.gcShedule.Name = "gcShedule";
            this.gcShedule.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.rchIsClose,
            this.rcbZonesToGroups,
            this.rcbVehiclesGroups,
            this.rcbZonesTo,
            this.rcbVehicles,
            this.rteTime});
            this.gcShedule.Size = new System.Drawing.Size(708, 363);
            this.gcShedule.TabIndex = 0;
            this.gcShedule.UseEmbeddedNavigator = true;
            this.gcShedule.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvShedule});
            // 
            // gvShedule
            // 
            this.gvShedule.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colId,
            this.colZoneToGroup,
            this.colZone,
            this.colVehicleGroup,
            this.colVehicle,
            this.colDistance,
            this.colTimePlanStart,
            this.colTimePlanEnd,
            this.colTimeFactStart,
            this.colTimeFactEnd,
            this.colState,
            this.colIsClose});
            this.gvShedule.GridControl = this.gcShedule;
            this.gvShedule.Name = "gvShedule";
            this.gvShedule.OptionsView.ShowGroupPanel = false;
            this.gvShedule.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gvShedule_CustomRowCellEdit);
            this.gvShedule.InitNewRow += new DevExpress.XtraGrid.Views.Grid.InitNewRowEventHandler(this.gvShedule_InitNewRow);
            this.gvShedule.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gvShedule_CellValueChanged);
            this.gvShedule.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.gvShedule_ValidateRow);
            this.gvShedule.Click += new System.EventHandler(this.gvShedule_Click);
            // 
            // colId
            // 
            this.colId.AppearanceHeader.Options.UseTextOptions = true;
            this.colId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colId.Caption = "Id";
            this.colId.Name = "colId";
            // 
            // colZoneToGroup
            // 
            this.colZoneToGroup.AppearanceHeader.Options.UseTextOptions = true;
            this.colZoneToGroup.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colZoneToGroup.Caption = "Группа";
            this.colZoneToGroup.ColumnEdit = this.rcbZonesToGroups;
            this.colZoneToGroup.FieldName = "ZoneToGroup";
            this.colZoneToGroup.Name = "colZoneToGroup";
            this.colZoneToGroup.Visible = true;
            this.colZoneToGroup.VisibleIndex = 0;
            // 
            // rcbZonesToGroups
            // 
            this.rcbZonesToGroups.AutoHeight = false;
            this.rcbZonesToGroups.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rcbZonesToGroups.Name = "rcbZonesToGroups";
            // 
            // colZone
            // 
            this.colZone.AppearanceHeader.Options.UseTextOptions = true;
            this.colZone.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colZone.Caption = "Зона";
            this.colZone.ColumnEdit = this.rcbZonesTo;
            this.colZone.FieldName = "ZoneTo";
            this.colZone.Name = "colZone";
            this.colZone.Visible = true;
            this.colZone.VisibleIndex = 1;
            this.colZone.Width = 144;
            // 
            // rcbZonesTo
            // 
            this.rcbZonesTo.AutoHeight = false;
            this.rcbZonesTo.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rcbZonesTo.Name = "rcbZonesTo";
            // 
            // colVehicleGroup
            // 
            this.colVehicleGroup.AppearanceHeader.Options.UseTextOptions = true;
            this.colVehicleGroup.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colVehicleGroup.Caption = "Группа";
            this.colVehicleGroup.ColumnEdit = this.rcbVehiclesGroups;
            this.colVehicleGroup.FieldName = "VehicleGroup";
            this.colVehicleGroup.Name = "colVehicleGroup";
            this.colVehicleGroup.Visible = true;
            this.colVehicleGroup.VisibleIndex = 2;
            // 
            // rcbVehiclesGroups
            // 
            this.rcbVehiclesGroups.AutoHeight = false;
            this.rcbVehiclesGroups.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rcbVehiclesGroups.Name = "rcbVehiclesGroups";
            // 
            // colVehicle
            // 
            this.colVehicle.AppearanceHeader.Options.UseTextOptions = true;
            this.colVehicle.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colVehicle.Caption = "Транспорт";
            this.colVehicle.ColumnEdit = this.rcbVehicles;
            this.colVehicle.FieldName = "Vehicle";
            this.colVehicle.Name = "colVehicle";
            this.colVehicle.Visible = true;
            this.colVehicle.VisibleIndex = 3;
            this.colVehicle.Width = 144;
            // 
            // rcbVehicles
            // 
            this.rcbVehicles.AutoHeight = false;
            this.rcbVehicles.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rcbVehicles.CaseSensitiveSearch = true;
            this.rcbVehicles.DropDownRows = 20;
            this.rcbVehicles.Name = "rcbVehicles";
            // 
            // colDistance
            // 
            this.colDistance.AppearanceHeader.Options.UseTextOptions = true;
            this.colDistance.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDistance.Caption = "Путь, км";
            this.colDistance.FieldName = "Distance";
            this.colDistance.Name = "colDistance";
            this.colDistance.Visible = true;
            this.colDistance.VisibleIndex = 4;
            // 
            // colTimePlanStart
            // 
            this.colTimePlanStart.AppearanceCell.Options.UseTextOptions = true;
            this.colTimePlanStart.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimePlanStart.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimePlanStart.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimePlanStart.Caption = "План старт";
            this.colTimePlanStart.ColumnEdit = this.rteTime;
            this.colTimePlanStart.DisplayFormat.FormatString = "d MMM HH:mm";
            this.colTimePlanStart.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colTimePlanStart.FieldName = "TimePlanStart";
            this.colTimePlanStart.Name = "colTimePlanStart";
            this.colTimePlanStart.Visible = true;
            this.colTimePlanStart.VisibleIndex = 5;
            this.colTimePlanStart.Width = 144;
            // 
            // rteTime
            // 
            this.rteTime.AutoHeight = false;
            this.rteTime.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rteTime.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F4);
            this.rteTime.DisplayFormat.FormatString = "d MMM HH:mm";
            this.rteTime.Mask.EditMask = "g";
            this.rteTime.Name = "rteTime";
            this.rteTime.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Default;
            // 
            // colTimePlanEnd
            // 
            this.colTimePlanEnd.AppearanceCell.Options.UseTextOptions = true;
            this.colTimePlanEnd.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimePlanEnd.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimePlanEnd.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimePlanEnd.Caption = "План финиш";
            this.colTimePlanEnd.ColumnEdit = this.rteTime;
            this.colTimePlanEnd.DisplayFormat.FormatString = "d MMM HH:mm";
            this.colTimePlanEnd.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colTimePlanEnd.FieldName = "TimePlanEnd";
            this.colTimePlanEnd.Name = "colTimePlanEnd";
            this.colTimePlanEnd.Visible = true;
            this.colTimePlanEnd.VisibleIndex = 6;
            this.colTimePlanEnd.Width = 144;
            // 
            // colTimeFactStart
            // 
            this.colTimeFactStart.AppearanceCell.Options.UseTextOptions = true;
            this.colTimeFactStart.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeFactStart.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeFactStart.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeFactStart.Caption = "Факт старт";
            this.colTimeFactStart.ColumnEdit = this.rteTime;
            this.colTimeFactStart.DisplayFormat.FormatString = "d MMM HH:mm";
            this.colTimeFactStart.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colTimeFactStart.FieldName = "TimeFactStart";
            this.colTimeFactStart.Name = "colTimeFactStart";
            this.colTimeFactStart.OptionsColumn.AllowEdit = false;
            this.colTimeFactStart.OptionsColumn.ReadOnly = true;
            this.colTimeFactStart.Visible = true;
            this.colTimeFactStart.VisibleIndex = 7;
            this.colTimeFactStart.Width = 144;
            // 
            // colTimeFactEnd
            // 
            this.colTimeFactEnd.AppearanceCell.Options.UseTextOptions = true;
            this.colTimeFactEnd.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeFactEnd.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeFactEnd.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeFactEnd.Caption = "Факт финиш";
            this.colTimeFactEnd.ColumnEdit = this.rteTime;
            this.colTimeFactEnd.DisplayFormat.FormatString = "d MMM HH:mm";
            this.colTimeFactEnd.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colTimeFactEnd.FieldName = "TimeFactEnd";
            this.colTimeFactEnd.Name = "colTimeFactEnd";
            this.colTimeFactEnd.OptionsColumn.AllowEdit = false;
            this.colTimeFactEnd.OptionsColumn.ReadOnly = true;
            this.colTimeFactEnd.Visible = true;
            this.colTimeFactEnd.VisibleIndex = 8;
            this.colTimeFactEnd.Width = 144;
            // 
            // colState
            // 
            this.colState.AppearanceHeader.Options.UseTextOptions = true;
            this.colState.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colState.Caption = "Статус";
            this.colState.FieldName = "State";
            this.colState.Name = "colState";
            this.colState.Visible = true;
            this.colState.VisibleIndex = 9;
            this.colState.Width = 215;
            // 
            // colIsClose
            // 
            this.colIsClose.AppearanceHeader.Options.UseTextOptions = true;
            this.colIsClose.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIsClose.Caption = "Откл";
            this.colIsClose.ColumnEdit = this.rchIsClose;
            this.colIsClose.FieldName = "IsClose";
            this.colIsClose.Name = "colIsClose";
            this.colIsClose.Visible = true;
            this.colIsClose.VisibleIndex = 10;
            this.colIsClose.Width = 73;
            // 
            // rchIsClose
            // 
            this.rchIsClose.AutoHeight = false;
            this.rchIsClose.Caption = "Check";
            this.rchIsClose.Name = "rchIsClose";
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.beRegime,
            this.beDateGraph,
            this.bbiNew,
            this.chEdit});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 8;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemRadioGroup1,
            this.repositoryItemTextEdit1,
            this.repositoryItemToggleSwitch1,
            this.repositoryItemTextEdit2,
            this.rrgRegime,
            this.rdeDateGraph});
            // 
            // bar2
            // 
            this.bar2.BarName = "Главное меню";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.beDateGraph, DevExpress.XtraBars.BarItemPaintStyle.Caption),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bbiNew, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.chEdit, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.beRegime, DevExpress.XtraBars.BarItemPaintStyle.Caption)});
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Главное меню";
            // 
            // beRegime
            // 
            this.beRegime.Caption = "Вид";
            this.beRegime.Edit = this.rrgRegime;
            this.beRegime.EditValue = ((short)(0));
            this.beRegime.Id = 4;
            this.beRegime.Name = "beRegime";
            this.beRegime.Width = 231;
            // 
            // rrgRegime
            // 
            this.rrgRegime.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.rrgRegime.Appearance.BackColor2 = System.Drawing.Color.Transparent;
            this.rrgRegime.Appearance.Options.UseBackColor = true;
            this.rrgRegime.AppearanceFocused.BackColor = System.Drawing.Color.Transparent;
            this.rrgRegime.AppearanceFocused.BackColor2 = System.Drawing.Color.Transparent;
            this.rrgRegime.AppearanceFocused.Options.UseBackColor = true;
            this.rrgRegime.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(((short)(0)), "Редактирование"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(((short)(1)), "Слежение")});
            this.rrgRegime.Name = "rrgRegime";
            this.rrgRegime.EditValueChanged += new System.EventHandler(this.rrgRegime_EditValueChanged);
            // 
            // beDateGraph
            // 
            this.beDateGraph.Caption = "Дата графика";
            this.beDateGraph.Edit = this.rdeDateGraph;
            this.beDateGraph.Id = 5;
            this.beDateGraph.Name = "beDateGraph";
            this.beDateGraph.Width = 94;
            this.beDateGraph.EditValueChanged += new System.EventHandler(this.beDateGraph_EditValueChanged);
            // 
            // rdeDateGraph
            // 
            this.rdeDateGraph.AutoHeight = false;
            this.rdeDateGraph.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rdeDateGraph.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rdeDateGraph.CalendarTimeProperties.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F4);
            this.rdeDateGraph.CalendarTimeProperties.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Default;
            this.rdeDateGraph.Name = "rdeDateGraph";
            // 
            // bbiNew
            // 
            this.bbiNew.Caption = "Новый";
            this.bbiNew.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiNew.Glyph")));
            this.bbiNew.Id = 6;
            this.bbiNew.Name = "bbiNew";
            this.bbiNew.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.bbiNew.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiNew_ItemClick);
            // 
            // chEdit
            // 
            this.chEdit.Caption = "Редактировать";
            this.chEdit.Glyph = ((System.Drawing.Image)(resources.GetObject("chEdit.Glyph")));
            this.chEdit.Id = 7;
            this.chEdit.Name = "chEdit";
            this.chEdit.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.chEdit_CheckedChanged);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(708, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 530);
            this.barDockControlBottom.Size = new System.Drawing.Size(708, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 504);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(708, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 504);
            // 
            // repositoryItemRadioGroup1
            // 
            this.repositoryItemRadioGroup1.Name = "repositoryItemRadioGroup1";
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // repositoryItemToggleSwitch1
            // 
            this.repositoryItemToggleSwitch1.AutoHeight = false;
            this.repositoryItemToggleSwitch1.Name = "repositoryItemToggleSwitch1";
            this.repositoryItemToggleSwitch1.ShowText = false;
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // SheduleControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainerControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "SheduleControl";
            this.Size = new System.Drawing.Size(708, 530);
            ((System.ComponentModel.ISupportInitialize)(this.pgShedule)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdeDate.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdeDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rcbZones)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rcbZonesGroups)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rcbDictionary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcShedule)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvShedule)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rcbZonesToGroups)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rcbZonesTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rcbVehiclesGroups)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rcbVehicles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rteTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rchIsClose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rrgRegime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdeDateGraph.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rdeDateGraph)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemRadioGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemToggleSwitch1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraVerticalGrid.PropertyGridControl pgShedule;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit rdeDate;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erId;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erDate;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erZone;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erOutLinkId;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erRemark;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraGrid.GridControl gcShedule;
        private DevExpress.XtraGrid.Views.Grid.GridView gvShedule;
        private DevExpress.XtraGrid.Columns.GridColumn colId;
        private DevExpress.XtraGrid.Columns.GridColumn colZone;
        private DevExpress.XtraGrid.Columns.GridColumn colVehicle;
        private DevExpress.XtraGrid.Columns.GridColumn colTimePlanEnd;
        private DevExpress.XtraGrid.Columns.GridColumn colTimeFactStart;
        private DevExpress.XtraGrid.Columns.GridColumn colTimeFactEnd;
        private DevExpress.XtraGrid.Columns.GridColumn colState;
        private DevExpress.XtraGrid.Columns.GridColumn colIsClose;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit rchIsClose;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox rcbZones;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erZoneGroup;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox rcbZonesGroups;
        private DevExpress.XtraGrid.Columns.GridColumn colZoneToGroup;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox rcbZonesToGroups;
        private DevExpress.XtraGrid.Columns.GridColumn colVehicleGroup;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox rcbVehiclesGroups;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox rcbZonesTo;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox rcbVehicles;
        private DevExpress.XtraGrid.Columns.GridColumn colDistance;
        private DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit rteTime;
        private DevExpress.XtraGrid.Columns.GridColumn colTimePlanStart;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraEditors.Repository.RepositoryItemToggleSwitch repositoryItemToggleSwitch1;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup repositoryItemRadioGroup1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private DevExpress.XtraBars.BarEditItem beRegime;
        private DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup rrgRegime;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox rcbDictionary;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erDictionary;
        private DevExpress.XtraBars.BarEditItem beDateGraph;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit rdeDateGraph;
        private DevExpress.XtraBars.BarButtonItem bbiNew;
        private DevExpress.XtraBars.BarCheckItem chEdit;
    }
}
