﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Agro;
using Agro.Properties;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraPrinting;
using TrackControl.General;
using Route.DAL;
using Route.Properties;
using TrackControl.Vehicles;


namespace Route.Documents.Shedule
{
    public partial class SheduleControl : UserControl
    {
        public event VoidHandler SelecteVehicleOnMap = delegate { };

        public Shedule SheduleActive { get; private set; }
        
        public SheduleControl()
        {
            InitializeComponent();
            LoadComboDictionary();
            LoadComboZoneGroups();
            LoadComboVehiclesGroups();
            InitShedule();
        }

        void InitShedule()
        {
            beDateGraph.EditValue = DateTime.Today;
            gcShedule.UseEmbeddedNavigator = false;
            gvShedule.OptionsBehavior.Editable = false;

        }

        void SetGraphToDate()
        {
            Shedule shedule = SheduleProvider.GetOneToDate((DateTime)beDateGraph.EditValue);
            //if (shedule.IsNew && (DateTime)beDateGraph.EditValue == DateTime.Today)
            if (shedule.IsNew)
            {
                bbiNew.Visibility = BarItemVisibility.Always;
                ClearGraph();
                return;
            }
            else
            {
                bbiNew.Visibility = BarItemVisibility.Never;
                if (shedule.IsNew)
                {
                    ClearGraph();
                    return;
                }

            }
            SetShedule(shedule);
        }

        private void SetShedule(Shedule shedule)
        {
            pgShedule.SelectedObject = shedule;
            SheduleActive = shedule;
            gcShedule.DataSource = shedule.SheduleRecords;
            LoadComboZones(rcbZones, SheduleActive.ZoneMainGroup);
        }

        void SetGraphNew()
        {
            var shedule = new Shedule { DateShedule = (DateTime)beDateGraph.EditValue };
            if (SheduleProvider.Save(shedule))
            {
                chEdit.Checked = true;
                pgShedule.Focus();
                bbiNew.Visibility = BarItemVisibility.Never;
                SetShedule(shedule);
                
            }

        }

        void ClearGraph()
        {
            pgShedule.SelectedObject = null;
            gcShedule.DataSource = null;
            pgShedule.Refresh(); 
            RefreshDataSource();
        }

        void LoadComboZoneGroups()
        {
            foreach (var zoneGroup in DocItem.ZonesModel.Groups)
            {
                rcbZonesGroups.Items.Add(zoneGroup);
                rcbZonesToGroups.Items.Add(zoneGroup);
            }
        }

        void LoadComboVehiclesGroups()
        {
            foreach (var vehGroup in DocItem.VehiclesModel.RootGroupsWithItems() )
            {
                rcbVehiclesGroups.Items.Add(vehGroup);
            }
        }

        void LoadComboZones(RepositoryItemComboBox combo,  ZonesGroup group)
        {
            combo.Items.Clear();
            if (group == null) return;
            foreach (var zone in DocItem.ZonesModel.Zones.Where(z => z.Group.Id == group.Id))
            {
                combo.Items.Add(zone);
            }
        }

        void LoadComboVehicles(VehiclesGroup group)
        {
            rcbVehicles.Items.Clear();
            if (group == null) return;
            foreach (var vehicle in group.OwnRealItems())
            {
                rcbVehicles.Items.Add(vehicle);
            }
        }

        void LoadComboDictionary()
        {
            rcbDictionary.Items.Clear();
            foreach (var dict in DictionarySheduleProvider.GetAll())
            {
                rcbDictionary.Items.Add(dict);
            }
        }

        #region XtraGrid
        private void gvShedule_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {

        }

        private void gcShedule_EmbeddedNavigator_ButtonClick(object sender, DevExpress.XtraEditors.NavigatorButtonClickEventArgs e)
        {
            switch (e.Button.ButtonType)
            {
                case NavigatorButtonType.Remove:
                    {
                        if (XtraMessageBox.Show(Resources.DataDeleteConfirm,
                            Resources.Routers1,
                            MessageBoxButtons.YesNoCancel,
                            MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            var record = gvShedule.GetRow(gvShedule.FocusedRowHandle) as SheduleRecord;
                            if (record == null)
                                e.Handled = true;
                            else
                                e.Handled = !record.Delete();
                        }
                        else
                        {
                            e.Handled = true;
                        }
                        break;
                    }
            }
        }

        private void gvShedule_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            var record = gvShedule.GetRow(e.RowHandle) as SheduleRecord;
            if (record == null) return;
            switch (e.Column.FieldName)
            {
                case "ZoneToGroup":
                    {
                        record.ZoneTo = null;
                        LoadComboZones(rcbZonesTo, record.ZoneToGroup);
                        break;
                    }
                case "VehicleGroup":
                    {
                        record.Vehicle = null;
                        LoadComboVehicles(record.VehicleGroup);
                        break;
                    }
                default:
                    {

                        if (record.IsNew)
                        {
                            record.Shedule = SheduleActive;
                        }
                        if (!record.Validate()) return;
                        record.Save();
                        break;
                    }
            }

        }

        private void gvShedule_InitNewRow(object sender, DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs e)
        {
            gvShedule.SetRowCellValue(e.RowHandle, gvShedule.Columns["TimePlanStart"], DateTime.Today);
        }

        private void gvShedule_CustomRowCellEdit(object sender, DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventArgs e)
        {

            if (gvShedule.FocusedColumn.FieldName == "ZoneTo" && gvShedule.ActiveEditor is ComboBoxEdit)
            {
                var record = gvShedule.GetRow(gvShedule.FocusedRowHandle) as SheduleRecord;
                if (record == null) return;
                LoadComboZones(rcbZonesTo, record.ZoneToGroup);
            }
            else if (gvShedule.FocusedColumn.FieldName == "Vehicle" && gvShedule.ActiveEditor is ComboBoxEdit)
            {
                var record = gvShedule.GetRow(gvShedule.FocusedRowHandle) as SheduleRecord;
                if (record == null) return;
                LoadComboVehicles(record.VehicleGroup);
            }
        }

        private void gvShedule_Click(object sender, EventArgs e)
        {
            SelecteVehicle(gvShedule.FocusedRowHandle);
        }
        #endregion

        #region PropertyGrid

        private void pgShedule_ValidatingEditor(object sender, DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs e)
        {
            
            if (pgShedule.FocusedRow.Name == "erDictionary")
            {
                if (SheduleActive.DictionaryShedule != null)
                    if (XtraMessageBox.Show(Resources.ActionConfirm,
                                            Resources.Routers1,
                                            MessageBoxButtons.YesNo,
                                            MessageBoxIcon.Question) != DialogResult.Yes)
                    {
                        e.Valid = false;
                    }
            }

        }
        private void pgShedule_CellValueChanged(object sender, DevExpress.XtraVerticalGrid.Events.CellValueChangedEventArgs e)
        {

            switch (e.Row.Name)
            {
                case "erZone":
                    {

                        SheduleActive.Save();
                        break;
                    }
                case "erRemark":
                    {
                        SheduleActive.Save();
                        break;
                    }
                case "erOutLinkId":
                    {
                        SheduleActive.Save();
                        break;
                    }
                case "erZoneGroup":
                    {
                        SheduleActive.ZoneMain = null;
                        LoadComboZones(rcbZones, SheduleActive.ZoneMainGroup);
                        break;
                    }
                case "erDictionary":
                    {
                        if (SheduleActive.SetFromDictionaryShedule())
                        {
                            SheduleActive.Save();
                            SetShedule(SheduleActive);
                        }
                        break;
                    }
            }

        }

        private void pgShedule_Enter(object sender, EventArgs e)
        {
            LoadComboDictionary();
        }
        #endregion

        public void RefreshDataSource()
        {
            gcShedule.RefreshDataSource();
        }

        void SetColumnsGridForRegime(int selectedIndex)
        {
            if (selectedIndex == 1)
            {
                colZoneToGroup.Visible = false; 
                colVehicleGroup.Visible = false;
                colDistance.Visible = false;
                colTimePlanStart.Visible = false;
                colTimeFactStart.Visible = false;
                colTimeFactEnd.Visible = false;
                colIsClose.Visible = false;  

            }
            else
            {
                colZoneToGroup.Visible = true;
                colVehicleGroup.Visible = true;
                colDistance.Visible = true;
                colTimePlanStart.Visible = true;
                colTimeFactStart.Visible = true;
                colTimeFactEnd.Visible = true;
                colIsClose.Visible = true;

                colZoneToGroup.VisibleIndex = 0;
                colZone.VisibleIndex = 1;
                colVehicleGroup.VisibleIndex = 2;
                colVehicle.VisibleIndex = 3;
                colDistance.VisibleIndex = 4;
                colTimePlanStart.VisibleIndex = 5;
                colTimePlanEnd.VisibleIndex = 6;
                colTimeFactStart.VisibleIndex = 7;
                colTimeFactEnd.VisibleIndex = 8;
                colState.VisibleIndex = 9;
                colIsClose.VisibleIndex = 10;
            }
        }

        private void rrgRegime_EditValueChanged(object sender, EventArgs e)
        {
            var rg = (RadioGroup)sender;
            SetColumnsGridForRegime(rg.SelectedIndex);
        }

        private void bbiNew_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            SetGraphNew();
        }

        private void beDateGraph_EditValueChanged(object sender, EventArgs e)
        {
            SetGraphToDate();
        }

        void SelecteVehicle(int rowHandle)
        {
            var record = gvShedule.GetRow(rowHandle) as SheduleRecord;
            if (record != null && OnlineController.Instance.IsStarted)
            {
                foreach (var routesChecker in OnlineController.Instance.RoutesCheckers)
                 {
                     routesChecker.OVehicle.IsSelected = routesChecker.SheduleRecordCheking == record;
                 }
                SelecteVehicleOnMap();
            }
        }

        public void ExcelExport()
        {
            string xslFilePath = StaticMethods.ShowSaveFileDialog(string.Format("{0} {1}", "Shedule" + StaticMethods.SuffixDateToFileName(), StaticMethods.SuffixDateToFileName()), "Excel", "Excel|*.xls"); ;
            if (xslFilePath.Length > 0)
            {
                var options = new XlsExportOptions {TextExportMode = TextExportMode.Text};
                colIsClose.Visible = false;
                gvShedule.OptionsPrint.ExpandAllDetails = true;
                gvShedule.OptionsPrint.PrintDetails = true;
                gvShedule.OptionsPrint.AutoWidth = true;
                gvShedule.ExportToXls(xslFilePath, options);
                System.Diagnostics.Process.Start(xslFilePath);
                colIsClose.Visible = true;
            }
           
            
        }

        private void chEdit_CheckedChanged(object sender, ItemClickEventArgs e)
        {
            if (chEdit.Checked)
            {
                gcShedule.UseEmbeddedNavigator = true;
                gvShedule.OptionsBehavior.Editable = true;
            }
            else
            {
                gcShedule.UseEmbeddedNavigator = false;
                gvShedule.OptionsBehavior.Editable = false;
            }
        }




    }
}
