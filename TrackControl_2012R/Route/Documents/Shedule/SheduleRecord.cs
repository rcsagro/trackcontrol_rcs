﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Agro.Properties;
using DevExpress.XtraEditors;
using Route.Online;
using TrackControl.MySqlDal;
using TrackControl.Zones;
using TrackControl.General;
using TrackControl.Vehicles;
using Route.DAL;
using Route.Properties;  

namespace Route.Documents.Shedule
{
    public class SheduleRecord:Entity
    {
        private ZonesGroup _zoneGroup;
        private VehiclesGroup  _vehicleGroup;
        public Shedule Shedule { get; set; }
        public IZone ZoneTo { get; set; }
        public Vehicle Vehicle { get; set; }
        public DateTime TimePlanStart { get; set; }
        public DateTime? TimePlanEnd{ get; set; }
        public DateTime? TimeFactStart { get; set; }
        public DateTime? TimeFactEnd { get; set; }
        public float Distance { get; set; } 
        public string State { get; set; }
        public bool IsClose { get; set; }

        public SheduleRecord()
        {

        }

        public ZonesGroup ZoneToGroup
        {
            get { return ZoneTo == null ? _zoneGroup : ZoneTo.Group; }
            set
            {
                _zoneGroup = value;
                ZoneTo = null;
            }
        }

        public VehiclesGroup VehicleGroup
        {
            get { return Vehicle == null ? _vehicleGroup : Vehicle.Group; }
            set
            {
                _vehicleGroup = value;
                Vehicle = null;
            }
        }

        public void Save()
        {
            SheduleRecordProvider.Save(this);
        }

        public bool Delete()
        {
            try
            {
                SheduleRecordProvider.DeleteOne(this);
                return true;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, Resources.Routers1);
                return false;
            }

        }

        public bool Validate()
        {
            if (Shedule != null && ZoneTo != null && Vehicle != null)
                return true;
            else
            {
                return false;
            }
        }
        
        public bool IsReadyForTracking()
        {
            if (IsClose || TimePlanEnd == null || Distance == 0 || Vehicle == null || TimeFactEnd  != null) 
                return false; 
            else if (IsTrackCompletedEarlier())
                return false;
            else
                return true;
        }



        bool IsTrackCompletedEarlier()
        {
            if (TimePlanEnd == null) return false;
            var timePlanEnd = (DateTime)TimePlanEnd;
            if (DateTime.Now > timePlanEnd.AddHours(-1))
            {
                var gpsDataProvider = new GpsDataProvider();
                var gpsDatas = gpsDataProvider.GetValidDataForPeriod(Vehicle.MobitelId, new DateTime(TimePlanStart.Year, TimePlanStart.Month, TimePlanStart.Day),
                                                                     timePlanEnd.AddHours(10));

                var gpsDataInZoneTo = gpsDatas.FirstOrDefault(gps => gps.Time > TimePlanStart && ZoneTo.Contains(gps.LatLng));  
                if (gpsDataInZoneTo != null)
                {
                    TimeFactEnd = gpsDataInZoneTo.Time;
                    var gpsDataBeforZoneTo = gpsDatas.Where(gps => gps.Time < TimeFactEnd && Shedule.ZoneMain.Contains(gps.LatLng)).OrderByDescending(gps => gps.Time).FirstOrDefault();
                    if (gpsDataBeforZoneTo != null)
                    {
                        TimeFactStart = gpsDataBeforZoneTo.Time;
                    }
                    State = "Завершен";
                    Save();
                    return true;
                }
            }
            return false;
        }
    }
}
