﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Route.Dictionaries;
using TrackControl.Zones;
using TrackControl.General; 
using Route.DAL;

namespace Route.Documents.Shedule
{
    public class Shedule:Entity
    {
        private ZonesGroup _zoneGroup;
        public IZone ZoneMain { get; set;}
        public ZonesGroup ZoneMainGroup
        {
            get { return ZoneMain == null ? _zoneGroup : ZoneMain.Group; }
            set { 
                _zoneGroup = value;
                ZoneMain = null;
            }
        }
        public DateTime DateShedule { get; set; }
        public string Remark { get; set; }
        public string OutLinkId { get; set; }
        public IList<SheduleRecord> SheduleRecords { get; set; }
        public DictionaryShedule DictionaryShedule { get; set; }


        public void Save()
        {
            SheduleProvider.Save(this);
            if (SheduleRecords != null)
            {
                foreach (var sheduleRecord in SheduleRecords)
                {
                    SheduleRecordProvider.Save(sheduleRecord);
                }
            }
        }

        public bool SetFromDictionaryShedule()
        {
            if (DictionaryShedule == null) return false;
            ZoneMain = DictionaryShedule.ZoneMain;
            if (SheduleRecords != null)
            {
                foreach (var sheduleRecord in SheduleRecords)
                {
                    sheduleRecord.Delete();
                }
                SheduleRecords.Clear();
            }

            SheduleRecords = new List<SheduleRecord>();
            foreach (var dictSheduleRecord in DictionaryShedule.DictionarySheduleRecords)
            {
                var sheduleRecord = new SheduleRecord
                    {
                        ZoneTo = dictSheduleRecord.ZoneTo,
                        Distance = dictSheduleRecord.Distance,
                        TimePlanStart = GetPlanTime(dictSheduleRecord.TimePlanStart),
                        TimePlanEnd = GetPlanTime(dictSheduleRecord.TimePlanEnd),
                        Shedule = this
                    };
                if (sheduleRecord.TimePlanEnd != null && sheduleRecord.TimePlanEnd != null && sheduleRecord.TimePlanEnd < sheduleRecord.TimePlanStart)
                    sheduleRecord.TimePlanEnd = ((DateTime)sheduleRecord.TimePlanEnd).AddDays(1);
                SheduleRecords.Add(sheduleRecord);
            }
            return true;
        }

        DateTime GetPlanTime(string dictTime)
        {
            int hours = Convert.ToInt32(dictTime.Substring(0, 2));
            int minutes =Convert.ToInt32(dictTime.Substring(3, 2));
            return new DateTime(DateShedule.Year, DateShedule.Month, DateShedule.Day, hours,  minutes,0);
        }
    }
}
