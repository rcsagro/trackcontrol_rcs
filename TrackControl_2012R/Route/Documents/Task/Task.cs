using Agro;
using Agro.Utilites;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.BandedGrid;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraVerticalGrid.Rows;
using Route.Properties;
using Route.XtraReports;
using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using TrackControl.General;
using TrackControl.General.DatabaseDriver;
using TUtil = Agro.Utilites.TotUtilites;
namespace Route
{
    public partial class Task : DevExpress.XtraEditors.XtraForm, IFormDictionary
    {
        DateTime dtTest = DateTime.Now;  
        /// <summary>
        /// ������ ������� - ������ ��� ��������������
        /// </summary>
        BandedGridView _gvGrn;
        bool bNewTask = false;
        /// <summary>
        /// ���������� ��������
        /// </summary>
        private bool bDeleteEnable;
        private bool bSaveEnable;
        private bool bFormLoading;
        private GraficMoving _GraficMoving;
        private DgrmGant _DgrmGant;
        /// <summary>
        /// ������ ����������� �����
        /// </summary>
        TaskItem _ti;
        #region ������������
        public Task()
        {
            //Debug.Print("2 " + string.Format("{0:tt}", DateTime.Now - dtTest)); 
            InitializeComponent();
            Localization();
            DicUtilites.LookUpLoad(leGroupe, "SELECT   team.id, team.Name FROM    team ORDER BY  team.Name");
            DicUtilites.LookUpLoad(leRoute, "SELECT   rt_sample.id, rt_sample.Name FROM    rt_sample WHERE rt_sample.IsGroupe = 0 ORDER BY  rt_sample.Name ");
            DicUtilites.LookUpLoad(leDriver, "SELECT   driver.id, driver.Family FROM    driver ORDER BY  driver.Family");
            DicUtilites.LookUpLoad(leType, "SELECT   rt_route_type.Id, rt_route_type.Name FROM    rt_route_type");
            SetControls();
        }
        public Task(ref BandedGridView gvGrn, bool bNew)
            : this()
        {
            //Debug.Print("1 " + string.Format("{0:tt}",DateTime.Now - dtTest)); 
            _gvGrn = gvGrn;
            if (bNew)
            {
                btSave.Caption = Resources.Create;
                deStart.DateTime = DateTime.Today;
                deEnd.DateTime = deStart.DateTime.AddDays(1).AddMinutes(-1) ;
                leType.EditValue = (int)Consts.RouteType.ChackZone;  
            }
            else
            {
                bFormLoading = true;
                SetStatus(Resources.DataLoaded);
                int _Id = (int)_gvGrn.GetRowCellValue(_gvGrn.FocusedRowHandle, _gvGrn.Columns["Id"]);
                txId.Text = _Id.ToString();
                _ti = new TaskItem(_Id);
                GetFields();
                LoadContent();
                btSave.Caption = Resources.Save;
                bFormLoading = false;
            }
            SetStatus(Resources.Ready);
            btSave.Enabled = false;
            TotUtilites.FormSize800_600(this); 
        }
        #endregion
        #region IFormDictionary Members
        public bool ValidateFields()
        {
            if (bFormLoading) return true;
            if (!DicUtilites.ValidateLookUp(leMobitel, dxErrP)
                || !DicUtilites.ValidateLookUp(leDriver, dxErrP)
                || !DicUtilites.ValidateDateEditPair(deStart,deEnd, dxErrP)
                || !DicUtilites.ValidateLookUp(leType, dxErrP))
            {
                btSave.Enabled = false;
                return false;
            }
            else
            {
                btSave.Enabled = true;
                return true;
            }
        }
        public void SaveChanges(bool question)
        {
            //DeleteDocContent
            if (!ValidateFields()) return;
            if (txId.Text == "0")
            {
                if (question)
                {
                    if (DialogResult.Yes != XtraMessageBox.Show(Resources.RouteCreateConfirm , Resources.Routers1, MessageBoxButtons.YesNoCancel)) 
                        return;
                }
                    SetStatus(Resources.RouteListCreate);
                    int iRoute = (leRoute.EditValue == null ? 0 : (int)leRoute.EditValue);
                    _ti = new TaskItem(deStart.DateTime,deEnd.DateTime ,  iRoute, (int)leMobitel.EditValue, (int)leDriver.EditValue, (short)Convert.ToInt16(leType.EditValue));
                    SetFields(); 
                        _ti.ChangeStatusEvent += SetStatus;
                        txId.Text = _ti.AddDoc().ToString();
                        if (txId.Text == "0")
                        {
                            SetStatus(Resources.DemoRectrict);
                            return;
                        }
                        if (iRoute > 0)
                        {
                            if (_ti.ContentCreateFromSample() > 0)
                            {
                                if (_ti.ContentAutoFillFact())
                                {
                                    GetFields();
                                }
                            }
                            LoadContent();
                        }
                        _ti.ChangeStatusEvent -= SetStatus;
                        btSave.Caption = Resources.Save;
                        btDelete.Enabled = true;
                        SetStatus(Resources.Ready);
                        bNewTask = true;
            }
            else
            {
                if (question)
                {
                    if (DialogResult.Yes != XtraMessageBox.Show(Resources.RouteListSaveConfirm , Resources.Routers1, MessageBoxButtons.YesNoCancel)) 
                        return;
                }
                   SetStatus(Resources.RouteListRefresh);
                bool bUpdateContent = (_ti.Date != deStart.DateTime) && (_ti.Type == (short)Consts.RouteType.ChackZone);

                // ��� ����� ���� ����������� ����� ����� �������� ���������� - ������ ������ � ������������ ������
                        if (_ti.Type != (short)Convert.ToInt32(leType.EditValue))
                        {
                            _ti.DeleteDocContent();
                            bUpdateContent = true;
                        }    
                        SetFields();
                        _ti.UpdateDoc();
                        if (bUpdateContent)
                        {
                            LoadContent();
                        }
                        GetFields();
                        SetStatus(Resources.Ready);

            }
            SetControls();
        }
        public bool TestLinks()
        {
            return true;
        }
        public bool DeleteRecord(bool question)
        {
            if (_ti.DeleteDoc(question))
                {
                    if (!bNewTask)
                    {
                        DataRow row = _gvGrn.GetDataRow(_gvGrn.FocusedRowHandle);
                        row.Delete();
                    }
                    return true;
                }
                else
                    return false;
        }
        public void SetControls()
        {
            if (leType.EditValue != null)
            {
                if (Convert.ToInt32(leType.EditValue) == (int)Consts.RouteType.ChackZone)
                    gcContent.MainView = gvContent;
                else
                {
                    gcContent.MainView = gvContentLoc;
                    LockSample(true);
                }
            }
            if (gcContent.MainView.RowCount == 0)
            {
                short stType = Convert.ToInt16(leType.EditValue ?? 0);
                if (stType == (short)Consts.RouteType.ChackZone)
                {
                    LockSample (false); 
                }
                else
                {
                    LockSample(true);
                }
                    btMapView.Enabled = false;
            }
            else
            {
                LockSample(false); 
                btMapView.Enabled = true;
            }
            if (txId.Text == "0")
            {
                bsiExcel.Enabled = false;
                btFact.Enabled = false;
                btDelete.Enabled = false;
                chEdit.Enabled = false;
                btAddSimple.Enabled = false;
                btDeleteSimple.Enabled = true;
                btRecalcFuel.Enabled = false; 
            }
            else
            {
                bsiExcel.Enabled = true;
                btFact.Enabled = true;
                btDelete.Enabled = true;
                chEdit.Enabled = true;
           }
            switch (tbCont.SelectedTabPageIndex)
            {
                case 0:
                    {
                        barGrid.Visible = true;
                        btClear.Visibility = BarItemVisibility.Always;
                        btRecalcFuel.Visibility = BarItemVisibility.Never;
                        break;
                    }
                case 1:
                    {
                        barGrid.Visible = false;
                        break;
                    }
                case 2:
                    {
                        barGrid.Visible = false;
                        break;
                    }
                case 3:
                    {
                        barGrid.Visible = true;
                        btClear.Visibility = BarItemVisibility.Never;
                        btRecalcFuel.Visibility = BarItemVisibility.Always;
                        break;
                    }
                case 4:
                    {
                        if ((_GraficMoving == null) && (txId.Text != "0"))
                        {
                            _GraficMoving = new GraficMoving {Dock = DockStyle.Fill};
                            pcGraficMoving.Controls.Add(_GraficMoving);
                            _GraficMoving.DataSourse = GetGraficDataSource((DataTable)gcContent.DataSource);  
                        }
                        barGrid.Visible = false;
                        break;
                    }
                case 5:
                    {
                        if ((_DgrmGant == null) && (txId.Text != "0"))
                        {
                            _DgrmGant = new DgrmGant();
                            _DgrmGant.Dock = DockStyle.Fill;
                            pcDgrmGant.Controls.Add(_DgrmGant);
                            _DgrmGant.DataSourse = GetDgrmGantDataSource((DataTable)gcContent.DataSource);
                        }
                        barGrid.Visible = false;
                        break;
                    }
            }

        }
        public void GetFields()
        {
            bool bSaveEnable = btSave.Enabled;
            using (var tiProp = new TaskItem(Convert.ToInt32(txId.Text)))
            {
                leGroupe.EditValue = tiProp.TeamId;
                SetLeMobitel();
                leMobitel.EditValue = tiProp.MobitelId;
                meRemark.Text = tiProp.Remark;
                txOutLinkId.Text = tiProp.IdOutLink;
                leRoute.EditValue = tiProp.SampleId;
                leDriver.EditValue = tiProp.DriverId;
                leType.EditValue = tiProp.Type;
                deStart.DateTime = tiProp.Date;
                deEnd.DateTime = tiProp.DateEnd; 
                pgTask.SelectedObject = tiProp;
                stateIndicatorComponent1.StateIndex = tiProp.PointsValidityGauge;
            }
            SetControls();
            btSave.Enabled = bSaveEnable;
        }
        public bool ValidateFieldsAction()
        {
            if (!ValidateFields())
            {
                btSave.Enabled = false;
                return false;
            }
            else
            {
                btSave.Enabled = true;
                return true;
            }
        }
        public void SetFields()
        {
            if (_ti != null)
            {
                _ti.Date = deStart.DateTime;
                _ti.DateEnd = deEnd.DateTime;
                _ti.MobitelId = (int)leMobitel.EditValue;
                _ti.SampleId = leRoute.EditValue == null ? 0 : (int)leRoute.EditValue;
                _ti.DriverId = (int)leDriver.EditValue;
                _ti.Remark = meRemark.Text;
                _ti.TeamId = (int)leGroupe.EditValue;
                _ti.Type = (short)Convert.ToInt16(leType.EditValue);
                _ti.IdOutLink = txOutLinkId.Text;
            }
        }
        public void UpdateGRN()
        {
            throw new Exception("The method or operation is not implemented.");
        }
        #endregion
        #region ������
        private void btFact_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (DialogResult.No == XtraMessageBox.Show(Resources.RouteFactCalcConfirm, Resources.Routers1, MessageBoxButtons.YesNo)) return;
            if (chEdit.Checked) chEdit.Checked = false;
            SaveChanges(false);
            SetStatus(Resources.RouteFactReceive);
            Application.DoEvents();
            LockMenuBar(true);
            bool bUpdateContent = false;
            _ti.ChangeStatusEvent += SetStatus;
            switch (Convert.ToInt32(leType.EditValue))
            {
                case (int)Consts.RouteType.ChackZone:
                    {
                        if (_ti.ContentAutoFillFact()) bUpdateContent = true;
                        break;
                    }
                case (int)Consts.RouteType.Location:
                    {
                        _ti.ChangeProgressBar += SetProgress;
                        if (_ti.ContentAutoFillFactLocation()) bUpdateContent = true;
                        _ti.ChangeProgressBar -= SetProgress;
                        break;
                    }
            }
            _ti.ChangeStatusEvent -= SetStatus;
            if (bUpdateContent)
            {
                LoadContent();
                GetFields();
            }
            LockMenuBar(false);
        }
        private void btExcel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (DialogResult.No == XtraMessageBox.Show(Resources.ExcelOutConfirm, Resources.Routers1, MessageBoxButtons.YesNo)) return;
            string sFileName = tbCont.SelectedTabPage.Text + "_" + txId.Text + StaticMethods.SuffixDateToFileName();
            GridView gvPrint = gvContent;
            IChartControl IChart = (IChartControl)_GraficMoving;
            bool bPrintChart = false;
            switch (tbCont.SelectedTabPageIndex)
            {
                case 0:
                    {
                        gvPrint = gvContent;
                        break;
                    }
                case 1:
                    {
                        gvPrint = gvStop;
                        break;
                    }
                case 2:
                    {
                        gvPrint = gvSensor;
                        break;
                    }
                case 3:
                    {
                        gvPrint = gvFuel;
                        break;
                    }
                case 4:
                    {
                        bPrintChart = true;
                        IChart = (IChartControl)_GraficMoving;
                        break;
                    }
                case 5:
                    {
                        bPrintChart = true;
                        IChart = (IChartControl)_DgrmGant;
                        break;
                    }
            }

            string xslFilePath = StaticMethods.ShowSaveFileDialog(sFileName, "Excel", "Excel|*.xls");
            if (xslFilePath.Length > 0)
            {
                try
                {
                    if (bPrintChart)
                    {
                        IChart.ExportToXls(xslFilePath); 
                    }
                    else
                    {
                        gvPrint.OptionsPrint.ExpandAllDetails = true;
                        gvPrint.OptionsPrint.PrintDetails = true;
                        gvPrint.OptionsPrint.AutoWidth = false;
                        gvPrint.BestFitColumns();
                        gvPrint.OptionsPrint.AutoWidth = true;
                        gvPrint.ExportToXls(xslFilePath);
                    }
                    System.Diagnostics.Process.Start(xslFilePath);
                }
                catch (Exception)
                {
                    XtraMessageBox.Show(Resources.FileRewriteBan, Resources.Routers1, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        private void btExcelTotal_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (DialogResult.No == XtraMessageBox.Show(Resources.ExcelOutConfirm, Resources.Routers1, MessageBoxButtons.YesNo)) return;
            string sFileName = "RouteList_" + txId.Text + StaticMethods.SuffixDateToFileName();  

            string xslFilePath = StaticMethods.ShowSaveFileDialog(sFileName, "Excel", "Excel|*.xls");
            if (xslFilePath.Length > 0)
            {
                try
                {
                    foreach (BaseRow br in pgTask.Rows)
                    {
                        br.Expanded = true;
                    }
                    pgTask.ExportToXls(xslFilePath);
                    System.Diagnostics.Process.Start(xslFilePath);
                }
                catch (Exception)
                {
                    XtraMessageBox.Show(Resources.FileRewriteBan, Resources.Routers1, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        private void btMapView_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (leMobitel.EditValue != null)
            {
                if (DialogResult.No == XtraMessageBox.Show(Resources.RouteViewMapConfirm, Resources.Routers1, MessageBoxButtons.YesNo)) return;
                this.WindowState = FormWindowState.Minimized;
                foreach (Form frm in Application.OpenForms)
                {
                    if (frm.GetType().ToString() == "Route.MainRoute")
                    {
                        frm.WindowState = FormWindowState.Minimized;
                    }
                }
                Form_Utils.DrawEventArgs ea = new Form_Utils.DrawEventArgs(deStart.DateTime,deEnd.DateTime ,  (int)leMobitel.EditValue);
                Form_Utils.DrawMapEventHandler(this, ea);
            }
        }
        private void btSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            SaveChanges(true);
            btSave.Enabled = false;
        }
        private void btDelete_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (DeleteRecord(true)) this.Close();
        }
        private void btAddSimple_Click(object sender, EventArgs e)
        {
            if ((leRoute.EditValue == null) || ((int)leRoute.EditValue == 0))
            {
                XtraMessageBox.Show(Resources.RouteSampleSelect, Resources.Routers1);
                return;
            }
            if (DialogResult.No == XtraMessageBox.Show(Resources.RouteAddConfirm, Resources.Routers1, MessageBoxButtons.YesNo)) return;
            SaveChanges(false);
            _ti.ChangeStatusEvent += SetStatus;
            LockMenuBar(true);
            if (_ti.ContentCreateFromSample() > 0)
            {
                if (_ti.ContentAutoFillFact())
                {
                    GetFields();
                }
            }
            LoadContent();
            bsiStatus.Caption = Resources.Ready;
            _ti.ChangeStatusEvent -= SetStatus;
            LockMenuBar(false);
        }
        private void btDeleteSimple_Click(object sender, EventArgs e)
        {
            leRoute.EditValue = null;
            SetFields();
            LockSampleCreateButton(); 
            ValidateFieldsAction();
        }
        private void btRefresh_ItemClick(object sender, ItemClickEventArgs e)
        {
            GetFields();
            LoadContent();
        }
        private void btReport_ItemClick(object sender, ItemClickEventArgs e)
        {
                TaskTotal report = new TaskTotal(Convert.ToInt32(txId.Text));
                //report.ShowPreview(); aketner
        }
        private void btReportZone_ItemClick(object sender, ItemClickEventArgs e)
        {
            TaskZones report = new TaskZones(Convert.ToInt32(txId.Text));
            //report.ShowPreview(); aketner
        }
        private void btReportStop_ItemClick(object sender, ItemClickEventArgs e)
        {
            TaskStops report = new TaskStops(Convert.ToInt32(txId.Text));
            //report.ShowPreview(); aketner
        }
        private void btReportSensor_ItemClick(object sender, ItemClickEventArgs e)
        {
            TaskSensors report = new TaskSensors(Convert.ToInt32(txId.Text));
            //report.ShowPreview(); aketner
        }
        private void btReportFuel_ItemClick(object sender, ItemClickEventArgs e)
        {
            TaskFuel report = new TaskFuel(Convert.ToInt32(txId.Text));
            //report.ShowPreview(); aketner
        }
        private void btRecalcFuel_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (DialogResult.No == XtraMessageBox.Show(Resources.FuelRecalcConfirm, Resources.Routers1, MessageBoxButtons.YesNo)) return;
            _ti.ChangeStatusEvent += SetStatus;
            LockMenuBar(true);
            if (_ti.ContentRefreshFuelDUT())
            {
                LoadFuelDut();
                GetFields();
            }
            SetStatus(Resources.Ready);
            _ti.ChangeStatusEvent -= SetStatus;
            LockMenuBar(false);
        }
        private void btTamplate_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (_ti == null)
            {
                return;
            }
            if (DialogResult.No == XtraMessageBox.Show(Resources.RouteCreateFromSampleConfirm , Resources.Routers1, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2)) return ;
            if (_ti.CreateTamplate())
            {

                DicUtilites.LookUpLoad(leRoute, "SELECT   rt_sample.id, rt_sample.Name FROM    rt_sample WHERE rt_sample.IsGroupe = 0 ORDER BY  rt_sample.Name ");
                GetFields();
            }
        }
        #endregion
        #region ��������� ��������

        private void leGroupe_EditValueChanged(object sender, EventArgs e)
        {
            SetLeMobitel();
            ValidateFieldsAction();
        }
        private void leDriver_EditValueChanged(object sender, EventArgs e)
        {
            ValidateFieldsAction();
        }
        private void leRoute_EditValueChanged(object sender, EventArgs e)
        {
            ValidateFieldsAction();
        }
        private void leMobitel_EditValueChanged(object sender, EventArgs e)
        {

            if (DicUtilites.ValidateLookUp(leMobitel, dxErrP))
            {
                int iDriver = DicUtilites.GetDriverId_Mobitel((int)leMobitel.EditValue);
                if (iDriver > 0) leDriver.EditValue = iDriver;
            }
            ValidateFieldsAction();
        }
        private void deStart_EditValueChanged(object sender, EventArgs e)
        {
            ValidateFieldsAction();
        }
        private void deStart_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
            {
                ValidateFieldsAction();
            }
        }
        private void deEnd_EditValueChanged(object sender, EventArgs e)
        {
            ValidateFieldsAction();
        }
        private void deEnd_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
            {
                ValidateFieldsAction();
            }
        }
        private void leType_EditValueChanged(object sender, EventArgs e)
        {
            SetControls();
            ValidateFieldsAction();
        }
        private void meRemark_EditValueChanged(object sender, EventArgs e)
        {
            ValidateFieldsAction();
        }
        private void SetLeMobitel()
        {
            leMobitel.EditValue = null;
            if (DicUtilites.ValidateLookUp(leGroupe, dxErrP))
            {
                DicUtilites.LookUpLoad(leMobitel, "SELECT   vehicle.Mobitel_id, " + AgroQuery.SqlVehicleIdent  + " AS F, vehicle.Team_id FROM   vehicle"
                + " WHERE   (vehicle.Team_id = " + leGroupe.EditValue.ToString() + " OR vehicle.Team_id IS NULL) ORDER BY " + AgroQuery.SqlVehicleIdent  );
            }
        }
        private void SetStatus(string sMessage)
        {
            bsiStatus.Caption = sMessage;
        }
        private string GetStatus()
        {
            return (bsiStatus.Caption);
        }
        private void SetProgress(int iValue)
        {
            TotUtilites.SetProgressBar(pbStatus, iValue);   
        }
        private void tbCont_Click(object sender, EventArgs e)
        {
            SetControls();
        }
        private void Task_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (GetStatus() != Resources.Ready)
            {
                e.Cancel = true;
                return;
            }
            if (_ti != null) _ti.Dispose();
        }
        private void LockMenuBar(bool bLock)
        {
            btMapView.Enabled = !bLock;
            btDelete.Enabled = !bLock;
            bsiExcel.Enabled = !bLock;
            btFact.Enabled = !bLock;
            btRefresh.Enabled = !bLock;
            btSave.Enabled = !bLock;
            bsiReports.Enabled = !bLock;
            if (bLock)
            {
                bDeleteEnable = btDelete.Enabled;
                bSaveEnable = btSave.Enabled;
            }
            else
            {
                btDelete.Enabled = bDeleteEnable;
                btSave.Enabled = bSaveEnable;
            }
        }
        private void LockSample(bool bLock)
        {
            leRoute.Enabled = !bLock;
            btAddSimple.Enabled = !bLock;
            btDeleteSimple.Enabled = !bLock;
        }
        private void LockSampleCreateButton()
        {
            if ((_ti != null ) && ((_ti.SampleId != 0) || (_ti.Type == (short)Consts.RouteType.Location) || (gvContent.RowCount == 0)))
            {
                btTamplate.Enabled = false;
            }
            else
                btTamplate.Enabled = true;
        }
        #endregion
        #region ���������� ��������
        /// <summary>
        /// �������� ������ ����������� � ���������� ������������ �����
        /// </summary>
        private void LoadContent()
        {
            if (_ti == null)
            {
                return;
            }
            LoadSubForm();
            LoadStops();
            LoadSensors();
            LoadFuelDut();
            SetControls();
            LockSampleCreateButton(); 
        }
        /// <summary>
        /// ������ ��� ����� ���������� �������� + ������� �������� + ��������� �����
        /// </summary>
        private void LoadSubForm()
        {

 
            //using (ConnectMySQL cnMySQL = new ConnectMySQL())
            DriverDb db = new DriverDb();
            db.ConnectDb();
            {
                DataTable dtContent = _ti.GetContent();
                gcContent.DataSource = dtContent;
                string sSQLselect = " SELECT  zones.Zone_ID as Id, zones.Name, zones.ZonesGroupId FROM   zones ORDER BY zones.Name";
                //ZoneLookUp.DataSource = cnMySQL.GetDataTable(sSQLselect);
                ZoneLookUp.DataSource = db.GetDataTable( sSQLselect );
                sSQLselect = " SELECT  rt_events.Id, rt_events.Name FROM   rt_events ORDER BY rt_events.Name";
                //EventLookUp.DataSource = cnMySQL.GetDataTable(sSQLselect);
                EventLookUp.DataSource = db.GetDataTable( sSQLselect );
            }
            db.CloseDbConnection();
        }
        /// <summary>
        /// �������� ������ ��� ��������
        /// </summary>
        /// <param name="dtContent"></param>
        private DataTable GetGraficDataSource(DataTable dtContent)
        {
            if (dtContent == null) return null;
            DataTable dtGrafic = dtContent.Copy();
            dtGrafic.Columns.Add("DistancePlan", typeof(double));
            dtGrafic.Columns["P"].ReadOnly = false;
            if (dtGrafic.Rows.Count > 0)
            {
                for (int i = 0; i < dtGrafic.Rows.Count; i++)
                {
                    dtGrafic.Rows[i]["DistancePlan"] = 0;
                    dtGrafic.Rows[i]["P"] = dtGrafic.Rows[i]["P"] + " [" + (i + 1) + "]";
                }
            }
            return dtGrafic;
        }
        private DataTable GetDgrmGantDataSource(DataTable dtContent)
        {
            if (dtContent == null) return null;
             DataTable dtGant = new DataTable();
            dtGant.Columns.AddRange(new DataColumn[]{
            new DataColumn("P", typeof(string)),     
            new DataColumn("DatePlan", typeof(DateTime)),
            new DataColumn("DatePlanNext", typeof(DateTime)),
            new DataColumn("DateFact", typeof(DateTime)),
            new DataColumn("DateFactNext", typeof(DateTime))});
            if (dtContent.Rows.Count > 0)
            {
                DateTime dtPrev = DateTime.Today;
                DateTime dtPrevFact = DateTime.Today;
                for (int i = 0; i < dtContent.Rows.Count; i++)
                {
                    //��������� �����-----������ ���� 2 ���� ��� ������ ������� ����� � ����� --------------------------------
                    if (dtContent.Rows[i]["DateFact"].ToString().Length > 0)
                    {
                        if (dtContent.Rows[i]["DatePlan"].ToString().Length > 0)
                            dtGant.Rows.Add(new object[] { dtContent.Rows[i]["P"], (i == 0)? dtContent.Rows[i]["DatePlan"] : dtPrev, 
                                        dtContent.Rows[i]["DatePlan"],  (i == 0) ? dtContent.Rows[i]["DateFact"] : dtPrevFact, dtContent.Rows[i]["DateFact"] });
                        else
                            dtGant.Rows.Add(new object[] { dtContent.Rows[i]["P"], null, null, (i == 0) ? dtContent.Rows[i]["DateFact"] : dtPrevFact, dtContent.Rows[i]["DateFact"] });
                    }
                    else
                        dtGant.Rows.Add(new object[] { dtContent.Rows[i]["P"], (i == 0) ? dtContent.Rows[i]["DatePlan"] : dtPrev, dtContent.Rows[i]["DatePlan"], null, null });
                    if (dtContent.Rows[i]["DatePlan"].ToString().Length > 0)
                        DateTime.TryParse(dtContent.Rows[i]["DatePlan"].ToString(), out dtPrev);
                    if (dtContent.Rows[i]["DateFact"].ToString().Length > 0)
                        DateTime.TryParse(dtContent.Rows[i]["DateFact"].ToString(), out dtPrevFact);
                    //------------------------------------------------------------------------------------------------------------
                }
            }
            return dtGant;
        }
        /// <summary>
        /// ���������� �� ���������� �� ��������
        /// </summary>
        private void LoadStops()
        {
                gcStop.DataSource = _ti.GetStops() ;
        }
        /// <summary>
        /// ������������ �������� �� ��������
        /// </summary>
        private void LoadSensors()
        {
            gcSensor.DataSource = _ti.GetSensors(); 
        }
        /// <summary>
        /// �������� - �����
        /// </summary>
        private void LoadFuelDut()
        {
            gcFuel.DataSource = _ti.GetFuelDUT();
        }
        /// <summary>
        /// ������� ������� ���������� ����� (�� �� �������)
        /// </summary>
        /// <returns></returns>
        private bool PlanPresent()
        {
            DataTable dtContent = gcContent.DataSource as DataTable;
            if (dtContent == null) return false;
            if (dtContent.Rows.Count == 0) return false;

            foreach (DataRow dr in dtContent.Rows)
            {
                if (dr.RowState != DataRowState.Deleted)
                {
                    if (!(dr["DatePlan"] is DateTime))
                        return false;
                }
            }
            return true;
        }
        #endregion
        #region ���������� ���������� ��������
        private void chEdit_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (chEdit.Checked)
            {
                //-------- ���������� ----------------------
                gcContent.UseEmbeddedNavigator = true;
                LoadContent(); 
                Id_zone.OptionsColumn.AllowEdit = true;
                Id_event.OptionsColumn.AllowEdit = true;
                DatePlan.OptionsColumn.AllowEdit = true;
                Remark.OptionsColumn.AllowEdit = true;
                btClear.Enabled = true;
                //-------- ������� ------------------------
                gcFuel.UseEmbeddedNavigator = true;
                Location_f.OptionsColumn.AllowEdit = true;
                time_.OptionsColumn.AllowEdit = true;
                ValueHandle.OptionsColumn.AllowEdit = true;
                btRecalcFuel.Enabled = true;
            }
            else
            {
                //-------- ���������� ----------------------
                gcContent.UseEmbeddedNavigator = false;
                gcFuel.UseEmbeddedNavigator = false;
                Id_zone.OptionsColumn.AllowEdit = false;
                Id_event.OptionsColumn.AllowEdit = false;
                DatePlan.OptionsColumn.AllowEdit = false;
                Remark.OptionsColumn.AllowEdit = false;
                btClear.Enabled = false;
                //-------- ������� ------------------------
                gcFuel.UseEmbeddedNavigator = false;
                Location_f.OptionsColumn.AllowEdit = false;
                time_.OptionsColumn.AllowEdit = false;
                ValueHandle.OptionsColumn.AllowEdit = false;
                btRecalcFuel.Enabled = false;
            }
        }

        private void btClear_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (DialogResult.No == XtraMessageBox.Show(Resources.RouteListClearConfirm , Resources.Routers1, MessageBoxButtons.YesNo)) return;
            if (_ti.DeleteDocContent())
            {
                LoadContent();
                //����� ���������� ������ � ������� ����
                stateIndicatorComponent1.StateIndex = 1;
                chEdit.Checked = false;

            }
        }
        private void gvContent_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (txId.Text == "0") return;
            int iID = 0;
            GridView gv = (GridView)gcContent.FocusedView;
            if (Int32.TryParse(gv.GetRowCellValue(e.RowHandle, gv.Columns["Id"]).ToString(), out iID))
            {
                if (iID == 0) return;
                if (e.Column.FieldName == "Remark")
                {
                    //using (ConnectMySQL cnMySQL = new ConnectMySQL())
                    DriverDb db = new DriverDb();
                    db.ConnectDb();
                    {
                        string sSQLupdate = "UPDATE rt_routet SET Remark = " + db.ParamPrefics + "Remark" + " WHERE (Id = " + iID + ")";
                        //MySqlParameter[] parDate = new MySqlParameter[1];
                        db.NewSqlParameterArray(1);
                        //parDate[0] = new MySqlParameter("?Remark", MySqlDbType.String);
                        db.NewSqlParameter( db.ParamPrefics + "Remark", db.GettingString(), 0);
                        //parDate[0].Value = e.Value;
                        db.SetSqlParameterValue(e.Value, 0);
                        db.ExecuteNonQueryCommand(sSQLupdate, db.GetSqlParameterArray);
                    } // using
                    db.CloseDbConnection();
                } // if
                else if (e.Column.FieldName == "DatePlan")
                {
                    DateTime dtPlan;
                    if (DateTime.TryParse(e.Value.ToString(), out dtPlan))
                    {
                       // using (ConnectMySQL _cnMySQL = new ConnectMySQL())
                        DriverDb db = new DriverDb();
                        db.ConnectDb();
                        {
                            string sSQLupdate = "UPDATE rt_routet SET DatePlan = " + db.ParamPrefics + "DatePlan";
                            DateTime dtFact;
                            if (DateTime.TryParse(gv.GetRowCellValue(e.RowHandle, gv.Columns["DateFact"]).ToString(), out dtFact))
                            {
                                TimeSpan tsDev = dtFact.Subtract(dtPlan);
                                sSQLupdate += " ,Deviation  ='" + tsDev + "'";
                                gv.SetRowCellValue(e.RowHandle, gv.Columns["Deviation"], tsDev.ToString());
                            }
                            sSQLupdate += " WHERE (Id = " + iID + ")";
                           // MySqlParameter[] parDate = new MySqlParameter[1];
                            db.NewSqlParameterArray(1);
                            //parDate[0] = new MySqlParameter("?DatePlan", MySqlDbType.DateTime);
                            db.NewSqlParameter( db.ParamPrefics + "DatePlan", db.GettingDateTime(), 0);
                            //parDate[0].Value = dtPlan;
                            db.SetSqlParameterValue(dtPlan, 0);
                            //_cnMySQL.ExecuteNonQueryCommand(sSQLupdate, parDate);
                            db.ExecuteNonQueryCommand( sSQLupdate, db.GetSqlParameterArray );
                        }
                        db.CloseDbConnection();
                    }

                }
                else if (e.Column.FieldName == "Id_event")
                {
                    //using (ConnectMySQL cnMySQL = new ConnectMySQL())
                    DriverDb db = new DriverDb();
                    db.ConnectDb();
                    {
                        string sSQLupdate = "UPDATE rt_routet SET Id_event = " + e.Value + " WHERE (Id = " + iID + ")";
                        //cnMySQL.ExecuteNonQueryCommand(sSQLupdate);
                        db.ExecuteNonQueryCommand( sSQLupdate );
                    }
                    db.CloseDbConnection();
                }
                else if (e.Column.FieldName.ToString() == "Id_zone")
                {
                    //using (ConnectMySQL cnMySQL = new ConnectMySQL())
                    DriverDb db = new DriverDb();
                    db.ConnectDb();
                    {
                        string sSQLupdate = "UPDATE rt_routet SET Id_zone = " + e.Value + " WHERE (Id = " + iID + ")";
                        //cnMySQL.ExecuteNonQueryCommand(sSQLupdate);
                        db.ExecuteNonQueryCommand( sSQLupdate );
                    }
                    db.CloseDbConnection();
                }

            }
        }
        private void gvContent_RowCellStyle(object sender, RowCellStyleEventArgs e)
        {
            if (e.RowHandle >= 0)
            {
                if (e.Column.FieldName == "Deviation")
                {
                    string sValue = gvContent.GetRowCellValue(e.RowHandle, gvContent.Columns["Deviation"]).ToString();
                    if ((sValue != "00:00:00") && (sValue.Length > 0) && (sValue != "0"))
                    {
                        if (sValue.Substring(0, 1) == "-")
                            e.Appearance.ForeColor = Color.Green;
                        else
                            e.Appearance.ForeColor = Color.Red;
                    }

                }
            }
        }
        private void gvContent_InitNewRow(object sender, InitNewRowEventArgs e)
        {
            if (txId.Text == "0") return;
            DataRow row = gvContent.GetDataRow(e.RowHandle);
            int ZoneId =0;
            if (gvContent.RowCount > 1)
            {
                // ������� �����������
                if (((int)gvContent.GetRowCellValue(gvContent.RowCount - 2, "Id_event") == (int)Consts.RouteEvent.Entry))
                {
                    row["Id_event"] = (int)Consts.RouteEvent.Exit;
                }
                else
                {
                    row["Id_event"] = (int)Consts.RouteEvent.Entry;
                }
                if (Int32.TryParse(gvContent.GetRowCellValue(gvContent.RowCount - 2, "Id_zone").ToString(), out  ZoneId))
                    row["Id_zone"] = ZoneId;
                else
                    row["Id_zone"] = 0;
                //���� ��������� ����������
                DateTime PrevDate;
                if (DateTime.TryParse(gvContent.GetRowCellValue(gvContent.RowCount - 2, "DatePlan").ToString(), out  PrevDate))
                    row["DatePlan"] = PrevDate;
            }
            else
            {
                row["Id_event"] = (int)Consts.RouteEvent.Entry;
                row["DatePlan"] = deStart.DateTime;
                row["Id_zone"] = 0;
            }
            row["Id_main"] = txId.Text;
            row["Id"] = ConstsGen.RECORD_MISSING;
            SetControls();
        }
        private void gcContent_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            int iIdRecord = 0;
            GridView gv = (GridView)gcContent.FocusedView;
            if (e.Button.ButtonType == DevExpress.XtraEditors.NavigatorButtonType.Remove)
            {
                if (Int32.TryParse(gv.GetRowCellValue(gv.FocusedRowHandle, gv.Columns["Id"]).ToString(), out iIdRecord))
                {
                    if (DialogResult.Yes == XtraMessageBox.Show(Resources.DataDeleteConfirm, Resources.Routers1, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
                    {
                        //using (ConnectMySQL cnMySQL = new ConnectMySQL())
                        DriverDb db = new DriverDb();
                        db.ConnectDb();
                        {
                            string sSQLdelete = "DELETE FROM rt_routet WHERE rt_routet.Id = " + iIdRecord;
                            //cnMySQL.ExecuteNonQueryCommand(sSQLdelete);
                            db.ExecuteNonQueryCommand( sSQLdelete );
                        }
                        db.CloseDbConnection();
                        gv.RefreshData();
                        //LoadSubForm();
                        GetFields();
                    }
                    else
                    {
                        e.Handled = true;
                    }
                }
            }
            else if (e.Button.ButtonType == DevExpress.XtraEditors.NavigatorButtonType.EndEdit)
            {
                if (Int32.TryParse(gv.GetRowCellValue(gv.FocusedRowHandle, gv.Columns["Id"]).ToString(), out iIdRecord) && iIdRecord==ConstsGen.RECORD_MISSING)
                {
                    DataRow row = gvContent.GetDataRow(gv.FocusedRowHandle);
                   // using (ConnectMySQL cnMySQL = new ConnectMySQL())
                    DriverDb db = new DriverDb();
                    db.ConnectDb();
                    {
                        string sSQLinsert = string.Format("INSERT INTO rt_routet (Id_main ,Id_zone,Id_event,DatePlan)"
                        + " VALUES ({0},{1},{2},{3})", txId.Text, db.ParamPrefics + "Id_zone", db.ParamPrefics + "Id_event", db.ParamPrefics + "DatePlan");

                        //MySqlParameter[] parDate = new MySqlParameter[3];
                        db.NewSqlParameterArray(3);
                        //parDate[0] = new MySqlParameter("?Id_zone", MySqlDbType.Int32);
                        db.NewSqlParameter( db.ParamPrefics + "Id_zone", db.GettingInt32(), 0);
                        //parDate[0].Value = row["Id_zone"];
                        db.SetSqlParameterValue( row["Id_zone"], 0);
                        //parDate[1] = new MySqlParameter("?Id_event", MySqlDbType.Int32);
                        db.NewSqlParameter( db.ParamPrefics + "Id_event", db.GettingInt32(), 1);
                        //parDate[1].Value = row["Id_event"];
                        db.SetSqlParameterValue( row["Id_event"], 1);
                        //parDate[2] = new MySqlParameter("?DatePlan", MySqlDbType.DateTime);
                        db.NewSqlParameter( db.ParamPrefics + "DatePlan", db.GettingDateTime(), 2);
                        //parDate[2].Value = row["DatePlan"];
                        db.SetSqlParameterValue( row["DatePlan"], 2);
                        //row["Id"] = cnMySQL.ExecuteReturnLastInsert(sSQLinsert, parDate);
                        row["Id"] = db.ExecuteReturnLastInsert( sSQLinsert, db.GetSqlParameterArray, "rt_routet" );
                    }
                    db.CloseDbConnection();
                    gvContent.RefreshData();
                }
            }
        }
        private void gvContentLoc_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (txId.Text == "0") return;
            int iID = 0;
            GridView gv = (GridView)gcContent.FocusedView;
            if (Int32.TryParse(gv.GetRowCellValue(e.RowHandle, gv.Columns["Id"]).ToString(), out iID))
            {
                if (iID == 0) return;
                if (e.Column.FieldName.ToString() == "Remark")
                {
                    //using (ConnectMySQL cnMySQL = new ConnectMySQL())
                    DriverDb db = new DriverDb();
                    db.ConnectDb();
                    {
                        string sSQLupdate = "UPDATE rt_routet SET Remark = " + db.ParamPrefics + "Remark" + " WHERE (Id = " + iID + ")";
                        //MySqlParameter[] parDate = new MySqlParameter[1];
                        db.NewSqlParameterArray(1);
                        //parDate[0] = new MySqlParameter("?Remark", MySqlDbType.String);
                        db.NewSqlParameter( db.ParamPrefics + "Remark", db.GettingString(), 0);
                        //parDate[0].Value = e.Value;
                        db.SetSqlParameterValue(e.Value, 0);
                        //cnMySQL.ExecuteNonQueryCommand(sSQLupdate, parDate);
                        db.ExecuteNonQueryCommand( sSQLupdate, db.GetSqlParameterArray );
                    }
                    db.CloseDbConnection();
                }

            }
        }
        #endregion
        #region ���������� ���������� - �������
        private void gvFuel_InitNewRow(object sender, InitNewRowEventArgs e)
        {
            DataRow row = gvFuel.GetDataRow(e.RowHandle);
            row["dValueCalc"] = 0;
            row["dValueHandle"] = 0;
            if (gvFuel.RowCount > 1)
            {
                row["time_"] = gvFuel.GetRowCellValue(gvFuel.RowCount - 2, "time_"); ;
            }
            else
                row["time_"] = deStart.DateTime;

        }
        private void gvFuel_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (e.Column.FieldName.ToString() == "Id") return;
            int iIdRecord = 0;
            DateTime dtFuel;
            double dValueHandle;
            if (!Double.TryParse(gvFuel.GetRowCellValue(gvFuel.FocusedRowHandle, gvFuel.Columns["dValueHandle"]).ToString(), out dValueHandle)) return;
            if (DateTime.TryParse(gvFuel.GetRowCellValue(e.RowHandle, gvFuel.Columns["time_"]).ToString(), out dtFuel))
            {
                string Location = gvFuel.GetRowCellValue(gvFuel.FocusedRowHandle, gvFuel.Columns["Location"]).ToString();
                if (Int32.TryParse(gvFuel.GetRowCellValue(e.RowHandle, gvFuel.Columns["Id"]).ToString(), out iIdRecord))
                {
                    _ti.SetFuelDUT(iIdRecord, dtFuel, dValueHandle, Location); 
                }
                else
                {
                    gvFuel.SetRowCellValue(e.RowHandle, gvFuel.Columns["Id"], _ti.SetFuelDUT(0, dtFuel, dValueHandle, Location)); 
                }
                if (e.Column.FieldName.ToString() == "dValueHandle") FuelRecalcTotals();
            }
        }
        private void gvFuel_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {
            DateTime dtFuel;
            double dValueHandle;
            if (!((Double.TryParse(gvFuel.GetRowCellValue(gvFuel.FocusedRowHandle, gvFuel.Columns["dValueHandle"]).ToString(), out dValueHandle))
                && (DateTime.TryParse(gvFuel.GetRowCellValue(gvFuel.FocusedRowHandle, gvFuel.Columns["time_"]).ToString(), out dtFuel))))
            {
                e.Valid = false;
            }
        }
        private void gvFuel_InvalidRowException(object sender, DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs e)
        {
            e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.NoAction;
        }
        private void gcFuel_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            if (e.Button.ButtonType == DevExpress.XtraEditors.NavigatorButtonType.Remove)
            {
                int iIdRecord = 0;
                if (Int32.TryParse(gvFuel.GetRowCellValue(gvFuel.FocusedRowHandle, gvFuel.Columns["Id"]).ToString(), out iIdRecord))
                {
                    if (DialogResult.Yes == XtraMessageBox.Show(Resources.DataDeleteConfirm, Resources.Routers1, MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question))
                    {
                     //   using (ConnectMySQL cnMySQL = new ConnectMySQL())
                        DriverDb db = new DriverDb();
                        db.ConnectDb();
                        {
                            string sSQLdelete = "DELETE FROM rt_route_fuel WHERE rt_route_fuel.Id = " + iIdRecord;
                            //cnMySQL.ExecuteNonQueryCommand(sSQLdelete);
                            db.ExecuteNonQueryCommand( sSQLdelete );
                        }
                        db.CloseDbConnection();
                        gvFuel.RefreshData(); 
                        FuelRecalcTotals();
                    }
                    else
                    {
                        e.Handled = true;
                    }
                }
            }
        }
        /// <summary>
        /// �������� ����� ����������� ������� ������� ����� �������������� ��������-������
        /// </summary>
        private void FuelRecalcTotals()
        {
            LoadFuelDut();
            if (gvFuel == null) return;
            Double dbZapr = 0;
            Double dbSliv = 0;
            int iZapr = 0;
            int iSliv = 0;
            Double dbValueHandle = 0;
            for (int i = 0; i < gvFuel.RowCount; i++)
            {
                if (Double.TryParse((gvFuel.GetRowCellValue(i, gvFuel.Columns["dValueHandle"]) ?? 0).ToString(), out dbValueHandle))
                {
                    if (dbValueHandle > 0)
                    {
                        dbZapr += dbValueHandle;
                        iZapr++;
                    }
                    else
                    {
                        dbSliv += dbValueHandle;
                        iSliv++;
                    }
                }
            }
                _ti.FuelAdd = dbZapr;
                _ti.FuelSub = dbSliv;
                _ti.FuelAddQty = iZapr;
                _ti.FuelSubQty = iSliv;
                _ti.ContentRecalcFuelDUT();
                pgTask.SelectedObject = _ti;
        }
        #endregion
        #region �����������
        private void Localization()
        {
            LocationL.Caption = Resources.Settlement;
            Id_event�.Caption = Resources.Event;
            icbEvent.Items[0].Description = Resources.Entry;
            icbEvent.Items[1].Description = Resources.Exit;
            DateFactL.Caption = Resources.TimeFact;
            DistanceL.Caption = Resources.Distance;
            DistanceCollectL.Caption = Resources.DistanceTotal;
            RemarkL.Caption = Resources.Remark;
            Id_zone.Caption = Resources.CheckZoneCZ;
            ZoneLookUp.Columns[0].Caption = Resources.NameRes;
            Id_event.Caption = Resources.Event;
            DatePlan.Caption = Resources.TimePlan;
            DateFact.Caption = Resources.TimeFact;
            Deviation.Caption = Resources.Deviation;
            DistanceT.Caption = Resources.Distance;
            DistanceCollect.Caption = Resources.DistanceTotal;
            Remark.Caption = Resources.Remark;
            EventLookUp.Columns[0].Caption = Resources.NameRes;
            xtraTabPage1.Text = Resources.RouteContent;
            xtbContent.Text = Resources.Stops;
            CheckZone.Caption = Resources.CZ;
            Location_s.Caption = Resources.Location;
            InitialTime.Caption = Resources.TimeStart;
            FinalTime.Caption = Resources.TimeEnd;
            Interval.Caption = Resources.Interval;
            repositoryItemLookUpEdit1.Columns[0].Caption = Resources.NameRes;
            repositoryItemLookUpEdit2.Columns[0].Caption = Resources.NameRes;
            xtraTabPage2.Text = Resources.SensorEvents;
            SensorName.Caption = Resources.Sensor;
            Location_ev.Caption = Resources.Location;
            EventTime.Caption = Resources.Time;
            Duration.Caption = Resources.DurationFact;
            Dist.Caption = Resources.Distance;
            Description.Caption = Resources.Event;
            Speed.Caption = Resources.SpeedKmH;
            repositoryItemLookUpEdit3.Columns[0].Caption = Resources.NameRes;
            repositoryItemLookUpEdit4.Columns[0].Caption = Resources.NameRes;
            xtraTabPage3.Text = Resources.Fuel;
            Location_f.Caption = Resources.Location;
            time_.Caption = Resources.Time;
            dValueCalc.Caption = Resources.FuelAddSub;
            ValueHandle.Caption = Resources.Correction;
            repositoryItemLookUpEdit5.Columns[0].Caption = Resources.NameRes;
            repositoryItemLookUpEdit6.Columns[0].Caption = Resources.NameRes;
            xtraTabPage4.Text = Resources.MovingGrafic;
            xtraTabPage5.Text = Resources.GantDiagram;
            labelControl9.Text = Resources.Remark;
            labelControl3.Text = Resources.Finish;
            btDeleteSimple.ToolTip = Resources.DeleteReference;
            labelControl6.Text = Resources.TypeRes;
            btAddSimple.ToolTip = Resources.AddSample;
            labelControl8.Text = Resources.StartLit;
            labelControl1.Text = Resources.Number;
            labelControl5.Text = Resources.RouteSample;
            labelControl2.Text = Resources.Driver;
            labelControl4.Text = Resources.Car;
            labelControl7.Text = Resources.BaseCode;
            TotalData.Properties.Caption = Resources.TotalInformation;
            SampleName.Properties.Caption = Resources.Route;
            VehicleName.Properties.Caption = Resources.Car;
            DriverName.Properties.Caption = Resources.Driver;
            RemarkPG.Properties.Caption = Resources.Remark;
            CatPath.Properties.Caption = Resources.MovingParams;
            Distance.Properties.Caption = Resources.Distance;
            TimeMoveProc.Properties.Caption = Resources.MovingTime;
            TimeStopProc.Properties.Caption = Resources.StopsTime;
            SpeedAvg.Properties.Caption = Resources.SpeedAvgKmH;
            DistanceWithLogicSensor.Properties.Caption = Resources.DistanceWithLogicSensor;
            CatTime.Properties.Caption = Resources.TimeParameters;
            TimePlanTotal.Properties.Caption = Resources.RouteDurationsPlan;
            TimeFactTotal.Properties.Caption = Resources.RouteDurationsFact;
            DeviationRow.Properties.Caption = Resources.DeviationDuration;
            DeviationAr.Properties.Caption = Resources.DeviationArrival;
            FuelDUT.Properties.Caption = Resources.DUT;
            FuelStart.Properties.Caption = Resources.FuelStartL;
            FuelEnd.Properties.Caption = Resources.FuelEndL;
            FuelAddQty.Properties.Caption = Resources.FuelAddQty;
            FuelAdd.Properties.Caption = Resources.FuelAddL;
            FuelSubQty.Properties.Caption = Resources.FuelSubQty;
            FuelSub.Properties.Caption = Resources.FuelSubL;
            FuelExpens.Properties.Caption = Resources.FuelExpenceL;
            FuelExpensAvg.Properties.Caption = Resources.FuelExpenceAvgLKm;
            FuelDRT.Properties.Caption = Resources.DRT_CAN;
            Fuel_ExpensMove.Properties.Caption = Resources.FuelExpMovingL;
            Fuel_ExpensStop.Properties.Caption = Resources.FuelExpStopsL;
            Fuel_ExpensTotal.Properties.Caption = Resources.FuelExpenceL;
            Fuel_ExpensAvg.Properties.Caption = Resources.FuelExpenceAvgLKm;
            ValidityData.Properties.Caption = Resources.DataQuality;
            PointsValidity.Properties.Caption = Resources.DataValidity;
            PointsCalc.Properties.Caption = Resources.PointsCalc;
            PointsFact.Properties.Caption = Resources.PointsFact;
            PointsIntervalMax.Properties.Caption = Resources.Max_Interval;
            gcQuality.Text = Resources.DataValidity;
            btSave.Caption = Resources.MenuSave;
            TUtil.ToolTipChangeText(btSave, Resources.RouteSave);
            btRefresh.Caption = Resources.MenuRefresh;
            TUtil.ToolTipChangeText(btRefresh, Resources.RouteRefresh);
            btTamplate.Caption = Resources.RouteCreate;
            btFact.Caption = Resources.MenuFact;
            TUtil.ToolTipChangeText(btFact, Resources.RecalcFact);
            btMapView.Caption = Resources.Map;
            TUtil.ToolTipChangeText(btMapView, Resources.ViewTrackInMap);
            bsiReports.Caption = Resources.Reports;
            btReport.Caption = Resources.ReportTotal;
            btReportZone.Caption = Resources.ReportContent;
            btReportStop.Caption = Resources.Stops;
            btReportSensor.Caption = Resources.SensorEvents;
            btReportFuel.Caption = Resources.FuelAddsSubs;
            bsiExcel.Caption = Resources.MenuExport;
            TUtil.ToolTipChangeText(bsiExcel, Resources.RouteExportExcel);
            btExcelTotal.Caption = Resources.InformationAssembled;
            btExcel.Caption = Resources.ExcelDataInsert;
            btDelete.Caption = Resources.MenuDelete;
            TUtil.ToolTipChangeText(btDelete, Resources.MenuDelete);
            bsiStatus.Caption = Resources.Ready;
            chEdit.Caption = Resources.MenuEdit;
            TUtil.ToolTipChangeText(chEdit, Resources.EditSwitch);
            btClear.Caption = Resources.MenuClear;
            TUtil.ToolTipChangeText(btClear, Resources.ClearTotal);
            btRecalcFuel.Caption = Resources.Recalc;
            Text = Resources.RouteList;

            Deviation.SummaryItem.DisplayFormat = Resources.TOTAL;
            time_.SummaryItem.DisplayFormat = Resources.TOTAL;
            DateFactL.SummaryItem.DisplayFormat = Resources.TOTAL;
            Location_s.SummaryItem.DisplayFormat = Resources.StopsTotal  + ": {0}";// "����� ���������: {0}";
            SensorName.SummaryItem.DisplayFormat = Resources.TOTAL + "{0}";
        }

        #endregion

        private void txOutLinkId_EditValueChanged(object sender, EventArgs e)
        {
            ValidateFieldsAction();
        }
    }
}
