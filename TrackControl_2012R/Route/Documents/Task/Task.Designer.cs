using System;
namespace Route
{
    partial class Task
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Task));
            DevExpress.XtraGrid.GridLevelNode gridLevelNode2 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState5 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState6 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState7 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            DevExpress.XtraGauges.Core.Model.IndicatorState indicatorState8 = new DevExpress.XtraGauges.Core.Model.IndicatorState();
            DevExpress.Utils.SuperToolTip superToolTip12 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipItem toolTipItem12 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip16 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipItem toolTipItem16 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip10 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipItem toolTipItem10 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip9 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipItem toolTipItem9 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip13 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipItem toolTipItem13 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip11 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipItem toolTipItem11 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip14 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipItem toolTipItem14 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip15 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipItem toolTipItem15 = new DevExpress.Utils.ToolTipItem();
            this.gvContentLoc = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.Id_cont_L = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LocationL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Id_event� = new DevExpress.XtraGrid.Columns.GridColumn();
            this.icbEvent = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.icEvents = new DevExpress.Utils.ImageCollection(this.components);
            this.DateFactL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DistanceL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DistanceCollectL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.RemarkL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcContent = new DevExpress.XtraGrid.GridControl();
            this.gvContent = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.Id_cont = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Id_zone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ZoneLookUp = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.Id_event = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DatePlan = new DevExpress.XtraGrid.Columns.GridColumn();
            this.teDateTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.DateFact = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Deviation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DistanceT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DistanceCollect = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Remark = new DevExpress.XtraGrid.Columns.GridColumn();
            this.EventLookUp = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.tbCont = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.xtbContent = new DevExpress.XtraTab.XtraTabPage();
            this.gcStop = new DevExpress.XtraGrid.GridControl();
            this.gvStop = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.Id_stop = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CheckZone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.icCheckZone = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.Location_s = new DevExpress.XtraGrid.Columns.GridColumn();
            this.InitialTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.FinalTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Interval = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemLookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemLookUpEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.gcSensor = new DevExpress.XtraGrid.GridControl();
            this.gvSensor = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.Id_ev = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SensorName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Location_ev = new DevExpress.XtraGrid.Columns.GridColumn();
            this.EventTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Duration = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Dist = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Description = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Speed = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemLookUpEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemLookUpEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.xtraTabPage3 = new DevExpress.XtraTab.XtraTabPage();
            this.gcFuel = new DevExpress.XtraGrid.GridControl();
            this.gvFuel = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.Id_fuel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Location_f = new DevExpress.XtraGrid.Columns.GridColumn();
            this.time_ = new DevExpress.XtraGrid.Columns.GridColumn();
            this.teDate = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.dValueCalc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ValueHandle = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemLookUpEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemLookUpEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.xtraTabPage4 = new DevExpress.XtraTab.XtraTabPage();
            this.pcGraficMoving = new DevExpress.XtraEditors.PanelControl();
            this.xtraTabPage5 = new DevExpress.XtraTab.XtraTabPage();
            this.pcDgrmGant = new DevExpress.XtraEditors.PanelControl();
            this.dxErrP = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.meRemark = new DevExpress.XtraEditors.MemoEdit();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.txOutLinkId = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.deEnd = new DevExpress.XtraEditors.DateEdit();
            this.btDeleteSimple = new DevExpress.XtraEditors.SimpleButton();
            this.leType = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.btAddSimple = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.deStart = new DevExpress.XtraEditors.DateEdit();
            this.txId = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.leRoute = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.leDriver = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.leMobitel = new DevExpress.XtraEditors.LookUpEdit();
            this.leGroupe = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl5 = new DevExpress.XtraEditors.PanelControl();
            this.pgTask = new DevExpress.XtraVerticalGrid.PropertyGridControl();
            this.pbValidity = new DevExpress.XtraEditors.Repository.RepositoryItemProgressBar();
            this.TotalData = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.SampleName = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.VehicleName = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.DriverName = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.RemarkPG = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.CatPath = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.Distance = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.TimeMoveProc = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.TimeStopProc = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.SpeedAvg = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.DistanceWithLogicSensor = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.CatTime = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.TimePlanTotal = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.TimeFactTotal = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.DeviationRow = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.DeviationAr = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.FuelDUT = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.FuelStart = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.FuelEnd = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.FuelAddQty = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.FuelAdd = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.FuelSubQty = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.FuelSub = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.FuelExpens = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.FuelExpensAvg = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.FuelDRT = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.Fuel_ExpensMove = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.Fuel_ExpensStop = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.Fuel_ExpensTotal = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.Fuel_ExpensAvg = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.ValidityData = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.PointsValidity = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.PointsCalc = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.PointsFact = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.PointsIntervalMax = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.gcQuality = new DevExpress.XtraEditors.GroupControl();
            this.elLightsIndicator = new DevExpress.XtraGauges.Win.GaugeControl();
            this.stateIndicatorGauge1 = new DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorGauge();
            this.stateIndicatorComponent1 = new DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent();
            this.bmTask = new DevExpress.XtraBars.BarManager(this.components);
            this.barButtons = new DevExpress.XtraBars.Bar();
            this.btSave = new DevExpress.XtraBars.BarButtonItem();
            this.btRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.btTamplate = new DevExpress.XtraBars.BarButtonItem();
            this.btFact = new DevExpress.XtraBars.BarButtonItem();
            this.btMapView = new DevExpress.XtraBars.BarButtonItem();
            this.bsiReports = new DevExpress.XtraBars.BarSubItem();
            this.btReport = new DevExpress.XtraBars.BarButtonItem();
            this.btReportZone = new DevExpress.XtraBars.BarButtonItem();
            this.btReportStop = new DevExpress.XtraBars.BarButtonItem();
            this.btReportSensor = new DevExpress.XtraBars.BarButtonItem();
            this.btReportFuel = new DevExpress.XtraBars.BarButtonItem();
            this.bsiExcel = new DevExpress.XtraBars.BarSubItem();
            this.btExcelTotal = new DevExpress.XtraBars.BarButtonItem();
            this.btExcel = new DevExpress.XtraBars.BarButtonItem();
            this.btDelete = new DevExpress.XtraBars.BarButtonItem();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.pbStatus = new DevExpress.XtraBars.BarEditItem();
            this.rpbStatus = new DevExpress.XtraEditors.Repository.RepositoryItemProgressBar();
            this.bsiStatus = new DevExpress.XtraBars.BarStaticItem();
            this.barGrid = new DevExpress.XtraBars.Bar();
            this.chEdit = new DevExpress.XtraBars.BarCheckItem();
            this.btClear = new DevExpress.XtraBars.BarButtonItem();
            this.btRecalcFuel = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.gvContentLoc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.icbEvent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.icEvents)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcContent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvContent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ZoneLookUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teDateTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EventLookUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCont)).BeginInit();
            this.tbCont.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            this.xtbContent.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcStop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvStop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.icCheckZone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit2)).BeginInit();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcSensor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvSensor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit4)).BeginInit();
            this.xtraTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcFuel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvFuel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.xtraTabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pcGraficMoving)).BeginInit();
            this.xtraTabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pcDgrmGant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.meRemark.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txOutLinkId.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEnd.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEnd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deStart.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deStart.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txId.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leRoute.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leDriver.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leMobitel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leGroupe.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).BeginInit();
            this.panelControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pgTask)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbValidity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcQuality)).BeginInit();
            this.gcQuality.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.stateIndicatorGauge1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stateIndicatorComponent1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bmTask)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rpbStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // gvContentLoc
            // 
            this.gvContentLoc.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvContentLoc.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvContentLoc.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gvContentLoc.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvContentLoc.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvContentLoc.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvContentLoc.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvContentLoc.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvContentLoc.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gvContentLoc.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvContentLoc.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvContentLoc.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvContentLoc.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvContentLoc.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gvContentLoc.Appearance.Empty.Options.UseBackColor = true;
            this.gvContentLoc.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvContentLoc.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvContentLoc.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gvContentLoc.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvContentLoc.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gvContentLoc.Appearance.EvenRow.Options.UseForeColor = true;
            this.gvContentLoc.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvContentLoc.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvContentLoc.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gvContentLoc.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvContentLoc.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvContentLoc.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gvContentLoc.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvContentLoc.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gvContentLoc.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvContentLoc.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvContentLoc.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvContentLoc.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.gvContentLoc.Appearance.FixedLine.Options.UseBackColor = true;
            this.gvContentLoc.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gvContentLoc.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gvContentLoc.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gvContentLoc.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gvContentLoc.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(192)))), ((int)(((byte)(157)))));
            this.gvContentLoc.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(219)))), ((int)(((byte)(188)))));
            this.gvContentLoc.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gvContentLoc.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvContentLoc.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gvContentLoc.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvContentLoc.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvContentLoc.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvContentLoc.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvContentLoc.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvContentLoc.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvContentLoc.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gvContentLoc.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvContentLoc.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvContentLoc.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvContentLoc.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvContentLoc.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvContentLoc.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvContentLoc.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gvContentLoc.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvContentLoc.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvContentLoc.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gvContentLoc.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(242)))), ((int)(((byte)(213)))));
            this.gvContentLoc.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gvContentLoc.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gvContentLoc.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvContentLoc.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvContentLoc.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvContentLoc.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvContentLoc.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gvContentLoc.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvContentLoc.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gvContentLoc.Appearance.GroupRow.Options.UseForeColor = true;
            this.gvContentLoc.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvContentLoc.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvContentLoc.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gvContentLoc.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvContentLoc.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvContentLoc.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gvContentLoc.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvContentLoc.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvContentLoc.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gvContentLoc.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvContentLoc.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.gvContentLoc.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gvContentLoc.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvContentLoc.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvContentLoc.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvContentLoc.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvContentLoc.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gvContentLoc.Appearance.OddRow.Options.UseBackColor = true;
            this.gvContentLoc.Appearance.OddRow.Options.UseBorderColor = true;
            this.gvContentLoc.Appearance.OddRow.Options.UseForeColor = true;
            this.gvContentLoc.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(247)))));
            this.gvContentLoc.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gvContentLoc.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(148)))), ((int)(((byte)(148)))));
            this.gvContentLoc.Appearance.Preview.Options.UseBackColor = true;
            this.gvContentLoc.Appearance.Preview.Options.UseFont = true;
            this.gvContentLoc.Appearance.Preview.Options.UseForeColor = true;
            this.gvContentLoc.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvContentLoc.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gvContentLoc.Appearance.Row.Options.UseBackColor = true;
            this.gvContentLoc.Appearance.Row.Options.UseForeColor = true;
            this.gvContentLoc.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvContentLoc.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gvContentLoc.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvContentLoc.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.gvContentLoc.Appearance.SelectedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvContentLoc.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.gvContentLoc.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvContentLoc.Appearance.SelectedRow.Options.UseBorderColor = true;
            this.gvContentLoc.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gvContentLoc.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gvContentLoc.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gvContentLoc.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvContentLoc.Appearance.VertLine.Options.UseBackColor = true;
            this.gvContentLoc.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.Id_cont_L,
            this.LocationL,
            this.Id_event�,
            this.DateFactL,
            this.DistanceL,
            this.DistanceCollectL,
            this.RemarkL});
            this.gvContentLoc.GridControl = this.gcContent;
            this.gvContentLoc.Name = "gvContentLoc";
            this.gvContentLoc.OptionsView.EnableAppearanceEvenRow = true;
            this.gvContentLoc.OptionsView.EnableAppearanceOddRow = true;
            this.gvContentLoc.OptionsView.ShowFooter = true;
            this.gvContentLoc.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gvContentLoc_CellValueChanged);
            // 
            // Id_cont_L
            // 
            this.Id_cont_L.Caption = "Id";
            this.Id_cont_L.FieldName = "Id";
            this.Id_cont_L.Name = "Id_cont_L";
            this.Id_cont_L.OptionsColumn.AllowEdit = false;
            // 
            // LocationL
            // 
            this.LocationL.AppearanceHeader.Options.UseTextOptions = true;
            this.LocationL.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.LocationL.Caption = "���������� �����";
            this.LocationL.FieldName = "Location";
            this.LocationL.Name = "LocationL";
            this.LocationL.OptionsColumn.AllowEdit = false;
            this.LocationL.Visible = true;
            this.LocationL.VisibleIndex = 0;
            this.LocationL.Width = 430;
            // 
            // Id_event�
            // 
            this.Id_event�.AppearanceCell.Options.UseTextOptions = true;
            this.Id_event�.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Id_event�.AppearanceHeader.Options.UseTextOptions = true;
            this.Id_event�.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Id_event�.Caption = "�������";
            this.Id_event�.ColumnEdit = this.icbEvent;
            this.Id_event�.FieldName = "Id_event";
            this.Id_event�.Name = "Id_event�";
            this.Id_event�.OptionsColumn.AllowEdit = false;
            this.Id_event�.Visible = true;
            this.Id_event�.VisibleIndex = 1;
            this.Id_event�.Width = 93;
            // 
            // icbEvent
            // 
            this.icbEvent.AutoHeight = false;
            this.icbEvent.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.icbEvent.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("�����", 1, 0),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("�����", 2, 1)});
            this.icbEvent.Name = "icbEvent";
            this.icbEvent.ShowDropDown = DevExpress.XtraEditors.Controls.ShowDropDown.DoubleClick;
            this.icbEvent.SmallImages = this.icEvents;
            // 
            // icEvents
            // 
            this.icEvents.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("icEvents.ImageStream")));
            this.icEvents.Images.SetKeyName(0, "input.png");
            this.icEvents.Images.SetKeyName(1, "output.png");
            this.icEvents.Images.SetKeyName(2, "Zone.gif");
            // 
            // DateFactL
            // 
            this.DateFactL.AppearanceCell.Options.UseTextOptions = true;
            this.DateFactL.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DateFactL.AppearanceHeader.Options.UseTextOptions = true;
            this.DateFactL.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DateFactL.Caption = "����������� ����� ";
            this.DateFactL.DisplayFormat.FormatString = "g";
            this.DateFactL.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DateFactL.FieldName = "DateFact";
            this.DateFactL.Name = "DateFactL";
            this.DateFactL.OptionsColumn.AllowEdit = false;
            this.DateFactL.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Custom, "DateFact", "�����:")});
            this.DateFactL.Visible = true;
            this.DateFactL.VisibleIndex = 2;
            this.DateFactL.Width = 160;
            // 
            // DistanceL
            // 
            this.DistanceL.AppearanceCell.Options.UseTextOptions = true;
            this.DistanceL.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DistanceL.AppearanceHeader.Options.UseTextOptions = true;
            this.DistanceL.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DistanceL.Caption = "����, ��";
            this.DistanceL.FieldName = "Distance";
            this.DistanceL.Name = "DistanceL";
            this.DistanceL.OptionsColumn.AllowEdit = false;
            this.DistanceL.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.DistanceL.Visible = true;
            this.DistanceL.VisibleIndex = 3;
            this.DistanceL.Width = 107;
            // 
            // DistanceCollectL
            // 
            this.DistanceCollectL.AppearanceCell.Options.UseTextOptions = true;
            this.DistanceCollectL.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DistanceCollectL.AppearanceHeader.Options.UseTextOptions = true;
            this.DistanceCollectL.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DistanceCollectL.Caption = "���� �����, ��";
            this.DistanceCollectL.FieldName = "DistanceCollect";
            this.DistanceCollectL.Name = "DistanceCollectL";
            this.DistanceCollectL.OptionsColumn.AllowEdit = false;
            this.DistanceCollectL.Visible = true;
            this.DistanceCollectL.VisibleIndex = 4;
            this.DistanceCollectL.Width = 133;
            // 
            // RemarkL
            // 
            this.RemarkL.AppearanceHeader.Options.UseTextOptions = true;
            this.RemarkL.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.RemarkL.Caption = "����������";
            this.RemarkL.FieldName = "Remark";
            this.RemarkL.Name = "RemarkL";
            this.RemarkL.Visible = true;
            this.RemarkL.VisibleIndex = 5;
            this.RemarkL.Width = 167;
            // 
            // gcContent
            // 
            this.gcContent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcContent.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gcContent_EmbeddedNavigator_ButtonClick);
            gridLevelNode2.LevelTemplate = this.gvContentLoc;
            gridLevelNode2.RelationName = "Level1";
            this.gcContent.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode2});
            this.gcContent.Location = new System.Drawing.Point(0, 0);
            this.gcContent.MainView = this.gvContent;
            this.gcContent.Name = "gcContent";
            this.gcContent.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.ZoneLookUp,
            this.EventLookUp,
            this.teDateTime,
            this.icbEvent});
            this.gcContent.Size = new System.Drawing.Size(904, 343);
            this.gcContent.TabIndex = 0;
            this.gcContent.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvContent,
            this.gvContentLoc});
            // 
            // gvContent
            // 
            this.gvContent.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvContent.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvContent.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gvContent.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvContent.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvContent.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvContent.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvContent.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvContent.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gvContent.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvContent.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvContent.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvContent.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvContent.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gvContent.Appearance.Empty.Options.UseBackColor = true;
            this.gvContent.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvContent.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvContent.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gvContent.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvContent.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gvContent.Appearance.EvenRow.Options.UseForeColor = true;
            this.gvContent.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvContent.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvContent.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gvContent.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvContent.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvContent.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gvContent.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvContent.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gvContent.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvContent.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvContent.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvContent.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.gvContent.Appearance.FixedLine.Options.UseBackColor = true;
            this.gvContent.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gvContent.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gvContent.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gvContent.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gvContent.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(192)))), ((int)(((byte)(157)))));
            this.gvContent.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(219)))), ((int)(((byte)(188)))));
            this.gvContent.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gvContent.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvContent.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gvContent.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvContent.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvContent.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvContent.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvContent.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvContent.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvContent.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gvContent.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvContent.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvContent.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvContent.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvContent.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvContent.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvContent.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gvContent.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvContent.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvContent.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gvContent.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(242)))), ((int)(((byte)(213)))));
            this.gvContent.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gvContent.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gvContent.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvContent.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvContent.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvContent.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvContent.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gvContent.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvContent.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gvContent.Appearance.GroupRow.Options.UseForeColor = true;
            this.gvContent.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvContent.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvContent.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gvContent.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvContent.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvContent.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gvContent.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvContent.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvContent.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gvContent.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvContent.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.gvContent.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gvContent.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvContent.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvContent.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvContent.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvContent.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gvContent.Appearance.OddRow.Options.UseBackColor = true;
            this.gvContent.Appearance.OddRow.Options.UseBorderColor = true;
            this.gvContent.Appearance.OddRow.Options.UseForeColor = true;
            this.gvContent.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(247)))));
            this.gvContent.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gvContent.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(148)))), ((int)(((byte)(148)))));
            this.gvContent.Appearance.Preview.Options.UseBackColor = true;
            this.gvContent.Appearance.Preview.Options.UseFont = true;
            this.gvContent.Appearance.Preview.Options.UseForeColor = true;
            this.gvContent.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvContent.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gvContent.Appearance.Row.Options.UseBackColor = true;
            this.gvContent.Appearance.Row.Options.UseForeColor = true;
            this.gvContent.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvContent.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gvContent.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvContent.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.gvContent.Appearance.SelectedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvContent.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.gvContent.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvContent.Appearance.SelectedRow.Options.UseBorderColor = true;
            this.gvContent.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gvContent.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gvContent.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gvContent.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvContent.Appearance.VertLine.Options.UseBackColor = true;
            this.gvContent.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.Id_cont,
            this.Id_zone,
            this.Id_event,
            this.DatePlan,
            this.DateFact,
            this.Deviation,
            this.DistanceT,
            this.DistanceCollect,
            this.Remark});
            this.gvContent.GridControl = this.gcContent;
            this.gvContent.Name = "gvContent";
            this.gvContent.OptionsView.EnableAppearanceEvenRow = true;
            this.gvContent.OptionsView.EnableAppearanceOddRow = true;
            this.gvContent.OptionsView.ShowFooter = true;
            this.gvContent.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.gvContent_RowCellStyle);
            this.gvContent.InitNewRow += new DevExpress.XtraGrid.Views.Grid.InitNewRowEventHandler(this.gvContent_InitNewRow);
            this.gvContent.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gvContent_CellValueChanged);
            // 
            // Id_cont
            // 
            this.Id_cont.AppearanceHeader.Options.UseTextOptions = true;
            this.Id_cont.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Id_cont.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Id_cont.Caption = "Id";
            this.Id_cont.FieldName = "Id";
            this.Id_cont.Name = "Id_cont";
            this.Id_cont.OptionsColumn.AllowEdit = false;
            // 
            // Id_zone
            // 
            this.Id_zone.AppearanceHeader.Options.UseTextOptions = true;
            this.Id_zone.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Id_zone.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Id_zone.Caption = "����������� ���� (��)";
            this.Id_zone.ColumnEdit = this.ZoneLookUp;
            this.Id_zone.FieldName = "Id_zone";
            this.Id_zone.Name = "Id_zone";
            this.Id_zone.OptionsColumn.AllowEdit = false;
            this.Id_zone.Visible = true;
            this.Id_zone.VisibleIndex = 0;
            this.Id_zone.Width = 153;
            // 
            // ZoneLookUp
            // 
            this.ZoneLookUp.AutoHeight = false;
            this.ZoneLookUp.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ZoneLookUp.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "��������")});
            this.ZoneLookUp.DisplayMember = "Name";
            this.ZoneLookUp.Name = "ZoneLookUp";
            this.ZoneLookUp.ShowDropDown = DevExpress.XtraEditors.Controls.ShowDropDown.DoubleClick;
            this.ZoneLookUp.ValueMember = "Id";
            // 
            // Id_event
            // 
            this.Id_event.AppearanceHeader.Options.UseTextOptions = true;
            this.Id_event.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Id_event.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Id_event.Caption = "�������";
            this.Id_event.ColumnEdit = this.icbEvent;
            this.Id_event.FieldName = "Id_event";
            this.Id_event.Name = "Id_event";
            this.Id_event.OptionsColumn.AllowEdit = false;
            this.Id_event.Visible = true;
            this.Id_event.VisibleIndex = 1;
            this.Id_event.Width = 86;
            // 
            // DatePlan
            // 
            this.DatePlan.AppearanceCell.Options.UseTextOptions = true;
            this.DatePlan.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DatePlan.AppearanceHeader.Options.UseTextOptions = true;
            this.DatePlan.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DatePlan.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.DatePlan.Caption = "�������� �����";
            this.DatePlan.ColumnEdit = this.teDateTime;
            this.DatePlan.DisplayFormat.FormatString = "dd/MM/yyyy  HH:mm";
            this.DatePlan.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DatePlan.FieldName = "DatePlan";
            this.DatePlan.Name = "DatePlan";
            this.DatePlan.OptionsColumn.AllowEdit = false;
            this.DatePlan.Visible = true;
            this.DatePlan.VisibleIndex = 2;
            this.DatePlan.Width = 205;
            // 
            // teDateTime
            // 
            this.teDateTime.AutoHeight = false;
            this.teDateTime.EditFormat.FormatString = "dd/MM/yyyy  HH:mm";
            this.teDateTime.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.teDateTime.Mask.EditMask = "dd/MM/yyyy  HH:mm";
            this.teDateTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime;
            this.teDateTime.Name = "teDateTime";
            // 
            // DateFact
            // 
            this.DateFact.AppearanceCell.Options.UseTextOptions = true;
            this.DateFact.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DateFact.AppearanceHeader.Options.UseTextOptions = true;
            this.DateFact.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DateFact.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.DateFact.Caption = "����������� ����� ";
            this.DateFact.DisplayFormat.FormatString = "dd/MM/yyyy  HH:mm";
            this.DateFact.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DateFact.FieldName = "DateFact";
            this.DateFact.Name = "DateFact";
            this.DateFact.OptionsColumn.AllowEdit = false;
            this.DateFact.Visible = true;
            this.DateFact.VisibleIndex = 3;
            this.DateFact.Width = 189;
            // 
            // Deviation
            // 
            this.Deviation.AppearanceCell.Options.UseTextOptions = true;
            this.Deviation.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Deviation.AppearanceHeader.Options.UseTextOptions = true;
            this.Deviation.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Deviation.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Deviation.Caption = "����������";
            this.Deviation.FieldName = "Deviation";
            this.Deviation.Name = "Deviation";
            this.Deviation.OptionsColumn.AllowEdit = false;
            this.Deviation.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Custom, "Deviation", "�����:")});
            this.Deviation.Visible = true;
            this.Deviation.VisibleIndex = 4;
            this.Deviation.Width = 101;
            // 
            // DistanceT
            // 
            this.DistanceT.AppearanceCell.Options.UseTextOptions = true;
            this.DistanceT.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.DistanceT.AppearanceHeader.Options.UseTextOptions = true;
            this.DistanceT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DistanceT.Caption = "����, ��";
            this.DistanceT.FieldName = "Distance";
            this.DistanceT.Name = "DistanceT";
            this.DistanceT.OptionsColumn.AllowEdit = false;
            this.DistanceT.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.DistanceT.Visible = true;
            this.DistanceT.VisibleIndex = 5;
            this.DistanceT.Width = 96;
            // 
            // DistanceCollect
            // 
            this.DistanceCollect.AppearanceCell.Options.UseTextOptions = true;
            this.DistanceCollect.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.DistanceCollect.AppearanceHeader.Options.UseTextOptions = true;
            this.DistanceCollect.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DistanceCollect.Caption = "���� �����, ��";
            this.DistanceCollect.FieldName = "DistanceCollect";
            this.DistanceCollect.Name = "DistanceCollect";
            this.DistanceCollect.OptionsColumn.AllowEdit = false;
            this.DistanceCollect.Visible = true;
            this.DistanceCollect.VisibleIndex = 6;
            this.DistanceCollect.Width = 110;
            // 
            // Remark
            // 
            this.Remark.AppearanceHeader.Options.UseTextOptions = true;
            this.Remark.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Remark.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Remark.Caption = "����������";
            this.Remark.FieldName = "Remark";
            this.Remark.Name = "Remark";
            this.Remark.OptionsColumn.AllowEdit = false;
            this.Remark.Visible = true;
            this.Remark.VisibleIndex = 7;
            this.Remark.Width = 150;
            // 
            // EventLookUp
            // 
            this.EventLookUp.AutoHeight = false;
            this.EventLookUp.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.EventLookUp.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "��������")});
            this.EventLookUp.DisplayMember = "Name";
            this.EventLookUp.Name = "EventLookUp";
            this.EventLookUp.ValueMember = "Id";
            // 
            // tbCont
            // 
            this.tbCont.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbCont.Location = new System.Drawing.Point(0, 277);
            this.tbCont.Name = "tbCont";
            this.tbCont.SelectedTabPage = this.xtraTabPage1;
            this.tbCont.Size = new System.Drawing.Size(910, 371);
            this.tbCont.TabIndex = 66;
            this.tbCont.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtbContent,
            this.xtraTabPage2,
            this.xtraTabPage3,
            this.xtraTabPage4,
            this.xtraTabPage5});
            this.tbCont.Click += new System.EventHandler(this.tbCont_Click);
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.gcContent);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(904, 343);
            this.xtraTabPage1.Text = "���������� ��������";
            // 
            // xtbContent
            // 
            this.xtbContent.Controls.Add(this.gcStop);
            this.xtbContent.Name = "xtbContent";
            this.xtbContent.Size = new System.Drawing.Size(904, 343);
            this.xtbContent.Text = "���������";
            // 
            // gcStop
            // 
            this.gcStop.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcStop.Location = new System.Drawing.Point(0, 0);
            this.gcStop.MainView = this.gvStop;
            this.gcStop.Name = "gcStop";
            this.gcStop.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemLookUpEdit1,
            this.repositoryItemLookUpEdit2,
            this.icCheckZone});
            this.gcStop.Size = new System.Drawing.Size(904, 343);
            this.gcStop.TabIndex = 1;
            this.gcStop.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvStop});
            // 
            // gvStop
            // 
            this.gvStop.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvStop.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvStop.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gvStop.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvStop.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvStop.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvStop.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvStop.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvStop.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gvStop.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvStop.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvStop.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvStop.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvStop.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gvStop.Appearance.Empty.Options.UseBackColor = true;
            this.gvStop.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvStop.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvStop.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gvStop.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvStop.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gvStop.Appearance.EvenRow.Options.UseForeColor = true;
            this.gvStop.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvStop.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvStop.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gvStop.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvStop.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvStop.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gvStop.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvStop.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gvStop.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvStop.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvStop.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvStop.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.gvStop.Appearance.FixedLine.Options.UseBackColor = true;
            this.gvStop.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gvStop.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gvStop.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gvStop.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gvStop.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(192)))), ((int)(((byte)(157)))));
            this.gvStop.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(219)))), ((int)(((byte)(188)))));
            this.gvStop.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gvStop.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvStop.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gvStop.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvStop.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvStop.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvStop.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvStop.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvStop.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvStop.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gvStop.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvStop.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvStop.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvStop.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvStop.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvStop.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvStop.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gvStop.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvStop.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvStop.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gvStop.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(242)))), ((int)(((byte)(213)))));
            this.gvStop.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gvStop.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gvStop.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvStop.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvStop.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvStop.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvStop.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gvStop.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvStop.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gvStop.Appearance.GroupRow.Options.UseForeColor = true;
            this.gvStop.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvStop.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvStop.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gvStop.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvStop.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvStop.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gvStop.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvStop.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvStop.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gvStop.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvStop.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.gvStop.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gvStop.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvStop.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvStop.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvStop.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvStop.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gvStop.Appearance.OddRow.Options.UseBackColor = true;
            this.gvStop.Appearance.OddRow.Options.UseBorderColor = true;
            this.gvStop.Appearance.OddRow.Options.UseForeColor = true;
            this.gvStop.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(247)))));
            this.gvStop.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gvStop.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(148)))), ((int)(((byte)(148)))));
            this.gvStop.Appearance.Preview.Options.UseBackColor = true;
            this.gvStop.Appearance.Preview.Options.UseFont = true;
            this.gvStop.Appearance.Preview.Options.UseForeColor = true;
            this.gvStop.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvStop.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gvStop.Appearance.Row.Options.UseBackColor = true;
            this.gvStop.Appearance.Row.Options.UseForeColor = true;
            this.gvStop.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvStop.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gvStop.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvStop.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.gvStop.Appearance.SelectedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvStop.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.gvStop.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvStop.Appearance.SelectedRow.Options.UseBorderColor = true;
            this.gvStop.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gvStop.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gvStop.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gvStop.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvStop.Appearance.VertLine.Options.UseBackColor = true;
            this.gvStop.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.Id_stop,
            this.CheckZone,
            this.Location_s,
            this.InitialTime,
            this.FinalTime,
            this.Interval});
            this.gvStop.GridControl = this.gcStop;
            this.gvStop.GroupFormat = "[#image]{1} {2}";
            this.gvStop.Images = this.icEvents;
            this.gvStop.Name = "gvStop";
            this.gvStop.OptionsView.EnableAppearanceEvenRow = true;
            this.gvStop.OptionsView.EnableAppearanceOddRow = true;
            this.gvStop.OptionsView.ShowFooter = true;
            // 
            // Id_stop
            // 
            this.Id_stop.AppearanceCell.Options.UseTextOptions = true;
            this.Id_stop.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Id_stop.AppearanceHeader.Options.UseTextOptions = true;
            this.Id_stop.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Id_stop.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Id_stop.Caption = "Id";
            this.Id_stop.FieldName = "Id";
            this.Id_stop.Name = "Id_stop";
            this.Id_stop.OptionsColumn.AllowEdit = false;
            // 
            // CheckZone
            // 
            this.CheckZone.AppearanceCell.Options.UseTextOptions = true;
            this.CheckZone.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.CheckZone.AppearanceHeader.Options.UseTextOptions = true;
            this.CheckZone.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.CheckZone.Caption = "��";
            this.CheckZone.ColumnEdit = this.icCheckZone;
            this.CheckZone.FieldName = "CheckZone";
            this.CheckZone.ImageAlignment = System.Drawing.StringAlignment.Center;
            this.CheckZone.Name = "CheckZone";
            this.CheckZone.OptionsColumn.AllowEdit = false;
            this.CheckZone.OptionsColumn.FixedWidth = true;
            this.CheckZone.Visible = true;
            this.CheckZone.VisibleIndex = 0;
            this.CheckZone.Width = 38;
            // 
            // icCheckZone
            // 
            this.icCheckZone.Appearance.Options.UseTextOptions = true;
            this.icCheckZone.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.icCheckZone.AutoHeight = false;
            this.icCheckZone.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.icCheckZone.GlyphAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.icCheckZone.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", false, -1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", true, 2)});
            this.icCheckZone.Name = "icCheckZone";
            this.icCheckZone.SmallImages = this.icEvents;
            // 
            // Location_s
            // 
            this.Location_s.AppearanceCell.Options.UseTextOptions = true;
            this.Location_s.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.Location_s.AppearanceHeader.Options.UseTextOptions = true;
            this.Location_s.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Location_s.Caption = "��������������";
            this.Location_s.FieldName = "Location";
            this.Location_s.Name = "Location_s";
            this.Location_s.OptionsColumn.AllowEdit = false;
            this.Location_s.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "Location", "����� ���������: {0}")});
            this.Location_s.Visible = true;
            this.Location_s.VisibleIndex = 1;
            this.Location_s.Width = 414;
            // 
            // InitialTime
            // 
            this.InitialTime.AppearanceCell.Options.UseTextOptions = true;
            this.InitialTime.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.InitialTime.AppearanceHeader.Options.UseTextOptions = true;
            this.InitialTime.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.InitialTime.Caption = "����� ������";
            this.InitialTime.DisplayFormat.FormatString = "dd/MM/yyyy  HH:mm";
            this.InitialTime.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.InitialTime.FieldName = "InitialTime";
            this.InitialTime.Name = "InitialTime";
            this.InitialTime.OptionsColumn.AllowEdit = false;
            this.InitialTime.Visible = true;
            this.InitialTime.VisibleIndex = 2;
            this.InitialTime.Width = 192;
            // 
            // FinalTime
            // 
            this.FinalTime.AppearanceCell.Options.UseTextOptions = true;
            this.FinalTime.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FinalTime.AppearanceHeader.Options.UseTextOptions = true;
            this.FinalTime.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FinalTime.Caption = "����� ���������";
            this.FinalTime.DisplayFormat.FormatString = "dd/MM/yyyy  HH:mm";
            this.FinalTime.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.FinalTime.FieldName = "FinalTime";
            this.FinalTime.Name = "FinalTime";
            this.FinalTime.OptionsColumn.AllowEdit = false;
            this.FinalTime.Visible = true;
            this.FinalTime.VisibleIndex = 3;
            this.FinalTime.Width = 209;
            // 
            // Interval
            // 
            this.Interval.AppearanceCell.Options.UseTextOptions = true;
            this.Interval.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Interval.AppearanceHeader.Options.UseTextOptions = true;
            this.Interval.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Interval.Caption = "��������, ���.�����";
            this.Interval.FieldName = "Interval";
            this.Interval.Name = "Interval";
            this.Interval.OptionsColumn.AllowEdit = false;
            this.Interval.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.Interval.Visible = true;
            this.Interval.VisibleIndex = 4;
            this.Interval.Width = 237;
            // 
            // repositoryItemLookUpEdit1
            // 
            this.repositoryItemLookUpEdit1.AutoHeight = false;
            this.repositoryItemLookUpEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit1.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "��������")});
            this.repositoryItemLookUpEdit1.DisplayMember = "Name";
            this.repositoryItemLookUpEdit1.Name = "repositoryItemLookUpEdit1";
            this.repositoryItemLookUpEdit1.ShowDropDown = DevExpress.XtraEditors.Controls.ShowDropDown.DoubleClick;
            this.repositoryItemLookUpEdit1.ValueMember = "Id";
            // 
            // repositoryItemLookUpEdit2
            // 
            this.repositoryItemLookUpEdit2.AutoHeight = false;
            this.repositoryItemLookUpEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit2.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "��������")});
            this.repositoryItemLookUpEdit2.DisplayMember = "Name";
            this.repositoryItemLookUpEdit2.Name = "repositoryItemLookUpEdit2";
            this.repositoryItemLookUpEdit2.ValueMember = "Id";
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.gcSensor);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(904, 343);
            this.xtraTabPage2.Text = "������� ��������";
            // 
            // gcSensor
            // 
            this.gcSensor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcSensor.Location = new System.Drawing.Point(0, 0);
            this.gcSensor.MainView = this.gvSensor;
            this.gcSensor.Name = "gcSensor";
            this.gcSensor.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemLookUpEdit3,
            this.repositoryItemLookUpEdit4});
            this.gcSensor.Size = new System.Drawing.Size(904, 343);
            this.gcSensor.TabIndex = 2;
            this.gcSensor.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvSensor});
            // 
            // gvSensor
            // 
            this.gvSensor.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvSensor.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvSensor.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gvSensor.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvSensor.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvSensor.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvSensor.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvSensor.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvSensor.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gvSensor.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvSensor.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvSensor.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvSensor.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvSensor.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gvSensor.Appearance.Empty.Options.UseBackColor = true;
            this.gvSensor.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvSensor.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvSensor.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gvSensor.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvSensor.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gvSensor.Appearance.EvenRow.Options.UseForeColor = true;
            this.gvSensor.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvSensor.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvSensor.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gvSensor.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvSensor.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvSensor.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gvSensor.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvSensor.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gvSensor.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvSensor.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvSensor.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvSensor.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.gvSensor.Appearance.FixedLine.Options.UseBackColor = true;
            this.gvSensor.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gvSensor.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gvSensor.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gvSensor.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gvSensor.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(192)))), ((int)(((byte)(157)))));
            this.gvSensor.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(219)))), ((int)(((byte)(188)))));
            this.gvSensor.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gvSensor.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvSensor.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gvSensor.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvSensor.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvSensor.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvSensor.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvSensor.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvSensor.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvSensor.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gvSensor.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvSensor.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvSensor.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvSensor.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvSensor.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvSensor.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvSensor.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gvSensor.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvSensor.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvSensor.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gvSensor.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(242)))), ((int)(((byte)(213)))));
            this.gvSensor.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gvSensor.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gvSensor.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvSensor.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvSensor.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvSensor.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvSensor.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gvSensor.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvSensor.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gvSensor.Appearance.GroupRow.Options.UseForeColor = true;
            this.gvSensor.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvSensor.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvSensor.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gvSensor.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvSensor.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvSensor.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gvSensor.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvSensor.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvSensor.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gvSensor.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvSensor.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.gvSensor.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gvSensor.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvSensor.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvSensor.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvSensor.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvSensor.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gvSensor.Appearance.OddRow.Options.UseBackColor = true;
            this.gvSensor.Appearance.OddRow.Options.UseBorderColor = true;
            this.gvSensor.Appearance.OddRow.Options.UseForeColor = true;
            this.gvSensor.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(247)))));
            this.gvSensor.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gvSensor.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(148)))), ((int)(((byte)(148)))));
            this.gvSensor.Appearance.Preview.Options.UseBackColor = true;
            this.gvSensor.Appearance.Preview.Options.UseFont = true;
            this.gvSensor.Appearance.Preview.Options.UseForeColor = true;
            this.gvSensor.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvSensor.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gvSensor.Appearance.Row.Options.UseBackColor = true;
            this.gvSensor.Appearance.Row.Options.UseForeColor = true;
            this.gvSensor.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvSensor.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gvSensor.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvSensor.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.gvSensor.Appearance.SelectedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvSensor.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.gvSensor.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvSensor.Appearance.SelectedRow.Options.UseBorderColor = true;
            this.gvSensor.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gvSensor.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gvSensor.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gvSensor.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvSensor.Appearance.VertLine.Options.UseBackColor = true;
            this.gvSensor.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.Id_ev,
            this.SensorName,
            this.Location_ev,
            this.EventTime,
            this.Duration,
            this.Dist,
            this.Description,
            this.Speed});
            this.gvSensor.GridControl = this.gcSensor;
            this.gvSensor.GroupCount = 1;
            this.gvSensor.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Count, "Id", null, ""),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Duration", null, "")});
            this.gvSensor.Name = "gvSensor";
            this.gvSensor.OptionsView.EnableAppearanceEvenRow = true;
            this.gvSensor.OptionsView.EnableAppearanceOddRow = true;
            this.gvSensor.OptionsView.ShowFooter = true;
            this.gvSensor.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.Description, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // Id_ev
            // 
            this.Id_ev.AppearanceHeader.Options.UseTextOptions = true;
            this.Id_ev.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Id_ev.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Id_ev.Caption = "Id";
            this.Id_ev.FieldName = "Id";
            this.Id_ev.Name = "Id_ev";
            this.Id_ev.OptionsColumn.AllowEdit = false;
            // 
            // SensorName
            // 
            this.SensorName.AppearanceHeader.Options.UseTextOptions = true;
            this.SensorName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SensorName.Caption = "������";
            this.SensorName.FieldName = "SensorName";
            this.SensorName.Name = "SensorName";
            this.SensorName.OptionsColumn.AllowEdit = false;
            this.SensorName.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "SensorName", "�����:{0}")});
            this.SensorName.Visible = true;
            this.SensorName.VisibleIndex = 0;
            this.SensorName.Width = 141;
            // 
            // Location_ev
            // 
            this.Location_ev.AppearanceCell.Options.UseTextOptions = true;
            this.Location_ev.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.Location_ev.AppearanceHeader.Options.UseTextOptions = true;
            this.Location_ev.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Location_ev.Caption = "��������������";
            this.Location_ev.FieldName = "Location";
            this.Location_ev.Name = "Location_ev";
            this.Location_ev.OptionsColumn.AllowEdit = false;
            this.Location_ev.Visible = true;
            this.Location_ev.VisibleIndex = 1;
            this.Location_ev.Width = 313;
            // 
            // EventTime
            // 
            this.EventTime.AppearanceCell.Options.UseTextOptions = true;
            this.EventTime.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.EventTime.AppearanceHeader.Options.UseTextOptions = true;
            this.EventTime.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.EventTime.Caption = "�����";
            this.EventTime.DisplayFormat.FormatString = "f";
            this.EventTime.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.EventTime.FieldName = "EventTime";
            this.EventTime.Name = "EventTime";
            this.EventTime.OptionsColumn.AllowEdit = false;
            this.EventTime.Visible = true;
            this.EventTime.VisibleIndex = 2;
            this.EventTime.Width = 166;
            // 
            // Duration
            // 
            this.Duration.AppearanceCell.Options.UseTextOptions = true;
            this.Duration.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Duration.AppearanceHeader.Options.UseTextOptions = true;
            this.Duration.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Duration.Caption = "������������";
            this.Duration.FieldName = "Duration";
            this.Duration.Name = "Duration";
            this.Duration.OptionsColumn.AllowEdit = false;
            this.Duration.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.Duration.Visible = true;
            this.Duration.VisibleIndex = 3;
            this.Duration.Width = 115;
            // 
            // Dist
            // 
            this.Dist.AppearanceHeader.Options.UseTextOptions = true;
            this.Dist.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Dist.Caption = "����, ��";
            this.Dist.FieldName = "Distance";
            this.Dist.Name = "Dist";
            this.Dist.OptionsColumn.AllowEdit = false;
            this.Dist.Visible = true;
            this.Dist.VisibleIndex = 4;
            // 
            // Description
            // 
            this.Description.AppearanceHeader.Options.UseTextOptions = true;
            this.Description.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Description.Caption = "�������";
            this.Description.FieldName = "Description";
            this.Description.Name = "Description";
            this.Description.OptionsColumn.AllowEdit = false;
            this.Description.Width = 221;
            // 
            // Speed
            // 
            this.Speed.AppearanceHeader.Options.UseTextOptions = true;
            this.Speed.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Speed.Caption = "��������, ��/�";
            this.Speed.FieldName = "Speed";
            this.Speed.Name = "Speed";
            this.Speed.OptionsColumn.AllowEdit = false;
            this.Speed.Visible = true;
            this.Speed.VisibleIndex = 5;
            this.Speed.Width = 134;
            // 
            // repositoryItemLookUpEdit3
            // 
            this.repositoryItemLookUpEdit3.AutoHeight = false;
            this.repositoryItemLookUpEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit3.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "��������")});
            this.repositoryItemLookUpEdit3.DisplayMember = "Name";
            this.repositoryItemLookUpEdit3.Name = "repositoryItemLookUpEdit3";
            this.repositoryItemLookUpEdit3.ShowDropDown = DevExpress.XtraEditors.Controls.ShowDropDown.DoubleClick;
            this.repositoryItemLookUpEdit3.ValueMember = "Id";
            // 
            // repositoryItemLookUpEdit4
            // 
            this.repositoryItemLookUpEdit4.AutoHeight = false;
            this.repositoryItemLookUpEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit4.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "��������")});
            this.repositoryItemLookUpEdit4.DisplayMember = "Name";
            this.repositoryItemLookUpEdit4.Name = "repositoryItemLookUpEdit4";
            this.repositoryItemLookUpEdit4.ValueMember = "Id";
            // 
            // xtraTabPage3
            // 
            this.xtraTabPage3.Controls.Add(this.gcFuel);
            this.xtraTabPage3.Name = "xtraTabPage3";
            this.xtraTabPage3.Size = new System.Drawing.Size(904, 343);
            this.xtraTabPage3.Text = "������� ";
            // 
            // gcFuel
            // 
            this.gcFuel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcFuel.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gcFuel_EmbeddedNavigator_ButtonClick);
            this.gcFuel.Location = new System.Drawing.Point(0, 0);
            this.gcFuel.MainView = this.gvFuel;
            this.gcFuel.Name = "gcFuel";
            this.gcFuel.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemLookUpEdit5,
            this.repositoryItemLookUpEdit6,
            this.teDate});
            this.gcFuel.Size = new System.Drawing.Size(904, 343);
            this.gcFuel.TabIndex = 3;
            this.gcFuel.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvFuel,
            this.gridView1});
            // 
            // gvFuel
            // 
            this.gvFuel.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvFuel.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvFuel.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gvFuel.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvFuel.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvFuel.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvFuel.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvFuel.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvFuel.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gvFuel.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvFuel.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvFuel.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvFuel.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvFuel.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gvFuel.Appearance.Empty.Options.UseBackColor = true;
            this.gvFuel.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvFuel.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvFuel.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gvFuel.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvFuel.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gvFuel.Appearance.EvenRow.Options.UseForeColor = true;
            this.gvFuel.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvFuel.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvFuel.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gvFuel.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvFuel.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvFuel.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gvFuel.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvFuel.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gvFuel.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvFuel.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvFuel.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvFuel.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.gvFuel.Appearance.FixedLine.Options.UseBackColor = true;
            this.gvFuel.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gvFuel.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gvFuel.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gvFuel.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gvFuel.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(192)))), ((int)(((byte)(157)))));
            this.gvFuel.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(219)))), ((int)(((byte)(188)))));
            this.gvFuel.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gvFuel.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvFuel.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gvFuel.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvFuel.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvFuel.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvFuel.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvFuel.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvFuel.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvFuel.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gvFuel.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvFuel.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvFuel.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvFuel.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvFuel.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvFuel.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvFuel.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gvFuel.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvFuel.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvFuel.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gvFuel.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(242)))), ((int)(((byte)(213)))));
            this.gvFuel.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gvFuel.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gvFuel.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvFuel.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvFuel.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvFuel.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvFuel.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gvFuel.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvFuel.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gvFuel.Appearance.GroupRow.Options.UseForeColor = true;
            this.gvFuel.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvFuel.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvFuel.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gvFuel.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvFuel.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvFuel.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gvFuel.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvFuel.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvFuel.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gvFuel.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvFuel.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.gvFuel.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gvFuel.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvFuel.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvFuel.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvFuel.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvFuel.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gvFuel.Appearance.OddRow.Options.UseBackColor = true;
            this.gvFuel.Appearance.OddRow.Options.UseBorderColor = true;
            this.gvFuel.Appearance.OddRow.Options.UseForeColor = true;
            this.gvFuel.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(247)))));
            this.gvFuel.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gvFuel.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(148)))), ((int)(((byte)(148)))));
            this.gvFuel.Appearance.Preview.Options.UseBackColor = true;
            this.gvFuel.Appearance.Preview.Options.UseFont = true;
            this.gvFuel.Appearance.Preview.Options.UseForeColor = true;
            this.gvFuel.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvFuel.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gvFuel.Appearance.Row.Options.UseBackColor = true;
            this.gvFuel.Appearance.Row.Options.UseForeColor = true;
            this.gvFuel.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvFuel.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gvFuel.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvFuel.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.gvFuel.Appearance.SelectedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvFuel.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.gvFuel.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvFuel.Appearance.SelectedRow.Options.UseBorderColor = true;
            this.gvFuel.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gvFuel.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gvFuel.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gvFuel.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvFuel.Appearance.VertLine.Options.UseBackColor = true;
            this.gvFuel.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.Id_fuel,
            this.Location_f,
            this.time_,
            this.dValueCalc,
            this.ValueHandle});
            this.gvFuel.GridControl = this.gcFuel;
            this.gvFuel.Name = "gvFuel";
            this.gvFuel.OptionsView.EnableAppearanceEvenRow = true;
            this.gvFuel.OptionsView.EnableAppearanceOddRow = true;
            this.gvFuel.OptionsView.ShowFooter = true;
            this.gvFuel.InitNewRow += new DevExpress.XtraGrid.Views.Grid.InitNewRowEventHandler(this.gvFuel_InitNewRow);
            this.gvFuel.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gvFuel_CellValueChanged);
            this.gvFuel.InvalidRowException += new DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventHandler(this.gvFuel_InvalidRowException);
            this.gvFuel.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.gvFuel_ValidateRow);
            // 
            // Id_fuel
            // 
            this.Id_fuel.AppearanceHeader.Options.UseTextOptions = true;
            this.Id_fuel.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Id_fuel.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Id_fuel.Caption = "Id";
            this.Id_fuel.FieldName = "Id";
            this.Id_fuel.Name = "Id_fuel";
            this.Id_fuel.OptionsColumn.AllowEdit = false;
            // 
            // Location_f
            // 
            this.Location_f.AppearanceHeader.Options.UseTextOptions = true;
            this.Location_f.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Location_f.Caption = "��������������";
            this.Location_f.FieldName = "Location";
            this.Location_f.Name = "Location_f";
            this.Location_f.OptionsColumn.AllowEdit = false;
            this.Location_f.Visible = true;
            this.Location_f.VisibleIndex = 0;
            this.Location_f.Width = 506;
            // 
            // time_
            // 
            this.time_.AppearanceCell.Options.UseTextOptions = true;
            this.time_.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.time_.AppearanceHeader.Options.UseTextOptions = true;
            this.time_.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.time_.Caption = "�����";
            this.time_.ColumnEdit = this.teDate;
            this.time_.DisplayFormat.FormatString = "dd/MM/yyyy  HH:mm";
            this.time_.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.time_.FieldName = "time_";
            this.time_.Name = "time_";
            this.time_.OptionsColumn.AllowEdit = false;
            this.time_.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Custom, "time_", "�����:")});
            this.time_.Visible = true;
            this.time_.VisibleIndex = 1;
            this.time_.Width = 255;
            // 
            // teDate
            // 
            this.teDate.AutoHeight = false;
            this.teDate.EditFormat.FormatString = "g";
            this.teDate.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.teDate.Name = "teDate";
            // 
            // dValueCalc
            // 
            this.dValueCalc.AppearanceCell.Options.UseTextOptions = true;
            this.dValueCalc.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.dValueCalc.AppearanceHeader.Options.UseTextOptions = true;
            this.dValueCalc.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.dValueCalc.Caption = "��������/����";
            this.dValueCalc.FieldName = "dValueCalc";
            this.dValueCalc.Name = "dValueCalc";
            this.dValueCalc.OptionsColumn.AllowEdit = false;
            this.dValueCalc.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.dValueCalc.Visible = true;
            this.dValueCalc.VisibleIndex = 2;
            this.dValueCalc.Width = 180;
            // 
            // ValueHandle
            // 
            this.ValueHandle.AppearanceCell.Options.UseTextOptions = true;
            this.ValueHandle.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ValueHandle.AppearanceHeader.Options.UseTextOptions = true;
            this.ValueHandle.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ValueHandle.Caption = "���������";
            this.ValueHandle.FieldName = "dValueHandle";
            this.ValueHandle.Name = "ValueHandle";
            this.ValueHandle.OptionsColumn.AllowEdit = false;
            this.ValueHandle.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum)});
            this.ValueHandle.Visible = true;
            this.ValueHandle.VisibleIndex = 3;
            this.ValueHandle.Width = 149;
            // 
            // repositoryItemLookUpEdit5
            // 
            this.repositoryItemLookUpEdit5.AutoHeight = false;
            this.repositoryItemLookUpEdit5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit5.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "��������")});
            this.repositoryItemLookUpEdit5.DisplayMember = "Name";
            this.repositoryItemLookUpEdit5.Name = "repositoryItemLookUpEdit5";
            this.repositoryItemLookUpEdit5.ShowDropDown = DevExpress.XtraEditors.Controls.ShowDropDown.DoubleClick;
            this.repositoryItemLookUpEdit5.ValueMember = "Id";
            // 
            // repositoryItemLookUpEdit6
            // 
            this.repositoryItemLookUpEdit6.AutoHeight = false;
            this.repositoryItemLookUpEdit6.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit6.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "��������")});
            this.repositoryItemLookUpEdit6.DisplayMember = "Name";
            this.repositoryItemLookUpEdit6.Name = "repositoryItemLookUpEdit6";
            this.repositoryItemLookUpEdit6.ValueMember = "Id";
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this.gcFuel;
            this.gridView1.Name = "gridView1";
            // 
            // xtraTabPage4
            // 
            this.xtraTabPage4.Controls.Add(this.pcGraficMoving);
            this.xtraTabPage4.Name = "xtraTabPage4";
            this.xtraTabPage4.Size = new System.Drawing.Size(904, 343);
            this.xtraTabPage4.Text = "������ ��������";
            // 
            // pcGraficMoving
            // 
            this.pcGraficMoving.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pcGraficMoving.Location = new System.Drawing.Point(0, 0);
            this.pcGraficMoving.Name = "pcGraficMoving";
            this.pcGraficMoving.Size = new System.Drawing.Size(904, 343);
            this.pcGraficMoving.TabIndex = 0;
            // 
            // xtraTabPage5
            // 
            this.xtraTabPage5.Controls.Add(this.pcDgrmGant);
            this.xtraTabPage5.Name = "xtraTabPage5";
            this.xtraTabPage5.Size = new System.Drawing.Size(904, 343);
            this.xtraTabPage5.Text = "��������� �����";
            // 
            // pcDgrmGant
            // 
            this.pcDgrmGant.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pcDgrmGant.Location = new System.Drawing.Point(0, 0);
            this.pcDgrmGant.Name = "pcDgrmGant";
            this.pcDgrmGant.Size = new System.Drawing.Size(904, 343);
            this.pcDgrmGant.TabIndex = 2;
            // 
            // dxErrP
            // 
            this.dxErrP.ContainerControl = this;
            // 
            // meRemark
            // 
            this.meRemark.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.meRemark.Location = new System.Drawing.Point(142, 221);
            this.meRemark.Name = "meRemark";
            this.meRemark.Size = new System.Drawing.Size(643, 50);
            this.meRemark.TabIndex = 68;
            this.meRemark.UseOptimizedRendering = true;
            this.meRemark.EditValueChanged += new System.EventHandler(this.meRemark_EditValueChanged);
            // 
            // labelControl9
            // 
            this.labelControl9.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.labelControl9.Location = new System.Drawing.Point(56, 219);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(71, 14);
            this.labelControl9.TabIndex = 67;
            this.labelControl9.Text = "����������";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.labelControl7);
            this.panelControl1.Controls.Add(this.txOutLinkId);
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Controls.Add(this.deEnd);
            this.panelControl1.Controls.Add(this.btDeleteSimple);
            this.panelControl1.Controls.Add(this.leType);
            this.panelControl1.Controls.Add(this.labelControl6);
            this.panelControl1.Controls.Add(this.btAddSimple);
            this.panelControl1.Controls.Add(this.labelControl8);
            this.panelControl1.Controls.Add(this.deStart);
            this.panelControl1.Controls.Add(this.txId);
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.leRoute);
            this.panelControl1.Controls.Add(this.labelControl5);
            this.panelControl1.Controls.Add(this.leDriver);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Controls.Add(this.leMobitel);
            this.panelControl1.Controls.Add(this.leGroupe);
            this.panelControl1.Controls.Add(this.labelControl4);
            this.panelControl1.Location = new System.Drawing.Point(2, 32);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(437, 181);
            this.panelControl1.TabIndex = 70;
            // 
            // labelControl7
            // 
            this.labelControl7.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.labelControl7.Location = new System.Drawing.Point(35, 43);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(51, 14);
            this.labelControl7.TabIndex = 96;
            this.labelControl7.Text = "��� ����";
            // 
            // txOutLinkId
            // 
            this.txOutLinkId.EditValue = "";
            this.txOutLinkId.Location = new System.Drawing.Point(120, 40);
            this.txOutLinkId.Name = "txOutLinkId";
            this.txOutLinkId.Properties.Appearance.Options.UseTextOptions = true;
            this.txOutLinkId.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txOutLinkId.Size = new System.Drawing.Size(79, 20);
            this.txOutLinkId.TabIndex = 95;
            this.txOutLinkId.EditValueChanged += new System.EventHandler(this.txOutLinkId_EditValueChanged);
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.labelControl3.Location = new System.Drawing.Point(205, 43);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(39, 14);
            this.labelControl3.TabIndex = 94;
            this.labelControl3.Text = "�����";
            // 
            // deEnd
            // 
            this.deEnd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.deEnd.EditValue = null;
            this.deEnd.Location = new System.Drawing.Point(270, 41);
            this.deEnd.Name = "deEnd";
            this.deEnd.Properties.Appearance.Options.UseTextOptions = true;
            this.deEnd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.deEnd.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deEnd.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deEnd.Properties.CalendarTimeProperties.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F4);
            this.deEnd.Properties.CalendarTimeProperties.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Default;
            this.deEnd.Properties.DisplayFormat.FormatString = "dd/MM/yyyy  HH:mm";
            this.deEnd.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deEnd.Properties.EditFormat.FormatString = "dd/MM/yyyy  HH:mm";
            this.deEnd.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deEnd.Properties.Mask.EditMask = "dd/MM/yyyy  HH:mm";
            this.deEnd.Size = new System.Drawing.Size(141, 20);
            this.deEnd.TabIndex = 93;
            this.deEnd.EditValueChanged += new System.EventHandler(this.deEnd_EditValueChanged);
            this.deEnd.KeyDown += new System.Windows.Forms.KeyEventHandler(this.deEnd_KeyDown);
            // 
            // btDeleteSimple
            // 
            this.btDeleteSimple.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btDeleteSimple.Image = global::Route.Properties.ResourcesImages.Remove_16;
            this.btDeleteSimple.Location = new System.Drawing.Point(363, 149);
            this.btDeleteSimple.Name = "btDeleteSimple";
            this.btDeleteSimple.Size = new System.Drawing.Size(24, 21);
            this.btDeleteSimple.TabIndex = 92;
            this.btDeleteSimple.ToolTip = "�������� ������ �� ������ ��������";
            this.btDeleteSimple.Click += new System.EventHandler(this.btDeleteSimple_Click);
            // 
            // leType
            // 
            this.leType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.leType.Location = new System.Drawing.Point(118, 69);
            this.leType.Name = "leType";
            this.leType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leType.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.leType.Properties.DisplayMember = "Name";
            this.leType.Properties.NullText = "";
            this.leType.Properties.ValueMember = "id";
            this.leType.Size = new System.Drawing.Size(293, 20);
            this.leType.TabIndex = 91;
            this.leType.EditValueChanged += new System.EventHandler(this.leType_EditValueChanged);
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.labelControl6.Location = new System.Drawing.Point(92, 72);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(22, 14);
            this.labelControl6.TabIndex = 90;
            this.labelControl6.Text = "���";
            // 
            // btAddSimple
            // 
            this.btAddSimple.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btAddSimple.Image = global::Route.Properties.ResourcesImages.Add_16;
            this.btAddSimple.Location = new System.Drawing.Point(390, 149);
            this.btAddSimple.Name = "btAddSimple";
            this.btAddSimple.Size = new System.Drawing.Size(24, 21);
            this.btAddSimple.TabIndex = 89;
            this.btAddSimple.ToolTip = "���������� ������� �������� � ���������� ����������� �����";
            this.btAddSimple.Click += new System.EventHandler(this.btAddSimple_Click);
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.labelControl8.Location = new System.Drawing.Point(212, 17);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(32, 14);
            this.labelControl8.TabIndex = 88;
            this.labelControl8.Text = "�����";
            // 
            // deStart
            // 
            this.deStart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.deStart.EditValue = null;
            this.deStart.Location = new System.Drawing.Point(271, 14);
            this.deStart.Name = "deStart";
            this.deStart.Properties.Appearance.Options.UseTextOptions = true;
            this.deStart.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.deStart.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deStart.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deStart.Properties.CalendarTimeProperties.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F4);
            this.deStart.Properties.CalendarTimeProperties.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Default;
            this.deStart.Properties.DisplayFormat.FormatString = "dd/MM/yyyy  HH:mm";
            this.deStart.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deStart.Properties.EditFormat.FormatString = "dd/MM/yyyy  HH:mm";
            this.deStart.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deStart.Properties.Mask.EditMask = "dd/MM/yyyy  HH:mm";
            this.deStart.Size = new System.Drawing.Size(141, 20);
            this.deStart.TabIndex = 87;
            this.deStart.EditValueChanged += new System.EventHandler(this.deStart_EditValueChanged);
            this.deStart.KeyDown += new System.Windows.Forms.KeyEventHandler(this.deStart_KeyDown);
            // 
            // txId
            // 
            this.txId.EditValue = "0";
            this.txId.Enabled = false;
            this.txId.Location = new System.Drawing.Point(120, 14);
            this.txId.Name = "txId";
            this.txId.Properties.Appearance.Options.UseTextOptions = true;
            this.txId.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txId.Size = new System.Drawing.Size(79, 20);
            this.txId.TabIndex = 78;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.labelControl1.Location = new System.Drawing.Point(35, 16);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(37, 14);
            this.labelControl1.TabIndex = 77;
            this.labelControl1.Text = "�����";
            // 
            // leRoute
            // 
            this.leRoute.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.leRoute.Location = new System.Drawing.Point(118, 149);
            this.leRoute.Name = "leRoute";
            this.leRoute.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leRoute.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.leRoute.Properties.DisplayMember = "Name";
            this.leRoute.Properties.NullText = "";
            this.leRoute.Properties.ValueMember = "id";
            this.leRoute.Size = new System.Drawing.Size(242, 20);
            this.leRoute.TabIndex = 86;
            this.leRoute.EditValueChanged += new System.EventHandler(this.leRoute_EditValueChanged);
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.labelControl5.Location = new System.Drawing.Point(12, 152);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(102, 14);
            this.labelControl5.TabIndex = 85;
            this.labelControl5.Text = "������ ��������";
            // 
            // leDriver
            // 
            this.leDriver.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.leDriver.Location = new System.Drawing.Point(118, 121);
            this.leDriver.Name = "leDriver";
            this.leDriver.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leDriver.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.leDriver.Properties.DisplayMember = "Name";
            this.leDriver.Properties.NullText = "";
            this.leDriver.Properties.ValueMember = "id";
            this.leDriver.Size = new System.Drawing.Size(293, 20);
            this.leDriver.TabIndex = 84;
            this.leDriver.EditValueChanged += new System.EventHandler(this.leDriver_EditValueChanged);
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.labelControl2.Location = new System.Drawing.Point(60, 126);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(54, 14);
            this.labelControl2.TabIndex = 83;
            this.labelControl2.Text = "��������";
            // 
            // leMobitel
            // 
            this.leMobitel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.leMobitel.Location = new System.Drawing.Point(221, 95);
            this.leMobitel.Name = "leMobitel";
            this.leMobitel.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leMobitel.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.leMobitel.Properties.DisplayMember = "Name";
            this.leMobitel.Properties.NullText = "";
            this.leMobitel.Properties.ValueMember = "id";
            this.leMobitel.Size = new System.Drawing.Size(190, 20);
            this.leMobitel.TabIndex = 82;
            this.leMobitel.EditValueChanged += new System.EventHandler(this.leMobitel_EditValueChanged);
            // 
            // leGroupe
            // 
            this.leGroupe.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.leGroupe.Location = new System.Drawing.Point(118, 95);
            this.leGroupe.Name = "leGroupe";
            this.leGroupe.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leGroupe.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.leGroupe.Properties.DisplayMember = "Name";
            this.leGroupe.Properties.NullText = "";
            this.leGroupe.Properties.ValueMember = "id";
            this.leGroupe.Size = new System.Drawing.Size(97, 20);
            this.leGroupe.TabIndex = 81;
            this.leGroupe.EditValueChanged += new System.EventHandler(this.leGroupe_EditValueChanged);
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.labelControl4.Location = new System.Drawing.Point(71, 97);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(43, 14);
            this.labelControl4.TabIndex = 80;
            this.labelControl4.Text = "������";
            // 
            // panelControl5
            // 
            this.panelControl5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl5.Controls.Add(this.pgTask);
            this.panelControl5.Location = new System.Drawing.Point(455, 32);
            this.panelControl5.Name = "panelControl5";
            this.panelControl5.Size = new System.Drawing.Size(455, 185);
            this.panelControl5.TabIndex = 72;
            // 
            // pgTask
            // 
            this.pgTask.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pgTask.Location = new System.Drawing.Point(2, 2);
            this.pgTask.Name = "pgTask";
            this.pgTask.RecordWidth = 93;
            this.pgTask.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.pbValidity});
            this.pgTask.RowHeaderWidth = 107;
            this.pgTask.Rows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.TotalData,
            this.CatPath,
            this.CatTime,
            this.FuelDUT,
            this.FuelDRT,
            this.ValidityData});
            this.pgTask.Size = new System.Drawing.Size(451, 181);
            this.pgTask.TabIndex = 74;
            // 
            // pbValidity
            // 
            this.pbValidity.Name = "pbValidity";
            this.pbValidity.ShowTitle = true;
            // 
            // TotalData
            // 
            this.TotalData.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.SampleName,
            this.VehicleName,
            this.DriverName,
            this.RemarkPG});
            this.TotalData.Height = 19;
            this.TotalData.Name = "TotalData";
            this.TotalData.Properties.Caption = "����� ����������";
            // 
            // SampleName
            // 
            this.SampleName.Name = "SampleName";
            this.SampleName.Properties.Caption = "�������";
            this.SampleName.Properties.FieldName = "SampleName";
            this.SampleName.Properties.ReadOnly = true;
            // 
            // VehicleName
            // 
            this.VehicleName.Height = 16;
            this.VehicleName.Name = "VehicleName";
            this.VehicleName.Properties.Caption = "������";
            this.VehicleName.Properties.FieldName = "VehicleName";
            this.VehicleName.Properties.ReadOnly = true;
            // 
            // DriverName
            // 
            this.DriverName.Name = "DriverName";
            this.DriverName.Properties.Caption = "��������";
            this.DriverName.Properties.FieldName = "DriverName";
            this.DriverName.Properties.ReadOnly = true;
            // 
            // RemarkPG
            // 
            this.RemarkPG.Name = "RemarkPG";
            this.RemarkPG.Properties.Caption = "����������";
            this.RemarkPG.Properties.FieldName = "Remark";
            this.RemarkPG.Properties.ReadOnly = true;
            // 
            // CatPath
            // 
            this.CatPath.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.Distance,
            this.TimeMoveProc,
            this.TimeStopProc,
            this.SpeedAvg,
            this.DistanceWithLogicSensor});
            this.CatPath.Name = "CatPath";
            this.CatPath.Properties.Caption = "��������� ��������";
            // 
            // Distance
            // 
            this.Distance.Appearance.Options.UseTextOptions = true;
            this.Distance.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.Distance.Expanded = false;
            this.Distance.Name = "Distance";
            this.Distance.Properties.Caption = "����, ��";
            this.Distance.Properties.FieldName = "Distance";
            this.Distance.Properties.ReadOnly = true;
            // 
            // TimeMoveProc
            // 
            this.TimeMoveProc.Appearance.Options.UseTextOptions = true;
            this.TimeMoveProc.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.TimeMoveProc.Height = 16;
            this.TimeMoveProc.Name = "TimeMoveProc";
            this.TimeMoveProc.Properties.Caption = "����� ��������";
            this.TimeMoveProc.Properties.FieldName = "TimeMoveProc";
            this.TimeMoveProc.Properties.ReadOnly = true;
            // 
            // TimeStopProc
            // 
            this.TimeStopProc.Appearance.Options.UseTextOptions = true;
            this.TimeStopProc.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.TimeStopProc.Height = 16;
            this.TimeStopProc.Name = "TimeStopProc";
            this.TimeStopProc.Properties.Caption = "����� ���������";
            this.TimeStopProc.Properties.FieldName = "TimeStopProc";
            this.TimeStopProc.Properties.ReadOnly = true;
            // 
            // SpeedAvg
            // 
            this.SpeedAvg.Appearance.Options.UseTextOptions = true;
            this.SpeedAvg.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.SpeedAvg.Enabled = false;
            this.SpeedAvg.Name = "SpeedAvg";
            this.SpeedAvg.Properties.Caption = "������� �������� ��������, ��/�";
            this.SpeedAvg.Properties.FieldName = "SpeedAvg";
            this.SpeedAvg.Properties.ReadOnly = true;
            // 
            // DistanceWithLogicSensor
            // 
            this.DistanceWithLogicSensor.Name = "DistanceWithLogicSensor";
            this.DistanceWithLogicSensor.Properties.Caption = "���� � ���������� ���������� ��������, ��";
            this.DistanceWithLogicSensor.Properties.FieldName = "DistanceWithLogicSensor";
            this.DistanceWithLogicSensor.Properties.ReadOnly = true;
            // 
            // CatTime
            // 
            this.CatTime.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.TimePlanTotal,
            this.TimeFactTotal,
            this.DeviationRow,
            this.DeviationAr});
            this.CatTime.Expanded = false;
            this.CatTime.Height = 19;
            this.CatTime.Name = "CatTime";
            this.CatTime.Properties.Caption = "��������� �������";
            // 
            // TimePlanTotal
            // 
            this.TimePlanTotal.Appearance.Options.UseTextOptions = true;
            this.TimePlanTotal.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.TimePlanTotal.Name = "TimePlanTotal";
            this.TimePlanTotal.Properties.Caption = "����������������� �������� (����)";
            this.TimePlanTotal.Properties.FieldName = "TimePlanTotal";
            this.TimePlanTotal.Properties.ReadOnly = true;
            // 
            // TimeFactTotal
            // 
            this.TimeFactTotal.Appearance.Options.UseTextOptions = true;
            this.TimeFactTotal.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.TimeFactTotal.Height = 16;
            this.TimeFactTotal.Name = "TimeFactTotal";
            this.TimeFactTotal.Properties.Caption = "����������������� �������� (����)";
            this.TimeFactTotal.Properties.FieldName = "TimeFactTotal";
            this.TimeFactTotal.Properties.ReadOnly = true;
            // 
            // DeviationRow
            // 
            this.DeviationRow.Appearance.Options.UseTextOptions = true;
            this.DeviationRow.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.DeviationRow.Name = "DeviationRow";
            this.DeviationRow.Properties.Caption = "���������� �� �����������������";
            this.DeviationRow.Properties.FieldName = "Deviation";
            this.DeviationRow.Properties.ReadOnly = true;
            // 
            // DeviationAr
            // 
            this.DeviationAr.Appearance.Options.UseTextOptions = true;
            this.DeviationAr.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.DeviationAr.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.DeviationAr.Name = "DeviationAr";
            this.DeviationAr.Properties.Caption = "���������� �� ������� ��������";
            this.DeviationAr.Properties.FieldName = "DeviationAr";
            this.DeviationAr.Properties.ReadOnly = true;
            // 
            // FuelDUT
            // 
            this.FuelDUT.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.FuelStart,
            this.FuelEnd,
            this.FuelAddQty,
            this.FuelAdd,
            this.FuelSubQty,
            this.FuelSub,
            this.FuelExpens,
            this.FuelExpensAvg});
            this.FuelDUT.Expanded = false;
            this.FuelDUT.Name = "FuelDUT";
            this.FuelDUT.Properties.Caption = "������� (���)";
            // 
            // FuelStart
            // 
            this.FuelStart.Name = "FuelStart";
            this.FuelStart.Properties.Caption = "������� � ������, �";
            this.FuelStart.Properties.FieldName = "FuelStart";
            this.FuelStart.Properties.ReadOnly = true;
            // 
            // FuelEnd
            // 
            this.FuelEnd.Name = "FuelEnd";
            this.FuelEnd.Properties.Caption = "������� � �����, �";
            this.FuelEnd.Properties.FieldName = "FuelEnd";
            this.FuelEnd.Properties.ReadOnly = true;
            // 
            // FuelAddQty
            // 
            this.FuelAddQty.Name = "FuelAddQty";
            this.FuelAddQty.Properties.Caption = "���������� ��������";
            this.FuelAddQty.Properties.FieldName = "FuelAddQty";
            this.FuelAddQty.Properties.ReadOnly = true;
            // 
            // FuelAdd
            // 
            this.FuelAdd.Name = "FuelAdd";
            this.FuelAdd.Properties.Caption = "����������, �";
            this.FuelAdd.Properties.FieldName = "FuelAdd";
            this.FuelAdd.Properties.ReadOnly = true;
            // 
            // FuelSubQty
            // 
            this.FuelSubQty.Name = "FuelSubQty";
            this.FuelSubQty.Properties.Caption = "���������� ������";
            this.FuelSubQty.Properties.FieldName = "FuelSubQty";
            this.FuelSubQty.Properties.ReadOnly = true;
            // 
            // FuelSub
            // 
            this.FuelSub.Name = "FuelSub";
            this.FuelSub.Properties.Caption = "�����, �";
            this.FuelSub.Properties.FieldName = "FuelSub";
            this.FuelSub.Properties.ReadOnly = true;
            // 
            // FuelExpens
            // 
            this.FuelExpens.Name = "FuelExpens";
            this.FuelExpens.Properties.Caption = "����� ������, �";
            this.FuelExpens.Properties.FieldName = "FuelExpens";
            this.FuelExpens.Properties.ReadOnly = true;
            // 
            // FuelExpensAvg
            // 
            this.FuelExpensAvg.Name = "FuelExpensAvg";
            this.FuelExpensAvg.Properties.Caption = "������� ������,  �/100 ��";
            this.FuelExpensAvg.Properties.FieldName = "FuelExpensAvg";
            this.FuelExpensAvg.Properties.ReadOnly = true;
            // 
            // FuelDRT
            // 
            this.FuelDRT.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.Fuel_ExpensMove,
            this.Fuel_ExpensStop,
            this.Fuel_ExpensTotal,
            this.Fuel_ExpensAvg});
            this.FuelDRT.Expanded = false;
            this.FuelDRT.Name = "FuelDRT";
            this.FuelDRT.Properties.Caption = "������� (���/CAN)";
            // 
            // Fuel_ExpensMove
            // 
            this.Fuel_ExpensMove.Name = "Fuel_ExpensMove";
            this.Fuel_ExpensMove.Properties.Caption = "������ ������� � ��������, �";
            this.Fuel_ExpensMove.Properties.FieldName = "Fuel_ExpensMove";
            this.Fuel_ExpensMove.Properties.ReadOnly = true;
            // 
            // Fuel_ExpensStop
            // 
            this.Fuel_ExpensStop.Name = "Fuel_ExpensStop";
            this.Fuel_ExpensStop.Properties.Caption = "������ ������� �� ��������, �";
            this.Fuel_ExpensStop.Properties.FieldName = "Fuel_ExpensStop";
            this.Fuel_ExpensStop.Properties.ReadOnly = true;
            // 
            // Fuel_ExpensTotal
            // 
            this.Fuel_ExpensTotal.Name = "Fuel_ExpensTotal";
            this.Fuel_ExpensTotal.Properties.Caption = "����� ������, �";
            this.Fuel_ExpensTotal.Properties.FieldName = "Fuel_ExpensTotal";
            this.Fuel_ExpensTotal.Properties.ReadOnly = true;
            // 
            // Fuel_ExpensAvg
            // 
            this.Fuel_ExpensAvg.Name = "Fuel_ExpensAvg";
            this.Fuel_ExpensAvg.Properties.Caption = "������� ������, �/100 ��";
            this.Fuel_ExpensAvg.Properties.FieldName = "Fuel_ExpensAvg";
            this.Fuel_ExpensAvg.Properties.ReadOnly = true;
            // 
            // ValidityData
            // 
            this.ValidityData.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.PointsValidity,
            this.PointsCalc,
            this.PointsFact,
            this.PointsIntervalMax});
            this.ValidityData.Expanded = false;
            this.ValidityData.Name = "ValidityData";
            this.ValidityData.Properties.Caption = "�������� ������";
            // 
            // PointsValidity
            // 
            this.PointsValidity.Appearance.Options.UseTextOptions = true;
            this.PointsValidity.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.PointsValidity.Name = "PointsValidity";
            this.PointsValidity.Properties.Caption = "������� ������";
            this.PointsValidity.Properties.FieldName = "PointsValidity";
            this.PointsValidity.Properties.ReadOnly = true;
            this.PointsValidity.Properties.RowEdit = this.pbValidity;
            // 
            // PointsCalc
            // 
            this.PointsCalc.Appearance.Options.UseTextOptions = true;
            this.PointsCalc.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.PointsCalc.Name = "PointsCalc";
            this.PointsCalc.Properties.Caption = "���������� ����� (�� Log_ID) ���������";
            this.PointsCalc.Properties.FieldName = "PointsCalc";
            this.PointsCalc.Properties.ReadOnly = true;
            // 
            // PointsFact
            // 
            this.PointsFact.Appearance.Options.UseTextOptions = true;
            this.PointsFact.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.PointsFact.Name = "PointsFact";
            this.PointsFact.Properties.Caption = "���������� ����� (�� Log_ID) �����������";
            this.PointsFact.Properties.FieldName = "PointsFact";
            this.PointsFact.Properties.ReadOnly = true;
            // 
            // PointsIntervalMax
            // 
            this.PointsIntervalMax.Appearance.Options.UseTextOptions = true;
            this.PointsIntervalMax.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.PointsIntervalMax.Name = "PointsIntervalMax";
            this.PointsIntervalMax.Properties.Caption = "������������ �������� ����� �������";
            this.PointsIntervalMax.Properties.FieldName = "PointsIntervalMax";
            this.PointsIntervalMax.Properties.ReadOnly = true;
            // 
            // gcQuality
            // 
            this.gcQuality.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.gcQuality.Controls.Add(this.elLightsIndicator);
            this.gcQuality.Location = new System.Drawing.Point(791, 219);
            this.gcQuality.Name = "gcQuality";
            this.gcQuality.Size = new System.Drawing.Size(116, 72);
            this.gcQuality.TabIndex = 74;
            this.gcQuality.Text = "������� ������";
            // 
            // elLightsIndicator
            // 
            this.elLightsIndicator.AutoLayout = false;
            this.elLightsIndicator.BackColor = System.Drawing.Color.Transparent;
            this.elLightsIndicator.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.elLightsIndicator.Gauges.AddRange(new DevExpress.XtraGauges.Base.IGauge[] {
            this.stateIndicatorGauge1});
            this.elLightsIndicator.Location = new System.Drawing.Point(26, 27);
            this.elLightsIndicator.Name = "elLightsIndicator";
            this.elLightsIndicator.Size = new System.Drawing.Size(61, 44);
            this.elLightsIndicator.TabIndex = 75;
            // 
            // stateIndicatorGauge1
            // 
            this.stateIndicatorGauge1.Bounds = new System.Drawing.Rectangle(10, 2, 38, 33);
            this.stateIndicatorGauge1.Indicators.AddRange(new DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent[] {
            this.stateIndicatorComponent1});
            this.stateIndicatorGauge1.Name = "stateIndicatorGauge1";
            // 
            // stateIndicatorComponent1
            // 
            this.stateIndicatorComponent1.Center = new DevExpress.XtraGauges.Core.Base.PointF2D(125F, 125F);
            this.stateIndicatorComponent1.Name = "stateIndicatorComponent1";
            this.stateIndicatorComponent1.Size = new System.Drawing.SizeF(200F, 200F);
            this.stateIndicatorComponent1.StateIndex = 1;
            indicatorState5.Name = "State1";
            indicatorState5.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight1;
            indicatorState6.Name = "State2";
            indicatorState6.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight2;
            indicatorState7.Name = "State3";
            indicatorState7.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight3;
            indicatorState8.Name = "State4";
            indicatorState8.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.ElectricLight4;
            this.stateIndicatorComponent1.States.AddRange(new DevExpress.XtraGauges.Core.Model.IIndicatorState[] {
            indicatorState5,
            indicatorState6,
            indicatorState7,
            indicatorState8});
            // 
            // bmTask
            // 
            this.bmTask.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.barButtons,
            this.bar1,
            this.barGrid});
            this.bmTask.DockControls.Add(this.barDockControl1);
            this.bmTask.DockControls.Add(this.barDockControl2);
            this.bmTask.DockControls.Add(this.barDockControl3);
            this.bmTask.DockControls.Add(this.barDockControl4);
            this.bmTask.Form = this;
            this.bmTask.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btMapView,
            this.btFact,
            this.btDelete,
            this.btSave,
            this.bsiExcel,
            this.btExcelTotal,
            this.btExcel,
            this.bsiStatus,
            this.chEdit,
            this.btClear,
            this.btRecalcFuel,
            this.btRefresh,
            this.pbStatus,
            this.btReport,
            this.bsiReports,
            this.btReportZone,
            this.btReportStop,
            this.btReportSensor,
            this.btReportFuel,
            this.btTamplate});
            this.bmTask.MainMenu = this.barButtons;
            this.bmTask.MaxItemId = 25;
            this.bmTask.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit1,
            this.rpbStatus});
            // 
            // barButtons
            // 
            this.barButtons.BarName = "Main menu";
            this.barButtons.DockCol = 0;
            this.barButtons.DockRow = 0;
            this.barButtons.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.barButtons.FloatLocation = new System.Drawing.Point(57, 150);
            this.barButtons.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btSave, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btRefresh, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btTamplate, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btFact, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btMapView, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bsiReports, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bsiExcel, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btDelete, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.barButtons.OptionsBar.AllowQuickCustomization = false;
            this.barButtons.OptionsBar.DrawDragBorder = false;
            this.barButtons.OptionsBar.MultiLine = true;
            this.barButtons.OptionsBar.UseWholeRow = true;
            this.barButtons.Text = "Main menu";
            // 
            // btSave
            // 
            this.btSave.Caption = "���������";
            this.btSave.Glyph = global::Route.Properties.ResourcesImages.Save;
            this.btSave.Id = 4;
            this.btSave.Name = "btSave";
            toolTipItem12.Text = "��������� ���������� ����";
            superToolTip12.Items.Add(toolTipItem12);
            this.btSave.SuperTip = superToolTip12;
            this.btSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btSave_ItemClick);
            // 
            // btRefresh
            // 
            this.btRefresh.Caption = "��������";
            this.btRefresh.Glyph = global::Route.Properties.ResourcesImages.ref_161;
            this.btRefresh.Id = 14;
            this.btRefresh.Name = "btRefresh";
            toolTipItem16.Text = "�������� ���������� ����";
            superToolTip16.Items.Add(toolTipItem16);
            this.btRefresh.SuperTip = superToolTip16;
            this.btRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btRefresh_ItemClick);
            // 
            // btTamplate
            // 
            this.btTamplate.Caption = "������� ������  ";
            this.btTamplate.Glyph = ((System.Drawing.Image)(resources.GetObject("btTamplate.Glyph")));
            this.btTamplate.Id = 24;
            this.btTamplate.Name = "btTamplate";
            this.btTamplate.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btTamplate_ItemClick);
            // 
            // btFact
            // 
            this.btFact.Caption = "����";
            this.btFact.Glyph = global::Route.Properties.ResourcesImages.calculator;
            this.btFact.Id = 2;
            this.btFact.Name = "btFact";
            toolTipItem10.Text = "����������� ����������� ����������";
            superToolTip10.Items.Add(toolTipItem10);
            this.btFact.SuperTip = superToolTip10;
            this.btFact.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btFact_ItemClick);
            // 
            // btMapView
            // 
            this.btMapView.Caption = "�����";
            this.btMapView.Glyph = global::Route.Properties.ResourcesImages.Map;
            this.btMapView.Id = 0;
            this.btMapView.Name = "btMapView";
            toolTipItem9.Text = "����������� ���� �������� �� �����";
            superToolTip9.Items.Add(toolTipItem9);
            this.btMapView.SuperTip = superToolTip9;
            this.btMapView.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btMapView_ItemClick);
            // 
            // bsiReports
            // 
            this.bsiReports.Caption = "������";
            this.bsiReports.Glyph = global::Route.Properties.ResourcesImages.reports_stack;
            this.bsiReports.Id = 19;
            this.bsiReports.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btReport, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(this.btReportZone),
            new DevExpress.XtraBars.LinkPersistInfo(this.btReportStop),
            new DevExpress.XtraBars.LinkPersistInfo(this.btReportSensor),
            new DevExpress.XtraBars.LinkPersistInfo(this.btReportFuel)});
            this.bsiReports.Name = "bsiReports";
            // 
            // btReport
            // 
            this.btReport.Caption = "�����";
            this.btReport.Glyph = global::Route.Properties.ResourcesImages.report;
            this.btReport.Id = 18;
            this.btReport.Name = "btReport";
            this.btReport.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btReport_ItemClick);
            // 
            // btReportZone
            // 
            this.btReportZone.Caption = "����������";
            this.btReportZone.Glyph = global::Route.Properties.ResourcesImages.report;
            this.btReportZone.Id = 20;
            this.btReportZone.Name = "btReportZone";
            this.btReportZone.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btReportZone_ItemClick);
            // 
            // btReportStop
            // 
            this.btReportStop.Caption = "���������";
            this.btReportStop.Glyph = global::Route.Properties.ResourcesImages.report;
            this.btReportStop.Id = 21;
            this.btReportStop.Name = "btReportStop";
            this.btReportStop.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btReportStop_ItemClick);
            // 
            // btReportSensor
            // 
            this.btReportSensor.Caption = "������� ��������";
            this.btReportSensor.Glyph = global::Route.Properties.ResourcesImages.report;
            this.btReportSensor.Id = 22;
            this.btReportSensor.Name = "btReportSensor";
            this.btReportSensor.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btReportSensor_ItemClick);
            // 
            // btReportFuel
            // 
            this.btReportFuel.Caption = "�������� / �����";
            this.btReportFuel.Glyph = global::Route.Properties.ResourcesImages.report;
            this.btReportFuel.Id = 23;
            this.btReportFuel.Name = "btReportFuel";
            this.btReportFuel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btReportFuel_ItemClick);
            // 
            // bsiExcel
            // 
            this.bsiExcel.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Left;
            this.bsiExcel.Caption = "������� � Excel";
            this.bsiExcel.Glyph = global::Route.Properties.ResourcesImages.doc_excel_table;
            this.bsiExcel.Id = 5;
            this.bsiExcel.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btExcelTotal),
            new DevExpress.XtraBars.LinkPersistInfo(this.btExcel)});
            this.bsiExcel.Name = "bsiExcel";
            toolTipItem13.Text = "������� � Excel ������ ����������� �����";
            superToolTip13.Items.Add(toolTipItem13);
            this.bsiExcel.SuperTip = superToolTip13;
            // 
            // btExcelTotal
            // 
            this.btExcelTotal.Caption = "������� ����������";
            this.btExcelTotal.Id = 6;
            this.btExcelTotal.Name = "btExcelTotal";
            this.btExcelTotal.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btExcelTotal_ItemClick);
            // 
            // btExcel
            // 
            this.btExcel.Caption = "������� ������";
            this.btExcel.Id = 7;
            this.btExcel.Name = "btExcel";
            this.btExcel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btExcel_ItemClick);
            // 
            // btDelete
            // 
            this.btDelete.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.btDelete.Caption = "�������";
            this.btDelete.Glyph = global::Route.Properties.ResourcesImages.Remove_16;
            this.btDelete.Id = 3;
            this.btDelete.Name = "btDelete";
            toolTipItem11.Text = "������� ���������� ����";
            superToolTip11.Items.Add(toolTipItem11);
            this.btDelete.SuperTip = superToolTip11;
            this.btDelete.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.btDelete.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btDelete_ItemClick);
            // 
            // bar1
            // 
            this.bar1.BarName = "StatusBar";
            this.bar1.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.Width, this.pbStatus, "", false, true, true, 148),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiStatus)});
            this.bar1.OptionsBar.AllowRename = true;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "StatusBar";
            // 
            // pbStatus
            // 
            this.pbStatus.Caption = "barEditItem1";
            this.pbStatus.Edit = this.rpbStatus;
            this.pbStatus.Id = 15;
            this.pbStatus.Name = "pbStatus";
            this.pbStatus.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // rpbStatus
            // 
            this.rpbStatus.Name = "rpbStatus";
            // 
            // bsiStatus
            // 
            this.bsiStatus.Caption = "������";
            this.bsiStatus.Id = 9;
            this.bsiStatus.Name = "bsiStatus";
            this.bsiStatus.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barGrid
            // 
            this.barGrid.BarName = "GridBar";
            this.barGrid.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.barGrid.DockCol = 0;
            this.barGrid.DockRow = 1;
            this.barGrid.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.barGrid.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.chEdit, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btClear, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btRecalcFuel, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.barGrid.OptionsBar.AllowRename = true;
            this.barGrid.OptionsBar.UseWholeRow = true;
            this.barGrid.Text = "GridBar";
            // 
            // chEdit
            // 
            this.chEdit.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.chEdit.Caption = "�������������";
            this.chEdit.Glyph = global::Route.Properties.ResourcesImages.Edit_P_02_18;
            this.chEdit.Id = 10;
            this.chEdit.Name = "chEdit";
            toolTipItem14.Text = "��������� / ���������� ������ �������������� ����������� ��������";
            superToolTip14.Items.Add(toolTipItem14);
            this.chEdit.SuperTip = superToolTip14;
            this.chEdit.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.chEdit_CheckedChanged);
            // 
            // btClear
            // 
            this.btClear.Caption = "�������� ����������";
            this.btClear.Enabled = false;
            this.btClear.Glyph = global::Route.Properties.ResourcesImages._1280996123_table__delete__16x16;
            this.btClear.Id = 11;
            this.btClear.Name = "btClear";
            toolTipItem15.Text = "������ ������� ����������� ��������";
            superToolTip15.Items.Add(toolTipItem15);
            this.btClear.SuperTip = superToolTip15;
            this.btClear.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btClear_ItemClick);
            // 
            // btRecalcFuel
            // 
            this.btRecalcFuel.Caption = "�����������";
            this.btRecalcFuel.Enabled = false;
            this.btRecalcFuel.Glyph = global::Route.Properties.ResourcesImages.ref_161;
            this.btRecalcFuel.Id = 12;
            this.btRecalcFuel.Name = "btRecalcFuel";
            this.btRecalcFuel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btRecalcFuel_ItemClick);
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Size = new System.Drawing.Size(922, 24);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 637);
            this.barDockControl2.Size = new System.Drawing.Size(922, 62);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 24);
            this.barDockControl3.Size = new System.Drawing.Size(0, 613);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(922, 24);
            this.barDockControl4.Size = new System.Drawing.Size(0, 613);
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // Task
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(922, 699);
            this.Controls.Add(this.gcQuality);
            this.Controls.Add(this.panelControl5);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.meRemark);
            this.Controls.Add(this.labelControl9);
            this.Controls.Add(this.tbCont);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(798, 568);
            this.Name = "Task";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "���������� ����";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Task_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.gvContentLoc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.icbEvent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.icEvents)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcContent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvContent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ZoneLookUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teDateTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EventLookUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbCont)).EndInit();
            this.tbCont.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            this.xtbContent.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcStop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvStop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.icCheckZone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit2)).EndInit();
            this.xtraTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcSensor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvSensor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit4)).EndInit();
            this.xtraTabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcFuel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvFuel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.xtraTabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pcGraficMoving)).EndInit();
            this.xtraTabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pcDgrmGant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.meRemark.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txOutLinkId.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEnd.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEnd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deStart.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deStart.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txId.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leRoute.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leDriver.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leMobitel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leGroupe.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl5)).EndInit();
            this.panelControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pgTask)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbValidity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcQuality)).EndInit();
            this.gcQuality.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.stateIndicatorGauge1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stateIndicatorComponent1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bmTask)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rpbStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gcContent;
        private DevExpress.XtraGrid.Views.Grid.GridView gvContent;
        private DevExpress.XtraGrid.Columns.GridColumn Id_cont;
        private DevExpress.XtraGrid.Columns.GridColumn Id_zone;
        private DevExpress.XtraGrid.Columns.GridColumn Id_event;
        private DevExpress.XtraGrid.Columns.GridColumn DatePlan;
        private DevExpress.XtraGrid.Columns.GridColumn DateFact;
        private DevExpress.XtraGrid.Columns.GridColumn Deviation;
        private DevExpress.XtraGrid.Columns.GridColumn Remark;
        private DevExpress.XtraTab.XtraTabControl tbCont;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraTab.XtraTabPage xtbContent;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrP;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit ZoneLookUp;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit EventLookUp;
        private DevExpress.XtraEditors.MemoEdit meRemark;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.DateEdit deStart;
        private DevExpress.XtraEditors.TextEdit txId;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LookUpEdit leRoute;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LookUpEdit leDriver;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LookUpEdit leMobitel;
        private DevExpress.XtraEditors.LookUpEdit leGroupe;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraGrid.GridControl gcStop;
        private DevExpress.XtraGrid.Views.Grid.GridView gvStop;
        private DevExpress.XtraGrid.Columns.GridColumn Id_stop;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn Location_s;
        private DevExpress.XtraGrid.Columns.GridColumn InitialTime;
        private DevExpress.XtraGrid.Columns.GridColumn FinalTime;
        private DevExpress.XtraGrid.Columns.GridColumn Interval;
        private DevExpress.XtraGrid.GridControl gcSensor;
        private DevExpress.XtraGrid.Views.Grid.GridView gvSensor;
        private DevExpress.XtraGrid.Columns.GridColumn Id_ev;
        private DevExpress.XtraGrid.Columns.GridColumn SensorName;
        private DevExpress.XtraGrid.Columns.GridColumn Location_ev;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit4;
        private DevExpress.XtraGrid.Columns.GridColumn EventTime;
        private DevExpress.XtraGrid.Columns.GridColumn Duration;
        private DevExpress.XtraGrid.Columns.GridColumn Description;
        private DevExpress.XtraGrid.Columns.GridColumn Speed;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage3;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit teDateTime;
        private DevExpress.Utils.ImageCollection icEvents;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox icbEvent;
        private DevExpress.XtraEditors.PanelControl panelControl5;
        private DevExpress.XtraVerticalGrid.PropertyGridControl pgTask;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow CatPath;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow Distance;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow TimeMoveProc;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow TimeStopProc;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow SpeedAvg;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow CatTime;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow TimePlanTotal;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow TimeFactTotal;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow DeviationRow;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow DeviationAr;
        private DevExpress.XtraGrid.GridControl gcFuel;
        private DevExpress.XtraGrid.Views.Grid.GridView gvFuel;
        private DevExpress.XtraGrid.Columns.GridColumn Id_fuel;
        private DevExpress.XtraGrid.Columns.GridColumn Location_f;
        private DevExpress.XtraGrid.Columns.GridColumn time_;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit teDate;
        private DevExpress.XtraGrid.Columns.GridColumn dValueCalc;
        private DevExpress.XtraGrid.Columns.GridColumn ValueHandle;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit5;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit6;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow FuelDUT;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow FuelStart;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow FuelEnd;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow FuelAddQty;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow FuelAdd;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow FuelSubQty;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow FuelSub;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow FuelExpens;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow FuelExpensAvg;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow FuelDRT;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow Fuel_ExpensMove;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow Fuel_ExpensStop;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow Fuel_ExpensTotal;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow Fuel_ExpensAvg;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow ValidityData;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow PointsValidity;
        private DevExpress.XtraEditors.Repository.RepositoryItemProgressBar pbValidity;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow PointsCalc;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow PointsFact;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow PointsIntervalMax;
        private DevExpress.XtraGrid.Columns.GridColumn DistanceT;
        private DevExpress.XtraEditors.GroupControl gcQuality;
        private DevExpress.XtraGauges.Win.GaugeControl elLightsIndicator;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage4;
        private DevExpress.XtraGrid.Columns.GridColumn DistanceCollect;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage5;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarManager bmTask;
        private DevExpress.XtraBars.Bar barButtons;
        private DevExpress.XtraBars.BarButtonItem btMapView;
        private DevExpress.XtraBars.BarButtonItem btFact;
        private DevExpress.XtraBars.BarButtonItem btDelete;
        private DevExpress.XtraBars.BarButtonItem btSave;
        private DevExpress.XtraBars.BarSubItem bsiExcel;
        private DevExpress.XtraBars.BarButtonItem btExcelTotal;
        private DevExpress.XtraBars.BarButtonItem btExcel;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow TotalData;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow SampleName;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow VehicleName;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow DriverName;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow RemarkPG;
        private DevExpress.XtraEditors.SimpleButton btAddSimple;
        private DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorGauge stateIndicatorGauge1;
        private DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent stateIndicatorComponent1;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarStaticItem bsiStatus;
        private DevExpress.XtraBars.Bar barGrid;
        private DevExpress.XtraBars.BarCheckItem chEdit;
        private DevExpress.XtraBars.BarButtonItem btClear;
        private DevExpress.XtraBars.BarButtonItem btRecalcFuel;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn CheckZone;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox icCheckZone;
        private DevExpress.XtraBars.BarButtonItem btRefresh;
        private DevExpress.XtraGrid.Views.Grid.GridView gvContentLoc;
        private DevExpress.XtraGrid.Columns.GridColumn Id_cont_L;
        private DevExpress.XtraGrid.Columns.GridColumn LocationL;
        private DevExpress.XtraGrid.Columns.GridColumn Id_event�;
        private DevExpress.XtraGrid.Columns.GridColumn DateFactL;
        private DevExpress.XtraGrid.Columns.GridColumn DistanceL;
        private DevExpress.XtraGrid.Columns.GridColumn DistanceCollectL;
        private DevExpress.XtraGrid.Columns.GridColumn RemarkL;
        private DevExpress.XtraEditors.LookUpEdit leType;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraBars.BarEditItem pbStatus;
        private DevExpress.XtraEditors.Repository.RepositoryItemProgressBar rpbStatus;
        private DevExpress.XtraEditors.SimpleButton btDeleteSimple;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.DateEdit deEnd;
        private DevExpress.XtraBars.BarButtonItem btReport;
        private DevExpress.XtraBars.BarSubItem bsiReports;
        private DevExpress.XtraBars.BarButtonItem btReportZone;
        private DevExpress.XtraBars.BarButtonItem btReportStop;
        private DevExpress.XtraBars.BarButtonItem btReportSensor;
        private DevExpress.XtraBars.BarButtonItem btReportFuel;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow DistanceWithLogicSensor;
        private DevExpress.XtraGrid.Columns.GridColumn Dist;
        private DevExpress.XtraBars.BarButtonItem btTamplate;
        private DevExpress.XtraEditors.PanelControl pcGraficMoving;
        private DevExpress.XtraEditors.PanelControl pcDgrmGant;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.TextEdit txOutLinkId;
    }
}