using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using TrackControl.General;
using MySql.Data.MySqlClient;
using Agro.Utilites;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid;
using Route.Dictionaries;
using Route.Properties;
using TrackControl.General.DatabaseDriver;

namespace Route.GridEditForms
{
    public partial class RouteForm : Agro.GridEditForms.FormSample
    {
        /// <summary>
        /// ������������ ��� ���������� �� � �����
        /// </summary>
        DataView _dvClone = null;
        bool bNotCellValueChanged = false;
        public RouteForm()
        {
            InitThis();
        }
        public RouteForm(GridView gvDict, bool bNew)
            : base(gvDict, bNew)
        {
            InitThis();
            di = new DictionaryRoute(_Id);
            this.Text = Resources.RouteSample;
            GetFields();
            bFormLoading = false;
        }
        public override bool ValidateFields()
        {
            if (!DicUtilites.ValidateFieldsTextWFocus(txName, dxErrP) | !DicUtilites.ValidateFieldsTextDoublePositiveWFocus(tePath, dxErrP))

            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public override void UpdateGRN()
        {
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["Id"], txId.Text);
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["Name"], txName.Text);
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["Remark"], meComment.Text);
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["Id_main"], (int)(leGroupe.EditValue ?? 0));
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["Distance"], tePath.Text);
            _gvDict.RefreshData();
            SelectEditedRow();
           
        }

        private void SelectEditedRow()
        {
            int rowHandle = _gvDict.LocateByValue("Id", Convert.ToInt32(txId.Text));
            if (rowHandle != DevExpress.XtraGrid.GridControl.InvalidRowHandle)
            {
                _gvDict.FocusedRowHandle = rowHandle;
                _gvDict.SelectRow(_gvDict.FocusedRowHandle);
                ((MainRoute)_gvDict.GridControl.FindForm()).SetControls();
            }
            //DataTable dt = (DataTable)_gvDict.GridControl.DataSource;
            //int index = dt.Rows.IndexOf(dt.Rows.Find(txId.Text));
            //_gvDict.FocusedRowHandle = _gvDict.GetRowHandle(index);
            //if (index > _gvDict.RowCount) index = _gvDict.RowCount;
            //if (index > 0 && _gvDict.FocusedRowHandle == 0) _gvDict.FocusedRowHandle = _gvDict.GetRowHandle(index) - 1;

        }
        public override void SetFields()
        {
            di.Name = txName.Text;
            di.Comment = meComment.Text;
            var dictionaryRoute = di as DictionaryRoute;
            if (dictionaryRoute != null)
            {
                dictionaryRoute.Distance = Convert.ToDouble(tePath.Text);
                dictionaryRoute.IdOutLink = teOutLinkId.Text;
                dictionaryRoute.Id_main = (int)(leGroupe.EditValue ?? 0);
            }
        }
        public override void GetFields()
        {
            txName.Text = di.Name;
            meComment.Text = di.Comment;
            var dictionaryRoute = di as DictionaryRoute;
            if (dictionaryRoute != null)
            {
                tePath.Text = dictionaryRoute.Distance.ToString();
                teOutLinkId.Text = dictionaryRoute.IdOutLink;
                leGroupe.EditValue = dictionaryRoute.Id_main;
            }
            if (_Id != 0) LoadContent(); 
        }
        public override bool SaveContent()
        {
            try
            {
                SaveRows();
                SelectEditedRow();
                return true;
            }
            catch(Exception ex)
            {
                XtraMessageBox.Show(ex.Message);  
                return false;
            }
        }
        public override bool LoadContent()
        {
            try
            {
                gcSample.DataSource = (di as DictionaryRoute).GetContent(); 
                //using (ConnectMySQL cnMySQL = new ConnectMySQL())
                using( DriverDb db = new DriverDb() )
                { 
                db.ConnectDb();
                
                    string sSQLselect = " SELECT   zonesgroup.Id, zonesgroup.Title FROM   zonesgroup ORDER BY zonesgroup.Title";
                    //GroupeLookUp.DataSource = cnMySQL.GetDataTable(sSQLselect);
                    GroupeLookUp.DataSource = db.GetDataTable( sSQLselect );
                    sSQLselect = " SELECT  zones.Zone_ID as Id, zones.Name, zones.ZonesGroupId FROM   zones ORDER BY zones.Name";
                    //ZRouteLookUp.DataSource = cnMySQL.GetDataTable(sSQLselect);
                    ZRouteLookUp.DataSource = db.GetDataTable( sSQLselect );
                    sSQLselect = " SELECT  rt_events.Id, rt_events.Name FROM   rt_events ORDER BY rt_events.Name";
                    //EventLookUp.DataSource = cnMySQL.GetDataTable(sSQLselect);
                    EventLookUp.DataSource = db.GetDataTable( sSQLselect );
                }
                //SetTimeRelToAbs();
                return true;
            }
            catch
            {
                return false;
            }
        }
        private void InitThis()
        {
            IsContentExist = true;
            InitializeComponent();
            DicUtilites.LookUpLoad(leGroupe, "SELECT   rt_sample.Id, rt_sample.Name"
            + " FROM rt_sample WHERE  rt_sample.IsGroupe = 1  ORDER BY  rt_sample.Name");
            Localization();
        }
        private void leGroupe_EditValueChanged(object sender, EventArgs e)
        {
            ValidateFieldsAction();
        }
        private void tePath_EditValueChanged(object sender, EventArgs e)
        {
            ValidateFieldsAction();
        }
        private void leGroupe_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            leGroupe.EditValue = null;
            leGroupe.Text = "";
        }

        #region ����
        /// <summary>
        /// ��������� ����������� ������� �� ��������� ��������������
        /// </summary>
        private void SetTimeRelToAbs()
        {
            bNotCellValueChanged = true;
            TimeSpan tsPrev = TimeSpan.Zero;
            TimeSpan tsStartAbs = TimeSpan.Zero;
            TimeSpanConverter tsc = new TimeSpanConverter();
            for (int i = 0; i < gvSample.RowCount; i++)
            {
                if (gvSample.GetRowCellValue(i, gvSample.Columns["TimeRel"]) != null)
                {
                    string sTime = gvSample.GetRowCellValue(i, gvSample.Columns["TimeRel"]).ToString();
                    TimeSpan tsWork = (TimeSpan)tsc.ConvertFromInvariantString(sTime);
                    if (i == 0)
                    {
                        string sTimeAbs = gvSample.GetRowCellValue(i, gvSample.Columns["TimeAbs"]).ToString();
                        tsStartAbs = (TimeSpan)tsc.ConvertFromInvariantString(sTimeAbs);
                    }
                    else
                    {
                        tsWork = tsWork.Add(tsPrev);
                        gvSample.SetRowCellValue(i, gvSample.Columns["TimeAbs"], tsWork.Add(tsStartAbs).ToString().Substring(0, tsWork.ToString().Length - 3));
                        tsPrev = tsWork;
                    }
                    
                }
            }
            bNotCellValueChanged = false;
        }
        /// <summary>
        /// ��������� �������������� ������� �� ��������� ����������� 
        /// </summary>
        private void SetTimeAbsToRel()
        {
            bNotCellValueChanged = true;
            TimeSpan tsPrev = TimeSpan.MinValue;
            TimeSpanConverter tsc = new TimeSpanConverter();
            for (int i = 0; i < gvSample.RowCount; i++)
            {
                if (gvSample.GetRowCellValue(i, gvSample.Columns["TimeAbs"]) != null)
                {
                    string sTime = gvSample.GetRowCellValue(i, gvSample.Columns["TimeAbs"]).ToString();
                    TimeSpan tsWork = (TimeSpan)tsc.ConvertFromInvariantString(sTime);
                    if (i == 0)
                    {
                        //gvSample.SetRowCellValue(i, gvSample.Columns["TimeRel"], "00:00");
                    }
                    else
                    {
                        gvSample.SetRowCellValue(i, gvSample.Columns["TimeRel"], tsWork.Subtract(tsPrev).ToString().Substring(0, tsWork.ToString().Length - 3));
                    }
                    tsPrev = tsWork;
                }
            }
            bNotCellValueChanged = false;
        }
        private void gvSample_InitNewRow(object sender, InitNewRowEventArgs e)
        {
            DataRow row = gvSample.GetDataRow(e.RowHandle);
            if (gvSample.RowCount > 1)
            {
                int iWork;
                if (((int)gvSample.GetRowCellValue(gvSample.RowCount - 2, "Id_event") == (int)Consts.RouteEvent.Entry))
                {

                    row["Id_event"] = (int)Consts.RouteEvent.Exit;
                    if (Int32.TryParse(gvSample.GetRowCellValue(gvSample.RowCount - 2, "Id_zone_grp").ToString(), out  iWork))
                        row["Id_zone_grp"] = iWork;
                    if (Int32.TryParse(gvSample.GetRowCellValue(gvSample.RowCount - 2, "Zone_ID").ToString(), out  iWork))
                        row["Zone_ID"] = iWork;
                }
                else
                {
                    row["Id_event"] = (int)Consts.RouteEvent.Entry;
                }
                if (Int32.TryParse(gvSample.GetRowCellValue(gvSample.RowCount - 2, "Position").ToString(), out  iWork))
                    row["Position"] = iWork + 1;
                row["TimeAbs"] = gvSample.GetRowCellValue(gvSample.RowCount - 2, "TimeAbs").ToString();
                row["Id"] = 0;
            }
            else
            {
                row["Id_event"] = (int)Consts.RouteEvent.Entry;
                row["Position"] = 1;
                row["TimeAbs"] = "00:00";
            }
            row["TimeRel"] = "00:00";
            row["TimeRelP"] = "00:00";
            row["TimeRelM"] = "00:00";
            row["Zone_Id"] = row["Id_zone_grp"];
            row["Id"] = 0;
            //SaveRow(e.RowHandle);
            //gvSample.RefreshData();
        }
        /// <summary>
        /// ��� ������� ������ - ������ ��� + ����
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void gvSample_ShownEditor(object sender, EventArgs e)
        {
            if (gvSample.FocusedColumn.FieldName == "Zone_ID" && gvSample.ActiveEditor is LookUpEdit)
            {
                DataRow row = gvSample.GetDataRow(gvSample.FocusedRowHandle);
                int iGroupe = 0;
                if (!Int32.TryParse(row["Id_zone_grp"].ToString(), out iGroupe)) return;
                var edit = (LookUpEdit)gvSample.ActiveEditor;
                DataTable table = edit.Properties.DataSource as DataTable;
                _dvClone = new DataView(table);

                _dvClone.RowFilter = "[ZonesGroupId] = " + iGroupe.ToString();
                edit.Properties.DataSource = _dvClone;
            }
        }
        private void gvSample_HiddenEditor(object sender, EventArgs e)
        {
            if (_dvClone != null)
            {
                _dvClone.Dispose();
                _dvClone = null;
            }
        }
        private void gvSample_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (bNotCellValueChanged || !ValidateCHZ()) return;

            if (e.Column.FieldName.ToString() == "TimeRel")
            {
                TimeSpan tsCur;
                if (!(TimeSpan.TryParse(gvSample.GetRowCellValue(e.RowHandle, gvSample.Columns["TimeRel"]).ToString(), out tsCur))) return;
                if (e.RowHandle < 0 && gvSample.RowCount > 1)
                {
                    bNotCellValueChanged = true;
                    TimeSpanConverter tsc = new TimeSpanConverter();
                    string sTime = gvSample.GetRowCellValue(gvSample.RowCount - 2, gvSample.Columns["TimeAbs"]).ToString();
                    TimeSpan tsPrevAbs = (TimeSpan)tsc.ConvertFromInvariantString(sTime);
                    tsCur = tsPrevAbs.Add(tsCur);
                    gvSample.SetRowCellValue(e.RowHandle, gvSample.Columns["TimeAbs"], tsCur.ToString().Substring(0, tsCur.ToString().Length - 3));
                    bNotCellValueChanged = false;
                }
                SetTimeRelToAbs();
            }
            else if (e.Column.FieldName.ToString() == "TimeAbs")
            {
                TimeSpan tsCur;
                if (!(TimeSpan.TryParse(gvSample.GetRowCellValue(e.RowHandle, gvSample.Columns["TimeAbs"]).ToString(), out tsCur))) return;
                if (e.RowHandle < 0 && gvSample.RowCount > 1 )
                {
                    bNotCellValueChanged = true;
                    TimeSpanConverter tsc = new TimeSpanConverter();
                    string sTime = gvSample.GetRowCellValue(gvSample.RowCount - 2, gvSample.Columns["TimeAbs"]).ToString();
                    TimeSpan tsPrev = (TimeSpan)tsc.ConvertFromInvariantString(sTime);
                    tsCur = tsCur.Subtract(tsPrev);
                    gvSample.SetRowCellValue(e.RowHandle, gvSample.Columns["TimeRel"], tsCur.ToString().Substring(0, tsCur.ToString().Length - 3));
                    bNotCellValueChanged = false;
                }
                else
                {
                    SetTimeAbsToRel();
                }
            }
            bContentChange = true;
            ValidateFieldsAction();
        }
        private void gvSample_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {
            TimeSpan ts;
            TimeSpan ts_abs;
            if (!((TimeSpan.TryParse(gvSample.GetRowCellValue(gvSample.FocusedRowHandle, gvSample.Columns["TimeRel"]).ToString(), out ts))
                && (TimeSpan.TryParse(gvSample.GetRowCellValue(gvSample.FocusedRowHandle, gvSample.Columns["TimeAbs"]).ToString(), out ts_abs))))
            {
                e.Valid = false;
                gvSample.SetColumnError(gvSample.Columns["TimeRel"],Resources.DataNeedInterval + " 00:00 - 24:00");
                gvSample.SetColumnError(gvSample.Columns["TimeAbs"], Resources.DataNeedInterval + " 00:00 - 24:00");
                return;
            }
            // ���������� ����� ������ ���� �� �����������
            if (e.RowHandle > 0)
            {
                if (((TimeSpan.TryParse(gvSample.GetRowCellValue(gvSample.FocusedRowHandle - 1, gvSample.Columns["TimeAbs"]).ToString(), out ts))
                    && (TimeSpan.TryParse(gvSample.GetRowCellValue(gvSample.FocusedRowHandle, gvSample.Columns["TimeAbs"]).ToString(), out ts_abs))))
                {
                    if (ts_abs.Subtract(ts).TotalMinutes < 0)
                    {
                        e.Valid = false;
                        gvSample.SetColumnError(gvSample.Columns["TimeAbs"], Resources.TimeNeedGrouth );
                    }
                }
            }
        }
        private void gvSample_InvalidRowException(object sender, DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs e)
        {
            e.ExceptionMode = DevExpress.XtraEditors.Controls.ExceptionMode.DisplayError;
        }
        private void SaveRows()
        {
            for (int i = 0; i < gvSample.RowCount; i++)
            {
                SaveRow(i);
            }
        }
        private void SaveRow(int Row)
        {
            int IdRecord = 0;
            int iZone = 0;
            int iPos = 0;
            int iEvent = 0;
            TimeSpan ts;
            if (!Int32.TryParse(gvSample.GetRowCellValue(Row, gvSample.Columns["Zone_ID"]).ToString(), out iZone)) return;
            if (!Int32.TryParse(gvSample.GetRowCellValue(Row, gvSample.Columns["Position"]).ToString(), out iPos)) return;
            if (!Int32.TryParse(gvSample.GetRowCellValue(Row, gvSample.Columns["Id_event"]).ToString(), out iEvent)) return;
            if (!TimeSpan.TryParse(gvSample.GetRowCellValue(Row, gvSample.Columns["TimeRel"]).ToString(), out ts)) return;
            if (Int32.TryParse(gvSample.GetRowCellValue(Row, gvSample.Columns["Id"]).ToString(), out IdRecord) && (di as DictionaryRoute).IsContentExis(IdRecord))
            {
                (di as DictionaryRoute).EditContent(IdRecord, iPos, iZone, iEvent,
                    gvSample.GetRowCellValue(Row, gvSample.Columns["TimeRel"]).ToString(),
                    gvSample.GetRowCellValue(Row, gvSample.Columns["TimeAbs"]).ToString(),
                    gvSample.GetRowCellValue(Row, gvSample.Columns["TimeRelP"]).ToString(),
                    gvSample.GetRowCellValue(Row, gvSample.Columns["TimeRelM"]).ToString());
            }
            else
            {
                gvSample.SetRowCellValue(Row, gvSample.Columns["Id"], (di as DictionaryRoute).AddContent(iPos, iZone, iEvent,
                    gvSample.GetRowCellValue(Row, gvSample.Columns["TimeRel"]).ToString(),
                    gvSample.GetRowCellValue(Row, gvSample.Columns["TimeAbs"]).ToString(),
                    gvSample.GetRowCellValue(Row, gvSample.Columns["TimeRelP"]).ToString(),
                    gvSample.GetRowCellValue(Row, gvSample.Columns["TimeRelM"]).ToString()));
            }
        }
        private void gcSample_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            if (e.Button.ButtonType == DevExpress.XtraEditors.NavigatorButtonType.Remove)
            {
                int IdRecord;
                if (Int32.TryParse(gvSample.GetRowCellValue(gvSample.FocusedRowHandle, gvSample.Columns["Id"]).ToString(), out IdRecord))
                {
                    if (DialogResult.Yes == XtraMessageBox.Show(Resources.DataDeleteConfirm , Resources.Routers1, MessageBoxButtons.YesNoCancel , MessageBoxIcon.Question ,MessageBoxDefaultButton.Button2 ))
                    {
                        (di as DictionaryRoute).DeleteContent(IdRecord); 
                        e.Handled = false;
                    }
                    else
                        e.Handled = true;

                }
            }
        }
        private bool ValidateCHZ()
        {
            string ErrMess = Resources.CheckZonePoint;
            int Zone_ID = 0;
            bool Valid = true;
            if (!Int32.TryParse(gvSample.GetRowCellValue(gvSample.FocusedRowHandle, gvSample.Columns["Zone_ID"]).ToString(), out Zone_ID))
            {
                Valid = false;
            }
            else if (Zone_ID == 0) Valid = false;
            if (!Valid)
            {
                gvSample.SetColumnError(gvSample.Columns["Zone_ID"], ErrMess);
                XtraMessageBox.Show(ErrMess, Resources.Routers1, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return Valid;
        }
        #endregion

        private void Localization()
        {
            labelControl1.Text = Resources.Groupe;
            Id_zone_grp.Caption = Resources.Groupe;
            GroupeLookUp.Columns[0].Caption = Resources.CheckZoneGroupe;
            Zone_ID.Caption = Resources.CheckZone;
            ZRouteLookUp.Columns[0].Caption = Resources.NameRes;
            Event.Caption = Resources.Event;
            EventLookUp.Columns[0].Caption = Resources.Event;
            TimeRel.Caption = Resources.EventTimeRelative;
            TimeAbs.Caption = Resources.EventTimeAbsolute;
            TimeRelP.Caption =  Resources.Deviation + " (+)";
            TimeRelM.Caption = Resources.Deviation + " (-)";
            //DriverLookUp.NullText = Resources.DriverPoint;
            //DriverLookUp.Columns[0].Caption = Resources.Driver;
            //ZoneLookUp.Columns[0].Caption = Resources.NameRes;
            labelControl4.Text = Resources.km;
            labelControl5.Text = Resources.DistanceOne;
            labelControl6.Text = Resources.BaseCode;
        }

        private void teOutLinkId_EditValueChanged(object sender, EventArgs e)
        {
            ValidateFieldsAction();
        }
    }
}

