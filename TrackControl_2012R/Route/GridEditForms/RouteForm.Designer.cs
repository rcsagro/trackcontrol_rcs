namespace Route.GridEditForms
{
    partial class RouteForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.leGroupe = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.gcSample = new DevExpress.XtraGrid.GridControl();
            this.gvSample = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.Id = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Position = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Id_zone_grp = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GroupeLookUp = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.Zone_ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ZRouteLookUp = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.Event = new DevExpress.XtraGrid.Columns.GridColumn();
            this.EventLookUp = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.TimeRel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.teTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.TimeAbs = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TimeRelP = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TimeRelM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.tePath = new DevExpress.XtraEditors.TextEdit();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.teOutLinkId = new DevExpress.XtraEditors.TextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.meComment.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txId.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leGroupe.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcSample)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvSample)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GroupeLookUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ZRouteLookUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EventLookUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tePath.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teOutLinkId.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // meComment
            // 
            // 
            // txId
            // 
            this.txId.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txId.Properties.Appearance.Options.UseFont = true;
            this.txId.Properties.Appearance.Options.UseTextOptions = true;
            this.txId.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            // 
            // txName
            // 
            this.txName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txName.Properties.Appearance.Options.UseFont = true;
            // 
            // lbName
            // 
            this.lbName.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            // 
            // btCancel
            // 
            this.btCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btCancel.Location = new System.Drawing.Point(707, 11);
            // 
            // btSave
            // 
            this.btSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btSave.Location = new System.Drawing.Point(616, 11);
            // 
            // btDelete
            // 
            this.btDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btDelete.Location = new System.Drawing.Point(525, 11);
            // 
            // leGroupe
            // 
            this.leGroupe.Location = new System.Drawing.Point(72, 12);
            this.leGroupe.Name = "leGroupe";
            this.leGroupe.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.leGroupe.Properties.Appearance.Options.UseFont = true;
            this.leGroupe.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leGroupe.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.leGroupe.Properties.DisplayMember = "Name";
            this.leGroupe.Properties.NullText = "";
            this.leGroupe.Properties.ValueMember = "id";
            this.leGroupe.Size = new System.Drawing.Size(542, 20);
            this.leGroupe.TabIndex = 19;
            this.leGroupe.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.leGroupe_ButtonClick);
            this.leGroupe.EditValueChanged += new System.EventHandler(this.leGroupe_EditValueChanged);
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.labelControl1.Location = new System.Drawing.Point(19, 15);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(43, 14);
            this.labelControl1.TabIndex = 18;
            this.labelControl1.Text = "������ ";
            // 
            // gcSample
            // 
            this.gcSample.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gcSample.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gcSample_EmbeddedNavigator_ButtonClick);
            this.gcSample.Location = new System.Drawing.Point(12, 171);
            this.gcSample.MainView = this.gvSample;
            this.gcSample.Name = "gcSample";
            this.gcSample.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.EventLookUp,
            this.GroupeLookUp,
            this.ZRouteLookUp,
            this.teTime});
            this.gcSample.Size = new System.Drawing.Size(780, 273);
            this.gcSample.TabIndex = 42;
            this.gcSample.UseEmbeddedNavigator = true;
            this.gcSample.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvSample});
            // 
            // gvSample
            // 
            this.gvSample.ColumnPanelRowHeight = 40;
            this.gvSample.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.Id,
            this.Position,
            this.Id_zone_grp,
            this.Zone_ID,
            this.Event,
            this.TimeRel,
            this.TimeAbs,
            this.TimeRelP,
            this.TimeRelM});
            this.gvSample.GridControl = this.gcSample;
            this.gvSample.Name = "gvSample";
            this.gvSample.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CellSelect;
            this.gvSample.OptionsView.ShowGroupPanel = false;
            this.gvSample.ScrollStyle = DevExpress.XtraGrid.Views.Grid.ScrollStyleFlags.None;
            this.gvSample.InitNewRow += new DevExpress.XtraGrid.Views.Grid.InitNewRowEventHandler(this.gvSample_InitNewRow);
            this.gvSample.HiddenEditor += new System.EventHandler(this.gvSample_HiddenEditor);
            this.gvSample.ShownEditor += new System.EventHandler(this.gvSample_ShownEditor);
            this.gvSample.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gvSample_CellValueChanged);
            this.gvSample.InvalidRowException += new DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventHandler(this.gvSample_InvalidRowException);
            this.gvSample.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.gvSample_ValidateRow);
            // 
            // Id
            // 
            this.Id.AppearanceHeader.Options.UseTextOptions = true;
            this.Id.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Id.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Id.Caption = "Idt";
            this.Id.CustomizationCaption = "Id";
            this.Id.FieldName = "Id";
            this.Id.Name = "Id";
            this.Id.OptionsColumn.AllowEdit = false;
            // 
            // Position
            // 
            this.Position.AppearanceCell.Options.UseTextOptions = true;
            this.Position.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Position.AppearanceHeader.Options.UseTextOptions = true;
            this.Position.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Position.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Position.Caption = "#";
            this.Position.FieldName = "Position";
            this.Position.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.Position.Name = "Position";
            this.Position.Visible = true;
            this.Position.VisibleIndex = 0;
            this.Position.Width = 44;
            // 
            // Id_zone_grp
            // 
            this.Id_zone_grp.AppearanceCell.Options.UseTextOptions = true;
            this.Id_zone_grp.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.Id_zone_grp.AppearanceHeader.Options.UseTextOptions = true;
            this.Id_zone_grp.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Id_zone_grp.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Id_zone_grp.Caption = "������";
            this.Id_zone_grp.ColumnEdit = this.GroupeLookUp;
            this.Id_zone_grp.FieldName = "Id_zone_grp";
            this.Id_zone_grp.Name = "Id_zone_grp";
            this.Id_zone_grp.Visible = true;
            this.Id_zone_grp.VisibleIndex = 1;
            this.Id_zone_grp.Width = 124;
            // 
            // GroupeLookUp
            // 
            this.GroupeLookUp.AutoHeight = false;
            this.GroupeLookUp.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.GroupeLookUp.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Title", "������ ����������� ���")});
            this.GroupeLookUp.DisplayMember = "Title";
            this.GroupeLookUp.Name = "GroupeLookUp";
            this.GroupeLookUp.ValueMember = "Id";
            // 
            // Zone_ID
            // 
            this.Zone_ID.AppearanceCell.Options.UseTextOptions = true;
            this.Zone_ID.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.Zone_ID.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Zone_ID.AppearanceHeader.Options.UseTextOptions = true;
            this.Zone_ID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Zone_ID.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Zone_ID.Caption = "����������� ����";
            this.Zone_ID.ColumnEdit = this.ZRouteLookUp;
            this.Zone_ID.FieldName = "Zone_ID";
            this.Zone_ID.Name = "Zone_ID";
            this.Zone_ID.Visible = true;
            this.Zone_ID.VisibleIndex = 2;
            this.Zone_ID.Width = 243;
            // 
            // ZRouteLookUp
            // 
            this.ZRouteLookUp.AutoHeight = false;
            this.ZRouteLookUp.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ZRouteLookUp.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "��������", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.ZRouteLookUp.DisplayMember = "Name";
            this.ZRouteLookUp.Name = "ZRouteLookUp";
            this.ZRouteLookUp.ValueMember = "Id";
            // 
            // Event
            // 
            this.Event.AppearanceCell.Options.UseTextOptions = true;
            this.Event.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Event.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Event.AppearanceHeader.Options.UseTextOptions = true;
            this.Event.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Event.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Event.Caption = "�������";
            this.Event.ColumnEdit = this.EventLookUp;
            this.Event.FieldName = "Id_event";
            this.Event.Name = "Event";
            this.Event.Visible = true;
            this.Event.VisibleIndex = 3;
            this.Event.Width = 125;
            // 
            // EventLookUp
            // 
            this.EventLookUp.AutoHeight = false;
            this.EventLookUp.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.EventLookUp.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "�������")});
            this.EventLookUp.DisplayMember = "Name";
            this.EventLookUp.Name = "EventLookUp";
            this.EventLookUp.ValueMember = "Id";
            // 
            // TimeRel
            // 
            this.TimeRel.AppearanceCell.Options.UseTextOptions = true;
            this.TimeRel.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeRel.AppearanceHeader.Options.UseTextOptions = true;
            this.TimeRel.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeRel.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TimeRel.Caption = "����� ������� �������������";
            this.TimeRel.ColumnEdit = this.teTime;
            this.TimeRel.FieldName = "TimeRel";
            this.TimeRel.Name = "TimeRel";
            this.TimeRel.Visible = true;
            this.TimeRel.VisibleIndex = 4;
            this.TimeRel.Width = 172;
            // 
            // teTime
            // 
            this.teTime.AutoHeight = false;
            this.teTime.EditFormat.FormatString = "t";
            this.teTime.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.teTime.Mask.EditMask = "90:00";
            this.teTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple;
            this.teTime.Name = "teTime";
            // 
            // TimeAbs
            // 
            this.TimeAbs.AppearanceCell.Options.UseTextOptions = true;
            this.TimeAbs.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeAbs.AppearanceHeader.Options.UseTextOptions = true;
            this.TimeAbs.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeAbs.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TimeAbs.Caption = "����� ������� ����������";
            this.TimeAbs.ColumnEdit = this.teTime;
            this.TimeAbs.FieldName = "TimeAbs";
            this.TimeAbs.Name = "TimeAbs";
            this.TimeAbs.Visible = true;
            this.TimeAbs.VisibleIndex = 5;
            this.TimeAbs.Width = 151;
            // 
            // TimeRelP
            // 
            this.TimeRelP.AppearanceCell.Options.UseTextOptions = true;
            this.TimeRelP.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeRelP.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TimeRelP.AppearanceHeader.Options.UseTextOptions = true;
            this.TimeRelP.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeRelP.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TimeRelP.Caption = "���������� (+)";
            this.TimeRelP.ColumnEdit = this.teTime;
            this.TimeRelP.FieldName = "TimeRelP";
            this.TimeRelP.Name = "TimeRelP";
            this.TimeRelP.Visible = true;
            this.TimeRelP.VisibleIndex = 6;
            this.TimeRelP.Width = 113;
            // 
            // TimeRelM
            // 
            this.TimeRelM.AppearanceCell.Options.UseTextOptions = true;
            this.TimeRelM.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeRelM.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TimeRelM.AppearanceHeader.Options.UseTextOptions = true;
            this.TimeRelM.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeRelM.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TimeRelM.Caption = "���������� (-)";
            this.TimeRelM.ColumnEdit = this.teTime;
            this.TimeRelM.FieldName = "TimeRelM";
            this.TimeRelM.Name = "TimeRelM";
            this.TimeRelM.Visible = true;
            this.TimeRelM.VisibleIndex = 7;
            this.TimeRelM.Width = 118;
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.labelControl4.Location = new System.Drawing.Point(753, 51);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(14, 14);
            this.labelControl4.TabIndex = 45;
            this.labelControl4.Text = "��";
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.labelControl5.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.labelControl5.Location = new System.Drawing.Point(632, 51);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(26, 14);
            this.labelControl5.TabIndex = 44;
            this.labelControl5.Text = "����";
            // 
            // tePath
            // 
            this.tePath.EditValue = "0";
            this.tePath.Location = new System.Drawing.Point(687, 48);
            this.tePath.Name = "tePath";
            this.tePath.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.tePath.Properties.Appearance.Options.UseFont = true;
            this.tePath.Properties.Appearance.Options.UseTextOptions = true;
            this.tePath.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.tePath.Properties.NullText = "0";
            this.tePath.Size = new System.Drawing.Size(60, 20);
            this.tePath.TabIndex = 43;
            this.tePath.EditValueChanged += new System.EventHandler(this.tePath_EditValueChanged);
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.labelControl6.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.labelControl6.Location = new System.Drawing.Point(632, 77);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(51, 14);
            this.labelControl6.TabIndex = 47;
            this.labelControl6.Text = "��� ����";
            // 
            // teOutLinkId
            // 
            this.teOutLinkId.EditValue = "";
            this.teOutLinkId.Location = new System.Drawing.Point(687, 74);
            this.teOutLinkId.Name = "teOutLinkId";
            this.teOutLinkId.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.teOutLinkId.Properties.Appearance.Options.UseFont = true;
            this.teOutLinkId.Properties.Appearance.Options.UseTextOptions = true;
            this.teOutLinkId.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.teOutLinkId.Properties.NullText = "0";
            this.teOutLinkId.Size = new System.Drawing.Size(60, 20);
            this.teOutLinkId.TabIndex = 46;
            this.teOutLinkId.EditValueChanged += new System.EventHandler(this.teOutLinkId_EditValueChanged);
            // 
            // RouteForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(804, 500);
            this.Controls.Add(this.labelControl6);
            this.Controls.Add(this.teOutLinkId);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.tePath);
            this.Controls.Add(this.gcSample);
            this.Controls.Add(this.leGroupe);
            this.Controls.Add(this.labelControl1);
            this.Name = "RouteForm";
            this.Controls.SetChildIndex(this.labelControl1, 0);
            this.Controls.SetChildIndex(this.leGroupe, 0);
            this.Controls.SetChildIndex(this.gcSample, 0);
            this.Controls.SetChildIndex(this.tePath, 0);
            this.Controls.SetChildIndex(this.labelControl5, 0);
            this.Controls.SetChildIndex(this.labelControl4, 0);
            this.Controls.SetChildIndex(this.teOutLinkId, 0);
            this.Controls.SetChildIndex(this.labelControl6, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dxErrP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.meComment.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txId.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leGroupe.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcSample)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvSample)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GroupeLookUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ZRouteLookUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EventLookUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tePath.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teOutLinkId.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public DevExpress.XtraEditors.LookUpEdit leGroupe;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraGrid.GridControl gcSample;
        private DevExpress.XtraGrid.Views.Grid.GridView gvSample;
        private DevExpress.XtraGrid.Columns.GridColumn Id;
        private DevExpress.XtraGrid.Columns.GridColumn Position;
        private DevExpress.XtraGrid.Columns.GridColumn Id_zone_grp;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit GroupeLookUp;
        private DevExpress.XtraGrid.Columns.GridColumn Zone_ID;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit ZRouteLookUp;
        private DevExpress.XtraGrid.Columns.GridColumn Event;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit EventLookUp;
        private DevExpress.XtraGrid.Columns.GridColumn TimeRel;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit teTime;
        private DevExpress.XtraGrid.Columns.GridColumn TimeAbs;
        private DevExpress.XtraGrid.Columns.GridColumn TimeRelP;
        private DevExpress.XtraGrid.Columns.GridColumn TimeRelM;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.TextEdit tePath;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.TextEdit teOutLinkId;
    }
}
