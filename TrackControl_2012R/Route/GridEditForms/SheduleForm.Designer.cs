﻿namespace Route.GridEditForms
{
    partial class SheduleForm
    {
        /// <summary>
        ///Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.gcShedule = new DevExpress.XtraGrid.GridControl();
            this.gvShedule = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colZoneToGroup = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rcbZonesToGroups = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.colZone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rcbZonesTo = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.colDistance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimePlanStart = new DevExpress.XtraGrid.Columns.GridColumn();
            this.teTime = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colTimePlanEnd = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rcbVehiclesGroups = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.rcbVehicles = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.lbZone = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.cbZoneGroup = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbZone = new DevExpress.XtraEditors.ComboBoxEdit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.meComment.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txId.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcShedule)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvShedule)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rcbZonesToGroups)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rcbZonesTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rcbVehiclesGroups)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rcbVehicles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbZoneGroup.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbZone.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // meComment
            // 
            // 
            // txId
            // 
            this.txId.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txId.Properties.Appearance.Options.UseFont = true;
            this.txId.Properties.Appearance.Options.UseTextOptions = true;
            this.txId.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            // 
            // txName
            // 
            this.txName.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txName.Properties.Appearance.Options.UseFont = true;
            // 
            // lbName
            // 
            this.lbName.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            // 
            // btCancel
            // 
            this.btCancel.Location = new System.Drawing.Point(3495, 11);
            // 
            // btSave
            // 
            this.btSave.Location = new System.Drawing.Point(3404, 11);
            // 
            // btDelete
            // 
            this.btDelete.Location = new System.Drawing.Point(3313, 11);
            // 
            // gcShedule
            // 
            this.gcShedule.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gcShedule_EmbeddedNavigator_ButtonClick);
            this.gcShedule.Location = new System.Drawing.Point(12, 171);
            this.gcShedule.MainView = this.gvShedule;
            this.gcShedule.Name = "gcShedule";
            this.gcShedule.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.rcbZonesToGroups,
            this.rcbVehiclesGroups,
            this.rcbZonesTo,
            this.rcbVehicles,
            this.teTime});
            this.gcShedule.Size = new System.Drawing.Size(945, 328);
            this.gcShedule.TabIndex = 9;
            this.gcShedule.UseEmbeddedNavigator = true;
            this.gcShedule.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvShedule});
            // 
            // gvShedule
            // 
            this.gvShedule.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colId,
            this.colZoneToGroup,
            this.colZone,
            this.colDistance,
            this.colTimePlanStart,
            this.colTimePlanEnd});
            this.gvShedule.GridControl = this.gcShedule;
            this.gvShedule.Name = "gvShedule";
            this.gvShedule.OptionsView.ShowGroupPanel = false;
            this.gvShedule.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gvShedule_CustomRowCellEdit);
            this.gvShedule.InitNewRow += new DevExpress.XtraGrid.Views.Grid.InitNewRowEventHandler(this.gvShedule_InitNewRow);
            this.gvShedule.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gvShedule_CellValueChanged);
            this.gvShedule.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.gvShedule_ValidateRow);
            // 
            // colId
            // 
            this.colId.AppearanceHeader.Options.UseTextOptions = true;
            this.colId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colId.Caption = "Id";
            this.colId.Name = "colId";
            // 
            // colZoneToGroup
            // 
            this.colZoneToGroup.AppearanceHeader.Options.UseTextOptions = true;
            this.colZoneToGroup.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colZoneToGroup.Caption = "Группа";
            this.colZoneToGroup.ColumnEdit = this.rcbZonesToGroups;
            this.colZoneToGroup.FieldName = "ZoneToGroup";
            this.colZoneToGroup.Name = "colZoneToGroup";
            this.colZoneToGroup.Visible = true;
            this.colZoneToGroup.VisibleIndex = 0;
            this.colZoneToGroup.Width = 178;
            // 
            // rcbZonesToGroups
            // 
            this.rcbZonesToGroups.AutoHeight = false;
            this.rcbZonesToGroups.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rcbZonesToGroups.Name = "rcbZonesToGroups";
            // 
            // colZone
            // 
            this.colZone.AppearanceHeader.Options.UseTextOptions = true;
            this.colZone.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colZone.Caption = "Зона";
            this.colZone.ColumnEdit = this.rcbZonesTo;
            this.colZone.FieldName = "ZoneTo";
            this.colZone.Name = "colZone";
            this.colZone.Visible = true;
            this.colZone.VisibleIndex = 1;
            this.colZone.Width = 253;
            // 
            // rcbZonesTo
            // 
            this.rcbZonesTo.AutoHeight = false;
            this.rcbZonesTo.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rcbZonesTo.Name = "rcbZonesTo";
            // 
            // colDistance
            // 
            this.colDistance.AppearanceHeader.Options.UseTextOptions = true;
            this.colDistance.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDistance.Caption = "Путь, км";
            this.colDistance.FieldName = "Distance";
            this.colDistance.Name = "colDistance";
            this.colDistance.Visible = true;
            this.colDistance.VisibleIndex = 2;
            this.colDistance.Width = 103;
            // 
            // colTimePlanStart
            // 
            this.colTimePlanStart.AppearanceCell.Options.UseTextOptions = true;
            this.colTimePlanStart.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimePlanStart.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimePlanStart.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimePlanStart.Caption = "План старт";
            this.colTimePlanStart.ColumnEdit = this.teTime;
            this.colTimePlanStart.FieldName = "TimePlanStart";
            this.colTimePlanStart.Name = "colTimePlanStart";
            this.colTimePlanStart.Visible = true;
            this.colTimePlanStart.VisibleIndex = 3;
            this.colTimePlanStart.Width = 117;
            // 
            // teTime
            // 
            this.teTime.AutoHeight = false;
            this.teTime.EditFormat.FormatString = "t";
            this.teTime.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.teTime.Mask.EditMask = "90:00";
            this.teTime.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple;
            this.teTime.Name = "teTime";
            // 
            // colTimePlanEnd
            // 
            this.colTimePlanEnd.AppearanceCell.Options.UseTextOptions = true;
            this.colTimePlanEnd.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimePlanEnd.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimePlanEnd.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimePlanEnd.Caption = "План финиш";
            this.colTimePlanEnd.ColumnEdit = this.teTime;
            this.colTimePlanEnd.FieldName = "TimePlanEnd";
            this.colTimePlanEnd.Name = "colTimePlanEnd";
            this.colTimePlanEnd.Visible = true;
            this.colTimePlanEnd.VisibleIndex = 4;
            this.colTimePlanEnd.Width = 152;
            // 
            // rcbVehiclesGroups
            // 
            this.rcbVehiclesGroups.AutoHeight = false;
            this.rcbVehiclesGroups.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rcbVehiclesGroups.Name = "rcbVehiclesGroups";
            // 
            // rcbVehicles
            // 
            this.rcbVehicles.AutoHeight = false;
            this.rcbVehicles.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rcbVehicles.Name = "rcbVehicles";
            // 
            // lbZone
            // 
            this.lbZone.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.lbZone.Location = new System.Drawing.Point(635, 114);
            this.lbZone.Name = "lbZone";
            this.lbZone.Size = new System.Drawing.Size(103, 14);
            this.lbZone.TabIndex = 18;
            this.lbZone.Text = "Контрольная зона";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.labelControl1.Location = new System.Drawing.Point(635, 46);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(140, 14);
            this.labelControl1.TabIndex = 20;
            this.labelControl1.Text = "Группа контрольных зон";
            // 
            // cbZoneGroup
            // 
            this.cbZoneGroup.Location = new System.Drawing.Point(635, 74);
            this.cbZoneGroup.Name = "cbZoneGroup";
            this.cbZoneGroup.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbZoneGroup.Size = new System.Drawing.Size(322, 20);
            this.cbZoneGroup.TabIndex = 21;
            this.cbZoneGroup.SelectedValueChanged += new System.EventHandler(this.cbZoneGroup_SelectedValueChanged);
            // 
            // cbZone
            // 
            this.cbZone.Location = new System.Drawing.Point(635, 134);
            this.cbZone.Name = "cbZone";
            this.cbZone.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbZone.Size = new System.Drawing.Size(322, 20);
            this.cbZone.TabIndex = 22;
            this.cbZone.SelectedValueChanged += new System.EventHandler(this.cbZone_SelectedValueChanged);
            // 
            // SheduleForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(960, 555);
            this.Controls.Add(this.cbZone);
            this.Controls.Add(this.cbZoneGroup);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.lbZone);
            this.Controls.Add(this.gcShedule);
            this.Name = "SheduleForm";
            this.Text = "RCS";
            this.Controls.SetChildIndex(this.gcShedule, 0);
            this.Controls.SetChildIndex(this.lbZone, 0);
            this.Controls.SetChildIndex(this.labelControl1, 0);
            this.Controls.SetChildIndex(this.cbZoneGroup, 0);
            this.Controls.SetChildIndex(this.cbZone, 0);
            ((System.ComponentModel.ISupportInitialize)(this.dxErrP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.meComment.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txId.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcShedule)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvShedule)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rcbZonesToGroups)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rcbZonesTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rcbVehiclesGroups)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rcbVehicles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbZoneGroup.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbZone.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gcShedule;
        private DevExpress.XtraGrid.Views.Grid.GridView gvShedule;
        private DevExpress.XtraGrid.Columns.GridColumn colId;
        private DevExpress.XtraGrid.Columns.GridColumn colZoneToGroup;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox rcbZonesToGroups;
        private DevExpress.XtraGrid.Columns.GridColumn colZone;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox rcbZonesTo;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox rcbVehiclesGroups;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox rcbVehicles;
        private DevExpress.XtraGrid.Columns.GridColumn colDistance;
        private DevExpress.XtraGrid.Columns.GridColumn colTimePlanStart;
        private DevExpress.XtraGrid.Columns.GridColumn colTimePlanEnd;
        private DevExpress.XtraEditors.LabelControl lbZone;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.ComboBoxEdit cbZoneGroup;
        private DevExpress.XtraEditors.ComboBoxEdit cbZone;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit teTime;
    }
}
