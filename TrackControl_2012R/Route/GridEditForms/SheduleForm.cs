﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Agro;
using Agro.Utilites;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid;
using Route.Dictionaries;
using Route.Properties;
using TrackControl.General;
using TrackControl.Vehicles;

namespace Route.GridEditForms
{
    public partial class SheduleForm : Agro.GridEditForms.FormSample
    {
        public SheduleForm()
        {
            InitThis();
        }
        public SheduleForm(GridView gvDict, bool bNew)
            : base(gvDict, bNew)
        {
            di = new DictionaryShedule(_Id);
            InitThis();
       }

        public override bool ValidateFields()
        {
            if (!DicUtilites.ValidateFieldsText(txName, dxErrP))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public override void UpdateGRN()
        {
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["Id"], txId.Text);
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["Name"], txName.Text);
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["Remark"], meComment.Text);
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["ZoneName"], (cbZone.EditValue as IZone).Name);
            _gvDict.RefreshData();
        }
        public override void SetFields()
        {
            di.Name = txName.Text;
            di.Comment = meComment.Text;
            (di as DictionaryShedule).ZoneMain = cbZone.EditValue as IZone;
            LoadContent();
        }
        public override void GetFields()
        {
            txName.Text = di.Name;
            meComment.Text = di.Comment;
            var dictionaryShedule = di as DictionaryShedule;
            if (dictionaryShedule != null && dictionaryShedule.ZoneMainGroup != null)
            {
                cbZoneGroup.EditValue = dictionaryShedule.ZoneMainGroup;
                LoadComboZones(cbZone,dictionaryShedule.ZoneMainGroup);
                cbZone.EditValue = dictionaryShedule.ZoneMain;
                LoadContent();
            }


        }

        private void InitThis()
        {
            InitializeComponent();
            Localization();
            IsContentExist = true;
            this.Text = Resources.SheduleDictionary;
            if (!di.IsNew) GetFields();
            bFormLoading = false;
            LoadComboZoneGroups();
            this.btCancel.Location = new System.Drawing.Point(863, 11);
            this.btSave.Location = new System.Drawing.Point(772, 11);
            this.btDelete.Location = new System.Drawing.Point(681, 11);
            this.btSave.DialogResult = DialogResult.No;
        }

        private void Localization()
        {

        }

        void LoadComboZoneGroups()
        {
            foreach (var zone in DocItem.ZonesModel.Groups)
            {
                cbZoneGroup.Properties.Items.Add(zone);
                rcbZonesToGroups.Items.Add(zone);
            }
        } 

        void LoadComboZones(ComboBoxEdit combo,  ZonesGroup group)
        {

            combo.Properties.Items.Clear();
            if (group == null) return;
            foreach (var zone in DocItem.ZonesModel.Zones.Where(z => z.Group.Id == group.Id))
            {
                combo.Properties.Items.Add(zone);
            }
        }

        void LoadComboZones(RepositoryItemComboBox combo, ZonesGroup group)
        {
            combo.Items.Clear();
            if (group == null) return;
            foreach (var zone in DocItem.ZonesModel.Zones.Where(z => z.Group.Id == group.Id))
            {
                combo.Items.Add(zone);
            }
        }

        void LoadComboVehicles(VehiclesGroup group)
        {
            rcbVehicles.Items.Clear();
            if (group == null) return;
            foreach (var zone in DocItem.VehiclesModel.Vehicles.Where(v => v.Group == group))
            {
                rcbVehicles.Items.Add(zone);
            }
        }



        private void cbZoneGroup_SelectedValueChanged(object sender, EventArgs e)
        {
            LoadComboZones(cbZone,(ZonesGroup)cbZoneGroup.EditValue);
        }

        private void cbZone_SelectedValueChanged(object sender, EventArgs e)
        {
            ValidateFieldsAction();
        }

         public override bool LoadContent()
         {
             if (di == null) return false;
             LoadComboVehiclesGroups();
             gcShedule.DataSource = (di as DictionaryShedule).DictionarySheduleRecords;
             return true;
         }

         void LoadComboVehiclesGroups()
         {
             if (DocItem.VehiclesModel == null) return;
             foreach (var zone in DocItem.VehiclesModel.RootGroupsWithItems())
             {
                 rcbVehiclesGroups.Items.Add(zone);
             }
         }

         private void gvShedule_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
         {
             var record = gvShedule.GetRow(e.RowHandle) as DictionarySheduleRecord;
             if (record == null) return;
             switch (e.Column.FieldName)
             {
                 case "ZoneToGroup":
                     {
                         record.ZoneTo = null;
                         LoadComboZones(rcbZonesTo, record.ZoneToGroup);
                         break;
                     }
                 default:
                     {

                         if (record.IsNew)
                         {
                             record.DictionaryShedule  = (di as DictionaryShedule) ;
                         }
                         if (!record.Validate()) return;
                         record.Save();
                         break;
                     }
             }
             bContentChange = true;
             ValidateFieldsAction();
         }

         private void gvShedule_InitNewRow(object sender, InitNewRowEventArgs e)
         {
             var record = gvShedule.GetRow(e.RowHandle) as DictionarySheduleRecord;
             record.TimePlanStart = "00:00";
             record.TimePlanEnd = "00:00";
         }

         private void gvShedule_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
         {
             var record = gvShedule.GetRow(e.RowHandle) as DictionarySheduleRecord;
             if (record == null) return;
             TimeSpan tsTest;
             if (!TimeSpan.TryParse(record.TimePlanStart, out tsTest))
             {
                 e.Valid = false;
                 gvShedule.SetColumnError(gvShedule.Columns["TimePlanStart"], Resources.DataNeedInterval + " 00:00 - 24:00");
                 return;
             }
             if (!TimeSpan.TryParse(record.TimePlanEnd, out tsTest))
             {
                 e.Valid = false;
                 gvShedule.SetColumnError(gvShedule.Columns["TimePlanEnd"], Resources.DataNeedInterval + " 00:00 - 24:00");
                 return;
             }

         }

         private void gcShedule_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
         {
             switch (e.Button.ButtonType)
             {
                 case NavigatorButtonType.Remove:
                     {
                         if (XtraMessageBox.Show(Resources.DataDeleteConfirm,
                             Resources.Routers1,
                             MessageBoxButtons.YesNoCancel,
                             MessageBoxIcon.Question) == DialogResult.Yes)
                         {
                             var record = gvShedule.GetRow(gvShedule.FocusedRowHandle) as DictionarySheduleRecord;
                             if (record == null)
                                 e.Handled = true;
                             else
                                 e.Handled = !record.Delete();
                         }
                         else
                         {
                             e.Handled = true;
                         }
                         break;
                     }
             }
         }

         private void gvShedule_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
         {
             if (gvShedule.FocusedColumn.FieldName == "ZoneTo" && gvShedule.ActiveEditor is ComboBoxEdit)
             {
                 var record = gvShedule.GetRow(gvShedule.FocusedRowHandle) as DictionarySheduleRecord;
                 if (record == null) return;
                 LoadComboZones(rcbZonesTo, record.ZoneToGroup);
             }
         }
    }
} 
