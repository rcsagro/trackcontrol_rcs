using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using TrackControl.General;
using MySql.Data.MySqlClient;
using Agro.Utilites;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid;
using Route.Dictionaries;
using Route.Properties;
namespace Route.GridEditForms
{
    public partial class RouteGroupeForm : Agro.GridEditForms.FormSample
    {
        public RouteGroupeForm()
        {
            InitThis();
        }
        public RouteGroupeForm(GridView gvDict, bool bNew)
            : base(gvDict, bNew)
        {
            InitThis();
            di = new DictionaryRouteGrp(_Id);
            this.Text = Resources.SampleGroupe;
            GetFields();
            bFormLoading = false;
        }
        public override bool ValidateFields()
        {
            if (!DicUtilites.ValidateFieldsText(txName, dxErrP))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public override void UpdateGRN()
        {
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["Id"], txId.Text);
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["Name"], txName.Text);
            _gvDict.SetRowCellValue(_gvDict.FocusedRowHandle, _gvDict.Columns["Remark"], meComment.Text);
            _gvDict.RefreshData();
        }
        public override void SetFields()
        {
            di.Name = txName.Text;
            di.Comment = meComment.Text;
        }
        public override void GetFields()
        {
            txName.Text = di.Name;
            meComment.Text = di.Comment;
        }
        private void InitThis()
        {
            InitializeComponent();
            Localization();
        }
        private void Localization()
        {

        }
    }
}
