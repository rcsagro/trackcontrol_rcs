using System;
using System.Collections.Generic;
using System.Text;
using Agro.Dictionaries;
using System.Windows.Forms;
using TrackControl.General;
using MySql.Data.MySqlClient;
using Agro.Utilites;
using DevExpress.XtraEditors;
using Route.Properties;
using TrackControl.General.DatabaseDriver;

namespace Route.Dictionaries
{
    /// <summary>
    /// ������ �������� ���������
    /// </summary>
    public class DictionaryRouteGrp : DictionaryItem 
    {
        public DictionaryRouteGrp()
        {
        }
        public DictionaryRouteGrp(int ID)
            : base(ID)
        {
                Sql = "SELECT   rt_sample.* FROM rt_sample WHERE   rt_sample.Id = " + ID;
                //MySqlDataReader dr = _cnMySQL.GetDataReader(_sSql);
            using (DriverDb db = new DriverDb())
            {
                db.ConnectDb();
                db.GetDataReader(Sql); //_cnMySQL.GetDataReader(_sSql);
                if (db.Read())
                {
                    Name = TotUtilites.NdbNullReader(db, "Name", "").ToString();
                    Comment = TotUtilites.NdbNullReader(db, "Remark", "").ToString();
                }
                db.CloseDataReader();
            }
        }
        public override int AddItem()
        {
            if (base.AddItem() == 0 || (Id != 0)) return 0;
            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                Sql = "INSERT INTO `rt_sample`(Name,Id_main,IsGroupe,Remark) VALUES (?Name,0,1,?Remark)";
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                Sql = "INSERT INTO rt_sample(Name,Id_main,IsGroupe,Remark) VALUES (@Name,0,1,@Remark)";
            }

            using (DriverDb db = new DriverDb())
            {
                db.ConnectDb();
                //MySqlParameter[] parDate = new MySqlParameter[2];
                db.NewSqlParameterArray(2);
                //parDate[0] = new MySqlParameter("?Name", db.GettingString());
                db.NewSqlParameter(db.ParamPrefics + "Name", db.GettingString(), 0);
                //parDate[0].Value = (Name.Length > 100) ? Name.Substring(0, 100) : Name;
                db.SetSqlParameterValue((Name.Length > 100) ? Name.Substring(0, 100) : Name, 0);
                //parDate[1] = new MySqlParameter("?Remark", db.GettingString());
                db.NewSqlParameter(db.ParamPrefics + "Remark", db.GettingString(), 1);
                //parDate[1].Value = Comment;
                db.SetSqlParameterValue(Comment, 1);
                //Id = _cnMySQL.ExecuteReturnLastInsert(_sSql, db.GetSqlParameterArray);
                Id = db.ExecuteReturnLastInsert( Sql, db.GetSqlParameterArray, "rt_sample" );
            }
            return Id;
        }

        public override bool EditItem()
        {
            try
            {
                DriverDb dbs = new DriverDb();
                Sql = "UPDATE rt_sample SET Name = " + dbs.ParamPrefics + "Name ,Remark = " + dbs.ParamPrefics + "Remark WHERE (ID = " + Id + ")";

                using (DriverDb db = new DriverDb())
                {
                    db.ConnectDb();
                    // MySqlParameter[] parDate = new MySqlParameter[2];
                    db.NewSqlParameterArray(2);
                    //parDate[0] = new MySqlParameter("?Name", db.GettingString());
                    db.NewSqlParameter(db.ParamPrefics + "Name", db.GettingString(), 0);
                    //parDate[0].Value = Name;
                    db.SetSqlParameterValue(Name, 0);
                    //parDate[1] = new MySqlParameter("?Remark", db.GettingString());
                    db.NewSqlParameter(db.ParamPrefics + "Remark", db.GettingString(), 1);
                    //parDate[1].Value = Comment;
                    db.SetSqlParameterValue(Comment, 1);
                    //_cnMySQL.ExecuteNonQueryCommand(_sSql, db.GetSqlParameterArray);
                    db.ExecuteNonQueryCommand( Sql, db.GetSqlParameterArray );
                }
                return true;

            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, Resources.Routers1);
                return false;
            }
        }

        public override bool DeleteItem(bool bQuestionNeed)
        {
            try
            {
                if (TestLinks())
                {
                    if (bQuestionNeed)
                    {
                        if (DialogResult.No == XtraMessageBox.Show(Resources.DataDeleteConfirm, Resources.Routers1, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2)) return false;
                    }

                     Sql = "DELETE FROM rt_sample WHERE ID = " + Id;
                    using (DriverDb db = new DriverDb())
                    {
                        db.ConnectDb();
                        //_cnMySQL.ExecuteNonQueryCommand(_sSql);
                        db.ExecuteNonQueryCommand( Sql );
                        Id = 0;
                    }
                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, Resources.Routers1);
                
                return false;
            }
        }
        public override bool TestLinks()
        {
            Sql = "SELECT   COUNT(rt_sample.Id) FROM   rt_sample WHERE   Id_main  = " + Id;

            using (DriverDb db = new DriverDb())
            {
                db.ConnectDb();
                //int iLinks = _cnMySQL.GetScalarValueIntNull(_sSql);
                int iLinks = db.GetScalarValueIntNull( Sql );

                if (iLinks == 0)
                {
                    return true;
                }
                else
                {
                    XtraMessageBox.Show(Resources.DeleteBan, Resources.Routers1);
                    return false;
                }
            } // using
        }

        public override bool TestNameForExist(string sNameForTest, bool bViewInfo = true)
        {
            DriverDb dbs = new DriverDb();

            Sql = "SELECT   rt_sample.Id FROM rt_sample WHERE   rt_sample.Name = " + dbs.ParamPrefics + "Name"
                + "  AND rt_sample.IsGroupe = 1  AND rt_sample.Id <> " + Id;

            using (DriverDb db = new DriverDb())
            {
                db.ConnectDb();
                //MySqlParameter[] parDate = new MySqlParameter[1];
                db.NewSqlParameterArray(1);
                //parDate[0] = new MySqlParameter("?Name", db.GettingString());
                db.NewSqlParameter(db.ParamPrefics + "Name", db.GettingString(), 0);
                //parDate[0].Value = sNameForTest;
                db.SetSqlParameterValue(sNameForTest, 0);
                //int Id_name = _cnMySQL.GetScalarValueIntNull(_sSql, db.GetSqlParameterArray);
                int idName = db.GetScalarValueIntNull( Sql, db.GetSqlParameterArray );

                if (idName > 0 && bViewInfo)
                {
                    XtraMessageBox.Show(Resources.SampleGroupeExist + " " + sNameForTest + "!", Resources.Routers1);
                    return true;
                }
                else
                {
                    return false;
                }
            } // using
        }
    }
}
