using System;
using System.Collections.Generic;
using System.Text;
using Agro.Dictionaries;
using System.Windows.Forms;
using TrackControl.General;
using System.Data; 
using MySql.Data.MySqlClient;
using Agro.Utilites;
using DevExpress.XtraEditors;
using BaseReports.Procedure;
using Route.Properties;
using TrackControl.General.DatabaseDriver;
using TrackControl.General.DAL;

namespace Route.Dictionaries
{
    /// <summary>
    /// ������� ��������� �� ��������� ����������� ���
    /// </summary>
    public class DictionaryRoute:DictionaryItem 
    {
        #region ����
        /// <summary>
        /// ������ �������� �� ������� � �� (����������)
        /// </summary>
        private double _Distance;
        public double Distance
        {
            get { return _Distance; }
            set { _Distance = value; }
        }
        /// <summary>
        /// ������ 
        /// </summary>
        private int _Id_main;
        public int Id_main
        {
            get { return _Id_main; }
            set { _Id_main = value; }
        }
        #endregion
        public DictionaryRoute()
        {
        }
        public DictionaryRoute(int id)
            : base(id)
        {
            GetFromDbById(id);
        }

        public void GetFromDbById(int id)
        {
            Sql = "SELECT   rt_sample.* FROM rt_sample WHERE   rt_sample.Id = " + id;
            //MySqlDataReader dr = _cnMySQL.GetDataReader(_sSql);
            using (var db = new DriverDb())
            {
                db.ConnectDb();
                db.GetDataReader(Sql); //_cnMySQL.GetDataReader(_sSql);
                if (db.Read())
                {
                    Name = TotUtilites.NdbNullReader(db, "Name", "").ToString();
                    Comment = TotUtilites.NdbNullReader(db, "Remark", "").ToString();
                    IdOutLink = TotUtilites.NdbNullReader(db, "OutLinkId", "").ToString();
                    _Distance = Convert.ToDouble(TotUtilites.NdbNullReader(db, "Distance", 0));
                    _Id_main = Convert.ToInt32(TotUtilites.NdbNullReader(db, "Id_main", 0));
                }
                db.CloseDataReader();
            } // using
        }

        public int GetIdFromOutLinkId(string idOutLink)
        {
            Sql = string.Format(TrackControlQuery.RouteProvider.SelectIdByOutLinkId,idOutLink);
            int idSample = 0;
            using (var db = new DriverDb())
            {
                db.ConnectDb();
                db.GetDataReader(Sql); //_cnMySQL.GetDataReader(_sSql);
                if (db.Read())
                {
                    idSample = Convert.ToInt32(TotUtilites.NdbNullReader(db, "Id", 0));
                }
                db.CloseDataReader();
            } // using
            return idSample;
        }

        public override int AddItem()
        {
            if (base.AddItem() == 0 || (Id > 0)) return 0;
            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                Sql =
                    "INSERT INTO `rt_sample`(Name,Id_main,IsGroupe,Distance,Remark,OutLinkId) VALUES (?Name,?Id_main,0,?Distance,?Remark,?OutLinkId)";
            }
            else if (DriverDb.MssqlUse == DriverDb.TypeDataBaseUsing)
            {
                Sql = "INSERT INTO rt_sample(Name,Id_main,IsGroupe,Distance,Remark,OutLinkId) VALUES (@Name,@Id_main,0,@Distance,@Remark,@OutLinkId)";
            }

            using (DriverDb db = new DriverDb())
            {
                db.ConnectDb();

                //MySqlParameter[] parDate = new MySqlParameter[4];
                db.NewSqlParameterArray(5);
                //parDate[0] = new MySqlParameter("?Name", db.GettingString());
                db.NewSqlParameter(db.ParamPrefics + "Name", db.GettingString(), 0);
                //parDate[0].Value = (Name.Length > 100) ? Name.Substring(0, 100) : Name;
                db.SetSqlParameterValue((Name.Length > 100) ? Name.Substring(0, 100) : Name, 0);
                //parDate[1] = new MySqlParameter("?Remark", db.GettingString());
                db.NewSqlParameter(db.ParamPrefics + "Remark", db.GettingString(), 1);
                //parDate[1].Value = Comment;
                db.SetSqlParameterValue(Comment, 1);
                //parDate[2] = new MySqlParameter("?Id_main", db.GettingInt32());
                db.NewSqlParameter(db.ParamPrefics + "Id_main", db.GettingInt32(), 2);
                //parDate[2].Value = _Id_main;
                db.SetSqlParameterValue(_Id_main, 2);
                //parDate[3] = new MySqlParameter("?Distance", db.GettingDouble());
                db.NewSqlParameter(db.ParamPrefics + "Distance", db.GettingDouble(), 3);
                //parDate[3].Value = _Distance;
                db.SetSqlParameterValue(_Distance, 3);
                db.NewSqlParameter(db.ParamPrefics + "OutLinkId", db.GettingString(), 4);
                db.SetSqlParameterValue(IdOutLink, 4);
                //Id = _cnMySQL.ExecuteReturnLastInsert(_sSql, db.GetSqlParameterArray);
                Id = db.ExecuteReturnLastInsert( Sql, db.GetSqlParameterArray, "rt_sample" );
            }
            return Id;
        }
        public override bool EditItem()
        {
            try
            {
                if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                {
                    Sql =
                        "UPDATE rt_sample SET Name = ?Name,Id_main = ?Id_main ,Remark = ?Remark,Distance = ?Distance,OutLinkId = ?OutLinkId WHERE (ID = " +
                        Id + ")";
                }
                else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                {
                    Sql = "UPDATE rt_sample SET Name = @Name,Id_main = @Id_main ,Remark = @Remark,Distance = @Distance,OutLinkId = @OutLinkId WHERE (ID = " + Id + ")";
                }

                using (DriverDb db = new DriverDb())
                {
                    db.ConnectDb();
                    //MySqlParameter[] parDate = new MySqlParameter[4];
                    db.NewSqlParameterArray(5);
                    //parDate[0] = new MySqlParameter("?Name", db.GettingString());
                    db.NewSqlParameter(db.ParamPrefics + "Name", db.GettingString(), 0);
                    //parDate[0].Value = Name;
                    db.SetSqlParameterValue(Name, 0);
                    //parDate[1] = new MySqlParameter("?Remark", db.GettingString());
                    db.NewSqlParameter(db.ParamPrefics + "Remark", db.GettingString(), 1);
                    //parDate[1].Value = Comment;
                    db.SetSqlParameterValue(Comment, 1);
                    //parDate[2] = new MySqlParameter("?Id_main", db.GettingInt32());
                    db.NewSqlParameter(db.ParamPrefics + "Id_main", db.GettingInt32(), 2);
                    //parDate[2].Value = _Id_main;
                    db.SetSqlParameterValue(_Id_main, 2);
                    //parDate[3] = new MySqlParameter("?Distance", db.GettingDouble());
                    db.NewSqlParameter(db.ParamPrefics + "Distance", db.GettingDouble(), 3);
                    //parDate[3].Value = _Distance;
                    db.SetSqlParameterValue(_Distance, 3);
                    db.NewSqlParameter(db.ParamPrefics + "OutLinkId", db.GettingString(), 4);
                    db.SetSqlParameterValue(IdOutLink, 4);
                    //_cnMySQL.ExecuteNonQueryCommand(_sSql, parDate);
                    db.ExecuteNonQueryCommand(Sql, db.GetSqlParameterArray);
                    return true;
                } // using
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, Resources.Routers1);
                return false;
            }
        }
        public override bool DeleteItem(bool bQuestionNeed)
        {
            try
            {
                if (TestLinks())
                {
                    if (bQuestionNeed)
                    {
                        if (DialogResult.No == XtraMessageBox.Show(Resources.DataDeleteConfirm, Resources.Routers1, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2)) return false;

                    } 
                    
                    Sql = "DELETE FROM rt_samplet WHERE Id_main = " + Id;
                    using (DriverDb db = new DriverDb())
                    {
                        db.ConnectDb();
                        //_cnMySQL.ExecuteNonQueryCommand( _sSql );
                        db.ExecuteNonQueryCommand( Sql );
                    }

                    Sql = "DELETE FROM rt_sample WHERE ID = " + Id;
                    using( DriverDb db = new DriverDb() )
                    {
                        db.ConnectDb();
                        //_cnMySQL.ExecuteNonQueryCommand( _sSql ); 
                        db.ExecuteNonQueryCommand( Sql );
                    }
                    
                    Id = 0;
                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, Resources.Routers1);
                
                return false;
            }
        }

        public override bool TestLinks()
        {
            Sql = "SELECT   COUNT(rt_route.Id) FROM   rt_route WHERE   Id_sample  = " + Id;
            using (DriverDb db = new DriverDb())
            {
                db.ConnectDb();
                //int iLinks = _cnMySQL.GetScalarValueIntNull(_sSql);
                int iLinks = db.GetScalarValueIntNull( Sql );
                if (iLinks == 0)
                    return true;
                else
                {
                    XtraMessageBox.Show(Resources.DeleteBan, Resources.Routers1);
                    return false;
                }
            }
        }

        public override bool TestNameForExist(string sNameForTest, bool bViewInfo = true)
        {
            DriverDb dbs = new DriverDb();
            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                Sql = "SELECT   rt_sample.Id FROM rt_sample WHERE   rt_sample.Name = ?Name"
                        + "  AND rt_sample.IsGroupe = 0  AND rt_sample.Id <> " + Id;
            }
            else if( DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse )
            {
                Sql = "SELECT   rt_sample.Id FROM rt_sample WHERE   rt_sample.Name = '" + dbs.ParamPrefics + "Name"
                        + "'  AND rt_sample.IsGroupe = 0  AND rt_sample.Id <> " + Id;
            }

            using (DriverDb db = new DriverDb())
            {
                db.ConnectDb();
                //MySqlParameter[] parDate = new MySqlParameter[1];
                db.NewSqlParameterArray(1);
                //parDate[0] = new MySqlParameter("?Name", db.GettingString());
                db.NewSqlParameter(db.ParamPrefics + "Name", db.GettingString(), 0);
                //parDate[0].Value = sNameForTest;
                db.SetSqlParameterValue(sNameForTest, 0);
                //int Id_name = _cnMySQL.GetScalarValueIntNull(_sSql, db.GetSqlParameterArray);
                int idName = db.GetScalarValueIntNull(Sql, db.GetSqlParameterArray);
                if (idName > 0 && bViewInfo)
                {
                    XtraMessageBox.Show(Resources.SampleExist + " " + sNameForTest + "!", Resources.Routers1);
                    return true;
                }
                else
                {
                    return false;
                }
            } // using
        }
        public int AddContent(int Pos, int Zone, int Event, string TimeRel, string TimeAbs, string TimeRelP, string TimeRelM)
        {
            TestTimeLenght(TimeRel, "TimeRel");
            //TestTimeLenght(TimeAbs, "TimeAbs");
            TestTimeLenght(TimeRelP, "TimeRelP");
            TestTimeLenght(TimeRelM, "TimeRelM");
            try
            {
                if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                {
                    Sql =
                        string.Format(
                            "INSERT INTO rt_samplet (Id_main ,Position, Id_zone,Id_event,TimeRel,TimeRelP,TimeRelM,TimeAbs)"
                            + " VALUES ({0},{1},{2},{3},?TimeRel,?TimeRelP,?TimeRelM,?TimeAbs)", Id, Pos, Zone, Event,
                            TimeRel, TimeRelP, TimeRelM);
                }
                else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                {
                    Sql = string.Format( "INSERT INTO rt_samplet (Id_main ,Position, Id_zone,Id_event,TimeRel,TimeRelP,TimeRelM,TimeAbs)"
                   + " VALUES ({0},{1},{2},{3},@TimeRel,@TimeRelP,@TimeRelM,@TimeAbs)", Id, Pos, Zone, Event, TimeRel, TimeRelP, TimeRelM );
                }

                using (DriverDb db = new DriverDb())
                {
                    db.ConnectDb();
                    //MySqlParameter[] parDate = new MySqlParameter[4];
                    db.NewSqlParameterArray(4);
                    //parDate[0] = new MySqlParameter("?TimeRel", db.GettingString());
                    db.NewSqlParameter(db.ParamPrefics + "TimeRel", db.GettingString(), 0);
                    //parDate[0].Value = TimeRel;
                    db.SetSqlParameterValue(TimeRel, 0);
                    //parDate[1] = new MySqlParameter("?TimeRelP", db.GettingString());
                    db.NewSqlParameter(db.ParamPrefics + "TimeRelP", db.GettingString(), 1);
                    //parDate[1].Value = TimeRelP;
                    db.SetSqlParameterValue(TimeRelP, 1);
                    //parDate[2] = new MySqlParameter("?TimeRelM", db.GettingString());
                    db.NewSqlParameter(db.ParamPrefics + "TimeRelM", db.GettingString(), 2);
                    //parDate[2].Value = TimeRelM;
                    db.SetSqlParameterValue(TimeRelM, 2);
                    //parDate[3] = new MySqlParameter("?TimeAbs", db.GettingString());
                    db.NewSqlParameter(db.ParamPrefics + "TimeAbs", db.GettingString(), 3);
                    //parDate[3].Value = TimeAbs;
                    db.SetSqlParameterValue(TimeAbs, 3);
                    //return _cnMySQL.ExecuteReturnLastInsert(_sSql, db.GetSqlParameterArray);
                    return db.ExecuteReturnLastInsert( Sql, db.GetSqlParameterArray, "rt_samplet" );
                } // using
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message);
                return ConstsGen.RECORD_MISSING;
            }
        }
        public bool EditContent(int Id_record, int Pos, int Zone, int Event, string TimeRel, string TimeAbs, string TimeRelP, string TimeRelM)
        {
            TestTimeLenght(TimeRel, "TimeRel");
            //TestTimeLenght(TimeAbs, "TimeAbs");
            TestTimeLenght(TimeRelP, "TimeRelP");
            TestTimeLenght(TimeRelM, "TimeRelM");
            try
            {
                if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                {
                    Sql = string.Format("UPDATE rt_samplet SET Position = {0},Id_zone = {1},Id_event = {2},"
                                          +
                                          " TimeRel = ?TimeRel,TimeRelP = ?TimeRelP,TimeRelM = ?TimeRelM,TimeAbs = ?TimeAbs",
                                          Pos, Zone, Event);
                    Sql = Sql + " WHERE (ID = " + Id_record + ")";
                }
                else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                {
                    Sql = string.Format( "UPDATE rt_samplet SET Position = {0},Id_zone = {1},Id_event = {2},"
                                          + " TimeRel = @TimeRel,TimeRelP = @TimeRelP,TimeRelM = @TimeRelM,TimeAbs = @TimeAbs",
                                          Pos, Zone, Event );
                    Sql = Sql + " WHERE (ID = " + Id_record + ")";                    
                }

                using (DriverDb db = new DriverDb())
                {
                    db.ConnectDb();
                    //MySqlParameter[] parDate = new MySqlParameter[4];
                    db.NewSqlParameterArray(4);
                    //parDate[0] = new MySqlParameter("?TimeRel", db.GettingString());
                    db.NewSqlParameter(db.ParamPrefics + "TimeRel", db.GettingString(), 0);
                    //parDate[0].Value = TimeRel;
                    db.SetSqlParameterValue(TimeRel, 0);
                    //parDate[1] = new MySqlParameter("?TimeRelP", db.GettingString());
                    db.NewSqlParameter(db.ParamPrefics + "TimeRelP", db.GettingString(), 1);
                    //parDate[1].Value = TimeRelP;
                    db.SetSqlParameterValue(TimeRelP, 1);
                    //parDate[2] = new MySqlParameter("?TimeRelM", db.GettingString());
                    db.NewSqlParameter(db.ParamPrefics + "TimeRelM", db.GettingString(), 2);
                    //parDate[2].Value = TimeRelM;
                    db.SetSqlParameterValue(TimeRelM, 2);
                    //parDate[3] = new MySqlParameter("?TimeAbs", db.GettingString());
                    db.NewSqlParameter(db.ParamPrefics + "TimeAbs", db.GettingString(), 3);
                    //parDate[3].Value = TimeAbs;
                    db.SetSqlParameterValue(TimeAbs, 3);
                    //_cnMySQL.ExecuteNonQueryCommand(_sSql, db.GetSqlParameterArray);
                    db.ExecuteNonQueryCommand( Sql, db.GetSqlParameterArray );
                } // using

                return true;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message);  
                return false;
            }

        }
        public bool DeleteContent(int Id_record)
        {
            try
            {
                Sql = "DELETE FROM rt_samplet WHERE (ID = " + Id_record + ")";
                using (DriverDb db = new DriverDb())
                {
                    db.ConnectDb();
                    //_cnMySQL.ExecuteNonQueryCommand(_sSql);
                    db.ExecuteNonQueryCommand( Sql );
                }
                return true;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message);
                return false;
            }
        }
        public DataTable GetContent()
        {
            Sql = "SELECT   rt_samplet.Id, rt_samplet.Position, zones.ZonesGroupId AS Id_zone_grp,"
            + " rt_samplet.Id_zone as Zone_ID, rt_samplet.Id_event, rt_samplet.TimeRel,rt_samplet.TimeAbs, rt_samplet.TimeRelP, rt_samplet.TimeRelM"
            + " FROM zones   INNER JOIN  rt_samplet ON zones.Zone_ID = rt_samplet.Id_zone "
            + " WHERE  rt_samplet.Id_main = " + Id + " ORDER BY rt_samplet.Position";
            using (DriverDb db = new DriverDb())
            {
                db.ConnectDb();
                return db.GetDataTable( Sql );
            }
            //return _cnMySQL.GetDataTable( _sSql );
        }
        public bool IsContentExis(int Id_new_record)
        {
            try
            {
                Sql = string.Format("SELECT Id FROM rt_samplet WHERE (Id = {0})",Id_new_record) ;
                using (DriverDb db = new DriverDb())
                {
                    db.ConnectDb();
                    //if (_cnMySQL.GetScalarValueNull<int>(_sSql, 0) == 0)
                    if( db.GetScalarValueNull<int>( Sql, 0 ) == 0 )
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message);
                return false;
            }
        }
        public bool TestTimeLenght(string timeForTest,string nameField)
        {
            if (timeForTest.Length > 5)
            {
                throw new ArgumentException(string.Format("The lenght {0} violates the MaxLenght = 5 limit (00:00)",timeForTest.Length, 1), nameField);
            }
            else
            return true;
        }

        
    }
}
