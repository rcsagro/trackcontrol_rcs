﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.XtraEditors;
using Route.Documents.Shedule;
using Route.Properties;
using TrackControl.General;
using TrackControl.Vehicles;
using Route.DAL;

namespace Route.Dictionaries
{
    public class DictionarySheduleRecord: Entity
    {
        private ZonesGroup _zoneGroup;
        public DictionaryShedule DictionaryShedule { get; set; }
        public IZone ZoneTo { get; set; }
        public string TimePlanStart { get; set; }
        public string TimePlanEnd { get; set; }
        public float Distance { get; set; } 



        public ZonesGroup ZoneToGroup
        {
            get { return ZoneTo == null ? _zoneGroup : ZoneTo.Group; }
            set
            {
                _zoneGroup = value;
                ZoneTo = null;
            }
        }


        public void Save()
        {
            DictionaryDictionarySheduleRecordProvider.Save(this);
        }

        public bool Delete()
        {
            try
            {
                DictionaryDictionarySheduleRecordProvider.DeleteOne(this);
                return true;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, Resources.Routers1);
                return false;
            }

        }

        public bool Validate()
        {
            if (DictionaryShedule != null && ZoneTo != null )
                return true;
            else
            {
                return false;
            }
        }
        
        //public bool IsReadyForTracking()
        //{
        //    if (IsClose ||  TimePlanEnd == null || Distance == 0) 
        //        return false; 
        //    else
        //        return true;
        //}
    }
}
