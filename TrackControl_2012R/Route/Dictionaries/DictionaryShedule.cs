﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using Agro.Dictionaries;
using Route.DAL;
using TrackControl.General;

namespace Route.Dictionaries
{
    public class DictionaryShedule : DictionaryItem
    {
        private ZonesGroup _zoneGroup;
        public IZone ZoneMain { get; set;}
        public ZonesGroup ZoneMainGroup
        {
            get { return ZoneMain == null ? _zoneGroup : ZoneMain.Group; }
            set { 
                _zoneGroup = value;
                ZoneMain = null;
            }
        }


        public IList<DictionarySheduleRecord> DictionarySheduleRecords { get; set; }

        public DictionaryShedule()
        {

        }

        public DictionaryShedule(int id) 
        {
            if (id == 0) return;
            DictionaryShedule ds = DictionarySheduleProvider.GetOne(id);
            Id = ds.Id;
            Name = ds.Name;
            Comment = ds.Comment;
            ZoneMain = ds.ZoneMain;
            DictionarySheduleRecords = ds.DictionarySheduleRecords;
        }


        public override int AddItem()
        {
            DictionarySheduleRecords = new BindingList<DictionarySheduleRecord>();
            return DictionarySheduleProvider.Save(this);
        }

        public override bool EditItem()
        {
             DictionarySheduleProvider.Save(this);
            return true;
        }

        public override bool DeleteItem(bool bQuestionNeed)
        {
            return DictionarySheduleProvider.DeleteOne(this);
        }

        public override string ToString()
        {
            return String.Format("{0}", Name);
        }

    }
}
