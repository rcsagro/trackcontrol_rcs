namespace Route
{
    partial class DgrmGant
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraCharts.GanttDiagram ganttDiagram1 = new DevExpress.XtraCharts.GanttDiagram();
            DevExpress.XtraCharts.ConstantLine constantLine1 = new DevExpress.XtraCharts.ConstantLine();
            DevExpress.XtraCharts.Series series1 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.RangeBarSeriesLabel rangeBarSeriesLabel1 = new DevExpress.XtraCharts.RangeBarSeriesLabel();
            DevExpress.XtraCharts.RangeBarPointOptions rangeBarPointOptions1 = new DevExpress.XtraCharts.RangeBarPointOptions();
            DevExpress.XtraCharts.OverlappedGanttSeriesView overlappedGanttSeriesView1 = new DevExpress.XtraCharts.OverlappedGanttSeriesView();
            DevExpress.XtraCharts.Series series2 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.RangeBarSeriesLabel rangeBarSeriesLabel2 = new DevExpress.XtraCharts.RangeBarSeriesLabel();
            DevExpress.XtraCharts.RangeBarPointOptions rangeBarPointOptions2 = new DevExpress.XtraCharts.RangeBarPointOptions();
            DevExpress.XtraCharts.OverlappedGanttSeriesView overlappedGanttSeriesView2 = new DevExpress.XtraCharts.OverlappedGanttSeriesView();
            DevExpress.XtraCharts.RangeBarSeriesLabel rangeBarSeriesLabel3 = new DevExpress.XtraCharts.RangeBarSeriesLabel();
            DevExpress.XtraCharts.OverlappedGanttSeriesView overlappedGanttSeriesView3 = new DevExpress.XtraCharts.OverlappedGanttSeriesView();
            this.chartGant = new DevExpress.XtraCharts.ChartControl();
            ((System.ComponentModel.ISupportInitialize)(this.chartGant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(ganttDiagram1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(rangeBarSeriesLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(overlappedGanttSeriesView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(rangeBarSeriesLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(overlappedGanttSeriesView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(rangeBarSeriesLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(overlappedGanttSeriesView3)).BeginInit();
            this.SuspendLayout();
            // 
            // chartGant
            // 
            ganttDiagram1.AxisX.GridLines.Visible = true;
            ganttDiagram1.AxisX.Range.AlwaysShowZeroLevel = true;
            ganttDiagram1.AxisX.Range.ScrollingRange.SideMarginsEnabled = true;
            ganttDiagram1.AxisX.Range.SideMarginsEnabled = true;
            ganttDiagram1.AxisX.Title.Text = "";
            ganttDiagram1.AxisX.Title.Visible = true;
            ganttDiagram1.AxisX.VisibleInPanesSerializable = "-1";
            constantLine1.AxisValueSerializable = "09/25/2006 00:00:00.000";
            constantLine1.Name = "Progress Line";
            constantLine1.ShowInLegend = false;
            ganttDiagram1.AxisY.ConstantLines.AddRange(new DevExpress.XtraCharts.ConstantLine[] {
            constantLine1});
            ganttDiagram1.AxisY.DateTimeScaleOptions.GridAlignment = DevExpress.XtraCharts.DateTimeGridAlignment.Hour;
            ganttDiagram1.AxisY.DateTimeScaleOptions.MeasureUnit = DevExpress.XtraCharts.DateTimeMeasureUnit.Minute;
            ganttDiagram1.AxisY.GridLines.MinorVisible = true;
            ganttDiagram1.AxisY.GridSpacing = 0.5D;
            ganttDiagram1.AxisY.GridSpacingAuto = false;
            ganttDiagram1.AxisY.Interlaced = true;
            ganttDiagram1.AxisY.Label.DateTimeOptions.Format = DevExpress.XtraCharts.DateTimeFormat.Custom;
            ganttDiagram1.AxisY.Label.DateTimeOptions.FormatString = "dd/MM HH:mm";
            ganttDiagram1.AxisY.Label.Staggered = true;
            ganttDiagram1.AxisY.MinorCount = 4;
            ganttDiagram1.AxisY.Range.AlwaysShowZeroLevel = true;
            ganttDiagram1.AxisY.Range.ScrollingRange.SideMarginsEnabled = true;
            ganttDiagram1.AxisY.Range.SideMarginsEnabled = true;
            ganttDiagram1.AxisY.Title.Text = "Date";
            ganttDiagram1.AxisY.VisibleInPanesSerializable = "-1";
            ganttDiagram1.EnableAxisXScrolling = true;
            ganttDiagram1.EnableAxisXZooming = true;
            ganttDiagram1.EnableAxisYScrolling = true;
            ganttDiagram1.EnableAxisYZooming = true;
            ganttDiagram1.LabelsResolveOverlappingMinIndent = 3;
            this.chartGant.Diagram = ganttDiagram1;
            this.chartGant.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartGant.Legend.AlignmentHorizontal = DevExpress.XtraCharts.LegendAlignmentHorizontal.Right;
            this.chartGant.Legend.Direction = DevExpress.XtraCharts.LegendDirection.LeftToRight;
            this.chartGant.Location = new System.Drawing.Point(0, 0);
            this.chartGant.Name = "chartGant";
            series1.ArgumentDataMember = "P";
            series1.CrosshairEnabled = DevExpress.Utils.DefaultBoolean.True;
            series1.CrosshairHighlightPoints = DevExpress.Utils.DefaultBoolean.True;
            series1.CrosshairLabelPattern = "{S}  {V1:HH:mm}  {V2:HH:mm}";
            series1.CrosshairLabelVisibility = DevExpress.Utils.DefaultBoolean.True;
            rangeBarPointOptions1.ValueDateTimeOptions.Format = DevExpress.XtraCharts.DateTimeFormat.ShortTime;
            rangeBarSeriesLabel1.PointOptions = rangeBarPointOptions1;
            rangeBarSeriesLabel1.ResolveOverlappingMode = DevExpress.XtraCharts.ResolveOverlappingMode.Default;
            series1.Label = rangeBarSeriesLabel1;
            series1.Name = "����";
            series1.ValueDataMembersSerializable = "DatePlan;DatePlanNext";
            series1.ValueScaleType = DevExpress.XtraCharts.ScaleType.DateTime;
            overlappedGanttSeriesView1.BarWidth = 0.4D;
            series1.View = overlappedGanttSeriesView1;
            series2.ArgumentDataMember = "P";
            series2.CrosshairLabelPattern = "{S}  {V1:HH:mm}  {V2:HH:mm}";
            rangeBarPointOptions2.ValueDateTimeOptions.Format = DevExpress.XtraCharts.DateTimeFormat.ShortTime;
            rangeBarSeriesLabel2.PointOptions = rangeBarPointOptions2;
            rangeBarSeriesLabel2.ResolveOverlappingMode = DevExpress.XtraCharts.ResolveOverlappingMode.Default;
            series2.Label = rangeBarSeriesLabel2;
            series2.Name = "����";
            series2.ValueDataMembersSerializable = "DateFact;DateFactNext";
            series2.ValueScaleType = DevExpress.XtraCharts.ScaleType.DateTime;
            overlappedGanttSeriesView2.BarWidth = 0.3D;
            series2.View = overlappedGanttSeriesView2;
            this.chartGant.SeriesSerializable = new DevExpress.XtraCharts.Series[] {
        series1,
        series2};
            rangeBarSeriesLabel3.ResolveOverlappingMode = DevExpress.XtraCharts.ResolveOverlappingMode.HideOverlapped;
            this.chartGant.SeriesTemplate.Label = rangeBarSeriesLabel3;
            this.chartGant.SeriesTemplate.View = overlappedGanttSeriesView3;
            this.chartGant.Size = new System.Drawing.Size(726, 358);
            this.chartGant.TabIndex = 3;
            this.chartGant.TabStop = false;
            // 
            // DgrmGant
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.chartGant);
            this.Name = "DgrmGant";
            this.Size = new System.Drawing.Size(726, 358);
            ((System.ComponentModel.ISupportInitialize)(ganttDiagram1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(rangeBarSeriesLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(overlappedGanttSeriesView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(rangeBarSeriesLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(overlappedGanttSeriesView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(rangeBarSeriesLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(overlappedGanttSeriesView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartGant)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraCharts.ChartControl chartGant;
    }
}
