using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraCharts;
using Route.Properties;

namespace Route
{
    public partial class GraficMoving : DevExpress.XtraEditors.XtraUserControl,IChartControl
    {
        public GraficMoving()
        {
            InitializeComponent();
            Localization();
        }
        
        private void chartTask_CustomDrawSeriesPoint(object sender, DevExpress.XtraCharts.CustomDrawSeriesPointEventArgs e)
        {
            //if (e.Series.Name == "����")
            //{
            DataRowView drv = e.SeriesPoint.Tag as DataRowView;
            if ((int)drv["Id_zone"] > 0)
            {
                e.LabelText = e.LabelText + " " + drv["P"].ToString();
            }
            //}
        }

        #region IChartControl Members

        public DataTable DataSourse
        {
            get
            {
                return (DataTable)chartTask.DataSource;
            }
            set
            {
                chartTask.DataSource = value;
            }
        }

        public void ExportToXls(string xslFilePath)
        {
            chartTask.ExportToXls(xslFilePath);
        }

        #endregion
        private void Localization()
        {
            XYDiagram diagram = (XYDiagram)chartTask.Diagram;
            diagram.AxisX.Title.Text = Resources.MovingTime;
            diagram.AxisY.Title.Text = Resources.DistanceKm;
            chartTask.Series[0].Name = Resources.Fact;
            chartTask.Series[1].Name = Resources.Plan; 
        }
    }
}
