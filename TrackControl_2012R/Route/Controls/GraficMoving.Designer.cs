namespace Route
{
    partial class GraficMoving
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraCharts.XYDiagram xyDiagram1 = new DevExpress.XtraCharts.XYDiagram();
            DevExpress.XtraCharts.Series series1 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.PointSeriesLabel pointSeriesLabel1 = new DevExpress.XtraCharts.PointSeriesLabel();
            DevExpress.XtraCharts.PointOptions pointOptions1 = new DevExpress.XtraCharts.PointOptions();
            DevExpress.XtraCharts.PointOptions pointOptions2 = new DevExpress.XtraCharts.PointOptions();
            DevExpress.XtraCharts.LineSeriesView lineSeriesView1 = new DevExpress.XtraCharts.LineSeriesView();
            DevExpress.XtraCharts.Series series2 = new DevExpress.XtraCharts.Series();
            DevExpress.XtraCharts.PointSeriesLabel pointSeriesLabel2 = new DevExpress.XtraCharts.PointSeriesLabel();
            DevExpress.XtraCharts.PointOptions pointOptions3 = new DevExpress.XtraCharts.PointOptions();
            DevExpress.XtraCharts.PointOptions pointOptions4 = new DevExpress.XtraCharts.PointOptions();
            DevExpress.XtraCharts.LineSeriesView lineSeriesView2 = new DevExpress.XtraCharts.LineSeriesView();
            DevExpress.XtraCharts.PointSeriesLabel pointSeriesLabel3 = new DevExpress.XtraCharts.PointSeriesLabel();
            DevExpress.XtraCharts.PointOptions pointOptions5 = new DevExpress.XtraCharts.PointOptions();
            DevExpress.XtraCharts.LineSeriesView lineSeriesView3 = new DevExpress.XtraCharts.LineSeriesView();
            DevExpress.XtraCharts.ChartTitle chartTitle1 = new DevExpress.XtraCharts.ChartTitle();
            DevExpress.XtraCharts.ChartTitle chartTitle2 = new DevExpress.XtraCharts.ChartTitle();
            this.chartTask = new DevExpress.XtraCharts.ChartControl();
            ((System.ComponentModel.ISupportInitialize)(this.chartTask)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(xyDiagram1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(lineSeriesView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(lineSeriesView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(lineSeriesView3)).BeginInit();
            this.SuspendLayout();
            // 
            // chartTask
            // 
            xyDiagram1.AxisX.AutoScaleBreaks.MaxCount = 1;
            xyDiagram1.AxisX.DateTimeGridAlignment = DevExpress.XtraCharts.DateTimeMeasurementUnit.Minute;
            xyDiagram1.AxisX.DateTimeMeasureUnit = DevExpress.XtraCharts.DateTimeMeasurementUnit.Minute;
            xyDiagram1.AxisX.DateTimeOptions.Format = DevExpress.XtraCharts.DateTimeFormat.Custom;
            xyDiagram1.AxisX.DateTimeOptions.FormatString = "dd/MM HH:mm";
            xyDiagram1.AxisX.Interlaced = true;
            xyDiagram1.AxisX.Label.Staggered = true;
            xyDiagram1.AxisX.MinorCount = 1;
            xyDiagram1.AxisX.Range.ScrollingRange.SideMarginsEnabled = true;
            xyDiagram1.AxisX.Range.SideMarginsEnabled = true;
            xyDiagram1.AxisX.Title.Text = "����� ��������";
            xyDiagram1.AxisX.Title.Visible = true;
            xyDiagram1.AxisX.VisibleInPanesSerializable = "-1";
            xyDiagram1.AxisY.Range.ScrollingRange.SideMarginsEnabled = true;
            xyDiagram1.AxisY.Range.SideMarginsEnabled = true;
            xyDiagram1.AxisY.Title.Text = "����������, ��";
            xyDiagram1.AxisY.Title.Visible = true;
            xyDiagram1.AxisY.VisibleInPanesSerializable = "-1";
            xyDiagram1.EnableAxisXScrolling = true;
            xyDiagram1.EnableAxisYScrolling = true;
            xyDiagram1.EnableAxisXZooming = true;
            xyDiagram1.EnableAxisXZooming = true;
            xyDiagram1.LabelsResolveOverlappingMinIndent = 3;
            this.chartTask.Diagram = xyDiagram1;
            this.chartTask.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartTask.Location = new System.Drawing.Point(0, 0);
            this.chartTask.Name = "chartTask";
            series1.ArgumentDataMember = "DateFact";
            series1.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.DateTime;
            pointSeriesLabel1.LineVisible = true;
            pointSeriesLabel1.ResolveOverlappingMode = DevExpress.XtraCharts.ResolveOverlappingMode.Default;
            pointSeriesLabel1.Shadow.Size = 1;
            pointSeriesLabel1.Shadow.Visible = true;
            series1.Label = pointSeriesLabel1;
            pointOptions1.ArgumentDateTimeOptions.Format = DevExpress.XtraCharts.DateTimeFormat.LongDate;
            pointOptions1.ArgumentDateTimeOptions.FormatString = "t";
            series1.LegendPointOptions = pointOptions1;
            series1.Name = "����";
            pointOptions2.ArgumentDateTimeOptions.Format = DevExpress.XtraCharts.DateTimeFormat.ShortTime;
            pointOptions2.ArgumentDateTimeOptions.FormatString = "t";
            pointOptions2.PointView = DevExpress.XtraCharts.PointView.Argument;
            pointOptions2.ValueNumericOptions.Format = DevExpress.XtraCharts.NumericFormat.Number;
            series1.PointOptions = pointOptions2;
            series1.SynchronizePointOptions = false;
            series1.ValueDataMembersSerializable = "DistanceCollect";
            series1.View = lineSeriesView1;
            series2.ArgumentDataMember = "DatePlan";
            series2.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.DateTime;
            pointSeriesLabel2.LineVisible = true;
            pointSeriesLabel2.ResolveOverlappingMode = DevExpress.XtraCharts.ResolveOverlappingMode.Default;
            pointSeriesLabel2.Shadow.Size = 1;
            pointSeriesLabel2.Shadow.Visible = true;
            series2.Label = pointSeriesLabel2;
            pointOptions3.ArgumentDateTimeOptions.Format = DevExpress.XtraCharts.DateTimeFormat.LongDate;
            pointOptions3.ArgumentDateTimeOptions.FormatString = "t";
            series2.LegendPointOptions = pointOptions3;
            series2.Name = "����";
            pointOptions4.ArgumentDateTimeOptions.Format = DevExpress.XtraCharts.DateTimeFormat.ShortTime;
            pointOptions4.ArgumentDateTimeOptions.FormatString = "t";
            pointOptions4.PointView = DevExpress.XtraCharts.PointView.Argument;
            pointOptions4.ValueNumericOptions.Format = DevExpress.XtraCharts.NumericFormat.Number;
            series2.PointOptions = pointOptions4;
            series2.SynchronizePointOptions = false;
            series2.ValueDataMembersSerializable = "DistancePlan";
            series2.View = lineSeriesView2;
            this.chartTask.SeriesSerializable = new DevExpress.XtraCharts.Series[] {
        series1,
        series2};
            this.chartTask.SeriesTemplate.ArgumentScaleType = DevExpress.XtraCharts.ScaleType.DateTime;
            pointSeriesLabel3.LineVisible = true;
            this.chartTask.SeriesTemplate.Label = pointSeriesLabel3;
            pointOptions5.ArgumentDateTimeOptions.Format = DevExpress.XtraCharts.DateTimeFormat.ShortTime;
            this.chartTask.SeriesTemplate.LegendPointOptions = pointOptions5;
            this.chartTask.SeriesTemplate.SynchronizePointOptions = false;
            this.chartTask.SeriesTemplate.View = lineSeriesView3;
            this.chartTask.Size = new System.Drawing.Size(722, 359);
            this.chartTask.TabIndex = 4;
            this.chartTask.TabStop = false;
            chartTitle1.Text = "";
            chartTitle2.Alignment = System.Drawing.StringAlignment.Far;
            chartTitle2.Dock = DevExpress.XtraCharts.ChartTitleDockStyle.Bottom;
            chartTitle2.Font = new System.Drawing.Font("Tahoma", 8F);
            chartTitle2.Text = "";
            chartTitle2.TextColor = System.Drawing.Color.Gray;
            this.chartTask.Titles.AddRange(new DevExpress.XtraCharts.ChartTitle[] {
            chartTitle1,
            chartTitle2});
            this.chartTask.CustomDrawSeriesPoint += new DevExpress.XtraCharts.CustomDrawSeriesPointEventHandler(this.chartTask_CustomDrawSeriesPoint);
            // 
            // GraficMoving
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.chartTask);
            this.Name = "GraficMoving";
            this.Size = new System.Drawing.Size(722, 359);
            ((System.ComponentModel.ISupportInitialize)(xyDiagram1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(lineSeriesView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(lineSeriesView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(lineSeriesView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartTask)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraCharts.ChartControl chartTask;
    }
}
