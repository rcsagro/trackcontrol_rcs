using System;
using System.Data; 
using System.Collections.Generic;
using System.Text;

namespace Route
{
    interface IChartControl
    {
        DataTable DataSourse { get;set;}
        void ExportToXls(string xslFilePath);
    }
}
