using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Route.Properties;
namespace Route
{
    public partial class DgrmGant : DevExpress.XtraEditors.XtraUserControl,IChartControl
    {
        public DgrmGant()
        {
            InitializeComponent();
            Localization();
        }

        #region IChartControl Members

        public DataTable DataSourse
        {
            get
            {
                return (DataTable)chartGant.DataSource;
            }
            set
            {
                chartGant.DataSource = value;
            }
        }

        public void ExportToXls(string xslFilePath)
        {
            chartGant.ExportToXls(xslFilePath); ;
        }
        private void Localization()
        {
            chartGant.Series[0].Name = Resources.Plan;
            chartGant.Series[1].Name = Resources.Fact;
        }
        #endregion
    }
}
