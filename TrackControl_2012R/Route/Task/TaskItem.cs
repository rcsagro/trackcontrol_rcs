using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Agro;
using Agro.Utilites;
using BaseReports.CrossingCZ;
using BaseReports.Procedure;
using BaseReports.Procedure.Calibration;
using DevExpress.XtraEditors;
using LocalCache;
using MySql.Data.MySqlClient;
using Route.Dictionaries;
using Route.Properties;
using TrackControl.General;
using TrackControl.General.DatabaseDriver;
using TrackControl.Reports;
using TrackControl.Vehicles;

namespace Route
{
    /// <summary>
    /// ���������� ����
    /// </summary>
    public class TaskItem : DocItem
    {
    #region �������
        public event StatusMessage ChangeStatusEvent;
        public event ProgressBarValue ChangeProgressBar;
        private void SetProgressBar(int Value)
        {
            if (ChangeProgressBar != null) ChangeProgressBar(Value);
        }
        private void SetStatusEvent(string sMessage)
        {
            if (ChangeStatusEvent != null)
            {
                if (sMessage == Resources.Ready)
                {
                    ChangeStatusEvent(sMessage);
                }
                else
                {
                    ChangeStatusEvent(_ID > 0 ? ( Resources.Number + " " + _ID + ":" + sMessage) : sMessage);
                }
                Application.DoEvents();
            }
        }
    #endregion
    #region ����
            /// <summary>
            /// ��������� ���� ������� ����������
            /// </summary>
            DateTime _dtStart =  DateTime.Today;
            /// <summary>
            /// �������� ���� ������� ����������
            /// </summary>
            DateTime _dtEnd  = DateTime.Today.AddDays(1).AddMinutes(-1);
            public DateTime DateEnd
            {
                get { return _dtEnd; }
                set { _dtEnd = value; }
            }
            /// <summary>
            /// ��������� ����������� ����� ��������
            /// </summary>
            DateTime _dtFactStart = DateTime.Today;
           /// <summary>
            /// �������� ����������� ����� ��������
           /// </summary>
            DateTime _dtFactEnd = DateTime.Today.AddDays(1).AddMinutes(-1);
            /// <summary>
            /// ��������� �������� ����� ��������
            /// </summary>
            DateTime _dtPlanStart = DateTime.Today;
            /// <summary>
            /// �������� �������� ����� ��������
            /// </summary>
            DateTime _dtPlanEnd = DateTime.Today.AddDays(1).AddMinutes(-1);
            /// <summary>
            /// ��� ������ ������������� ��������
            /// </summary>
            int _Team_Id;
            public int Team_Id
            {
                get { return _Team_Id; }
                set { _Team_Id = value; }
            }
            /// <summary>
            /// ��� ������������� ��������
            /// </summary>
            int _Mobitel_Id;
            public int Mobitel_Id
            {
                get
                {
                    return _Mobitel_Id;
                }
                set
                {
                    _Mobitel_Id = value;
                }
            }
            /// <summary>
            /// ��� ��������
            /// </summary>
            int _Driver_Id ;
            public int Driver_Id
            {
                get
                {
                    return _Driver_Id;
                }
                set
                {
                    _Driver_Id = value;
                }
            }
            /// <summary>
            /// ��� ������� ��������. ����=0 - ���������� ���� ������ �� �����
            /// </summary>
            int _Sample_Id ;
            public int Sample_Id
            {
                get
                {
                    return _Sample_Id;
                }
                set
                {
                    _Sample_Id = value;
                }
            }
            /// <summary>
            /// ���������� ����
            /// </summary>
            private Double _Distance ;
            public Double Distance
            {
                get { return _Distance; }
                set { _Distance = value; }
            }
            /// <summary>
            /// ����� ��������
            /// </summary>
            private string _TimeMove = "";
            public string TimeMove
            {
                get { return _TimeMove; }
                set { _TimeMove = value; }
            }
            public string TimeMoveProc
            {
                get 
                {
                    return _TimeMove + TimePersent(_TimeMove,_TimeFactTotal) ; 
                }
            }
            /// <summary>
            /// ����� �������
            /// </summary>
            private string _TimeStop = "";
            public string TimeStop
            {
                get { return _TimeStop; }
                set { _TimeStop = value; }
            }
            public string TimeStopProc
            {
                get
                {
                    return _TimeStop + TimePersent(_TimeStop, _TimeFactTotal);
                }
            }
            /// <summary>
            /// ������� ��������
            /// </summary>
            private Double _SpeedAvg ;
            public Double SpeedAvg
            {
                get { return _SpeedAvg; }
                set { _SpeedAvg = value; }
            }
            /// <summary>
            /// ����� ����� �� �������� (����)
            /// </summary>
            private string _TimePlanTotal = "";
            public string TimePlanTotal
            {
                get { return _TimePlanTotal; }
                set { _TimePlanTotal = value; }
            }
            /// <summary>
            /// ����� ����� �� �������� (����)
            /// </summary>
            private string _TimeFactTotal = "";
            public string TimeFactTotal
            {
                get { return _TimeFactTotal; }
                set { _TimeFactTotal = value; }
            }
            /// <summary>
            /// ���������� �� �������
            /// </summary>
            private string _Deviation = "";
            public string Deviation
            {
                get { return _Deviation; }
                set { _Deviation = value; }
            }
            /// <summary>
            /// ���������� �� ������� ��������
            /// </summary>
            private string _DeviationAr = "";
            public string DeviationAr
            {
                get { return _DeviationAr; }
                set { _DeviationAr = value; }
            }
            private double _DistanceWithLogicSensor;
            /// <summary>
            /// ���� � ���������� ���������� ���������
            /// </summary>
            public double DistanceWithLogicSensor
            {
                get { return _DistanceWithLogicSensor; }
                set { _DistanceWithLogicSensor = value; }
            }
            #region FuelDUT
            

            /// <summary>
            /// ��� ������� � ������
            /// </summary>
            private double _FuelStart ;
            public double FuelStart
            {
                get { return _FuelStart; }
                set { _FuelStart = value; }
            }
            /// <summary>
            /// ��� ������� � �����
            /// </summary>
            private double _FuelEnd;
            public double FuelEnd
            {
                get { return _FuelEnd; }
                set { _FuelEnd = value; }
            }
            /// <summary>
            /// ��� ���������� - ���-��
            /// </summary>
            private int _FuelAddQty;
            public int FuelAddQty
            {
                get { return _FuelAddQty; }
                set { _FuelAddQty = value; }
            }
            /// <summary>
            /// ��� ���������� - �����
            /// </summary>
            private double _FuelAdd ;
            public double FuelAdd
            {
                get { return _FuelAdd; }
                set {
                    _FuelAdd = value;
                    SetFuelExpensAndAvg();
                }
            }
            /// <summary>
            /// ��� ����� - ���-��
            /// </summary>
            private int _FuelSubQty ;
            public int FuelSubQty
            {
                get { return _FuelSubQty; }
                set { _FuelSubQty = value; }
            }
            /// <summary>
            /// ��� ����� - �����
            /// </summary>
            private double _FuelSub ;
            public double FuelSub
            {
                get { return _FuelSub; }
                set { _FuelSub = value; }
            }
            /// <summary>
            /// ��� ����� ������ �������
            /// </summary>
            private double _FuelExpens ;
            public double FuelExpens
            {
                get { return _FuelExpens; }
                set { _FuelExpens = value; }
            }
            /// <summary>
            /// ��� ������� ������,  �/100 ��
            /// </summary>
            private double _FuelExpensAvg ;
            public double FuelExpensAvg
            {
                get { return _FuelExpensAvg; }
                set { _FuelExpensAvg = value; }
            }
            #endregion
            #region FuelDRT
            /// <summary>
            /// ������ ������� � ��������, �
            /// </summary>
            double _Fuel_ExpensMove ;
            public double Fuel_ExpensMove
            {
                get { return _Fuel_ExpensMove; }
                set { _Fuel_ExpensMove = value; }
            }
            /// <summary>
            /// ������ ������� �� ��������, �
            /// </summary>
            double _Fuel_ExpensStop ;
            public double Fuel_ExpensStop
            {
                get { return _Fuel_ExpensStop; }
                set { _Fuel_ExpensStop = value; }
            }
            /// <summary>
            /// ����� ������, �
            /// </summary>
            double _Fuel_ExpensTotal ;

            public double Fuel_ExpensTotal
            {
                get { return _Fuel_ExpensTotal; }
                set { _Fuel_ExpensTotal = value; }
            }
            /// <summary>
            /// ������� ������, �/100 ��
            /// </summary>
            double _Fuel_ExpensAvg ;
            public double Fuel_ExpensAvg
            {
                get { return _Fuel_ExpensAvg; }
                set { _Fuel_ExpensAvg = value; }
            }
            #endregion
            #region ����� ������
            /// <summary>
            /// �������� ������
            /// </summary>
            private string _VehicleName = "";
            public string VehicleName
            {
                get { return _VehicleName; }
                set { _VehicleName = value; }
            }
            /// <summary>
            /// ��� ��������
            /// </summary>
            private string _SampleName = "";
            public string SampleName
            {
                get { return _SampleName; }
                set { _SampleName = value; }
            }
            /// <summary>
            /// ��� ��������
            /// </summary>
            private string _DriverName = "";
            public string DriverName
            {
                get { return _DriverName; }
                set { _DriverName = value; }
            }
            /// <summary>
            /// ��� ����������� ����� - �� ��� ���������� ������
            /// </summary>
            private short _Type;
            public short Type
            {
                get { return _Type; }
                set { _Type = value; }
            }
            #endregion
            /// <summary>
            /// ������ ����������� ��� ��������
            /// </summary>
            List<IZone> _taskZones; 
            TimeSpan tsUpdateContent = TimeSpan.Zero;
            atlantaDataSet _dsFromAlgorithm;
            atlantaDataSet.mobitelsRow m_row;
    #endregion
    #region ������������
        public TaskItem()
        {

        }
        public TaskItem(DateTime dtDoc, DateTime dtEnd, int Sample_Id, int Mobitel_Id, int Driver_Id, short Type)
            {
                _dtStart = dtDoc;
                _dtEnd = dtEnd;
                _Sample_Id = Sample_Id;
                _Mobitel_Id = Mobitel_Id;
                _Driver_Id = Driver_Id;
                _Type = Type;
            }
        public TaskItem(DateTime dtStart, DateTime dtEnd,  int Mobitel_Id, short Type,string Remark)
        {
            _dtStart = dtStart;
            _dtEnd = dtEnd;
            _Mobitel_Id = Mobitel_Id;
            _Driver_Id = DicUtilites.GetDriverId_Mobitel(Mobitel_Id);
            _Remark = Remark;
            _Type = Type;
        }
        public TaskItem(int idTask)
            {
                _ID = idTask;
                if (_ID == 0) return;
                GetDocById(_ID);
            }

    #endregion        
    #region ��������
    public override DateTime Date
    {
        get
        {
            return _dtStart;
        }
        set
        {
            if (_ID > 0)
            {
                if (_dtStart != value)
                {
                    tsUpdateContent = value.Subtract(_dtStart);
                }
            }
            _dtStart = value;
        }
    }
    public override int AddDoc()
    {
        if (_Mobitel_Id == 0) return 0;
        if (TestDemo())
        {
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                Sql =
                    String.Format(
                        "INSERT INTO rt_route (TimeStartPlan,TimeEndFact,Id_sample, Id_mobitel, Id_driver, Remark) "
                        + " VALUES (" + driverDb.ParamPrefics + "TimeStart," + driverDb.ParamPrefics +
                        "TimeEndFact,{0},{1},{2}," + driverDb.ParamPrefics + "Remark)", _Sample_Id, _Mobitel_Id,
                        _Driver_Id);
                //MySqlParameter[] parDate = new MySqlParameter[3];
                driverDb.NewSqlParameterArray(3);
                //parDate[0] = new MySqlParameter("?TimeStart", MySqlDbType.DateTime);
                driverDb.NewSqlParameter(driverDb.ParamPrefics + "TimeStart", driverDb.GettingDateTime(), 0);
                //parDate[0].Value = _dtStart;
                driverDb.SetSqlParameterValue(_dtStart, 0);
                //parDate[1] = new MySqlParameter("?Remark", MySqlDbType.String);
                driverDb.NewSqlParameter(driverDb.ParamPrefics + "Remark", driverDb.GettingString(), 1);
                //parDate[1].Value = _Remark;
                driverDb.SetSqlParameterValue(_Remark, 1);
                //parDate[2] = new MySqlParameter("?TimeEndFact", MySqlDbType.DateTime);
                driverDb.NewSqlParameter(driverDb.ParamPrefics + "TimeEndFact", driverDb.GettingDateTime(), 2);
                //parDate[2].Value = (_Mobitel_Id == 0) ? _dtStart : _dtEnd;
                driverDb.SetSqlParameterValue((_Mobitel_Id == 0) ? _dtStart : _dtEnd, 2);
                //_ID = cnMySQL.ExecuteReturnLastInsert(_sSql, parDate);
                _ID = driverDb.ExecuteReturnLastInsert(Sql, driverDb.GetSqlParameterArray, "rt_route");
                IsDocNew = true;
            }
        }
        return _ID;
    }
    public override bool UpdateDoc()
    {
        //using (ConnectMySQL cnMySQL = new ConnectMySQL())
        using (var driverDb = new DriverDb())
        {
            driverDb.ConnectDb();
            string sSQLupdate = "UPDATE rt_route SET TimeStartPlan = " + driverDb.ParamPrefics +
                                "TimeStart,TimeEndFact = " + driverDb.ParamPrefics + "TimeEndFact, Id_sample = " +
                                _Sample_Id + ", Type = " + Type
                                + ",Id_mobitel = " + _Mobitel_Id + ",Id_driver = " + _Driver_Id + ", Remark = " +
                                driverDb.ParamPrefics + "Remark" + " WHERE (ID = " + _ID + ")";
            //MySqlParameter[] parDate = new MySqlParameter[3];
            driverDb.NewSqlParameterArray(3);
            //parDate[0] = new MySqlParameter("?TimeStart", MySqlDbType.DateTime);
            driverDb.NewSqlParameter(driverDb.ParamPrefics + "TimeStart", driverDb.GettingDateTime(), 0);
            //parDate[0].Value = _dtStart;
            driverDb.SetSqlParameterValue(_dtStart, 0);
            //parDate[1] = new MySqlParameter("?Remark", MySqlDbType.String);
            driverDb.NewSqlParameter(driverDb.ParamPrefics + "Remark", driverDb.GettingString(), 1);
            //parDate[1].Value = _Remark;
            driverDb.SetSqlParameterValue(_Remark, 1);
            //parDate[2] = new MySqlParameter("?TimeEndFact", MySqlDbType.DateTime);
            driverDb.NewSqlParameter(driverDb.ParamPrefics + "TimeEndFact", driverDb.GettingDateTime(), 2);
            //parDate[2].Value = _dtEnd;
            driverDb.SetSqlParameterValue(_dtEnd, 2);
            //cnMySQL.ExecuteNonQueryCommand(sSQLupdate, parDate);
            driverDb.ExecuteNonQueryCommand(sSQLupdate, driverDb.GetSqlParameterArray);
        }
        if (tsUpdateContent.Subtract(TimeSpan.Zero).TotalSeconds != 0)
            ContentUpdateSampleTimes();
        return true;
    }
    public override bool DeleteDoc(bool bQuestionNeed)
    {
        if (DeleteDocTest())
        {
            if (bQuestionNeed)
            {
                if (DialogResult.No == XtraMessageBox.Show(Resources.RouteDeleteQuestion + " " + _ID + "?", Resources.Routers1, MessageBoxButtons.YesNo))
                    return false;
            }
            try
            {
                using (DriverDb db = new DriverDb())
                {
                    db.ConnectDb();
                    Sql = "DELETE FROM rt_route WHERE ID = " + _ID;
                    //_cnDI.ExecuteNonQueryCommand(_sSql);
                    db.ExecuteNonQueryCommand(Sql);
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }
        else
            return false;
    }
    public override bool DeleteDocTest()
    {
        return true;
    }
    public override bool DeleteDocContent()
    {
        try
        {
            //using (ConnectMySQL cnMySQL = new ConnectMySQL())
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                string sSql = "DELETE FROM rt_routet where Id_main = " + _ID;
                //cnMySQL.ExecuteNonQueryCommand(sSQL);
                driverDb.ExecuteNonQueryCommand(sSql);
            }
            ClearTotals();
            return true;
        }
        catch
        {
            return false;
        }
    }
    public override bool TestDemo()
    {
        if (GlobalVars.g_ROUTE == (int)Consts.RegimeType.Demo)
        {
            int cnt = 0;
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                //using (ConnectMySQL cnMySQL = new ConnectMySQL())
                Sql = "SELECT   COUNT(rt_route.Id) AS CC FROM rt_route";
                cnt = driverDb.GetScalarValueIntNull(Sql);
            }
            if (cnt >= Consts.ROUTE_LIMIT)
            {
                XtraMessageBox.Show(Resources.RouteDemoRestrict +  " - " + Consts.ROUTE_LIMIT.ToString() + "!", Resources.Routers1);
                return false;
            }
        }
        return true;
    }
    public override DataTable GetContent()
    {
      //  using (ConnectMySQL _cnMySQL = new ConnectMySQL())
        using( DriverDb db = new DriverDb() )
        { 
        db.ConnectDb();

        Sql = "SELECT rt_routet.Id, rt_routet.Id_zone, rt_routet.Id_event,rt_routet.DatePlan, rt_routet.DateFact,COALESCE(zones.Name,rt_routet.Location) as Location, rt_routet.Deviation, rt_routet.Remark,   rt_routet.Id_main, rt_routet.Distance,CONCAT(rt_events.Name,' ', zones.Name) as P,rt_events.Name as NameEvent"
            + " FROM rt_routet "
            + " LEFT OUTER JOIN  rt_events ON rt_routet.Id_event = rt_events.Id "
            + " LEFT OUTER JOIN  zones ON rt_routet.Id_zone = zones.Zone_ID"
            + " WHERE  rt_routet.Id_main = " + _ID + " ORDER BY rt_routet.DatePlan,rt_routet.Id";
            //DataTable dtContent =  _cnMySQL.GetDataTable(_sSql);
            DataTable dtContent = db.GetDataTable( Sql );
            DataColumn dc = new DataColumn("DistanceCollect", typeof(double));
            dc.DefaultValue = 0;
            dtContent.Columns.Add(dc);
            if (dtContent.Rows.Count > 0)
            {

                double dbDistColl = 0;
                double dbDist = 0;
                for (int i = 0; i < dtContent.Rows.Count; i++)
                {
                    dbDist = (double)dtContent.Rows[i]["Distance"];
                    if (dbDist > 0)
                    {
                        dbDistColl = dbDistColl + dbDist;

                    }
                    dtContent.Rows[i]["DistanceCollect"] = dbDistColl;
                }
            }
            return dtContent;
        }
    }
    public DataTable GetStops()
        {
            //using (ConnectMySQL cnMySQL = new ConnectMySQL())
            using( DriverDb db = new DriverDb() )
            { 
        db.ConnectDb();
            
                Sql = " SELECT   rt_route_stops.* FROM  rt_route_stops "
                + " WHERE   rt_route_stops.Id_main = " + _ID  + " ORDER BY rt_route_stops.InitialTime";
                //return cnMySQL.GetDataTable(_sSql);
                return db.GetDataTable( Sql );
            }
        }
    public DataTable GetSensors()
    {
        //using (ConnectMySQL cnMySQL = new ConnectMySQL())
        using( DriverDb db = new DriverDb() )
        { 
        db.ConnectDb();
        
            Sql  = " SELECT   rt_route_sensors.* FROM  rt_route_sensors "
            + " WHERE   rt_route_sensors.Id_main = " + _ID + " ORDER BY rt_route_sensors.EventTime";
            //return cnMySQL.GetDataTable(_sSql);
            return db.GetDataTable( Sql );
        }
    }
    public DataTable GetFuelDUT()
    {
        //using (ConnectMySQL cnMySQL = new ConnectMySQL())
        using( DriverDb db = new DriverDb() )
        { 
        db.ConnectDb();
        
            Sql = " SELECT   rt_route_fuel.* FROM  rt_route_fuel "
            + " WHERE   rt_route_fuel.Id_main = " + _ID + " ORDER BY rt_route_fuel.time_";
            //return cnMySQL.GetDataTable(_sSql);
            return db.GetDataTable( Sql );
        }
    }
    public int SetFuelDUT(int RecordID,DateTime TimeAction,double ValueHandle, string Location)
    {
        DriverDb db = new DriverDb();
        //MySqlParameter[] parDate = new MySqlParameter[3];
        db.NewSqlParameterArray(3);
        //parDate[0] = new MySqlParameter("?time_", MySqlDbType.DateTime);
        db.NewSqlParameter(db.ParamPrefics + "time_", db.GettingDateTime(), 0);
        //parDate[0].Value = TimeAction;
        db.SetSqlParameterValue( TimeAction, 0 );
        //parDate[1] = new MySqlParameter("?Location", MySqlDbType.String);
        db.NewSqlParameter( db.ParamPrefics + "Location", db.GettingString(), 1);
        //parDate[1].Value = Location;
        db.SetSqlParameterValue(Location, 1);
        //parDate[2] = new MySqlParameter("?dValueHandle", MySqlDbType.Double);
        db.NewSqlParameter( db.ParamPrefics + "dValueHandle", db.GettingDouble(), 2);
        //parDate[2].Value = ValueHandle;
        db.SetSqlParameterValue(ValueHandle, 2);

        if (RecordID > 0)
        {
            //using (ConnectMySQL cnMySQL = new ConnectMySQL())
            using( DriverDb drDb = new DriverDb() )
            { 
            drDb.ConnectDb();

            Sql = "UPDATE rt_route_fuel SET Location = " + db.ParamPrefics + "Location, "
                + "time_ = " + db.ParamPrefics + "time_ , dValueHandle = " + db.ParamPrefics + "dValueHandle WHERE (ID = " + RecordID + ")";
                //cnMySQL.ExecuteNonQueryCommand(_sSql, parDate);
                drDb.ExecuteNonQueryCommand(Sql, db.GetSqlParameterArray);
                return RecordID;
            }
        }
        else
        {
            //using (ConnectMySQL cnMySQL = new ConnectMySQL())
            using( DriverDb dbDr = new DriverDb() )
            { 
            dbDr.ConnectDb();
            
                Sql = "INSERT INTO rt_route_fuel (Id_main ,Location, time_,dValueHandle)"
                + " VALUES (" + _ID +",?Location,?time_,?dValueHandle)";
                //return cnMySQL.ExecuteReturnLastInsert(_sSql, parDate);
                return dbDr.ExecuteReturnLastInsert( Sql, db.GetSqlParameterArray, "rt_route_fuel" );
            }
        }
    }
    /// <summary>
    /// ������ ��������� ���������� ����� ���������
    /// </summary>
    public override void UpdateDocTotals()
    {
        string sSQLselect = "SELECT SEC_TO_TIME(SUM(TIME_TO_SEC(rt_route_stops.`Interval`))) AS StopsSUM  FROM  rt_route_stops   WHERE  rt_route_stops.Id_main = " + _ID;
        //TimeSpan tsTimeStop;
        //using (ConnectMySQL cnMySQL = new ConnectMySQL())
        DriverDb db = new DriverDb();
        db.ConnectDb();
        {
            //TimeSpan tsTimeStop = (TimeSpan)cnMySQL.GetScalarValueTimeSpanNull(sSQLselect);
            TimeSpan tsTimeStop = (TimeSpan)db.GetScalarValueTimeSpanNull( sSQLselect );

            TimeSpan tsFact = _dtFactEnd.Subtract(_dtFactStart);
            TimeSpan tsTimeMove = tsFact.Subtract(tsTimeStop);
            _SpeedAvg = (tsTimeMove.TotalSeconds == 0 ? 0 : Math.Round(_Distance / (tsTimeMove.TotalSeconds / 3600), 2));
            //atlantaDataSet.dataviewRow[] dataRows = (atlantaDataSet.dataviewRow[])_dsAtlanta.dataview.Select("", "time ASC");

            SetPointsIntervalMax(GpsDatasDocItem);
            SetPointsValidity(GpsDatasDocItem);

            _TimeFactTotal = tsFact.ToString();
            _TimeMove = tsTimeMove.ToString();
            _TimeStop = tsTimeStop.ToString();
            if (_Sample_Id != 0)
            {
                TimeSpan tsPlan = _dtPlanEnd.Subtract(_dtPlanStart);
                TimeSpan tsDev = tsFact.Subtract(tsPlan);
                TimeSpan tsDevAr = _dtFactEnd.Subtract(_dtPlanEnd);
                _TimePlanTotal = tsPlan.ToString();
                _Deviation = tsDev.ToString();
                _DeviationAr = tsDevAr.ToString();
            }
            else
            {
                _TimePlanTotal = "";
                _Deviation = "";
                _DeviationAr = "";
            }
            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                Sql = string.Format(@"UPDATE rt_route Set Distance = ?Distance
           ,TimeFactTotal = ?TimeFactTotal
           ,TimeMove = ?TimeMove
           ,TimeStop = ?TimeStop
           ,SpeedAvg = ?SpeedAvg
           ,PointsFact = {0}
           ,PointsCalc = {1}
           ,PointsValidity = {2}
           ,Type = {3}
           ,PointsIntervalMax = ?PointsIntervalMax
           ,TimeEndFact = ?TimeEndFact
           ,TimePlanTotal = ?TimePlanTotal
           ,Deviation = ?Deviation
           ,DeviationAr = ?DeviationAr
            WHERE rt_route.Id = {4}", _PointsFact, _PointsCalc, _PointsValidity, _Type, _ID);
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                Sql = string.Format( @"UPDATE rt_route Set Distance = @Distance
           ,TimeFactTotal = @TimeFactTotal
           ,TimeMove = @TimeMove
           ,TimeStop = @TimeStop
           ,SpeedAvg = @SpeedAvg
           ,PointsFact = {0}
           ,PointsCalc = {1}
           ,PointsValidity = {2}
           ,Type = {3}
           ,PointsIntervalMax = @PointsIntervalMax
           ,TimeEndFact = @TimeEndFact
           ,TimePlanTotal = @TimePlanTotal
           ,Deviation = @Deviation
           ,DeviationAr = @DeviationAr
            WHERE rt_route.Id = {4}", _PointsFact, _PointsCalc, _PointsValidity, _Type, _ID );
            }
            //MySqlParameter[] parDate = new MySqlParameter[10];
            db.NewSqlParameterArray(10);
            //parDate[0] = new MySqlParameter("?Distance", _Distance);
            db.NewSqlParameter( db.ParamPrefics + "Distance", _Distance, 0 );
            //parDate[1] = new MySqlParameter("?SpeedAvg", _SpeedAvg);
            db.NewSqlParameter( db.ParamPrefics + "SpeedAvg", _SpeedAvg, 1 );
            //parDate[2] = new MySqlParameter("?TimeEndFact", _dtFactEnd);
            db.NewSqlParameter( db.ParamPrefics + "TimeEndFact", _dtFactEnd, 2 );
            //parDate[3] = new MySqlParameter("?TimeFactTotal", _TimeFactTotal);
            db.NewSqlParameter( db.ParamPrefics + "TimeFactTotal", _TimeFactTotal, 3 );
            //parDate[4] = new MySqlParameter("?TimeMove", _TimeMove);
            db.NewSqlParameter( db.ParamPrefics + "TimeMove", _TimeMove, 4 );
            //parDate[5] = new MySqlParameter("?TimeStop", _TimeStop);
            db.NewSqlParameter( db.ParamPrefics + "TimeStop", _TimeStop, 5);
            //parDate[6] = new MySqlParameter("?TimePlanTotal", _TimePlanTotal);
            db.NewSqlParameter( db.ParamPrefics + "TimePlanTotal", _TimePlanTotal, 6);
            //parDate[7] = new MySqlParameter(db.ParamPrefics + "Deviation", _Deviation);
            db.NewSqlParameter( db.ParamPrefics + "Deviation", _Deviation, 7);
            //parDate[8] = new MySqlParameter("?DeviationAr", _DeviationAr);
            db.NewSqlParameter( db.ParamPrefics + "DeviationAr", _DeviationAr, 8);
            //parDate[9] = new MySqlParameter("?PointsIntervalMax", _PointsIntervalMax);
            db.NewSqlParameter( db.ParamPrefics + "PointsIntervalMax", _PointsIntervalMax, 9);
            //cnMySQL.ExecuteNonQueryCommand(_sSql, parDate);
            db.ExecuteNonQueryCommand( Sql, db.GetSqlParameterArray );
            // ���������������� ����������� ���� � � �������
            Sql = "UPDATE rt_sample Set Distance = " + _Distance.ToString().Replace(",", ".")
                + " WHERE rt_sample.Id = " + _Sample_Id;
            //cnMySQL.ExecuteNonQueryCommand(_sSql);
            db.ExecuteNonQueryCommand( Sql );
        }
        db.CloseDbConnection();
    }
    public override sealed bool GetDocById(int id)
    {
        try
        {
            using (var driverDb = new DriverDb())
            {
                driverDb.ConnectDb();
                Sql =
                    string.Format(@"SELECT  rt_sample.Name as SampleName, driver.Family as DriverName, {0} as VehicleName, vehicle.MakeCar,vehicle.Team_id ,rt_route.*
                FROM  rt_route 
                LEFT OUTER JOIN  rt_sample ON rt_route.Id_sample = rt_sample.Id
                LEFT OUTER JOIN  driver ON rt_route.Id_driver = driver.id
                LEFT OUTER JOIN  vehicle ON rt_route.Id_mobitel = vehicle.Mobitel_id
                WHERE rt_route.Id = {1}", AgroQuery.SqlVehicleIdent , id);
                //MySqlDataReader dr = _cnDI.GetDataReader(_sSql);
                //using (DriverDb driverDb = new DriverDb())
                //{
                //    driverDb.ConnectDb();
                driverDb.GetDataReader(Sql);
                if (driverDb.Read())
                {
                    TimeSpanConverter tsc = new TimeSpanConverter();
                    //_Remark = (string)TotUtilites.NDBNullReader(dr, "Remark", "");
                    _Remark = (string) TotUtilites.NdbNullReader(driverDb, "Remark", "");
                    //_dtStart = dr.GetDateTime("TimeStartPlan");
                    _dtStart = driverDb.GetDateTime("TimeStartPlan");
                    // ��������� ���������� ��� �������
                    //_dtEnd = (DateTime)TotUtilites.NDBNullReader(dr, "TimeEndFact", _dtEnd);
                    _dtEnd = (DateTime) TotUtilites.NdbNullReader(driverDb, "TimeEndFact", _dtEnd);
                    //_Team_Id = (int)TotUtilites.NDBNullReader(dr, "Team_id", 0);// dr.GetInt32("Team_id");
                    _Team_Id = (int) TotUtilites.NdbNullReader(driverDb, "Team_id", 0); // dr.GetInt32("Team_id");
                    //_Type = (short)dr.GetUInt16("Type");
                    _Type = (short) driverDb.GetUInt16("Type");
                    //_Mobitel_Id = (int)TotUtilites.NDBNullReader(dr, "Id_mobitel", 0);// dr.GetInt32("Id_mobitel");
                    _Mobitel_Id = (int) TotUtilites.NdbNullReader(driverDb, "Id_mobitel", 0);
                    //_Sample_Id = dr.GetInt32("Id_sample");
                    _Sample_Id = driverDb.GetInt32("Id_sample");
                    //_Driver_Id = dr.GetInt32( "Id_driver" );
                    _Driver_Id = driverDb.GetInt32("Id_driver");
                    // _Distance = dr.GetDouble( "Distance" );
                    _Distance = driverDb.GetDouble("Distance");
                    //_TimeMove = TotUtilites.NDBNullReader(dr, "TimeMove", "").ToString();
                    _TimeMove = TotUtilites.NdbNullReader(driverDb, "TimeMove", "").ToString();
                    //_TimeStop = TotUtilites.NDBNullReader(dr, "TimeStop", "").ToString();
                    _TimeStop = TotUtilites.NdbNullReader(driverDb, "TimeStop", "").ToString();
                    //_SpeedAvg = dr.GetDouble( "SpeedAvg" );
                    _SpeedAvg = driverDb.GetDouble("SpeedAvg");
                    //_TimePlanTotal = TotUtilites.NDBNullReader(dr, "TimePlanTotal", "").ToString();
                    _TimePlanTotal = TotUtilites.NdbNullReader(driverDb, "TimePlanTotal", "").ToString();
                    //_TimeFactTotal = TotUtilites.NDBNullReader( dr, "TimeFactTotal", "" ).ToString();
                    _TimeFactTotal = TotUtilites.NdbNullReader(driverDb, "TimeFactTotal", "").ToString();
                    //_Deviation = TotUtilites.NDBNullReader(dr, "Deviation", "").ToString();
                    _Deviation = TotUtilites.NdbNullReader(driverDb, "Deviation", "").ToString();
                    //_DeviationAr = TotUtilites.NDBNullReader(dr, "DeviationAr", "").ToString();
                    _DeviationAr = TotUtilites.NdbNullReader(driverDb, "DeviationAr", "").ToString();
                    //_FuelStart = dr.GetDouble( "FuelStart" );
                    _FuelStart = driverDb.GetDouble("FuelStart");
                    //_FuelEnd = dr.GetDouble( "FuelEnd" );
                    _FuelEnd = driverDb.GetDouble("FuelEnd");
                    //_FuelAddQty = dr.GetInt32( "FuelAddQty" );
                    _FuelAddQty = driverDb.GetInt32("FuelAddQty");
                    //_FuelSubQty = dr.GetInt32( "FuelSubQty" );
                    _FuelSubQty = driverDb.GetInt32("FuelSubQty");
                    //_FuelSub = dr.GetDouble( "FuelSub" );
                    _FuelSub = driverDb.GetDouble("FuelSub");
                    //_FuelAdd = dr.GetDouble( "FuelAdd" );
                    _FuelAdd = driverDb.GetDouble("FuelAdd");
                    //_FuelExpensAvg = dr.GetDouble( "FuelExpensAvg" );
                    _FuelExpensAvg = driverDb.GetDouble("FuelExpensAvg");
                    //_FuelExpens = dr.GetDouble( "FuelExpens" );
                    _FuelExpens = driverDb.GetDouble("FuelExpens");
                    //_Fuel_ExpensMove = dr.GetDouble( "Fuel_ExpensMove" );
                    _Fuel_ExpensMove = driverDb.GetDouble("Fuel_ExpensMove");
                    //_Fuel_ExpensStop = dr.GetDouble( "Fuel_ExpensStop" );
                    _Fuel_ExpensStop = driverDb.GetDouble("Fuel_ExpensStop");
                    //_Fuel_ExpensTotal = dr.GetDouble( "Fuel_ExpensTotal" );
                    _Fuel_ExpensTotal = driverDb.GetDouble("Fuel_ExpensTotal");
                    //_Fuel_ExpensAvg = dr.GetDouble( "Fuel_ExpensAvg" );
                    _Fuel_ExpensAvg = driverDb.GetDouble("Fuel_ExpensAvg");
                    //_PointsValidity = dr.GetInt32( "PointsValidity" );
                    _PointsValidity = driverDb.GetInt32("PointsValidity");
                    //_PointsCalc = dr.GetInt32( "PointsCalc" );
                    _PointsCalc = driverDb.GetInt32("PointsCalc");
                    //_PointsFact = dr.GetInt32( "PointsFact" );
                    _PointsFact = driverDb.GetInt32("PointsFact");
                    //_PointsIntervalMax = TotUtilites.NDBNullReader(dr, "PointsIntervalMax", "").ToString();
                    _PointsIntervalMax = TotUtilites.NdbNullReader(driverDb, "PointsIntervalMax", "").ToString();
                    //_VehicleName = TotUtilites.NDBNullReader(dr, "VehicleName", "").ToString();
                    _VehicleName = TotUtilites.NdbNullReader(driverDb, "VehicleName", "").ToString();
                    // _SampleName = TotUtilites.NDBNullReader(dr, "SampleName", "").ToString();
                    _SampleName = TotUtilites.NdbNullReader(driverDb, "SampleName", "").ToString();
                    // _DriverName = TotUtilites.NDBNullReader(dr, "DriverName", "").ToString();
                    _DriverName = TotUtilites.NdbNullReader(driverDb, "DriverName", "").ToString();
                    //_DistanceWithLogicSensor = dr.GetDouble( "DistLogicSensor" );
                    _DistanceWithLogicSensor = driverDb.GetDouble("DistLogicSensor");
                }
                driverDb.CloseDataReader();
            }
            //dr.Close();
                
            return true;
        }
        catch
        {
            return false;
        }
    }
    #endregion
    #region �������� ����������
    /// <summary>
    /// �������� ����������� ����� �� ������� 
    /// </summary>
    /// <returns></returns>
    public int ContentCreateFromSample()
    {
        SetStatusEvent(Resources.RouteCreateSample);
        if ((_Sample_Id == 0) || (_ID == 0)) return 0;
        ContentDelete();
        Sql = "SELECT  rt_samplet.Id_event, rt_samplet.Id_zone, rt_samplet.TimeRel,rt_samplet.TimeAbs,zones.Name"
        + " FROM zones INNER JOIN rt_samplet ON  zones.Zone_ID = rt_samplet.Id_zone WHERE   rt_samplet.Id_main = " + _Sample_Id
        + " ORDER BY rt_samplet.Position";
        var db = new DriverDb();
        db.ConnectDb();
        //MySqlDataReader dr = _cnDI.GetDataReader(_sSql);
        db.GetDataReader(Sql);
        DateTime dtPlan = _dtStart;
        var tsc = new TimeSpanConverter();
        int iRecords = 0;
        //using (ConnectMySQL cnMySQLins = new ConnectMySQL())
        var driverDb = new DriverDb();
        driverDb.ConnectDb();
        {
            bool first = true;
            while (db.Read())
            {
                string sTime;
                if (first)
                {
                    //sTime = dr.GetString(dr.GetOrdinal("TimeAbs"));
                    sTime = db.GetString(db.GetOrdinal("TimeAbs"));
                    first = false;
                }
                else
                {
                    sTime = db.GetString(db.GetOrdinal("TimeRel"));
                }
                var tsWork = (TimeSpan)tsc.ConvertFromInvariantString(sTime);
                dtPlan = dtPlan.AddMinutes(tsWork.TotalMinutes);
                int ordinal = db.GetOrdinal("Id_event");
                string sSqLinsert = String.Format("INSERT INTO rt_routet (Id_main, Id_zone, Id_event, DatePlan, Location) "
                + " VALUES ({0},{1},{2}," + driverDb.ParamPrefics + "DatePlan," + driverDb.ParamPrefics + "Location)", _ID, db.GetInt32( "Id_zone" ),
                db.GetInt32(ordinal));
                
                //MySqlParameter[] parDate = new MySqlParameter[2];
                //parDate[0] = new MySqlParameter("?DatePlan", MySqlDbType.DateTime);
                //parDate[0].Value = dtPlan;
                //parDate[1] = new MySqlParameter("?Location", MySqlDbType.String);
                //parDate[1].Value = dr.GetString("Name");
                //cnMySQLins.ExecuteNonQueryCommand(sSQLinsert, parDate);

                driverDb.NewSqlParameterArray(2);
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "DatePlan", dtPlan);
                driverDb.SetNewSqlParameter(driverDb.ParamPrefics + "Location", db.GetString("Name"));
                driverDb.ExecuteNonQueryCommand(sSqLinsert, driverDb.GetSqlParameterArray);
                iRecords++;
            } // while
        } // using
        driverDb.CloseDataReader();
        driverDb.CloseDbConnection();
        db.CloseDbConnection(); 
        return iRecords;
    } // ContentCreateFromSample
    
    /// <summary>
    /// ��������� ��������� ����������� ������ � �������� 
    /// </summary>
    /// <returns></returns>
    public bool ContentAutoFill()
    {
        if (_Type == (int)Consts.RouteType.Location)
        {
            return ContentAutoFillFactLocation();
        }
        else
        {
            return ContentAutoFillFact();
        }

    }
    /// <summary>
    /// ��������� ����������� ������ � �������� (��� �������� "�� ����������� �����")
    /// </summary>
    /// <returns></returns>
    public bool ContentAutoFillFact()
    {
        if (!TestData()) return false;
        //TaskZones = new List<int>();
        //TaskZoneNames = new List<string>();
        if (!GetZoneTask())
        {
            XtraMessageBox.Show(Resources.CheckZonesAbsence, Resources.Routers1); 
            ContentDataSetReturnClear();
            return false;
        }
        m_row = (LocalCache.atlantaDataSet.mobitelsRow)_dsAtlanta.mobitels.FindByMobitel_ID(_Mobitel_Id);
        m_row.Check = true;
        SetStatusEvent(Resources.WorkZonesDetect);
        if (!ContentAnalizZonesCross(m_row))
        {
            SetStatusEvent(Resources.CheckZonesAbsenceVisits);
            XtraMessageBox.Show(Resources.RouteNCreate, Resources.Routers1);
            ContentDataSetReturnClear();
            return false;
        }
        if (_ID == 0) AddDoc();
        if (_ID == 0) return false;
        // �������� �� �����
        if (_Sample_Id == 0 & !IsExistPlanInEveryRecord())
        {
            SetStatusEvent(Resources.RouteListCreate);
            if (!ContentCreateFromFact())
            {
                if (IsDocNew) DeleteDoc(false);
                ContentDataSetReturnClear();
                return false;
            }
        }
        //�������� �� �����
        else
        {
            if (!ContentPlanFactCompare())
            {
                ContentDataSetReturnClear();
                return false;
            }
        }
        SetStatusEvent(Resources.DataFiter);
        ContentClearNoRouteData();
        SetDistance();
        SetDistanceWithLogicSensor(); 
        //-----------------------------------------------------------------------
        SetStatusEvent(Resources.MovingParamsCalc);
        ContentFindStops(m_row);
        ContentFindFuelDUT(m_row);
        ContentFindFuelDRT(m_row);
        ContentFindSensors(m_row);
        //-----------------------------------------------------------------------
        SetStatusEvent(Resources.RouteHeaderSave);
        UpdateDocTotals();
        ContentDataSetReturnClear();
        return true;
    }
    /// <summary>
    /// ��������� ����������� ������ � �������� (��� �������� "�� ���������� �������")
    /// </summary>
    /// <returns></returns>
    public bool ContentAutoFillFactLocation()
    {
        if (!TestData()) return false;
        m_row = (LocalCache.atlantaDataSet.mobitelsRow)_dsAtlanta.mobitels.FindByMobitel_ID(_Mobitel_Id);
        m_row.Check = true;
        SetStatusEvent(Resources.RouteListCreate);
        if (_ID == 0) AddDoc();
        if (_ID == 0) return false;
        if (!ContentCreateFromLocation())
        {
            if (IsDocNew) DeleteDoc(false);
            ContentDataSetReturnClear();
            return false;
        }
        SetStatusEvent(Resources.DataFiter);
        ContentClearNoRouteData();
        SetDistance();
        SetDistanceWithLogicSensor(); 
        //-----------------------------------------------------------------------
        SetStatusEvent(Resources.MovingParamsCalc);
        ContentFindStops(m_row);
        ContentFindFuelDUT(m_row);
        ContentFindFuelDRT(m_row);
        ContentFindSensors(m_row);
        //-----------------------------------------------------------------------
        SetStatusEvent(Resources.RouteHeaderSave);
        UpdateDocTotals();
        ContentDataSetReturnClear();
        return true;
    }

        private Boolean  TestData()
        {
            if (_Mobitel_Id == 0)
            {
                XtraMessageBox.Show(Resources.CarNShow, Resources.Routers1);
                ContentDataSetReturnClear();
                return false;
            }
            int iRecords = ContentDataSetCreate();
            if (iRecords == 0)
            {
                XtraMessageBox.Show(Resources.DataInitialAbsence, Resources.Routers1);
                ContentDataSetReturnClear();
                return false;
            }
            return true;
        }
    /// <summary>
    /// �������� �������� �� ��������� ����������� ���������� �������
    /// </summary>
    /// <returns></returns>
    private bool ContentCreateFromLocation()
    {
        if (Algorithm.GeoLocator == null) return false;
        SetStatusEvent(Resources.SettlementSearch);
        // �������� ����� ���������� ��������
        if (!ContentDelete()) return false;
        double dbPath = 0;
        RouteSetItem rsi = new RouteSetItem();
        // ����� � �������.���� ������ ���� � �� �� ����� �����������
        int iLimitMinutes = rsi.RouteDelayForIdent;
        // ������ ������ � ���� GDB � ������
        int iRadius = rsi.RouteRadius;
        // ����� ������ � ��
        //DateTime dtLocationEntry = ((atlantaDataSet.dataviewRow)_dsAtlanta.dataview.Rows[0]).time;
        DateTime dtLocationEntry = GpsDatasDocItem[0].Time;
        // ���������� � �� ����
        double dbPathLocationEntry = 0;
        // ���� ������ � ��
        bool InLocation = false;
        // �������� ��
        string sInLocation = "";
        // ���� ������ ������ � ��
        bool InLocationEntryWrite = false;
        // ������ ����� ����������� ��� �������� �������
        bool bFirst = true;
        // ;������� ���������� �����
        string sLoc = "";
        int iRecords = 0;
        int iCnt = 0;
        _dtFactStart = DateTime.MinValue;
        _dtFactEnd = DateTime.MinValue;
        SetProgressBar(GpsDatasDocItem.Length) ;
        Application.DoEvents();
        //sLoc = Algorithm.GeoLocator.GetLocationInfo(new PointLatLng(((atlantaDataSet.dataviewRow)_dsAtlanta.dataview.Rows[0]).Lat ,
        //    ((atlantaDataSet.dataviewRow)_dsAtlanta.dataview.Rows[0]).Lon));
        foreach (GpsData gpsData in GpsDatasDocItem)
        {
            dbPath = dbPath + gpsData.Dist;
            if (bFirst)
            {
                sLoc = Algorithm.GeoLocator.GetLocationInfoRadius(gpsData.LatLng, iRadius);
            }
            // �������� ������ - ������ ����� �� ������ � �������� 
            else if (gpsData.Dist > 0)
            {
                //������ �� �����. ���� � ��,�� ������� �������� ������ ����� ����� ���������� ��������
                //���� � ��� ������ ������ iLimitMinutes - ������� ������� ������ ������������
                if (InLocation)
                { 
                    if ((gpsData.Time.Subtract(dtLocationEntry).TotalMinutes) >= iLimitMinutes)
                    {
                        sLoc = Algorithm.GeoLocator.GetLocationInfoRadius(gpsData.LatLng, iRadius);
                    }
                }
                else
                    sLoc = Algorithm.GeoLocator.GetLocationInfoRadius(gpsData.LatLng, iRadius);
            }
            iCnt++;
            if (iCnt % 100 == 0) SetProgressBar(iCnt);
            if (sLoc.Length > 0 && ((sLoc == sInLocation) || (sInLocation.Length == 0)))
            {
                if (!InLocation)
                {
                    InLocation = true;
                    InLocationEntryWrite = false;
                    dtLocationEntry = gpsData.Time;
                    dbPathLocationEntry = dbPath;
                    sInLocation = sLoc;
                }
                //���� � �� ������ iLimitMinutes - ������� ������� ������ ������������
                else if (!InLocationEntryWrite)
                {
                    if (((gpsData.Time.Subtract(dtLocationEntry).TotalMinutes) >= iLimitMinutes) || bFirst)
                    {
                        ContentSetFact((int)Consts.RouteEvent.Entry, dtLocationEntry, dbPathLocationEntry, sInLocation);
                        SetStatusEvent(Resources.Entry + " " + sInLocation);
                        dbPath = dbPath - dbPathLocationEntry;
                        InLocationEntryWrite = true;
                        iRecords++;
                        if (_dtFactStart == DateTime.MinValue)
                        {
                            _dtFactStart = dtLocationEntry;
                        }
                        _dtFactEnd = dtLocationEntry;
                    }
                    bFirst = false;
                }
            }
            else
            {
                // ����� �� �� � ����� �������
                if (InLocation && InLocationEntryWrite)
                {
                    InLocation = false;
                    InLocationEntryWrite = false;
                    ContentSetFact((int)Consts.RouteEvent.Exit, gpsData.Time, dbPath, sInLocation);
                    SetStatusEvent(Resources.Exit  + " " + sInLocation);
                    sInLocation = "";
                    dbPath = 0;
                    iRecords++;
                    if (_dtFactStart == DateTime.MinValue)
                    {
                        _dtFactStart = gpsData.Time;
                    }
                    _dtFactEnd = gpsData.Time;
                }
                else
                {
                    InLocation = false;
                    InLocationEntryWrite = false;
                    sInLocation = "";
                }

            }
            Application.DoEvents();
        }
        if (InLocation && !InLocationEntryWrite)
        {
            ContentSetFact((int)Consts.RouteEvent.Entry, dtLocationEntry, dbPath, sInLocation);
            SetStatusEvent(Resources.Entry  + " " + sInLocation);
        }
        SetProgressBar(Consts.PROGRESS_BAR_STOP);
        if (iRecords > 0)
            return true;
        else
            return false;
    }
    /// <summary>
    /// �������� �������� �� ��������� ����������� ������ ����������� ����������� ���
    /// </summary>
    /// <returns></returns>
    private bool ContentCreateFromFact()
    {
        // �������� ����� ���������� ��������
        if (!ContentDelete()) return false;
        SetStatusEvent(Resources.CheckZonesCrossSearch);
        CrossingInfo[] crossInfoArray = CrossZones.Instance.GetCrossInfoArray(_Mobitel_Id);
        bool bFirst = true;
        for (int i = 0; i < crossInfoArray.Length; i++)
        {
            int Event = 0;
            CrossingInfo cz_row = crossInfoArray[i];
            switch (cz_row.PointLocationType)
            {
                case PointLocationType.CrossIn:
                    {
                        Event = (int)Consts.RouteEvent.Entry;
                        break;
                    }
                case PointLocationType.BeginIn:
                    {
                        Event = (int)Consts.RouteEvent.Entry;
                        break;
                    }
                case PointLocationType.CrossOut:
                    {
                        Event = (int)Consts.RouteEvent.Exit;
                        break;
                    }
            }

            if ((bFirst) && (Event > 0))
            {
                _dtFactStart = cz_row.CrossingTime;
                ContentSetFact(cz_row.ZoneID, Event, cz_row.CrossingTime, 0, cz_row.ZoneName);
                bFirst = false;
            }
            else if (Event > 0)
            {
                _dtFactEnd = cz_row.CrossingTime;
                ContentSetFact(cz_row.ZoneID, Event, cz_row.CrossingTime, cz_row.Kilometrage, cz_row.ZoneName);
            }
        }
        if (bFirst)
        {
            XtraMessageBox.Show(Resources.RouteNCreate, Resources.Routers1);
            return false;
        }
        else
        {
            return true;
        }
    }
    #endregion
    #region �������������� ���������� 

            /// <summary>
            /// ���������� ��������� �������
            /// </summary>
            /// <returns></returns>
            public int ContentUpdateSampleTimes()
            {
                if (_ID == 0) return 0;
                //ConnectMySQL cnMySQL = new ConnectMySQL();
                DriverDb db = new DriverDb();
                db.ConnectDb();
                string sSQLselect = "SELECT  rt_routet.Id,rt_routet.DatePlan,rt_routet.DateFact"
                + " FROM rt_routet WHERE   rt_routet.Id_main = " + _ID
                + " ORDER BY rt_routet.Id";
                //MySqlDataReader dr = cnMySQL.GetDataReader(sSQLselect);
                db.GetDataReader(sSQLselect);
                //ConnectMySQL cnMySQLins = new ConnectMySQL();
                DriverDb driverDb = new DriverDb();
                driverDb.ConnectDb();
                
                int iRecords = 0;
                DateTime dtFact = _dtEnd;
                DateTime dtPlan = _dtEnd;
                string sSQLupdate = "";
                bool bFactPresent = false;
                //while (dr.Read())
                while(db.Read())
                {
                    //if (DateTime.TryParse(TotUtilites.NDBNullReader(dr, "DatePlan", "").ToString(), out dtPlan))
                    if( DateTime.TryParse( TotUtilites.NdbNullReader( db, "DatePlan", "" ).ToString(), out dtPlan ) )
                    {
                        dtPlan = dtPlan.AddMinutes(tsUpdateContent.TotalMinutes);
                        sSQLupdate = "UPDATE rt_routet SET DatePlan = " + db.ParamPrefics + "DatePlan";
                        if( DateTime.TryParse( TotUtilites.NdbNullReader( db, "DateFact", "" ).ToString(), out dtFact ) )
                        {
                            TimeSpan tsDeltaPlanFact = dtFact.Subtract(dtPlan);
                            sSQLupdate = sSQLupdate + ",Deviation = '" + tsDeltaPlanFact.ToString() + "'";
                            bFactPresent = true;
                        }
                        else
                            bFactPresent = false;
                        sSQLupdate = sSQLupdate + " WHERE rt_routet.Id = " + db.GetInt32("Id");
                        //MySqlParameter[] parDate = new MySqlParameter[1];
                        db.NewSqlParameterArray(1);
                        //parDate[0] = new MySqlParameter("?DatePlan", MySqlDbType.DateTime);
                        db.NewSqlParameter(db.ParamPrefics + "DatePlan", db.GettingDateTime(), 0);
                        //parDate[0].Value = dtPlan;
                        db.SetSqlParameterValue(dtPlan, 0);
                        //cnMySQLins.ExecuteNonQueryCommand(sSQLupdate, parDate);
                        driverDb.ExecuteNonQueryCommand(sSQLupdate, db.GetSqlParameterArray);
                        iRecords++;
                    }
                }
                db.CloseDataReader();
                if (bFactPresent)
                {
                    sSQLupdate = "UPDATE rt_route Set DeviationAr = '" + dtPlan.Subtract(dtFact) + "' WHERE rt_route.Id = " + _ID;
                    //cnMySQL.ExecuteNonQueryCommand(sSQLupdate);
                    db.ExecuteNonQueryCommand(sSQLupdate);
                }
                //cnMySQL.CloseMySQLConnection();
                db.CloseDbConnection();
                //cnMySQLins.CloseMySQLConnection();
                driverDb.CloseDbConnection();
                tsUpdateContent = TimeSpan.Zero;
                return iRecords;
            }
            
            /// <summary>
            /// ������ �������� ����������� ��� ��������
            /// </summary>
            /// <returns></returns>
            private bool GetZoneTask()
            {
                _taskZones = new List<IZone>(); 
                //using (ConnectMySQL cnMySQL = new ConnectMySQL())
                DriverDb db = new DriverDb();
                db.ConnectDb();
                {
                    if (_Sample_Id == 0)
                        GetZonesFromTrack();
                    else
                    {
                        string sSQLselect = "SELECT  zones.Zone_ID, zones.Name FROM rt_routet "
                        + " INNER JOIN  zones ON rt_routet.Id_zone = zones.Zone_ID"
                        + " WHERE   rt_routet.Id_main = " + _ID;
                        //MySqlDataReader dr = cnMySQL.GetDataReader(sSQLselect);
                        db.GetDataReader(sSQLselect);
                        //while (dr.Read())
                        while(db.Read())
                        {
                            int iZone = (int)TotUtilites.NdbNullReader( db, "Zone_ID", 0 );
                            if (iZone > 0)
                            {
                                IZone zone = ZonesModel.GetById(iZone);
                                if (zone != null && !_taskZones.Contains(zone)) _taskZones.Add(zone);
                            }
                        }
                        //dr.Close();
                        db.CloseDataReader();
                    }
                }
                db.CloseDbConnection();
                if (_taskZones.Count > 0)
                    return true;
                else
                    return false;
            }
        /// <summary>
        /// ����������� ������ ����������� ���, ����������� � ������� �����
        /// </summary>
            void GetZonesFromTrack()
            {
                SetStatusEvent(Resources.WorkZoneRestriction);
             _taskZones.Clear(); 
                foreach (GpsData gpsData in GpsDatasDocItem)
                {
                    foreach (IZone zone in ZonesModel.Zones)
                    {
                        if (zone.BoundContains(gpsData.LatLng))
                        {
                            if (!_taskZones.Contains(zone))
                            {
                                _taskZones.Add(zone);
                                //Console.WriteLine("{2} {0} {1}", zone.Group.Name, zone.Name, ++_cntZones);
                            }
                        }

                    }
                }
            }

            /// <summary>
            /// ������ ������������ ������� + ����������
            /// </summary>
            /// <param name="dtPlan"></param>
            /// <param name="dtFact"></param>
            /// <param name="Id"></param>
            private void ContentSetFact(DateTime dtPlan, DateTime dtFact,double dbPath, int Id)
            {
                //using (ConnectMySQL cnMySQL = new ConnectMySQL())
                DriverDb db = new DriverDb();
                db.ConnectDb();
                {
                    dtFact = new DateTime(dtFact.Year, dtFact.Month, dtFact.Day, dtFact.Hour, dtFact.Minute, 0);
                    TimeSpan tsDelta = dtFact.Subtract(dtPlan);
                    Sql = "UPDATE rt_routet SET DateFact = " + db.ParamPrefics + "DateFact,Deviation = '" + tsDelta.ToString()
                        + "',Distance = " + db.ParamPrefics + "Path WHERE rt_routet.Id = " + Id;
                    //MySqlParameter[] parDate = new MySqlParameter[2];
                    db.NewSqlParameterArray(2);
                    //parDate[0] = new MySqlParameter("?DateFact", MySqlDbType.DateTime);
                    db.NewSqlParameter( db.ParamPrefics + "DateFact", db.GettingDateTime(), 0);
                    //parDate[0].Value = dtFact;
                    db.SetSqlParameterValue(dtFact, 0);
                    //parDate[1] = new MySqlParameter("?Path", MySqlDbType.Double);
                    db.NewSqlParameter(db.ParamPrefics + "Path", db.GettingDouble(), 1);
                    //parDate[1].Value = Math.Round(dbPath, 2);
                    db.SetSqlParameterValue( Math.Round( dbPath, 2 ), 1);
                    //cnMySQL.ExecuteNonQueryCommand(_sSql, parDate);
                    db.ExecuteNonQueryCommand(Sql, db.GetSqlParameterArray);
                } // using
                db.CloseDbConnection();
            } // ContentSetFact

            private void ContentSetFact(int ZoneId, int Event, DateTime dtFact, double dbPath, string sLocation)
            {
                //using (ConnectMySQL cnMySQL = new ConnectMySQL())
                DriverDb db = new DriverDb();
                db.ConnectDb();
                {
                    dtFact = new DateTime(dtFact.Year, dtFact.Month, dtFact.Day, dtFact.Hour, dtFact.Minute, 0);
                    Sql = "INSERT INTO rt_routet (Id_main,DatePlan,DateFact ,Deviation,Id_zone,Id_event,Distance,Location) "
                            + " VALUES (" + _ID + ",NULL," + db.ParamPrefics + "DateFact,0," + ZoneId + "," + Event + "," + db.ParamPrefics + "Path," + db.ParamPrefics + "Location)";
                    //MySqlParameter[] parDate = new MySqlParameter[3];
                    db.NewSqlParameterArray(3);
                    //parDate[0] = new MySqlParameter("?DateFact", MySqlDbType.DateTime);
                    db.NewSqlParameter(db.ParamPrefics + "DateFact", db.GettingDateTime(), 0 );
                    //parDate[0].Value = dtFact;
                    db.SetSqlParameterValue(dtFact, 0);
                    //parDate[1] = new MySqlParameter("?Path", MySqlDbType.Double);
                    db.NewSqlParameter(db.ParamPrefics + "Path", db.GettingDouble(), 1 );
                    //parDate[1].Value = Math.Round(dbPath, 2);
                    db.SetSqlParameterValue( Math.Round( dbPath, 2 ), 1 );
                    //parDate[2] = new MySqlParameter("?Location", MySqlDbType.String);
                    db.NewSqlParameter(db.ParamPrefics + "Location", db.GettingString(), 2);
                    //parDate[2].Value = sLocation;
                    db.SetSqlParameterValue( sLocation, 2);
                    //cnMySQL.ExecuteNonQueryCommand(_sSql, parDate);
                    db.ExecuteNonQueryCommand(Sql, db.GetSqlParameterArray);
                }
                db.CloseDbConnection();
            }

            private void ContentSetFact(int Event, DateTime dtFact, double dbPath,string sLocation)
            {
                //using (ConnectMySQL cnMySQL = new ConnectMySQL())
                DriverDb db = new DriverDb();
                db.ConnectDb();
                {
                    dtFact = new DateTime(dtFact.Year, dtFact.Month, dtFact.Day, dtFact.Hour, dtFact.Minute, 0);
                    Sql = "INSERT INTO rt_routet (Id_main,DatePlan,DateFact ,Deviation,Id_zone,Id_event,Distance,Location) "
                            + " VALUES (" + _ID + ",NULL," + db.ParamPrefics + "DateFact,0,0," + Event + "," + db.ParamPrefics + "Path," + db.ParamPrefics + "Location)";

                    //MySqlParameter[] parDate = new MySqlParameter[3];
                    db.NewSqlParameterArray(3);
                    //parDate[0] = new MySqlParameter("?DateFact", MySqlDbType.DateTime);
                    db.NewSqlParameter(db.ParamPrefics + "DateFact", db.GettingDateTime(), 0);
                    //parDate[0].Value = dtFact;
                    db.SetSqlParameterValue(dtFact, 0);
                    //parDate[1] = new MySqlParameter("?Path", MySqlDbType.Double);
                    db.NewSqlParameter(db.ParamPrefics + "Path", db.GettingDouble(), 1);
                    //parDate[1].Value = Math.Round(dbPath, 2);
                    db.SetSqlParameterValue( Math.Round( dbPath, 2 ), 1);
                    //parDate[2] = new MySqlParameter("?Location", MySqlDbType.String);
                    db.NewSqlParameter( db.ParamPrefics + "Location", db.GettingString(), 2);
                    //parDate[2].Value = sLocation;
                    db.SetSqlParameterValue(sLocation, 2);
                    //cnMySQL.ExecuteNonQueryCommand(_sSql, parDate);
                    db.ExecuteNonQueryCommand(Sql, db.GetSqlParameterArray);
                }
                db.CloseDbConnection();
            }
            /// <summary>
            /// ����� ������������ �������� �� �������� � ��������� �� ������ � ����� ������������ �������
            /// </summary>
            private void ContentFindSensors(atlantaDataSet.mobitelsRow m_row)
            {
                SetStatusEvent(Resources.SensorEventsSearch);
                //using (ConnectMySQL cnMySQL = new ConnectMySQL())
                DriverDb db = new DriverDb();
                db.ConnectDb();
                {
                    SensorEvents se = new SensorEvents();
                    se.SelectItem(m_row);
                    se.Run();
                    string sSQLdelete = "DELETE FROM rt_route_sensors WHERE Id_main = " + _ID;
                    //cnMySQL.ExecuteNonQueryCommand(sSQLdelete);
                    db.ExecuteNonQueryCommand(sSQLdelete);
                    foreach (atlantaDataSet.SensorEventsRow se_row in _dsAtlanta.SensorEvents.Rows)
                    {
                        string sSQLinsert = "";
                        if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                        {
                            sSQLinsert = "INSERT INTO rt_route_sensors (Id_main,Location,SensorName, EventTime,Duration,Description,Speed,Distance)"
                                                + " VALUES (" + _ID +
                                                ",?Location,?SensorName,?EventTime,?Duration,?Description,?Speed,?Distance)";
                        }
                        else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                        {
                            sSQLinsert = "INSERT INTO rt_route_sensors (Id_main,Location,SensorName, EventTime,Duration,Description,Speed,Distance)"
                                + " VALUES (" + _ID + ",@Location,@SensorName,@EventTime,@Duration,@Description,@Speed,@Distance)";
                        }

                        //MySqlParameter[] parDate = new MySqlParameter[7];
                        db.NewSqlParameterArray(7);
                            //parDate[0] = new MySqlParameter("?EventTime", MySqlDbType.DateTime);
                        db.NewSqlParameter( db.ParamPrefics + "EventTime", db.GettingDateTime(), 0);
                            //parDate[0].Value = se_row.EventTime;
                        db.SetSqlParameterValue( se_row.EventTime, 0);
                            //parDate[1] = new MySqlParameter("?Duration", MySqlDbType.Time);
                        db.NewSqlParameter(db.ParamPrefics + "Duration", db.GettingTime(), 1);
                            //parDate[1].Value = se_row.Duration;
                        db.SetSqlParameterValue( se_row.Duration, 1);
                            //parDate[2] = new MySqlParameter("?Location", MySqlDbType.String);
                        db.NewSqlParameter(db.ParamPrefics + "Location", db.GettingString(), 2);
                            //parDate[2].Value = se_row.Location;
                        db.SetSqlParameterValue(se_row.Location, 2);
                            //parDate[3] = new MySqlParameter("?SensorName", MySqlDbType.String);
                        db.NewSqlParameter(db.ParamPrefics + "SensorName", db.GettingString(), 3);
                            //parDate[3].Value = se_row.SensorName;
                        db.SetSqlParameterValue(se_row.SensorName, 3);
                            //parDate[4] = new MySqlParameter("?Speed", MySqlDbType.Double);
                        db.NewSqlParameter(db.ParamPrefics + "Speed", db.GettingDouble(), 4);
                            //parDate[4].Value = Math.Round(se_row.Speed, 2);
                        db.SetSqlParameterValue( Math.Round( se_row.Speed, 2 ), 4);
                            //parDate[5] = new MySqlParameter("?Description", MySqlDbType.String);
                        db.NewSqlParameter(db.ParamPrefics + "Description", db.GettingString(), 5);
                            //parDate[5].Value = se_row.Description;
                        db.SetSqlParameterValue( se_row.Description, 5);
                            //parDate[6] = new MySqlParameter("?Distance", MySqlDbType.Double);
                        db.NewSqlParameter(db.ParamPrefics + "Distance", db.GettingDouble(), 6);
                            //parDate[6].Value = se_row.Distance;
                        db.SetSqlParameterValue( se_row.Distance, 6);
                            //cnMySQL.ExecuteNonQueryCommand(sSQLinsert, parDate);
                        db.ExecuteNonQueryCommand( sSQLinsert, db.GetSqlParameterArray );
                    } // foreach
                }
                db.CloseDbConnection();
            }

            /// <summary>
            /// ����� �������� ������ �� �������� - ���
            /// </summary>
            /// <param name="m_row"></param>
            private void ContentFindFuelDUT(atlantaDataSet.mobitelsRow m_row)
            {
                SetStatusEvent(Resources.FuelAddSubSearch);
                //using (ConnectMySQL cnMySQL = new ConnectMySQL())
                DriverDb db = new DriverDb();
                db.ConnectDb();
                {
                        Fuel fl = new Fuel(AlgorithmType.FUEL1);
                        fl.SelectItem(m_row);
                        fl.Run();
                        //������ �������� �������� 
                        Fuel.Summary _summary = fl.summary;
                        _FuelStart = Math.Round ( _summary.Before,2);
                        _FuelEnd =  Math.Round ( _summary.After,2);
                        _FuelAdd =  Math.Round ( _summary.Fueling,2);
                        _FuelSub =  Math.Round ( _summary.Discharge,2);
                        _FuelAddQty =   _summary.FuelingCount;
                        _FuelSubQty =   _summary.DischargeCount;
                        _FuelExpens = Math.Round(_summary.Total, 2);
                        _FuelExpensAvg = (_Distance == 0 ? 0 : Math.Round(_summary.Total * 100 / _Distance, 2));
                        //----------------------------------------------------------------------
                    if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                    {
                        Sql = "Update rt_route SET FuelStart = ?FuelStart"
                                + ",FuelAdd = ?FuelAdd" // ����� ����������
                                + ",FuelSub = ?FuelSub" // ����� �����
                                + ",FuelAddQty = ?FuelAddQty" // ���������� ��������
                                + ",FuelSubQty = ?FuelSubQty" // ���������� ������
                                + ",FuelEnd = ?FuelEnd"
                                + ",FuelExpens = ?FuelExpens" // ����� ������
                                + ",FuelExpensAvg  = ?FuelExpensAvg" // ������ �� 100 ��
                                + "  WHERE Id = " + _ID;
                    }
                    else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                    {
                        Sql = "Update rt_route SET FuelStart = @FuelStart"
                               + ",FuelAdd = @FuelAdd" // ����� ����������
                               + ",FuelSub = @FuelSub" // ����� �����
                               + ",FuelAddQty = @FuelAddQty" // ���������� ��������
                               + ",FuelSubQty = @FuelSubQty" // ���������� ������
                               + ",FuelEnd = @FuelEnd"
                               + ",FuelExpens = @FuelExpens" // ����� ������
                               + ",FuelExpensAvg  = @FuelExpensAvg" // ������ �� 100 ��
                               + "  WHERE Id = " + _ID;
                    }
                    //MySqlParameter[] parDate = new MySqlParameter[8];
                    db.NewSqlParameterArray(8);
                        //parDate[0] = new MySqlParameter("?FuelStart", MySqlDbType.Double);
                    db.NewSqlParameter(db.ParamPrefics + "FuelStart", db.GettingDouble(), 0);
                        //parDate[0].Value = _FuelStart;
                    db.SetSqlParameterValue(_FuelStart, 0);
                        //parDate[1] = new MySqlParameter("?FuelAdd", MySqlDbType.Double);
                    db.NewSqlParameter(db.ParamPrefics + "FuelAdd", db.GettingDouble(), 1);
                        //parDate[1].Value = _FuelAdd;
                    db.SetSqlParameterValue(_FuelAdd, 1);
                        //parDate[2] = new MySqlParameter("?FuelSub", MySqlDbType.Double);
                    db.NewSqlParameter( db.ParamPrefics + "FuelSub", db.GettingDouble(), 2 );
                        //parDate[2].Value = _FuelSub;
                    db.SetSqlParameterValue( _FuelSub, 2 );
                        //parDate[3] = new MySqlParameter("?FuelAddQty", MySqlDbType.Double);
                        db.NewSqlParameter( db.ParamPrefics + "FuelAddQty", db.GettingDouble(), 3 );
                        //parDate[3].Value = _FuelAddQty;
                        db.SetSqlParameterValue( _FuelAddQty, 3 );
                        //parDate[4] = new MySqlParameter("?FuelSubQty", MySqlDbType.Double);
                        db.NewSqlParameter( db.ParamPrefics + "FuelSubQty", db.GettingDouble(), 4 );
                        //parDate[4].Value = _FuelSubQty;
                        db.SetSqlParameterValue( _FuelSubQty, 4 );
                        //parDate[5] = new MySqlParameter("?FuelEnd", MySqlDbType.Double);
                        db.NewSqlParameter( db.ParamPrefics + "FuelEnd", db.GettingDouble(), 5 );
                        //parDate[5].Value = _FuelEnd;
                        db.SetSqlParameterValue( _FuelEnd, 5 );
                        //parDate[6] = new MySqlParameter("?FuelExpens", MySqlDbType.Double);
                        db.NewSqlParameter( db.ParamPrefics + "FuelExpens", db.GettingDouble(), 6 );
                        //parDate[6].Value = _FuelExpens;
                        db.SetSqlParameterValue( _FuelExpens, 6 );
                        //parDate[7] = new MySqlParameter("?FuelExpensAvg", MySqlDbType.Double);
                        db.NewSqlParameter( db.ParamPrefics + "FuelExpensAvg", db.GettingDouble(), 7 );
                        //parDate[7].Value = _FuelExpensAvg;
                        db.SetSqlParameterValue( _FuelExpensAvg, 7 );
                        //cnMySQL.ExecuteNonQueryCommand(_sSql, parDate);
                        db.ExecuteNonQueryCommand( Sql, db.GetSqlParameterArray );

                        //������������ ����� �������� - ������
                        string sSQLdelete = "DELETE FROM rt_route_fuel WHERE Id_main = " + _ID;
                        //cnMySQL.ExecuteNonQueryCommand(sSQLdelete);
                        db.ExecuteNonQueryCommand( sSQLdelete );
                        foreach (atlantaDataSet.FuelReportRow f_row in _dsAtlanta.FuelReport.Rows)
                        {
                            string sSQLinsert = "";
                            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                            {
                                sSQLinsert = "INSERT INTO rt_route_fuel (Id_main,Location,time_,dValueCalc, dValueHandle)"
                                                    + " VALUES (" + _ID + ",?Location,?time_,?Value,?Value)";
                            }
                            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                            {

                                sSQLinsert = "INSERT INTO rt_route_fuel (Id_main,Location,time_,dValueCalc, dValueHandle)"
                                                    + " VALUES (" + _ID + ",@Location,@time_,@Value,@Value)";
                            }

                            //MySqlParameter[] par_fuel = new MySqlParameter[3];
                            db.NewSqlParameterArray(3);
                            //par_fuel[0] = new MySqlParameter("?time_", MySqlDbType.DateTime);
                            db.NewSqlParameter( db.ParamPrefics + "time_", db.GettingDateTime(), 0);
                            //par_fuel[0].Value = f_row.time_;
                            db.SetSqlParameterValue( f_row.time_, 0);
                            //par_fuel[1] = new MySqlParameter("?Location", MySqlDbType.String);
                            db.NewSqlParameter( db.ParamPrefics + "Location", db.GettingString(), 1 );
                            //par_fuel[1].Value = f_row.Location;
                            db.SetSqlParameterValue( f_row.Location, 1 );
                            //par_fuel[2] = new MySqlParameter("?Value", MySqlDbType.Double);
                            db.NewSqlParameter( db.ParamPrefics + "Value", db.GettingDouble(), 2 );
                            //par_fuel[2].Value = Math.Round(f_row.dValue, 2);
                            db.SetSqlParameterValue( Math.Round( f_row.dValue, 2 ), 2 );

                            //cnMySQL.ExecuteNonQueryCommand(sSQLinsert, par_fuel);
                            db.ExecuteNonQueryCommand(sSQLinsert, db.GetSqlParameterArray);
                        } // foreach
                    }
                    db.CloseDbConnection();
            }

            /// <summary>
            /// ����� �������� ������ �� �������� - ���
            /// </summary>
            /// <param name="m_row"></param>
            private void ContentFindFuelDRT(atlantaDataSet.mobitelsRow m_row)
            {
                PartialAlgorithms PA = new PartialAlgorithms(m_row);
                //atlantaDataSet.dataviewRow[] dataRows = (atlantaDataSet.dataviewRow[])_dsAtlanta.dataview.Select() ; 
                if (GpsDatasDocItem != null && GpsDatasDocItem.Length > 0)
                {
                    // ������ ������� � ��������, �
                    _Fuel_ExpensMove = Math.Round(PA.GetFuelUseInMotionDrt(GpsDatasDocItem, 0), 2);
                    // ������ ������� �� ��������, �
                    _Fuel_ExpensStop = Math.Round(PA.GetFuelUseInStopDrt(GpsDatasDocItem, 0, 0), 2);
                    // ����� ������ ������� � ����, �
                    _Fuel_ExpensTotal = Math.Round(_Fuel_ExpensMove + _Fuel_ExpensStop, 2);
                    // ������ ������� �� 100 ��, �
                    _Fuel_ExpensAvg = 0;
                    if (_Distance  > 0)
                    {
                        _Fuel_ExpensAvg = Math.Round(_Fuel_ExpensMove * 100 / _Distance, 0);
                    }
                    //using (ConnectMySQL cnMySQL = new ConnectMySQL())
                    DriverDb db = new DriverDb();
                    db.ConnectDb();
                    {
                        if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                        {
                            Sql = "Update rt_route SET Fuel_ExpensMove = ?Fuel_ExpensMove"
                                    + ",Fuel_ExpensStop = ?Fuel_ExpensStop"
                                    + ",Fuel_ExpensTotal = ?Fuel_ExpensTotal"
                                    + ",Fuel_ExpensAvg = ?Fuel_ExpensAvg"
                                    + "  WHERE Id = " + _ID;
                        }
                        else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                        {
                            Sql = "Update rt_route SET Fuel_ExpensMove = @Fuel_ExpensMove"
                                    + ",Fuel_ExpensStop = @Fuel_ExpensStop"
                                    + ",Fuel_ExpensTotal = @Fuel_ExpensTotal"
                                    + ",Fuel_ExpensAvg = @Fuel_ExpensAvg"
                                    + "  WHERE Id = " + _ID;
                        }

                        //MySqlParameter[] parDate = new MySqlParameter[4];
                        db.NewSqlParameterArray(4);
                        //parDate[0] = new MySqlParameter("?Fuel_ExpensStop", MySqlDbType.Double);
                        db.NewSqlParameter(db.ParamPrefics + "Fuel_ExpensStop", db.GettingDouble(), 0);
                        //parDate[0].Value = _Fuel_ExpensStop;
                        db.SetSqlParameterValue( _Fuel_ExpensStop, 0);
                        //parDate[1] = new MySqlParameter("?Fuel_ExpensTotal", MySqlDbType.Double);
                        db.NewSqlParameter( db.ParamPrefics + "Fuel_ExpensTotal", db.GettingDouble(), 1 );
                        //parDate[1].Value = _Fuel_ExpensTotal;
                        db.SetSqlParameterValue( _Fuel_ExpensTotal, 1 );
                        //parDate[2] = new MySqlParameter("?Fuel_ExpensAvg", MySqlDbType.Double);
                        db.NewSqlParameter( db.ParamPrefics + "Fuel_ExpensAvg", db.GettingDouble(), 2 );
                        //parDate[2].Value = _Fuel_ExpensAvg;
                        db.SetSqlParameterValue( _Fuel_ExpensAvg, 2 );
                        //parDate[3] = new MySqlParameter("?Fuel_ExpensMove", MySqlDbType.Double);
                        db.NewSqlParameter( db.ParamPrefics + "Fuel_ExpensMove", db.GettingDouble(), 3 );
                        //parDate[3].Value = _Fuel_ExpensMove;
                        db.SetSqlParameterValue( _Fuel_ExpensMove, 3 );

                        //cnMySQL.ExecuteNonQueryCommand(_sSql, parDate);
                        db.ExecuteNonQueryCommand( Sql, db.GetSqlParameterArray );
                    } 
                    db.CloseDbConnection();
                } // if
            }

            /// <summary>
            /// ����� ��������� �� ��������
            /// </summary>
            /// <param name="m_row"></param>
            private void ContentFindStops(atlantaDataSet.mobitelsRow m_row)
            {
                SetStatusEvent(Resources.StopsSearch);
                Kilometrage km = new Kilometrage();
                km.SelectItem(m_row);
                km.Run();
                string sSQLdelete = "DELETE FROM rt_route_stops WHERE Id_main = " + _ID;
                //using (ConnectMySQL cnMySQL = new ConnectMySQL())
                DriverDb db = new DriverDb();
                db.ConnectDb();
                {
                    //cnMySQL.ExecuteNonQueryCommand(sSQLdelete);
                    db.ExecuteNonQueryCommand( sSQLdelete );
                    foreach (atlantaDataSet.KilometrageReportRow km_row in _dsAtlanta.KilometrageReport.Rows)
                    {
                        if (km_row.State == Resources.Stop)
                        {
                            string sSQLinsert = "";
                            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                            {
                                sSQLinsert = "INSERT INTO rt_route_stops (Id_main,Location,InitialTime, FinalTime,`Interval`,CheckZone)"
                                                    + " VALUES (" + _ID +
                                                    ",?Location,?InitialTime,?FinalTime,?Interval,?CheckZone)";
                            }
                            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                            {
                                sSQLinsert = "INSERT INTO rt_route_stops (Id_main,Location,InitialTime, FinalTime,Interval,CheckZone)"
                                                    + " VALUES (" + _ID +
                                                    ",@Location,@InitialTime,@FinalTime,@Interval,@CheckZone)";
                            }

                            //MySqlParameter[] parDate = new MySqlParameter[5];
                            db.NewSqlParameterArray(5);
                            //parDate[0] = new MySqlParameter("?InitialTime", MySqlDbType.DateTime);
                            db.NewSqlParameter(db.ParamPrefics + "InitialTime", db.GettingDateTime(), 0);
                            //parDate[0].Value = km_row.InitialTime;
                            db.SetSqlParameterValue( km_row.InitialTime, 0 );
                            //parDate[1] = new MySqlParameter("?FinalTime", MySqlDbType.DateTime);
                            db.NewSqlParameter( db.ParamPrefics + "FinalTime", db.GettingDateTime(), 1 );
                            //parDate[1].Value = km_row.FinalTime;
                            db.SetSqlParameterValue( km_row.FinalTime, 1 );
                            //parDate[2] = new MySqlParameter("?Interval", MySqlDbType.Time);
                            db.NewSqlParameter( db.ParamPrefics + "Interval", db.GettingTime(), 2 );                           
                            //parDate[2].Value = km_row.Interval.TotalHours > Consts.MY_SQL_DATATYPE_TIME_MAX_HOUER ? TimeSpan.Zero : km_row.Interval;
                            db.SetSqlParameterValue( km_row.Interval.TotalHours > Consts.MY_SQL_DATATYPE_TIME_MAX_HOUER ? TimeSpan.Zero : km_row.Interval, 2 );
                            //parDate[3] = new MySqlParameter("?Location", MySqlDbType.String, 255);
                            db.NewSqlParameter(db.ParamPrefics + "Location", db.GettingString(), 255, 3); 
                            //parDate[3].Value = km_row.Location;
                            db.SetSqlParameterValue( km_row.Location, 3 );
                            //parDate[4] = new MySqlParameter("?CheckZone", MySqlDbType.Bit);
                            db.NewSqlParameter( db.ParamPrefics + "CheckZone", db.GettingBit(), 4 ); 

                            if (_taskZones != null && _taskZones.Count >0 )
                            {
                                bool isZone = false;
                                foreach (IZone  zone in _taskZones)
                                {
                                     if (zone.Name == km_row.Location) 
                                     {
                                         isZone = true;
                                         break;
                                     }
                                }
                                //parDate[4].Value = isZone;
                                db.SetSqlParameterValue( isZone, 4 );
                             }
                            else
                                //parDate[4].Value = false;
                                db.SetSqlParameterValue( false, 4 );

                            //cnMySQL.ExecuteNonQueryCommand(sSQLinsert, parDate);
                            db.ExecuteNonQueryCommand( sSQLinsert, db.GetSqlParameterArray );
                        }
                    }
                }
                db.CloseDbConnection();
            }
            /// <summary>
            ///  ��������� � dataview ������  ������ ������������ ��������
            /// </summary>
            private void ContentClearNoRouteData()
            {
                GpsDatasDocItem = GpsDatasDocItem.Where(gps => ((gps.Time < _dtFactStart) || (gps.Time > _dtFactEnd))).ToArray(); 
                //List<System.Int64> lsDelDataGPS = new List<System.Int64>();
                ////foreach (atlantaDataSet.dataviewRow d_row in _dsAtlanta.dataview.Rows)
                
                //foreach (GpsData gpsData in GpsDatas)
                //{
                //    if ((gpsData.Time < _dtFactStart) || (gpsData.Time > _dtFactEnd))
                //    {
                //        lsDelDataGPS.Add(gpsData.Id);
                //    }
                //    else
                //    {
                //        //string sLoc = Algorithm.GeoLocator.GetLocationInfo(new PointLatLng(d_row.Lat, d_row.Lon));
                //        //Console.WriteLine("{0} {1}", ++i, sLoc);
                //    }
                //}
                //lsDelDataGPS.ForEach(delegate(System.Int64 DataGPS_ID)
                //    {
                //        atlantaDataSet.dataviewRow d_row = _dsAtlanta.dataview.FindByDataGps_ID(DataGPS_ID);
                //        d_row.Delete();
                //    });
            }
            /// <summary>
            /// ������������� ����� � �����
            /// </summary>
            private bool ContentPlanFactCompare()
            {
                SetStatusEvent(Resources.ComparisonPlanFact);
                //using (ConnectMySQL cnMySQL = new ConnectMySQL())
                DriverDb db = new DriverDb();
                db.ConnectDb();
                {
                    //using (ConnectMySQL cnMySQLup = new ConnectMySQL())
                    DriverDb driverDb = new DriverDb();
                    driverDb.ConnectDb();
                    {
                        string sSQLselect = "SELECT   rt_routet.* FROM rt_routet WHERE   rt_routet.Id_main = " + _ID + " ORDER BY rt_routet.DatePlan";
                        //MySqlDataReader dr = cnMySQL.GetDataReader(sSQLselect);
                        db.GetDataReader(sSQLselect);
                        int i_start = 0;
                        bool bFirstPoint = true;
                        bool bFindEvent = false;
                        _dtPlanEnd = _dtStart;
                        _dtPlanStart = _dtStart;
                        _dtFactEnd = _dtStart;
                        _dtFactStart = _dtStart;
                        int Id_first = 0;
                        CrossingInfo[] crossInfoArray = CrossZones.Instance.GetCrossInfoArray(_Mobitel_Id);
                        //while (dr.Read())
                        while( db.Read() )
                        {
                            //int iZone = (int)TotUtilites.NDBNullReader( dr, "Id_zone", 0 );
                            int iZone = (int)TotUtilites.NdbNullReader( db, "Id_zone", 0 );
                            //int iEvent = (int)TotUtilites.NDBNullReader( dr, "Id_event", 0 );
                            int iEvent = (int)TotUtilites.NdbNullReader( db, "Id_event", 0 );
                            //_dtPlanEnd = (DateTime)TotUtilites.NDBNullReader(dr, "DatePlan", (DateTime)TotUtilites.NDBNullReader(dr, "DateFact", _dtStart));
                            _dtPlanEnd = (DateTime)TotUtilites.NdbNullReader( db, "DatePlan", (DateTime)TotUtilites.NdbNullReader( db, "DateFact", _dtStart ) );
                            //���� ����� ������� ��������
                            double dbPath = 0;
                            // ����� ������� ������ ������� � ���������� �� ���� ������.����������� ������ ������� ����� �� ��������� � �������� 
                            // ������� ���� ��������� ����� ��������, ���� ��� - ��������� ����� ��������.���������� � ���������� - ����� �� ������ 
                            if (bFirstPoint)
                            {
                                TimeSpan tsDelta = _dtEnd.Subtract(_dtStart);
                                for (int i = 0; i < crossInfoArray.Length; i++)
                                {
                                    CrossingInfo cz_row = crossInfoArray[i];
                                    if (iZone == cz_row.ZoneID)
                                    {
                                        switch (iEvent)
                                        {
                                            case (int)Consts.RouteEvent.Entry:
                                                {
                                                    if ((cz_row.PointLocationType == PointLocationType.CrossIn)
                                                        || (cz_row.PointLocationType == PointLocationType.BeginIn))
                                                    {
                                                        bFindEvent = true;
                                                    }
                                                    break;
                                                }
                                            case (int)Consts.RouteEvent.Exit:
                                                {
                                                    if (cz_row.PointLocationType == PointLocationType.CrossOut)
                                                    {
                                                        bFindEvent = true;
                                                    }
                                                    break;
                                                }
                                        }
                                        if (bFindEvent)
                                        {
                                            TimeSpan tsWork = _dtPlanEnd.Subtract(cz_row.CrossingTime);
                                            if (tsWork.TotalSeconds < 0)
                                            {
                                                // ����� ��� ������� ����� �������� ��������
                                                if (!bFirstPoint)
                                                    break;
                                                else
                                                    tsWork = -tsWork;
                                            }
                                            if (tsDelta.Subtract(tsWork).TotalSeconds > 0)
                                            {
                                                tsDelta = tsWork;
                                                i_start = i + 1;
                                                bFirstPoint = false;
                                                _dtPlanStart = _dtPlanEnd;
                                                _dtFactStart = cz_row.CrossingTime;
                                                dbPath = cz_row.Kilometrage;
                                                //Id_first = dr.GetInt32(dr.GetOrdinal("Id"));
                                                Id_first = db.GetInt32( db.GetOrdinal( "Id" ) );
                                            }
                                            bFindEvent = false;
                                        }
                                    }
                                }
                                if (bFirstPoint)
                                {
                                    XtraMessageBox.Show(Resources.EventFirstNFind, Resources.Routers1);
                                    return false;
                                }
                                else
                                {
                                    ContentSetFact(_dtPlanStart, _dtFactStart, 0, Id_first);
                                }
                            }
                            else
                            {
                                dbPath = 0;
                                bool bFindZoneEvent = false;
                                // �� ���������� ������� ������� ������������ ���������� �������
                                int i_start_marker = i_start;
                                for (int i = i_start; i < crossInfoArray.Length; i++)
                                {
                                    i_start++;
                                    CrossingInfo cz_row = crossInfoArray[i];
                                    // ��������� ������ ��� �� ���������� � ����,�� ����������� ����� ������� �����
                                    dbPath = dbPath + cz_row.Kilometrage;
                                    if (iZone == cz_row.ZoneID)
                                    {
                                        bFindEvent = false;
                                        switch (iEvent)
                                        {
                                            case (int)Consts.RouteEvent.Entry:
                                                {
                                                    if ((cz_row.PointLocationType == PointLocationType.CrossIn)
                                                        || (cz_row.PointLocationType == PointLocationType.BeginIn))
                                                    {
                                                        bFindEvent = true;
                                                    }
                                                    break;
                                                }
                                            case (int)Consts.RouteEvent.Exit:
                                                {
                                                    if (cz_row.PointLocationType == PointLocationType.CrossOut)
                                                    {
                                                        bFindEvent = true;
                                                    }
                                                    break;
                                                }
                                        }
                                        if (bFindEvent)
                                        {
                                            _dtFactEnd = cz_row.CrossingTime;
                                            //ContentSetFact(_dtPlanEnd, _dtFactEnd, dbPath, dr.GetInt32("Id"));
                                            ContentSetFact( _dtPlanEnd, _dtFactEnd, dbPath, db.GetInt32( "Id" ) );
                                            dbPath = 0;
                                            bFindZoneEvent = true;
                                            break;
                                        }
                                    }
                                }
                                // ���������� ����
                                if (!bFindZoneEvent) i_start = i_start_marker;
                            }
                        }
                        //dr.Close();
                        db.CloseDataReader();
                    }// cnMySQLup.Close
                }// cnMySQL.Close
                db.CloseDbConnection();
                return true;
            }
            /// <summary>
            /// ����� ����������� ���, ������������ �������
            /// </summary>
            private bool ContentAnalizZonesCross(atlantaDataSet.mobitelsRow m_row)
            {
                SetStatusEvent(string.Format("{0} {1}/{2}", Resources.CheckZonesVisitsAnaliz, _taskZones.Count, ZonesModel.Zones.Count));
                CrossZonesAlgorithm cza = new CrossZonesAlgorithm();
                List<int> zonesIdList = new List<int>();
                foreach (IZone zone in _taskZones)
                {
                    zonesIdList.Add(zone.Id);
                }
                cza.SelectItem(m_row);
                cza.Run(zonesIdList);
                CrossingInfo[] crossInfoArray = CrossZones.Instance.GetCrossInfoArray(_Mobitel_Id);
                for (int i = 0; i < crossInfoArray.Length; i++)
                {
                    if (zonesIdList.Contains(crossInfoArray[i].ZoneID)) return true; 
                }
                return false;
            }
            /// <summary>
            /// ���������� ����������� ������� ����� ������ ��������� � ����� �����
            /// </summary>
            public void ContentRecalcFuelDUT()
            {
                //using (ConnectMySQL cnMySQL = new ConnectMySQL())
                DriverDb db = new DriverDb();
                db.ConnectDb();
                {
                    if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                    {
                        Sql = "Update rt_route SET FuelAdd = ?FuelAdd"
                                + ",FuelSub = ?FuelSub"
                                + ",FuelSubQty = ?FuelSubQty"
                                + ",FuelAddQty = ?FuelAddQty"
                                + ",FuelExpens = ?FuelExpens"
                                + ",FuelExpensAvg  = ?FuelExpensAvg"
                                + "  WHERE Id = " + _ID;
                    }
                    else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                    {
                        Sql = "Update rt_route SET FuelAdd = @FuelAdd"
                               + ",FuelSub = @FuelSub"
                               + ",FuelSubQty = @FuelSubQty"
                               + ",FuelAddQty = @FuelAddQty"
                               + ",FuelExpens = @FuelExpens"
                               + ",FuelExpensAvg  = @FuelExpensAvg"
                               + "  WHERE Id = " + _ID;
                    }

                    //MySqlParameter[] parDate = new MySqlParameter[6];
                    db.NewSqlParameterArray(6);
                    //parDate[0] = new MySqlParameter("?FuelSub", MySqlDbType.Double);
                    db.NewSqlParameter(db.ParamPrefics + "FuelSub", db.GettingDouble(), 0);
                    //parDate[0].Value = _FuelSub;// ����� �����
                    db.SetSqlParameterValue(_FuelSub, 0);
                    //parDate[1] = new MySqlParameter("?FuelSubQty", MySqlDbType.Int32);
                    db.NewSqlParameter( db.ParamPrefics + "FuelSubQty", db.GettingInt32(), 1 );
                    //parDate[1].Value = _FuelSubQty;// ����� ����� ���
                    db.SetSqlParameterValue( _FuelSubQty, 1 );
                    //parDate[2] = new MySqlParameter("?FuelAdd", MySqlDbType.Double);
                    db.NewSqlParameter( db.ParamPrefics + "FuelAdd", db.GettingDouble(), 2 );
                    //parDate[2].Value = _FuelAdd;// // ����� ����������
                    db.SetSqlParameterValue( _FuelAdd, 2 );
                    //parDate[3] = new MySqlParameter("?FuelAddQty", MySqlDbType.Int32);
                    db.NewSqlParameter( db.ParamPrefics + "FuelAddQty", db.GettingInt32(), 3 );
                    //parDate[3].Value = _FuelAddQty;// ����� ���������� ���
                    db.SetSqlParameterValue( _FuelAddQty, 3 );
                    //parDate[4] = new MySqlParameter("?FuelExpens", MySqlDbType.Double);
                    db.NewSqlParameter( db.ParamPrefics + "FuelExpens", db.GettingDouble(), 4 );
                    //parDate[4].Value = _FuelExpens;// // ����� ������
                    db.SetSqlParameterValue( _FuelExpens, 4 );
                    //parDate[5] = new MySqlParameter("?FuelExpensAvg", MySqlDbType.Double);
                    db.NewSqlParameter( db.ParamPrefics + "FuelExpensAvg", db.GettingDouble(), 5 );
                    //parDate[5].Value = _FuelExpensAvg;// // ������ �� 100 ��
                    db.SetSqlParameterValue( _FuelExpensAvg, 5 );

                    //cnMySQL.ExecuteNonQueryCommand(_sSql, parDate);
                    db.ExecuteNonQueryCommand(Sql, db.GetSqlParameterArray);
                }
                db.CloseDbConnection();
            }

            private void SetFuelExpensAndAvg()
            {
                _FuelExpens = Math.Round(_FuelEnd + _FuelAdd - FuelStart,2);
                _FuelExpensAvg =_Distance  == 0 ? 0 : Math.Round(_FuelExpens * 100 / _Distance , 2);
            }
            /// <summary>
            /// ������� ������ ������� �� ������� - ��� ���������� ���������� � ����� ����������
            /// </summary>
            /// <param name="TimeTest"></param>
            /// <param name="TimeBase"></param>
            /// <returns></returns>
            private string  TimePersent(string TimeTest, string TimeBase)
            {
                if ((TimeTest.Length == 0) || (TimeBase.Length == 0)) return "(0%)";
                TimeSpanConverter tsc = new TimeSpanConverter();
                TimeSpan tsTimeBase = (TimeSpan)tsc.ConvertFromString(TimeBase);
                TimeSpan tsTimeTest = (TimeSpan)tsc.ConvertFromString(TimeTest);
                if (tsTimeBase.TotalSeconds ==0)
                    return " (0%)";
                else
                {
                double dbProc = Math.Round(tsTimeTest.TotalSeconds / tsTimeBase.TotalSeconds, 2) * 100;
                return " (" + dbProc.ToString()  + "%)";
                }

            }
            /// <summary>
            /// ��������  �������� ������ �� �������� - ���
            /// </summary>
            /// <returns></returns>
            public bool ContentRefreshFuelDUT()
            {
                int iRecords = ContentDataSetCreate();
                if (iRecords == 0)
                {
                    SetStatusEvent(Resources.DataFactAbsence);
                    //using (ConnectMySQL cnMySQL = new ConnectMySQL())
                    DriverDb db = new DriverDb();
                    db.ConnectDb();
                    {
                        Sql = "DELETE FROM rt_route_fuel WHERE Id_main = " + _ID;
                        //cnMySQL.ExecuteNonQueryCommand(_sSql);
                        db.ExecuteNonQueryCommand( Sql );
                    }
                    Algorithm.AtlantaDataSet = _dsFromAlgorithm;
                    Algorithm.RunFromModule = false;
                    return true;
                }
                atlantaDataSet.mobitelsRow m_row = (LocalCache.atlantaDataSet.mobitelsRow)_dsAtlanta.mobitels.FindByMobitel_ID(_Mobitel_Id);
                m_row.Check = true;
                SetDataFactStart();
                _dtFactEnd = _dtEnd;
                ContentClearNoRouteData(); 
                ContentFindStops(m_row);
                ContentFindFuelDUT(m_row);
                ContentDataSetReturnClear();
                return true;
            }
            private bool ContentDelete()
            {
                DriverDb db = new DriverDb();
                try
                {
                    //using (ConnectMySQL cnMySQL = new ConnectMySQL())
                   
                    db.ConnectDb();
                    {
                        string sSQLdelete = "DELETE FROM rt_routet  WHERE rt_routet.Id_main = " + _ID;
                        //cnMySQL.ExecuteNonQueryCommand(sSQLdelete);
                        db.ExecuteNonQueryCommand( sSQLdelete );
                    }
                    ClearTotals();
                    db.CloseDbConnection();
                    return true;
                }
                catch
                {
                    db.CloseDbConnection();
                     return false;
                }
            }
            /// <summary>
            /// ����� ������
            /// </summary>
            private void SetDistance()
            {
                //using (ConnectMySQL cnMySQL = new ConnectMySQL())
                DriverDb db = new DriverDb();
                db.ConnectDb();
                {
                    SetStatusEvent(Resources.DistanceCalc);
                    Sql = "SELECT   SUM(rt_routet.Distance) FROM  rt_routet WHERE   rt_routet.Id_main = " + _ID
                        + " GROUP BY   rt_routet.Id_main";
                    //_Distance = cnMySQL.GetScalarValueDblNull(_sSql);
                    _Distance = db.GetScalarValueDblNull( Sql );
                }
                db.CloseDbConnection();
            }
            /// <summary>
            /// ������� �������� ������ �����������,������������ � ���������
            /// </summary>
            private void ClearTotals()
            {
                _Distance = 0;
                _TimeMove = "";
                _TimeStop = "";
                _SpeedAvg = 0;
                _TimePlanTotal = "";
                _TimeFactTotal = "";
                _Deviation = "";
                _DeviationAr = "";
                _FuelStart = 0;
                _FuelEnd = 0;
                _FuelAddQty = 0;
                _FuelSubQty = 0;
                _FuelSub =0;
                _FuelAdd = 0;
                _FuelExpensAvg = 0;
                _FuelExpens = 0;
                _Fuel_ExpensMove = 0;
                _Fuel_ExpensStop = 0;
                _Fuel_ExpensTotal = 0;
                _Fuel_ExpensAvg = 0;
                _PointsValidity = 0;
                _PointsCalc = 0;
                _PointsFact = 0;
                _PointsIntervalMax = "";
            }
            /// <summary>
            /// �������� � ����������� ���������� �������� ������ ������ ������������.� ����� ��������� �������� ��������� 
        /// </summary>
        /// <returns> ���������� ������� </returns>
            private int ContentDataSetCreate()
            {
                _dsAtlanta = new atlantaDataSet();
                LocalCacheItem lci = new LocalCacheItem(_dtStart, _dtEnd, _Mobitel_Id);
                _dsFromAlgorithm = Algorithm.AtlantaDataSet;
                Algorithm.RunFromModule = true;
                Algorithm.AtlantaDataSet = _dsAtlanta;
                SetStatusEvent(Resources.DataInitialReceipt);
                int countGpsData = lci.CreateDSAtlanta(ref _dsAtlanta);
                GpsDatasDocItem = lci.GpsDatas;
                Algorithm.GpsDatas = GpsDatasDocItem;
                return countGpsData;
                
            }
            private void ContentDataSetReturnClear()
            {
                Algorithm.AtlantaDataSet = _dsFromAlgorithm;
                Algorithm.RunFromModule = false;
                if (_dsAtlanta!=null) _dsAtlanta.Clear();
                SetStatusEvent(Resources.Ready);
                 
            }
        /// <summary>
        /// ���� � ���������� ���������� ��������
        /// </summary>
        private void SetDistanceWithLogicSensor()
        {
            //using (ConnectMySQL cnMySQL = new ConnectMySQL())
            DriverDb db = new DriverDb();
            db.ConnectDb();
            {
                    SetStatusEvent(Resources.DistanceWithOnLogicSensor );
                    int senLogicStartBit = 0;
                    int senActiveStateValue = 0;
                    GetSensor(AlgorithmType.WORK_M, out  senLogicStartBit, out  senActiveStateValue);
                    if (senLogicStartBit == 0) return;
                    _DistanceWithLogicSensor = 0;
                    foreach (GpsData gpsData in GpsDatasDocItem)
                    {
                        if (senActiveStateValue == Convert.ToInt32(Calibrate.ulongSector(gpsData.Sensors , 1, senLogicStartBit)))
                        {
                            _DistanceWithLogicSensor = _DistanceWithLogicSensor + gpsData.Dist; 
                        }
                    }
                    if (DistanceWithLogicSensor > 0)
                    {

                        Sql = "UPDATE  rt_route SET DistLogicSensor = " + db.ParamPrefics + "DistanceWithLogicSensor  WHERE rt_route.Id = " + _ID;
                        
                        //MySqlParameter[] parDate = new MySqlParameter[1];
                        db.NewSqlParameterArray(1);
                        //parDate[0] = new MySqlParameter("?DistanceWithLogicSensor", MySqlDbType.Double);
                        db.NewSqlParameter( db.ParamPrefics + "DistanceWithLogicSensor", db.GettingDouble(), 0);
                        //parDate[0].Value = _DistanceWithLogicSensor;
                        db.SetSqlParameterValue( _DistanceWithLogicSensor, 0 );
                        //cnMySQL.ExecuteNonQueryCommand(_sSql,parDate);
                        db.ExecuteNonQueryCommand( Sql, db.GetSqlParameterArray );
                    } // if
                 }
            db.CloseDbConnection();

        }

        /// <summary>
        /// ��������� �������
        /// </summary>
        /// <param name="alg">���������� ������</param>
        /// <param name="senStartBit">��������� ��� � ����������</param>
        /// <param name="senActiveStateValue">�������� �������� 0 ��� 1 </param>
        private void GetSensor(AlgorithmType alg, out int senStartBit, out int senActiveStateValue)
        {
            senStartBit = 0;
            senActiveStateValue = 0;
            if (m_row == null) return;
            Algorithm Alg = new Algorithm();
            Alg.SelectItem(m_row);
            atlantaDataSet.sensorsRow sensor = Alg.FindSensor(alg);
            if (sensor != null)
            {
                senStartBit = sensor.StartBit;
                senActiveStateValue = (int)sensor.K;
            }
        }
        private void SetDataFactStart()
        {
            //using (ConnectMySQL cnMySQL = new ConnectMySQL())
            DriverDb db = new DriverDb();
            db.ConnectDb();
            {
                Sql = "SELECT DateFact FROM rt_routet WHERE Id_main = " + _ID + " ORDER BY DateFact LIMIT 1";
                //_dtFactStart = cnMySQL.GetScalarValueDateTimeNull(_sSql,_dtStart);
                _dtFactStart = db.GetScalarValueDateTimeNull( Sql, _dtStart );
            }
            db.CloseDbConnection();
        }
        /// <summary>
        /// ������� �������� ���� � ������ ������ ����������� �����
        /// </summary>
        /// <returns></returns>
        private bool IsExistPlanInEveryRecord()
        {
            DataTable dt = GetContent();
            if (dt.Rows.Count == 0) return false;
            DateTime planDate;
            foreach (DataRow dr in dt.Rows)
            {
                if (!DateTime.TryParse(dr["DatePlan"].ToString(), out planDate)) return false;
            }
            return true;
        }
    #endregion
    #region �������� ������� �� ��������� ��������
        public bool CreateTamplate()
        {
            if (Sample_Id == 0)
            {
                 using(  DictionaryRoute dr = new DictionaryRoute())
                 {
                       dr.Comment =Resources.SampleCreateFromRoute + " " + _ID;
                       dr.Name =Resources.RouteList + " " + _ID;
                       if (dr.Name.Length > 0)
                       {
                           dr.Distance = _Distance;
                           DataTable dt = GetContent();
                           if (TestContentData(dt))
                           {
                               _Sample_Id = dr.AddItem();
                               TimeSpan TimePrev = TimeSpan.Zero;
                               for (int i = 0; i < dt.Rows.Count; i++)
                               {
                                   DateTime Fact = (DateTime)dt.Rows[i]["DateFact"];
                                   TimeSpan TimeAbs = new TimeSpan(Fact.Hour, Fact.Minute, Fact.Second);
                                   int Id_zone = (int)dt.Rows[i]["Id_zone"];
                                   if (Id_zone > 0)
                                   {
                                       dr.AddContent(i + 1, Id_zone, (int)dt.Rows[i]["Id_event"],
                                       (i == 0) ? TimeSpan.Zero.ToString().Substring(0, 5) : TimeAbs.Subtract(TimePrev).ToString().Substring(0, 5),
                                       TimeAbs.ToString().Substring(0, 5), "00:00", "00:00");
                                       TimePrev = TimeAbs;
                                   }
                               }
                               UpdateDoc();
                               return true;
                           }
                           else
                               return false;
                       }
                       else
                           return false;
                }
            }
            else
                return false;
        }
        private bool TestContentData(DataTable TestTable)
        {
            string MessError = Resources.ChackZoneAbsenceRow + " {0}";
            for (int i = 0; i < TestTable.Rows.Count; i++)
            {
                int Id_zone = 0;
                if (Int32.TryParse(TestTable.Rows[i]["Id_zone"].ToString(), out Id_zone))
                {
                    if (Id_zone > 0)
                    {
                        DateTime Fact;
                        if (DateTime.TryParse(TestTable.Rows[i]["DateFact"].ToString(), out Fact))
                        {
                        }
                        else
                        {
                            XtraMessageBox.Show(string.Format(Resources.TimeFactAbsence +  " {0}", i+1), Resources.Routers1);
                            return false;
                        }
                    }
                    else
                    {
                        XtraMessageBox.Show(string.Format(MessError, i + 1), Resources.Routers1);      
                        return false;
                    }
                }
                else
                {
                    XtraMessageBox.Show(string.Format(MessError, i + 1), Resources.Routers1); 
                    return false;
                }
            }
            return true;
        }
    #endregion
    }
}
