using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using Agro.Utilites;
using Agro;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid;
using Route.DAL;
using Route.Dictionaries;
using Route.GridEditForms;
using Route.Online;
using Route.Properties;
using TrackControl.General;
using TrackControl.General.DatabaseDriver;
using TUtil = Agro.Utilites.TotUtilites;
using Route.Import;

namespace Route
{
    public partial class MainRoute : DevExpress.XtraEditors.XtraForm
    {
        //ResourceManager RM;        
        #region �����������
        public MainRoute()
        {
            InitializeComponent();
            string MainCaption = System.Reflection.Assembly.GetCallingAssembly().GetName().Name.ToString() + " "
            + System.Reflection.Assembly.GetCallingAssembly().GetName().Version.ToString();
            MainCaption = MainCaption + " - " + Resources.Routers1;

            try
            {
                Text = TUtil.GetMainFormCaption( MainCaption, ( int )Consts.L_NUMBER_ROUTE );
            }
            catch( Exception e )
            {
                XtraMessageBox.Show( e.Message, "Error MainRoute", MessageBoxButtons.OK );
            }

            //------------------------------------------------------------
            Localization();
            //------------------------------------------------------------
            deStart.DateTime = DateTime.Today;
            deEnd.DateTime = DateTime.Today.AddDays(1).AddMinutes(-1)  ;
            DicUtilites.LookUpLoad(leGroupe, "SELECT   team.id, team.Name FROM    team ORDER BY  team.Name");
            DicUtilites.LookUpLoad(leType, "SELECT   rt_route_type.Id, rt_route_type.Name FROM    rt_route_type");
            leType.EditValue = (int)Consts.RouteType.ChackZone;
                DicUtilites.LookUpLoad(leMobitel,
                                       "SELECT   vehicle.Mobitel_id, " + AgroQuery.SqlVehicleIdent  +
                                       " AS F, vehicle.Team_id FROM   vehicle"
                                       + " WHERE   (vehicle.Team_id IS NULL) ORDER BY " + AgroQuery.SqlVehicleIdent );
            tlDictionary.ExpandAll(); 
            LoadDictionary();
            xtbView.SelectedTabPageIndex = 1;
            SetControls();
        }
        #endregion
        //��������� �������� ������� ���������� ������
        private const string sFileXml = "Task.xml";
        private const string sFileInitXml = "TaskInit.xml";
        private OnlineControl _onlineControl;
        #region �����������
        private enum Dictionaries : int
            {
                ��������������� = 0,
                �������,
                ������
            }
        private void tlDictionary_FocusedNodeChanged(object sender, DevExpress.XtraTreeList.FocusedNodeChangedEventArgs e)
        {
            LoadDictionary();
            SetControls();
        }
        private void LoadDictionary()
        {
            if (tlDictionary.FocusedNode == null) return;
            switch ((Dictionaries)tlDictionary.FocusedNode.Id)
            {
                case Dictionaries.���������������:
                    {
                        gcDict.MainView = gvRouteGroups;
                        //using (ConnectMySQL _cnMySQL = new ConnectMySQL())
                        DriverDb db = new DriverDb();
                        db.ConnectDb();
                        {
                            string sSQL = "SELECT   rt_sample.Id, rt_sample.Name, rt_sample.Remark "
                            + " FROM   rt_sample WHERE   rt_sample.IsGroupe = 1 AND rt_sample.Id_main = 0"
                            + " ORDER BY   rt_sample.Name";
                            //gcDict.DataSource = _cnMySQL.GetDataTable(sSQL);
                            gcDict.DataSource = db.GetDataTable( sSQL );
                            sSQL = "SELECT   rt_sample.Id, rt_sample.Name"
                            + " FROM rt_sample WHERE  rt_sample.IsGroupe = 1  ORDER BY  rt_sample.Name";
                            //leRouteGrp.DataSource = _cnMySQL.GetDataTable(sSQL); 
                            leRouteGrp.DataSource = db.GetDataTable( sSQL );
                        }
                        db.CloseDbConnection();
                        break;
                    }
                case Dictionaries.�������:
                    {
                        gcDict.MainView = gvRoute;
                        //using (ConnectMySQL _cnMySQL = new ConnectMySQL())
                        DriverDb db = new DriverDb();
                        db.ConnectDb();
                        {
                            string sSQL = "SELECT rt_sample.Id, rt_sample.Id_main ,  rt_sample.Name, rt_sample.Distance, rt_sample.Remark "
                            + " FROM   rt_sample "
                            + " WHERE   rt_sample.IsGroupe = 0 "
                            + " ORDER BY   rt_sample.Name";
                            gcDict.DataSource = db.GetDataTable( sSQL );
                        }
                        db.CloseDbConnection();
                        break;
                    }
                case Dictionaries.������:
                    {
                        gcDict.MainView = gvShedule;
                        gcDict.DataSource = DictionarySheduleProvider.GetAllDataTable();
                        break;
                    }
            }
        }
        /// <summary>
        /// �������� ����� �����������
        /// </summary>
        /// <param name="bNew"></param>
        /// <param name="bDelete"></param>
        private void ViewForm(bool bNew, bool bDelete)
        {
            if (tlDictionary.FocusedNode == null) return;
            switch ((Dictionaries)tlDictionary.FocusedNode.Id)
            {
                case Dictionaries.���������������:
                    {
                        if (bDelete)
                        {
                            using (var diRg = new DictionaryRouteGrp())
                            {
                                diRg.DeleteSelectedFromGrid(gvRouteGroups);
                            }
                        }
                        else
                            new RouteGroupeForm(gvRouteGroups, bNew).ShowDialog();
                        break;
                    }
                case Dictionaries.�������:
                    {
                        if (bDelete)
                        {
                            using (var diR = new DictionaryRoute())
                            {
                                diR.DeleteSelectedFromGrid(gvRoute);
                            }
                        }
                        else
                            new RouteForm(gvRoute, bNew).ShowDialog();
                        break;
                    }
                case Dictionaries.������:
                    {
                        if (bDelete)
                        {
                            using (var diSh = new DictionaryShedule())
                            {
                                diSh.DeleteSelectedFromGrid(gvShedule);
                            }
                        }
                        else
                            new SheduleForm(gvShedule, bNew).ShowDialog();
                        break;
                    }
            }
        }
        private void gvRouteGroups_DoubleClick(object sender, EventArgs e)
        {
            ViewDoc();
        }
        private void gvRoute_DoubleClick(object sender, EventArgs e)
        {
            ViewDoc();
        }
        #endregion
        #region ������
        private void btAdd_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            switch (xtbView.SelectedTabPageIndex)
            {
                case 0:
                    {
                        ViewForm(true, false);
                        break;
                    }
                case 1:
                    {
                        new Task(ref gvTask, true).ShowDialog();
                        break;
                    }
            }
        }
        private void btEdit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            ViewDoc();
        }
        private void btDelete_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            switch (xtbView.SelectedTabPageIndex)
            {
                case 0:
                    {
                        if (tlDictionary.FocusedNode == null)
                        {
                            XtraMessageBox.Show(Resources.DataSelect, Resources.DataDelete);
                            return;
                        }
                        ViewForm(false, true);
                        break;
                    }
                case 1:
                    {
                        using (TaskItem ti = new TaskItem())
                        {
                            ti.DeleteSelectedFromGrid(gvTask); 
                        }
                        //if (!gvTask.IsValidRowHandle(gvTask.FocusedRowHandle))
                        //{
                        //    XtraMessageBox.Show("�������� �������� ��� ��������!", "�������� ������");
                        //    return;
                        //}
                        //int _Id = (int)gvTask.GetRowCellValue(gvTask.FocusedRowHandle, gvTask.Columns["Id"]);
                        //using (TaskItem ti = new TaskItem(_Id))
                        //{
                        //    if (ti.DeleteDoc(true))
                        //    {
                        //        gvTask.DeleteRow(gvTask.FocusedRowHandle);
                        //    }
                        //}
                        break;
                    }
            }
            SetControls();
        }
        private void btRefresh_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            LoadTabGrid();
        }
        private void btSettings_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            new RouteSet().ShowDialog();
        }
        private void btColumnsSet_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            gvTask.ColumnsCustomization();
        }
        private void btColumnsSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (LayOutSave(sFileXml))
                XtraMessageBox.Show(Resources.SettingsSave , Resources.Routers1);
             
        }
        private void btColumnRestore_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            LayOutRead(sFileInitXml);
            //gcTask.ForceInitialize();  
        }
        private void sbtFilterClear_Click(object sender, EventArgs e)
        {
            leGroupe.EditValue = null;
            leMobitel.EditValue = null;
            leType.EditValue = null;
        }
        private void sbtAutoCreate_Click(object sender, EventArgs e)
        {
            if (!(DicUtilites.ValidateLookUp(leMobitel)))
            {
                XtraMessageBox.Show(Resources.CarSelect, Resources.Routers1);
                return;
            }
            if ((deStart.DateTime.Subtract(deEnd.DateTime)).TotalSeconds > 0)
            {
                XtraMessageBox.Show(Resources.DateTest, Resources.Routers1);
                return;
            }
            if (!(DicUtilites.ValidateLookUp(leType)))
            {
                XtraMessageBox.Show(Resources.RouteTypeSampleSelect, Resources.Routers1);
                return;
            }
            if (DialogResult.No == XtraMessageBox.Show(String.Format(Resources.RouteListCreateConfirmFor + " {0} " + Resources.IntervalFrom + " {1} " + Resources.IntervalTo + " {2}?", leMobitel.Text, deStart.DateTime, deEnd.DateTime), Resources.Routers1, MessageBoxButtons.YesNo)) return;
            sbtAutoCreate.Enabled = false;
            SetStatus(Resources.RouteListAutoDefinition);
            Application.DoEvents();
            LockMenuBar(true);
            using (var ti = new TaskItem(deStart.DateTime, deEnd.DateTime, (int)leMobitel.EditValue, Convert.ToInt16(leType.EditValue), Resources.AutoCreate))
            {
                if (ti.TestDemo())
                {
                    ti.ChangeStatusEvent += SetStatus;
                    ti.ChangeProgressBar += SetProgress;
                    if (ti.ContentAutoFill())
                    {
                        LoadTasks();
                        SetControls();
                    }
                    ti.ChangeStatusEvent -= SetStatus;
                    ti.ChangeProgressBar -= SetProgress;
                }
            }
            LockMenuBar(false);
            SetStatus(Resources.Ready); 
        }

        private void btStart_Click(object sender, EventArgs e)
        {
            LoadTasks();
        }

        private void btFact_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (gvTask == null || gvTask.SelectedRowsCount == 0) return;
            if (DialogResult.No == XtraMessageBox.Show(Resources.RouteSelectedRecalcConfirm , Resources.Routers1, MessageBoxButtons.YesNo)) return;
            LockMenuBar(true);
            if (gvTask.SelectedRowsCount > 1) SetProgress(gvTask.SelectedRowsCount + 1);
            for (int i = 0; i < gvTask.SelectedRowsCount; i++)
            {
                int iRoute = 0;
                if (gvTask.SelectedRowsCount > 1) SetProgress(i + 1);
                if (Int32.TryParse(gvTask.GetRowCellValue(gvTask.GetSelectedRows()[i], gvTask.Columns["Id"]).ToString(), out iRoute))
                {
                    using (TaskItem ti = new TaskItem(iRoute))
                    {
                        ti.ChangeStatusEvent += SetStatus;
                        if (ti.Type == (short)Consts.RouteType.Location) ti.ChangeProgressBar += SetProgress;
                        ti.ContentAutoFill();
                        ti.ChangeStatusEvent -= SetStatus;
                        if (ti.Type == (short)Consts.RouteType.Location) ti.ChangeProgressBar -= SetProgress;

                    }
                }
            }
            SetProgress(Consts.PROGRESS_BAR_STOP);
            LoadTasks();
            LockMenuBar(false);
            SetStatus(Resources.Ready);
        }
        private void btExcel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string xslFilePath;
            switch (xtbView.SelectedTabPageIndex)
            {
                case 0:
                    {
                        break;
                    }
                case 1:
                    {
                        xslFilePath = StaticMethods.ShowSaveFileDialog("Rep_Full" + StaticMethods.SuffixDateToFileName() , "Excel", "Excel|*.xls");
                        if (xslFilePath.Length > 0)
                        {
                            try
                            {
                                gvTask.OptionsPrint.ExpandAllDetails = true;
                                gvTask.OptionsPrint.PrintDetails = true;
                                gvTask.OptionsPrint.AutoWidth = false;
                                gvTask.BestFitColumns();
                                gvTaskContent.OptionsPrint.AutoWidth = true;
                                gvTask.ExportToXls(xslFilePath);
                                System.Diagnostics.Process.Start(xslFilePath);
                            }
                            catch
                            {
                                XtraMessageBox.Show(Resources.FileRewriteBan, Resources.Routers1, MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                        break;
                    }
            }
        }
        private void btExcelBrief_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string xslFilePath;
            switch (xtbView.SelectedTabPageIndex)
            {
                case 0:
                    {
                        break;
                    }
                case 1:
                    {
                        xslFilePath = StaticMethods.ShowSaveFileDialog("Rep_brief"  + StaticMethods.SuffixDateToFileName() , "Excel", "Excel|*.xls");
                        if (xslFilePath.Length > 0)
                        {
                            try
                            {
                                gvTask.OptionsPrint.PrintDetails = false;
                                gvTask.OptionsPrint.AutoWidth = false;
                                gvTask.BestFitColumns();
                                gvTask.ExportToXls(xslFilePath);
                                System.Diagnostics.Process.Start(xslFilePath);
                            }
                            catch
                            {
                                XtraMessageBox.Show(Resources.FileRewriteBan, Resources.Routers1, MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                        break;
                    }
                case 3:
                    {
                        _onlineControl.ExcelExport();  
                        break;  
                    }
            }
        }
        #endregion
        #region ���������� ��������� 

        private void xtbView_SelectedPageChanged(object sender, DevExpress.XtraTab.TabPageChangedEventArgs e)
        {

            LoadTabGrid();
        }
        public void SetControls()
        {
            btImportXml.Enabled = false;
            switch (xtbView.SelectedTabPageIndex)
            {
                case 0:
                    {
                        btAdd.Enabled = true;
                        var gv = (GridView)gcDict.FocusedView;
                        if (gv.RowCount == 0)
                        {
                            btEdit.Enabled = false;
                            btDelete.Enabled = false;
                        }
                        else
                        {
                            btEdit.Enabled = true;
                            btDelete.Enabled = true;
                        }
                        bsiExcel.Enabled = false;
                        bsiSettings.Enabled = false;
                        btFact.Enabled = false; 
                        break;
                    }
                case 1:
                    {
                        btImportXml.Enabled = true;
                        btAdd.Enabled = true;
                        bsiSettings.Enabled = true; 
                        if (gvTask.RowCount == 0)
                        {
                            bsiExcel.Enabled = false;
                            btEdit.Enabled = false;
                            btDelete.Enabled = false;
                            btFact.Enabled = false; 
                        }
                        else
                        {
                            bsiExcel.Enabled = true;
                            btExcel.Enabled = true;
                            btExcelBrief.Enabled = true;
                            btEdit.Enabled = true;
                            btDelete.Enabled = true;
                            btFact.Enabled = true; 
                        }
                        break;
                    }
                case 2:
                    {
                        btDelete.Enabled = false;
                        btAdd.Enabled = false;
                        btEdit.Enabled = false;
                        bsiSettings.Enabled = false; 
                        if (gvReport.RowCount == 0)
                        {
                            bsiExcel.Enabled = false;
                        }
                        else
                        {
                            bsiExcel.Enabled = true;
                        }
                        break;
                    }
                case 3:
                    {
                        btDelete.Enabled = false;
                        btAdd.Enabled = false;
                        btEdit.Enabled = false;
                        bsiSettings.Enabled = false;
                        
                        btRefresh.Enabled = false;
                        if (pnOnline.Controls.Count == 0)
                        {
                            _onlineControl = new OnlineControl {Dock = DockStyle.Fill};
                            pnOnline.Controls.Add(_onlineControl);
                        }
                            
                        bsiExcel.Enabled = true;
                        btExcel.Enabled = false;
                        btExcelBrief.Enabled = true;
                        break;
                    }
            }
        }
        /// <summary>
        /// �������� �������� ������� ����� �� �������
        /// </summary>
        private void ViewDoc()
        {
            switch (xtbView.SelectedTabPageIndex)
            {
                case 0:
                    {
                        if (tlDictionary.FocusedNode == null)
                        {
                            XtraMessageBox.Show(Resources.DataEditSelect, Resources.DataEdit);
                            return;
                        }
                        ViewForm(false, false);
                        break;
                    }
                case 1:
                    {
                        if (gvTask.FocusedRowHandle == -1) return; 
                        if (!gvTask.IsValidRowHandle(gvTask.FocusedRowHandle))
                        {
                            XtraMessageBox.Show(Resources.DataEditSelect, Resources.DataEdit);
                            return;
                        }

                        if (gvTask.GetRowCellValue(gvTask.FocusedRowHandle, "Id") != null)
                        {
                            try
                            {
                                using (Form f = new Task(ref gvTask, false))
                                {
                                    f.ShowDialog();
                                }
   
                            }
                            catch (Exception ex)
                            {
                                XtraMessageBox.Show(ex.Message, "MainRoute.ViewDoc:499");
                            }
                        }
                        break;
                    }
            }
        }
        /// <summary>
        /// �������� �������� ������ �� �������
        /// </summary>
        private void LoadTabGrid()
        {
            switch (xtbView.SelectedTabPageIndex)
            {
                case 0:
                    {
                        LoadDictionary();
                        break;
                    }
                case 1:
                    {
                        LoadTasks();
                        break;
                    }
                case 2:
                    {
                        //ReportDrivers();
                        break;
                    }
            }
            SetControls();
        }
        private void SetStatus(string sMessage)
        {
            bsiStatus.Caption = sMessage;
        }
        private string GetStatus()
        {
            return (bsiStatus.Caption);
        }
        private void SetProgress(int iValue)
        {
             TotUtilites.SetProgressBar(pbStatus,  iValue);  
        }
        private void LockMenuBar(bool bLock)
        {
            btAdd.Enabled = !bLock;
            btDelete.Enabled = !bLock;
            btEdit.Enabled = !bLock;
            btFact.Enabled = !bLock;
            btSettings.Enabled = !bLock;
            bsiSettings.Enabled = !bLock;
            bsiExcel.Enabled = !bLock;
            btStart.Enabled = !bLock;
            sbtAutoCreate.Enabled = !bLock;
            btRefresh.Enabled = !bLock;
            if (bLock == false) SetControls(); 
        }
        private void MainRoute_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (GetStatus() != Resources.Ready)
            {
                e.Cancel = true;
                return;
            }
            RouteController.Instance.Dispose();
        }
        #endregion
        #region ������ ���������� ������
        private void leGroupe_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                leGroupe.EditValue = null;
                leMobitel.EditValue = null;
            }
        }
        private void leMobitel_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                leMobitel.EditValue = null;
            }
        }
        private void leGroupe_EditValueChanged(object sender, EventArgs e)
        {
           
            if (DicUtilites.ValidateLookUp(leGroupe))
            {
                    DicUtilites.LookUpLoad(leMobitel,
                                           "SELECT   vehicle.Mobitel_id, " + AgroQuery.SqlVehicleIdent  +
                                           " AS F, vehicle.Team_id FROM   vehicle"
                                           + " WHERE   (vehicle.Team_id = " + leGroupe.EditValue.ToString() +
                                           " OR vehicle.Team_id IS NULL) ORDER BY " + AgroQuery.SqlVehicleIdent );
            }
            //leMobitel.EditValue = null;
        }
        private void gvTask_DoubleClick(object sender, EventArgs e)
        {
            ViewDoc();
        }
        /// <summary>
        /// �������� ������� ���������� ������
        /// </summary>
        private void LoadTasks()
        {
            //+ "(SELECT COUNT(rt_route_sensors.Id_main) AS Sensors FROM  rt_route_sensors   WHERE rt_route_sensors.Id_main = rt_route.Id) AS Sensors"
            //using (ConnectMySQL cnMySQL = new ConnectMySQL())
            DriverDb db = new DriverDb();
            db.ConnectDb();
            {
                string sSQLselect = " SELECT rt_route.Id_sample, rt_route.Id, rt_route.Distance, "
                + "rt_route.Id_mobitel, rt_route.Id_driver, rt_route.Remark,vehicle.Team_id,rt_route.TimeStartPlan ,"
                + " TimePlanTotal,TimeFactTotal,Deviation,TimeStop,TimeMove, SpeedAvg,FuelStart ,FuelEnd ,FuelAdd ,FuelSub ,FuelExpens ,FuelExpensAvg ,FuelAddQty,FuelSubQty,PointsValidity,PointsIntervalMax,"
                + " (SELECT MIN(rt_routet.DatePlan) AS PLAN_START FROM  rt_routet   WHERE rt_routet.Id_main = rt_route.Id   GROUP BY  rt_routet.Id_main) AS PLAN_START ,"
                + " (SELECT MIN(rt_routet.DateFact) AS FACT_START FROM  rt_routet   WHERE rt_routet.Id_main = rt_route.Id   GROUP BY  rt_routet.Id_main) AS FACT_START ,"
                + "(SELECT MAX(rt_routet.DatePlan) AS PLAN_FIN FROM  rt_routet   WHERE rt_routet.Id_main = rt_route.Id   GROUP BY  rt_routet.Id_main) AS PLAN_FIN ,"
                + " rt_route.TimeEndFact AS FACT_FIN ,"
                + "(SELECT COUNT(rt_route_stops.Id_main) AS StopsQTY  FROM  rt_route_stops   WHERE  rt_route_stops.Id_main = rt_route.Id) AS StopsQTY ,"
                + "(SELECT COUNT(rt_route_stops.Id) AS SensQTY FROM  rt_route_stops WHERE   rt_route_stops.Id_main = rt_route.Id) AS SensQTY"
                + " FROM   rt_route INNER JOIN vehicle ON rt_route.Id_mobitel = vehicle.Mobitel_id"
                + " WHERE rt_route.TimeStartPlan >= " + db.ParamPrefics + "TimeStart AND rt_route.TimeStartPlan < " + db.ParamPrefics + "TimeEnd";
                //+ "(SELECT SEC_TO_TIME(SUM(TIME_TO_SEC(rt_route_stops.`Interval`))) AS StopsSUM  FROM  rt_route_stops   WHERE  rt_route_stops.Id_main = rt_route.Id) AS StopsSUM ,"
                if (DicUtilites.ValidateLookUp(leGroupe)) sSQLselect = sSQLselect + " AND vehicle.Team_id = " + leGroupe.EditValue.ToString();
                if (DicUtilites.ValidateLookUp(leMobitel)) sSQLselect = sSQLselect + " AND rt_route.Id_mobitel = " + leMobitel.EditValue.ToString();
                if (DicUtilites.ValidateLookUp(leType)) sSQLselect = sSQLselect + " AND rt_route.Type = " + leType.EditValue.ToString();
                sSQLselect = sSQLselect + " ORDER BY   rt_route.Id";
                
                //MySqlParameter[] parDate = new MySqlParameter[2];
                db.NewSqlParameterArray(2);
                //parDate[0] = new MySqlParameter("?TimeStart", MySqlDbType.DateTime);
                db.NewSqlParameter( db.ParamPrefics + "TimeStart", db.GettingDateTime(), 0);
                //parDate[0].Value = deStart.DateTime;
                db.SetSqlParameterValue( deStart.DateTime, 0);
                //parDate[1] = new MySqlParameter("?TimeEnd", MySqlDbType.DateTime);
                db.NewSqlParameter( db.ParamPrefics + "TimeEnd", db.GettingDateTime(), 1 );
                //parDate[1].Value = deEnd.DateTime;
                db.SetSqlParameterValue( deEnd.DateTime, 1 );
                //DataSet dsTask = cnMySQL.GetDataSet(sSQLselect, parDate, "rt_route");
                DataSet dsTask = db.GetDataSet( sSQLselect, db.GetSqlParameterArray, "rt_route" );

                //gcTask.DataSource = dsTask;// cnMySQL.GetDataTable(sSQLselect, parDate);
                if (dsTask == null) return;
                if (dsTask.Tables[0].Rows.Count > 0)
                {
                    // ������ ����� ------------------------------------------
                    sSQLselect = " SELECT  rt_sample.Id, rt_sample.Name FROM   rt_sample ORDER BY rt_sample.Name";
                    //RouteLookUp.DataSource = cnMySQL.GetDataTable(sSQLselect);
                    RouteLookUp.DataSource = db.GetDataTable( sSQLselect );
                    sSQLselect = string.Format(" SELECT   vehicle.Mobitel_id as Id,{0} AS Name FROM   vehicle", AgroQuery.SqlVehicleIdent);
                    //MobitelLookUp.DataSource = cnMySQL.GetDataTable(sSQLselect);
                    MobitelLookUp.DataSource = db.GetDataTable( sSQLselect );
                    sSQLselect = " SELECT driver.id as Id, driver.Family  as Name FROM    driver ORDER BY  driver.Family ";
                    //DriverLookUp.DataSource = cnMySQL.GetDataTable(sSQLselect);
                    DriverLookUp.DataSource = db.GetDataTable( sSQLselect );
                    sSQLselect = " SELECT  zones.Zone_ID as Id, zones.Name, zones.ZonesGroupId FROM   zones ORDER BY zones.Name";
                    //ZoneLookUp.DataSource = cnMySQL.GetDataTable(sSQLselect);
                    ZoneLookUp.DataSource = db.GetDataTable( sSQLselect );
                    sSQLselect = " SELECT  rt_events.Id, rt_events.Name FROM   rt_events ORDER BY rt_events.Name";
                    //EventLookUp.DataSource = cnMySQL.GetDataTable(sSQLselect);
                    EventLookUp.DataSource = db.GetDataTable( sSQLselect );
                    //---------------------------------------------------------
                    sSQLselect = "SELECT rt_routet.Id,rt_routet.Id_main, rt_routet.Id_zone, rt_routet.Id_event, rt_routet.DatePlan, rt_routet.DateFact, rt_routet.Deviation, rt_routet.Remark,rt_routet.Distance, rt_routet.Id_main, rt_routet.Location"
                    + " FROM    rt_routet   INNER JOIN  rt_route ON rt_routet.Id_main = rt_route.Id "
                    + " INNER JOIN  vehicle ON rt_route.Id_mobitel = vehicle.Mobitel_id "
                    + " WHERE rt_route.TimeStartPlan >= " + db.ParamPrefics + "TimeStart AND rt_route.TimeStartPlan < " + db.ParamPrefics + "TimeEnd";
                    if (DicUtilites.ValidateLookUp(leGroupe)) sSQLselect = sSQLselect + " AND vehicle.Team_id = " + leGroupe.EditValue.ToString();
                    if (DicUtilites.ValidateLookUp(leMobitel)) sSQLselect = sSQLselect + " AND rt_route.Id_mobitel = " + leMobitel.EditValue.ToString();
                    if (DicUtilites.ValidateLookUp(leType)) sSQLselect = sSQLselect + " AND rt_route.Type = " + leType.EditValue.ToString();
                    sSQLselect = sSQLselect + " ORDER BY   rt_routet.Id";
                    //cnMySQL.FillDataSet(sSQLselect, parDate, dsTask, "rt_routet");
                    db.NewSqlParameterArray(2);

                    db.SetNewSqlParameter(db.ParamPrefics + "TimeStart", deStart.DateTime);
                    db.SetNewSqlParameter(db.ParamPrefics + "TimeEnd", deEnd.DateTime);
                    db.FillDataSet( sSQLselect, db.GetSqlParameterArray, dsTask, "rt_routet" );

                    DataColumn keyColumn = dsTask.Tables["rt_route"].Columns["Id"];
                    DataColumn foreignKeyColumn = dsTask.Tables["rt_routet"].Columns["Id_main"];
                    dsTask.Relations.Add(Resources.RouteContent, keyColumn, foreignKeyColumn);
                    gcTask.DataSource = dsTask.Tables["rt_route"];
                    //gcTask.ForceInitialize();
                    gcTask.LevelTree.Nodes.Add(Resources.RouteContent, gvTaskContent);
                }
                else
                {
                    gcTask.DataSource = null;
                }
                SetControls(); 
            }
            db.CloseDbConnection();
            LayOutRead(sFileXml);
        }


        #region ���������� ����������� �������
        private bool LayOutSave(string XMLfile)
        {
            try
            {
                gcTask.ForceInitialize();
                string sPath = Consts.APP_DATA_ROUTER;
                if (!Directory.Exists(sPath))
                    Directory.CreateDirectory(sPath);
                sPath = Path.Combine(sPath, XMLfile);
                FileStream fs = new FileStream(sPath, FileMode.Create);
                gvTask.SaveLayoutToStream(fs);
                fs.Close();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        private bool LayOutRead(string XMLfile)
        {
            try
            {
                string sPath = Path.Combine(Consts.APP_DATA_ROUTER, XMLfile);
                if (File.Exists(sPath))
                    gvTask.RestoreLayoutFromXml(sPath);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion
        private void gvTask_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            if (e.RowHandle >= 0)
            {
                if (e.Column.FieldName == "Deviation")
                {
                    string sValue = gvTask.GetRowCellValue(e.RowHandle, gvTask.Columns["Deviation"]).ToString();
                    if ((sValue != "0") && (sValue.Length > 0))
                    {
                        if (sValue.Substring(0, 1) == "-")
                            e.Appearance.ForeColor = Color.Green;
                        else
                            e.Appearance.ForeColor = Color.Red;
                    }

                }
                if (e.Column.FieldName == "PointsIntervalMax")
                {
                    //1. MAX < 10 ��� - �������
                    //2. 10 ��� <=MAX< 30 ��� - ����������
                    //3. 30 ��� <=MAX - �������
                    string sValue = gvTask.GetRowCellValue(e.RowHandle, gvTask.Columns["PointsIntervalMax"]).ToString();
                    TimeSpan tsMax;
                    if (TimeSpan.TryParse(sValue, out tsMax))
                    {
                        if (tsMax.TotalMinutes < 10)
                        {
                            e.Appearance.ForeColor = Color.Green;
                        }
                        else if (tsMax.TotalMinutes < 30)
                        {
                            e.Appearance.ForeColor = Color.Brown;
                        }
                        else if (tsMax.TotalMinutes >= 30)
                        {
                            e.Appearance.ForeColor = Color.Red;
                        }
                    }
                }
            }
        }
        #endregion
        #region �����������
        private void Localization()
        {
            Text = String.Format("TrackControl - ROUTE {0} � Server: {1}, DB: {2}, {3}: {4}",
              Application.ProductVersion,
              UserBaseCurrent.Instance.Server,
              UserBaseCurrent.Instance.Database,
              Resources.User, 
              UserBaseCurrent.Instance.Name);
            btAdd.Caption = Resources.MenuAdd;
            btDelete.Caption = Resources.MenuDelete;
            btEdit.Caption = Resources.MenuEdit;
            btRefresh.Caption = Resources.MenuRefresh;
            btFact.Caption = Resources.MenuFact;
            bsiExcel.Caption = Resources.MenuExport;
            btExcel.Caption = Resources.MenuExportFull;
            btExcelBrief.Caption = Resources.MenuExportBreaf;
            btSettings.Caption = Resources.MenuSetup;
            bsiSettings.Caption = Resources.MenuAdjust;
            btColumnsSet.Caption = Resources.MenuColumns;
            btColumnsSave.Caption = Resources.MenuSave;
            btColumnRestore.Caption = Resources.MenuRestore;

            xtraTabPage1.Text = Resources.Dictionaries;
            xtraTabPage2.Text = Resources.RouterLists;
            xtraTabPage3.Text = Resources.Reports;

            tlDictionary.Columns[0].Caption = Resources.Code;
            tlDictionary.Columns[1].Caption = Resources.DictName;
            tlDictionary.Nodes[0][1] = Resources.GroupesPatternRoute;
            tlDictionary.Nodes[0].Nodes[0][1] = Resources.PatternRoutes;

            GroupeName.Caption = Resources.NameRes;
            GroupeRemark.Caption = Resources.Remark;
            Distance.Caption = Resources.Distance;

            RouteGroupeName.Caption = Resources.Groupe;
            leRouteGrp.Columns[0].Caption = Resources.NameRes;
            RouteName.Caption = Resources.NameRes;
            RoutePath.Caption = Resources.Distance;
            RouteRemark.Caption = Resources.Remark;

            bsiStatus.Caption = Resources.Ready;
            lbDateFrom.Text = Resources.DateFrom;
            lbDateTo.Text = Resources.DateTo;
            lbType.Text = Resources.TypeRes;
            lbCarGroupe.Text = Resources.CarGroupe;
            lbCar.Text = Resources.Car;
            sbtAutoCreate.Text = Resources.AutoCreate;
            btStart.Text = Resources.START;

            gridBand1.Caption = Resources.TotalInformation;
            Id.Caption = Resources.Number;
            Route.Caption = Resources.Route;
            RouteLookUp.Columns[0].Caption = Resources.NameRes;
            Id_mobitel.Caption = Resources.Car;
            MobitelLookUp.Columns[0].Caption = Resources.NameRes;
            Id_driver.Caption = Resources.Driver;
            DriverLookUp.Columns[0].Caption = Resources.NameRes;
            RemarkTask.Caption = Resources.Comment;
            gridBand2.Caption = Resources.Time;
            PLAN_START.Caption = Resources.Start_Plan;
            FACT_START.Caption = Resources.Start_Fact;
            PLAN_FIN.Caption = Resources.Fin_Plan;
            FACT_FIN.Caption = Resources.Fin_Fact;
            TimePlanTotal.Caption = Resources.DurationPlan;
            TimeFactTotal.Caption = Resources.DurationFact;
            DeviationCol.Caption = Resources.Deviation;
            gridBand4.Caption = Resources.MovingParams;
            Distance.Caption = Resources.Distance;
            TimeMove.Caption = Resources.MovingTime;
            SpeedAvg.Caption = Resources.SpeedAvg;
            StopsQTY.Caption = Resources.StopsQTY;
            TimeStop.Caption = Resources.StopsTime;
            gridBand6.Caption = Resources.DUT;
            FuelStart.Caption = Resources.FuelStart;
            FuelEnd.Caption = Resources.FuelEnd;
            FuelAddQty.Caption = Resources.FuelAddQty;
            FuelAdd.Caption = Resources.FuelAdd;
            FuelSubQty.Caption = Resources.FuelSubQty;
            FuelSub.Caption = Resources.FuelSub;
            FuelExpense.Caption = Resources.FuelExpense;
            FuelExpensAvg.Caption = Resources.FuelExpensAvg;
            gridBand3.Caption = Resources.DRT_CAN;
            Fuel_ExpensTotal.Caption = Resources.FuelExpense;
            Fuel_ExpensMove.Caption = Resources.MovingExpence;
            Fuel_ExpensStop.Caption = Resources.StopExpence;
            Fuel_ExpensAvg.Caption = Resources.FuelExpensAvg;
            gridBand7.Caption = Resources.Sensors;
            SensQTY.Caption = Resources.EventsTotal;
            gridQuontity.Caption = Resources.DataQuality;
            Validity.Caption = Resources.DataValidity;
            PointsIntervalMax.Caption = Resources.Max_Interval;
            ZoneLookUp.Columns[0].Caption = Resources.NameRes;
            sbtFilterClear.ToolTip = Resources.FiltersDelete;
            sbtAutoCreate.ToolTip = Resources.AutoCreateRouters;
            LocalizationDataBaseConstants();
        }
        private void LocalizationDataBaseConstants()
        {
            //using (ConnectMySQL cnMySQL = new ConnectMySQL())
            DriverDb db = new DriverDb();
            db.ConnectDb();
            {
                string sSQL = "Select * From rt_events WHERE rt_events.Id = " + (int)Consts.RouteEvent.Entry;
                //MySqlDataReader dr = cnMySQL.GetDataReader(sSQL);
                db.GetDataReader(sSQL);
                //if (dr.Read())
                if(db.Read())
                {
                    //if (dr.GetString("Name") != Resources.Entry)
                    if( db.GetString( "Name" ) != Resources.Entry )
                    {
                        int[] Ids = { (int)Consts.RouteEvent.Entry, (int)Consts.RouteEvent.Exit };
                        string[] Names = { Resources.Entry, Resources.Exit };

                        //using (ConnectMySQL cnMySQLupd = new ConnectMySQL())
                        DriverDb driverDb = new DriverDb();
                        driverDb.ConnectDb();
                        {
                            for (int i = 0; i < Ids.Length; i++)
                            {
                                sSQL = "UPDATE rt_events Set rt_events.Name = " + db.ParamPrefics + "Name WHERE rt_events.Id = " + Ids[i];
                               //MySqlParameter[] parDate = new MySqlParameter[1];
                                driverDb.NewSqlParameterArray(1);
                                //parDate[0] = new MySqlParameter("?Name", MySqlDbType.String);
                                driverDb.NewSqlParameter( db.ParamPrefics + "Name", driverDb.GettingString(), 0 );
                                //parDate[0].Value = Names[i];
                                driverDb.SetSqlParameterValue(Names[i], 0);
                                //cnMySQLupd.ExecuteNonQueryCommand(sSQL, parDate);
                                driverDb.ExecuteNonQueryCommand(sSQL, driverDb.GetSqlParameterArray);
                            } // for
                        }
                    } // if
                } // if
                //dr.Close();
                db.CloseDataReader();
                sSQL = "Select * From rt_route_type WHERE rt_route_type.Id = " + (int)Consts.RouteType.ChackZone;
                //dr = cnMySQL.GetDataReader(sSQL);
                db.GetDataReader( sSQL );
                if (db.Read())
                {
                    if (db.GetString("Name") != Resources.CheckZones)
                    {
                        int[] Ids = { (int)Consts.RouteType.ChackZone, (int)Consts.RouteType.Location};
                        string[] Names = { Resources.CheckZones, Resources.Settlements};
                        //using (ConnectMySQL cnMySQLupd = new ConnectMySQL())
                        DriverDb driverDb = new DriverDb();
                        driverDb.ConnectDb();
                        {
                            for (int i = 0; i < Ids.Length ; i++)
                            {
                                sSQL = "UPDATE rt_route_type Set rt_route_type.Name = " + db.ParamPrefics + "Name WHERE rt_route_type.Id = " + Ids[i];
                                //MySqlParameter[] parDate = new MySqlParameter[1];
                                driverDb.NewSqlParameterArray(1);
                                //parDate[0] = new MySqlParameter("?Name", MySqlDbType.String);
                                driverDb.NewSqlParameter( db.ParamPrefics + "Name", driverDb.GettingString(), 0 );
                                //parDate[0].Value = Names[i];
                                driverDb.SetSqlParameterValue(Names[i], 0);
                                //cnMySQLupd.ExecuteNonQueryCommand(sSQL, parDate);
                                driverDb.ExecuteNonQueryCommand(sSQL, driverDb.GetSqlParameterArray);
                            } // for
                        }
                        
                    }
                }
                //dr.Close();
                db.CloseDataReader();
            }
            db.CloseDbConnection();
        }
        #endregion

        private void deEnd_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void gvShedule_DoubleClick(object sender, EventArgs e)
        {
            ViewDoc();
        }

        private void btImportXml_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            var importer = new ImporterTasksFromXml();
            importer.Start();
            if (importer.TasksImported >0) LoadTasks();
        }
    }
}