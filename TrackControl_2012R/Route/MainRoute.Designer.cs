using Route.Properties;

namespace Route
{
    partial class MainRoute
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode2 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode3 = new DevExpress.XtraGrid.GridLevelNode();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainRoute));
            this.gvRoute = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.RouteId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.RouteGroupeName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.leRouteGrp = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.RouteName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.RoutePath = new DevExpress.XtraGrid.Columns.GridColumn();
            this.RouteRemark = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcDict = new DevExpress.XtraGrid.GridControl();
            this.gvShedule = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colZone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colComment = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gvRouteGroups = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.RouteGroupeId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GroupeName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.GroupeRemark = new DevExpress.XtraGrid.Columns.GridColumn();
            this.bmRouteGrn = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.btAdd = new DevExpress.XtraBars.BarButtonItem();
            this.btDelete = new DevExpress.XtraBars.BarButtonItem();
            this.btEdit = new DevExpress.XtraBars.BarButtonItem();
            this.btRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.btFact = new DevExpress.XtraBars.BarButtonItem();
            this.bsiExcel = new DevExpress.XtraBars.BarSubItem();
            this.btExcel = new DevExpress.XtraBars.BarButtonItem();
            this.btExcelBrief = new DevExpress.XtraBars.BarButtonItem();
            this.bsiSettings = new DevExpress.XtraBars.BarSubItem();
            this.btColumnsSet = new DevExpress.XtraBars.BarButtonItem();
            this.btColumnsSave = new DevExpress.XtraBars.BarButtonItem();
            this.btColumnRestore = new DevExpress.XtraBars.BarButtonItem();
            this.btSettings = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.pbStatus = new DevExpress.XtraBars.BarEditItem();
            this.rpbStatus = new DevExpress.XtraEditors.Repository.RepositoryItemProgressBar();
            this.bsiStatus = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.gvTaskContent = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.Id_cont = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LocationM = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Id_event = new DevExpress.XtraGrid.Columns.GridColumn();
            this.EventLookUp = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.DatePlan = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DateFact = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Deviation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DistanceT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Remark = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcTask = new DevExpress.XtraGrid.GridControl();
            this.gvTask = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridView();
            this.gridBand1 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.Id = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.Route = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.RouteLookUp = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.Id_mobitel = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.MobitelLookUp = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.Id_driver = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.DriverLookUp = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.RemarkTask = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand2 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.PLAN_START = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.FACT_START = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.PLAN_FIN = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.FACT_FIN = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.TimePlanTotal = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.TimeFactTotal = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.DeviationCol = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand4 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.Distance = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.TimeMove = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.SpeedAvg = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.StopsQTY = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.TimeStop = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand6 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.FuelStart = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.FuelEnd = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.FuelAddQty = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.FuelAdd = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.FuelSubQty = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.FuelSub = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.FuelExpense = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.FuelExpensAvg = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand3 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.Fuel_ExpensTotal = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.Fuel_ExpensMove = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.Fuel_ExpensStop = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.Fuel_ExpensAvg = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridBand7 = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.SensQTY = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.gridQuontity = new DevExpress.XtraGrid.Views.BandedGrid.GridBand();
            this.Validity = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.pbValidity = new DevExpress.XtraEditors.Repository.RepositoryItemProgressBar();
            this.PointsIntervalMax = new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn();
            this.ZoneLookUp = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.xtbView = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.tlDictionary = new DevExpress.XtraTreeList.TreeList();
            this.DicCode = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.DicName = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.imCollection = new DevExpress.Utils.ImageCollection(this.components);
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.panel1 = new DevExpress.XtraEditors.PanelControl();
            this.leType = new DevExpress.XtraEditors.LookUpEdit();
            this.lbType = new DevExpress.XtraEditors.LabelControl();
            this.leMobitel = new DevExpress.XtraEditors.LookUpEdit();
            this.btStart = new DevExpress.XtraEditors.SimpleButton();
            this.sbtFilterClear = new DevExpress.XtraEditors.SimpleButton();
            this.sbtAutoCreate = new DevExpress.XtraEditors.SimpleButton();
            this.leGroupe = new DevExpress.XtraEditors.LookUpEdit();
            this.lbCar = new DevExpress.XtraEditors.LabelControl();
            this.lbCarGroupe = new DevExpress.XtraEditors.LabelControl();
            this.lbDateTo = new DevExpress.XtraEditors.LabelControl();
            this.lbDateFrom = new DevExpress.XtraEditors.LabelControl();
            this.deEnd = new DevExpress.XtraEditors.DateEdit();
            this.deStart = new DevExpress.XtraEditors.DateEdit();
            this.xtraTabPage3 = new DevExpress.XtraTab.XtraTabPage();
            this.gcReport = new DevExpress.XtraGrid.GridControl();
            this.gvReport = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPage4 = new DevExpress.XtraTab.XtraTabPage();
            this.pnOnline = new DevExpress.XtraEditors.PanelControl();
            this.btImportXml = new DevExpress.XtraBars.BarButtonItem();
            ((System.ComponentModel.ISupportInitialize)(this.gvRoute)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leRouteGrp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcDict)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvShedule)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvRouteGroups)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bmRouteGrn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rpbStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvTaskContent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EventLookUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcTask)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvTask)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RouteLookUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MobitelLookUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DriverLookUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbValidity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ZoneLookUp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtbView)).BeginInit();
            this.xtbView.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tlDictionary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imCollection)).BeginInit();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panel1)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.leType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leMobitel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leGroupe.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEnd.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEnd.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deStart.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deStart.Properties)).BeginInit();
            this.xtraTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvReport)).BeginInit();
            this.xtraTabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnOnline)).BeginInit();
            this.SuspendLayout();
            // 
            // gvRoute
            // 
            this.gvRoute.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvRoute.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvRoute.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gvRoute.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvRoute.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvRoute.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvRoute.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvRoute.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvRoute.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gvRoute.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvRoute.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvRoute.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvRoute.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvRoute.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gvRoute.Appearance.Empty.Options.UseBackColor = true;
            this.gvRoute.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvRoute.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvRoute.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gvRoute.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvRoute.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gvRoute.Appearance.EvenRow.Options.UseForeColor = true;
            this.gvRoute.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvRoute.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvRoute.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gvRoute.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvRoute.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvRoute.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gvRoute.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvRoute.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gvRoute.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvRoute.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvRoute.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvRoute.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.gvRoute.Appearance.FixedLine.Options.UseBackColor = true;
            this.gvRoute.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gvRoute.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gvRoute.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gvRoute.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gvRoute.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(192)))), ((int)(((byte)(157)))));
            this.gvRoute.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(219)))), ((int)(((byte)(188)))));
            this.gvRoute.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gvRoute.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvRoute.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gvRoute.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvRoute.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvRoute.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvRoute.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvRoute.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvRoute.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvRoute.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gvRoute.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvRoute.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvRoute.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvRoute.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvRoute.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvRoute.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvRoute.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gvRoute.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvRoute.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvRoute.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gvRoute.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(242)))), ((int)(((byte)(213)))));
            this.gvRoute.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gvRoute.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gvRoute.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvRoute.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvRoute.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvRoute.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvRoute.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gvRoute.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvRoute.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gvRoute.Appearance.GroupRow.Options.UseForeColor = true;
            this.gvRoute.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvRoute.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvRoute.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gvRoute.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvRoute.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvRoute.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gvRoute.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvRoute.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvRoute.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gvRoute.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvRoute.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.gvRoute.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gvRoute.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvRoute.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvRoute.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvRoute.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvRoute.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gvRoute.Appearance.OddRow.Options.UseBackColor = true;
            this.gvRoute.Appearance.OddRow.Options.UseBorderColor = true;
            this.gvRoute.Appearance.OddRow.Options.UseForeColor = true;
            this.gvRoute.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(247)))));
            this.gvRoute.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gvRoute.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(148)))), ((int)(((byte)(148)))));
            this.gvRoute.Appearance.Preview.Options.UseBackColor = true;
            this.gvRoute.Appearance.Preview.Options.UseFont = true;
            this.gvRoute.Appearance.Preview.Options.UseForeColor = true;
            this.gvRoute.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvRoute.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gvRoute.Appearance.Row.Options.UseBackColor = true;
            this.gvRoute.Appearance.Row.Options.UseForeColor = true;
            this.gvRoute.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvRoute.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gvRoute.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvRoute.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.gvRoute.Appearance.SelectedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvRoute.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.gvRoute.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvRoute.Appearance.SelectedRow.Options.UseBorderColor = true;
            this.gvRoute.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gvRoute.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gvRoute.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gvRoute.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvRoute.Appearance.VertLine.Options.UseBackColor = true;
            this.gvRoute.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.RouteId,
            this.RouteGroupeName,
            this.RouteName,
            this.RoutePath,
            this.RouteRemark});
            this.gvRoute.GridControl = this.gcDict;
            this.gvRoute.Name = "gvRoute";
            this.gvRoute.OptionsBehavior.AutoSelectAllInEditor = false;
            this.gvRoute.OptionsBehavior.Editable = false;
            this.gvRoute.OptionsSelection.MultiSelect = true;
            this.gvRoute.OptionsView.EnableAppearanceEvenRow = true;
            this.gvRoute.OptionsView.EnableAppearanceOddRow = true;
            this.gvRoute.DoubleClick += new System.EventHandler(this.gvRoute_DoubleClick);
            // 
            // RouteId
            // 
            this.RouteId.Caption = "Id";
            this.RouteId.FieldName = "Id";
            this.RouteId.Name = "RouteId";
            // 
            // RouteGroupeName
            // 
            this.RouteGroupeName.AppearanceHeader.Options.UseTextOptions = true;
            this.RouteGroupeName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.RouteGroupeName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.RouteGroupeName.Caption = "������";
            this.RouteGroupeName.ColumnEdit = this.leRouteGrp;
            this.RouteGroupeName.FieldName = "Id_main";
            this.RouteGroupeName.Name = "RouteGroupeName";
            this.RouteGroupeName.Visible = true;
            this.RouteGroupeName.VisibleIndex = 0;
            this.RouteGroupeName.Width = 190;
            // 
            // leRouteGrp
            // 
            this.leRouteGrp.AutoHeight = false;
            this.leRouteGrp.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leRouteGrp.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "��������")});
            this.leRouteGrp.DisplayMember = "Name";
            this.leRouteGrp.Name = "leRouteGrp";
            this.leRouteGrp.ValueMember = "Id";
            // 
            // RouteName
            // 
            this.RouteName.AppearanceHeader.Options.UseTextOptions = true;
            this.RouteName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.RouteName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.RouteName.Caption = "��������";
            this.RouteName.FieldName = "Name";
            this.RouteName.Name = "RouteName";
            this.RouteName.Visible = true;
            this.RouteName.VisibleIndex = 1;
            this.RouteName.Width = 367;
            // 
            // RoutePath
            // 
            this.RoutePath.AppearanceCell.Options.UseTextOptions = true;
            this.RoutePath.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.RoutePath.AppearanceHeader.Options.UseTextOptions = true;
            this.RoutePath.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.RoutePath.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.RoutePath.Caption = "����, ��";
            this.RoutePath.FieldName = "Distance";
            this.RoutePath.Name = "RoutePath";
            this.RoutePath.Visible = true;
            this.RoutePath.VisibleIndex = 2;
            this.RoutePath.Width = 95;
            // 
            // RouteRemark
            // 
            this.RouteRemark.AppearanceHeader.Options.UseTextOptions = true;
            this.RouteRemark.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.RouteRemark.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.RouteRemark.Caption = "����������";
            this.RouteRemark.FieldName = "Remark";
            this.RouteRemark.Name = "RouteRemark";
            this.RouteRemark.Visible = true;
            this.RouteRemark.VisibleIndex = 3;
            this.RouteRemark.Width = 438;
            // 
            // gcDict
            // 
            this.gcDict.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.LevelTemplate = this.gvRoute;
            gridLevelNode1.RelationName = "Level1";
            gridLevelNode2.LevelTemplate = this.gvShedule;
            gridLevelNode2.RelationName = "Level2";
            this.gcDict.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1,
            gridLevelNode2});
            this.gcDict.Location = new System.Drawing.Point(0, 0);
            this.gcDict.MainView = this.gvRouteGroups;
            this.gcDict.MenuManager = this.bmRouteGrn;
            this.gcDict.Name = "gcDict";
            this.gcDict.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.leRouteGrp});
            this.gcDict.Size = new System.Drawing.Size(825, 506);
            this.gcDict.TabIndex = 0;
            this.gcDict.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvShedule,
            this.gvRouteGroups,
            this.gvRoute});
            // 
            // gvShedule
            // 
            this.gvShedule.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvShedule.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvShedule.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gvShedule.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvShedule.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvShedule.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvShedule.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvShedule.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvShedule.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gvShedule.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvShedule.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvShedule.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvShedule.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvShedule.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gvShedule.Appearance.Empty.Options.UseBackColor = true;
            this.gvShedule.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvShedule.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvShedule.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gvShedule.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvShedule.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gvShedule.Appearance.EvenRow.Options.UseForeColor = true;
            this.gvShedule.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvShedule.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvShedule.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gvShedule.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvShedule.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvShedule.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gvShedule.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvShedule.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gvShedule.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvShedule.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvShedule.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvShedule.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.gvShedule.Appearance.FixedLine.Options.UseBackColor = true;
            this.gvShedule.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gvShedule.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gvShedule.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gvShedule.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gvShedule.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(192)))), ((int)(((byte)(157)))));
            this.gvShedule.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(219)))), ((int)(((byte)(188)))));
            this.gvShedule.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gvShedule.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvShedule.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gvShedule.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvShedule.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvShedule.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvShedule.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvShedule.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvShedule.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvShedule.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gvShedule.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvShedule.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvShedule.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvShedule.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvShedule.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvShedule.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvShedule.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gvShedule.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvShedule.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvShedule.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gvShedule.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(242)))), ((int)(((byte)(213)))));
            this.gvShedule.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gvShedule.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gvShedule.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvShedule.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvShedule.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvShedule.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvShedule.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gvShedule.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvShedule.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gvShedule.Appearance.GroupRow.Options.UseForeColor = true;
            this.gvShedule.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvShedule.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvShedule.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gvShedule.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvShedule.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvShedule.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gvShedule.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvShedule.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvShedule.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gvShedule.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvShedule.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.gvShedule.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gvShedule.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvShedule.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvShedule.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvShedule.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvShedule.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gvShedule.Appearance.OddRow.Options.UseBackColor = true;
            this.gvShedule.Appearance.OddRow.Options.UseBorderColor = true;
            this.gvShedule.Appearance.OddRow.Options.UseForeColor = true;
            this.gvShedule.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(247)))));
            this.gvShedule.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gvShedule.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(148)))), ((int)(((byte)(148)))));
            this.gvShedule.Appearance.Preview.Options.UseBackColor = true;
            this.gvShedule.Appearance.Preview.Options.UseFont = true;
            this.gvShedule.Appearance.Preview.Options.UseForeColor = true;
            this.gvShedule.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvShedule.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gvShedule.Appearance.Row.Options.UseBackColor = true;
            this.gvShedule.Appearance.Row.Options.UseForeColor = true;
            this.gvShedule.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvShedule.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gvShedule.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvShedule.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.gvShedule.Appearance.SelectedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvShedule.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.gvShedule.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvShedule.Appearance.SelectedRow.Options.UseBorderColor = true;
            this.gvShedule.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gvShedule.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gvShedule.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gvShedule.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvShedule.Appearance.VertLine.Options.UseBackColor = true;
            this.gvShedule.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colId,
            this.colName,
            this.colZone,
            this.colComment});
            this.gvShedule.GridControl = this.gcDict;
            this.gvShedule.Name = "gvShedule";
            this.gvShedule.OptionsBehavior.Editable = false;
            this.gvShedule.OptionsSelection.MultiSelect = true;
            this.gvShedule.OptionsView.EnableAppearanceEvenRow = true;
            this.gvShedule.OptionsView.EnableAppearanceOddRow = true;
            this.gvShedule.DoubleClick += new System.EventHandler(this.gvShedule_DoubleClick);
            // 
            // colId
            // 
            this.colId.Caption = "Id";
            this.colId.FieldName = "Id";
            this.colId.Name = "colId";
            this.colId.OptionsColumn.AllowEdit = false;
            this.colId.OptionsColumn.ReadOnly = true;
            // 
            // colName
            // 
            this.colName.AppearanceHeader.Options.UseTextOptions = true;
            this.colName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colName.Caption = "��������";
            this.colName.FieldName = "Name";
            this.colName.Name = "colName";
            this.colName.OptionsColumn.AllowEdit = false;
            this.colName.OptionsColumn.ReadOnly = true;
            this.colName.Visible = true;
            this.colName.VisibleIndex = 0;
            // 
            // colZone
            // 
            this.colZone.AppearanceHeader.Options.UseTextOptions = true;
            this.colZone.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colZone.Caption = "������� ����������� ����";
            this.colZone.FieldName = "ZoneName";
            this.colZone.Name = "colZone";
            this.colZone.OptionsColumn.AllowEdit = false;
            this.colZone.OptionsColumn.ReadOnly = true;
            this.colZone.Visible = true;
            this.colZone.VisibleIndex = 1;
            // 
            // colComment
            // 
            this.colComment.AppearanceHeader.Options.UseTextOptions = true;
            this.colComment.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colComment.Caption = "����������";
            this.colComment.FieldName = "Remark";
            this.colComment.Name = "colComment";
            this.colComment.OptionsColumn.AllowEdit = false;
            this.colComment.OptionsColumn.ReadOnly = true;
            this.colComment.Visible = true;
            this.colComment.VisibleIndex = 2;
            // 
            // gvRouteGroups
            // 
            this.gvRouteGroups.ActiveFilterEnabled = false;
            this.gvRouteGroups.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvRouteGroups.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvRouteGroups.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gvRouteGroups.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvRouteGroups.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvRouteGroups.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvRouteGroups.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvRouteGroups.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvRouteGroups.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gvRouteGroups.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvRouteGroups.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvRouteGroups.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvRouteGroups.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvRouteGroups.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gvRouteGroups.Appearance.Empty.Options.UseBackColor = true;
            this.gvRouteGroups.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvRouteGroups.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvRouteGroups.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gvRouteGroups.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvRouteGroups.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gvRouteGroups.Appearance.EvenRow.Options.UseForeColor = true;
            this.gvRouteGroups.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvRouteGroups.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvRouteGroups.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gvRouteGroups.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvRouteGroups.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvRouteGroups.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gvRouteGroups.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvRouteGroups.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gvRouteGroups.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvRouteGroups.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvRouteGroups.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvRouteGroups.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.gvRouteGroups.Appearance.FixedLine.Options.UseBackColor = true;
            this.gvRouteGroups.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gvRouteGroups.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gvRouteGroups.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gvRouteGroups.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gvRouteGroups.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(192)))), ((int)(((byte)(157)))));
            this.gvRouteGroups.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(219)))), ((int)(((byte)(188)))));
            this.gvRouteGroups.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gvRouteGroups.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvRouteGroups.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gvRouteGroups.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvRouteGroups.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvRouteGroups.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvRouteGroups.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvRouteGroups.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvRouteGroups.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvRouteGroups.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gvRouteGroups.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvRouteGroups.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvRouteGroups.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvRouteGroups.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvRouteGroups.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvRouteGroups.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvRouteGroups.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gvRouteGroups.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvRouteGroups.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvRouteGroups.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gvRouteGroups.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(242)))), ((int)(((byte)(213)))));
            this.gvRouteGroups.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gvRouteGroups.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gvRouteGroups.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvRouteGroups.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvRouteGroups.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvRouteGroups.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvRouteGroups.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gvRouteGroups.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvRouteGroups.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gvRouteGroups.Appearance.GroupRow.Options.UseForeColor = true;
            this.gvRouteGroups.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvRouteGroups.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvRouteGroups.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gvRouteGroups.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvRouteGroups.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvRouteGroups.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gvRouteGroups.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvRouteGroups.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvRouteGroups.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gvRouteGroups.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvRouteGroups.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.gvRouteGroups.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gvRouteGroups.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvRouteGroups.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvRouteGroups.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvRouteGroups.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvRouteGroups.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gvRouteGroups.Appearance.OddRow.Options.UseBackColor = true;
            this.gvRouteGroups.Appearance.OddRow.Options.UseBorderColor = true;
            this.gvRouteGroups.Appearance.OddRow.Options.UseForeColor = true;
            this.gvRouteGroups.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(247)))));
            this.gvRouteGroups.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gvRouteGroups.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(148)))), ((int)(((byte)(148)))));
            this.gvRouteGroups.Appearance.Preview.Options.UseBackColor = true;
            this.gvRouteGroups.Appearance.Preview.Options.UseFont = true;
            this.gvRouteGroups.Appearance.Preview.Options.UseForeColor = true;
            this.gvRouteGroups.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvRouteGroups.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gvRouteGroups.Appearance.Row.Options.UseBackColor = true;
            this.gvRouteGroups.Appearance.Row.Options.UseForeColor = true;
            this.gvRouteGroups.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvRouteGroups.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gvRouteGroups.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvRouteGroups.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.gvRouteGroups.Appearance.SelectedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvRouteGroups.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.gvRouteGroups.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvRouteGroups.Appearance.SelectedRow.Options.UseBorderColor = true;
            this.gvRouteGroups.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gvRouteGroups.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gvRouteGroups.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gvRouteGroups.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvRouteGroups.Appearance.VertLine.Options.UseBackColor = true;
            this.gvRouteGroups.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.RouteGroupeId,
            this.GroupeName,
            this.GroupeRemark});
            this.gvRouteGroups.GridControl = this.gcDict;
            this.gvRouteGroups.Name = "gvRouteGroups";
            this.gvRouteGroups.OptionsBehavior.Editable = false;
            this.gvRouteGroups.OptionsSelection.MultiSelect = true;
            this.gvRouteGroups.OptionsView.EnableAppearanceEvenRow = true;
            this.gvRouteGroups.OptionsView.EnableAppearanceOddRow = true;
            this.gvRouteGroups.DoubleClick += new System.EventHandler(this.gvRouteGroups_DoubleClick);
            // 
            // RouteGroupeId
            // 
            this.RouteGroupeId.Caption = "Id";
            this.RouteGroupeId.FieldName = "Id";
            this.RouteGroupeId.Name = "RouteGroupeId";
            // 
            // GroupeName
            // 
            this.GroupeName.AppearanceHeader.Options.UseTextOptions = true;
            this.GroupeName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GroupeName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.GroupeName.Caption = "��������";
            this.GroupeName.FieldName = "Name";
            this.GroupeName.Name = "GroupeName";
            this.GroupeName.Visible = true;
            this.GroupeName.VisibleIndex = 0;
            this.GroupeName.Width = 371;
            // 
            // GroupeRemark
            // 
            this.GroupeRemark.AppearanceHeader.Options.UseTextOptions = true;
            this.GroupeRemark.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.GroupeRemark.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.GroupeRemark.Caption = "����������";
            this.GroupeRemark.FieldName = "Remark";
            this.GroupeRemark.Name = "GroupeRemark";
            this.GroupeRemark.Visible = true;
            this.GroupeRemark.VisibleIndex = 1;
            this.GroupeRemark.Width = 719;
            // 
            // bmRouteGrn
            // 
            this.bmRouteGrn.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2,
            this.bar3});
            this.bmRouteGrn.DockControls.Add(this.barDockControlTop);
            this.bmRouteGrn.DockControls.Add(this.barDockControlBottom);
            this.bmRouteGrn.DockControls.Add(this.barDockControlLeft);
            this.bmRouteGrn.DockControls.Add(this.barDockControlRight);
            this.bmRouteGrn.Form = this;
            this.bmRouteGrn.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.btAdd,
            this.btDelete,
            this.btEdit,
            this.btRefresh,
            this.bsiExcel,
            this.btExcel,
            this.btExcelBrief,
            this.bsiStatus,
            this.bsiSettings,
            this.btColumnsSet,
            this.btColumnsSave,
            this.btColumnRestore,
            this.btFact,
            this.pbStatus,
            this.btSettings,
            this.btImportXml});
            this.bmRouteGrn.MainMenu = this.bar2;
            this.bmRouteGrn.MaxItemId = 21;
            this.bmRouteGrn.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.rpbStatus});
            this.bmRouteGrn.StatusBar = this.bar3;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btAdd, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btDelete, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btEdit, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btRefresh, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btFact, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bsiExcel, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btImportXml, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bsiSettings, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.btSettings, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // btAdd
            // 
            this.btAdd.Caption = "��������";
            this.btAdd.Glyph = global::Route.Properties.ResourcesImages.Add_16;
            this.btAdd.Id = 0;
            this.btAdd.Name = "btAdd";
            this.btAdd.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btAdd_ItemClick);
            // 
            // btDelete
            // 
            this.btDelete.Caption = "�������";
            this.btDelete.Glyph = global::Route.Properties.ResourcesImages.Remove_16;
            this.btDelete.Id = 1;
            this.btDelete.Name = "btDelete";
            this.btDelete.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btDelete_ItemClick);
            // 
            // btEdit
            // 
            this.btEdit.Caption = "�������������";
            this.btEdit.Glyph = global::Route.Properties.ResourcesImages.Edit_P_02_18;
            this.btEdit.Id = 2;
            this.btEdit.Name = "btEdit";
            this.btEdit.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btEdit_ItemClick);
            // 
            // btRefresh
            // 
            this.btRefresh.Caption = "��������";
            this.btRefresh.Glyph = global::Route.Properties.ResourcesImages.ref_16;
            this.btRefresh.Id = 3;
            this.btRefresh.Name = "btRefresh";
            this.btRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btRefresh_ItemClick);
            // 
            // btFact
            // 
            this.btFact.Caption = "����";
            this.btFact.Glyph = global::Route.Properties.ResourcesImages.calculator;
            this.btFact.Id = 17;
            this.btFact.Name = "btFact";
            toolTipItem1.Text = "����������� ����������� ���������� ���������� ���������� ������";
            superToolTip1.Items.Add(toolTipItem1);
            this.btFact.SuperTip = superToolTip1;
            this.btFact.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btFact_ItemClick);
            // 
            // bsiExcel
            // 
            this.bsiExcel.Caption = "������� � Excel";
            this.bsiExcel.Glyph = global::Route.Properties.ResourcesImages.doc_excel_table;
            this.bsiExcel.Id = 5;
            this.bsiExcel.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btExcel),
            new DevExpress.XtraBars.LinkPersistInfo(this.btExcelBrief)});
            this.bsiExcel.Name = "bsiExcel";
            // 
            // btExcel
            // 
            this.btExcel.Caption = "������";
            this.btExcel.Id = 7;
            this.btExcel.Name = "btExcel";
            this.btExcel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btExcel_ItemClick);
            // 
            // btExcelBrief
            // 
            this.btExcelBrief.Caption = "�������";
            this.btExcelBrief.Id = 8;
            this.btExcelBrief.Name = "btExcelBrief";
            this.btExcelBrief.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btExcelBrief_ItemClick);
            // 
            // bsiSettings
            // 
            this.bsiSettings.Caption = "��������� �������";
            this.bsiSettings.Glyph = ((System.Drawing.Image)(resources.GetObject("bsiSettings.Glyph")));
            this.bsiSettings.Id = 10;
            this.bsiSettings.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.btColumnsSet),
            new DevExpress.XtraBars.LinkPersistInfo(this.btColumnsSave),
            new DevExpress.XtraBars.LinkPersistInfo(this.btColumnRestore)});
            this.bsiSettings.Name = "bsiSettings";
            // 
            // btColumnsSet
            // 
            this.btColumnsSet.Caption = "����� ��������";
            this.btColumnsSet.Glyph = ((System.Drawing.Image)(resources.GetObject("btColumnsSet.Glyph")));
            this.btColumnsSet.Id = 12;
            this.btColumnsSet.Name = "btColumnsSet";
            this.btColumnsSet.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btColumnsSet_ItemClick);
            // 
            // btColumnsSave
            // 
            this.btColumnsSave.Caption = "���������";
            this.btColumnsSave.Glyph = ((System.Drawing.Image)(resources.GetObject("btColumnsSave.Glyph")));
            this.btColumnsSave.Id = 13;
            this.btColumnsSave.Name = "btColumnsSave";
            this.btColumnsSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btColumnsSave_ItemClick);
            // 
            // btColumnRestore
            // 
            this.btColumnRestore.Caption = "������������";
            this.btColumnRestore.Glyph = ((System.Drawing.Image)(resources.GetObject("btColumnRestore.Glyph")));
            this.btColumnRestore.Id = 16;
            this.btColumnRestore.Name = "btColumnRestore";
            this.btColumnRestore.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btColumnRestore_ItemClick);
            // 
            // btSettings
            // 
            this.btSettings.Caption = "���������";
            this.btSettings.Glyph = ((System.Drawing.Image)(resources.GetObject("btSettings.Glyph")));
            this.btSettings.Id = 19;
            this.btSettings.Name = "btSettings";
            this.btSettings.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btSettings_ItemClick);
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.Width, this.pbStatus, "", false, true, true, 173),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiStatus)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // pbStatus
            // 
            this.pbStatus.Caption = "barEditItem1";
            this.pbStatus.Edit = this.rpbStatus;
            this.pbStatus.Id = 18;
            this.pbStatus.Name = "pbStatus";
            this.pbStatus.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // rpbStatus
            // 
            this.rpbStatus.Name = "rpbStatus";
            // 
            // bsiStatus
            // 
            this.bsiStatus.Caption = "������";
            this.bsiStatus.Id = 9;
            this.bsiStatus.Name = "bsiStatus";
            this.bsiStatus.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(1052, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 560);
            this.barDockControlBottom.Size = new System.Drawing.Size(1052, 25);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 534);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1052, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 534);
            // 
            // gvTaskContent
            // 
            this.gvTaskContent.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvTaskContent.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvTaskContent.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gvTaskContent.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvTaskContent.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvTaskContent.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvTaskContent.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvTaskContent.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvTaskContent.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gvTaskContent.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvTaskContent.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvTaskContent.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvTaskContent.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvTaskContent.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gvTaskContent.Appearance.Empty.Options.UseBackColor = true;
            this.gvTaskContent.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvTaskContent.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvTaskContent.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gvTaskContent.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvTaskContent.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gvTaskContent.Appearance.EvenRow.Options.UseForeColor = true;
            this.gvTaskContent.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvTaskContent.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvTaskContent.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gvTaskContent.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvTaskContent.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvTaskContent.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gvTaskContent.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvTaskContent.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gvTaskContent.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvTaskContent.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvTaskContent.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvTaskContent.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.gvTaskContent.Appearance.FixedLine.Options.UseBackColor = true;
            this.gvTaskContent.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gvTaskContent.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gvTaskContent.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gvTaskContent.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gvTaskContent.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(192)))), ((int)(((byte)(157)))));
            this.gvTaskContent.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(219)))), ((int)(((byte)(188)))));
            this.gvTaskContent.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gvTaskContent.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvTaskContent.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gvTaskContent.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvTaskContent.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvTaskContent.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvTaskContent.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvTaskContent.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvTaskContent.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvTaskContent.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gvTaskContent.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvTaskContent.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvTaskContent.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvTaskContent.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvTaskContent.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvTaskContent.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvTaskContent.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gvTaskContent.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvTaskContent.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvTaskContent.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gvTaskContent.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(242)))), ((int)(((byte)(213)))));
            this.gvTaskContent.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gvTaskContent.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gvTaskContent.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvTaskContent.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvTaskContent.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvTaskContent.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvTaskContent.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gvTaskContent.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvTaskContent.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gvTaskContent.Appearance.GroupRow.Options.UseForeColor = true;
            this.gvTaskContent.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvTaskContent.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvTaskContent.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gvTaskContent.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvTaskContent.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvTaskContent.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gvTaskContent.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvTaskContent.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvTaskContent.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gvTaskContent.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvTaskContent.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.gvTaskContent.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gvTaskContent.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvTaskContent.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvTaskContent.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvTaskContent.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvTaskContent.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gvTaskContent.Appearance.OddRow.Options.UseBackColor = true;
            this.gvTaskContent.Appearance.OddRow.Options.UseBorderColor = true;
            this.gvTaskContent.Appearance.OddRow.Options.UseForeColor = true;
            this.gvTaskContent.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(247)))));
            this.gvTaskContent.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gvTaskContent.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(148)))), ((int)(((byte)(148)))));
            this.gvTaskContent.Appearance.Preview.Options.UseBackColor = true;
            this.gvTaskContent.Appearance.Preview.Options.UseFont = true;
            this.gvTaskContent.Appearance.Preview.Options.UseForeColor = true;
            this.gvTaskContent.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvTaskContent.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gvTaskContent.Appearance.Row.Options.UseBackColor = true;
            this.gvTaskContent.Appearance.Row.Options.UseForeColor = true;
            this.gvTaskContent.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvTaskContent.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gvTaskContent.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvTaskContent.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.gvTaskContent.Appearance.SelectedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvTaskContent.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.gvTaskContent.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvTaskContent.Appearance.SelectedRow.Options.UseBorderColor = true;
            this.gvTaskContent.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gvTaskContent.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gvTaskContent.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gvTaskContent.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvTaskContent.Appearance.VertLine.Options.UseBackColor = true;
            this.gvTaskContent.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.Id_cont,
            this.LocationM,
            this.Id_event,
            this.DatePlan,
            this.DateFact,
            this.Deviation,
            this.DistanceT,
            this.Remark});
            this.gvTaskContent.GridControl = this.gcTask;
            this.gvTaskContent.Name = "gvTaskContent";
            this.gvTaskContent.OptionsView.EnableAppearanceEvenRow = true;
            this.gvTaskContent.OptionsView.EnableAppearanceOddRow = true;
            this.gvTaskContent.OptionsView.ShowGroupPanel = false;
            // 
            // Id_cont
            // 
            this.Id_cont.Caption = "Id";
            this.Id_cont.FieldName = "Id";
            this.Id_cont.Name = "Id_cont";
            this.Id_cont.OptionsColumn.AllowEdit = false;
            // 
            // LocationM
            // 
            this.LocationM.AppearanceHeader.Options.UseTextOptions = true;
            this.LocationM.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.LocationM.Caption = "��������������";
            this.LocationM.FieldName = "Location";
            this.LocationM.Name = "LocationM";
            this.LocationM.OptionsColumn.AllowEdit = false;
            this.LocationM.Visible = true;
            this.LocationM.VisibleIndex = 0;
            this.LocationM.Width = 169;
            // 
            // Id_event
            // 
            this.Id_event.AppearanceHeader.Options.UseTextOptions = true;
            this.Id_event.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Id_event.Caption = "�������";
            this.Id_event.ColumnEdit = this.EventLookUp;
            this.Id_event.FieldName = "Id_event";
            this.Id_event.Name = "Id_event";
            this.Id_event.OptionsColumn.AllowEdit = false;
            this.Id_event.Visible = true;
            this.Id_event.VisibleIndex = 1;
            this.Id_event.Width = 98;
            // 
            // EventLookUp
            // 
            this.EventLookUp.AutoHeight = false;
            this.EventLookUp.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.EventLookUp.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "��������")});
            this.EventLookUp.DisplayMember = "Name";
            this.EventLookUp.Name = "EventLookUp";
            this.EventLookUp.ValueMember = "Id";
            // 
            // DatePlan
            // 
            this.DatePlan.AppearanceCell.Options.UseTextOptions = true;
            this.DatePlan.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DatePlan.AppearanceHeader.Options.UseTextOptions = true;
            this.DatePlan.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DatePlan.Caption = "�������� �����";
            this.DatePlan.DisplayFormat.FormatString = "F";
            this.DatePlan.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DatePlan.FieldName = "DatePlan";
            this.DatePlan.Name = "DatePlan";
            this.DatePlan.OptionsColumn.AllowEdit = false;
            this.DatePlan.Visible = true;
            this.DatePlan.VisibleIndex = 2;
            this.DatePlan.Width = 186;
            // 
            // DateFact
            // 
            this.DateFact.AppearanceCell.Options.UseTextOptions = true;
            this.DateFact.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DateFact.AppearanceHeader.Options.UseTextOptions = true;
            this.DateFact.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DateFact.Caption = "����������� ����� ";
            this.DateFact.DisplayFormat.FormatString = "F";
            this.DateFact.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.DateFact.FieldName = "DateFact";
            this.DateFact.Name = "DateFact";
            this.DateFact.OptionsColumn.AllowEdit = false;
            this.DateFact.Visible = true;
            this.DateFact.VisibleIndex = 3;
            this.DateFact.Width = 186;
            // 
            // Deviation
            // 
            this.Deviation.AppearanceCell.Options.UseTextOptions = true;
            this.Deviation.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Deviation.AppearanceHeader.Options.UseTextOptions = true;
            this.Deviation.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Deviation.Caption = "����������";
            this.Deviation.FieldName = "Deviation";
            this.Deviation.Name = "Deviation";
            this.Deviation.OptionsColumn.AllowEdit = false;
            this.Deviation.Visible = true;
            this.Deviation.VisibleIndex = 4;
            this.Deviation.Width = 97;
            // 
            // DistanceT
            // 
            this.DistanceT.AppearanceHeader.Options.UseTextOptions = true;
            this.DistanceT.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DistanceT.Caption = "����������, ��";
            this.DistanceT.FieldName = "Distance";
            this.DistanceT.Name = "DistanceT";
            this.DistanceT.OptionsColumn.AllowEdit = false;
            this.DistanceT.Visible = true;
            this.DistanceT.VisibleIndex = 5;
            this.DistanceT.Width = 171;
            // 
            // Remark
            // 
            this.Remark.AppearanceHeader.Options.UseTextOptions = true;
            this.Remark.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Remark.Caption = "����������";
            this.Remark.FieldName = "Remark";
            this.Remark.Name = "Remark";
            this.Remark.OptionsColumn.AllowEdit = false;
            this.Remark.Visible = true;
            this.Remark.VisibleIndex = 6;
            this.Remark.Width = 183;
            // 
            // gcTask
            // 
            this.gcTask.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            gridLevelNode3.LevelTemplate = this.gvTaskContent;
            gridLevelNode3.RelationName = "Level1";
            this.gcTask.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode3});
            this.gcTask.Location = new System.Drawing.Point(0, 63);
            this.gcTask.MainView = this.gvTask;
            this.gcTask.MenuManager = this.bmRouteGrn;
            this.gcTask.Name = "gcTask";
            this.gcTask.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.RouteLookUp,
            this.MobitelLookUp,
            this.DriverLookUp,
            this.EventLookUp,
            this.ZoneLookUp,
            this.pbValidity});
            this.gcTask.Size = new System.Drawing.Size(1052, 448);
            this.gcTask.TabIndex = 9;
            this.gcTask.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvTask,
            this.gvTaskContent});
            // 
            // gvTask
            // 
            this.gvTask.ActiveFilterEnabled = false;
            this.gvTask.Appearance.BandPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvTask.Appearance.BandPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvTask.Appearance.BandPanel.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.gvTask.Appearance.BandPanel.ForeColor = System.Drawing.Color.Black;
            this.gvTask.Appearance.BandPanel.Options.UseBackColor = true;
            this.gvTask.Appearance.BandPanel.Options.UseBorderColor = true;
            this.gvTask.Appearance.BandPanel.Options.UseFont = true;
            this.gvTask.Appearance.BandPanel.Options.UseForeColor = true;
            this.gvTask.Appearance.BandPanelBackground.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvTask.Appearance.BandPanelBackground.BackColor2 = System.Drawing.Color.White;
            this.gvTask.Appearance.BandPanelBackground.Options.UseBackColor = true;
            this.gvTask.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvTask.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvTask.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gvTask.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvTask.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvTask.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvTask.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvTask.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvTask.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gvTask.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvTask.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvTask.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvTask.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvTask.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gvTask.Appearance.Empty.Options.UseBackColor = true;
            this.gvTask.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvTask.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvTask.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gvTask.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvTask.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gvTask.Appearance.EvenRow.Options.UseForeColor = true;
            this.gvTask.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvTask.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvTask.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gvTask.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvTask.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvTask.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gvTask.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvTask.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gvTask.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvTask.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvTask.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvTask.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.gvTask.Appearance.FixedLine.Options.UseBackColor = true;
            this.gvTask.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gvTask.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gvTask.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gvTask.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gvTask.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(192)))), ((int)(((byte)(157)))));
            this.gvTask.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(219)))), ((int)(((byte)(188)))));
            this.gvTask.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gvTask.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvTask.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gvTask.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvTask.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvTask.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvTask.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvTask.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvTask.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvTask.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gvTask.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvTask.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvTask.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvTask.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvTask.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvTask.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvTask.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gvTask.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvTask.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvTask.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gvTask.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(242)))), ((int)(((byte)(213)))));
            this.gvTask.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gvTask.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gvTask.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvTask.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvTask.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvTask.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvTask.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gvTask.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvTask.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gvTask.Appearance.GroupRow.Options.UseForeColor = true;
            this.gvTask.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvTask.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvTask.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gvTask.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvTask.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvTask.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gvTask.Appearance.HeaderPanelBackground.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvTask.Appearance.HeaderPanelBackground.BackColor2 = System.Drawing.Color.White;
            this.gvTask.Appearance.HeaderPanelBackground.Options.UseBackColor = true;
            this.gvTask.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvTask.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvTask.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gvTask.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvTask.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.gvTask.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gvTask.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvTask.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvTask.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvTask.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvTask.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gvTask.Appearance.OddRow.Options.UseBackColor = true;
            this.gvTask.Appearance.OddRow.Options.UseBorderColor = true;
            this.gvTask.Appearance.OddRow.Options.UseForeColor = true;
            this.gvTask.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(247)))));
            this.gvTask.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gvTask.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(148)))), ((int)(((byte)(148)))));
            this.gvTask.Appearance.Preview.Options.UseBackColor = true;
            this.gvTask.Appearance.Preview.Options.UseFont = true;
            this.gvTask.Appearance.Preview.Options.UseForeColor = true;
            this.gvTask.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvTask.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gvTask.Appearance.Row.Options.UseBackColor = true;
            this.gvTask.Appearance.Row.Options.UseForeColor = true;
            this.gvTask.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvTask.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gvTask.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvTask.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.gvTask.Appearance.SelectedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvTask.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.gvTask.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvTask.Appearance.SelectedRow.Options.UseBorderColor = true;
            this.gvTask.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gvTask.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gvTask.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gvTask.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvTask.Appearance.VertLine.Options.UseBackColor = true;
            this.gvTask.Bands.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.GridBand[] {
            this.gridBand1,
            this.gridBand2,
            this.gridBand4,
            this.gridBand6,
            this.gridBand3,
            this.gridBand7,
            this.gridQuontity});
            this.gvTask.ColumnPanelRowHeight = 35;
            this.gvTask.Columns.AddRange(new DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn[] {
            this.Id,
            this.Route,
            this.Id_mobitel,
            this.Id_driver,
            this.Distance,
            this.StopsQTY,
            this.TimeStop,
            this.TimeMove,
            this.SpeedAvg,
            this.SensQTY,
            this.RemarkTask,
            this.PLAN_START,
            this.FACT_START,
            this.PLAN_FIN,
            this.FACT_FIN,
            this.TimePlanTotal,
            this.TimeFactTotal,
            this.FuelStart,
            this.FuelEnd,
            this.FuelAdd,
            this.FuelSub,
            this.FuelExpense,
            this.FuelExpensAvg,
            this.Fuel_ExpensMove,
            this.Fuel_ExpensStop,
            this.Fuel_ExpensTotal,
            this.Fuel_ExpensAvg,
            this.FuelAddQty,
            this.FuelSubQty,
            this.DeviationCol,
            this.Validity,
            this.PointsIntervalMax});
            this.gvTask.GridControl = this.gcTask;
            this.gvTask.Name = "gvTask";
            this.gvTask.OptionsBehavior.Editable = false;
            this.gvTask.OptionsSelection.MultiSelect = true;
            this.gvTask.OptionsView.AllowHtmlDrawHeaders = true;
            this.gvTask.OptionsView.ColumnAutoWidth = false;
            this.gvTask.OptionsView.EnableAppearanceEvenRow = true;
            this.gvTask.OptionsView.EnableAppearanceOddRow = true;
            this.gvTask.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.gvTask_RowCellStyle);
            this.gvTask.DoubleClick += new System.EventHandler(this.gvTask_DoubleClick);
            // 
            // gridBand1
            // 
            this.gridBand1.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand1.Caption = "����� ����������";
            this.gridBand1.Columns.Add(this.Id);
            this.gridBand1.Columns.Add(this.Route);
            this.gridBand1.Columns.Add(this.Id_mobitel);
            this.gridBand1.Columns.Add(this.Id_driver);
            this.gridBand1.Columns.Add(this.RemarkTask);
            this.gridBand1.MinWidth = 20;
            this.gridBand1.Name = "gridBand1";
            this.gridBand1.VisibleIndex = 0;
            this.gridBand1.Width = 450;
            // 
            // Id
            // 
            this.Id.AppearanceCell.Options.UseTextOptions = true;
            this.Id.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Id.AppearanceHeader.Options.UseTextOptions = true;
            this.Id.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Id.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Id.Caption = "�����";
            this.Id.FieldName = "Id";
            this.Id.Name = "Id";
            this.Id.OptionsColumn.AllowEdit = false;
            this.Id.Visible = true;
            this.Id.Width = 50;
            // 
            // Route
            // 
            this.Route.AppearanceHeader.Options.UseTextOptions = true;
            this.Route.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Route.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Route.Caption = "�������";
            this.Route.ColumnEdit = this.RouteLookUp;
            this.Route.FieldName = "Id_sample";
            this.Route.Name = "Route";
            this.Route.OptionsColumn.AllowEdit = false;
            this.Route.OptionsColumn.ReadOnly = true;
            this.Route.Visible = true;
            this.Route.Width = 100;
            // 
            // RouteLookUp
            // 
            this.RouteLookUp.AutoHeight = false;
            this.RouteLookUp.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.RouteLookUp.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "��������")});
            this.RouteLookUp.DisplayMember = "Name";
            this.RouteLookUp.HideSelection = false;
            this.RouteLookUp.Name = "RouteLookUp";
            this.RouteLookUp.ValueMember = "Id";
            // 
            // Id_mobitel
            // 
            this.Id_mobitel.AppearanceHeader.Options.UseTextOptions = true;
            this.Id_mobitel.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Id_mobitel.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Id_mobitel.Caption = "������";
            this.Id_mobitel.ColumnEdit = this.MobitelLookUp;
            this.Id_mobitel.FieldName = "Id_mobitel";
            this.Id_mobitel.Name = "Id_mobitel";
            this.Id_mobitel.OptionsColumn.AllowEdit = false;
            this.Id_mobitel.OptionsColumn.ReadOnly = true;
            this.Id_mobitel.Visible = true;
            this.Id_mobitel.Width = 100;
            // 
            // MobitelLookUp
            // 
            this.MobitelLookUp.AutoHeight = false;
            this.MobitelLookUp.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.MobitelLookUp.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "��������")});
            this.MobitelLookUp.DisplayMember = "Name";
            this.MobitelLookUp.Name = "MobitelLookUp";
            this.MobitelLookUp.ValueMember = "Id";
            // 
            // Id_driver
            // 
            this.Id_driver.AppearanceHeader.Options.UseTextOptions = true;
            this.Id_driver.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Id_driver.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Id_driver.Caption = "��������";
            this.Id_driver.ColumnEdit = this.DriverLookUp;
            this.Id_driver.FieldName = "Id_driver";
            this.Id_driver.Name = "Id_driver";
            this.Id_driver.OptionsColumn.AllowEdit = false;
            this.Id_driver.OptionsColumn.ReadOnly = true;
            this.Id_driver.Visible = true;
            this.Id_driver.Width = 100;
            // 
            // DriverLookUp
            // 
            this.DriverLookUp.AutoHeight = false;
            this.DriverLookUp.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DriverLookUp.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "��������")});
            this.DriverLookUp.DisplayMember = "Name";
            this.DriverLookUp.Name = "DriverLookUp";
            this.DriverLookUp.ValueMember = "Id";
            // 
            // RemarkTask
            // 
            this.RemarkTask.AppearanceHeader.Options.UseTextOptions = true;
            this.RemarkTask.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.RemarkTask.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.RemarkTask.Caption = "�����������";
            this.RemarkTask.FieldName = "Remark";
            this.RemarkTask.Name = "RemarkTask";
            this.RemarkTask.OptionsColumn.AllowEdit = false;
            this.RemarkTask.OptionsColumn.ReadOnly = true;
            this.RemarkTask.Visible = true;
            this.RemarkTask.Width = 100;
            // 
            // gridBand2
            // 
            this.gridBand2.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand2.Caption = "�����";
            this.gridBand2.Columns.Add(this.PLAN_START);
            this.gridBand2.Columns.Add(this.FACT_START);
            this.gridBand2.Columns.Add(this.PLAN_FIN);
            this.gridBand2.Columns.Add(this.FACT_FIN);
            this.gridBand2.Columns.Add(this.TimePlanTotal);
            this.gridBand2.Columns.Add(this.TimeFactTotal);
            this.gridBand2.Columns.Add(this.DeviationCol);
            this.gridBand2.MinWidth = 20;
            this.gridBand2.Name = "gridBand2";
            this.gridBand2.VisibleIndex = 1;
            this.gridBand2.Width = 795;
            // 
            // PLAN_START
            // 
            this.PLAN_START.AppearanceCell.Options.UseTextOptions = true;
            this.PLAN_START.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.PLAN_START.AppearanceHeader.Options.UseTextOptions = true;
            this.PLAN_START.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.PLAN_START.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.PLAN_START.Caption = "����� (����)";
            this.PLAN_START.DisplayFormat.FormatString = "dd/MM/yyyy  HH:mm";
            this.PLAN_START.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.PLAN_START.FieldName = "PLAN_START";
            this.PLAN_START.Name = "PLAN_START";
            this.PLAN_START.OptionsColumn.AllowEdit = false;
            this.PLAN_START.OptionsColumn.ReadOnly = true;
            this.PLAN_START.Visible = true;
            this.PLAN_START.Width = 120;
            // 
            // FACT_START
            // 
            this.FACT_START.AppearanceCell.Options.UseTextOptions = true;
            this.FACT_START.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FACT_START.AppearanceHeader.Options.UseTextOptions = true;
            this.FACT_START.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FACT_START.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FACT_START.Caption = "����� (����)";
            this.FACT_START.DisplayFormat.FormatString = "dd/MM/yyyy  HH:mm";
            this.FACT_START.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.FACT_START.FieldName = "FACT_START";
            this.FACT_START.Name = "FACT_START";
            this.FACT_START.OptionsColumn.AllowEdit = false;
            this.FACT_START.OptionsColumn.ReadOnly = true;
            this.FACT_START.Visible = true;
            this.FACT_START.Width = 120;
            // 
            // PLAN_FIN
            // 
            this.PLAN_FIN.AppearanceCell.Options.UseTextOptions = true;
            this.PLAN_FIN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.PLAN_FIN.AppearanceHeader.Options.UseTextOptions = true;
            this.PLAN_FIN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.PLAN_FIN.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.PLAN_FIN.Caption = "����� (����)";
            this.PLAN_FIN.DisplayFormat.FormatString = "dd/MM/yyyy  HH:mm";
            this.PLAN_FIN.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.PLAN_FIN.FieldName = "PLAN_FIN";
            this.PLAN_FIN.Name = "PLAN_FIN";
            this.PLAN_FIN.OptionsColumn.AllowEdit = false;
            this.PLAN_FIN.OptionsColumn.ReadOnly = true;
            this.PLAN_FIN.Visible = true;
            this.PLAN_FIN.Width = 120;
            // 
            // FACT_FIN
            // 
            this.FACT_FIN.AppearanceCell.Options.UseTextOptions = true;
            this.FACT_FIN.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FACT_FIN.AppearanceHeader.Options.UseTextOptions = true;
            this.FACT_FIN.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FACT_FIN.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FACT_FIN.Caption = "����� (����)";
            this.FACT_FIN.DisplayFormat.FormatString = "dd/MM/yyyy  HH:mm";
            this.FACT_FIN.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.FACT_FIN.FieldName = "FACT_FIN";
            this.FACT_FIN.Name = "FACT_FIN";
            this.FACT_FIN.OptionsColumn.AllowEdit = false;
            this.FACT_FIN.OptionsColumn.ReadOnly = true;
            this.FACT_FIN.Visible = true;
            this.FACT_FIN.Width = 120;
            // 
            // TimePlanTotal
            // 
            this.TimePlanTotal.AppearanceCell.Options.UseTextOptions = true;
            this.TimePlanTotal.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimePlanTotal.AppearanceHeader.Options.UseTextOptions = true;
            this.TimePlanTotal.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimePlanTotal.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TimePlanTotal.Caption = "������������ (����) ";
            this.TimePlanTotal.FieldName = "TimePlanTotal";
            this.TimePlanTotal.Name = "TimePlanTotal";
            this.TimePlanTotal.OptionsColumn.AllowEdit = false;
            this.TimePlanTotal.OptionsColumn.ReadOnly = true;
            this.TimePlanTotal.Visible = true;
            this.TimePlanTotal.Width = 120;
            // 
            // TimeFactTotal
            // 
            this.TimeFactTotal.AppearanceCell.Options.UseTextOptions = true;
            this.TimeFactTotal.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeFactTotal.AppearanceHeader.Options.UseTextOptions = true;
            this.TimeFactTotal.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeFactTotal.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TimeFactTotal.Caption = "������������ (����)";
            this.TimeFactTotal.FieldName = "TimeFactTotal";
            this.TimeFactTotal.Name = "TimeFactTotal";
            this.TimeFactTotal.OptionsColumn.AllowEdit = false;
            this.TimeFactTotal.OptionsColumn.ReadOnly = true;
            this.TimeFactTotal.Visible = true;
            this.TimeFactTotal.Width = 120;
            // 
            // DeviationCol
            // 
            this.DeviationCol.AppearanceCell.Options.UseTextOptions = true;
            this.DeviationCol.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DeviationCol.AppearanceHeader.Options.UseTextOptions = true;
            this.DeviationCol.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DeviationCol.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.DeviationCol.Caption = "����������";
            this.DeviationCol.FieldName = "Deviation";
            this.DeviationCol.Name = "DeviationCol";
            this.DeviationCol.Visible = true;
            // 
            // gridBand4
            // 
            this.gridBand4.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand4.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand4.Caption = "��������� ��������";
            this.gridBand4.Columns.Add(this.Distance);
            this.gridBand4.Columns.Add(this.TimeMove);
            this.gridBand4.Columns.Add(this.SpeedAvg);
            this.gridBand4.Columns.Add(this.StopsQTY);
            this.gridBand4.Columns.Add(this.TimeStop);
            this.gridBand4.MinWidth = 20;
            this.gridBand4.Name = "gridBand4";
            this.gridBand4.VisibleIndex = 2;
            this.gridBand4.Width = 500;
            // 
            // Distance
            // 
            this.Distance.AppearanceCell.Options.UseTextOptions = true;
            this.Distance.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Distance.AppearanceHeader.Options.UseTextOptions = true;
            this.Distance.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Distance.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Distance.Caption = "����, ��";
            this.Distance.FieldName = "Distance";
            this.Distance.Name = "Distance";
            this.Distance.OptionsColumn.AllowEdit = false;
            this.Distance.OptionsColumn.ReadOnly = true;
            this.Distance.Visible = true;
            this.Distance.Width = 100;
            // 
            // TimeMove
            // 
            this.TimeMove.AppearanceCell.Options.UseTextOptions = true;
            this.TimeMove.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeMove.AppearanceHeader.Options.UseTextOptions = true;
            this.TimeMove.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeMove.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TimeMove.Caption = "����� ��������";
            this.TimeMove.FieldName = "TimeMove";
            this.TimeMove.Name = "TimeMove";
            this.TimeMove.OptionsColumn.AllowEdit = false;
            this.TimeMove.OptionsColumn.ReadOnly = true;
            this.TimeMove.Visible = true;
            this.TimeMove.Width = 100;
            // 
            // SpeedAvg
            // 
            this.SpeedAvg.AppearanceHeader.Options.UseTextOptions = true;
            this.SpeedAvg.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SpeedAvg.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.SpeedAvg.Caption = "������� ��������";
            this.SpeedAvg.FieldName = "SpeedAvg";
            this.SpeedAvg.Name = "SpeedAvg";
            this.SpeedAvg.OptionsColumn.AllowEdit = false;
            this.SpeedAvg.OptionsColumn.ReadOnly = true;
            this.SpeedAvg.Visible = true;
            this.SpeedAvg.Width = 100;
            // 
            // StopsQTY
            // 
            this.StopsQTY.AppearanceCell.Options.UseTextOptions = true;
            this.StopsQTY.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.StopsQTY.AppearanceHeader.Options.UseTextOptions = true;
            this.StopsQTY.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.StopsQTY.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.StopsQTY.Caption = "���-�� �������";
            this.StopsQTY.FieldName = "StopsQTY";
            this.StopsQTY.Name = "StopsQTY";
            this.StopsQTY.OptionsColumn.AllowEdit = false;
            this.StopsQTY.OptionsColumn.ReadOnly = true;
            this.StopsQTY.Visible = true;
            this.StopsQTY.Width = 100;
            // 
            // TimeStop
            // 
            this.TimeStop.AppearanceCell.Options.UseTextOptions = true;
            this.TimeStop.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeStop.AppearanceHeader.Options.UseTextOptions = true;
            this.TimeStop.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeStop.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.TimeStop.Caption = "����� �������";
            this.TimeStop.FieldName = "TimeStop";
            this.TimeStop.Name = "TimeStop";
            this.TimeStop.OptionsColumn.AllowEdit = false;
            this.TimeStop.OptionsColumn.ReadOnly = true;
            this.TimeStop.Visible = true;
            this.TimeStop.Width = 100;
            // 
            // gridBand6
            // 
            this.gridBand6.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand6.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand6.Caption = "������� (���)";
            this.gridBand6.Columns.Add(this.FuelStart);
            this.gridBand6.Columns.Add(this.FuelEnd);
            this.gridBand6.Columns.Add(this.FuelAddQty);
            this.gridBand6.Columns.Add(this.FuelAdd);
            this.gridBand6.Columns.Add(this.FuelSubQty);
            this.gridBand6.Columns.Add(this.FuelSub);
            this.gridBand6.Columns.Add(this.FuelExpense);
            this.gridBand6.Columns.Add(this.FuelExpensAvg);
            this.gridBand6.MinWidth = 20;
            this.gridBand6.Name = "gridBand6";
            this.gridBand6.VisibleIndex = 3;
            this.gridBand6.Width = 849;
            // 
            // FuelStart
            // 
            this.FuelStart.AppearanceCell.Options.UseTextOptions = true;
            this.FuelStart.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FuelStart.AppearanceHeader.Options.UseTextOptions = true;
            this.FuelStart.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FuelStart.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FuelStart.Caption = "������� � ������";
            this.FuelStart.FieldName = "FuelStart";
            this.FuelStart.Name = "FuelStart";
            this.FuelStart.OptionsColumn.AllowEdit = false;
            this.FuelStart.Visible = true;
            this.FuelStart.Width = 100;
            // 
            // FuelEnd
            // 
            this.FuelEnd.AppearanceCell.Options.UseTextOptions = true;
            this.FuelEnd.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FuelEnd.AppearanceHeader.Options.UseTextOptions = true;
            this.FuelEnd.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FuelEnd.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FuelEnd.Caption = "������� � �����";
            this.FuelEnd.FieldName = "FuelEnd";
            this.FuelEnd.Name = "FuelEnd";
            this.FuelEnd.OptionsColumn.AllowEdit = false;
            this.FuelEnd.Visible = true;
            this.FuelEnd.Width = 100;
            // 
            // FuelAddQty
            // 
            this.FuelAddQty.AppearanceHeader.Options.UseTextOptions = true;
            this.FuelAddQty.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FuelAddQty.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FuelAddQty.Caption = "���-�� ��������";
            this.FuelAddQty.FieldName = "FuelAddQty";
            this.FuelAddQty.Name = "FuelAddQty";
            this.FuelAddQty.Visible = true;
            this.FuelAddQty.Width = 125;
            // 
            // FuelAdd
            // 
            this.FuelAdd.AppearanceCell.Options.UseTextOptions = true;
            this.FuelAdd.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FuelAdd.AppearanceHeader.Options.UseTextOptions = true;
            this.FuelAdd.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FuelAdd.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FuelAdd.Caption = "����� ��������";
            this.FuelAdd.FieldName = "FuelAdd";
            this.FuelAdd.Name = "FuelAdd";
            this.FuelAdd.OptionsColumn.AllowEdit = false;
            this.FuelAdd.Visible = true;
            this.FuelAdd.Width = 100;
            // 
            // FuelSubQty
            // 
            this.FuelSubQty.AppearanceHeader.Options.UseTextOptions = true;
            this.FuelSubQty.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FuelSubQty.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FuelSubQty.Caption = "���-�� ������";
            this.FuelSubQty.FieldName = "FuelSubQty";
            this.FuelSubQty.Name = "FuelSubQty";
            this.FuelSubQty.Visible = true;
            this.FuelSubQty.Width = 124;
            // 
            // FuelSub
            // 
            this.FuelSub.AppearanceCell.Options.UseTextOptions = true;
            this.FuelSub.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FuelSub.AppearanceHeader.Options.UseTextOptions = true;
            this.FuelSub.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FuelSub.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FuelSub.Caption = "����� ������";
            this.FuelSub.FieldName = "FuelSub";
            this.FuelSub.Name = "FuelSub";
            this.FuelSub.OptionsColumn.AllowEdit = false;
            this.FuelSub.Visible = true;
            this.FuelSub.Width = 100;
            // 
            // FuelExpense
            // 
            this.FuelExpense.AppearanceCell.Options.UseTextOptions = true;
            this.FuelExpense.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FuelExpense.AppearanceHeader.Options.UseTextOptions = true;
            this.FuelExpense.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FuelExpense.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FuelExpense.Caption = "����� ������";
            this.FuelExpense.FieldName = "FuelExpens";
            this.FuelExpense.Name = "FuelExpense";
            this.FuelExpense.OptionsColumn.AllowEdit = false;
            this.FuelExpense.Visible = true;
            this.FuelExpense.Width = 100;
            // 
            // FuelExpensAvg
            // 
            this.FuelExpensAvg.AppearanceCell.Options.UseTextOptions = true;
            this.FuelExpensAvg.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FuelExpensAvg.AppearanceHeader.Options.UseTextOptions = true;
            this.FuelExpensAvg.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.FuelExpensAvg.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.FuelExpensAvg.Caption = "��. ������ �/100 ��";
            this.FuelExpensAvg.FieldName = "FuelExpensAvg";
            this.FuelExpensAvg.Name = "FuelExpensAvg";
            this.FuelExpensAvg.OptionsColumn.AllowEdit = false;
            this.FuelExpensAvg.Visible = true;
            this.FuelExpensAvg.Width = 100;
            // 
            // gridBand3
            // 
            this.gridBand3.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand3.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand3.Caption = "������� (���/CAN)";
            this.gridBand3.Columns.Add(this.Fuel_ExpensTotal);
            this.gridBand3.Columns.Add(this.Fuel_ExpensMove);
            this.gridBand3.Columns.Add(this.Fuel_ExpensStop);
            this.gridBand3.Columns.Add(this.Fuel_ExpensAvg);
            this.gridBand3.Name = "gridBand3";
            this.gridBand3.VisibleIndex = 4;
            this.gridBand3.Width = 400;
            // 
            // Fuel_ExpensTotal
            // 
            this.Fuel_ExpensTotal.AppearanceCell.Options.UseTextOptions = true;
            this.Fuel_ExpensTotal.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Fuel_ExpensTotal.AppearanceHeader.Options.UseTextOptions = true;
            this.Fuel_ExpensTotal.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Fuel_ExpensTotal.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Fuel_ExpensTotal.Caption = "����� ������";
            this.Fuel_ExpensTotal.FieldName = "Fuel_ExpensTotal";
            this.Fuel_ExpensTotal.Name = "Fuel_ExpensTotal";
            this.Fuel_ExpensTotal.OptionsColumn.AllowEdit = false;
            this.Fuel_ExpensTotal.Visible = true;
            this.Fuel_ExpensTotal.Width = 100;
            // 
            // Fuel_ExpensMove
            // 
            this.Fuel_ExpensMove.AppearanceCell.Options.UseTextOptions = true;
            this.Fuel_ExpensMove.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Fuel_ExpensMove.AppearanceHeader.Options.UseTextOptions = true;
            this.Fuel_ExpensMove.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Fuel_ExpensMove.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Fuel_ExpensMove.Caption = "������ � ��������";
            this.Fuel_ExpensMove.FieldName = "Fuel_ExpensMove";
            this.Fuel_ExpensMove.Name = "Fuel_ExpensMove";
            this.Fuel_ExpensMove.OptionsColumn.AllowEdit = false;
            this.Fuel_ExpensMove.Visible = true;
            this.Fuel_ExpensMove.Width = 100;
            // 
            // Fuel_ExpensStop
            // 
            this.Fuel_ExpensStop.AppearanceCell.Options.UseTextOptions = true;
            this.Fuel_ExpensStop.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Fuel_ExpensStop.AppearanceHeader.Options.UseTextOptions = true;
            this.Fuel_ExpensStop.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Fuel_ExpensStop.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Fuel_ExpensStop.Caption = "������ �� ��������";
            this.Fuel_ExpensStop.FieldName = "Fuel_ExpensStop";
            this.Fuel_ExpensStop.Name = "Fuel_ExpensStop";
            this.Fuel_ExpensStop.OptionsColumn.AllowEdit = false;
            this.Fuel_ExpensStop.Visible = true;
            this.Fuel_ExpensStop.Width = 100;
            // 
            // Fuel_ExpensAvg
            // 
            this.Fuel_ExpensAvg.AppearanceCell.Options.UseTextOptions = true;
            this.Fuel_ExpensAvg.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Fuel_ExpensAvg.AppearanceHeader.Options.UseTextOptions = true;
            this.Fuel_ExpensAvg.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Fuel_ExpensAvg.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Fuel_ExpensAvg.Caption = "��. ������ �/100 ��";
            this.Fuel_ExpensAvg.FieldName = "Fuel_ExpensAvg";
            this.Fuel_ExpensAvg.Name = "Fuel_ExpensAvg";
            this.Fuel_ExpensAvg.OptionsColumn.AllowEdit = false;
            this.Fuel_ExpensAvg.Visible = true;
            this.Fuel_ExpensAvg.Width = 100;
            // 
            // gridBand7
            // 
            this.gridBand7.AppearanceHeader.Options.UseTextOptions = true;
            this.gridBand7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridBand7.Caption = "�������";
            this.gridBand7.Columns.Add(this.SensQTY);
            this.gridBand7.MinWidth = 20;
            this.gridBand7.Name = "gridBand7";
            this.gridBand7.VisibleIndex = 5;
            this.gridBand7.Width = 100;
            // 
            // SensQTY
            // 
            this.SensQTY.AppearanceCell.Options.UseTextOptions = true;
            this.SensQTY.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SensQTY.AppearanceHeader.Options.UseTextOptions = true;
            this.SensQTY.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.SensQTY.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.SensQTY.Caption = "����� �������";
            this.SensQTY.FieldName = "SensQTY";
            this.SensQTY.Name = "SensQTY";
            this.SensQTY.OptionsColumn.AllowEdit = false;
            this.SensQTY.OptionsColumn.ReadOnly = true;
            this.SensQTY.Visible = true;
            this.SensQTY.Width = 100;
            // 
            // gridQuontity
            // 
            this.gridQuontity.AppearanceHeader.Options.UseTextOptions = true;
            this.gridQuontity.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridQuontity.Caption = "�������� ������";
            this.gridQuontity.Columns.Add(this.Validity);
            this.gridQuontity.Columns.Add(this.PointsIntervalMax);
            this.gridQuontity.Name = "gridQuontity";
            this.gridQuontity.VisibleIndex = 6;
            this.gridQuontity.Width = 198;
            // 
            // Validity
            // 
            this.Validity.AppearanceHeader.Options.UseTextOptions = true;
            this.Validity.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Validity.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.Validity.Caption = "������� ������";
            this.Validity.ColumnEdit = this.pbValidity;
            this.Validity.FieldName = "PointsValidity";
            this.Validity.Name = "Validity";
            this.Validity.Visible = true;
            this.Validity.Width = 98;
            // 
            // pbValidity
            // 
            this.pbValidity.Name = "pbValidity";
            this.pbValidity.ShowTitle = true;
            // 
            // PointsIntervalMax
            // 
            this.PointsIntervalMax.AppearanceCell.Options.UseTextOptions = true;
            this.PointsIntervalMax.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.PointsIntervalMax.AppearanceHeader.Options.UseTextOptions = true;
            this.PointsIntervalMax.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.PointsIntervalMax.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.PointsIntervalMax.Caption = "MAX �������� ����� �������";
            this.PointsIntervalMax.FieldName = "PointsIntervalMax";
            this.PointsIntervalMax.Name = "PointsIntervalMax";
            this.PointsIntervalMax.OptionsColumn.AllowEdit = false;
            this.PointsIntervalMax.Visible = true;
            this.PointsIntervalMax.Width = 100;
            // 
            // ZoneLookUp
            // 
            this.ZoneLookUp.AutoHeight = false;
            this.ZoneLookUp.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ZoneLookUp.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "��������")});
            this.ZoneLookUp.DisplayMember = "Name";
            this.ZoneLookUp.Name = "ZoneLookUp";
            this.ZoneLookUp.ValueMember = "Id";
            // 
            // xtbView
            // 
            this.xtbView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtbView.Location = new System.Drawing.Point(0, 26);
            this.xtbView.Name = "xtbView";
            this.xtbView.SelectedTabPage = this.xtraTabPage1;
            this.xtbView.Size = new System.Drawing.Size(1052, 534);
            this.xtbView.TabIndex = 4;
            this.xtbView.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2,
            this.xtraTabPage3,
            this.xtraTabPage4});
            this.xtbView.SelectedPageChanged += new DevExpress.XtraTab.TabPageChangedEventHandler(this.xtbView_SelectedPageChanged);
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.splitContainer1);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(1046, 506);
            this.xtraTabPage1.Text = "�����������";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.tlDictionary);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.gcDict);
            this.splitContainer1.Size = new System.Drawing.Size(1046, 506);
            this.splitContainer1.SplitterDistance = 217;
            this.splitContainer1.TabIndex = 0;
            // 
            // tlDictionary
            // 
            this.tlDictionary.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.tlDictionary.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.tlDictionary.Appearance.Empty.Options.UseBackColor = true;
            this.tlDictionary.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.tlDictionary.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.tlDictionary.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.tlDictionary.Appearance.EvenRow.Options.UseBackColor = true;
            this.tlDictionary.Appearance.EvenRow.Options.UseBorderColor = true;
            this.tlDictionary.Appearance.EvenRow.Options.UseForeColor = true;
            this.tlDictionary.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.tlDictionary.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.tlDictionary.Appearance.FocusedCell.Options.UseBackColor = true;
            this.tlDictionary.Appearance.FocusedCell.Options.UseForeColor = true;
            this.tlDictionary.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(192)))), ((int)(((byte)(157)))));
            this.tlDictionary.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(219)))), ((int)(((byte)(188)))));
            this.tlDictionary.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.tlDictionary.Appearance.FocusedRow.Options.UseBackColor = true;
            this.tlDictionary.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.tlDictionary.Appearance.FocusedRow.Options.UseForeColor = true;
            this.tlDictionary.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.tlDictionary.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.tlDictionary.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.tlDictionary.Appearance.FooterPanel.Options.UseBackColor = true;
            this.tlDictionary.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.tlDictionary.Appearance.FooterPanel.Options.UseForeColor = true;
            this.tlDictionary.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.tlDictionary.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.tlDictionary.Appearance.GroupButton.Options.UseBackColor = true;
            this.tlDictionary.Appearance.GroupButton.Options.UseBorderColor = true;
            this.tlDictionary.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.tlDictionary.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.tlDictionary.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.tlDictionary.Appearance.GroupFooter.Options.UseBackColor = true;
            this.tlDictionary.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.tlDictionary.Appearance.GroupFooter.Options.UseForeColor = true;
            this.tlDictionary.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.tlDictionary.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.tlDictionary.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.tlDictionary.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.tlDictionary.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.tlDictionary.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.tlDictionary.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.tlDictionary.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.tlDictionary.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.tlDictionary.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.tlDictionary.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.tlDictionary.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.tlDictionary.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.tlDictionary.Appearance.HorzLine.Options.UseBackColor = true;
            this.tlDictionary.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.tlDictionary.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.tlDictionary.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.tlDictionary.Appearance.OddRow.Options.UseBackColor = true;
            this.tlDictionary.Appearance.OddRow.Options.UseBorderColor = true;
            this.tlDictionary.Appearance.OddRow.Options.UseForeColor = true;
            this.tlDictionary.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(247)))));
            this.tlDictionary.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.tlDictionary.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(148)))), ((int)(((byte)(148)))));
            this.tlDictionary.Appearance.Preview.Options.UseBackColor = true;
            this.tlDictionary.Appearance.Preview.Options.UseFont = true;
            this.tlDictionary.Appearance.Preview.Options.UseForeColor = true;
            this.tlDictionary.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.tlDictionary.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.tlDictionary.Appearance.Row.Options.UseBackColor = true;
            this.tlDictionary.Appearance.Row.Options.UseForeColor = true;
            this.tlDictionary.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.tlDictionary.Appearance.SelectedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.tlDictionary.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.tlDictionary.Appearance.SelectedRow.Options.UseBackColor = true;
            this.tlDictionary.Appearance.SelectedRow.Options.UseBorderColor = true;
            this.tlDictionary.Appearance.SelectedRow.Options.UseForeColor = true;
            this.tlDictionary.Appearance.TreeLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.tlDictionary.Appearance.TreeLine.Options.UseBackColor = true;
            this.tlDictionary.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.tlDictionary.Appearance.VertLine.Options.UseBackColor = true;
            this.tlDictionary.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.DicCode,
            this.DicName});
            this.tlDictionary.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlDictionary.Location = new System.Drawing.Point(0, 0);
            this.tlDictionary.Name = "tlDictionary";
            this.tlDictionary.BeginUnboundLoad();
            this.tlDictionary.AppendNode(new object[] {
            "1",
            "������ �������� ���������"}, -1, 0, 0, 0);
            this.tlDictionary.AppendNode(new object[] {
            "2",
            "������� ���������"}, 0, 0, 0, 1);
            this.tlDictionary.AppendNode(new object[] {
            null,
            "������� �������� ��������"}, -1, 0, 0, 0);
            this.tlDictionary.EndUnboundLoad();
            this.tlDictionary.OptionsBehavior.Editable = false;
            this.tlDictionary.OptionsBehavior.PopulateServiceColumns = true;
            this.tlDictionary.OptionsView.EnableAppearanceEvenRow = true;
            this.tlDictionary.OptionsView.EnableAppearanceOddRow = true;
            this.tlDictionary.Size = new System.Drawing.Size(217, 506);
            this.tlDictionary.StateImageList = this.imCollection;
            this.tlDictionary.TabIndex = 0;
            this.tlDictionary.FocusedNodeChanged += new DevExpress.XtraTreeList.FocusedNodeChangedEventHandler(this.tlDictionary_FocusedNodeChanged);
            // 
            // DicCode
            // 
            this.DicCode.Caption = "���";
            this.DicCode.FieldName = "���";
            this.DicCode.Name = "DicCode";
            this.DicCode.Width = 55;
            // 
            // DicName
            // 
            this.DicName.AppearanceHeader.Options.UseTextOptions = true;
            this.DicName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.DicName.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.DicName.Caption = "�������� �����������";
            this.DicName.FieldName = "treeListColumn1";
            this.DicName.MinWidth = 69;
            this.DicName.Name = "DicName";
            this.DicName.Visible = true;
            this.DicName.VisibleIndex = 0;
            this.DicName.Width = 69;
            // 
            // imCollection
            // 
            this.imCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imCollection.ImageStream")));
            this.imCollection.Images.SetKeyName(0, "bullet_green.png");
            this.imCollection.Images.SetKeyName(1, "bullet_red.png");
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.gcTask);
            this.xtraTabPage2.Controls.Add(this.panel1);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(1046, 506);
            this.xtraTabPage2.Text = "���������� �����";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.leType);
            this.panel1.Controls.Add(this.lbType);
            this.panel1.Controls.Add(this.leMobitel);
            this.panel1.Controls.Add(this.btStart);
            this.panel1.Controls.Add(this.sbtFilterClear);
            this.panel1.Controls.Add(this.sbtAutoCreate);
            this.panel1.Controls.Add(this.leGroupe);
            this.panel1.Controls.Add(this.lbCar);
            this.panel1.Controls.Add(this.lbCarGroupe);
            this.panel1.Controls.Add(this.lbDateTo);
            this.panel1.Controls.Add(this.lbDateFrom);
            this.panel1.Controls.Add(this.deEnd);
            this.panel1.Controls.Add(this.deStart);
            this.panel1.Location = new System.Drawing.Point(0, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1377, 59);
            this.panel1.TabIndex = 6;
            // 
            // leType
            // 
            this.leType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.leType.Location = new System.Drawing.Point(456, 9);
            this.leType.Name = "leType";
            this.leType.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leType.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.leType.Properties.DisplayMember = "Name";
            this.leType.Properties.DropDownRows = 20;
            this.leType.Properties.NullText = "";
            this.leType.Properties.ValueMember = "id";
            this.leType.Size = new System.Drawing.Size(127, 20);
            this.leType.TabIndex = 24;
            // 
            // lbType
            // 
            this.lbType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lbType.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.lbType.Location = new System.Drawing.Point(428, 11);
            this.lbType.Name = "lbType";
            this.lbType.Size = new System.Drawing.Size(22, 14);
            this.lbType.TabIndex = 23;
            this.lbType.Text = "���";
            // 
            // leMobitel
            // 
            this.leMobitel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.leMobitel.Location = new System.Drawing.Point(312, 34);
            this.leMobitel.Name = "leMobitel";
            this.leMobitel.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leMobitel.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.leMobitel.Properties.DisplayMember = "Name";
            this.leMobitel.Properties.DropDownRows = 20;
            this.leMobitel.Properties.NullText = "";
            this.leMobitel.Properties.ValueMember = "id";
            this.leMobitel.Size = new System.Drawing.Size(141, 20);
            this.leMobitel.TabIndex = 22;
            // 
            // btStart
            // 
            this.btStart.Image = global::Route.Properties.ResourcesImages.next;
            this.btStart.Location = new System.Drawing.Point(5, 5);
            this.btStart.Name = "btStart";
            this.btStart.Size = new System.Drawing.Size(63, 25);
            this.btStart.TabIndex = 21;
            this.btStart.Text = "����";
            this.btStart.Click += new System.EventHandler(this.btStart_Click);
            // 
            // sbtFilterClear
            // 
            this.sbtFilterClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.sbtFilterClear.Image = ((System.Drawing.Image)(resources.GetObject("sbtFilterClear.Image")));
            this.sbtFilterClear.Location = new System.Drawing.Point(589, 9);
            this.sbtFilterClear.Name = "sbtFilterClear";
            this.sbtFilterClear.Size = new System.Drawing.Size(23, 19);
            this.sbtFilterClear.TabIndex = 20;
            this.sbtFilterClear.ToolTip = "������� ������� �������";
            this.sbtFilterClear.Click += new System.EventHandler(this.sbtFilterClear_Click);
            // 
            // sbtAutoCreate
            // 
            this.sbtAutoCreate.Appearance.Font = new System.Drawing.Font("Tahoma", 8F);
            this.sbtAutoCreate.Appearance.Options.UseFont = true;
            this.sbtAutoCreate.Image = ((System.Drawing.Image)(resources.GetObject("sbtAutoCreate.Image")));
            this.sbtAutoCreate.Location = new System.Drawing.Point(473, 34);
            this.sbtAutoCreate.Name = "sbtAutoCreate";
            this.sbtAutoCreate.Size = new System.Drawing.Size(111, 19);
            this.sbtAutoCreate.TabIndex = 19;
            this.sbtAutoCreate.Text = "������������ ";
            this.sbtAutoCreate.ToolTip = "�������������� �������� ���������� ������";
            this.sbtAutoCreate.Click += new System.EventHandler(this.sbtAutoCreate_Click);
            // 
            // leGroupe
            // 
            this.leGroupe.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.leGroupe.Location = new System.Drawing.Point(116, 34);
            this.leGroupe.Name = "leGroupe";
            this.leGroupe.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leGroupe.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "", 20, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Default, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.leGroupe.Properties.DisplayMember = "Name";
            this.leGroupe.Properties.NullText = "";
            this.leGroupe.Properties.ValueMember = "id";
            this.leGroupe.Size = new System.Drawing.Size(141, 20);
            this.leGroupe.TabIndex = 15;
            this.leGroupe.EditValueChanged += new System.EventHandler(this.leGroupe_EditValueChanged);
            // 
            // lbCar
            // 
            this.lbCar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lbCar.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.lbCar.Location = new System.Drawing.Point(263, 36);
            this.lbCar.Name = "lbCar";
            this.lbCar.Size = new System.Drawing.Size(43, 14);
            this.lbCar.TabIndex = 14;
            this.lbCar.Text = "������";
            // 
            // lbCarGroupe
            // 
            this.lbCarGroupe.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lbCarGroupe.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.lbCarGroupe.Location = new System.Drawing.Point(27, 36);
            this.lbCarGroupe.Name = "lbCarGroupe";
            this.lbCarGroupe.Size = new System.Drawing.Size(83, 14);
            this.lbCarGroupe.TabIndex = 13;
            this.lbCarGroupe.Text = "������ ����� ";
            // 
            // lbDateTo
            // 
            this.lbDateTo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lbDateTo.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.lbDateTo.Location = new System.Drawing.Point(271, 11);
            this.lbDateTo.Name = "lbDateTo";
            this.lbDateTo.Size = new System.Drawing.Size(14, 14);
            this.lbDateTo.TabIndex = 3;
            this.lbDateTo.Text = "��";
            // 
            // lbDateFrom
            // 
            this.lbDateFrom.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lbDateFrom.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.lbDateFrom.Location = new System.Drawing.Point(74, 10);
            this.lbDateFrom.Name = "lbDateFrom";
            this.lbDateFrom.Size = new System.Drawing.Size(64, 14);
            this.lbDateFrom.TabIndex = 2;
            this.lbDateFrom.Text = "���� �       ";
            // 
            // deEnd
            // 
            this.deEnd.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.deEnd.EditValue = null;
            this.deEnd.Location = new System.Drawing.Point(291, 9);
            this.deEnd.MenuManager = this.bmRouteGrn;
            this.deEnd.Name = "deEnd";
            this.deEnd.Properties.Appearance.Options.UseTextOptions = true;
            this.deEnd.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.deEnd.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deEnd.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deEnd.Properties.CalendarTimeProperties.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F4);
            this.deEnd.Properties.CalendarTimeProperties.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Default;
            this.deEnd.Properties.DisplayFormat.FormatString = "dd/MM/yyyy  HH:mm";
            this.deEnd.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deEnd.Properties.EditFormat.FormatString = "dd/MM/yyyy  HH:mm";
            this.deEnd.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deEnd.Properties.Mask.EditMask = "dd/MM/yyyy  HH:mm";
            this.deEnd.Size = new System.Drawing.Size(120, 20);
            this.deEnd.TabIndex = 1;
            this.deEnd.EditValueChanged += new System.EventHandler(this.deEnd_EditValueChanged);
            // 
            // deStart
            // 
            this.deStart.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.deStart.EditValue = null;
            this.deStart.Location = new System.Drawing.Point(144, 8);
            this.deStart.MenuManager = this.bmRouteGrn;
            this.deStart.Name = "deStart";
            this.deStart.Properties.Appearance.Options.UseTextOptions = true;
            this.deStart.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.deStart.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deStart.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deStart.Properties.CalendarTimeProperties.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F4);
            this.deStart.Properties.CalendarTimeProperties.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Default;
            this.deStart.Properties.DisplayFormat.FormatString = "dd/MM/yyyy  HH:mm";
            this.deStart.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deStart.Properties.EditFormat.FormatString = "dd/MM/yyyy  HH:mm";
            this.deStart.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deStart.Properties.Mask.EditMask = "dd/MM/yyyy  HH:mm";
            this.deStart.Size = new System.Drawing.Size(120, 20);
            this.deStart.TabIndex = 0;
            // 
            // xtraTabPage3
            // 
            this.xtraTabPage3.Controls.Add(this.gcReport);
            this.xtraTabPage3.Name = "xtraTabPage3";
            this.xtraTabPage3.PageVisible = false;
            this.xtraTabPage3.Size = new System.Drawing.Size(1046, 506);
            this.xtraTabPage3.Text = "������";
            // 
            // gcReport
            // 
            this.gcReport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcReport.Location = new System.Drawing.Point(0, 0);
            this.gcReport.MainView = this.gvReport;
            this.gcReport.MenuManager = this.bmRouteGrn;
            this.gcReport.Name = "gcReport";
            this.gcReport.Size = new System.Drawing.Size(1046, 506);
            this.gcReport.TabIndex = 0;
            this.gcReport.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvReport});
            // 
            // gvReport
            // 
            this.gvReport.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvReport.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvReport.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gvReport.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvReport.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvReport.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvReport.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvReport.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvReport.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gvReport.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvReport.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvReport.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvReport.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvReport.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gvReport.Appearance.Empty.Options.UseBackColor = true;
            this.gvReport.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvReport.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvReport.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gvReport.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvReport.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gvReport.Appearance.EvenRow.Options.UseForeColor = true;
            this.gvReport.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvReport.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvReport.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gvReport.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvReport.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvReport.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gvReport.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvReport.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gvReport.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvReport.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvReport.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvReport.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.gvReport.Appearance.FixedLine.Options.UseBackColor = true;
            this.gvReport.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gvReport.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gvReport.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gvReport.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gvReport.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(192)))), ((int)(((byte)(157)))));
            this.gvReport.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(219)))), ((int)(((byte)(188)))));
            this.gvReport.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gvReport.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvReport.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gvReport.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvReport.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvReport.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvReport.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvReport.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvReport.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvReport.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gvReport.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvReport.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvReport.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvReport.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvReport.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvReport.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvReport.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gvReport.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvReport.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvReport.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gvReport.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(242)))), ((int)(((byte)(213)))));
            this.gvReport.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gvReport.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gvReport.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvReport.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvReport.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvReport.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvReport.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gvReport.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvReport.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gvReport.Appearance.GroupRow.Options.UseForeColor = true;
            this.gvReport.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvReport.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvReport.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gvReport.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvReport.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvReport.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gvReport.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvReport.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvReport.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gvReport.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvReport.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.gvReport.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gvReport.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvReport.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvReport.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvReport.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvReport.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gvReport.Appearance.OddRow.Options.UseBackColor = true;
            this.gvReport.Appearance.OddRow.Options.UseBorderColor = true;
            this.gvReport.Appearance.OddRow.Options.UseForeColor = true;
            this.gvReport.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(247)))));
            this.gvReport.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gvReport.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(148)))), ((int)(((byte)(148)))));
            this.gvReport.Appearance.Preview.Options.UseBackColor = true;
            this.gvReport.Appearance.Preview.Options.UseFont = true;
            this.gvReport.Appearance.Preview.Options.UseForeColor = true;
            this.gvReport.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvReport.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gvReport.Appearance.Row.Options.UseBackColor = true;
            this.gvReport.Appearance.Row.Options.UseForeColor = true;
            this.gvReport.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvReport.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gvReport.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvReport.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.gvReport.Appearance.SelectedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvReport.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.gvReport.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvReport.Appearance.SelectedRow.Options.UseBorderColor = true;
            this.gvReport.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gvReport.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gvReport.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gvReport.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvReport.Appearance.VertLine.Options.UseBackColor = true;
            this.gvReport.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1});
            this.gvReport.GridControl = this.gcReport;
            this.gvReport.Name = "gvReport";
            this.gvReport.OptionsView.EnableAppearanceEvenRow = true;
            this.gvReport.OptionsView.EnableAppearanceOddRow = true;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // xtraTabPage4
            // 
            this.xtraTabPage4.Controls.Add(this.pnOnline);
            this.xtraTabPage4.Name = "xtraTabPage4";
            this.xtraTabPage4.Size = new System.Drawing.Size(1046, 506);
            this.xtraTabPage4.Text = "������-��������";
            // 
            // pnOnline
            // 
            this.pnOnline.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnOnline.Location = new System.Drawing.Point(0, 0);
            this.pnOnline.Name = "pnOnline";
            this.pnOnline.Size = new System.Drawing.Size(1046, 506);
            this.pnOnline.TabIndex = 0;
            // 
            // btImportXml
            // 
            this.btImportXml.Caption = "������ XML";
            this.btImportXml.Glyph = ((System.Drawing.Image)(resources.GetObject("btImportXml.Glyph")));
            this.btImportXml.Id = 20;
            this.btImportXml.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("btImportXml.LargeGlyph")));
            this.btImportXml.Name = "btImportXml";
            this.btImportXml.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btImportXml_ItemClick);
            // 
            // MainRoute
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1052, 585);
            this.Controls.Add(this.xtbView);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainRoute";
            this.Text = "��������";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainRoute_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.gvRoute)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leRouteGrp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcDict)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvShedule)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvRouteGroups)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bmRouteGrn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rpbStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvTaskContent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EventLookUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcTask)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvTask)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RouteLookUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MobitelLookUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DriverLookUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbValidity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ZoneLookUp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtbView)).EndInit();
            this.xtbView.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tlDictionary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imCollection)).EndInit();
            this.xtraTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panel1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.leType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leMobitel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leGroupe.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEnd.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deEnd.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deStart.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deStart.Properties)).EndInit();
            this.xtraTabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvReport)).EndInit();
            this.xtraTabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnOnline)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.BarManager bmRouteGrn;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem btAdd;
        private DevExpress.XtraBars.BarButtonItem btDelete;
        private DevExpress.XtraBars.BarButtonItem btEdit;
        private DevExpress.XtraBars.BarButtonItem btRefresh;
        private DevExpress.XtraTab.XtraTabControl xtbView;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage3;
        private DevExpress.XtraGrid.GridControl gcDict;
        private DevExpress.XtraGrid.Views.Grid.GridView gvRouteGroups;
        private DevExpress.XtraTreeList.TreeList tlDictionary;
        private DevExpress.XtraGrid.GridControl gcReport;
        private DevExpress.XtraGrid.Views.Grid.GridView gvReport;
        private DevExpress.Utils.ImageCollection imCollection;
        private DevExpress.XtraTreeList.Columns.TreeListColumn DicName;
        private DevExpress.XtraTreeList.Columns.TreeListColumn DicCode;
        private DevExpress.XtraGrid.Views.Grid.GridView gvRoute;
        private DevExpress.XtraGrid.Columns.GridColumn RouteGroupeId;
        private DevExpress.XtraGrid.Columns.GridColumn GroupeName;
        private DevExpress.XtraGrid.Columns.GridColumn GroupeRemark;
        private DevExpress.XtraGrid.Columns.GridColumn RouteId;
        private DevExpress.XtraGrid.Columns.GridColumn RouteName;
        private DevExpress.XtraGrid.Columns.GridColumn RouteGroupeName;
        private DevExpress.XtraGrid.Columns.GridColumn RoutePath;
        private DevExpress.XtraGrid.Columns.GridColumn RouteRemark;
        private DevExpress.XtraGrid.GridControl gcTask;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit RouteLookUp;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit MobitelLookUp;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit DriverLookUp;
        private DevExpress.XtraEditors.PanelControl panel1;
        private DevExpress.XtraEditors.SimpleButton sbtFilterClear;
        private DevExpress.XtraEditors.SimpleButton sbtAutoCreate;
        private DevExpress.XtraEditors.LookUpEdit leGroupe;
        private DevExpress.XtraEditors.LabelControl lbCar;
        private DevExpress.XtraEditors.LabelControl lbCarGroupe;
        private DevExpress.XtraEditors.LabelControl lbDateTo;
        private DevExpress.XtraEditors.LabelControl lbDateFrom;
        private DevExpress.XtraEditors.DateEdit deEnd;
        private DevExpress.XtraEditors.DateEdit deStart;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridView gvTask;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn Id;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn Route;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn Id_mobitel;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn Id_driver;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn Distance;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn StopsQTY;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn RemarkTask;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn PLAN_START;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn FACT_START;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn PLAN_FIN;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn FACT_FIN;
        private DevExpress.XtraGrid.Views.Grid.GridView gvTaskContent;
        private DevExpress.XtraGrid.Columns.GridColumn Id_cont;
        private DevExpress.XtraGrid.Columns.GridColumn LocationM;
        private DevExpress.XtraGrid.Columns.GridColumn Id_event;
        private DevExpress.XtraGrid.Columns.GridColumn DatePlan;
        private DevExpress.XtraGrid.Columns.GridColumn DateFact;
        private DevExpress.XtraGrid.Columns.GridColumn Deviation;
        private DevExpress.XtraGrid.Columns.GridColumn Remark;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit EventLookUp;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit ZoneLookUp;
        private DevExpress.XtraBars.BarSubItem bsiExcel;
        private DevExpress.XtraBars.BarButtonItem btExcel;
        private DevExpress.XtraBars.BarButtonItem btExcelBrief;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn TimeStop;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn SensQTY;
        private DevExpress.XtraBars.BarStaticItem bsiStatus;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn TimePlanTotal;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn TimeFactTotal;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn TimeMove;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn SpeedAvg;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn FuelStart;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn FuelEnd;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn FuelAdd;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn FuelSub;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn FuelExpense;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn FuelExpensAvg;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn Fuel_ExpensMove;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn Fuel_ExpensStop;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn Fuel_ExpensTotal;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn Fuel_ExpensAvg;
        private DevExpress.XtraBars.BarSubItem bsiSettings;
        private DevExpress.XtraBars.BarButtonItem btColumnsSet;
        private DevExpress.XtraBars.BarButtonItem btColumnsSave;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn FuelAddQty;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn FuelSubQty;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn DeviationCol;
        private DevExpress.XtraBars.BarButtonItem btColumnRestore;
        private DevExpress.XtraEditors.SimpleButton btStart;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn Validity;
        private DevExpress.XtraEditors.Repository.RepositoryItemProgressBar pbValidity;
        private DevExpress.XtraGrid.Views.BandedGrid.BandedGridColumn PointsIntervalMax;
        private DevExpress.XtraGrid.Columns.GridColumn DistanceT;
        private DevExpress.XtraBars.BarButtonItem btFact;
        private DevExpress.XtraEditors.LookUpEdit leMobitel;
        private DevExpress.XtraEditors.LookUpEdit leType;
        private DevExpress.XtraEditors.LabelControl lbType;
        private DevExpress.XtraBars.BarEditItem pbStatus;
        private DevExpress.XtraEditors.Repository.RepositoryItemProgressBar rpbStatus;
        private DevExpress.XtraBars.BarButtonItem btSettings;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit leRouteGrp;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand1;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand2;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand4;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand6;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand3;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridBand7;
        private DevExpress.XtraGrid.Views.BandedGrid.GridBand gridQuontity;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage4;
        private DevExpress.XtraEditors.PanelControl pnOnline;
        private DevExpress.XtraGrid.Views.Grid.GridView gvShedule;
        private DevExpress.XtraGrid.Columns.GridColumn colId;
        private DevExpress.XtraGrid.Columns.GridColumn colName;
        private DevExpress.XtraGrid.Columns.GridColumn colZone;
        private DevExpress.XtraGrid.Columns.GridColumn colComment;
        private DevExpress.XtraBars.BarButtonItem btImportXml;
    }
}