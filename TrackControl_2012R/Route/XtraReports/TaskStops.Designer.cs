namespace Route.XtraReports
{
    partial class TaskStops
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.xrControlStyle1 = new DevExpress.XtraReports.UI.XRControlStyle();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrSubStop = new DevExpress.XtraReports.UI.XRSubreport();
            this.subTaskStop1 = new Route.XtraReports.SubTaskStop();
            ((System.ComponentModel.ISupportInitialize)(this.xrHeaderTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subTaskStop1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // xrHeader
            // 
            this.xrHeader.StylePriority.UseFont = false;
            this.xrHeader.StylePriority.UseTextAlignment = false;
            // 
            // xrHeaderTable
            // 
            this.xrHeaderTable.StylePriority.UseBorders = false;
            this.xrHeaderTable.StylePriority.UseFont = false;
            // 
            // CellVehicle
            // 
            this.CellVehicle.StylePriority.UseBorders = false;
            this.CellVehicle.StylePriority.UseFont = false;
            this.CellVehicle.StylePriority.UsePadding = false;
            this.CellVehicle.StylePriority.UseTextAlignment = false;
            // 
            // CellDriver
            // 
            this.CellDriver.StylePriority.UseBorders = false;
            this.CellDriver.StylePriority.UseFont = false;
            this.CellDriver.StylePriority.UsePadding = false;
            this.CellDriver.StylePriority.UseTextAlignment = false;
            // 
            // CellSample
            // 
            this.CellSample.StylePriority.UseBorders = false;
            this.CellSample.StylePriority.UseFont = false;
            this.CellSample.StylePriority.UsePadding = false;
            this.CellSample.StylePriority.UseTextAlignment = false;
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSubStop});
            this.Detail.Height = 29;
            // 
            // xrControlStyle1
            // 
            this.xrControlStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.xrControlStyle1.Name = "xrControlStyle1";
            this.xrControlStyle1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Location = new System.Drawing.Point(988, 0);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo1.Size = new System.Drawing.Size(79, 21);
            this.xrPageInfo1.StylePriority.UseTextAlignment = false;
            this.xrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrSubStop
            // 
            this.xrSubStop.Location = new System.Drawing.Point(0, 0);
            this.xrSubStop.Name = "xrSubStop";
            this.xrSubStop.ReportSource = this.subTaskStop1;
            this.xrSubStop.Size = new System.Drawing.Size(1067, 29);
            // 
            // subTaskStop1
            // 
            this.subTaskStop1.Landscape = true;
            this.subTaskStop1.Margins = new System.Drawing.Printing.Margins(50, 50, 50, 50);
            this.subTaskStop1.Name = "subTaskStop1";
            this.subTaskStop1.PageColor = System.Drawing.Color.White;
            this.subTaskStop1.PageHeight = 827;
            this.subTaskStop1.PageWidth = 1169;
            this.subTaskStop1.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.subTaskStop1.Version = "9.2";
            // 
            // TaskStops
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail});
            this.StyleSheet.AddRange(new DevExpress.XtraReports.UI.XRControlStyle[] {
            this.xrControlStyle1});
            this.Version = "9.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrHeaderTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subTaskStop1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.XRControlStyle xrControlStyle1;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo1;
        private DevExpress.XtraReports.UI.XRSubreport xrSubStop;
        private SubTaskStop subTaskStop1;
    }
}
