using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Route.Properties;

namespace Route.XtraReports
{
    public partial class SubTaskZone : DevExpress.XtraReports.UI.XtraReport, Route.XtraReports.ISubReport
    {
        public SubTaskZone()
        {
            InitializeComponent();
            Localization();
        }
        public void Init(int Id_task)
        {
            using (TaskItem ti = new TaskItem(Id_task))
            {
                DataSource = ti.GetContent();
            }
            CreateDataBindings();
        }
        void CreateDataBindings()
        {
            xrDetalTable.DataBindings.Add(new DevExpress.XtraReports.UI.XRBinding("Bookmark", DataSource, "Location", "{0}"));
            xrDetalCell1.DataBindings.Add("Text", DataSource, "Location");
            xrDetalCell2.DataBindings.Add("Text", DataSource, "NameEvent");
            xrDetalCell3.DataBindings.Add("Text", DataSource, "DatePlan");
            xrDetalCell4.DataBindings.Add("Text", DataSource, "DateFact");
            xrDetalCell5.DataBindings.Add("Text", DataSource, "Deviation");
            xrDetalCell6.DataBindings.Add("Text", DataSource, "Distance");
            xrDetalCell7.DataBindings.Add("Text", DataSource, "DistanceCollect");
            xrDetalCell8.DataBindings.Add("Text", DataSource, "Remark");
        }
        private void Localization()
        {
            xrDetalCell1.Text = Resources.CheckZone;
            xrDetalCell2.Text = Resources.Event;
            xrDetalCell3.Text = Resources.TimePlan;
            xrDetalCell4.Text = Resources.TimeFact;
            xrDetalCell5.Text = Resources.Deviation;
            xrDetalCell6.Text = Resources.Distance;
            xrDetalCell7.Text = Resources.DistanceTotal; 
            xrDetalCell8.Text = Resources.Comment;
            xrLabel1.Text = Resources.RouteContent;
            xrCell1.Text = Resources.CheckZone;
            xrCell2.Text = Resources.Event;
            xrCell3.Text = Resources.TimePlan;
            xrCell4.Text = Resources.TimeFact;
            xrCell5.Text = Resources.Deviation;
            xrCell6.Text = Resources.Distance;
            xrCell7.Text = Resources.DistanceTotal; 
            xrCell8.Text = Resources.Comment;
        }
    }
}
