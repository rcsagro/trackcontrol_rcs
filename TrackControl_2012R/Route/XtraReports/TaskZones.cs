using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace Route.XtraReports
{
    public partial class TaskZones : Route.XtraReports.TaskHeader
    {
        
        public TaskZones(int Id_task):base(Id_task)
        {
            InitializeComponent();
            subTaskZone1.Init(Id_task); 
        }
    }
}
