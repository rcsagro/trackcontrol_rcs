namespace Route.XtraReports
{
    partial class SubTaskZone
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrDetalTable = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrDetalCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrDetalCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrDetalCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrDetalCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrDetalCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrDetalCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrDetalCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrDetalCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrControlStyle1 = new DevExpress.XtraReports.UI.XRControlStyle();
            ((System.ComponentModel.ISupportInitialize)(this.xrDetalTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrDetalTable});
            this.Detail.Height = 25;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrDetalTable
            // 
            this.xrDetalTable.BorderColor = System.Drawing.Color.Black;
            this.xrDetalTable.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrDetalTable.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrDetalTable.Location = new System.Drawing.Point(0, 0);
            this.xrDetalTable.Name = "xrDetalTable";
            this.xrDetalTable.OddStyleName = "xrControlStyle1";
            this.xrDetalTable.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow9});
            this.xrDetalTable.Size = new System.Drawing.Size(1067, 25);
            this.xrDetalTable.StylePriority.UseBorderColor = false;
            this.xrDetalTable.StylePriority.UseBorders = false;
            this.xrDetalTable.StylePriority.UseFont = false;
            this.xrDetalTable.StylePriority.UseTextAlignment = false;
            this.xrDetalTable.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.BorderColor = System.Drawing.Color.Silver;
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrDetalCell1,
            this.xrDetalCell2,
            this.xrDetalCell3,
            this.xrDetalCell4,
            this.xrDetalCell5,
            this.xrDetalCell6,
            this.xrDetalCell7,
            this.xrDetalCell8});
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.StylePriority.UseBorderColor = false;
            this.xrTableRow9.Weight = 1;
            // 
            // xrDetalCell1
            // 
            this.xrDetalCell1.Name = "xrDetalCell1";
            this.xrDetalCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100F);
            this.xrDetalCell1.StylePriority.UsePadding = false;
            this.xrDetalCell1.StylePriority.UseTextAlignment = false;
            this.xrDetalCell1.Text = "����������� ����";
            this.xrDetalCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrDetalCell1.Weight = 0.89;
            // 
            // xrDetalCell2
            // 
            this.xrDetalCell2.Name = "xrDetalCell2";
            this.xrDetalCell2.Text = "�������";
            this.xrDetalCell2.Weight = 0.21999999999999997;
            // 
            // xrDetalCell3
            // 
            this.xrDetalCell3.Name = "xrDetalCell3";
            this.xrDetalCell3.Text = "�������� �����";
            this.xrDetalCell3.Weight = 0.42999999999999994;
            // 
            // xrDetalCell4
            // 
            this.xrDetalCell4.Name = "xrDetalCell4";
            this.xrDetalCell4.Text = "����������� �����";
            this.xrDetalCell4.Weight = 0.46000000000000008;
            // 
            // xrDetalCell5
            // 
            this.xrDetalCell5.Name = "xrDetalCell5";
            this.xrDetalCell5.Text = "����������";
            this.xrDetalCell5.Weight = 0.27666666666666662;
            // 
            // xrDetalCell6
            // 
            this.xrDetalCell6.Name = "xrDetalCell6";
            this.xrDetalCell6.Text = "����, ��";
            this.xrDetalCell6.Weight = 0.30666666666666653;
            // 
            // xrDetalCell7
            // 
            this.xrDetalCell7.Name = "xrDetalCell7";
            this.xrDetalCell7.Text = "���� �����, ��";
            this.xrDetalCell7.Weight = 0.33333333333333326;
            // 
            // xrDetalCell8
            // 
            this.xrDetalCell8.Name = "xrDetalCell8";
            this.xrDetalCell8.Padding = new DevExpress.XtraPrinting.PaddingInfo(5, 0, 0, 0, 100F);
            this.xrDetalCell8.StylePriority.UsePadding = false;
            this.xrDetalCell8.StylePriority.UseTextAlignment = false;
            this.xrDetalCell8.Text = "�����������";
            this.xrDetalCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrDetalCell8.Weight = 0.63999999999999968;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel1,
            this.xrTable1});
            this.PageHeader.Height = 50;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.PageHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel1.Location = new System.Drawing.Point(0, 0);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.Size = new System.Drawing.Size(1067, 25);
            this.xrLabel1.StylePriority.UseBorders = false;
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "���������� ��������";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable1
            // 
            this.xrTable1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable1.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrTable1.Location = new System.Drawing.Point(0, 25);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow8});
            this.xrTable1.Size = new System.Drawing.Size(1067, 25);
            this.xrTable1.StylePriority.UseBorders = false;
            this.xrTable1.StylePriority.UseFont = false;
            this.xrTable1.StylePriority.UseTextAlignment = false;
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrCell1,
            this.xrCell2,
            this.xrCell3,
            this.xrCell4,
            this.xrCell5,
            this.xrCell6,
            this.xrCell7,
            this.xrCell8});
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Weight = 1;
            // 
            // xrCell1
            // 
            this.xrCell1.Name = "xrCell1";
            this.xrCell1.Text = "����������� ����";
            this.xrCell1.Weight = 0.98888888888888893;
            // 
            // xrCell2
            // 
            this.xrCell2.Name = "xrCell2";
            this.xrCell2.Text = "�������";
            this.xrCell2.Weight = 0.24555555555555558;
            // 
            // xrCell3
            // 
            this.xrCell3.Name = "xrCell3";
            this.xrCell3.Text = "�������� �����";
            this.xrCell3.Weight = 0.4777777777777778;
            // 
            // xrCell4
            // 
            this.xrCell4.Name = "xrCell4";
            this.xrCell4.Text = "����������� �����";
            this.xrCell4.Weight = 0.51111111111111118;
            // 
            // xrCell5
            // 
            this.xrCell5.Name = "xrCell5";
            this.xrCell5.Text = "����������";
            this.xrCell5.Weight = 0.30629629629629629;
            // 
            // xrCell6
            // 
            this.xrCell6.Name = "xrCell6";
            this.xrCell6.Text = "����, ��";
            this.xrCell6.Weight = 0.33962962962962956;
            // 
            // xrCell7
            // 
            this.xrCell7.Name = "xrCell7";
            this.xrCell7.Text = "���� �����, ��";
            this.xrCell7.Weight = 0.37111111111111122;
            // 
            // xrCell8
            // 
            this.xrCell8.Name = "xrCell8";
            this.xrCell8.Text = "�����������";
            this.xrCell8.Weight = 0.71148148148148138;
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0;
            this.PageFooter.Name = "PageFooter";
            this.PageFooter.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.PageFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrControlStyle1
            // 
            this.xrControlStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.xrControlStyle1.Name = "xrControlStyle1";
            this.xrControlStyle1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            // 
            // SubTaskZone
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageHeader,
            this.PageFooter});
            this.GridSize = new System.Drawing.Size(4, 4);
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(50, 50, 50, 50);
            this.PageHeight = 827;
            this.PageWidth = 1169;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.StyleSheet.AddRange(new DevExpress.XtraReports.UI.XRControlStyle[] {
            this.xrControlStyle1});
            this.Version = "9.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrDetalTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.XRTable xrDetalTable;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow9;
        private DevExpress.XtraReports.UI.XRTableCell xrDetalCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrDetalCell2;
        private DevExpress.XtraReports.UI.XRTableCell xrDetalCell3;
        private DevExpress.XtraReports.UI.XRTableCell xrDetalCell4;
        private DevExpress.XtraReports.UI.XRTableCell xrDetalCell5;
        private DevExpress.XtraReports.UI.XRTableCell xrDetalCell6;
        private DevExpress.XtraReports.UI.XRTableCell xrDetalCell7;
        private DevExpress.XtraReports.UI.XRTableCell xrDetalCell8;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
        private DevExpress.XtraReports.UI.XRTableCell xrCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrCell2;
        private DevExpress.XtraReports.UI.XRTableCell xrCell3;
        private DevExpress.XtraReports.UI.XRTableCell xrCell4;
        private DevExpress.XtraReports.UI.XRTableCell xrCell5;
        private DevExpress.XtraReports.UI.XRTableCell xrCell6;
        private DevExpress.XtraReports.UI.XRTableCell xrCell7;
        private DevExpress.XtraReports.UI.XRTableCell xrCell8;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRControlStyle xrControlStyle1;
    }
}
