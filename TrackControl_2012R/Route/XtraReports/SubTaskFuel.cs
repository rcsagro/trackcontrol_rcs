using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Route.Properties;

namespace Route.XtraReports
{
    public partial class SubTaskFuel : DevExpress.XtraReports.UI.XtraReport, Route.XtraReports.ISubReport
    {
        public SubTaskFuel()
        {
            InitializeComponent();
            Localization();
        }
        public void Init(int Id_task)
        {
            using (TaskItem ti = new TaskItem(Id_task))
            {
                DataSource = ti.GetFuelDUT();
            }
            CreateDataBindings();
        }
        private void CreateDataBindings()
        {
            xrDetalTable.DataBindings.Add(new DevExpress.XtraReports.UI.XRBinding("Bookmark", DataSource, "Location", "{0}"));
            xrDetalCell1.DataBindings.Add("Text", DataSource, "Location");
            xrDetalCell2.DataBindings.Add("Text", DataSource, "time_");
            xrDetalCell3.DataBindings.Add("Text", DataSource, "dValueCalc");
            xrDetalCell4.DataBindings.Add("Text", DataSource, "dValueHandle");
        }
        private void Localization()
        {
            xrDetalCell1.Text = Resources.Location;
            xrDetalCell2.Text = Resources.Time;
            xrDetalCell3.Text = Resources.FuelAddSub ;
            xrTableCell6.Text = Resources.Location;
            xrTableCell7.Text = Resources.Time;
            xrTableCell8.Text = Resources.FuelAddSub;
            xrLabel1.Text = Resources.FuelAddsSubs;
            xrTableCell1.Text = Resources.AfterCorrection;

        }
    }
}
