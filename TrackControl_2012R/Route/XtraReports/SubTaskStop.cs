using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Route.Properties;

namespace Route.XtraReports
{
    public partial class SubTaskStop : DevExpress.XtraReports.UI.XtraReport, Route.XtraReports.ISubReport
    {
        public SubTaskStop()
        {
            InitializeComponent();
            Localization();
        }
        public void Init(int Id_task)
        {
            using (TaskItem ti = new TaskItem(Id_task))
            {
                DataSource = ti.GetStops();
            }
            CreateDataBindings();
        }
        private void CreateDataBindings()
        {
            xrDetalTable.DataBindings.Add(new DevExpress.XtraReports.UI.XRBinding("Bookmark", DataSource, "Location", "{0}"));
            xrDetalCell1.DataBindings.Add("Text", DataSource, "Location");
            xrDetalCell2.DataBindings.Add("Text", DataSource, "InitialTime");
            xrDetalCell3.DataBindings.Add("Text", DataSource, "FinalTime");
            xrDetalCell4.DataBindings.Add("Text", DataSource, "Interval");
        }
        private void Localization()
        {
            xrDetalCell1.Text = Resources.Location;
            xrDetalCell2.Text = Resources.TimeStart;
            xrDetalCell3.Text = Resources.TimeEnd;
            xrDetalCell4.Text = Resources.Interval;
            xrLabel1.Text = Resources.StopsOnRoute;
            xrTableCell6.Text = Resources.Location;
            xrTableCell7.Text = Resources.TimeStart;
            xrTableCell8.Text = Resources.TimeEnd;
            xrTableCell9.Text = Resources.Interval;
        }
    }
}
