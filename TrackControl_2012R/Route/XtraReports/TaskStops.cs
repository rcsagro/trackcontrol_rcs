using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace Route.XtraReports
{
    public partial class TaskStops : Route.XtraReports.TaskHeader
    {
        public TaskStops(int Id_task): base(Id_task)
        {
            InitializeComponent();
            subTaskStop1.Init(Id_task); 
        } 

    }
}
