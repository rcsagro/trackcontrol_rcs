namespace Route.XtraReports
{
    partial class TaskZones
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.xrStyleOdd = new DevExpress.XtraReports.UI.XRControlStyle();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.xrSubZones = new DevExpress.XtraReports.UI.XRSubreport();
            this.subTaskZone1 = new Route.XtraReports.SubTaskZone();
            ((System.ComponentModel.ISupportInitialize)(this.xrHeaderTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subTaskZone1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // xrHeader
            // 
            this.xrHeader.StylePriority.UseFont = false;
            this.xrHeader.StylePriority.UseTextAlignment = false;
            // 
            // xrHeaderTable
            // 
            this.xrHeaderTable.StylePriority.UseBorders = false;
            this.xrHeaderTable.StylePriority.UseFont = false;
            // 
            // CellVehicle
            // 
            this.CellVehicle.StylePriority.UseBorders = false;
            this.CellVehicle.StylePriority.UseFont = false;
            this.CellVehicle.StylePriority.UsePadding = false;
            this.CellVehicle.StylePriority.UseTextAlignment = false;
            // 
            // CellDriver
            // 
            this.CellDriver.StylePriority.UseBorders = false;
            this.CellDriver.StylePriority.UseFont = false;
            this.CellDriver.StylePriority.UsePadding = false;
            this.CellDriver.StylePriority.UseTextAlignment = false;
            // 
            // CellSample
            // 
            this.CellSample.StylePriority.UseBorders = false;
            this.CellSample.StylePriority.UseFont = false;
            this.CellSample.StylePriority.UsePadding = false;
            this.CellSample.StylePriority.UseTextAlignment = false;
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSubZones});
            this.Detail.Height = 26;
            // 
            // xrStyleOdd
            // 
            this.xrStyleOdd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.xrStyleOdd.Name = "xrStyleOdd";
            this.xrStyleOdd.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Location = new System.Drawing.Point(988, 0);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo1.Size = new System.Drawing.Size(79, 21);
            this.xrPageInfo1.StylePriority.UseTextAlignment = false;
            this.xrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrSubZones
            // 
            this.xrSubZones.Location = new System.Drawing.Point(0, 0);
            this.xrSubZones.Name = "xrSubZones";
            this.xrSubZones.ReportSource = this.subTaskZone1;
            this.xrSubZones.Size = new System.Drawing.Size(1062, 26);
            // 
            // subTaskZone1
            // 
            this.subTaskZone1.GridSize = new System.Drawing.Size(4, 4);
            this.subTaskZone1.Landscape = true;
            this.subTaskZone1.Margins = new System.Drawing.Printing.Margins(50, 50, 50, 50);
            this.subTaskZone1.Name = "subTaskZone1";
            this.subTaskZone1.PageColor = System.Drawing.Color.White;
            this.subTaskZone1.PageHeight = 827;
            this.subTaskZone1.PageWidth = 1169;
            this.subTaskZone1.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.subTaskZone1.Version = "9.2";
            // 
            // TaskZones
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail});
            this.StyleSheet.AddRange(new DevExpress.XtraReports.UI.XRControlStyle[] {
            this.xrStyleOdd});
            this.Version = "9.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrHeaderTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subTaskZone1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.XRControlStyle xrStyleOdd;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo1;
        private DevExpress.XtraReports.UI.XRSubreport xrSubZones;
        private SubTaskZone subTaskZone1;
    }
}
