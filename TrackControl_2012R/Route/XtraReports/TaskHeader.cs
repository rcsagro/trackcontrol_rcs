using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Route.Properties;

namespace Route.XtraReports
{
    public partial class TaskHeader : DevExpress.XtraReports.UI.XtraReport
    {
        public TaskHeader()
        {
            InitializeComponent();
            Localization();
        }
        public TaskHeader(int Id_task):this()
        {
            xrHeader.Text = Resources.RouteList + " " + Id_task.ToString();
            using (TaskItem ti_prop = new TaskItem(Id_task))
            {
                //--------����� ���������� ---------------------
                CellVehicle.Text = ti_prop.VehicleName;
                CellDriver.Text = ti_prop.DriverName;
                CellSample.Text = ti_prop.SampleName;
                CellRemark.Text = ti_prop.Remark;
                CellStart.Text = ti_prop.Date.ToString();
                CellEnd.Text = ti_prop.DateEnd.ToString();
                //--------���������� �� �������------------------
                CellDistance.Text = ti_prop.Distance.ToString();
                CellTimeMove.Text = ti_prop.TimeMoveProc.ToString();
                CellTimeStop.Text = ti_prop.TimeStopProc.ToString();
                CellSpeedAvg.Text = ti_prop.SpeedAvg.ToString();
                //--------����������������� ��������-------------
                CellTimePlanTotal.Text = ti_prop.TimePlanTotal.ToString();
                CellTimeFactTotal.Text = ti_prop.TimeFactTotal.ToString();
                CellDeviation.Text = ti_prop.Deviation.ToString();
                CellDeviationAr.Text = ti_prop.DeviationAr.ToString();
                //--------�������-------------------------------
                CellFuelStart.Text = ti_prop.FuelStart.ToString();
                CellFuelEnd.Text = ti_prop.FuelEnd.ToString();
                CellFuelAdd.Text = ti_prop.FuelAdd.ToString();
                CellFuelExpens.Text = ti_prop.FuelExpens == 0 ? ti_prop.Fuel_ExpensTotal.ToString() : ti_prop.FuelExpens.ToString();
                CellFuelExpensAvg.Text = ti_prop.FuelExpensAvg == 0 ? ti_prop.Fuel_ExpensAvg.ToString() : ti_prop.FuelExpensAvg.ToString();
            }
        }
        private void Localization()
        {
            xrTableCell22.Text = Resources.TotalInformation ;
            xrTableCell24.Text = Resources.DistanceInformation;
            xrTableCell29.Text = Resources.RouteDuration;
            xrTableCell31.Text = Resources.Fuel;
            xrHeaderCell1.Text = Resources.Car;
            xrHeaderCell11.Text = Resources.DistanceTotalKm;
            xrTableCell1.Text = Resources.Plan;
            xrTableCell11.Text = Resources.StartL;
            xrHeaderCell2.Text = Resources.Driver;
            xrHeaderCell12.Text = Resources.MovingTime ;
            xrTableCell2.Text = Resources.Fact;
            xrTableCell12.Text = Resources.EndL;
            xrHeaderCell3.Text = Resources.Route;
            xrHeaderCell13.Text = Resources.StopsTime;
            xrTableCell3.Text = Resources.DurationDeviation;
            xrTableCell13.Text = Resources.FuelL;
            lbCellStart.Text = Resources.StartDateTime;
            xrHeaderCell14.Text = Resources.SpeedAvgKmH_1;
            xrTableCell4.Text = Resources.DeviationArrival_1;
            xrTableCell14.Text = Resources.FuelSubL_1;
            xrHeaderCell5.Text = Resources.EndDateTime;
            xrTableCell15.Text = Resources.ExpenseL;
            xrTableCell21.Text = Resources.Comment;
            xrTableCell27.Text = Resources.ExpenseL100;

        }

    }
}
