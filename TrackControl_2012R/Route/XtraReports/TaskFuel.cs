using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Route.Properties;

namespace Route.XtraReports
{
    public partial class TaskFuel : Route.XtraReports.TaskHeader
    {
        public TaskFuel(int Id_task): base(Id_task)
        {
            InitializeComponent();
            Localization();
            subTaskFuel1.Init(Id_task); 
        }
        private void Localization()
        {

        }
    }
}
