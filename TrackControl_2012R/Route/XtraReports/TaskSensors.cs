using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace Route.XtraReports
{
    public partial class TaskSensors : Route.XtraReports.TaskHeader
    {
        public TaskSensors(int Id_task): base(Id_task)
        {
            InitializeComponent();
            subTaskSensor1.Init(Id_task);  
        }
    }
}
