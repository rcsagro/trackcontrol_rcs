using System;
using System.Drawing;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace Route.XtraReports
{
    public partial class TaskTotal : Route.XtraReports.TaskHeader
    {
        public TaskTotal(int Id_task):base(Id_task)
        {
            InitializeComponent();
            subTaskZone1.Init(Id_task);
            subTaskStop1.Init(Id_task);
            subTaskSensor1.Init(Id_task);
            subTaskFuel1.Init(Id_task);  
        }

    }
}
