namespace Route.XtraReports
{
    partial class TaskHeader
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            this.xrPageInfo1 = new DevExpress.XtraReports.UI.XRPageInfo();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrHeader = new DevExpress.XtraReports.UI.XRLabel();
            this.xrHeaderTable = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrHeaderCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.CellVehicle = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrHeaderCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.CellDistance = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.CellTimePlanTotal = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.CellFuelStart = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrHeaderCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.CellDriver = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrHeaderCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.CellTimeMove = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.CellTimeFactTotal = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.CellFuelEnd = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrHeaderCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.CellSample = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrHeaderCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.CellTimeStop = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.CellDeviation = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.CellFuelAdd = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lbCellStart = new DevExpress.XtraReports.UI.XRTableCell();
            this.CellStart = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrHeaderCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.CellSpeedAvg = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.CellDeviationAr = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.CellFuelSub = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrHeaderCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.CellEnd = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrHeaderCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrHeaderCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.CellFuelExpens = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.CellRemark = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.CellFuelExpensAvg = new DevExpress.XtraReports.UI.XRTableCell();
            ((System.ComponentModel.ISupportInitialize)(this.xrHeaderTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Height = 0;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // PageFooter
            // 
            this.PageFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPageInfo1});
            this.PageFooter.Height = 25;
            this.PageFooter.Name = "PageFooter";
            this.PageFooter.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.PageFooter.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrPageInfo1
            // 
            this.xrPageInfo1.Location = new System.Drawing.Point(967, 0);
            this.xrPageInfo1.Name = "xrPageInfo1";
            this.xrPageInfo1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrPageInfo1.Size = new System.Drawing.Size(100, 25);
            this.xrPageInfo1.StylePriority.UseTextAlignment = false;
            this.xrPageInfo1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrHeader,
            this.xrHeaderTable});
            this.ReportHeader.Height = 246;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // xrHeader
            // 
            this.xrHeader.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.xrHeader.Location = new System.Drawing.Point(104, 8);
            this.xrHeader.Name = "xrHeader";
            this.xrHeader.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrHeader.Size = new System.Drawing.Size(938, 33);
            this.xrHeader.StylePriority.UseFont = false;
            this.xrHeader.StylePriority.UseTextAlignment = false;
            this.xrHeader.Text = "���������� ���� �";
            this.xrHeader.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrHeaderTable
            // 
            this.xrHeaderTable.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrHeaderTable.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.xrHeaderTable.Location = new System.Drawing.Point(28, 46);
            this.xrHeaderTable.Name = "xrHeaderTable";
            this.xrHeaderTable.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2,
            this.xrTableRow3,
            this.xrTableRow7,
            this.xrTableRow6,
            this.xrTableRow5,
            this.xrTableRow4,
            this.xrTableRow1});
            this.xrHeaderTable.Size = new System.Drawing.Size(1013, 175);
            this.xrHeaderTable.StylePriority.UseBorders = false;
            this.xrHeaderTable.StylePriority.UseFont = false;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell22,
            this.xrTableCell24,
            this.xrTableCell29,
            this.xrTableCell31});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.StylePriority.UseTextAlignment = false;
            this.xrTableRow2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableRow2.Weight = 1;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell22.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.StylePriority.UseBorders = false;
            this.xrTableCell22.StylePriority.UseFont = false;
            this.xrTableCell22.Text = "����� ���������� ";
            this.xrTableCell22.Weight = 2.9674396594314336;
            // 
            // xrTableCell24
            // 
            this.xrTableCell24.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell24.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.xrTableCell24.Name = "xrTableCell24";
            this.xrTableCell24.StylePriority.UseBorders = false;
            this.xrTableCell24.StylePriority.UseFont = false;
            this.xrTableCell24.Text = "���������� �� �������";
            this.xrTableCell24.Weight = 1.7016864387814825;
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell29.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.StylePriority.UseBorders = false;
            this.xrTableCell29.StylePriority.UseFont = false;
            this.xrTableCell29.Text = "����������������� ��������";
            this.xrTableCell29.Weight = 1.8684561454566573;
            // 
            // xrTableCell31
            // 
            this.xrTableCell31.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell31.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.xrTableCell31.Name = "xrTableCell31";
            this.xrTableCell31.StylePriority.UseBorders = false;
            this.xrTableCell31.StylePriority.UseFont = false;
            this.xrTableCell31.Text = "�������";
            this.xrTableCell31.Weight = 1.2149687767385913;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrHeaderCell1,
            this.CellVehicle,
            this.xrHeaderCell11,
            this.CellDistance,
            this.xrTableCell1,
            this.CellTimePlanTotal,
            this.xrTableCell11,
            this.CellFuelStart});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Weight = 1;
            // 
            // xrHeaderCell1
            // 
            this.xrHeaderCell1.Name = "xrHeaderCell1";
            this.xrHeaderCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 4, 0, 0, 100F);
            this.xrHeaderCell1.StylePriority.UsePadding = false;
            this.xrHeaderCell1.StylePriority.UseTextAlignment = false;
            this.xrHeaderCell1.Text = "������       ";
            this.xrHeaderCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrHeaderCell1.Weight = 0.93035364830863854;
            // 
            // CellVehicle
            // 
            this.CellVehicle.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.CellVehicle.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CellVehicle.Name = "CellVehicle";
            this.CellVehicle.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100F);
            this.CellVehicle.StylePriority.UseBorders = false;
            this.CellVehicle.StylePriority.UseFont = false;
            this.CellVehicle.StylePriority.UsePadding = false;
            this.CellVehicle.StylePriority.UseTextAlignment = false;
            this.CellVehicle.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.CellVehicle.Weight = 2.0370860111227946;
            // 
            // xrHeaderCell11
            // 
            this.xrHeaderCell11.Name = "xrHeaderCell11";
            this.xrHeaderCell11.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 4, 0, 0, 100F);
            this.xrHeaderCell11.StylePriority.UsePadding = false;
            this.xrHeaderCell11.StylePriority.UseTextAlignment = false;
            this.xrHeaderCell11.Text = "����� ������, ��";
            this.xrHeaderCell11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrHeaderCell11.Weight = 0.990788931651822;
            // 
            // CellDistance
            // 
            this.CellDistance.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.CellDistance.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CellDistance.Name = "CellDistance";
            this.CellDistance.StylePriority.UseBorders = false;
            this.CellDistance.StylePriority.UseFont = false;
            this.CellDistance.StylePriority.UseTextAlignment = false;
            this.CellDistance.Text = "0";
            this.CellDistance.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.CellDistance.Weight = 0.7108975071296606;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 4, 0, 0, 100F);
            this.xrTableCell1.StylePriority.UsePadding = false;
            this.xrTableCell1.StylePriority.UseTextAlignment = false;
            this.xrTableCell1.Text = "����";
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell1.Weight = 1.1509363483099027;
            // 
            // CellTimePlanTotal
            // 
            this.CellTimePlanTotal.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.CellTimePlanTotal.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CellTimePlanTotal.Name = "CellTimePlanTotal";
            this.CellTimePlanTotal.StylePriority.UseBorders = false;
            this.CellTimePlanTotal.StylePriority.UseFont = false;
            this.CellTimePlanTotal.StylePriority.UseTextAlignment = false;
            this.CellTimePlanTotal.Text = " ";
            this.CellTimePlanTotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.CellTimePlanTotal.Weight = 0.7251728583712439;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 4, 0, 0, 100F);
            this.xrTableCell11.StylePriority.UsePadding = false;
            this.xrTableCell11.StylePriority.UseTextAlignment = false;
            this.xrTableCell11.Text = "� ������, �";
            this.xrTableCell11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell11.Weight = 0.7069741842876629;
            // 
            // CellFuelStart
            // 
            this.CellFuelStart.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.CellFuelStart.Name = "CellFuelStart";
            this.CellFuelStart.StylePriority.UseBorders = false;
            this.CellFuelStart.StylePriority.UseTextAlignment = false;
            this.CellFuelStart.Text = "0";
            this.CellFuelStart.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.CellFuelStart.Weight = 0.50034153122643821;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrHeaderCell2,
            this.CellDriver,
            this.xrHeaderCell12,
            this.CellTimeMove,
            this.xrTableCell2,
            this.CellTimeFactTotal,
            this.xrTableCell12,
            this.CellFuelEnd});
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Weight = 1;
            // 
            // xrHeaderCell2
            // 
            this.xrHeaderCell2.Name = "xrHeaderCell2";
            this.xrHeaderCell2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 4, 0, 0, 100F);
            this.xrHeaderCell2.StylePriority.UsePadding = false;
            this.xrHeaderCell2.StylePriority.UseTextAlignment = false;
            this.xrHeaderCell2.Text = "��������  ";
            this.xrHeaderCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrHeaderCell2.Weight = 0.9303536483086382;
            // 
            // CellDriver
            // 
            this.CellDriver.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.CellDriver.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CellDriver.Name = "CellDriver";
            this.CellDriver.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100F);
            this.CellDriver.StylePriority.UseBorders = false;
            this.CellDriver.StylePriority.UseFont = false;
            this.CellDriver.StylePriority.UsePadding = false;
            this.CellDriver.StylePriority.UseTextAlignment = false;
            this.CellDriver.Text = " ";
            this.CellDriver.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.CellDriver.Weight = 2.0370860111227946;
            // 
            // xrHeaderCell12
            // 
            this.xrHeaderCell12.Name = "xrHeaderCell12";
            this.xrHeaderCell12.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 4, 0, 0, 100F);
            this.xrHeaderCell12.StylePriority.UsePadding = false;
            this.xrHeaderCell12.StylePriority.UseTextAlignment = false;
            this.xrHeaderCell12.Text = "����� � ��������";
            this.xrHeaderCell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrHeaderCell12.Weight = 0.990788931651822;
            // 
            // CellTimeMove
            // 
            this.CellTimeMove.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.CellTimeMove.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CellTimeMove.Name = "CellTimeMove";
            this.CellTimeMove.StylePriority.UseBorders = false;
            this.CellTimeMove.StylePriority.UseFont = false;
            this.CellTimeMove.StylePriority.UseTextAlignment = false;
            this.CellTimeMove.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.CellTimeMove.Weight = 0.7108975071296606;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 4, 0, 0, 100F);
            this.xrTableCell2.StylePriority.UsePadding = false;
            this.xrTableCell2.StylePriority.UseTextAlignment = false;
            this.xrTableCell2.Text = "����";
            this.xrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell2.Weight = 1.1509363483099027;
            // 
            // CellTimeFactTotal
            // 
            this.CellTimeFactTotal.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.CellTimeFactTotal.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CellTimeFactTotal.Name = "CellTimeFactTotal";
            this.CellTimeFactTotal.StylePriority.UseBorders = false;
            this.CellTimeFactTotal.StylePriority.UseFont = false;
            this.CellTimeFactTotal.StylePriority.UseTextAlignment = false;
            this.CellTimeFactTotal.Text = " ";
            this.CellTimeFactTotal.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.CellTimeFactTotal.Weight = 0.7251728583712439;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 4, 0, 0, 100F);
            this.xrTableCell12.StylePriority.UsePadding = false;
            this.xrTableCell12.StylePriority.UseTextAlignment = false;
            this.xrTableCell12.Text = "� �����, �";
            this.xrTableCell12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell12.Weight = 0.69932112306317307;
            // 
            // CellFuelEnd
            // 
            this.CellFuelEnd.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.CellFuelEnd.Name = "CellFuelEnd";
            this.CellFuelEnd.StylePriority.UseBorders = false;
            this.CellFuelEnd.StylePriority.UseTextAlignment = false;
            this.CellFuelEnd.Text = "0";
            this.CellFuelEnd.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.CellFuelEnd.Weight = 0.50799459245092793;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrHeaderCell3,
            this.CellSample,
            this.xrHeaderCell13,
            this.CellTimeStop,
            this.xrTableCell3,
            this.CellDeviation,
            this.xrTableCell13,
            this.CellFuelAdd});
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Weight = 1;
            // 
            // xrHeaderCell3
            // 
            this.xrHeaderCell3.Name = "xrHeaderCell3";
            this.xrHeaderCell3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 4, 0, 0, 100F);
            this.xrHeaderCell3.StylePriority.UsePadding = false;
            this.xrHeaderCell3.StylePriority.UseTextAlignment = false;
            this.xrHeaderCell3.Text = "�������";
            this.xrHeaderCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrHeaderCell3.Weight = 0.930353648308638;
            // 
            // CellSample
            // 
            this.CellSample.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.CellSample.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CellSample.Name = "CellSample";
            this.CellSample.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 0, 0, 0, 100F);
            this.CellSample.StylePriority.UseBorders = false;
            this.CellSample.StylePriority.UseFont = false;
            this.CellSample.StylePriority.UsePadding = false;
            this.CellSample.StylePriority.UseTextAlignment = false;
            this.CellSample.Text = " ";
            this.CellSample.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.CellSample.Weight = 2.0370860111227946;
            // 
            // xrHeaderCell13
            // 
            this.xrHeaderCell13.Name = "xrHeaderCell13";
            this.xrHeaderCell13.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 4, 0, 0, 100F);
            this.xrHeaderCell13.StylePriority.UsePadding = false;
            this.xrHeaderCell13.StylePriority.UseTextAlignment = false;
            this.xrHeaderCell13.Text = "����� �������";
            this.xrHeaderCell13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrHeaderCell13.Weight = 0.990788931651822;
            // 
            // CellTimeStop
            // 
            this.CellTimeStop.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.CellTimeStop.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CellTimeStop.Name = "CellTimeStop";
            this.CellTimeStop.StylePriority.UseBorders = false;
            this.CellTimeStop.StylePriority.UseFont = false;
            this.CellTimeStop.StylePriority.UseTextAlignment = false;
            this.CellTimeStop.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.CellTimeStop.Weight = 0.71089750712966071;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 4, 0, 0, 100F);
            this.xrTableCell3.StylePriority.UsePadding = false;
            this.xrTableCell3.StylePriority.UseTextAlignment = false;
            this.xrTableCell3.Text = "���������� �� ������������";
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell3.Weight = 1.1509363483099024;
            // 
            // CellDeviation
            // 
            this.CellDeviation.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.CellDeviation.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CellDeviation.Name = "CellDeviation";
            this.CellDeviation.StylePriority.UseBorders = false;
            this.CellDeviation.StylePriority.UseFont = false;
            this.CellDeviation.StylePriority.UseTextAlignment = false;
            this.CellDeviation.Text = " ";
            this.CellDeviation.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.CellDeviation.Weight = 0.7251728583712439;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 4, 0, 0, 100F);
            this.xrTableCell13.StylePriority.UsePadding = false;
            this.xrTableCell13.StylePriority.UseTextAlignment = false;
            this.xrTableCell13.Text = "����������, �";
            this.xrTableCell13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell13.Weight = 0.69932112306317307;
            // 
            // CellFuelAdd
            // 
            this.CellFuelAdd.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.CellFuelAdd.Name = "CellFuelAdd";
            this.CellFuelAdd.StylePriority.UseBorders = false;
            this.CellFuelAdd.StylePriority.UseTextAlignment = false;
            this.CellFuelAdd.Text = "0";
            this.CellFuelAdd.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.CellFuelAdd.Weight = 0.50799459245092793;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lbCellStart,
            this.CellStart,
            this.xrHeaderCell14,
            this.CellSpeedAvg,
            this.xrTableCell4,
            this.CellDeviationAr,
            this.xrTableCell14,
            this.CellFuelSub});
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Weight = 1;
            // 
            // lbCellStart
            // 
            this.lbCellStart.Name = "lbCellStart";
            this.lbCellStart.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 4, 0, 0, 100F);
            this.lbCellStart.StylePriority.UsePadding = false;
            this.lbCellStart.StylePriority.UseTextAlignment = false;
            this.lbCellStart.Text = "���� � ����� ������";
            this.lbCellStart.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lbCellStart.Weight = 0.93035364830863831;
            // 
            // CellStart
            // 
            this.CellStart.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.CellStart.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CellStart.Name = "CellStart";
            this.CellStart.StylePriority.UseBorders = false;
            this.CellStart.StylePriority.UseFont = false;
            this.CellStart.StylePriority.UseTextAlignment = false;
            this.CellStart.Text = " ";
            this.CellStart.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.CellStart.Weight = 2.0370860111227951;
            // 
            // xrHeaderCell14
            // 
            this.xrHeaderCell14.Name = "xrHeaderCell14";
            this.xrHeaderCell14.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 4, 0, 0, 100F);
            this.xrHeaderCell14.StylePriority.UsePadding = false;
            this.xrHeaderCell14.StylePriority.UseTextAlignment = false;
            this.xrHeaderCell14.Text = "������� ��������, ��/�";
            this.xrHeaderCell14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrHeaderCell14.Weight = 0.990788931651822;
            // 
            // CellSpeedAvg
            // 
            this.CellSpeedAvg.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.CellSpeedAvg.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CellSpeedAvg.Name = "CellSpeedAvg";
            this.CellSpeedAvg.StylePriority.UseBorders = false;
            this.CellSpeedAvg.StylePriority.UseFont = false;
            this.CellSpeedAvg.StylePriority.UseTextAlignment = false;
            this.CellSpeedAvg.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.CellSpeedAvg.Weight = 0.7108975071296606;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 4, 0, 0, 100F);
            this.xrTableCell4.StylePriority.UsePadding = false;
            this.xrTableCell4.StylePriority.UseTextAlignment = false;
            this.xrTableCell4.Text = "���������� �� ��������";
            this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell4.Weight = 1.1509363483099027;
            // 
            // CellDeviationAr
            // 
            this.CellDeviationAr.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.CellDeviationAr.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CellDeviationAr.Name = "CellDeviationAr";
            this.CellDeviationAr.StylePriority.UseBorders = false;
            this.CellDeviationAr.StylePriority.UseFont = false;
            this.CellDeviationAr.StylePriority.UseTextAlignment = false;
            this.CellDeviationAr.Text = " ";
            this.CellDeviationAr.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.CellDeviationAr.Weight = 0.7251728583712439;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 4, 0, 0, 100F);
            this.xrTableCell14.StylePriority.UsePadding = false;
            this.xrTableCell14.StylePriority.UseTextAlignment = false;
            this.xrTableCell14.Text = "�����, �";
            this.xrTableCell14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell14.Weight = 0.69932112306317307;
            // 
            // CellFuelSub
            // 
            this.CellFuelSub.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.CellFuelSub.Name = "CellFuelSub";
            this.CellFuelSub.StylePriority.UseBorders = false;
            this.CellFuelSub.StylePriority.UseTextAlignment = false;
            this.CellFuelSub.Text = "0";
            this.CellFuelSub.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.CellFuelSub.Weight = 0.50799459245092793;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrHeaderCell5,
            this.CellEnd,
            this.xrHeaderCell15,
            this.xrHeaderCell20,
            this.xrTableCell5,
            this.xrTableCell10,
            this.xrTableCell15,
            this.CellFuelExpens});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Weight = 1;
            // 
            // xrHeaderCell5
            // 
            this.xrHeaderCell5.Name = "xrHeaderCell5";
            this.xrHeaderCell5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 4, 0, 0, 100F);
            this.xrHeaderCell5.StylePriority.UsePadding = false;
            this.xrHeaderCell5.StylePriority.UseTextAlignment = false;
            this.xrHeaderCell5.Text = "���� � ����� ���������";
            this.xrHeaderCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrHeaderCell5.Weight = 0.9303536483086382;
            // 
            // CellEnd
            // 
            this.CellEnd.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.CellEnd.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CellEnd.Name = "CellEnd";
            this.CellEnd.StylePriority.UseBorders = false;
            this.CellEnd.StylePriority.UseFont = false;
            this.CellEnd.StylePriority.UseTextAlignment = false;
            this.CellEnd.Text = " ";
            this.CellEnd.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.CellEnd.Weight = 2.0370860111227951;
            // 
            // xrHeaderCell15
            // 
            this.xrHeaderCell15.Name = "xrHeaderCell15";
            this.xrHeaderCell15.StylePriority.UseTextAlignment = false;
            this.xrHeaderCell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrHeaderCell15.Weight = 0.990788931651822;
            // 
            // xrHeaderCell20
            // 
            this.xrHeaderCell20.Name = "xrHeaderCell20";
            this.xrHeaderCell20.Weight = 0.7108975071296606;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.StylePriority.UseTextAlignment = false;
            this.xrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell5.Weight = 1.1509363483099027;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.StylePriority.UseTextAlignment = false;
            this.xrTableCell10.Text = " ";
            this.xrTableCell10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.xrTableCell10.Weight = 0.7251728583712439;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 4, 0, 0, 100F);
            this.xrTableCell15.StylePriority.UsePadding = false;
            this.xrTableCell15.StylePriority.UseTextAlignment = false;
            this.xrTableCell15.Text = "������, �";
            this.xrTableCell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell15.Weight = 0.69932112306317307;
            // 
            // CellFuelExpens
            // 
            this.CellFuelExpens.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.CellFuelExpens.Name = "CellFuelExpens";
            this.CellFuelExpens.StylePriority.UseBorders = false;
            this.CellFuelExpens.StylePriority.UseTextAlignment = false;
            this.CellFuelExpens.Text = "0";
            this.CellFuelExpens.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.CellFuelExpens.Weight = 0.50799459245092793;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell21,
            this.CellRemark,
            this.xrTableCell27,
            this.CellFuelExpensAvg});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1;
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 4, 0, 0, 100F);
            this.xrTableCell21.StylePriority.UsePadding = false;
            this.xrTableCell21.StylePriority.UseTextAlignment = false;
            this.xrTableCell21.Text = "�����������";
            this.xrTableCell21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell21.Weight = 0.93078091775315375;
            // 
            // CellRemark
            // 
            this.CellRemark.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.CellRemark.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CellRemark.Name = "CellRemark";
            this.CellRemark.StylePriority.UseBorders = false;
            this.CellRemark.StylePriority.UseFont = false;
            this.CellRemark.StylePriority.UseTextAlignment = false;
            this.CellRemark.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.CellRemark.Weight = 5.6068013259164191;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 4, 0, 0, 100F);
            this.xrTableCell27.StylePriority.UsePadding = false;
            this.xrTableCell27.StylePriority.UseTextAlignment = false;
            this.xrTableCell27.Text = "������, �/100��";
            this.xrTableCell27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell27.Weight = 0.7069741842876629;
            // 
            // CellFuelExpensAvg
            // 
            this.CellFuelExpensAvg.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.CellFuelExpensAvg.Name = "CellFuelExpensAvg";
            this.CellFuelExpensAvg.StylePriority.UseBorders = false;
            this.CellFuelExpensAvg.StylePriority.UseTextAlignment = false;
            this.CellFuelExpensAvg.Text = "0";
            this.CellFuelExpensAvg.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.CellFuelExpensAvg.Weight = 0.50799459245092793;
            // 
            // TaskHeader
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageFooter,
            this.ReportHeader});
            this.GridSize = new System.Drawing.Size(4, 4);
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(50, 50, 50, 50);
            this.PageHeight = 827;
            this.PageWidth = 1169;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.Version = "9.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrHeaderTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        protected DevExpress.XtraReports.UI.XRLabel xrHeader;
        protected DevExpress.XtraReports.UI.XRTable xrHeaderTable;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell xrHeaderCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrHeaderCell11;
        private DevExpress.XtraReports.UI.XRTableCell CellDistance;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell CellTimePlanTotal;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
        private DevExpress.XtraReports.UI.XRTableCell CellFuelStart;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        private DevExpress.XtraReports.UI.XRTableCell xrHeaderCell2;
        private DevExpress.XtraReports.UI.XRTableCell xrHeaderCell12;
        private DevExpress.XtraReports.UI.XRTableCell CellTimeMove;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell CellTimeFactTotal;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell12;
        private DevExpress.XtraReports.UI.XRTableCell CellFuelEnd;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell xrHeaderCell3;
        private DevExpress.XtraReports.UI.XRTableCell xrHeaderCell13;
        private DevExpress.XtraReports.UI.XRTableCell CellTimeStop;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTableCell CellDeviation;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell13;
        private DevExpress.XtraReports.UI.XRTableCell CellFuelAdd;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell lbCellStart;
        private DevExpress.XtraReports.UI.XRTableCell CellStart;
        private DevExpress.XtraReports.UI.XRTableCell xrHeaderCell14;
        private DevExpress.XtraReports.UI.XRTableCell CellSpeedAvg;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableCell CellDeviationAr;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell14;
        private DevExpress.XtraReports.UI.XRTableCell CellFuelSub;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell xrHeaderCell5;
        private DevExpress.XtraReports.UI.XRTableCell CellEnd;
        private DevExpress.XtraReports.UI.XRTableCell xrHeaderCell15;
        private DevExpress.XtraReports.UI.XRTableCell xrHeaderCell20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell15;
        private DevExpress.XtraReports.UI.XRTableCell CellFuelExpens;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell24;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell29;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell31;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell CellRemark;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell27;
        private DevExpress.XtraReports.UI.XRTableCell CellFuelExpensAvg;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell21;
        public DevExpress.XtraReports.UI.XRTableCell CellVehicle;
        public DevExpress.XtraReports.UI.XRTableCell CellDriver;
        public DevExpress.XtraReports.UI.XRTableCell CellSample;
        protected DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.XRPageInfo xrPageInfo1;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
    }
}
