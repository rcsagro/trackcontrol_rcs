namespace Route.XtraReports
{
    partial class TaskTotal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.xrSubZone = new DevExpress.XtraReports.UI.XRSubreport();
            this.subTaskZone1 = new Route.XtraReports.SubTaskZone();
            this.xrSubStop = new DevExpress.XtraReports.UI.XRSubreport();
            this.subTaskStop1 = new Route.XtraReports.SubTaskStop();
            this.xrSubSensor = new DevExpress.XtraReports.UI.XRSubreport();
            this.subTaskSensor1 = new Route.XtraReports.SubTaskSensor();
            this.xrSubFuel = new DevExpress.XtraReports.UI.XRSubreport();
            this.subTaskFuel1 = new Route.XtraReports.SubTaskFuel();
            ((System.ComponentModel.ISupportInitialize)(this.xrHeaderTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subTaskZone1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subTaskStop1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subTaskSensor1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subTaskFuel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // xrHeader
            // 
            this.xrHeader.StylePriority.UseFont = false;
            this.xrHeader.StylePriority.UseTextAlignment = false;
            // 
            // xrHeaderTable
            // 
            this.xrHeaderTable.StylePriority.UseBorders = false;
            this.xrHeaderTable.StylePriority.UseFont = false;
            // 
            // CellVehicle
            // 
            this.CellVehicle.StylePriority.UseBorders = false;
            this.CellVehicle.StylePriority.UseFont = false;
            this.CellVehicle.StylePriority.UsePadding = false;
            this.CellVehicle.StylePriority.UseTextAlignment = false;
            // 
            // CellDriver
            // 
            this.CellDriver.StylePriority.UseBorders = false;
            this.CellDriver.StylePriority.UseFont = false;
            this.CellDriver.StylePriority.UsePadding = false;
            this.CellDriver.StylePriority.UseTextAlignment = false;
            // 
            // CellSample
            // 
            this.CellSample.StylePriority.UseBorders = false;
            this.CellSample.StylePriority.UseFont = false;
            this.CellSample.StylePriority.UsePadding = false;
            this.CellSample.StylePriority.UseTextAlignment = false;
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSubZone,
            this.xrSubStop,
            this.xrSubSensor,
            this.xrSubFuel});
            this.Detail.Height = 150;
            // 
            // xrSubZone
            // 
            this.xrSubZone.Location = new System.Drawing.Point(0, 0);
            this.xrSubZone.Name = "xrSubZone";
            this.xrSubZone.ReportSource = this.subTaskZone1;
            this.xrSubZone.Size = new System.Drawing.Size(1067, 25);
            // 
            // subTaskZone1
            // 
            this.subTaskZone1.GridSize = new System.Drawing.Size(4, 4);
            this.subTaskZone1.Landscape = true;
            this.subTaskZone1.Margins = new System.Drawing.Printing.Margins(50, 50, 50, 50);
            this.subTaskZone1.Name = "subTaskZone1";
            this.subTaskZone1.PageColor = System.Drawing.Color.White;
            this.subTaskZone1.PageHeight = 827;
            this.subTaskZone1.PageWidth = 1169;
            this.subTaskZone1.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.subTaskZone1.Version = "9.2";
            // 
            // xrSubStop
            // 
            this.xrSubStop.Location = new System.Drawing.Point(0, 38);
            this.xrSubStop.Name = "xrSubStop";
            this.xrSubStop.ReportSource = this.subTaskStop1;
            this.xrSubStop.Size = new System.Drawing.Size(1067, 25);
            // 
            // subTaskStop1
            // 
            this.subTaskStop1.Landscape = true;
            this.subTaskStop1.Margins = new System.Drawing.Printing.Margins(50, 50, 50, 50);
            this.subTaskStop1.Name = "subTaskStop1";
            this.subTaskStop1.PageColor = System.Drawing.Color.White;
            this.subTaskStop1.PageHeight = 827;
            this.subTaskStop1.PageWidth = 1169;
            this.subTaskStop1.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.subTaskStop1.Version = "9.2";
            // 
            // xrSubSensor
            // 
            this.xrSubSensor.Location = new System.Drawing.Point(0, 75);
            this.xrSubSensor.Name = "xrSubSensor";
            this.xrSubSensor.ReportSource = this.subTaskSensor1;
            this.xrSubSensor.Size = new System.Drawing.Size(1067, 29);
            // 
            // subTaskSensor1
            // 
            this.subTaskSensor1.Landscape = true;
            this.subTaskSensor1.Margins = new System.Drawing.Printing.Margins(50, 50, 50, 50);
            this.subTaskSensor1.Name = "subTaskSensor1";
            this.subTaskSensor1.PageColor = System.Drawing.Color.White;
            this.subTaskSensor1.PageHeight = 827;
            this.subTaskSensor1.PageWidth = 1169;
            this.subTaskSensor1.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.subTaskSensor1.Version = "9.2";
            // 
            // xrSubFuel
            // 
            this.xrSubFuel.Location = new System.Drawing.Point(0, 117);
            this.xrSubFuel.Name = "xrSubFuel";
            this.xrSubFuel.ReportSource = this.subTaskFuel1;
            this.xrSubFuel.Size = new System.Drawing.Size(1067, 25);
            // 
            // subTaskFuel1
            // 
            this.subTaskFuel1.Landscape = true;
            this.subTaskFuel1.Margins = new System.Drawing.Printing.Margins(50, 50, 50, 50);
            this.subTaskFuel1.Name = "subTaskFuel1";
            this.subTaskFuel1.PageColor = System.Drawing.Color.White;
            this.subTaskFuel1.PageHeight = 827;
            this.subTaskFuel1.PageWidth = 1169;
            this.subTaskFuel1.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.subTaskFuel1.Version = "9.2";
            // 
            // TaskTotal
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail});
            this.Version = "9.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrHeaderTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subTaskZone1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subTaskStop1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subTaskSensor1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subTaskFuel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.XRSubreport xrSubZone;
        private SubTaskZone subTaskZone1;
        private DevExpress.XtraReports.UI.XRSubreport xrSubStop;
        private SubTaskStop subTaskStop1;
        private DevExpress.XtraReports.UI.XRSubreport xrSubSensor;
        private SubTaskSensor subTaskSensor1;
        private DevExpress.XtraReports.UI.XRSubreport xrSubFuel;
        private SubTaskFuel subTaskFuel1;
    }
}
