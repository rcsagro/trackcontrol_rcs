namespace Route.XtraReports
{
    partial class TaskFuel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.xrControlStyle1 = new DevExpress.XtraReports.UI.XRControlStyle();
            this.xrSubFuel = new DevExpress.XtraReports.UI.XRSubreport();
            this.subTaskFuel1 = new Route.XtraReports.SubTaskFuel();
            ((System.ComponentModel.ISupportInitialize)(this.xrHeaderTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.subTaskFuel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // xrHeader
            // 
            this.xrHeader.StylePriority.UseFont = false;
            this.xrHeader.StylePriority.UseTextAlignment = false;
            // 
            // xrHeaderTable
            // 
            this.xrHeaderTable.StylePriority.UseBorders = false;
            this.xrHeaderTable.StylePriority.UseFont = false;
            // 
            // CellVehicle
            // 
            this.CellVehicle.StylePriority.UseBorders = false;
            this.CellVehicle.StylePriority.UseFont = false;
            this.CellVehicle.StylePriority.UsePadding = false;
            this.CellVehicle.StylePriority.UseTextAlignment = false;
            // 
            // CellDriver
            // 
            this.CellDriver.StylePriority.UseBorders = false;
            this.CellDriver.StylePriority.UseFont = false;
            this.CellDriver.StylePriority.UsePadding = false;
            this.CellDriver.StylePriority.UseTextAlignment = false;
            // 
            // CellSample
            // 
            this.CellSample.StylePriority.UseBorders = false;
            this.CellSample.StylePriority.UseFont = false;
            this.CellSample.StylePriority.UsePadding = false;
            this.CellSample.StylePriority.UseTextAlignment = false;
            // 
            // Detail
            // 
            this.Detail.BorderColor = System.Drawing.Color.Silver;
            this.Detail.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrSubFuel});
            this.Detail.Height = 25;
            this.Detail.OddStyleName = "xrControlStyle1";
            this.Detail.StylePriority.UseBorderColor = false;
            this.Detail.StylePriority.UseBorders = false;
            // 
            // xrControlStyle1
            // 
            this.xrControlStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.xrControlStyle1.Name = "xrControlStyle1";
            this.xrControlStyle1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            // 
            // xrSubFuel
            // 
            this.xrSubFuel.Location = new System.Drawing.Point(0, 0);
            this.xrSubFuel.Name = "xrSubFuel";
            this.xrSubFuel.ReportSource = this.subTaskFuel1;
            this.xrSubFuel.Size = new System.Drawing.Size(1067, 25);
            // 
            // subTaskFuel1
            // 
            this.subTaskFuel1.Landscape = true;
            this.subTaskFuel1.Margins = new System.Drawing.Printing.Margins(50, 50, 50, 50);
            this.subTaskFuel1.Name = "subTaskFuel1";
            this.subTaskFuel1.PageColor = System.Drawing.Color.White;
            this.subTaskFuel1.PageHeight = 827;
            this.subTaskFuel1.PageWidth = 1169;
            this.subTaskFuel1.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.subTaskFuel1.Version = "9.2";
            // 
            // TaskFuel
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail});
            this.StyleSheet.AddRange(new DevExpress.XtraReports.UI.XRControlStyle[] {
            this.xrControlStyle1});
            this.Version = "9.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrHeaderTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.subTaskFuel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.XRControlStyle xrControlStyle1;
        private DevExpress.XtraReports.UI.XRSubreport xrSubFuel;
        private SubTaskFuel subTaskFuel1;
    }
}
