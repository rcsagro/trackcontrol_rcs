using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Route.Properties;

namespace Route.XtraReports
{
    public partial class SubTaskSensor : DevExpress.XtraReports.UI.XtraReport, Route.XtraReports.ISubReport
    {
        public SubTaskSensor()
        {
            InitializeComponent();
            Localization();
        }
        public void Init(int Id_task)
        {
            using (TaskItem ti = new TaskItem(Id_task))
            {
                DataSource = ti.GetSensors();
            }
            CreateDataBindings();
        }
        private void CreateDataBindings()
        {
            xrDetalTable.DataBindings.Add(new DevExpress.XtraReports.UI.XRBinding("Bookmark", DataSource, "Description", "{0}"));
            xrDetalCell1.DataBindings.Add("Text", DataSource, "Description");
            xrDetalCell2.DataBindings.Add("Text", DataSource, "SensorName");
            xrDetalCell3.DataBindings.Add("Text", DataSource, "Location");
            xrDetalCell4.DataBindings.Add("Text", DataSource, "EventTime");
            xrDetalCell5.DataBindings.Add("Text", DataSource, "Duration");
            xrDetalCell6.DataBindings.Add("Text", DataSource, "Speed");
        }
        private void Localization()
        {
            xrTableCell18.Text = Resources.Event;
            xrTableCell19.Text = Resources.Sensor;
            xrTableCell20.Text = Resources.Location;
            xrTableCell23.Text = Resources.Time;
            xrTableCell25.Text = Resources.Duration;
            xrTableCell26.Text = Resources.SpeedKmH;
            xrLabel1.Text = Resources.SensorEvents;
            xrDetalCell1.Text = Resources.Event;
            xrDetalCell2.Text = Resources.Sensor;
            xrDetalCell3.Text = Resources.Location;
            xrDetalCell4.Text = Resources.Time;
            xrDetalCell5.Text = Resources.Duration;
            xrDetalCell6.Text = Resources.SpeedKmH;

        }
    }
}
