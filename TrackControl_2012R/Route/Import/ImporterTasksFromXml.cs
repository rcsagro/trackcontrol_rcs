﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using TrackControl.General;
using MySql.Data.MySqlClient;
using System.IO;
using System.Collections;
using TrackControl.General.DatabaseDriver;
using TrackControl.General.DAL;
using Route.Documents;
using Route.Dictionaries;
using TrackControl.Vehicles;

//<?xml version="1.0" encoding="UTF-8"?>
//<ROUTE>
//   <CARD>
//      <DATE_PLAN>2017-06-23</DATE_PLAN>   <!-- Плановая дата начала маршрута (задания) - ОБЯЗАТЕЛЬНОЕ ПОЛЕ -->
//      <TIME_PLAN>22:05:00</TIME_PLAN>   <!-- Плановое время начала маршрута (задания) - НЕ ОБЯЗАТЕЛЬНОЕ ПОЛЕ -->
//      <NUMBER>1234567</NUMBER>   <!-- Номер маршрута (задания) - ОБЯЗАТЕЛЬНОЕ ПОЛЕ -->
//      <BODY>
//         <AVTO>MAN TGS 18.440 сідловий тягач вантажний ВІ 40-51 СА</AVTO>   <!-- Автомобиль - НЕ ОБЯЗАТЕЛЬНОЕ ПОЛЕ -->
//         <AVTO_ID>9999999</AVTO_ID>   <!-- ID автомобиля - ОБЯЗАТЕЛЬНОЕ ПОЛЕ -->
//         <TRAILER>Прицеп_01</TRAILER>   <!-- прицеп - НЕ ОБЯЗАТЕЛЬНОЕ ПОЛЕ -->
//         <TRAILER_ID>8888888</TRAILER_ID>   <!-- ID прицепа - НЕ ОБЯЗАТЕЛЬНОЕ ПОЛЕ -->
//         <CITY>Кременчуг</CITY>   <!-- Маршрут - НЕ ОБЯЗАТЕЛЬНОЕ ПОЛЕ -->
//         <CITY_ID>3333333</CITY_ID>   <!-- ID маршрута - ОБЯЗАТЕЛЬНОЕ ПОЛЕ -->
//         <WORK_TYPE>Перевозка колбасы</WORK_TYPE>   <!-- вид работ - НЕ ОБЯЗАТЕЛЬНОЕ ПОЛЕ -->
//         <WORK_TYPE_ID>7777777</WORK_TYPE_ID>   <!-- ID вида работ - ОБЯЗАТЕЛЬНОЕ ПОЛЕ -->
//         <DRIVER>Петров П.П.</DRIVER>   <!-- Водитель - НЕ ОБЯЗАТЕЛЬНОЕ ПОЛЕ -->
//         <DRIVER_ID>5555555</DRIVER_ID>   <!-- ID водителя - НЕ ОБЯЗАТЕЛЬНОЕ ПОЛЕ -->
//         <DISPATCHER>Иванова И.И.</DISPATCHER>   <!-- Диспетчер - НЕ ОБЯЗАТЕЛЬНОЕ ПОЛЕ -->
//         <DISPATCHER_ID>4444444</DISPATCHER_ID>   <!-- ID диспетчера - НЕ ОБЯЗАТЕЛЬНОЕ ПОЛЕ -->
//         <TONNAGE>9999.999</TONNAGE>   <!-- Тоннаж автомобиля - НЕ ОБЯЗАТЕЛЬНОЕ ПОЛЕ -->
//      </BODY>
//   </CARD>
//   <CARD>
//      <DATE_PLAN>2017-06-23</DATE_PLAN>   <!-- Плановая дата начала маршрута (задания) - ОБЯЗАТЕЛЬНОЕ ПОЛЕ -->
//      <TIME_PLAN>23:05:00</TIME_PLAN>   <!-- Плановое время начала маршрута (задания) - НЕ ОБЯЗАТЕЛЬНОЕ ПОЛЕ -->
//      <NUMBER>7654321</NUMBER>   <!-- Номер маршрута (задания) - ОБЯЗАТЕЛЬНОЕ ПОЛЕ -->
//      <BODY>
//         <AVTO>MAN TGS 18.440 сідловий тягач вантажний ВІ 50-51 СА</AVTO>   <!-- Автомобиль - НЕ ОБЯЗАТЕЛЬНОЕ ПОЛЕ -->
//         <AVTO_ID>1999999</AVTO_ID>   <!-- ID автомобиля - ОБЯЗАТЕЛЬНОЕ ПОЛЕ -->
//         <TRAILER>Прицеп_01</TRAILER>   <!-- прицеп - НЕ ОБЯЗАТЕЛЬНОЕ ПОЛЕ -->
//         <TRAILER_ID>1888888</TRAILER_ID>   <!-- ID прицепа - НЕ ОБЯЗАТЕЛЬНОЕ ПОЛЕ -->
//         <CITY>Киев</CITY>   <!-- Маршрут - НЕ ОБЯЗАТЕЛЬНОЕ ПОЛЕ -->
//         <CITY_ID>1333333</CITY_ID>   <!-- ID маршрута - ОБЯЗАТЕЛЬНОЕ ПОЛЕ -->
//         <WORK_TYPE>Перевозка колбасы</WORK_TYPE>   <!-- вид работ - НЕ ОБЯЗАТЕЛЬНОЕ ПОЛЕ -->
//         <WORK_TYPE_ID>1777777</WORK_TYPE_ID>   <!-- ID вида работ - ОБЯЗАТЕЛЬНОЕ ПОЛЕ -->
//         <DRIVER>Петрова П.П.</DRIVER>   <!-- Водитель - НЕ ОБЯЗАТЕЛЬНОЕ ПОЛЕ -->
//         <DRIVER_ID>1555555</DRIVER_ID>   <!-- ID водителя - НЕ ОБЯЗАТЕЛЬНОЕ ПОЛЕ -->
//         <DISPATCHER>Иванов И.И.</DISPATCHER>   <!-- Диспетчер - НЕ ОБЯЗАТЕЛЬНОЕ ПОЛЕ -->
//         <DISPATCHER_ID>1444444</DISPATCHER_ID>   <!-- ID диспетчера - НЕ ОБЯЗАТЕЛЬНОЕ ПОЛЕ -->
//         <TONNAGE>1999.999</TONNAGE>   <!-- Тоннаж автомобиля - НЕ ОБЯЗАТЕЛЬНОЕ ПОЛЕ -->
//      </BODY>
//   </CARD>
//</ROUTE>

namespace Route.Import
{
    public  class ImporterTasksFromXml
    {
        public int TasksImported{ get; set; }

        public ImporterTasksFromXml()
        {
        }

        public void Start ()
        {
            var dlg = new OpenFileDialog {Title = "Выберите XML файл для ввода маршрутов", Filter = "XML - файл|*.xml"};
            if (dlg.ShowDialog () == DialogResult.OK) {
                var reader = new XmlTextReader (dlg.FileName) {WhitespaceHandling = WhitespaceHandling.None};
                string lastNodeName = "";
                string dateStart = "";
                string remark = "";
                TasksImported = 0;
                var taskItem = new TaskItem();
                while (reader.Read ())
                {
                    switch (reader.NodeType) {
                    case XmlNodeType.Element: // Узел является элементом.
                            if (reader.Name == "CARD")
                        {
                            taskItem = new TaskItem();
                            dateStart = "";
                            remark = "";
                        }
                        lastNodeName = reader.Name;
                        break;
                      case XmlNodeType.Text:
                            if (lastNodeName == "DATE_PLAN")
                            {
                                dateStart = reader.Value;
                            }
                            else if (lastNodeName == "TIME_PLAN")
                            {
                                dateStart = string.Format("{0} {1}", dateStart, reader.Value);
                                DateTime dtStart;
                                if (DateTime.TryParse(dateStart, out dtStart))
                                {
                                    taskItem.Date = dtStart;
                                }
                            }
                            else if (lastNodeName == "NUMBER")
                            {
                                taskItem.IdOutLink = reader.Value;
                            }
                            else if (lastNodeName == "AVTO_ID")
                            {
                                string idOutLinkVeh = reader.Value;
                                var vh = new Vehicle(0, idOutLinkVeh);
                                if (vh.MobitelId > 0) taskItem.MobitelId = vh.MobitelId;
                            }
                            else if (lastNodeName == "DRIVER_ID")
                            {
                                string idOutLinkDrv = reader.Value;
                                var driver = new Driver(0, 0, idOutLinkDrv);
                                if (driver.Id > 0) taskItem.DriverId = driver.Id;
                            }
                            else if (lastNodeName == "TRAILER")
                            {
                                remark = string.Format("{0} прицеп:{1}", remark, reader.Value);
                            }
                            else if (lastNodeName == "WORK_TYPE")
                            {
                                remark = string.Format("{0}  вид работ:{1}", remark, reader.Value);
                            }
                            else if (lastNodeName == "DISPATCHER")
                            {
                                remark = string.Format("{0}  диспетчер:{1}", remark, reader.Value);
                            }
                            else if (lastNodeName == "TONNAGE")
                            {
                                remark = string.Format("{0}  тоннаж:{1}", remark, reader.Value);
                            }
                            else if (lastNodeName == "CITY_ID")
                            {
                                var sample = new DictionaryRoute();
                                int sampleId = sample.GetIdFromOutLinkId(reader.Value);
                                if (sampleId >0)
                                    taskItem.SampleId = sampleId;
                            }break;
                      case XmlNodeType.EndElement:
                          if (reader.Name == "CARD")
                            {
                                if (taskItem.MobitelId > 0 && taskItem.SampleId > 0)
                                {
                                    taskItem.Remark = remark;
                                    taskItem.AddDoc();
                                    taskItem.ContentCreateFromSample();
                                    TasksImported++;
                                }
                            }
                            break;
                    }
                }
                reader.Close ();
                MessageBox.Show(string.Format("Импортировано {0} маршрутов" ,TasksImported));  
            } else {
                MessageBox.Show ("File not found!");
            }
        }

    }
}
