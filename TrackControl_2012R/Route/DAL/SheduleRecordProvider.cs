﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using Agro;
using Route.Documents.Shedule;
using TrackControl.General.DatabaseDriver;
using TrackControl.MySqlDal;
using TrackControl.Vehicles;

namespace Route.DAL
{
    public static class SheduleRecordProvider
    {
        public static bool Save(SheduleRecord sheduleRecord)
        {
            using (var db = new DriverDb())
            {

                db.ConnectDb();
                if (sheduleRecord.IsNew)
                {
                    db.NewSqlParameterArray(10);
                    db.SetNewSqlParameter(db.ParamPrefics + "Id_main", sheduleRecord.Shedule.Id);
                    GetParams(sheduleRecord, db);
                    sheduleRecord.Id = db.ExecuteReturnLastInsert(string.Format(RouteQuery.SheduleRecordProvider.Insert, db.ParamPrefics), db.GetSqlParameterArray, "rt_shedulet");
                }
                else
                {
                    db.NewSqlParameterArray(10);
                    db.SetNewSqlParameter(db.ParamPrefics + "Id", sheduleRecord.Id);
                    GetParams(sheduleRecord, db);
                    db.ExecuteNonQueryCommand(string.Format(RouteQuery.SheduleRecordProvider.UpdateOne, db.ParamPrefics), db.GetSqlParameterArray);
                }
                return true;
            }
            }

        private static void GetParams(SheduleRecord sheduleRecord, DriverDb db)
        {

            db.SetNewSqlParameter(db.ParamPrefics + "Id_zone_to", sheduleRecord.ZoneTo.Id);
            db.SetNewSqlParameter(db.ParamPrefics + "Id_vehicle", sheduleRecord.Vehicle==null ? 0: sheduleRecord.Vehicle.Id);
            db.SetNewSqlParameter(db.ParamPrefics + "TimePlanStart", sheduleRecord.TimePlanStart);
            db.SetNewSqlParameter(db.ParamPrefics + "TimePlanEnd", sheduleRecord.TimePlanEnd);
            db.SetNewSqlParameter(db.ParamPrefics + "TimeFactStart", sheduleRecord.TimeFactStart);
            db.SetNewSqlParameter(db.ParamPrefics + "TimeFactEnd", sheduleRecord.TimeFactEnd);
            db.SetNewSqlParameter(db.ParamPrefics + "Distance", sheduleRecord.Distance);
            db.SetNewSqlParameter(db.ParamPrefics + "State", sheduleRecord.State);
            db.SetNewSqlParameter(db.ParamPrefics + "IsClose", sheduleRecord.IsClose);


        }

        public static SheduleRecord GetOne(Shedule shedule ,int idSheduleRecord)
        {
            var sheduleRecord = new SheduleRecord();
            using (var db = new DriverDb())
            {db.ConnectDb();
                db.GetDataReader(string.Format(RouteQuery.SheduleRecordProvider.SelectOne, idSheduleRecord));
                if (db.Read())
                {
                    sheduleRecord.Id = idSheduleRecord;
                    sheduleRecord.Shedule = shedule;
                    sheduleRecord.ZoneTo = DocItem.ZonesModel.GetZoneById(db.GetInt32("Id_zone_to"));
                    sheduleRecord.Vehicle = DocItem.VehiclesModel.GetVehicleById(db.GetInt32("Id_vehicle"));
                    sheduleRecord.TimePlanStart = db.GetDateTime("TimePlanStart");
                    sheduleRecord.TimePlanEnd = db.GetDateTime("TimePlanEnd");
                    if (sheduleRecord.TimePlanEnd == DateTime.MinValue) sheduleRecord.TimePlanEnd = null;
                    if (!db.IsDbNull(db.GetOrdinal("TimeFactStart")))
                    {
                        sheduleRecord.TimeFactStart = db.GetDateTime("TimeFactStart");
                        if (sheduleRecord.TimeFactStart == DateTime.MinValue) sheduleRecord.TimeFactStart = null;
                    }
                    if (!db.IsDbNull(db.GetOrdinal("TimeFactEnd")))
                    {
                        sheduleRecord.TimeFactEnd = db.GetDateTime("TimeFactEnd");
                        if (sheduleRecord.TimeFactEnd == DateTime.MinValue) sheduleRecord.TimeFactEnd = null;
                    } 
                    sheduleRecord.Distance =(float)db.GetDouble("Distance");
                    if (!db.IsDbNull(db.GetOrdinal("State")))
                    {
                        sheduleRecord.State = db.GetString("State");
                    }
                    sheduleRecord.IsClose = db.GetBit("IsClose");
                }
            }
            return sheduleRecord;
        }

        public static IList<SheduleRecord> GetList (Shedule shedule)
        {
            var sheduleRecords = new BindingList<SheduleRecord>();
            using (var db = new DriverDb())
            {
                db.ConnectDb();
                db.GetDataReader(string.Format(RouteQuery.SheduleRecordProvider.SelectShedule, shedule.Id));
                while (db.Read())
                {
                    SheduleRecord sheduleRecord = GetOne(shedule, db.GetInt32("Id"));
                    sheduleRecords.Add(sheduleRecord);
                }
            }
            return sheduleRecords;
        }


        public static bool DeleteOne(SheduleRecord sheduleRecord)
        {
            using (var db = new DriverDb())
            {
                db.ConnectDb();
                db.ExecuteNonQueryCommand(string.Format(RouteQuery.SheduleRecordProvider.DeleteOne, sheduleRecord.Id));
            }
            return true;
        }
    }
}
