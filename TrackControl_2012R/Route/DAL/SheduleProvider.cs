﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Route.Documents.Shedule;
using TrackControl.General.DAL;
using TrackControl.General.DatabaseDriver;
using TrackControl.MySqlDal; 

namespace Route.DAL
{
    public static class SheduleProvider
    {
        public static bool  Save (Shedule shedule)
        {
           using (var db = new DriverDb())
           {
              
                db.ConnectDb();
                if (shedule.IsNew)
                {
                    db.NewSqlParameterArray(5);
                    GetParams(shedule, db);
                    shedule.Id = db.ExecuteReturnLastInsert(string.Format(RouteQuery.SheduleProvider.Insert, db.ParamPrefics), db.GetSqlParameterArray, "rt_shedule");
                }
                else
                {
                    db.NewSqlParameterArray(6);
                    db.SetNewSqlParameter(db.ParamPrefics + "Id", shedule.Id);
                    GetParams(shedule, db);
                    db.ExecuteNonQueryCommand(string.Format(RouteQuery.SheduleProvider.UpdateOne,db.ParamPrefics) , db.GetSqlParameterArray);
                }
                return true;
           }
        }

        private static void GetParams(Shedule shedule, DriverDb db)
        {
            db.SetNewSqlParameter(db.ParamPrefics + "Id_shedule_dc", shedule.DictionaryShedule == null ? 0 : shedule.DictionaryShedule.Id);
            db.SetNewSqlParameter(db.ParamPrefics + "Id_zone_main", shedule.ZoneMain == null ? 0 : shedule.ZoneMain.Id);
            db.SetNewSqlParameter(db.ParamPrefics + "DateShedule", shedule.DateShedule);
            db.SetNewSqlParameter(db.ParamPrefics + "Remark", shedule.Remark ??  "");
            db.SetNewSqlParameter(db.ParamPrefics + "OutLinkId", shedule.OutLinkId ?? "");
        }

        public static Shedule GetOne(int idShedule)
        {
            var shedule = new Shedule();
            using (var db = new DriverDb())
            {
                db.ConnectDb();
                db.GetDataReader(string.Format(RouteQuery.SheduleProvider.SelectOne,idShedule));
                if (db.Read())
                {
                    shedule.Id = idShedule;
                    shedule.DateShedule = db.GetDateTime("DateShedule");
                    shedule.ZoneMain = new ZonesProvider().GetZone(db.GetInt32("Id_zone_main"));
                    shedule.DictionaryShedule = DictionarySheduleProvider.GetOne(db.GetInt32("Id_shedule_dc"));
                    shedule.Remark = db.GetString("Remark");
                    shedule.OutLinkId = db.GetString("OutLinkId");
                    shedule.SheduleRecords = SheduleRecordProvider.GetList(shedule);
                    
                }
            }
            return shedule;
        }

        public static Shedule GetOneToDate(DateTime dateShedule)
        {
            var shedule = new Shedule();
            using (var db = new DriverDb())
            {
                db.ConnectDb();
                db.NewSqlParameterArray(1);
                db.SetNewSqlParameter(db.ParamPrefics + "DateShedule", dateShedule);
                db.GetDataReader(string.Format(RouteQuery.SheduleProvider.SelectOneToDate, db.ParamPrefics), db.GetSqlParameterArray);
                if (db.Read())
                {
                    shedule = GetOne(db.GetInt32("Id"));
                }
            }
            return shedule;
        }

        public static bool DeleteOne(Shedule shedule)
        {
            using (var db = new DriverDb())
            {
                db.ConnectDb();
                db.ExecuteNonQueryCommand(string.Format(RouteQuery.SheduleProvider.DeleteOne, shedule.Id));
            }
            return true;
        }
    }
}
