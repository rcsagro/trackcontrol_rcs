﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using TrackControl.General.DatabaseDriver;

namespace Route
{
    public class RouteQuery
    {
        public class  TaskItem
        {
            public static string UpdateTaskHeader
            {
                get
                {
                    return
                        @"UPDATE rt_route SET TimeStartPlan = {0}TimeStart,
                        TimeEndFact = {0}TimeEndFact, Id_sample = {1}, Type = {2},
                        Id_mobitel = {3},Id_driver = {4}, Remark = {0}Remark, OutLinkId = {0}OutLinkId  WHERE (ID = {5})";
                }
            }
        }


        public class SheduleProvider
        {
            public static string UpdateOne
            {
                get
                {
                    return @"UPDATE rt_shedule SET Id_shedule_dc = {0}Id_shedule_dc,Id_zone_main = {0}Id_zone_main,
                           DateShedule = {0}DateShedule, Remark ={0}Remark, OutLinkId = {0}OutLinkId
                           WHERE(Id = {0}Id)";
                }
            }

            public static string Insert
            {
                get
                {
                    return @"INSERT INTO rt_shedule
                         (Id_shedule_dc,Id_zone_main, DateShedule, Remark, OutLinkId)
                        VALUES ({0}Id_zone_main,{0}Id_shedule_dc, {0}DateShedule, {0}Remark, {0}OutLinkId)";
                }
            }

            public static string SelectOne
            {
                get
                {
                    return @"SELECT * FROM  rt_shedule  WHERE (Id = {0})";
                }
            }

            public static string SelectOneToDate
            {
                get
                {
                    return @"SELECT * FROM  rt_shedule  WHERE (DateShedule = {0}DateShedule)";
                }
            }

            public static string DeleteOne
            {
                get
                {
                    return @"DELETE FROM rt_shedule WHERE (Id = {0})";
                }
            }
        }

        public class SheduleRecordProvider
        {
            public static string UpdateOne
            {
                get
                {
                    return @"UPDATE rt_shedulet SET Id_zone_to = {0}Id_zone_to,
                           Id_vehicle = {0}Id_vehicle, TimePlanStart ={0}TimePlanStart, TimePlanEnd = {0}TimePlanEnd,
                           TimeFactStart = {0}TimeFactStart, TimeFactEnd ={0}TimeFactEnd,Distance = {0}Distance, State = {0}State,
                           IsClose = {0}IsClose
                           WHERE(Id = {0}Id)";
                }
            }

            public static string Insert
            {
                get
                {
                    return @"INSERT INTO rt_shedulet
                         (Id_main, Id_zone_to, Id_vehicle, TimePlanStart, TimePlanEnd, TimeFactStart, TimeFactEnd,Distance, State, IsClose)
                        VALUES ({0}Id_main, {0}Id_zone_to, {0}Id_vehicle, {0}TimePlanStart, 
                        {0}TimePlanEnd, {0}TimeFactStart, {0}TimeFactEnd, {0}Distance, {0}State, {0}IsClose)";
                }
            }

            public static string SelectOne
            {
                get { return @"SELECT * FROM  rt_shedulet  WHERE (Id = {0})"; }
            }

            public static string SelectShedule
            {
                get { return @"SELECT * FROM  rt_shedulet  WHERE (Id_main = {0})"; }
            }

            public static string DeleteOne
            {
                get { return @"DELETE FROM rt_shedulet WHERE (Id = {0})";}
            }
        }

        public class DictionarySheduleProvider
        {
            public static string UpdateOne
            {
                get
                {
                    return @"UPDATE rt_shedule_dc SET Name ={0}Name, Id_zone_main = {0}Id_zone_main,
                           Remark ={0}Remark
                           WHERE(Id = {0}Id)";
                }
            }

            public static string Insert
            {
                get
                {
                    return @"INSERT INTO rt_shedule_dc
                         (Id_zone_main, Name, Remark)
                        VALUES ({0}Id_zone_main, {0}Name, {0}Remark)";
                }
            }

            public static string SelectOne
            {
                get
                {
                    return @"SELECT * FROM  rt_shedule_dc  WHERE (Id = {0})";
                }
            }



            public static string DeleteOne
            {
                get
                {
                    return @"DELETE FROM rt_shedule_dc WHERE (Id = {0})";
                }
            }

            public static string GetAllWithZones
            {
                get
                {
                    return @"SELECT  rt_shedule_dc.Id, rt_shedule_dc.Name, zones.Name AS ZoneName, rt_shedule_dc.Remark
                    FROM   rt_shedule_dc LEFT OUTER JOIN
                    zones ON rt_shedule_dc.Id_zone_main = zones.Zone_ID";
                }

            }

            public static string GetAll
            {
                get
                {
                    return @"SELECT *  FROM rt_shedule_dc";
                }

            }
        }

        public class DictionarySheduleRecordProvider
        {
            public static string UpdateOne
            {
                get
                {
                    return @"UPDATE rt_shedulet_dc SET Id_zone_to = {0}Id_zone_to,
                           TimePlanStart ={0}TimePlanStart, TimePlanEnd = {0}TimePlanEnd,
                           Distance = {0}Distance
                           WHERE(Id = {0}Id)";
                }
            }

            public static string Insert
            {
                get
                {
                    return @"INSERT INTO rt_shedulet_dc
                         (Id_main, Id_zone_to,  TimePlanStart, TimePlanEnd, Distance)
                        VALUES ({0}Id_main, {0}Id_zone_to,  {0}TimePlanStart, 
                        {0}TimePlanEnd, {0}Distance)";
                }
            }

            public static string SelectOne
            {
                get { return @"SELECT * FROM  rt_shedulet_dc  WHERE (Id = {0})"; }
            }

            public static string SelectShedule
            {
                get { return @"SELECT * FROM  rt_shedulet_dc  WHERE (Id_main = {0})"; }
            }

            public static string DeleteOne
            {
                get { return @"DELETE FROM rt_shedulet_dc WHERE (Id = {0})"; }
            }
        }
    }
}
