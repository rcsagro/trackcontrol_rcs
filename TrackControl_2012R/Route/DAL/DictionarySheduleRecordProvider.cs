﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using Agro;
using Route.Dictionaries;
using Route.Documents.Shedule;
using TrackControl.General.DatabaseDriver;
using TrackControl.MySqlDal;
using TrackControl.Vehicles;

namespace Route.DAL
{
    public static class DictionaryDictionarySheduleRecordProvider
    {
        public static bool Save(DictionarySheduleRecord dictSheduleRecord)
        {
            using (var db = new DriverDb())
            {

                db.ConnectDb();
                if (dictSheduleRecord.IsNew)
                {
                    db.NewSqlParameterArray(5);
                    db.SetNewSqlParameter(db.ParamPrefics + "Id_main", dictSheduleRecord.DictionaryShedule.Id);
                    GetParams(dictSheduleRecord, db);
                    dictSheduleRecord.Id = db.ExecuteReturnLastInsert(string.Format(RouteQuery.DictionarySheduleRecordProvider.Insert, db.ParamPrefics), db.GetSqlParameterArray, "rt_shedulet_dc");
                }
                else
                {
                    db.NewSqlParameterArray(5);
                    db.SetNewSqlParameter(db.ParamPrefics + "Id", dictSheduleRecord.Id);
                    GetParams(dictSheduleRecord, db);
                    db.ExecuteNonQueryCommand(string.Format(RouteQuery.DictionarySheduleRecordProvider.UpdateOne, db.ParamPrefics), db.GetSqlParameterArray);
                }
                return true;
            }
            }

        private static void GetParams(DictionarySheduleRecord dictSheduleRecord, DriverDb db)
        {

            db.SetNewSqlParameter(db.ParamPrefics + "Id_zone_to", dictSheduleRecord.ZoneTo.Id);
            db.SetNewSqlParameter(db.ParamPrefics + "TimePlanStart", dictSheduleRecord.TimePlanStart);
            db.SetNewSqlParameter(db.ParamPrefics + "TimePlanEnd", dictSheduleRecord.TimePlanEnd);
            db.SetNewSqlParameter(db.ParamPrefics + "Distance", dictSheduleRecord.Distance);
        }

        public static DictionarySheduleRecord GetOne(DictionaryShedule dictShedule, int idDictSheduleRecord)
        {
            var dictSheduleRecord = new DictionarySheduleRecord();
            using (var db = new DriverDb())
            {db.ConnectDb();
            db.GetDataReader(string.Format(RouteQuery.DictionarySheduleRecordProvider.SelectOne, idDictSheduleRecord));
                if (db.Read())
                {
                    dictSheduleRecord.Id = idDictSheduleRecord;
                    dictSheduleRecord.DictionaryShedule = dictShedule;
                    dictSheduleRecord.ZoneTo = DocItem.ZonesModel.GetZoneById(db.GetInt32("Id_zone_to"));
                    dictSheduleRecord.TimePlanStart = db.GetString("TimePlanStart");
                    dictSheduleRecord.TimePlanEnd = db.GetString("TimePlanEnd");
                    dictSheduleRecord.Distance = (float)db.GetDouble("Distance");
                }
            }
            return dictSheduleRecord;
        }

        public static IList<DictionarySheduleRecord> GetList(DictionaryShedule dictShedule)
        {
            var dictSheduleRecords = new BindingList<DictionarySheduleRecord>();
            using (var db = new DriverDb())
            {
                db.ConnectDb();
                db.GetDataReader(string.Format(RouteQuery.DictionarySheduleRecordProvider.SelectShedule, dictShedule.Id));
                while (db.Read())
                {
                    DictionarySheduleRecord dictSheduleRecord = GetOne(dictShedule, db.GetInt32("Id"));
                    dictSheduleRecords.Add(dictSheduleRecord);
                }
            }
            return dictSheduleRecords;
        }


        public static bool DeleteOne(DictionarySheduleRecord dictSheduleRecord)
        {
            using (var db = new DriverDb())
            {
                db.ConnectDb();
                db.ExecuteNonQueryCommand(string.Format(RouteQuery.DictionarySheduleRecordProvider.DeleteOne, dictSheduleRecord.Id));
            }
            return true;
        }
    }
}
