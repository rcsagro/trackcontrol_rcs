﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using Route.Dictionaries;
using Route.Documents.Shedule;
using TrackControl.General.DAL;
using TrackControl.General.DatabaseDriver;
using TrackControl.MySqlDal; 

namespace Route.DAL
{
    public static class DictionarySheduleProvider
    {
        public static int Save(DictionaryShedule dictionaryShedule)
        {
           using (var db = new DriverDb())
           {
              
                db.ConnectDb();
                if (dictionaryShedule.IsNew)
                {
                    db.NewSqlParameterArray(3);
                    GetParams(dictionaryShedule, db);
                    dictionaryShedule.Id = db.ExecuteReturnLastInsert(string.Format(RouteQuery.DictionarySheduleProvider.Insert, db.ParamPrefics), db.GetSqlParameterArray, "rt_shedule");
                }
                else
                {
                    db.NewSqlParameterArray(4);
                    db.SetNewSqlParameter(db.ParamPrefics + "Id", dictionaryShedule.Id);
                    GetParams(dictionaryShedule, db);
                    db.ExecuteNonQueryCommand(string.Format(RouteQuery.DictionarySheduleProvider.UpdateOne, db.ParamPrefics), db.GetSqlParameterArray);
                }
                return dictionaryShedule.Id;
           }
        }

        private static void GetParams(DictionaryShedule shedule, DriverDb db)
        {
            db.SetNewSqlParameter(db.ParamPrefics + "Id_zone_main", shedule.ZoneMain == null ? 0 : shedule.ZoneMain.Id);
            db.SetNewSqlParameter(db.ParamPrefics + "Name", shedule.Name);
            db.SetNewSqlParameter(db.ParamPrefics + "Remark", shedule.Comment ?? "");
        }

        public static DictionaryShedule GetOne(int idDictionaryShedule)
        {
            var dictShedule = new DictionaryShedule();
            using (var db = new DriverDb())
            {
                db.ConnectDb();
                db.GetDataReader(string.Format(RouteQuery.DictionarySheduleProvider.SelectOne, idDictionaryShedule));
                if (db.Read())
                {
                    dictShedule.Id = idDictionaryShedule;
                    dictShedule.ZoneMain = new ZonesProvider().GetZone(db.GetInt32("Id_zone_main"));
                    dictShedule.Comment = db.GetString("Remark");
                    dictShedule.Name = db.GetString("Name");
                    dictShedule.DictionarySheduleRecords = DictionaryDictionarySheduleRecordProvider.GetList(dictShedule);
                }
            }
            return dictShedule;
        }


        public static bool DeleteOne(DictionaryShedule dictShedule)
        {
            using (var db = new DriverDb())
            {
                db.ConnectDb();
                db.ExecuteNonQueryCommand(string.Format(RouteQuery.DictionarySheduleProvider.DeleteOne, dictShedule.Id));
            }
            return true;
        }

        public static DataTable GetAllDataTable()
        {
           var dt = new DataTable();
           using (var db = new DriverDb())
           {
               
            db.ConnectDb();
            dt = db.GetDataTable(RouteQuery.DictionarySheduleProvider.GetAllWithZones);
            db.CloseDbConnection();
           }
            return dt;
        }

        public static IList<DictionaryShedule> GetAll()
        {
            var shedules = new BindingList<DictionaryShedule>();
            using (var db = new DriverDb())
            {
                db.ConnectDb();
                db.GetDataReader(RouteQuery.DictionarySheduleProvider.GetAll);
                while (db.Read())
                {
                    DictionaryShedule shedule = GetOne(db.GetInt32("Id"));
                    shedules.Add(shedule);
                }
            }
            return shedules;
        }
    }
}
