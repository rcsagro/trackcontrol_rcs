using System;
using System.Xml;
using System.Text;
using System.Collections.Generic;
using TrackControl.Hasp.Properties;
using Aladdin.HASP;
using System.Windows.Forms;


namespace TrackControl.Hasp
{
    public static class HaspService
    {
        private const string HASP_ID_NODE = "hasp_info/keyspec/hasp/haspid";

        private static HaspStatus _status;
        private static List<UInt32> _mapCodes;

        public static HaspState GetHaspNumberState(int L_NUMBER)
        {
            using (Aladdin.HASP.Hasp hasp = new Aladdin.HASP.Hasp(HaspFeature.FromProgNum(L_NUMBER)))
            {
                _status = hasp.Login(ASCIIEncoding.Default.GetBytes(Resources.HASP));
                return State;
            }
        }

        static HaspService()
        {
            using (Aladdin.HASP.Hasp hasp = new Aladdin.HASP.Hasp(HaspFeature.FromProgNum(GlobalsHasp.LICENSE_NUMBER)))
            {
                _status = hasp.Login(ASCIIEncoding.Default.GetBytes(Resources.HASP));
                _mapCodes = getMapCodes(hasp);
            }
        }

        public static HaspState State
        {
            get
            {
                if (HaspStatus.StatusOk == _status)
                    return HaspState.OK;

                if (HaspStatus.FeatureNotFound == _status)
                    return HaspState.LicenseNotFound;

                return HaspState.HaspNotFound;
            }
        }

        /// <summary>
        /// ��������� ����� ��� ���� EPRASYS (GIS-���������)
        /// </summary>
        public static List<UInt32> MapCodes
        {
            get { return _mapCodes; }
        }

        #region --   �������   --

        /// <summary>
        /// ���������� ���������� � ������
        /// </summary>
    private static string getSessionInfo(Aladdin.HASP.Hasp hasp, string nodename)
        {
            // firstly we will retrieve the key info.
            string info = null;
			HaspStatus status = hasp.GetSessionInfo(Aladdin.HASP.Hasp.KeyInfo, ref info);
            XmlDocument xmlInfo = new XmlDocument();
            xmlInfo.LoadXml(info);
            XmlNode node = xmlInfo.SelectSingleNode(nodename);
            return node.InnerText;
        }

        /// <summary>
        /// ���������� ��������� ������ ��� ����
        /// </summary>
        private static List<UInt32> getMapCodes(Aladdin.HASP.Hasp hasp)
        {
            List<UInt32> result = new List<uint>();
            try
            {

                if ((null != hasp) && hasp.IsLoggedIn())
                {
                    int id = Convert.ToInt32(getSessionInfo(hasp, HASP_ID_NODE));
                    HaspFile file = hasp.GetFile(HaspFiles.Main);
                    if (file.IsLoggedIn())
                    {
                        int size = 0;
                        file.FileSize(ref size);
                        byte[] bytes = new byte[size];
                        file.Read(bytes, 0, bytes.Length);

                        int firstBlock = bytes[0];
                        int secondBlock = bytes[firstBlock + 1];

                        for (int i = 0; i < secondBlock/4; i++)
                        {
                            uint code = bytes[firstBlock + i*4 + 2];
                            code += (uint) bytes[firstBlock + i*4 + 3] << 8;
                            code += (uint) bytes[firstBlock + i*4 + 4] << 16;
                            result.Add(Convert.ToUInt32(code ^ id));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "HASP");
            }
            return result;
        }

        #endregion
    }
}
