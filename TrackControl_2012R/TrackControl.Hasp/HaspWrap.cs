﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows.Forms;
using System.Threading;
using System.Linq;
using TrackControl.General.Log;

namespace TrackControl.Hasp
{
    public static class HaspWrap
    {
        static HaspStatus _status;
        static HaspState _licState;
        static List<UInt32> _mapCodes;
        static int _numOutputLines;
        static int _mapCodesCounter;


        public static List<UInt32> MapCodes
        {
            get {
                if (_mapCodes == null)
                {
                    GetMapCodes();

                }
                return _mapCodes; 
                }
        }

        public static HaspState State
        {
            get
            {
                return GetHaspStateFromStatus(_status);
            }
        }

        public static HaspState GetHaspNumberState(int L_NUMBER)
        {
            GetLicNumber(L_NUMBER);
            if (_mapCodes == null) GetMapCodes();
            return _licState;
        }

        static void GetMapCodes()
        {
            try
            {
                _numOutputLines = 0;
                _mapCodesCounter = 0;
                _mapCodes = new List<UInt32>();
                Process p = new Process();
                p.StartInfo.FileName = string.Format("{0}\\{1}", Application.StartupPath, "WrapDriver.exe");
                p.StartInfo.Arguments = "MapCodes";
                p.StartInfo.UseShellExecute = false;
                p.StartInfo.RedirectStandardOutput = true;
                p.OutputDataReceived += new DataReceivedEventHandler(GetHaspValues);
                p.Start();
                p.BeginOutputReadLine();
                p.WaitForExit();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "GetMapCodes");
            }
        }

        private static void GetHaspValues(object sendingProcess,DataReceivedEventArgs outLine)
        {
            try
            {

                if (!String.IsNullOrEmpty(outLine.Data))
                {
                    if (outLine.Data == "End")
                    {
                        ((Process)sendingProcess).Dispose();
                    }
                    else
                    {
                        _numOutputLines++;
                        switch (_numOutputLines)
                        {
                            case 1:
                                {
                                    int code = 0;
                                    if (Int32.TryParse(outLine.Data, out code))
                                    {
                                        _status = (HaspStatus)code;
                                    }
                                    break;
                                }
                            case 2:
                                {
                                    Int32.TryParse(outLine.Data, out _mapCodesCounter);
                                    break;
                                }

                            default:
                                {
                                    if (_mapCodesCounter > 0)
                                    {
                                        UInt32 mapCode = 0;
                                        if (UInt32.TryParse(outLine.Data, out mapCode))
                                        {
                                            _mapCodes.Add(mapCode);
                                            _mapCodesCounter--;
                                        }
                                        //if (_mapCodesCounter == 0)
                                        //{
                                        //    ((Process)sendingProcess).Kill();
                                        //    ExecuteLogging.Log("App", "Process.Killed");
                                        //}
                                    }
                                }
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }


        }

        private static HaspState GetHaspStateFromStatus(HaspStatus hState)
        {
            if (HaspStatus.StatusOk == hState)
                return HaspState.OK;

            if (HaspStatus.FeatureNotFound == hState)
                return HaspState.LicenseNotFound;

            return HaspState.HaspNotFound;
        }

        private static HaspStatus GetHaspStatusFromState(HaspState hState)
        {
            if (HaspState.OK == hState)
                return HaspStatus.StatusOk;

            if (HaspState.LicenseNotFound == hState)
                return HaspStatus.FeatureNotFound;

            return HaspStatus.AccessDenied;
        }

        static void GetLicNumber(int L_NUMBER)
        {
            try
            {
                Process p = new Process();
                p.StartInfo.FileName = string.Format("{0}\\{1}", Application.StartupPath, "WrapDriver.exe");
                p.StartInfo.Arguments = string.Format("Lic {0}", L_NUMBER);
                p.StartInfo.UseShellExecute = false;
                p.StartInfo.RedirectStandardOutput = true;
                p.OutputDataReceived += new DataReceivedEventHandler(GetHaspLicNumber);
                p.Start();
                //Console.WriteLine(p.StartTime.ToString()); 
                //var processes = Process.GetProcesses();

                //procesQuery.ForEach(pr => pr.Kill());
                p.BeginOutputReadLine();
                p.WaitForExit();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, string.Format("GetLicNumber {0}",L_NUMBER));
            }
        }

        private static void GetHaspLicNumber(object sendingProcess, DataReceivedEventArgs outLine)
        {
            try
            {
                if (!String.IsNullOrEmpty(outLine.Data))
                {
                    if (outLine.Data == "End")
                    {
                        ((Process)sendingProcess).Dispose();
                    }
                    else
                    {
                        int code = 0;
                        if (Int32.TryParse(outLine.Data, out code))
                        {
                            _licState = (HaspState)code;
                            _status = GetHaspStatusFromState(_licState);
                            //((Process)sendingProcess).Kill();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "GetHaspLicNumber");
            }


        }

        private enum HaspStatus
        {
            StatusOk = 0,
            InvalidAddress = 1,
            InvalidFeature = 2,
            NotEnoughMemory = 3,
            TooManyOpenFeatures = 4,
            AccessDenied = 5,
            IncompatibleFeature = 6,
            ContainerNotFound = 7,
            BufferTooShort = 8,
            InvalidHandle = 9,
            InvalidFile = 10,
            DriverTooOld = 11,
            NoTime = 12,
            SystemError = 13,
            DriverNotFound = 14,
            InvalidFormat = 15,
            RequestNotSupported = 16,
            InvalidUpdateObject = 17,
            KeyIdNotFound = 18,
            InvalidUpdateData = 19,
            UpdateNotSupported = 20,
            InvalidUpdateCounter = 21,
            InvalidVendorCode = 22,
            EncryptionNotSupported = 23,
            InvalidTime = 24,
            NoBatteryPower = 25,
            UpdateNoACK = 26,
            TerminalServiceDetected = 27,
            FeatureNotImplemented = 28,
            UnknownAlgorithm = 29,
            InvalidSignature = 30,
            FeatureNotFound = 31,
            NoLog = 32,
            InvalidObject = 500,
            InvalidParameter = 501,
            AlreadyLoggedIn = 502,
            AlreadyLoggedOut = 503,
            OperationFailed = 525,
            NoExtensionBlock = 600,
            InvalidPortType = 650,
            InvalidPort = 651,
            NotImplemented = 698,
            InternalError = 699,
        }
    }
}
