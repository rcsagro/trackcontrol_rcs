using BaseReports.CrossingCZ;
using BaseReports.Procedure;
using ReportsOnCheckZones.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using TrackControl.Vehicles;

namespace ReportsOnCheckZones
{
    /// <summary>
    /// ������� ����� ���������� ��� ������� ��
    /// </summary>
    public class BaseCZAlgorithm : PartialAlgorithms
    {
        public class ZonePoint
        {
            public List<CrossingInfo> firstPoints;
            public List<CrossingInfo> lastPoints;
            public int mobid;
            public int numPair;

            public ZonePoint()
            {
                mobid = -1;
                numPair = 0;
            }
        }

        protected List<ZonePoint> zonePointVehicle;

        /// <summary>
        /// ������� CheckZonesDataSet
        /// </summary>
        private CheckZonesDataSet czDataSet;

        /// <summary>
        /// ������� CheckZonesDataSet
        /// </summary>
        protected CheckZonesDataSet CzDataSet
        {
            get { return czDataSet; }
        }

        public BaseCZAlgorithm()
        {
            czDataSet = CheckZonesDataSetCreator.GetDataSet();
            zonePointVehicle = new List<ZonePoint>();
        }

        /// <summary>
        /// ������� �������� ��� ��������-����.
        /// </summary>
        private int progressCounter;

        /// <summary>
        /// ������ ��� �������
        /// </summary>
        public override void Run()
        {
            if (GpsDatas.Length == 0)
                return;

            try
            {
                BeforeReportFilling(GetReportName(), CrossZones.Instance.GetCrossingCount(m_row.Mobitel_ID));
                progressCounter = 0;
                FillReport(CrossZones.Instance.GetCrossInfoCollection(m_row.Mobitel_ID).GetEnumerator());
                CalcWayInZones(CrossZones.Instance.GetCrossInfoCollection(m_row.Mobitel_ID).GetEnumerator(), m_row.Mobitel_ID);
            }
            finally
            {
                AfterReportFilling();
                Algorithm.OnAction(this, null);
            }
        }

        /// <summary>
        /// ���������� �������� ��������-����.
        /// </summary>
        protected void ProgressIncrement()
        {
            ReportProgressChanged(++progressCounter);
        }

        /// <summary>
        /// ������������ ������
        /// </summary>
        /// <returns>������������ ������</returns>
        protected virtual string GetReportName()
        {
            throw new NotImplementedException(Resources.CzGetReportNameException);
        }

        //public virtual void CalcWayInZones(IEnumerator<CrossingInfo> enumerator, int mobid)
        //{
        //    throw new NotImplementedException(Resources.CzFillReportException);
        //}

        /// <summary>
        /// ���������� �������-������
        /// </summary>
        /// <param name="enumerator">�������� ��������� CrossingInfo.</param>
        protected virtual void FillReport(IEnumerator<CrossingInfo> enumerator)
        {
            throw new NotImplementedException(Resources.CzFillReportException);
        }

        private const string ZONE_DESCR_FORMAT = "{0} ({1})";

        /// <summary>
        /// ������ ������� ��������(������������) ��������� ����� ������������ ����
        /// </summary>
        /// <param name="zoneName">������������ ����</param>
        /// <param name="pointLocationType">��������� ����� ������������ ����</param>
        /// <param name="dataGpsID">������������� ������ dataview.</param>
        /// <returns>��������</returns>
        protected string BuildZoneDescr(string zoneName, PointLocationType pointLocationType, System.Int64 dataGpsID)
        {
            try
            {
                switch (pointLocationType)
                {
                    case PointLocationType.CrossIn:
                    case PointLocationType.CrossOut:
                    case PointLocationType.BeginIn:
                    case PointLocationType.EndIn:

                        return String.Format(ZONE_DESCR_FORMAT, zoneName,
                            CrossZonesAlgorithm.GetCrossTypeAsString(pointLocationType));

                    case PointLocationType.BeginOut:
                    case PointLocationType.EndOut:

                        GpsData gpsData = GpsDatas.Where(gps => gps.Id == dataGpsID).FirstOrDefault();
                        //atlantaDataSet.dataviewRow dataViewRow = atlantaDataSet.dataview.FindByDataGps_ID(dataGpsID);
                        //string location = FindLocation(new PointLatLng(dataViewRow.Lat, dataViewRow.Lon));
                        string location = FindLocation(gpsData.LatLng);
                        if (location.Length == 0)
                        {
                            location = String.Format("{0}; {1}", gpsData.LatLng.Lat, gpsData.LatLng.Lng);
                        }

                        return String.Format(ZONE_DESCR_FORMAT, location,
                            CrossZonesAlgorithm.GetCrossTypeAsString(pointLocationType));

                    default:
                        return "";
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show("Error BaseCZAlgorithm", ex.Message + "\n" + ex.StackTrace, MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                return ""; 
            }
        }

        public virtual void CalcWayInZones(IEnumerator<CrossingInfo> enumerator, int mobid)
        {
            CrossingInfo next, current;
            List<CrossingInfo> first = new List<CrossingInfo>();
            List<CrossingInfo> last = new List<CrossingInfo>();

            if (!enumerator.MoveNext())
                return;

            current = enumerator.Current;

            while (enumerator.MoveNext())
            {
                next = enumerator.Current;

                if ((current.PointLocationType == PointLocationType.CrossOut) &&
                    (next.PointLocationType == PointLocationType.EndOut))
                {
                    current = next;
                    continue;
                }

                if ((current.PointLocationType == PointLocationType.CrossOut) &&
                    (next.PointLocationType == PointLocationType.CrossIn))
                {
                    current = next;
                    continue;
                }

                if ((current.PointLocationType == PointLocationType.BeginOut) &&
                    (next.PointLocationType == PointLocationType.CrossIn))
                {
                    current = next;
                    continue;
                }

                if ((current.PointLocationType == PointLocationType.BeginOut) &&
                    (next.PointLocationType == PointLocationType.EndOut))
                {
                    current = next;
                    continue;
                }

                first.Add(current);
                last.Add(next);
                current = next;
            }

            ZonePoint znPoint = new ZonePoint();
            znPoint.firstPoints = first;
            znPoint.lastPoints = last;
            znPoint.mobid = mobid;

            if (first.Count == last.Count)
            {
                znPoint.numPair = first.Count;
                zonePointVehicle.Add(znPoint);
            }
        }
    }
}
