﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Text;
using System.Windows.Forms;
using BaseReports.ReportsDE;
using BaseReports.Procedure;
using BaseReports.Properties;
using BaseReports.RFID;
using LocalCache;
using TrackControl.General;
using TrackControl.Reports;
using TrackControl.Vehicles;
using TrackControl.Reports.Graph;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrinting.Drawing;
using DevExpress.XtraExport;
using DevExpress.XtraReports;
using DevExpress.XtraReports.UI;
using DevExpress.XtraPrinting.Native;
using DevExpress.XtraPrinting.Control;
using DevExpress.XtraPrinting.Preview;
using DevExpress.XtraPrintingLinks;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views;
using DevExpress.XtraGrid.Views.Grid;
using BaseReports;
using Report;
using BaseReports.CrossingCZ;
using ReportsOnCheckZones.Properties;
using DevExpress.XtraScheduler;

namespace ReportsOnCheckZones
{
    /// <summary>
    /// Детальный отчет по прохождению контрольных зон
    /// </summary>
    public partial class CzDetailed : BaseReports.ReportsDE.BaseControl
    {
        public class ReportCz
        {
            string nameCZ1;
            string nameCZ2;
            string crossTimeCz1;
            string crossTimeCz2;
            double travel;
            TimeSpan travel_time;
            TimeSpan motionTravelTime;
            TimeSpan parkingTime;
            double averageSpeed;
            double max_speed;

            public ReportCz(string nameCZ1, string nameCZ2, string crossTimeCz1, string crossTimeCz2,
                            double travel, TimeSpan travel_time, TimeSpan motionTravelTime, TimeSpan parkingTime,
                            double averageSpeed, double max_speed)
            {
                this.nameCZ1 = nameCZ1;
                this.nameCZ2 = nameCZ2;
                this.crossTimeCz1 = crossTimeCz1;
                this.crossTimeCz2 = crossTimeCz2;
                this.travel = travel;
                this.travel_time = travel_time;
                this.motionTravelTime = motionTravelTime;
                this.parkingTime = parkingTime;
                this.averageSpeed = averageSpeed;
                this.max_speed = max_speed;
            }

            public string NameCZ1
            {
                get { return nameCZ1; }
            }

            public string NameCZ2
            {
                get { return nameCZ2; }
            }

            public string CrossTimeCZ1
            {
                get { return crossTimeCz1; }
            }

            public string CrossTimeCZ2
            {
                get { return crossTimeCz2; }
            }

            public double Travel
            {
                get { return travel; }
            }

            public TimeSpan TravelTime
            {
                get { return travel_time; }
            }

            public TimeSpan MotionTravelTime
            {
                get { return motionTravelTime; }
            }

            public TimeSpan ParkingTime
            {
                get { return parkingTime; }
            }

            public double AverageSpeed
            {
                get { return averageSpeed; }
            }

            public double MaxSpeed
            {
                get { return max_speed; }
            }
        }
        // reportCz
        /// <summary>
        /// Датасет CheckZonesDataSet
        /// </summary>
        private CheckZonesDataSet czDataSet;
        /// <summary>
        /// Общий пробег, км
        /// </summary>
        private float totalTravel;

        /// <summary>
        /// Общий пробег вне контрольных зон, км
        /// </summary>
        private double totalWayWithoutKZ;

        /// <summary>
        /// Общий пробег в контрольных зонах, км
        /// </summary>
        private double totalWayInKZ;
        /// <summary>
        /// Общее время на маршруте
        /// </summary>
        private TimeSpan totalTravelTime;
        /// <summary>
        /// Общее время движения
        /// </summary>
        private TimeSpan totalMotionTravelTime;
        /// <summary>
        /// Общее время стоянок
        /// </summary>
        private TimeSpan totalParkingTime;
        private VehicleInfo vehicleInfo;
        protected static atlantaDataSet dataset;
        ReportBase<ReportCz, TInfo> ReportCzDetailed;
        protected CzDetailedAlgorithm czDetailed;

        public CzDetailed()
        {
            InitializeComponent();
            Localization();
            HideStatusBar();
            DisableButton();
            VisionPanel(gridView, gridControl, bar1);
            gridView.RowClick += new RowClickEventHandler(gridView_RowClick);

            compositeReportLink1.CreateMarginalHeaderArea +=
                new CreateAreaEventHandler(composLink_CreateMarginalHeaderArea);

            ReportCzDetailed =
                new ReportBase<ReportCz, TInfo>(Controls, compositeReportLink1, gridView,
                GetStringBreackLeft, GetStringBreackRight, GetStringBreackUp);

            czDataSet = CheckZonesDataSetCreator.GetDataSet();
            atlantaDataSetBindingSource.DataSource = czDataSet;
            atlantaDataSetBindingSource.DataMember = "CZ_Detailed";
            //gridControl.DataSource = atlantaDataSetBindingSource;
            checkZonesDataSet = null;
            dataset = ReportTabControl.Dataset;
            AddAlgorithm(new CrossZonesAlgorithm());
            czDetailed = new CzDetailedAlgorithm();
            AddAlgorithm(czDetailed);
        }
        
        private BindingList<ReportCz> GetCrossZonesData(BindingSource setBindingSource)
        {
            BindingList<ReportCz> bndList = new BindingList<ReportCz>();

            try
            {
                if (setBindingSource == null)
                    return bndList;
                
                CultureInfo culture = CultureInfo.CurrentCulture;
                DateTimeFormatInfo dateTimeFormat = culture.DateTimeFormat;
                string[] datePattern = dateTimeFormat.GetAllDateTimePatterns();
                string dateFormat = dateTimeFormat.ShortDatePattern;
                string timeFormat = dateTimeFormat.LongTimePattern;
                string format = dateFormat + " " + timeFormat;

                if (setBindingSource.Count > 0)
                {
                    foreach (DataRowView dr in setBindingSource)
                    {
                        CheckZonesDataSet.CZ_DetailedRow row =
                            (CheckZonesDataSet.CZ_DetailedRow) dr.Row;

                        string nameCZ1 = row.NameCZ1;
                        string nameCZ2 = row.NameCZ2;
                        string crossTimeCz1 = row.CrossTimeCZ1.ToString(format);
                        string crossTimeCz2 = row.CrossTimeCZ2.ToString(format);
                        double travel = Math.Round(row.Travel, 2);
                        TimeSpan travel_time = row.TravelTime;
                        TimeSpan motionTravelTime = row.MotionTravelTime;
                        TimeSpan parkingTime = row.ParkingTime;
                        double averageSpeed = Math.Round(row.AverageSpeed, 2);
                        double max_speed = Math.Round(row.MaxSpeed, 2);

                        bndList.Add(new ReportCz(nameCZ1, nameCZ2, crossTimeCz1,
                            crossTimeCz2, travel, travel_time, motionTravelTime, parkingTime, averageSpeed,
                            max_speed));
                    } // foreach 1
                } // if

                return bndList;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Error CzDetailed", MessageBoxButtons.OK);
                return bndList;
            }
        }

        // DevCzDetailed
        public override void Select(LocalCache.atlantaDataSet.mobitelsRow m_row)
        {
            if (m_row != null)
            {
                curMrow = m_row;
                vehicleInfo = new VehicleInfo(m_row);
                atlantaDataSetBindingSource.Filter = String.Format("Mobitel_id={0}", m_row.Mobitel_ID);

                if (m_row.Check)
                {
                    if (atlantaDataSetBindingSource.Count > 0)
                    {
                        BindingList<ReportCz> reportCzes = GetCrossZonesData(atlantaDataSetBindingSource);
                        gridControl.DataSource = reportCzes; //atlantaDataSetBindingSource;
                        EnableButton();
                        CalcTotals();
                        SetStatusLine();
                    }
                }
                else
                {
                    DisableButton();
                    ClearStatusBar();
                    gridControl.DataSource = null;
                }
            } // if
        }
        // Select
        /// <summary>
        /// Расчет итогов
        /// </summary>
        private void CalcTotals()
        {
            totalTravel = 0;
            totalTravelTime = new TimeSpan();
            totalMotionTravelTime = new TimeSpan();
            totalParkingTime = new TimeSpan();
            totalWayInKZ = 0; // общий пробег в КЗ
            totalWayWithoutKZ = 0; // общий пробег вне КЗ

            foreach (DataRowView dr in atlantaDataSetBindingSource)
            {
                CheckZonesDataSet.CZ_DetailedRow row =
                    (CheckZonesDataSet.CZ_DetailedRow)dr.Row;

                totalTravel += row.Travel;
                totalTravelTime += row.TravelTime;
                totalMotionTravelTime += row.MotionTravelTime;
                totalParkingTime += row.ParkingTime;

                if (row.inZonesPoint)
                {
                    totalWayInKZ += row.Travel;
                }
            } // foreach

            totalWayWithoutKZ = totalTravel - totalWayInKZ;
        }
        // CalcTotals
        protected override void Algorithm_Action(object sender, EventArgs e)
        {
            if (sender is CzDetailedAlgorithm)
            {
                // EnableButton();
            }
        }

        /// <summary>
        /// Очищает данные отчета
        /// </summary>
        public override void ClearReport()
        {
            CrossZones.Instance.Clear();
            czDataSet.CZ_Detailed.Clear();
        }
        // Очитсим статусную строку
        public void ClearStatusBar()
        {
            barStaticItem1.Caption = Resources.CzDetailedCommonTravel + ":";
            barStaticItem2.Caption = Resources.CzDetailedCommonTimeInRoute + ":";
            barStaticItem3.Caption = Resources.CzDetailedCommonTimeMotion + ":";
            barStaticItem4.Caption = Resources.CzDetailedCommonTimeStops + ":";
        }

        // Нажатие кнопки формирования отчета
        protected override void bbiStart_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (!CrossZonesAlgorithm.AtLeastOneZoneExists)
            {
                XtraMessageBox.Show(Resources.CzDetailedNotZone,
                    "TrackControl", MessageBoxButtons.OK, MessageBoxIcon.Information);

                return;
            } // if

            if (!CrossZonesAlgorithm.AtLeastOneZoneIsChecked)
            {
                XtraMessageBox.Show(Resources.CzDetailedNotChooseZone,
                    "TrackControl", MessageBoxButtons.OK, MessageBoxIcon.Information);

                return;
            } // if

            bool noData = true; // За выбранный период нет данных ни по одной машине
            if (bbiStart.Caption == Resources.Start)
            {
                SetStopButton();
                DisableButton();
                ClearReport();
                ClearStatusBar();
                BeginReport();
                _stopRun = false;
                czDetailed.ClearPointList();

                //atlantaDataSetBindingSource.RaiseListChangedEvents = false;
                //atlantaDataSetBindingSource.SuspendBinding();

                foreach (atlantaDataSet.mobitelsRow m_row in dataset.mobitels)
                {
                    if (m_row.Check && DataSetManager.IsGpsDataExist(m_row))
                    {
                        SelectItem(m_row);
                        noData = false;
                        if (_stopRun)
                            break;
                    }
                }

                if (noData)
                {
                    XtraMessageBox.Show(Resources.WarningNoData, Resources.Notification,
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

                //atlantaDataSetBindingSource.RaiseListChangedEvents = true;
                //atlantaDataSetBindingSource.ResumeBinding();
                atlantaDataSetBindingSource.Filter = "";

                Select(curMrow);

                if (atlantaDataSetBindingSource.Count > 0)
                {
                    CalcTotals();
                    SetStatusLine();
                } // if

                ReportsControl.ShowGraph(curMrow);

                SetStartButton();
                EnableButton();
            } // if
            else
            {
                SetStartButton();
                _stopRun = true;
                StopReport();

                if (!noData)
                    EnableButton();
            }
        } // bbiStart_ItemClick

        protected void SetStatusLine()
        {
            barStaticItem1.Caption = Resources.CzDetailedCommonTravel + ": " + totalTravel.ToString("#,##0.00");
            barStaticItem2.Caption = Resources.CzDetailedCommonTimeInRoute + ": " + totalTravelTime.ToString();
            barStaticItem3.Caption = Resources.CzDetailedCommonTimeMotion + ": " + totalMotionTravelTime.ToString();
            barStaticItem4.Caption = Resources.CzDetailedCommonTimeStops + ": " + totalParkingTime.ToString();
        }

        /// <summary>
        /// Выборка данных
        /// </summary>
        public void SelectItem(atlantaDataSet.mobitelsRow m_row)
        {
            Application.DoEvents();

            foreach (IAlgorithm alg in _algoritms)
            {
                alg.SelectItem(m_row);
                alg.Run();

                if (_stopRun)
                    return;
            }

            Application.DoEvents();
            // ReportsControl.ShowGraph(m_row);
        }
        // SelectItem
        // show track on map
        protected override void bbiShowOnMap_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // в старом отчете эта функция не реализована
        }
        // показать график
        protected override void bbiShowOnGraf_ItemClick(object sender, ItemClickEventArgs e)
        {
            // в старом отчете эта функция не реализована
            ReportsControl.OnGraphShowNeeded();
            ShowOnGraph();
        }
        // клик по строке таблицы
        protected void gridView_RowClick(object sender, RowClickEventArgs e)
        {
            // в старом отчете эта функция не реализована
            ShowOnGraph();
        }

        protected void ShowOnGraph()
        {
            try
            {
                if (curMrow == null)
                    return;

                if (!DataSetManager.IsGpsDataExist(curMrow))
                    return;

                Graph.ClearRegion();
                Graph.ClearLabel();
                Int32[] iSelectList = gridView.GetSelectedRows();

                for (int i = 0; i < iSelectList.Length; i++)
                {
                    CheckZonesDataSet.CZ_DetailedRow row = (CheckZonesDataSet.CZ_DetailedRow)
                        ((DataRowView) atlantaDataSetBindingSource.List[iSelectList[i]]).Row;

                    if (row != null)
                    {
                        Graph.AddTimeRegion(row.CrossTimeCZ1, row.CrossTimeCZ2); // выделяем цветом
                    }
                }

                // ReportsControl.ShowGraph( curMrow );
                Graph.SellectZoom();
            }
            catch (Exception ex)
            {
                // to do this
            }
        } // ShowOnGraph

        protected double Round(double number)
        {
            double rnumber = number*100.0;
            int x = (int)(rnumber);
            double mem = rnumber - x;
            if (mem > 0.5)
                x = x + 1;
            return ((double) x)/100.0;
        }

        protected double GetTotalWayInCZ(atlantaDataSet.mobitelsRow curMRow)
        {
            double totalWayKZ = 0.0;

            GpsData[] gpsDatas = DataSetManager.GetDataGpsArray(curMRow);
            BaseCZAlgorithm.ZonePoint zonePointList = czDetailed.GetlistZonePoints(curMRow.Mobitel_ID);

            if (zonePointList != null)
            {
                for (int i = 0; i < zonePointList.numPair; i++)
                {
                    DateTime firstPoint = zonePointList.firstPoints[i].CrossingTime;
                    DateTime lastPoint = zonePointList.lastPoints[i].CrossingTime;
                    double firstDist = zonePointList.firstPoints[i].Kilometrage;
                    double lastDist = zonePointList.lastPoints[i].Kilometrage;

                    totalWayKZ += Round(lastDist);

                    //foreach (GpsData gpsData in gpsDatas)
                    //{
                    //    if (firstPoint < gpsData.Time)
                    //    {
                    //        if (lastPoint > gpsData.Time)
                    //        {
                    //            totalWayKZ += gpsData.Dist;
                    //        }
                    //    }
                    //} // foreach
                } // for
            } // if

            return Math.Round(totalWayKZ, 2);
        }

        // формирование отчета по текущей машине
        protected override void ExportToExcelDevExpress()
        {
            XtraGridService.SetupGidViewForPrint(gridView, true, true);

            TInfo t_info = new TInfo();
            VehicleInfo info = new VehicleInfo(curMrow.Mobitel_ID, curMrow);

            t_info.periodBeging = Algorithm.Period.Begin;
            t_info.periodEnd = Algorithm.Period.End;

            t_info.infoVehicle = info.Info; // Транспорт
            t_info.infoDriverName = info.DriverFullName; // Водитель транспорта

            t_info.totalWay = totalTravel; 
            t_info.totalTimeWay = totalTravelTime; 
            t_info.totalTimerTour = totalMotionTravelTime; 
            t_info.totalParking = totalParkingTime;

            if (curMrow.Check && DataSetManager.IsGpsDataExist(curMrow))
            {
                if (atlantaDataSetBindingSource.Count > 0)
                {
                    totalWayInKZ = GetTotalWayInCZ(curMrow);
                    totalWayWithoutKZ = Math.Abs(totalTravel - totalWayInKZ);
                    t_info.totalWayInKZ = totalWayInKZ; // общий пробег в КЗ
                    t_info.totalWayWithoutKZ = Math.Round(totalWayWithoutKZ, 2); // общий пробег вне КЗ
                } // if
            } // if

            ReportCzDetailed.AddInfoStructToList(t_info); 
            ReportCzDetailed.CreateAndShowReport(gridControl);
        }
        // ExportToExcelDevExpress
        // формирование отчета по текущим машинам
        protected override void ExportAllDevToReport()
        {
            CultureInfo culture = CultureInfo.CurrentCulture;
            DateTimeFormatInfo dateTimeFormat = culture.DateTimeFormat;
            string[] datePattern = dateTimeFormat.GetAllDateTimePatterns();
            string dateFormat = dateTimeFormat.ShortDatePattern;
            string timeFormat = dateTimeFormat.LongTimePattern;
            string format = dateFormat + " " + timeFormat;

            foreach (atlantaDataSet.mobitelsRow m_Row in dataset.mobitels)
            {
                if (m_Row.Check && DataSetManager.IsGpsDataExist(m_Row))
                {
                    atlantaDataSetBindingSource.Filter = String.Format("Mobitel_id={0}", m_Row.Mobitel_ID);
                    GpsData[] gpsDatas = DataSetManager.GetDataGpsArray(m_Row);

                    if (atlantaDataSetBindingSource.Count > 0)
                    {
                        TInfo t_info = new TInfo();
                        VehicleInfo info = new VehicleInfo(m_Row.Mobitel_ID, m_Row);

                        t_info.periodBeging = Algorithm.Period.Begin;
                        t_info.periodEnd = Algorithm.Period.End;

                        t_info.infoVehicle = info.Info; // Транспорт
                        t_info.infoDriverName = info.DriverFullName; // Водитель транспорта

                        totalTravel = 0;
                        totalTravelTime = new TimeSpan();
                        totalMotionTravelTime = new TimeSpan();
                        totalParkingTime =  new TimeSpan();

                        foreach (DataRowView dr in atlantaDataSetBindingSource)
                        {
                            CheckZonesDataSet.CZ_DetailedRow row = (CheckZonesDataSet.CZ_DetailedRow) dr.Row;

                            totalTravel += row.Travel;
                            totalTravelTime += row.TravelTime;
                            totalMotionTravelTime += row.MotionTravelTime;
                            totalParkingTime += row.ParkingTime;
                        }

                        t_info.totalWay = totalTravel;
                        t_info.totalTimeWay = totalTravelTime;
                        t_info.totalTimerTour = totalMotionTravelTime;
                        t_info.totalParking = totalParkingTime;

                        totalWayInKZ = GetTotalWayInCZ(m_Row);
                        totalWayWithoutKZ = Math.Abs(totalTravel - totalWayInKZ);
                        t_info.totalWayInKZ = totalWayInKZ; // общий пробег в КЗ
                        t_info.totalWayWithoutKZ = Math.Round(totalWayWithoutKZ, 2); // общий пробег вне КЗ

                        ReportCzDetailed.AddInfoStructToList(t_info); /* формируем заголовки таблиц отчета */
                        ReportCzDetailed.CreateBindDataList(); // создать новый список данных таблицы отчета

                        foreach (DataRowView dr in atlantaDataSetBindingSource)
                        {
                            CheckZonesDataSet.CZ_DetailedRow row = 
                                (CheckZonesDataSet.CZ_DetailedRow)dr.Row;

                            string nameCZ1 = row.NameCZ1;
                            string nameCZ2 = row.NameCZ2;
                            string crossTimeCz1 = row.CrossTimeCZ1.ToString(format);
                            string crossTimeCz2 = row.CrossTimeCZ2.ToString(format);
                            double travel = Math.Round(row.Travel, 2);
                            TimeSpan travel_time = row.TravelTime;
                            TimeSpan motionTravelTime = row.MotionTravelTime;
                            TimeSpan parkingTime = row.ParkingTime;
                            double averageSpeed = Math.Round(row.AverageSpeed, 2);
                            double max_speed = Math.Round(row.MaxSpeed, 2);

                            ReportCzDetailed.AddDataToBindList(new ReportCz(nameCZ1, nameCZ2, crossTimeCz1,
                                crossTimeCz2, travel, travel_time, motionTravelTime, parkingTime, averageSpeed,
                                max_speed));
                        } // foreach 2

                        ReportCzDetailed.CreateElementReport();
                    } // if
                } // if
            } // foreach 3

            ReportCzDetailed.CreateAndShowReport();
            ReportCzDetailed.DeleteData();
        }
        // ExportAllDevToReport
        // функция для формирования колонтитула отчета
        private void composLink_CreateMarginalHeaderArea(object sender, CreateAreaEventArgs e)
        {
            DevExpressReportHeader(Resources.ReportCzDetailTitle, e);
            TInfo info = ReportCzDetailed.GetInfoStructure;
            string strPeriod = Resources.PeriodFrom + " " + info.periodBeging + " " +
                               Resources.PeriodTo + " " + info.periodEnd;
            DevExpressReportSubHeader(strPeriod, 22, e);
        }
        /* функция для формирования левой части заголовка отчета */
        protected string GetStringBreackLeft()
        {
            ReportCzDetailed.SetRectangleBrckLetf(0, 0, 300, 85);
            TInfo info = ReportCzDetailed.GetInfoStructure;
            return (Resources.Vehicle + ": " + info.infoVehicle + "\n" +
            Resources.Driver + ": " + info.infoDriverName + "\n" + 
            Resources.TotalInKZ + ": " + info.totalWayInKZ);
        }
        /* функция для формирования верхней части заголовка отчета - будет пусто*/
        protected string GetStringBreackUp()
        {
            ReportCzDetailed.SetRectangleBrckUP(380, 0, 320, 85);
            return ("");
        }
        /* функция для формирования правой части заголовка отчета */
        protected string GetStringBreackRight()
        {
            TInfo info = ReportCzDetailed.GetInfoStructure;
            ReportCzDetailed.SetRectangleBrckRight(770, 0, 300, 85);
            return (Resources.TotalTrave + ": " + Math.Round(info.totalWay, 2) + "\n" +
            Resources.TotalTravelTime + ": " + info.totalTimeWay + "\n" +
            Resources.TotalTimeMotion + ": " + info.totalTimerTour + "\n" +
            Resources.TotalTimeParking + ": " + info.totalParking + "\n" +
            Resources.TotalWayWithoutKZ + ": " + info.totalWayWithoutKZ);
        }

        protected override void barButtonGroupPanel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GroupPanel(gridView);
        }

        protected override void barButtonFooterPanel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            FooterPanel(gridView);
        }

        protected override void barButtonNavigator_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            NavigatorPanel(gridControl);
        }

        protected override void barButtonStatusPanel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            StatusBar(bar1);
        }

        public override string Caption
        {
            get { return Resources.CzDetailedReportName; }
        }

        public void Localization()
        {
            barStaticItem1.Caption = Resources.CzDetailedCommonTravel + ":";
            barStaticItem2.Caption = Resources.CzDetailedCommonTimeInRoute + ":";
            barStaticItem3.Caption = Resources.CzDetailedCommonTimeMotion + ":";
            barStaticItem4.Caption = Resources.CzDetailedCommonTimeStops + ":";

            colNameCZ1.Caption = Resources.CzDetailedCz1;
            colNameCZ1.ToolTip = Resources.CzDetailedCz1;

            colNameCZ2.Caption = Resources.CzDetailedCz2;
            colNameCZ2.ToolTip = Resources.CzDetailedCz2;

            colCrossTimeCZ1.Caption = Resources.CzDetailedTimeCrossCz1;
            colCrossTimeCZ1.ToolTip = Resources.CzDetailedDateTimeCrossCz1;

            colCrossTimeCZ2.Caption = Resources.CzDetailedTimeCrossCz2;
            colCrossTimeCZ2.ToolTip = Resources.CzDetailedDateTimeCrossCz2;

            colTravel.Caption = Resources.CzDetailedTravel;
            colTravel.ToolTip = Resources.CzDetailedTravel;

            colTravelTime.Caption = Resources.CzDetailedCommonTimeInRoute;
            colTravelTime.ToolTip = Resources.CzDetailedCommonTimeInRoute;

            colMotionTravelTime.Caption = Resources.CzDetailedCommonTimeMotion;
            colMotionTravelTime.ToolTip = Resources.CzDetailedCommonTimeMotion;
          
            colParkingTime.Caption = Resources.CzDetailedCommonTimeStops;
            colParkingTime.ToolTip = Resources.CzDetailedSumTimeStopAndParking;

            colAverageSpeed.Caption = Resources.CzDetailedMiddleSpeed;
            colAverageSpeed.ToolTip = Resources.CzDetailedMiddleSpeed;

            colMaxSpeed.Caption = Resources.CzDetailedMaxSpeed;
            colMaxSpeed.ToolTip = Resources.CzDetailedMaxSpeed;
        }
        // Localization
    }
    // DevCzDetailed
}
// ReportsOnCheckZones