#region Using directives
using System;
using System.Collections.Generic;
using ReportsOnCheckZones.Properties;
using BaseReports.CrossingCZ;
using BaseReports.Procedure;
using LocalCache;
using TrackControl.Vehicles;
using TrackControl.Reports;
#endregion

namespace ReportsOnCheckZones
{
  /// <summary>
  /// ������������ ���������� ������ �� ������ � �����������
  /// ����� ������������� ����� ����������� ��� - 
  /// ������ ������� � ��������, �� �������� � ���. ���������� � ��.
  /// <para>��� ������� ������� ������� ����������� ������ ������ �������</para>
  /// ����� �������������� ������ ������ ���������� ������� ����
  /// �����������.
  /// ����. ����� ������.
  /// </summary>
  public class FuelCzInkomDutAlgorithm : BaseCZAlgorithm
  {
    /// <summary>
    /// ������������ ������
    /// </summary>
    /// <returns>������������ ������</returns>
    protected override string GetReportName()
    {
      return Resources.CzDutReportName;
    }

    /// <summary>
    /// ����� ����������� �� ��������� �������� � ������������
    /// �� ����������� (�������� � atlantaDataSet.CrossZones). 
    /// </summary>
    /// <param name="enumerator">�������� ��������� CrossingInfo.</param>
    protected override void FillReport(IEnumerator<CrossingInfo> enumerator)
    {
      if (!enumerator.MoveNext())
      {
        return;
      }

      // ������� ��� ��������� ������ � ������� RotateValue ������ �� ���������� ����� ������
      bool rotateValuesExist = m_row.GetRotateValueRows().Length > 0;

      //��������� ������ �� ������ �������
      FuelDictionarys fuelDict = new FuelDictionarys();
      Fuel fuelAlg = new Fuel();
      fuelAlg.SelectItem(m_row);
      fuelAlg.SettingAlgoritm(AlgorithmType.FUEL1);
      fuelAlg.GettingValuesDUT(fuelDict);

      CrossingInfo current = enumerator.Current;
      CrossingInfo next;

      while (enumerator.MoveNext())
      {
        next = enumerator.Current;

        CheckZonesDataSet.FuelCZ_DUT_InkomRow row =
          CzDataSet.FuelCZ_DUT_Inkom.NewFuelCZ_DUT_InkomRow();
        row.Mobitel_ID = m_row.Mobitel_ID;
        row.NameCZ1 = BuildZoneDescr(
          current.ZoneName, current.PointLocationType, current.PointId);
        row.NameCZ2 = BuildZoneDescr(
          next.ZoneName, next.PointLocationType, next.PointId);

        // ������� ������. 
        GpsData[] dArray = DataSetManager.GetDataGpsForPeriod(
          m_row, current.CrossingTime, next.CrossingTime);

        // ����� �����������
        row.CrossTimeCZ1 = current.CrossingTime;
        row.CrossTimeCZ2 = next.CrossingTime;

        // ����� ����� �� ��������
        row.TravelTime = (float)((row.CrossTimeCZ2 - row.CrossTimeCZ1).TotalHours);

        // ����� ����� ��������, �
        row.MotionTravelTime = (float)BasicMethods.GetTotalMotionTime(dArray).TotalHours;

        // ���������� ����, ��
        row.Travel = next.Kilometrage;

        // ������� ��������
        row.AverageSpeed = row.MotionTravelTime == 0 ? 0 :
          (float)Math.Round(row.Travel / row.MotionTravelTime, 2);

        // ������� ������� - ���� ���� ������ � FuelValue, �� ������� ������
        if (dArray.Length > 0)
        {
          if (fuelDict.ValueFuelSum.Count > 0)
          {
            // ������� �������
            row.BeginFuelLevel = (float)fuelDict.ValueFuelSum[dArray[0].Id].averageValue;
            // ������� � �����
            row.EndFuelLevel = (float)fuelDict.ValueFuelSum[dArray[dArray.Length - 1].Id].averageValue;
          }
        }

        // �������� � ����� ����� �� ������������ ������
        row.TotalFueling = 0;
        row.TotalFuelDischarge = 0;
        LocalCache.atlantaDataSet.FuelReportRow[] fuel_report_rows =
          GetFuelReportRow(dArray);
        if (fuel_report_rows.Length > 0)
        {
          foreach (LocalCache.atlantaDataSet.FuelReportRow fuel_rep_row in fuel_report_rows)
          {
            if (fuel_rep_row.dValue > 0)
            {
              // ��������
              row.TotalFueling += (float)fuel_rep_row.dValue;
            }
            else
            {
              // �����
              row.TotalFuelDischarge += (float)Math.Abs(fuel_rep_row.dValue);
            }
          }
        }

        // ����� ������ ������� � ����, �
        if (!row.IsBeginFuelLevelNull() && !row.IsEndFuelLevelNull())
        {
          row.TotalFuelRate = row.BeginFuelLevel + row.TotalFueling - row.EndFuelLevel;
        }

        CzDataSet.FuelCZ_DUT_Inkom.AddFuelCZ_DUT_InkomRow(row);

        current = next;
        ProgressIncrement();
      }
      fuelDict.Clear();
    }
  }
}
