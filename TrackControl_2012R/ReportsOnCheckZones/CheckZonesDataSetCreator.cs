using System;

namespace ReportsOnCheckZones
{
  /// <summary>
  /// ������� CheckZonesDataSet.
  /// ��� �������� � ���������, ������� ��������
  /// � ������ ��������� ������� �������� ��������� �
  /// ������� ������, � �� �������������� ��������������.
  /// </summary>
  internal static class CheckZonesDataSetCreator
  {
    /// <summary>
    /// ������ �������������
    /// </summary>
    private static volatile object syncObject = new object();
    /// <summary>
    /// ��������� ��������
    /// </summary>
    private static volatile CheckZonesDataSet dataSet;

    /// <summary>
    /// ���������� ��������� ��������
    /// </summary>
    /// <returns>CheckZonesDataSett</returns>
    internal static CheckZonesDataSet GetDataSet()
    {
      if (dataSet == null)
      {
        lock (syncObject)
        {
          if (dataSet == null)
            dataSet = new CheckZonesDataSet();
        }
      }

      return dataSet;
    } // GetDataSet()

  }
}
