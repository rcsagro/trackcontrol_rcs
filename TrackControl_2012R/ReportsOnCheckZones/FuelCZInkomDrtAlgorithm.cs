#region Using directives
using System;
using System.Collections.Generic;
using ReportsOnCheckZones.Properties;
using BaseReports.CrossingCZ;
using LocalCache;
using TrackControl.Vehicles;
using TrackControl.Reports;
#endregion

namespace ReportsOnCheckZones
{
  /// <summary>
  /// ������������ ���������� ������ �� ������ � �����������
  /// ����� ������������� ����� ����������� ��� - 
  /// ������ ������� � ��������, �� �������� � ���. ���������� � ��.
  /// <para>��� ������� ������� ������� ����������� ������ ������� �������</para>
  /// ����� �������������� ������ ������ ���������� ������� ����
  /// �����������.
  /// ����. ����� ������.
  /// </summary>
  public class FuelCZInkomDrtAlgorithm : BaseCZAlgorithm
  {
    /// <summary>
    /// ������������ ������
    /// </summary>
    /// <returns>������������ ������</returns>
    protected override string GetReportName()
    {
      return Resources.CzDrtReportName;
    }

    /// <summary>
    /// ����� ����������� �� ��������� �������� � ������������
    /// �� ����������� (�������� � atlantaDataSet.CrossZones). 
    /// </summary>
    /// <param name="enumerator">�������� ��������� CrossingInfo.</param>
    protected override void FillReport(IEnumerator<CrossingInfo> enumerator)
    {
      if (!enumerator.MoveNext())
      {
        return;
      }

      // ������� ��� ��������� ������ � ������� RotateValue ������ �� ���������� ����� ������
      bool rotateValuesExist = m_row.GetRotateValueRows().Length > 0;

      CrossingInfo current = enumerator.Current;
      CrossingInfo next;

      while (enumerator.MoveNext())
      {
        next = enumerator.Current;

        CheckZonesDataSet.FuelCZ_DRT_InkomRow row = CzDataSet.FuelCZ_DRT_Inkom.NewFuelCZ_DRT_InkomRow();
        row.Mobitel_ID = m_row.Mobitel_ID;
        row.NameCZ1 = BuildZoneDescr(
          current.ZoneName, current.PointLocationType, current.PointId);
        row.NameCZ2 = BuildZoneDescr(
          next.ZoneName, next.PointLocationType, next.PointId);

        // ������� ������. ���� ������ �������� ��� ������� ������� 
        // � ����������� ����������.
        GpsData[] dataRows = DataSetManager.GetDataGpsForPeriod(
          m_row, current.CrossingTime, next.CrossingTime);
        // ������ ��� ������� ��������� �����������
        GpsData[] fuelDataRows = DataSetManager.GetGpsDataExceptLastPoint(
          m_row, current.CrossingTime, next.CrossingTime);

        // ����� �����������
        row.CrossTimeCZ1 = current.CrossingTime;
        row.CrossTimeCZ2 = next.CrossingTime;

        // ����� ����� ��������, �
        TimeSpan travelTime = BasicMethods.GetTotalMotionTime(dataRows);
        row.TravelTime = (float)travelTime.TotalHours;

        // ���������� ����, ��
        row.Travel = next.Kilometrage;

        // ������� ��������
        row.AverageSpeed = row.TravelTime == 0 ? 0 :
          (float)Math.Round(row.Travel / travelTime.TotalHours, 2);

        // ������ ������� � ��������, �
        row.MotionFuelRate = (float)GetFuelUseInMotionDrt(fuelDataRows, 0);

        // ������� ������ � ��������, �/100��
        row.MotionAverageFuelRate = row.Travel == 0 ? 0 :
          row.MotionFuelRate / row.Travel * 100;

        // ������ ������� �� ��������, �
        row.ParkingFuelRate = (float)GetFuelUseInStopDrt(fuelDataRows, 0, 0);

        // ����� ������ ������� � ����, �
        row.TotalFuelRate = row.MotionFuelRate + row.ParkingFuelRate;

        if (rotateValuesExist)
        {
          // ����� ������� � ���������� ����������, �
          row.ParkingTimeWithEngineOn = (float)(
            BasicMethods.GetTotalStopsTime(dataRows, 0).TotalHours -
            GetTimeStopEnginOff(dataRows, 0, 0).TotalHours);

          // ������� ������ ������� �� ��������, �
          row.ParkingAverageFuelRate = row.ParkingTimeWithEngineOn == 0 ? 0 :
            row.ParkingFuelRate / row.ParkingTimeWithEngineOn;
        }
        else
        {
          row.ParkingTimeWithEngineOn = 0;
          row.ParkingAverageFuelRate = 0;
        }

        CzDataSet.FuelCZ_DRT_Inkom.AddFuelCZ_DRT_InkomRow(row);

        current = next;
        ProgressIncrement();
      }
    }
  }
}
