﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using BaseReports.ReportsDE;
using BaseReports.Procedure;
using BaseReports.Properties;
using BaseReports.RFID;
using LocalCache;
using TrackControl.General;
using TrackControl.Reports;
using TrackControl.Vehicles;
using TrackControl.Reports.Graph;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrinting.Drawing;
using DevExpress.XtraExport;
using DevExpress.XtraReports;
using DevExpress.XtraReports.UI;
using DevExpress.XtraPrinting.Native;
using DevExpress.XtraPrinting.Control;
using DevExpress.XtraPrinting.Preview;
using DevExpress.XtraPrintingLinks;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views;
using DevExpress.XtraGrid.Views.Grid;
using BaseReports;
using Report;
using BaseReports.CrossingCZ;
using ReportsOnCheckZones.Properties;
using DevExpress.XtraScheduler;
using ReportsOnCheckZones;
using TrackControl.Zones;

namespace BaseReports.ReportsOnDevExpress
{
    /// <summary>
    /// Класс контрола для отображения отчета о посещении контрольной зоны 
    /// транспортным средством
    /// </summary>
    [Serializable]
    public partial class KZ_Visitors : BaseReports.ReportsDE.BaseControl
    {
        /// <summary>
        /// Метод для локализации формы отчета
        /// </summary>
        void Localization()
        {
            // to do
        }

        /// <summary>
        /// Датасет CheckZonesDataSet
        /// </summary>
        private CheckZonesDataSet czDataSet;
        /// <summary>
        /// Контейнер данных
        /// </summary>
        protected static atlantaDataSet dataset;
        /// <summary>
        /// Класс CzDetailedAlgorithm
        /// </summary>
        protected KZ_ReportAlgorithm kzReported;

        private CzDetailedAlgorithm czDetailed = null;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="caption">Название отчета</param>
        public KZ_Visitors()
        {
            InitializeComponent();
            Localization();
            HideStatusBar();
            DisableButton();
            DisableRun();
            Dock = DockStyle.Fill;
            barStatus.Visible = false;
            SetNewNameToPrintBar("Текущая зона", "Формирует отчет по текущей зоне",
                                 "По всем вибраным зонам", "Формирует отчет по всем выбраным зонам");
            czDataSet = CheckZonesDataSetCreator.GetDataSet();
            dataset = ReportTabControl.Dataset;
            atlantaDataSetBindingSource.DataSource = czDataSet;
            atlantaDataSetBindingSource.DataMember = "CZ_Detailed";
            checkZonesDataSet = null;
            AddAlgorithm(new CrossZonesAlgorithm());
            czDetailed = new CzDetailedAlgorithm();
            AddAlgorithm(czDetailed);
            kzReported = new KZ_ReportAlgorithm();
            AddAlgorithm(kzReported);
            EnableRun();
            Hide();
        }

        public override string Caption
        {
            get
            {
                return "КЗ посещений";
            }
        }

        public override void Select(atlantaDataSet.mobitelsRow mobitel)
        {
            if (kzReported.IsWorking)
            {
                DataSet reportDataSet = kzReported.GetReportDataSet;
                gridKZControl.DataSource = reportDataSet.Tables["TrnsSummary"];
                gridKZControl.ForceInitialize();
                gridKZControl.LevelTree.Nodes.Add("Детально", gridKZDetailView);
            }
        }

        protected override void bbiStart_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (!CrossZonesAlgorithm.AtLeastOneZoneExists)
            {
                XtraMessageBox.Show(Resources.CzDetailedNotZone,
                    "TrackControl", MessageBoxButtons.OK, MessageBoxIcon.Information);

                return;
            } // if

            if (!CrossZonesAlgorithm.AtLeastOneZoneIsChecked)
            {
                XtraMessageBox.Show("Формирование отчета KZ_Report прервано. Нет выбранных зон!",
                    "TrackControl", MessageBoxButtons.OK, MessageBoxIcon.Information);

                return;
            } // if

            bool noData = true;

            if (bbiStart.Caption == Resources.Start)
            {
                SetStopButton();
                DisableButton();
                ClearReport();
                BeginReport();
                _stopRun = false;
                czDetailed.ClearPointList();
                kzReported.ClearPointList();
                gridKZControl.DataSource = null;

                foreach (atlantaDataSet.mobitelsRow mRow in _dataset.mobitels)
                {
                    if (mRow.Check && DataSetManager.IsGpsDataExist(mRow))
                    {
                        SelectItem(mRow);
                        noData = false;
                        if (_stopRun)
                            break;
                    } // if
                } // foreach

                if (noData)
                {
                    XtraMessageBox.Show(Resources.WarningNoData, Resources.Notification,
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

                atlantaDataSetBindingSource.Filter = "";

                Select(curMrow);

                if (atlantaDataSetBindingSource.Count > 0)
                {
                    // CalcTotals();
                    // SetStatusLine();
                }

                SetStartButton();
                EnableButton();
            }
            else
            {
                _stopRun = true;
                StopReport();
                if (!noData)
                    EnableButton();
                SetStartButton();
            }
        } // bbiStart_ItemClick

        public void SelectItem(atlantaDataSet.mobitelsRow m_row)
        {
            Application.DoEvents();

            foreach (IAlgorithm alg in _algoritms)
            {
                alg.SelectItem(m_row);
                alg.Run();

                if (_stopRun)
                    return;
            }

            Application.DoEvents();
        }

        public override void EnableButton()
        {
            bbiShowOnMap.Enabled = true;
            bbiShowOnGraf.Enabled = true;
            bbiPrintReport.Enabled = true;
        }

        public override void DisableButton()
        {
            bbiShowOnMap.Enabled = false;
            bbiShowOnGraf.Enabled = false;
            bbiPrintReport.Enabled = false;
        }

        /// <summary>
        /// Очищает данные отчета
        /// </summary>
        public override void ClearReport()
        {
            CrossZones.Instance.Clear();
            czDataSet.CZ_Detailed.Clear();
            //_summaries.Clear();
            ClearStatusLine();
        }

        private void ClearStatusLine()
        {
            //_distanceStatus.Caption = Resources.KilometrageCtrlSumDist.ToString() + ": ----";
            //_motionTimeStatus.Caption = Resources.KilometrageTravelTime.ToString() + ": --:--:--";
            //_parkingTimeStatus.Caption = Resources.KilometrageParkingTime.ToString() + ": --:--:--";
        }

        protected override void bbiShowOnMap_ItemClick(object sender, ItemClickEventArgs e)
        {
            // to do 	
        }

        protected override void bbiShowOnGraf_ItemClick(object sender, ItemClickEventArgs e)
        {
            // to do
        }

        // Формирует данные для отчета по всем активным машинам
        protected override void ExportAllDevToReport()
        {
            // to do
        }

        // формирование отчета по текущей машине
        protected override void ExportToExcelDevExpress()
        {
            // to do
        }

        protected override void barButtonGroupPanel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GroupPanel(gridKZView);
        }

        protected override void barButtonFooterPanel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            FooterPanel(gridKZView);
        }

        protected override void barButtonNavigator_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            NavigatorPanel(gridKZControl);
        }

        protected override void barButtonStatusPanel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            StatusBar(barStatus);
        }
    }
}
