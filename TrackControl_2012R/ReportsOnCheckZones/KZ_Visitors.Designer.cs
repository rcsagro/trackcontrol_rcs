﻿namespace BaseReports.ReportsOnDevExpress
{
    partial class KZ_Visitors
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            this.gridKZDetailView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gcolID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIDMobitel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateInZone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateOutZone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDatePlaceZone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateStopZone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colWayZone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridKZControl = new DevExpress.XtraGrid.GridControl();
            this.gridKZView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMobitelID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colZonesID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTransport = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateIn = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateOut = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNumInto = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateInZoneAll = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateStopZoneAll = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLongWay = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLongWayZone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager1 = new DevExpress.XtraBars.BarManager();
            this.barStatus = new DevExpress.XtraBars.Bar();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.atlantaDataSetBindingSource = new System.Windows.Forms.BindingSource();
            this.printingSystem1 = new DevExpress.XtraPrinting.PrintingSystem();
            this.CZDetailedBindingSource = new System.Windows.Forms.BindingSource();
            this.checkZonesDataSet = new ReportsOnCheckZones.CheckZonesDataSet();
            ((System.ComponentModel.ISupportInitialize)(this.gridKZDetailView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridKZControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridKZView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.atlantaDataSetBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CZDetailedBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkZonesDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // gridKZDetailView
            // 
            this.gridKZDetailView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gcolID,
            this.colIDMobitel,
            this.colDateInZone,
            this.colDateOutZone,
            this.colDatePlaceZone,
            this.colDateStopZone,
            this.colWayZone});
            this.gridKZDetailView.GridControl = this.gridKZControl;
            this.gridKZDetailView.Name = "gridKZDetailView";
            this.gridKZDetailView.OptionsBehavior.Editable = false;
            this.gridKZDetailView.OptionsView.ShowGroupPanel = false;
            // 
            // gcolID
            // 
            this.gcolID.AppearanceCell.Options.UseTextOptions = true;
            this.gcolID.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gcolID.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gcolID.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gcolID.AppearanceHeader.Options.UseTextOptions = true;
            this.gcolID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gcolID.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gcolID.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gcolID.Caption = "ID";
            this.gcolID.FieldName = "ID";
            this.gcolID.Name = "gcolID";
            this.gcolID.OptionsColumn.AllowEdit = false;
            this.gcolID.OptionsColumn.AllowFocus = false;
            this.gcolID.OptionsColumn.ReadOnly = true;
            this.gcolID.ToolTip = "ID";
            // 
            // colIDMobitel
            // 
            this.colIDMobitel.AppearanceCell.Options.UseTextOptions = true;
            this.colIDMobitel.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIDMobitel.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colIDMobitel.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colIDMobitel.AppearanceHeader.Options.UseTextOptions = true;
            this.colIDMobitel.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIDMobitel.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colIDMobitel.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colIDMobitel.Caption = "MobitelID";
            this.colIDMobitel.FieldName = "MobitelID";
            this.colIDMobitel.Name = "colIDMobitel";
            this.colIDMobitel.OptionsColumn.AllowEdit = false;
            this.colIDMobitel.OptionsColumn.AllowFocus = false;
            this.colIDMobitel.OptionsColumn.ReadOnly = true;
            this.colIDMobitel.ToolTip = "MobitelID";
            // 
            // colDateInZone
            // 
            this.colDateInZone.AppearanceCell.Options.UseTextOptions = true;
            this.colDateInZone.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDateInZone.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colDateInZone.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDateInZone.AppearanceHeader.Options.UseTextOptions = true;
            this.colDateInZone.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDateInZone.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colDateInZone.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDateInZone.Caption = "Дата и время въезда";
            this.colDateInZone.FieldName = "DateInZone";
            this.colDateInZone.Name = "colDateInZone";
            this.colDateInZone.OptionsColumn.AllowEdit = false;
            this.colDateInZone.OptionsColumn.AllowFocus = false;
            this.colDateInZone.OptionsColumn.ReadOnly = true;
            this.colDateInZone.ToolTip = "Дата и время каждого въезда в зону";
            this.colDateInZone.Visible = true;
            this.colDateInZone.VisibleIndex = 0;
            // 
            // colDateOutZone
            // 
            this.colDateOutZone.AppearanceCell.Options.UseTextOptions = true;
            this.colDateOutZone.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDateOutZone.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colDateOutZone.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDateOutZone.AppearanceHeader.Options.UseTextOptions = true;
            this.colDateOutZone.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDateOutZone.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colDateOutZone.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDateOutZone.Caption = "Дата и время выезда";
            this.colDateOutZone.FieldName = "DateOutZone";
            this.colDateOutZone.Name = "colDateOutZone";
            this.colDateOutZone.OptionsColumn.AllowEdit = false;
            this.colDateOutZone.OptionsColumn.AllowFocus = false;
            this.colDateOutZone.OptionsColumn.ReadOnly = true;
            this.colDateOutZone.ToolTip = "Дата и время каждого выезда из зоны";
            this.colDateOutZone.Visible = true;
            this.colDateOutZone.VisibleIndex = 1;
            // 
            // colDatePlaceZone
            // 
            this.colDatePlaceZone.AppearanceCell.Options.UseTextOptions = true;
            this.colDatePlaceZone.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDatePlaceZone.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colDatePlaceZone.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDatePlaceZone.AppearanceHeader.Options.UseTextOptions = true;
            this.colDatePlaceZone.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDatePlaceZone.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colDatePlaceZone.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDatePlaceZone.Caption = "Время в зоне";
            this.colDatePlaceZone.FieldName = "DatePlaceZone";
            this.colDatePlaceZone.Name = "colDatePlaceZone";
            this.colDatePlaceZone.OptionsColumn.AllowEdit = false;
            this.colDatePlaceZone.OptionsColumn.AllowFocus = false;
            this.colDatePlaceZone.OptionsColumn.ReadOnly = true;
            this.colDatePlaceZone.ToolTip = "Время нахождения в зоне";
            this.colDatePlaceZone.Visible = true;
            this.colDatePlaceZone.VisibleIndex = 2;
            // 
            // colDateStopZone
            // 
            this.colDateStopZone.AppearanceCell.Options.UseTextOptions = true;
            this.colDateStopZone.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDateStopZone.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colDateStopZone.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDateStopZone.AppearanceHeader.Options.UseTextOptions = true;
            this.colDateStopZone.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDateStopZone.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colDateStopZone.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDateStopZone.Caption = "Время стоянок в зоне";
            this.colDateStopZone.FieldName = "DateStopZone";
            this.colDateStopZone.Name = "colDateStopZone";
            this.colDateStopZone.OptionsColumn.AllowEdit = false;
            this.colDateStopZone.OptionsColumn.AllowFocus = false;
            this.colDateStopZone.OptionsColumn.ReadOnly = true;
            this.colDateStopZone.ToolTip = "Время каждой стоянки в зоне";
            this.colDateStopZone.Visible = true;
            this.colDateStopZone.VisibleIndex = 3;
            // 
            // colWayZone
            // 
            this.colWayZone.AppearanceCell.Options.UseTextOptions = true;
            this.colWayZone.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colWayZone.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colWayZone.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colWayZone.AppearanceHeader.Options.UseTextOptions = true;
            this.colWayZone.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colWayZone.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colWayZone.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colWayZone.Caption = "Пробег внутри зоны, км";
            this.colWayZone.FieldName = "WayZone";
            this.colWayZone.Name = "colWayZone";
            this.colWayZone.OptionsColumn.AllowEdit = false;
            this.colWayZone.OptionsColumn.AllowFocus = false;
            this.colWayZone.OptionsColumn.ReadOnly = true;
            this.colWayZone.ToolTip = "Пробег внутри зоны в км";
            this.colWayZone.Visible = true;
            this.colWayZone.VisibleIndex = 4;
            // 
            // gridKZControl
            // 
            this.gridKZControl.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.LevelTemplate = this.gridKZDetailView;
            gridLevelNode1.RelationName = "Fields";
            this.gridKZControl.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridKZControl.Location = new System.Drawing.Point(0, 24);
            this.gridKZControl.MainView = this.gridKZView;
            this.gridKZControl.MenuManager = this.barManager1;
            this.gridKZControl.Name = "gridKZControl";
            this.gridKZControl.Size = new System.Drawing.Size(799, 315);
            this.gridKZControl.TabIndex = 13;
            this.gridKZControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridKZView,
            this.gridKZDetailView});
            // 
            // gridKZView
            // 
            this.gridKZView.ColumnPanelRowHeight = 40;
            this.gridKZView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID,
            this.colMobitelID,
            this.colZonesID,
            this.colTransport,
            this.colDateIn,
            this.colDateOut,
            this.colNumInto,
            this.colDateInZoneAll,
            this.colDateStopZoneAll,
            this.colLongWay,
            this.colLongWayZone});
            this.gridKZView.GridControl = this.gridKZControl;
            this.gridKZView.Name = "gridKZView";
            this.gridKZView.OptionsBehavior.Editable = false;
            this.gridKZView.OptionsView.AnimationType = DevExpress.XtraGrid.Views.Base.GridAnimationType.AnimateFocusedItem;
            this.gridKZView.OptionsView.EnableAppearanceEvenRow = true;
            this.gridKZView.OptionsView.EnableAppearanceOddRow = true;
            this.gridKZView.OptionsView.ShowGroupPanel = false;
            // 
            // colID
            // 
            this.colID.AppearanceCell.Options.UseTextOptions = true;
            this.colID.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colID.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colID.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colID.AppearanceHeader.Options.UseTextOptions = true;
            this.colID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colID.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colID.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colID.Caption = "ID";
            this.colID.FieldName = "ID";
            this.colID.Name = "colID";
            this.colID.OptionsColumn.AllowEdit = false;
            this.colID.OptionsColumn.AllowFocus = false;
            this.colID.OptionsColumn.ReadOnly = true;
            this.colID.ToolTip = "ID";
            // 
            // colMobitelID
            // 
            this.colMobitelID.AppearanceCell.Options.UseTextOptions = true;
            this.colMobitelID.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMobitelID.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colMobitelID.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colMobitelID.AppearanceHeader.Options.UseTextOptions = true;
            this.colMobitelID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMobitelID.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colMobitelID.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colMobitelID.Caption = "MobitelID";
            this.colMobitelID.FieldName = "MobitelID";
            this.colMobitelID.Name = "colMobitelID";
            this.colMobitelID.OptionsColumn.AllowEdit = false;
            this.colMobitelID.OptionsColumn.AllowFocus = false;
            this.colMobitelID.OptionsColumn.ReadOnly = true;
            // 
            // colZonesID
            // 
            this.colZonesID.AppearanceCell.Options.UseTextOptions = true;
            this.colZonesID.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colZonesID.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colZonesID.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colZonesID.AppearanceHeader.Options.UseTextOptions = true;
            this.colZonesID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colZonesID.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colZonesID.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colZonesID.Caption = "ZonesID";
            this.colZonesID.FieldName = "ZonesID";
            this.colZonesID.Name = "colZonesID";
            this.colZonesID.OptionsColumn.AllowEdit = false;
            this.colZonesID.OptionsColumn.AllowFocus = false;
            this.colZonesID.OptionsColumn.ReadOnly = true;
            this.colZonesID.ToolTip = "Идентификатор зоны";
            // 
            // colTransport
            // 
            this.colTransport.AppearanceCell.Options.UseTextOptions = true;
            this.colTransport.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTransport.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTransport.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTransport.AppearanceHeader.Options.UseTextOptions = true;
            this.colTransport.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTransport.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTransport.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTransport.Caption = "Транспорт";
            this.colTransport.FieldName = "Transport";
            this.colTransport.Name = "colTransport";
            this.colTransport.OptionsColumn.AllowEdit = false;
            this.colTransport.OptionsColumn.AllowFocus = false;
            this.colTransport.OptionsColumn.ReadOnly = true;
            this.colTransport.ToolTip = "Транспортное средство";
            this.colTransport.Visible = true;
            this.colTransport.VisibleIndex = 0;
            // 
            // colDateIn
            // 
            this.colDateIn.AppearanceCell.Options.UseTextOptions = true;
            this.colDateIn.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDateIn.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colDateIn.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDateIn.AppearanceHeader.Options.UseTextOptions = true;
            this.colDateIn.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDateIn.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colDateIn.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDateIn.Caption = "Дата и время первого въезда";
            this.colDateIn.FieldName = "DateIn";
            this.colDateIn.Name = "colDateIn";
            this.colDateIn.OptionsColumn.AllowEdit = false;
            this.colDateIn.OptionsColumn.AllowFocus = false;
            this.colDateIn.OptionsColumn.ReadOnly = true;
            this.colDateIn.ToolTip = "Дата и время первого въезда в зону";
            this.colDateIn.Visible = true;
            this.colDateIn.VisibleIndex = 1;
            // 
            // colDateOut
            // 
            this.colDateOut.AppearanceCell.Options.UseTextOptions = true;
            this.colDateOut.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDateOut.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colDateOut.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDateOut.AppearanceHeader.Options.UseTextOptions = true;
            this.colDateOut.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDateOut.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colDateOut.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDateOut.Caption = "Дата и время последнего выезда";
            this.colDateOut.FieldName = "DateOut";
            this.colDateOut.Name = "colDateOut";
            this.colDateOut.OptionsColumn.AllowEdit = false;
            this.colDateOut.OptionsColumn.AllowFocus = false;
            this.colDateOut.OptionsColumn.ReadOnly = true;
            this.colDateOut.ToolTip = "Дата и время последнего выезда из зоны";
            this.colDateOut.Visible = true;
            this.colDateOut.VisibleIndex = 2;
            // 
            // colNumInto
            // 
            this.colNumInto.AppearanceCell.Options.UseTextOptions = true;
            this.colNumInto.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNumInto.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colNumInto.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colNumInto.AppearanceHeader.Options.UseTextOptions = true;
            this.colNumInto.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNumInto.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colNumInto.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colNumInto.Caption = "Кол-во посещений";
            this.colNumInto.FieldName = "NumInto";
            this.colNumInto.Name = "colNumInto";
            this.colNumInto.OptionsColumn.AllowEdit = false;
            this.colNumInto.OptionsColumn.AllowFocus = false;
            this.colNumInto.OptionsColumn.ReadOnly = true;
            this.colNumInto.ToolTip = "Количество посещений зоны";
            this.colNumInto.Visible = true;
            this.colNumInto.VisibleIndex = 3;
            // 
            // colDateInZoneAll
            // 
            this.colDateInZoneAll.AppearanceCell.Options.UseTextOptions = true;
            this.colDateInZoneAll.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDateInZoneAll.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colDateInZoneAll.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDateInZoneAll.AppearanceHeader.Options.UseTextOptions = true;
            this.colDateInZoneAll.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDateInZoneAll.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colDateInZoneAll.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDateInZoneAll.Caption = "Общее время в зоне";
            this.colDateInZoneAll.FieldName = "DateInZoneAll";
            this.colDateInZoneAll.Name = "colDateInZoneAll";
            this.colDateInZoneAll.OptionsColumn.AllowEdit = false;
            this.colDateInZoneAll.OptionsColumn.AllowFocus = false;
            this.colDateInZoneAll.OptionsColumn.ReadOnly = true;
            this.colDateInZoneAll.ToolTip = "Общее время нахождения в зоне";
            this.colDateInZoneAll.Visible = true;
            this.colDateInZoneAll.VisibleIndex = 4;
            // 
            // colDateStopZoneAll
            // 
            this.colDateStopZoneAll.AppearanceCell.Options.UseTextOptions = true;
            this.colDateStopZoneAll.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDateStopZoneAll.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colDateStopZoneAll.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDateStopZoneAll.AppearanceHeader.Options.UseTextOptions = true;
            this.colDateStopZoneAll.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDateStopZoneAll.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colDateStopZoneAll.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDateStopZoneAll.Caption = "Общее время стоянок в зоне";
            this.colDateStopZoneAll.FieldName = "DateStopZoneAll";
            this.colDateStopZoneAll.Name = "colDateStopZoneAll";
            this.colDateStopZoneAll.OptionsColumn.AllowEdit = false;
            this.colDateStopZoneAll.OptionsColumn.AllowFocus = false;
            this.colDateStopZoneAll.OptionsColumn.ReadOnly = true;
            this.colDateStopZoneAll.ToolTip = "Общее время стоянок в зоне";
            this.colDateStopZoneAll.Visible = true;
            this.colDateStopZoneAll.VisibleIndex = 5;
            // 
            // colLongWay
            // 
            this.colLongWay.AppearanceCell.Options.UseTextOptions = true;
            this.colLongWay.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLongWay.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colLongWay.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colLongWay.AppearanceHeader.Options.UseTextOptions = true;
            this.colLongWay.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLongWay.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colLongWay.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colLongWay.Caption = "Общий пробег, км";
            this.colLongWay.FieldName = "LongWay";
            this.colLongWay.Name = "colLongWay";
            this.colLongWay.OptionsColumn.AllowEdit = false;
            this.colLongWay.OptionsColumn.AllowFocus = false;
            this.colLongWay.OptionsColumn.ReadOnly = true;
            this.colLongWay.ToolTip = "Общий пробег транспорта в км";
            this.colLongWay.Visible = true;
            this.colLongWay.VisibleIndex = 6;
            // 
            // colLongWayZone
            // 
            this.colLongWayZone.AppearanceCell.Options.UseTextOptions = true;
            this.colLongWayZone.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLongWayZone.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colLongWayZone.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colLongWayZone.AppearanceHeader.Options.UseTextOptions = true;
            this.colLongWayZone.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLongWayZone.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colLongWayZone.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colLongWayZone.Caption = "Общий пробег внутри зоны, км";
            this.colLongWayZone.FieldName = "LongWayZone";
            this.colLongWayZone.Name = "colLongWayZone";
            this.colLongWayZone.OptionsColumn.AllowEdit = false;
            this.colLongWayZone.OptionsColumn.AllowFocus = false;
            this.colLongWayZone.OptionsColumn.ReadOnly = true;
            this.colLongWayZone.ToolTip = "Общий пробег транспорта внутри зоны в км";
            this.colLongWayZone.Visible = true;
            this.colLongWayZone.VisibleIndex = 7;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.barStatus});
            this.barManager1.DockControls.Add(this.barDockControl1);
            this.barManager1.DockControls.Add(this.barDockControl2);
            this.barManager1.DockControls.Add(this.barDockControl3);
            this.barManager1.DockControls.Add(this.barDockControl4);
            this.barManager1.Form = this;
            this.barManager1.MaxItemId = 0;
            this.barManager1.StatusBar = this.barStatus;
            // 
            // barStatus
            // 
            this.barStatus.BarName = "Status bar";
            this.barStatus.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.barStatus.DockCol = 0;
            this.barStatus.DockRow = 0;
            this.barStatus.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.barStatus.OptionsBar.AllowQuickCustomization = false;
            this.barStatus.OptionsBar.DrawDragBorder = false;
            this.barStatus.OptionsBar.UseWholeRow = true;
            this.barStatus.Text = "Status bar";
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Size = new System.Drawing.Size(799, 0);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 339);
            this.barDockControl2.Size = new System.Drawing.Size(799, 23);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 0);
            this.barDockControl3.Size = new System.Drawing.Size(0, 339);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(799, 0);
            this.barDockControl4.Size = new System.Drawing.Size(0, 339);
            // 
            // CZDetailedBindingSource
            // 
            this.CZDetailedBindingSource.DataMember = "CZ_Detailed";
            this.CZDetailedBindingSource.DataSource = this.checkZonesDataSet;
            // 
            // checkZonesDataSet
            // 
            this.checkZonesDataSet.DataSetName = "CheckZonesDataSet";
            this.checkZonesDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // KZ_Visitors
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridKZControl);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.Name = "KZ_Visitors";
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.gridKZControl, 0);
            ((System.ComponentModel.ISupportInitialize)(this.gridKZDetailView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridKZControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridKZView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.atlantaDataSetBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CZDetailedBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkZonesDataSet)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar barStatus;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraGrid.GridControl gridKZControl;
        private DevExpress.XtraGrid.Views.Grid.GridView gridKZView;
        private DevExpress.XtraGrid.Columns.GridColumn colTransport;
        private DevExpress.XtraGrid.Columns.GridColumn colDateIn;
        private DevExpress.XtraGrid.Columns.GridColumn colDateOut;
        private DevExpress.XtraGrid.Columns.GridColumn colNumInto;
        private DevExpress.XtraGrid.Columns.GridColumn colDateInZoneAll;
        private DevExpress.XtraGrid.Columns.GridColumn colDateStopZoneAll;
        private DevExpress.XtraGrid.Columns.GridColumn colLongWay;
        private DevExpress.XtraGrid.Columns.GridColumn colDateInZone;
        private DevExpress.XtraGrid.Columns.GridColumn colDateOutZone;
        private DevExpress.XtraGrid.Columns.GridColumn colDatePlaceZone;
        private DevExpress.XtraGrid.Columns.GridColumn colDateStopZone;
        private DevExpress.XtraGrid.Columns.GridColumn colWayZone;
        private DevExpress.XtraGrid.Columns.GridColumn colLongWayZone;
        internal DevExpress.XtraGrid.Views.Grid.GridView gridKZDetailView;
        private DevExpress.XtraGrid.Columns.GridColumn colIDMobitel;
        private DevExpress.XtraGrid.Columns.GridColumn colMobitelID;
        protected System.Windows.Forms.BindingSource atlantaDataSetBindingSource;
        private DevExpress.XtraPrinting.PrintingSystem printingSystem1;
        private System.Windows.Forms.BindingSource CZDetailedBindingSource;
        private ReportsOnCheckZones.CheckZonesDataSet checkZonesDataSet;
        private DevExpress.XtraGrid.Columns.GridColumn colZonesID;
        private DevExpress.XtraGrid.Columns.GridColumn gcolID;
        private DevExpress.XtraGrid.Columns.GridColumn colID;
    }
}
