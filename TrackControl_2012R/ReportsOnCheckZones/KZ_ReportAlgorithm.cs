﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraPivotGrid.Customization.ViewInfo;
using ReportsOnCheckZones.Properties;
using BaseReports.CrossingCZ;
using BaseReports.Procedure;
using DevExpress.XtraBars.Docking.Helpers;
using LocalCache;
using TrackControl.Reports;
using TrackControl.Vehicles;

namespace ReportsOnCheckZones
{
    /// <summary>
    /// Детальный отчет по выбранной зоне
    /// </summary>
    public class KZ_ReportAlgorithm : BaseCZAlgorithm
    {
        DataSet dsTrs = null;
        DataTable dtReport = null;
        DataTable dtData = null;

        /// <summary>
        /// Получить список точек зоны
        /// </summary>
        /// <returns>Список точек зоны</returns>
        public ZonePoint GetlistZonePoints(int mobid)
        {
            foreach (ZonePoint pointLst in zonePointVehicle)
            {
                if (pointLst.mobid == mobid)
                    return pointLst;
            }

            return null;
        }

        public KZ_ReportAlgorithm()
        {
            IsWorking = false;
        }

        /// <summary>
        /// Наименование отчета
        /// </summary>
        /// <returns>Наименование отчета</returns>
        protected override string GetReportName()
        {
            return "KZ_Reporting";
        }

        public void ClearPointList()
        {
            zonePointVehicle.Clear();
            NewDataStructure();
        }

        void CreateReportColumns(DataTable dtReport)
        {
            DataColumn col = null;

            // ID
            col = new DataColumn("ID", System.Type.GetType("System.Int32"));
            dtReport.Columns.Add(col);

            // ZonesID
            col = new DataColumn("ZonesID", System.Type.GetType("System.Int32"));
            dtReport.Columns.Add(col);

            // MobitelID
            col = new DataColumn("MobitelID", System.Type.GetType("System.Int32"));
            dtReport.Columns.Add(col);

            // Название и номер тренспортного средства
            col = new DataColumn("Transport", System.Type.GetType("System.String"));
            dtReport.Columns.Add(col);

            // Дата/время последнего въезда
            col = new DataColumn("DateIn", System.Type.GetType("System.String"));
            dtReport.Columns.Add(col);

            // Дата/время последнего выезда
            col = new DataColumn("DateOut", System.Type.GetType("System.String"));
            dtReport.Columns.Add(col);

            // Количество посещений зоны
            col = new DataColumn("NumInto", System.Type.GetType("System.Int32"));
            dtReport.Columns.Add(col);

            // Общее время в зоне
            col = new DataColumn("DateInZoneAll", System.Type.GetType("System.TimeSpan"));
            dtReport.Columns.Add(col);

            // Общее время стоянок в зоне
            col = new DataColumn("DateStopZoneAll", System.Type.GetType("System.TimeSpan"));
            dtReport.Columns.Add(col);

            // Общий пробег транспортного средства в км
            col = new DataColumn("LongWay", System.Type.GetType("System.Double"));
            dtReport.Columns.Add(col);

            // Общий пробег транспортного средства в нутри зоны в км
            col = new DataColumn("LongWayZone", System.Type.GetType("System.Double"));
            dtReport.Columns.Add(col);
        }

        private void KZ_Reports_Load(atlantaDataSet.mobitelsRow mobi, DateTime tmin, DateTime tmout, TimeSpan ts, double way, int idLine)
        {
            DataRow row = null;
            row = dtData.NewRow();
            row["ID"] = idLine;
            row["MobitelID"] = mobi.Mobitel_ID;
            row["DateInZone"] = tmin.ToString("dd:MM:yyyy HH:mm:ss");
            row["DateOutZone"] = tmout.ToString("dd:MM:yyyy HH:mm:ss");
            row["DatePlaceZone"] = tmout - tmin;
            row["DateStopZone"] = ts;
            row["WayZone"] = Math.Round(way, 3);
            dtData.Rows.Add(row);
        }

        private void KZ_Visitors_Load(atlantaDataSet.mobitelsRow mobi, string trName, DateTime dtbegin, DateTime dtEnd, int numVisitors, TimeSpan allTimeInZone, TimeSpan ts, double kolometerAll, double allway, int idzone, int idLine)
        {
            DataRow row = null;
            row = dtReport.NewRow();
            row["ID"] = idLine;
            row["ZonesID"] = idzone;
            row["MobitelID"] = mobi.Mobitel_ID;
            row["Transport"] = trName + " " + mobi.NumTT;
            row["DateIn"] = dtbegin.ToString("dd:MM:yyyy HH:mm:ss");
            row["DateOut"] = dtEnd.ToString("dd:MM:yyyy HH:mm:ss"); 
            row["NumInto"] = numVisitors;
            row["DateInZoneAll"] = allTimeInZone;
            row["DateStopZoneAll"] = ts;
            row["LongWay"] = Math.Round(allway, 3);
            row["LongWayZone"] = Math.Round(kolometerAll, 3);
            dtReport.Rows.Add(row);
        }

        private void CreateDataColumns(DataTable dtData)
        {
            DataColumn col = null;

            // IDLine
            col = new DataColumn("ID", System.Type.GetType("System.Int32"));
            dtData.Columns.Add(col);

            // MobitelID
            col = new DataColumn("MobitelID", System.Type.GetType("System.Int32"));
            dtData.Columns.Add(col);

            // Дата/время въезда
            col = new DataColumn("DateInZone", System.Type.GetType("System.String"));
            dtData.Columns.Add(col);

            // Дата/время выезда
            col = new DataColumn("DateOutZone", System.Type.GetType("System.String"));
            dtData.Columns.Add(col);

            // Время в зоне
            col = new DataColumn("DatePlaceZone", System.Type.GetType("System.TimeSpan"));
            dtData.Columns.Add(col);

            // Время стоянок в зоне
            col = new DataColumn("DateStopZone", System.Type.GetType("System.TimeSpan"));
            dtData.Columns.Add(col);

            // Пробег внутри зоны
            col = new DataColumn("WayZone", System.Type.GetType("System.Double"));
            dtData.Columns.Add(col);
        }

        public DataSet GetReportDataSet
        {
            get { return dsTrs; }
        }

        public void NewDataStructure()
        {
            dsTrs = new DataSet();
            dtReport = new DataTable("TrnsSummary");
            dtData = new DataTable("TrnsData");
        }

        public bool IsWorking { get; set; }

        public override void Run()
        {
            if (GpsDatas.Length == 0)
                return;

            try
            {
                string reportName = GetReportName();
                // Количество пересечений с КЗ у заданного телетрека
                int maximum = CrossZones.Instance.GetCrossingCount(m_row.Mobitel_ID);
                // Количество телетреков с информацией о пересечениях с КЗ
                int numTeletrack = CrossZones.Instance.Count;
                // Список пересечений заданного телетрека с КЗ
                IEnumerable<CrossingInfo> crossingInfos = CrossZones.Instance.GetCrossInfoCollection(m_row.Mobitel_ID);
                // Массив пересечений заданного телетрека с КЗ
                CrossingInfo[] crossingArray = CrossZones.Instance.GetCrossInfoArray(m_row.Mobitel_ID);

                List<Zones> idzonesList = new List<Zones>();
                Zones zn = new Zones(crossingArray[0].ZoneID, crossingArray[0].ZoneName);
                idzonesList.Add(zn);
                for (int j = 1; j < crossingArray.Length; j++)
                {
                    bool isInst = true;

                    for(int l = 0; l < idzonesList.Count; l++)
                    {
                        if (crossingArray[j].ZoneID == idzonesList[l].idZone)
                        {
                            isInst = false;
                            break;
                        }
                    }

                    if (isInst)
                    {
                        zn = new Zones(crossingArray[j].ZoneID, crossingArray[j].ZoneName);
                        idzonesList.Add(zn);
                    }
                }

                CreateReportColumns(dtReport);
                CreateDataColumns(dtData);

                if (CzDataSet.CZ_Detailed.Rows.Count > 0)
                {
                    int IDZone = 0;
                    int IDLine = 0;

                    for (int i = 0; i < idzonesList.Count; i++, IDLine++)
                    {
                        List<DataRow> dataRows = new List<DataRow>();
                        string nameOneColon = "NameCZ1";
                        string nameSecondColon = "NameCZ2";
                        string nameZoneIn = idzonesList[i].nameZone + " (Въезд)";
                        string nameZoneOut = idzonesList[i].nameZone + " (Выезд)";
                        double longWay = 0;
                        string transport = "";

                        DataRow[] kzDataRows = CzDataSet.CZ_Detailed.Select("ZonesID = " + idzonesList[i].idZone);
                        for (int k = 0; k < kzDataRows.Length; k++)
                        {
                            string name1 = kzDataRows[k][nameOneColon].ToString();
                            string name2 = kzDataRows[k][nameSecondColon].ToString();

                            if (name1 == nameZoneIn && name2 == nameZoneOut)
                            {
                                dataRows.Add(kzDataRows[k]);
                            }

                            string way = kzDataRows[k]["Travel"].ToString();
                            longWay += Convert.ToDouble(way);
                        }

                        TimeSpan allTimeInZone = new TimeSpan();
                        TimeSpan allparking = new TimeSpan();
                        double kolometerAll = 0;
                        TimeSpan parkSpan = new TimeSpan();

                        for (int k = 0; k < dataRows.Count; k++)
                        {
                            string time = dataRows[k]["CrossTimeCZ1"].ToString();
                            DateTime dtin = Convert.ToDateTime(time);
                            time = dataRows[k]["CrossTimeCZ2"].ToString();
                            DateTime dtout = Convert.ToDateTime(time);
                            string way = dataRows[k]["Travel"].ToString();
                            double kolometer = Convert.ToDouble(way);
                            string parking = dataRows[k]["ParkingTime"].ToString();
                            TimeSpan.TryParse(parking, out parkSpan);
                            KZ_Reports_Load(m_row, dtin, dtout, parkSpan, kolometer, IDLine);
                            allTimeInZone += (dtout - dtin);
                            kolometerAll += kolometer;
                            allparking += parkSpan;
                        } // for

                        if (dataRows.Count > 0)
                        {
                            transport = dataRows[0]["Transport"].ToString();
                            string tm = dataRows[0]["CrossTimeCZ1"].ToString();
                            DateTime dtbegin = Convert.ToDateTime(tm);
                            tm = dataRows[dataRows.Count - 1]["CrossTimeCZ2"].ToString();
                            DateTime dtEnd = Convert.ToDateTime(tm);
                            int numVisitors = dataRows.Count;
                            KZ_Visitors_Load(m_row, transport, dtbegin, dtEnd, numVisitors, allTimeInZone, allparking,
                                kolometerAll, longWay, IDZone, IDLine);
                            IsWorking = true;
                        } // if
                    } // for

                    if (dtReport.Rows.Count > 0)
                    {
                        dsTrs.Tables.Add(dtReport);
                        dsTrs.Tables.Add(dtData);
                        DataColumn keyColumn = dsTrs.Tables["TrnsSummary"].Columns["ID"];
                        DataColumn foreignKeyColumn = dsTrs.Tables["TrnsData"].Columns["ID"];
                        dsTrs.Relations.Add("Детально", keyColumn, foreignKeyColumn);
                    }
                } // if
            } // try
            finally
            {
                AfterReportFilling();
                Algorithm.OnAction(this, null);
            }
        }
    }

    public class Zones
    {
        public int idZone;
        public string nameZone;

        public Zones(int id, string name)
        {
            idZone = id;
            nameZone = name;
        }
    }
}
