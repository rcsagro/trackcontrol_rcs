﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using BaseReports;
using BaseReports.CrossingCZ;
using BaseReports.Procedure;
using TrackControl.Reports;
using ReportsOnCheckZones.Properties;
using BaseReports.ReportsDE;
using BaseReports.Properties;
using BaseReports.RFID;
using LocalCache;
using TrackControl.General;
using TrackControl.Vehicles;
using TrackControl.Reports.Graph;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrinting.Drawing;
using DevExpress.XtraExport;
using DevExpress.XtraReports;
using DevExpress.XtraReports.UI;
using DevExpress.XtraPrinting.Native;
using DevExpress.XtraPrinting.Control;
using DevExpress.XtraPrinting.Preview;
using DevExpress.XtraPrintingLinks;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views;
using DevExpress.XtraGrid.Views.Grid;
using Report;

namespace ReportsOnCheckZones
{
    /// <summary>
    /// Детальный отчет по машине в промежутках между пересечениями трека контрольных зон - 
    /// расход топлива в движении, на стоянках с вкл. двигателем и др.
    /// <para>Для расчета расхода топлива испольуются ДАТЧКИ УРОВНЯ ТОПЛИВА</para>
    /// Спец. заказ Инкома.
    /// </summary>
    public partial class DevFuelCZ_Inkom_DUT : BaseReports.ReportsDE.BaseControl
    {
        /// <summary>
        /// Датасет CheckZonesDataSet
        /// </summary>
        private CheckZonesDataSet czDataSet;
        VehicleInfo vehicleInfo;
        /// <summary>
        /// Общий пробег, км
        /// </summary>
        private float totalTravel;
        /// <summary>
        /// Общее время движения, ч
        /// </summary>
        private float motionTravelTime;
        /// <summary>
        /// Общий расход топлива, л
        /// </summary>
        private float totalFuelRate;
        /// <summary>
        /// Средний расход, л/100км
        /// </summary>
        private float avgFuelRate;
        protected static atlantaDataSet dataset;
        ReportBase<ReportFuelCzDut, TInfoDut> ReportFuelCZ_Inkom_DUT;
        
    #region -- .ctor --

        /// <summary>
        /// Конструктор.
        /// </summary>
        /// <param name="caption">Заголовок отчета</param>
        public DevFuelCZ_Inkom_DUT()
        {
            InitializeComponent();
            Localization();
            HideStatusBar();
            DisableButton();
            VisionPanel(gridView, gridControl, bar1);
            
            czDataSet = CheckZonesDataSetCreator.GetDataSet();
            atlantaDataSetBindingSource.DataSource = czDataSet;
            atlantaDataSetBindingSource.DataMember = "FuelCZ_DUT_Inkom";
            gridControl.DataSource = atlantaDataSetBindingSource;
            dataset = ReportTabControl.Dataset;
            checkZonesDataSet = null;
            
            gridView.RowClick += new RowClickEventHandler(gridView_RowClick);
            compositeLink1.CreateMarginalHeaderArea +=
                new CreateAreaEventHandler(composLink_CreateMarginalHeaderArea);
            ReportFuelCZ_Inkom_DUT =
                new ReportBase<ReportFuelCzDut, TInfoDut>( Controls, compositeLink1, gridView,
                    GetStringBreackLeft, GetStringBreackRight, GetStringBreackUp);
            
            AddAlgorithm(new Kilometrage());
            AddAlgorithm(new Fuel(AlgorithmType.FUEL1));
            AddAlgorithm(new CrossZonesAlgorithm());
            AddAlgorithm(new FuelCZInkomDutAlgorithm());
        } // DevFuelCZ_Inkom_DUT

        public override void Select(LocalCache.atlantaDataSet.mobitelsRow m_row)
        {
            if (m_row != null)
            {
                curMrow = m_row;
                vehicleInfo = new VehicleInfo(m_row);
                atlantaDataSetBindingSource.Filter = String.Format("Mobitel_id={0}", m_row.Mobitel_ID);
                
                if (m_row.Check)
                {
                    if (atlantaDataSetBindingSource.Count > 0)
                    {
                        gridControl.DataSource = atlantaDataSetBindingSource;
                        CalcTotals();
                        EnableButton();
                        SetStatusBar();
                    } // if
                }
                else
                {
                    DisableButton();
                    ClearStatusLine();
                    gridControl.DataSource = null;
                } // else                              
            } // if
        } // Select

        protected override void Algorithm_Action(object sender, EventArgs e)
        {
            if (sender is FuelCZInkomDutAlgorithm)
            {
                // EnableButton();
            }
        }

        /// <summary>
        /// Очищает данные отчета
        /// </summary>
        public override void ClearReport()
        {
            CrossZones.Instance.Clear();
            czDataSet.FuelCZ_DUT_Inkom.Clear();
        }

        /// <summary>
        /// Нажатие кнопки формирования отчета
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void bbiStart_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (!CrossZonesAlgorithm.AtLeastOneZoneExists)
            {
                XtraMessageBox.Show(Resources.FuelCzDutNotZone,
                    "TrackControl", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if (!CrossZonesAlgorithm.AtLeastOneZoneIsChecked)
            {
                XtraMessageBox.Show(Resources.FuelCzDutNotChooseZone,
                    "TrackControl", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

             bool noData = true; // За выбранный период нет данных ни по одной машине

            if (bbiStart.Caption == Resources.Start)
            {
                SetStopButton();
                DisableButton();
                ClearReport();
                ClearStatusLine();
                BeginReport();
                _stopRun = false;
                
                // Очистка таблицы
                ClearReport();

                atlantaDataSetBindingSource.RaiseListChangedEvents = false;
                atlantaDataSetBindingSource.SuspendBinding();

                foreach (atlantaDataSet.mobitelsRow m_row in dataset.mobitels)
                {
                    if (m_row.Check && m_row.GetdataviewRows().Length > 0)
                    {
                        SelectItem(m_row);
                        noData = false;
                        
                        if (_stopRun)
                            break;
                    } // if
                } // foraech

                if (noData)
                {
                    XtraMessageBox.Show(Resources.WarningNoData, Resources.Notification);
                }

                atlantaDataSetBindingSource.RaiseListChangedEvents = true;
                atlantaDataSetBindingSource.ResumeBinding();
                atlantaDataSetBindingSource.Filter = "";
                Select(curMrow);
                                
                if (atlantaDataSetBindingSource.Count > 0)
                {
                    CalcTotals();
                    SetStatusBar();
                    ReportsControl.ShowGraph(curMrow);
                }
                
                SetStartButton();
                EnableButton();
            } // if
            else
            {
                SetStartButton();
                _stopRun = true;
                StopReport();

                if (!noData)
                    EnableButton();
            } // else
        } // bbiStart_ItemClick
        
         // show track on map
        protected override void bbiShowOnMap_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // в старом отчете эта функция не реализована
        }

        // показать график
        protected override void bbiShowOnGraf_ItemClick(object sender, ItemClickEventArgs e)
        {
            ReportsControl.OnGraphShowNeeded();
            ShowOnGraph();
        }

        // клик по строке таблицы
        protected void gridView_RowClick(object sender, RowClickEventArgs e)
        {
            ShowOnGraph();
        }

        protected void ShowOnGraph()
        {
            if (curMrow == null)
                return;

            if (curMrow.GetdataviewRows() == null || curMrow.GetdataviewRows().Length == 0)
                return;

            Graph.ClearRegion();
            Graph.ClearLabel();
            Int32[] iSelectList = gridView.GetSelectedRows();
            for (int i = 0; i < iSelectList.Length; i++)
            {
                CheckZonesDataSet.FuelCZ_DUT_InkomRow row = (CheckZonesDataSet.FuelCZ_DUT_InkomRow)
                    ((DataRowView)atlantaDataSetBindingSource.List[iSelectList[i]]).Row;

                if (row != null)
                {
                    Graph.AddTimeRegion(row.CrossTimeCZ1, row.CrossTimeCZ2); // выделяем цветом
                }
            } // for

            // ReportsControl.ShowGraph( curMrow );
            Graph.SellectZoom();
        } // ShowOnGraph
        
        // функция для формирования колонтитула отчета
        private void composLink_CreateMarginalHeaderArea(object sender, CreateAreaEventArgs e)
        {
            DevExpressReportHeader(Resources.ReportFuelCZInkomDUT, e);
            TInfoDut info = ReportFuelCZ_Inkom_DUT.GetInfoStructure;
            string strPeriod = Resources.PeriodFrom + " " + info.periodBeging + " " +
                Resources.PeriodTo + " " + info.periodEnd;
            DevExpressReportSubHeader(strPeriod, 22, e);
        }
        
         /* функция для формирования левой части заголовка отчета */
        protected string GetStringBreackLeft()
        {
            ReportFuelCZ_Inkom_DUT.SetRectangleBrckLetf(0, 0, 300, 85);
            TInfoDut info = ReportFuelCZ_Inkom_DUT.GetInfoStructure;
            return (Resources.Vehicle + ": " + info.infoVehicle + "\n" +
                Resources.Driver + ": " + info.infoDriverName);
        }

        /* функция для формирования верхней части заголовка отчета - будет пусто*/
        protected string GetStringBreackUp()
        {
            TInfoDut info = ReportFuelCZ_Inkom_DUT.GetInfoStructure;
            ReportFuelCZ_Inkom_DUT.SetRectangleBrckUP(380, 0, 320, 85);
            return (Resources.TotalTravel + "; " + String.Format("{0:f2}", info.totalWay) + "\n" +
                Resources.TotalFuelConsumption + ": " + String.Format("{0:f2}", info.totalFuel));
        }

        /* функция для формирования правой части заголовка отчета */
        protected string GetStringBreackRight()
        {
            TInfoDut info = ReportFuelCZ_Inkom_DUT.GetInfoStructure;
            ReportFuelCZ_Inkom_DUT.SetRectangleBrckRight(740, 0, 330, 85);
            return (Resources.TotalTimeMoving + ": " + String.Format("{0:f2}", info.MotionTreavelTime) + "\n" +
                Resources.MiddleFuel + ": " + String.Format("{0:f2}", info.MiddleFuel));
        }
        
        // формирование отчета по текущей машине
        protected override void ExportToExcelDevExpress()
        {
            XtraGridService.SetupGidViewForPrint(gridView, true, true);

            TInfoDut t_info = new TInfoDut();
            VehicleInfo info = new VehicleInfo(curMrow.Mobitel_ID);

            t_info.periodBeging = Algorithm.Period.Begin;
            t_info.periodEnd = Algorithm.Period.End;

            t_info.infoDriverName = info.DriverFullName;
            t_info.infoVehicle = info.Info;

            t_info.totalWay = totalTravel;
            t_info.MotionTreavelTime = motionTravelTime;
            t_info.totalFuel = totalFuelRate;
            t_info.MiddleFuel = avgFuelRate;

            ReportFuelCZ_Inkom_DUT.AddInfoStructToList(t_info);
            ReportFuelCZ_Inkom_DUT.CreateAndShowReport(gridControl);
        } // ExportToExcelDevExpress
        
        // формирование отчета по текущим машинам
        protected override void ExportAllDevToReport()
        {
            foreach (atlantaDataSet.mobitelsRow m_Row in dataset.mobitels)
            {
                if (m_Row.Check && m_Row.GetdataviewRows().Length > 0)
                {
                    atlantaDataSetBindingSource.Filter = String.Format("Mobitel_id={0}", m_Row.Mobitel_ID);

                    if (atlantaDataSetBindingSource.Count > 0)
                    {
                        TInfoDut t_info = new TInfoDut();
                        VehicleInfo info = new VehicleInfo(m_Row.Mobitel_ID);
                        t_info.periodBeging = Algorithm.Period.Begin;
                        t_info.periodEnd = Algorithm.Period.End;

                        t_info.infoVehicle = info.Info;
                        t_info.infoDriverName = info.DriverFullName;
                        
                        CalcTotals(); // тоже сделать в DevFuelCZ_Inkom_DRT.cs

                        t_info.totalWay = totalTravel;
                        t_info.MotionTreavelTime = motionTravelTime;
                        t_info.totalFuel = totalFuelRate;
                        t_info.MiddleFuel = avgFuelRate;

                        ReportFuelCZ_Inkom_DUT.AddInfoStructToList(t_info); /* формируем заголовки таблиц отчета */
                        ReportFuelCZ_Inkom_DUT.CreateBindDataList(); // создать новый список данных таблицы отчета

                        foreach (DataRowView dr in atlantaDataSetBindingSource)
                        {
                            CheckZonesDataSet.FuelCZ_DUT_InkomRow row =
                                (CheckZonesDataSet.FuelCZ_DUT_InkomRow)dr.Row;

                            string namecz1 = row.NameCZ1;
                            string namecz2 = row.NameCZ2;
                            DateTime crossTimeCz1 = row.CrossTimeCZ1;
                            DateTime crossTimeCz2 = row.CrossTimeCZ2;
                            double travel = Math.Round(row.Travel, 2);
                            double travelTime = Math.Round(row.TravelTime, 2);
                            double moveTravelTime = Math.Round(row.MotionTravelTime, 2);
                            double averageSpeed = Math.Round(row.AverageSpeed, 2);
                            double beginFuelLevel = row.IsBeginFuelLevelNull() ? 0 : Math.Round(row.BeginFuelLevel, 2);
                            double endFuelLevel = row.IsEndFuelLevelNull() ? 0 : Math.Round(row.EndFuelLevel, 2);
                            double totalFueling = row.IsTotalFuelingNull() ? 0 : Math.Round(row.TotalFueling, 2);
                            double totalFuelDischarge = row.IsTotalFuelDischargeNull() ? 0 : Math.Round(row.TotalFuelDischarge);
                            double totFuelRate = row.IsTotalFuelRateNull() ? 0 : Math.Round(row.TotalFuelRate, 2);
                           
                            ReportFuelCZ_Inkom_DUT.AddDataToBindList(new ReportFuelCzDut(namecz1, namecz2, crossTimeCz1,
                                        crossTimeCz2, travel, travelTime, moveTravelTime, averageSpeed, beginFuelLevel, endFuelLevel,
                                        totalFueling, totalFuelDischarge, totFuelRate));
                        } // foreach 1

                        ReportFuelCZ_Inkom_DUT.CreateElementReport();
                    }  // if
                } // if
            } // foreach 2

            ReportFuelCZ_Inkom_DUT.CreateAndShowReport();
            ReportFuelCZ_Inkom_DUT.DeleteData();
        } // ExportAllDevToReport

        private void SetStatusBar()
        {
            barStaticItem1.Caption = Resources.CzDetailedCommonTravel + ":" + totalTravel.ToString("#,##0.00");
            barStaticItem2.Caption = Resources.FuelCzDrtCommonFuelrate + ":" + totalFuelRate.ToString("#,##0.00");
            barStaticItem3.Caption = Resources.FuelCzDrtCommonTimeMotion + ":" + motionTravelTime.ToString("#,##0.00");
            barStaticItem4.Caption = Resources.FuelCzDutMiddleFuelrate + ":" + avgFuelRate.ToString("#,##0.00");
        }

        /// <summary>
        /// Выборка данных
        /// </summary>
        public void SelectItem(atlantaDataSet.mobitelsRow m_row)
        {
            Application.DoEvents();
            foreach (IAlgorithm alg in _algoritms)
            {
                alg.SelectItem(m_row);
                alg.Run();

                if (_stopRun)
                    return;
            } // foreach
          
            Application.DoEvents();
            // ReportsControl.ShowGraph(m_row);
        } // SelectItem

        /// <summary>
        /// Control
        /// </summary>
        public UserControl Control
        {
            get { return (UserControl)this; }
        }

        /// <summary>
        /// Расчет итогов
        /// </summary>
        private void CalcTotals()
        {
            totalTravel = 0;
            motionTravelTime = 0;
            totalFuelRate = 0;
            avgFuelRate = 0;

            foreach (DataRowView dr in atlantaDataSetBindingSource)
            {
                CheckZonesDataSet.FuelCZ_DUT_InkomRow row =
                  (CheckZonesDataSet.FuelCZ_DUT_InkomRow)dr.Row;

                totalTravel += row.Travel;
                motionTravelTime += row.MotionTravelTime;
                if (row["TotalFuelRate"] != DBNull.Value)
                {
                    totalFuelRate += row.TotalFuelRate;
                }
            }

            avgFuelRate = totalTravel == 0 ? 0 : totalFuelRate / totalTravel * 100;
        } // CalcTotals
        
        public void ClearStatusLine()
        {
            barStaticItem1.Caption = Resources.CzDetailedCommonTravel + ":";
            barStaticItem2.Caption = Resources.FuelCzDrtCommonFuelrate + ":";
            barStaticItem3.Caption = Resources.FuelCzDrtCommonTimeMotion + ":";
            barStaticItem4.Caption = Resources.FuelCzDutMiddleFuelrate + ":";
        }
        
        protected override void barButtonGroupPanel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GroupPanel(gridView);
        }

        protected override void barButtonFooterPanel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            FooterPanel(gridView);
        }

        protected override void barButtonNavigator_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            NavigatorPanel(gridControl);
        }

        protected override void barButtonStatusPanel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            StatusBar(bar1);
        }
        
        public void Localization()
        {
            barStaticItem1.Caption = Resources.CzDetailedCommonTravel + ":";
            barStaticItem2.Caption = Resources.FuelCzDrtCommonFuelrate + ":";
            barStaticItem3.Caption = Resources.FuelCzDrtCommonTimeMotion + ":";
            barStaticItem4.Caption = Resources.FuelCzDutMiddleFuelrate + ":";
            
            colNameCZ1.Caption = Resources.CzDetailedCz1;
            colNameCZ1.ToolTip = Resources.CzDetailedCz1;
            
            colNameCZ2.Caption = Resources.CzDetailedCz2;
            colNameCZ2.ToolTip = Resources.CzDetailedCz2;
            
            colCrossTimeCZ1.Caption = Resources.CzDetailedTimeCrossCz1;
            colCrossTimeCZ1.ToolTip = Resources.CzDetailedDateTimeCrossCz1;
            
            colCrossTimeCZ2.Caption = Resources.CzDetailedTimeCrossCz2;
            colCrossTimeCZ2.ToolTip = Resources.CzDetailedDateTimeCrossCz2;
            
            colTravel.Caption = Resources.CzDetailedTravel;
            colTravel.ToolTip = Resources.CzDetailedTravel;
            
            colTravelTime.Caption = Resources.FuelCzDutCommonTimeInRoute;
            colTravelTime.ToolTip = Resources.FuelCzDutCommonTimeInRoute;
            
            colMotionTravelTime.Caption = Resources.FuelCzDrtCommonTimeMotion;
            colMotionTravelTime.ToolTip = Resources.FuelCzDrtCommonTimeMotion;
            
            colAverageSpeed.Caption = Resources.CzDetailedMiddleSpeed;
            colAverageSpeed.ToolTip = Resources.CzDetailedMiddleSpeed;
            
            colBeginFuelLevel.Caption = Resources.FuelCzDutFuelBegin;
            colBeginFuelLevel.ToolTip = Resources.FuelCzDutFuelBegin;
            
            colEndFuelLevel.Caption = Resources.FuelCzDutFuelEnd;
            colEndFuelLevel.ToolTip = Resources.FuelCzDutFuelEnd;
            
            colTotalFueling.Caption = Resources.FuelCzDutFuelAdd;
            colTotalFueling.ToolTip = Resources.FuelCzDutFuelAdd;
            
            colTotalFuelDischarge.Caption = Resources.FuelCzDutPourOff;
            colTotalFuelDischarge.ToolTip = Resources.FuelCzDutPourOff;
            
            colTotalFuelRate.Caption = Resources.FuelCzDrtCommonFuelrate;
            colTotalFuelRate.ToolTip = Resources.FuelCzDrtCommonFuelrate;
        } // Localization
        
    #endregion
    
        public override string Caption
        {
            get { return Resources.CzDutReportName; }
        }
        
        public class ReportFuelCzDut
        {
			string namecz1;
            string namecz2;
            DateTime crossTimeCz1;
            DateTime crossTimeCz2;
			double travel;
            double travelTime;
            double moveTravelTime;
            double averageSpeed;
            double beginFuelLevel;
            double endFuelLevel;
			double totalFueling;
			double totalFuelDischarge;
			double totFuelRate;
			
            public ReportFuelCzDut(string namecz1, string namecz2, DateTime crossTimeCz1, DateTime crossTimeCz2, 
								   double travel, double travelTime, double moveTravelTime, double averageSpeed, 
								   double beginFuelLevel, double endFuelLevel, double totalFueling, double totalFuelDischarge, 
								   double totFuelRate)
            {
                this.namecz1 = namecz1;
				this.namecz2 = namecz2;
				this.crossTimeCz1 = crossTimeCz1;
				this.crossTimeCz2 = crossTimeCz2;
				this.travel = travel;
				this.travelTime = travelTime;
				this.moveTravelTime = moveTravelTime;
				this.averageSpeed = averageSpeed;
				this.beginFuelLevel = beginFuelLevel;
				this.endFuelLevel = endFuelLevel;
				this.totalFueling = totalFueling;
				this.totalFuelDischarge = totalFuelDischarge;
				this.totFuelRate = totFuelRate;
            } // ReportFuelCzDut
			
			public string NameCZ1 
			{
				get { return namecz1; }
			}
			
			public string NameCZ2 
			{ 
				get { return namecz2; }
			}
			
			public DateTime CrossTimeCz1
			{
				get { return crossTimeCz1; }
			}
			
			public DateTime CrossTimeCz2
			{			
				get { return crossTimeCz2; }
			}
			
			public double Travel
			{
				get { return travel; }
			}
			
			public double TravelTime 
			{
				get { return travelTime; }
			}

			public double MotionTravelTime
            {
                get { return moveTravelTime; }
            }
            
            public double AverageSpeed
            {
                get { return averageSpeed; }
            }
            
            public double BeginFuelLevel
            {
                get { return beginFuelLevel; }
            }
            
            public double EndFuelLevel
            {
                get { return endFuelLevel; }
            }
            
            public double TotalFueling
            {
                get { return totalFueling; }
            }
            
            public double TotalFuelDischarge
            {
                get { return totalFuelDischarge; }
            }
            
            public double TotalFuelRate
            {
                get { return totFuelRate; }
            }
        } // class ReportFuelCzDut
    } // DevFuelCZ_Inkom_DUT
} // ReportsOnCheckZones
