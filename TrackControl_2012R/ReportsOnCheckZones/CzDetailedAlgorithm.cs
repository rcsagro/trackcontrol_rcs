using System;
using System.Collections.Generic;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraPivotGrid.Customization.ViewInfo;
using ReportsOnCheckZones.Properties;
using BaseReports.CrossingCZ;
using BaseReports.Procedure;
using DevExpress.XtraPivotGrid.Data;
using LocalCache;
using TrackControl.Reports;
using TrackControl.Vehicles;

namespace ReportsOnCheckZones
{
    /// <summary>
    /// ��������� ����� �� ����������� ����������� ���
    /// </summary>
    public class CzDetailedAlgorithm : BaseCZAlgorithm
    {
        protected atlantaDataSet.settingRow set_row;

        public ZonePoint GetlistZonePoints(int mobid)
        {
            foreach (ZonePoint pointLst in zonePointVehicle)
            {
                if (pointLst.mobid == mobid)
                    return pointLst;
            }

            return null;
        }
        /// <summary>
        /// ������������ ������
        /// </summary>
        /// <returns>������������ ������</returns>
        protected override string GetReportName()
        {
            return Resources.CzDetailedReportName;
        }

        /// <summary>
        /// ����� ����������� �� ��������� �������� � ������������
        /// �� ����������� (�������� � atlantaDataSet.CrossZones). 
        /// </summary>
        /// <param name="enumerator">�������� ��������� CrossingInfo.</param>
        protected override void FillReport(IEnumerator<CrossingInfo> enumerator)
        {
            try
            {
                if (!enumerator.MoveNext())
                {
                    return;
                }

                CrossingInfo current = enumerator.Current;
                CrossingInfo next;

                atlantaDataSet.vehicleRow[] vehicles = m_row.GetvehicleRows();

                while (enumerator.MoveNext())
                {
                    next = enumerator.Current;

                    CheckZonesDataSet.CZ_DetailedRow row = CzDataSet.CZ_Detailed.NewCZ_DetailedRow();

                    if (vehicles != null || vehicles.Length != 0)
                    {
                        row.Transport = vehicles[0].MakeCar + " (" + vehicles[0].NumberPlate + ")";
                    }
                    else
                    {
                        row.Transport = "����������";
                    }

                    row.Mobitel_ID = m_row.Mobitel_ID;
                    row.ZonesID = current.ZoneID;
                    row.NameCZ1 = BuildZoneDescr(current.ZoneName, current.PointLocationType, current.PointId);
                    row.NameCZ2 = BuildZoneDescr(next.ZoneName, next.PointLocationType, next.PointId);

                    // ������� ������
                    GpsData[] dataRows = DataSetManager.GetDataGpsForPeriod(m_row, current.CrossingTime,
                        next.CrossingTime);

                    // ����� �����������
                    row.CrossTimeCZ1 = current.CrossingTime;
                    row.CrossTimeCZ2 = next.CrossingTime;

                    // ����� ����� �� ��������
                    row.TravelTime = row.CrossTimeCZ2 - row.CrossTimeCZ1;

                    // ����� ����� ��������
                    row.MotionTravelTime = BasicMethods.GetTotalMotionTime(dataRows);

                    // ����� ����� �������
                    row.ParkingTime = row.TravelTime - row.MotionTravelTime;

                    // ���������� ����, ��
                    row.Travel = next.Kilometrage;

                    // ������� ��������
                    row.AverageSpeed = row.MotionTravelTime.TotalHours == 0
                        ? 0
                        : (float) Math.Round(row.Travel/row.MotionTravelTime.TotalHours, 2);

                    // ������������ ��������
                    row.MaxSpeed = BasicMethods.GetMaxSpeed(dataRows);
                    row.inZonesPoint = false;

                    CzDataSet.CZ_Detailed.AddCZ_DetailedRow(row);

                    current = next;

                    ProgressIncrement();
                }
            }
            catch (Exception e)
            {
                XtraMessageBox.Show(e.Message + "\n" + e.StackTrace, "Error Fill Report", MessageBoxButtons.OK);
            }
        }

        //public double CalcingDistance(GpsData[] gpsDatas)
        //{
        //    try
        //    {
        //        if (set_row != null) //���� ���� ������ � ����������� �� ����� ������ ������
        //        {
        //            _minTimeMove = set_row.TimeMinimMovingSize.TotalSeconds;
        //        }

        //        // �������� ������� �������
        //        if (gpsDatas == null || 0 == gpsDatas.Length || m_row == null)
        //            return;

        //        if (m_row.GetKilometrageReportRows() != null)
        //        {
        //            if (m_row.GetKilometrageReportRows().Length != 0)
        //            {
        //                return;
        //            }
        //        }

        //        BeforeReportFilling(BaseReports.Properties.Resources.Kilometrage, GpsDatas.Length);

        //        // ������� ����� � ��������� ��� �������. ����� ��� ����������� �������������
        //        // ����� ��� ����������� �� �����.
        //        GpsData breakPoint = GpsDatas[0];

        //        // ��������� ����� ��������� ��� �������� � ���������
        //        GpsData beginPoint = GpsDatas[0];

        //        // �������� ����� ��������� ��� �������� � ���������
        //        GpsData endPoint = GpsDatas[0];

        //        // ������ � ������� ��������� �������
        //        double distance = GpsDatas[0].Dist;

        //        // ������������ �������� �� DataGps
        //        double MaxSpeed = GpsDatas[0].Speed;

        //        // ���� �������� ������������� ��������
        //        bool isMoving = GpsDatas[0].Speed > 0;

        //        if (set_row != null)
        //        {
        //            _legalTimeBreak = set_row.TimeBreak;
        //        }
        //        _notBegin = false;
        //        _summary = new Kilometrage.Summary();
        //        byte j = 0;

        //        //double tempSpeed = 0.0;
        //        // ��������� ������� ����������� �� ���� �� ���������� ������
        //        int i = 0;
        //        for (i = 1; i < GpsDatas.Length; i++) // for optimization, 28 secund
        //        {
        //            if (GpsDatas[i].Speed > MaxSpeed)
        //                MaxSpeed = GpsDatas[i].Speed;

        //            if (!isMoving && GpsDatas[i].Speed > 0)
        //            {
        //                TimeSpan timeBreak = GpsDatas[i].Time - endPoint.Time;

        //                    if (timeBreak >= _legalTimeBreak)
        //                    {
        //                        if (endPoint != beginPoint)
        //                        {
        //                            addMotionToReport(breakPoint, beginPoint, endPoint, distance, ref MaxSpeed); // add Movement
        //                        }

        //                        addBreakToReport(endPoint, GpsDatas[i], timeBreak); // add Parking

        //                        beginPoint = GpsDatas[i];
        //                        breakPoint = endPoint;
        //                        endPoint = null;

        //                        if (_notBegin)
        //                            distance = 0;
        //                    } // if

        //                    isMoving = true;
        //            } // if

        //            if (isMoving && GpsDatas[i].Speed == 0)
        //            {
        //                isMoving = false;
        //                endPoint = GpsDatas[i];
        //            }

        //            distance += GpsDatas[i].Dist;

        //            if (j > 100)
        //            {
        //                ReportProgressChanged(i);
        //                j = 0;
        //            }
        //            else
        //            {
        //                j++;
        //            }
        //        } // for
        //        value = i;
                
        //        // ������������ ������� ����� �����
        //        if (isMoving)
        //        {
        //            if (GpsDatas.Length > 0)
        //            {
        //                addMotionToReport(breakPoint, beginPoint, GpsDatas[GpsDatas.Length - 1], distance, ref MaxSpeed);
        //            }
        //        }
        //        else
        //        {
        //            if (endPoint != beginPoint)
        //            {
        //                addMotionToReport(breakPoint, beginPoint, endPoint, distance, ref MaxSpeed);
        //            }

        //            addBreakToReport(endPoint, GpsDatas[GpsDatas.Length - 1], GpsDatas[GpsDatas.Length - 1].Time - endPoint.Time);
        //        }

        //        FormatingIntervals(); // for optimization

        //        analizingFirstInterval();
        //        AfterReportFilling();
        //    }
        //    catch (Exception ex)
        //    {
        //        XtraMessageBox.Show(ex.Message + "\n" + ex.StackTrace, "Error kilometrage", MessageBoxButtons.OK);
        //    }
        //}

        public void ClearPointList()
        {
            zonePointVehicle.Clear();
        }
    }
}
