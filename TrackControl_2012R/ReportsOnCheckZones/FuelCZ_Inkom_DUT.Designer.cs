﻿namespace ReportsOnCheckZones
{
    partial class FuelCZ_Inkom_DUT
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FuelCZ_Inkom_DUT));
            this.gridControl = new DevExpress.XtraGrid.GridControl();
            this.fuelCZInkomBindingSource = new System.Windows.Forms.BindingSource();
            this.checkZonesDataSet = new ReportsOnCheckZones.CheckZonesDataSet();
            this.gridView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colNameCZ1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNameCZ2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCrossTimeCZ1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCrossTimeCZ2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTravel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTravelTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMotionTravelTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAverageSpeed = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBeginFuelLevel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEndFuelLevel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalFueling = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalFuelDischarge = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalFuelRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.atlantaDataSetBindingSource = new System.Windows.Forms.BindingSource();
            this.barManager1 = new DevExpress.XtraBars.BarManager();
            this.bar1 = new DevExpress.XtraBars.Bar();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem2 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem3 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem4 = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.printingSystem1 = new DevExpress.XtraPrinting.PrintingSystem();
            this.compositeLink1 = new DevExpress.XtraPrintingLinks.CompositeLink();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fuelCZInkomBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkZonesDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.atlantaDataSetBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.compositeLink1.ImageCollection)).BeginInit();
            this.SuspendLayout();
            // 
            // gridControl
            // 
            this.gridControl.DataSource = this.fuelCZInkomBindingSource;
            this.gridControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gridControl.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gridControl.Location = new System.Drawing.Point(0, 24);
            this.gridControl.MainView = this.gridView;
            this.gridControl.Name = "gridControl";
            this.gridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit1});
            this.gridControl.Size = new System.Drawing.Size(824, 377);
            this.gridControl.TabIndex = 13;
            this.gridControl.UseEmbeddedNavigator = true;
            this.gridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView,
            this.gridView2});
            // 
            // fuelCZInkomBindingSource
            // 
            this.fuelCZInkomBindingSource.DataMember = "FuelCZ_DUT_Inkom";
            this.fuelCZInkomBindingSource.DataSource = this.checkZonesDataSet;
            // 
            // checkZonesDataSet
            // 
            this.checkZonesDataSet.DataSetName = "CheckZonesDataSet";
            this.checkZonesDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView
            // 
            this.gridView.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.DarkGray;
            this.gridView.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.DarkGray;
            this.gridView.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.DimGray;
            this.gridView.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gridView.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gridView.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gridView.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.DarkGray;
            this.gridView.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.DarkGray;
            this.gridView.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Gainsboro;
            this.gridView.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gridView.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gridView.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gridView.Appearance.Empty.BackColor = System.Drawing.Color.DimGray;
            this.gridView.Appearance.Empty.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.gridView.Appearance.Empty.Options.UseBackColor = true;
            this.gridView.Appearance.EvenRow.BackColor = System.Drawing.Color.White;
            this.gridView.Appearance.EvenRow.Options.UseBackColor = true;
            this.gridView.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.Gray;
            this.gridView.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.Gray;
            this.gridView.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gridView.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gridView.Appearance.FilterPanel.BackColor = System.Drawing.Color.Gray;
            this.gridView.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gridView.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gridView.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gridView.Appearance.FocusedRow.BackColor = System.Drawing.Color.LightSlateGray;
            this.gridView.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gridView.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridView.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridView.Appearance.FooterPanel.BackColor = System.Drawing.Color.DarkGray;
            this.gridView.Appearance.FooterPanel.BorderColor = System.Drawing.Color.DarkGray;
            this.gridView.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gridView.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gridView.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.gridView.Appearance.FooterPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView.Appearance.FooterPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridView.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.gridView.Appearance.GroupButton.BackColor = System.Drawing.Color.Silver;
            this.gridView.Appearance.GroupButton.BorderColor = System.Drawing.Color.Silver;
            this.gridView.Appearance.GroupButton.Options.UseBackColor = true;
            this.gridView.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gridView.Appearance.GroupFooter.BackColor = System.Drawing.Color.Silver;
            this.gridView.Appearance.GroupFooter.BorderColor = System.Drawing.Color.Silver;
            this.gridView.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gridView.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gridView.Appearance.GroupPanel.BackColor = System.Drawing.Color.DimGray;
            this.gridView.Appearance.GroupPanel.ForeColor = System.Drawing.Color.White;
            this.gridView.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gridView.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridView.Appearance.GroupRow.BackColor = System.Drawing.Color.Silver;
            this.gridView.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.gridView.Appearance.GroupRow.Options.UseBackColor = true;
            this.gridView.Appearance.GroupRow.Options.UseFont = true;
            this.gridView.Appearance.HeaderPanel.BackColor = System.Drawing.Color.DarkGray;
            this.gridView.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.DarkGray;
            this.gridView.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gridView.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gridView.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightSlateGray;
            this.gridView.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridView.Appearance.HorzLine.BackColor = System.Drawing.Color.LightGray;
            this.gridView.Appearance.HorzLine.Options.UseBackColor = true;
            this.gridView.Appearance.OddRow.BackColor = System.Drawing.Color.WhiteSmoke;
            this.gridView.Appearance.OddRow.Options.UseBackColor = true;
            this.gridView.Appearance.Preview.BackColor = System.Drawing.Color.Gainsboro;
            this.gridView.Appearance.Preview.ForeColor = System.Drawing.Color.DimGray;
            this.gridView.Appearance.Preview.Options.UseBackColor = true;
            this.gridView.Appearance.Preview.Options.UseForeColor = true;
            this.gridView.Appearance.Row.BackColor = System.Drawing.Color.White;
            this.gridView.Appearance.Row.Options.UseBackColor = true;
            this.gridView.Appearance.RowSeparator.BackColor = System.Drawing.Color.DimGray;
            this.gridView.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gridView.Appearance.SelectedRow.BackColor = System.Drawing.Color.DimGray;
            this.gridView.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gridView.Appearance.VertLine.BackColor = System.Drawing.Color.LightGray;
            this.gridView.Appearance.VertLine.Options.UseBackColor = true;
            this.gridView.AppearancePrint.FooterPanel.Options.UseTextOptions = true;
            this.gridView.AppearancePrint.FooterPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridView.AppearancePrint.FooterPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridView.AppearancePrint.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.gridView.BestFitMaxRowCount = 2;
            this.gridView.ColumnPanelRowHeight = 40;
            this.gridView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colNameCZ1,
            this.colNameCZ2,
            this.colCrossTimeCZ1,
            this.colCrossTimeCZ2,
            this.colTravel,
            this.colTravelTime,
            this.colMotionTravelTime,
            this.colAverageSpeed,
            this.colBeginFuelLevel,
            this.colEndFuelLevel,
            this.colTotalFueling,
            this.colTotalFuelDischarge,
            this.colTotalFuelRate});
            this.gridView.GridControl = this.gridControl;
            this.gridView.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always;
            this.gridView.Name = "gridView";
            this.gridView.OptionsDetail.AllowZoomDetail = false;
            this.gridView.OptionsDetail.EnableMasterViewMode = false;
            this.gridView.OptionsDetail.ShowDetailTabs = false;
            this.gridView.OptionsDetail.SmartDetailExpand = false;
            this.gridView.OptionsSelection.MultiSelect = true;
            this.gridView.OptionsView.ColumnAutoWidth = false;
            this.gridView.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView.OptionsView.EnableAppearanceOddRow = true;
            this.gridView.OptionsView.ShowFooter = true;
            // 
            // colNameCZ1
            // 
            this.colNameCZ1.AppearanceCell.Options.UseTextOptions = true;
            this.colNameCZ1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colNameCZ1.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colNameCZ1.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colNameCZ1.AppearanceHeader.Options.UseTextOptions = true;
            this.colNameCZ1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNameCZ1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colNameCZ1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colNameCZ1.Caption = "Контрольная зона 1";
            this.colNameCZ1.FieldName = "NameCZ1";
            this.colNameCZ1.ImageAlignment = System.Drawing.StringAlignment.Center;
            this.colNameCZ1.MinWidth = 10;
            this.colNameCZ1.Name = "colNameCZ1";
            this.colNameCZ1.OptionsColumn.AllowEdit = false;
            this.colNameCZ1.OptionsColumn.AllowFocus = false;
            this.colNameCZ1.OptionsColumn.FixedWidth = true;
            this.colNameCZ1.OptionsColumn.ReadOnly = true;
            this.colNameCZ1.ToolTip = "Контрольная зона 1";
            this.colNameCZ1.Visible = true;
            this.colNameCZ1.VisibleIndex = 0;
            this.colNameCZ1.Width = 100;
            // 
            // colNameCZ2
            // 
            this.colNameCZ2.AppearanceCell.Options.UseTextOptions = true;
            this.colNameCZ2.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colNameCZ2.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colNameCZ2.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colNameCZ2.AppearanceHeader.Options.UseTextOptions = true;
            this.colNameCZ2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNameCZ2.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colNameCZ2.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colNameCZ2.Caption = "Контрольная зона 2";
            this.colNameCZ2.FieldName = "NameCZ2";
            this.colNameCZ2.MinWidth = 10;
            this.colNameCZ2.Name = "colNameCZ2";
            this.colNameCZ2.OptionsColumn.AllowEdit = false;
            this.colNameCZ2.OptionsColumn.AllowFocus = false;
            this.colNameCZ2.OptionsColumn.FixedWidth = true;
            this.colNameCZ2.OptionsColumn.ReadOnly = true;
            this.colNameCZ2.ToolTip = "Контрольная зона 2";
            this.colNameCZ2.Visible = true;
            this.colNameCZ2.VisibleIndex = 1;
            this.colNameCZ2.Width = 100;
            // 
            // colCrossTimeCZ1
            // 
            this.colCrossTimeCZ1.AppearanceCell.Options.UseTextOptions = true;
            this.colCrossTimeCZ1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCrossTimeCZ1.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colCrossTimeCZ1.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCrossTimeCZ1.AppearanceHeader.Options.UseTextOptions = true;
            this.colCrossTimeCZ1.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCrossTimeCZ1.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colCrossTimeCZ1.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCrossTimeCZ1.Caption = "Время пересечения Контр. зоны 1";
            this.colCrossTimeCZ1.DisplayFormat.FormatString = "dd.MM.yyyy HH:mm:ss";
            this.colCrossTimeCZ1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colCrossTimeCZ1.FieldName = "CrossTimeCZ1";
            this.colCrossTimeCZ1.MinWidth = 10;
            this.colCrossTimeCZ1.Name = "colCrossTimeCZ1";
            this.colCrossTimeCZ1.OptionsColumn.AllowEdit = false;
            this.colCrossTimeCZ1.OptionsColumn.AllowFocus = false;
            this.colCrossTimeCZ1.OptionsColumn.FixedWidth = true;
            this.colCrossTimeCZ1.OptionsColumn.ReadOnly = true;
            this.colCrossTimeCZ1.ToolTip = "Дата и время пересечения контрольной зоны 1";
            this.colCrossTimeCZ1.Visible = true;
            this.colCrossTimeCZ1.VisibleIndex = 2;
            this.colCrossTimeCZ1.Width = 100;
            // 
            // colCrossTimeCZ2
            // 
            this.colCrossTimeCZ2.AppearanceCell.Options.UseTextOptions = true;
            this.colCrossTimeCZ2.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCrossTimeCZ2.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colCrossTimeCZ2.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCrossTimeCZ2.AppearanceHeader.Options.UseTextOptions = true;
            this.colCrossTimeCZ2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCrossTimeCZ2.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colCrossTimeCZ2.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colCrossTimeCZ2.Caption = "Время пересечения Контр. зоны 2";
            this.colCrossTimeCZ2.DisplayFormat.FormatString = "dd.MM.yyyy HH:mm:ss";
            this.colCrossTimeCZ2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colCrossTimeCZ2.FieldName = "CrossTimeCZ2";
            this.colCrossTimeCZ2.MinWidth = 10;
            this.colCrossTimeCZ2.Name = "colCrossTimeCZ2";
            this.colCrossTimeCZ2.OptionsColumn.AllowEdit = false;
            this.colCrossTimeCZ2.OptionsColumn.AllowFocus = false;
            this.colCrossTimeCZ2.OptionsColumn.FixedWidth = true;
            this.colCrossTimeCZ2.OptionsColumn.ReadOnly = true;
            this.colCrossTimeCZ2.ToolTip = "Дата и время пересечения контрольной зоны 2";
            this.colCrossTimeCZ2.Visible = true;
            this.colCrossTimeCZ2.VisibleIndex = 3;
            this.colCrossTimeCZ2.Width = 100;
            // 
            // colTravel
            // 
            this.colTravel.AppearanceCell.Options.UseTextOptions = true;
            this.colTravel.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTravel.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTravel.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTravel.AppearanceHeader.Options.UseTextOptions = true;
            this.colTravel.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTravel.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTravel.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTravel.Caption = "Пройденный путь, км";
            this.colTravel.DisplayFormat.FormatString = "{0:f2}";
            this.colTravel.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTravel.FieldName = "Travel";
            this.colTravel.MinWidth = 10;
            this.colTravel.Name = "colTravel";
            this.colTravel.OptionsColumn.AllowEdit = false;
            this.colTravel.OptionsColumn.AllowFocus = false;
            this.colTravel.OptionsColumn.FixedWidth = true;
            this.colTravel.OptionsColumn.ReadOnly = true;
            this.colTravel.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Travel", "{0:f2}")});
            this.colTravel.ToolTip = "Пройденный путь, км";
            this.colTravel.Visible = true;
            this.colTravel.VisibleIndex = 4;
            this.colTravel.Width = 80;
            // 
            // colTravelTime
            // 
            this.colTravelTime.AppearanceCell.Options.UseTextOptions = true;
            this.colTravelTime.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTravelTime.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTravelTime.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTravelTime.AppearanceHeader.Options.UseTextOptions = true;
            this.colTravelTime.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTravelTime.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTravelTime.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTravelTime.Caption = "Общее время на маршруте, ч";
            this.colTravelTime.DisplayFormat.FormatString = "{0:f2}";
            this.colTravelTime.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTravelTime.FieldName = "TravelTime";
            this.colTravelTime.Name = "colTravelTime";
            this.colTravelTime.OptionsColumn.AllowEdit = false;
            this.colTravelTime.OptionsColumn.AllowFocus = false;
            this.colTravelTime.OptionsColumn.ReadOnly = true;
            this.colTravelTime.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TravelTime", "{0:f2}")});
            this.colTravelTime.ToolTip = "Общее время на маршруте, ч";
            this.colTravelTime.Visible = true;
            this.colTravelTime.VisibleIndex = 5;
            this.colTravelTime.Width = 110;
            // 
            // colMotionTravelTime
            // 
            this.colMotionTravelTime.AppearanceCell.Options.UseTextOptions = true;
            this.colMotionTravelTime.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMotionTravelTime.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colMotionTravelTime.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colMotionTravelTime.AppearanceHeader.Options.UseTextOptions = true;
            this.colMotionTravelTime.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMotionTravelTime.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colMotionTravelTime.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colMotionTravelTime.Caption = "Общее время движения, ч";
            this.colMotionTravelTime.DisplayFormat.FormatString = "{0:f2}";
            this.colMotionTravelTime.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colMotionTravelTime.FieldName = "MotionTravelTime";
            this.colMotionTravelTime.MinWidth = 10;
            this.colMotionTravelTime.Name = "colMotionTravelTime";
            this.colMotionTravelTime.OptionsColumn.AllowEdit = false;
            this.colMotionTravelTime.OptionsColumn.AllowFocus = false;
            this.colMotionTravelTime.OptionsColumn.FixedWidth = true;
            this.colMotionTravelTime.OptionsColumn.ReadOnly = true;
            this.colMotionTravelTime.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "MotionTravelTime", "{0:f2}")});
            this.colMotionTravelTime.ToolTip = "Общее время только в движении, ч";
            this.colMotionTravelTime.Visible = true;
            this.colMotionTravelTime.VisibleIndex = 6;
            this.colMotionTravelTime.Width = 100;
            // 
            // colAverageSpeed
            // 
            this.colAverageSpeed.AppearanceCell.Options.UseTextOptions = true;
            this.colAverageSpeed.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colAverageSpeed.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colAverageSpeed.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colAverageSpeed.AppearanceHeader.Options.UseTextOptions = true;
            this.colAverageSpeed.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colAverageSpeed.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colAverageSpeed.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colAverageSpeed.Caption = "Средняя скорость, км/ч";
            this.colAverageSpeed.DisplayFormat.FormatString = "{0:f2}";
            this.colAverageSpeed.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colAverageSpeed.FieldName = "AverageSpeed";
            this.colAverageSpeed.Name = "colAverageSpeed";
            this.colAverageSpeed.OptionsColumn.AllowEdit = false;
            this.colAverageSpeed.OptionsColumn.AllowFocus = false;
            this.colAverageSpeed.OptionsColumn.ReadOnly = true;
            this.colAverageSpeed.ToolTip = "Средняя скорость, км/ч";
            this.colAverageSpeed.Visible = true;
            this.colAverageSpeed.VisibleIndex = 7;
            this.colAverageSpeed.Width = 80;
            // 
            // colBeginFuelLevel
            // 
            this.colBeginFuelLevel.AppearanceCell.Options.UseTextOptions = true;
            this.colBeginFuelLevel.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBeginFuelLevel.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colBeginFuelLevel.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBeginFuelLevel.AppearanceHeader.Options.UseTextOptions = true;
            this.colBeginFuelLevel.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBeginFuelLevel.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colBeginFuelLevel.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBeginFuelLevel.Caption = "Топлива в начале, л";
            this.colBeginFuelLevel.DisplayFormat.FormatString = "{0:f2}";
            this.colBeginFuelLevel.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colBeginFuelLevel.FieldName = "BeginFuelLevel";
            this.colBeginFuelLevel.MinWidth = 10;
            this.colBeginFuelLevel.Name = "colBeginFuelLevel";
            this.colBeginFuelLevel.OptionsColumn.AllowEdit = false;
            this.colBeginFuelLevel.OptionsColumn.AllowFocus = false;
            this.colBeginFuelLevel.OptionsColumn.FixedWidth = true;
            this.colBeginFuelLevel.OptionsColumn.ReadOnly = true;
            this.colBeginFuelLevel.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "BeginFuelLevel", "{0:f2}")});
            this.colBeginFuelLevel.ToolTip = "Топлива в начале, л";
            this.colBeginFuelLevel.Visible = true;
            this.colBeginFuelLevel.VisibleIndex = 8;
            this.colBeginFuelLevel.Width = 90;
            // 
            // colEndFuelLevel
            // 
            this.colEndFuelLevel.AppearanceCell.Options.UseTextOptions = true;
            this.colEndFuelLevel.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colEndFuelLevel.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colEndFuelLevel.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colEndFuelLevel.AppearanceHeader.Options.UseTextOptions = true;
            this.colEndFuelLevel.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colEndFuelLevel.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colEndFuelLevel.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colEndFuelLevel.Caption = "Топлива в конце, л";
            this.colEndFuelLevel.DisplayFormat.FormatString = "{0:f2}";
            this.colEndFuelLevel.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colEndFuelLevel.FieldName = "EndFuelLevel";
            this.colEndFuelLevel.MinWidth = 10;
            this.colEndFuelLevel.Name = "colEndFuelLevel";
            this.colEndFuelLevel.OptionsColumn.AllowEdit = false;
            this.colEndFuelLevel.OptionsColumn.AllowFocus = false;
            this.colEndFuelLevel.OptionsColumn.FixedWidth = true;
            this.colEndFuelLevel.OptionsColumn.ReadOnly = true;
            this.colEndFuelLevel.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "EndFuelLevel", "{0:f2}")});
            this.colEndFuelLevel.ToolTip = "Топлива в конце, л";
            this.colEndFuelLevel.Visible = true;
            this.colEndFuelLevel.VisibleIndex = 9;
            this.colEndFuelLevel.Width = 80;
            // 
            // colTotalFueling
            // 
            this.colTotalFueling.AppearanceCell.Options.UseTextOptions = true;
            this.colTotalFueling.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTotalFueling.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTotalFueling.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTotalFueling.AppearanceHeader.Options.UseTextOptions = true;
            this.colTotalFueling.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTotalFueling.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTotalFueling.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTotalFueling.Caption = "Всего заправлено, л";
            this.colTotalFueling.DisplayFormat.FormatString = "{0:f2}";
            this.colTotalFueling.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTotalFueling.FieldName = "TotalFueling";
            this.colTotalFueling.MinWidth = 10;
            this.colTotalFueling.Name = "colTotalFueling";
            this.colTotalFueling.OptionsColumn.AllowEdit = false;
            this.colTotalFueling.OptionsColumn.AllowFocus = false;
            this.colTotalFueling.OptionsColumn.FixedWidth = true;
            this.colTotalFueling.OptionsColumn.ReadOnly = true;
            this.colTotalFueling.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalFueling", "{0:f2}")});
            this.colTotalFueling.ToolTip = "Всего заправлено, л";
            this.colTotalFueling.Visible = true;
            this.colTotalFueling.VisibleIndex = 10;
            this.colTotalFueling.Width = 90;
            // 
            // colTotalFuelDischarge
            // 
            this.colTotalFuelDischarge.AppearanceCell.Options.UseTextOptions = true;
            this.colTotalFuelDischarge.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTotalFuelDischarge.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTotalFuelDischarge.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTotalFuelDischarge.AppearanceHeader.Options.UseTextOptions = true;
            this.colTotalFuelDischarge.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTotalFuelDischarge.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTotalFuelDischarge.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTotalFuelDischarge.Caption = "Всего слито, л";
            this.colTotalFuelDischarge.DisplayFormat.FormatString = "{0:f2}";
            this.colTotalFuelDischarge.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTotalFuelDischarge.FieldName = "TotalFuelDischarge";
            this.colTotalFuelDischarge.MinWidth = 10;
            this.colTotalFuelDischarge.Name = "colTotalFuelDischarge";
            this.colTotalFuelDischarge.OptionsColumn.AllowEdit = false;
            this.colTotalFuelDischarge.OptionsColumn.AllowFocus = false;
            this.colTotalFuelDischarge.OptionsColumn.FixedWidth = true;
            this.colTotalFuelDischarge.OptionsColumn.ReadOnly = true;
            this.colTotalFuelDischarge.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalFuelDischarge", "{0:f2}")});
            this.colTotalFuelDischarge.ToolTip = "Всего слито, л";
            this.colTotalFuelDischarge.Visible = true;
            this.colTotalFuelDischarge.VisibleIndex = 11;
            this.colTotalFuelDischarge.Width = 90;
            // 
            // colTotalFuelRate
            // 
            this.colTotalFuelRate.AppearanceCell.Options.UseTextOptions = true;
            this.colTotalFuelRate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTotalFuelRate.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTotalFuelRate.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTotalFuelRate.AppearanceHeader.Options.UseTextOptions = true;
            this.colTotalFuelRate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTotalFuelRate.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTotalFuelRate.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTotalFuelRate.Caption = "Общий расход топлива, л";
            this.colTotalFuelRate.DisplayFormat.FormatString = "{0:f2}";
            this.colTotalFuelRate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTotalFuelRate.FieldName = "TotalFuelRate";
            this.colTotalFuelRate.Name = "colTotalFuelRate";
            this.colTotalFuelRate.OptionsColumn.AllowEdit = false;
            this.colTotalFuelRate.OptionsColumn.AllowFocus = false;
            this.colTotalFuelRate.OptionsColumn.ReadOnly = true;
            this.colTotalFuelRate.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalFuelRate", "{0:f2}")});
            this.colTotalFuelRate.ToolTip = "Общий расход топлива, л";
            this.colTotalFuelRate.Visible = true;
            this.colTotalFuelRate.VisibleIndex = 12;
            this.colTotalFuelRate.Width = 80;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.ExportMode = DevExpress.XtraEditors.Repository.ExportMode.DisplayText;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // gridView2
            // 
            this.gridView2.GridControl = this.gridControl;
            this.gridView2.Name = "gridView2";
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar1});
            this.barManager1.DockControls.Add(this.barDockControl1);
            this.barManager1.DockControls.Add(this.barDockControl2);
            this.barManager1.DockControls.Add(this.barDockControl3);
            this.barManager1.DockControls.Add(this.barDockControl4);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barStaticItem1,
            this.barStaticItem2,
            this.barStaticItem3,
            this.barStaticItem4});
            this.barManager1.MaxItemId = 4;
            this.barManager1.StatusBar = this.bar1;
            // 
            // bar1
            // 
            this.bar1.BarName = "Status bar";
            this.bar1.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar1.DockCol = 0;
            this.bar1.DockRow = 0;
            this.bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem2),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem3),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticItem4)});
            this.bar1.OptionsBar.AllowQuickCustomization = false;
            this.bar1.OptionsBar.DrawDragBorder = false;
            this.bar1.OptionsBar.UseWholeRow = true;
            this.bar1.Text = "Status bar";
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Caption = "Общий пробег, км: 0";
            this.barStaticItem1.Id = 0;
            this.barStaticItem1.Name = "barStaticItem1";
            this.barStaticItem1.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItem2
            // 
            this.barStaticItem2.Caption = "Общее время движения, ч: 0";
            this.barStaticItem2.Id = 1;
            this.barStaticItem2.Name = "barStaticItem2";
            this.barStaticItem2.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItem3
            // 
            this.barStaticItem3.Caption = "Общий расход топлива, л: 0";
            this.barStaticItem3.Id = 2;
            this.barStaticItem3.Name = "barStaticItem3";
            this.barStaticItem3.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItem4
            // 
            this.barStaticItem4.Caption = "Средний расход, л/100км: 0";
            this.barStaticItem4.Id = 3;
            this.barStaticItem4.Name = "barStaticItem4";
            this.barStaticItem4.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Size = new System.Drawing.Size(824, 0);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 401);
            this.barDockControl2.Size = new System.Drawing.Size(824, 25);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 0);
            this.barDockControl3.Size = new System.Drawing.Size(0, 401);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(824, 0);
            this.barDockControl4.Size = new System.Drawing.Size(0, 401);
            // 
            // printingSystem1
            // 
            this.printingSystem1.Links.AddRange(new object[] {
            this.compositeLink1});
            // 
            // compositeLink1
            // 
            // 
            // 
            // 
            this.compositeLink1.ImageCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("compositeLink1.ImageCollection.ImageStream")));
            this.compositeLink1.Landscape = true;
            this.compositeLink1.Margins = new System.Drawing.Printing.Margins(10, 10, 70, 10);
            this.compositeLink1.MinMargins = new System.Drawing.Printing.Margins(10, 10, 15, 10);
            this.compositeLink1.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.compositeLink1.PrintingSystemBase = this.printingSystem1;
            // 
            // FuelCZ_Inkom_DUT
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridControl);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.Name = "FuelCZ_Inkom_DUT";
            this.Size = new System.Drawing.Size(824, 426);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.gridControl, 0);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fuelCZInkomBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.checkZonesDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.atlantaDataSetBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.compositeLink1.ImageCollection)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView;
        private DevExpress.XtraGrid.Columns.GridColumn colNameCZ1;
        private DevExpress.XtraGrid.Columns.GridColumn colNameCZ2;
        private DevExpress.XtraGrid.Columns.GridColumn colCrossTimeCZ1;
        private DevExpress.XtraGrid.Columns.GridColumn colCrossTimeCZ2;
        private DevExpress.XtraGrid.Columns.GridColumn colTravel;
        private DevExpress.XtraGrid.Columns.GridColumn colMotionTravelTime;
        private DevExpress.XtraGrid.Columns.GridColumn colAverageSpeed;
        private DevExpress.XtraGrid.Columns.GridColumn colBeginFuelLevel;
        private DevExpress.XtraGrid.Columns.GridColumn colEndFuelLevel;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalFueling;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalFuelDischarge;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalFuelRate;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private CheckZonesDataSet checkZonesDataSet;
        private System.Windows.Forms.BindingSource fuelCZInkomBindingSource;
        protected System.Windows.Forms.BindingSource atlantaDataSetBindingSource;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar1;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraBars.BarStaticItem barStaticItem2;
        private DevExpress.XtraBars.BarStaticItem barStaticItem3;
        private DevExpress.XtraBars.BarStaticItem barStaticItem4;
        private DevExpress.XtraGrid.Columns.GridColumn colTravelTime;
        private DevExpress.XtraPrinting.PrintingSystem printingSystem1;
        private DevExpress.XtraPrintingLinks.CompositeLink compositeLink1;
    }
}
