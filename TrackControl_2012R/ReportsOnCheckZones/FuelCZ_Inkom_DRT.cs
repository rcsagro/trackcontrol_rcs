﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using BaseReports;
using BaseReports.CrossingCZ;
using BaseReports.Procedure;
using TrackControl.Reports;
using ReportsOnCheckZones.Properties;
using BaseReports.ReportsDE;
using BaseReports.Properties;
using BaseReports.RFID;
using LocalCache;
using TrackControl.General;
using TrackControl.Vehicles;
using TrackControl.Reports.Graph;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrinting.Drawing;
using DevExpress.XtraExport;
using DevExpress.XtraReports;
using DevExpress.XtraReports.UI;
using DevExpress.XtraPrinting.Native;
using DevExpress.XtraPrinting.Control;
using DevExpress.XtraPrinting.Preview;
using DevExpress.XtraPrintingLinks;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views;
using DevExpress.XtraGrid.Views.Grid;
using Report;

namespace ReportsOnCheckZones
{
    /// <summary>
    /// Детальный отчет по машине в промежутках между пересечениями трека контрольных зон - 
    /// расход топлива в движении, на стоянках с вкл. двигателем и др.
    /// <para>Для расчета расхода топлива испольуются ДАТЧКИ РАСХОДА ТОПЛИВА</para>
    /// Спец. заказ Инкома.
    /// </summary>
    public partial class FuelCZ_Inkom_DRT : BaseReports.ReportsDE.BaseControl
    {
        /// <summary>
        /// Датасет CheckZonesDataSet
        /// </summary>
        private CheckZonesDataSet czDataSet;
        protected static atlantaDataSet dataset;
        /// <summary>
        /// Общий пробег
        /// </summary>
        private float totalTravel;
        /// <summary>
        /// Общий расход топлива
        /// </summary>
        private float totalFuelRate;
        /// <summary>
        /// Расход топлива в движении
        /// </summary>
        private float motionFuelRate;
        /// <summary>
        /// Средний расход топлива в движении
        /// </summary>
        private float avgMotionFuelRate;
        /// <summary>
        /// Расход топлива на стоянках
        /// </summary>
        private float parkingFuelRate;
        /// <summary>
        /// Средний расход топлива на стоянках
        /// </summary>
        private float avgParkingFuelRate;
        VehicleInfo vehicleInfo;
        ReportBase<ReportFuelCzDrt, TInfoDut> ReportFuelCZ_Inkom_DRT;

        public FuelCZ_Inkom_DRT()
        {
            InitializeComponent();
            Localization();
            HideStatusBar();
            DisableButton();
            VisionPanel(gridView, gridControl, bar1);

            czDataSet = CheckZonesDataSetCreator.GetDataSet();
            atlantaDataSetBindingSource.DataSource = czDataSet;
            atlantaDataSetBindingSource.DataMember = "FuelCZ_DRT_Inkom";
            gridControl.DataSource = atlantaDataSetBindingSource;
            checkZonesDataSet = null;
            dataset = ReportTabControl.Dataset;
            gridView.RowClick +=new RowClickEventHandler(gridView_RowClick);

            compositeReportLink1.CreateMarginalHeaderArea +=
                new CreateAreaEventHandler(composLink_CreateMarginalHeaderArea);

            ReportFuelCZ_Inkom_DRT =
                new ReportBase<ReportFuelCzDrt, TInfoDut>( Controls, compositeReportLink1, gridView,
                    GetStringBreackLeft, GetStringBreackRight, GetStringBreackUp);

            AddAlgorithm(new Kilometrage());
            AddAlgorithm(new Rotate(AlgorithmType.ROTATE_E));
            AddAlgorithm(FlowMeter.GetFlowMeter()); // new FlowMeter()); // .FlowMeter(AlgorithmType.FUEL2));
            AddAlgorithm(new CrossZonesAlgorithm());
            AddAlgorithm(new FuelCZInkomDrtAlgorithm());
        } // DevFuelCZ_Inkom_DRT

        protected override void Algorithm_Action(object sender, EventArgs e)
        {
            if (sender is FuelCZInkomDrtAlgorithm)
            {
                // EnableButton();
            }
        }

        public override void Select(LocalCache.atlantaDataSet.mobitelsRow m_row)
        {
            if (m_row != null)
            {
                curMrow = m_row;
                vehicleInfo = new VehicleInfo(m_row);
                atlantaDataSetBindingSource.Filter = String.Format("Mobitel_id={0}", m_row.Mobitel_ID);

                if (m_row.Check)
                {
                    if (atlantaDataSetBindingSource.Count > 0)
                    {
                        gridControl.DataSource = atlantaDataSetBindingSource;
                        EnableButton();
                        CalcTotals();
                        SetStatusLine();
                    }
                }
                else
                {
                    DisableButton();
                    ClearStatusLine();
                    gridControl.DataSource = null;
                } // else
            } // if
        } // Select

        /// <summary>
        /// Расчет итогов
        /// </summary>
        private void CalcTotals()
        {
            totalTravel = 0;
            totalFuelRate = 0;
            motionFuelRate = 0;
            avgMotionFuelRate = 0;
            parkingFuelRate = 0;
            avgParkingFuelRate = 0;

            float totalParkingTimeWithEngineOn = 0;

            foreach (DataRowView dr in atlantaDataSetBindingSource)
            {
                CheckZonesDataSet.FuelCZ_DRT_InkomRow row =
                  (CheckZonesDataSet.FuelCZ_DRT_InkomRow)dr.Row;

                totalTravel += row.Travel;
                totalFuelRate += row.TotalFuelRate;
                motionFuelRate += row.MotionFuelRate;
                parkingFuelRate += row.ParkingFuelRate;
                if (!row.IsParkingTimeWithEngineOnNull())
                {
                    totalParkingTimeWithEngineOn += row.ParkingTimeWithEngineOn;
                }
            }

            avgMotionFuelRate = totalTravel == 0 ? 0 :
              motionFuelRate / totalTravel * 100;

            avgParkingFuelRate = totalParkingTimeWithEngineOn == 0 ? 0 :
              parkingFuelRate / totalParkingTimeWithEngineOn;
        } // CalcTotals

        /// <summary>
        /// Очищает данные отчета
        /// </summary>
        public override void ClearReport()
        {
            CrossZones.Instance.Clear();
            czDataSet.FuelCZ_DRT_Inkom.Clear();
        }

        // Нажатие кнопки формирования отчета
        protected override void bbiStart_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (!CrossZonesAlgorithm.AtLeastOneZoneExists)
            {
                XtraMessageBox.Show(Resources.FuelCzDrtNotZone, "TrackControl", 
                    MessageBoxButtons.OK, MessageBoxIcon.Information);

                return;
            }

            if (!CrossZonesAlgorithm.AtLeastOneZoneIsChecked)
            {
                XtraMessageBox.Show(Resources.FuelCzDrtNotChooseZone,
                    "TrackControl", MessageBoxButtons.OK, MessageBoxIcon.Information);

                return;
            }

            bool noData = true; // За выбранный период нет данных ни по одной машине

            if (bbiStart.Caption == Resources.Start)
            {
                SetStopButton();
                DisableButton();
                ClearReport();
                ClearStatusLine();
                BeginReport();
                _stopRun = false;

                atlantaDataSetBindingSource.RaiseListChangedEvents = false;
                atlantaDataSetBindingSource.SuspendBinding();

                foreach (atlantaDataSet.mobitelsRow m_row in dataset.mobitels)
                {
                    if (m_row.Check && DataSetManager.IsGpsDataExist(m_row))
                    {
                        SelectItem(m_row);
                        noData = false;
                        if (_stopRun)
                            break;
                    }
                } // foreach

                if (noData)
                {
                    XtraMessageBox.Show(Resources.WarningNoData, Resources.Notification,
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

                atlantaDataSetBindingSource.RaiseListChangedEvents = true;
                atlantaDataSetBindingSource.ResumeBinding();
                atlantaDataSetBindingSource.Filter = "";

                Select(curMrow);

                if (atlantaDataSetBindingSource.Count > 0)
                {
                    CalcTotals();
                    SetStatusLine();
                } // if

                ReportsControl.ShowGraph(curMrow);

                SetStartButton();
                EnableButton();
            } // if
            else
            {
                SetStartButton();
                _stopRun = true;
                StopReport();

                if (!noData)
                    EnableButton();
            } // else
        } // bbiStart_ItemClick

        protected void SetStatusLine()
        {
            barStaticItem1.Caption = Resources.CzDetailedCommonTravel + ": " + totalTravel.ToString("#,##0.00");
            barStaticItem2.Caption = Resources.FuelCzDrtCommonFuelrate + ": " + totalFuelRate.ToString("#,##0.00");
            barStaticItem3.Caption = Resources.FuelCzDrtFuelrateInMoving + ": " + motionFuelRate.ToString("#,##0.00");
            barStaticItem4.Caption = Resources.FuelCzDrtMiddleFuelrateInMoving + ": " + avgMotionFuelRate.ToString("#,##0.00");
            barStaticItem5.Caption = Resources.FuelCzDrtFuelrateOnStops + ": " + parkingFuelRate.ToString("#,##0.00");
            // barStaticItem6.Caption = avgParkingFuelRate.ToString("#,##0.00");
        }

        /// <summary>
        /// Выборка данных
        /// </summary>
        public void SelectItem(atlantaDataSet.mobitelsRow m_row)
        {
            Application.DoEvents();
            foreach (IAlgorithm alg in _algoritms)
            {
                alg.SelectItem(m_row);
                alg.Run();

                if (_stopRun)
                    return;
            }
            Application.DoEvents();
            // ReportsControl.ShowGraph(m_row);
        } // SelectItem

        public override string Caption
        {
            get
            {
                return Resources.CzDrtReportName;
            }
        }

        // show track on map
        protected override void bbiShowOnMap_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            // в старом отчете эта функция не реализована
        }

        // показать график
        protected override void bbiShowOnGraf_ItemClick(object sender, ItemClickEventArgs e)
        {
            // в старом отчете эта функция не реализована
            ReportsControl.OnGraphShowNeeded();
            ShowOnGraph();
        }

        // клик по строке таблицы
        protected void gridView_RowClick(object sender, RowClickEventArgs e)
        {
            // в старом отчете эта функция не реализована
            ShowOnGraph();
        }

        protected void ShowOnGraph()
        {
            if ( curMrow == null )
                return;

            if (!DataSetManager.IsGpsDataExist(curMrow))
                return;

            Graph.ClearRegion();
            Graph.ClearLabel();
            Int32[] iSelectList = gridView.GetSelectedRows();
            for ( int i = 0 ; i < iSelectList.Length ; i++ )
            {
                CheckZonesDataSet.FuelCZ_DRT_InkomRow row = ( CheckZonesDataSet.FuelCZ_DRT_InkomRow )
                    (( DataRowView ) atlantaDataSetBindingSource.List[iSelectList[i]]).Row;

                if ( row != null )
                {
                    Graph.AddTimeRegion( row.CrossTimeCZ1, row.CrossTimeCZ2 ); // выделяем цветом
                }
            }

            // ReportsControl.ShowGraph( curMrow );
            Graph.SellectZoom();
        } // ShowOnGraph

        // функция для формирования колонтитула отчета
        private void composLink_CreateMarginalHeaderArea(object sender, CreateAreaEventArgs e)
        {
            DevExpressReportHeader(Resources.ReportFuelCZInkomDRT, e);
            TInfoDut info = ReportFuelCZ_Inkom_DRT.GetInfoStructure;
            string strPeriod = Resources.PeriodFrom + " " + info.periodBeging + " " +
                Resources.PeriodTo + " " + info.periodEnd;
            DevExpressReportSubHeader(strPeriod, 22, e);
        }

        /* функция для формирования левой части заголовка отчета */
        protected string GetStringBreackLeft()
        {
            ReportFuelCZ_Inkom_DRT.SetRectangleBrckLetf(0, 0, 300, 85);
            TInfoDut info = ReportFuelCZ_Inkom_DRT.GetInfoStructure;
            return (Resources.Vehicle + ": " + info.infoVehicle + "\n" +
                Resources.Driver + ": " + info.infoDriverName);
        }

        /* функция для формирования верхней части заголовка отчета - будет пусто*/
        protected string GetStringBreackUp()
        {
            TInfoDut info = ReportFuelCZ_Inkom_DRT.GetInfoStructure;
            ReportFuelCZ_Inkom_DRT.SetRectangleBrckUP(380, 0, 320, 85);
            return (Resources.TotalTrave + "; " + String.Format("{0:f2}", info.totalWay) + "\n" +
                Resources.TotalFuelConsumption + ": " + String.Format("{0:f2}", info.totalFuel) + "\n" +
                Resources.FuelConsumptionMotion + ": " + String.Format("{0:f2}", info.totalFuelSub));
        }

        /* функция для формирования правой части заголовка отчета */
        protected string GetStringBreackRight()
        {
            TInfoDut info = ReportFuelCZ_Inkom_DRT.GetInfoStructure;
            ReportFuelCZ_Inkom_DRT.SetRectangleBrckRight(740, 0, 330, 85);
            return (Resources.MiddleFuel + ": " + String.Format("{0:f2}", info.MiddleFuel) + "\n" +
                Resources.FuelParking + ": " + String.Format("{0:f2}", info.FuelStops) + "\n" +
                Resources.MiddleFuelParking + ": " + String.Format("{0:f2}", info.MiddleFuelStops));
        }

        // формирование отчета по текущей машине
        protected override void ExportToExcelDevExpress()
        {
            XtraGridService.SetupGidViewForPrint(gridView, true, true);

            TInfoDut t_info = new TInfoDut();
            VehicleInfo info = new VehicleInfo(curMrow.Mobitel_ID);

            t_info.periodBeging = Algorithm.Period.Begin;
            t_info.periodEnd = Algorithm.Period.End;

            t_info.infoDriverName = info.DriverFullName;
            t_info.infoVehicle = info.Info;

            t_info.totalWay = totalTravel;
            t_info.totalFuel = totalFuelRate;
            t_info.totalFuelSub = motionFuelRate;
            t_info.MiddleFuel = avgMotionFuelRate;
            t_info.FuelStops = parkingFuelRate;
            t_info.MiddleFuelStops = avgParkingFuelRate;

            ReportFuelCZ_Inkom_DRT.AddInfoStructToList(t_info);
            ReportFuelCZ_Inkom_DRT.CreateAndShowReport(gridControl);
        } // ExportToExcelDevExpress

        // формирование отчета по текущим машинам
        protected override void ExportAllDevToReport()
        {
            foreach ( atlantaDataSet.mobitelsRow m_Row in dataset.mobitels )
            {
                if (m_Row.Check && DataSetManager.IsGpsDataExist(m_Row))
                {
                    atlantaDataSetBindingSource.Filter = String.Format( "Mobitel_id={0}", m_Row.Mobitel_ID );

                    if ( atlantaDataSetBindingSource.Count > 0 )
                    {
                        TInfoDut t_info = new TInfoDut();
                        VehicleInfo info = new VehicleInfo( m_Row.Mobitel_ID );
                        t_info.periodBeging = Algorithm.Period.Begin;
                        t_info.periodEnd = Algorithm.Period.End;

                        t_info.infoVehicle = info.Info;
                        t_info.infoDriverName = info.DriverFullName;

                        CalcTotals();

                        t_info.totalWay = totalTravel;
                        t_info.totalFuel = totalFuelRate;
                        t_info.totalFuelSub = motionFuelRate;
                        t_info.MiddleFuel = avgMotionFuelRate;
                        t_info.FuelStops = parkingFuelRate;
                        t_info.MiddleFuelStops = avgParkingFuelRate;

                        ReportFuelCZ_Inkom_DRT.AddInfoStructToList( t_info ); /* формируем заголовки таблиц отчета */
                        ReportFuelCZ_Inkom_DRT.CreateBindDataList(); // создать новый список данных таблицы отчета

                        foreach ( DataRowView dr in atlantaDataSetBindingSource )
                        {
                            CheckZonesDataSet.FuelCZ_DRT_InkomRow row =
                                ( CheckZonesDataSet.FuelCZ_DRT_InkomRow ) dr.Row;

                            string namecz1 = row.NameCZ1;
                            string namecz2 = row.NameCZ2;
                            DateTime crossTimeCz1 = row.CrossTimeCZ1;
                            DateTime crossTimeCz2 = row.CrossTimeCZ2;
                            double travel = Math.Round(row.Travel, 2);
                            double travelTime = Math.Round(row.TravelTime, 2);
                            double averageSpeed = Math.Round(row.AverageSpeed, 2);
                            double motionFuel = Math.Round(row.MotionFuelRate, 2);
                            double motionAverageFuel = Math.Round(row.MotionAverageFuelRate, 2);
                            double parkingTimeEngineOn = row.IsParkingTimeWithEngineOnNull() ? 0.00 : Math.Round(row.ParkingTimeWithEngineOn, 2);
                            double parkingFuel = Math.Round(row.ParkingFuelRate, 2);
                            double parkingAverageFuel = row.IsParkingAverageFuelRateNull() ? 0.00 : Math.Round(row.ParkingAverageFuelRate, 2);
                            double totalFuel = Math.Round(row.TotalFuelRate, 2);

                            ReportFuelCZ_Inkom_DRT.AddDataToBindList( new ReportFuelCzDrt( namecz1, namecz2, crossTimeCz1,
                                        crossTimeCz2, travel, travelTime, averageSpeed, motionFuel, motionAverageFuel,
                                        parkingTimeEngineOn, parkingFuel, parkingAverageFuel, totalFuel ) );
                        } // foreach 1

                        ReportFuelCZ_Inkom_DRT.CreateElementReport();
                    }  // if
                } // if
            } // foreach 2

            ReportFuelCZ_Inkom_DRT.CreateAndShowReport();
            ReportFuelCZ_Inkom_DRT.DeleteData();
        } // ExportAllDevToReport

        protected override void barButtonGroupPanel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GroupPanel(gridView);
        }

        protected override void barButtonFooterPanel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            FooterPanel(gridView);
        }

        protected override void barButtonNavigator_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            NavigatorPanel(gridControl);
        }

        protected override void barButtonStatusPanel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            StatusBar(bar1);
        }

        public void ClearStatusLine()
        {
            barStaticItem1.Caption = Resources.CzDetailedCommonTravel + ":";
            barStaticItem2.Caption = Resources.FuelCzDrtCommonFuelrate + ":";
            barStaticItem3.Caption = Resources.FuelCzDrtFuelrateInMoving + ":";
            barStaticItem4.Caption = Resources.FuelCzDrtMiddleFuelrateInMoving + ":";
            barStaticItem5.Caption = Resources.FuelCzDrtFuelrateOnStops + ":";
        }

        public void Localization()
        {
            barStaticItem1.Caption = Resources.CzDetailedCommonTravel + ":";
            barStaticItem2.Caption = Resources.FuelCzDrtCommonFuelrate + ":";
            barStaticItem3.Caption = Resources.FuelCzDrtFuelrateInMoving + ":";
            barStaticItem4.Caption = Resources.FuelCzDrtMiddleFuelrateInMoving + ":";
            barStaticItem5.Caption = Resources.FuelCzDrtFuelrateOnStops + ":";

            colNameCZ1.Caption = Resources.CzDetailedCz1;
            colNameCZ1.ToolTip = Resources.CzDetailedCz1;

            colNameCZ2.Caption = Resources.CzDetailedCz2;
            colNameCZ2.ToolTip = Resources.CzDetailedCz2;

            colCrossTimeCZ1.Caption = Resources.CzDetailedTimeCrossCz1;
            colCrossTimeCZ1.ToolTip = Resources.CzDetailedDateTimeCrossCz1;

            colCrossTimeCZ2.Caption = Resources.CzDetailedTimeCrossCz2;
            colCrossTimeCZ2.ToolTip = Resources.CzDetailedDateTimeCrossCz2;

            colTravel.Caption = Resources.CzDetailedTravel;
            colTravel.ToolTip = Resources.CzDetailedTravel;

            colTravelTime.Caption = Resources.FuelCzDrtCommonTimeMotion;
            colTravelTime.Caption = Resources.FuelCzDrtCommonTimeMotion;

            colAverageSpeed.Caption = Resources.CzDetailedMiddleSpeed;
            colAverageSpeed.ToolTip = Resources.CzDetailedMiddleSpeed;

            colMotionFuelRate.Caption = Resources.FuelCzDrtFuelrateInMoving;
            colMotionFuelRate.ToolTip = Resources.FuelCzDrtFuelrateInMoving;

            colMotionAverageFuelRate.Caption = Resources.FuelCzDrtMiddleFuelrateInMoving;
            colMotionAverageFuelRate.ToolTip = Resources.FuelCzDrtMiddleFuelrateInMoving;

            colParkingTimeWithEngineOn.Caption = Resources.FuelCzDrtTimeStopsWithEngineOn;
            colParkingTimeWithEngineOn.ToolTip = Resources.FuelCzDrtTimeStopsWithEngineOn;

            colParkingFuelRate.Caption = Resources.FuelCzDrtFuelrateOnStops;
            colParkingFuelRate.ToolTip = Resources.FuelCzDrtFuelrateOnStops;

            colParkingAverageFuelRate.Caption = Resources.FuelCzDrtMiddleFuelrateOnStops;
            colParkingAverageFuelRate.ToolTip = Resources.FuelCzDrtMiddleFuelrateOnStops;

            colTotalFuelRate.Caption = Resources.FuelCzDrtCommonFuelrate;
            colTotalFuelRate.ToolTip = Resources.FuelCzDrtCommonFuelrate;
        } // Localization
    } // DevFuelCZ_Inkom_DRT

    public class ReportFuelCzDrt
    {
        string namecz1;
        string namecz2;
        DateTime crossTimeCz1;
        DateTime crossTimeCz2;
        double travel;
        double travelTime;
        double averageSpeed;
        double motionFuel;
        double motionAverageFuel = 0;
        double parkingTimeEngineOn;
        double parkingFuel;
        double parkingAverageFuel;
        double totalFuel;

        public ReportFuelCzDrt( string namecz1, string namecz2, DateTime crossTimeCz1,
            DateTime crossTimeCz2, double travel, double travelTime, double averageSpeed,
            double motionFuel, double motionAverageFuel, double parkingTimeEngineOn,
            double parkingFuel, double parkingAverageFuel, double totalFuel )
        {
            this.namecz1 = namecz1;
            this.namecz2 = namecz2;
            this.crossTimeCz1 = crossTimeCz1;
            this.crossTimeCz2 = crossTimeCz2;
            this.travel = travel;
            this.travelTime = travelTime;
            this.averageSpeed = averageSpeed;
            this.motionFuel = motionFuel;
            this.parkingAverageFuel = motionAverageFuel;
            this.parkingTimeEngineOn = parkingTimeEngineOn;
            this.parkingFuel = parkingFuel;
            this.parkingAverageFuel = parkingAverageFuel;
            this.totalFuel = totalFuel;
        } // ReportFuelCzDrt

        public string NameCZ1
        {
            get { return namecz1; }
        }

        public string NameCZ2
        {
            get { return namecz2; }
        }

        public DateTime CrossTimeCZ1
        {
            get { return crossTimeCz1; }
        }

        public DateTime CrossTimeCZ2
        {
            get { return crossTimeCz2; }
        }

        public double Travel
        {
            get { return travel; }
        }

        public double TravelTime
        {
            get { return travelTime; }
        }

        public double AverageSpeed
        {
            get { return averageSpeed; }
        }

        public double MotionFuelRate
        {
            get { return motionFuel; }
        }

        public double MotionAverageFuelRate
        {
            get { return motionAverageFuel; }
        }

        public double ParkingTimeWithEngineOn
        {
            get { return parkingTimeEngineOn; }
        }

        public double ParkingFuelRate
        {
            get { return parkingFuel; }
        }

        public double ParkingAverageFuelRate
        {
            get { return parkingAverageFuel; }
        }

        public double TotalFuelRate
        {
            get { return totalFuel; }
        }
    } // ReportFuelCzDrt
} // ReportsOnCheckZones
