﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using TrackControl.General;
using TrackControl.Zones.Tuning.Properties;

namespace TrackControl.Zones.Tuning
{
    public class MifPointsFinder:IDisposable 
    {
        FileStream _fileStream ;

        StreamReader _streamReader;

        readonly string _stringFind = "REGION";

        List<PointLatLng> _points;

        public List<PointLatLng> Points
        {
            get { return _points; }
            set { _points = value; }
        }
        /// <summary>
        /// количество точек, прописанное в самом файле
        /// </summary>
        int _countPoints;

        string _errorMessage = Resources.ImportMIFPointsError;

        public string ErrorMessage
        {
            get { return _errorMessage; }
            set { _errorMessage = value; }
        }

        public bool FindPoints()
        {
            try
            {
                if (!InitStream()) return false;
                _points = new List<PointLatLng>(); 
                while (!_streamReader.EndOfStream)
                {
                    string line = _streamReader.ReadLine();
                    if (LineContainsNeeded(line))
                    {
                        if (_points.Count > 0)
                        {
                            _errorMessage = Resources.ImportMIFPoints2Zones;
                            return false;
                        }
                        line = _streamReader.ReadLine();
                        if (Int32.TryParse(line, out _countPoints))
                        {
                            for (int i = 0; i < _countPoints; i++)
                            {
                                if (!AddPoint(ref line)) return false; ;
                            }
                            if (_points.Count != _countPoints) return false;
                        }
                        else
                            return false;
                    }

                }
                if (_points.Count>0) 
                    return true;
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        private bool LineContainsNeeded(string line)
        {
            return line !=null && line.Length >= _stringFind.Length && line.ToUpper().Substring(0, _stringFind.Length) == _stringFind;
        }

        private bool AddPoint(ref string line)
        {
            line = _streamReader.ReadLine();
            string[] LotLng = line.Split(' ');
            double Lat = 0;
            double Lng = 0;
            if (double.TryParse(LotLng[0].Replace(".", ","), out Lng) && double.TryParse(LotLng[1].Replace(".", ","), out Lat))
            {
                _points.Add(new PointLatLng(Lat, Lng));
            }
            return true;
        }

        private bool InitStream()
        {
            try
            {
                string fileName = StaticMethods.ShowOpenFileDialog(Resources.ImportMIFStartFindFile, "MIF |*.mif");
                if (fileName.Length == 0) return false;
                _fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read);
                _streamReader = new StreamReader(_fileStream);
                return true;
            }
            catch
            {
                return false;
            }
        }

        #region IDisposable Members

        public void Dispose()
        {
            if (_streamReader != null) _streamReader.Close();
            if (_fileStream != null) _fileStream.Close() ;
        }

        #endregion
    }
}
