using System;
using System.Windows.Forms;
using System.IO;
using System.Text;
using System.Threading;
using System.Collections.Generic;
using TrackControl.General;
using TrackControl.Zones.Tuning.Properties;

namespace TrackControl.Zones.Tuning
{
  public class ImportController : IController
  {
    const char DEFAULT_DELIMITER = '\t'; // TAB

    MainView _view;
    FileStream _mifStream;
    FileStream _midStream;
    StreamReader _mifReader;
    StreamReader _midReader;
    int _columnIndex;
    int _columnCount;
    string _defaultName;
    char _delimiter;
    ZonesGroup _tempGroup;
    ZonesGroup _targetGroup;
    IZonesManager _manager;
    bool _visible;
    bool _isZonesCreated;
    List<IZone> _zones;

    public event VoidHandler HideSelected;
    


    public ImportController(IZonesManager manager)
    {
      if (null == manager)
        throw new ArgumentNullException("manager");

      _manager = manager;
      _targetGroup = _manager.Root;

      _view = new MainView();
      _view.BorderStyle = BorderStyle.FixedSingle;
      _delimiter = DEFAULT_DELIMITER;
      _columnIndex = -1;
      _columnCount = -1;
      _defaultName = Resources.ImportMIFDefaultNameZone;
      _zones = new List<IZone>(); 
    }

    public Control View
    {
      get { return _view; }
    }

    public FileStream MIFStream
    {
      get { return _mifStream; }
      set
      {
        if (null != _mifReader)
          _mifReader.Dispose();
        if (null != _mifStream)
          _mifStream.Dispose();

        _mifStream = value;
        if (null != _mifStream)
          _mifReader = new StreamReader(_mifStream, Encoding.GetEncoding(1251));
        else
          _mifReader = null;
      }
    }

    public FileStream MIDStream
    {
      get { return _midStream; }
      set
      {
        if (null != _midReader)
          _midReader.Dispose();
        if (null != _midStream)
          _midStream.Dispose();

        _midStream = value;
        if (null != _midStream)
          _midReader = new StreamReader(_midStream, Encoding.GetEncoding(1251));
        else
          _mifReader = null;
      }
    }

    public StreamReader MIFReader
    {
      get { return _mifReader; }
    }

    public StreamReader MIDReader
    {
      get { return _midReader; }
    }

    public string DefaultName
    {
      get { return Resources.ImportMIFDefaultNameZone; }
    }

    public IList<IZone> Zones
    {
        get { return _zones; }
    }

    public ZonesGroup TargetGroup
    {
      get { return _targetGroup; }
      set { _targetGroup = value ?? _manager.Root; }
    }

    public IZonesManager Manager
    {
      get { return _manager; }
    }

    public bool Visible
    {
      get { return _visible; }
      set
      {
        _visible = value;
      }
    }

    public bool IsZonesCreated
    {
        get { return _isZonesCreated; }
        set { 
            _isZonesCreated = value;
            if (!_isZonesCreated) _zones.Clear();  
            }
    }

    public void Start()
    {
      _tempGroup = ZonesGroup.CreateRoot();

      SelectMIFState state = new SelectMIFState(this);
      state.Activate();
    }

    public void Reset()
    {
      MIDStream = null;
      MIFStream = null;
      _delimiter = DEFAULT_DELIMITER;
      _columnIndex = -1;
      _columnCount = -1;
      _defaultName = Resources.ImportMIFDefaultNameZone;
    }

    public string GetName()
    {
      if(_columnIndex<0)
        return _defaultName;

      if (_midReader.Peek() < 0)
      {
        _columnIndex = -1;
        return _defaultName;
      }

      string line = _midReader.ReadLine();
      return getNameFromMidLine(line);
    }

    public void SetName(string name)
    {
      _defaultName = name;
      if (String.IsNullOrEmpty(_defaultName))
        _defaultName = Resources.ImportMIFDefaultNameZone;
    }

    public void SetDelimiter(char delimiter)
    {
      _delimiter = delimiter;
    }

    public void SetColumnIndex(int index)
    {
      _columnIndex = index;
    }

    public void SetColumnCount(int count)
    {
      _columnCount = count;
    }

    public void CreateZone(string name, PointLatLng[] points)
    {
      Zone zone = Zone.CreateNew(_tempGroup);
      zone.Name = name;
      zone.Points = points;
      zone.IsGeometryChanged = true;
      _zones.Add(zone); 
    }

    public void SetView(Control control)
    {
      _view.SetView(control);
    }

    public void SetError(FileTrouble trouble)
    {
      WaitCallback callback = delegate(object obj)
      {
        MIFStream = null;
        BadMIFState state = new BadMIFState(this, (FileTrouble)obj);
        state.Activate();
      };
      ThreadPool.QueueUserWorkItem(callback, trouble);
    }

    public void OnHideSelected()
    {
      VoidHandler handler = HideSelected;
      if (null != handler)
        handler();
    }

    string getNameFromMidLine(string line)
    {
      string[] items = line.Split(_delimiter);
      if (items.Length == _columnCount)
        return items[_columnIndex];

      List<string> list = new List<string>(_columnCount);
      string item;
      for (int i = 0; i < (items.Length - 1); i++)
      {
        if (isItItemPart(items[i]))
          items[i + 1] = String.Format("{0},{1}", items[i], items[i + 1]);
        else
          list.Add(items[i]);
      }
      list.Add(items[items.Length - 1]);
      if (list.Count == _columnCount)
      {
        string res = list[_columnIndex].Trim();
        if (res.Length > 2)
        {
          if (res.StartsWith("\"") && res.EndsWith("\""))
          {
            res = res.Remove(0, 1);
            res = res.Remove(res.Length - 1, 1);
          }
          res = res.Replace("\"\"", "\"");
          return res;
        }
      }

      return _defaultName;
    }

    static bool isItItemPart(string line)
    {
      string item = line.Trim();
      if (item == "\"\"") return false;
      bool result = item.Length > 0 && item[0] == '"';
      if (result)
      {
        for (int i = item.Length; i > 0; i--)
        {
          if (item[i - 1] != '"')
            break;
          else
            result = !result;
        }
      }
      return result;
    }
  }
}
