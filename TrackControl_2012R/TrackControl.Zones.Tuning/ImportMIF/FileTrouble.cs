using System;

namespace TrackControl.Zones.Tuning
{
  /// <summary>
  /// ��� �������� � ������ ��� �������
  /// </summary>
  public enum FileTrouble 
  {
    // ����������� ��������
    Unknown = 0,
    
    // �������� � �������
    AccessDenied = 1,

    // ���� �� �������� ������
    Empty = 2,

    // �������� ������ �����
    WrongFormat = 3,

    // ���� �� ������
    NotFound = 4
  }
}
