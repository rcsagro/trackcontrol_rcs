using System;

namespace TrackControl.Zones.Tuning
{
  public interface IState
  {
    void Activate();
  }
}
