using System;

namespace TrackControl.Zones
{
  interface IMifFinder
  {
    void PathSelected(string path);
    void OnHide();
  }
}
