using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

using TrackControl.General;

namespace TrackControl.Zones.Tuning
{
  public interface IController
  {
    string DefaultName { get; }
    IList<IZone> Zones { get; }
    ZonesGroup TargetGroup { get; set; }
    IZonesManager Manager { get; }

    FileStream MIFStream { get; set; }
    FileStream MIDStream { get; set; }
    StreamReader MIFReader { get; }
    StreamReader MIDReader { get; }

    string GetName();
    void SetName(string name);
    void SetDelimiter(char delimiter);
    void SetColumnIndex(int index);
    void SetColumnCount(int count);
    void SetView(Control view);
    void SetError(FileTrouble trouble);
    void CreateZone(string name, PointLatLng[] points);
    void OnHideSelected();
    void Reset();
  }
}
