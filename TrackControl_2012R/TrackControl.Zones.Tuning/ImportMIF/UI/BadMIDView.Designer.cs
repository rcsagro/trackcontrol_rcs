namespace TrackControl.Zones.Tuning
{
  partial class BadMIDView
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this._layout = new DevExpress.XtraLayout.LayoutControl();
      this._nextBtn = new DevExpress.XtraEditors.SimpleButton();
      this._messageLabel = new DevExpress.XtraEditors.LabelControl();
      this._imageLabel = new System.Windows.Forms.Label();
      this._nameBox = new DevExpress.XtraEditors.TextEdit();
      this._backBtn = new DevExpress.XtraEditors.SimpleButton();
      this._textLbl = new DevExpress.XtraEditors.LabelControl();
      this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
      this._imageItem = new DevExpress.XtraLayout.LayoutControlItem();
      this._messageItem = new DevExpress.XtraLayout.LayoutControlItem();
      this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
      this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
      this._nextItem = new DevExpress.XtraLayout.LayoutControlItem();
      this._backItem = new DevExpress.XtraLayout.LayoutControlItem();
      this._textItem = new DevExpress.XtraLayout.LayoutControlItem();
      this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
      this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
      ((System.ComponentModel.ISupportInitialize)(this._layout)).BeginInit();
      this._layout.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this._nameBox.Properties)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this._imageItem)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this._messageItem)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this._nextItem)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this._backItem)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this._textItem)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
      this.SuspendLayout();
      // 
      // _layout
      // 
      this._layout.Controls.Add(this._nextBtn);
      this._layout.Controls.Add(this._messageLabel);
      this._layout.Controls.Add(this._imageLabel);
      this._layout.Controls.Add(this._nameBox);
      this._layout.Controls.Add(this._backBtn);
      this._layout.Controls.Add(this._textLbl);
      this._layout.Dock = System.Windows.Forms.DockStyle.Fill;
      this._layout.Location = new System.Drawing.Point(0, 0);
      this._layout.Margin = new System.Windows.Forms.Padding(0);
      this._layout.Name = "_layout";
      this._layout.Root = this.layoutControlGroup1;
      this._layout.Size = new System.Drawing.Size(292, 164);
      this._layout.TabIndex = 0;
      this._layout.Text = "layoutControl1";
      // 
      // _nextBtn
      // 
      this._nextBtn.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
      this._nextBtn.Appearance.Options.UseFont = true;
      this._nextBtn.Location = new System.Drawing.Point(67, 128);
      this._nextBtn.Name = "_nextBtn";
      this._nextBtn.Size = new System.Drawing.Size(120, 26);
      this._nextBtn.StyleController = this._layout;
      this._nextBtn.TabIndex = 6;
      this._nextBtn.Text = "����������...";
      this._nextBtn.Click += new System.EventHandler(this.nextBtn_Click);
      // 
      // _messageLabel
      // 
      this._messageLabel.AllowHtmlString = true;
      this._messageLabel.Location = new System.Drawing.Point(82, 2);
      this._messageLabel.Margin = new System.Windows.Forms.Padding(0);
      this._messageLabel.Name = "_messageLabel";
      this._messageLabel.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
      this._messageLabel.Size = new System.Drawing.Size(198, 61);
      this._messageLabel.StyleController = this._layout;
      this._messageLabel.TabIndex = 5;
      // 
      // _imageLabel
      // 
      this._imageLabel.Location = new System.Drawing.Point(12, 2);
      this._imageLabel.Margin = new System.Windows.Forms.Padding(0);
      this._imageLabel.Name = "_imageLabel";
      this._imageLabel.Size = new System.Drawing.Size(66, 61);
      this._imageLabel.TabIndex = 4;
      // 
      // _nameBox
      // 
      this._nameBox.EditValue = "";
      this._nameBox.Location = new System.Drawing.Point(91, 105);
      this._nameBox.Name = "_nameBox";
      this._nameBox.Size = new System.Drawing.Size(169, 20);
      this._nameBox.StyleController = this._layout;
      this._nameBox.TabIndex = 9;
      this._nameBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.nameBox_KeyUp);
      // 
      // _backBtn
      // 
      this._backBtn.Location = new System.Drawing.Point(197, 128);
      this._backBtn.Name = "_backBtn";
      this._backBtn.Size = new System.Drawing.Size(70, 26);
      this._backBtn.StyleController = this._layout;
      this._backBtn.TabIndex = 7;
      this._backBtn.Text = "�����";
      this._backBtn.Click += new System.EventHandler(this.backBtn_Click);
      // 
      // _textLbl
      // 
      this._textLbl.AllowHtmlString = true;
      this._textLbl.Appearance.Options.UseTextOptions = true;
      this._textLbl.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Bottom;
      this._textLbl.Location = new System.Drawing.Point(10, 65);
      this._textLbl.Margin = new System.Windows.Forms.Padding(0);
      this._textLbl.Name = "_textLbl";
      this._textLbl.Padding = new System.Windows.Forms.Padding(0, 0, 0, 5);
      this._textLbl.Size = new System.Drawing.Size(272, 40);
      this._textLbl.StyleController = this._layout;
      this._textLbl.TabIndex = 8;
      this._textLbl.Text = "����� ������������ �������� <b>\"�� ���������\".</b> ������� \"�����\", ����� �������" +
          " ������ ����.";
      // 
      // layoutControlGroup1
      // 
      this.layoutControlGroup1.CustomizationFormText = "Root";
      this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
      this.layoutControlGroup1.GroupBordersVisible = false;
      this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this._imageItem,
            this._messageItem,
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this._nextItem,
            this._backItem,
            this._textItem,
            this.layoutControlItem2,
            this.emptySpaceItem3});
      this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
      this.layoutControlGroup1.Name = "Root";
      this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(10, 10, 0, 10);
      this.layoutControlGroup1.Size = new System.Drawing.Size(292, 164);
      this.layoutControlGroup1.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
      this.layoutControlGroup1.Text = "Root";
      this.layoutControlGroup1.TextVisible = false;
      // 
      // _imageItem
      // 
      this._imageItem.Control = this._imageLabel;
      this._imageItem.CustomizationFormText = "_imageItem";
      this._imageItem.Location = new System.Drawing.Point(0, 0);
      this._imageItem.MaxSize = new System.Drawing.Size(70, 65);
      this._imageItem.MinSize = new System.Drawing.Size(70, 35);
      this._imageItem.Name = "_imageItem";
      this._imageItem.Size = new System.Drawing.Size(70, 65);
      this._imageItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
      this._imageItem.Text = "_imageItem";
      this._imageItem.TextSize = new System.Drawing.Size(0, 0);
      this._imageItem.TextToControlDistance = 0;
      this._imageItem.TextVisible = false;
      // 
      // _messageItem
      // 
      this._messageItem.Control = this._messageLabel;
      this._messageItem.CustomizationFormText = "_messageItem";
      this._messageItem.Location = new System.Drawing.Point(70, 0);
      this._messageItem.MaxSize = new System.Drawing.Size(0, 65);
      this._messageItem.MinSize = new System.Drawing.Size(67, 35);
      this._messageItem.Name = "_messageItem";
      this._messageItem.Size = new System.Drawing.Size(202, 65);
      this._messageItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
      this._messageItem.Text = "_messageItem";
      this._messageItem.TextSize = new System.Drawing.Size(0, 0);
      this._messageItem.TextToControlDistance = 0;
      this._messageItem.TextVisible = false;
      // 
      // emptySpaceItem1
      // 
      this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
      this.emptySpaceItem1.Location = new System.Drawing.Point(0, 125);
      this.emptySpaceItem1.MinSize = new System.Drawing.Size(10, 2);
      this.emptySpaceItem1.Name = "emptySpaceItem1";
      this.emptySpaceItem1.Size = new System.Drawing.Size(272, 3);
      this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
      this.emptySpaceItem1.Text = "emptySpaceItem1";
      this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
      // 
      // emptySpaceItem2
      // 
      this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
      this.emptySpaceItem2.Location = new System.Drawing.Point(0, 128);
      this.emptySpaceItem2.Name = "emptySpaceItem2";
      this.emptySpaceItem2.Size = new System.Drawing.Size(57, 26);
      this.emptySpaceItem2.Text = "emptySpaceItem2";
      this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
      // 
      // _nextItem
      // 
      this._nextItem.Control = this._nextBtn;
      this._nextItem.CustomizationFormText = "layoutControlItem3";
      this._nextItem.Location = new System.Drawing.Point(57, 128);
      this._nextItem.MaxSize = new System.Drawing.Size(125, 26);
      this._nextItem.MinSize = new System.Drawing.Size(125, 26);
      this._nextItem.Name = "_nextItem";
      this._nextItem.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 5, 0, 0);
      this._nextItem.Size = new System.Drawing.Size(125, 26);
      this._nextItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
      this._nextItem.Text = "_nextItem";
      this._nextItem.TextSize = new System.Drawing.Size(0, 0);
      this._nextItem.TextToControlDistance = 0;
      this._nextItem.TextVisible = false;
      // 
      // _backItem
      // 
      this._backItem.Control = this._backBtn;
      this._backItem.CustomizationFormText = "layoutControlItem4";
      this._backItem.Location = new System.Drawing.Point(182, 128);
      this._backItem.MaxSize = new System.Drawing.Size(90, 26);
      this._backItem.MinSize = new System.Drawing.Size(90, 26);
      this._backItem.Name = "_backItem";
      this._backItem.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 15, 0, 0);
      this._backItem.Size = new System.Drawing.Size(90, 26);
      this._backItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
      this._backItem.Text = "_backItem";
      this._backItem.TextSize = new System.Drawing.Size(0, 0);
      this._backItem.TextToControlDistance = 0;
      this._backItem.TextVisible = false;
      // 
      // _textItem
      // 
      this._textItem.Control = this._textLbl;
      this._textItem.CustomizationFormText = "_textTopItem";
      this._textItem.Location = new System.Drawing.Point(0, 65);
      this._textItem.MaxSize = new System.Drawing.Size(0, 40);
      this._textItem.MinSize = new System.Drawing.Size(67, 40);
      this._textItem.Name = "_textItem";
      this._textItem.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
      this._textItem.Size = new System.Drawing.Size(272, 40);
      this._textItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
      this._textItem.Text = "_textItem";
      this._textItem.TextSize = new System.Drawing.Size(0, 0);
      this._textItem.TextToControlDistance = 0;
      this._textItem.TextVisible = false;
      // 
      // layoutControlItem2
      // 
      this.layoutControlItem2.Control = this._nameBox;
      this.layoutControlItem2.CustomizationFormText = "�� ���������:";
      this.layoutControlItem2.Location = new System.Drawing.Point(0, 105);
      this.layoutControlItem2.MaxSize = new System.Drawing.Size(250, 20);
      this.layoutControlItem2.MinSize = new System.Drawing.Size(250, 20);
      this.layoutControlItem2.Name = "layoutControlItem2";
      this.layoutControlItem2.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
      this.layoutControlItem2.Size = new System.Drawing.Size(250, 20);
      this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
      this.layoutControlItem2.Text = "�� ���������:";
      this.layoutControlItem2.TextSize = new System.Drawing.Size(77, 13);
      // 
      // emptySpaceItem3
      // 
      this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
      this.emptySpaceItem3.Location = new System.Drawing.Point(250, 105);
      this.emptySpaceItem3.Name = "emptySpaceItem3";
      this.emptySpaceItem3.Size = new System.Drawing.Size(22, 20);
      this.emptySpaceItem3.Text = "emptySpaceItem3";
      this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
      // 
      // BadMIDView
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this._layout);
      this.Name = "BadMIDView";
      this.Size = new System.Drawing.Size(292, 164);
      ((System.ComponentModel.ISupportInitialize)(this._layout)).EndInit();
      this._layout.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this._nameBox.Properties)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this._imageItem)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this._messageItem)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this._nextItem)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this._backItem)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this._textItem)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private DevExpress.XtraLayout.LayoutControl _layout;
    private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
    private DevExpress.XtraEditors.SimpleButton _backBtn;
    private DevExpress.XtraEditors.SimpleButton _nextBtn;
    private DevExpress.XtraEditors.LabelControl _messageLabel;
    private System.Windows.Forms.Label _imageLabel;
    private DevExpress.XtraLayout.LayoutControlItem _imageItem;
    private DevExpress.XtraLayout.LayoutControlItem _messageItem;
    private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
    private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
    private DevExpress.XtraLayout.LayoutControlItem _nextItem;
    private DevExpress.XtraLayout.LayoutControlItem _backItem;
    private DevExpress.XtraEditors.TextEdit _nameBox;
    private DevExpress.XtraEditors.LabelControl _textLbl;
    private DevExpress.XtraLayout.LayoutControlItem _textItem;
    private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
    private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
  }
}
