namespace TrackControl.Zones.Tuning
{
  partial class AdjustMIDView
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this._layout = new DevExpress.XtraLayout.LayoutControl();
        this._list = new DevExpress.XtraEditors.ListBoxControl();
        this._cancelBtn = new DevExpress.XtraEditors.SimpleButton();
        this._nextBtn = new DevExpress.XtraEditors.SimpleButton();
        this._messageLbl = new DevExpress.XtraEditors.LabelControl();
        this._root = new DevExpress.XtraLayout.LayoutControlGroup();
        this._messageItem = new DevExpress.XtraLayout.LayoutControlItem();
        this._nextItem = new DevExpress.XtraLayout.LayoutControlItem();
        this._cancelItem = new DevExpress.XtraLayout.LayoutControlItem();
        this._empty = new DevExpress.XtraLayout.EmptySpaceItem();
        this._listItem = new DevExpress.XtraLayout.LayoutControlItem();
        ((System.ComponentModel.ISupportInitialize)(this._layout)).BeginInit();
        this._layout.SuspendLayout();
        ((System.ComponentModel.ISupportInitialize)(this._list)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this._root)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this._messageItem)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this._nextItem)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this._cancelItem)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this._empty)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this._listItem)).BeginInit();
        this.SuspendLayout();
        // 
        // _layout
        // 
        this._layout.Controls.Add(this._list);
        this._layout.Controls.Add(this._cancelBtn);
        this._layout.Controls.Add(this._nextBtn);
        this._layout.Controls.Add(this._messageLbl);
        this._layout.Dock = System.Windows.Forms.DockStyle.Fill;
        this._layout.Location = new System.Drawing.Point(0, 0);
        this._layout.Margin = new System.Windows.Forms.Padding(0);
        this._layout.Name = "_layout";
        this._layout.Root = this._root;
        this._layout.Size = new System.Drawing.Size(282, 185);
        this._layout.TabIndex = 0;
        this._layout.Text = "layoutControl1";
        // 
        // _list
        // 
        this._list.Location = new System.Drawing.Point(12, 37);
        this._list.Name = "_list";
        this._list.Size = new System.Drawing.Size(258, 110);
        this._list.StyleController = this._layout;
        this._list.TabIndex = 8;
        // 
        // _cancelBtn
        // 
        this._cancelBtn.Location = new System.Drawing.Point(184, 151);
        this._cancelBtn.Name = "_cancelBtn";
        this._cancelBtn.Size = new System.Drawing.Size(73, 22);
        this._cancelBtn.StyleController = this._layout;
        this._cancelBtn.TabIndex = 7;
        this._cancelBtn.Text = "�����";
        this._cancelBtn.Click += new System.EventHandler(this.cancelBtn_Click);
        // 
        // _nextBtn
        // 
        this._nextBtn.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
        this._nextBtn.Appearance.Options.UseFont = true;
        this._nextBtn.Location = new System.Drawing.Point(49, 151);
        this._nextBtn.Name = "_nextBtn";
        this._nextBtn.Size = new System.Drawing.Size(123, 22);
        this._nextBtn.StyleController = this._layout;
        this._nextBtn.TabIndex = 6;
        this._nextBtn.Text = "����������...";
        this._nextBtn.Click += new System.EventHandler(this.nextBtn_Click);
        // 
        // _messageLbl
        // 
        this._messageLbl.AllowHtmlString = true;
        this._messageLbl.Location = new System.Drawing.Point(10, 0);
        this._messageLbl.Name = "_messageLbl";
        this._messageLbl.Size = new System.Drawing.Size(262, 35);
        this._messageLbl.StyleController = this._layout;
        this._messageLbl.TabIndex = 4;
        this._messageLbl.Text = "������� �������, �������� �� ������� ����� ������������ ��� �������� ���.";
        // 
        // _root
        // 
        this._root.CustomizationFormText = "layoutControlGroup1";
        this._root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
        this._root.GroupBordersVisible = false;
        this._root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this._messageItem,
            this._nextItem,
            this._cancelItem,
            this._empty,
            this._listItem});
        this._root.Location = new System.Drawing.Point(0, 0);
        this._root.Name = "_root";
        this._root.Padding = new DevExpress.XtraLayout.Utils.Padding(10, 10, 0, 10);
        this._root.Size = new System.Drawing.Size(282, 185);
        this._root.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
        this._root.Text = "_root";
        this._root.TextVisible = false;
        // 
        // _messageItem
        // 
        this._messageItem.Control = this._messageLbl;
        this._messageItem.CustomizationFormText = "layoutControlItem1";
        this._messageItem.Location = new System.Drawing.Point(0, 0);
        this._messageItem.MaxSize = new System.Drawing.Size(0, 35);
        this._messageItem.MinSize = new System.Drawing.Size(67, 35);
        this._messageItem.Name = "_messageItem";
        this._messageItem.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
        this._messageItem.Size = new System.Drawing.Size(262, 35);
        this._messageItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
        this._messageItem.Text = "_messageItem";
        this._messageItem.TextSize = new System.Drawing.Size(0, 0);
        this._messageItem.TextToControlDistance = 0;
        this._messageItem.TextVisible = false;
        // 
        // _nextItem
        // 
        this._nextItem.Control = this._nextBtn;
        this._nextItem.CustomizationFormText = "_nextItem";
        this._nextItem.Location = new System.Drawing.Point(37, 149);
        this._nextItem.MaxSize = new System.Drawing.Size(135, 26);
        this._nextItem.MinSize = new System.Drawing.Size(135, 26);
        this._nextItem.Name = "_nextItem";
        this._nextItem.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 10, 2, 2);
        this._nextItem.Size = new System.Drawing.Size(135, 26);
        this._nextItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
        this._nextItem.Text = "_nextItem";
        this._nextItem.TextSize = new System.Drawing.Size(0, 0);
        this._nextItem.TextToControlDistance = 0;
        this._nextItem.TextVisible = false;
        // 
        // _cancelItem
        // 
        this._cancelItem.Control = this._cancelBtn;
        this._cancelItem.CustomizationFormText = "_cancelItem";
        this._cancelItem.Location = new System.Drawing.Point(172, 149);
        this._cancelItem.MaxSize = new System.Drawing.Size(90, 26);
        this._cancelItem.MinSize = new System.Drawing.Size(90, 26);
        this._cancelItem.Name = "_cancelItem";
        this._cancelItem.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 15, 2, 2);
        this._cancelItem.Size = new System.Drawing.Size(90, 26);
        this._cancelItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
        this._cancelItem.Text = "_cancelItem";
        this._cancelItem.TextSize = new System.Drawing.Size(0, 0);
        this._cancelItem.TextToControlDistance = 0;
        this._cancelItem.TextVisible = false;
        // 
        // _empty
        // 
        this._empty.CustomizationFormText = "_empty";
        this._empty.Location = new System.Drawing.Point(0, 149);
        this._empty.MinSize = new System.Drawing.Size(10, 10);
        this._empty.Name = "_empty";
        this._empty.Size = new System.Drawing.Size(37, 26);
        this._empty.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
        this._empty.Text = "_empty";
        this._empty.TextSize = new System.Drawing.Size(0, 0);
        // 
        // _listItem
        // 
        this._listItem.Control = this._list;
        this._listItem.CustomizationFormText = "_listItem";
        this._listItem.Location = new System.Drawing.Point(0, 35);
        this._listItem.Name = "_listItem";
        this._listItem.Size = new System.Drawing.Size(262, 114);
        this._listItem.Text = "_listItem";
        this._listItem.TextSize = new System.Drawing.Size(0, 0);
        this._listItem.TextToControlDistance = 0;
        this._listItem.TextVisible = false;
        // 
        // AdjustMIDView
        // 
        this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        this.Controls.Add(this._layout);
        this.Name = "AdjustMIDView";
        this.Size = new System.Drawing.Size(282, 185);
        ((System.ComponentModel.ISupportInitialize)(this._layout)).EndInit();
        this._layout.ResumeLayout(false);
        ((System.ComponentModel.ISupportInitialize)(this._list)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this._root)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this._messageItem)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this._nextItem)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this._cancelItem)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this._empty)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this._listItem)).EndInit();
        this.ResumeLayout(false);

    }

    #endregion

    private DevExpress.XtraLayout.LayoutControl _layout;
    private DevExpress.XtraLayout.LayoutControlGroup _root;
    private DevExpress.XtraEditors.SimpleButton _cancelBtn;
    private DevExpress.XtraEditors.SimpleButton _nextBtn;
    private DevExpress.XtraEditors.LabelControl _messageLbl;
    private DevExpress.XtraLayout.LayoutControlItem _messageItem;
    private DevExpress.XtraLayout.LayoutControlItem _nextItem;
    private DevExpress.XtraLayout.LayoutControlItem _cancelItem;
    private DevExpress.XtraLayout.EmptySpaceItem _empty;
    private DevExpress.XtraEditors.ListBoxControl _list;
    private DevExpress.XtraLayout.LayoutControlItem _listItem;
  }
}
