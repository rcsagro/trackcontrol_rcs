using System;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using TrackControl.General;
using TrackControl.Zones.Tuning.Properties;

namespace TrackControl.Zones.Tuning
{
    public partial class StartView : XtraUserControl
    {
        private IMifFinder _state;

        internal StartView(IMifFinder state, Image image, string title, string message)
        {
            if (null == state)
                throw new ArgumentNullException("state");
            _state = state;

            InitializeComponent();
            Init();

            _startBtn.Image = Shared.FolderOpen;
            _cancelBtn.Image = Shared.Up;
            _imageLbl.Image = image;
            _titleLbl.Text = title;
            _messageLbl.Text = message;
        }

        private void Init()
        {
            this._layout.Text = "layoutControl1";
            this._root.CustomizationFormText = "Root";
            this._root.Text = "_root";
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this._cancelBtn.Text = Resources.ImportMIFStartHide;
            this._startBtn.Text = Resources.ImportMIFStartFindFile;
            this._imageItem.CustomizationFormText = "_imageItem";
            this._imageItem.Text = "_imageItem";
            this._titleItem.CustomizationFormText = "_messageItem";
            this._titleItem.Text = "_titleItem";
            this._startItem.CustomizationFormText = "_startItem";
            this._startItem.Text = "_startItem";
            this._cancelItem.CustomizationFormText = "_cancelItem";
            this._cancelItem.Text = "_cancelItem";
            this._messageItem.CustomizationFormText = "_textItem";
            this._messageItem.Text = "_messageItem";

        }

        private void startBtn_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog dlg = new OpenFileDialog())
            {
                dlg.Filter = Resources.ImportMIFStartMaskFileSelect + " (*.MIF)|*.MIF";
                if (DialogResult.OK == dlg.ShowDialog())
                {
                    Parent = null;
                    _state.PathSelected(dlg.FileName);
                    Dispose();
                }
            }
        }

        private void cancelBtn_Click(object sender, EventArgs e)
        {
            _state.OnHide();
        }
    }
}
