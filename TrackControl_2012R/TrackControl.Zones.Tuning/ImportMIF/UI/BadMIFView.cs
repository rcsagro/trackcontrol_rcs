using System;
using DevExpress.XtraEditors;
using TrackControl.General;
using TrackControl.Zones.Tuning.Properties;

namespace TrackControl.Zones.Tuning
{
    public partial class BadMIFView : XtraUserControl
    {
        private BadMIFState _state;

        public BadMIFView(BadMIFState state)
        {
            if (null == state)
                throw new ArgumentNullException("state");

            _state = state;
            InitializeComponent();
            Init();

            _oneMoreBtn.Image = Shared.Cancel;
            _cancelBtn.Image = Shared.Cross;
            _imageLabel.Image = _state.Image;
            _messageLabel.Text = _state.Message;
        }

        private void Init()
        {
            this._cancelBtn.Text = Resources.UI_TuningCancel;
            this._oneMoreBtn.Text = Resources.ImportMIFBack;
            this.layoutControlGroup1.CustomizationFormText = Resources.ImportMIFRoot;
            this.layoutControlGroup1.Text = Resources.ImportMIFRoot;
        }

        private void oneMoreBtn_Click(object sender, EventArgs e)
        {
            releaseView();
            _state.StepBack();
        }

        private void cancelBtn_Click(object sender, EventArgs e)
        {
            releaseView();
            _state.onCancel();
        }

        private void releaseView()
        {
            Parent = null;
            Dispose();
        }
    }
}
