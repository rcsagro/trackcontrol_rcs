namespace TrackControl.Zones.Tuning
{
  partial class CompletedView
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this._layout = new DevExpress.XtraLayout.LayoutControl();
        this._cancelBtn = new DevExpress.XtraEditors.SimpleButton();
        this._finishBtn = new DevExpress.XtraEditors.SimpleButton();
        this._groupBox = new DevExpress.XtraEditors.ComboBoxEdit();
        this._textLbl = new DevExpress.XtraEditors.LabelControl();
        this._titleLbl = new DevExpress.XtraEditors.LabelControl();
        this._imageLbl = new System.Windows.Forms.Label();
        this._root = new DevExpress.XtraLayout.LayoutControlGroup();
        this._imageItem = new DevExpress.XtraLayout.LayoutControlItem();
        this._titleItem = new DevExpress.XtraLayout.LayoutControlItem();
        this._textItem = new DevExpress.XtraLayout.LayoutControlItem();
        this._groupItem = new DevExpress.XtraLayout.LayoutControlItem();
        this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
        this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
        this._finishItem = new DevExpress.XtraLayout.LayoutControlItem();
        this._cancelItem = new DevExpress.XtraLayout.LayoutControlItem();
        this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
        ((System.ComponentModel.ISupportInitialize)(this._layout)).BeginInit();
        this._layout.SuspendLayout();
        ((System.ComponentModel.ISupportInitialize)(this._groupBox.Properties)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this._root)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this._imageItem)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this._titleItem)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this._textItem)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this._groupItem)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this._finishItem)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this._cancelItem)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
        this.SuspendLayout();
        // 
        // _layout
        // 
        this._layout.Controls.Add(this._cancelBtn);
        this._layout.Controls.Add(this._finishBtn);
        this._layout.Controls.Add(this._groupBox);
        this._layout.Controls.Add(this._textLbl);
        this._layout.Controls.Add(this._titleLbl);
        this._layout.Controls.Add(this._imageLbl);
        this._layout.Dock = System.Windows.Forms.DockStyle.Fill;
        this._layout.Location = new System.Drawing.Point(0, 0);
        this._layout.Margin = new System.Windows.Forms.Padding(0);
        this._layout.Name = "_layout";
        this._layout.Root = this._root;
        this._layout.Size = new System.Drawing.Size(281, 200);
        this._layout.TabIndex = 0;
        this._layout.Text = "layoutControl1";
        // 
        // _cancelBtn
        // 
        this._cancelBtn.Location = new System.Drawing.Point(186, 166);
        this._cancelBtn.Margin = new System.Windows.Forms.Padding(0);
        this._cancelBtn.Name = "_cancelBtn";
        this._cancelBtn.Size = new System.Drawing.Size(70, 22);
        this._cancelBtn.StyleController = this._layout;
        this._cancelBtn.TabIndex = 9;
        this._cancelBtn.Text = "������";
        this._cancelBtn.Click += new System.EventHandler(this.cancelBtn_Click);
        // 
        // _finishBtn
        // 
        this._finishBtn.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
        this._finishBtn.Appearance.Options.UseFont = true;
        this._finishBtn.Location = new System.Drawing.Point(43, 166);
        this._finishBtn.Name = "_finishBtn";
        this._finishBtn.Size = new System.Drawing.Size(133, 22);
        this._finishBtn.StyleController = this._layout;
        this._finishBtn.TabIndex = 8;
        this._finishBtn.Text = "��������� � ��...";
        this._finishBtn.Click += new System.EventHandler(this.finishBtn_Click);
        // 
        // _groupBox
        // 
        this._groupBox.Location = new System.Drawing.Point(62, 113);
        this._groupBox.Name = "_groupBox";
        this._groupBox.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
        this._groupBox.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
        this._groupBox.Size = new System.Drawing.Size(146, 20);
        this._groupBox.StyleController = this._layout;
        this._groupBox.TabIndex = 7;
        // 
        // _textLbl
        // 
        this._textLbl.AllowHtmlString = true;
        this._textLbl.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
        this._textLbl.Location = new System.Drawing.Point(10, 64);
        this._textLbl.Margin = new System.Windows.Forms.Padding(0);
        this._textLbl.Name = "_textLbl";
        this._textLbl.Size = new System.Drawing.Size(261, 47);
        this._textLbl.StyleController = this._layout;
        this._textLbl.TabIndex = 6;
        this._textLbl.Text = "�������� <b>������,</b> ���� ������� ��������� ��������� ����. ����� ���������  �" +
            " ���� ������.";
        // 
        // _titleLbl
        // 
        this._titleLbl.AllowHtmlString = true;
        this._titleLbl.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
        this._titleLbl.Appearance.Options.UseFont = true;
        this._titleLbl.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
        this._titleLbl.Location = new System.Drawing.Point(84, 0);
        this._titleLbl.Margin = new System.Windows.Forms.Padding(0);
        this._titleLbl.Name = "_titleLbl";
        this._titleLbl.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
        this._titleLbl.Size = new System.Drawing.Size(187, 64);
        this._titleLbl.StyleController = this._layout;
        this._titleLbl.TabIndex = 5;
        // 
        // _imageLbl
        // 
        this._imageLbl.Location = new System.Drawing.Point(10, 0);
        this._imageLbl.Margin = new System.Windows.Forms.Padding(0);
        this._imageLbl.Name = "_imageLbl";
        this._imageLbl.Size = new System.Drawing.Size(74, 64);
        this._imageLbl.TabIndex = 4;
        // 
        // _root
        // 
        this._root.CustomizationFormText = "_root";
        this._root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
        this._root.GroupBordersVisible = false;
        this._root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this._imageItem,
            this._titleItem,
            this._textItem,
            this._groupItem,
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this._finishItem,
            this._cancelItem,
            this.emptySpaceItem3});
        this._root.Location = new System.Drawing.Point(0, 0);
        this._root.Name = "_root";
        this._root.Padding = new DevExpress.XtraLayout.Utils.Padding(10, 10, 0, 10);
        this._root.Size = new System.Drawing.Size(281, 200);
        this._root.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
        this._root.Text = "_root";
        this._root.TextVisible = false;
        // 
        // _imageItem
        // 
        this._imageItem.Control = this._imageLbl;
        this._imageItem.CustomizationFormText = "_imageItem";
        this._imageItem.Location = new System.Drawing.Point(0, 0);
        this._imageItem.MaxSize = new System.Drawing.Size(74, 64);
        this._imageItem.MinSize = new System.Drawing.Size(74, 64);
        this._imageItem.Name = "_imageItem";
        this._imageItem.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
        this._imageItem.Size = new System.Drawing.Size(74, 64);
        this._imageItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
        this._imageItem.Text = "_imageItem";
        this._imageItem.TextSize = new System.Drawing.Size(0, 0);
        this._imageItem.TextToControlDistance = 0;
        this._imageItem.TextVisible = false;
        // 
        // _titleItem
        // 
        this._titleItem.Control = this._titleLbl;
        this._titleItem.CustomizationFormText = "_titleItem";
        this._titleItem.Location = new System.Drawing.Point(74, 0);
        this._titleItem.MaxSize = new System.Drawing.Size(0, 64);
        this._titleItem.MinSize = new System.Drawing.Size(10, 64);
        this._titleItem.Name = "_titleItem";
        this._titleItem.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
        this._titleItem.Size = new System.Drawing.Size(187, 64);
        this._titleItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
        this._titleItem.Text = "_titleItem";
        this._titleItem.TextSize = new System.Drawing.Size(0, 0);
        this._titleItem.TextToControlDistance = 0;
        this._titleItem.TextVisible = false;
        // 
        // _textItem
        // 
        this._textItem.Control = this._textLbl;
        this._textItem.CustomizationFormText = "_textItem";
        this._textItem.Location = new System.Drawing.Point(0, 64);
        this._textItem.MinSize = new System.Drawing.Size(67, 17);
        this._textItem.Name = "_textItem";
        this._textItem.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
        this._textItem.Size = new System.Drawing.Size(261, 47);
        this._textItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
        this._textItem.Text = "_textItem";
        this._textItem.TextSize = new System.Drawing.Size(0, 0);
        this._textItem.TextToControlDistance = 0;
        this._textItem.TextVisible = false;
        // 
        // _groupItem
        // 
        this._groupItem.Control = this._groupBox;
        this._groupItem.CustomizationFormText = "������";
        this._groupItem.Location = new System.Drawing.Point(0, 111);
        this._groupItem.MaxSize = new System.Drawing.Size(200, 24);
        this._groupItem.MinSize = new System.Drawing.Size(200, 24);
        this._groupItem.Name = "_groupItem";
        this._groupItem.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 2, 2, 2);
        this._groupItem.Size = new System.Drawing.Size(200, 24);
        this._groupItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
        this._groupItem.Text = "������: ";
        this._groupItem.TextSize = new System.Drawing.Size(43, 13);
        // 
        // emptySpaceItem1
        // 
        this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
        this.emptySpaceItem1.Location = new System.Drawing.Point(0, 135);
        this.emptySpaceItem1.Name = "emptySpaceItem1";
        this.emptySpaceItem1.Size = new System.Drawing.Size(261, 29);
        this.emptySpaceItem1.Text = "emptySpaceItem1";
        this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
        // 
        // emptySpaceItem2
        // 
        this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
        this.emptySpaceItem2.Location = new System.Drawing.Point(0, 164);
        this.emptySpaceItem2.Name = "emptySpaceItem2";
        this.emptySpaceItem2.Size = new System.Drawing.Size(31, 26);
        this.emptySpaceItem2.Text = "emptySpaceItem2";
        this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
        // 
        // _finishItem
        // 
        this._finishItem.Control = this._finishBtn;
        this._finishItem.CustomizationFormText = "layoutControlItem5";
        this._finishItem.Location = new System.Drawing.Point(31, 164);
        this._finishItem.MaxSize = new System.Drawing.Size(140, 26);
        this._finishItem.MinSize = new System.Drawing.Size(140, 26);
        this._finishItem.Name = "_finishItem";
        this._finishItem.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 5, 2, 2);
        this._finishItem.Size = new System.Drawing.Size(140, 26);
        this._finishItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
        this._finishItem.Text = "_finishItem";
        this._finishItem.TextSize = new System.Drawing.Size(0, 0);
        this._finishItem.TextToControlDistance = 0;
        this._finishItem.TextVisible = false;
        // 
        // _cancelItem
        // 
        this._cancelItem.Control = this._cancelBtn;
        this._cancelItem.CustomizationFormText = "layoutControlItem6";
        this._cancelItem.Location = new System.Drawing.Point(171, 164);
        this._cancelItem.MaxSize = new System.Drawing.Size(90, 26);
        this._cancelItem.MinSize = new System.Drawing.Size(90, 26);
        this._cancelItem.Name = "_cancelItem";
        this._cancelItem.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 15, 2, 2);
        this._cancelItem.Size = new System.Drawing.Size(90, 26);
        this._cancelItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
        this._cancelItem.Text = "_cancelItem";
        this._cancelItem.TextSize = new System.Drawing.Size(0, 0);
        this._cancelItem.TextToControlDistance = 0;
        this._cancelItem.TextVisible = false;
        // 
        // emptySpaceItem3
        // 
        this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
        this.emptySpaceItem3.Location = new System.Drawing.Point(200, 111);
        this.emptySpaceItem3.Name = "emptySpaceItem3";
        this.emptySpaceItem3.Size = new System.Drawing.Size(61, 24);
        this.emptySpaceItem3.Text = "emptySpaceItem3";
        this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
        // 
        // CompletedView
        // 
        this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        this.Controls.Add(this._layout);
        this.DoubleBuffered = true;
        this.Name = "CompletedView";
        this.Size = new System.Drawing.Size(281, 200);
        ((System.ComponentModel.ISupportInitialize)(this._layout)).EndInit();
        this._layout.ResumeLayout(false);
        ((System.ComponentModel.ISupportInitialize)(this._groupBox.Properties)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this._root)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this._imageItem)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this._titleItem)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this._textItem)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this._groupItem)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this._finishItem)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this._cancelItem)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
        this.ResumeLayout(false);

    }

    #endregion

    private DevExpress.XtraLayout.LayoutControl _layout;
    private DevExpress.XtraLayout.LayoutControlGroup _root;
    private DevExpress.XtraEditors.SimpleButton _cancelBtn;
    private DevExpress.XtraEditors.SimpleButton _finishBtn;
    private DevExpress.XtraEditors.ComboBoxEdit _groupBox;
    private DevExpress.XtraEditors.LabelControl _textLbl;
    private DevExpress.XtraEditors.LabelControl _titleLbl;
    private System.Windows.Forms.Label _imageLbl;
    private DevExpress.XtraLayout.LayoutControlItem _imageItem;
    private DevExpress.XtraLayout.LayoutControlItem _titleItem;
    private DevExpress.XtraLayout.LayoutControlItem _textItem;
    private DevExpress.XtraLayout.LayoutControlItem _groupItem;
    private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
    private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
    private DevExpress.XtraLayout.LayoutControlItem _finishItem;
    private DevExpress.XtraLayout.LayoutControlItem _cancelItem;
    private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
  }
}
