using DevExpress.XtraEditors;
using System;
using System.Windows.Forms;
using TrackControl.General;
using TrackControl.Zones.Tuning.Properties;

namespace TrackControl.Zones.Tuning
{
  public partial class MainView : XtraUserControl
  {
    public MainView()
    {
      InitializeComponent();
      Init();
      _panel.CaptionImage = Shared.ZoneMIF;
    }

    private void Init()
    {
      this._panel.Text = Resources.ImportMIFMainImportFromMIF;
    }

    public void SetView(Control view)
    {
      MethodInvoker m = delegate
      {
        foreach (Control cnt in _panel.Controls)
          cnt.Parent = null;

        view.Dock = DockStyle.Fill;
        view.Parent = _panel;
      };

      if (InvokeRequired)
        BeginInvoke(m);
      else
        m();
    }
  }
}
