using System;
using System.Drawing;
using DevExpress.XtraEditors;
using TrackControl.Zones.Tuning.Properties;

namespace TrackControl.Zones.Tuning
{
  public partial class ProgressView : XtraUserControl
  {
    public ProgressView(Image image, string message, int max)
    {
      InitializeComponent();
      Init();

      _imageLbl.Image = image;
      _messageLbl.Text = message;
      _progress.Properties.Maximum = max;
    }

    private void Init()
    {
    }

    public void SetProgress(int current)
    {
      _progress.EditValue = current;
    }

    public void Release()
    {
      Parent = null;
      Dispose();
    }
  }
}
