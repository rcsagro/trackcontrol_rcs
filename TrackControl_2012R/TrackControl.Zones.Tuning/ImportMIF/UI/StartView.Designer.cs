namespace TrackControl.Zones.Tuning
{
  partial class StartView
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
            this._layout = new DevExpress.XtraLayout.LayoutControl();
            this._messageLbl = new DevExpress.XtraEditors.LabelControl();
            this._cancelBtn = new DevExpress.XtraEditors.SimpleButton();
            this._startBtn = new DevExpress.XtraEditors.SimpleButton();
            this._imageLbl = new System.Windows.Forms.Label();
            this._titleLbl = new DevExpress.XtraEditors.LabelControl();
            this._root = new DevExpress.XtraLayout.LayoutControlGroup();
            this._imageItem = new DevExpress.XtraLayout.LayoutControlItem();
            this._titleItem = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this._startItem = new DevExpress.XtraLayout.LayoutControlItem();
            this._cancelItem = new DevExpress.XtraLayout.LayoutControlItem();
            this._messageItem = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this._layout)).BeginInit();
            this._layout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._imageItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._titleItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._startItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._cancelItem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._messageItem)).BeginInit();
            this.SuspendLayout();
            // 
            // _layout
            // 
            this._layout.Controls.Add(this._messageLbl);
            this._layout.Controls.Add(this._cancelBtn);
            this._layout.Controls.Add(this._startBtn);
            this._layout.Controls.Add(this._imageLbl);
            this._layout.Controls.Add(this._titleLbl);
            this._layout.Dock = System.Windows.Forms.DockStyle.Fill;
            this._layout.Location = new System.Drawing.Point(0, 0);
            this._layout.Margin = new System.Windows.Forms.Padding(0);
            this._layout.Name = "_layout";
            this._layout.Root = this._root;
            this._layout.Size = new System.Drawing.Size(321, 179);
            this._layout.TabIndex = 0;
            this._layout.Text = "layoutControl1";
            // 
            // _messageLbl
            // 
            this._messageLbl.AllowHtmlString = true;
            this._messageLbl.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this._messageLbl.Location = new System.Drawing.Point(10, 64);
            this._messageLbl.Margin = new System.Windows.Forms.Padding(0);
            this._messageLbl.Name = "_messageLbl";
            this._messageLbl.Size = new System.Drawing.Size(301, 50);
            this._messageLbl.StyleController = this._layout;
            this._messageLbl.TabIndex = 8;
            // 
            // _cancelBtn
            // 
            this._cancelBtn.Location = new System.Drawing.Point(216, 145);
            this._cancelBtn.Name = "_cancelBtn";
            this._cancelBtn.Size = new System.Drawing.Size(75, 22);
            this._cancelBtn.StyleController = this._layout;
            this._cancelBtn.TabIndex = 7;
            this._cancelBtn.Text = "������";
            this._cancelBtn.Click += new System.EventHandler(this.cancelBtn_Click);
            // 
            // _startBtn
            // 
            this._startBtn.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this._startBtn.Appearance.Options.UseFont = true;
            this._startBtn.Location = new System.Drawing.Point(58, 145);
            this._startBtn.Name = "_startBtn";
            this._startBtn.Size = new System.Drawing.Size(143, 22);
            this._startBtn.StyleController = this._layout;
            this._startBtn.TabIndex = 6;
            this._startBtn.Text = "����� MIF-����...";
            this._startBtn.Click += new System.EventHandler(this.startBtn_Click);
            // 
            // _imageLbl
            // 
            this._imageLbl.Location = new System.Drawing.Point(10, 0);
            this._imageLbl.Name = "_imageLbl";
            this._imageLbl.Size = new System.Drawing.Size(75, 64);
            this._imageLbl.TabIndex = 5;
            // 
            // _titleLbl
            // 
            this._titleLbl.AllowHtmlString = true;
            this._titleLbl.Location = new System.Drawing.Point(87, 2);
            this._titleLbl.Name = "_titleLbl";
            this._titleLbl.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this._titleLbl.Size = new System.Drawing.Size(222, 60);
            this._titleLbl.StyleController = this._layout;
            this._titleLbl.TabIndex = 4;
            // 
            // _root
            // 
            this._root.CustomizationFormText = "Root";
            this._root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this._root.GroupBordersVisible = false;
            this._root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this._imageItem,
            this._titleItem,
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this._startItem,
            this._cancelItem,
            this._messageItem});
            this._root.Location = new System.Drawing.Point(0, 0);
            this._root.Name = "_root";
            this._root.Padding = new DevExpress.XtraLayout.Utils.Padding(10, 10, 0, 10);
            this._root.Size = new System.Drawing.Size(321, 179);
            this._root.Text = "_root";
            this._root.TextVisible = false;
            // 
            // _imageItem
            // 
            this._imageItem.Control = this._imageLbl;
            this._imageItem.CustomizationFormText = "_imageItem";
            this._imageItem.Location = new System.Drawing.Point(0, 0);
            this._imageItem.MaxSize = new System.Drawing.Size(75, 64);
            this._imageItem.MinSize = new System.Drawing.Size(75, 64);
            this._imageItem.Name = "_imageItem";
            this._imageItem.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this._imageItem.Size = new System.Drawing.Size(75, 64);
            this._imageItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this._imageItem.Text = "_imageItem";
            this._imageItem.TextSize = new System.Drawing.Size(0, 0);
            this._imageItem.TextToControlDistance = 0;
            this._imageItem.TextVisible = false;
            // 
            // _titleItem
            // 
            this._titleItem.Control = this._titleLbl;
            this._titleItem.CustomizationFormText = "_messageItem";
            this._titleItem.Location = new System.Drawing.Point(75, 0);
            this._titleItem.MaxSize = new System.Drawing.Size(0, 64);
            this._titleItem.MinSize = new System.Drawing.Size(67, 64);
            this._titleItem.Name = "_titleItem";
            this._titleItem.Size = new System.Drawing.Size(226, 64);
            this._titleItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this._titleItem.Text = "_titleItem";
            this._titleItem.TextSize = new System.Drawing.Size(0, 0);
            this._titleItem.TextToControlDistance = 0;
            this._titleItem.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 114);
            this.emptySpaceItem1.MinSize = new System.Drawing.Size(10, 1);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.emptySpaceItem1.Size = new System.Drawing.Size(301, 29);
            this.emptySpaceItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 143);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(46, 26);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // _startItem
            // 
            this._startItem.Control = this._startBtn;
            this._startItem.CustomizationFormText = "_startItem";
            this._startItem.Location = new System.Drawing.Point(46, 143);
            this._startItem.MaxSize = new System.Drawing.Size(155, 26);
            this._startItem.MinSize = new System.Drawing.Size(155, 26);
            this._startItem.Name = "_startItem";
            this._startItem.Padding = new DevExpress.XtraLayout.Utils.Padding(2, 10, 2, 2);
            this._startItem.Size = new System.Drawing.Size(155, 26);
            this._startItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this._startItem.Text = "_startItem";
            this._startItem.TextSize = new System.Drawing.Size(0, 0);
            this._startItem.TextToControlDistance = 0;
            this._startItem.TextVisible = false;
            // 
            // _cancelItem
            // 
            this._cancelItem.Control = this._cancelBtn;
            this._cancelItem.CustomizationFormText = "_cancelItem";
            this._cancelItem.Location = new System.Drawing.Point(201, 143);
            this._cancelItem.MaxSize = new System.Drawing.Size(100, 26);
            this._cancelItem.MinSize = new System.Drawing.Size(100, 26);
            this._cancelItem.Name = "_cancelItem";
            this._cancelItem.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 20, 2, 2);
            this._cancelItem.Size = new System.Drawing.Size(100, 26);
            this._cancelItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this._cancelItem.Text = "_cancelItem";
            this._cancelItem.TextSize = new System.Drawing.Size(0, 0);
            this._cancelItem.TextToControlDistance = 0;
            this._cancelItem.TextVisible = false;
            // 
            // _messageItem
            // 
            this._messageItem.Control = this._messageLbl;
            this._messageItem.CustomizationFormText = "_textItem";
            this._messageItem.Location = new System.Drawing.Point(0, 64);
            this._messageItem.MaxSize = new System.Drawing.Size(0, 50);
            this._messageItem.MinSize = new System.Drawing.Size(67, 50);
            this._messageItem.Name = "_messageItem";
            this._messageItem.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this._messageItem.Size = new System.Drawing.Size(301, 50);
            this._messageItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this._messageItem.Text = "_messageItem";
            this._messageItem.TextSize = new System.Drawing.Size(0, 0);
            this._messageItem.TextToControlDistance = 0;
            this._messageItem.TextVisible = false;
            // 
            // StartView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._layout);
            this.Name = "StartView";
            this.Size = new System.Drawing.Size(321, 179);
            ((System.ComponentModel.ISupportInitialize)(this._layout)).EndInit();
            this._layout.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._imageItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._titleItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._startItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._cancelItem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._messageItem)).EndInit();
            this.ResumeLayout(false);

    }

    #endregion

    private DevExpress.XtraLayout.LayoutControl _layout;
    private DevExpress.XtraLayout.LayoutControlGroup _root;
    private DevExpress.XtraEditors.SimpleButton _cancelBtn;
    private DevExpress.XtraEditors.SimpleButton _startBtn;
    private System.Windows.Forms.Label _imageLbl;
    private DevExpress.XtraEditors.LabelControl _titleLbl;
    private DevExpress.XtraLayout.LayoutControlItem _imageItem;
    private DevExpress.XtraLayout.LayoutControlItem _titleItem;
    private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
    private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
    private DevExpress.XtraLayout.LayoutControlItem _startItem;
    private DevExpress.XtraLayout.LayoutControlItem _cancelItem;
    private DevExpress.XtraEditors.LabelControl _messageLbl;
    private DevExpress.XtraLayout.LayoutControlItem _messageItem;
  }
}
