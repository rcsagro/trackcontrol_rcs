namespace TrackControl.Zones.Tuning
{
  partial class MainView
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
            this._panel = new DevExpress.XtraEditors.GroupControl();
            ((System.ComponentModel.ISupportInitialize)(this._panel)).BeginInit();
            this.SuspendLayout();
            // 
            // _panel
            // 
            this._panel.AppearanceCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this._panel.AppearanceCaption.Options.UseFont = true;
            this._panel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this._panel.ContentImageAlignment = System.Drawing.ContentAlignment.TopLeft;
            this._panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this._panel.Location = new System.Drawing.Point(0, 0);
            this._panel.Margin = new System.Windows.Forms.Padding(0);
            this._panel.Name = "_panel";
            this._panel.Size = new System.Drawing.Size(416, 289);
            this._panel.TabIndex = 0;
            this._panel.Text = " ������ ����������� ��� �� MIF";
            // 
            // MainView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._panel);
            this.Name = "MainView";
            this.Size = new System.Drawing.Size(416, 289);
            ((System.ComponentModel.ISupportInitialize)(this._panel)).EndInit();
            this.ResumeLayout(false);

    }

    #endregion

    private DevExpress.XtraEditors.GroupControl _panel;
  }
}
