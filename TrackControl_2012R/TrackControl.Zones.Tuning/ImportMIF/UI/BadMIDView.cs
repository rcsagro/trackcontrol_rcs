using DevExpress.XtraEditors;

using System;
using System.Windows.Forms;
using TrackControl.General;
using TrackControl.Zones.Tuning.Properties;

namespace TrackControl.Zones.Tuning
{
    public partial class BadMIDView : XtraUserControl
    {
        private BadMIDState _state;

        internal BadMIDView(BadMIDState state)
        {
            if (null == state)
                throw new ArgumentNullException("state");

            _state = state;
            InitializeComponent();
            Init();

            _backBtn.Image = Shared.Cancel;
            _nextBtn.Image = Shared.Right;
            _imageLabel.Image = _state.Image;
            _messageLabel.Text = _state.Message;
            _nameBox.EditValue = _state.DefaultName;
            ActiveControl = _imageLabel;
        }

        private void Init()
        {
            this._nextBtn.Text = Resources.ImportMIFContinue;
            this._backBtn.Text = Resources.ImportMIFBack;
            this._textLbl.Text = Resources.ImportMIFUseDefaultFile;
            this.layoutControlGroup1.Text = Resources.ImportMIFRoot;
            this.layoutControlItem2.CustomizationFormText = Resources.ImportMIFDefault;
            this.layoutControlItem2.Text = Resources.ImportMIFDefault;
        }

        private void nextBtn_Click(object sender, EventArgs e)
        {
            releaseView();
            _state.StepForward();
        }

        private void backBtn_Click(object sender, EventArgs e)
        {
            releaseView();
            _state.StepBack();
        }

        private void nameBox_KeyUp(object sender, KeyEventArgs e)
        {
            _state.DefaultName = (string) _nameBox.EditValue;
        }

        private void releaseView()
        {
            Parent = null;
            Dispose();
        }
    }
}
