namespace TrackControl.Zones.Tuning
{
  partial class ProgressView
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this._layout = new DevExpress.XtraLayout.LayoutControl();
      this._progress = new DevExpress.XtraEditors.ProgressBarControl();
      this._messageLbl = new DevExpress.XtraEditors.LabelControl();
      this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
      this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
      this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
      this._imageLbl = new System.Windows.Forms.Label();
      this._imageItem = new DevExpress.XtraLayout.LayoutControlItem();
      this._messageItem = new DevExpress.XtraLayout.LayoutControlItem();
      this._progressItem = new DevExpress.XtraLayout.LayoutControlItem();
      ((System.ComponentModel.ISupportInitialize)(this._layout)).BeginInit();
      this._layout.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this._progress.Properties)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this._imageItem)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this._messageItem)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this._progressItem)).BeginInit();
      this.SuspendLayout();
      // 
      // _layout
      // 
      this._layout.Controls.Add(this._progress);
      this._layout.Controls.Add(this._messageLbl);
      this._layout.Controls.Add(this._imageLbl);
      this._layout.Dock = System.Windows.Forms.DockStyle.Fill;
      this._layout.Location = new System.Drawing.Point(0, 0);
      this._layout.Margin = new System.Windows.Forms.Padding(0);
      this._layout.Name = "_layout";
      this._layout.Root = this.layoutControlGroup1;
      this._layout.Size = new System.Drawing.Size(288, 212);
      this._layout.TabIndex = 0;
      this._layout.Text = "layoutControl1";
      // 
      // _progress
      // 
      this._progress.Location = new System.Drawing.Point(40, 98);
      this._progress.Name = "_progress";
      this._progress.Size = new System.Drawing.Size(208, 47);
      this._progress.StyleController = this._layout;
      this._progress.TabIndex = 6;
      // 
      // _messageLbl
      // 
      this._messageLbl.AllowHtmlString = true;
      this._messageLbl.Location = new System.Drawing.Point(87, 12);
      this._messageLbl.Name = "_messageLbl";
      this._messageLbl.Size = new System.Drawing.Size(189, 60);
      this._messageLbl.StyleController = this._layout;
      this._messageLbl.TabIndex = 5;
      this._messageLbl.Text = "";
      // 
      // layoutControlGroup1
      // 
      this.layoutControlGroup1.CustomizationFormText = "Root";
      this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
      this.layoutControlGroup1.GroupBordersVisible = false;
      this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this._imageItem,
            this._messageItem,
            this._progressItem,
            this.emptySpaceItem1,
            this.emptySpaceItem2});
      this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
      this.layoutControlGroup1.Name = "Root";
      this.layoutControlGroup1.Size = new System.Drawing.Size(288, 212);
      this.layoutControlGroup1.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
      this.layoutControlGroup1.Text = "Root";
      this.layoutControlGroup1.TextVisible = false;
      // 
      // emptySpaceItem1
      // 
      this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
      this.emptySpaceItem1.Location = new System.Drawing.Point(0, 137);
      this.emptySpaceItem1.Name = "emptySpaceItem1";
      this.emptySpaceItem1.Size = new System.Drawing.Size(268, 55);
      this.emptySpaceItem1.Text = "emptySpaceItem1";
      this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
      // 
      // emptySpaceItem2
      // 
      this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
      this.emptySpaceItem2.Location = new System.Drawing.Point(0, 64);
      this.emptySpaceItem2.Name = "emptySpaceItem2";
      this.emptySpaceItem2.Size = new System.Drawing.Size(268, 22);
      this.emptySpaceItem2.Text = "emptySpaceItem2";
      this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
      // 
      // _imageLbl
      // 
      this._imageLbl.Location = new System.Drawing.Point(12, 12);
      this._imageLbl.Name = "_imageLbl";
      this._imageLbl.Size = new System.Drawing.Size(71, 60);
      this._imageLbl.TabIndex = 4;
      // 
      // _imageItem
      // 
      this._imageItem.Control = this._imageLbl;
      this._imageItem.CustomizationFormText = "_imageItem";
      this._imageItem.Location = new System.Drawing.Point(0, 0);
      this._imageItem.MaxSize = new System.Drawing.Size(75, 64);
      this._imageItem.MinSize = new System.Drawing.Size(75, 64);
      this._imageItem.Name = "_imageItem";
      this._imageItem.Size = new System.Drawing.Size(75, 64);
      this._imageItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
      this._imageItem.Text = "_imageItem";
      this._imageItem.TextSize = new System.Drawing.Size(0, 0);
      this._imageItem.TextToControlDistance = 0;
      this._imageItem.TextVisible = false;
      // 
      // _messageItem
      // 
      this._messageItem.Control = this._messageLbl;
      this._messageItem.CustomizationFormText = "_messageItem";
      this._messageItem.Location = new System.Drawing.Point(75, 0);
      this._messageItem.MinSize = new System.Drawing.Size(67, 17);
      this._messageItem.Name = "_messageItem";
      this._messageItem.Size = new System.Drawing.Size(193, 64);
      this._messageItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
      this._messageItem.Text = "_messageItem";
      this._messageItem.TextSize = new System.Drawing.Size(0, 0);
      this._messageItem.TextToControlDistance = 0;
      this._messageItem.TextVisible = false;
      // 
      // _progressItem
      // 
      this._progressItem.Control = this._progress;
      this._progressItem.CustomizationFormText = "_progressItem";
      this._progressItem.Location = new System.Drawing.Point(0, 86);
      this._progressItem.MinSize = new System.Drawing.Size(54, 16);
      this._progressItem.Name = "_progressItem";
      this._progressItem.Padding = new DevExpress.XtraLayout.Utils.Padding(30, 30, 2, 2);
      this._progressItem.Size = new System.Drawing.Size(268, 51);
      this._progressItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
      this._progressItem.Text = "_progressItem";
      this._progressItem.TextSize = new System.Drawing.Size(0, 0);
      this._progressItem.TextToControlDistance = 0;
      this._progressItem.TextVisible = false;
      // 
      // ProgressView
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this._layout);
      this.Name = "ProgressView";
      this.Size = new System.Drawing.Size(288, 212);
      ((System.ComponentModel.ISupportInitialize)(this._layout)).EndInit();
      this._layout.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this._progress.Properties)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this._imageItem)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this._messageItem)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this._progressItem)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private DevExpress.XtraLayout.LayoutControl _layout;
    private DevExpress.XtraEditors.ProgressBarControl _progress;
    private DevExpress.XtraEditors.LabelControl _messageLbl;
    private System.Windows.Forms.Label _imageLbl;
    private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
    private DevExpress.XtraLayout.LayoutControlItem _imageItem;
    private DevExpress.XtraLayout.LayoutControlItem _messageItem;
    private DevExpress.XtraLayout.LayoutControlItem _progressItem;
    private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
    private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
  }
}
