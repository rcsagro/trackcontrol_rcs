using System;
using DevExpress.XtraEditors;
using TrackControl.General;
using TrackControl.Zones.Tuning.Properties;

namespace TrackControl.Zones.Tuning
{
    public partial class AdjustMIDView : XtraUserControl
    {
        private AdjustMIDState _state;

        public AdjustMIDView(AdjustMIDState state, string[] columns)
        {
            if (null == state)
                throw new ArgumentNullException("state");
            if (null == columns)
                throw new ArgumentNullException("columns");

            _state = state;
            InitializeComponent();
            Init();

            _cancelBtn.Image = Shared.Cancel;
            _nextBtn.Image = Shared.Right;

            _list.DataSource = columns;
        }

        private void Init()
        {
            this._cancelBtn.Text = Resources.ImportMIFBack;
            this._nextBtn.Text = Resources.ImportMIFContinue;
            this._messageLbl.Text = Resources.ImportMIFAdjustShowColumn;
        }

        private void nextBtn_Click(object sender, EventArgs e)
        {
            Parent = null;
            _state.IndexSelected(_list.SelectedIndex);
            Dispose();
        }

        private void cancelBtn_Click(object sender, EventArgs e)
        {
            Parent = null;
            _state.OnCancel();
            Dispose();
        }
    }
}
