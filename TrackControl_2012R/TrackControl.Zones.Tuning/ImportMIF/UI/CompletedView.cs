using System;
using DevExpress.XtraEditors;
using TrackControl.General;
using TrackControl.Zones.Tuning.Properties;

namespace TrackControl.Zones.Tuning
{
    public partial class CompletedView : XtraUserControl
    {
        private CompletedState _state;

        public CompletedView(CompletedState state)
        {
            if (null == state)
                throw new ArgumentNullException("state");

            _state = state;

            InitializeComponent();
            Init();

            _cancelBtn.Image = Shared.Cross;
            _imageLbl.Image = Shared.TickBig;
            _finishBtn.Image = Shared.Save;
            _titleLbl.Text = String.Format(Resources.ImportMIFCompletedImportEnd + " {0}", _state.ZonesCount);
            _groupBox.Properties.Items.AddRange(_state.AllGroups);
            _groupBox.EditValue = _state.Group;
            _groupBox.EditValueChanged += targetGroupChanged;
        }

        private void Init()
        {
            this._cancelBtn.Text = Resources.UI_TuningCancel;
            this._finishBtn.Text = Resources.ImportMIFCompletedSaveInDB;
            this._textLbl.Text = Resources.ImportMIFCompletedSelectGroup;
            this._groupItem.CustomizationFormText = Resources.UI_TuningZoneViewGroup;
            this._groupItem.Text = Resources.UI_TuningZoneViewGroup + ": ";
        }

        private void finishBtn_Click(object sender, EventArgs e)
        {
            releaseView();
            _state.SaveInDB();
        }

        private void cancelBtn_Click(object sender, EventArgs e)
        {
            releaseView();
            _state.OnCancel();
        }

        private void targetGroupChanged(object sender, EventArgs e)
        {
            _state.Group = (ZonesGroup) _groupBox.EditValue;
        }

        private void releaseView()
        {
            Parent = null;
            _groupBox.EditValueChanged -= targetGroupChanged;
            Dispose();
        }
    }
}
