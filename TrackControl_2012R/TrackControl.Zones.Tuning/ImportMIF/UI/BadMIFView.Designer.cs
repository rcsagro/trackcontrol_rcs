namespace TrackControl.Zones.Tuning
{
  partial class BadMIFView
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this._layout = new DevExpress.XtraLayout.LayoutControl();
      this._messageLabel = new DevExpress.XtraEditors.LabelControl();
      this._imageLabel = new System.Windows.Forms.Label();
      this._cancelBtn = new DevExpress.XtraEditors.SimpleButton();
      this._oneMoreBtn = new DevExpress.XtraEditors.SimpleButton();
      this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
      this._oneMoreItem = new DevExpress.XtraLayout.LayoutControlItem();
      this._cancelItem = new DevExpress.XtraLayout.LayoutControlItem();
      this._buttonsSpace = new DevExpress.XtraLayout.EmptySpaceItem();
      this._imageItem = new DevExpress.XtraLayout.LayoutControlItem();
      this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
      ((System.ComponentModel.ISupportInitialize)(this._layout)).BeginInit();
      this._layout.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this._oneMoreItem)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this._cancelItem)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this._buttonsSpace)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this._imageItem)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
      this.SuspendLayout();
      // 
      // _layout
      // 
      this._layout.Controls.Add(this._messageLabel);
      this._layout.Controls.Add(this._imageLabel);
      this._layout.Controls.Add(this._cancelBtn);
      this._layout.Controls.Add(this._oneMoreBtn);
      this._layout.Dock = System.Windows.Forms.DockStyle.Fill;
      this._layout.Location = new System.Drawing.Point(0, 0);
      this._layout.Margin = new System.Windows.Forms.Padding(0);
      this._layout.Name = "_layout";
      this._layout.Root = this.layoutControlGroup1;
      this._layout.Size = new System.Drawing.Size(305, 189);
      this._layout.TabIndex = 0;
      this._layout.Text = "Layout";
      // 
      // _messageLabel
      // 
      this._messageLabel.AllowHtmlString = true;
      this._messageLabel.Appearance.Options.UseTextOptions = true;
      this._messageLabel.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Top;
      this._messageLabel.Location = new System.Drawing.Point(82, 12);
      this._messageLabel.Name = "_messageLabel";
      this._messageLabel.Size = new System.Drawing.Size(211, 139);
      this._messageLabel.StyleController = this._layout;
      this._messageLabel.TabIndex = 8;
      // 
      // _imageLabel
      // 
      this._imageLabel.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
      this._imageLabel.Location = new System.Drawing.Point(12, 12);
      this._imageLabel.Margin = new System.Windows.Forms.Padding(0);
      this._imageLabel.Name = "_imageLabel";
      this._imageLabel.Size = new System.Drawing.Size(66, 139);
      this._imageLabel.TabIndex = 7;
      // 
      // _cancelBtn
      // 
      this._cancelBtn.Location = new System.Drawing.Point(205, 153);
      this._cancelBtn.Margin = new System.Windows.Forms.Padding(0);
      this._cancelBtn.Name = "_cancelBtn";
      this._cancelBtn.Size = new System.Drawing.Size(70, 26);
      this._cancelBtn.StyleController = this._layout;
      this._cancelBtn.TabIndex = 6;
      this._cancelBtn.Text = "������";
      this._cancelBtn.Click += new System.EventHandler(this.cancelBtn_Click);
      // 
      // _oneMoreBtn
      // 
      this._oneMoreBtn.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
      this._oneMoreBtn.Appearance.Options.UseFont = true;
      this._oneMoreBtn.Location = new System.Drawing.Point(75, 153);
      this._oneMoreBtn.Name = "_oneMoreBtn";
      this._oneMoreBtn.Size = new System.Drawing.Size(110, 26);
      this._oneMoreBtn.StyleController = this._layout;
      this._oneMoreBtn.TabIndex = 5;
      this._oneMoreBtn.Text = "�����...";
      this._oneMoreBtn.Click += new System.EventHandler(this.oneMoreBtn_Click);
      // 
      // layoutControlGroup1
      // 
      this.layoutControlGroup1.CustomizationFormText = "Root";
      this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
      this.layoutControlGroup1.GroupBordersVisible = false;
      this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this._oneMoreItem,
            this._cancelItem,
            this._buttonsSpace,
            this._imageItem,
            this.layoutControlItem2});
      this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
      this.layoutControlGroup1.Name = "Root";
      this.layoutControlGroup1.Size = new System.Drawing.Size(305, 189);
      this.layoutControlGroup1.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
      this.layoutControlGroup1.Text = "Root";
      this.layoutControlGroup1.TextVisible = false;
      // 
      // _oneMoreItem
      // 
      this._oneMoreItem.Control = this._oneMoreBtn;
      this._oneMoreItem.CustomizationFormText = "_oneMoreItem";
      this._oneMoreItem.Location = new System.Drawing.Point(65, 143);
      this._oneMoreItem.MaxSize = new System.Drawing.Size(120, 26);
      this._oneMoreItem.MinSize = new System.Drawing.Size(120, 26);
      this._oneMoreItem.Name = "_oneMoreItem";
      this._oneMoreItem.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 10, 0, 0);
      this._oneMoreItem.Size = new System.Drawing.Size(120, 26);
      this._oneMoreItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
      this._oneMoreItem.Text = "_oneMoreItem";
      this._oneMoreItem.TextSize = new System.Drawing.Size(0, 0);
      this._oneMoreItem.TextToControlDistance = 0;
      this._oneMoreItem.TextVisible = false;
      // 
      // _cancelItem
      // 
      this._cancelItem.Control = this._cancelBtn;
      this._cancelItem.CustomizationFormText = "_cancelItem";
      this._cancelItem.Location = new System.Drawing.Point(185, 143);
      this._cancelItem.MaxSize = new System.Drawing.Size(100, 26);
      this._cancelItem.MinSize = new System.Drawing.Size(100, 26);
      this._cancelItem.Name = "_cancelItem";
      this._cancelItem.Padding = new DevExpress.XtraLayout.Utils.Padding(10, 20, 0, 0);
      this._cancelItem.Size = new System.Drawing.Size(100, 26);
      this._cancelItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
      this._cancelItem.Text = "_cancelItem";
      this._cancelItem.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
      this._cancelItem.TextSize = new System.Drawing.Size(0, 0);
      this._cancelItem.TextToControlDistance = 0;
      this._cancelItem.TextVisible = false;
      // 
      // _buttonsSpace
      // 
      this._buttonsSpace.CustomizationFormText = "_buttonsSpace";
      this._buttonsSpace.Location = new System.Drawing.Point(0, 143);
      this._buttonsSpace.MinSize = new System.Drawing.Size(10, 10);
      this._buttonsSpace.Name = "_buttonsSpace";
      this._buttonsSpace.Size = new System.Drawing.Size(65, 26);
      this._buttonsSpace.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
      this._buttonsSpace.Text = "_buttonsSpace";
      this._buttonsSpace.TextSize = new System.Drawing.Size(0, 0);
      // 
      // _imageItem
      // 
      this._imageItem.Control = this._imageLabel;
      this._imageItem.CustomizationFormText = "layoutControlItem1";
      this._imageItem.Location = new System.Drawing.Point(0, 0);
      this._imageItem.MaxSize = new System.Drawing.Size(70, 0);
      this._imageItem.MinSize = new System.Drawing.Size(70, 50);
      this._imageItem.Name = "_imageItem";
      this._imageItem.Size = new System.Drawing.Size(70, 143);
      this._imageItem.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
      this._imageItem.Text = "_imageItem";
      this._imageItem.TextSize = new System.Drawing.Size(0, 0);
      this._imageItem.TextToControlDistance = 0;
      this._imageItem.TextVisible = false;
      // 
      // layoutControlItem2
      // 
      this.layoutControlItem2.Control = this._messageLabel;
      this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
      this.layoutControlItem2.Location = new System.Drawing.Point(70, 0);
      this.layoutControlItem2.MinSize = new System.Drawing.Size(67, 17);
      this.layoutControlItem2.Name = "layoutControlItem2";
      this.layoutControlItem2.Size = new System.Drawing.Size(215, 143);
      this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
      this.layoutControlItem2.Text = "layoutControlItem2";
      this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
      this.layoutControlItem2.TextToControlDistance = 0;
      this.layoutControlItem2.TextVisible = false;
      // 
      // BadMIFView
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this._layout);
      this.Name = "BadMIFView";
      this.Size = new System.Drawing.Size(305, 189);
      ((System.ComponentModel.ISupportInitialize)(this._layout)).EndInit();
      this._layout.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this._oneMoreItem)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this._cancelItem)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this._buttonsSpace)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this._imageItem)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private DevExpress.XtraLayout.LayoutControl _layout;
    private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
    private DevExpress.XtraEditors.SimpleButton _cancelBtn;
    private DevExpress.XtraEditors.SimpleButton _oneMoreBtn;
    private DevExpress.XtraLayout.LayoutControlItem _oneMoreItem;
    private DevExpress.XtraLayout.LayoutControlItem _cancelItem;
    private DevExpress.XtraLayout.EmptySpaceItem _buttonsSpace;
    private DevExpress.XtraEditors.LabelControl _messageLabel;
    private System.Windows.Forms.Label _imageLabel;
    private DevExpress.XtraLayout.LayoutControlItem _imageItem;
    private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
  }
}
