using System;
using System.Threading;
using TrackControl.General;
using TrackControl.Zones.Tuning.Properties;

namespace TrackControl.Zones.Tuning
{
    public class SelectMIFState : IState, IMifFinder
    {
        private IController _controller;
        private StartView _view;

        public SelectMIFState(IController controller)
        {
            if (null == controller)
                throw new ArgumentNullException("controller");

            _controller = controller;
        }

        public void Activate()
        {
            string title = "<b>" + Resources.SelectMIFStateTitle + "</b>";
            string message = Resources.SelectMIFStateMessage;
            _controller.Reset();
            _view = new StartView(this, Shared.MIFSearch, title, message);
            _controller.SetView(_view);
        }

        public void PathSelected(string path)
        {
            IState state = new PreMIFState(_controller, path);
            WaitCallback callback = delegate(object obj) { ((IState) obj).Activate(); };
            ThreadPool.QueueUserWorkItem(callback, state);
        }

        public void OnHide()
        {
            _controller.OnHideSelected();
        }
    }
}
