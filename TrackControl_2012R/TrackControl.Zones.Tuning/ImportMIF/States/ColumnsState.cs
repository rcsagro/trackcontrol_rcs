using System;
using System.IO;
using System.Threading;

namespace TrackControl.Zones.Tuning
{
  public class ColumnsState : IState  
  {
    IController _controller;
    string[] _columns;

    public ColumnsState(IController controller, int count)
    {
      if (null == controller)
        throw new ArgumentNullException("controller");

      _controller = controller;
      _columns = new string[count];
    }

    public void Activate()
    {
      StreamReader reader = _controller.MIFReader;
      for (int i = 0; i < _columns.Length; i++)
      {
        if (reader.Peek() >= 0)
        {
          string line = reader.ReadLine().Trim();
          string[] words = line.Split(' ');
          if (words.Length < 2)
          {
            _controller.SetError(FileTrouble.WrongFormat);
            return;
          }
          _columns[i] = words[0];
        }
        else
        {
          _controller.SetError(FileTrouble.WrongFormat);
          return;
        }
      }
      ThreadPool.QueueUserWorkItem(activatePreMIDState, _columns);
    }

    void activatePreMIDState(object obj)
    {
      string[] columns = (string[])obj;
      IState state = new PreMIDState(_controller, columns);
      state.Activate();
    }
  }
}
