using System;
using System.Collections.Generic;
using TrackControl.General;
using System.Threading;

namespace TrackControl.Zones.Tuning
{
    public class CompletedState : IState
    {
        private IController _controller;
        private CompletedView _view;

        public CompletedState(IController controller)
        {
            if (null == controller)
                throw new ArgumentNullException("controller");

            _controller = controller;
        }

        public int ZonesCount
        {
            get { return _controller.Zones.Count; }
        }

        public ZonesGroup Group
        {
            get { return _controller.TargetGroup; }
            set { _controller.TargetGroup = value; }
        }

        public ZonesGroup[] AllGroups
        {
            get
            {
                List<ZonesGroup> groups = new List<ZonesGroup>(_controller.Manager.Groups);
                return groups.ToArray();
            }
        }

        public void Activate()
        {
            _view = new CompletedView(this);
            _controller.SetView(_view);
        }

        public void SaveInDB()
        {
            IState state = new SavingState(_controller);
            WaitCallback callback = delegate(object obj) { ((IState) obj).Activate(); };
            ThreadPool.QueueUserWorkItem(callback, state);
            if (_controller is ImportController)
            {
                ((ImportController) _controller).IsZonesCreated = true;
            }
        }

        public void OnCancel()
        {
            IState state = new SelectMIFState(_controller);
            WaitCallback callback = delegate(object obj) { ((IState) obj).Activate(); };
            ThreadPool.QueueUserWorkItem(callback, state);
        }
    }
}
