using System;
using System.Threading;
using TrackControl.General;
using TrackControl.Zones.Tuning.Properties;

namespace TrackControl.Zones.Tuning
{
    internal class FinishState : IState, IMifFinder
    {
        private IController _controller;
        private StartView _view;

        public FinishState(IController controller)
        {
            if (null == controller)
                throw new ArgumentNullException("controller");

            _controller = controller;
        }

        public void Activate()
        {
            string title = Resources.FinishStateActivateTitle;
            string message = Resources.FinishStateActivateMessage;
            _view = new StartView(this, Shared.TickBig, title, message);
            _controller.SetView(_view);
        }

        public void PathSelected(string path)
        {
            IState state = new PreMIFState(_controller, path);
            WaitCallback callback = delegate(object obj) { ((IState) obj).Activate(); };
            ThreadPool.QueueUserWorkItem(callback, state);
        }

        public void OnHide()
        {
            _controller.OnHideSelected();
            SelectMIFState state = new SelectMIFState(_controller);
            WaitCallback callback = delegate(object obj) { ((IState) obj).Activate(); };
            ThreadPool.QueueUserWorkItem(callback, state);
        }
    }
}
