using System;
using System.Globalization;
using System.IO;
using System.Threading;

using TrackControl.General;

namespace TrackControl.Zones.Tuning
{
  public class RegionsState : IState
  {
    IController _controller;
    int _count;
    string _name;

    public RegionsState(IController controller, int count, string name)
    {
      if (null == controller)
        throw new ArgumentNullException("controller");

      _controller = controller;
      _count = count;
      _name = name ?? _controller.DefaultName;
    }

    public void Activate()
    {
      for (int i = 0; i < _count; i++)
      {
        int pointsCount = getPointsCount();
        if (pointsCount < 1) return;

        string name = _count > 1 ? String.Format("{0} {1}", _name, i + 1) : _name;
        PointLatLng[] points = new PointLatLng[pointsCount];
        for (int j = 0; j < pointsCount; j++)
        {
          PointLatLng point = getPoint();
          if (point.IsCorrect)
            points[j] = point;
          else
            return;
        }

        if (pointsCount > 2)
          _controller.CreateZone(name, points);
      }

      finish();
    }

    int getPointsCount()
    {
      StreamReader reader = _controller.MIFReader;
      if (reader.Peek() < 0)
      {
        _controller.SetError(FileTrouble.WrongFormat);
        return 0;
      }

      string line = reader.ReadLine().Trim();
      int result = 0;
      if (!Int32.TryParse(line, out result))
        _controller.SetError(FileTrouble.WrongFormat);

      return result;
    }

    PointLatLng getPoint()
    {
      StreamReader reader = _controller.MIFReader;
      if (reader.Peek() < 0)
      {
        _controller.SetError(FileTrouble.WrongFormat);
        return PointLatLng.IncorrectPoint;
      }

      string[] lines = reader.ReadLine().Trim().Split(' ');
      if (lines.Length != 2)
      {
        _controller.SetError(FileTrouble.WrongFormat);
        return PointLatLng.IncorrectPoint;
      }

      double lat = 0;
      double lng = 0;
      if (!Double.TryParse(lines[0], NumberStyles.Number, CultureInfo.InvariantCulture, out lng)
        || !Double.TryParse(lines[1], NumberStyles.Number, CultureInfo.InvariantCulture, out lat))
      {
        _controller.SetError(FileTrouble.WrongFormat);
        return PointLatLng.IncorrectPoint;
      }

      return new PointLatLng(lat, lng);
    }

    void finish()
    {
      IState state = new GeometryState(_controller);
      WaitCallback callback = delegate(object obj) { ((IState)obj).Activate(); };
      ThreadPool.QueueUserWorkItem(callback, state);
    }
  }
}
