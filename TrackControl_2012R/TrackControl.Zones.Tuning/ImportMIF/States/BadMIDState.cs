using System;
using System.Drawing;
using System.Threading;
using TrackControl.General;
using TrackControl.Zones.Tuning.Properties;

namespace TrackControl.Zones.Tuning
{
  internal class BadMIDState : IState
  {
    IController _controller;
    BadMIDView _view;
    string _message;
    Image _image;

    internal BadMIDState(IController controller, FileTrouble trouble)
    {
      if (null == controller)
        throw new ArgumentNullException("controller");

      _controller = controller;
      init(trouble);
    }

    internal string Message
    {
      get { return _message; }
    }

    internal Image Image
    {
      get { return _image; }
    }

    internal string DefaultName
    {
      get { return _controller.DefaultName; }
      set { _controller.SetName(value); }
    }

    public void Activate()
    {
      _view = new BadMIDView(this);
      _controller.SetView(_view);
    }

    public void StepForward()
    {
      IState state = new DataState(_controller);
      WaitCallback callback = delegate(object obj) { ((IState)obj).Activate(); };
      ThreadPool.QueueUserWorkItem(callback, state);
    }

    public void StepBack()
    {
      IState state = new SelectMIFState(_controller);
      WaitCallback callback = delegate(object obj) { ((IState)obj).Activate(); };
      ThreadPool.QueueUserWorkItem(callback, state);
    }

    void init(FileTrouble trouble)
    {
      if (FileTrouble.NotFound == trouble)
      {
        _message = Resources.MIDStateNotFound;
        _image = Shared.WarningBig;
      }
      else if (FileTrouble.AccessDenied == trouble)
      {
        _message = Resources.MIDStateAccessDenied;
        _image = Shared.WarningBig;
      }
      else if (FileTrouble.Empty == trouble)
      {
        _message = Resources.MIDStateEmpty;
        _image = Shared.WarningBig;
      }
      else
      {
        _message = Resources.MIDStateWrong;
        _image = Shared.WarningBig;
      }
    }
  }
}
