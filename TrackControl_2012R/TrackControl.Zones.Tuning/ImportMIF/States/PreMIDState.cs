using System;
using System.IO;
using System.Threading;

namespace TrackControl.Zones.Tuning
{
  public class PreMIDState : IState
  {
    IController _controller;
    string[] _columns;

    public PreMIDState(IController controller, string[] columns)
    {
      if (null == controller)
        throw new ArgumentNullException("controller");
      if (null == columns)
        throw new ArgumentNullException("columns");

      _controller = controller;
      _columns = columns;
    }

    public void Activate() 
    {
      string midPath = Path.ChangeExtension(_controller.MIFStream.Name, "mid");
      if (isMidFileNotFound(midPath)) return;
      if (isMidFileBlocked(midPath)) return;
      if (isMidFileEmpty()) return;

      setAdjusting();
    }

    bool isMidFileNotFound(string midPath)
    {
      if (!File.Exists(midPath))
      {
        setTrouble(FileTrouble.NotFound);
        return true;
      }
      return false;
    }

    bool isMidFileBlocked(string midPath)
    {
      WaitCallback callback = delegate(object obj) { ((IState)obj).Activate(); };
      try
      {
        _controller.MIDStream = new FileStream(midPath, FileMode.Open, FileAccess.Read);
      }
      catch (UnauthorizedAccessException)
      {
        setTrouble(FileTrouble.AccessDenied);
        return true;
      }
      catch (Exception)
      {
        setTrouble(FileTrouble.Unknown);
        return true;
      }

      return false;
    }

    bool isMidFileEmpty()
    {
      if (_controller.MIDStream.Length == 0)
      {
        setTrouble(FileTrouble.Empty);
        return true;
      }
      else
      {
        return false;
      }
    }

    void setTrouble(FileTrouble trouble)
    {
      IState state = new BadMIDState(_controller, trouble);
      WaitCallback callback = delegate(object obj) { ((IState)obj).Activate(); };
      ThreadPool.QueueUserWorkItem(callback, state);
    }

    void setAdjusting()
    {
      IState state = new AdjustMIDState(_controller, _columns);
      WaitCallback callback = delegate(object obj) { ((IState)obj).Activate(); };
      ThreadPool.QueueUserWorkItem(callback, state);
    }
  }
}
