using System;
using System.Drawing;
using System.Threading;
using TrackControl.General;
using TrackControl.Zones.Tuning.Properties;

namespace TrackControl.Zones.Tuning
{
  public class BadMIFState : IState
  {
    IController _controller;
    BadMIFView _view;
    string _message;
    Image _image;

    internal BadMIFState(IController controller, FileTrouble trouble)
    {
      if (null == controller)
        throw new ArgumentNullException("controller");

      _controller = controller;
      init(trouble);
    }

    internal Image Image
    {
      get { return _image; }
    }

    internal string Message
    {
      get { return _message; }
    }

    public void Activate()
    {
      _view = new BadMIFView(this);
      _controller.SetView(_view);
    }

    internal void StepBack()
    {
      IState state = new SelectMIFState(_controller);
      WaitCallback callback = delegate(object obj) { ((IState)obj).Activate(); };
      ThreadPool.QueueUserWorkItem(callback, state);
    }

    internal void onCancel()
    {
      _controller.OnHideSelected();
      StepBack();
    }

    void init(FileTrouble trouble)
    {
      if (FileTrouble.Empty == trouble)
      {
        _image = Shared.MIFWarning;
        _message = Resources.StateFileEmpty;
      }
      else if (FileTrouble.AccessDenied == trouble)
      {
        _image = Shared.MIFNoAccess;
        _message = Resources.StateAccessDenied;
      }
      else if (FileTrouble.WrongFormat == trouble)
      {
        _image = Shared.MIFWarning;
        _message = Resources.StateWrongFormat;
      }
      else
      {
        _image = Shared.MIFWarning;
        _message = Resources.StateUndefinedWrong;
      }
    }
  }
}
