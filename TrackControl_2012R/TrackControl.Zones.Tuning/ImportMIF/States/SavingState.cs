using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows.Forms;

using TrackControl.General;
using TrackControl.Zones.Tuning.Properties;

namespace TrackControl.Zones.Tuning
{
    public class SavingState : IState
    {
        private IController _controller;
        private ProgressView _view;

        public SavingState(IController controller)
        {
            if (null == controller)
                throw new ArgumentNullException("controller");
            _controller = controller;
        }

        public void Activate()
        {
            string message = Resources.SavingStateActivateMessage;
            _view = new ProgressView(Shared.DbSave, message, _controller.Zones.Count);
            _controller.SetView(_view);
            ThreadPool.QueueUserWorkItem(startSaving);
        }

        private void startSaving(object obj)
        {
            IZonesManager manager = _controller.Manager;
            ZonesGroup group = _controller.TargetGroup;
            IList<IZone> zones = _controller.Zones;
            int current = 0;

            foreach (IZone zone in zones)
            {
                current += 1;
                zone.Group = group;
                manager.SaveZone(zone);
                updateProgress(current);
            }

            MethodInvoker m = delegate
            {
                manager.AddZones(zones);
                    // �������� ������� ��� ��-�� ����������� �������������� �������� � ��������� ������ �� �����.�����
                _view.Release();
            };

            _view.Invoke(m);
            handleFinish();
        }

        private void updateProgress(int current)
        {
            try
            {
                MethodInvoker m = delegate
                {
                    _view.SetProgress( current );
                };

                _view.Invoke( m );
            }
            catch( Exception ex )
            {
                MessageBox.Show( ex.Message + "\n" + ex.StackTrace, "Error update progress", MessageBoxButtons.OK);
            }
        }

        private void handleFinish()
        {
            IState state = new FinishState(_controller);
            WaitCallback callback = delegate(object obj) { ((IState) obj).Activate(); };
            ThreadPool.QueueUserWorkItem(callback, state);
        }
    }
}
