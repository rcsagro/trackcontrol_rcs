using System;
using System.IO;
using System.Threading;
using System.Text;

namespace TrackControl.Zones.Tuning
{
  class HeaderState : IState
  {
    IController _controller;
    Predicate<string> parse;

    string _charset;
    //string _delimiter;

    internal HeaderState(IController controller)
    {
      if (null == controller)
        throw new ArgumentNullException("controller");

      _controller = controller;
    }

    public void Activate()
    {
      parse = isVersionOk;
      StreamReader reader = _controller.MIFReader;
      while (reader.Peek() >= 0)
      {
        string line = reader.ReadLine();
        if (!parse(line))
          break;
      }
    }

    bool isVersionOk(string line)
    {
      line = line.Trim();
      if (0 == line.Length)
        return true;

      line = line.ToUpper();
      if (line.StartsWith("VERSION "))
      {
        parse = isHeaderOk;
        return true;
      }
      else
      {
        _controller.SetError(FileTrouble.WrongFormat);
        return false;
      }      
    }

    bool isHeaderOk(string line)
    {
      line = line.Trim();
      if (0 == line.Length)
        return true;

      string upper = line.ToUpper();
      if (upper.StartsWith("CHARSET "))
      {
        setCharset(line);
        return true;
      }
      else if (upper.StartsWith("DELIMITER "))
      {
        setDelimiter(line);
        return true;
      }
      else if (upper.StartsWith("COLUMNS "))
      {
        activateColumnsState(line);
        return false;
      }
      else if (upper.StartsWith("DATA"))
      {
        return false;
      }
      else
      {
        return true;
      }
    }

    void setCharset(string line)
    {
      StringBuilder builder = new StringBuilder(line);
      int length = 8; // ���������� �������� � ��������� "CHARSET "
      builder = builder.Remove(0, 8);
      if (builder[0] == '"' && builder[builder.Length - 1] == '"')
      {
        builder = builder.Remove(0, 1);
        builder = builder.Remove(builder.Length - 1, 1);
        _charset = builder.ToString();
      }
    }

    void setDelimiter(string line)
    {
      StringBuilder builder = new StringBuilder(line);
      int length = 10; // ���������� �������� � ��������� "DELIMITER "
      builder = builder.Remove(0, length);
      if (3 == builder.Length && builder[0] == '"' && builder[2] == '"')
      {
        _controller.SetDelimiter(builder[1]);
      }
    }

    void activateColumnsState(string line)
    {
      int length = 8; // ���������� �������� � ��������� "COLUMNS "
      string str = line.Remove(0, length);
      int count;
      if (Int32.TryParse(str, out count))
      {
        _controller.SetColumnCount(count);
        IState state = new ColumnsState(_controller, count);
        WaitCallback callback = delegate(object obj) { ((IState)obj).Activate(); };
        ThreadPool.QueueUserWorkItem(callback, state);
      }
      else
      {
        _controller.SetError(FileTrouble.WrongFormat);
      }
    }
  }
}
