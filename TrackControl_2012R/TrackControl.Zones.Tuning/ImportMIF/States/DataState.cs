using System;
using System.IO;
using System.Threading;

namespace TrackControl.Zones.Tuning
{
  public class DataState : IState
  {
    IController _controller;

    public DataState(IController controller)
    {
      if (null == controller)
        throw new ArgumentNullException("controller");

      _controller = controller;
    }

    public void Activate()
    {
      StreamReader reader = _controller.MIFReader;
      while (reader.Peek() >= 0)
      {
        string line = reader.ReadLine().Trim();
        if (line.Length == 0)
        {
          continue;
        }
        else if ("DATA" == line.ToUpper())
        {
          ThreadPool.QueueUserWorkItem(activateGeometryState);
          return;
        }
        else
        {
          _controller.SetError(FileTrouble.WrongFormat);
          return;
        }
      }
    }

    void activateGeometryState(object obj)
    {
      IState state = new GeometryState(_controller);
      state.Activate();
    }
  }
}
