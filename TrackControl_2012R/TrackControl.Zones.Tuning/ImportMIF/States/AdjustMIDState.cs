using System;
using System.Threading;

namespace TrackControl.Zones.Tuning
{
  public class AdjustMIDState : IState
  {
    IController _controller;
    string[] _columns;
    AdjustMIDView _view;

    public AdjustMIDState(IController controller, string[] columns)
    {
      if (null == controller)
        throw new ArgumentNullException("controller");
      if (null == columns)
        throw new ArgumentNullException("columns");

      _controller = controller;
      _columns = columns;
    }

    public void Activate()
    {
      _view = new AdjustMIDView(this, _columns);
      _controller.SetView(_view);
    }

    public void IndexSelected(int index)
    {
      _controller.SetColumnIndex(index);

      IState state = new DataState(_controller);
      WaitCallback callback = delegate(object obj) { ((IState)obj).Activate(); };
      ThreadPool.QueueUserWorkItem(callback, state);
    }

    public void OnCancel()
    {
      IState state = new SelectMIFState(_controller);
      WaitCallback callback = delegate(object obj) { ((IState)obj).Activate(); };
      ThreadPool.QueueUserWorkItem(callback, state);
    }
  }
}
