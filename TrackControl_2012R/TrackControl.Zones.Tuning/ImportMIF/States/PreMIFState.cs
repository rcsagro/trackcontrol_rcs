using System;
using System.IO;
using System.Threading;

namespace TrackControl.Zones.Tuning
{
    internal class PreMIFState : IState
    {
        private IController _controller;
        private string _mifPath;

        internal PreMIFState(IController controller, string mifPath)
        {
            if (null == controller)
                throw new ArgumentNullException("controller");
            if (null == mifPath)
                throw new ArgumentNullException("mifPath");
            if (mifPath.Length == 0)
                throw new Exception("mifPath can not be empty string");

            _controller = controller;
            _mifPath = mifPath;
        }

        public void Activate()
        {
            if (isMifFileBlocked()) return;
            if (isMifFileEmpty()) return;
            ThreadPool.QueueUserWorkItem(activateHeaderState);
        }

        private bool isMifFileBlocked()
        {
            try
            {
                _controller.MIFStream = new FileStream(_mifPath, FileMode.Open, FileAccess.Read);
            }
            catch (UnauthorizedAccessException)
            {
                _controller.SetError(FileTrouble.AccessDenied);
                return true;
            }
            catch (Exception)
            {
                _controller.SetError(FileTrouble.Unknown);
                return true;
            }

            return false;
        }

        private bool isMifFileEmpty()
        {
            if (_controller.MIFStream.Length == 0)
            {
                _controller.SetError(FileTrouble.Empty);
                return true;
            }
            else
            {
                return false;
            }
        }

        private void activateHeaderState(object obj)
        {
            IState state = new HeaderState(_controller);
            state.Activate();
        }
    }
}
