using System;
using System.IO;
using System.Threading;

namespace TrackControl.Zones.Tuning
{
  public class GeometryState : IState
  {
    IController _controller;

    public GeometryState(IController controller)
    {
      if (null == controller)
        throw new ArgumentNullException("controller");

      _controller = controller;
    }

    public void Activate()
    {
      StreamReader reader = _controller.MIFReader;
      while (reader.Peek() >= 0)
      {
        string line = reader.ReadLine().Trim().ToUpper();
        if (line.Length == 0)
          continue;

        if (line.StartsWith("POINT ") ||
          line.StartsWith("LINE") ||
          line.StartsWith("PLINE ") ||
          line.StartsWith("ARC") ||
          line.StartsWith("TEXT") ||
          line.StartsWith("RECT") ||
          line.StartsWith("ROUNDRECT") ||
          line.StartsWith("ELLIPSE"))
        {
          _controller.GetName();
          continue;
        }
        else if (line.StartsWith("REGION "))
        {
          handleRegion(line, _controller.GetName());
          return;
        }
        else
          continue;
      }

      handleFinish();
    }

    void handleRegion(string line, string name)
    {
      line = line.Replace("REGION ", "");
      line = line.Trim();
      int count = 0;
      if (Int32.TryParse(line, out count))
      {
        IState state = new RegionsState(_controller, count, name);
        WaitCallback callback = delegate(object obj) { ((IState)obj).Activate(); };
        ThreadPool.QueueUserWorkItem(callback, state);
      }
      else
      {
        _controller.SetError(FileTrouble.WrongFormat);
      }
    }

    void handleFinish()
    {
      IState state = new CompletedState(_controller);
      WaitCallback callback = delegate(object obj) { ((IState)obj).Activate(); };
      ThreadPool.QueueUserWorkItem(callback, state);
    }
  }
}
