using System;
using System.Drawing;

using TrackControl.General;

namespace TrackControl.Zones.Tuning
{
  public class TuningGroup
  {
    ZonesGroup _group;
    string _name;
    string _description;
    Image _icon;
    TuningStatus _status;
    bool _visible;

    public event VoidHandler StatusChanged;

    public TuningGroup(ZonesGroup group)
    {
      if (null == group)
        throw new ArgumentNullException("group");

      _group = group;
      RefreshData();
    }

    public ZonesGroup Group
    {
      get { return _group; }
    }

    public bool IsRoot
    {
      get { return _group.IsRoot; }
    }

    public string Id
    {
      get
      {
        if (_group.IsRoot || _group.IsNew)
          return "-";
        else
          return _group.Id.ToString();
      }
      set { return; }
    }

    public string Name
    {
      get { return _name; }
      set
      {
        if (value != _name)
        {
          _name = value;
          updateStatus();
        }
      }
    }

    public string Description
    {
      get { return _description; }
      set
      {
        if (value != _description)
        {
          _description = value;
          updateStatus();
        }
      }
    }

    public Image Icon
    {
      get { return _icon; }
    }

    public int ZonesCount
    {
      get { return _group.AllItems.Count; }
      set { return; }
    }

    public TuningStatus Status
    {
      get { return _status; }
    }

    public bool Visible
    {
      get { return _visible; }
      set { _visible = value; }
    }

    public void RefreshData()
    {
      _name = _group.Name;
      _description = _group.Description;
      _icon = _group.Style.Icon;
      _status = _group.IsNew ? TuningStatus.NewAdded : TuningStatus.Ok;
      _IdOutLink = _group.IdOutLink; 
    }

    public void Bind()
    {
      _group.Tag = this;
    }

    public void UnBind()
    {
      _group.Tag = null;
    }

    public override string ToString()
    {
      return _name;
    }

    void updateStatus()
    {
      if (TuningStatus.Ok != _status &&
        _name == _group.Name &&
        _description == _group.Description && _IdOutLink == _group.IdOutLink)
      {
        _status = _group.IsNew ? TuningStatus.NewAdded : TuningStatus.Ok;
        onStatusChanged();
      }
      else if (_name != _group.Name || _description != _group.Description || _IdOutLink != _group.IdOutLink)
      {
        _status = TuningStatus.NotSaved;
        onStatusChanged();
      }
    }

    void onStatusChanged()
    {
      VoidHandler handler = StatusChanged;
      if (null != handler)
        handler();
    }
    string _IdOutLink;
    /// <summary>
    /// ��� ������� ����
    /// </summary>
    public string IdOutLink
    {
        get { return _IdOutLink; }
        set 
        {
            if (value != _IdOutLink)
            {
                _IdOutLink = value;
                updateStatus();
            }
        }
    }  
  }
}
