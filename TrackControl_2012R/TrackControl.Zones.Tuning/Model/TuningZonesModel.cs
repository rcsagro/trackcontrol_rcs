using System;
using System.Collections.Generic;
using TrackControl.General;

namespace TrackControl.Zones.Tuning
{
    public class TuningZonesModel : ITreeModel<IZone>
    {
        private readonly IZonesManager _manager;
        private readonly List<TuningGroup> _groups;
        private readonly List<TuningZone> _zones;

        public event VoidHandler VisibilityChanged;

        public TuningZonesModel(IZonesManager manager)
        {
            _manager = manager;

            IList<Group<IZone>> groups = _manager.Root.AllGroups;
            _groups = new List<TuningGroup>(groups.Count + 1);
            TuningGroup root = new TuningGroup(_manager.Root);
            root.Bind();
            _groups.Add(root);
            foreach (ZonesGroup group in groups)
            {
                TuningGroup tuning = new TuningGroup(group);
                tuning.Bind();
                _groups.Add(tuning);
            }

            IList<IZone> zones = _manager.Zones;
            _zones = new List<TuningZone>(zones.Count);
            foreach (IZone zone in zones)
                _zones.Add(new TuningZone(zone));
        }

        public TuningGroup[] Groups
        {
            get { return _groups.ToArray(); }
        }

        public void Activate()
        {
            _groups.ForEach(delegate(TuningGroup tg) { tg.Bind(); });
            _zones.ForEach(delegate(TuningZone tz) { tz.Bind(); });
        }

        public void DeActivate()
        {
            _groups.ForEach(delegate(TuningGroup tg) { tg.UnBind(); });
            _zones.ForEach(delegate(TuningZone tz) { tz.UnBind(); });
        }

        public IZone Current
        {
            get { return null; }
        }

        public IList<IZone> Checked
        {
            get
            {
                List<IZone> zones = new List<IZone>();

                _zones.ForEach(delegate(TuningZone tuning)
                {
                    if (tuning.Visible)
                        zones.Add(tuning);
                });

                return zones;
            }
        }

        public IList<IZone> GetAll()
        {
            return new List<IZone>(_manager.Root.AllItems);
        }

        public void ChangeVisibility(IList<IZone> zones, bool visibility)
        {
            if (zones.Count > 0)
            {
                foreach (IZone zone in zones)
                {
                    TuningZone tuning;
                    if ((TuningZone) zone.Tag == null)
                    {
                        tuning = new TuningZone(zone);
                    }
                    else
                    {
                        tuning = (TuningZone) zone.Tag;
                    }

                    tuning.Visible = visibility;
                }
                onVisibilityChanged();
            }
        }

        public IZone GetById(int id)
        {
            return _manager.GetById(id);
        }

        public TuningZone GetByIdTuning(int id)
        {
            foreach (TuningZone  tZone in _zones)
            {
                if (id == tZone.Zone.Id)
                    return tZone;
            }
            return null;
        }

        public void AddGroup(TuningGroup tuning)
        {
            _groups.Add(tuning);
        }

        public void RemoveGroup(TuningGroup tuning)
        {
            _groups.Remove(tuning);
        }

        public void AddZone(TuningZone tuning)
        {
            _zones.Add(tuning);
        }

        public void RemoveZone(TuningZone tuning)
        {
            _zones.Remove(tuning);
        }

        private void onVisibilityChanged()
        {
            VoidHandler handler = VisibilityChanged;
            if (null != handler)
                handler();
        }
    }
}
