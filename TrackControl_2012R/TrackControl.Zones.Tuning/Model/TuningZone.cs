using System;
using System.Collections.Generic;
using System.Drawing;
using TrackControl.General;
using TrackControl.Zones.Tuning.Properties;

namespace TrackControl.Zones.Tuning
{
    public class TuningZone : IZone
    {
        private readonly IZone _zone;
        private ZonesGroup _group;
        private string _name;
        private string _description;
        private bool _visible;
        private Color _color;
        private TuningStatus _status;
        private List<PointLatLng> _points;
        private RectLatLng _bounds;
        private double _areaKm;
        private bool _isGeometryChanged;
        private DateTime? _dateChange;
        private object _tag;
        private string _idOutLink;
        private bool _passagerYes = false;
        private ZoneCategory _category = null;
        private ZoneCategory2 _category2 = null;
        
        public bool PassagerCalc
        {
            get
            {
                return _passagerYes;
            }

            set
            {
                _passagerYes = value;
            }
        } // PassengerCalc
 
        // ���������/��������� ����������� ����� ���������� � ����������� ����
        public bool PassengerCalc
        {
            get
           {
                return _passagerYes;
            }

            set
            {
                _passagerYes = value;
                updateStatus();
            }
        } // PassengerCalc

        /// <summary>
        /// ��� ������� ����
        /// </summary>
        public string IdOutLink
        {
            get { return _idOutLink; }
            set
            {
                if (value != _idOutLink)
                {
                    _idOutLink = value;
                    updateStatus();
                }
            }
        }

        public event VoidHandler StatusChanged;

        public TuningZone(IZone zone)
        {
            if (null == zone)
                throw new ArgumentNullException("zone");

            _zone = zone;

            

            RefreshData();
        }

        public IZone Zone
        {
            get { return _zone; }
        }

        int IEntity.Id
        {
            get { return _zone.Id; }
            set { return; }
        }

        public int IdZone
        {
            get { return _zone.Id; }
        }

        public string Id
        {
            get
            {
                if (_zone.IsNew)
                    return "-";
                else
                    return _zone.Id.ToString();
            }
        }
        
        public bool IsNew
        {
            get { return _zone.IsNew; }
        }

        public string Name
        {
            get { return _name; }
            set
            {
                if (value != _name)
                {
                    _name = value;
                    updateStatus();
                }
            }
        }

        public object Category
        {
            get 
            { 
                return _category; 
            }
 
            set 
            { 
                _category = (ZoneCategory) value;
                updateStatus();
            }
        }

        public object Category2
        {
            get 
            { 
                return _category2; 
            }

            set 
            { 
                _category2 = ( ZoneCategory2 )value;
                updateStatus();
            }
        }

        public string Description
        {
            get { return _description; }
            set
            {
                if (value != _description)
                {
                    _description = value;
                    updateStatus();
                }
            }
        }

        public IZoneStyle Style
        {
            get { return _zone.Style; }
            set { return; }
        }

        public PointLatLng[] Points
        {
            get
            {
                return _points.ToArray();
            }

            set
            {
                if (null == value)
                    throw new ZonePointsNullException();
                if (value.Length < 3)
                    throw new NotEnoughPointsException();

                _points = new List<PointLatLng>(value);
                _bounds = RectLatLng.Calculate(_points);
                _areaKm = �GeoDistance.CalculateArea(_points);
                //_dateChange = DateTime.Now; 
                _isGeometryChanged = true;
                updateStatus();
            }
        }

        public RectLatLng Bounds
        {
            get { return _bounds; }
        }

        public bool IsGeometryChanged
        {
            get { return _isGeometryChanged; }
            set { return; }
        }

        public string GeometryInfo
        {
            get { return String.Format("{0} " + Resources.TuningZoneGeometryInfo, _points.Count); }
        }

        public string AreaInfo
        {
            get
            {
                return String.Format("{0:N3} " + Resources.TuningZoneAreaInfoGectar +
                                     " ({1:N3} " + Resources.TuningZoneAreaInfoKilemetreSqrt + ")", AreaGa, AreaKm);
            }
        }

        public double AreaKm
        {
            get { return _areaKm; }
            set { _areaKm = value; }
        }

        public double AreaGa
        {
            get { return AreaKm * 100.0; }
            set { AreaKm = value / 100.0; }
        }

        public DateTime? DateChange
        {
            get { return _dateChange; }
            set
            {
                if (value != _dateChange)
                {
                    _dateChange = value;
                    updateStatus();
                }
            }
        }

        public ZonesGroup Group
        {
            get { return _group; }
            set
            {
                if (value != _group)
                {
                    _group = value;
                    updateStatus();
                }
            }
        }

        public TuningStatus Status
        {
            get { return _status; }
            set { _status = value; }
        }

        public Image Icon
        {
            get { return _zone.Style.Icon; }
        }

        public Color Color
        {
            get { return _color; }
            set
            {
                if (value != _color)
                {
                    _color = value;
                    updateStatus();
                }
            }
        }

        public bool Visible
        {
            get { return _visible; }
            set
            {
                if (value != _visible)
                    _visible = value;
            }
        }

        public object Tag
        {
            get { return _tag; }
            set { _tag = value; }
        }

        public void Bind()
        {
            _zone.Tag = this;
        }

        public void UnBind()
        {
            _zone.Tag = null;
        }

        public void RefreshData()
        {
            _name = _zone.Name;
            _description = _zone.Description;
            _group = _zone.Group;
            //_icon = _zone.Style.Icon;
            _color = _zone.Style.Color;
            _points = new List<PointLatLng>(_zone.Points);
            _bounds = _zone.Bounds;
            _areaKm = _zone.AreaKm;
            _dateChange = _zone.DateChange;
            _isGeometryChanged = false;
            _status = _zone.IsNew ? TuningStatus.NewAdded : TuningStatus.Ok;
            _idOutLink = _zone.IdOutLink;
            _category = (ZoneCategory) _zone.Category;
            _category2 = (ZoneCategory2) _zone.Category2;
        }

        public bool Contains(PointLatLng point)
        {
            return ZonesHelper.DoesZoneContainPoint(this, point);
        }

        public bool BoundContains(PointLatLng point)
        {
            return this.Bounds.Contains(point);
        }

        public override string ToString()
        {
            return _name;
        }

        private void updateStatus()
        {
            if (TuningStatus.Ok != _status &&
                _name == _zone.Name && _color == _zone.Style.Color &&
                _description == _zone.Description && _group == _zone.Group && _idOutLink == _zone.IdOutLink &&
                !_isGeometryChanged && _passagerYes == _zone.PassengerCalc && _category == _zone.Category && 
                _category2 == _zone.Category2)
            {
                _status = _zone.IsNew ? TuningStatus.NewAdded : TuningStatus.Ok;
                onStatusChanged();
            }
            else if (TuningStatus.NewAdded != _status &&
                     (_name != _zone.Name ||
                      _description != _zone.Description ||
                      _group != _zone.Group || _color != _zone.Style.Color || _idOutLink != _zone.IdOutLink ||
                      _isGeometryChanged || _passagerYes != _zone.PassengerCalc || _category != _zone.Category || _category2 != _zone.Category2 ) )
            {
                _status = TuningStatus.NotSaved;
                onStatusChanged();
            }
        }

        private void onStatusChanged()
        {
            VoidHandler handler = StatusChanged;

            if (null != handler)
                handler();
        }
        public string GetCultureActive()
        {
            return "";
        }
    }
}
