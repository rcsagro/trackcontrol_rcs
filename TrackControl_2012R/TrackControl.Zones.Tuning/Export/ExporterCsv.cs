﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
using TrackControl.Zones.Tuning.Properties   ;
using TrackControl.General;


namespace TrackControl.Zones.Tuning
{
    public class ExporterCsv
    {
        public string FileName { get; set; }

        public bool Write(IList<IZone> zones)
        {
            FileName = string.Format("Zones_{0}.txt", StaticMethods.SuffixDateToFileName());
            FileName = StaticMethods.ShowSaveFileDialog(FileName, Resources.CSVSave, "TXT |*.txt");
            if (FileName.Length == 0) return false;
            
            try
            {

                using (var writer = new StreamWriter(FileName, true))
                {
                    writer.WriteLine("id;NameGroup;Name;wkt"); 
                    foreach (IZone zone in zones)
                    {
                        WriteZone(zone, writer);
                    }
                }

            }
            catch
            {
                return false;
            }
            return true;
        }

        bool WriteZone(IZone zone,  StreamWriter writer)
        {
            try
            {
                var builder = new StringBuilder("POLYGON ((");

                foreach (PointLatLng pll in zone.Points)
                {
                    builder.Append(string.Format("{0} {1},", pll.Lng.ToString().Replace(",", "."), pll.Lat.ToString().Replace(",", ".")));
                }
                PointLatLng firstPoint = zone.Points[0];
                builder.Append(string.Format("{0} {1}))", firstPoint.Lng.ToString().Replace(",", "."),
                                                 firstPoint.Lat.ToString().Replace(",", ".")));
                string write = string.Format("{0};{1};{2};{3}", zone.Id, zone.Group.Name , zone.Name, builder.ToString());
                writer.WriteLine(write); 
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
