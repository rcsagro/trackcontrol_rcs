using DevExpress.XtraEditors;

using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

using TrackControl.General;
using TrackControl.Zones.Tuning.Properties;
using TrackControl.Zones;

namespace TrackControl.Zones.Tuning
{
    public class ZonesTuningMode : ITuning
    {
        private bool _released;

        private IZonesManager _zonesManager;
        private TuningZonesModel _tuningZonesModel;
        private IZonesEditorMap _map;
        private ImportController _importer;

        private TuningView _view;
        private ZonesTree _tree;
        private TuningGroupView _groupView;
        private TuningZoneView _zoneView;

        private ZonesGroup _newGroup;
        private IZone _newZone;
        public delegate void ZonesPassager(int id, bool state);
        public static ZonesPassager SetingsZonesPassager;
        private const int HeightTuningViewPanel = 355; // ������ _tuningViewPanel in px

        public ZonesTuningMode(IZonesManager zonesManager, IZonesEditorMap map)
        {
            _released = true;
            _zonesManager = zonesManager;
            _tuningZonesModel = new TuningZonesModel(_zonesManager);
            _tuningZonesModel.Activate();
            _tuningZonesModel.VisibilityChanged += zonesVisibilityChanged;
            _importer = new ImportController(_zonesManager);
            _importer.HideSelected += importerHideSelected;

            _importer.Start();
            _map = map;
            init();
        }

        #region ITuning Members

        public string Caption
        {
            get { return Resources.UI_ZTModeCZ; }
        }

        public Image Icon
        {
            get { return Shared.Editor; }
        }

        public Control View
        {
            get
            {
                if (null == _view)
                    throw new Exception("View can not be NULL. Probably not been called on the Advise()");

                return _view;
            }
        }

        public void Advise()
        {
            _tuningZonesModel.Activate();
            _map.IsZoneEditor = true;
            _map.EditorZonesOn = true;
            _map.ZoneSelected += map_ZoneSelected;

            _view = new TuningView();
            _view.SetSplitPosition(HeightTuningViewPanel);
            _view.ShowTree(_tree);
            _view.ShowMap((Control) _map);
            _view.SetImporter(_importer.View, _importer.Visible);
            _view.AddGroupClicked += addGroup;
            _view.AddZoneClicked += addZone;
            _view.DeleteCheckedClicked += deleteChecked;
            _view.WriteZonesToXML += writeZonesToXML;
            _view.ReadZonesFromXML += readZonesFromXML;
            _view.ExportZonesToCsv += exportZonesToCsv;
            _view.ImporterVisibilityChanged += importerVisibilityChanged;
            _view.ReducePointsSelectedZone += reducePointsSelectedZone;

            setView(_tree.Current);

            zonesVisibilityChanged();

            _released = false;

            if (_map.PointsFromTrack.Count > 0)
            {
                addZone();
            }

            _tree.TreeLoaded += checkZoneFromTrack;
        }

        public void Release()
        {
            if (_released) 
                return;

            _released = true;

            if (_map.PointsFromTrack.Count > 0) _map.PointsFromTrack.Clear();

            if (null != _newZone)
            {
                _newZone.Group.RemoveZone(_newZone);
                _newZone = null;
                _tree.RefreshTree();
            }

            _tuningZonesModel.DeActivate();
            _tree.Parent = null;

            _map.IsZoneEditor = false;
            _map.EditorZonesOn = false;

            _map.ReleaseEditor();

            _map.ZoneSelected -= map_ZoneSelected;
            Control map = (Control) _map;
            map.Parent = null;

            _groupView.Parent = null;
            _zoneView.Parent = null;
            _importer.View.Parent = null;

            _view.AddGroupClicked -= addGroup;
            _view.AddZoneClicked -= addZone;
            _view.DeleteCheckedClicked -= deleteChecked;
            _view.ImporterVisibilityChanged -= importerVisibilityChanged;
            _tree.TreeLoaded -= checkZoneFromTrack;
            _view.ReducePointsSelectedZone -= reducePointsSelectedZone;
            _view.Parent = null;
            _view.Dispose();
            _view = null;
        }

        #endregion

        private void init()
        {
            _tree = new ZonesTree(_zonesManager);
            _tree.VisibilityChanged += _tuningZonesModel.ChangeVisibility;
            _tree.ZoneNodeDoubleClicked += showOnMap;
            _tree.PanZoneClicked += showOnMap;
            _tree.RemoveZoneClicked += deleteZone;
            _tree.RemoveGroupOnlyClicked += deleteGroupOnly;
            _tree.SelectionChanged += setView;
            _tree.UpdateFromMifClicked += updateFromMif;

            _groupView = new TuningGroupView();
            _groupView.SaveClicked += saveGroup;
            _groupView.CancelClicked += cancelGroup;
            _groupView.DeleteGroupOnlyClicked += deleteGroupOnly;
            _groupView.DeleteGroupFullyClicked += deleteGroupFully;

            _zoneView = new TuningZoneView();
            _zoneView.ShowOnMapClicked += showOnMap;
            _zoneView.SaveClicked += saveZone;
            _zoneView.CancelClicked += cancelZone;
            _zoneView.DeleteClicked += deleteZone;
        }

        private void importerVisibilityChanged(bool visible)
        {
            _importer.Visible = visible;

            if (!visible && _importer.IsZonesCreated)
            {
                saveZonesFromMIF(_importer.Zones);
                _importer.IsZonesCreated = false;
            }
        }

        private void importerHideSelected()
        {
            if (_released) return;
            if (_view.InvokeRequired)
            {
                MethodInvoker m = delegate
                {
                    _view.HideImporter();
                };
                _view.Invoke(m);
            }
            else
            {
                _view.HideImporter();
            }
        }

        private void reducePointsSelectedZone()
        {
            if (_tuningZonesModel.Checked.Count > 0)
            {
                ReducePointsQty formRed = new ReducePointsQty(_tuningZonesModel.Checked[0]);
                formRed.ShowDialog();
            }
            else
                XtraMessageBox.Show(Resources.SelectCheckZone);
        }


        private void map_ZoneSelected(IZone zone)
        {

            TuningZone tuning = zone as TuningZone;
            if (null == tuning)
            {
                tuning = zone.Tag as TuningZone;
            }
            if (null == tuning) return;
            _tree.SetCurrentAndJumpTo(tuning.Zone);
            _tree.SetZoneChecked(zone);
            _map.SelectZone(zone);
            ((Control) _map).Focus();
        }

        private void zonesVisibilityChanged()
        {
            _map.ClearZones();
            IList<IZone> checkedZones = _tuningZonesModel.Checked;
            _map.AddZones(checkedZones);
        }

        private void showOnMap(IZone zone)
        {
            _tree.SetZoneChecked(zone);
            if (zone.Points.Length >= 3)
                _map.PanTo(zone.Bounds);
        }

        private void setView(IEntity entity)
        {
            if (entity is ZonesGroup)
            {
                TuningGroup tuning = (TuningGroup) ((ZonesGroup) entity).Tag;

                _groupView.Assign(tuning);

                _view.ShowTuning(_groupView);
                _map.ReleaseEditor();
            }
            else if (entity is IZone)
            {
                TuningZone tuning;

                if (((IZone) entity).Tag == null)
                {
                    tuning = new TuningZone((IZone) entity);
                }
                else
                {
                    tuning = (TuningZone) ((IZone) entity).Tag;
                }

                _zoneView.SetGroups(_zonesManager.Groups.ToArray());

                _zoneView.Assign(tuning);

                _view.ShowTuning(_zoneView);
                
                if (tuning.IsNew && tuning.Points.Length < 3)
                {
                    _map.SelectZone(tuning);
                    _map.CreateNewZone(tuning);
                }
                else
                {
                    _map.ReleaseEditor();

                    if (tuning.Visible)
                        _map.SelectZone(tuning);
                }
            }
        }

        private void addGroup()
        {
            if (null == _newGroup)
            {
                _newGroup = new ZonesGroup(Resources.UI_ZTModeNewGroup, "");
                TuningGroup tuning = new TuningGroup(_newGroup);
                tuning.Bind();
            }
            setView(_newGroup);
        }

        private void saveGroup(TuningGroup tuning)
        {
            ZonesGroup group = tuning.Group;
            if (group.IsRoot)
                throw new Exception(Resources.UI_ZTModeNotEditNodeException);

            group.Name = tuning.Name;
            group.Description = tuning.Description;
            group.IdOutLink = tuning.IdOutLink;

            if (group.IsNew)
            {
                _tuningZonesModel.AddGroup(tuning);
                _zonesManager.AddGroup(group);
                _newGroup = null;
            }

            _zonesManager.SaveGroup(group);
            refreshGroup(tuning);
            _tree.RefreshTree();
        }

        private void cancelGroup(TuningGroup tuning)
        {
            ZonesGroup group = tuning.Group;
            if (group.IsNew)
            {
                tuning.UnBind();
                _newGroup = null;
                setView(_tree.Current);
            }
            else
            {
                refreshGroup(tuning);
            }
        }

        private void deleteGroupOnly(ZonesGroup group)
        {
            if (group.IsRoot)
                throw new Exception(Resources.UI_ZTModeNotDeleteRootException);

            TuningGroup tuning = (TuningGroup) group.Tag;
            if (DialogResult.OK ==
                XtraMessageBox.Show(_view, Resources.UI_ZTModeDeleteGroupWarning,
                    String.Format(Resources.UI_ZTModeDeleteGroup + " \"{0}\"", group.Name), MessageBoxButtons.OKCancel,
                    MessageBoxIcon.Question))
            {
                if (group.IsNew)
                {
                    cancelGroup(tuning);
                }
                else
                {
                    tuning.UnBind();
                    _tuningZonesModel.RemoveGroup(tuning);
                    _zonesManager.RemoveGroupOnly(group);
                    _tree.RefreshTree();
                }
                setView(_tree.Current);
            }
        }

        private void deleteGroupFully(TuningGroup tuning)
        {
            ZonesGroup group = tuning.Group;
            if (DialogResult.OK ==
                XtraMessageBox.Show(_view, Resources.UI_ZTModeAskDeleteFullGroup,
                    String.Format(Resources.UI_ZTModeDeleteFullGroup + " \"{0}\"", group.Name),
                    MessageBoxButtons.OKCancel, MessageBoxIcon.Question))
            {
                tuning.UnBind();
                _tuningZonesModel.RemoveGroup(tuning);

                foreach (IZone zone in group.AllItems)
                {
                    TuningZone tz = (TuningZone) zone.Tag;

                    tz.UnBind();
                    _tuningZonesModel.RemoveZone(tz);
                }
                _zonesManager.RemoveGroupFully(group);
                _tree.RefreshTree();
                zonesVisibilityChanged();
                setView(_tree.Current);
            }
        }

        private void addZone()
        {
            zonesVisibilityChanged();

            if (null == _newZone)
            {
                _newZone = Zone.CreateNew(_zonesManager.Root);
                TuningZone tuning = new TuningZone(_newZone);
                tuning.Visible = true;
                tuning.Bind();
                _tuningZonesModel.AddZone(tuning);
                _map.IsZoneEditor = true;
            }

            setView(_newZone);
            TuningZone tz = _newZone.Tag as TuningZone;

            if (_map.PointsFromTrack.Count > 0)
            {
                tz.Points = _map.PointsFromTrack.ToArray();
            }

            if (tz.Points.Length < 3)
                _map.CreateNewZone(tz);
            else
                _map.PanTo(tz.Bounds);

            _tree.RefreshTree();
            _tree.SetCurrentAndJumpTo(_newZone);
        }

        private void saveZone(TuningZone tuning)
        {
            bool isNew = tuning.IsNew;

            if (isNew && tuning.Points.Length < 3)
            {
                XtraMessageBox.Show("��������� ���� �� ����������.");
                return;
            }

            IZone zone = tuning.Zone;

            zone.Name = tuning.Name;
            zone.Description = tuning.Description;
            zone.Group = tuning.Group;
            zone.Style.Color = tuning.Color;
            zone.Category = tuning.Category;
            zone.Category2 = tuning.Category2;

            // ��������� ���������� � ���������� ����������� ���������� � ���� ����
            if (zone.PassengerCalc != tuning.PassengerCalc)
            {
                try
                {
                    zone.PassengerCalc = tuning.PassengerCalc;

                    if (SetingsZonesPassager != null)
                        SetingsZonesPassager(tuning.IdZone, tuning.PassengerCalc);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show(ex.Message + "\n" + ex.StackTrace, "Error ZonesTuningMode", MessageBoxButtons.OK);
                }
            }

            if (tuning.IsGeometryChanged)
            {
                zone.Points = tuning.Points;
                zone.IsGeometryChanged = true;
                zone.DateChange = DateTime.Now;
            }

            zone.IdOutLink = tuning.IdOutLink;
            _zonesManager.SaveZone(zone);
            tuning.RefreshData();

            if (isNew)
            {
                _newZone = null;
                _zonesManager.AddZones(new IZone[] {zone});
            }
            //(_tree.Current as IZone).Name  = tuning.Name;

            //_tree.RefreshTree();
            _tree.UpdateZone(zone);
            _tree.SetZoneChecked(zone);
            _tree.SetCurrentAndJumpTo(zone);

            if ((IZone) zone.Tag == null)
            {
                _map.SelectZone((IZone) zone);
            }
            else
            {
                _map.SelectZone((IZone) zone.Tag);
            }

            // �������������� ������ ��������� ��������

        } // saveZone

        private void cancelZone(TuningZone tuning)
        {
            IZone zone = tuning.Zone;
            if (zone.IsNew)
            {
                tuning.UnBind();
                _tuningZonesModel.RemoveZone(tuning);
                zone.Group.RemoveZone(zone);
                _tree.RefreshTree();
                setView(_tree.Current);
                _newZone = null;
            }
            else
            {
                refreshZone(tuning);
            }
            if (tuning.Visible)
                zonesVisibilityChanged();
        }

        private void deleteZone(IZone zone)
        {
            TuningZone tuning;
            if (zone.Tag == null)
            {
                tuning = new TuningZone(zone);
            }
            else
            {
                tuning = (TuningZone) zone.Tag;
            }

            if (DialogResult.OK ==
                XtraMessageBox.Show(Resources.UI_ZTModeAskDeleteZone,
                    String.Format(Resources.UI_ZTModeDeleteZone + " \"{0}\"", zone.Name), MessageBoxButtons.OKCancel,
                    MessageBoxIcon.Question))
            {
                if (zone.IsNew)
                {
                    cancelZone(tuning);
                }
                else
                {
                    tuning.UnBind();
                    _tuningZonesModel.RemoveZone(tuning);
                    _zonesManager.RemoveZone(zone);
                    _tree.RefreshTree();
                }
                setView(_tree.Current);
                if (tuning.Visible)
                    zonesVisibilityChanged();
            }
        }

        private void updateFromMif(IZone zone)
        {
            using (MifPointsFinder pfinder = new MifPointsFinder())
            {
                if (pfinder.FindPoints())
                {
                    IZone zone_from = new Zone("MIF", Resources.UI_TuningViewImportMIF, new ZonesGroup("test", "test"), zone.Style,
                        DateTime.Now, "", null, null);

                    zone_from.Points = pfinder.Points.ToArray();
                    zone_from.IsGeometryChanged = true;
                    CompareZones cz = new CompareZones();
                    cz.SetZones(zone_from, zone);
                    if (DialogResult.OK == cz.ShowDialog())
                    {
                        zone.Points = zone_from.Points;
                        zone.IsGeometryChanged = true;
                        _zonesManager.SaveZone(zone);
                        TuningZone tuningExist = _tuningZonesModel.GetByIdTuning(zone.Id);
                        if (tuningExist != null)
                        {
                            tuningExist.Points = zone.Points;
                            tuningExist.IsGeometryChanged = true;
                            tuningExist.RefreshData();
                            tuningExist.DateChange = DateTime.Now;
                            setView(zone);
                        }
                    }
                }
                else
                    XtraMessageBox.Show(pfinder.ErrorMessage);
            }

        }

        private void deleteChecked()
        {
            List<ZonesGroup> groupsForDelete = new List<ZonesGroup>();
            foreach (TuningGroup tuning in _tuningZonesModel.Groups)
            {
                if (tuning.Visible)
                    groupsForDelete.Add(tuning.Group);
            }
        }

        private void refreshGroup(TuningGroup tuning)
        {
            tuning.RefreshData();
            _groupView.Assign(tuning);
            _view.ShowTuning(_groupView);
        }

        private void refreshZone(TuningZone tuning)
        {
            tuning.RefreshData();
            _zoneView.Assign(tuning);
            _view.ShowTuning(_zoneView);
        }

        private void checkZoneFromTrack()
        {
            if (_map.PointsFromTrack.Count > 0)
            {
                if (_newZone != null)
                {
                    _tree.SetCurrentAndJumpTo(_newZone);
                    showOnMap(_newZone);
                }
            }
        }

        private void writeZonesToXML()
        {
            if (_tuningZonesModel.Checked.Count > 0)
            {
                if (DialogResult.Yes ==
                    XtraMessageBox.Show(Resources.XMLSaveQuestion, Resources.XMLSave, MessageBoxButtons.YesNo))
                {
                    ZonesXML writer = new ZonesXML();
                    if (writer.Write(_tuningZonesModel.Checked))
                        XtraMessageBox.Show(String.Format("{0} {1}", Resources.XMLSaveFile, writer.FileName),
                            Resources.XMLSave);
                }
            }
        }

        private void exportZonesToCsv()
        {
            if (_tuningZonesModel.Checked.Count > 0)
            {
                if (DialogResult.Yes ==
                    XtraMessageBox.Show(Resources.CSVSaveQuestion, Resources.CSVSave, MessageBoxButtons.YesNo))
                {
                    var writer = new ExporterCsv();
                    if (writer.Write(_tuningZonesModel.Checked))
                        XtraMessageBox.Show(String.Format("{0} {1}", Resources.CSVSaveFile, writer.FileName),
                            Resources.CSVSave);
                }
            }
        }

        private void readZonesFromXML()
        {
            if (DialogResult.Yes ==
                XtraMessageBox.Show(Resources.XMLReadQuestion, Resources.XMLRead, MessageBoxButtons.YesNo))
            {
                ZonesXML reader = new ZonesXML();
                List<IZone> zones = reader.Read();
                if (zones.Count > 0)
                {
                    ZonesSelect zs = new ZonesSelect(zones);
                    zs.SaveZones += saveZonesFromXML;
                    zs.Show();
                }
            }
        }

        private void saveZonesFromXML(IEnumerable<IZone> zones)
        {
            int cntUpdate = 0;
            int cntAdd = 0;
            foreach (IZone zone in zones)
            {
                zone.Tag = null;
                ZonesGroup zg = _zonesManager.GetGroupeByName(zone.Group.Name);
                if (zg == null)
                {
                    addXMLGroup(zone);
                    zg = _zonesManager.GetGroupeByName(zone.Group.Name);
                }
                zone.Id = -1;
                IZone zoneExist = _zonesManager.GetZoneByNameAndGroup(zone.Name, zg);
                if (zoneExist != null && zoneExist.Id > 0)
                {
                    updateXMLZone(zone, zg, zoneExist);
                    cntUpdate++;
                }
                else
                {
                    addXMLzone(zone, zg);
                    cntAdd++;
                }
                _zonesManager.SaveZone(zone);
                Application.DoEvents();
            }
            _zonesManager.AddZones(zones);
            XtraMessageBox.Show(
                string.Format("{3}: {0}{1}{4} {2}", cntAdd, Environment.NewLine, cntUpdate, Resources.ZonesAdded,
                    Resources.ZonesEdited),
                "Import XML");
            _tree.RefreshTree();

        }

        private void addXMLzone(IZone zone, ZonesGroup zg)
        {
            zone.Group = zg;
            addModelzone(zone);
        }

        private void updateXMLZone(IZone zone, ZonesGroup zg, IZone zoneExist)
        {
            zone.Id = zoneExist.Id;
            zone.Group.Id = zg.Id == -1 ? 1 : zg.Id;
            zoneExist.Style.Color = zone.Style.Color;
            zoneExist.Description = zone.Description;
            zoneExist.Points = zone.Points;
            TuningZone tuningExist = _tuningZonesModel.GetByIdTuning(zoneExist.Id);
            if (tuningExist != null) tuningExist.RefreshData();
        }

        private void addXMLGroup(IZone zone)
        {
            _zonesManager.AddGroup(zone.Group);
            zone.Group.Id = -1;
            _zonesManager.SaveGroup(zone.Group);
            TuningGroup tuningG = new TuningGroup(zone.Group);
            tuningG.Bind();
            _tuningZonesModel.AddGroup(tuningG);
        }

        private void saveZonesFromMIF(IEnumerable<IZone> zones)
        {
            foreach (IZone zone in zones)
            {
                addModelzone(zone);
            }
            _zonesManager.AddZones(zones);
            _tree.RefreshTree();
        }

        private void addModelzone(IZone zone)
        {
            TuningZone tuning = new TuningZone(zone);
            tuning.Visible = true;
            tuning.Bind();

            _tuningZonesModel.AddZone(tuning);

            tuning.Visible = false;
        }
    }
}
