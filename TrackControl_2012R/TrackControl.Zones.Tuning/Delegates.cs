using System;
using TrackControl.General;

namespace TrackControl.Zones.Tuning
{
  public delegate void ImportComplete(string name, string description, PointLatLng[] points);
}
