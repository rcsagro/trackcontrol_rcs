﻿namespace TrackControl.Zones.Tuning
{
    partial class ZoneProperty
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pgZone = new DevExpress.XtraVerticalGrid.PropertyGridControl();
            this._descriptionRepo = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this._nameRepo = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this._groupRepo = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this._colorRepo = new DevExpress.XtraEditors.Repository.RepositoryItemColorEdit();
            this.zbeCategory = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.zbeCategory2 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this._idRow = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this._nameCol = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this._descriptionRow = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this._groupCol = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this._colorRow = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this._geometryCol = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this._areaCol = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this._dateChange = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this._ezCategory = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this._ezCategory2 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.peZone = new DevExpress.XtraEditors.PictureEdit();
            ((System.ComponentModel.ISupportInitialize)(this.pgZone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._descriptionRepo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._nameRepo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._groupRepo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._colorRepo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.zbeCategory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.zbeCategory2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.peZone.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // pgZone
            // 
            this.pgZone.Location = new System.Drawing.Point(0, 3);
            this.pgZone.Name = "pgZone";
            this.pgZone.RecordWidth = 93;
            this.pgZone.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this._descriptionRepo,
            this._nameRepo,
            this._groupRepo,
            this._colorRepo,
            this.zbeCategory,
            this.zbeCategory2});
            this.pgZone.RowHeaderWidth = 107;
            this.pgZone.Rows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this._idRow,
            this._nameCol,
            this._descriptionRow,
            this._groupCol,
            this._colorRow,
            this._geometryCol,
            this._areaCol,
            this._dateChange,
            this._ezCategory,
            this._ezCategory2});
            this.pgZone.Size = new System.Drawing.Size(443, 170);
            this.pgZone.TabIndex = 5;
            // 
            // _descriptionRepo
            // 
            this._descriptionRepo.MaxLength = 200;
            this._descriptionRepo.Name = "_descriptionRepo";
            // 
            // _nameRepo
            // 
            this._nameRepo.AutoHeight = false;
            this._nameRepo.MaxLength = 50;
            this._nameRepo.Name = "_nameRepo";
            // 
            // _groupRepo
            // 
            this._groupRepo.AutoHeight = false;
            this._groupRepo.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this._groupRepo.Name = "_groupRepo";
            this._groupRepo.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // _colorRepo
            // 
            this._colorRepo.AllowFocused = false;
            this._colorRepo.AutoHeight = false;
            this._colorRepo.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this._colorRepo.Name = "_colorRepo";
            this._colorRepo.ShowSystemColors = false;
            // 
            // zbeCategory
            // 
            this.zbeCategory.AutoHeight = false;
            this.zbeCategory.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.zbeCategory.Name = "zbeCategory";
            this.zbeCategory.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // zbeCategory2
            // 
            this.zbeCategory2.AutoHeight = false;
            this.zbeCategory2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.zbeCategory2.Name = "zbeCategory2";
            this.zbeCategory2.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // _idRow
            // 
            this._idRow.Appearance.Options.UseTextOptions = true;
            this._idRow.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this._idRow.Name = "_idRow";
            this._idRow.OptionsRow.AllowFocus = false;
            this._idRow.OptionsRow.AllowMove = false;
            this._idRow.OptionsRow.AllowMoveToCustomizationForm = false;
            this._idRow.OptionsRow.AllowSize = false;
            this._idRow.OptionsRow.DblClickExpanding = false;
            this._idRow.OptionsRow.ShowInCustomizationForm = false;
            this._idRow.Properties.Caption = "ID в таблице БД";
            this._idRow.Properties.FieldName = "Id";
            this._idRow.Properties.ReadOnly = true;
            // 
            // _nameCol
            // 
            this._nameCol.Name = "_nameCol";
            this._nameCol.OptionsRow.AllowMove = false;
            this._nameCol.OptionsRow.AllowMoveToCustomizationForm = false;
            this._nameCol.OptionsRow.AllowSize = false;
            this._nameCol.OptionsRow.DblClickExpanding = false;
            this._nameCol.OptionsRow.ShowInCustomizationForm = false;
            this._nameCol.Properties.Caption = "Название";
            this._nameCol.Properties.FieldName = "Name";
            this._nameCol.Properties.ReadOnly = true;
            // 
            // _descriptionRow
            // 
            this._descriptionRow.Height = 40;
            this._descriptionRow.Name = "_descriptionRow";
            this._descriptionRow.OptionsRow.AllowMove = false;
            this._descriptionRow.OptionsRow.AllowMoveToCustomizationForm = false;
            this._descriptionRow.OptionsRow.AllowSize = false;
            this._descriptionRow.OptionsRow.DblClickExpanding = false;
            this._descriptionRow.OptionsRow.ShowInCustomizationForm = false;
            this._descriptionRow.Properties.Caption = "Описание";
            this._descriptionRow.Properties.FieldName = "Description";
            this._descriptionRow.Properties.ReadOnly = true;
            // 
            // _groupCol
            // 
            this._groupCol.Name = "_groupCol";
            this._groupCol.OptionsRow.AllowMove = false;
            this._groupCol.OptionsRow.AllowMoveToCustomizationForm = false;
            this._groupCol.OptionsRow.AllowSize = false;
            this._groupCol.OptionsRow.DblClickExpanding = false;
            this._groupCol.OptionsRow.ShowInCustomizationForm = false;
            this._groupCol.Properties.Caption = "Группа";
            this._groupCol.Properties.FieldName = "Group.Name";
            this._groupCol.Properties.ReadOnly = true;
            // 
            // _colorRow
            // 
            this._colorRow.Name = "_colorRow";
            this._colorRow.OptionsRow.AllowMove = false;
            this._colorRow.OptionsRow.AllowMoveToCustomizationForm = false;
            this._colorRow.OptionsRow.AllowSize = false;
            this._colorRow.OptionsRow.DblClickExpanding = false;
            this._colorRow.OptionsRow.ShowInCustomizationForm = false;
            this._colorRow.Properties.Caption = "Цвет заливки";
            this._colorRow.Properties.FieldName = "Style.Color";
            this._colorRow.Properties.ReadOnly = true;
            this._colorRow.Properties.RowEdit = this._colorRepo;
            // 
            // _geometryCol
            // 
            this._geometryCol.Appearance.Options.UseTextOptions = true;
            this._geometryCol.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this._geometryCol.Name = "_geometryCol";
            this._geometryCol.OptionsRow.AllowFocus = false;
            this._geometryCol.OptionsRow.AllowMove = false;
            this._geometryCol.OptionsRow.AllowMoveToCustomizationForm = false;
            this._geometryCol.OptionsRow.AllowSize = false;
            this._geometryCol.OptionsRow.DblClickExpanding = false;
            this._geometryCol.OptionsRow.ShowInCustomizationForm = false;
            this._geometryCol.Properties.Caption = "Геометрия";
            this._geometryCol.Properties.FieldName = "GeometryInfo";
            this._geometryCol.Properties.ReadOnly = true;
            // 
            // _areaCol
            // 
            this._areaCol.Appearance.Options.UseTextOptions = true;
            this._areaCol.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this._areaCol.Name = "_areaCol";
            this._areaCol.OptionsRow.AllowFocus = false;
            this._areaCol.OptionsRow.AllowMove = false;
            this._areaCol.OptionsRow.AllowMoveToCustomizationForm = false;
            this._areaCol.OptionsRow.AllowSize = false;
            this._areaCol.OptionsRow.DblClickExpanding = false;
            this._areaCol.OptionsRow.ShowInCustomizationForm = false;
            this._areaCol.Properties.Caption = "Площадь";
            this._areaCol.Properties.FieldName = "AreaInfo";
            this._areaCol.Properties.ReadOnly = true;
            // 
            // _dateChange
            // 
            this._dateChange.Appearance.Options.UseTextOptions = true;
            this._dateChange.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this._dateChange.Name = "_dateChange";
            this._dateChange.OptionsRow.AllowFocus = false;
            this._dateChange.OptionsRow.AllowMove = false;
            this._dateChange.OptionsRow.AllowMoveToCustomizationForm = false;
            this._dateChange.OptionsRow.AllowSize = false;
            this._dateChange.OptionsRow.DblClickExpanding = false;
            this._dateChange.OptionsRow.ShowInCustomizationForm = false;
            this._dateChange.Properties.Caption = "Дата последнего изменения геометрии";
            this._dateChange.Properties.FieldName = "DateChange";
            this._dateChange.Properties.ReadOnly = true;
            // 
            // _ezCategory
            // 
            this._ezCategory.Name = "_ezCategory";
            this._ezCategory.OptionsRow.AllowFocus = false;
            this._ezCategory.OptionsRow.AllowMove = false;
            this._ezCategory.OptionsRow.AllowMoveToCustomizationForm = false;
            this._ezCategory.OptionsRow.AllowSize = false;
            this._ezCategory.OptionsRow.DblClickExpanding = false;
            this._ezCategory.Properties.Caption = "Категория";
            this._ezCategory.Properties.FieldName = "Category";
            this._ezCategory.Properties.RowEdit = this.zbeCategory;
            this._ezCategory.Properties.ToolTip = "Категория контрольной зоны";
            // 
            // _ezCategory2
            // 
            this._ezCategory2.Name = "_ezCategory2";
            this._ezCategory2.OptionsRow.AllowMove = false;
            this._ezCategory2.OptionsRow.AllowMoveToCustomizationForm = false;
            this._ezCategory2.OptionsRow.AllowSize = false;
            this._ezCategory2.OptionsRow.DblClickExpanding = false;
            this._ezCategory2.Properties.Caption = "Категория2";
            this._ezCategory2.Properties.FieldName = "Category2";
            this._ezCategory2.Properties.RowEdit = this.zbeCategory2;
            this._ezCategory2.Properties.ToolTip = "Категория 2 контрольной зоны";
            // 
            // peZone
            // 
            this.peZone.Location = new System.Drawing.Point(447, 3);
            this.peZone.Name = "peZone";
            this.peZone.Properties.NullText = "Контрольная зона";
            this.peZone.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            this.peZone.Size = new System.Drawing.Size(170, 170);
            this.peZone.TabIndex = 17;
            // 
            // ZoneProperty
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.peZone);
            this.Controls.Add(this.pgZone);
            this.Name = "ZoneProperty";
            this.Size = new System.Drawing.Size(624, 177);
            ((System.ComponentModel.ISupportInitialize)(this.pgZone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._descriptionRepo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._nameRepo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._groupRepo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._colorRepo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.zbeCategory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.zbeCategory2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.peZone.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraVerticalGrid.PropertyGridControl pgZone;
        private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit _descriptionRepo;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit _nameRepo;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox _groupRepo;
        private DevExpress.XtraEditors.Repository.RepositoryItemColorEdit _colorRepo;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow _idRow;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow _nameCol;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow _descriptionRow;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow _groupCol;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow _colorRow;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow _geometryCol;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow _areaCol;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow _dateChange;
        private DevExpress.XtraEditors.PictureEdit peZone;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox zbeCategory;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow _ezCategory;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox zbeCategory2;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow _ezCategory2;
    }
}
