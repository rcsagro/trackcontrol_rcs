﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using TrackControl.General;
using TrackControl.Zones.Tuning.Properties;

namespace TrackControl.Zones.Tuning.UI
{
    public partial class CategoriesEditor2 : DevExpress.XtraEditors.XtraForm
    {
        public event VoidHandler RefreshCategoriesList;
        bool _categoriesChanged;

        public CategoriesEditor2()
        {
            InitializeComponent();
            Localization();

            this.gcCategory2.DataSource = ZonesCategoryProvider.GetList2();
            this.gvCategory2.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler( this.gvCategory2_CellValueChanged );
            this.gcCategory2.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler( this.gcCategory2_EmbeddedNavigator_ButtonClick );
            this.gvCategory2.CustomDrawRowIndicator += new DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventHandler( this.gvCategory2_CustomDrawRowIndicator );
        }

        private void gvCategory2_CustomDrawRowIndicator( object sender, DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventArgs e )
        {
            if( e.Info.IsRowIndicator )
            {
                if( e.RowHandle >= 0 )
                    e.Info.DisplayText = ( e.RowHandle + 1 ).ToString();
            }
        }

        private void gcCategory2_EmbeddedNavigator_ButtonClick( object sender, NavigatorButtonClickEventArgs e )
        {
            if( e.Button.ButtonType == NavigatorButtonType.Remove )
            {
                e.Handled = !DeleteRowVehicleCategory();
                _categoriesChanged = !e.Handled;
            }
        }

        private void gvCategory2_CellValueChanged( object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e )
        {
            ZoneCategory2 zc = ( ZoneCategory2 )gvCategory2.GetRow( e.RowHandle );

            if( zc != null )
            {
                if( zc.Save() ) _categoriesChanged = true;
            }
        }

        bool DeleteRowVehicleCategory()
        {
            ZoneCategory2 zc = ( ZoneCategory2 )gvCategory2.GetRow( gvCategory2.FocusedRowHandle );

            if( zc != null )
            {
                int zones = ZonesCategoryProvider.CountVehiclesWithCategory( zc );

                if( zones > 0 )
                {
                    XtraMessageBox.Show( Resources.DeleteZones, string.Format( Resources.ZonesWithCategory, zones ) );
                    return false;
                }

                if( XtraMessageBox.Show( Resources.ConfirmDeleteQuestion, Resources.ConfirmDeletion,
                    MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question ) == DialogResult.Yes )
                {

                    return zc.Delete();
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        void Localization()
        {
            this.Text = string.Format( "{0}2", Resources.UI_ZonesCategoryTipBtn );
            colName.Caption = string.Format( "{0}2", Resources.UI_ZonesCategory );
        }

        private void CategoriesEditor2_FormClosed( object sender, FormClosedEventArgs e )
        {
            if( _categoriesChanged && RefreshCategoriesList != null )
                RefreshCategoriesList();
        }
    }
}