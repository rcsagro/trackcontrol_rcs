using DevExpress.XtraBars;
using DevExpress.XtraEditors;

using System;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using TrackControl.General;
using TrackControl.Zones.Tuning.Properties;
using TrackControl.Zones.Tuning.UI;

namespace TrackControl.Zones.Tuning
{
    public partial class TuningZoneView : XtraUserControl
    {
        private TuningZone _tuning;

        public event Action<IZone> ShowOnMapClicked;
        public event Action<TuningZone> SaveClicked;
        public event Action<TuningZone> CancelClicked;
        public event Action<IZone> DeleteClicked;

        public delegate bool getStatePassanger(int idZona);

        public static getStatePassanger statePassagerZone;

        public TuningZoneView()
        {
            InitializeComponent();
            init();

            cbzCategory.SelectedIndexChanged += CbzCategoryOnSelectedIndexChanged;
            cbzCategory2.SelectedIndexChanged += CbzCategory2OnSelectedIndexChanged;
        }

        private void CbzCategory2OnSelectedIndexChanged(object sender, EventArgs eventArgs)
        {
           _tuning.Status = TuningStatus.NotSaved;
           tuning_StatusChanged();
        }

        private void CbzCategoryOnSelectedIndexChanged(object sender, EventArgs eventArgs)
        {
            _tuning.Status = TuningStatus.NotSaved;
            tuning_StatusChanged();
        }
        
        public void Assign(TuningZone tuning)
        {
            if (null == tuning)
                throw new ArgumentNullException("tuning");

            if (null != _tuning)
                _tuning.StatusChanged -= tuning_StatusChanged;

            _tuning = tuning;
            _mainBtn.Glyph = _tuning.Icon;

            _tuning.Zone.PassagerCalc = statePassagerZone( _tuning.IdZone );
            _tuning.PassagerCalc = _tuning.Zone.PassagerCalc;

            _grid.SelectedObject = _tuning;

            setSaveCancelVisibility();

            _tuning.StatusChanged += tuning_StatusChanged;
            
            RefreshCategoriesList();
            RefreshCategoriesList2();
        }

        private void init()
        {
            this._tools.Text = Resources.UI_TuningTools;
            this._mainBtn.Caption = Resources.UI_TuningAction;
            this._showOnMapPopupBtn.Caption = Resources.UI_TuningZoneViewShowOnMap;
            this._deleteZonePopupBtn.Caption = Resources.UI_TuningZoneViewDeleteZone;
            this._showOnMapToolBtn.Caption = Resources.UI_TuningZoneViewShowOnMap;
            this._deleteZoneToolBtn.Caption = Resources.UI_TuningZoneViewDeleteZone;
            this._idRow.Properties.Caption = Resources.UI_TuningZoneViewID;
            this._nameCol.Properties.Caption = Resources.UI_TuningName;
            this._descriptionRow.Properties.Caption = Resources.UI_TuningDescription;
            this._groupCol.Properties.Caption = Resources.UI_TuningZoneViewGroup;
            this._colorRow.Properties.Caption = Resources.UI_TuningZoneViewStyle;
            this.znCategory.Properties.Caption = Resources.UI_ZonesCategory;
            this.znCategory2.Properties.Caption = Resources.UI_ZonesCategory2;
            this.znCategory.Properties.ToolTip = Resources.UI_ZonesCategoryTip;
            this.znCategory2.Properties.ToolTip = Resources.UI_ZonesCategoryTip2;
            this._geometryCol.Properties.Caption = Resources.UI_TuningZoneViewGeopetry;
            this._areaCol.Properties.Caption = Resources.UI_TuningZoneViewArea;
            this._dateChange.Properties.Caption = Resources.UI_TuningZoneGeometryDateChange;
            this._saveBtn.Text = Resources.UI_TuningSave;
            this._cancelBtn.Text = Resources.UI_TuningCancel;
            sbLog.Text = Resources.Log;
            
            erOutLink.Properties.Caption = Resources.OuterBaseLink;

            _cancelBtn.Image = Shared.Cancel;
            _deleteZonePopupBtn.Glyph = Shared.Cross;
            _deleteZoneToolBtn.Glyph = Shared.Cross;
            _saveBtn.Image = Shared.Save;
            _showOnMapPopupBtn.Glyph = Shared.ZoomFit;
            _showOnMapToolBtn.Glyph = Shared.ZoomFit;
            sbLog.Image = Shared.Log;

            _idRow.Properties.ToolTip = Resources.idRowToolTip;
            _nameCol.Properties.ToolTip = Resources.nameColToolTip;
            _groupCol.Properties.ToolTip = Resources.groupColToolTip;
            _colorRow.Properties.ToolTip = Resources.colorRowToolTip;
            _geometryCol.Properties.ToolTip = Resources.geometryColToolTip;
            _areaCol.Properties.ToolTip = Resources.areaColToolTip;
            _dateChange.Properties.ToolTip = Resources.dateChangeToolTip;
            erOutLink.Properties.ToolTip = Resources.erOutLinkToolTip;
            _passenger.Properties.Caption = Resources.passengerCaption;
            _passenger.Properties.ToolTip = Resources.passengerToolTip;
            _descriptionRow.Properties.ToolTip = Resources.decriptionRowToolTip;

            repItemCheckPass.CheckedChanged +=repItemCheckPass_CheckedChanged;

            btnCategory.Caption = Resources.UI_ZonesCategory;
            btnCategory.Hint = Resources.UI_ZonesCategoryTipBtn;
            btnCategory2.Caption = Resources.UI_ZonesCategory2;
            btnCategory2.Hint = Resources.UI_ZonesCategoryTip2Btn;
        }

        void repItemCheckPass_CheckedChanged( object sender, EventArgs e )
        {
            CheckEdit edit = ( CheckEdit )sender;
            //_tuning.PassengerCalc = ( bool ) edit.Checked;
        }

        private void tuning_StatusChanged()
        {
            setSaveCancelVisibility();
            _grid.Refresh();
        }

        private void setSaveCancelVisibility()
        {
            if (TuningStatus.Ok != _tuning.Status)
            {
                _saveBtn.Enabled = true;
                _cancelBtn.Enabled = true;
            }
            else
            {
                _saveBtn.Enabled = false;
                _cancelBtn.Enabled = false;
            }
        }

        public void SetGroups(ZonesGroup[] groups)
        {
            _groupRepo.Items.Clear();
            _groupRepo.Items.AddRange(groups);
        }

        private void nameRepo_EditValueChanged(object sender, EventArgs e)
       {
            TextEdit edit = (TextEdit) sender;
            _tuning.Name = (string) edit.EditValue;
        }

        private void descriptionRepo_EditValueChanged(object sender, EventArgs e)
        {
            MemoEdit edit = (MemoEdit) sender;
            _tuning.Description = (string) edit.EditValue;
        }

        private void colorChanged(object sender, EventArgs e)
        {
            ColorEdit edit = (ColorEdit) sender;
            _tuning.Color = edit.Color;
        }

        private void groupChanged(object sender, EventArgs e)
        {
            ComboBoxEdit edit = (ComboBoxEdit) sender;
            _tuning.Group = (ZonesGroup) edit.EditValue;
        }

        private void showOnMap_ItemClick(object sender, ItemClickEventArgs e)
        {
            onShowOnMapClicked();
        }

        private void delete_ItemClick(object sender, ItemClickEventArgs e)
        {
            onDeleteClicked();
        }

        private void saveBtn_Click(object sender, EventArgs e)
        {
            onSaveClicked();

            _tuning.Status = TuningStatus.Ok;
            tuning_StatusChanged();
        }

        private void cancelBtn_Click(object sender, EventArgs e)
        {
            onCancelClicked();
        }

        private void onShowOnMapClicked()
        {
            Action<IZone> handler = ShowOnMapClicked;
            if (null != handler)
                handler(_tuning.Zone);
        }

        private void onSaveClicked()
        {
            Action<TuningZone> handler = SaveClicked;
            if (null != handler)
                handler(_tuning);
        }

        private void onCancelClicked()
        {
            Action<TuningZone> handler = CancelClicked;
            if (null != handler)
                handler(_tuning);
        }

        private void onDeleteClicked()
        {
            Action<IZone> handler = DeleteClicked;
            if (null != handler)
                handler(_tuning.Zone);
        }

        private void rteOutLink_EditValueChanged(object sender, EventArgs e)
        {
            TextEdit edit = (TextEdit) sender;
            _tuning.IdOutLink = (string) edit.EditValue;
        }

        private void sbLog_Click(object sender, EventArgs e)
        {
            UserLog.ViewLog(UserLogTypes.CheckZone, _tuning.IdZone);
        }

        public void RefreshListCategory()
        {
            try
            {
                cbzCategory.Items.Clear();

                foreach( ZoneCategory zoneCategory in ZonesCategoryProvider.GetList() )
                    cbzCategory.Items.Add( zoneCategory );

                ZoneCategory emptyCategory = new ZoneCategory();
                emptyCategory.Name = " ";
                emptyCategory.Id = -1;
                cbzCategory.Items.Add( emptyCategory );
            }
            catch( Exception ex )
            {
                // to do
            }
        }

        public void RefreshList2Category()
        {
            try
            {
                cbzCategory2.Items.Clear();

                foreach( ZoneCategory2 zoneCategory in ZonesCategoryProvider.GetList2() )
                    cbzCategory2.Items.Add( zoneCategory );

                ZoneCategory2 emptyCategory = new ZoneCategory2();
                emptyCategory.Name = " ";
                emptyCategory.Id = -1;
                cbzCategory2.Items.Add( emptyCategory );
            }
            catch( Exception ex )
            {
                // to do
            }
        }

        private void RefreshCategoriesList()
        {
            RefreshListCategory();
        }

        private void RefreshCategoriesList2()
        {
            RefreshList2Category();
        }

        private void btnCategory_ItemClick( object sender, ItemClickEventArgs e )
        {
            CategoriesEditor formEditor = new CategoriesEditor();
            formEditor.RefreshCategoriesList += RefreshCategoriesList;
            formEditor.ShowDialog();
        }

        private void btnCategory2_ItemClick( object sender, ItemClickEventArgs e )
        {
            CategoriesEditor2 formEditor = new CategoriesEditor2();
            formEditor.RefreshCategoriesList += RefreshCategoriesList2;
            formEditor.ShowDialog();
        }
    }
}
