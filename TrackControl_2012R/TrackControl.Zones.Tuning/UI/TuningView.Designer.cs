namespace TrackControl.Zones.Tuning
{
  partial class TuningView
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TuningView));
            this._barManager = new DevExpress.XtraBars.BarManager(this.components);
            this._tools = new DevExpress.XtraBars.Bar();
            this._mainBtn = new DevExpress.XtraBars.BarButtonItem();
            this._mainPopup = new DevExpress.XtraBars.PopupMenu(this.components);
            this._addGroupPopupBtn = new DevExpress.XtraBars.BarButtonItem();
            this._addZonePopupBtn = new DevExpress.XtraBars.BarButtonItem();
            this._importMifPopupBtn = new DevExpress.XtraBars.BarButtonItem();
            this._deleteCheckedPopupBtn = new DevExpress.XtraBars.BarButtonItem();
            this._addGroupToolBtn = new DevExpress.XtraBars.BarButtonItem();
            this._addZoneToolBtn = new DevExpress.XtraBars.BarButtonItem();
            this._importMifToolBtn = new DevExpress.XtraBars.BarButtonItem();
            this.xmlExport = new DevExpress.XtraBars.BarButtonItem();
            this.xmlImport = new DevExpress.XtraBars.BarButtonItem();
            this._deleteCheckedToolBtn = new DevExpress.XtraBars.BarButtonItem();
            this.bbiCutPoints = new DevExpress.XtraBars.BarButtonItem();
            this._helpBtn = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.splitContainerControl3 = new DevExpress.XtraEditors.SplitContainerControl();
            this._servicePanel = new DevExpress.XtraEditors.PanelControl();
            this._treePanel = new DevExpress.XtraEditors.PanelControl();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this._mapPanel = new DevExpress.XtraEditors.PanelControl();
            this._tuningPanel = new DevExpress.XtraEditors.PanelControl();
            this.bbiCsvExport = new DevExpress.XtraBars.BarButtonItem();
            ((System.ComponentModel.ISupportInitialize)(this._barManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._mainPopup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl3)).BeginInit();
            this.splitContainerControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._servicePanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._treePanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._mapPanel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._tuningPanel)).BeginInit();
            this.SuspendLayout();
            // 
            // _barManager
            // 
            this._barManager.AllowCustomization = false;
            this._barManager.AllowMoveBarOnToolbar = false;
            this._barManager.AllowQuickCustomization = false;
            this._barManager.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this._tools});
            this._barManager.DockControls.Add(this.barDockControlTop);
            this._barManager.DockControls.Add(this.barDockControlBottom);
            this._barManager.DockControls.Add(this.barDockControlLeft);
            this._barManager.DockControls.Add(this.barDockControlRight);
            this._barManager.Form = this;
            this._barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this._mainBtn,
            this._addGroupToolBtn,
            this._addGroupPopupBtn,
            this._addZoneToolBtn,
            this._addZonePopupBtn,
            this._importMifToolBtn,
            this._helpBtn,
            this._importMifPopupBtn,
            this.xmlExport,
            this._deleteCheckedToolBtn,
            this._deleteCheckedPopupBtn,
            this.xmlImport,
            this.bbiCutPoints,
            this.bbiCsvExport});
            this._barManager.MaxItemId = 19;
            // 
            // _tools
            // 
            this._tools.BarName = "Tools";
            this._tools.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Top;
            this._tools.DockCol = 0;
            this._tools.DockRow = 0;
            this._tools.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this._tools.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.None, this._mainBtn, "", false, false, true, 0),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.None, this._addGroupToolBtn, "", true, false, true, 0),
            new DevExpress.XtraBars.LinkPersistInfo(this._addZoneToolBtn),
            new DevExpress.XtraBars.LinkPersistInfo(this._importMifToolBtn),
            new DevExpress.XtraBars.LinkPersistInfo(this.xmlExport),
            new DevExpress.XtraBars.LinkPersistInfo(this.xmlImport),
            new DevExpress.XtraBars.LinkPersistInfo(this._deleteCheckedToolBtn),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiCsvExport),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiCutPoints),
            new DevExpress.XtraBars.LinkPersistInfo(this._helpBtn)});
            this._tools.OptionsBar.AllowQuickCustomization = false;
            this._tools.OptionsBar.DisableClose = true;
            this._tools.OptionsBar.DisableCustomization = true;
            this._tools.OptionsBar.DrawDragBorder = false;
            this._tools.OptionsBar.UseWholeRow = true;
            this._tools.Text = "Tools";
            // 
            // _mainBtn
            // 
            this._mainBtn.ActAsDropDown = true;
            this._mainBtn.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this._mainBtn.Caption = "��������";
            this._mainBtn.DropDownControl = this._mainPopup;
            this._mainBtn.Id = 0;
            this._mainBtn.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this._mainBtn.ItemAppearance.Normal.Options.UseFont = true;
            this._mainBtn.Name = "_mainBtn";
            this._mainBtn.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // _mainPopup
            // 
            this._mainPopup.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this._addGroupPopupBtn),
            new DevExpress.XtraBars.LinkPersistInfo(this._addZonePopupBtn),
            new DevExpress.XtraBars.LinkPersistInfo(this._importMifPopupBtn),
            new DevExpress.XtraBars.LinkPersistInfo(this._deleteCheckedPopupBtn)});
            this._mainPopup.Manager = this._barManager;
            this._mainPopup.Name = "_mainPopup";
            // 
            // _addGroupPopupBtn
            // 
            this._addGroupPopupBtn.Caption = "�������� ������";
            this._addGroupPopupBtn.Id = 2;
            this._addGroupPopupBtn.Name = "_addGroupPopupBtn";
            this._addGroupPopupBtn.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.addGroup_ItemClick);
            // 
            // _addZonePopupBtn
            // 
            this._addZonePopupBtn.Caption = "�������� ����";
            this._addZonePopupBtn.Id = 4;
            this._addZonePopupBtn.Name = "_addZonePopupBtn";
            this._addZonePopupBtn.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.addZone_ItemClick);
            // 
            // _importMifPopupBtn
            // 
            this._importMifPopupBtn.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this._importMifPopupBtn.Caption = "������ �� MIF �����";
            this._importMifPopupBtn.Id = 7;
            this._importMifPopupBtn.Name = "_importMifPopupBtn";
            this._importMifPopupBtn.DownChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.importMiflBtn_DownChanged);
            // 
            // _deleteCheckedPopupBtn
            // 
            this._deleteCheckedPopupBtn.Caption = "������� ���������� ��������";
            this._deleteCheckedPopupBtn.Hint = "����� ������� ���������� �������� ������";
            this._deleteCheckedPopupBtn.Id = 9;
            this._deleteCheckedPopupBtn.Name = "_deleteCheckedPopupBtn";
            this._deleteCheckedPopupBtn.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this._deleteCheckedPopupBtn.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.deleteChecked_ItemClick);
            // 
            // _addGroupToolBtn
            // 
            this._addGroupToolBtn.Caption = "�������� ������";
            this._addGroupToolBtn.Hint = "�������� ����� ���������� ����� ������";
            this._addGroupToolBtn.Id = 1;
            this._addGroupToolBtn.Name = "_addGroupToolBtn";
            this._addGroupToolBtn.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.addGroup_ItemClick);
            // 
            // _addZoneToolBtn
            // 
            this._addZoneToolBtn.Caption = "�������� ����";
            this._addZoneToolBtn.Hint = "�������� ����� ���������� ����� ����������� ����";
            this._addZoneToolBtn.Id = 3;
            this._addZoneToolBtn.Name = "_addZoneToolBtn";
            this._addZoneToolBtn.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.addZone_ItemClick);
            // 
            // _importMifToolBtn
            // 
            this._importMifToolBtn.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this._importMifToolBtn.Caption = "������ �� MIF";
            this._importMifToolBtn.Hint = "������ ����������� ��� �� MIF-�����";
            this._importMifToolBtn.Id = 6;
            this._importMifToolBtn.Name = "_importMifToolBtn";
            this._importMifToolBtn.DownChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.importMiflBtn_DownChanged);
            // 
            // xmlExport
            // 
            this.xmlExport.Caption = "XML export";
            this.xmlExport.Glyph = ((System.Drawing.Image)(resources.GetObject("xmlExport.Glyph")));
            this.xmlExport.Id = 14;
            this.xmlExport.Name = "xmlExport";
            this.xmlExport.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.xmlExport_ItemClick);
            // 
            // xmlImport
            // 
            this.xmlImport.Caption = "XML import";
            this.xmlImport.Glyph = ((System.Drawing.Image)(resources.GetObject("xmlImport.Glyph")));
            this.xmlImport.Id = 15;
            this.xmlImport.Name = "xmlImport";
            this.xmlImport.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.xmlImport_ItemClick);
            // 
            // _deleteCheckedToolBtn
            // 
            this._deleteCheckedToolBtn.Caption = "������� ����������";
            this._deleteCheckedToolBtn.Hint = "����� ������� ���������� ��������";
            this._deleteCheckedToolBtn.Id = 8;
            this._deleteCheckedToolBtn.Name = "_deleteCheckedToolBtn";
            this._deleteCheckedToolBtn.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this._deleteCheckedToolBtn.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.deleteChecked_ItemClick);
            // 
            // bbiCutPoints
            // 
            this.bbiCutPoints.Caption = "��������� ���-�� �����";
            this.bbiCutPoints.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiCutPoints.Glyph")));
            this.bbiCutPoints.Hint = "���������� ���������� ����� �� ���������  ������� - ������";
            this.bbiCutPoints.Id = 16;
            this.bbiCutPoints.Name = "bbiCutPoints";
            this.bbiCutPoints.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiCutPoints_ItemClick);
            // 
            // _helpBtn
            // 
            this._helpBtn.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this._helpBtn.Caption = "���������";
            this._helpBtn.Id = 5;
            this._helpBtn.Name = "_helpBtn";
            this._helpBtn.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.helpBtn_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(1017, 39);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 678);
            this.barDockControlBottom.Size = new System.Drawing.Size(1017, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 39);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 639);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1017, 39);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 639);
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 39);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.splitContainerControl3);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.splitContainerControl2);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(1017, 639);
            this.splitContainerControl1.SplitterPosition = 266;
            this.splitContainerControl1.TabIndex = 10;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // splitContainerControl3
            // 
            this.splitContainerControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl3.Horizontal = false;
            this.splitContainerControl3.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl3.Name = "splitContainerControl3";
            this.splitContainerControl3.Panel1.Controls.Add(this._servicePanel);
            this.splitContainerControl3.Panel1.Text = "Panel1";
            this.splitContainerControl3.Panel2.Controls.Add(this._treePanel);
            this.splitContainerControl3.Panel2.Text = "Panel2";
            this.splitContainerControl3.PanelVisibility = DevExpress.XtraEditors.SplitPanelVisibility.Panel2;
            this.splitContainerControl3.Size = new System.Drawing.Size(266, 639);
            this.splitContainerControl3.SplitterPosition = 223;
            this.splitContainerControl3.TabIndex = 0;
            this.splitContainerControl3.Text = "splitContainerControl3";
            // 
            // _servicePanel
            // 
            this._servicePanel.AutoSize = true;
            this._servicePanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this._servicePanel.ContentImageAlignment = System.Drawing.ContentAlignment.TopCenter;
            this._servicePanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this._servicePanel.Location = new System.Drawing.Point(0, 0);
            this._servicePanel.Name = "_servicePanel";
            this._servicePanel.Size = new System.Drawing.Size(0, 0);
            this._servicePanel.TabIndex = 0;
            // 
            // _treePanel
            // 
            this._treePanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this._treePanel.Location = new System.Drawing.Point(0, 0);
            this._treePanel.Name = "_treePanel";
            this._treePanel.Size = new System.Drawing.Size(266, 639);
            this._treePanel.TabIndex = 0;
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl2.FixedPanel = DevExpress.XtraEditors.SplitFixedPanel.None;
            this.splitContainerControl2.Horizontal = false;
            this.splitContainerControl2.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.Controls.Add(this._mapPanel);
            this.splitContainerControl2.Panel1.Text = "Panel1";
            this.splitContainerControl2.Panel2.Controls.Add(this._tuningPanel);
            this.splitContainerControl2.Panel2.Text = "Panel2";
            this.splitContainerControl2.Size = new System.Drawing.Size(746, 639);
            this.splitContainerControl2.SplitterPosition = 319;
            this.splitContainerControl2.TabIndex = 0;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // _mapPanel
            // 
            this._mapPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this._mapPanel.Location = new System.Drawing.Point(0, 0);
            this._mapPanel.Name = "_mapPanel";
            this._mapPanel.Size = new System.Drawing.Size(746, 319);
            this._mapPanel.TabIndex = 0;
            // 
            // _tuningPanel
            // 
            this._tuningPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this._tuningPanel.Location = new System.Drawing.Point(0, 0);
            this._tuningPanel.Name = "_tuningPanel";
            this._tuningPanel.Size = new System.Drawing.Size(746, 315);
            this._tuningPanel.TabIndex = 0;
            // 
            // bbiCsvExport
            // 
            this.bbiCsvExport.Caption = "CSV";
            this.bbiCsvExport.Id = 18;
            this.bbiCsvExport.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.bbiCsvExport.ItemAppearance.Normal.Options.UseFont = true;
            this.bbiCsvExport.Name = "bbiCsvExport";
            this.bbiCsvExport.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiCsvExport_ItemClick);
            // 
            // TuningView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainerControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "TuningView";
            this.Size = new System.Drawing.Size(1017, 678);
            ((System.ComponentModel.ISupportInitialize)(this._barManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._mainPopup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl3)).EndInit();
            this.splitContainerControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._servicePanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._treePanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._mapPanel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._tuningPanel)).EndInit();
            this.ResumeLayout(false);

    }

    #endregion

    private DevExpress.XtraBars.BarManager _barManager;
    private DevExpress.XtraBars.Bar _tools;
    private DevExpress.XtraBars.BarDockControl barDockControlTop;
    private DevExpress.XtraBars.BarDockControl barDockControlBottom;
    private DevExpress.XtraBars.BarDockControl barDockControlLeft;
    private DevExpress.XtraBars.BarDockControl barDockControlRight;
    private DevExpress.XtraBars.BarButtonItem _mainBtn;
    private DevExpress.XtraBars.PopupMenu _mainPopup;
    private DevExpress.XtraBars.BarButtonItem _addGroupToolBtn;
    private DevExpress.XtraBars.BarButtonItem _addGroupPopupBtn;
    private DevExpress.XtraBars.BarButtonItem _addZoneToolBtn;
    private DevExpress.XtraBars.BarButtonItem _addZonePopupBtn;
    private DevExpress.XtraBars.BarButtonItem _helpBtn;
    private DevExpress.XtraBars.BarButtonItem _importMifToolBtn;
    private DevExpress.XtraBars.BarButtonItem _importMifPopupBtn;
    private DevExpress.XtraBars.BarButtonItem _deleteCheckedToolBtn;
    private DevExpress.XtraBars.BarButtonItem _deleteCheckedPopupBtn;
    private DevExpress.XtraBars.BarButtonItem xmlExport;
    private DevExpress.XtraBars.BarButtonItem xmlImport;
    private DevExpress.XtraBars.BarButtonItem bbiCutPoints;
    private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
    private DevExpress.XtraEditors.SplitContainerControl splitContainerControl3;
    private DevExpress.XtraEditors.PanelControl _servicePanel;
    private DevExpress.XtraEditors.PanelControl _treePanel;
    private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
    private DevExpress.XtraEditors.PanelControl _mapPanel;
    private DevExpress.XtraEditors.PanelControl _tuningPanel;
    private DevExpress.XtraBars.BarButtonItem bbiCsvExport;
    private DevExpress.XtraBars.BarButtonItem barButtonItem1;
  }
}
