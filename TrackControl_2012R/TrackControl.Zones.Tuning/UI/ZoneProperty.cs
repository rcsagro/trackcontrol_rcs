﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using TrackControl.General;
using TrackControl.GMap;
using TrackControl.Zones.Tuning.Properties;

namespace TrackControl.Zones.Tuning
{
    public partial class ZoneProperty : UserControl
    {
        TuningZone  _tZone;

        public ZoneProperty()
        {
            InitializeComponent();
            Localization();
        }

        public void SetZone(IZone zone)
        {
            if (null == zone)
                throw new ArgumentNullException("zone");
            _tZone = new TuningZone(zone);
            pgZone.SelectedObject = _tZone;
            BmpLatLng bll = new BmpLatLng(_tZone.Points, peZone.Width, peZone.Height);
            bll.SetZoneColor(_tZone.Style.Color); 
            if (peZone.Image != null) peZone.Image.Dispose();
            peZone.Image = bll.DrawOnBitmap();
        }

        public double AreaGa
        {
            get {
                if (_tZone != null)
                    return _tZone.AreaGa;
                else
                    return 0;
            } 
        }

        public void RefreshListCategory()
        {
            try
            {
                zbeCategory.Items.Clear();

                foreach( ZoneCategory zoneCategory in ZonesCategoryProvider.GetList() )
                    zbeCategory.Items.Add(zoneCategory);

                ZoneCategory emptyCategory = new ZoneCategory();
                emptyCategory.Name = " ";
                emptyCategory.Id = -1;
                zbeCategory.Items.Add( emptyCategory );
            }
            catch( Exception ex )
            {
                // to do
            }
        }

        public void RefreshList2Category()
        {
            try
            {
                zbeCategory2.Items.Clear();

                foreach( ZoneCategory2 zoneCategory in ZonesCategoryProvider.GetList2() )
                    zbeCategory2.Items.Add( zoneCategory );

                ZoneCategory2 emptyCategory = new ZoneCategory2();
                emptyCategory.Name = " ";
                emptyCategory.Id = -1;
                zbeCategory2.Items.Add( emptyCategory );
            }
            catch( Exception ex )
            {
                // to do
            }
        }

        void Localization()
        {
            this._idRow.Properties.Caption = Resources.UI_TuningZoneViewID;
            this._nameCol.Properties.Caption = Resources.UI_TuningName;
            this._descriptionRow.Properties.Caption = Resources.UI_TuningDescription;
            this._groupCol.Properties.Caption = Resources.UI_TuningZoneViewGroup;
            this._colorRow.Properties.Caption = Resources.UI_TuningZoneViewStyle;
            this._geometryCol.Properties.Caption = Resources.UI_TuningZoneViewGeopetry;
            this._areaCol.Properties.Caption = Resources.UI_TuningZoneViewArea;
            this._dateChange.Properties.Caption = Resources.UI_TuningZoneGeometryDateChange;
            this._ezCategory.Properties.Caption = Resources.UI_ZonesCategory;
            this._ezCategory2.Properties.Caption = Resources.UI_ZonesCategory2;
            this._ezCategory.Properties.ToolTip = Resources.UI_ZonesCategoryTip;
            this._ezCategory.Properties.ToolTip = Resources.UI_ZonesCategoryTip2;
        }
    }
}
