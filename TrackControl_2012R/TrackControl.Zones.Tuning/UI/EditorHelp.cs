using System;
using DevExpress.XtraEditors;
using TrackControl.General;
using TrackControl.Zones.Tuning.Properties;

namespace TrackControl.Zones.Tuning
{
    public partial class EditorHelp : XtraForm
    {
        public EditorHelp()
        {
            InitializeComponent();
            Init();

            _btn.Image = Shared.Cross;
        }

        private void Init()
        {
            this._addingGroup.Text = Resources.UI_EditorAddZone;
            this._editingGroup.Text = Resources.UI_EditorEditZone;
            this._btn.Text = Resources.UI_EditorClose;
            this._lab1.Text = Resources.UI_EditorBeginPoint;
            this._lab2.Text = Resources.UI_EditorAddNewPoint;
            this._lab3.Text = Resources.UI_EditorEndAddPoints;
            this._lab4.Text = Resources.UI_EditorInsertPoint;
            this._lab5.Text = Resources.UI_EditorDeletePoint;
            this._lab6.Text = Resources.UI_EditorMovePeak;
            this.labelControl1.Text = "<b>�������: '='</b> ��������� ������� �����.";
            this.labelControl2.Text = "<b>�������: '-'</b> ��������� ������� �����.";
            this.Text = Resources.UI_EditorPrompt;
        }

        private void btn_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void _btn_Click( object sender, EventArgs e )
        {
            Close();
        }
    }
}