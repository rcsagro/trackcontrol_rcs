namespace TrackControl.Zones.Tuning
{
  partial class TuningZoneView
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TuningZoneView));
            this._barManager = new DevExpress.XtraBars.BarManager(this.components);
            this._tools = new DevExpress.XtraBars.Bar();
            this._mainBtn = new DevExpress.XtraBars.BarButtonItem();
            this._mainPopup = new DevExpress.XtraBars.PopupMenu(this.components);
            this._showOnMapPopupBtn = new DevExpress.XtraBars.BarButtonItem();
            this._deleteZonePopupBtn = new DevExpress.XtraBars.BarButtonItem();
            this._showOnMapToolBtn = new DevExpress.XtraBars.BarButtonItem();
            this._deleteZoneToolBtn = new DevExpress.XtraBars.BarButtonItem();
            this.btnCategory = new DevExpress.XtraBars.BarButtonItem();
            this.btnCategory2 = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this._grid = new DevExpress.XtraVerticalGrid.PropertyGridControl();
            this._descriptionRepo = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
            this._nameRepo = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this._groupRepo = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this._colorRepo = new DevExpress.XtraEditors.Repository.RepositoryItemColorEdit();
            this.rteOutLink = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repItemCheckPass = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.cbzCategory = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.cbzCategory2 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this._idRow = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this._nameCol = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this._groupCol = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this._colorRow = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this._geometryCol = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this._areaCol = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this._dateChange = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erOutLink = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this._passenger = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this._descriptionRow = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.znCategory = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.znCategory2 = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this._saveBtn = new DevExpress.XtraEditors.SimpleButton();
            this._cancelBtn = new DevExpress.XtraEditors.SimpleButton();
            this.sbLog = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this._barManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._mainPopup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._grid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._descriptionRepo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._nameRepo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._groupRepo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._colorRepo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rteOutLink)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repItemCheckPass)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbzCategory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbzCategory2)).BeginInit();
            this.SuspendLayout();
            // 
            // _barManager
            // 
            this._barManager.AllowCustomization = false;
            this._barManager.AllowMoveBarOnToolbar = false;
            this._barManager.AllowQuickCustomization = false;
            this._barManager.AllowShowToolbarsPopup = false;
            this._barManager.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this._tools});
            this._barManager.DockControls.Add(this.barDockControlTop);
            this._barManager.DockControls.Add(this.barDockControlBottom);
            this._barManager.DockControls.Add(this.barDockControlLeft);
            this._barManager.DockControls.Add(this.barDockControlRight);
            this._barManager.Form = this;
            this._barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this._mainBtn,
            this._showOnMapPopupBtn,
            this._showOnMapToolBtn,
            this._deleteZoneToolBtn,
            this._deleteZonePopupBtn,
            this.btnCategory,
            this.btnCategory2});
            this._barManager.MaxItemId = 7;
            // 
            // _tools
            // 
            this._tools.BarName = "Tools";
            this._tools.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Top;
            this._tools.DockCol = 0;
            this._tools.DockRow = 0;
            this._tools.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this._tools.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this._mainBtn),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.None, this._showOnMapToolBtn, "", true, false, true, 0),
            new DevExpress.XtraBars.LinkPersistInfo(this._deleteZoneToolBtn),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnCategory),
            new DevExpress.XtraBars.LinkPersistInfo(this.btnCategory2)});
            this._tools.OptionsBar.AllowQuickCustomization = false;
            this._tools.OptionsBar.DisableClose = true;
            this._tools.OptionsBar.DisableCustomization = true;
            this._tools.OptionsBar.DrawDragBorder = false;
            this._tools.OptionsBar.UseWholeRow = true;
            this._tools.Text = "Tools";
            // 
            // _mainBtn
            // 
            this._mainBtn.ActAsDropDown = true;
            this._mainBtn.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this._mainBtn.Caption = "��������";
            this._mainBtn.DropDownControl = this._mainPopup;
            this._mainBtn.Id = 0;
            this._mainBtn.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this._mainBtn.ItemAppearance.Normal.Options.UseFont = true;
            this._mainBtn.Name = "_mainBtn";
            this._mainBtn.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            // 
            // _mainPopup
            // 
            this._mainPopup.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this._showOnMapPopupBtn),
            new DevExpress.XtraBars.LinkPersistInfo(this._deleteZonePopupBtn)});
            this._mainPopup.Manager = this._barManager;
            this._mainPopup.Name = "_mainPopup";
            // 
            // _showOnMapPopupBtn
            // 
            this._showOnMapPopupBtn.Caption = "�������� �� �����";
            this._showOnMapPopupBtn.Id = 1;
            this._showOnMapPopupBtn.Name = "_showOnMapPopupBtn";
            this._showOnMapPopupBtn.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.showOnMap_ItemClick);
            // 
            // _deleteZonePopupBtn
            // 
            this._deleteZonePopupBtn.Caption = "������� ����";
            this._deleteZonePopupBtn.Id = 4;
            this._deleteZonePopupBtn.Name = "_deleteZonePopupBtn";
            this._deleteZonePopupBtn.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.delete_ItemClick);
            // 
            // _showOnMapToolBtn
            // 
            this._showOnMapToolBtn.Caption = "�������� �� �����";
            this._showOnMapToolBtn.Id = 2;
            this._showOnMapToolBtn.Name = "_showOnMapToolBtn";
            this._showOnMapToolBtn.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.showOnMap_ItemClick);
            // 
            // _deleteZoneToolBtn
            // 
            this._deleteZoneToolBtn.Caption = "������� ����";
            this._deleteZoneToolBtn.Id = 3;
            this._deleteZoneToolBtn.Name = "_deleteZoneToolBtn";
            this._deleteZoneToolBtn.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.delete_ItemClick);
            // 
            // btnCategory
            // 
            this.btnCategory.Caption = "���������";
            this.btnCategory.Glyph = ((System.Drawing.Image)(resources.GetObject("btnCategory.Glyph")));
            this.btnCategory.Hint = "�������� ���������";
            this.btnCategory.Id = 5;
            this.btnCategory.Name = "btnCategory";
            this.btnCategory.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnCategory.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnCategory_ItemClick);
            // 
            // btnCategory2
            // 
            this.btnCategory2.Caption = "���������2";
            this.btnCategory2.Glyph = ((System.Drawing.Image)(resources.GetObject("btnCategory2.Glyph")));
            this.btnCategory2.Hint = "�������� ��������� 2";
            this.btnCategory2.Id = 6;
            this.btnCategory2.Name = "btnCategory2";
            this.btnCategory2.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
            this.btnCategory2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnCategory2_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(565, 31);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 295);
            this.barDockControlBottom.Size = new System.Drawing.Size(565, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 31);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 264);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(565, 31);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 264);
            // 
            // _grid
            // 
            this._grid.Location = new System.Drawing.Point(3, 32);
            this._grid.Name = "_grid";
            this._grid.RecordWidth = 93;
            this._grid.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this._descriptionRepo,
            this._nameRepo,
            this._groupRepo,
            this._colorRepo,
            this.rteOutLink,
            this.repItemCheckPass,
            this.cbzCategory,
            this.cbzCategory2});
            this._grid.RowHeaderWidth = 107;
            this._grid.Rows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this._idRow,
            this._nameCol,
            this._groupCol,
            this._colorRow,
            this._geometryCol,
            this._areaCol,
            this._dateChange,
            this.erOutLink,
            this._passenger,
            this._descriptionRow,
            this.znCategory,
            this.znCategory2});
            this._grid.Size = new System.Drawing.Size(445, 263);
            this._grid.TabIndex = 4;
            // 
            // _descriptionRepo
            // 
            this._descriptionRepo.MaxLength = 2000;
            this._descriptionRepo.Name = "_descriptionRepo";
            this._descriptionRepo.EditValueChanged += new System.EventHandler(this.descriptionRepo_EditValueChanged);
            // 
            // _nameRepo
            // 
            this._nameRepo.AutoHeight = false;
            this._nameRepo.MaxLength = 50;
            this._nameRepo.Name = "_nameRepo";
            this._nameRepo.EditValueChanged += new System.EventHandler(this.nameRepo_EditValueChanged);
            // 
            // _groupRepo
            // 
            this._groupRepo.AutoHeight = false;
            this._groupRepo.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this._groupRepo.Name = "_groupRepo";
            this._groupRepo.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this._groupRepo.EditValueChanged += new System.EventHandler(this.groupChanged);
            // 
            // _colorRepo
            // 
            this._colorRepo.AllowFocused = false;
            this._colorRepo.AutoHeight = false;
            this._colorRepo.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this._colorRepo.Name = "_colorRepo";
            this._colorRepo.ShowSystemColors = false;
            this._colorRepo.EditValueChanged += new System.EventHandler(this.colorChanged);
            // 
            // rteOutLink
            // 
            this.rteOutLink.AutoHeight = false;
            this.rteOutLink.MaxLength = 20;
            this.rteOutLink.Name = "rteOutLink";
            this.rteOutLink.EditValueChanged += new System.EventHandler(this.rteOutLink_EditValueChanged);
            // 
            // repItemCheckPass
            // 
            this.repItemCheckPass.AutoHeight = false;
            this.repItemCheckPass.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.repItemCheckPass.Caption = "Check";
            this.repItemCheckPass.Name = "repItemCheckPass";
            // 
            // cbzCategory
            // 
            this.cbzCategory.AutoHeight = false;
            this.cbzCategory.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbzCategory.Name = "cbzCategory";
            this.cbzCategory.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // cbzCategory2
            // 
            this.cbzCategory2.AutoHeight = false;
            this.cbzCategory2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbzCategory2.Name = "cbzCategory2";
            this.cbzCategory2.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // _idRow
            // 
            this._idRow.Appearance.Options.UseTextOptions = true;
            this._idRow.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this._idRow.Name = "_idRow";
            this._idRow.OptionsRow.AllowFocus = false;
            this._idRow.OptionsRow.AllowMove = false;
            this._idRow.OptionsRow.AllowMoveToCustomizationForm = false;
            this._idRow.OptionsRow.AllowSize = false;
            this._idRow.OptionsRow.DblClickExpanding = false;
            this._idRow.OptionsRow.ShowInCustomizationForm = false;
            this._idRow.Properties.Caption = "ID � ������� ��";
            this._idRow.Properties.FieldName = "Id";
            this._idRow.Properties.ToolTip = "������������� ����";
            // 
            // _nameCol
            // 
            this._nameCol.Name = "_nameCol";
            this._nameCol.OptionsRow.AllowMove = false;
            this._nameCol.OptionsRow.AllowMoveToCustomizationForm = false;
            this._nameCol.OptionsRow.AllowSize = false;
            this._nameCol.OptionsRow.DblClickExpanding = false;
            this._nameCol.OptionsRow.ShowInCustomizationForm = false;
            this._nameCol.Properties.Caption = "��������";
            this._nameCol.Properties.FieldName = "Name";
            this._nameCol.Properties.RowEdit = this._nameRepo;
            this._nameCol.Properties.ToolTip = "�������� ����";
            // 
            // _groupCol
            // 
            this._groupCol.Name = "_groupCol";
            this._groupCol.OptionsRow.AllowMove = false;
            this._groupCol.OptionsRow.AllowMoveToCustomizationForm = false;
            this._groupCol.OptionsRow.AllowSize = false;
            this._groupCol.OptionsRow.DblClickExpanding = false;
            this._groupCol.OptionsRow.ShowInCustomizationForm = false;
            this._groupCol.Properties.Caption = "������";
            this._groupCol.Properties.FieldName = "Group";
            this._groupCol.Properties.RowEdit = this._groupRepo;
            this._groupCol.Properties.ToolTip = "������ ����";
            // 
            // _colorRow
            // 
            this._colorRow.Name = "_colorRow";
            this._colorRow.OptionsRow.AllowMove = false;
            this._colorRow.OptionsRow.AllowMoveToCustomizationForm = false;
            this._colorRow.OptionsRow.AllowSize = false;
            this._colorRow.OptionsRow.DblClickExpanding = false;
            this._colorRow.OptionsRow.ShowInCustomizationForm = false;
            this._colorRow.Properties.Caption = "���� �������";
            this._colorRow.Properties.FieldName = "Color";
            this._colorRow.Properties.RowEdit = this._colorRepo;
            this._colorRow.Properties.ToolTip = "���� ����";
            // 
            // _geometryCol
            // 
            this._geometryCol.Appearance.Options.UseTextOptions = true;
            this._geometryCol.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this._geometryCol.Name = "_geometryCol";
            this._geometryCol.OptionsRow.AllowFocus = false;
            this._geometryCol.OptionsRow.AllowMove = false;
            this._geometryCol.OptionsRow.AllowMoveToCustomizationForm = false;
            this._geometryCol.OptionsRow.AllowSize = false;
            this._geometryCol.OptionsRow.DblClickExpanding = false;
            this._geometryCol.OptionsRow.ShowInCustomizationForm = false;
            this._geometryCol.Properties.Caption = "���������";
            this._geometryCol.Properties.FieldName = "GeometryInfo";
            this._geometryCol.Properties.ReadOnly = true;
            this._geometryCol.Properties.ToolTip = "��������� ����";
            // 
            // _areaCol
            // 
            this._areaCol.Appearance.Options.UseTextOptions = true;
            this._areaCol.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this._areaCol.Name = "_areaCol";
            this._areaCol.OptionsRow.AllowFocus = false;
            this._areaCol.OptionsRow.AllowMove = false;
            this._areaCol.OptionsRow.AllowMoveToCustomizationForm = false;
            this._areaCol.OptionsRow.AllowSize = false;
            this._areaCol.OptionsRow.DblClickExpanding = false;
            this._areaCol.OptionsRow.ShowInCustomizationForm = false;
            this._areaCol.Properties.Caption = "�������";
            this._areaCol.Properties.FieldName = "AreaInfo";
            this._areaCol.Properties.ReadOnly = true;
            this._areaCol.Properties.ToolTip = "������� ����, ��";
            // 
            // _dateChange
            // 
            this._dateChange.Appearance.Options.UseTextOptions = true;
            this._dateChange.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this._dateChange.Name = "_dateChange";
            this._dateChange.OptionsRow.AllowFocus = false;
            this._dateChange.OptionsRow.AllowMove = false;
            this._dateChange.OptionsRow.AllowMoveToCustomizationForm = false;
            this._dateChange.OptionsRow.AllowSize = false;
            this._dateChange.OptionsRow.DblClickExpanding = false;
            this._dateChange.OptionsRow.ShowInCustomizationForm = false;
            this._dateChange.Properties.Caption = "���� ���������� ��������� ���������";
            this._dateChange.Properties.FieldName = "DateChange";
            this._dateChange.Properties.ReadOnly = true;
            this._dateChange.Properties.ToolTip = "���� ���������� ��������� ��������� ����";
            // 
            // erOutLink
            // 
            this.erOutLink.Name = "erOutLink";
            this.erOutLink.Properties.Caption = "��� ������� ����";
            this.erOutLink.Properties.FieldName = "IdOutLink";
            this.erOutLink.Properties.RowEdit = this.rteOutLink;
            this.erOutLink.Properties.ToolTip = "��� ������� ���� ��� ���� ����";
            // 
            // _passenger
            // 
            this._passenger.Appearance.Options.UseTextOptions = true;
            this._passenger.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this._passenger.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._passenger.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._passenger.Name = "_passenger";
            this._passenger.OptionsRow.AllowMove = false;
            this._passenger.OptionsRow.AllowMoveToCustomizationForm = false;
            this._passenger.OptionsRow.AllowSize = false;
            this._passenger.OptionsRow.DblClickExpanding = false;
            this._passenger.OptionsRow.ShowInCustomizationForm = false;
            this._passenger.Properties.Caption = "���������� ����� ����������";
            this._passenger.Properties.FieldName = "PassengerCalc";
            this._passenger.Properties.RowEdit = this.repItemCheckPass;
            this._passenger.Properties.ToolTip = "����������/�� ���������� ����� ���������� � ���� ����������� ����";
            // 
            // _descriptionRow
            // 
            this._descriptionRow.Height = 40;
            this._descriptionRow.Name = "_descriptionRow";
            this._descriptionRow.OptionsRow.AllowMove = false;
            this._descriptionRow.OptionsRow.AllowMoveToCustomizationForm = false;
            this._descriptionRow.OptionsRow.AllowSize = false;
            this._descriptionRow.OptionsRow.DblClickExpanding = false;
            this._descriptionRow.OptionsRow.ShowInCustomizationForm = false;
            this._descriptionRow.Properties.Caption = "��������";
            this._descriptionRow.Properties.FieldName = "Description";
            this._descriptionRow.Properties.RowEdit = this._descriptionRepo;
            this._descriptionRow.Properties.ToolTip = "������� �������� ����";
            // 
            // znCategory
            // 
            this.znCategory.Name = "znCategory";
            this.znCategory.OptionsRow.AllowMove = false;
            this.znCategory.OptionsRow.AllowMoveToCustomizationForm = false;
            this.znCategory.OptionsRow.AllowSize = false;
            this.znCategory.OptionsRow.DblClickExpanding = false;
            this.znCategory.OptionsRow.ShowInCustomizationForm = false;
            this.znCategory.Properties.Caption = "���������";
            this.znCategory.Properties.FieldName = "Category";
            this.znCategory.Properties.RowEdit = this.cbzCategory;
            this.znCategory.Properties.ToolTip = "��������� ���� ����";
            // 
            // znCategory2
            // 
            this.znCategory2.Name = "znCategory2";
            this.znCategory2.OptionsRow.AllowMove = false;
            this.znCategory2.OptionsRow.AllowMoveToCustomizationForm = false;
            this.znCategory2.OptionsRow.AllowSize = false;
            this.znCategory2.OptionsRow.DblClickExpanding = false;
            this.znCategory2.OptionsRow.ShowInCustomizationForm = false;
            this.znCategory2.Properties.Caption = "���������2";
            this.znCategory2.Properties.FieldName = "Category2";
            this.znCategory2.Properties.RowEdit = this.cbzCategory2;
            this.znCategory2.Properties.ToolTip = "���������2 ��� ���� ����";
            // 
            // _saveBtn
            // 
            this._saveBtn.Location = new System.Drawing.Point(454, 163);
            this._saveBtn.Name = "_saveBtn";
            this._saveBtn.Size = new System.Drawing.Size(95, 23);
            this._saveBtn.TabIndex = 5;
            this._saveBtn.Text = "���������";
            this._saveBtn.Click += new System.EventHandler(this.saveBtn_Click);
            // 
            // _cancelBtn
            // 
            this._cancelBtn.Location = new System.Drawing.Point(454, 192);
            this._cancelBtn.Name = "_cancelBtn";
            this._cancelBtn.Size = new System.Drawing.Size(95, 23);
            this._cancelBtn.TabIndex = 6;
            this._cancelBtn.Text = "������";
            this._cancelBtn.Click += new System.EventHandler(this.cancelBtn_Click);
            // 
            // sbLog
            // 
            this.sbLog.Location = new System.Drawing.Point(454, 35);
            this.sbLog.Name = "sbLog";
            this.sbLog.Size = new System.Drawing.Size(95, 23);
            this.sbLog.TabIndex = 11;
            this.sbLog.Text = "������";
            this.sbLog.Click += new System.EventHandler(this.sbLog_Click);
            // 
            // TuningZoneView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.sbLog);
            this.Controls.Add(this._cancelBtn);
            this.Controls.Add(this._saveBtn);
            this.Controls.Add(this._grid);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "TuningZoneView";
            this.Size = new System.Drawing.Size(565, 295);
            ((System.ComponentModel.ISupportInitialize)(this._barManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._mainPopup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._grid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._descriptionRepo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._nameRepo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._groupRepo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._colorRepo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rteOutLink)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repItemCheckPass)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbzCategory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbzCategory2)).EndInit();
            this.ResumeLayout(false);

    }

    #endregion

    private DevExpress.XtraBars.BarManager _barManager;
    private DevExpress.XtraBars.Bar _tools;
    private DevExpress.XtraBars.BarDockControl barDockControlTop;
    private DevExpress.XtraBars.BarDockControl barDockControlBottom;
    private DevExpress.XtraBars.BarDockControl barDockControlLeft;
    private DevExpress.XtraBars.BarDockControl barDockControlRight;
    private DevExpress.XtraBars.BarButtonItem _mainBtn;
    private DevExpress.XtraBars.PopupMenu _mainPopup;
    private DevExpress.XtraBars.BarButtonItem _showOnMapPopupBtn;
    private DevExpress.XtraBars.BarButtonItem _showOnMapToolBtn;
    private DevExpress.XtraBars.BarButtonItem _deleteZoneToolBtn;
    private DevExpress.XtraBars.BarButtonItem _deleteZonePopupBtn;
    private DevExpress.XtraVerticalGrid.PropertyGridControl _grid;
    private DevExpress.XtraVerticalGrid.Rows.EditorRow _idRow;
    private DevExpress.XtraVerticalGrid.Rows.EditorRow _nameCol;
    private DevExpress.XtraEditors.SimpleButton _cancelBtn;
    private DevExpress.XtraEditors.SimpleButton _saveBtn;
    private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit _descriptionRepo;
    private DevExpress.XtraVerticalGrid.Rows.EditorRow _descriptionRow;
    private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit _nameRepo;
    private DevExpress.XtraEditors.Repository.RepositoryItemComboBox _groupRepo;
    private DevExpress.XtraVerticalGrid.Rows.EditorRow _groupCol;
    private DevExpress.XtraVerticalGrid.Rows.EditorRow _colorRow;
    private DevExpress.XtraVerticalGrid.Rows.EditorRow _geometryCol;
    private DevExpress.XtraVerticalGrid.Rows.EditorRow _areaCol;
    private DevExpress.XtraEditors.Repository.RepositoryItemColorEdit _colorRepo;
    private DevExpress.XtraVerticalGrid.Rows.EditorRow _dateChange;
    private DevExpress.XtraVerticalGrid.Rows.EditorRow erOutLink;
    private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit rteOutLink;
    private DevExpress.XtraEditors.SimpleButton sbLog;
    private DevExpress.XtraVerticalGrid.Rows.EditorRow _passenger;
    private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repItemCheckPass;
    private DevExpress.XtraEditors.Repository.RepositoryItemComboBox cbzCategory;
    private DevExpress.XtraEditors.Repository.RepositoryItemComboBox cbzCategory2;
    private DevExpress.XtraVerticalGrid.Rows.EditorRow znCategory;
    private DevExpress.XtraVerticalGrid.Rows.EditorRow znCategory2;
    private DevExpress.XtraBars.BarButtonItem btnCategory;
    private DevExpress.XtraBars.BarButtonItem btnCategory2;
  }
}
