﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using TrackControl.General;
using TrackControl.Zones.Tuning.Properties;

namespace TrackControl.Zones.Tuning.UI
{
    public partial class EditorHelpXtra : DevExpress.XtraEditors.XtraForm
    {
        public EditorHelpXtra()
        {
            InitializeComponent();
            Init();

            _btn.Image = Shared.Cross;
        }

        private void Init()
        {
            _btn.Text = Resources.UI_EditorClose;

            _lab0.Text = Resources.UI_EditorBeginPoint0;
            _lab1.Text = Resources.UI_EditorBeginPoint1;

            _lab20.Text = Resources.UI_EditorAddNewPoint0;
            _lab21.Text = Resources.UI_EditorAddNewPoint1;
            _lab22.Text = Resources.UI_EditorAddNewPoint2;

            _lab30.Text = Resources.UI_EditorEndAddPoints0;
            _lab31.Text = Resources.UI_EditorEndAddPoints1;

            _lab40.Text = Resources.UI_EditorInsertPoint0;
            _lab41.Text = Resources.UI_EditorInsertPoint1;

            _lab50.Text = Resources.UI_EditorDeletePoint0;
            _lab51.Text = Resources.UI_EditorDeletePoint1;
            _lab52.Text = Resources.UI_EditorDeletePoint2;

            _lab60.Text = Resources.UI_EditorMovePeak0;
            _lab61.Text = Resources.UI_EditorMovePeak1;
            _lab62.Text = Resources.UI_EditorMovePeak2;

            lab7.Text = Resources.UI_Left;
            lab8.Text = Resources.UI_Right;
            lab9.Text = Resources.UI_Down;
            lab10.Text = Resources.UI_Up;
            lab11.Text = Resources.UI_BigScale;
            lab12.Text = Resources.UI_LessScale;

            Text = Resources.UI_Title;
            groupControl1.Text = Resources.UI_Control1;
            groupControl2.Text = Resources.UI_Control2;
            groupControl3.Text = Resources.UI_Control3;
            groupControl4.Text = Resources.UI_Control4;
        }

        private void simpleButton1_Click( object sender, EventArgs e )
        {
            Close();
        }
    }
}