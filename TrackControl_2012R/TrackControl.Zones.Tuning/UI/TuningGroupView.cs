using DevExpress.XtraBars;
using DevExpress.XtraEditors;

using System;
using TrackControl.General;
using TrackControl.Zones.Tuning.Properties;

namespace TrackControl.Zones.Tuning
{
    public partial class TuningGroupView : XtraUserControl
    {
        private TuningGroup _tuning;

        public event Action<TuningGroup> SaveClicked;
        public event Action<TuningGroup> CancelClicked;
        public event Action<ZonesGroup> DeleteGroupOnlyClicked;
        public event Action<TuningGroup> DeleteGroupFullyClicked;

        public TuningGroupView()
        {
            InitializeComponent();
            init();
        }

        public void Assign(TuningGroup tuning)
        {
            if (null == tuning)
                throw new ArgumentNullException("tuning");

            if (null != _tuning)
                _tuning.StatusChanged -= setSaveCancelVisibility;

            _tuning = tuning;
            _mainBtn.Glyph = _tuning.Icon;
            _grid.SelectedObject = _tuning;

            if (_tuning.IsRoot)
                assignRootMode();
            else
                assignGroupMode();
        }

        private void init()
        {
            this._tools.Text = Resources.UI_TuningTools;
            this._mainBtn.Caption = Resources.UI_TuningAction;
            this._deleteGroupOnlyPopupBtn.Caption = Resources.UI_TuningGroupDeleteOnlyGroup;
            this._deleteGroupFullyPopupBtn.Caption = Resources.UI_TuningGroupDeleteFullGroup;
            this._deleteGroupOnlyToolsBtn.Caption = Resources.UI_TuningGroupDeleteOnlyGroup;
            this._deleteGroupFullyToolsBtn.Caption = Resources.UI_TuningGroupDeleteFullGroup;
            this._idRow.Properties.Caption = Resources.UI_TuningGroupID;
            this._nameRow.Properties.Caption = Resources.UI_TuningName;
            this._descriptionRow.Properties.Caption = Resources.UI_TuningDescription;
            this._zonesCountRow.Properties.Caption = Resources.UI_TuningGroupNumZones;
            this._saveBtn.Text = Resources.UI_TuningSave;
            this._cancelBtn.Text = Resources.UI_TuningCancel;

            erOutLink.Properties.Caption = Resources.OuterBaseLink;

            _cancelBtn.Image = Shared.Cancel;
            _deleteGroupFullyPopupBtn.Glyph = Shared.Cross;
            _deleteGroupFullyToolsBtn.Glyph = Shared.Cross;
            _deleteGroupOnlyPopupBtn.Glyph = Shared.FolderMinus;
            _deleteGroupOnlyToolsBtn.Glyph = Shared.FolderMinus;
            _saveBtn.Image = Shared.Save;
        }

        private void nameRepo_EditValueChanged(object sender, EventArgs e)
        {
            TextEdit edit = (TextEdit) sender;
            _tuning.Name = (string) edit.EditValue;
        }

        private void descRepo_EditValueChanged(object sender, EventArgs e)
        {
            MemoEdit edit = (MemoEdit) sender;
            _tuning.Description = (string) edit.EditValue;
        }

        private void saveBtn_Click(object sender, EventArgs e)
        {
            onSaveClicked(_tuning);
        }

        private void cancelBtn_Click(object sender, EventArgs e)
        {
            onCancelClicked(_tuning);
        }

        private void deleteGroupOnly_ItemClick(object sender, ItemClickEventArgs e)
        {
            onDeleteGroupOnlyClicked(_tuning.Group);
        }

        private void deleteGroupFully_ItemClick(object sender, ItemClickEventArgs e)
        {
            onDeleteGroupFullyClicked(_tuning);
        }

        private void setSaveCancelVisibility()
        {
            if (TuningStatus.Ok != _tuning.Status)
            {
                _saveBtn.Visible = true;
                _cancelBtn.Visible = true;
            }
            else
            {
                _saveBtn.Visible = false;
                _cancelBtn.Visible = false;
            }
        }

        private void onSaveClicked(TuningGroup tuning)
        {
            Action<TuningGroup> handler = SaveClicked;
            if (null != handler)
                handler(tuning);
        }

        private void onCancelClicked(TuningGroup tuning)
        {
            Action<TuningGroup> handler = CancelClicked;
            if (null != handler)
                handler(tuning);
        }

        private void onDeleteGroupOnlyClicked(ZonesGroup group)
        {
            Action<ZonesGroup> handler = DeleteGroupOnlyClicked;
            if (null != handler)
                handler(group);
        }

        private void onDeleteGroupFullyClicked(TuningGroup tuning)
        {
            Action<TuningGroup> handler = DeleteGroupFullyClicked;
            if (null != handler)
                handler(tuning);
        }

        private void assignRootMode()
        {
            _deleteGroupOnlyToolsBtn.Enabled = false;
            _deleteGroupOnlyPopupBtn.Visibility = BarItemVisibility.Never;
            _deleteGroupFullyToolsBtn.Visibility = BarItemVisibility.Never;
            _deleteGroupFullyPopupBtn.Visibility = BarItemVisibility.Never;
            _nameRow.OptionsRow.AllowFocus = false;
            _descriptionRow.OptionsRow.AllowFocus = false;
            _saveBtn.Visible = false;
            _cancelBtn.Visible = false;
        }

        private void assignGroupMode()
        {
            _deleteGroupOnlyToolsBtn.Enabled = true;
            _deleteGroupOnlyPopupBtn.Visibility = BarItemVisibility.Always;
            if (_tuning.Group.AllItems.Count > 0)
            {
                _deleteGroupFullyPopupBtn.Visibility = BarItemVisibility.Always;
                _deleteGroupFullyToolsBtn.Visibility = BarItemVisibility.Always;
            }
            else
            {
                _deleteGroupFullyPopupBtn.Visibility = BarItemVisibility.Never;
                _deleteGroupFullyToolsBtn.Visibility = BarItemVisibility.Never;
            }
            _nameRow.OptionsRow.AllowFocus = true;
            _descriptionRow.OptionsRow.AllowFocus = true;
            setSaveCancelVisibility();
            _tuning.StatusChanged += setSaveCancelVisibility;
        }

        private void rteOutLink_EditValueChanged(object sender, EventArgs e)
        {
            TextEdit edit = (TextEdit) sender;
            _tuning.IdOutLink = (string) edit.EditValue;
        }
    }
}
