namespace TrackControl.Zones.Tuning
{
  partial class TuningGroupView
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.components = new System.ComponentModel.Container();
        this._barManager = new DevExpress.XtraBars.BarManager(this.components);
        this._tools = new DevExpress.XtraBars.Bar();
        this._mainBtn = new DevExpress.XtraBars.BarButtonItem();
        this._mainPopup = new DevExpress.XtraBars.PopupMenu(this.components);
        this._deleteGroupOnlyPopupBtn = new DevExpress.XtraBars.BarButtonItem();
        this._deleteGroupFullyPopupBtn = new DevExpress.XtraBars.BarButtonItem();
        this._deleteGroupOnlyToolsBtn = new DevExpress.XtraBars.BarButtonItem();
        this._deleteGroupFullyToolsBtn = new DevExpress.XtraBars.BarButtonItem();
        this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
        this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
        this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
        this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
        this._grid = new DevExpress.XtraVerticalGrid.PropertyGridControl();
        this._descRepo = new DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit();
        this._nameRepo = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
        this._idRow = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
        this._nameRow = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
        this._descriptionRow = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
        this._zonesCountRow = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
        this.erOutLink = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
        this._saveBtn = new DevExpress.XtraEditors.SimpleButton();
        this._cancelBtn = new DevExpress.XtraEditors.SimpleButton();
        this.rteOutLink = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
        ((System.ComponentModel.ISupportInitialize)(this._barManager)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this._mainPopup)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this._grid)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this._descRepo)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this._nameRepo)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.rteOutLink)).BeginInit();
        this.SuspendLayout();
        // 
        // _barManager
        // 
        this._barManager.AllowCustomization = false;
        this._barManager.AllowMoveBarOnToolbar = false;
        this._barManager.AllowQuickCustomization = false;
        this._barManager.AllowShowToolbarsPopup = false;
        this._barManager.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this._tools});
        this._barManager.DockControls.Add(this.barDockControlTop);
        this._barManager.DockControls.Add(this.barDockControlBottom);
        this._barManager.DockControls.Add(this.barDockControlLeft);
        this._barManager.DockControls.Add(this.barDockControlRight);
        this._barManager.Form = this;
        this._barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this._mainBtn,
            this._deleteGroupOnlyPopupBtn,
            this._deleteGroupFullyPopupBtn,
            this._deleteGroupOnlyToolsBtn,
            this._deleteGroupFullyToolsBtn});
        this._barManager.MaxItemId = 5;
        // 
        // _tools
        // 
        this._tools.BarName = "Tools";
        this._tools.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Top;
        this._tools.DockCol = 0;
        this._tools.DockRow = 0;
        this._tools.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
        this._tools.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this._mainBtn),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.None, this._deleteGroupOnlyToolsBtn, "", true, false, true, 0),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.None, this._deleteGroupFullyToolsBtn, "", false, false, true, 0)});
        this._tools.OptionsBar.AllowQuickCustomization = false;
        this._tools.OptionsBar.DisableClose = true;
        this._tools.OptionsBar.DisableCustomization = true;
        this._tools.OptionsBar.DrawDragBorder = false;
        this._tools.OptionsBar.UseWholeRow = true;
        this._tools.Text = "Tools";
        // 
        // _mainBtn
        // 
        this._mainBtn.ActAsDropDown = true;
        this._mainBtn.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
        this._mainBtn.Appearance.Options.UseFont = true;
        this._mainBtn.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
        this._mainBtn.Caption = "��������";
        this._mainBtn.DropDownControl = this._mainPopup;
        this._mainBtn.Id = 0;
        this._mainBtn.Name = "_mainBtn";
        this._mainBtn.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph;
        // 
        // _mainPopup
        // 
        this._mainPopup.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this._deleteGroupOnlyPopupBtn),
            new DevExpress.XtraBars.LinkPersistInfo(this._deleteGroupFullyPopupBtn)});
        this._mainPopup.Manager = this._barManager;
        this._mainPopup.Name = "_mainPopup";
        // 
        // _deleteGroupOnlyPopupBtn
        // 
        this._deleteGroupOnlyPopupBtn.Caption = "������� ������ ������";
        this._deleteGroupOnlyPopupBtn.Id = 1;
        this._deleteGroupOnlyPopupBtn.Name = "_deleteGroupOnlyPopupBtn";
        this._deleteGroupOnlyPopupBtn.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.deleteGroupOnly_ItemClick);
        // 
        // _deleteGroupFullyPopupBtn
        // 
        this._deleteGroupFullyPopupBtn.Caption = "������� ������ � ����������";
        this._deleteGroupFullyPopupBtn.Id = 2;
        this._deleteGroupFullyPopupBtn.Name = "_deleteGroupFullyPopupBtn";
        this._deleteGroupFullyPopupBtn.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.deleteGroupFully_ItemClick);
        // 
        // _deleteGroupOnlyToolsBtn
        // 
        this._deleteGroupOnlyToolsBtn.Caption = "������� ������";
        this._deleteGroupOnlyToolsBtn.Id = 3;
        this._deleteGroupOnlyToolsBtn.Name = "_deleteGroupOnlyToolsBtn";
        this._deleteGroupOnlyToolsBtn.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.deleteGroupOnly_ItemClick);
        // 
        // _deleteGroupFullyToolsBtn
        // 
        this._deleteGroupFullyToolsBtn.Caption = "������� ������ � ����������";
        this._deleteGroupFullyToolsBtn.Id = 4;
        this._deleteGroupFullyToolsBtn.Name = "_deleteGroupFullyToolsBtn";
        this._deleteGroupFullyToolsBtn.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.deleteGroupFully_ItemClick);
        // 
        // _grid
        // 
        this._grid.Location = new System.Drawing.Point(5, 32);
        this._grid.Name = "_grid";
        this._grid.RecordWidth = 117;
        this._grid.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this._descRepo,
            this._nameRepo,
            this.rteOutLink});
        this._grid.RowHeaderWidth = 83;
        this._grid.Rows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this._idRow,
            this._nameRow,
            this._descriptionRow,
            this._zonesCountRow,
            this.erOutLink});
        this._grid.Size = new System.Drawing.Size(350, 156);
        this._grid.TabIndex = 4;
        // 
        // _descRepo
        // 
        this._descRepo.MaxLength = 200;
        this._descRepo.Name = "_descRepo";
        this._descRepo.EditValueChanged += new System.EventHandler(this.descRepo_EditValueChanged);
        // 
        // _nameRepo
        // 
        this._nameRepo.AutoHeight = false;
        this._nameRepo.MaxLength = 50;
        this._nameRepo.Name = "_nameRepo";
        this._nameRepo.EditValueChanged += new System.EventHandler(this.nameRepo_EditValueChanged);
        // 
        // _idRow
        // 
        this._idRow.Appearance.Options.UseTextOptions = true;
        this._idRow.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
        this._idRow.Name = "_idRow";
        this._idRow.OptionsRow.AllowFocus = false;
        this._idRow.OptionsRow.AllowMove = false;
        this._idRow.OptionsRow.AllowMoveToCustomizationForm = false;
        this._idRow.OptionsRow.AllowSize = false;
        this._idRow.OptionsRow.DblClickExpanding = false;
        this._idRow.OptionsRow.ShowInCustomizationForm = false;
        this._idRow.Properties.Caption = "ID";
        this._idRow.Properties.FieldName = "Id";
        this._idRow.Properties.ReadOnly = true;
        // 
        // _nameRow
        // 
        this._nameRow.Name = "_nameRow";
        this._nameRow.OptionsRow.AllowMove = false;
        this._nameRow.OptionsRow.AllowMoveToCustomizationForm = false;
        this._nameRow.OptionsRow.AllowSize = false;
        this._nameRow.OptionsRow.DblClickExpanding = false;
        this._nameRow.OptionsRow.ShowInCustomizationForm = false;
        this._nameRow.Properties.Caption = "��������";
        this._nameRow.Properties.FieldName = "Name";
        this._nameRow.Properties.RowEdit = this._nameRepo;
        // 
        // _descriptionRow
        // 
        this._descriptionRow.Height = 80;
        this._descriptionRow.Name = "_descriptionRow";
        this._descriptionRow.OptionsRow.AllowMove = false;
        this._descriptionRow.OptionsRow.AllowMoveToCustomizationForm = false;
        this._descriptionRow.OptionsRow.AllowSize = false;
        this._descriptionRow.OptionsRow.DblClickExpanding = false;
        this._descriptionRow.OptionsRow.ShowInCustomizationForm = false;
        this._descriptionRow.Properties.Caption = "��������";
        this._descriptionRow.Properties.FieldName = "Description";
        this._descriptionRow.Properties.RowEdit = this._descRepo;
        // 
        // _zonesCountRow
        // 
        this._zonesCountRow.Appearance.Options.UseTextOptions = true;
        this._zonesCountRow.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
        this._zonesCountRow.Name = "_zonesCountRow";
        this._zonesCountRow.OptionsRow.AllowFocus = false;
        this._zonesCountRow.OptionsRow.AllowMove = false;
        this._zonesCountRow.OptionsRow.AllowMoveToCustomizationForm = false;
        this._zonesCountRow.OptionsRow.AllowSize = false;
        this._zonesCountRow.OptionsRow.DblClickExpanding = false;
        this._zonesCountRow.OptionsRow.ShowInCustomizationForm = false;
        this._zonesCountRow.Properties.Caption = "����� ���";
        this._zonesCountRow.Properties.FieldName = "ZonesCount";
        this._zonesCountRow.Properties.ReadOnly = true;
        // 
        // erOutLink
        // 
        this.erOutLink.Name = "erOutLink";
        this.erOutLink.Properties.Caption = "��� ������� ����";
        this.erOutLink.Properties.FieldName = "IdOutLink";
        this.erOutLink.Properties.RowEdit = this.rteOutLink;
        // 
        // _saveBtn
        // 
        this._saveBtn.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
        this._saveBtn.Appearance.Options.UseFont = true;
        this._saveBtn.Location = new System.Drawing.Point(361, 128);
        this._saveBtn.Name = "_saveBtn";
        this._saveBtn.Size = new System.Drawing.Size(93, 25);
        this._saveBtn.TabIndex = 5;
        this._saveBtn.Text = "���������";
        this._saveBtn.Click += new System.EventHandler(this.saveBtn_Click);
        // 
        // _cancelBtn
        // 
        this._cancelBtn.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
        this._cancelBtn.Appearance.Options.UseFont = true;
        this._cancelBtn.Location = new System.Drawing.Point(361, 159);
        this._cancelBtn.Name = "_cancelBtn";
        this._cancelBtn.Size = new System.Drawing.Size(93, 23);
        this._cancelBtn.TabIndex = 6;
        this._cancelBtn.Text = "������";
        this._cancelBtn.Click += new System.EventHandler(this.cancelBtn_Click);
        // 
        // rteOutLink
        // 
        this.rteOutLink.AutoHeight = false;
        this.rteOutLink.MaxLength = 20;
        this.rteOutLink.Name = "rteOutLink";
        this.rteOutLink.EditValueChanged += new System.EventHandler(this.rteOutLink_EditValueChanged);
        // 
        // TuningGroupView
        // 
        this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        this.Controls.Add(this._cancelBtn);
        this.Controls.Add(this._saveBtn);
        this.Controls.Add(this._grid);
        this.Controls.Add(this.barDockControlLeft);
        this.Controls.Add(this.barDockControlRight);
        this.Controls.Add(this.barDockControlBottom);
        this.Controls.Add(this.barDockControlTop);
        this.Name = "TuningGroupView";
        this.Size = new System.Drawing.Size(575, 235);
        ((System.ComponentModel.ISupportInitialize)(this._barManager)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this._mainPopup)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this._grid)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this._descRepo)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this._nameRepo)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.rteOutLink)).EndInit();
        this.ResumeLayout(false);

    }

    #endregion

    private DevExpress.XtraBars.BarManager _barManager;
    private DevExpress.XtraBars.Bar _tools;
    private DevExpress.XtraBars.BarDockControl barDockControlTop;
    private DevExpress.XtraBars.BarDockControl barDockControlBottom;
    private DevExpress.XtraBars.BarDockControl barDockControlLeft;
    private DevExpress.XtraBars.BarDockControl barDockControlRight;
    private DevExpress.XtraBars.BarButtonItem _mainBtn;
    private DevExpress.XtraBars.PopupMenu _mainPopup;
    private DevExpress.XtraBars.BarButtonItem _deleteGroupOnlyPopupBtn;
    private DevExpress.XtraBars.BarButtonItem _deleteGroupFullyPopupBtn;
    private DevExpress.XtraVerticalGrid.PropertyGridControl _grid;
    private DevExpress.XtraVerticalGrid.Rows.EditorRow _nameRow;
    private DevExpress.XtraVerticalGrid.Rows.EditorRow _idRow;
    private DevExpress.XtraVerticalGrid.Rows.EditorRow _descriptionRow;
    private DevExpress.XtraVerticalGrid.Rows.EditorRow _zonesCountRow;
    private DevExpress.XtraEditors.SimpleButton _cancelBtn;
    private DevExpress.XtraEditors.SimpleButton _saveBtn;
    private DevExpress.XtraEditors.Repository.RepositoryItemMemoEdit _descRepo;
    private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit _nameRepo;
    private DevExpress.XtraBars.BarButtonItem _deleteGroupOnlyToolsBtn;
    private DevExpress.XtraBars.BarButtonItem _deleteGroupFullyToolsBtn;
    private DevExpress.XtraVerticalGrid.Rows.EditorRow erOutLink;
    private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit rteOutLink;
  }
}
