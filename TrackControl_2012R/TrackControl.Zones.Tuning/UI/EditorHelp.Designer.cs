namespace TrackControl.Zones.Tuning
{
  partial class EditorHelp
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditorHelp));
            this._table = new System.Windows.Forms.TableLayoutPanel();
            this._btn = new DevExpress.XtraEditors.SimpleButton();
            this._addingGroup = new DevExpress.XtraEditors.GroupControl();
            this._tab1 = new System.Windows.Forms.TableLayoutPanel();
            this._lab2 = new DevExpress.XtraEditors.LabelControl();
            this._lab1 = new DevExpress.XtraEditors.LabelControl();
            this._lab3 = new DevExpress.XtraEditors.LabelControl();
            this._editingGroup = new DevExpress.XtraEditors.GroupControl();
            this._tab2 = new System.Windows.Forms.TableLayoutPanel();
            this._lab4 = new DevExpress.XtraEditors.LabelControl();
            this._lab5 = new DevExpress.XtraEditors.LabelControl();
            this._lab6 = new DevExpress.XtraEditors.LabelControl();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this._table.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._addingGroup)).BeginInit();
            this._addingGroup.SuspendLayout();
            this._tab1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._editingGroup)).BeginInit();
            this._editingGroup.SuspendLayout();
            this._tab2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // _table
            // 
            this._table.ColumnCount = 1;
            this._table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._table.Controls.Add(this._btn, 0, 3);
            this._table.Controls.Add(this._addingGroup, 0, 0);
            this._table.Controls.Add(this._editingGroup, 0, 1);
            this._table.Controls.Add(this.panelControl1, 0, 2);
            this._table.Dock = System.Windows.Forms.DockStyle.Fill;
            this._table.Location = new System.Drawing.Point(0, 0);
            this._table.Name = "_table";
            this._table.RowCount = 4;
            this._table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 54.5098F));
            this._table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 45.4902F));
            this._table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 54F));
            this._table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 68F));
            this._table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this._table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this._table.Size = new System.Drawing.Size(382, 380);
            this._table.TabIndex = 0;
            // 
            // _btn
            // 
            this._btn.Dock = System.Windows.Forms.DockStyle.Right;
            this._btn.Location = new System.Drawing.Point(287, 311);
            this._btn.Margin = new System.Windows.Forms.Padding(10, 0, 10, 10);
            this._btn.Name = "_btn";
            this._btn.Size = new System.Drawing.Size(85, 59);
            this._btn.TabIndex = 3;
            this._btn.Text = "�������";
            this._btn.Click += new System.EventHandler(this._btn_Click);
            // 
            // _addingGroup
            // 
            this._addingGroup.Controls.Add(this._tab1);
            this._addingGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this._addingGroup.Location = new System.Drawing.Point(10, 10);
            this._addingGroup.Margin = new System.Windows.Forms.Padding(10);
            this._addingGroup.Name = "_addingGroup";
            this._addingGroup.Size = new System.Drawing.Size(362, 120);
            this._addingGroup.TabIndex = 0;
            this._addingGroup.Text = "��� ���������� ����";
            // 
            // _tab1
            // 
            this._tab1.BackColor = System.Drawing.Color.White;
            this._tab1.ColumnCount = 1;
            this._tab1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._tab1.Controls.Add(this._lab1, 0, 0);
            this._tab1.Controls.Add(this._lab3, 0, 2);
            this._tab1.Controls.Add(this._lab2, 0, 1);
            this._tab1.Dock = System.Windows.Forms.DockStyle.Fill;
            this._tab1.Location = new System.Drawing.Point(2, 21);
            this._tab1.Name = "_tab1";
            this._tab1.RowCount = 3;
            this._tab1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this._tab1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.00001F));
            this._tab1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this._tab1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 16F));
            this._tab1.Size = new System.Drawing.Size(358, 97);
            this._tab1.TabIndex = 0;
            // 
            // _lab2
            // 
            this._lab2.AllowHtmlString = true;
            this._lab2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("_lab2.Appearance.Image")));
            this._lab2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this._lab2.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lab2.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this._lab2.Location = new System.Drawing.Point(0, 33);
            this._lab2.Margin = new System.Windows.Forms.Padding(0);
            this._lab2.Name = "_lab2";
            this._lab2.Size = new System.Drawing.Size(358, 34);
            this._lab2.TabIndex = 1;
            this._lab2.Text = "<b>���������� ����� �����:</b>  ����� ������ ����. ����� ��������� ��������� ����" +
    "���. ������� ���� �� ������ ������������. ��� �����!";
            // 
            // _lab1
            // 
            this._lab1.AllowHtmlString = true;
            this._lab1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("_lab1.Appearance.Image")));
            this._lab1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this._lab1.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lab1.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this._lab1.Location = new System.Drawing.Point(0, 0);
            this._lab1.Margin = new System.Windows.Forms.Padding(0);
            this._lab1.Name = "_lab1";
            this._lab1.Size = new System.Drawing.Size(358, 33);
            this._lab1.TabIndex = 0;
            this._lab1.Text = "<b>��������� �����:</b>  ����� ������ ����. ����� ��������� ������ �������.";
            // 
            // _lab3
            // 
            this._lab3.AllowHtmlString = true;
            this._lab3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this._lab3.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lab3.Location = new System.Drawing.Point(42, 67);
            this._lab3.Margin = new System.Windows.Forms.Padding(42, 0, 0, 0);
            this._lab3.Name = "_lab3";
            this._lab3.Size = new System.Drawing.Size(316, 30);
            this._lab3.TabIndex = 2;
            this._lab3.Text = "<b>��������� ����������:</b>  ������ ������ ����. ����� ��������� ��������� �����" +
    "��.";
            // 
            // _editingGroup
            // 
            this._editingGroup.Controls.Add(this._tab2);
            this._editingGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this._editingGroup.Location = new System.Drawing.Point(10, 140);
            this._editingGroup.Margin = new System.Windows.Forms.Padding(10, 0, 10, 10);
            this._editingGroup.Name = "_editingGroup";
            this._editingGroup.Size = new System.Drawing.Size(362, 107);
            this._editingGroup.TabIndex = 1;
            this._editingGroup.Text = "��� �������������� ����";
            // 
            // _tab2
            // 
            this._tab2.BackColor = System.Drawing.Color.White;
            this._tab2.ColumnCount = 1;
            this._tab2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._tab2.Controls.Add(this._lab4, 0, 0);
            this._tab2.Controls.Add(this._lab5, 0, 1);
            this._tab2.Controls.Add(this._lab6, 0, 2);
            this._tab2.Location = new System.Drawing.Point(2, 21);
            this._tab2.Name = "_tab2";
            this._tab2.RowCount = 3;
            this._tab2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this._tab2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this._tab2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this._tab2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 8F));
            this._tab2.Size = new System.Drawing.Size(358, 86);
            this._tab2.TabIndex = 0;
            // 
            // _lab4
            // 
            this._lab4.AllowHtmlString = true;
            this._lab4.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("_lab4.Appearance.Image")));
            this._lab4.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this._lab4.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lab4.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this._lab4.Location = new System.Drawing.Point(0, 0);
            this._lab4.Margin = new System.Windows.Forms.Padding(0);
            this._lab4.Name = "_lab4";
            this._lab4.Size = new System.Drawing.Size(358, 28);
            this._lab4.TabIndex = 0;
            this._lab4.Text = "<b>������� ����� �����:</b> Ctrl + ����� ������ ����. ��� ��������� ������� �� ��" +
    "����� ����.";
            // 
            // _lab5
            // 
            this._lab5.AllowHtmlString = true;
            this._lab5.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("_lab5.Appearance.Image")));
            this._lab5.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this._lab5.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lab5.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this._lab5.Location = new System.Drawing.Point(0, 28);
            this._lab5.Margin = new System.Windows.Forms.Padding(0);
            this._lab5.Name = "_lab5";
            this._lab5.Size = new System.Drawing.Size(358, 28);
            this._lab5.TabIndex = 1;
            this._lab5.Text = "<b>�������� �����:</b> Alt + ����� ������ ����. ��� ��������� ������� �� ���� �� " +
    "������ ����. � ���� ������ ���� ����� ���� ������.";
            // 
            // _lab6
            // 
            this._lab6.AllowHtmlString = true;
            this._lab6.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("_lab6.Appearance.Image")));
            this._lab6.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this._lab6.Dock = System.Windows.Forms.DockStyle.Fill;
            this._lab6.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this._lab6.Location = new System.Drawing.Point(5, 56);
            this._lab6.Margin = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this._lab6.Name = "_lab6";
            this._lab6.Size = new System.Drawing.Size(353, 30);
            this._lab6.TabIndex = 2;
            this._lab6.Text = "<b>����������� �������:</b> ����� ������ ����. ��� ������� ����� �� ������. �����" +
    "��, ����� ������� ���� �� ������������ ����� �����.";
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.labelControl2);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(3, 260);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(376, 48);
            this.panelControl1.TabIndex = 6;
            // 
            // labelControl1
            // 
            this.labelControl1.AllowHtmlString = true;
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl1.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.labelControl1.Location = new System.Drawing.Point(3, 2);
            this.labelControl1.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(360, 21);
            this.labelControl1.TabIndex = 8;
            this.labelControl1.Text = "<b>�������: \'=\'</b> ��������� ������� �����.";
            // 
            // labelControl2
            // 
            this.labelControl2.AllowHtmlString = true;
            this.labelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl2.ImageAlignToText = DevExpress.XtraEditors.ImageAlignToText.LeftCenter;
            this.labelControl2.Location = new System.Drawing.Point(3, 23);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(0);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(363, 22);
            this.labelControl2.TabIndex = 7;
            this.labelControl2.Text = "<b>�������: \'-\'</b> ��������� ������� �����.";
            // 
            // EditorHelp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(382, 380);
            this.Controls.Add(this._table);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(398, 418);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(398, 418);
            this.Name = "EditorHelp";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "��������� ��� ��������������";
            this._table.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._addingGroup)).EndInit();
            this._addingGroup.ResumeLayout(false);
            this._tab1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._editingGroup)).EndInit();
            this._editingGroup.ResumeLayout(false);
            this._tab2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.TableLayoutPanel _table;
    private DevExpress.XtraEditors.GroupControl _addingGroup;
    private DevExpress.XtraEditors.GroupControl _editingGroup;
    private System.Windows.Forms.TableLayoutPanel _tab1;
    private System.Windows.Forms.TableLayoutPanel _tab2;
    private DevExpress.XtraEditors.LabelControl _lab1;
    private DevExpress.XtraEditors.LabelControl _lab2;
    private DevExpress.XtraEditors.LabelControl _lab3;
    private DevExpress.XtraEditors.LabelControl _lab4;
    private DevExpress.XtraEditors.LabelControl _lab5;
    private DevExpress.XtraEditors.LabelControl _lab6;
    private DevExpress.XtraEditors.SimpleButton _btn;
    private DevExpress.XtraEditors.PanelControl panelControl1;
    private DevExpress.XtraEditors.LabelControl labelControl1;
    private DevExpress.XtraEditors.LabelControl labelControl2;

  }
}