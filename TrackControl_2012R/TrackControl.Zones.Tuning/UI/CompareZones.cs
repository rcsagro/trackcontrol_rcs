using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using TrackControl.General;
using TrackControl.Zones.Tuning.Properties;

namespace TrackControl.Zones.Tuning
{
    public partial class CompareZones : DevExpress.XtraEditors.XtraForm
    {
        public CompareZones()
        {
            InitializeComponent();
            Localization();
        }
        public void SetZones(IZone zone_from,IZone zone_to)
        {
            zonePropertyFrom.SetZone(zone_from);
            zonePropertyTo.SetZone(zone_to); 
        }
        void Localization()
        {
            sbSave.Text  = Resources.UI_TuningSave;
            sbSave.Image = Shared.Save;

            sbCancel.Text = Resources.UI_TuningCancel;
            sbCancel.Image = Shared.Cancel;
             
        }
    }
}