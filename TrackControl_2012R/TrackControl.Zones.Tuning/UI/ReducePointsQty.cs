﻿using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using TrackControl.General;
using TrackControl.GMap;
using TrackControl.GMap.Core;
using TrackControl.Zones.Tuning.Properties;  

namespace TrackControl.Zones.Tuning
{
    public partial class ReducePointsQty : DevExpress.XtraEditors.XtraForm
    {
        GoogleMapControl _mcGoogle;
        IZone _zone;
        IZone _zoneNew;
        List<PointLatLng> _initPoints;
        PropertyClass _prop;
        double _epsilon;
        bool _firstDraw = true;
        const int _defaultMeters = 1;

        public ReducePointsQty(IZone zone)
        {
            InitializeComponent();
            Localization();
            InitGoogleMapControl();
            _zone = zone;
            _epsilon = _defaultMeters;
            beiMaxDist.EditValue = _defaultMeters;
            SetDataSource();
            DrawZones();
        }

        private void SetDataSource()
        {
            _prop = new PropertyClass();
            _prop.ColorZone = _zone.Style.Color ;
            _prop.ColorNewZone = Color.BlueViolet;
            _prop.PointsBefor = _zone.Points.Count();
            _prop.SquareBefor = Math.Round(_zone.AreaGa, 3);
            _prop.ViewZone = true;
            _prop.ViewNewZone = true;
            _initPoints = _zone.Points.OfType<PointLatLng>().ToList();
            pgZones.SelectedObject = _prop;
        }

        private void InitGoogleMapControl()
        {
            _mcGoogle = new GoogleMapControl();
            pnMap.Controls.Add(_mcGoogle);
            _mcGoogle.Dock = DockStyle.Fill;
            _mcGoogle.MapType = MapType.GoogleMap;
            _mcGoogle.IsKeepZonesSelected = true;
        }

        private void InitYandexMapControl()
        {
            _mcGoogle = new GoogleMapControl();
            pnMap.Controls.Add(_mcGoogle);
            _mcGoogle.Dock = DockStyle.Fill;
            _mcGoogle.MapType = MapType.YandexMap;
            _mcGoogle.IsKeepZonesSelected = true;
        }

        void DrawZones()
        {
            _mcGoogle.ClearZones();
            if (_prop.ViewZone)
            {
                _mcGoogle.AddZone(_zone);
                if (_firstDraw)
                {
                    _mcGoogle.ZoomAndCenterZones();
                    _firstDraw = false;
                }
            }
            if (_prop.ViewNewZone)
            {
                double dist = _epsilon * StaticMethods.GetMetrAvgLatLng(_zone.Points[0].Lng, _zone.Points[0].Lat);
                _zoneNew = new Zone(_zone.Name, "DouglasPeucker", new ZonesGroup("test","test"), new ZonesStyle(_prop.ColorNewZone), null, "", null, null)
                    {
                        Points = DouglasPeuckerReduction.SetDouglasPeuckerReduction(_initPoints, dist).ToArray(),
                        IsGeometryChanged = true
                    };
                _mcGoogle.AddZone(_zoneNew);
                _prop.PointsAfter = _zoneNew.Points.Count();
                _prop.SquareAfter = Math.Round(_zoneNew.AreaGa, 3);
                pgZones.Refresh();
            }
            
        }



        private void bbiRecalc_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DrawZones();
            bbiAssign.Enabled = true; 
        }

        private void bbiAssign_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            AssignNewPoints();
        }

        void AssignNewPoints()
        {
            if (_zoneNew == null) return;
            if (DialogResult.Yes == XtraMessageBox.Show(Resources.ConfirmAssignment,_zone.Name,  MessageBoxButtons.YesNo, MessageBoxIcon.Question))
            {
                _zone.Points = _zoneNew.Points;
                DrawZones();
                bbiAssign.Enabled = false; 
            }
        }

        void Localization()
        {
            bbiAssign.Caption = Resources.Apply;
            bbiAssign.Glyph = Shared.ZonePoints;
            bbiRecalc.Caption = Resources.Recalc;
            bbiRecalc.Glyph = Shared.Calculator;
            bbiCancel.Caption = Resources.UI_TuningCancel;
            bbiCancel.Glyph = Shared.Cancel; 
            this.Text = Resources.UI_TuningViewCutPointsHint;
            crZoneAfter.Properties.Caption = Resources.ZoneAfter ;
            crZoneBefor.Properties.Caption = Resources.ZoneBefor;
            erPointsAfter.Properties.Caption = Resources.Points;
            erPointsBefor.Properties.Caption = Resources.Points;
            erSquareAfter.Properties.Caption = Resources.ZonesAreaGa;
            erSquareBefor.Properties.Caption = Resources.ZonesAreaGa;
            erColorZone.Properties.Caption = Resources.Color;
            erColorNewZone.Properties.Caption = Resources.Color;
            erViewZone.Properties.Caption = Resources.Show;
            erViewNewZone.Properties.Caption = Resources.Show;  
            bsiMaxDist.Caption = Resources.DistanceM;  
        }

        private class PropertyClass
        {
            public int PointsBefor { get; set; }
            public double SquareBefor { get; set; }
            public int PointsAfter { get; set; }
            public double SquareAfter { get; set; }
            public Color ColorZone { get; set; }
            public Color ColorNewZone { get; set; }
            public bool ViewZone { get; set; }
            public bool ViewNewZone { get; set; }
        }

        private void ReducePointsQty_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (_initPoints != null) _initPoints.Clear();
            _zoneNew = null;
        }

        private void pgZones_CellValueChanging(object sender, DevExpress.XtraVerticalGrid.Events.CellValueChangedEventArgs e)
        {
            pgZones.SetCellValue(e.Row ,e.CellIndex,e.Value);
            DrawZones();
        }

        private void beiMaxDist_EditValueChanged(object sender, EventArgs e)
        {
            Double.TryParse(beiMaxDist.EditValue.ToString(), out _epsilon);    
        }

        private void bbiCancel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            _zone.Points = _initPoints.ToArray() ; 
        }
    }
}