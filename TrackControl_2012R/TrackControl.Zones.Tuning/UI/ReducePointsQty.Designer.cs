﻿namespace TrackControl.Zones.Tuning
{
    partial class ReducePointsQty
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReducePointsQty));
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.bbiRecalc = new DevExpress.XtraBars.BarButtonItem();
            this.bbiAssign = new DevExpress.XtraBars.BarButtonItem();
            this.bbiCancel = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.pnMap = new DevExpress.XtraEditors.PanelControl();
            this.pnControls = new DevExpress.XtraEditors.PanelControl();
            this.pgZones = new DevExpress.XtraVerticalGrid.PropertyGridControl();
            this.rclColor = new DevExpress.XtraEditors.Repository.RepositoryItemColorEdit();
            this.crZoneBefor = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.erPointsBefor = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erSquareBefor = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erColorZone = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erViewZone = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.crZoneAfter = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.erPointsAfter = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erSquareAfter = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erColorNewZone = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erViewNewZone = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.chVisible = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.bsiMaxDist = new DevExpress.XtraBars.BarStaticItem();
            this.beiMaxDist = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnMap)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnControls)).BeginInit();
            this.pnControls.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pgZones)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rclColor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chVisible)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiAssign,
            this.bbiRecalc,
            this.bbiCancel,
            this.bsiMaxDist,
            this.beiMaxDist});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 6;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit1});
            // 
            // bar2
            // 
            this.bar2.BarName = "Главное меню";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bbiRecalc, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bbiAssign, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bbiCancel, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(this.bsiMaxDist),
            new DevExpress.XtraBars.LinkPersistInfo(this.beiMaxDist)});
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Главное меню";
            // 
            // bbiRecalc
            // 
            this.bbiRecalc.Caption = "Пересчитать";
            this.bbiRecalc.Id = 1;
            this.bbiRecalc.Name = "bbiRecalc";
            this.bbiRecalc.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRecalc_ItemClick);
            // 
            // bbiAssign
            // 
            this.bbiAssign.Caption = "Присвоить";
            this.bbiAssign.Id = 0;
            this.bbiAssign.Name = "bbiAssign";
            this.bbiAssign.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAssign_ItemClick);
            // 
            // bbiCancel
            // 
            this.bbiCancel.Caption = "Отменить";
            this.bbiCancel.Id = 2;
            this.bbiCancel.Name = "bbiCancel";
            this.bbiCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiCancel_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(839, 22);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 577);
            this.barDockControlBottom.Size = new System.Drawing.Size(839, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 22);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 555);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(839, 22);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 555);
            // 
            // pnMap
            // 
            this.pnMap.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnMap.Location = new System.Drawing.Point(12, 28);
            this.pnMap.Name = "pnMap";
            this.pnMap.Size = new System.Drawing.Size(637, 547);
            this.pnMap.TabIndex = 5;
            // 
            // pnControls
            // 
            this.pnControls.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnControls.Controls.Add(this.pgZones);
            this.pnControls.Location = new System.Drawing.Point(648, 29);
            this.pnControls.Name = "pnControls";
            this.pnControls.Size = new System.Drawing.Size(190, 546);
            this.pnControls.TabIndex = 10;
            // 
            // pgZones
            // 
            this.pgZones.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pgZones.Location = new System.Drawing.Point(2, 2);
            this.pgZones.Name = "pgZones";
            this.pgZones.OptionsBehavior.PropertySort = DevExpress.XtraVerticalGrid.PropertySort.NoSort;
            this.pgZones.RecordWidth = 73;
            this.pgZones.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.rclColor,
            this.chVisible});
            this.pgZones.RowHeaderWidth = 127;
            this.pgZones.Rows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.crZoneBefor,
            this.crZoneAfter});
            this.pgZones.Size = new System.Drawing.Size(186, 542);
            this.pgZones.TabIndex = 0;
            this.pgZones.CellValueChanging += new DevExpress.XtraVerticalGrid.Events.CellValueChangedEventHandler(this.pgZones_CellValueChanging);
            // 
            // rclColor
            // 
            this.rclColor.AutoHeight = false;
            this.rclColor.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rclColor.Name = "rclColor";
            // 
            // crZoneBefor
            // 
            this.crZoneBefor.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.erPointsBefor,
            this.erSquareBefor,
            this.erColorZone,
            this.erViewZone});
            this.crZoneBefor.Name = "crZoneBefor";
            this.crZoneBefor.Properties.Caption = "Зона было";
            // 
            // erPointsBefor
            // 
            this.erPointsBefor.Height = 17;
            this.erPointsBefor.Name = "erPointsBefor";
            this.erPointsBefor.Properties.Caption = "Точек";
            this.erPointsBefor.Properties.FieldName = "PointsBefor";
            this.erPointsBefor.Properties.ReadOnly = true;
            // 
            // erSquareBefor
            // 
            this.erSquareBefor.Name = "erSquareBefor";
            this.erSquareBefor.Properties.Caption = "Площадь, га";
            this.erSquareBefor.Properties.FieldName = "SquareBefor";
            this.erSquareBefor.Properties.ReadOnly = true;
            // 
            // erColorZone
            // 
            this.erColorZone.Name = "erColorZone";
            this.erColorZone.Properties.Caption = "Цвет";
            this.erColorZone.Properties.FieldName = "ColorZone";
            this.erColorZone.Properties.ReadOnly = true;
            this.erColorZone.Properties.RowEdit = this.rclColor;
            // 
            // erViewZone
            // 
            this.erViewZone.Name = "erViewZone";
            this.erViewZone.Properties.Caption = "Показать";
            this.erViewZone.Properties.FieldName = "ViewZone";
            this.erViewZone.Properties.RowEdit = this.chVisible;
            // 
            // crZoneAfter
            // 
            this.crZoneAfter.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.erPointsAfter,
            this.erSquareAfter,
            this.erColorNewZone,
            this.erViewNewZone});
            this.crZoneAfter.Name = "crZoneAfter";
            this.crZoneAfter.Properties.Caption = "Зона стало";
            // 
            // erPointsAfter
            // 
            this.erPointsAfter.Name = "erPointsAfter";
            this.erPointsAfter.Properties.Caption = "Точек";
            this.erPointsAfter.Properties.FieldName = "PointsAfter";
            this.erPointsAfter.Properties.ReadOnly = true;
            // 
            // erSquareAfter
            // 
            this.erSquareAfter.Name = "erSquareAfter";
            this.erSquareAfter.Properties.Caption = "Площадь, га";
            this.erSquareAfter.Properties.FieldName = "SquareAfter";
            this.erSquareAfter.Properties.ReadOnly = true;
            // 
            // erColorNewZone
            // 
            this.erColorNewZone.Name = "erColorNewZone";
            this.erColorNewZone.Properties.Caption = "Цвет";
            this.erColorNewZone.Properties.FieldName = "ColorNewZone";
            this.erColorNewZone.Properties.RowEdit = this.rclColor;
            // 
            // erViewNewZone
            // 
            this.erViewNewZone.Appearance.Options.UseTextOptions = true;
            this.erViewNewZone.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.erViewNewZone.Height = 23;
            this.erViewNewZone.Name = "erViewNewZone";
            this.erViewNewZone.Properties.Caption = "Показать";
            this.erViewNewZone.Properties.FieldName = "ViewNewZone";
            this.erViewNewZone.Properties.RowEdit = this.chVisible;
            // 
            // chVisible
            // 
            this.chVisible.AutoHeight = false;
            this.chVisible.Caption = "Check";
            this.chVisible.Name = "chVisible";
            // 
            // bsiMaxDist
            // 
            this.bsiMaxDist.Caption = "Максимальное отклонение, м ";
            this.bsiMaxDist.Id = 4;
            this.bsiMaxDist.Name = "bsiMaxDist";
            this.bsiMaxDist.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // beiMaxDist
            // 
            this.beiMaxDist.Edit = this.repositoryItemTextEdit1;
            this.beiMaxDist.Id = 5;
            this.beiMaxDist.Name = "beiMaxDist";
            this.beiMaxDist.EditValueChanged += new System.EventHandler(this.beiMaxDist_EditValueChanged);
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.Appearance.Options.UseTextOptions = true;
            this.repositoryItemTextEdit1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // ReducePointsQty
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 577);
            this.Controls.Add(this.pnControls);
            this.Controls.Add(this.pnMap);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ReducePointsQty";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Сокращение количества точек по алгоритму  Дугласа - Пекера";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ReducePointsQty_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnMap)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnControls)).EndInit();
            this.pnControls.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pgZones)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rclColor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chVisible)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem bbiAssign;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraEditors.PanelControl pnMap;
        private DevExpress.XtraEditors.PanelControl pnControls;
        private DevExpress.XtraVerticalGrid.PropertyGridControl pgZones;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow crZoneBefor;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erPointsBefor;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erSquareBefor;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow crZoneAfter;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erPointsAfter;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erSquareAfter;
        private DevExpress.XtraEditors.Repository.RepositoryItemColorEdit rclColor;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erColorNewZone;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erViewNewZone;
        private DevExpress.XtraBars.BarButtonItem bbiRecalc;
        private DevExpress.XtraBars.BarButtonItem bbiCancel;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erColorZone;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erViewZone;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chVisible;
        private DevExpress.XtraBars.BarStaticItem bsiMaxDist;
        private DevExpress.XtraBars.BarEditItem beiMaxDist;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
    }
}