﻿namespace TrackControl.Zones.Tuning.UI
{
    partial class EditorHelpXtra
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if( disposing && ( components != null ) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditorHelpXtra));
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.pictureEdit3 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit2 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this._lab31 = new DevExpress.XtraEditors.LabelControl();
            this._lab30 = new DevExpress.XtraEditors.LabelControl();
            this._lab22 = new DevExpress.XtraEditors.LabelControl();
            this._lab21 = new DevExpress.XtraEditors.LabelControl();
            this._lab20 = new DevExpress.XtraEditors.LabelControl();
            this._lab1 = new DevExpress.XtraEditors.LabelControl();
            this._lab0 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.pictureEdit6 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit5 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit4 = new DevExpress.XtraEditors.PictureEdit();
            this._lab62 = new DevExpress.XtraEditors.LabelControl();
            this._lab61 = new DevExpress.XtraEditors.LabelControl();
            this._lab60 = new DevExpress.XtraEditors.LabelControl();
            this._lab52 = new DevExpress.XtraEditors.LabelControl();
            this._lab51 = new DevExpress.XtraEditors.LabelControl();
            this._lab50 = new DevExpress.XtraEditors.LabelControl();
            this._lab41 = new DevExpress.XtraEditors.LabelControl();
            this._lab40 = new DevExpress.XtraEditors.LabelControl();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.pictureEdit10 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit9 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit8 = new DevExpress.XtraEditors.PictureEdit();
            this.pictureEdit7 = new DevExpress.XtraEditors.PictureEdit();
            this.lab10 = new DevExpress.XtraEditors.LabelControl();
            this.lab9 = new DevExpress.XtraEditors.LabelControl();
            this.lab7 = new DevExpress.XtraEditors.LabelControl();
            this.lab8 = new DevExpress.XtraEditors.LabelControl();
            this._btn = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl4 = new DevExpress.XtraEditors.GroupControl();
            this.lab12 = new DevExpress.XtraEditors.LabelControl();
            this.lab11 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit10.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit9.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit8.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit7.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).BeginInit();
            this.groupControl4.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.pictureEdit3);
            this.groupControl1.Controls.Add(this.pictureEdit2);
            this.groupControl1.Controls.Add(this.pictureEdit1);
            this.groupControl1.Controls.Add(this._lab31);
            this.groupControl1.Controls.Add(this._lab30);
            this.groupControl1.Controls.Add(this._lab22);
            this.groupControl1.Controls.Add(this._lab21);
            this.groupControl1.Controls.Add(this._lab20);
            this.groupControl1.Controls.Add(this._lab1);
            this.groupControl1.Controls.Add(this._lab0);
            this.groupControl1.Location = new System.Drawing.Point(2, 3);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(556, 113);
            this.groupControl1.TabIndex = 1;
            this.groupControl1.Text = "При добавлении зоны";
            // 
            // pictureEdit3
            // 
            this.pictureEdit3.EditValue = ((object)(resources.GetObject("pictureEdit3.EditValue")));
            this.pictureEdit3.Location = new System.Drawing.Point(5, 81);
            this.pictureEdit3.Name = "pictureEdit3";
            this.pictureEdit3.Size = new System.Drawing.Size(24, 24);
            this.pictureEdit3.TabIndex = 12;
            // 
            // pictureEdit2
            // 
            this.pictureEdit2.EditValue = ((object)(resources.GetObject("pictureEdit2.EditValue")));
            this.pictureEdit2.Location = new System.Drawing.Point(5, 53);
            this.pictureEdit2.Name = "pictureEdit2";
            this.pictureEdit2.Size = new System.Drawing.Size(24, 24);
            this.pictureEdit2.TabIndex = 11;
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.EditValue = ((object)(resources.GetObject("pictureEdit1.EditValue")));
            this.pictureEdit1.Location = new System.Drawing.Point(5, 24);
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Size = new System.Drawing.Size(24, 24);
            this.pictureEdit1.TabIndex = 10;
            // 
            // _lab31
            // 
            this._lab31.Location = new System.Drawing.Point(179, 85);
            this._lab31.Name = "_lab31";
            this._lab31.Size = new System.Drawing.Size(319, 13);
            this._lab31.TabIndex = 9;
            this._lab31.Text = "\'Правая кнопка мыши\'. Выбор положения последней вершины.";
            // 
            // _lab30
            // 
            this._lab30.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this._lab30.Location = new System.Drawing.Point(33, 85);
            this._lab30.Name = "_lab30";
            this._lab30.Size = new System.Drawing.Size(140, 13);
            this._lab30.TabIndex = 8;
            this._lab30.Text = "Окончание добавления:";
            // 
            // _lab22
            // 
            this._lab22.Location = new System.Drawing.Point(186, 68);
            this._lab22.Name = "_lab22";
            this._lab22.Size = new System.Drawing.Size(269, 13);
            this._lab22.TabIndex = 6;
            this._lab22.Text = "Границы зоны не должны пересекаться. Это важно!";
            // 
            // _lab21
            // 
            this._lab21.Location = new System.Drawing.Point(187, 54);
            this._lab21.Name = "_lab21";
            this._lab21.Size = new System.Drawing.Size(317, 13);
            this._lab21.TabIndex = 5;
            this._lab21.Text = "\'Левая кнопка мыши\'. Выбор положения очередной вершины. ";
            // 
            // _lab20
            // 
            this._lab20.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this._lab20.Location = new System.Drawing.Point(32, 54);
            this._lab20.Name = "_lab20";
            this._lab20.Size = new System.Drawing.Size(149, 13);
            this._lab20.TabIndex = 4;
            this._lab20.Text = "Добавление новой точки:";
            // 
            // _lab1
            // 
            this._lab1.Location = new System.Drawing.Point(144, 29);
            this._lab1.Name = "_lab1";
            this._lab1.Size = new System.Drawing.Size(295, 13);
            this._lab1.TabIndex = 2;
            this._lab1.Text = "\'Левая кнопка мыши\'. Выбор положения первой вершины.";
            // 
            // _lab0
            // 
            this._lab0.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this._lab0.Location = new System.Drawing.Point(33, 29);
            this._lab0.Name = "_lab0";
            this._lab0.Size = new System.Drawing.Size(105, 13);
            this._lab0.TabIndex = 1;
            this._lab0.Text = "Начальная точка:";
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.pictureEdit6);
            this.groupControl2.Controls.Add(this.pictureEdit5);
            this.groupControl2.Controls.Add(this.pictureEdit4);
            this.groupControl2.Controls.Add(this._lab62);
            this.groupControl2.Controls.Add(this._lab61);
            this.groupControl2.Controls.Add(this._lab60);
            this.groupControl2.Controls.Add(this._lab52);
            this.groupControl2.Controls.Add(this._lab51);
            this.groupControl2.Controls.Add(this._lab50);
            this.groupControl2.Controls.Add(this._lab41);
            this.groupControl2.Controls.Add(this._lab40);
            this.groupControl2.Location = new System.Drawing.Point(2, 122);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(556, 122);
            this.groupControl2.TabIndex = 2;
            this.groupControl2.Text = "При редактировании зоны";
            // 
            // pictureEdit6
            // 
            this.pictureEdit6.EditValue = ((object)(resources.GetObject("pictureEdit6.EditValue")));
            this.pictureEdit6.Location = new System.Drawing.Point(5, 85);
            this.pictureEdit6.Name = "pictureEdit6";
            this.pictureEdit6.Size = new System.Drawing.Size(24, 24);
            this.pictureEdit6.TabIndex = 13;
            // 
            // pictureEdit5
            // 
            this.pictureEdit5.EditValue = ((object)(resources.GetObject("pictureEdit5.EditValue")));
            this.pictureEdit5.Location = new System.Drawing.Point(5, 54);
            this.pictureEdit5.Name = "pictureEdit5";
            this.pictureEdit5.Size = new System.Drawing.Size(24, 24);
            this.pictureEdit5.TabIndex = 12;
            // 
            // pictureEdit4
            // 
            this.pictureEdit4.EditValue = ((object)(resources.GetObject("pictureEdit4.EditValue")));
            this.pictureEdit4.Location = new System.Drawing.Point(5, 24);
            this.pictureEdit4.Name = "pictureEdit4";
            this.pictureEdit4.Size = new System.Drawing.Size(24, 24);
            this.pictureEdit4.TabIndex = 11;
            // 
            // _lab62
            // 
            this._lab62.Location = new System.Drawing.Point(180, 104);
            this._lab62.Name = "_lab62";
            this._lab62.Size = new System.Drawing.Size(317, 13);
            this._lab62.TabIndex = 10;
            this._lab62.Text = "Следите, чтобы границы зоны не пересекались между собой.";
            // 
            // _lab61
            // 
            this._lab61.Location = new System.Drawing.Point(180, 90);
            this._lab61.Name = "_lab61";
            this._lab61.Size = new System.Drawing.Size(271, 13);
            this._lab61.TabIndex = 9;
            this._lab61.Text = "\'Левая кнопка мыши\'. При захвате одной из вершин. ";
            // 
            // _lab60
            // 
            this._lab60.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this._lab60.Location = new System.Drawing.Point(34, 90);
            this._lab60.Name = "_lab60";
            this._lab60.Size = new System.Drawing.Size(140, 13);
            this._lab60.TabIndex = 8;
            this._lab60.Text = "Перемещение вершины:";
            // 
            // _lab52
            // 
            this._lab52.Location = new System.Drawing.Point(137, 68);
            this._lab52.Name = "_lab52";
            this._lab52.Size = new System.Drawing.Size(211, 13);
            this._lab52.TabIndex = 6;
            this._lab52.Text = "У зоны должно быть более трех вершин.";
            // 
            // _lab51
            // 
            this._lab51.Location = new System.Drawing.Point(137, 54);
            this._lab51.Name = "_lab51";
            this._lab51.Size = new System.Drawing.Size(393, 13);
            this._lab51.TabIndex = 5;
            this._lab51.Text = "\'Alt + Левая кнопка мыши\'. При наведении курсора на одну из вершин зоны. ";
            // 
            // _lab50
            // 
            this._lab50.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this._lab50.Location = new System.Drawing.Point(33, 54);
            this._lab50.Name = "_lab50";
            this._lab50.Size = new System.Drawing.Size(98, 13);
            this._lab50.TabIndex = 4;
            this._lab50.Text = "Удаление точки:";
            // 
            // _lab41
            // 
            this._lab41.Location = new System.Drawing.Point(164, 29);
            this._lab41.Name = "_lab41";
            this._lab41.Size = new System.Drawing.Size(355, 13);
            this._lab41.TabIndex = 2;
            this._lab41.Text = "\'Ctrl + Левая кнопка мыши\'. При наведении курсора на границу зоны.";
            // 
            // _lab40
            // 
            this._lab40.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this._lab40.Location = new System.Drawing.Point(33, 29);
            this._lab40.Name = "_lab40";
            this._lab40.Size = new System.Drawing.Size(125, 13);
            this._lab40.TabIndex = 1;
            this._lab40.Text = "Вставка новой точки:";
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.pictureEdit10);
            this.groupControl3.Controls.Add(this.pictureEdit9);
            this.groupControl3.Controls.Add(this.pictureEdit8);
            this.groupControl3.Controls.Add(this.pictureEdit7);
            this.groupControl3.Controls.Add(this.lab10);
            this.groupControl3.Controls.Add(this.lab9);
            this.groupControl3.Controls.Add(this.lab7);
            this.groupControl3.Controls.Add(this.lab8);
            this.groupControl3.Location = new System.Drawing.Point(2, 250);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(220, 148);
            this.groupControl3.TabIndex = 3;
            this.groupControl3.Text = "Перемещение картой";
            // 
            // pictureEdit10
            // 
            this.pictureEdit10.EditValue = ((object)(resources.GetObject("pictureEdit10.EditValue")));
            this.pictureEdit10.Location = new System.Drawing.Point(5, 114);
            this.pictureEdit10.Name = "pictureEdit10";
            this.pictureEdit10.Size = new System.Drawing.Size(24, 24);
            this.pictureEdit10.TabIndex = 9;
            // 
            // pictureEdit9
            // 
            this.pictureEdit9.EditValue = ((object)(resources.GetObject("pictureEdit9.EditValue")));
            this.pictureEdit9.Location = new System.Drawing.Point(5, 84);
            this.pictureEdit9.Name = "pictureEdit9";
            this.pictureEdit9.Size = new System.Drawing.Size(24, 24);
            this.pictureEdit9.TabIndex = 8;
            // 
            // pictureEdit8
            // 
            this.pictureEdit8.EditValue = ((object)(resources.GetObject("pictureEdit8.EditValue")));
            this.pictureEdit8.Location = new System.Drawing.Point(5, 54);
            this.pictureEdit8.Name = "pictureEdit8";
            this.pictureEdit8.Size = new System.Drawing.Size(24, 24);
            this.pictureEdit8.TabIndex = 7;
            // 
            // pictureEdit7
            // 
            this.pictureEdit7.EditValue = ((object)(resources.GetObject("pictureEdit7.EditValue")));
            this.pictureEdit7.Location = new System.Drawing.Point(5, 24);
            this.pictureEdit7.Name = "pictureEdit7";
            this.pictureEdit7.Size = new System.Drawing.Size(24, 24);
            this.pictureEdit7.TabIndex = 6;
            // 
            // lab10
            // 
            this.lab10.Location = new System.Drawing.Point(33, 118);
            this.lab10.Name = "lab10";
            this.lab10.Size = new System.Drawing.Size(181, 13);
            this.lab10.TabIndex = 5;
            this.lab10.Text = "Клавиша: Сдвинуть карту в вверх.";
            // 
            // lab9
            // 
            this.lab9.Location = new System.Drawing.Point(34, 89);
            this.lab9.Name = "lab9";
            this.lab9.Size = new System.Drawing.Size(168, 13);
            this.lab9.TabIndex = 4;
            this.lab9.Text = "Клавиша: Сдвинуть карту в низ.";
            // 
            // lab7
            // 
            this.lab7.Location = new System.Drawing.Point(35, 29);
            this.lab7.Name = "lab7";
            this.lab7.Size = new System.Drawing.Size(175, 13);
            this.lab7.TabIndex = 3;
            this.lab7.Text = "Клавиша: Сдвинуть карту в лево.";
            // 
            // lab8
            // 
            this.lab8.Location = new System.Drawing.Point(33, 60);
            this.lab8.Name = "lab8";
            this.lab8.Size = new System.Drawing.Size(181, 13);
            this.lab8.TabIndex = 2;
            this.lab8.Text = "Клавиша: Сдвинуть карту в право.";
            // 
            // _btn
            // 
            this._btn.Location = new System.Drawing.Point(482, 409);
            this._btn.Name = "_btn";
            this._btn.Size = new System.Drawing.Size(75, 23);
            this._btn.TabIndex = 4;
            this._btn.Text = "Закрыть";
            this._btn.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // groupControl4
            // 
            this.groupControl4.Controls.Add(this.lab12);
            this.groupControl4.Controls.Add(this.lab11);
            this.groupControl4.Location = new System.Drawing.Point(228, 252);
            this.groupControl4.Name = "groupControl4";
            this.groupControl4.Size = new System.Drawing.Size(330, 146);
            this.groupControl4.TabIndex = 5;
            this.groupControl4.Text = "Изменение масштаба карты";
            // 
            // lab12
            // 
            this.lab12.Location = new System.Drawing.Point(6, 46);
            this.lab12.Name = "lab12";
            this.lab12.Size = new System.Drawing.Size(251, 13);
            this.lab12.TabIndex = 3;
            this.lab12.Text = "Kлавиша: \'NumLock -. Уменьшить масштаб карты.";
            // 
            // lab11
            // 
            this.lab11.Location = new System.Drawing.Point(5, 27);
            this.lab11.Name = "lab11";
            this.lab11.Size = new System.Drawing.Size(252, 13);
            this.lab11.TabIndex = 2;
            this.lab11.Text = "Клавиша: \'NumLock +\' Увеличить масштаб карты.";
            // 
            // EditorHelpXtra
            // 
            this.ActiveGlowColor = System.Drawing.Color.Empty;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(561, 440);
            this.Controls.Add(this.groupControl4);
            this.Controls.Add(this._btn);
            this.Controls.Add(this.groupControl3);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.groupControl1);
            this.FormBorderEffect = DevExpress.XtraEditors.FormBorderEffect.Shadow;
            this.InactiveGlowColor = System.Drawing.Color.Empty;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(577, 478);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(577, 478);
            this.Name = "EditorHelpXtra";
            this.ShowInTaskbar = false;
            this.Text = "Подсказка для редактирования объектов на карте";
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            this.groupControl3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit10.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit9.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit8.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit7.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).EndInit();
            this.groupControl4.ResumeLayout(false);
            this.groupControl4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.LabelControl _lab0;
        private DevExpress.XtraEditors.LabelControl _lab22;
        private DevExpress.XtraEditors.LabelControl _lab21;
        private DevExpress.XtraEditors.LabelControl _lab20;
        private DevExpress.XtraEditors.LabelControl _lab1;
        private DevExpress.XtraEditors.LabelControl _lab31;
        private DevExpress.XtraEditors.LabelControl _lab30;
        private DevExpress.XtraEditors.LabelControl _lab40;
        private DevExpress.XtraEditors.LabelControl _lab50;
        private DevExpress.XtraEditors.LabelControl _lab41;
        private DevExpress.XtraEditors.LabelControl _lab60;
        private DevExpress.XtraEditors.LabelControl _lab52;
        private DevExpress.XtraEditors.LabelControl _lab51;
        private DevExpress.XtraEditors.LabelControl _lab62;
        private DevExpress.XtraEditors.LabelControl _lab61;
        private DevExpress.XtraEditors.LabelControl lab8;
        private DevExpress.XtraEditors.LabelControl lab7;
        private DevExpress.XtraEditors.LabelControl lab9;
        private DevExpress.XtraEditors.LabelControl lab10;
        private DevExpress.XtraEditors.SimpleButton _btn;
        private DevExpress.XtraEditors.PictureEdit pictureEdit3;
        private DevExpress.XtraEditors.PictureEdit pictureEdit2;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private DevExpress.XtraEditors.PictureEdit pictureEdit6;
        private DevExpress.XtraEditors.PictureEdit pictureEdit5;
        private DevExpress.XtraEditors.PictureEdit pictureEdit4;
        private DevExpress.XtraEditors.PictureEdit pictureEdit10;
        private DevExpress.XtraEditors.PictureEdit pictureEdit9;
        private DevExpress.XtraEditors.PictureEdit pictureEdit8;
        private DevExpress.XtraEditors.PictureEdit pictureEdit7;
        private DevExpress.XtraEditors.GroupControl groupControl4;
        private DevExpress.XtraEditors.LabelControl lab12;
        private DevExpress.XtraEditors.LabelControl lab11;

    }
}