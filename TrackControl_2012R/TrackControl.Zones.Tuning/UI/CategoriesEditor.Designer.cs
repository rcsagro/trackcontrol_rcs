﻿namespace TrackControl.Zones.Tuning.UI
{
    partial class CategoriesEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if( disposing && ( components != null ) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gcCategory = new DevExpress.XtraGrid.GridControl();
            this.gvCategory = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            ((System.ComponentModel.ISupportInitialize)(this.gcCategory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvCategory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // gcCategory
            // 
            this.gcCategory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcCategory.Location = new System.Drawing.Point(0, 0);
            this.gcCategory.MainView = this.gvCategory;
            this.gcCategory.Name = "gcCategory";
            this.gcCategory.Size = new System.Drawing.Size(729, 340);
            this.gcCategory.TabIndex = 2;
            this.gcCategory.UseEmbeddedNavigator = true;
            this.gcCategory.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvCategory,
            this.gridView2});
            // 
            // gvCategory
            // 
            this.gvCategory.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvCategory.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvCategory.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gvCategory.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvCategory.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvCategory.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvCategory.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvCategory.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvCategory.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gvCategory.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvCategory.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvCategory.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvCategory.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvCategory.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gvCategory.Appearance.Empty.Options.UseBackColor = true;
            this.gvCategory.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvCategory.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvCategory.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gvCategory.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvCategory.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gvCategory.Appearance.EvenRow.Options.UseForeColor = true;
            this.gvCategory.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvCategory.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvCategory.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gvCategory.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvCategory.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvCategory.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gvCategory.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvCategory.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gvCategory.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvCategory.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvCategory.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvCategory.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.gvCategory.Appearance.FixedLine.Options.UseBackColor = true;
            this.gvCategory.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gvCategory.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gvCategory.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gvCategory.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gvCategory.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(192)))), ((int)(((byte)(157)))));
            this.gvCategory.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(219)))), ((int)(((byte)(188)))));
            this.gvCategory.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gvCategory.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvCategory.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gvCategory.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvCategory.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvCategory.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvCategory.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvCategory.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvCategory.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvCategory.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gvCategory.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvCategory.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvCategory.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvCategory.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvCategory.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvCategory.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvCategory.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gvCategory.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvCategory.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvCategory.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gvCategory.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(242)))), ((int)(((byte)(213)))));
            this.gvCategory.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gvCategory.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gvCategory.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvCategory.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvCategory.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvCategory.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvCategory.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gvCategory.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvCategory.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gvCategory.Appearance.GroupRow.Options.UseForeColor = true;
            this.gvCategory.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvCategory.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvCategory.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gvCategory.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvCategory.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvCategory.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gvCategory.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvCategory.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvCategory.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gvCategory.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvCategory.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.gvCategory.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gvCategory.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvCategory.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvCategory.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvCategory.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvCategory.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gvCategory.Appearance.OddRow.Options.UseBackColor = true;
            this.gvCategory.Appearance.OddRow.Options.UseBorderColor = true;
            this.gvCategory.Appearance.OddRow.Options.UseForeColor = true;
            this.gvCategory.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(247)))));
            this.gvCategory.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gvCategory.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(148)))), ((int)(((byte)(148)))));
            this.gvCategory.Appearance.Preview.Options.UseBackColor = true;
            this.gvCategory.Appearance.Preview.Options.UseFont = true;
            this.gvCategory.Appearance.Preview.Options.UseForeColor = true;
            this.gvCategory.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvCategory.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gvCategory.Appearance.Row.Options.UseBackColor = true;
            this.gvCategory.Appearance.Row.Options.UseForeColor = true;
            this.gvCategory.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvCategory.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gvCategory.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvCategory.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.gvCategory.Appearance.SelectedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvCategory.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.gvCategory.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvCategory.Appearance.SelectedRow.Options.UseBorderColor = true;
            this.gvCategory.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gvCategory.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gvCategory.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gvCategory.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvCategory.Appearance.VertLine.Options.UseBackColor = true;
            this.gvCategory.ColumnPanelRowHeight = 40;
            this.gvCategory.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colId,
            this.colName});
            this.gvCategory.GridControl = this.gcCategory;
            this.gvCategory.IndicatorWidth = 30;
            this.gvCategory.Name = "gvCategory";
            this.gvCategory.OptionsView.EnableAppearanceEvenRow = true;
            this.gvCategory.OptionsView.EnableAppearanceOddRow = true;
            this.gvCategory.OptionsView.ShowGroupPanel = false;
            // 
            // colId
            // 
            this.colId.AppearanceHeader.Options.UseTextOptions = true;
            this.colId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colId.Caption = "Id";
            this.colId.FieldName = "Id";
            this.colId.Name = "colId";
            this.colId.OptionsColumn.AllowEdit = false;
            this.colId.OptionsColumn.ReadOnly = true;
            // 
            // colName
            // 
            this.colName.AppearanceHeader.Options.UseTextOptions = true;
            this.colName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colName.Caption = "Категория";
            this.colName.FieldName = "Name";
            this.colName.Name = "colName";
            this.colName.Visible = true;
            this.colName.VisibleIndex = 0;
            this.colName.Width = 340;
            // 
            // gridView2
            // 
            this.gridView2.GridControl = this.gcCategory;
            this.gridView2.Name = "gridView2";
            // 
            // CategoriesEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(729, 340);
            this.Controls.Add(this.gcCategory);
            this.Name = "CategoriesEditor";
            this.Text = "CategoriesEditor";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.CategoriesEditor_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.gcCategory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvCategory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gcCategory;
        private DevExpress.XtraGrid.Views.Grid.GridView gvCategory;
        private DevExpress.XtraGrid.Columns.GridColumn colId;
        private DevExpress.XtraGrid.Columns.GridColumn colName;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
    }
}