using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.Utils;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using TrackControl.General;
using TrackControl.Zones.Tuning.Properties;
using TrackControl.Zones;
using System.Diagnostics; 

namespace TrackControl.Zones.Tuning
{
    public partial class ZonesTree : XtraUserControl
    {
        private bool _isLoaded;
        private bool _isGaMode;
        private IZonesManager _manager;
        private string _condition;
        private object _current;

        public event ZonesVisibilityChanged VisibilityChanged;
        public event Action<IEntity> SelectionChanged;
        public event Action<IZone> ZoneNodeDoubleClicked;
        public event Action<IZone> PanZoneClicked;
        public event Action<IZone> RemoveZoneClicked;
        public event Action<IZone> UpdateFromMifClicked;
        public event Action<ZonesGroup> RemoveGroupOnlyClicked;
        public event VoidHandler TreeLoaded;


        public ZonesTree(IZonesManager manager)
        {
            _manager = manager;
            InitializeComponent();
            Init();


            initPopupItems();
            setGaMode();
        }

        private void Init()
        {
            this._tools.Text = Resources.UI_TuningTools;
            this._funnelBox.Caption = Resources.UI_ZonesTreeFiltre;
            this._eraseBtn.Caption = Resources.UI_ZonesTreeClearFiltre;
            this._areaBtn.Caption = Resources.UI_TuningZoneViewArea;
            this._areaGaBtn.Caption = Resources.UI_ZonesTreeAreaGa;
            this._areaKmBtn.Caption = Resources.UI_ZonesTreeAreaKm2;
            this._titleCol.Caption = Resources.UI_TuningName;
            bbiExpand.Glyph = Shared.Expand;
            bbiCollapce.Glyph = Shared.Collapce;
            _areaBtn.Glyph = Shared.Area;
            _eraseBtn.Glyph = Shared.FunnelClear;
            bbiRefresh.Caption = Resources.Refresh;
        }

        public IEntity Current
        {
            get
            {
                if (_tree.Selection.Count > 0)
                    return (IEntity)_tree.GetDataRecordByNode(_tree.Selection[0]);
                else
                    return (IEntity)_manager.Root;
            }
        }

        public void RefreshTree()
        {
            _isLoaded = false;
            if (_tree.InvokeRequired)
            {
                MethodInvoker m = delegate { _tree.RefreshDataSource(); };
                _tree.Invoke(m);
            }
            else
                _tree.RefreshDataSource();
        }

        public void SetZoneChecked(IZone zone)
        {
            if (null == zone)
                throw new ArgumentNullException("zone");

            if (_tree.Selection.Count > 0)
            {
                TreeListNode node = _tree.Selection[0];
                if (!node.Checked)
                {
                    IZone z = _tree.GetDataRecordByNode(node) as IZone;
                    if (z == zone)
                    {
                        node.Checked = true;
                        TreeViewService.SetCheckedParentNodes(node);
                        changeVisibility((Entity) zone, true);
                    }
                }
            }
        }
        
        public void UpdateZone(IZone zone)
        {
            if (null == zone)
                throw new ArgumentNullException("zone");

            if (_tree.Selection.Count > 0)
            {
                TreeListNode node = _tree.Selection[0];
                IZone z = _tree.GetDataRecordByNode(node) as IZone;

                if (z == zone)
                {
                    node.SetValue(_titleCol, zone.Name);
                    node.SetValue(_areaCol, _isGaMode ? zone.AreaGa : zone.AreaKm);
                    var group = _tree.GetDataRecordByNode(node.ParentNode) as ZonesGroup;
                    if (group != zone.Group)
                    {
                        var nodeGr = _tree.FindNodeByFieldValue(_idCol.FieldName, zone.Group.Id);
                        if (nodeGr != null) _tree.MoveNode(node, nodeGr);
                    }
                }
            }
        }
        
        public void SetCurrentAndJumpTo(IEntity entity)
        {
            if (null == entity)
                throw new ArgumentNullException("entity");
            if (_tree.Nodes.Count == 0) return;
            _tree.Nodes.FirstNode.Expanded = true;

            if (entity is IZone)
            {
                IZone zone = (IZone) entity;
                if (zone.Group.IsRoot)
                {
                    foreach (TreeListNode node in _tree.Nodes.FirstNode.Nodes)
                    {
                        if (node.Nodes.Count > 0)
                            continue;

                        IZone z = _tree.GetDataRecordByNode(node) as IZone;
                        if (null != z && zone == z)
                        {
                            _tree.SetFocusedNode(node);
                            break;
                        }
                    }
                }
                else
                {
                    foreach (TreeListNode rootChild in _tree.Nodes.FirstNode.Nodes)
                    {
                        ZonesGroup group = _tree.GetDataRecordByNode(rootChild) as ZonesGroup;
                        if (null != group && group == zone.Group)
                        {
                            rootChild.Expanded = true;
                            foreach (TreeListNode zoneNode in rootChild.Nodes)
                            {
                                if (zoneNode.Nodes.Count > 0)
                                    continue;

                                IZone z = _tree.GetDataRecordByNode(zoneNode) as IZone;
                                if (null != z && zone == z)
                                {
                                    _tree.SetFocusedNode(zoneNode);
                                    break;
                                }
                            }
                            break;
                        }
                    }
                }
            }
        }

        private void this_Load(object sender, EventArgs e)
        {
            _tree.DataSource = new object();
            TreeViewService.TreeListInitialView(_tree);
            setCondition(String.Empty);
            if (TreeLoaded != null) TreeLoaded();
        }

        private void tree_VirtualTreeGetChildNodes(object sender, VirtualTreeGetChildNodesInfo e)
        {
            if (!_isLoaded)
            {
                e.Children = new ZonesGroup[] {_manager.Root};
                _isLoaded = true;
            }
            else
            {
                ZonesGroup group = e.Node as ZonesGroup;
                if (null != group)
                {
                    IList children = new List<object>();
                    foreach (ZonesGroup zg in group.OwnGroups)
                        children.Add(zg);
                    foreach (IZone zone in group.OwnItems)
                        children.Add(zone);

                    e.Children = children;
                }
                else // ���� ���� �� �������� �������, ������ �� ����� �������� �����
                {
                    e.Children = new object[] {};
                }
            }
        }

        private void tree_VirtualTreeGetCellValue(object sender, VirtualTreeGetCellValueInfo e)
        {
            if (e.Node is ZonesGroup)
            {
                ZonesGroup group = (ZonesGroup) e.Node;
                if (e.Column == _titleCol)
                    e.CellData = group.NameWithCounter;
                if (e.Column == _idCol)
                    e.CellData = group.Id;
            }
            else if (e.Node is IZone)
            {
                IZone zone = (IZone) e.Node;
                if (e.Column == _titleCol)
                    e.CellData = zone.Name;
                else if (e.Column == _areaCol)
                    e.CellData = _isGaMode ? zone.AreaGa : zone.AreaKm;
            }
        }

        private void tree_BeforeCheckNode(object sender, CheckNodeEventArgs e)
        {
            TreeViewService.FixNodeState(e);
        }

        private void tree_AfterCheckNode(object sender, NodeEventArgs e)
        {
            TreeListNode node = e.Node;
            TreeViewService.SetCheckedChildNodes(node);
            TreeViewService.SetCheckedParentNodes(node);
            _tree.FocusedNode = node;
            Entity entity = (Entity) _tree.GetDataRecordByNode(node);
            changeVisibility(entity, e.Node.Checked);
        }

        private void tree_CustomDrawNodeImages(object sender, CustomDrawNodeImagesEventArgs e)
        {
            Rectangle rect = e.SelectRect;
            rect.X += (rect.Width - 16)/2;
            rect.Y += (rect.Height - 16)/2;
            rect.Width = 16;
            rect.Height = 16;

            object obj = _tree.GetDataRecordByNode(e.Node);
            if (obj is IZone)
            {
                IZone zone = (IZone) obj;
                e.Graphics.DrawImage(zone.Style.Icon, rect);
            }
            else if (obj is ZonesGroup)
            {
                ZonesGroup group = (ZonesGroup) obj;
                e.Graphics.DrawImage(group.Style.Icon, rect);
            }

            e.Handled = true;
        }

        private void tree_CustomDrawNodeCell(object sender, CustomDrawNodeCellEventArgs e)
        {
            object obj = _tree.GetDataRecordByNode(e.Node);
            if (obj is IZone) return;
            e.Appearance.Font = new Font(AppearanceObject.DefaultFont, FontStyle.Bold);

            if (e.Node != _tree.FocusedNode)
                e.Appearance.BackColor = AppearanceObject.ControlAppearance.BorderColor;
        }

        private void tree_AfterFocusNode(object sender, NodeEventArgs e)
        {
            IEntity entity = (IEntity) _tree.GetDataRecordByNode(e.Node);
            onSelectionChanged(entity);
        }

        private void tree_DoubleClick(object sender, EventArgs e)
        {
            Point point = _tree.PointToClient(Control.MousePosition);
            TreeListHitInfo info = _tree.CalcHitInfo(point);
            TreeListNode node = info.Node;
            if (null != node)
            {
                IZone zone = _tree.GetDataRecordByNode(node) as IZone;
                if (null != zone)
                {
                    node.Checked = true;
                    TreeViewService.SetCheckedParentNodes(node);
                    changeVisibility((Entity) zone, true);
                    onZoneNodeDoubleClicked(zone);
                }
            }
        }

        private void tree_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right && ModifierKeys == Keys.None
                && TreeListState.Regular == _tree.State)
            {
                _popup.ItemLinks.Clear();

                Point pt = _tree.PointToClient(MousePosition);
                TreeListHitInfo info = _tree.CalcHitInfo(pt);
                TreeListNode node = info.Node;
                if (null != node)
                {
                    _tree.FocusedNode = node;
                    _current = _tree.GetDataRecordByNode(info.Node);
                    if (_current is IZone)
                    {
                        _popup.ItemLinks.Add(_panZoneBtn);
                        _popup.ItemLinks.Add(_removeZoneBtn);
                        _popup.ItemLinks.Add(_updateFromMif);
                    }
                    else if (_current is ZonesGroup && node.ParentNode != null)
                    {
                        _popup.ItemLinks.Add(_removeGroupOnlyBtn);
                    }
                    _popup.ShowPopup(MousePosition);
                }
            }
        }

        private void tree_FilterNode(object sender, FilterNodeEventArgs e)
        {
            if (_condition == null)
            {
                _condition = "";
            }
            else
            {
                _condition = _condition.ToUpper();
            }

            IZone zone = _tree.GetDataRecordByNode(e.Node) as IZone;
            if (null != zone)
            {
                e.Node.Visible = zone.Name.ToUpper().Contains(_condition);
                e.Handled = true;
            }
        }

        private void funnelRepo_KeyUp(object sender, KeyEventArgs e)
        {
            TextEdit edit = (TextEdit) sender;
            setCondition(edit.Text);
        }

        private void eraseBtn_ItemClick(object sender, ItemClickEventArgs e)
        {
            _funnelBox.EditValue = String.Empty;
            setCondition(String.Empty);
        }

        private void areaGaBtn_ItemClick(object sender, ItemClickEventArgs e)
        {
            setGaMode();
        }

        private void areaKmBtn_ItemClick(object sender, ItemClickEventArgs e)
        {
            setKmMode();
        }

        private void setGaMode()
        {
            _isGaMode = true;
            _areaCol.Caption = Resources.UI_ZonesTreeSquareGa;
            RefreshTree();
        }

        private void setKmMode()
        {
            _isGaMode = false;
            _areaCol.Caption = Resources.UI_ZonesTreeSquareKm2;
            RefreshTree();
        }

        private void setCondition(string condition)
        {
            _condition = condition;
            if (_condition.Length > 0)
            {
                _eraseBtn.Enabled = true;
                _tree.FilterNodes();
                hideEmpty();
            }
            else
            {
                _eraseBtn.Enabled = false;
                TreeListNode root = _tree.Nodes[0];
                TreeViewService.ShowAll(root);
            }
        }

        private void hideEmpty()
        {
            TreeListNode root = _tree.Nodes[0];
            foreach (TreeListNode node in root.Nodes)
            {
                ZonesGroup group = _tree.GetDataRecordByNode(node) as ZonesGroup;
                if (null != group)
                {
                    if (TreeViewService.GetVisibleCount(node) > 0)
                        node.Visible = true;
                    else
                        node.Visible = false;
                }
            }
        }

        private void changeVisibility(Entity entity, bool visibility)
        {
            List<IZone> zones = new List<IZone>();

            if (entity is IZone)
            {
                IZone zone = (IZone) entity;
                zones.Add(zone);
            }
            else if (entity is ZonesGroup)
            {
                ZonesGroup group = (ZonesGroup) entity;
                TuningGroup tuning = (TuningGroup) group.Tag;
                tuning.Visible = visibility;
                zones.AddRange(group.AllItems);
            }

            onVisibilityChanged(zones, visibility);
        }

        private void onVisibilityChanged(IList<IZone> zones, bool visibility)
        {
            ZonesVisibilityChanged handler = VisibilityChanged;
            if (null != handler)
                handler(zones, visibility);
        }

        private void onSelectionChanged(IEntity entity)
        {
            Action<IEntity> handler = SelectionChanged;
            if (null != handler)
                handler(entity);
        }

        private void onZoneNodeDoubleClicked(IZone zone)
        {
            Action<IZone> handler = ZoneNodeDoubleClicked;
            if (null != handler)
                handler(zone);
        }

        private void onPanZoneClicked(IZone zone)
        {
            Action<IZone> handler = PanZoneClicked;
            if (null != handler)
                handler(zone);
        }

        private void onRemoveZoneClicked(IZone zone)
        {
            Action<IZone> handler = RemoveZoneClicked;
            if (null != handler)
                handler(zone);
        }

        private void onRemoveGroupOnlyClicked(ZonesGroup group)
        {
            Action<ZonesGroup> handler = RemoveGroupOnlyClicked;
            if (null != handler)
                handler(group);
        }

        private void onUpdateFromMifClicked(IZone zone)
        {
            Action<IZone> handler = UpdateFromMifClicked;
            if (null != handler)
                handler(zone);
        }

        public bool HasData()
        {
            if (_tree.Nodes.Count == 0)
                return false;
            else
                return true;
        }

        #region --   �������� ������������ ����   --

        private BarButtonItem _panZoneBtn;
        private BarButtonItem _removeZoneBtn;
        private BarButtonItem _removeGroupOnlyBtn;
        private BarButtonItem _updateFromMif;

        private void initPopupItems()
        {
            _panZoneBtn = new BarButtonItem();
            _panZoneBtn.Caption = Resources.UI_ZonesTreeShowZone;
            _panZoneBtn.Glyph = Shared.ZoomFit;
            _panZoneBtn.ItemClick += panZoneBtn_ItemClick;

            _removeZoneBtn = new BarButtonItem();
            _removeZoneBtn.Caption = Resources.UI_TuningZoneViewDeleteZone;
            _removeZoneBtn.Glyph = Shared.Cross;
            _removeZoneBtn.ItemClick += removeZoneBtn_ItemClick;

            _removeGroupOnlyBtn = new BarButtonItem();
            _removeGroupOnlyBtn.Caption = Resources.UI_ZonesTreeDeleteGroup;
            _removeGroupOnlyBtn.Glyph = Shared.FolderMinus;
            _removeGroupOnlyBtn.ItemClick += removeGroupOnlyBtn_ItemClick;

            _updateFromMif = new BarButtonItem();
            _updateFromMif.Caption = Resources.UI_ZonesTreeUpdateFromMif;
            _updateFromMif.Glyph = Shared.ZoneMIF;
            _updateFromMif.ItemClick += updateFromMif_ItemClick;

        }

        // ���������� � Dispose().  ��. ���� "ZonesTreeView.Designer.cs";
        private void disposePopupItems()
        {
            _panZoneBtn.ItemClick -= panZoneBtn_ItemClick;
            _removeZoneBtn.ItemClick -= removeZoneBtn_ItemClick;
            _removeGroupOnlyBtn.ItemClick -= removeGroupOnlyBtn_ItemClick;
            _updateFromMif.ItemClick -= updateFromMif_ItemClick;
        }

        private void panZoneBtn_ItemClick(object sender, ItemClickEventArgs e)
        {
            IZone zone = _current as IZone;
            if (null != zone)
            {
                TreeListNode node = _tree.Selection[0];
                node.Checked = true;
                TreeViewService.SetCheckedParentNodes(node);
                changeVisibility((Entity) zone, true);
                onPanZoneClicked(zone);
            }
        }

        private void removeZoneBtn_ItemClick(object sender, ItemClickEventArgs e)
        {
            IZone zone = _current as IZone;
            if (null != zone)
                onRemoveZoneClicked(zone);
        }

        private void removeGroupOnlyBtn_ItemClick(object sender, ItemClickEventArgs e)
        {
            ZonesGroup group = _current as ZonesGroup;
            if (null != group && !group.IsRoot)
                onRemoveGroupOnlyClicked(group);
        }

        private void updateFromMif_ItemClick(object sender, ItemClickEventArgs e)
        {
            IZone zone = _current as IZone;
            if (null != zone)
                onUpdateFromMifClicked(zone);
        }

        #endregion

        private void bbiExpand_ItemClick(object sender, ItemClickEventArgs e)
        {
            _tree.ExpandAll();
        }

        private void bbiColapce_ItemClick(object sender, ItemClickEventArgs e)
        {
            _tree.CollapseAll();
            TreeViewService.TreeListInitialView(_tree);
        }

        private void bbiRefresh_ItemClick(object sender, ItemClickEventArgs e)
        {
            RefreshTree();
        }
    }
}
