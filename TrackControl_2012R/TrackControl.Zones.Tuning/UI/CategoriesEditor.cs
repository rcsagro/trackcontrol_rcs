﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using TrackControl.General;
using TrackControl.Zones.Tuning.Properties;

namespace TrackControl.Zones.Tuning.UI
{
    public partial class CategoriesEditor : DevExpress.XtraEditors.XtraForm
    {
        public event VoidHandler RefreshCategoriesList;
        bool _categoriesChanged;

        public CategoriesEditor()
        {
            InitializeComponent();
            Localization();

            gcCategory.DataSource = ZonesCategoryProvider.GetList();
            this.gvCategory.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler( this.gvCategory_CellValueChanged );
            this.gcCategory.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler( this.gcCategory_EmbeddedNavigator_ButtonClick );
            this.gvCategory.CustomDrawRowIndicator += new DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventHandler( this.gvCategory_CustomDrawRowIndicator );
        }

        private void gvCategory_CustomDrawRowIndicator( object sender, DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventArgs e )
        {
            if( e.Info.IsRowIndicator )
            {
                if( e.RowHandle >= 0 )
                    e.Info.DisplayText = ( e.RowHandle + 1 ).ToString();
            }
        }

        private void gcCategory_EmbeddedNavigator_ButtonClick( object sender, NavigatorButtonClickEventArgs e )
        {
            if( e.Button.ButtonType == NavigatorButtonType.Remove )
            {
                e.Handled = !DeleteRowVehicleCategory();
                _categoriesChanged = !e.Handled;
            }
        }

        private void gvCategory_CellValueChanged( object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e )
        {
            ZoneCategory zc = ( ZoneCategory )gvCategory.GetRow( e.RowHandle );

            if( zc != null )
            {
                if( zc.Save() ) _categoriesChanged = true;
            }
        }

        bool DeleteRowVehicleCategory()
        {
            ZoneCategory zc = ( ZoneCategory )gvCategory.GetRow( gvCategory.FocusedRowHandle );

            if( zc != null )
            {
                int zones = ZonesCategoryProvider.CountVehiclesWithCategory( zc );

                if( zones > 0 )
                {
                    XtraMessageBox.Show( Resources.DeleteZones, string.Format( Resources.ZonesWithCategory, zones ) );
                    return false;
                }

                if( XtraMessageBox.Show( Resources.ConfirmDeleteQuestion, Resources.ConfirmDeletion,
                    MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question ) == DialogResult.Yes )
                {

                    return zc.Delete();
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        void Localization()
        {
            this.Text = string.Format( "{0}", Resources.UI_ZonesCategoryTipBtn );
            colName.Caption = string.Format( "{0}", Resources.UI_ZonesCategory );
        }

        private void CategoriesEditor_FormClosed( object sender, FormClosedEventArgs e )
        {
            if( _categoriesChanged && RefreshCategoriesList != null )
                RefreshCategoriesList();
        }
    }
}