using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraLayout.Utils;

using System;
using System.Windows.Forms;

using TrackControl.General;
using TrackControl.Zones.Tuning.Properties;
using TrackControl.Zones.Tuning.UI;

namespace TrackControl.Zones.Tuning
{
    public partial class TuningView : XtraUserControl
    {
        private int SplitterPositionOne = 0;
        private bool flagSplitter = false;

        public event VoidHandler AddGroupClicked;
        public event VoidHandler AddZoneClicked;
        public event VoidHandler DeleteCheckedClicked;
        public event VoidHandler WriteZonesToXML;
        public event VoidHandler ExportZonesToCsv;
        public event VoidHandler ReadZonesFromXML;
        public event VoidHandler ReducePointsSelectedZone;
        public event Action<bool> ImporterVisibilityChanged;

        public TuningView()
        {
            InitializeComponent();
            init();

            this.Load += TuningView_Load;
        }

        void TuningView_Load( object sender, EventArgs e )
        {
           // to do
        }

        public void ShowTree(ZonesTree tree)
        {
            tree.Dock = DockStyle.Fill;
            tree.BorderStyle = BorderStyle.FixedSingle;
            tree.Parent = _treePanel;
        }

        public void ShowMap(Control map)
        {
            map.Dock = DockStyle.Fill;
            map.Parent = _mapPanel;
        }

        public void ShowTuning(Control tuning)
        {
            foreach (Control control in _tuningPanel.Controls)
                control.Parent = null;

            tuning.Dock = DockStyle.Fill;
            tuning.Parent = _tuningPanel;

            //splitContainerControl2.SplitterPosition = SplitterPosition;
        }

        public void SetImporter(Control importer, bool visible)
        {
            if (_servicePanel.Controls.Count > 0)
                throw new Exception("Control for imports MIF is already installed. You can not install again.");

            importer.Dock = DockStyle.Fill;
            importer.Parent = _servicePanel;
            _importMifPopupBtn.Down = visible;
            _importMifToolBtn.Down = visible;
            _servicePanel.Visible = visible;
            splitContainerControl3.Panel1.Visible = visible;

            if( visible )
                splitContainerControl3.PanelVisibility = SplitPanelVisibility.Both;
            else
                splitContainerControl3.PanelVisibility = SplitPanelVisibility.Panel2;
        }

        public void ShowImporter()
        {
            _servicePanel.Visible = true;
            splitContainerControl3.Panel1.Visible = true;
            _importMifPopupBtn.Down = true;
            _importMifToolBtn.Down = true;
            splitContainerControl3.PanelVisibility = SplitPanelVisibility.Both;
        }

        public void HideImporter()
        {
            _servicePanel.Visible= false;
            splitContainerControl3.Panel1.Visible = false;
            _importMifPopupBtn.Down = false;
            _importMifToolBtn.Down = false;
            splitContainerControl3.PanelVisibility = SplitPanelVisibility.Panel2;
        }

        private void init()
        {
            this._tools.Text = Resources.UI_TuningTools;
            this._mainBtn.Caption = Resources.UI_TuningAction;
            this._addGroupPopupBtn.Caption = Resources.UI_TuningViewAddGroup;
            this._addZonePopupBtn.Caption = Resources.UI_TuningViewAddZone;
            this._importMifPopupBtn.Caption = Resources.UI_TuningViewImportMIF;
            this._deleteCheckedPopupBtn.Caption = Resources.UI_TuningViewDeleteSelected;
            this._deleteCheckedPopupBtn.Hint = Resources.UI_TuningViewBeDeleteSelected;
            this._addGroupToolBtn.Caption = Resources.UI_TuningViewAddGroup;
            this._addGroupToolBtn.Hint = Resources.UI_TuningViewEnableAddGroup;
            this._addZoneToolBtn.Caption = Resources.UI_TuningViewAddZone;
            this._addZoneToolBtn.Hint = Resources.UI_TuningViewEnableAddCZ;
            this._importMifToolBtn.Caption = Resources.UI_TuningViewImportMIF;
            this._importMifToolBtn.Hint = Resources.UI_TuningViewImportCZfromMIF;
            this._deleteCheckedToolBtn.Caption = Resources.UI_TuningViewDeleteSelected;
            this._deleteCheckedToolBtn.Hint = Resources.UI_TuningViewBeDeleteSelected;
            this._helpBtn.Caption = Resources.UI_TuningViewPrompt;
            //this._rootItem.Text = Resources.UI_TuningViewRoot;
            //this._mapItem.CustomizationFormText = Resources.UI_TuningViewMap;
            //this._mapItem.Text = Resources.UI_TuningViewMap;
            //this._treeItem.CustomizationFormText = Resources.UI_TuningViewTreeZones;
            //this._treeItem.Text = Resources.UI_TuningViewTreeZones;
            //this._tuningItem.CustomizationFormText = Resources.UI_TuningViewEditor;
            //this._tuningItem.Text = Resources.UI_TuningViewEditor;
            //this._splitter.CustomizationFormText = Resources.UI_TuningViewSeparatorTree;
            //this._mapSeparator.CustomizationFormText = Resources.UI_TuningViewSeparator;

            xmlImport.Caption = Resources.UI_TuningViewXMLImport;
            xmlImport.Hint = Resources.UI_TuningViewXMLImportDetal;
            xmlExport.Caption = Resources.UI_TuningViewXMLExport;
            xmlExport.Hint = Resources.UI_TuningViewXMLExportDetal;

            _deleteCheckedPopupBtn.Glyph = Shared.DeleteChecked;
            _deleteCheckedToolBtn.Glyph = Shared.DeleteChecked;
            _addGroupPopupBtn.Glyph = Shared.FolderPlus;
            _addGroupToolBtn.Glyph = Shared.FolderPlus;
            _helpBtn.Glyph = Shared.Help;
            _mainBtn.Glyph = Shared.Settings;
            _importMifPopupBtn.Glyph = Shared.ZoneMIF;
            _importMifToolBtn.Glyph = Shared.ZoneMIF;
            _addZonePopupBtn.Glyph = Shared.ZonePlus;
            _addZoneToolBtn.Glyph = Shared.ZonePlus;

            bbiCutPoints.Glyph = Shared.ZonePoligon;
            bbiCutPoints.Caption = Resources.UI_TuningViewCutPoints;
            bbiCutPoints.Hint = Resources.UI_TuningViewCutPointsHint;

            bbiCsvExport.Hint = Resources.CSVSave;
        }

        private void addGroup_ItemClick(object sender, ItemClickEventArgs e)
        {
            onAddGroupClicked();
        }

        private void addZone_ItemClick(object sender, ItemClickEventArgs e)
        {
            onAddZoneClicked();
        }

        private void importMiflBtn_DownChanged(object sender, ItemClickEventArgs e)
        {
            bool down = ((BarButtonItem) sender).Down;
            _importMifPopupBtn.Down = down;
            _importMifToolBtn.Down = down;
            _servicePanel.Visible = down;
            splitContainerControl3.Panel1.Visible = down;

            if(down)
                splitContainerControl3.PanelVisibility = SplitPanelVisibility.Both;
            else
                splitContainerControl3.PanelVisibility = SplitPanelVisibility.Panel2;

            onImporterVisibilityChanged(down);
        }

        private void deleteChecked_ItemClick(object sender, ItemClickEventArgs e)
        {
            onDeleteCheckedClicked();
        }

        private void helpBtn_ItemClick(object sender, ItemClickEventArgs e)
        {
            using (EditorHelpXtra help = new EditorHelpXtra())
            {
                help.ShowDialog();
            }
        }

        private void onAddGroupClicked()
        {
            VoidHandler handler = AddGroupClicked;
            if (null != handler)
                handler();
        }

        private void onAddZoneClicked()
        {
            VoidHandler handler = AddZoneClicked;
            if (null != handler)
                handler();
        }

        private void onDeleteCheckedClicked()
        {
            VoidHandler handler = DeleteCheckedClicked;
            if (null != handler)
                handler();
        }

        private void onImporterVisibilityChanged(bool visible)
        {
            Action<bool> handler = ImporterVisibilityChanged;
            if (null != handler)
                handler(visible);
        }

        private void xmlExport_ItemClick(object sender, ItemClickEventArgs e)
        {
            VoidHandler handler = WriteZonesToXML;
            if (null != handler)
                handler();
        }

        private void xmlImport_ItemClick(object sender, ItemClickEventArgs e)
        {
            VoidHandler handler = ReadZonesFromXML;
            if (null != handler)
                handler();
        }

        private void bbiCutPoints_ItemClick(object sender, ItemClickEventArgs e)
        {
            VoidHandler handler = ReducePointsSelectedZone;
            if (null != handler)
                handler();
        }

        public void SetSplitPosition( int hght )
        {
            splitContainerControl2.SplitterPosition = Height - hght;
        }

        private void bbiCsvExport_ItemClick(object sender, ItemClickEventArgs e)
        {
            VoidHandler handler = ExportZonesToCsv;
            if (null != handler)
                handler();
        }
    }
}
