﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using TrackControl.General;
using MySql.Data.MySqlClient;
using System.IO;
using System.Collections;
using TrackControl.General.DatabaseDriver;
using TrackControl.General.DAL;

namespace TrackControl.Zones.Tuning
{
    public  class ImportPointsFromXML
    {
        private const int RADIUS_ZONE = 2500;
        private int _groupeZoneId;
        private int _zoneId;
        private double _lat;
        private double _lon;
        private int cnt;

        public ImportPointsFromXML ()
        {
        }

        public void Start ()
        {
            OpenFileDialog dlg = new OpenFileDialog ();
            dlg.Title = "Выберите XML файл для ввода точек в виде контольных зон";
            dlg.Filter = "XML - файл|*.xml";
            if (dlg.ShowDialog () == DialogResult.OK) {
                
                FileInfo fi = new FileInfo (dlg.FileName);
                _groupeZoneId = NewGroupe (fi.Name);
                XmlTextReader rdr = new XmlTextReader (dlg.FileName);
                while (rdr.Read ()) {
                    switch (rdr.NodeType) {
                    case XmlNodeType.Element: // Узел является элементом.
                        if (rdr.Name == "waypoint") {
                            _lat = 0;
                            _lon = 0;
                            _zoneId = 0;
                            while (rdr.MoveToNextAttribute ()) {
                                switch (rdr.Name) {
                                case "name":
                                    _zoneId = NewZone (rdr.Value); 
                                    break;
                                case "latitude":
                                    _lat = Convert.ToDouble (rdr.Value.Replace (".", ","));
                                    break;
                                case "longitude":
                                    _lon = Convert.ToDouble (rdr.Value.Replace (".", ","));
                                    SetZonePoints ();
                                    break;
                                }
                            }
                        }
                        break;
                    }
                }
                rdr.Close ();
                MessageBox.Show ("Импортировано " + cnt + " зон", fi.Name);  
            } else {
                MessageBox.Show ("File not found!");
            }
        }

        private int NewGroupe (string Name)
        {
            //using (ConnectMySQL cn = new ConnectMySQL())
            using (DriverDb db = new DriverDb()) { 
                db.ConnectDb ();

                string sql = TrackControlQuery.ImportPointsFromXML.InsertIntoZonesGroup;

                //MySqlParameter[] parDate = new MySqlParameter[2];
                db.NewSqlParameterArray (2);
                //parDate[0] = new MySqlParameter("?Title", MySqlDbType.String);
                db.NewSqlParameter (db.ParamPrefics + "Title", db.GettingString (), 0);
                //parDate[0].Value = Name;
                db.SetSqlParameterValue (Name, 0);
                //parDate[1] = new MySqlParameter("?Description", MySqlDbType.String);
                db.NewSqlParameter (db.ParamPrefics + "Description", db.GettingString (), 1);
                //parDate[1].Value = "Импорт из XML";
                db.SetSqlParameterValue ("Импорт из XML", 1);
                //return cn.ExecuteReturnLastInsert(sql, parDate);
                return db.ExecuteReturnLastInsert (sql, db.GetSqlParameterArray, "zonesgroup");
            }
        }

        private int NewZone (string Name)
        {
            //using (ConnectMySQL cn = new ConnectMySQL())
            using (DriverDb db = new DriverDb()) { 
                db.ConnectDb ();

                string sql = TrackControlQuery.ImportPointsFromXML.InsertIntoZones;
               
                //MySqlParameter[] parDate = new MySqlParameter[4];
                db.NewSqlParameterArray (4);
                //parDate[0] = new MySqlParameter("?Name", MySqlDbType.String);
                db.NewSqlParameter (db.ParamPrefics + "Name", db.GettingString (), 0);

                if (Name.Length > 50) {
                    //parDate[0].Value = Name.Substring(0, 50);
                    db.SetSqlParameterValue (Name.Substring (0, 50), 0);
                } else {
                    //parDate[0].Value = Name;
                    db.SetSqlParameterValue (Name, 0);
                }

                //parDate[1] = new MySqlParameter("?Description", MySqlDbType.String);
                db.NewSqlParameter (db.ParamPrefics + "Description", db.GettingString (), 1);
                //parDate[1].Value = "Импорт из XML";
                db.SetSqlParameterValue ("Импорт из XML", 1);
                //parDate[2] = new MySqlParameter("?ZonesGroupId", MySqlDbType.Int32);
                db.NewSqlParameter (db.ParamPrefics + "ZonesGroupId", db.GettingInt32 (), 2);
                //parDate[2].Value = _groupeZoneId;
                db.SetSqlParameterValue (_groupeZoneId, 2);
                //parDate[3] = new MySqlParameter("?Square", MySqlDbType.Int32);
                db.NewSqlParameter (db.ParamPrefics + "Square", db.GettingInt32 (), 3);
                //parDate[3].Value = Math.Pow (RADIUS_ZONE  *2,2);
                db.SetSqlParameterValue (Math.Pow (RADIUS_ZONE * 2, 2), 3);
                //return cn.ExecuteReturnLastInsert(sql,parDate);
                return db.ExecuteReturnLastInsert (sql, db.GetSqlParameterArray, "zones");
            }
        }
        // NewZone
        private void SetZonePoints ()
        {
            if (_zoneId == 0 || _lat == 0 || _lon == 0)
                return;
            double _lon_meter = GetMetrLon (_lon, _lat);
            double _lat_meter = GetMetrLat (_lon, _lat);
            AddOnePoint (_lat - _lat_meter * RADIUS_ZONE, _lon);
            AddOnePoint (_lat, _lon - _lon_meter * RADIUS_ZONE);
            AddOnePoint (_lat + _lat_meter * RADIUS_ZONE, _lon);
            AddOnePoint (_lat, _lon + _lon_meter * RADIUS_ZONE);
            
            cnt++;

        }

        private void AddOnePoint (double lat, double lon)
        {
            //using (ConnectMySQL cn = new ConnectMySQL())
            DriverDb db = new DriverDb ();
            db.ConnectDb ();
            {
                string sql = TrackControlQuery.ImportPointsFromXML.InsertIntoPoints;
              
                //MySqlParameter[] parDate = new MySqlParameter[3];
                db.NewSqlParameterArray (3);
                //parDate[0] = new MySqlParameter("?Latitude", MySqlDbType.Int32 );
                db.NewSqlParameter (db.ParamPrefics + "Latitude", db.GettingInt32 (), 0);
                //parDate[0].Value = Math.Ceiling(lat * 600000);
                db.SetSqlParameterValue (Math.Ceiling (lat * 600000), 0);
                //parDate[1] = new MySqlParameter("?Longitude", MySqlDbType.Int32);
                db.NewSqlParameter (db.ParamPrefics + "Longitude", db.GettingInt32 (), 1);
                //parDate[1].Value = Math.Ceiling(lon * 600000);
                db.SetSqlParameterValue (Math.Ceiling (lon * 600000), 1);
                //parDate[2] = new MySqlParameter("?Zone_ID", MySqlDbType.Int32);
                db.NewSqlParameter (db.ParamPrefics + "Zone_ID", db.GettingInt32 (), 2);
                //parDate[2].Value = _zoneId;
                db.SetSqlParameterValue (_zoneId, 2);
                //cn.ExecuteReturnLastInsert(sql, parDate);
                db.ExecuteReturnLastInsert (sql, db.GetSqlParameterArray, "points");
            }
            db.CloseDbConnection ();
        }

        /// <summary>
        /// Определение метра по долготе
        /// </summary>
        private  double GetMetrLon (double Lon, double Lat)
        {
            double segment = 0;
            double step = 0;
            for (int i = 0; i < 100; i++) {
                step = step + 0.000001;
                segment = СGeoDistance.CalculateFromGrad (Lat, Lon, Lat, Lon + step);
                if (segment >= 0.001)
                    break;
            }
            return step;
        }

        /// <summary>
        /// Определение метра по широте
        /// </summary>
        private  double GetMetrLat (double Lon, double Lat)
        {
            double segment = 0;
            double step = 0;
            for (int i = 0; i < 100; i++) {
                step = step + 0.000001;
                segment = СGeoDistance.CalculateFromGrad (Lat, Lon, Lat + step, Lon);
                if (segment >= 0.001)
                    break;
            }
            return step;
        }
    }
}
