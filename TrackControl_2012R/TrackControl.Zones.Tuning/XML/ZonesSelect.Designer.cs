namespace TrackControl.Zones.Tuning
{
    partial class ZonesSelect
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ZonesSelect));
            this.gcZones = new DevExpress.XtraGrid.GridControl();
            this.gvZones = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colGroupName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAreaGa = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateChange = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Id = new DevExpress.XtraGrid.Columns.GridColumn();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.bbiSave = new DevExpress.XtraBars.BarButtonItem();
            this.bbiCancel = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            ((System.ComponentModel.ISupportInitialize)(this.gcZones)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvZones)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // gcZones
            // 
            this.gcZones.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcZones.Location = new System.Drawing.Point(0, 26);
            this.gcZones.MainView = this.gvZones;
            this.gcZones.Name = "gcZones";
            this.gcZones.Size = new System.Drawing.Size(523, 535);
            this.gcZones.TabIndex = 0;
            this.gcZones.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvZones});
            // 
            // gvZones
            // 
            this.gvZones.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvZones.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvZones.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gvZones.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvZones.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvZones.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvZones.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvZones.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvZones.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gvZones.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvZones.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvZones.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvZones.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvZones.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gvZones.Appearance.Empty.Options.UseBackColor = true;
            this.gvZones.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvZones.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvZones.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gvZones.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvZones.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gvZones.Appearance.EvenRow.Options.UseForeColor = true;
            this.gvZones.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvZones.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvZones.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gvZones.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvZones.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvZones.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gvZones.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvZones.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gvZones.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvZones.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvZones.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvZones.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.gvZones.Appearance.FixedLine.Options.UseBackColor = true;
            this.gvZones.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gvZones.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gvZones.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gvZones.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gvZones.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(192)))), ((int)(((byte)(157)))));
            this.gvZones.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(219)))), ((int)(((byte)(188)))));
            this.gvZones.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gvZones.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvZones.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gvZones.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvZones.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvZones.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvZones.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvZones.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvZones.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvZones.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gvZones.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvZones.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvZones.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvZones.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvZones.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvZones.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvZones.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gvZones.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvZones.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvZones.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gvZones.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(242)))), ((int)(((byte)(213)))));
            this.gvZones.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gvZones.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gvZones.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvZones.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvZones.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvZones.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvZones.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gvZones.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvZones.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gvZones.Appearance.GroupRow.Options.UseForeColor = true;
            this.gvZones.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvZones.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvZones.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gvZones.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvZones.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvZones.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gvZones.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvZones.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvZones.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gvZones.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvZones.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.gvZones.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gvZones.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvZones.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvZones.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvZones.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvZones.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gvZones.Appearance.OddRow.Options.UseBackColor = true;
            this.gvZones.Appearance.OddRow.Options.UseBorderColor = true;
            this.gvZones.Appearance.OddRow.Options.UseForeColor = true;
            this.gvZones.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(247)))));
            this.gvZones.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gvZones.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(148)))), ((int)(((byte)(148)))));
            this.gvZones.Appearance.Preview.Options.UseBackColor = true;
            this.gvZones.Appearance.Preview.Options.UseFont = true;
            this.gvZones.Appearance.Preview.Options.UseForeColor = true;
            this.gvZones.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvZones.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gvZones.Appearance.Row.Options.UseBackColor = true;
            this.gvZones.Appearance.Row.Options.UseForeColor = true;
            this.gvZones.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvZones.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gvZones.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvZones.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.gvZones.Appearance.SelectedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvZones.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.gvZones.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvZones.Appearance.SelectedRow.Options.UseBorderColor = true;
            this.gvZones.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gvZones.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gvZones.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gvZones.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvZones.Appearance.VertLine.Options.UseBackColor = true;
            this.gvZones.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colGroupName,
            this.colName,
            this.colAreaGa,
            this.colDateChange,
            this.Id});
            this.gvZones.GridControl = this.gcZones;
            this.gvZones.IndicatorWidth = 30;
            this.gvZones.Name = "gvZones";
            this.gvZones.OptionsSelection.MultiSelect = true;
            this.gvZones.OptionsView.EnableAppearanceEvenRow = true;
            this.gvZones.OptionsView.EnableAppearanceOddRow = true;
            this.gvZones.CustomDrawRowIndicator += new DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventHandler(this.gvZones_CustomDrawRowIndicator);
            // 
            // colGroupName
            // 
            this.colGroupName.AppearanceHeader.Options.UseTextOptions = true;
            this.colGroupName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colGroupName.Caption = "������";
            this.colGroupName.FieldName = "Group.Name";
            this.colGroupName.Name = "colGroupName";
            this.colGroupName.OptionsColumn.AllowEdit = false;
            this.colGroupName.OptionsColumn.AllowFocus = false;
            this.colGroupName.OptionsColumn.ReadOnly = true;
            this.colGroupName.Visible = true;
            this.colGroupName.VisibleIndex = 0;
            this.colGroupName.Width = 90;
            // 
            // colName
            // 
            this.colName.AppearanceHeader.Options.UseTextOptions = true;
            this.colName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colName.Caption = "����������� ����";
            this.colName.FieldName = "Name";
            this.colName.Name = "colName";
            this.colName.OptionsColumn.AllowEdit = false;
            this.colName.OptionsColumn.AllowFocus = false;
            this.colName.OptionsColumn.ReadOnly = true;
            this.colName.Visible = true;
            this.colName.VisibleIndex = 1;
            this.colName.Width = 201;
            // 
            // colAreaGa
            // 
            this.colAreaGa.AppearanceHeader.Options.UseTextOptions = true;
            this.colAreaGa.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colAreaGa.Caption = "������� (��)";
            this.colAreaGa.DisplayFormat.FormatString = "N2";
            this.colAreaGa.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colAreaGa.FieldName = "AreaGa";
            this.colAreaGa.Name = "colAreaGa";
            this.colAreaGa.OptionsColumn.AllowEdit = false;
            this.colAreaGa.OptionsColumn.AllowFocus = false;
            this.colAreaGa.OptionsColumn.ReadOnly = true;
            this.colAreaGa.Visible = true;
            this.colAreaGa.VisibleIndex = 2;
            this.colAreaGa.Width = 103;
            // 
            // colDateChange
            // 
            this.colDateChange.AppearanceCell.Options.UseTextOptions = true;
            this.colDateChange.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDateChange.AppearanceHeader.Options.UseTextOptions = true;
            this.colDateChange.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDateChange.Caption = "���� ���������";
            this.colDateChange.DisplayFormat.FormatString = "g";
            this.colDateChange.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colDateChange.FieldName = "DateChange";
            this.colDateChange.Name = "colDateChange";
            this.colDateChange.OptionsColumn.AllowEdit = false;
            this.colDateChange.OptionsColumn.AllowFocus = false;
            this.colDateChange.OptionsColumn.ReadOnly = true;
            this.colDateChange.Visible = true;
            this.colDateChange.VisibleIndex = 3;
            this.colDateChange.Width = 108;
            // 
            // Id
            // 
            this.Id.Caption = "Id";
            this.Id.FieldName = "Tag";
            this.Id.Name = "Id";
            this.Id.OptionsColumn.AllowEdit = false;
            this.Id.OptionsColumn.AllowFocus = false;
            this.Id.OptionsColumn.ReadOnly = true;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiSave,
            this.bbiCancel});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 2;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bbiSave, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bbiCancel, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // bbiSave
            // 
            this.bbiSave.Caption = "���������";
            this.bbiSave.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiSave.Glyph")));
            this.bbiSave.Id = 0;
            this.bbiSave.Name = "bbiSave";
            this.bbiSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSave_ItemClick);
            // 
            // bbiCancel
            // 
            this.bbiCancel.Caption = "��������";
            this.bbiCancel.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiCancel.Glyph")));
            this.bbiCancel.Id = 1;
            this.bbiCancel.Name = "bbiCancel";
            this.bbiCancel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiCancel_ItemClick);
            // 
            // ZonesSelect
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(523, 561);
            this.Controls.Add(this.gcZones);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ZonesSelect";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "������ ����������� ��� � XML �����";
            this.TopMost = true;
            ((System.ComponentModel.ISupportInitialize)(this.gcZones)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvZones)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gcZones;
        private DevExpress.XtraGrid.Views.Grid.GridView gvZones;
        private DevExpress.XtraGrid.Columns.GridColumn colGroupName;
        private DevExpress.XtraGrid.Columns.GridColumn colName;
        private DevExpress.XtraGrid.Columns.GridColumn colAreaGa;
        private DevExpress.XtraGrid.Columns.GridColumn colDateChange;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem bbiSave;
        private DevExpress.XtraBars.BarButtonItem bbiCancel;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraGrid.Columns.GridColumn Id;
    }
}