using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using TrackControl.General;

namespace TrackControl.Zones.Tuning
{
    public partial class ZonesSelect : DevExpress.XtraEditors.XtraForm
    {
        public Action<List<IZone>> SaveZones;

        public ZonesSelect(List<IZone> zones)
        {
            InitializeComponent();
            gcZones.DataSource = zones;
            Localization();
        }

        private void bbiSave_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (gvZones.SelectedRowsCount == 0)
            {
                XtraMessageBox.Show(Properties.Resources.XMLSelectQuestion, Properties.Resources.XMLRead); 
                return;
            }
            List<IZone> selectedZones = new List<IZone>() ;
            List<int> zonesId = new List<int>() ;
            for (int i = 0; i < gvZones.SelectedRowsCount; i++)
            {
                int zoneId = 0;
                if (Int32.TryParse(gvZones.GetRowCellValue(gvZones.GetSelectedRows()[i], gvZones.Columns["Tag"]).ToString(), out zoneId))
                {
                    if (zoneId > 0 && !zonesId.Contains(zoneId)) zonesId.Add(zoneId);
                }
            }
            CurrencyManager cm = (CurrencyManager)this.BindingContext[gcZones.DataSource];
            foreach (IZone zone in cm.List)
            {
                if (zonesId.Contains(zone.Id)) selectedZones.Add(zone); 
            }
            SaveZones(selectedZones);
            this.Close(); 

            
        }

        private void bbiCancel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            this.Close(); 
        }

        void Localization()
        {
            bbiSave.Caption = Properties.Resources.UI_TuningSave;
            bbiSave.Glyph = Shared.Save;
            bbiCancel.Caption = Properties.Resources.UI_TuningCancel;
            bbiCancel.Glyph = Shared.Cancel;

            colGroupName.Caption = Properties.Resources.UI_TuningZoneViewGroup;
            colName.Caption = Properties.Resources.UI_ZoneSelect;
            colAreaGa.Caption = Properties.Resources.UI_ZonesTreeAreaGa;
            colDateChange.Caption = Properties.Resources.UI_ZoneSelectDate;
        }

        private void gvZones_CustomDrawRowIndicator(object sender, DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventArgs e)
        {
            if (e.Info.IsRowIndicator)
            {
                if (e.RowHandle >= 0) e.Info.DisplayText = (e.RowHandle + 1).ToString();
            }
        }
    }
}