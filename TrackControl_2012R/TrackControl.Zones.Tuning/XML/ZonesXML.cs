﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;
using System.Drawing;
using TrackControl.General;
using TrackControl.Zones.Tuning.Properties ;

namespace TrackControl.Zones.Tuning
{
    public class ZonesXML
    {
        private enum zone_fields 
        {
            Id,
            Name,
            Description,
            AreaKm,
            AreaGa,
            DateChange,
            StyleColor,
            Group,
            Category_id,
            Category_id2,
            CategoryName,
            CategoryName2
        }

        private enum point_fields
        {
            Lat,
            Lng
        }

        private enum groupe_fields
        {
            Id,
            Name,
            Description
        }

        private enum zone_nodes
        {
            zone_data,
            zone,
            groupe,
            point
        }

        string _fileName;

        public string FileName
        {
            get { return _fileName; }
        }


        public bool Write(IList<IZone> zones)
        {
            _fileName = string.Format("Zones_{0}.xml", StaticMethods.SuffixDateToFileName());
            _fileName = StaticMethods.ShowSaveFileDialog(_fileName, Resources.XMLSave, "XML |*.xml");
            if (_fileName.Length == 0) return false;
            XmlTextWriter writer = new XmlTextWriter(_fileName, System.Text.Encoding.Unicode);
            try
            {
                
                writer.Formatting = Formatting.Indented;
                writer.WriteStartDocument(false);
                writer.WriteComment(string.Format("Create {0}", DateTime.Now));
                writer.WriteStartElement(zone_nodes.zone_data.ToString());
                foreach (IZone zone in zones)
                {

                    if (WriteZone(zone, ref writer))
                        writer.Flush();
                }
                writer.WriteEndDocument();
            }
            catch 
            {
                return false ;
            }
          finally
             {
                 if (writer != null)
                     writer.Close();
             }

            return true; 
        }

        bool WriteZone(IZone zone,ref XmlTextWriter writer)
        {
            try
            {
                writer.WriteStartElement(zone_nodes.zone.ToString());
                writer.WriteAttributeString(zone_fields.Id.ToString(), zone.Id.ToString());
                writer.WriteAttributeString(zone_fields.Name.ToString(), zone.Name);
                writer.WriteAttributeString(zone_fields.Description.ToString(), zone.Description);
                writer.WriteAttributeString(zone_fields.AreaKm.ToString(), zone.AreaKm.ToString());
                writer.WriteAttributeString(zone_fields.AreaGa.ToString(), zone.AreaGa.ToString());
                writer.WriteAttributeString(zone_fields.DateChange.ToString(), zone.DateChange.ToString());
                writer.WriteAttributeString(zone_fields.StyleColor.ToString(), zone.Style.Color.ToArgb().ToString());
                writer.WriteAttributeString(zone_fields.Group.ToString(), zone.Group.Id.ToString());

                if (zone.Category != null)
                {
                    writer.WriteAttributeString(zone_fields.Category_id.ToString(), ((ZoneCategory) zone.Category).Id.ToString());
                    writer.WriteAttributeString(zone_fields.CategoryName.ToString(), ((ZoneCategory) zone.Category).Name);
                }
                else
                {
                    writer.WriteAttributeString(zone_fields.Category_id.ToString(), "-1");
                    writer.WriteAttributeString(zone_fields.CategoryName.ToString(), "");
                }

                if (zone.Category2 != null)
                { 
                    writer.WriteAttributeString( zone_fields.Category_id2.ToString(), (( ZoneCategory2)zone.Category2).Id.ToString());
                    writer.WriteAttributeString(zone_fields.CategoryName2.ToString(), ((ZoneCategory2) zone.Category2).Name);
                }
                else
                {
                    writer.WriteAttributeString( zone_fields.Category_id2.ToString(), "-1" );
                    writer.WriteAttributeString( zone_fields.CategoryName2.ToString(), "" );
                }

                writer.WriteEndElement();

                writer.WriteStartElement(zone_nodes.groupe.ToString(), null);
                writer.WriteAttributeString(groupe_fields.Id.ToString(), zone.Group.Id.ToString());
                writer.WriteAttributeString(groupe_fields.Name.ToString(), zone.Group.Name.ToString());
                writer.WriteAttributeString(groupe_fields.Description.ToString(), zone.Group.Description.ToString());
                writer.WriteEndElement();


                foreach (PointLatLng pll in zone.Points)
                {
                    writer.WriteStartElement(zone_nodes.point.ToString());
                    long lat = (long)(pll.Lat * Globals.LAT_LNG_GPS_CF);
                    long lng = (long)(pll.Lng * Globals.LAT_LNG_GPS_CF);
                    writer.WriteAttributeString(point_fields.Lat.ToString(), lat.ToString());
                    writer.WriteAttributeString(point_fields.Lng.ToString(), lng.ToString());
                    writer.WriteEndElement();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public List<IZone>  Read()
        {
            List<IZone> zones = new List<IZone>();
            _fileName = StaticMethods.ShowOpenFileDialog(Resources.XMLFindFile, "XML |*.xml");
            if (_fileName.Length == 0) return zones;
            XmlTextReader reader = new XmlTextReader(_fileName);
            reader.WhitespaceHandling = WhitespaceHandling.None; // пропускаем пустые узлы
            while (reader.Read())
            {
                switch (reader.NodeType)
                {
                    case XmlNodeType.Element: // Узел является элементом.
                        while (reader.Name == zone_nodes.zone.ToString())
                        {
                            zones.Add(ReadZone(reader)); 
                        }
                        break;
                }
            }
            reader.Close();
            return zones;
        }

        IZone ReadZone(XmlTextReader reader)
        {
            IZone zone;
            string name = reader.GetAttribute(zone_fields.Name.ToString());
            string decr = reader.GetAttribute(zone_fields.Description.ToString());

            int iCategory = Convert.ToInt32(reader.GetAttribute( zone_fields.Category_id.ToString() ));
            int iCategory2 = Convert.ToInt32(reader.GetAttribute( zone_fields.Category_id2.ToString() ));
            string CategoryName = reader.GetAttribute( zone_fields.CategoryName.ToString() );
            string CategoryName2 = reader.GetAttribute( zone_fields.CategoryName2.ToString() );

            ZoneCategory znCategory = null;
            ZoneCategory2 znCategory2 = null;

            if(iCategory > 0)
                znCategory = new ZoneCategory( iCategory, CategoryName );

            if(iCategory2 > 0)
                znCategory2 = new ZoneCategory2( iCategory2, CategoryName2 );

            int id_zone = Convert.ToInt32(reader.GetAttribute(zone_fields.Id.ToString()));
            double areaKm = Convert.ToDouble(reader.GetAttribute(zone_fields.AreaKm.ToString()));
            double areaGa = Convert.ToDouble(reader.GetAttribute(zone_fields.AreaGa.ToString()));
            DateTime? dateChange = null;
            DateTime dateFromReader;
            if (DateTime.TryParse(reader.GetAttribute(zone_fields.DateChange.ToString()), out dateFromReader))
                dateChange = dateFromReader;
            Color color = Color.FromArgb(Convert.ToInt32(reader.GetAttribute(zone_fields.StyleColor.ToString())));
            ZonesStyle zoneStyle = new ZonesStyle();
            zoneStyle.Color = color;
            reader.Read();

            if (reader.Name == zone_nodes.groupe.ToString())
            {
                string nameGrp = reader.GetAttribute(groupe_fields.Name.ToString());
                string decrGrp = reader.GetAttribute(groupe_fields.Description.ToString());
                ZonesGroup zoneGroupe = new ZonesGroup(nameGrp, decrGrp);
                zoneGroupe.Id = Convert.ToInt32(reader.GetAttribute(groupe_fields.Id.ToString()));
                
                zone = new Zone(name, decr, zoneGroupe, zoneStyle, dateChange,"", znCategory, znCategory2);  
            }
            else
                zone = new Zone(name, decr, null, zoneStyle, dateChange, "", znCategory, znCategory2);

            zone.Id = id_zone;
            zone.Tag = id_zone;
            zone.AreaGa = areaGa;
            zone.AreaKm = areaKm;
            List<PointLatLng> points = new List<PointLatLng>() ;
            while (reader.Read() && reader.Name == zone_nodes.point.ToString())
            {
                double lat = 0;
                double lng = 0;
                if (double.TryParse(reader.GetAttribute(point_fields.Lat.ToString()), out lat) &&
                double.TryParse(reader.GetAttribute(point_fields.Lng.ToString()), out lng))
                {
                    points.Add(new PointLatLng(lat / Globals.LAT_LNG_GPS_CF, lng / Globals.LAT_LNG_GPS_CF));
                }
            }
            zone.Points = points.ToArray();
            zone.DateChange = dateChange; 
            return zone;
        }
    }
}
