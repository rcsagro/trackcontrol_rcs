﻿using System;
using System.Collections.Generic;
using System.Text;
using TrackControl.General;

namespace TrackControl.Notification
{
    /// <summary>
    /// Почтовый адрес уведомления
    /// </summary>
    public class EMail:Entity
    {
        public EMail()
        {
            Id = 0;
            Addres = "";
            IsActive = false;
        }
        public EMail(int id, string addres,bool isActive)
        {
            Id = id;
            Addres = addres;
            IsActive = isActive;
        }

        public string Addres { get; set; }

        public bool IsActive { get; set; }

        public static bool IsEmailValid(string addres)
        {
            if (addres.Length < 3) return false;
            if (!addres.Contains("@")) return false;
            return addres.Contains(".");
        }
    }
}
