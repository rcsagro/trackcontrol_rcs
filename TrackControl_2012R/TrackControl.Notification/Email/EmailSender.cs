﻿using System;
using System.Net.Mail;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using TrackControl.Notification.Properties;

namespace TrackControl.Notification
{
    internal class EmailSender
    {
        private bool _isReady;

        public EmailSender()
        {
            ReadParameters();
        }

        private void ReadParameters()
        {
            NoticeSetItem nsi = new NoticeSetItem();
            MailFromAdress = nsi.MailAddressFrom;
            SmtpAdress = nsi.MailSmtpAddress;
            SmtpPort = nsi.MailSmtpPort;
            SmtpLogin = nsi.MailSmtpLogin;
            SmtpPassword = nsi.MailSmtpPassword;
            StartTls = nsi.MailStartTls;
            if (!string.IsNullOrEmpty(MailFromAdress) && !string.IsNullOrEmpty(SmtpAdress))
            {
                _isReady = true;
            }
            else
            {
                _isReady = false;
            }
        }


        public string MailSubject { get; set; }

        public string MailBody { get; set; }

        public string MailToAdress { get; set; }

        public string MailFromAdress { get; set; }

        public string SmtpAdress { get; set; }

        public int SmtpPort { get; set; }

        public string SmtpLogin { get; set; }

        public string SmtpPassword { get; set; }

        public bool StartTls { get; set; }

        public bool IsReady
        {
            get
            {
                return _isReady;
            }
        }


        public void SendMailMessage()
        {
            //mail.rcs.kiev.ua
            //    или по 25 или по 26 порту
            //http://www.gotdotnet.ru/forums/3/94430/
            //http://www.codeproject.com/Articles/20546/How-to-Send-Mails-from-your-GMAIL-Account-through

            if (string.IsNullOrEmpty(MailFromAdress))
            {
                XtraMessageBox.Show(Resources.SpecifySendersAddress, Resources.Notifications);
                return;
            }

            if (string.IsNullOrEmpty(MailToAdress))
            {
                XtraMessageBox.Show(Resources.SpecifyRecipientAddress, Resources.Notifications);
                return;
            }

            MailMessage message = new MailMessage(MailFromAdress, MailToAdress);
            message.IsBodyHtml = true;
            message.Subject = MailSubject;
            message.Body = MailBody;
            SmtpClient client = new SmtpClient(SmtpAdress, SmtpPort);
            client.Timeout = 10000;

            if (StartTls) client.EnableSsl = true;

            if (SmtpLogin.Length > 0)
            {
                client.Credentials = new System.Net.NetworkCredential(SmtpLogin, SmtpPassword);
            }
            else
            {
                client.UseDefaultCredentials = true;
            }

            client.Send(message);
        }

        private bool IsMessageParametersTrue()
        {
            return true;
        }
    }
}
