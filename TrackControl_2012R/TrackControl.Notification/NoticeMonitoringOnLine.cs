﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using TrackControl.MySqlDal;
using TrackControl.Notification.Properties;
using TrackControl.Vehicles;
using TrackControl.General;

namespace TrackControl.Notification
{
    /// <summary>
    /// поиск заданного события или совокупности событий по одному транспорту 
    /// </summary>
    public class NoticeMonitoringOnLine
    {
        /// <summary>
        /// Таймер для задания периодичности опроса.
        /// </summary>
        private Timer _timer;

        private int _periodInSeconds;

        private NoticeSetItem _nseti;

        private List<NoticeItem> _nis = new List<NoticeItem>();

        private static bool _isActive;

        public static bool IsActive
        {
            get { return _isActive; }
            set { _isActive = value; }
        }

        private OnlineDataProvider _onlineDP;

        private System.Int64 _lastGPSId;

        private bool _isMonitoringReady = true;

        public static event VoidHandler RecordsCreatedOnLine;

        public NoticeMonitoringOnLine()
        {
            _nseti = new NoticeSetItem();
            _periodInSeconds = _nseti.TimePeriod;
        }

        private void LoadNotices()
        {
            _nis = NoticeProvider.GetNoticeEntityListForAnalizOneQuery();
        }

        public bool StartOnLine()
        {
            if (_isActive)
                return false;

            LoadNotices();

            if (!IsNoticeItemsExists())
                return false;
            _isMonitoringReady = true;
            _onlineDP = new OnlineDataProvider();
            _timer = new Timer();
            _timer.Tick += MonitoringOnLineAllProtocols;
            _timer.Interval = 1000 * _periodInSeconds;
            _timer.Start();
            _isActive = true;
            return true;
        }

        private bool IsNoticeItemsExists()
        {
            if (_nis.Count == 0)
            {
                XtraMessageBox.Show(Resources.NotificationsAbsent);
                return false;
            }
            return true;
        }

        public void StopOnLine()
        {
            if (_timer != null)
            {
                _timer.Enabled = false;
                _timer.Dispose();
            }
            _isMonitoringReady = true;
            _isActive = false;
            _nis.Clear();
        }

        private void MonitoringOnLine(object sender, EventArgs e)
        {
            if (_isMonitoringReady)
            {
                IList<GpsData> gpss = _onlineDP.GetLastData(_lastGPSId);
                MonitoringListGpsData(gpss);
                gpss.Clear();
            }

        }
        private void MonitoringOnLineAllProtocols(object sender, EventArgs e)
        {
            if (_isMonitoringReady)
            {
                MonitoringListGpsDataAllProtocols();
            }
        }

        private void MonitoringListGpsData(IList<GpsData> gpss)
        {
            _isMonitoringReady = false;
            foreach (GpsData gps in gpss)
            {
                _nis.ForEach(delegate(NoticeItem ni)
                {
                    _lastGPSId = gps.Id;
                    if (gps.Time.Subtract(ni.TimeStartControl).TotalSeconds > 0 && ni.CheckEventOnLine(gps))
                    {
                        RecordsCreatedOnLine();
                    }
                }
                    );
                if (!_isActive) return;
                Application.DoEvents();
            }
            if (!_isActive) return;
            _nseti.LastLogId = NoticeProvider.GetLastLogId();
            _nis.ForEach(delegate(NoticeItem ni)
            {
                if (ni.CheckStateOnLine())
                {
                    RecordsCreatedOnLine();
                }
            }
                );
            _isMonitoringReady = true;
        }

        private void MonitoringListGpsDataAllProtocols()
        {
            _isMonitoringReady = false;

                _nis.ForEach(delegate(NoticeItem ni)
                {
                    //_lastGPSId = gps.Id;
                    if (ni.CheckEventOnLineAllProtocols(_onlineDP))
                    {
                        RecordsCreatedOnLine();
                    }
                }
                    );
                if (!_isActive) return;
                Application.DoEvents();

            if (!_isActive) return;
            _nseti.LastLogId = NoticeProvider.GetLastLogId();
            _nis.ForEach(delegate(NoticeItem ni)
            {
                if (ni.CheckStateOnLine())
                {
                    RecordsCreatedOnLine();
                }
            }
                );
            _isMonitoringReady = true;
        }
    }
}
