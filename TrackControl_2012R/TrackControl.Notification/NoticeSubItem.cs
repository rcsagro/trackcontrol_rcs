﻿using System;
using System.Collections.Generic;
using System.Text;
using TrackControl.Vehicles;
using TrackControl.General;

namespace TrackControl.Notification
{
  
    /// <summary>
    /// класс события для уведомления
    /// </summary>
    public abstract class NoticeSubItem : Entity, ICloneable,INoticeSubItem
    {
        public NoticeSubItem(NoticeItem ni)
        {
            Location = "";
            Ni = ni;
        }

        protected DateTime TimePrevEvent = DateTime.MinValue;

        public NoticeSubItemTypes TypeEvent { get; set; }

        public NoticeItem Ni { get; set; }

        public GpsData GpsEvent { get; set; }

        public string Location { get; set; }

        /// <summary>
        /// событие активно. Реакция должна быть после деактивации и повторного срабатывания 
        /// </summary>
        protected bool _isActive;

        public double Value { get; set; }


        public bool CheckEventWithPeriodLimit(GpsData gps)
        {
            bool isEvent = CheckEvent(gps);
            if (isEvent)
            {
                return IsTestPeriod(gps.Time);
            }
            return false;
        }
        /// <summary>
        /// мгновенная реакция на событие
        /// </summary>
        /// <param name="gps"></param>
        /// <returns></returns>
        public virtual bool CheckEvent(GpsData gps)
        {
            return false;
        }

        /// <summary>
        /// реакция на событие после прогона пакета GPS данных
        /// </summary>
        /// <returns></returns>
        public virtual bool CheckState()
        {
            return false;
        }

        public virtual bool Save()
        {
            return false;
        }

        #region ICloneable Members

        object ICloneable.Clone()
        {
            return this.Clone();
        }
        public NoticeSubItem Clone()
        {
            return (NoticeSubItem)this.MemberwiseClone();
        }
        #endregion

        #region Накопление времени между событиями

        /// <summary>
        /// накопитель времени
        /// </summary>
        protected TimeSpan _time_accumulate;
        public TimeSpan Time_accumulate
        {
            get { return _time_accumulate; }
            set { _time_accumulate = value; }
        }
        /// <summary>
        /// дата предыдущей точки GPS
        /// </summary>
        protected DateTime _prevDate = DateTime.MinValue;

        protected void AddAcumulate(GpsData gps)
        {
            _time_accumulate = _time_accumulate.Add(gps.Time.Subtract(_prevDate));
            _prevDate = gps.Time;
        }

        protected void StartAccumulate(GpsData gps)
        {
            _time_accumulate = TimeSpan.Zero;
            _prevDate = gps.Time;
            GpsEvent = gps;
            _isActive = true;
        }

        protected void StopAccumulate()
        {
            _prevDate = DateTime.MinValue;
            _isActive = false;
        }
        protected bool TestAccumulate(GpsData gps, double valueForTest)
        {
            AddAcumulate(gps);
            if (_time_accumulate.TotalMinutes >= valueForTest)
            {
                _time_accumulate = TimeSpan.Zero;
                GpsEvent = gps;
                return true;
            }
            else
                return false;
        }

        protected bool TestAccumulateManyEvents(GpsData gps, double valueForTest)
        {
            AddAcumulate(gps);
            if (_time_accumulate.TotalMinutes >= valueForTest)
            {
                _time_accumulate = TimeSpan.Zero;
                return true;
            }
            else
                return false;
        }

        #endregion

        protected Driver GetDriverFromRfid(Sensor sensorRfidDriver, GpsData gps)
        {
            Driver driverFromRfid = null;
            if (sensorRfidDriver != null)
            {
                var rfidDriver = (ushort)sensorRfidDriver.GetValue(gps.Sensors);
                if (rfidDriver > 0 && rfidDriver < sensorRfidDriver.GetMaxRfidValue())
                {
                    driverFromRfid = new Driver(0, rfidDriver);
                }
            }
            return driverFromRfid;
        }

        protected bool IsTestPeriod(DateTime eventTime)
        {
            if (Ni.Period == 0) return true;
            if (eventTime.Subtract(TimePrevEvent).TotalMinutes >= Ni.Period)
            {
                TimePrevEvent = eventTime;
                return true;
            }
            return false;
        }
    }
}
