﻿using System;
using System.Collections.Generic;
using System.Text;
using TrackControl.Vehicles;
using TrackControl.General;

namespace TrackControl.Notification
{
    public class NoticeLogRecord
    {
        public DateTime DateEvent { set; get; }
        #region Vihicle
        Vehicle _vehicleEvent;
        public Vehicle VehicleEvent
        {
            get { return _vehicleEvent; }
            set { _vehicleEvent = value; }
        }

        public string VehicleFullName
        {
            get { return _vehicleEvent.Info; }
        }

        public int Mobitel_id
        {
            get { return _vehicleEvent.MobitelId; }
        } 
        #endregion
        #region Point
        PointLatLng _geoPoint;
        public PointLatLng GeoPoint
        {
            get { return _geoPoint; }
            set { _geoPoint = value; }
        }
        public double Lat
        {
            get { return _geoPoint.Lat; }
        }
        public double Lng
        {
            get { return _geoPoint.Lng; }
        } 
        #endregion
        public string Infor { set; get; }
        public int TypeEvent { set; get; }
        public string Location { set; get; }
        public Double Speed { set; get; }
        public Double Value { set; get; }
        public System.Int64 IdDataGps { set; get; }
        public int Zone_id{ set; get; }
        public int Id_main { set; get; }
        public bool IsRead
        {
            get { return true; }
        }
        public bool IsPopup{ set; get; }
        public byte[] IconSmall { set; get; } 
    }
}
