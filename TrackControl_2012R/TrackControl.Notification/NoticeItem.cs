﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using TrackControl.General;
using TrackControl.General.DatabaseDriver;
using TrackControl.MySqlDal;
using TrackControl.Notification.Properties;
using TrackControl.UkrGIS.SearchEngine;
using TrackControl.Vehicles;
using TrackControl.Zones;
using Agro.Dictionaries;
using BaseReports.Procedure;
using DevExpress.Internal;
using TrackControl.General.Log;
using TrackControl.Reports;
using System.Diagnostics;

namespace TrackControl.Notification
{
    public  class NoticeItem:Entity,IDisposable 
    {
        internal List<Vehicle> Vehicles ;
        // трнаспорт для дополнительных функций: проточный датчик:  топливо: ДУТ на машинах с топливозаправщиком,
        // ограничительный список машин для выгрузки зерна (поиск машин , что были возле выгрузки зерна, когда там сработал датчик на оборотах шнека)
        internal List<Vehicle> VehiclesForAddFunction;

        internal List<DictionaryAgroWorkType> AgroWorks;

        internal BindingList<EMail> Emails;

        internal BindingList<PopupUser> PopupUsers;

        internal List<NoticeSubItem> NoticeSIs;

        internal List<Zone> AgroZones;

        private List<NoticeEventAnalyser> _analysers;

        public static IZonesManager ZonesModel;

        public int Period { get; set; }

        //public event Action<List<GpsData>> EventFire;

        public NoticeItem()
        {
            NoticeSIs = new List<NoticeSubItem>();
            Vehicles = new List<Vehicle>();
            AgroWorks = new List<DictionaryAgroWorkType>();
            VehiclesForAddFunction = new List<Vehicle>();
            _analysers = new List<NoticeEventAnalyser>();
            PopupUsers = new BindingList<PopupUser>();
            MessageRfidDriverLog = "";

        }

        public NoticeItem(int idNotice, LoadRegime  loadRegime):base()
        {
            NoticeItem ni = NoticeProvider.GetNotice(idNotice);
            if (ni == null) return;
            Id = ni.Id;
            IsActive = ni.IsActive;
            IsEmailActive = ni.IsEmailActive;
            Title = ni.Title;
            IconSmall = ni.IconSmall;
            IconLarge = ni.IconLarge;
            SoundName = ni.SoundName; 
            IsPopup = ni.IsPopup;
            TimeForControl = ni.TimeForControl;
            Emails = ni.Emails;
            PopupUsers = ni.PopupUsers;
            Period = ni.Period;
            SetSubItems(loadRegime);
        }

        public void CreateNoticeItemForMdOrder(Vehicle vehicle, Zone zone, ZoneEventTypes zoneEventType)
        {
            Vehicles.Add(vehicle);
            NoticeCheckZone nchz = new NoticeCheckZone(this, zone.Id, zoneEventType);
            NoticeSIs.Add(nchz);
            CreateEventAnalysersZoneCross(NoticeSIs, vehicle);

        }

        public void SetSubItems(LoadRegime loadRegime)
        {
            switch (loadRegime)
            {
                case LoadRegime.Events:
                    LoadNoticeSubItems(loadRegime);
                    break;
                case LoadRegime.EventsZonesBriefVeh:
                    LoadNoticeSubItems(loadRegime);
                    LoadEntitiesDependOnTypeNsi();
                    break;
                case LoadRegime.EventsZonesFullVehAnaliz:
                    LoadNoticeSubItems(loadRegime);
                    LoadEntitiesDependOnTypeNsi();
                     if (NoticeSIs != null && Vehicles != null) CreateAnalizItems();
                    break;
                default:
                    break;
            }
        }

        private void LoadEntitiesDependOnTypeNsi()
        {
            if (IsTypeExistInSubItems<NoticeFlowTypeSensor>())
            {
                LoadVehiclesWithSensor();
                LoadVehiclesWithoutSensor();
            }
            else if (IsTypeExistInSubItems<NoticeFuelDischarge>())
            {
                LoadVehiclesWithSensor();
            }
            else if (IsTypeExistInSubItems<NoticeSpeedAgroWork>())
            {
                LoadAgroWorks();
                LoadVehiclesAgroWorks();
                LoadAgroZones();
            }
            else
                LoadVehicles();
        }


        public bool IsActive { get; set; }

        public bool IsPopup { get; set; }

        public bool IsEmailActive { get; set; }

        public string Title { get; set; }

        public string MesPopup { get; set; }

        private string MesEmailShort { get; set; }

        public string SoundName { get; set; }

        public byte[] IconSmall { get; set; }

        public byte[] IconLarge { get; set; }

        /// <summary>
        /// время от текущего назад в часах - период анализа данных
        /// </summary>
        Double _timeForControl;
        public Double TimeForControl
        {
            get { return _timeForControl; }
            set { 
                _timeForControl = value;
                TimeStartControl = DateTime.Now.AddHours(-_timeForControl);
                }
        }

        public DateTime TimeStartControl { get; set; }

        public bool CheckEventOnLine(GpsData gps)
        {
            if (!IsValid()) return false;
            bool dataAdd = false;
            if (_analysers == null) return false;
            foreach (NoticeEventAnalyser analyzer in _analysers)
            {
                if (analyzer.CheckEvent(gps))
                {
                    string mesLog = CreateMessages(gps, analyzer);
                    dataAdd = WriteLog(dataAdd, analyzer, mesLog);
                    SetOnlineActions(analyzer);
                    //analyzer.DataGpsBetweenEvents.Clear();
                }
            }
            return dataAdd;
        }

        public bool CheckEventOnLineAllProtocols(OnlineDataProvider onlineDP)
        {
            if (!IsValid()) 
                return false;

            bool dataAdd = false;
             if (_analysers == null) 
                 return false;

            foreach (NoticeEventAnalyser analyser in _analysers)
            {
                
                IList<GpsData> gpss = new List<GpsData>();
                if (analyser.Nsi.TypeEvent == NoticeSubItemTypes.LossGps)
                {
                    gpss = GpsDataProvider.GetLast100GpsDataAllProtocolsAllPonts(analyser.Vehicle.MobitelId,
                                                                                  analyser.LastLogId, analyser.Vehicle.Mobitel.Is64BitPackets);
                }
                else
                {
                    gpss = onlineDP.GetLastDataMobitelAllProtocols(analyser.Vehicle.MobitelId,
                                                                                  analyser.LastLogId,
                                                                                  analyser.Vehicle.Mobitel.Is64BitPackets, analyser.Vehicle.Mobitel.IsNotDrawDgps);
                }

                foreach (GpsData gps in gpss)
                {
                    if (gps.Time.Subtract(TimeStartControl).TotalSeconds > 0 && analyser.CheckEvent(gps))
                    {
                        string mesLog = CreateMessages(gps, analyser);
                        dataAdd = WriteLog(dataAdd, analyser, mesLog);
                        SetOnlineActions(analyser);
                        //analyzer.DataGpsBetweenEvents.Clear();
                    };
                    analyser.LastLogId = gps.LogId;
                }
                Application.DoEvents();  
            }
            return dataAdd;
        }
        
        public bool CheckEventPeriod(GpsData gps,List<NoticeLogRecord> _periodRecords)
        {
            if (!IsValid()) return false;
            bool dataAdd = false;
            if (_analysers == null) return false;
            foreach (NoticeEventAnalyser analyzer in _analysers)
            {
                if (analyzer.CheckEvent(gps))
                {
                    string mesLog = CreateMessages(gps, analyzer);
                    AddPeriodRecord(analyzer, mesLog, _periodRecords);
                    //SetOnlineActions(analyzer);
                    dataAdd = true;
                };
            }
            return dataAdd;
        }

        private string CreateMessages(GpsData gps, NoticeEventAnalyser analyzer)
        {
            string mesLog = SetLogPopupTitle(gps, analyzer);
            if (analyzer.Nsi_chz != null)
            {
                string direct = SetDirection(analyzer);
                mesLog = SetLogPopupMessageCheckZone(analyzer, mesLog, direct);
            }
            if (analyzer.Nsi != null)
            {
                if (analyzer.Nsi.GpsEvent != null)
                analyzer.Nsi.Location = GeoLocator.Instance.GetLocationInfo(analyzer.Nsi.GpsEvent.LatLng);
                mesLog = SetLogPopupMessageTypeEvent(gps, analyzer, mesLog);
            }

            MesEmailShort = MesPopup;
            return mesLog;
        }

        private void AddPeriodRecord( NoticeEventAnalyser analyzer, string mesLog, List<NoticeLogRecord> _periodRecords)
        {
            if (analyzer == null && _periodRecords==null) return;
            var record = new NoticeLogRecord();
            if (analyzer.Nsi != null && analyzer.Nsi.GpsEvent != null)
            {
                record.DateEvent = analyzer.Nsi.GpsEvent.Time;
                record.GeoPoint = analyzer.Nsi.GpsEvent.LatLng;
                record.IdDataGps = analyzer.Nsi.GpsEvent.Id;
                record.Speed = analyzer.Nsi.GpsEvent.Speed;
            }
            if (analyzer.Vehicle != null) 
                record.VehicleEvent = analyzer.Vehicle;
            record.Infor = mesLog;
            if (analyzer.Nsi != null && analyzer.Nsi.GpsEvent != null)
            {
                record.Location = analyzer.Nsi.Location;
                record.Value = analyzer.Nsi.Value;
            }
            
            if (analyzer.Nsi_chz != null)
            {
                record.Zone_id = analyzer.Nsi_chz.Zone_Id;
                record.TypeEvent = (int)NoticeSubItemTypes.CheckZoneControl;
            }
            else
            {
                record.Zone_id = ConstsGen.RECORD_MISSING;
                if (analyzer.Nsi != null) record.TypeEvent = (int)analyzer.Nsi.TypeEvent;
            }
            if (analyzer.Nsi != null && analyzer.Nsi.Ni != null)
            {
                record.Id_main = analyzer.Nsi.Ni.Id;
                record.IconSmall = analyzer.Nsi.Ni.IconSmall;
                record.IsPopup = analyzer.Nsi.Ni.IsPopup;
            }
            if (record.TypeEvent == (int) NoticeSubItemTypes.LossGps)
            {
                _periodRecords.RemoveAll(x => x.IdDataGps == analyzer.Nsi.GpsEvent.Id);
            }
            if (!_periodRecords.Contains(record)) _periodRecords.Add(record);
         }

        private void SetOnlineActions(NoticeEventAnalyser analyzer)
        {
            if (UserBaseCurrent.Instance.IsVehicleVisibleForRole(analyzer.Vehicle.Id) )
            {
                if (IsEmailActive &&  Emails.Count > 0)
                {
                    try
                    {
                        SendEmail(analyzer);
                    }
                    catch (Exception ex)
                    {
                        string message = string.Format(Resources.EmailException, Emails[0].Addres);
                        ExecuteLogging.Log("NoticeItem.SetOnlineActions", ", SendMailMessage. " + message);
                        //XtraMessageBox.Show(ex.Message + "\n" + message, "SendMailMessage", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        //IsEmailActive = false;
                    }
                }
                
                if (IsPopup && IsUserCurrentAllowPopup())
                {
                    if (analyzer.GpsBeforEvent != null)
                    {
                        MessageNoticeFuel mNoticeFuel = null;
                        mNoticeFuel = new MessageNoticeFuel(analyzer);
                        mNoticeFuel.Show();
                        Application.DoEvents();

                    }
                    else
                    {
                        MessageNotice mNotice = null;
                        mNotice = new MessageNotice(analyzer);
                        mNotice.Show();
                        Application.DoEvents();
                    }
                }
            }
        }

        private bool IsUserCurrentAllowPopup()
        {
            if (UserBaseCurrent.Instance.Admin || PopupUsers.Count == 0) return true;
            foreach (PopupUser pUser in PopupUsers)
            {
                if (pUser.User.Id == UserBaseCurrent.Instance.Id) return true;
            }
            return false;
        }

        private bool WriteLog(bool dataAdd, NoticeEventAnalyser analyzer, string mesLog)
        {
            if (analyzer.Nsi.TypeEvent == NoticeSubItemTypes.LossGps)
            {
                NoticeProvider.WriteLog(analyzer.Nsi, analyzer.Vehicle.Mobitel.Id, mesLog, analyzer.Nsi.GpsEvent.Time);
                return true;
            }
            if (!IsEventInLog(analyzer.Nsi == null ? analyzer.Nsi_chz.GpsEvent.Id : analyzer.Nsi.GpsEvent.Id))
            {
                NoticeProvider.WriteLog(analyzer.Nsi_chz ?? analyzer.Nsi, analyzer.Vehicle.Mobitel.Id, mesLog, analyzer.Nsi.GpsEvent.Time);
                dataAdd = true;
            }
            return dataAdd;
        }

        private string SetLogPopupTitle(GpsData gps, NoticeEventAnalyser analyzer)
        {
            SetPopupTitle(gps.Time, analyzer);
            string mesLog = string.Format("{0}", this.Title);
            return mesLog;
        }

        private void SetPopupTitle(DateTime time, NoticeEventAnalyser analyzer)
        {
            MesPopup = string.Format("{3}: {0}{2}{4}: {1}{2}", analyzer.Vehicle.RegNumber, time, Environment.NewLine, Resources.Vehicle, Resources.Time);
        }

        private string SetLogPopupMessageCheckZone(NoticeEventAnalyser analyzer, string mesLog, string direct)
        {
            MesPopup += string.Format("{0} {3}: {1}{2}", direct, analyzer.Nsi_chz.Zone_name, Environment.NewLine, Resources.CHZ);
            mesLog += string.Format(" (-> {0})", direct);
            return mesLog;
        }

        private string SetDirection(NoticeEventAnalyser analyzer)
        {
            string direct = "";
            switch (analyzer.Nsi_chz.ZoneEventType)
            {
                case ZoneEventTypes.EntryToZone:
                    direct = Resources.Entry;
                    break;
                case ZoneEventTypes.ExitFromZone:
                    direct = Resources.Exit;
                    break;
                case ZoneEventTypes.LocateInZone:
                    direct = Resources.ZoneIn;
                    break;
                case ZoneEventTypes.LocateOutZone:
                    direct = Resources.ZonesOut;
                    analyzer.Nsi_chz.Location = GeoLocator.Instance.GetLocationInfo(analyzer.Nsi.GpsEvent.LatLng);
                    break;
                default:
                    break;
            }
            return direct;
        }

        private string SetLogPopupMessageTypeEvent(GpsData gps, NoticeEventAnalyser analyzer, string mesLog)
        {
            MessageRfidDriverLog = "";

                switch (analyzer.Nsi.TypeEvent)
                {
                    case NoticeSubItemTypes.SpeedControl:
                        MesPopup += string.Format("{2} {0} {3} {1}", gps.Speed, Environment.NewLine, Resources.Speed, Resources.kmh);
                        mesLog += string.Format(" (-> {0} {1})", gps.Speed, Resources.kmh);
                        break;
                    case NoticeSubItemTypes.SensorControl:
                        MesPopup += string.Format("{6} {0}{2}{5} {7} {3} {8} {4}{2}{9} {1}{2}", ((NoticeSensor)analyzer.Nsi).Sensor.Name, ((NoticeSensor)analyzer.Nsi).Sensor.Value,
                            Environment.NewLine, ((NoticeSensor)analyzer.Nsi).ValueBottom, ((NoticeSensor)analyzer.Nsi).ValueTop,
                            ((NoticeSensor)analyzer.Nsi).InRange == true ? Resources.OperateInDiapazon : Resources.OperateOutDiapazon,
                            Resources.Sensor, Resources.From, Resources.To, Resources.Value);

                        if (((NoticeSensor)analyzer.Nsi).Sensor.Algoritm == (int)AlgorithmType.DRIVER)
                        {
                            mesLog += string.Format(" (-> {0})", ((NoticeSensor)analyzer.Nsi).Sensor.Value + "; " + Resources.Driver + ": " + analyzer.Vehicle.Driver);
                            string drv = "" + analyzer.Vehicle.Driver;

                            if (drv == " ")
                                MesPopup += Resources.Driver + ": " + Resources.NotImplement + " \r\n";
                            else
                                MesPopup += Resources.Driver + ": " + analyzer.Vehicle.Driver + "\r\n";
                        }
                        else
                        {
                            MesPopup += Resources.Driver + ": " + Resources.NotImplement + " \r\n";
                            mesLog += string.Format(" (-> {0})", ((NoticeSensor)analyzer.Nsi).Sensor.Value);
                        }
                        analyzer.Nsi.Value = ((NoticeSensor)analyzer.Nsi).Sensor.Value;

                        string location = Algorithm.FindAddressLocation(gps.LatLng);
                        MesPopup += Resources.Location + ": " + location + "\r\n";
                        analyzer.Nsi.Ni.MessageRfidDriverLog = mesLog;
                        break;
                    case NoticeSubItemTypes.SensorLogicControl:
                        MesPopup += string.Format("{2} {0} {1}", ((NoticeLogicSensor)analyzer.Nsi).Sensor.Name, Environment.NewLine, Resources.Sensor);
                        mesLog += string.Format(" (-> {0})", ((NoticeLogicSensor)analyzer.Nsi).Sensor.Name);
                        break;
                    case NoticeSubItemTypes.StopControl:
                        string driverVehicle = analyzer.Vehicle.Driver == null 
                                                ? "" 
                                                : analyzer.Vehicle.Driver.FullName;
                        string driver = ((NoticeStop)analyzer.Nsi).EventDriver == null 
                                                ? driverVehicle 
                                                : ((NoticeStop)analyzer.Nsi).EventDriver.FullName;
                        string driverVehiclePhone = analyzer.Vehicle.Driver == null 
                                                ? "" 
                                                : analyzer.Vehicle.Driver.NumTelephone;
                        string driverPhone = ((NoticeStop) analyzer.Nsi).EventDriver == null
                                                 ? driverVehiclePhone
                                                 : ((NoticeStop) analyzer.Nsi).EventDriver.NumTelephone;
                        MesPopup += string.Format("{2}  {0} {3} {1}{4} {5} {1}", ((NoticeStop)analyzer.Nsi).StopTime, Environment.NewLine, Resources.Parking, Resources.MinBrief, driver, driverPhone);
                        mesLog += string.Format(" (> {0} {1})", ((NoticeStop)analyzer.Nsi).StopTime, Resources.MinBrief);
                        break;
                    case NoticeSubItemTypes.LossGps:
                        if (Math.Abs(((NoticeLossGPS)analyzer.Nsi).Loss_time - ((NoticeLossGPS)analyzer.Nsi).Loss_time_total) < 0.01)
                        {
                            MesPopup += string.Format("{3} > {0} {2} {1}", ((NoticeLossGPS)analyzer.Nsi).Loss_time, Environment.NewLine, Resources.MinBrief, Resources.LossGPS);
                        }
                        else
                        {
                            IsEmailActive = false;
                            IsPopup = false;
                        }
                        mesLog += string.Format(" (> {0} ({1}) {2})", ((NoticeLossGPS)analyzer.Nsi).Loss_time, ((NoticeLossGPS)analyzer.Nsi).Loss_time_total, Resources.MinBrief);
                        break;
                    case NoticeSubItemTypes.LossPower:
                        MesPopup += string.Format("{3} > {0} {2} {1}", ((NoticeLossPower)analyzer.Nsi).Loss_time, Environment.NewLine, Resources.MinBrief, Resources.BackupWorking);
                        mesLog += string.Format(" (> {0} {1})", ((NoticeLossPower)analyzer.Nsi).Loss_time, Resources.MinBrief);
                        break;
                    case NoticeSubItemTypes.TimeControl:
                        MesPopup += string.Format("{3} > {0} {2} {1}", ((NoticeTimeControl)analyzer.Nsi).TimeForContrrol, Environment.NewLine, Resources.MinBrief, Resources.TimeControl);
                        mesLog += string.Format(" (> {0} {1})", ((NoticeTimeControl)analyzer.Nsi).TimeForContrrol, Resources.MinBrief);
                        break;
                    case NoticeSubItemTypes.SensorFlowTypeControl:
                        MesPopup += string.Format("начало: {0}{1}длительность(мин): {2}{1}получатель: {3}{1} "
                            , ((NoticeFlowTypeSensor)analyzer.Nsi).StartDateFormated, Environment.NewLine
                            , Math.Round(((NoticeFlowTypeSensor)analyzer.Nsi).EndDate.Subtract(((NoticeFlowTypeSensor)analyzer.Nsi).StartDate).TotalMinutes, 2)
                            , ((NoticeFlowTypeSensor)analyzer.Nsi).CrossListVehiclesInfo);
                        mesLog += string.Format(" (начало: {0} длит.(мин):  {1}  -> {2})"
                            , ((NoticeFlowTypeSensor)analyzer.Nsi).StartDateFormated
                            , Math.Round(((NoticeFlowTypeSensor)analyzer.Nsi).EndDate.Subtract(((NoticeFlowTypeSensor)analyzer.Nsi).StartDate).TotalMinutes, 2)
                            , ((NoticeFlowTypeSensor)analyzer.Nsi).CrossListVehiclesRegNumber);
                        break;
                    case NoticeSubItemTypes.FuelDischarge:
                        if (((NoticeFuelDischarge)analyzer.Nsi).ErrorSensorMessage.Length > 0)
                        {
                            string addErrMess = string.Format(" ({0})", ((NoticeFuelDischarge)analyzer.Nsi).ErrorSensorMessage);
                            MesPopup += addErrMess;
                            mesLog += addErrMess;
                        }
                        else
                        {
                            double sensorValue = 0;
                            if (analyzer.Nsi.GpsEvent != null) sensorValue = analyzer.Nsi.GpsEvent.SensorValue;
                            MesPopup += string.Format("{1} > {0} {2} ({3}->{4})", ((NoticeFuelDischarge)analyzer.Nsi).Threshold, Resources.FuelDischarge, Resources.LiterShort, ((NoticeFuelDischarge)analyzer.Nsi).PrevFuelValue, sensorValue);
                            mesLog += string.Format(" (> {0} {1} ({2}->{3}))", ((NoticeFuelDischarge)analyzer.Nsi).Threshold, Resources.LiterShort, ((NoticeFuelDischarge)analyzer.Nsi).PrevFuelValue, sensorValue);
                        }
                        break;
                    case NoticeSubItemTypes.VehicleUnload:
                        string zoneName = ((NoticeVehicleUnload)analyzer.Nsi).ZoneUnload == null ? "" : ((NoticeVehicleUnload)analyzer.Nsi).ZoneUnload.Name;
                        string vehicleUnloadMessasge = string.Format("{0} RFID {1} ({2})", ((NoticeVehicleUnload)analyzer.Nsi).NoticeMessage, analyzer.Nsi.Value, zoneName);
                        MesPopup += vehicleUnloadMessasge;
                        mesLog += vehicleUnloadMessasge;
                        break;
                    case NoticeSubItemTypes.SpeedAgroWorksControl:
                        string speedAgroWorksControlMessasge = string.Format("{9}: {0}{7}{11}: {1} (tel:{2}){7}{10}: {3}({8}:min {4} max {5}) -> {6}"
                            , ((NoticeSpeedAgroWork)analyzer.Nsi).EventAgregat.Name
                            , ((NoticeSpeedAgroWork)analyzer.Nsi).EventDriver == null ? analyzer.Vehicle.Driver.FullName : ((NoticeSpeedAgroWork)analyzer.Nsi).EventDriver.FullName
                            , ((NoticeSpeedAgroWork)analyzer.Nsi).EventDriver == null ? analyzer.Vehicle.Driver.NumTelephone : ((NoticeSpeedAgroWork)analyzer.Nsi).EventDriver.NumTelephone
                            , ((NoticeSpeedAgroWork)analyzer.Nsi).EventAgroWork.Name
                            , ((NoticeSpeedAgroWork)analyzer.Nsi).EventAgroWork.SpeedBottom
                            , ((NoticeSpeedAgroWork)analyzer.Nsi).EventAgroWork.SpeedTop
                            , analyzer.Nsi.GpsEvent.Speed
                            , Environment.NewLine
                            , Resources.Speed
                            , Resources.Agregat
                            , Resources.WorkType
                            , Resources.Driver);
                        MesPopup += speedAgroWorksControlMessasge;
                        string speedAgroWorksControlMessasgeLog = string.Format(" {0} {1} (min {2} max {3}) -> {4}"
                            , ((NoticeSpeedAgroWork)analyzer.Nsi).EventAgregat.Name
                            , ((NoticeSpeedAgroWork)analyzer.Nsi).EventAgroWork.Name
                            , ((NoticeSpeedAgroWork)analyzer.Nsi).EventAgroWork.SpeedBottom
                            , ((NoticeSpeedAgroWork)analyzer.Nsi).EventAgroWork.SpeedTop
                            , analyzer.Nsi.GpsEvent.Speed);
                        mesLog += speedAgroWorksControlMessasgeLog;
                        break;
                    default:
                        break;
                }
            return mesLog;
        }

        public string MessageRfidDriverLog { get; set; }

        public void  AddVehicle(int idMobitel)
        {
            Vehicle veh;

            try
            {
                veh = new Vehicle(idMobitel) {Mobitel = new Mobitel(idMobitel, "", "")};
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, Resources.Notice, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (veh.Id != ConstsGen.RECORD_MISSING)
                    Vehicles.Add(veh);
        }

        public void AddAgroWork(int idAgroWork)
        {
            DictionaryAgroWorkType agroWork;

            try
            {
                agroWork = new DictionaryAgroWorkType(idAgroWork);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, Resources.Notice, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (agroWork.Id  != ConstsGen.RECORD_MISSING)
                AgroWorks.Add(agroWork);
        }

        public void AddPopupUser(int idUserBase)
        {
            var pUser = new PopupUser(0, idUserBase);
            PopupUsers.Add(pUser); 
        }

        public void AddVehicleForAddFunction(int idMobitel)
        {
            Vehicle veh;

            try
            {
                veh = new Vehicle(idMobitel);
                veh.Mobitel = new Mobitel(idMobitel, "", "");

            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (veh.Id != ConstsGen.RECORD_MISSING)
                VehiclesForAddFunction.Add(veh);
        }

        public void AddVehicle(int idMobitel, int idLogicSensor, int idSensor)
        {
            Vehicle veh = new Vehicle(idMobitel);
            //veh.Mobitel = new Mobitel(idMobitel, "", "");
            if (veh.Id != ConstsGen.RECORD_MISSING)
            {
                if (idLogicSensor>0) veh.LogicSensor = new LogicSensor(idLogicSensor);
                if (idSensor > 0) veh.Sensor = new Sensor(idSensor);
                 Vehicles.Add(veh);
            }
        }

        public bool IsVehicleContain(int idMobitel)
        {
            foreach (Vehicle vh in Vehicles)
            {
                if (vh.MobitelId == idMobitel) return true;
            }
            return false;
        }

        public void ClearVehicles()
        {
            if (Vehicles != null) Vehicles.Clear(); 
        }

        public void ClearAgroWorks()
        {
            if (AgroWorks == null) 
                AgroWorks = new List<DictionaryAgroWorkType>();
            else
                AgroWorks.Clear();
        }

        public void ClearVehiclesForAddFunction()
        {
            if (VehiclesForAddFunction != null) VehiclesForAddFunction.Clear();
        }

        public void ClearSubItems<T>()
        {
            List<NoticeSubItem> nsi_del = new  List<NoticeSubItem>();
            if (NoticeSIs == null) return;
            foreach (NoticeSubItem nsi in NoticeSIs)
            {
                if (nsi is T)
                {
                    nsi_del.Add(nsi); 
                }
            }
            if (nsi_del.Count > 0)
            {
            foreach (NoticeSubItem nsi in nsi_del)
            {
                NoticeSIs.Remove(nsi);
            }
            nsi_del.Clear();
            }
        }

        public bool IsTypeExistInSubItems<T>()
        {
            if (NoticeSIs == null) return false;
            foreach (NoticeSubItem nsi in NoticeSIs)
            {
                if (nsi is T)
                {
                    return true;
                }
            }
            return false;
        }

        public void AddSubItem(NoticeSubItem nsi)
        {
            NoticeSIs.Add(nsi); 
        }

        public bool Save()
        {
            try
            {
                if (IsNew)
                {
                    Id = NoticeProvider.InsertRecord(this);
                    if (Id == ConstsGen.RECORD_MISSING)
                        return false;
                    else
                        return true;
                }
                else
                {
                    if (NoticeProvider.UpdateRecord(this))
                        return true;
                    else
                        return false;
                }
            }
            catch 
            {
                return false;
            }
        }

        private void  LoadNoticeSubItems(LoadRegime loadRegime)
        {
            if (NoticeSIs != null) NoticeSIs.Clear();
            NoticeSIs = NoticeProvider.GetNoticeSubItems(this, loadRegime);
        }

        private void LoadVehicles()
        {
            if (Vehicles != null) Vehicles.Clear();
            Vehicles = NoticeVehicleProvider.GetVehicles(this);
        }

        private void LoadVehiclesWithSensor()
        {
            if (Vehicles != null) Vehicles.Clear();
            Vehicles = NoticeVehicleProvider.GetVehiclesWithSensor(this);
        }

        private void LoadVehiclesAgroWorks()
        {
            if (Vehicles != null) Vehicles.Clear();
            Vehicles = NoticeVehicleProvider.GetVehiclesFromAgroWorks(this);
        }

        private void LoadAgroWorks()
        {
            if (AgroWorks != null) AgroWorks.Clear();
            AgroWorks = NoticeProvider.GetAgroWorks(this);
        }

        private void LoadAgroZones()
        {
            if (AgroZones != null) AgroZones.Clear();
            AgroZones = NoticeProvider.GetAgroZones(this, ZonesModel);
        }

        private void LoadVehiclesWithoutSensor()
        {
            if (VehiclesForAddFunction != null) VehiclesForAddFunction.Clear();
            VehiclesForAddFunction = NoticeVehicleProvider.GetVehiclesWithoutSensor(this);
        }

        public NoticeSubItem GetSubItem<T>()
        {
            foreach (NoticeSubItem nsi in NoticeSIs)
            {
                if (nsi is T)
                {
                    return nsi;
                }
            }
            return null;
        }

        public List<NoticeSubItem> GetSubItemsType<T>()
        {
            var nsis = new List<NoticeSubItem>(); 
            foreach (NoticeSubItem nsi in NoticeSIs)
            {
                if (nsi is T)
                {
                    nsis.Add(nsi);
                }
            }
            return nsis;
        }

        public List<NoticeSubItem> GetSubItemsExcludeType<T>()
        {
            var nsis = new List<NoticeSubItem>();
            foreach (NoticeSubItem nsi in NoticeSIs)
            {
                if (!(nsi is T))
                {
                    nsis.Add(nsi);
                }
            }
            return nsis;
        }

        public int GetNoticeSubItemsCount<T>()
        {
            int cnt = 0;
            if (NoticeSIs == null) return 0;
            foreach (NoticeSubItem nsi in NoticeSIs)
            {
                if (nsi is T)
                {
                    cnt++;
                }
            }
            return cnt;
        }

        /// <summary>
        /// обобщенные записи событий разбиваются на многочисленные анализаторы подсобытий, состоящие из одного транспорта и события + КЗ (для составных событий) 
        /// </summary>
        private void CreateAnalizItems()
        {
            // подсобытия , связанные с КЗ
            var nisChz = GetSubItemsType<NoticeCheckZone>();
            // подсобытия , не связанные с КЗ
            var nisExcludeChz = GetSubItemsExcludeType<NoticeCheckZone>();
            foreach (Vehicle vh in Vehicles)
            {
                if (nisChz.Count > 0)
                {switch (((NoticeCheckZone)nisChz[0]).ZoneEventType)
                    {
                        case ZoneEventTypes.EntryToZone:
                            // если подсобытие КЗ  связано с пересечением - доп критерий не нужен
                            CreateEventAnalysersZoneCross(nisChz, vh);
                            break;
                        case ZoneEventTypes.ExitFromZone:
                            // если подсобытие КЗ  связано с пересечением - доп критерий не нужен
                            CreateEventAnalysersZoneCross(nisChz, vh);
                            break;
                        case ZoneEventTypes.LocateInZone:
                            // если подсобытие КЗ связано с нахождением внутри КЗ добавляем контролируемое событие 
                            CreateEventAnalysersZoneLocateIn(nisChz, nisExcludeChz, vh);
                            break;
                        case ZoneEventTypes.LocateOutZone:
                            // если событие  связано с нахождением вне КЗ - все зоны контролируются как одна +  добавляем контролируемое событие  
                            CreateEventAnalysersZoneLocateOut(nisChz, nisExcludeChz, vh);
                            break;
                        default:
                            break;
                    }
               }
                else if (nisExcludeChz.Count > 0)
                {
                    CreateEventAnalysersNotice(nisExcludeChz, vh);
                }
            }
        }

        private void CreateEventAnalysersZoneLocateOut(List<NoticeSubItem> nisChz, List<NoticeSubItem> nisExcludeChz, Vehicle vh)
        {
            List<Zone> zonesForOutZone = new List<Zone>();
            if (nisChz != null)
                foreach (NoticeCheckZone niChz in nisChz)
                {
                    zonesForOutZone.Add(niChz.Zone);
                }
            NoticeCheckZoneList niChzList = new NoticeCheckZoneList(this, ConstsGen.RECORD_MISSING, ZoneEventTypes.LocateOutZone, zonesForOutZone);
            foreach (NoticeSubItem niExcludeChz in nisExcludeChz)
            {
                NoticeEventAnalyser analyser = new NoticeEventAnalyser(vh, niChzList, niExcludeChz.Clone());
                _analysers.Add(analyser);
            }
        }

        private void CreateEventAnalysersNotice(List<NoticeSubItem> nisExcludeChz, Vehicle vh)
        {
            foreach (NoticeSubItem niExcludeChz in nisExcludeChz)
            {
                var analyser = new NoticeEventAnalyser(vh, null, niExcludeChz.Clone());
                _analysers.Add(analyser);
            }
        }

        private void CreateEventAnalysersZoneLocateIn(List<NoticeSubItem> nisChz, List<NoticeSubItem> nisExcludeChz, Vehicle vh)
        {
            if (nisChz != null)
                foreach (NoticeCheckZone niChz in nisChz)
                {
                    if (niChz.ZoneEventType == ZoneEventTypes.LocateInZone)
                    {
                        foreach (NoticeSubItem niExcludeChz in nisExcludeChz)
                        {
                            NoticeEventAnalyser analyser = new NoticeEventAnalyser(vh, (NoticeCheckZone)niChz.Clone(), niExcludeChz.Clone());
                            _analysers.Add(analyser);
                        }
                    }
                }
        }

        private void CreateEventAnalysersZoneCross(List<NoticeSubItem> nisChz, Vehicle vh)
        {
            foreach (NoticeCheckZone niChz in nisChz)
            {
                NoticeEventAnalyser analyser = new NoticeEventAnalyser(vh, (NoticeCheckZone)niChz.Clone(), (NoticeSubItem)niChz.Clone());
                _analysers.Add(analyser);
            }
        }

        public bool CheckStateOnLine()
        {
            bool dataAdd = false;
            if (_analysers == null) return false;
            foreach (NoticeEventAnalyser analyzer in _analysers)
            {
                if ( analyzer.CheckState())
                {
                    SetPopupTitle(DateTime.Now, analyzer);    
                    switch (analyzer.Nsi.TypeEvent)
                        {
                            case NoticeSubItemTypes.LossData:
                                //если нет информации о последних данных в таблице online - берем последнюю информацию из DataGps
                                if (analyzer.Nsi.GpsEvent.Id == 0)
                                {
                                    GpsData dataGps = GpsDataProvider.GetLastActiveDataGps(analyzer.Vehicle.Mobitel.Id);
                                    if (dataGps != null && dataGps.Time.Subtract(analyzer.Nsi.Ni.TimeStartControl).TotalSeconds > 0) analyzer.Nsi.GpsEvent = dataGps;
                                }
                                if (!IsStateInLog(analyzer.Vehicle.Mobitel.Id, analyzer.Nsi.GpsEvent, (int)((NoticeLossData)analyzer.Nsi).Loss_time))
                                {
                                    string mesLog = SetLogPopupMessageState(analyzer);
                                    NoticeProvider.WriteLog(analyzer.Nsi, analyzer.Vehicle.Mobitel.Id, mesLog, analyzer.Nsi.GpsEvent.Time);
                                    SetOnlineActions(analyzer);
                                    dataAdd = true;
                                }
                                break;
                            default:
                                break;
                        }
                    }
                };
            return dataAdd;
        }

        private string SetLogPopupMessageState(NoticeEventAnalyser analyzer)
        {
            MesPopup += string.Format("{3} > {0} {2} {1}", ((NoticeLossData)analyzer.Nsi).Loss_time, Environment.NewLine, Resources.MinBrief, Resources.LossData);
            string mesLog = string.Format("{0} (> {1} {2})", this.Title, ((NoticeLossData)analyzer.Nsi).Loss_time, Resources.MinBrief);
            if (analyzer.Nsi.GpsEvent.Id != 0) analyzer.Nsi.Location = GeoLocator.Instance.GetLocationInfo(analyzer.Nsi.GpsEvent.LatLng);
            return mesLog;
        }

        public bool CheckStatePeriod(List<NoticeLogRecord> _periodRecords)
        {
            bool dataAdd = false;
            if (_analysers == null) return false;
            foreach (NoticeEventAnalyser analyzer in _analysers)
            {
                if (analyzer.CheckState())
                {
                    SetPopupTitle(DateTime.Now, analyzer);
                    switch (analyzer.Nsi.TypeEvent)
                    {
                        case NoticeSubItemTypes.LossData:
                            string mesLog = SetLogPopupMessageState(analyzer);
                            AddPeriodRecord(analyzer, mesLog, _periodRecords);
                            dataAdd = true;
                            break;
                    }
                }
            };
            return dataAdd;
        }

        private bool IsEventInLog(System.Int64 gpsId)
        {
            return NoticeProvider.IsEventInLog(gpsId,Id);  
        }

        private bool IsStateInLog(int mobitelId, GpsData gps, int periodMinute)
        {
            if (gps.Id  > 0)
                return NoticeProvider.IsEventInLog(gps.Id, Id); 
            else
                return NoticeProvider.IsStateInLog(Id, mobitelId, gps.Time , periodMinute);
        }

        public bool IsValid()
        {
            if (NoticeSIs != null && NoticeSIs.Count > 0 && Vehicles != null && Vehicles.Count > 0)
                return true;
            else
                return false;
        }

        void SendEmail(NoticeEventAnalyser analyser)
        {
            EmailSender esender = new EmailSender();
            string vehicleInfo = analyser.Vehicle.Info;
            string log_string = "";
            log_string += "vehicleInfo:" + vehicleInfo + "\n";
            log_string += "esender.IsReady:" + esender.IsReady + "\n";

            if (esender.IsReady)
            {
                esender.MailBody = GetShortFormatedBody();
                log_string += "esender.MailBody:" + esender.MailBody + "\n";
                esender.MailSubject = string.Format("{0} {1}", vehicleInfo, this.Title);
                log_string += "esender.MailSubject:" + esender.MailSubject + "\n";
                string emailsTo = "";

                foreach (EMail email in Emails)
                {
                    if (email.IsActive) 
                        emailsTo = string.Format("{0}{1},", emailsTo, email.Addres);
                    log_string += "emailsTo:" + emailsTo + "\n";
                }

                if (emailsTo.Length > 3)
                {
                    esender.MailToAdress = emailsTo.Substring(0, emailsTo.Length - 1);
                    log_string += "esender.MailToAdress:" + esender.MailToAdress + "\n";
                    esender.SendMailMessage();
                }
            }

            string lastLogId = analyser.LastLogId.ToString();
            log_string += "lastLogId:" + lastLogId + "\n";
            string state = analyser.Vehicle.LogicSensorState.ToString();
            log_string += "state:" + state + "\n";
            string unixTime = analyser.Nsi.GpsEvent.UnixTime.ToString();
            log_string += "unixTime:" + unixTime + "\n";
            string zona_name = analyser.Nsi_chz.Zone_name;
            log_string += "zona_name:" + zona_name + "\n";

            StackTrace st = new StackTrace(true);
            for (int i = 0; i < st.FrameCount; i++)
            {
                StackFrame sf = st.GetFrame(i);
                log_string += string.Format("High up the call stack, Method: {0}", sf.GetMethod()) + "\n";
                log_string += string.Format("File: {0}",sf.GetFileName()) + "\n";
                log_string += string.Format("High up the call stack, Line Number: {0}", sf.GetFileLineNumber()) + "\n";
            }
            
            // запись в файл
            using (FileStream fstream = new FileStream("log_sender_email.txt", FileMode.OpenOrCreate))
            {
                // преобразуем строку в байты
                byte[] array = System.Text.Encoding.Default.GetBytes(log_string);
                // запись массива байтов в файл
                fstream.Write(array, 0, array.Length);
            }
        } // SendEMail

        string GetFormatedBody()
        {
            string[] separ = { Environment.NewLine };
            string[] parts = MesPopup.Split(separ, 20, StringSplitOptions.RemoveEmptyEntries);
            string formatedBody = string.Format("<b><font color = blue>{0}</font color = blue></b><br>", this.Title);
            if (parts.Length > 0)
            {
                formatedBody += string.Format("<b>{0}</b><br>", parts[0]);
                for (int i = 1; i < parts.Length; i++)
                {
                    formatedBody += string.Format("{0}<br>", parts[i]);
                }
            }
            return formatedBody;
        }

        string GetShortFormatedBody()
        {
            string[] separ = { Environment.NewLine };
            string[] parts = MesEmailShort.Split(separ, 20, StringSplitOptions.RemoveEmptyEntries);
            string formatedBody = string.Format("<b><font color = blue>{0}</font color = blue></b><br>", this.Title);
            if (parts.Length > 0)
            {
                formatedBody += string.Format("<b>{0}</b><br>", parts[0]);
                for (int i = 1; i < parts.Length; i++)
                {
                    formatedBody += string.Format("{0}<br>", parts[i]);
                }
            }
            return formatedBody;
        }

        public void DeleteSubItems()
        {
            NoticeSIs.Clear(); 
        }

        #region IDisposable Members

        public void Dispose()
        {
            if (Vehicles != null) Vehicles.Clear();
            if (NoticeSIs != null) NoticeSIs.Clear();
            if (_analysers!=null) _analysers.Clear(); 
        }
        #endregion

        #region Testing

        double lat_in = 29882160;
        double lon_in = 18453960;

        double lat_out = 29881620;
        double lon_out = 18448320;

        int tt = 108;
        private List<MessageNotice> nListNotice = new List<MessageNotice>();

        private void TestClearOnline()
        {
            //using (ConnectMySQL cn = new ConnectMySQL())
            DriverDb db = new DriverDb();
            db.ConnectDb();
            {
                string sql = "Delete FROM Online";
                //cn.ExecuteNonQueryCommand(sql);
                db.ExecuteNonQueryCommand( sql );
            }
            db.CloseDbConnection();
        }
        
        private void TestClearNtfLog()
        {
            //using (ConnectMySQL cn = new ConnectMySQL())
            DriverDb db = new DriverDb();
            db.ConnectDb();
            {
                string sql = "Delete FROM ntf_log";
                //cn.ExecuteNonQueryCommand(sql);
                db.ExecuteNonQueryCommand( sql );
            }
            db.CloseDbConnection();
        }

        private int TestConvertToTimestamp(DateTime value)
        {
            //TimeSpan span = (value - new DateTime(1970, 1, 1, 0, 0, 0, 0).ToLocalTime());
            //return (int)span.TotalSeconds;

            TimeSpan dt = new DateTime( DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Millisecond ) - DateTime.UtcNow;
            DateTime origin = new DateTime( 1970, 1, 1, 0, 0, 0, 0 ) + dt;
            TimeSpan diff = value - origin;

            return Convert.ToInt32( diff.TotalSeconds );
        }

        private void TestTTEventsArray()
        {
            //using (ConnectMySQL cn = new ConnectMySQL())
            DriverDb db = new DriverDb();
            db.ConnectDb();
            {
                TestClearOnline();
                TestTTEvent(TtEvents.TtEventsDescr.RESERVE_POWER_ON);
                Thread.Sleep(2000);
                TestTTEvent(0);
                Thread.Sleep(2000);
                TestTTEvent(TtEvents.TtEventsDescr.MAIN_POWER_ON);
                Thread.Sleep(2000);
                TestTTEvent(TtEvents.TtEventsDescr.RESERVE_POWER_ON);
                Thread.Sleep(2000);
                TestTTEvent(0);
                Thread.Sleep(2000);
                TestTTEvent(0);
                MessageBox.Show("Ready");
                //TtEvents.TtEventsDescr.RESERVE_POWER_ON
            }
            db.CloseDbConnection();
        }
        
        private void TestTTEvent(uint gpsEvent)
        {
            //using (ConnectMySQL cn = new ConnectMySQL())
            DriverDb db = new DriverDb();
            db.ConnectDb();
            {
                string sql = string.Format(@"Insert into Online (Mobitel_ID,Latitude,Longitude,Events,UnixTime,Valid) 
                Values ({0},{1},{2},{3},{4},{5})", tt, lat_in, lon_in, gpsEvent, TestConvertToTimestamp(DateTime.Now), 1);
                //cn.ExecuteNonQueryCommand(sql);
                db.ExecuteNonQueryCommand( sql );
            }
            db.CloseDbConnection();
        }

        private void TestCrossZonesArray()
        {

            TestZoneOut();
            TestZoneIn();
            TestZoneOut();
            TestZoneOut();
            TestZoneIn();
            TestZoneIn();
            TestZoneOut();
            TestZoneIn();
        }
        private void TestZoneOut()
        {
            //using (ConnectMySQL cn = new ConnectMySQL())
            DriverDb db = new DriverDb();
            db.ConnectDb();
            {
                string sql = string.Format(@"Insert into Online (Mobitel_ID,Latitude,Longitude,Events,UnixTime,Valid,Speed) 
                Values ({0},{1},{2},{3},{4},{5},{6})", tt, 29881620, 18448320, 0, TestConvertToTimestamp(DateTime.Now), 1, 20);
                //cn.ExecuteNonQueryCommand(sql);
                db.ExecuteNonQueryCommand( sql );
            }
            db.CloseDbConnection();
        }
        private void TestZoneIn()
        {
            //using (ConnectMySQL cn = new ConnectMySQL())
            DriverDb db = new DriverDb();
            db.ConnectDb();
            {
                string sql = string.Format(@"Insert into Online (Mobitel_ID,Latitude,Longitude,Events,UnixTime,Valid,Speed) 
                Values ({0},{1},{2},{3},{4},{5},{6})", tt, 29882160, 18453960, 0, TestConvertToTimestamp(DateTime.Now), 1, 25);
                //cn.ExecuteNonQueryCommand(sql);
                db.ExecuteNonQueryCommand( sql );
            }
            db.CloseDbConnection();
        }

        private void TestSpeedArray()
        {
            TestSpeedOne(1);
            TestSpeedOne(25);
            TestSpeedOne(30);
            TestSpeedOne(59);
            TestSpeedOne(61);
        }
        private void TestSpeedOne(double speed)
        {
            //using (ConnectMySQL cn = new ConnectMySQL())
            DriverDb db = new DriverDb();
            db.ConnectDb();
            {
                string sql = string.Format(@"Insert into Online (Mobitel_ID,Latitude,Longitude,Events,UnixTime,Valid,Speed) 
                Values ({0},{1},{2},{3},{4},{5}, " + db.ParamPrefics + "speed)", tt, 29881620, 18448320, 0, TestConvertToTimestamp(DateTime.Now), 1);

                //MySqlParameter[] pars = new MySqlParameter[1];
                db.NewSqlParameterArray(1);
                //pars[0] = new MySqlParameter("?speed", speed / 1.852);
                db.NewSqlParameter( db.ParamPrefics + "speed", speed / 1.852, 0);
                //cn.ExecuteNonQueryCommand(sql, pars);
                db.ExecuteNonQueryCommand( sql, db.GetSqlParameterArray );
            }
            db.CloseDbConnection();
        }

        private void TestStopArray()
        {
            TestSpeedOne(0);
            TestSpeedOne(0);
            Thread.Sleep(2000);
            TestSpeedOne(0);
            TestSpeedOne(0);
            Thread.Sleep(6000);
            TestSpeedOne(0);
            TestSpeedOne(0);
            MessageBox.Show("Ready");  
        }

        private void TestValidOne(int valid)
        {
            //using (ConnectMySQL cn = new ConnectMySQL())
            DriverDb db = new DriverDb();
            db.ConnectDb();
            {
                string sql = string.Format(@"Insert into Online (Mobitel_ID,Latitude,Longitude,Events,UnixTime,Valid,Speed) 
                Values ({0},{1},{2},{3},{4},{5},{6})", tt, 29882160, 18453960, 0, TestConvertToTimestamp(DateTime.Now), valid, 25);
                //cn.ExecuteNonQueryCommand(sql);
                db.ExecuteNonQueryCommand( sql );
            }
            db.CloseDbConnection();
        }
        private void TestValidArray()
        {
            TestValidOne(0);
            Thread.Sleep(1001);
            TestValidOne(0); 
        }

        private void TestSpeedInZoneArray()
        {
            TestZoneOutSpeed(5);
            TestZoneOutSpeed(100);
            TestZoneInSpeed(50);
            TestZoneInSpeed(77);
            TestZoneOutSpeed(100);
        }
        private void TestZoneOutSpeed(double speed)
        {
            //using (ConnectMySQL cn = new ConnectMySQL())
            DriverDb db = new DriverDb();
            db.ConnectDb();
            {
                string sql = string.Format(@"Insert into Online (Mobitel_ID,Latitude,Longitude,Events,UnixTime,Valid,Speed) 
                Values ({0},{1},{2},{3},{4},{5}, " + db.ParamPrefics + "speed)", tt, lat_out, lon_out, 0, TestConvertToTimestamp( DateTime.Now ), 1 );

                //MySqlParameter[] pars = new MySqlParameter[1];
                db.NewSqlParameterArray(1);
                //pars[0] = new MySqlParameter("?speed", speed / 1.852);
                db.NewSqlParameter( db.ParamPrefics + "speed", speed / 1.852, 0);
                //cn.ExecuteNonQueryCommand(sql, pars);
                db.ExecuteNonQueryCommand( sql, db.GetSqlParameterArray );
            }
            db.CloseDbConnection();
        }
        
        private void TestZoneInSpeed(double speed)
        {
            //using (ConnectMySQL cn = new ConnectMySQL())
            DriverDb db = new DriverDb();
            db.ConnectDb();
            {
                string sql = string.Format(@"Insert into Online (Mobitel_ID,Latitude,Longitude,Events,UnixTime,Valid,Speed) 
                Values ({0},{1},{2},{3},{4},{5}, " + db.ParamPrefics + "speed)", tt, lat_in, lon_in, 0, TestConvertToTimestamp( DateTime.Now ), 1 );

                //MySqlParameter[] pars = new MySqlParameter[1];
                db.NewSqlParameterArray(1);
                //pars[0] = new MySqlParameter("?speed", speed / 1.852);
                db.NewSqlParameter( db.ParamPrefics + "speed", speed / 1.852, 0);
                //cn.ExecuteNonQueryCommand(sql, pars);
                db.ExecuteNonQueryCommand( sql, db.GetSqlParameterArray );
            }
            db.CloseDbConnection();
        }


        private void TestStopInZoneArray()
        {
            TestZoneOutSpeed(0);
            Thread.Sleep(2000); 
            TestZoneOutSpeed(0);
            Thread.Sleep(2000); 
            TestZoneInSpeed(0);
            Thread.Sleep(2000); 
            TestZoneInSpeed(0);
            TestZoneOutSpeed(0);
        }

        [Conditional("DEBUG")] 
        public void TestEvents()
        {
            TestClearOnline();
            TestClearNtfLog();
            TestFlowTypeSensorArray();
        }

        private void TestFlowTypeSensorArray()
        {
            int TT_main = 108;
            int TT_1 = 98;
            int TT_2 = 101;
            int TT_3 = 105;
            TestFlowTypeSensor(TT_1, 1, 0);
            TestFlowTypeSensor(TT_2, 2, 0);
            for (int i = 0; i < 4; i++)
            {
                TestFlowTypeSensor(TT_main, i + 3, 0);
                Thread.Sleep(2000);
            }
            for (int i = 0; i < 4; i++)
            {
                TestFlowTypeSensor(TT_main, i + 10, 0);
                Thread.Sleep(2000);
            }
            TestFlowTypeSensor(TT_3, 30, 0);
            TestFlowTypeSensor(TT_2, 40, 0);

            for (int i = 0; i < 4; i++)
            {
                TestFlowTypeSensor(TT_main, i + 50, 0);
                Thread.Sleep(2000);
            }
        }


        void TestFlowTypeSensor(int TT, int LodId, int deltaLat)
        {
            DriverDb db = new DriverDb();
            db.ConnectDb();
            {
                string sql =
                    string.Format(@"Insert into Online (Mobitel_ID,Latitude,Longitude,Events,UnixTime,Valid,LogID) 
                Values ({0},{1},{2},{3},{4},{5},{6})", TT, lat_in + deltaLat, lon_in, 0,
                        TestConvertToTimestamp(DateTime.Now.AddHours(-2)), 1, LodId);
                db.ExecuteNonQueryCommand(sql);
            }
            db.CloseDbConnection();
        }

        #endregion

        public enum LoadRegime
        {
            Events,
            EventsZonesBriefVeh,
            EventsZonesFullVehAnaliz
        }

    }
}
