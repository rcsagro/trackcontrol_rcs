﻿using System;
using TrackControl.Vehicles;
namespace TrackControl.Notification
{
    interface INoticeSubItem
    {
        bool CheckEvent(GpsData gps);
    }
}
