﻿namespace TrackControl.Notification
{
    partial class MessageNoticeFuel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pcGraph = new DevExpress.XtraEditors.PanelControl();
            this.lbPoints = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pcGraph)).BeginInit();
            this.SuspendLayout();
            // 
            // sbOk
            // 
            this.sbOk.Location = new System.Drawing.Point(432, 528);
            // 
            // pcGraph
            // 
            this.pcGraph.Location = new System.Drawing.Point(8, 169);
            this.pcGraph.Name = "pcGraph";
            this.pcGraph.Size = new System.Drawing.Size(488, 353);
            this.pcGraph.TabIndex = 15;
            // 
            // lbPoints
            // 
            this.lbPoints.AutoSize = true;
            this.lbPoints.Location = new System.Drawing.Point(11, 527);
            this.lbPoints.Name = "lbPoints";
            this.lbPoints.Size = new System.Drawing.Size(75, 13);
            this.lbPoints.TabIndex = 16;
            this.lbPoints.Text = "Всего  точек:";
            // 
            // MessageNoticeFuel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(513, 566);
            this.Controls.Add(this.lbPoints);
            this.Controls.Add(this.pcGraph);
            this.Name = "MessageNoticeFuel";
            this.Controls.SetChildIndex(this.sbOk, 0);
            this.Controls.SetChildIndex(this.pcGraph, 0);
            this.Controls.SetChildIndex(this.lbPoints, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pcGraph)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl pcGraph;
        private System.Windows.Forms.Label lbPoints;
    }
}
