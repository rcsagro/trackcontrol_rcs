namespace TrackControl.Notification
{
    partial class NoticeControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NoticeControl));
            this.gvNoticeListDetal = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.coldId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateEvent = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcNoticeList = new DevExpress.XtraGrid.GridControl();
            this.gvNoticeList = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIconSmall = new DevExpress.XtraGrid.Columns.GridColumn();
            this.peIcon = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.colIsPopup = new DevExpress.XtraGrid.Columns.GridColumn();
            this.icbExclam = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.icExclam = new DevExpress.Utils.ImageCollection(this.components);
            this.colCNT_MAIL = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTitle = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCNT_VEH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpeed_min = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpeed_max = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStop = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colZones = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLossData = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLossGPS = new DevExpress.XtraGrid.Columns.GridColumn();
            this.icImages = new DevExpress.Utils.ImageCollection(this.components);
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.bbtAdd = new DevExpress.XtraBars.BarButtonItem();
            this.bbiDelete = new DevExpress.XtraBars.BarButtonItem();
            this.bbiEdit = new DevExpress.XtraBars.BarButtonItem();
            this.bbtRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.ceBox = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.repositoryItemLookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.prParams = new DevExpress.XtraVerticalGrid.PropertyGridControl();
            this.teRoute = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.reTimeEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit();
            this.teTimeOrder = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.tePassword = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.leDriverTypes = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.chStartTls = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.crSettings = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.erTimeForDelete = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erTimeForControl = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erTimePeriod = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.crMail = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.erMailAddressFrom = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erMailSmtpAddress = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erMailSmtpPort = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erMailSmtpLogin = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erMailSmtpPassword = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.erStartTls = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            this.crCOM = new DevExpress.XtraVerticalGrid.Rows.CategoryRow();
            this.erComDriverType = new DevExpress.XtraVerticalGrid.Rows.EditorRow();
            ((System.ComponentModel.ISupportInitialize)(this.gvNoticeListDetal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcNoticeList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvNoticeList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.peIcon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.icbExclam)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.icExclam)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.icImages)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.prParams)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teRoute)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reTimeEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teTimeOrder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tePassword)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leDriverTypes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chStartTls)).BeginInit();
            this.SuspendLayout();
            // 
            // gvNoticeListDetal
            // 
            this.gvNoticeListDetal.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.coldId,
            this.colDateEvent,
            this.colLocation});
            this.gvNoticeListDetal.GridControl = this.gcNoticeList;
            this.gvNoticeListDetal.Name = "gvNoticeListDetal";
            // 
            // coldId
            // 
            this.coldId.Caption = "ID";
            this.coldId.FieldName = "Id";
            this.coldId.Name = "coldId";
            this.coldId.OptionsColumn.ReadOnly = true;
            this.coldId.OptionsColumn.ShowCaption = false;
            // 
            // colDateEvent
            // 
            this.colDateEvent.AppearanceCell.Options.UseTextOptions = true;
            this.colDateEvent.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDateEvent.AppearanceHeader.Options.UseTextOptions = true;
            this.colDateEvent.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDateEvent.Caption = "����";
            this.colDateEvent.DisplayFormat.FormatString = "g";
            this.colDateEvent.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colDateEvent.FieldName = "DateEvent";
            this.colDateEvent.Name = "colDateEvent";
            this.colDateEvent.Visible = true;
            this.colDateEvent.VisibleIndex = 0;
            this.colDateEvent.Width = 258;
            // 
            // colLocation
            // 
            this.colLocation.AppearanceHeader.Options.UseTextOptions = true;
            this.colLocation.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLocation.Caption = "��������������";
            this.colLocation.FieldName = "Location";
            this.colLocation.Name = "colLocation";
            this.colLocation.Visible = true;
            this.colLocation.VisibleIndex = 1;
            this.colLocation.Width = 992;
            // 
            // gcNoticeList
            // 
            this.gcNoticeList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcNoticeList.Location = new System.Drawing.Point(0, 0);
            this.gcNoticeList.MainView = this.gvNoticeList;
            this.gcNoticeList.MenuManager = this.barManager1;
            this.gcNoticeList.Name = "gcNoticeList";
            this.gcNoticeList.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.peIcon,
            this.ceBox,
            this.repositoryItemLookUpEdit1,
            this.icbExclam});
            this.gcNoticeList.Size = new System.Drawing.Size(791, 615);
            this.gcNoticeList.TabIndex = 0;
            this.gcNoticeList.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvNoticeList,
            this.gvNoticeListDetal});
            // 
            // gvNoticeList
            // 
            this.gvNoticeList.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvNoticeList.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvNoticeList.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gvNoticeList.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvNoticeList.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvNoticeList.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvNoticeList.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvNoticeList.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvNoticeList.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gvNoticeList.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvNoticeList.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvNoticeList.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvNoticeList.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvNoticeList.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gvNoticeList.Appearance.Empty.Options.UseBackColor = true;
            this.gvNoticeList.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvNoticeList.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvNoticeList.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gvNoticeList.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvNoticeList.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gvNoticeList.Appearance.EvenRow.Options.UseForeColor = true;
            this.gvNoticeList.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvNoticeList.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvNoticeList.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gvNoticeList.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvNoticeList.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvNoticeList.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gvNoticeList.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvNoticeList.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gvNoticeList.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvNoticeList.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvNoticeList.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvNoticeList.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.gvNoticeList.Appearance.FixedLine.Options.UseBackColor = true;
            this.gvNoticeList.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gvNoticeList.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gvNoticeList.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gvNoticeList.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gvNoticeList.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(192)))), ((int)(((byte)(157)))));
            this.gvNoticeList.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(219)))), ((int)(((byte)(188)))));
            this.gvNoticeList.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gvNoticeList.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvNoticeList.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gvNoticeList.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvNoticeList.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvNoticeList.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvNoticeList.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvNoticeList.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvNoticeList.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvNoticeList.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gvNoticeList.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvNoticeList.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvNoticeList.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvNoticeList.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvNoticeList.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvNoticeList.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvNoticeList.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gvNoticeList.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvNoticeList.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvNoticeList.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gvNoticeList.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(242)))), ((int)(((byte)(213)))));
            this.gvNoticeList.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gvNoticeList.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gvNoticeList.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvNoticeList.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvNoticeList.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvNoticeList.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvNoticeList.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gvNoticeList.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvNoticeList.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gvNoticeList.Appearance.GroupRow.Options.UseForeColor = true;
            this.gvNoticeList.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvNoticeList.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvNoticeList.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gvNoticeList.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvNoticeList.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvNoticeList.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gvNoticeList.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvNoticeList.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvNoticeList.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gvNoticeList.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvNoticeList.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.gvNoticeList.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gvNoticeList.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvNoticeList.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvNoticeList.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvNoticeList.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvNoticeList.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gvNoticeList.Appearance.OddRow.Options.UseBackColor = true;
            this.gvNoticeList.Appearance.OddRow.Options.UseBorderColor = true;
            this.gvNoticeList.Appearance.OddRow.Options.UseForeColor = true;
            this.gvNoticeList.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(247)))));
            this.gvNoticeList.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gvNoticeList.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(148)))), ((int)(((byte)(148)))));
            this.gvNoticeList.Appearance.Preview.Options.UseBackColor = true;
            this.gvNoticeList.Appearance.Preview.Options.UseFont = true;
            this.gvNoticeList.Appearance.Preview.Options.UseForeColor = true;
            this.gvNoticeList.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvNoticeList.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gvNoticeList.Appearance.Row.Options.UseBackColor = true;
            this.gvNoticeList.Appearance.Row.Options.UseForeColor = true;
            this.gvNoticeList.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvNoticeList.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gvNoticeList.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvNoticeList.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.gvNoticeList.Appearance.SelectedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvNoticeList.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.gvNoticeList.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvNoticeList.Appearance.SelectedRow.Options.UseBorderColor = true;
            this.gvNoticeList.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gvNoticeList.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gvNoticeList.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gvNoticeList.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvNoticeList.Appearance.VertLine.Options.UseBackColor = true;
            this.gvNoticeList.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colId,
            this.colActive,
            this.colIconSmall,
            this.colIsPopup,
            this.colCNT_MAIL,
            this.colTitle,
            this.colCNT_VEH,
            this.colSpeed_min,
            this.colSpeed_max,
            this.colStop,
            this.colZones,
            this.colLossData,
            this.colLossGPS});
            this.gvNoticeList.GridControl = this.gcNoticeList;
            this.gvNoticeList.Images = this.icImages;
            this.gvNoticeList.Name = "gvNoticeList";
            this.gvNoticeList.OptionsSelection.MultiSelect = true;
            this.gvNoticeList.OptionsView.EnableAppearanceEvenRow = true;
            this.gvNoticeList.OptionsView.EnableAppearanceOddRow = true;
            this.gvNoticeList.CellValueChanging += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gvNoticeList_CellValueChanging);
            this.gvNoticeList.DoubleClick += new System.EventHandler(this.gvNoticeList_DoubleClick);
            // 
            // colId
            // 
            this.colId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colId.Caption = "Id";
            this.colId.FieldName = "Id";
            this.colId.Name = "colId";
            this.colId.OptionsColumn.AllowEdit = false;
            this.colId.OptionsColumn.ReadOnly = true;
            // 
            // colActive
            // 
            this.colActive.AppearanceHeader.Options.UseTextOptions = true;
            this.colActive.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colActive.Caption = " ";
            this.colActive.FieldName = "IsActive";
            this.colActive.ImageAlignment = System.Drawing.StringAlignment.Center;
            this.colActive.ImageIndex = 7;
            this.colActive.Name = "colActive";
            this.colActive.OptionsColumn.ShowCaption = false;
            this.colActive.ToolTip = "���������� �����������";
            this.colActive.Visible = true;
            this.colActive.VisibleIndex = 1;
            this.colActive.Width = 30;
            // 
            // colIconSmall
            // 
            this.colIconSmall.AppearanceHeader.Options.UseTextOptions = true;
            this.colIconSmall.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIconSmall.Caption = " ���";
            this.colIconSmall.ColumnEdit = this.peIcon;
            this.colIconSmall.FieldName = "IconSmall";
            this.colIconSmall.Name = "colIconSmall";
            this.colIconSmall.OptionsColumn.AllowEdit = false;
            this.colIconSmall.OptionsColumn.ReadOnly = true;
            this.colIconSmall.Visible = true;
            this.colIconSmall.VisibleIndex = 0;
            this.colIconSmall.Width = 97;
            // 
            // peIcon
            // 
            this.peIcon.Name = "peIcon";
            // 
            // colIsPopup
            // 
            this.colIsPopup.Caption = "IsPopup";
            this.colIsPopup.ColumnEdit = this.icbExclam;
            this.colIsPopup.FieldName = "IsPopup";
            this.colIsPopup.ImageAlignment = System.Drawing.StringAlignment.Center;
            this.colIsPopup.ImageIndex = 8;
            this.colIsPopup.Name = "colIsPopup";
            this.colIsPopup.OptionsColumn.AllowEdit = false;
            this.colIsPopup.OptionsColumn.AllowFocus = false;
            this.colIsPopup.OptionsColumn.ReadOnly = true;
            this.colIsPopup.OptionsColumn.ShowCaption = false;
            this.colIsPopup.ToolTip = "����������� �����������";
            this.colIsPopup.Visible = true;
            this.colIsPopup.VisibleIndex = 2;
            this.colIsPopup.Width = 30;
            // 
            // icbExclam
            // 
            this.icbExclam.AutoHeight = false;
            this.icbExclam.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.icbExclam.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", false, -1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", true, 0)});
            this.icbExclam.Name = "icbExclam";
            this.icbExclam.SmallImages = this.icExclam;
            // 
            // icExclam
            // 
            this.icExclam.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("icExclam.ImageStream")));
            this.icExclam.Images.SetKeyName(0, "exclamation-red-frame.png");
            // 
            // colCNT_MAIL
            // 
            this.colCNT_MAIL.AppearanceCell.Options.UseTextOptions = true;
            this.colCNT_MAIL.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCNT_MAIL.AppearanceHeader.Options.UseTextOptions = true;
            this.colCNT_MAIL.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCNT_MAIL.FieldName = "CNT_MAIL";
            this.colCNT_MAIL.ImageAlignment = System.Drawing.StringAlignment.Center;
            this.colCNT_MAIL.ImageIndex = 9;
            this.colCNT_MAIL.Name = "colCNT_MAIL";
            this.colCNT_MAIL.OptionsColumn.AllowEdit = false;
            this.colCNT_MAIL.OptionsColumn.ReadOnly = true;
            this.colCNT_MAIL.OptionsColumn.ShowCaption = false;
            this.colCNT_MAIL.Visible = true;
            this.colCNT_MAIL.VisibleIndex = 3;
            this.colCNT_MAIL.Width = 30;
            // 
            // colTitle
            // 
            this.colTitle.AppearanceHeader.Options.UseTextOptions = true;
            this.colTitle.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTitle.Caption = "�����������";
            this.colTitle.FieldName = "Title";
            this.colTitle.Name = "colTitle";
            this.colTitle.OptionsColumn.AllowEdit = false;
            this.colTitle.OptionsColumn.ReadOnly = true;
            this.colTitle.Visible = true;
            this.colTitle.VisibleIndex = 4;
            this.colTitle.Width = 148;
            // 
            // colCNT_VEH
            // 
            this.colCNT_VEH.AppearanceCell.Options.UseTextOptions = true;
            this.colCNT_VEH.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCNT_VEH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCNT_VEH.FieldName = "CNT_VEH";
            this.colCNT_VEH.ImageAlignment = System.Drawing.StringAlignment.Center;
            this.colCNT_VEH.ImageIndex = 0;
            this.colCNT_VEH.Name = "colCNT_VEH";
            this.colCNT_VEH.OptionsColumn.AllowEdit = false;
            this.colCNT_VEH.OptionsColumn.ReadOnly = true;
            this.colCNT_VEH.OptionsColumn.ShowCaption = false;
            this.colCNT_VEH.Visible = true;
            this.colCNT_VEH.VisibleIndex = 5;
            this.colCNT_VEH.Width = 22;
            // 
            // colSpeed_min
            // 
            this.colSpeed_min.Caption = "min";
            this.colSpeed_min.FieldName = "SPEED_MIN";
            this.colSpeed_min.ImageIndex = 4;
            this.colSpeed_min.Name = "colSpeed_min";
            this.colSpeed_min.OptionsColumn.AllowEdit = false;
            this.colSpeed_min.OptionsColumn.ReadOnly = true;
            this.colSpeed_min.Visible = true;
            this.colSpeed_min.VisibleIndex = 7;
            this.colSpeed_min.Width = 58;
            // 
            // colSpeed_max
            // 
            this.colSpeed_max.Caption = "max";
            this.colSpeed_max.FieldName = "SPEED_MAX";
            this.colSpeed_max.ImageIndex = 4;
            this.colSpeed_max.Name = "colSpeed_max";
            this.colSpeed_max.OptionsColumn.AllowEdit = false;
            this.colSpeed_max.OptionsColumn.ReadOnly = true;
            this.colSpeed_max.Visible = true;
            this.colSpeed_max.VisibleIndex = 8;
            this.colSpeed_max.Width = 71;
            // 
            // colStop
            // 
            this.colStop.Caption = "stop";
            this.colStop.FieldName = "STOP";
            this.colStop.ImageIndex = 3;
            this.colStop.Name = "colStop";
            this.colStop.OptionsColumn.AllowEdit = false;
            this.colStop.OptionsColumn.ReadOnly = true;
            this.colStop.Visible = true;
            this.colStop.VisibleIndex = 9;
            this.colStop.Width = 88;
            // 
            // colZones
            // 
            this.colZones.AppearanceCell.Options.UseTextOptions = true;
            this.colZones.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colZones.FieldName = "CNT_ZONE";
            this.colZones.ImageAlignment = System.Drawing.StringAlignment.Center;
            this.colZones.ImageIndex = 1;
            this.colZones.Name = "colZones";
            this.colZones.OptionsColumn.AllowEdit = false;
            this.colZones.OptionsColumn.ReadOnly = true;
            this.colZones.OptionsColumn.ShowCaption = false;
            this.colZones.Visible = true;
            this.colZones.VisibleIndex = 6;
            this.colZones.Width = 22;
            // 
            // colLossData
            // 
            this.colLossData.Caption = "gprs";
            this.colLossData.FieldName = "LOSS_DATA";
            this.colLossData.ImageIndex = 5;
            this.colLossData.Name = "colLossData";
            this.colLossData.OptionsColumn.AllowEdit = false;
            this.colLossData.OptionsColumn.ReadOnly = true;
            this.colLossData.Visible = true;
            this.colLossData.VisibleIndex = 10;
            this.colLossData.Width = 55;
            // 
            // colLossGPS
            // 
            this.colLossGPS.Caption = "gps";
            this.colLossGPS.FieldName = "LOSS_GPS";
            this.colLossGPS.ImageIndex = 6;
            this.colLossGPS.Name = "colLossGPS";
            this.colLossGPS.Visible = true;
            this.colLossGPS.VisibleIndex = 11;
            this.colLossGPS.Width = 122;
            // 
            // icImages
            // 
            this.icImages.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("icImages.ImageStream")));
            this.icImages.Images.SetKeyName(0, "Car.png");
            this.icImages.Images.SetKeyName(1, "Zone.gif");
            this.icImages.Images.SetKeyName(2, "dashboard.png");
            this.icImages.Images.SetKeyName(3, "1281944712_traffic_lights.png");
            this.icImages.Images.SetKeyName(4, "Gauge.png");
            this.icImages.Images.SetKeyName(5, "no-gprs_16.png");
            this.icImages.Images.SetKeyName(6, "1319098306_globe--exclamation.png");
            this.icImages.Images.SetKeyName(7, "exclamation--frame.png");
            this.icImages.Images.SetKeyName(8, "exclamation-red-frame.png");
            this.icImages.Images.SetKeyName(9, "mail-medium.png");
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbtAdd,
            this.bbiDelete,
            this.bbtRefresh,
            this.bbiEdit});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 12;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bbtAdd, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bbiDelete, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bbiEdit, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bbtRefresh, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph)});
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // bbtAdd
            // 
            this.bbtAdd.Caption = "��������";
            this.bbtAdd.Glyph = ((System.Drawing.Image)(resources.GetObject("bbtAdd.Glyph")));
            this.bbtAdd.Id = 0;
            this.bbtAdd.Name = "bbtAdd";
            this.bbtAdd.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbtAdd_ItemClick);
            // 
            // bbiDelete
            // 
            this.bbiDelete.Caption = "�������";
            this.bbiDelete.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiDelete.Glyph")));
            this.bbiDelete.Id = 8;
            this.bbiDelete.Name = "bbiDelete";
            this.bbiDelete.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiDelete_ItemClick);
            // 
            // bbiEdit
            // 
            this.bbiEdit.Caption = "�������������";
            this.bbiEdit.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiEdit.Glyph")));
            this.bbiEdit.Id = 11;
            this.bbiEdit.Name = "bbiEdit";
            this.bbiEdit.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiEdit_ItemClick);
            // 
            // bbtRefresh
            // 
            this.bbtRefresh.Caption = "��������";
            this.bbtRefresh.Glyph = ((System.Drawing.Image)(resources.GetObject("bbtRefresh.Glyph")));
            this.bbtRefresh.Id = 10;
            this.bbtRefresh.Name = "bbtRefresh";
            this.bbtRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbtRefresh_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(1091, 26);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 641);
            this.barDockControlBottom.Size = new System.Drawing.Size(1091, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 26);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 615);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1091, 26);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 615);
            // 
            // ceBox
            // 
            this.ceBox.AutoHeight = false;
            this.ceBox.Caption = "Check";
            this.ceBox.Name = "ceBox";
            // 
            // repositoryItemLookUpEdit1
            // 
            this.repositoryItemLookUpEdit1.AutoHeight = false;
            this.repositoryItemLookUpEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit1.Name = "repositoryItemLookUpEdit1";
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 26);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.gcNoticeList);
            this.splitContainerControl1.Panel1.Text = "Panel1";
            this.splitContainerControl1.Panel2.Controls.Add(this.prParams);
            this.splitContainerControl1.Panel2.Text = "Panel2";
            this.splitContainerControl1.Size = new System.Drawing.Size(1091, 615);
            this.splitContainerControl1.SplitterPosition = 791;
            this.splitContainerControl1.TabIndex = 0;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // prParams
            // 
            this.prParams.Dock = System.Windows.Forms.DockStyle.Fill;
            this.prParams.Location = new System.Drawing.Point(0, 0);
            this.prParams.Name = "prParams";
            this.prParams.RecordWidth = 55;
            this.prParams.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.teRoute,
            this.reTimeEdit,
            this.teTimeOrder,
            this.tePassword,
            this.leDriverTypes,
            this.chStartTls});
            this.prParams.RowHeaderWidth = 145;
            this.prParams.Rows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.crSettings,
            this.crMail,
            this.crCOM,
            this.erComDriverType});
            this.prParams.Size = new System.Drawing.Size(295, 615);
            this.prParams.TabIndex = 3;
            // 
            // teRoute
            // 
            this.teRoute.AutoHeight = false;
            this.teRoute.Name = "teRoute";
            // 
            // reTimeEdit
            // 
            this.reTimeEdit.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.reTimeEdit.AutoHeight = false;
            this.reTimeEdit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.reTimeEdit.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F4);
            this.reTimeEdit.DisplayFormat.FormatString = "t";
            this.reTimeEdit.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.reTimeEdit.EditFormat.FormatString = "t";
            this.reTimeEdit.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.reTimeEdit.Mask.EditMask = "t";
            this.reTimeEdit.Name = "reTimeEdit";
            this.reTimeEdit.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Default;
            // 
            // teTimeOrder
            // 
            this.teTimeOrder.AutoHeight = false;
            this.teTimeOrder.Mask.EditMask = "00:00";
            this.teTimeOrder.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Simple;
            this.teTimeOrder.Name = "teTimeOrder";
            // 
            // tePassword
            // 
            this.tePassword.AutoHeight = false;
            this.tePassword.Name = "tePassword";
            this.tePassword.PasswordChar = '*';
            // 
            // leDriverTypes
            // 
            this.leDriverTypes.AutoHeight = false;
            this.leDriverTypes.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leDriverTypes.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TypeName", "��������")});
            this.leDriverTypes.DisplayMember = "TypeName";
            this.leDriverTypes.Name = "leDriverTypes";
            this.leDriverTypes.ValueMember = "Id";
            // 
            // chStartTls
            // 
            this.chStartTls.AutoHeight = false;
            this.chStartTls.Caption = "Check";
            this.chStartTls.Name = "chStartTls";
            // 
            // crSettings
            // 
            this.crSettings.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.erTimeForDelete,
            this.erTimeForControl,
            this.erTimePeriod});
            this.crSettings.Height = 19;
            this.crSettings.Name = "crSettings";
            this.crSettings.Properties.Caption = "��������� �����������";
            // 
            // erTimeForDelete
            // 
            this.erTimeForDelete.Name = "erTimeForDelete";
            this.erTimeForDelete.Properties.Caption = "����� �������� ������� ��������,���";
            this.erTimeForDelete.Properties.FieldName = "TimeForDelete";
            this.erTimeForDelete.Properties.RowEdit = this.teRoute;
            // 
            // erTimeForControl
            // 
            this.erTimeForControl.Height = 16;
            this.erTimeForControl.Name = "erTimeForControl";
            this.erTimeForControl.Properties.Caption = "����� �������� ������������ �������� ������� , ���";
            this.erTimeForControl.Properties.FieldName = "TimeForControl";
            // 
            // erTimePeriod
            // 
            this.erTimePeriod.Name = "erTimePeriod";
            this.erTimePeriod.Properties.Caption = "������ ������ ������� ������, �";
            this.erTimePeriod.Properties.FieldName = "TimePeriod";
            // 
            // crMail
            // 
            this.crMail.ChildRows.AddRange(new DevExpress.XtraVerticalGrid.Rows.BaseRow[] {
            this.erMailAddressFrom,
            this.erMailSmtpAddress,
            this.erMailSmtpPort,
            this.erMailSmtpLogin,
            this.erMailSmtpPassword,
            this.erStartTls});
            this.crMail.Name = "crMail";
            this.crMail.Properties.Caption = "��������� E-mail ���������";
            // 
            // erMailAddressFrom
            // 
            this.erMailAddressFrom.Height = 16;
            this.erMailAddressFrom.Name = "erMailAddressFrom";
            this.erMailAddressFrom.Properties.Caption = "�������� ����� �����������";
            this.erMailAddressFrom.Properties.FieldName = "MailAddressFrom";
            // 
            // erMailSmtpAddress
            // 
            this.erMailSmtpAddress.Name = "erMailSmtpAddress";
            this.erMailSmtpAddress.Properties.Caption = "����� SMTP �������";
            this.erMailSmtpAddress.Properties.FieldName = "MailSmtpAddress";
            // 
            // erMailSmtpPort
            // 
            this.erMailSmtpPort.Name = "erMailSmtpPort";
            this.erMailSmtpPort.Properties.Caption = "���� SMTP �������";
            this.erMailSmtpPort.Properties.FieldName = "MailSmtpPort";
            // 
            // erMailSmtpLogin
            // 
            this.erMailSmtpLogin.Name = "erMailSmtpLogin";
            this.erMailSmtpLogin.Properties.Caption = "����� SMTP �������";
            this.erMailSmtpLogin.Properties.FieldName = "MailSmtpLogin";
            // 
            // erMailSmtpPassword
            // 
            this.erMailSmtpPassword.Name = "erMailSmtpPassword";
            this.erMailSmtpPassword.Properties.Caption = "������ SMTP �������";
            this.erMailSmtpPassword.Properties.FieldName = "MailSmtpPassword";
            this.erMailSmtpPassword.Properties.RowEdit = this.tePassword;
            // 
            // erStartTls
            // 
            this.erStartTls.Name = "erStartTls";
            this.erStartTls.Properties.Caption = "STARTTLS";
            this.erStartTls.Properties.FieldName = "MailStartTls";
            this.erStartTls.Properties.RowEdit = this.chStartTls;
            // 
            // crCOM
            // 
            this.crCOM.Name = "crCOM";
            this.crCOM.Properties.Caption = "��������� ��� �������";
            // 
            // erComDriverType
            // 
            this.erComDriverType.Name = "erComDriverType";
            this.erComDriverType.Properties.Caption = "C������������ �������� (GetHarvestSource)";
            this.erComDriverType.Properties.FieldName = "ComDriverType";
            this.erComDriverType.Properties.RowEdit = this.leDriverTypes;
            // 
            // NoticeControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainerControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "NoticeControl";
            this.Size = new System.Drawing.Size(1091, 641);
            ((System.ComponentModel.ISupportInitialize)(this.gvNoticeListDetal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcNoticeList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvNoticeList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.peIcon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.icbExclam)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.icExclam)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.icImages)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ceBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.prParams)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teRoute)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reTimeEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teTimeOrder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tePassword)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leDriverTypes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chStartTls)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarButtonItem bbtAdd;
        private DevExpress.XtraGrid.GridControl gcNoticeList;
        private DevExpress.XtraGrid.Views.Grid.GridView gvNoticeList;
        private DevExpress.XtraGrid.Columns.GridColumn colId;
        private DevExpress.XtraGrid.Columns.GridColumn colTitle;
        private DevExpress.XtraGrid.Columns.GridColumn colCNT_VEH;
        private DevExpress.Utils.ImageCollection icImages;
        private DevExpress.XtraGrid.Columns.GridColumn colSpeed_min;
        private DevExpress.XtraGrid.Columns.GridColumn colSpeed_max;
        private DevExpress.XtraGrid.Columns.GridColumn colStop;
        private DevExpress.XtraGrid.Columns.GridColumn colZones;
        private DevExpress.XtraBars.BarButtonItem bbiDelete;
        private DevExpress.XtraBars.BarButtonItem bbtRefresh;
        private DevExpress.XtraBars.BarButtonItem bbiEdit;
        private DevExpress.XtraGrid.Columns.GridColumn colIconSmall;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit peIcon;
        private DevExpress.XtraGrid.Columns.GridColumn colActive;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit ceBox;
        private DevExpress.XtraGrid.Columns.GridColumn colIsPopup;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox icbExclam;
        private DevExpress.Utils.ImageCollection icExclam;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit1;
        private DevExpress.XtraGrid.Views.Grid.GridView gvNoticeListDetal;
        private DevExpress.XtraGrid.Columns.GridColumn coldId;
        private DevExpress.XtraGrid.Columns.GridColumn colDateEvent;
        private DevExpress.XtraGrid.Columns.GridColumn colLocation;
        private DevExpress.XtraGrid.Columns.GridColumn colLossData;
        private DevExpress.XtraGrid.Columns.GridColumn colLossGPS;
        private DevExpress.XtraVerticalGrid.PropertyGridControl prParams;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit teRoute;
        private DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit reTimeEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit teTimeOrder;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow crSettings;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erTimeForDelete;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erTimeForControl;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erTimePeriod;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow crMail;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erMailAddressFrom;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erMailSmtpAddress;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erMailSmtpPort;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erMailSmtpLogin;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erMailSmtpPassword;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit tePassword;
        private DevExpress.XtraGrid.Columns.GridColumn colCNT_MAIL;
        private DevExpress.XtraVerticalGrid.Rows.CategoryRow crCOM;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erComDriverType;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit leDriverTypes;
        private DevExpress.XtraVerticalGrid.Rows.EditorRow erStartTls;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit chStartTls;
        //private NoticeSettings nsSettings;
    }
}
