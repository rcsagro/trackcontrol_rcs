namespace TrackControl.Notification
{
    partial class MessageNotice
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MessageNotice));
            this.pePopup = new DevExpress.XtraEditors.PictureEdit();
            this.meMess = new DevExpress.XtraEditors.MemoEdit();
            this.sbOk = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.pePopup.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.meMess.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // pePopup
            // 
            this.pePopup.Location = new System.Drawing.Point(12, 12);
            this.pePopup.Name = "pePopup";
            this.pePopup.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pePopup.Properties.Appearance.Options.UseBackColor = true;
            this.pePopup.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pePopup.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.pePopup.Size = new System.Drawing.Size(64, 64);
            this.pePopup.TabIndex = 12;
            // 
            // meMess
            // 
            this.meMess.Location = new System.Drawing.Point(82, 9);
            this.meMess.Name = "meMess";
            this.meMess.Size = new System.Drawing.Size(414, 154);
            this.meMess.TabIndex = 13;
            this.meMess.TabStop = false;
            this.meMess.UseOptimizedRendering = true;
            // 
            // sbOk
            // 
            this.sbOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.sbOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.sbOk.Location = new System.Drawing.Point(437, 174);
            this.sbOk.Name = "sbOk";
            this.sbOk.Size = new System.Drawing.Size(64, 26);
            this.sbOk.TabIndex = 14;
            this.sbOk.Text = "Ok";
            this.sbOk.Click += new System.EventHandler(this.sbOk_Click);
            // 
            // MessageNotice
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(513, 212);
            this.Controls.Add(this.sbOk);
            this.Controls.Add(this.meMess);
            this.Controls.Add(this.pePopup);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MessageNotice";
            this.Text = "����������� �������";
            this.TopMost = true;
            ((System.ComponentModel.ISupportInitialize)(this.pePopup.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.meMess.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PictureEdit pePopup;
        private DevExpress.XtraEditors.MemoEdit meMess;
        public DevExpress.XtraEditors.SimpleButton sbOk;
    }
}