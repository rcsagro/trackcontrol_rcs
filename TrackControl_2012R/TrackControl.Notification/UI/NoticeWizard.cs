using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Reflection;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraWizard;
using TrackControl.General;
using TrackControl.Notification.Properties;

namespace TrackControl.Notification
{
    public partial class NoticeWizard : DevExpress.XtraEditors.XtraForm
    {
        private bool _finish;
        public bool PageChanged { get; set; }

        /// <summary>
        /// ������ ������� �����������
        /// </summary>
        public NoticeItem Ni
        {
            get { return _ni; }
            set { _ni = value; }
        }

        private NoticeItem _ni;
        private readonly PageSelector _ps;
        private const int StopSelect = -1;

        /// <summary>
        /// ���������� ������������ �� ���������
        /// </summary>
        private readonly List<int> _pageSequence;

        public event MethodInvoker NoticeListChanged;

        public NoticeWizard(int idNotice)
        {
            InitializeComponent();

            _ni = idNotice == ConstsGen.RECORD_MISSING
                ? new NoticeItem()
                : new NoticeItem(idNotice, NoticeItem.LoadRegime.EventsZonesBriefVeh);
            _ps = new PageSelector(new StateSelectType(this));
            _pageSequence = new List<int>();
            Localization();
            InitImages();
            PageChanged = true;
        }

        private void InitImages()
        {
            peCH.Image = icTypesSmall.Images[(int) NoticeSubItemTypes.CheckZoneControl];
            peSpeed.Image = icTypesSmall.Images[(int) NoticeSubItemTypes.SpeedControl];
            peSensor.Image = icTypesSmall.Images[(int) NoticeSubItemTypes.SensorControl];
            peStop.Image = icTypesSmall.Images[(int) NoticeSubItemTypes.StopControl];
            peLossData.Image = icTypesSmall.Images[(int) NoticeSubItemTypes.LossData];
            pePower.Image = icTypesSmall.Images[(int) NoticeSubItemTypes.LossPower];
            peLossGPS.Image = icTypesSmall.Images[(int) NoticeSubItemTypes.LossGps];
            peLogicSensor.Image = icTypesSmall.Images[(int) NoticeSubItemTypes.SensorLogicControl];
            peSensorFlowType.Image = icTypesSmall.Images[(int) NoticeSubItemTypes.SensorFlowTypeControl - 1];
            peFuelDischarge.Image = icTypesSmall.Images[(int) NoticeSubItemTypes.SensorFlowTypeControl - 1];
            peVehUnload.Image = icTypesSmall.Images[(int) NoticeSubItemTypes.SensorFlowTypeControl];
            peSpeedAgro.Image = peSpeed.Image;
        }

        private void WcNoticeCancelClick(object sender, CancelEventArgs e)
        {
            this.Close();
        }

        private void WcNoticeFinishClick(object sender, CancelEventArgs e)
        {
            if (!_ps.SetPage()) return;
            _finish = true;
            if (_ni.Save()) NoticeListChanged();
            this.Close();
        }

        private void NoticeWizardFormClosing(object sender, FormClosingEventArgs e)
        {
            if (_finish) return;
            if (
                XtraMessageBox.Show(this, Resources.QuestionEditStop, Resources.NoticeMaster, MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question) == DialogResult.No)
                e.Cancel = true;
        }

        #region SELECTOR

        private void WcNoticeSelectedPageChanged(object sender, WizardPageChangedEventArgs e)
        {
            if (!_pageSequence.Contains(wcNotice.SelectedPageIndex)) _pageSequence.Add(wcNotice.SelectedPageIndex);
        }

        private void WcNoticeSelectedPageChanging(object sender, DevExpress.XtraWizard.WizardPageChangingEventArgs e)
        {
            switch (e.Direction)
            {
                case Direction.Backward:
                    if (_pageSequence.Count > 1)
                    {
                        int indexNext = _pageSequence[_pageSequence.Count - 2];
                        _pageSequence.Remove(_pageSequence[_pageSequence.Count - 1]);
                        e.Page = wcNotice.Pages[indexNext];
                        _ps.SetPageIndexToState(indexNext);
                    }
                    break;
                case Direction.Forward:
                {
                    e.Page = wcNotice.Pages[_ps.SelectPageIndex()];
                }
                    break;
            }
        }

        #endregion

        #region Properties

        private void SbIconSmallClick(object sender, EventArgs e)
        {
            if (ofdIcon.ShowDialog() == DialogResult.OK)
            {
                if (ofdIcon.OpenFile().Length > 10000)
                {
                    XtraMessageBox.Show(Resources.RestrictSize10, Resources.NoticeMaster);
                    return;
                }
                peNotice.Image = Image.FromStream(ofdIcon.OpenFile());
                PageChanged = true;
            }
        }

        private void SbIconLargeClick(object sender, EventArgs e)
        {
            if (ofdIcon.ShowDialog() == DialogResult.OK)
            {
                if (ofdIcon.OpenFile().Length > 100000)
                {
                    XtraMessageBox.Show(Resources.RestrictSize100, Resources.NoticeMaster);
                    return;
                }
                pePopup.Image = Image.FromStream(ofdIcon.OpenFile());
                PageChanged = true;
            }
        }

        private void CheActiveNoticeCheckedChanged(object sender, EventArgs e)
        {
            PageChanged = true;
        }

        private void ChePopupCheckedChanged(object sender, EventArgs e)
        {
            PageChanged = true;
        }

        private void CheActiveEmailCheckedChanged(object sender, EventArgs e)
        {
            gcMail.Enabled = (bool) cheActiveEmail.Checked;
            PageChanged = true;
        }

        private void TeTitleEditValueChanged(object sender, EventArgs e)
        {
            PageChanged = true;
        }

        private void TeTimeControlEditValueChanged(object sender, EventArgs e)
        {
            PageChanged = true;
        }

        private void LeSoundsEditValueChanged(object sender, EventArgs e)
        {
            PageChanged = true;
        }

        private void CbSoundPlayClick(object sender, EventArgs e)
        {
            var rw = new ResourceWorker(Assembly.GetExecutingAssembly());
            rw.PlaySound(leSounds.Text);
        }

        private void gvMail_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            PageChanged = true;
        }

        private void gvMail_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {
            if (!EMail.IsEmailValid(gvMail.GetRowCellValue(e.RowHandle, colMailEmail).ToString()))
            {
                gvMail.SetColumnError(colMailEmail, Resources.NotValidEmail);
                e.Valid = false;
            }
        }

        private void gvMail_RowCellStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs e)
        {
            if (e.RowHandle >= 0)
            {

                bool bActive = Convert.ToBoolean(gvMail.GetRowCellValue(e.RowHandle, colMailActive));
                if (!bActive)
                {
                    e.Appearance.BackColor = Color.FromArgb(50, 128, 128, 128);
                }
            }
        }

        private void tePeriod_Validating(object sender, CancelEventArgs e)
        {
            int period;
            if (!Int32.TryParse(((TextEdit) sender).Text, out period))
            {
                e.Cancel = true;
            }
        }

        private void tePeriod_EditValueChanged(object sender, EventArgs e)
        {
            PageChanged = true;
        }

        #endregion

        #region Vehicles

        private void gvVehicles_Click(object sender, EventArgs e)
        {
            PageChanged = true;
        }

        private void wpVehicles_PageValidating(object sender, WizardPageValidatingEventArgs e)
        {
            e.Valid = _ps.SetPage();
            if (!e.Valid)
            {
                e.ErrorText = Resources.SelectAtLeastOneVehicle;
            }
        }

        #endregion

        #region VehiclesSensors

        private void gvSensors_Click(object sender, EventArgs e)
        {
            PageChanged = true;
        }

        private void wpVehicleSensors_PageValidating(object sender, WizardPageValidatingEventArgs e)
        {
            e.Valid = _ps.SetPage();
            if (!e.Valid)
            {
                e.ErrorText = Resources.SelectAtLeastOneSensor;
            }
        }

        #endregion

        #region VehiclesLogicSensors

        private void gvLogicSensors_Click(object sender, EventArgs e)
        {
            PageChanged = true;
        }

        private void wpVehicleLogicSensors_PageValidating(object sender, WizardPageValidatingEventArgs e)
        {
            e.Valid = _ps.SetPage();
            if (!e.Valid)
            {
                e.ErrorText = Resources.SelectAtLeastOneSensor;
            }
        }

        #endregion

        #region VehicleFlowTypeSensor

        private void teFlowTypeRadius_EditValueChanged(object sender, EventArgs e)
        {
            PageChanged = true;
        }

        private void teFlowTypeTimeForFind_EditValueChanged(object sender, EventArgs e)
        {
            PageChanged = true;
        }

        private void teFlowTypeTimeForStop_EditValueChanged(object sender, EventArgs e)
        {
            PageChanged = true;
        }

        private void cheFlowTypeDUT_EditValueChanged(object sender, EventArgs e)
        {
            PageChanged = true;
        }

        private void teFlowTypeTimeForAction_EditValueChanged(object sender, EventArgs e)
        {
            PageChanged = true;
        }

        private void wpSensorFlowType_PageValidating(object sender, WizardPageValidatingEventArgs e)
        {
            e.Valid = _ps.SetPage();
            if (!e.Valid)
            {
                e.ErrorText = Resources.SpecifyFlowTypeSensor;
            }
        }

        #endregion

        #region Zones

        private void wpZones_PageValidating(object sender, WizardPageValidatingEventArgs e)
        {
            e.Valid = _ps.SetPage();
            if (!e.Valid)
            {
                e.ErrorText = Resources.SelectAtLeastOneCheckZone;
            }
        }

        private void gvZones_Click(object sender, EventArgs e)
        {
            PageChanged = true;
        }

        private void rgZoneTypeEventCross_SelectedIndexChanged(object sender, EventArgs e)
        {
            PageChanged = true;
        }

        private void rgZoneTypeEventLocate_SelectedIndexChanged(object sender, EventArgs e)
        {
            PageChanged = true;
        }

        private void rgZoneAddItem_SelectedIndexChanged(object sender, EventArgs e)
        {
            PageChanged = true;
        }

        private void chZoneLocate_CheckedChanged(object sender, EventArgs e)
        {
            if ((bool) chZoneLocate.EditValue) SetZoneControls(true);
        }

        private void chZoneCross_CheckedChanged(object sender, EventArgs e)
        {
            if ((bool) chZoneCross.EditValue) SetZoneControls(false);
        }

        private void SetZoneControls(bool IsLocate)
        {
            if (IsLocate)
            {
                rgZoneTypeEventCross.Enabled = false;
                rgZoneTypeEventLocate.Enabled = true;
                rgZoneAddItem.Enabled = true;
                chZoneLocate.Enabled = false;
                chZoneCross.Enabled = true;
                chZoneCross.EditValue = false;
            }
            else
            {
                rgZoneTypeEventCross.Enabled = true;
                rgZoneTypeEventLocate.Enabled = false;
                rgZoneAddItem.Enabled = false;
                chZoneCross.Enabled = false;
                chZoneLocate.Enabled = true;
                chZoneLocate.EditValue = false;
            }
            PageChanged = true;
        }

        #endregion

        #region Speed

        private void teSpeedMin_EditValueChanged(object sender, EventArgs e)
        {
            PageChanged = true;
        }

        private void teSpeedMax_EditValueChanged(object sender, EventArgs e)
        {
            PageChanged = true;
        }

        private void wpSpeed_PageValidating(object sender, WizardPageValidatingEventArgs e)
        {
            e.Valid = _ps.SetPage();
            if (!e.Valid)
            {
                e.ErrorText = Resources.SpecifySpeedInterval;
            }
        }

        #endregion

        #region Stops

        private void teStopTime_EditValueChanged(object sender, EventArgs e)
        {
            PageChanged = true;
        }

        private void wpStop_PageValidating(object sender, WizardPageValidatingEventArgs e)
        {
            e.Valid = _ps.SetPage();
            if (!e.Valid)
            {
                e.ErrorText = Resources.SpecifyStopInterval;
            }
        }

        #endregion

        #region LossData

        private void teLossData_EditValueChanged(object sender, EventArgs e)
        {
            PageChanged = true;
        }

        private void wpLossData_PageValidating(object sender, WizardPageValidatingEventArgs e)
        {
            e.Valid = _ps.SetPage();
            if (!e.Valid)
            {
                e.ErrorText = Resources.SpecifyLossDataInterval;
            }
        }

        #endregion

        #region LossGPS

        private void teLossGPS_EditValueChanged(object sender, EventArgs e)
        {
            PageChanged = true;
        }

        private void wpLossGPS_PageValidating(object sender, WizardPageValidatingEventArgs e)
        {
            e.Valid = _ps.SetPage();
            if (!e.Valid)
            {
                e.ErrorText = Resources.SpecifyLossGpsInterval;
            }
        }

        #endregion

        #region LossPower

        private void teLossPower_EditValueChanged(object sender, EventArgs e)
        {
            PageChanged = true;
        }

        private void wpLossPower_PageValidating(object sender, WizardPageValidatingEventArgs e)
        {
            e.Valid = _ps.SetPage();
            if (!e.Valid)
            {
                e.ErrorText = Resources.SpecifyLossBackupInterval;
            }
        }

        #endregion

        #region LogicSensors

        private void rgLogicSensors_SelectedIndexChanged(object sender, EventArgs e)
        {
            PageChanged = true;
        }

        private void wpLogicSensor_PageValidating(object sender, WizardPageValidatingEventArgs e)
        {
            e.Valid = _ps.SetPage();
            if (!e.Valid)
            {
                e.ErrorText = Resources.SpecifyLSensorValue;
            }
        }

        #endregion

        #region Sensors

        private void teValueBottom_EditValueChanged(object sender, EventArgs e)
        {
            PageChanged = true;
        }

        private void teValueTop_EditValueChanged(object sender, EventArgs e)
        {
            PageChanged = true;
        }

        private void rgSensorRange_SelectedIndexChanged(object sender, EventArgs e)
        {
            PageChanged = true;
        }

        private void wpSensor_PageValidating(object sender, WizardPageValidatingEventArgs e)
        {
            e.Valid = _ps.SetPage();
            if (!e.Valid)
            {
                e.ErrorText = Resources.SpecifySensorValueDiapazon;
            }
        }

        #endregion

        #region Time

        private void teTime_EditValueChanged(object sender, EventArgs e)
        {
            PageChanged = true;
        }

        private void wpTime_PageValidating(object sender, WizardPageValidatingEventArgs e)
        {
            e.Valid = _ps.SetPage();
            if (!e.Valid)
            {
                e.ErrorText = Resources.SpecifyTimeInterval;
            }
        }

        #endregion

        #region PopupUsers

        private void gvPopupUsers_Click(object sender, EventArgs e)
        {
            PageChanged = true;
        }

        private void wcPopupUsers_PageValidating(object sender, WizardPageValidatingEventArgs e)
        {
            _ps.SetPage();
        }

        private void gvPopupUsers_FocusedRowChanged(object sender,
            DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            gvPopupUsers.OptionsSelection.EnableAppearanceFocusedRow = true;
        }

        #endregion

        #region FuelDischarge

        private void teFuelDischarge_EditValueChanged(object sender, EventArgs e)
        {
            PageChanged = true;
        }

        private void teFuelZeroPointsCount_EditValueChanged(object sender, EventArgs e)
        {
            PageChanged = true;
        }

        private void teFuelAvgExpense_EditValueChanged(object sender, EventArgs e)
        {
            PageChanged = true;
        }

        private void wpFuelDischarge_PageValidating(object sender, WizardPageValidatingEventArgs e)
        {
            e.Valid = _ps.SetPage();
            if (!e.Valid)
            {
                e.ErrorText = Resources.SpecifyFuelDescharge;
            }
        }


        #endregion

        #region VehicleUnload

        private void wpVehicleUnload_PageValidating(object sender, WizardPageValidatingEventArgs e)
        {
            e.Valid = _ps.SetPage();
            if (!e.Valid)
            {
                e.ErrorText = Resources.SpecifyVehicleUnload;
            }
        }

        private void teTimeMaxUnload_EditValueChanged(object sender, EventArgs e)
        {
            PageChanged = true;
        }

        #endregion

        #region AgroWorksSpeed

        private void gvWorks_Click(object sender, EventArgs e)
        {
            PageChanged = true;
        }

        private void wpSpeedAgro_PageValidating(object sender, WizardPageValidatingEventArgs e)
        {
            e.Valid = _ps.SetPage();
            if (!e.Valid)
            {
                e.ErrorText = Resources.SelectAtLeastOneRecord;
            }
        }

        #endregion

        private void Localization()
        {
            this.Text = Resources.Notice;

            wpSelectType.Text = Resources.SelectNoticeType;
            grSelectType.Properties.Items[0].Description = Resources.ControlCheckZone;
            grSelectType.Properties.Items[1].Description = Resources.ControlSpeed;
            grSelectType.Properties.Items[2].Description = Resources.ControlStops;
            grSelectType.Properties.Items[3].Description = Resources.ControlLogicSensors;
            grSelectType.Properties.Items[4].Description = Resources.ControlSensors;
            grSelectType.Properties.Items[5].Description = Resources.ControlDataLoss;
            grSelectType.Properties.Items[6].Description = Resources.ControlGpsLoss;
            grSelectType.Properties.Items[7].Description = Resources.ControlPowerLoss;
            grSelectType.Properties.Items[8].Description = Resources.ControlFuelTypeSensors;
            grSelectType.Properties.Items[9].Description = Resources.FuelDischarge;
            grSelectType.Properties.Items[10].Description = Resources.VehicleUnload;
            grSelectType.Properties.Items[11].Description = Resources.ControlSpeedAgroModule;

            wpVehicles.Text = Resources.SelectVehicle;
            colGName.Caption = Resources.Groupe;
            colMakeCar.Caption = Resources.CarBrand;
            colCarModel.Caption = Resources.CarModel;
            colNumberPlate.Caption = Resources.Number;
            colDName.Caption = Resources.Driver;

            wpVehicleSensors.Text = Resources.SelectSensors;
            colGNameSensor.Caption = Resources.Groupe;
            colMakeCarSensor.Caption = Resources.CarBrand;
            colCarModelSensor.Caption = Resources.CarModel;
            colNumberPlateSensor.Caption = Resources.Number;
            colSensor.Caption = Resources.Sensor;
            colAlgoritm.Caption = Resources.Algoritm;

            wpZones.Text = Resources.SlectCheckZones;
            chZoneCross.Text = Resources.Crossing;
            chZoneLocate.Text = Resources.Finding;
            rgZoneTypeEventLocate.Properties.Items[0].Description = Resources.CheckZoneIn;
            rgZoneTypeEventLocate.Properties.Items[1].Description = Resources.CheckZoneOut;
            rgZoneAddItem.Properties.Items[0].Description = Resources.SpeedControl;
            rgZoneAddItem.Properties.Items[1].Description = Resources.StopControl;
            rgZoneAddItem.Properties.Items[2].Description = Resources.TimeControl;
            colGZone.Caption = Resources.Groupe;
            colZoneName.Caption = Resources.ControlCheckZone;
            rgZoneTypeEventCross.Properties.Items[0].Description = Resources.CheckZoneEntry;
            rgZoneTypeEventCross.Properties.Items[1].Description = Resources.CheckZoneExit;

            wpStop.Text = Resources.Downtime;
            lbStopTime.Text = Resources.DowntimeLimit;

            wpTime.Text = Resources.TimeControl;
            lbTime.Text = Resources.TimeControlLable;

            wpLossData.Text = Resources.LossData;
            lbLossData.Text = Resources.LossDataTime;

            wpLogicSensor.Text = Resources.SelectLSensorsValue;
            rgLogicSensors.Properties.Items[0].Description = Resources.ActiveOneZero;
            rgLogicSensors.Properties.Items[1].Description = Resources.ActiveZeroOne;
            chLogicSensorZones.Text = Resources.WatchCheckZones;

            wpSensor.Text = Resources.SelectSensorsValue;
            rgSensorRange.Properties.Items[0].Description = Resources.OperateInDiapazon;
            rgSensorRange.Properties.Items[1].Description = Resources.OperateOutDiapazon;
            lbLowLevel.Text = Resources.LevelLow;
            lbTopLevel.Text = Resources.LevelTop;
            chSensorZones.Text = Resources.WatchCheckZones;

            wpVehicleLogicSensors.Text = Resources.SelectLSensors;

            wpPower.Text = Resources.BackupPower;
            lbPower.Text = Resources.BackupPowerTime;

            cwpProperty.Text = Resources.NoticeProperty;
            lbPropName.Text = Resources.Description;
            cheActiveNotice.Text = Resources.Active;
            chePopup.Text = Resources.Active;
            lbTimeControl.Text = Resources.TimeForControl;
            lbIconSmall.Text = Resources.IconGournal;
            gcPopup.Text = Resources.PopupSettings;
            lbIconLarge.Text = Resources.Icon;
            lbSound.Text = Resources.Sound;
            lbPeriod.Text = Resources.PeriodAppearancesMinutes;

            wpSpeed.Text = Resources.SpeedControl;
            lbSpeedMin.Text = Resources.SpeedMin;
            lbSpeedMax.Text = Resources.SpeedMax;

            wpLossGPS.Text = Resources.LossGPS;
            lbLossGPS.Text = Resources.LossGPSTime;

            gcEmail.Text = Resources.MailSettings;
            cheActiveEmail.Text = Resources.Active;
            colMailActive.Caption = Resources.Active;

            wpFuelDischarge.Text = Resources.FuelDischarge;
            lbFuelDischarge.Text = Resources.ThresholdLiter;
            lbFuelZeroControl.Text = Resources.FuelDischargeZeroQty;
            lbFuelAvgExpense.Text = Resources.FuelAvgExpense;

            wpVehicleUnload.Text = Resources.VehicleUnload;
            lbTimeMaxUnload.Text = Resources.VehicleUnloadMaxTime;

            wpSpeedAgro.Text = Resources.SelectTypesWork;
            colGroupWork.Caption = Resources.Groupe;
            colTypeWork.Caption = Resources.Type;
            colNameWork.Caption = Resources.WorkType;
            colSpeedBottom.Caption = Resources.SpeedMin;
            colSpeedTop.Caption = Resources.SpeedMax;
        }

        private void grSelectType_SelectedIndexChanged(object sender, EventArgs e)
        {
            // �� ������� - ������ �������, �� ��� ���� �� ������������� ��������
        }
    }
}