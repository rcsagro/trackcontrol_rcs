﻿using System;
using System.Collections.Generic;
using System.Text;
using TrackControl.General;
using TrackControl.Notification.Properties;



namespace TrackControl.Notification
{
    public class NoticeTuning:ITuning
    {
        NoticeControl _view;
        #region ITuning Members
        public NoticeTuning()
        {
            // to do
        }
        string ITuning.Caption
        {
            get
            {
                return Resources.Notifications; 
            }
        }

        System.Drawing.Image ITuning.Icon
        {
            get { return Shared.WarningLittle; }
        }

        System.Windows.Forms.Control ITuning.View
        {
            get
            {
                if (null == _view)
                {
                    _view = new NoticeControl();
                }

                return _view;
            }
        }

        void ITuning.Advise()
        {
 
        }

        void ITuning.Release()
        {
            if (_view != null)
            _view.Parent = null;
        }

        #endregion
    }
}
