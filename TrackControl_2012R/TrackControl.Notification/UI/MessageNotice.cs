using System;
using System.Collections.Generic;
using System.Drawing;
using System.Reflection;
using System.Windows.Forms;
using Graph;
using TrackControl.General;
using TrackControl.Notification.Properties;

namespace TrackControl.Notification
{
    public partial class MessageNotice : DevExpress.XtraEditors.XtraForm
    {
        protected NoticeEventAnalyser _analyzer;

        public MessageNotice()
        {
            InitializeComponent();
        }

        public MessageNotice(NoticeEventAnalyser analyzer)
        {
            InitializeComponent();
            _analyzer = analyzer;
            this.Text = _analyzer.Nsi.Ni.Title;
            SetRandomLocation();
            Show(_analyzer.Nsi.Ni);
        }

        public List<MessageNotice> ListNotice { get; set; }
        public int Number { get; set; }

        protected void Show(NoticeItem ni)
        {
            if (ni.IconLarge != null)
                pePopup.Image = ImageConvert.byteArrayToImage(ni.IconLarge);

            meMess.Text = ni.MesPopup + "\r\n";

            if (ni.SoundName != null && ni.SoundName.Length > 0)
            {
                ResourceWorker rw = new ResourceWorker(Assembly.GetExecutingAssembly());
                rw.PlaySound(ni.SoundName);
            }
            Application.DoEvents();
        }

        /// <summary>
        /// ������������� ��������� ����� �� ������ � ��������� �������.
        /// ���������� ������ �����������.
        /// </summary>
        protected void SetRandomLocation()
        {
            int margin = 50; // ������ �� ����� ������ � ��������.
            int bottomMargin = 100; // ������ �� ������� ���� ������ � ��������.

            Random rnd = new Random();
            int x = rnd.Next(margin, Screen.PrimaryScreen.Bounds.Width - this.Width - margin);
            int y = rnd.Next(margin, Screen.PrimaryScreen.Bounds.Height - this.Height - bottomMargin);
            this.Location = new Point(x, y);

            //timer.Start();
        }

        private void sbOk_Click(object sender, EventArgs e)
        {
            NoticeProvider.WriteLog(_analyzer.Nsi_chz == null ? _analyzer.Nsi : _analyzer.Nsi_chz,
                _analyzer.Vehicle.Mobitel.Id,
                string.Format("{0}: {1} ({2} {3}) ", Resources.Received, UserBaseCurrent.Instance.Name,
                    _analyzer.Nsi.GpsEvent.Time, _analyzer.Nsi.Ni.Title), _analyzer.Nsi.GpsEvent.Time);
            this.Close();
        }
    }
}