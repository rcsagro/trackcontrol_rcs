namespace TrackControl.Notification
{
    partial class NoticeLogControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NoticeLogControl));
            this.gvLogDetal = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.coldID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateEvent = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocation = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcLog = new DevExpress.XtraGrid.GridControl();
            this.gvLog = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimeEvent = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIconSmall = new DevExpress.XtraGrid.Columns.GridColumn();
            this.peSmallIcon = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.colIsPopup = new DevExpress.XtraGrid.Columns.GridColumn();
            this.icbPopup = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.icExclam = new DevExpress.Utils.ImageCollection();
            this.colVehicle = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInfor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRead = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMobitel_id = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLat = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLng = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTypeEvent = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colZone_id = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colId_main = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpeed = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.barManager1 = new DevExpress.XtraBars.BarManager();
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.bbiStart = new DevExpress.XtraBars.BarButtonItem();
            this.beiSelectRegime = new DevExpress.XtraBars.BarEditItem();
            this.riSelectRegime = new DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup();
            this.beiStart = new DevExpress.XtraBars.BarEditItem();
            this.riStart = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.beiEnd = new DevExpress.XtraBars.BarEditItem();
            this.riEnd = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.bbiRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.bbiExcel = new DevExpress.XtraBars.BarButtonItem();
            this.bbiTest = new DevExpress.XtraBars.BarButtonItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.bbiTrack = new DevExpress.XtraBars.BarButtonItem();
            this.repositoryItemRadioGroup1 = new DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup();
            this.splitContainerControl1 = new DevExpress.XtraEditors.SplitContainerControl();
            this.psReport = new DevExpress.XtraPrinting.PrintingSystem();
            this.LinkGrid = new DevExpress.XtraPrinting.PrintableComponentLink();
            this.realTimeSource1 = new DevExpress.Data.RealTimeSource();
            ((System.ComponentModel.ISupportInitialize)(this.gvLogDetal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcLog)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvLog)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.peSmallIcon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.icbPopup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.icExclam)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.riSelectRegime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.riStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.riStart.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.riEnd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.riEnd.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemRadioGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).BeginInit();
            this.splitContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.psReport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkGrid.ImageCollection)).BeginInit();
            this.SuspendLayout();
            // 
            // gvLogDetal
            // 
            this.gvLogDetal.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.coldID,
            this.colDateEvent,
            this.colLocation});
            this.gvLogDetal.GridControl = this.gcLog;
            this.gvLogDetal.Name = "gvLogDetal";
            this.gvLogDetal.OptionsView.ShowGroupPanel = false;
            // 
            // coldID
            // 
            this.coldID.AppearanceHeader.Options.UseTextOptions = true;
            this.coldID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.coldID.Caption = "ID";
            this.coldID.FieldName = "Id";
            this.coldID.Name = "coldID";
            this.coldID.OptionsColumn.AllowEdit = false;
            this.coldID.OptionsColumn.AllowFocus = false;
            this.coldID.OptionsColumn.ReadOnly = true;
            // 
            // colDateEvent
            // 
            this.colDateEvent.AppearanceCell.Options.UseTextOptions = true;
            this.colDateEvent.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDateEvent.AppearanceHeader.Options.UseTextOptions = true;
            this.colDateEvent.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDateEvent.Caption = "����";
            this.colDateEvent.FieldName = "DateEvent";
            this.colDateEvent.Name = "colDateEvent";
            this.colDateEvent.OptionsColumn.AllowEdit = false;
            this.colDateEvent.OptionsColumn.AllowFocus = false;
            this.colDateEvent.OptionsColumn.ReadOnly = true;
            this.colDateEvent.Visible = true;
            this.colDateEvent.VisibleIndex = 0;
            this.colDateEvent.Width = 234;
            // 
            // colLocation
            // 
            this.colLocation.AppearanceHeader.Options.UseTextOptions = true;
            this.colLocation.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLocation.Caption = "��������������";
            this.colLocation.FieldName = "Location";
            this.colLocation.Name = "colLocation";
            this.colLocation.OptionsColumn.AllowEdit = false;
            this.colLocation.OptionsColumn.AllowFocus = false;
            this.colLocation.OptionsColumn.ReadOnly = true;
            this.colLocation.Visible = true;
            this.colLocation.VisibleIndex = 1;
            this.colLocation.Width = 1016;
            // 
            // gcLog
            // 
            this.gcLog.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.LevelTemplate = this.gvLogDetal;
            gridLevelNode1.RelationName = "Level1";
            this.gcLog.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gcLog.Location = new System.Drawing.Point(0, 0);
            this.gcLog.MainView = this.gvLog;
            this.gcLog.Name = "gcLog";
            this.gcLog.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.peSmallIcon,
            this.icbPopup});
            this.gcLog.Size = new System.Drawing.Size(497, 305);
            this.gcLog.TabIndex = 0;
            this.gcLog.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvLog,
            this.gridView2,
            this.gvLogDetal});
            // 
            // gvLog
            // 
            this.gvLog.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvLog.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvLog.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.Black;
            this.gvLog.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvLog.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvLog.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvLog.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvLog.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvLog.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Black;
            this.gvLog.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvLog.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvLog.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvLog.Appearance.Empty.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvLog.Appearance.Empty.BackColor2 = System.Drawing.Color.White;
            this.gvLog.Appearance.Empty.Options.UseBackColor = true;
            this.gvLog.Appearance.EvenRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvLog.Appearance.EvenRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(231)))), ((int)(((byte)(234)))));
            this.gvLog.Appearance.EvenRow.ForeColor = System.Drawing.Color.Black;
            this.gvLog.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvLog.Appearance.EvenRow.Options.UseBorderColor = true;
            this.gvLog.Appearance.EvenRow.Options.UseForeColor = true;
            this.gvLog.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvLog.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvLog.Appearance.FilterCloseButton.ForeColor = System.Drawing.Color.Black;
            this.gvLog.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvLog.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvLog.Appearance.FilterCloseButton.Options.UseForeColor = true;
            this.gvLog.Appearance.FilterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvLog.Appearance.FilterPanel.BackColor2 = System.Drawing.Color.White;
            this.gvLog.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvLog.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvLog.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvLog.Appearance.FixedLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(122)))), ((int)(((byte)(114)))), ((int)(((byte)(113)))));
            this.gvLog.Appearance.FixedLine.Options.UseBackColor = true;
            this.gvLog.Appearance.FocusedCell.BackColor = System.Drawing.Color.White;
            this.gvLog.Appearance.FocusedCell.ForeColor = System.Drawing.Color.Black;
            this.gvLog.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gvLog.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gvLog.Appearance.FocusedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(192)))), ((int)(((byte)(157)))));
            this.gvLog.Appearance.FocusedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(219)))), ((int)(((byte)(188)))));
            this.gvLog.Appearance.FocusedRow.ForeColor = System.Drawing.Color.Black;
            this.gvLog.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvLog.Appearance.FocusedRow.Options.UseBorderColor = true;
            this.gvLog.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvLog.Appearance.FooterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvLog.Appearance.FooterPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvLog.Appearance.FooterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvLog.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvLog.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvLog.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gvLog.Appearance.GroupButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvLog.Appearance.GroupButton.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(179)))));
            this.gvLog.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvLog.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvLog.Appearance.GroupFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvLog.Appearance.GroupFooter.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvLog.Appearance.GroupFooter.ForeColor = System.Drawing.Color.Black;
            this.gvLog.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvLog.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvLog.Appearance.GroupFooter.Options.UseForeColor = true;
            this.gvLog.Appearance.GroupPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(246)))), ((int)(((byte)(242)))), ((int)(((byte)(213)))));
            this.gvLog.Appearance.GroupPanel.BackColor2 = System.Drawing.Color.White;
            this.gvLog.Appearance.GroupPanel.ForeColor = System.Drawing.Color.Black;
            this.gvLog.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvLog.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvLog.Appearance.GroupRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvLog.Appearance.GroupRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvLog.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gvLog.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvLog.Appearance.GroupRow.Options.UseBorderColor = true;
            this.gvLog.Appearance.GroupRow.Options.UseForeColor = true;
            this.gvLog.Appearance.HeaderPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvLog.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvLog.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black;
            this.gvLog.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvLog.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvLog.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gvLog.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvLog.Appearance.HideSelectionRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvLog.Appearance.HideSelectionRow.ForeColor = System.Drawing.Color.Black;
            this.gvLog.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvLog.Appearance.HideSelectionRow.Options.UseBorderColor = true;
            this.gvLog.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gvLog.Appearance.HorzLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvLog.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvLog.Appearance.OddRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvLog.Appearance.OddRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvLog.Appearance.OddRow.ForeColor = System.Drawing.Color.Black;
            this.gvLog.Appearance.OddRow.Options.UseBackColor = true;
            this.gvLog.Appearance.OddRow.Options.UseBorderColor = true;
            this.gvLog.Appearance.OddRow.Options.UseForeColor = true;
            this.gvLog.Appearance.Preview.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(251)))), ((int)(((byte)(252)))), ((int)(((byte)(247)))));
            this.gvLog.Appearance.Preview.Font = new System.Drawing.Font("Verdana", 7.5F);
            this.gvLog.Appearance.Preview.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(148)))), ((int)(((byte)(148)))));
            this.gvLog.Appearance.Preview.Options.UseBackColor = true;
            this.gvLog.Appearance.Preview.Options.UseFont = true;
            this.gvLog.Appearance.Preview.Options.UseForeColor = true;
            this.gvLog.Appearance.Row.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(244)))), ((int)(((byte)(236)))));
            this.gvLog.Appearance.Row.ForeColor = System.Drawing.Color.Black;
            this.gvLog.Appearance.Row.Options.UseBackColor = true;
            this.gvLog.Appearance.Row.Options.UseForeColor = true;
            this.gvLog.Appearance.RowSeparator.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(232)))), ((int)(((byte)(201)))));
            this.gvLog.Appearance.RowSeparator.BackColor2 = System.Drawing.Color.White;
            this.gvLog.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvLog.Appearance.SelectedRow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(215)))), ((int)(((byte)(188)))));
            this.gvLog.Appearance.SelectedRow.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(230)))), ((int)(((byte)(203)))));
            this.gvLog.Appearance.SelectedRow.ForeColor = System.Drawing.Color.Black;
            this.gvLog.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvLog.Appearance.SelectedRow.Options.UseBorderColor = true;
            this.gvLog.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gvLog.Appearance.TopNewRow.BackColor = System.Drawing.Color.White;
            this.gvLog.Appearance.TopNewRow.Options.UseBackColor = true;
            this.gvLog.Appearance.VertLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(216)))), ((int)(((byte)(209)))), ((int)(((byte)(170)))));
            this.gvLog.Appearance.VertLine.Options.UseBackColor = true;
            this.gvLog.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colId,
            this.colTimeEvent,
            this.colIconSmall,
            this.colIsPopup,
            this.colVehicle,
            this.colInfor,
            this.colRead,
            this.colMobitel_id,
            this.colLat,
            this.colLng,
            this.colTypeEvent,
            this.colZone_id,
            this.colId_main,
            this.colSpeed});
            this.gvLog.GridControl = this.gcLog;
            this.gvLog.Images = this.icExclam;
            this.gvLog.Name = "gvLog";
            this.gvLog.OptionsView.EnableAppearanceEvenRow = true;
            this.gvLog.OptionsView.EnableAppearanceOddRow = true;
            this.gvLog.OptionsView.ShowFooter = true;
            this.gvLog.RowClick += new DevExpress.XtraGrid.Views.Grid.RowClickEventHandler(this.gvLog_RowClick);
            this.gvLog.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.gvLog_RowStyle);
            this.gvLog.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gvLog_FocusedRowChanged);
            // 
            // colId
            // 
            this.colId.AppearanceHeader.Options.UseTextOptions = true;
            this.colId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colId.Caption = "Id";
            this.colId.FieldName = "Id";
            this.colId.Name = "colId";
            this.colId.OptionsColumn.AllowEdit = false;
            this.colId.OptionsColumn.ReadOnly = true;
            this.colId.Width = 62;
            // 
            // colTimeEvent
            // 
            this.colTimeEvent.AppearanceCell.Options.UseTextOptions = true;
            this.colTimeEvent.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeEvent.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeEvent.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeEvent.Caption = "����";
            this.colTimeEvent.DisplayFormat.FormatString = "dd.MM.yyyy HH:mm:ss";
            this.colTimeEvent.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colTimeEvent.FieldName = "DateEvent";
            this.colTimeEvent.Name = "colTimeEvent";
            this.colTimeEvent.OptionsColumn.AllowEdit = false;
            this.colTimeEvent.OptionsColumn.ReadOnly = true;
            this.colTimeEvent.Visible = true;
            this.colTimeEvent.VisibleIndex = 2;
            this.colTimeEvent.Width = 109;
            // 
            // colIconSmall
            // 
            this.colIconSmall.AppearanceHeader.Options.UseTextOptions = true;
            this.colIconSmall.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIconSmall.Caption = "���";
            this.colIconSmall.ColumnEdit = this.peSmallIcon;
            this.colIconSmall.FieldName = "IconSmall";
            this.colIconSmall.Name = "colIconSmall";
            this.colIconSmall.OptionsColumn.ReadOnly = true;
            this.colIconSmall.Visible = true;
            this.colIconSmall.VisibleIndex = 0;
            this.colIconSmall.Width = 74;
            // 
            // peSmallIcon
            // 
            this.peSmallIcon.Name = "peSmallIcon";
            // 
            // colIsPopup
            // 
            this.colIsPopup.ColumnEdit = this.icbPopup;
            this.colIsPopup.FieldName = "IsPopup";
            this.colIsPopup.ImageAlignment = System.Drawing.StringAlignment.Center;
            this.colIsPopup.ImageIndex = 0;
            this.colIsPopup.Name = "colIsPopup";
            this.colIsPopup.OptionsColumn.ReadOnly = true;
            this.colIsPopup.OptionsColumn.ShowCaption = false;
            this.colIsPopup.ToolTip = "����������� �����������";
            this.colIsPopup.Visible = true;
            this.colIsPopup.VisibleIndex = 1;
            this.colIsPopup.Width = 59;
            // 
            // icbPopup
            // 
            this.icbPopup.AutoHeight = false;
            this.icbPopup.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.icbPopup.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", false, -1),
            new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", true, 0)});
            this.icbPopup.Name = "icbPopup";
            this.icbPopup.SmallImages = this.icExclam;
            // 
            // icExclam
            // 
            this.icExclam.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("icExclam.ImageStream")));
            this.icExclam.Images.SetKeyName(0, "exclamation-red-frame.png");
            // 
            // colVehicle
            // 
            this.colVehicle.AppearanceHeader.Options.UseTextOptions = true;
            this.colVehicle.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colVehicle.Caption = "���������";
            this.colVehicle.FieldName = "VehicleFullName";
            this.colVehicle.Name = "colVehicle";
            this.colVehicle.OptionsColumn.AllowEdit = false;
            this.colVehicle.OptionsColumn.ReadOnly = true;
            this.colVehicle.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "Id", "�����: {0} ")});
            this.colVehicle.Visible = true;
            this.colVehicle.VisibleIndex = 3;
            this.colVehicle.Width = 122;
            // 
            // colInfor
            // 
            this.colInfor.AppearanceHeader.Options.UseTextOptions = true;
            this.colInfor.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colInfor.Caption = "�������";
            this.colInfor.FieldName = "Infor";
            this.colInfor.Name = "colInfor";
            this.colInfor.OptionsColumn.AllowEdit = false;
            this.colInfor.OptionsColumn.ReadOnly = true;
            this.colInfor.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Max, "DateEvent", "���������  �����������: {0} ")});
            this.colInfor.Visible = true;
            this.colInfor.VisibleIndex = 4;
            this.colInfor.Width = 509;
            // 
            // colRead
            // 
            this.colRead.AppearanceCell.Options.UseTextOptions = true;
            this.colRead.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRead.AppearanceHeader.Options.UseTextOptions = true;
            this.colRead.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRead.Caption = "���������";
            this.colRead.FieldName = "IsRead";
            this.colRead.Name = "colRead";
            this.colRead.OptionsColumn.AllowEdit = false;
            this.colRead.OptionsColumn.ReadOnly = true;
            this.colRead.Width = 105;
            // 
            // colMobitel_id
            // 
            this.colMobitel_id.Caption = "Mobitel_id";
            this.colMobitel_id.FieldName = "Mobitel_id";
            this.colMobitel_id.Name = "colMobitel_id";
            this.colMobitel_id.OptionsColumn.AllowEdit = false;
            this.colMobitel_id.OptionsColumn.ReadOnly = true;
            // 
            // colLat
            // 
            this.colLat.Caption = "Lat";
            this.colLat.FieldName = "Lat";
            this.colLat.Name = "colLat";
            this.colLat.OptionsColumn.AllowEdit = false;
            this.colLat.OptionsColumn.ReadOnly = true;
            // 
            // colLng
            // 
            this.colLng.Caption = "Lng";
            this.colLng.FieldName = "Lng";
            this.colLng.Name = "colLng";
            this.colLng.OptionsColumn.AllowEdit = false;
            this.colLng.OptionsColumn.ReadOnly = true;
            // 
            // colTypeEvent
            // 
            this.colTypeEvent.Caption = "TypeEvent";
            this.colTypeEvent.FieldName = "TypeEvent";
            this.colTypeEvent.Name = "colTypeEvent";
            this.colTypeEvent.OptionsColumn.AllowEdit = false;
            this.colTypeEvent.OptionsColumn.ReadOnly = true;
            // 
            // colZone_id
            // 
            this.colZone_id.Caption = "Zone_id";
            this.colZone_id.FieldName = "Zone_id";
            this.colZone_id.Name = "colZone_id";
            this.colZone_id.OptionsColumn.AllowEdit = false;
            this.colZone_id.OptionsColumn.ReadOnly = true;
            // 
            // colId_main
            // 
            this.colId_main.Caption = "Id_main";
            this.colId_main.FieldName = "Id_main";
            this.colId_main.Name = "colId_main";
            this.colId_main.OptionsColumn.AllowEdit = false;
            this.colId_main.OptionsColumn.ReadOnly = true;
            // 
            // colSpeed
            // 
            this.colSpeed.Caption = "��������";
            this.colSpeed.FieldName = "Speed";
            this.colSpeed.Name = "colSpeed";
            this.colSpeed.OptionsColumn.AllowEdit = false;
            this.colSpeed.OptionsColumn.ReadOnly = true;
            // 
            // gridView2
            // 
            this.gridView2.GridControl = this.gcLog;
            this.gridView2.Name = "gridView2";
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.bbiRefresh,
            this.bbiTrack,
            this.bbiTest,
            this.bbiStart,
            this.bbiExcel,
            this.beiSelectRegime,
            this.beiStart,
            this.beiEnd});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 11;
            this.barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemRadioGroup1,
            this.riSelectRegime,
            this.riStart,
            this.riEnd});
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.FloatLocation = new System.Drawing.Point(290, 155);
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bbiStart, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.Width, this.beiSelectRegime, "", false, true, true, 130),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.Width, this.beiStart, "", false, true, true, 119),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.Width, this.beiEnd, "", false, true, true, 126),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bbiRefresh, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.bbiExcel, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph),
            new DevExpress.XtraBars.LinkPersistInfo(this.bbiTest)});
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // bbiStart
            // 
            this.bbiStart.Caption = "����";
            this.bbiStart.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiStart.Glyph")));
            this.bbiStart.Id = 5;
            this.bbiStart.Name = "bbiStart";
            this.bbiStart.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiStart_ItemClick);
            // 
            // beiSelectRegime
            // 
            this.beiSelectRegime.Edit = this.riSelectRegime;
            this.beiSelectRegime.EditValue = 1;
            this.beiSelectRegime.Id = 8;
            this.beiSelectRegime.ItemAppearance.Normal.BackColor = System.Drawing.Color.Transparent;
            this.beiSelectRegime.ItemAppearance.Normal.Options.UseBackColor = true;
            this.beiSelectRegime.Name = "beiSelectRegime";
            this.beiSelectRegime.EditValueChanged += new System.EventHandler(this.beiSelectRegime_EditValueChanged);
            // 
            // riSelectRegime
            // 
            this.riSelectRegime.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.riSelectRegime.Appearance.BackColor2 = System.Drawing.Color.Transparent;
            this.riSelectRegime.Appearance.Options.UseBackColor = true;
            this.riSelectRegime.AppearanceDisabled.BackColor = System.Drawing.Color.Transparent;
            this.riSelectRegime.AppearanceDisabled.BackColor2 = System.Drawing.Color.Transparent;
            this.riSelectRegime.AppearanceDisabled.Options.UseBackColor = true;
            this.riSelectRegime.AppearanceFocused.BackColor = System.Drawing.Color.Transparent;
            this.riSelectRegime.AppearanceFocused.BackColor2 = System.Drawing.Color.Transparent;
            this.riSelectRegime.AppearanceFocused.Options.UseBackColor = true;
            this.riSelectRegime.AppearanceReadOnly.BackColor = System.Drawing.Color.Transparent;
            this.riSelectRegime.AppearanceReadOnly.BackColor2 = System.Drawing.Color.Transparent;
            this.riSelectRegime.AppearanceReadOnly.Options.UseBackColor = true;
            this.riSelectRegime.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(1, "OnLine"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(2, "������")});
            this.riSelectRegime.Name = "riSelectRegime";
            // 
            // beiStart
            // 
            this.beiStart.Caption = "���� �";
            this.beiStart.Edit = this.riStart;
            this.beiStart.Id = 9;
            this.beiStart.Name = "beiStart";
            this.beiStart.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.Caption;
            // 
            // riStart
            // 
            this.riStart.Appearance.BackColor = System.Drawing.Color.White;
            this.riStart.Appearance.Options.UseBackColor = true;
            this.riStart.AutoHeight = false;
            this.riStart.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.riStart.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.riStart.CalendarTimeProperties.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F4);
            this.riStart.CalendarTimeProperties.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Default;
            this.riStart.DisplayFormat.FormatString = "g";
            this.riStart.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.riStart.EditFormat.FormatString = "g";
            this.riStart.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.riStart.Mask.EditMask = "g";
            this.riStart.Name = "riStart";
            // 
            // beiEnd
            // 
            this.beiEnd.Caption = "��";
            this.beiEnd.Edit = this.riEnd;
            this.beiEnd.Id = 10;
            this.beiEnd.Name = "beiEnd";
            this.beiEnd.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.Caption;
            // 
            // riEnd
            // 
            this.riEnd.AutoHeight = false;
            this.riEnd.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.riEnd.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.riEnd.CalendarTimeProperties.CloseUpKey = new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F4);
            this.riEnd.CalendarTimeProperties.PopupBorderStyle = DevExpress.XtraEditors.Controls.PopupBorderStyles.Default;
            this.riEnd.DisplayFormat.FormatString = "g";
            this.riEnd.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.riEnd.EditFormat.FormatString = "g";
            this.riEnd.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.riEnd.Mask.EditMask = "g";
            this.riEnd.Name = "riEnd";
            // 
            // bbiRefresh
            // 
            this.bbiRefresh.Caption = "��������";
            this.bbiRefresh.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiRefresh.Glyph")));
            this.bbiRefresh.Id = 0;
            this.bbiRefresh.Name = "bbiRefresh";
            this.bbiRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRefresh_ItemClick);
            // 
            // bbiExcel
            // 
            this.bbiExcel.Caption = "�������";
            this.bbiExcel.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiExcel.Glyph")));
            this.bbiExcel.Id = 6;
            this.bbiExcel.Name = "bbiExcel";
            this.bbiExcel.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiExcel_ItemClick);
            // 
            // bbiTest
            // 
            this.bbiTest.Caption = "Test";
            this.bbiTest.Id = 3;
            this.bbiTest.Name = "bbiTest";
            this.bbiTest.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.bbiTest.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiTest_ItemClick);
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(497, 48);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 538);
            this.barDockControlBottom.Size = new System.Drawing.Size(497, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 48);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 490);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(497, 48);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 490);
            // 
            // bbiTrack
            // 
            this.bbiTrack.Id = 2;
            this.bbiTrack.Name = "bbiTrack";
            // 
            // repositoryItemRadioGroup1
            // 
            this.repositoryItemRadioGroup1.Name = "repositoryItemRadioGroup1";
            // 
            // splitContainerControl1
            // 
            this.splitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl1.Horizontal = false;
            this.splitContainerControl1.Location = new System.Drawing.Point(0, 48);
            this.splitContainerControl1.Name = "splitContainerControl1";
            this.splitContainerControl1.Panel1.Controls.Add(this.gcLog);
            this.splitContainerControl1.Panel1.Text = "pnGrid";
            this.splitContainerControl1.Panel2.Text = "pnMap";
            this.splitContainerControl1.Size = new System.Drawing.Size(497, 490);
            this.splitContainerControl1.SplitterPosition = 305;
            this.splitContainerControl1.TabIndex = 4;
            this.splitContainerControl1.Text = "splitContainerControl1";
            // 
            // psReport
            // 
            this.psReport.Links.AddRange(new object[] {
            this.LinkGrid});
            // 
            // LinkGrid
            // 
            this.LinkGrid.Component = this.gcLog;
            // 
            // 
            // 
            this.LinkGrid.ImageCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("LinkGrid.ImageCollection.ImageStream")));
            this.LinkGrid.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.LinkGrid.PrintingSystemBase = this.psReport;
            // 
            // realTimeSource1
            // 
            this.realTimeSource1.DisplayableProperties = null;
            this.realTimeSource1.UseWeakEventHandler = true;
            // 
            // NoticeLogControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainerControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Name = "NoticeLogControl";
            this.Size = new System.Drawing.Size(497, 538);
            ((System.ComponentModel.ISupportInitialize)(this.gvLogDetal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcLog)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvLog)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.peSmallIcon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.icbPopup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.icExclam)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.riSelectRegime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.riStart.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.riStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.riEnd.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.riEnd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemRadioGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl1)).EndInit();
            this.splitContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.psReport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkGrid.ImageCollection)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gcLog;
        private DevExpress.XtraGrid.Views.Grid.GridView gvLog;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn colId;
        private DevExpress.XtraGrid.Columns.GridColumn colTimeEvent;
        private DevExpress.XtraGrid.Columns.GridColumn colVehicle;
        private DevExpress.XtraGrid.Columns.GridColumn colInfor;
        private DevExpress.XtraGrid.Columns.GridColumn colRead;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarButtonItem bbiRefresh;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraGrid.Columns.GridColumn colIconSmall;
        private DevExpress.XtraGrid.Columns.GridColumn colIsPopup;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit peSmallIcon;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox icbPopup;
        private DevExpress.Utils.ImageCollection icExclam;
        private DevExpress.XtraGrid.Views.Grid.GridView gvLogDetal;
        private DevExpress.XtraGrid.Columns.GridColumn coldID;
        private DevExpress.XtraGrid.Columns.GridColumn colDateEvent;
        private DevExpress.XtraGrid.Columns.GridColumn colLocation;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl1;
        private DevExpress.XtraGrid.Columns.GridColumn colMobitel_id;
        private DevExpress.XtraGrid.Columns.GridColumn colLat;
        private DevExpress.XtraGrid.Columns.GridColumn colLng;
        private DevExpress.XtraGrid.Columns.GridColumn colTypeEvent;
        private DevExpress.XtraGrid.Columns.GridColumn colZone_id;
        private DevExpress.XtraBars.BarButtonItem bbiTrack;
        private DevExpress.XtraGrid.Columns.GridColumn colId_main;
        private DevExpress.XtraGrid.Columns.GridColumn colSpeed;
        private DevExpress.XtraBars.BarButtonItem bbiTest;
        private DevExpress.XtraBars.BarButtonItem bbiStart;
        private DevExpress.XtraBars.BarButtonItem bbiExcel;
        private DevExpress.XtraPrinting.PrintingSystem psReport;
        private DevExpress.XtraPrinting.PrintableComponentLink LinkGrid;
        private DevExpress.XtraBars.BarEditItem beiSelectRegime;
        private DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup riSelectRegime;
        private DevExpress.XtraEditors.Repository.RepositoryItemRadioGroup repositoryItemRadioGroup1;
        private DevExpress.XtraBars.BarEditItem beiStart;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit riStart;
        private DevExpress.XtraBars.BarEditItem beiEnd;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit riEnd;
        private DevExpress.Data.RealTimeSource realTimeSource1;
    }
}
