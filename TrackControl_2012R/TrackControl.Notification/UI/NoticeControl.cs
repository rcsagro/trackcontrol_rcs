using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using TrackControl.Vehicles;
using TrackControl.General;
using TrackControl.Notification.Properties;  
using System.Runtime.Serialization;

namespace TrackControl.Notification
{
    public partial class NoticeControl : DevExpress.XtraEditors.XtraUserControl
    {
        public NoticeControl()
        {
             InitializeComponent();
             RefreshData();
             SetProperties();
             Localization();
        }

        private void SetProperties()
        {
            NoticeSetItem nsi = new NoticeSetItem();
            prParams.SelectedObject = nsi;
        }

        private void bbtAdd_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
           ShowWizard(ConstsGen.RECORD_MISSING);
        }

        private void bbtRefresh_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RefreshData();
        }
        private void RefreshData()
        {
            gcNoticeList.DataSource = NoticeProvider.GetNoticeList();
            leDriverTypes.DataSource = DriverVehicleProvider.GetListDriverTypes();
        }

        private void bbiDelete_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            XtraGridService.DeleteSelectedFromGrid(gvNoticeList, NoticeProvider.DeleteRecord, 0);
        }

        private void bbiEdit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            EditRecord();

        }

        private void EditRecord()
        {
            if (!gvNoticeList.IsValidRowHandle(gvNoticeList.FocusedRowHandle))
            {
                return;
            }
            int idNotice;
            if (Int32.TryParse(gvNoticeList.GetRowCellValue(gvNoticeList.FocusedRowHandle, "Id").ToString(), out idNotice))
            {
                ShowWizard(idNotice);
            }
        }
        private void ShowWizard(int idNotice)
        {
            NoticeWizard nw = new NoticeWizard(idNotice);
            nw.NoticeListChanged += RefreshData;
            nw.Show(); 
        }



        private void gvNoticeList_CellValueChanging(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            int idRecord = 0;
            if (e.Column.Name == "colActive")
            {
                if (Int32.TryParse(gvNoticeList.GetRowCellValue(e.RowHandle, gvNoticeList.Columns["Id"]).ToString(), out idRecord) && idRecord > 0)
                {

                    NoticeProvider.UpdateActivity(idRecord, e.Value); 
                }
            }
        }

        private void gvNoticeList_DoubleClick(object sender, EventArgs e)
        {
            EditRecord();
        }

        void Localization()
        {
            bbiDelete.Caption = Resources.Delete;
            bbiEdit.Caption = Resources.Edit;
            bbtAdd.Caption = Resources.Add;
            bbtRefresh.Caption = Resources.Refresh;
            colIconSmall.Caption = Resources.Type;

            colTitle.Caption = Resources.Notice;
            colSpeed_min.Caption = string.Format("{0},{1}", colSpeed_min.Caption,Resources.kmh);
            colSpeed_max.Caption = string.Format("{0},{1}", colSpeed_max.Caption, Resources.kmh);
            colStop.Caption = string.Format("{0},{1}", colStop.Caption, Resources.MinBrief);
            colLossData.Caption = string.Format("{0},{1}", colLossData.Caption, Resources.MinBrief);
            colLossGPS.Caption = string.Format("{0},{1}", colLossGPS.Caption, Resources.MinBrief);
            colActive.ToolTip = Resources.NoticeSwitch;
            colIsPopup.ToolTip = Resources.NoticePopup; 

            crSettings.Properties.Caption  =  Resources.NoticeSettings ;
            erTimeForDelete.Properties.Caption = Resources.TimeForDelete;
            erTimeForControl.Properties.Caption = Resources.TimeForControl;
            erTimePeriod.Properties.Caption = Resources.TimePeriod;

            crMail.Properties.Caption = Resources.MailSettings;
            erMailAddressFrom.Properties.Caption = Resources.MailAddressFrom;
            erMailSmtpAddress.Properties.Caption = Resources.MailSmtpAddress;
            erMailSmtpPort.Properties.Caption = Resources.MailSmtpPort;
            erMailSmtpLogin.Properties.Caption = Resources.MailSmtpLogin;
            erMailSmtpPassword.Properties.Caption = Resources.MailSmtpPassword;

            crCOM.Properties.Caption = Resources.ComSettings ;
            erComDriverType.Properties.Caption = Resources.ComDriverType;
        }
    }
}
