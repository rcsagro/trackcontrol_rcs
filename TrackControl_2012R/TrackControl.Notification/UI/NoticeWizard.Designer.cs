namespace TrackControl.Notification
{
    partial class NoticeWizard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NoticeWizard));
            this.wcNotice = new DevExpress.XtraWizard.WizardControl();
            this.wpVehicles = new DevExpress.XtraWizard.WizardPage();
            this.gcVehicles = new DevExpress.XtraGrid.GridControl();
            this.gvVehicles = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.TID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.VID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMakeCar = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCarModel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNumberPlate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.cwpProperty = new DevExpress.XtraWizard.CompletionWizardPage();
            this.tePeriod = new DevExpress.XtraEditors.TextEdit();
            this.lbPeriod = new DevExpress.XtraEditors.LabelControl();
            this.gcMail = new DevExpress.XtraGrid.GridControl();
            this.gvMail = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colMailId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMailActive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMailEmail = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gcEmail = new DevExpress.XtraEditors.GroupControl();
            this.cheActiveEmail = new DevExpress.XtraEditors.CheckEdit();
            this.teTimeControl = new DevExpress.XtraEditors.TextEdit();
            this.lbTimeControl = new DevExpress.XtraEditors.LabelControl();
            this.gcPopup = new DevExpress.XtraEditors.GroupControl();
            this.cbSoundPlay = new DevExpress.XtraEditors.SimpleButton();
            this.leSounds = new DevExpress.XtraEditors.LookUpEdit();
            this.lbSound = new DevExpress.XtraEditors.LabelControl();
            this.sbIconLarge = new DevExpress.XtraEditors.SimpleButton();
            this.lbIconLarge = new DevExpress.XtraEditors.LabelControl();
            this.pePopup = new DevExpress.XtraEditors.PictureEdit();
            this.chePopup = new DevExpress.XtraEditors.CheckEdit();
            this.cheActiveNotice = new DevExpress.XtraEditors.CheckEdit();
            this.sbIconSmall = new DevExpress.XtraEditors.SimpleButton();
            this.lbIconSmall = new DevExpress.XtraEditors.LabelControl();
            this.peNotice = new DevExpress.XtraEditors.PictureEdit();
            this.teTitle = new DevExpress.XtraEditors.TextEdit();
            this.lbPropName = new DevExpress.XtraEditors.LabelControl();
            this.wpZones = new DevExpress.XtraWizard.WizardPage();
            this.chZoneLocate = new DevExpress.XtraEditors.CheckEdit();
            this.chZoneCross = new DevExpress.XtraEditors.CheckEdit();
            this.rgZoneTypeEventLocate = new DevExpress.XtraEditors.RadioGroup();
            this.rgZoneAddItem = new DevExpress.XtraEditors.RadioGroup();
            this.rgZoneTypeEventCross = new DevExpress.XtraEditors.RadioGroup();
            this.gcZones = new DevExpress.XtraGrid.GridControl();
            this.gvZones = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.Id = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGZone = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Zone_ID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colZoneName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.wpSelectType = new DevExpress.XtraWizard.WizardPage();
            this.peSpeedAgro = new DevExpress.XtraEditors.PictureEdit();
            this.peVehUnload = new DevExpress.XtraEditors.PictureEdit();
            this.peFuelDischarge = new DevExpress.XtraEditors.PictureEdit();
            this.peSensorFlowType = new DevExpress.XtraEditors.PictureEdit();
            this.peLogicSensor = new DevExpress.XtraEditors.PictureEdit();
            this.peLossGPS = new DevExpress.XtraEditors.PictureEdit();
            this.pePower = new DevExpress.XtraEditors.PictureEdit();
            this.peStop = new DevExpress.XtraEditors.PictureEdit();
            this.peLossData = new DevExpress.XtraEditors.PictureEdit();
            this.peSensor = new DevExpress.XtraEditors.PictureEdit();
            this.peSpeed = new DevExpress.XtraEditors.PictureEdit();
            this.peCH = new DevExpress.XtraEditors.PictureEdit();
            this.grSelectType = new DevExpress.XtraEditors.RadioGroup();
            this.wpSpeed = new DevExpress.XtraWizard.WizardPage();
            this.teSpeedMax = new DevExpress.XtraEditors.TextEdit();
            this.lbSpeedMax = new DevExpress.XtraEditors.LabelControl();
            this.teSpeedMin = new DevExpress.XtraEditors.TextEdit();
            this.lbSpeedMin = new DevExpress.XtraEditors.LabelControl();
            this.wpStop = new DevExpress.XtraWizard.WizardPage();
            this.teStopTime = new DevExpress.XtraEditors.TextEdit();
            this.lbStopTime = new DevExpress.XtraEditors.LabelControl();
            this.wpLossData = new DevExpress.XtraWizard.WizardPage();
            this.teLossData = new DevExpress.XtraEditors.TextEdit();
            this.lbLossData = new DevExpress.XtraEditors.LabelControl();
            this.wpLossGPS = new DevExpress.XtraWizard.WizardPage();
            this.teLossGPS = new DevExpress.XtraEditors.TextEdit();
            this.lbLossGPS = new DevExpress.XtraEditors.LabelControl();
            this.wpVehicleLogicSensors = new DevExpress.XtraWizard.WizardPage();
            this.gcLogicSensors = new DevExpress.XtraGrid.GridControl();
            this.gvLogicSensors = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGNameLSensor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMakeCarLSensor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCarModelLSensor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNumberPlateLSensor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLSensor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.wpVehicleSensors = new DevExpress.XtraWizard.WizardPage();
            this.gcSensors = new DevExpress.XtraGrid.GridControl();
            this.gvSensors = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGNameSensor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMakeCarSensor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCarModelSensor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNumberPlateSensor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSensor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAlgoritm = new DevExpress.XtraGrid.Columns.GridColumn();
            this.wpSensor = new DevExpress.XtraWizard.WizardPage();
            this.chSensorZones = new DevExpress.XtraEditors.CheckEdit();
            this.rgSensorRange = new DevExpress.XtraEditors.RadioGroup();
            this.teValueTop = new DevExpress.XtraEditors.TextEdit();
            this.lbTopLevel = new DevExpress.XtraEditors.LabelControl();
            this.teValueBottom = new DevExpress.XtraEditors.TextEdit();
            this.lbLowLevel = new DevExpress.XtraEditors.LabelControl();
            this.wpLogicSensor = new DevExpress.XtraWizard.WizardPage();
            this.chLogicSensorZones = new DevExpress.XtraEditors.CheckEdit();
            this.rgLogicSensors = new DevExpress.XtraEditors.RadioGroup();
            this.wpPower = new DevExpress.XtraWizard.WizardPage();
            this.tePower = new DevExpress.XtraEditors.TextEdit();
            this.lbPower = new DevExpress.XtraEditors.LabelControl();
            this.wpTime = new DevExpress.XtraWizard.WizardPage();
            this.teTime = new DevExpress.XtraEditors.TextEdit();
            this.lbTime = new DevExpress.XtraEditors.LabelControl();
            this.wpSensorFlowType = new DevExpress.XtraWizard.WizardPage();
            this.teFlowTypeTimeForAction = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.cheFlowTypeDUT = new DevExpress.XtraEditors.CheckEdit();
            this.teFlowTypeTimeForStop = new DevExpress.XtraEditors.TextEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.teFlowTypeTimeForFind = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.teFlowTypeRadius = new DevExpress.XtraEditors.TextEdit();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.wcPopupUsers = new DevExpress.XtraWizard.WizardPage();
            this.gcPopupUsers = new DevExpress.XtraGrid.GridControl();
            this.gvPopupUsers = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colUId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rleUsers = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.rchActive = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.rlePUsers = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.wpFuelDischarge = new DevExpress.XtraWizard.WizardPage();
            this.teFuelAvgExpense = new DevExpress.XtraEditors.TextEdit();
            this.lbFuelAvgExpense = new DevExpress.XtraEditors.LabelControl();
            this.teFuelZeroPointsCount = new DevExpress.XtraEditors.TextEdit();
            this.lbFuelZeroControl = new DevExpress.XtraEditors.LabelControl();
            this.teFuelDischarge = new DevExpress.XtraEditors.TextEdit();
            this.lbFuelDischarge = new DevExpress.XtraEditors.LabelControl();
            this.wpVehicleUnload = new DevExpress.XtraWizard.WizardPage();
            this.lbTimeMaxUnload = new DevExpress.XtraEditors.LabelControl();
            this.teTimeMaxUnload = new DevExpress.XtraEditors.TextEdit();
            this.wpSpeedAgro = new DevExpress.XtraWizard.WizardPage();
            this.gcWorks = new DevExpress.XtraGrid.GridControl();
            this.gvWorks = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.WID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGroupWork = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTypeWork = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNameWork = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpeedBottom = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSpeedTop = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dxErrP = new DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider(this.components);
            this.icTypesSmall = new DevExpress.Utils.ImageCollection(this.components);
            this.ofdIcon = new System.Windows.Forms.OpenFileDialog();
            this.icTypesLarge = new DevExpress.Utils.ImageCollection(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.wcNotice)).BeginInit();
            this.wcNotice.SuspendLayout();
            this.wpVehicles.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcVehicles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvVehicles)).BeginInit();
            this.cwpProperty.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tePeriod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcMail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvMail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcEmail)).BeginInit();
            this.gcEmail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cheActiveEmail.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teTimeControl.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcPopup)).BeginInit();
            this.gcPopup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.leSounds.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pePopup.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chePopup.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cheActiveNotice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.peNotice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teTitle.Properties)).BeginInit();
            this.wpZones.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chZoneLocate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chZoneCross.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgZoneTypeEventLocate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgZoneAddItem.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgZoneTypeEventCross.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcZones)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvZones)).BeginInit();
            this.wpSelectType.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.peSpeedAgro.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.peVehUnload.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.peFuelDischarge.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.peSensorFlowType.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.peLogicSensor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.peLossGPS.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pePower.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.peStop.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.peLossData.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.peSensor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.peSpeed.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.peCH.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grSelectType.Properties)).BeginInit();
            this.wpSpeed.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teSpeedMax.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teSpeedMin.Properties)).BeginInit();
            this.wpStop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teStopTime.Properties)).BeginInit();
            this.wpLossData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teLossData.Properties)).BeginInit();
            this.wpLossGPS.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teLossGPS.Properties)).BeginInit();
            this.wpVehicleLogicSensors.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcLogicSensors)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvLogicSensors)).BeginInit();
            this.wpVehicleSensors.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcSensors)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvSensors)).BeginInit();
            this.wpSensor.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chSensorZones.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgSensorRange.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teValueTop.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teValueBottom.Properties)).BeginInit();
            this.wpLogicSensor.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chLogicSensorZones.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgLogicSensors.Properties)).BeginInit();
            this.wpPower.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tePower.Properties)).BeginInit();
            this.wpTime.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teTime.Properties)).BeginInit();
            this.wpSensorFlowType.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teFlowTypeTimeForAction.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cheFlowTypeDUT.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teFlowTypeTimeForStop.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teFlowTypeTimeForFind.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teFlowTypeRadius.Properties)).BeginInit();
            this.wcPopupUsers.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcPopupUsers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvPopupUsers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleUsers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rchActive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rlePUsers)).BeginInit();
            this.wpFuelDischarge.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teFuelAvgExpense.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teFuelZeroPointsCount.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.teFuelDischarge.Properties)).BeginInit();
            this.wpVehicleUnload.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teTimeMaxUnload.Properties)).BeginInit();
            this.wpSpeedAgro.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcWorks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvWorks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.icTypesSmall)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.icTypesLarge)).BeginInit();
            this.SuspendLayout();
            // 
            // wcNotice
            // 
            this.wcNotice.Controls.Add(this.wpVehicles);
            this.wcNotice.Controls.Add(this.cwpProperty);
            this.wcNotice.Controls.Add(this.wpZones);
            this.wcNotice.Controls.Add(this.wpSelectType);
            this.wcNotice.Controls.Add(this.wpSpeed);
            this.wcNotice.Controls.Add(this.wpStop);
            this.wcNotice.Controls.Add(this.wpLossData);
            this.wcNotice.Controls.Add(this.wpLossGPS);
            this.wcNotice.Controls.Add(this.wpVehicleLogicSensors);
            this.wcNotice.Controls.Add(this.wpVehicleSensors);
            this.wcNotice.Controls.Add(this.wpSensor);
            this.wcNotice.Controls.Add(this.wpLogicSensor);
            this.wcNotice.Controls.Add(this.wpPower);
            this.wcNotice.Controls.Add(this.wpTime);
            this.wcNotice.Controls.Add(this.wpSensorFlowType);
            this.wcNotice.Controls.Add(this.wcPopupUsers);
            this.wcNotice.Controls.Add(this.wpFuelDischarge);
            this.wcNotice.Controls.Add(this.wpVehicleUnload);
            this.wcNotice.Controls.Add(this.wpSpeedAgro);
            this.wcNotice.Dock = System.Windows.Forms.DockStyle.Fill;
            this.wcNotice.Location = new System.Drawing.Point(0, 0);
            this.wcNotice.Name = "wcNotice";
            this.wcNotice.Pages.AddRange(new DevExpress.XtraWizard.BaseWizardPage[] {
            this.wpSelectType,
            this.wpVehicles,
            this.wpVehicleSensors,
            this.wpVehicleLogicSensors,
            this.wpZones,
            this.wpSpeed,
            this.wpStop,
            this.wpLossData,
            this.wpLossGPS,
            this.wpSensor,
            this.wpLogicSensor,
            this.wpPower,
            this.wpSensorFlowType,
            this.wpTime,
            this.wcPopupUsers,
            this.wpFuelDischarge,
            this.wpVehicleUnload,
            this.wpSpeedAgro,
            this.cwpProperty});
            this.wcNotice.PreviousButtonCausesValidation = true;
            this.wcNotice.Size = new System.Drawing.Size(716, 491);
            this.wcNotice.Text = "";
            this.wcNotice.WizardStyle = DevExpress.XtraWizard.WizardStyle.WizardAero;
            this.wcNotice.SelectedPageChanged += new DevExpress.XtraWizard.WizardPageChangedEventHandler(this.WcNoticeSelectedPageChanged);
            this.wcNotice.SelectedPageChanging += new DevExpress.XtraWizard.WizardPageChangingEventHandler(this.WcNoticeSelectedPageChanging);
            this.wcNotice.CancelClick += new System.ComponentModel.CancelEventHandler(this.WcNoticeCancelClick);
            this.wcNotice.FinishClick += new System.ComponentModel.CancelEventHandler(this.WcNoticeFinishClick);
            // 
            // wpVehicles
            // 
            this.wpVehicles.Controls.Add(this.gcVehicles);
            this.wpVehicles.Name = "wpVehicles";
            this.wpVehicles.Size = new System.Drawing.Size(656, 329);
            this.wpVehicles.Text = "�������� ������������ ��������";
            this.wpVehicles.PageValidating += new DevExpress.XtraWizard.WizardPageValidatingEventHandler(this.wpVehicles_PageValidating);
            // 
            // gcVehicles
            // 
            this.gcVehicles.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcVehicles.Location = new System.Drawing.Point(0, 0);
            this.gcVehicles.MainView = this.gvVehicles;
            this.gcVehicles.Name = "gcVehicles";
            this.gcVehicles.Size = new System.Drawing.Size(656, 329);
            this.gcVehicles.TabIndex = 2;
            this.gcVehicles.TabStop = false;
            this.gcVehicles.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvVehicles});
            // 
            // gvVehicles
            // 
            this.gvVehicles.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.TID,
            this.colGName,
            this.VID,
            this.colMakeCar,
            this.colCarModel,
            this.colNumberPlate,
            this.colDName});
            this.gvVehicles.GridControl = this.gcVehicles;
            this.gvVehicles.Name = "gvVehicles";
            this.gvVehicles.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gvVehicles.OptionsSelection.MultiSelect = true;
            this.gvVehicles.Click += new System.EventHandler(this.gvVehicles_Click);
            // 
            // TID
            // 
            this.TID.Caption = "TID";
            this.TID.FieldName = "TID";
            this.TID.Name = "TID";
            this.TID.OptionsColumn.AllowEdit = false;
            this.TID.OptionsColumn.AllowFocus = false;
            this.TID.OptionsColumn.ReadOnly = true;
            // 
            // colGName
            // 
            this.colGName.AppearanceHeader.Options.UseTextOptions = true;
            this.colGName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colGName.Caption = "������";
            this.colGName.FieldName = "GName";
            this.colGName.Name = "colGName";
            this.colGName.OptionsColumn.AllowEdit = false;
            this.colGName.OptionsColumn.AllowFocus = false;
            this.colGName.OptionsColumn.ReadOnly = true;
            this.colGName.Visible = true;
            this.colGName.VisibleIndex = 0;
            // 
            // VID
            // 
            this.VID.Caption = "VID";
            this.VID.FieldName = "VID";
            this.VID.Name = "VID";
            this.VID.OptionsColumn.AllowEdit = false;
            this.VID.OptionsColumn.AllowFocus = false;
            this.VID.OptionsColumn.ReadOnly = true;
            // 
            // colMakeCar
            // 
            this.colMakeCar.AppearanceHeader.Options.UseTextOptions = true;
            this.colMakeCar.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMakeCar.Caption = "����� ";
            this.colMakeCar.FieldName = "MakeCar";
            this.colMakeCar.Name = "colMakeCar";
            this.colMakeCar.OptionsColumn.AllowEdit = false;
            this.colMakeCar.OptionsColumn.AllowFocus = false;
            this.colMakeCar.OptionsColumn.ReadOnly = true;
            this.colMakeCar.Visible = true;
            this.colMakeCar.VisibleIndex = 1;
            // 
            // colCarModel
            // 
            this.colCarModel.AppearanceHeader.Options.UseTextOptions = true;
            this.colCarModel.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCarModel.Caption = "������";
            this.colCarModel.FieldName = "CarModel";
            this.colCarModel.Name = "colCarModel";
            this.colCarModel.OptionsColumn.AllowEdit = false;
            this.colCarModel.OptionsColumn.AllowFocus = false;
            this.colCarModel.OptionsColumn.ReadOnly = true;
            this.colCarModel.Visible = true;
            this.colCarModel.VisibleIndex = 2;
            // 
            // colNumberPlate
            // 
            this.colNumberPlate.AppearanceHeader.Options.UseTextOptions = true;
            this.colNumberPlate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNumberPlate.Caption = "�����";
            this.colNumberPlate.FieldName = "NumberPlate";
            this.colNumberPlate.Name = "colNumberPlate";
            this.colNumberPlate.OptionsColumn.AllowEdit = false;
            this.colNumberPlate.OptionsColumn.AllowFocus = false;
            this.colNumberPlate.OptionsColumn.ReadOnly = true;
            this.colNumberPlate.Visible = true;
            this.colNumberPlate.VisibleIndex = 3;
            // 
            // colDName
            // 
            this.colDName.AppearanceHeader.Options.UseTextOptions = true;
            this.colDName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDName.Caption = "��������";
            this.colDName.FieldName = "DName";
            this.colDName.Name = "colDName";
            this.colDName.OptionsColumn.AllowEdit = false;
            this.colDName.OptionsColumn.AllowFocus = false;
            this.colDName.OptionsColumn.ReadOnly = true;
            this.colDName.Visible = true;
            this.colDName.VisibleIndex = 4;
            // 
            // cwpProperty
            // 
            this.cwpProperty.Controls.Add(this.tePeriod);
            this.cwpProperty.Controls.Add(this.lbPeriod);
            this.cwpProperty.Controls.Add(this.gcMail);
            this.cwpProperty.Controls.Add(this.gcEmail);
            this.cwpProperty.Controls.Add(this.teTimeControl);
            this.cwpProperty.Controls.Add(this.lbTimeControl);
            this.cwpProperty.Controls.Add(this.gcPopup);
            this.cwpProperty.Controls.Add(this.cheActiveNotice);
            this.cwpProperty.Controls.Add(this.sbIconSmall);
            this.cwpProperty.Controls.Add(this.lbIconSmall);
            this.cwpProperty.Controls.Add(this.peNotice);
            this.cwpProperty.Controls.Add(this.teTitle);
            this.cwpProperty.Controls.Add(this.lbPropName);
            this.cwpProperty.Name = "cwpProperty";
            this.cwpProperty.Size = new System.Drawing.Size(656, 329);
            this.cwpProperty.Text = "�������� �����������";
            // 
            // tePeriod
            // 
            this.tePeriod.EditValue = "0";
            this.tePeriod.Location = new System.Drawing.Point(553, 91);
            this.tePeriod.Name = "tePeriod";
            this.tePeriod.Properties.Appearance.Options.UseTextOptions = true;
            this.tePeriod.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.tePeriod.Size = new System.Drawing.Size(50, 20);
            this.tePeriod.TabIndex = 16;
            this.tePeriod.TabStop = false;
            this.tePeriod.EditValueChanged += new System.EventHandler(this.tePeriod_EditValueChanged);
            this.tePeriod.Validating += new System.ComponentModel.CancelEventHandler(this.tePeriod_Validating);
            // 
            // lbPeriod
            // 
            this.lbPeriod.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.lbPeriod.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lbPeriod.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbPeriod.Location = new System.Drawing.Point(234, 94);
            this.lbPeriod.Name = "lbPeriod";
            this.lbPeriod.Size = new System.Drawing.Size(313, 14);
            this.lbPeriod.TabIndex = 15;
            this.lbPeriod.Text = "������ ��������� (������)";
            // 
            // gcMail
            // 
            this.gcMail.Location = new System.Drawing.Point(299, 153);
            this.gcMail.MainView = this.gvMail;
            this.gcMail.Name = "gcMail";
            this.gcMail.Size = new System.Drawing.Size(349, 88);
            this.gcMail.TabIndex = 14;
            this.gcMail.UseEmbeddedNavigator = true;
            this.gcMail.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvMail});
            // 
            // gvMail
            // 
            this.gvMail.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colMailId,
            this.colMailActive,
            this.colMailEmail});
            this.gvMail.GridControl = this.gcMail;
            this.gvMail.Name = "gvMail";
            this.gvMail.OptionsNavigation.AutoFocusNewRow = true;
            this.gvMail.OptionsView.ShowGroupPanel = false;
            this.gvMail.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.gvMail_RowCellStyle);
            this.gvMail.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gvMail_CellValueChanged);
            this.gvMail.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.gvMail_ValidateRow);
            // 
            // colMailId
            // 
            this.colMailId.AppearanceHeader.Options.UseTextOptions = true;
            this.colMailId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMailId.Caption = "MailId";
            this.colMailId.FieldName = "Id";
            this.colMailId.Name = "colMailId";
            // 
            // colMailActive
            // 
            this.colMailActive.AppearanceHeader.Options.UseTextOptions = true;
            this.colMailActive.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMailActive.Caption = "�������";
            this.colMailActive.FieldName = "IsActive";
            this.colMailActive.Name = "colMailActive";
            this.colMailActive.Visible = true;
            this.colMailActive.VisibleIndex = 0;
            this.colMailActive.Width = 65;
            // 
            // colMailEmail
            // 
            this.colMailEmail.AppearanceHeader.Options.UseTextOptions = true;
            this.colMailEmail.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMailEmail.Caption = "Email";
            this.colMailEmail.FieldName = "Addres";
            this.colMailEmail.Name = "colMailEmail";
            this.colMailEmail.Visible = true;
            this.colMailEmail.VisibleIndex = 1;
            this.colMailEmail.Width = 168;
            // 
            // gcEmail
            // 
            this.gcEmail.Controls.Add(this.cheActiveEmail);
            this.gcEmail.Location = new System.Drawing.Point(294, 125);
            this.gcEmail.Name = "gcEmail";
            this.gcEmail.Size = new System.Drawing.Size(359, 142);
            this.gcEmail.TabIndex = 13;
            this.gcEmail.Text = "��������� E-mail ��������";
            // 
            // cheActiveEmail
            // 
            this.cheActiveEmail.EditValue = true;
            this.cheActiveEmail.Location = new System.Drawing.Point(5, 24);
            this.cheActiveEmail.Name = "cheActiveEmail";
            this.cheActiveEmail.Properties.Appearance.Options.UseTextOptions = true;
            this.cheActiveEmail.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.cheActiveEmail.Properties.Caption = "�������";
            this.cheActiveEmail.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.cheActiveEmail.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cheActiveEmail.Size = new System.Drawing.Size(74, 19);
            this.cheActiveEmail.TabIndex = 10;
            this.cheActiveEmail.TabStop = false;
            this.cheActiveEmail.CheckedChanged += new System.EventHandler(this.CheActiveEmailCheckedChanged);
            // 
            // teTimeControl
            // 
            this.teTimeControl.Location = new System.Drawing.Point(553, 65);
            this.teTimeControl.Name = "teTimeControl";
            this.teTimeControl.Properties.Appearance.Options.UseTextOptions = true;
            this.teTimeControl.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.teTimeControl.Size = new System.Drawing.Size(50, 20);
            this.teTimeControl.TabIndex = 12;
            this.teTimeControl.TabStop = false;
            this.teTimeControl.EditValueChanged += new System.EventHandler(this.TeTimeControlEditValueChanged);
            // 
            // lbTimeControl
            // 
            this.lbTimeControl.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.lbTimeControl.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.lbTimeControl.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.lbTimeControl.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.lbTimeControl.Location = new System.Drawing.Point(62, 65);
            this.lbTimeControl.Name = "lbTimeControl";
            this.lbTimeControl.Size = new System.Drawing.Size(485, 20);
            this.lbTimeControl.TabIndex = 11;
            this.lbTimeControl.Text = "����� �������� ������������ �������� ������� , ���";
            // 
            // gcPopup
            // 
            this.gcPopup.Controls.Add(this.cbSoundPlay);
            this.gcPopup.Controls.Add(this.leSounds);
            this.gcPopup.Controls.Add(this.lbSound);
            this.gcPopup.Controls.Add(this.sbIconLarge);
            this.gcPopup.Controls.Add(this.lbIconLarge);
            this.gcPopup.Controls.Add(this.pePopup);
            this.gcPopup.Controls.Add(this.chePopup);
            this.gcPopup.Location = new System.Drawing.Point(12, 125);
            this.gcPopup.Name = "gcPopup";
            this.gcPopup.Size = new System.Drawing.Size(276, 142);
            this.gcPopup.TabIndex = 10;
            this.gcPopup.Text = "��������� ������������ �������";
            // 
            // cbSoundPlay
            // 
            this.cbSoundPlay.Image = ((System.Drawing.Image)(resources.GetObject("cbSoundPlay.Image")));
            this.cbSoundPlay.Location = new System.Drawing.Point(244, 117);
            this.cbSoundPlay.Name = "cbSoundPlay";
            this.cbSoundPlay.Size = new System.Drawing.Size(23, 19);
            this.cbSoundPlay.TabIndex = 16;
            this.cbSoundPlay.Text = "...";
            this.cbSoundPlay.Click += new System.EventHandler(this.CbSoundPlayClick);
            // 
            // leSounds
            // 
            this.leSounds.Location = new System.Drawing.Point(110, 116);
            this.leSounds.Name = "leSounds";
            this.leSounds.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.leSounds.Properties.NullText = "Default";
            this.leSounds.Size = new System.Drawing.Size(128, 20);
            this.leSounds.TabIndex = 15;
            this.leSounds.TabStop = false;
            this.leSounds.EditValueChanged += new System.EventHandler(this.LeSoundsEditValueChanged);
            // 
            // lbSound
            // 
            this.lbSound.Location = new System.Drawing.Point(13, 119);
            this.lbSound.Name = "lbSound";
            this.lbSound.Size = new System.Drawing.Size(85, 13);
            this.lbSound.TabIndex = 14;
            this.lbSound.Text = "�������� ������";
            // 
            // sbIconLarge
            // 
            this.sbIconLarge.Location = new System.Drawing.Point(244, 27);
            this.sbIconLarge.Name = "sbIconLarge";
            this.sbIconLarge.Size = new System.Drawing.Size(23, 19);
            this.sbIconLarge.TabIndex = 13;
            this.sbIconLarge.Text = "...";
            this.sbIconLarge.Click += new System.EventHandler(this.SbIconLargeClick);
            // 
            // lbIconLarge
            // 
            this.lbIconLarge.Location = new System.Drawing.Point(125, 30);
            this.lbIconLarge.Name = "lbIconLarge";
            this.lbIconLarge.Size = new System.Drawing.Size(40, 13);
            this.lbIconLarge.TabIndex = 12;
            this.lbIconLarge.Text = "������ ";
            // 
            // pePopup
            // 
            this.pePopup.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pePopup.Location = new System.Drawing.Point(171, 27);
            this.pePopup.Name = "pePopup";
            this.pePopup.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pePopup.Properties.Appearance.Options.UseBackColor = true;
            this.pePopup.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pePopup.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.pePopup.Size = new System.Drawing.Size(64, 64);
            this.pePopup.TabIndex = 11;
            // 
            // chePopup
            // 
            this.chePopup.EditValue = true;
            this.chePopup.Location = new System.Drawing.Point(5, 25);
            this.chePopup.Name = "chePopup";
            this.chePopup.Properties.Appearance.Options.UseTextOptions = true;
            this.chePopup.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.chePopup.Properties.Caption = "�������";
            this.chePopup.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.chePopup.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.chePopup.Size = new System.Drawing.Size(74, 19);
            this.chePopup.TabIndex = 10;
            this.chePopup.TabStop = false;
            this.chePopup.CheckedChanged += new System.EventHandler(this.ChePopupCheckedChanged);
            // 
            // cheActiveNotice
            // 
            this.cheActiveNotice.EditValue = true;
            this.cheActiveNotice.Location = new System.Drawing.Point(12, 42);
            this.cheActiveNotice.Name = "cheActiveNotice";
            this.cheActiveNotice.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.cheActiveNotice.Properties.Appearance.Options.UseFont = true;
            this.cheActiveNotice.Properties.Appearance.Options.UseTextOptions = true;
            this.cheActiveNotice.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.cheActiveNotice.Properties.Caption = "�������";
            this.cheActiveNotice.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.cheActiveNotice.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cheActiveNotice.Size = new System.Drawing.Size(76, 19);
            this.cheActiveNotice.TabIndex = 9;
            this.cheActiveNotice.TabStop = false;
            this.cheActiveNotice.CheckedChanged += new System.EventHandler(this.CheActiveNoticeCheckedChanged);
            // 
            // sbIconSmall
            // 
            this.sbIconSmall.Location = new System.Drawing.Point(578, 39);
            this.sbIconSmall.Name = "sbIconSmall";
            this.sbIconSmall.Size = new System.Drawing.Size(25, 20);
            this.sbIconSmall.TabIndex = 8;
            this.sbIconSmall.Text = "...";
            this.sbIconSmall.Click += new System.EventHandler(this.SbIconSmallClick);
            // 
            // lbIconSmall
            // 
            this.lbIconSmall.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.lbIconSmall.Location = new System.Drawing.Point(334, 45);
            this.lbIconSmall.Name = "lbIconSmall";
            this.lbIconSmall.Size = new System.Drawing.Size(198, 14);
            this.lbIconSmall.TabIndex = 7;
            this.lbIconSmall.Text = "������ ��� ������� �����������";
            // 
            // peNotice
            // 
            this.peNotice.Location = new System.Drawing.Point(538, 39);
            this.peNotice.Name = "peNotice";
            this.peNotice.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.peNotice.Size = new System.Drawing.Size(34, 25);
            this.peNotice.TabIndex = 6;
            // 
            // teTitle
            // 
            this.teTitle.Location = new System.Drawing.Point(74, 13);
            this.teTitle.Name = "teTitle";
            this.teTitle.Size = new System.Drawing.Size(534, 20);
            this.teTitle.TabIndex = 1;
            this.teTitle.TabStop = false;
            this.teTitle.EditValueChanged += new System.EventHandler(this.TeTitleEditValueChanged);
            // 
            // lbPropName
            // 
            this.lbPropName.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.lbPropName.Location = new System.Drawing.Point(12, 16);
            this.lbPropName.Name = "lbPropName";
            this.lbPropName.Size = new System.Drawing.Size(56, 14);
            this.lbPropName.TabIndex = 0;
            this.lbPropName.Text = "��������";
            // 
            // wpZones
            // 
            this.wpZones.Controls.Add(this.chZoneLocate);
            this.wpZones.Controls.Add(this.chZoneCross);
            this.wpZones.Controls.Add(this.rgZoneTypeEventLocate);
            this.wpZones.Controls.Add(this.rgZoneAddItem);
            this.wpZones.Controls.Add(this.rgZoneTypeEventCross);
            this.wpZones.Controls.Add(this.gcZones);
            this.wpZones.Name = "wpZones";
            this.wpZones.Size = new System.Drawing.Size(656, 329);
            this.wpZones.Text = "�������� ����������� ����";
            this.wpZones.PageValidating += new DevExpress.XtraWizard.WizardPageValidatingEventHandler(this.wpZones_PageValidating);
            // 
            // chZoneLocate
            // 
            this.chZoneLocate.Location = new System.Drawing.Point(440, 92);
            this.chZoneLocate.Name = "chZoneLocate";
            this.chZoneLocate.Properties.Caption = "����������";
            this.chZoneLocate.Size = new System.Drawing.Size(133, 19);
            this.chZoneLocate.TabIndex = 8;
            this.chZoneLocate.CheckedChanged += new System.EventHandler(this.chZoneLocate_CheckedChanged);
            // 
            // chZoneCross
            // 
            this.chZoneCross.EditValue = true;
            this.chZoneCross.Location = new System.Drawing.Point(440, 3);
            this.chZoneCross.Name = "chZoneCross";
            this.chZoneCross.Properties.Caption = "�����������";
            this.chZoneCross.Size = new System.Drawing.Size(133, 19);
            this.chZoneCross.TabIndex = 7;
            this.chZoneCross.CheckedChanged += new System.EventHandler(this.chZoneCross_CheckedChanged);
            // 
            // rgZoneTypeEventLocate
            // 
            this.rgZoneTypeEventLocate.EditValue = 2;
            this.rgZoneTypeEventLocate.Enabled = false;
            this.rgZoneTypeEventLocate.Location = new System.Drawing.Point(442, 114);
            this.rgZoneTypeEventLocate.Name = "rgZoneTypeEventLocate";
            this.rgZoneTypeEventLocate.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(2, "� ����������� ����"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(3, "��� ����������� ����")});
            this.rgZoneTypeEventLocate.Size = new System.Drawing.Size(200, 63);
            this.rgZoneTypeEventLocate.TabIndex = 6;
            this.rgZoneTypeEventLocate.SelectedIndexChanged += new System.EventHandler(this.rgZoneTypeEventLocate_SelectedIndexChanged);
            // 
            // rgZoneAddItem
            // 
            this.rgZoneAddItem.EditValue = 1;
            this.rgZoneAddItem.Enabled = false;
            this.rgZoneAddItem.Location = new System.Drawing.Point(442, 174);
            this.rgZoneAddItem.Name = "rgZoneAddItem";
            this.rgZoneAddItem.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(1, "�������� ��������"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(4, "�������� ���������"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(8, "�������� �������")});
            this.rgZoneAddItem.Size = new System.Drawing.Size(200, 85);
            this.rgZoneAddItem.TabIndex = 5;
            this.rgZoneAddItem.SelectedIndexChanged += new System.EventHandler(this.rgZoneAddItem_SelectedIndexChanged);
            // 
            // rgZoneTypeEventCross
            // 
            this.rgZoneTypeEventCross.EditValue = 0;
            this.rgZoneTypeEventCross.Location = new System.Drawing.Point(442, 25);
            this.rgZoneTypeEventCross.Name = "rgZoneTypeEventCross";
            this.rgZoneTypeEventCross.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(0, "����� � ����������� ����"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(1, "����� �� ����������� ����")});
            this.rgZoneTypeEventCross.Size = new System.Drawing.Size(200, 63);
            this.rgZoneTypeEventCross.TabIndex = 2;
            this.rgZoneTypeEventCross.SelectedIndexChanged += new System.EventHandler(this.rgZoneTypeEventCross_SelectedIndexChanged);
            // 
            // gcZones
            // 
            this.gcZones.Location = new System.Drawing.Point(3, 3);
            this.gcZones.MainView = this.gvZones;
            this.gcZones.Name = "gcZones";
            this.gcZones.Size = new System.Drawing.Size(433, 259);
            this.gcZones.TabIndex = 1;
            this.gcZones.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvZones});
            // 
            // gvZones
            // 
            this.gvZones.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.Id,
            this.colGZone,
            this.Zone_ID,
            this.colZoneName});
            this.gvZones.GridControl = this.gcZones;
            this.gvZones.Name = "gvZones";
            this.gvZones.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gvZones.OptionsSelection.MultiSelect = true;
            this.gvZones.Click += new System.EventHandler(this.gvZones_Click);
            // 
            // Id
            // 
            this.Id.AppearanceHeader.Options.UseTextOptions = true;
            this.Id.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Id.Caption = "Id";
            this.Id.FieldName = "Id";
            this.Id.Name = "Id";
            this.Id.OptionsColumn.AllowEdit = false;
            this.Id.OptionsColumn.AllowFocus = false;
            this.Id.OptionsColumn.ReadOnly = true;
            // 
            // colGZone
            // 
            this.colGZone.AppearanceHeader.Options.UseTextOptions = true;
            this.colGZone.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colGZone.Caption = "������ ����������� ���";
            this.colGZone.FieldName = "Title";
            this.colGZone.Name = "colGZone";
            this.colGZone.OptionsColumn.AllowEdit = false;
            this.colGZone.OptionsColumn.AllowFocus = false;
            this.colGZone.OptionsColumn.ReadOnly = true;
            this.colGZone.Visible = true;
            this.colGZone.VisibleIndex = 0;
            // 
            // Zone_ID
            // 
            this.Zone_ID.AppearanceHeader.Options.UseTextOptions = true;
            this.Zone_ID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Zone_ID.Caption = "Zone_ID";
            this.Zone_ID.FieldName = "Zone_ID";
            this.Zone_ID.Name = "Zone_ID";
            this.Zone_ID.OptionsColumn.AllowEdit = false;
            this.Zone_ID.OptionsColumn.AllowFocus = false;
            this.Zone_ID.OptionsColumn.ReadOnly = true;
            // 
            // colZoneName
            // 
            this.colZoneName.AppearanceHeader.Options.UseTextOptions = true;
            this.colZoneName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colZoneName.Caption = "����������� ����";
            this.colZoneName.FieldName = "Name";
            this.colZoneName.Name = "colZoneName";
            this.colZoneName.OptionsColumn.AllowEdit = false;
            this.colZoneName.OptionsColumn.AllowFocus = false;
            this.colZoneName.OptionsColumn.ReadOnly = true;
            this.colZoneName.Visible = true;
            this.colZoneName.VisibleIndex = 1;
            // 
            // wpSelectType
            // 
            this.wpSelectType.Controls.Add(this.peSpeedAgro);
            this.wpSelectType.Controls.Add(this.peVehUnload);
            this.wpSelectType.Controls.Add(this.peFuelDischarge);
            this.wpSelectType.Controls.Add(this.peSensorFlowType);
            this.wpSelectType.Controls.Add(this.peLogicSensor);
            this.wpSelectType.Controls.Add(this.peLossGPS);
            this.wpSelectType.Controls.Add(this.pePower);
            this.wpSelectType.Controls.Add(this.peStop);
            this.wpSelectType.Controls.Add(this.peLossData);
            this.wpSelectType.Controls.Add(this.peSensor);
            this.wpSelectType.Controls.Add(this.peSpeed);
            this.wpSelectType.Controls.Add(this.peCH);
            this.wpSelectType.Controls.Add(this.grSelectType);
            this.wpSelectType.Name = "wpSelectType";
            this.wpSelectType.Size = new System.Drawing.Size(656, 329);
            this.wpSelectType.Text = "�������� ��� ��������";
            // 
            // peSpeedAgro
            // 
            this.peSpeedAgro.Location = new System.Drawing.Point(32, 290);
            this.peSpeedAgro.Name = "peSpeedAgro";
            this.peSpeedAgro.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.peSpeedAgro.Size = new System.Drawing.Size(34, 25);
            this.peSpeedAgro.TabIndex = 16;
            // 
            // peVehUnload
            // 
            this.peVehUnload.Location = new System.Drawing.Point(32, 265);
            this.peVehUnload.Name = "peVehUnload";
            this.peVehUnload.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.peVehUnload.Size = new System.Drawing.Size(34, 25);
            this.peVehUnload.TabIndex = 15;
            // 
            // peFuelDischarge
            // 
            this.peFuelDischarge.Location = new System.Drawing.Point(32, 240);
            this.peFuelDischarge.Name = "peFuelDischarge";
            this.peFuelDischarge.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.peFuelDischarge.Size = new System.Drawing.Size(34, 25);
            this.peFuelDischarge.TabIndex = 14;
            // 
            // peSensorFlowType
            // 
            this.peSensorFlowType.Location = new System.Drawing.Point(32, 215);
            this.peSensorFlowType.Name = "peSensorFlowType";
            this.peSensorFlowType.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.peSensorFlowType.Size = new System.Drawing.Size(34, 25);
            this.peSensorFlowType.TabIndex = 13;
            // 
            // peLogicSensor
            // 
            this.peLogicSensor.Location = new System.Drawing.Point(32, 90);
            this.peLogicSensor.Name = "peLogicSensor";
            this.peLogicSensor.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.peLogicSensor.Size = new System.Drawing.Size(34, 25);
            this.peLogicSensor.TabIndex = 12;
            // 
            // peLossGPS
            // 
            this.peLossGPS.Location = new System.Drawing.Point(32, 165);
            this.peLossGPS.Name = "peLossGPS";
            this.peLossGPS.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.peLossGPS.Size = new System.Drawing.Size(34, 25);
            this.peLossGPS.TabIndex = 11;
            // 
            // pePower
            // 
            this.pePower.Location = new System.Drawing.Point(32, 190);
            this.pePower.Name = "pePower";
            this.pePower.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pePower.Size = new System.Drawing.Size(34, 25);
            this.pePower.TabIndex = 10;
            // 
            // peStop
            // 
            this.peStop.Location = new System.Drawing.Point(32, 65);
            this.peStop.Name = "peStop";
            this.peStop.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.peStop.Size = new System.Drawing.Size(34, 25);
            this.peStop.TabIndex = 9;
            // 
            // peLossData
            // 
            this.peLossData.Location = new System.Drawing.Point(32, 140);
            this.peLossData.Name = "peLossData";
            this.peLossData.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.peLossData.Size = new System.Drawing.Size(34, 25);
            this.peLossData.TabIndex = 8;
            // 
            // peSensor
            // 
            this.peSensor.Location = new System.Drawing.Point(32, 115);
            this.peSensor.Name = "peSensor";
            this.peSensor.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.peSensor.Size = new System.Drawing.Size(34, 25);
            this.peSensor.TabIndex = 7;
            // 
            // peSpeed
            // 
            this.peSpeed.Location = new System.Drawing.Point(32, 40);
            this.peSpeed.Name = "peSpeed";
            this.peSpeed.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.peSpeed.Size = new System.Drawing.Size(34, 25);
            this.peSpeed.TabIndex = 6;
            // 
            // peCH
            // 
            this.peCH.Location = new System.Drawing.Point(32, 15);
            this.peCH.Name = "peCH";
            this.peCH.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.peCH.Size = new System.Drawing.Size(34, 25);
            this.peCH.TabIndex = 5;
            // 
            // grSelectType
            // 
            this.grSelectType.EditValue = ((short)(0));
            this.grSelectType.Location = new System.Drawing.Point(72, 13);
            this.grSelectType.Name = "grSelectType";
            this.grSelectType.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(((short)(0)), "����������� ����"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(((short)(1)), "�������� ��������"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(((short)(4)), "�������� ���������"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(((short)(7)), "�������� ���������� ��������"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(((short)(2)), "�������� ������ �������� "),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(((short)(3)), "���������� ������"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(((short)(6)), "������ GPS "),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(((short)(5)), "��������/��������� �������"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(((short)(9)), "��������� �������� ������� �������/ �����"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(((short)(10)), "���� �������"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(((short)(11)), "��������� ����������"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(((short)(12)), "�������� �������� ����")});
            this.grSelectType.Size = new System.Drawing.Size(338, 310);
            this.grSelectType.TabIndex = 4;
            this.grSelectType.SelectedIndexChanged += new System.EventHandler(this.grSelectType_SelectedIndexChanged);
            // 
            // wpSpeed
            // 
            this.wpSpeed.Controls.Add(this.teSpeedMax);
            this.wpSpeed.Controls.Add(this.lbSpeedMax);
            this.wpSpeed.Controls.Add(this.teSpeedMin);
            this.wpSpeed.Controls.Add(this.lbSpeedMin);
            this.wpSpeed.Name = "wpSpeed";
            this.wpSpeed.Size = new System.Drawing.Size(656, 329);
            this.wpSpeed.Text = "�������� ��������";
            this.wpSpeed.PageValidating += new DevExpress.XtraWizard.WizardPageValidatingEventHandler(this.wpSpeed_PageValidating);
            // 
            // teSpeedMax
            // 
            this.teSpeedMax.EditValue = "100";
            this.teSpeedMax.Location = new System.Drawing.Point(202, 53);
            this.teSpeedMax.Name = "teSpeedMax";
            this.teSpeedMax.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.teSpeedMax.Properties.Appearance.Options.UseFont = true;
            this.teSpeedMax.Size = new System.Drawing.Size(102, 20);
            this.teSpeedMax.TabIndex = 3;
            this.teSpeedMax.TabStop = false;
            this.teSpeedMax.EditValueChanged += new System.EventHandler(this.teSpeedMax_EditValueChanged);
            // 
            // lbSpeedMax
            // 
            this.lbSpeedMax.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.lbSpeedMax.Location = new System.Drawing.Point(57, 56);
            this.lbSpeedMax.Name = "lbSpeedMax";
            this.lbSpeedMax.Size = new System.Drawing.Size(108, 14);
            this.lbSpeedMax.TabIndex = 2;
            this.lbSpeedMax.Text = "�� ����� , ��/���:";
            // 
            // teSpeedMin
            // 
            this.teSpeedMin.AllowDrop = true;
            this.teSpeedMin.EditValue = "0";
            this.teSpeedMin.Location = new System.Drawing.Point(202, 26);
            this.teSpeedMin.Name = "teSpeedMin";
            this.teSpeedMin.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.teSpeedMin.Properties.Appearance.Options.UseFont = true;
            this.teSpeedMin.Size = new System.Drawing.Size(102, 20);
            this.teSpeedMin.TabIndex = 1;
            this.teSpeedMin.EditValueChanged += new System.EventHandler(this.teSpeedMin_EditValueChanged);
            // 
            // lbSpeedMin
            // 
            this.lbSpeedMin.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.lbSpeedMin.Location = new System.Drawing.Point(57, 29);
            this.lbSpeedMin.Name = "lbSpeedMin";
            this.lbSpeedMin.Size = new System.Drawing.Size(109, 14);
            this.lbSpeedMin.TabIndex = 0;
            this.lbSpeedMin.Text = "�� ����� , ��/���:";
            // 
            // wpStop
            // 
            this.wpStop.Controls.Add(this.teStopTime);
            this.wpStop.Controls.Add(this.lbStopTime);
            this.wpStop.Name = "wpStop";
            this.wpStop.Size = new System.Drawing.Size(656, 329);
            this.wpStop.Text = "������� ������������� ��������";
            this.wpStop.PageValidating += new DevExpress.XtraWizard.WizardPageValidatingEventHandler(this.wpStop_PageValidating);
            // 
            // teStopTime
            // 
            this.teStopTime.AllowDrop = true;
            this.teStopTime.EditValue = "10";
            this.teStopTime.Location = new System.Drawing.Point(220, 26);
            this.teStopTime.Name = "teStopTime";
            this.teStopTime.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.teStopTime.Properties.Appearance.Options.UseFont = true;
            this.teStopTime.Size = new System.Drawing.Size(102, 20);
            this.teStopTime.TabIndex = 5;
            this.teStopTime.EditValueChanged += new System.EventHandler(this.teStopTime_EditValueChanged);
            // 
            // lbStopTime
            // 
            this.lbStopTime.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.lbStopTime.Location = new System.Drawing.Point(17, 29);
            this.lbStopTime.Name = "lbStopTime";
            this.lbStopTime.Size = new System.Drawing.Size(197, 14);
            this.lbStopTime.TabIndex = 4;
            this.lbStopTime.Text = "���������� ����� ������� , ���:";
            // 
            // wpLossData
            // 
            this.wpLossData.Controls.Add(this.teLossData);
            this.wpLossData.Controls.Add(this.lbLossData);
            this.wpLossData.DescriptionText = "���������� ������";
            this.wpLossData.Name = "wpLossData";
            this.wpLossData.Size = new System.Drawing.Size(656, 329);
            this.wpLossData.Text = "���������� ������";
            this.wpLossData.PageValidating += new DevExpress.XtraWizard.WizardPageValidatingEventHandler(this.wpLossData_PageValidating);
            // 
            // teLossData
            // 
            this.teLossData.AllowDrop = true;
            this.teLossData.EditValue = "30";
            this.teLossData.Location = new System.Drawing.Point(256, 33);
            this.teLossData.Name = "teLossData";
            this.teLossData.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.teLossData.Properties.Appearance.Options.UseFont = true;
            this.teLossData.Size = new System.Drawing.Size(93, 20);
            this.teLossData.TabIndex = 7;
            this.teLossData.EditValueChanged += new System.EventHandler(this.teLossData_EditValueChanged);
            // 
            // lbLossData
            // 
            this.lbLossData.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.lbLossData.Location = new System.Drawing.Point(34, 36);
            this.lbLossData.Name = "lbLossData";
            this.lbLossData.Size = new System.Drawing.Size(186, 14);
            this.lbLossData.TabIndex = 6;
            this.lbLossData.Text = "����� ���������� ������ , ���:";
            // 
            // wpLossGPS
            // 
            this.wpLossGPS.Controls.Add(this.teLossGPS);
            this.wpLossGPS.Controls.Add(this.lbLossGPS);
            this.wpLossGPS.Name = "wpLossGPS";
            this.wpLossGPS.Size = new System.Drawing.Size(656, 329);
            this.wpLossGPS.Text = "������  GPS";
            this.wpLossGPS.PageValidating += new DevExpress.XtraWizard.WizardPageValidatingEventHandler(this.wpLossGPS_PageValidating);
            // 
            // teLossGPS
            // 
            this.teLossGPS.AllowDrop = true;
            this.teLossGPS.EditValue = "30";
            this.teLossGPS.Location = new System.Drawing.Point(212, 29);
            this.teLossGPS.Name = "teLossGPS";
            this.teLossGPS.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.teLossGPS.Properties.Appearance.Options.UseFont = true;
            this.teLossGPS.Size = new System.Drawing.Size(93, 20);
            this.teLossGPS.TabIndex = 9;
            this.teLossGPS.EditValueChanged += new System.EventHandler(this.teLossGPS_EditValueChanged);
            // 
            // lbLossGPS
            // 
            this.lbLossGPS.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.lbLossGPS.Location = new System.Drawing.Point(39, 32);
            this.lbLossGPS.Name = "lbLossGPS";
            this.lbLossGPS.Size = new System.Drawing.Size(167, 14);
            this.lbLossGPS.TabIndex = 8;
            this.lbLossGPS.Text = "����� ���������� GPS , ���:";
            // 
            // wpVehicleLogicSensors
            // 
            this.wpVehicleLogicSensors.Controls.Add(this.gcLogicSensors);
            this.wpVehicleLogicSensors.Name = "wpVehicleLogicSensors";
            this.wpVehicleLogicSensors.Size = new System.Drawing.Size(656, 329);
            this.wpVehicleLogicSensors.Text = "�������� ���������� �������";
            this.wpVehicleLogicSensors.PageValidating += new DevExpress.XtraWizard.WizardPageValidatingEventHandler(this.wpVehicleLogicSensors_PageValidating);
            // 
            // gcLogicSensors
            // 
            this.gcLogicSensors.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcLogicSensors.Location = new System.Drawing.Point(0, 0);
            this.gcLogicSensors.MainView = this.gvLogicSensors;
            this.gcLogicSensors.Name = "gcLogicSensors";
            this.gcLogicSensors.Size = new System.Drawing.Size(656, 329);
            this.gcLogicSensors.TabIndex = 3;
            this.gcLogicSensors.TabStop = false;
            this.gcLogicSensors.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvLogicSensors});
            // 
            // gvLogicSensors
            // 
            this.gvLogicSensors.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.colGNameLSensor,
            this.gridColumn3,
            this.colMakeCarLSensor,
            this.colCarModelLSensor,
            this.colNumberPlateLSensor,
            this.gridColumn7,
            this.colLSensor});
            this.gvLogicSensors.GridControl = this.gcLogicSensors;
            this.gvLogicSensors.Name = "gvLogicSensors";
            this.gvLogicSensors.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gvLogicSensors.OptionsSelection.MultiSelect = true;
            this.gvLogicSensors.Click += new System.EventHandler(this.gvLogicSensors_Click);
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "TID";
            this.gridColumn1.FieldName = "TID";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.OptionsColumn.AllowEdit = false;
            this.gridColumn1.OptionsColumn.AllowFocus = false;
            this.gridColumn1.OptionsColumn.ReadOnly = true;
            // 
            // colGNameLSensor
            // 
            this.colGNameLSensor.AppearanceHeader.Options.UseTextOptions = true;
            this.colGNameLSensor.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colGNameLSensor.Caption = "������";
            this.colGNameLSensor.FieldName = "GName";
            this.colGNameLSensor.Name = "colGNameLSensor";
            this.colGNameLSensor.OptionsColumn.AllowEdit = false;
            this.colGNameLSensor.OptionsColumn.AllowFocus = false;
            this.colGNameLSensor.OptionsColumn.ReadOnly = true;
            this.colGNameLSensor.Visible = true;
            this.colGNameLSensor.VisibleIndex = 0;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "VID";
            this.gridColumn3.FieldName = "VID";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.AllowEdit = false;
            this.gridColumn3.OptionsColumn.AllowFocus = false;
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            // 
            // colMakeCarLSensor
            // 
            this.colMakeCarLSensor.AppearanceHeader.Options.UseTextOptions = true;
            this.colMakeCarLSensor.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMakeCarLSensor.Caption = "����� ";
            this.colMakeCarLSensor.FieldName = "MakeCar";
            this.colMakeCarLSensor.Name = "colMakeCarLSensor";
            this.colMakeCarLSensor.OptionsColumn.AllowEdit = false;
            this.colMakeCarLSensor.OptionsColumn.AllowFocus = false;
            this.colMakeCarLSensor.OptionsColumn.ReadOnly = true;
            this.colMakeCarLSensor.Visible = true;
            this.colMakeCarLSensor.VisibleIndex = 1;
            // 
            // colCarModelLSensor
            // 
            this.colCarModelLSensor.AppearanceHeader.Options.UseTextOptions = true;
            this.colCarModelLSensor.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCarModelLSensor.Caption = "������";
            this.colCarModelLSensor.FieldName = "CarModel";
            this.colCarModelLSensor.Name = "colCarModelLSensor";
            this.colCarModelLSensor.OptionsColumn.AllowEdit = false;
            this.colCarModelLSensor.OptionsColumn.AllowFocus = false;
            this.colCarModelLSensor.OptionsColumn.ReadOnly = true;
            this.colCarModelLSensor.Visible = true;
            this.colCarModelLSensor.VisibleIndex = 2;
            // 
            // colNumberPlateLSensor
            // 
            this.colNumberPlateLSensor.AppearanceHeader.Options.UseTextOptions = true;
            this.colNumberPlateLSensor.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNumberPlateLSensor.Caption = "�����";
            this.colNumberPlateLSensor.FieldName = "NumberPlate";
            this.colNumberPlateLSensor.Name = "colNumberPlateLSensor";
            this.colNumberPlateLSensor.OptionsColumn.AllowEdit = false;
            this.colNumberPlateLSensor.OptionsColumn.AllowFocus = false;
            this.colNumberPlateLSensor.OptionsColumn.ReadOnly = true;
            this.colNumberPlateLSensor.Visible = true;
            this.colNumberPlateLSensor.VisibleIndex = 3;
            // 
            // gridColumn7
            // 
            this.gridColumn7.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn7.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn7.Caption = "SID";
            this.gridColumn7.FieldName = "SID";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsColumn.AllowEdit = false;
            this.gridColumn7.OptionsColumn.AllowFocus = false;
            this.gridColumn7.OptionsColumn.ReadOnly = true;
            // 
            // colLSensor
            // 
            this.colLSensor.AppearanceHeader.Options.UseTextOptions = true;
            this.colLSensor.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLSensor.Caption = "������";
            this.colLSensor.FieldName = "SName";
            this.colLSensor.Name = "colLSensor";
            this.colLSensor.OptionsColumn.AllowEdit = false;
            this.colLSensor.OptionsColumn.ReadOnly = true;
            this.colLSensor.Visible = true;
            this.colLSensor.VisibleIndex = 4;
            // 
            // wpVehicleSensors
            // 
            this.wpVehicleSensors.Controls.Add(this.gcSensors);
            this.wpVehicleSensors.Name = "wpVehicleSensors";
            this.wpVehicleSensors.Size = new System.Drawing.Size(656, 329);
            this.wpVehicleSensors.Text = "�������� �������";
            this.wpVehicleSensors.PageValidating += new DevExpress.XtraWizard.WizardPageValidatingEventHandler(this.wpVehicleSensors_PageValidating);
            // 
            // gcSensors
            // 
            this.gcSensors.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcSensors.Location = new System.Drawing.Point(0, 0);
            this.gcSensors.MainView = this.gvSensors;
            this.gcSensors.Name = "gcSensors";
            this.gcSensors.Size = new System.Drawing.Size(656, 329);
            this.gcSensors.TabIndex = 4;
            this.gcSensors.TabStop = false;
            this.gcSensors.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvSensors});
            // 
            // gvSensors
            // 
            this.gvSensors.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn9,
            this.colGNameSensor,
            this.gridColumn11,
            this.colMakeCarSensor,
            this.colCarModelSensor,
            this.colNumberPlateSensor,
            this.gridColumn15,
            this.colSensor,
            this.colAlgoritm});
            this.gvSensors.GridControl = this.gcSensors;
            this.gvSensors.Name = "gvSensors";
            this.gvSensors.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gvSensors.OptionsSelection.EnableAppearanceHideSelection = false;
            this.gvSensors.OptionsSelection.MultiSelect = true;
            this.gvSensors.Click += new System.EventHandler(this.gvSensors_Click);
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "TID";
            this.gridColumn9.FieldName = "TID";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.AllowEdit = false;
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            // 
            // colGNameSensor
            // 
            this.colGNameSensor.AppearanceHeader.Options.UseTextOptions = true;
            this.colGNameSensor.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colGNameSensor.Caption = "������";
            this.colGNameSensor.FieldName = "GName";
            this.colGNameSensor.Name = "colGNameSensor";
            this.colGNameSensor.OptionsColumn.AllowEdit = false;
            this.colGNameSensor.OptionsColumn.ReadOnly = true;
            this.colGNameSensor.Visible = true;
            this.colGNameSensor.VisibleIndex = 0;
            // 
            // gridColumn11
            // 
            this.gridColumn11.Caption = "VID";
            this.gridColumn11.FieldName = "VID";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsColumn.AllowEdit = false;
            this.gridColumn11.OptionsColumn.ReadOnly = true;
            // 
            // colMakeCarSensor
            // 
            this.colMakeCarSensor.AppearanceHeader.Options.UseTextOptions = true;
            this.colMakeCarSensor.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMakeCarSensor.Caption = "����� ";
            this.colMakeCarSensor.FieldName = "MakeCar";
            this.colMakeCarSensor.Name = "colMakeCarSensor";
            this.colMakeCarSensor.OptionsColumn.AllowEdit = false;
            this.colMakeCarSensor.OptionsColumn.ReadOnly = true;
            this.colMakeCarSensor.Visible = true;
            this.colMakeCarSensor.VisibleIndex = 1;
            // 
            // colCarModelSensor
            // 
            this.colCarModelSensor.AppearanceHeader.Options.UseTextOptions = true;
            this.colCarModelSensor.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCarModelSensor.Caption = "������";
            this.colCarModelSensor.FieldName = "CarModel";
            this.colCarModelSensor.Name = "colCarModelSensor";
            this.colCarModelSensor.OptionsColumn.AllowEdit = false;
            this.colCarModelSensor.OptionsColumn.ReadOnly = true;
            this.colCarModelSensor.Visible = true;
            this.colCarModelSensor.VisibleIndex = 2;
            // 
            // colNumberPlateSensor
            // 
            this.colNumberPlateSensor.AppearanceHeader.Options.UseTextOptions = true;
            this.colNumberPlateSensor.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNumberPlateSensor.Caption = "�����";
            this.colNumberPlateSensor.FieldName = "NumberPlate";
            this.colNumberPlateSensor.Name = "colNumberPlateSensor";
            this.colNumberPlateSensor.OptionsColumn.AllowEdit = false;
            this.colNumberPlateSensor.OptionsColumn.ReadOnly = true;
            this.colNumberPlateSensor.Visible = true;
            this.colNumberPlateSensor.VisibleIndex = 3;
            // 
            // gridColumn15
            // 
            this.gridColumn15.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColumn15.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColumn15.Caption = "SID";
            this.gridColumn15.FieldName = "SID";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.OptionsColumn.AllowEdit = false;
            this.gridColumn15.OptionsColumn.ReadOnly = true;
            // 
            // colSensor
            // 
            this.colSensor.AppearanceHeader.Options.UseTextOptions = true;
            this.colSensor.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSensor.Caption = "������";
            this.colSensor.FieldName = "SName";
            this.colSensor.Name = "colSensor";
            this.colSensor.OptionsColumn.AllowEdit = false;
            this.colSensor.OptionsColumn.ReadOnly = true;
            this.colSensor.Visible = true;
            this.colSensor.VisibleIndex = 4;
            // 
            // colAlgoritm
            // 
            this.colAlgoritm.AppearanceHeader.Options.UseTextOptions = true;
            this.colAlgoritm.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colAlgoritm.Caption = "��������";
            this.colAlgoritm.FieldName = "AName";
            this.colAlgoritm.Name = "colAlgoritm";
            this.colAlgoritm.OptionsColumn.AllowEdit = false;
            this.colAlgoritm.OptionsColumn.ReadOnly = true;
            this.colAlgoritm.Visible = true;
            this.colAlgoritm.VisibleIndex = 5;
            // 
            // wpSensor
            // 
            this.wpSensor.Controls.Add(this.chSensorZones);
            this.wpSensor.Controls.Add(this.rgSensorRange);
            this.wpSensor.Controls.Add(this.teValueTop);
            this.wpSensor.Controls.Add(this.lbTopLevel);
            this.wpSensor.Controls.Add(this.teValueBottom);
            this.wpSensor.Controls.Add(this.lbLowLevel);
            this.wpSensor.Name = "wpSensor";
            this.wpSensor.Size = new System.Drawing.Size(656, 329);
            this.wpSensor.Text = "�������� �������� ��������";
            this.wpSensor.PageValidating += new DevExpress.XtraWizard.WizardPageValidatingEventHandler(this.wpSensor_PageValidating);
            // 
            // chSensorZones
            // 
            this.chSensorZones.Location = new System.Drawing.Point(75, 184);
            this.chSensorZones.Name = "chSensorZones";
            this.chSensorZones.Properties.Caption = "�������� � ����������� �����";
            this.chSensorZones.Size = new System.Drawing.Size(203, 19);
            this.chSensorZones.TabIndex = 9;
            // 
            // rgSensorRange
            // 
            this.rgSensorRange.EditValue = true;
            this.rgSensorRange.Location = new System.Drawing.Point(77, 105);
            this.rgSensorRange.Name = "rgSensorRange";
            this.rgSensorRange.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(true, "������������ � �������� ���������"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(false, "������������ �� ��������� ���������")});
            this.rgSensorRange.Size = new System.Drawing.Size(241, 64);
            this.rgSensorRange.TabIndex = 8;
            this.rgSensorRange.SelectedIndexChanged += new System.EventHandler(this.rgSensorRange_SelectedIndexChanged);
            // 
            // teValueTop
            // 
            this.teValueTop.EditValue = "100";
            this.teValueTop.Location = new System.Drawing.Point(199, 62);
            this.teValueTop.Name = "teValueTop";
            this.teValueTop.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.teValueTop.Properties.Appearance.Options.UseFont = true;
            this.teValueTop.Size = new System.Drawing.Size(102, 20);
            this.teValueTop.TabIndex = 7;
            this.teValueTop.TabStop = false;
            this.teValueTop.EditValueChanged += new System.EventHandler(this.teValueTop_EditValueChanged);
            // 
            // lbTopLevel
            // 
            this.lbTopLevel.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.lbTopLevel.Location = new System.Drawing.Point(77, 65);
            this.lbTopLevel.Name = "lbTopLevel";
            this.lbTopLevel.Size = new System.Drawing.Size(98, 14);
            this.lbTopLevel.TabIndex = 6;
            this.lbTopLevel.Text = "������� ������:";
            // 
            // teValueBottom
            // 
            this.teValueBottom.AllowDrop = true;
            this.teValueBottom.EditValue = "0";
            this.teValueBottom.Location = new System.Drawing.Point(199, 35);
            this.teValueBottom.Name = "teValueBottom";
            this.teValueBottom.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.teValueBottom.Properties.Appearance.Options.UseFont = true;
            this.teValueBottom.Size = new System.Drawing.Size(102, 20);
            this.teValueBottom.TabIndex = 5;
            this.teValueBottom.EditValueChanged += new System.EventHandler(this.teValueBottom_EditValueChanged);
            // 
            // lbLowLevel
            // 
            this.lbLowLevel.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.lbLowLevel.Location = new System.Drawing.Point(77, 38);
            this.lbLowLevel.Name = "lbLowLevel";
            this.lbLowLevel.Size = new System.Drawing.Size(96, 14);
            this.lbLowLevel.TabIndex = 4;
            this.lbLowLevel.Text = "������ ������:";
            // 
            // wpLogicSensor
            // 
            this.wpLogicSensor.Controls.Add(this.chLogicSensorZones);
            this.wpLogicSensor.Controls.Add(this.rgLogicSensors);
            this.wpLogicSensor.Name = "wpLogicSensor";
            this.wpLogicSensor.Size = new System.Drawing.Size(656, 329);
            this.wpLogicSensor.Text = "�������� �������� ���������� ��������";
            this.wpLogicSensor.PageValidating += new DevExpress.XtraWizard.WizardPageValidatingEventHandler(this.wpLogicSensor_PageValidating);
            // 
            // chLogicSensorZones
            // 
            this.chLogicSensorZones.Location = new System.Drawing.Point(62, 120);
            this.chLogicSensorZones.Name = "chLogicSensorZones";
            this.chLogicSensorZones.Properties.Caption = "�������� � ����������� �����";
            this.chLogicSensorZones.Size = new System.Drawing.Size(203, 19);
            this.chLogicSensorZones.TabIndex = 1;
            // 
            // rgLogicSensors
            // 
            this.rgLogicSensors.EditValue = 0;
            this.rgLogicSensors.Location = new System.Drawing.Point(64, 30);
            this.rgLogicSensors.Name = "rgLogicSensors";
            this.rgLogicSensors.Properties.Items.AddRange(new DevExpress.XtraEditors.Controls.RadioGroupItem[] {
            new DevExpress.XtraEditors.Controls.RadioGroupItem(0, "�������� ������� 1 -> 0"),
            new DevExpress.XtraEditors.Controls.RadioGroupItem(1, "�������� ������� 0 -> 1")});
            this.rgLogicSensors.Size = new System.Drawing.Size(201, 75);
            this.rgLogicSensors.TabIndex = 0;
            this.rgLogicSensors.SelectedIndexChanged += new System.EventHandler(this.rgLogicSensors_SelectedIndexChanged);
            // 
            // wpPower
            // 
            this.wpPower.Controls.Add(this.tePower);
            this.wpPower.Controls.Add(this.lbPower);
            this.wpPower.Name = "wpPower";
            this.wpPower.Size = new System.Drawing.Size(656, 329);
            this.wpPower.Text = "��������/��������� �������";
            this.wpPower.PageValidating += new DevExpress.XtraWizard.WizardPageValidatingEventHandler(this.wpLossPower_PageValidating);
            // 
            // tePower
            // 
            this.tePower.AllowDrop = true;
            this.tePower.EditValue = "10";
            this.tePower.Location = new System.Drawing.Point(318, 30);
            this.tePower.Name = "tePower";
            this.tePower.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.tePower.Properties.Appearance.Options.UseFont = true;
            this.tePower.Size = new System.Drawing.Size(69, 20);
            this.tePower.TabIndex = 7;
            this.tePower.EditValueChanged += new System.EventHandler(this.teLossPower_EditValueChanged);
            // 
            // lbPower
            // 
            this.lbPower.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.lbPower.Location = new System.Drawing.Point(28, 33);
            this.lbPower.Name = "lbPower";
            this.lbPower.Size = new System.Drawing.Size(284, 14);
            this.lbPower.TabIndex = 6;
            this.lbPower.Text = "����� ������ �� ��������� ������������ , ���:";
            // 
            // wpTime
            // 
            this.wpTime.Controls.Add(this.teTime);
            this.wpTime.Controls.Add(this.lbTime);
            this.wpTime.Name = "wpTime";
            this.wpTime.Size = new System.Drawing.Size(656, 329);
            this.wpTime.Text = "�������� �������";
            this.wpTime.PageValidating += new DevExpress.XtraWizard.WizardPageValidatingEventHandler(this.wpTime_PageValidating);
            // 
            // teTime
            // 
            this.teTime.EditValue = "30";
            this.teTime.Location = new System.Drawing.Point(220, 26);
            this.teTime.Name = "teTime";
            this.teTime.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.teTime.Properties.Appearance.Options.UseFont = true;
            this.teTime.Size = new System.Drawing.Size(102, 20);
            this.teTime.TabIndex = 7;
            this.teTime.EditValueChanged += new System.EventHandler(this.teTime_EditValueChanged);
            // 
            // lbTime
            // 
            this.lbTime.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.lbTime.Location = new System.Drawing.Point(26, 29);
            this.lbTime.Name = "lbTime";
            this.lbTime.Size = new System.Drawing.Size(188, 14);
            this.lbTime.TabIndex = 6;
            this.lbTime.Text = "����� ���������� ����� , ���:";
            // 
            // wpSensorFlowType
            // 
            this.wpSensorFlowType.Controls.Add(this.teFlowTypeTimeForAction);
            this.wpSensorFlowType.Controls.Add(this.labelControl4);
            this.wpSensorFlowType.Controls.Add(this.cheFlowTypeDUT);
            this.wpSensorFlowType.Controls.Add(this.teFlowTypeTimeForStop);
            this.wpSensorFlowType.Controls.Add(this.labelControl3);
            this.wpSensorFlowType.Controls.Add(this.teFlowTypeTimeForFind);
            this.wpSensorFlowType.Controls.Add(this.labelControl1);
            this.wpSensorFlowType.Controls.Add(this.teFlowTypeRadius);
            this.wpSensorFlowType.Controls.Add(this.labelControl2);
            this.wpSensorFlowType.Name = "wpSensorFlowType";
            this.wpSensorFlowType.Size = new System.Drawing.Size(656, 329);
            this.wpSensorFlowType.Text = " �������� ������� �������/�����";
            this.wpSensorFlowType.PageValidating += new DevExpress.XtraWizard.WizardPageValidatingEventHandler(this.wpSensorFlowType_PageValidating);
            // 
            // teFlowTypeTimeForAction
            // 
            this.teFlowTypeTimeForAction.EditValue = "60";
            this.teFlowTypeTimeForAction.Location = new System.Drawing.Point(451, 75);
            this.teFlowTypeTimeForAction.Name = "teFlowTypeTimeForAction";
            this.teFlowTypeTimeForAction.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.teFlowTypeTimeForAction.Properties.Appearance.Options.UseFont = true;
            this.teFlowTypeTimeForAction.Size = new System.Drawing.Size(102, 20);
            this.teFlowTypeTimeForAction.TabIndex = 12;
            this.teFlowTypeTimeForAction.TabStop = false;
            this.teFlowTypeTimeForAction.EditValueChanged += new System.EventHandler(this.teFlowTypeTimeForAction_EditValueChanged);
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.labelControl4.Location = new System.Drawing.Point(106, 78);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(327, 14);
            this.labelControl4.TabIndex = 11;
            this.labelControl4.Text = "����������� ����� ����������� ������ ������� , ���:";
            // 
            // cheFlowTypeDUT
            // 
            this.cheFlowTypeDUT.Location = new System.Drawing.Point(355, 138);
            this.cheFlowTypeDUT.Name = "cheFlowTypeDUT";
            this.cheFlowTypeDUT.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.cheFlowTypeDUT.Properties.Appearance.Options.UseFont = true;
            this.cheFlowTypeDUT.Properties.Caption = "�������� ���";
            this.cheFlowTypeDUT.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.cheFlowTypeDUT.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cheFlowTypeDUT.Size = new System.Drawing.Size(113, 19);
            this.cheFlowTypeDUT.TabIndex = 10;
            this.cheFlowTypeDUT.EditValueChanged += new System.EventHandler(this.cheFlowTypeDUT_EditValueChanged);
            // 
            // teFlowTypeTimeForStop
            // 
            this.teFlowTypeTimeForStop.EditValue = "60";
            this.teFlowTypeTimeForStop.Location = new System.Drawing.Point(451, 107);
            this.teFlowTypeTimeForStop.Name = "teFlowTypeTimeForStop";
            this.teFlowTypeTimeForStop.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.teFlowTypeTimeForStop.Properties.Appearance.Options.UseFont = true;
            this.teFlowTypeTimeForStop.Size = new System.Drawing.Size(102, 20);
            this.teFlowTypeTimeForStop.TabIndex = 9;
            this.teFlowTypeTimeForStop.TabStop = false;
            this.teFlowTypeTimeForStop.EditValueChanged += new System.EventHandler(this.teFlowTypeTimeForStop_EditValueChanged);
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.labelControl3.Location = new System.Drawing.Point(19, 108);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(417, 14);
            this.labelControl3.TabIndex = 8;
            this.labelControl3.Text = "����� ���������� ���������, ����������, ��� ������ ��������� , ���:";
            // 
            // teFlowTypeTimeForFind
            // 
            this.teFlowTypeTimeForFind.EditValue = "1";
            this.teFlowTypeTimeForFind.Location = new System.Drawing.Point(451, 43);
            this.teFlowTypeTimeForFind.Name = "teFlowTypeTimeForFind";
            this.teFlowTypeTimeForFind.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.teFlowTypeTimeForFind.Properties.Appearance.Options.UseFont = true;
            this.teFlowTypeTimeForFind.Size = new System.Drawing.Size(102, 20);
            this.teFlowTypeTimeForFind.TabIndex = 7;
            this.teFlowTypeTimeForFind.TabStop = false;
            this.teFlowTypeTimeForFind.EditValueChanged += new System.EventHandler(this.teFlowTypeTimeForFind_EditValueChanged);
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.labelControl1.Location = new System.Drawing.Point(232, 50);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(204, 14);
            this.labelControl1.TabIndex = 6;
            this.labelControl1.Text = "�������� ������ ���������� , ���:";
            // 
            // teFlowTypeRadius
            // 
            this.teFlowTypeRadius.AllowDrop = true;
            this.teFlowTypeRadius.EditValue = "10";
            this.teFlowTypeRadius.Location = new System.Drawing.Point(451, 11);
            this.teFlowTypeRadius.Name = "teFlowTypeRadius";
            this.teFlowTypeRadius.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.teFlowTypeRadius.Properties.Appearance.Options.UseFont = true;
            this.teFlowTypeRadius.Size = new System.Drawing.Size(102, 20);
            this.teFlowTypeRadius.TabIndex = 5;
            this.teFlowTypeRadius.EditValueChanged += new System.EventHandler(this.teFlowTypeRadius_EditValueChanged);
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.labelControl2.Location = new System.Drawing.Point(261, 18);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(175, 14);
            this.labelControl2.TabIndex = 4;
            this.labelControl2.Text = "������ ������ ���������� , �:";
            // 
            // wcPopupUsers
            // 
            this.wcPopupUsers.Controls.Add(this.gcPopupUsers);
            this.wcPopupUsers.Name = "wcPopupUsers";
            this.wcPopupUsers.Size = new System.Drawing.Size(656, 329);
            this.wcPopupUsers.Text = "������������, ������� ��������� �������� ��������� ���������";
            this.wcPopupUsers.PageValidating += new DevExpress.XtraWizard.WizardPageValidatingEventHandler(this.wcPopupUsers_PageValidating);
            // 
            // gcPopupUsers
            // 
            this.gcPopupUsers.Location = new System.Drawing.Point(3, 3);
            this.gcPopupUsers.MainView = this.gvPopupUsers;
            this.gcPopupUsers.Name = "gcPopupUsers";
            this.gcPopupUsers.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.rleUsers,
            this.rchActive,
            this.rlePUsers});
            this.gcPopupUsers.Size = new System.Drawing.Size(650, 257);
            this.gcPopupUsers.TabIndex = 0;
            this.gcPopupUsers.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvPopupUsers});
            // 
            // gvPopupUsers
            // 
            this.gvPopupUsers.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colUId,
            this.colUName});
            this.gvPopupUsers.GridControl = this.gcPopupUsers;
            this.gvPopupUsers.Name = "gvPopupUsers";
            this.gvPopupUsers.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gvPopupUsers.OptionsSelection.EnableAppearanceFocusedRow = false;
            this.gvPopupUsers.OptionsSelection.EnableAppearanceHideSelection = false;
            this.gvPopupUsers.OptionsSelection.MultiSelect = true;
            this.gvPopupUsers.OptionsSelection.UseIndicatorForSelection = false;
            this.gvPopupUsers.OptionsView.ShowGroupPanel = false;
            this.gvPopupUsers.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gvPopupUsers_FocusedRowChanged);
            this.gvPopupUsers.Click += new System.EventHandler(this.gvPopupUsers_Click);
            // 
            // colUId
            // 
            this.colUId.Caption = "Id";
            this.colUId.FieldName = "Id";
            this.colUId.Name = "colUId";
            // 
            // colUName
            // 
            this.colUName.AppearanceHeader.Options.UseTextOptions = true;
            this.colUName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colUName.Caption = "��������";
            this.colUName.FieldName = "Name";
            this.colUName.Name = "colUName";
            this.colUName.Visible = true;
            this.colUName.VisibleIndex = 0;
            this.colUName.Width = 1091;
            // 
            // rleUsers
            // 
            this.rleUsers.AutoHeight = false;
            this.rleUsers.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rleUsers.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "��������")});
            this.rleUsers.DisplayMember = "Name";
            this.rleUsers.Name = "rleUsers";
            this.rleUsers.NullText = "";
            this.rleUsers.ValueMember = "Id";
            // 
            // rchActive
            // 
            this.rchActive.AutoHeight = false;
            this.rchActive.Caption = "Check";
            this.rchActive.Name = "rchActive";
            // 
            // rlePUsers
            // 
            this.rlePUsers.AutoHeight = false;
            this.rlePUsers.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rlePUsers.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "Name")});
            this.rlePUsers.DisplayMember = "Name";
            this.rlePUsers.Name = "rlePUsers";
            this.rlePUsers.NullText = "";
            this.rlePUsers.ValueMember = "Id";
            // 
            // wpFuelDischarge
            // 
            this.wpFuelDischarge.Controls.Add(this.teFuelAvgExpense);
            this.wpFuelDischarge.Controls.Add(this.lbFuelAvgExpense);
            this.wpFuelDischarge.Controls.Add(this.teFuelZeroPointsCount);
            this.wpFuelDischarge.Controls.Add(this.lbFuelZeroControl);
            this.wpFuelDischarge.Controls.Add(this.teFuelDischarge);
            this.wpFuelDischarge.Controls.Add(this.lbFuelDischarge);
            this.wpFuelDischarge.DescriptionText = "�������� �� ������ �������";
            this.wpFuelDischarge.Name = "wpFuelDischarge";
            this.wpFuelDischarge.Size = new System.Drawing.Size(656, 329);
            this.wpFuelDischarge.Text = "���� �������";
            this.wpFuelDischarge.PageValidating += new DevExpress.XtraWizard.WizardPageValidatingEventHandler(this.wpFuelDischarge_PageValidating);
            // 
            // teFuelAvgExpense
            // 
            this.teFuelAvgExpense.AllowDrop = true;
            this.teFuelAvgExpense.EditValue = "3";
            this.teFuelAvgExpense.Location = new System.Drawing.Point(539, 148);
            this.teFuelAvgExpense.Name = "teFuelAvgExpense";
            this.teFuelAvgExpense.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.teFuelAvgExpense.Properties.Appearance.Options.UseFont = true;
            this.teFuelAvgExpense.Size = new System.Drawing.Size(43, 20);
            this.teFuelAvgExpense.TabIndex = 7;
            this.teFuelAvgExpense.EditValueChanged += new System.EventHandler(this.teFuelAvgExpense_EditValueChanged);
            // 
            // lbFuelAvgExpense
            // 
            this.lbFuelAvgExpense.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.lbFuelAvgExpense.Location = new System.Drawing.Point(248, 151);
            this.lbFuelAvgExpense.Name = "lbFuelAvgExpense";
            this.lbFuelAvgExpense.Size = new System.Drawing.Size(285, 14);
            this.lbFuelAvgExpense.TabIndex = 6;
            this.lbFuelAvgExpense.Text = "������� ������ ������� �/� (0 - �������������)";
            // 
            // teFuelZeroPointsCount
            // 
            this.teFuelZeroPointsCount.AllowDrop = true;
            this.teFuelZeroPointsCount.EditValue = "3";
            this.teFuelZeroPointsCount.Location = new System.Drawing.Point(539, 124);
            this.teFuelZeroPointsCount.Name = "teFuelZeroPointsCount";
            this.teFuelZeroPointsCount.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.teFuelZeroPointsCount.Properties.Appearance.Options.UseFont = true;
            this.teFuelZeroPointsCount.Size = new System.Drawing.Size(43, 20);
            this.teFuelZeroPointsCount.TabIndex = 5;
            this.teFuelZeroPointsCount.EditValueChanged += new System.EventHandler(this.teFuelZeroPointsCount_EditValueChanged);
            // 
            // lbFuelZeroControl
            // 
            this.lbFuelZeroControl.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.lbFuelZeroControl.Location = new System.Drawing.Point(15, 127);
            this.lbFuelZeroControl.Name = "lbFuelZeroControl";
            this.lbFuelZeroControl.Size = new System.Drawing.Size(518, 14);
            this.lbFuelZeroControl.TabIndex = 4;
            this.lbFuelZeroControl.Text = "���������� ������� �������� ��� ����������� ���������� ������� (0 - �������������" +
    ")";
            // 
            // teFuelDischarge
            // 
            this.teFuelDischarge.AllowDrop = true;
            this.teFuelDischarge.EditValue = "1";
            this.teFuelDischarge.Location = new System.Drawing.Point(539, 100);
            this.teFuelDischarge.Name = "teFuelDischarge";
            this.teFuelDischarge.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.teFuelDischarge.Properties.Appearance.Options.UseFont = true;
            this.teFuelDischarge.Size = new System.Drawing.Size(43, 20);
            this.teFuelDischarge.TabIndex = 3;
            this.teFuelDischarge.EditValueChanged += new System.EventHandler(this.teFuelDischarge_EditValueChanged);
            // 
            // lbFuelDischarge
            // 
            this.lbFuelDischarge.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.lbFuelDischarge.Location = new System.Drawing.Point(444, 103);
            this.lbFuelDischarge.Name = "lbFuelDischarge";
            this.lbFuelDischarge.Size = new System.Drawing.Size(89, 14);
            this.lbFuelDischarge.TabIndex = 2;
            this.lbFuelDischarge.Text = "����� ����� , �";
            // 
            // wpVehicleUnload
            // 
            this.wpVehicleUnload.Controls.Add(this.lbTimeMaxUnload);
            this.wpVehicleUnload.Controls.Add(this.teTimeMaxUnload);
            this.wpVehicleUnload.DescriptionText = "��������� ����������� �� ��������� �������";
            this.wpVehicleUnload.Name = "wpVehicleUnload";
            this.wpVehicleUnload.Size = new System.Drawing.Size(656, 329);
            this.wpVehicleUnload.Text = "��������� ����������";
            this.wpVehicleUnload.PageValidating += new DevExpress.XtraWizard.WizardPageValidatingEventHandler(this.wpVehicleUnload_PageValidating);
            // 
            // lbTimeMaxUnload
            // 
            this.lbTimeMaxUnload.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.lbTimeMaxUnload.Location = new System.Drawing.Point(15, 28);
            this.lbTimeMaxUnload.Name = "lbTimeMaxUnload";
            this.lbTimeMaxUnload.Size = new System.Drawing.Size(229, 14);
            this.lbTimeMaxUnload.TabIndex = 1;
            this.lbTimeMaxUnload.Text = "������������ ����� ���������, ������";
            // 
            // teTimeMaxUnload
            // 
            this.teTimeMaxUnload.EditValue = "0";
            this.teTimeMaxUnload.Location = new System.Drawing.Point(250, 26);
            this.teTimeMaxUnload.Name = "teTimeMaxUnload";
            this.teTimeMaxUnload.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 9F);
            this.teTimeMaxUnload.Properties.Appearance.Options.UseFont = true;
            this.teTimeMaxUnload.Size = new System.Drawing.Size(76, 20);
            this.teTimeMaxUnload.TabIndex = 0;
            this.teTimeMaxUnload.EditValueChanged += new System.EventHandler(this.teTimeMaxUnload_EditValueChanged);
            // 
            // wpSpeedAgro
            // 
            this.wpSpeedAgro.Controls.Add(this.gcWorks);
            this.wpSpeedAgro.DescriptionText = "�������� ���� ����� ";
            this.wpSpeedAgro.Name = "wpSpeedAgro";
            this.wpSpeedAgro.Size = new System.Drawing.Size(656, 329);
            this.wpSpeedAgro.Text = "�������� ���� ����� ";
            this.wpSpeedAgro.PageValidating += new DevExpress.XtraWizard.WizardPageValidatingEventHandler(this.wpSpeedAgro_PageValidating);
            // 
            // gcWorks
            // 
            this.gcWorks.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcWorks.Location = new System.Drawing.Point(0, 0);
            this.gcWorks.MainView = this.gvWorks;
            this.gcWorks.Name = "gcWorks";
            this.gcWorks.Size = new System.Drawing.Size(656, 329);
            this.gcWorks.TabIndex = 3;
            this.gcWorks.TabStop = false;
            this.gcWorks.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvWorks});
            // 
            // gvWorks
            // 
            this.gvWorks.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.WID,
            this.colGroupWork,
            this.colTypeWork,
            this.colNameWork,
            this.colSpeedBottom,
            this.colSpeedTop});
            this.gvWorks.GridControl = this.gcWorks;
            this.gvWorks.Name = "gvWorks";
            this.gvWorks.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gvWorks.OptionsSelection.MultiSelect = true;
            this.gvWorks.Click += new System.EventHandler(this.gvWorks_Click);
            // 
            // WID
            // 
            this.WID.Caption = "WID";
            this.WID.FieldName = "Id";
            this.WID.Name = "WID";
            this.WID.OptionsColumn.AllowEdit = false;
            this.WID.OptionsColumn.AllowFocus = false;
            this.WID.OptionsColumn.ReadOnly = true;
            // 
            // colGroupWork
            // 
            this.colGroupWork.AppearanceHeader.Options.UseTextOptions = true;
            this.colGroupWork.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colGroupWork.Caption = "������";
            this.colGroupWork.FieldName = "GroupWork";
            this.colGroupWork.Name = "colGroupWork";
            this.colGroupWork.OptionsColumn.AllowEdit = false;
            this.colGroupWork.OptionsColumn.AllowFocus = false;
            this.colGroupWork.OptionsColumn.ReadOnly = true;
            this.colGroupWork.Visible = true;
            this.colGroupWork.VisibleIndex = 0;
            this.colGroupWork.Width = 230;
            // 
            // colTypeWork
            // 
            this.colTypeWork.AppearanceHeader.Options.UseTextOptions = true;
            this.colTypeWork.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTypeWork.Caption = "���";
            this.colTypeWork.FieldName = "TypeWork";
            this.colTypeWork.Name = "colTypeWork";
            this.colTypeWork.OptionsColumn.AllowEdit = false;
            this.colTypeWork.OptionsColumn.AllowFocus = false;
            this.colTypeWork.OptionsColumn.ReadOnly = true;
            this.colTypeWork.Visible = true;
            this.colTypeWork.VisibleIndex = 1;
            this.colTypeWork.Width = 101;
            // 
            // colNameWork
            // 
            this.colNameWork.AppearanceHeader.Options.UseTextOptions = true;
            this.colNameWork.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNameWork.Caption = "��� ������";
            this.colNameWork.FieldName = "Name";
            this.colNameWork.Name = "colNameWork";
            this.colNameWork.OptionsColumn.AllowEdit = false;
            this.colNameWork.OptionsColumn.AllowFocus = false;
            this.colNameWork.OptionsColumn.ReadOnly = true;
            this.colNameWork.Visible = true;
            this.colNameWork.VisibleIndex = 2;
            this.colNameWork.Width = 363;
            // 
            // colSpeedBottom
            // 
            this.colSpeedBottom.AppearanceHeader.Options.UseTextOptions = true;
            this.colSpeedBottom.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSpeedBottom.Caption = "Min ��������";
            this.colSpeedBottom.FieldName = "SpeedBottom";
            this.colSpeedBottom.Name = "colSpeedBottom";
            this.colSpeedBottom.OptionsColumn.AllowEdit = false;
            this.colSpeedBottom.OptionsColumn.AllowFocus = false;
            this.colSpeedBottom.OptionsColumn.ReadOnly = true;
            this.colSpeedBottom.Visible = true;
            this.colSpeedBottom.VisibleIndex = 3;
            this.colSpeedBottom.Width = 225;
            // 
            // colSpeedTop
            // 
            this.colSpeedTop.AppearanceHeader.Options.UseTextOptions = true;
            this.colSpeedTop.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSpeedTop.Caption = "Max ��������";
            this.colSpeedTop.FieldName = "SpeedTop";
            this.colSpeedTop.Name = "colSpeedTop";
            this.colSpeedTop.OptionsColumn.AllowEdit = false;
            this.colSpeedTop.OptionsColumn.AllowFocus = false;
            this.colSpeedTop.OptionsColumn.ReadOnly = true;
            this.colSpeedTop.Visible = true;
            this.colSpeedTop.VisibleIndex = 4;
            this.colSpeedTop.Width = 233;
            // 
            // dxErrP
            // 
            this.dxErrP.ContainerControl = this;
            // 
            // icTypesSmall
            // 
            this.icTypesSmall.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("icTypesSmall.ImageStream")));
            this.icTypesSmall.Images.SetKeyName(0, "Zone.gif");
            this.icTypesSmall.Images.SetKeyName(1, "dashboard--exclamation.png");
            this.icTypesSmall.Images.SetKeyName(2, "system-monitor--exclamation.png");
            this.icTypesSmall.Images.SetKeyName(3, "no-gprs_16.png");
            this.icTypesSmall.Images.SetKeyName(4, "1281944712_traffic_lights.png");
            this.icTypesSmall.Images.SetKeyName(5, "plug--exclamation.png");
            this.icTypesSmall.Images.SetKeyName(6, "1319098306_globe--exclamation.png");
            this.icTypesSmall.Images.SetKeyName(7, "switch--exclamation.png");
            this.icTypesSmall.Images.SetKeyName(8, "1372947699_gas.png");
            this.icTypesSmall.Images.SetKeyName(9, "load.png");
            // 
            // ofdIcon
            // 
            this.ofdIcon.DereferenceLinks = false;
            this.ofdIcon.Filter = "(*.png)|*.png| (*.gif)|*.gif| (*.ico)|*.ico";
            // 
            // icTypesLarge
            // 
            this.icTypesLarge.ImageSize = new System.Drawing.Size(96, 96);
            this.icTypesLarge.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("icTypesLarge.ImageStream")));
            this.icTypesLarge.Images.SetKeyName(0, "ZoneDraw.png");
            this.icTypesLarge.Images.SetKeyName(1, "speed_64.png");
            this.icTypesLarge.Images.SetKeyName(2, "system-monitor--exclamation_64.png");
            this.icTypesLarge.Images.SetKeyName(3, "no-gprs_64.png");
            this.icTypesLarge.Images.SetKeyName(4, "DontCross.png");
            this.icTypesLarge.Images.SetKeyName(5, "power_switch-64.png");
            this.icTypesLarge.Images.SetKeyName(6, "globe--exclamation_64_gps.png");
            this.icTypesLarge.Images.SetKeyName(7, "logic_sensor_on_off_64.png");
            this.icTypesLarge.Images.SetKeyName(8, "1372948440_Gas-pump.png");
            this.icTypesLarge.Images.SetKeyName(9, "harvester96.png");
            // 
            // NoticeWizard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(716, 491);
            this.Controls.Add(this.wcNotice);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "NoticeWizard";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "�����������";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.NoticeWizardFormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.wcNotice)).EndInit();
            this.wcNotice.ResumeLayout(false);
            this.wpVehicles.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcVehicles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvVehicles)).EndInit();
            this.cwpProperty.ResumeLayout(false);
            this.cwpProperty.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tePeriod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcMail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvMail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcEmail)).EndInit();
            this.gcEmail.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cheActiveEmail.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teTimeControl.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcPopup)).EndInit();
            this.gcPopup.ResumeLayout(false);
            this.gcPopup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.leSounds.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pePopup.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chePopup.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cheActiveNotice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.peNotice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teTitle.Properties)).EndInit();
            this.wpZones.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chZoneLocate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chZoneCross.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgZoneTypeEventLocate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgZoneAddItem.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgZoneTypeEventCross.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcZones)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvZones)).EndInit();
            this.wpSelectType.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.peSpeedAgro.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.peVehUnload.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.peFuelDischarge.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.peSensorFlowType.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.peLogicSensor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.peLossGPS.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pePower.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.peStop.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.peLossData.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.peSensor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.peSpeed.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.peCH.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grSelectType.Properties)).EndInit();
            this.wpSpeed.ResumeLayout(false);
            this.wpSpeed.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teSpeedMax.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teSpeedMin.Properties)).EndInit();
            this.wpStop.ResumeLayout(false);
            this.wpStop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teStopTime.Properties)).EndInit();
            this.wpLossData.ResumeLayout(false);
            this.wpLossData.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teLossData.Properties)).EndInit();
            this.wpLossGPS.ResumeLayout(false);
            this.wpLossGPS.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teLossGPS.Properties)).EndInit();
            this.wpVehicleLogicSensors.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcLogicSensors)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvLogicSensors)).EndInit();
            this.wpVehicleSensors.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcSensors)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvSensors)).EndInit();
            this.wpSensor.ResumeLayout(false);
            this.wpSensor.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chSensorZones.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgSensorRange.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teValueTop.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teValueBottom.Properties)).EndInit();
            this.wpLogicSensor.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chLogicSensorZones.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rgLogicSensors.Properties)).EndInit();
            this.wpPower.ResumeLayout(false);
            this.wpPower.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tePower.Properties)).EndInit();
            this.wpTime.ResumeLayout(false);
            this.wpTime.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teTime.Properties)).EndInit();
            this.wpSensorFlowType.ResumeLayout(false);
            this.wpSensorFlowType.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teFlowTypeTimeForAction.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cheFlowTypeDUT.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teFlowTypeTimeForStop.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teFlowTypeTimeForFind.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teFlowTypeRadius.Properties)).EndInit();
            this.wcPopupUsers.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcPopupUsers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvPopupUsers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rleUsers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rchActive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rlePUsers)).EndInit();
            this.wpFuelDischarge.ResumeLayout(false);
            this.wpFuelDischarge.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teFuelAvgExpense.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teFuelZeroPointsCount.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.teFuelDischarge.Properties)).EndInit();
            this.wpVehicleUnload.ResumeLayout(false);
            this.wpVehicleUnload.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.teTimeMaxUnload.Properties)).EndInit();
            this.wpSpeedAgro.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcWorks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvWorks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxErrP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.icTypesSmall)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.icTypesLarge)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.Columns.GridColumn TID;
        private DevExpress.XtraGrid.Columns.GridColumn colGName;
        private DevExpress.XtraGrid.Columns.GridColumn VID;
        private DevExpress.XtraGrid.Columns.GridColumn colMakeCar;
        private DevExpress.XtraGrid.Columns.GridColumn colCarModel;
        private DevExpress.XtraGrid.Columns.GridColumn colNumberPlate;
        private DevExpress.XtraGrid.Columns.GridColumn colDName;
        private DevExpress.XtraGrid.Columns.GridColumn Id;
        private DevExpress.XtraGrid.Columns.GridColumn colGZone;
        private DevExpress.XtraGrid.Columns.GridColumn Zone_ID;
        private DevExpress.XtraGrid.Columns.GridColumn colZoneName;
        private DevExpress.XtraEditors.LabelControl lbSpeedMax;
        private DevExpress.XtraEditors.LabelControl lbSpeedMin;
        private DevExpress.XtraEditors.LabelControl lbStopTime;
        private DevExpress.XtraEditors.LabelControl lbPropName;
        private DevExpress.XtraEditors.PictureEdit peCH;
        private DevExpress.XtraEditors.PictureEdit pePower;
        private DevExpress.XtraEditors.PictureEdit peStop;
        private DevExpress.XtraEditors.PictureEdit peLossData;
        private DevExpress.XtraEditors.PictureEdit peSensor;
        private DevExpress.XtraEditors.PictureEdit peSpeed;
        private DevExpress.XtraEditors.LabelControl lbIconSmall;
        private DevExpress.XtraEditors.SimpleButton sbIconSmall;
        private DevExpress.XtraEditors.GroupControl gcPopup;
        private DevExpress.XtraEditors.LabelControl lbIconLarge;
        private DevExpress.XtraEditors.SimpleButton sbIconLarge;
        private DevExpress.XtraEditors.LabelControl lbSound;
        private System.Windows.Forms.OpenFileDialog ofdIcon;
        private DevExpress.XtraEditors.PictureEdit peLossGPS;
        private DevExpress.XtraEditors.SimpleButton cbSoundPlay;
        private DevExpress.XtraEditors.LabelControl lbLossData;
        private DevExpress.XtraEditors.LabelControl lbTimeControl;
        private DevExpress.XtraEditors.LabelControl lbLossGPS;
        internal DevExpress.XtraWizard.CompletionWizardPage cwpProperty;
        internal DevExpress.XtraWizard.WizardPage wpVehicles;
        internal DevExpress.XtraWizard.WizardPage wpZones;
        internal DevExpress.XtraWizard.WizardPage wpSelectType;
        internal DevExpress.XtraWizard.WizardPage wpSpeed;
        internal DevExpress.XtraWizard.WizardPage wpStop;
        internal DevExpress.XtraWizard.WizardPage wpLossData;
        internal DevExpress.XtraWizard.WizardPage wpLossGPS;
        internal DevExpress.XtraWizard.WizardControl wcNotice;
        internal DevExpress.XtraEditors.RadioGroup grSelectType;
        private DevExpress.XtraWizard.WizardPage wpVehicleLogicSensors;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn colGNameLSensor;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn colMakeCarLSensor;
        private DevExpress.XtraGrid.Columns.GridColumn colCarModelLSensor;
        private DevExpress.XtraGrid.Columns.GridColumn colNumberPlateLSensor;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn colLSensor;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn colGNameSensor;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn colMakeCarSensor;
        private DevExpress.XtraGrid.Columns.GridColumn colCarModelSensor;
        private DevExpress.XtraGrid.Columns.GridColumn colNumberPlateSensor;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn colSensor;
        internal DevExpress.XtraGrid.GridControl gcVehicles;
        internal DevExpress.XtraGrid.GridControl gcLogicSensors;
        internal DevExpress.XtraGrid.GridControl gcSensors;
        private DevExpress.XtraEditors.PictureEdit peLogicSensor;
        internal DevExpress.XtraGrid.GridControl gcZones;
        internal DevExpress.XtraEditors.RadioGroup rgZoneTypeEventCross;
        internal DevExpress.XtraEditors.RadioGroup rgZoneAddItem;
        internal DevExpress.XtraEditors.CheckEdit chZoneLocate;
        internal DevExpress.XtraEditors.CheckEdit chZoneCross;
        internal DevExpress.XtraEditors.RadioGroup rgZoneTypeEventLocate;
        private DevExpress.XtraWizard.WizardPage wpSensor;
        private DevExpress.XtraWizard.WizardPage wpLogicSensor;
        private DevExpress.XtraGrid.Columns.GridColumn colAlgoritm;
        internal DevExpress.XtraEditors.CheckEdit chLogicSensorZones;
        private DevExpress.XtraEditors.LabelControl lbTopLevel;
        private DevExpress.XtraEditors.LabelControl lbLowLevel;
        internal DevExpress.XtraEditors.CheckEdit chSensorZones;
        internal DevExpress.XtraGrid.Views.Grid.GridView gvZones;
        internal DevExpress.XtraEditors.TextEdit teValueTop;
        internal DevExpress.XtraEditors.TextEdit teValueBottom;
        internal DevExpress.XtraEditors.RadioGroup rgSensorRange;
        internal DevExpress.XtraEditors.RadioGroup rgLogicSensors;
        internal DevExpress.XtraEditors.TextEdit teStopTime;
        internal DevExpress.XtraEditors.TextEdit teSpeedMax;
        internal DevExpress.XtraEditors.TextEdit teSpeedMin;
        internal DevExpress.XtraEditors.TextEdit teTitle;
        internal DevExpress.XtraEditors.PictureEdit peNotice;
        internal DevExpress.XtraEditors.PictureEdit pePopup;
        internal DevExpress.XtraEditors.LookUpEdit leSounds;
        internal DevExpress.XtraEditors.TextEdit teTimeControl;
        internal DevExpress.XtraEditors.CheckEdit cheActiveNotice;
        internal DevExpress.XtraEditors.CheckEdit chePopup;
        internal DevExpress.Utils.ImageCollection icTypesSmall;
        internal DevExpress.Utils.ImageCollection icTypesLarge;
        internal DevExpress.XtraGrid.Views.Grid.GridView gvVehicles;
        internal DevExpress.XtraGrid.Views.Grid.GridView gvLogicSensors;
        internal DevExpress.XtraGrid.Views.Grid.GridView gvSensors;
        internal DevExpress.XtraEditors.TextEdit teLossGPS;
        internal DevExpress.XtraEditors.TextEdit teLossData;
        private DevExpress.XtraWizard.WizardPage wpPower;
        internal DevExpress.XtraEditors.TextEdit tePower;
        private DevExpress.XtraEditors.LabelControl lbPower;
        internal DevExpress.XtraEditors.DXErrorProvider.DXErrorProvider dxErrP;
        internal DevExpress.XtraEditors.TextEdit teTime;
        private DevExpress.XtraEditors.LabelControl lbTime;
        internal DevExpress.XtraWizard.WizardPage wpTime;
        private DevExpress.XtraEditors.GroupControl gcEmail;
        internal DevExpress.XtraEditors.CheckEdit cheActiveEmail;
        private DevExpress.XtraGrid.Views.Grid.GridView gvMail;
        private DevExpress.XtraGrid.Columns.GridColumn colMailId;
        private DevExpress.XtraGrid.Columns.GridColumn colMailActive;
        private DevExpress.XtraGrid.Columns.GridColumn colMailEmail;
        internal DevExpress.XtraGrid.GridControl gcMail;
        private DevExpress.XtraWizard.WizardPage wpSensorFlowType;
        internal DevExpress.XtraEditors.TextEdit teFlowTypeTimeForStop;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        internal DevExpress.XtraEditors.TextEdit teFlowTypeTimeForFind;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        internal DevExpress.XtraEditors.TextEdit teFlowTypeRadius;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        internal DevExpress.XtraEditors.CheckEdit cheFlowTypeDUT;
        private DevExpress.XtraEditors.PictureEdit peSensorFlowType;
        internal DevExpress.XtraEditors.TextEdit teFlowTypeTimeForAction;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        internal DevExpress.XtraWizard.WizardPage wcPopupUsers;
        private DevExpress.XtraGrid.Columns.GridColumn colUId;
        private DevExpress.XtraGrid.Columns.GridColumn colUName;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit rchActive;
        internal DevExpress.XtraGrid.GridControl gcPopupUsers;
        internal DevExpress.XtraGrid.Views.Grid.GridView gvPopupUsers;
        internal DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit rleUsers;
        internal DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit rlePUsers;
        internal DevExpress.XtraWizard.WizardPage wpVehicleSensors;
        private DevExpress.XtraEditors.PictureEdit peFuelDischarge;
        private DevExpress.XtraWizard.WizardPage wpFuelDischarge;
        internal DevExpress.XtraEditors.TextEdit teFuelDischarge;
        private DevExpress.XtraEditors.LabelControl lbFuelDischarge;
        internal DevExpress.XtraEditors.TextEdit teFuelZeroPointsCount;
        private DevExpress.XtraEditors.LabelControl lbFuelZeroControl;
        internal DevExpress.XtraEditors.TextEdit teFuelAvgExpense;
        private DevExpress.XtraEditors.LabelControl lbFuelAvgExpense;
        private DevExpress.XtraEditors.PictureEdit peVehUnload;
        private DevExpress.XtraWizard.WizardPage wpVehicleUnload;
        private DevExpress.XtraEditors.LabelControl lbTimeMaxUnload;
        public DevExpress.XtraEditors.TextEdit teTimeMaxUnload;
        private DevExpress.XtraEditors.PictureEdit peSpeedAgro;
        private DevExpress.XtraWizard.WizardPage wpSpeedAgro;
        internal DevExpress.XtraGrid.GridControl gcWorks;
        internal DevExpress.XtraGrid.Views.Grid.GridView gvWorks;
        private DevExpress.XtraGrid.Columns.GridColumn WID;
        private DevExpress.XtraGrid.Columns.GridColumn colGroupWork;
        private DevExpress.XtraGrid.Columns.GridColumn colTypeWork;
        private DevExpress.XtraGrid.Columns.GridColumn colNameWork;
        private DevExpress.XtraGrid.Columns.GridColumn colSpeedBottom;
        private DevExpress.XtraGrid.Columns.GridColumn colSpeedTop;
        internal DevExpress.XtraEditors.TextEdit tePeriod;
        private DevExpress.XtraEditors.LabelControl lbPeriod;
    }
}