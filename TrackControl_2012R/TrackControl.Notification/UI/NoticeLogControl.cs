using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Xml;
using DevExpress.XtraEditors;
using TrackControl.General;
using TrackControl.GMap;
using TrackControl.GMap.Core;
using TrackControl.MySqlDal;
using TrackControl.Notification.Properties;
using TrackControl.Vehicles;
using TrackControl.Zones;

namespace TrackControl.Notification
{
    public partial class NoticeLogControl : DevExpress.XtraEditors.XtraUserControl
    {
        private GoogleMapControl _mcGoogle;
        //private readonly ZonesProvider _zprov;
        //private readonly GpsDataProvider _gpsProv;

        /// <summary>
        /// ������������ ��� ����������������� ������� ��� ��������� ������� ����
        /// </summary>
        private int _lastIdLog;

        /// <summary>
        /// ���������������� ����� � ���������� �������
        /// </summary>
        private System.Threading.Timer _tmLastIdLog;

        /// <summary>
        /// ������ ������ ���������� ������
        /// </summary>
        private readonly NoticeSetItem _nseti;

        public static IZonesManager ZonesModel;

        public event MethodInvoker StartMonitoringOnLine;
        public event ActionForPeriod StartMonitoringPeriod;
        public event MethodInvoker StopMonitoringOnLine;
        public event MethodInvoker StopMonitoringPeriod;

        #region Log_Fields

        private DateTime _timeEvent;
        private int _mobitelId;
        private int _zoneId;
        private int _niId;
        private double _lat;
        private double _lng;
        private int _typeEvent;
        private double _speed;

        #endregion

        public NoticeLogControl()
        {
            InitializeComponent();
            //_zprov = new ZonesProvider();
            //_gpsProv = new GpsDataProvider();
            NoticeMonitoringOnLine.RecordsCreatedOnLine += RefreshOnLineData;
            NoticeMonitoringPeriod.RecordsCreatedPeriod += RefreshPeriodData;
            NoticeMonitoringPeriod.StopPeriodAnaliz += StopPeriodAnaliz;

            InitGoogleMapControl();
            Localization();
            switch ((int) beiSelectRegime.EditValue)
            {
                case (int) Regime.Online:
                    if (NoticeMonitoringOnLine.IsActive)
                    {
                        bbiStart.Caption = Resources.Stop;
                        bbiStart.Glyph = Shared.StopMain;
                        RefreshOnLineData();
                    }
                    break;
                case (int) Regime.Period:
                    if (NoticeMonitoringPeriod.IsActive)
                    {
                        bbiStart.Caption = Resources.Stop;
                        bbiStart.Glyph = Shared.StopMain;
                    }
                    break;
            }

            _nseti = new NoticeSetItem();
            beiStart.EditValue = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day);
            beiEnd.EditValue = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day, 23, 59, 59);
            SetControls();
        }

        private void InitGoogleMapControl()
        {
            try
            {
                _mcGoogle = new GoogleMapControl();
                splitContainerControl1.Panel2.Controls.Add(_mcGoogle);
                _mcGoogle.Dock = DockStyle.Fill;
                //_mcGoogle.MapType = MapType.GoogleMap;
                string currentMap = UseMap.getTypeMap();
                object mapEnum = System.Enum.Parse(typeof(MapType), currentMap);
                MapType selectMap = (MapType) mapEnum;
                _mcGoogle.MapType = selectMap;
                int zoom = Convert.ToInt32(UseMap.getZoomMap());
                _mcGoogle.State = new MapState(zoom, new PointLatLng(ConstsGen.KievLat, ConstsGen.KievLng));
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message + "\n" + ex.StackTrace, Resources.ErrorMsg, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void InitYandexMapControl()
        {
            _mcGoogle = new GoogleMapControl();
            splitContainerControl1.Panel2.Controls.Add(_mcGoogle);
            _mcGoogle.Dock = DockStyle.Fill;
            _mcGoogle.MapType = MapType.YandexMap;
            _mcGoogle.State = new MapState(ConstsGen.KievZoom, new PointLatLng(ConstsGen.KievLat, ConstsGen.KievLng));
        }

        private void RefreshOnLineData()
        {
            DataTable dt = NoticeProvider.GetLog();
            DoRefreshData(dt);
        }

        private void RefreshDataByTimer(object data)
        {
            if (_nseti.LastLogId > _lastIdLog)
            {
                DataTable dt = NoticeProvider.GetLog();
                Action<DataTable> action = DoRefreshData;
                if (InvokeRequired)
                {
                    Invoke(action, dt);
                }
                else
                {
                    action(dt);
                }
                _lastIdLog = _nseti.LastLogId;
            }
        }

        private void RefreshPeriodData(List<NoticeLogRecord> records)
        {
            gcLog.DataSource = records;
            gcLog.RefreshDataSource();
        }

        private void StopPeriodAnaliz()
        {
            bbiStart.Caption = Resources.Start;
            bbiStart.Glyph = Shared.StartMain;
        }

        private void DoRefreshData(DataTable dt)
        {
            gcLog.DataSource = dt;
            gcLog.LevelTree.Nodes.Add(Resources.Location, gvLogDetal);
            if (gvLog.RowCount > 0)
            {
                Int32.TryParse(gvLog.GetRowCellValue(0, "Id").ToString(), out _lastIdLog);
            }
        }

        private void bbiRefresh_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            RefreshOnLineData();
        }

        private void bbiTest_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            using (NoticeItem ni = new NoticeItem())
            {
                ni.TestEvents();
            }
        }

        private void bbiExcel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            XtraGridService.SetupGidViewForPrint(gvLog, true, true);
            LinkGrid.CreateDocument();
            LinkGrid.ShowPreview();
        }

        private void gvLog_RowStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs e)
        {
            if (e.RowHandle >= 0 && gvLog.GetRowCellValue(e.RowHandle, gvLog.Columns["IsRead"]) != null)
            {
                int IsRead;
                if (Int32.TryParse(gvLog.GetRowCellValue(e.RowHandle, gvLog.Columns["IsRead"]).ToString(), out IsRead))
                {
                    if (IsRead == 0)
                        e.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
                    else
                        e.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F);

                }
            }
        }

        private void gvLog_RowClick(object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {
            if (e.RowHandle >= 0)
            {
                if ((int) beiSelectRegime.EditValue == (int) Regime.Online) SetIsRead(e.RowHandle);
                ShowNoticeOnMap(e.RowHandle);
            }
        }

        private void gvLog_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (e.FocusedRowHandle >= 0)
            {
                ShowNoticeOnMap(e.FocusedRowHandle);
            }
        }

        private void ShowNoticeOnMap(int rowHandle)
        {
            if (gvLog.GetRowCellValue(rowHandle, gvLog.Columns["DateEvent"]) == null)
            {
                _timeEvent = DateTime.MinValue;
                _niId = 0;
                _speed = 0;
                return;
            }
            if ( !DateTime.TryParse(gvLog.GetRowCellValue(rowHandle, gvLog.Columns["DateEvent"]).ToString(),
                    out _timeEvent))
                _timeEvent = DateTime.MinValue;
            if (!Int32.TryParse(gvLog.GetRowCellValue(rowHandle, gvLog.Columns["Id_main"]).ToString(), out _niId))
                _niId = 0;
            if (Int32.TryParse(gvLog.GetRowCellValue(rowHandle, gvLog.Columns["Mobitel_id"]).ToString(), out _mobitelId)
                && Double.TryParse(gvLog.GetRowCellValue(rowHandle, gvLog.Columns["Lat"]).ToString(), out _lat)
                && Double.TryParse(gvLog.GetRowCellValue(rowHandle, gvLog.Columns["Lng"]).ToString(), out _lng)
                &&
                Int32.TryParse(gvLog.GetRowCellValue(rowHandle, gvLog.Columns["TypeEvent"]).ToString(), out _typeEvent)
                && Int32.TryParse(gvLog.GetRowCellValue(rowHandle, gvLog.Columns["Zone_id"]).ToString(), out _zoneId))
            {
                _speed = 0;
                Double.TryParse(gvLog.GetRowCellValue(rowHandle, gvLog.Columns["Speed"]).ToString(), out _speed);
                DrawNotice();
            }
        }

        private void SetIsRead(int RowHandle)
        {
            if (gvLog.GetRowCellValue(RowHandle, gvLog.Columns["Id"]) == null) return;
            int idRecord;
            if (Int32.TryParse(gvLog.GetRowCellValue(RowHandle, gvLog.Columns["Id"]).ToString(), out idRecord))
            {
                if (gvLog.GetRowCellValue(RowHandle, gvLog.Columns["IsRead"]).ToString() == "0")
                {
                    gvLog.SetRowCellValue(RowHandle, gvLog.Columns["IsRead"], true);
                    NoticeProvider.UpdateReading(idRecord);
                    gvLog.RefreshRow(RowHandle);
                }
            }
        }

        private void DrawNotice()
        {
            _mcGoogle.ClearAll();
            if (_timeEvent == DateTime.MinValue || _niId == 0) return;
            if (_typeEvent == (int) NoticeSubItemTypes.LossGps)
            {
                DrawTrackBeforNoticeLossGps();
            }
            else
            {
                _mcGoogle.PanTo(new PointLatLng(_lat, _lng));
                Marker m = SetMarker();
                _mcGoogle.AddMarker(m);
                DrawTrackBeforNotice();
            }
           
            _mcGoogle.Repaint();
        }

        private Marker SetMarker()
        {
            MarkerType mtype;
            _mcGoogle.DrawTrackMode = DrawTrackModes.Simple;
            switch (_typeEvent)
            {
                case (int) NoticeSubItemTypes.CheckZoneControl:
                    if (_zoneId > 0) _mcGoogle.AddZone(ZonesModel.GetById(_zoneId));
                    SetSpeedControl();
                    mtype = SetMarkerMovingParking();
                    break;
                case (int) NoticeSubItemTypes.SpeedControl:
                    SetSpeedControl();
                    mtype = MarkerType.Moving;
                    break;
                case (int) NoticeSubItemTypes.SensorControl:
                    mtype = SetMarkerMovingParking();
                    break;
                case (int) NoticeSubItemTypes.SensorLogicControl:
                    mtype = SetMarkerMovingParking();
                    break;
                case (int) NoticeSubItemTypes.LossData:
                    mtype = MarkerType.Alarm;
                    break;
                case (int) NoticeSubItemTypes.StopControl:
                    mtype = MarkerType.Parking;
                    break;
                case (int) NoticeSubItemTypes.LossPower:
                    mtype = SetMarkerMovingParking();
                    break;
                case (int) NoticeSubItemTypes.LossGps:
                    mtype = MarkerType.Alarm;
                    break;
                case (int) NoticeSubItemTypes.VehicleUnload:
                    mtype = MarkerType.Parking;
                    if (_zoneId > 0) _mcGoogle.AddZone(ZonesModel.GetById(_zoneId));
                    break;
                default:
                    mtype = MarkerType.Moving;
                    break;
            }
            var m = new Marker(mtype, _mobitelId, new PointLatLng(_lat, _lng), "", "");
            return m;
        }

        private MarkerType SetMarkerMovingParking()
        {
            MarkerType mtype;
            if (_speed > 0)
                mtype = MarkerType.Moving;
            else
                mtype = MarkerType.Parking;
            return mtype;
        }

        private void SetSpeedControl()
        {
            using (NoticeItem ni = new NoticeItem(_niId, NoticeItem.LoadRegime.Events))
            {
                NoticeSpeed nispeed = (NoticeSpeed) ni.GetSubItem<NoticeSpeed>();
                if (nispeed != null)
                {
                    Dictionary<float, Color> codes = new Dictionary<float, Color>(3);
                    codes.Add((float) nispeed.SpeedMin, Color.Brown);
                    codes.Add((float) nispeed.SpeedMax, Color.DarkBlue);
                    codes.Add(1000, Color.Red);
                    _mcGoogle.DrawTrackMode = DrawTrackModes.DiffColors;
                    _mcGoogle.SetValueCodes(codes);
                }
            }
        }

        private void DrawTrackBeforNotice()
        {
            if (_timeEvent == DateTime.MinValue) return;
            List<GpsData> gps_data =
                new List<GpsData>(GpsDataProvider.GetGpsDataAllProtocolsAllPonts(_mobitelId, _timeEvent.AddHours(-24), _timeEvent));
            List<IGeoPoint> points = new List<IGeoPoint>();
            gps_data.ForEach(delegate(GpsData p)
            {
                if (p.Valid && p.LatLng.Lat != 0 && p.LatLng.Lng != 0)
                    points.Add(p);
            });
            _mcGoogle.ClearTracks();
            if (points.Count > 0)
            {
                _mcGoogle.AddTrack(new Track(_mobitelId, points));
            }

        }

        private void DrawTrackBeforNoticeLossGps()
        {
            if (_timeEvent == DateTime.MinValue) return;
            List<GpsData> gps_data =
                new List<GpsData>(GpsDataProvider.GetGpsDataAllProtocolsAllPonts(_mobitelId, _timeEvent.AddHours(-12), _timeEvent));
            List<IGeoPoint> points = new List<IGeoPoint>();
            gps_data.ForEach(delegate(GpsData p)
            {
                if (p.Valid && p.LatLng.Lat != 0 && p.LatLng.Lng != 0)
                    points.Add(p);
            });
            _mcGoogle.ClearTracks();
            if (points.Count > 0)
            {
                //if (points.Count > 5) points = points.GetRange(points.Count - 5,5).ToList();
                _lat = points.Last().LatLng.Lat;
                _lng = points.Last().LatLng.Lng;
                _mcGoogle.PanTo(new PointLatLng(_lat, _lng));
                Marker m = SetMarker();
                _mcGoogle.AddMarker(m);
                _mcGoogle.AddTrack(new Track(_mobitelId, points));

            }
        }

        private void bbiStart_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            SwitchWorkRegime();
        }

        private void SwitchWorkRegime()
        {
            if (bbiStart.Caption == Resources.Start)
            {
                bbiStart.Enabled = true;
                bbiStart.Caption = Resources.Stop;
                bbiStart.Glyph = Shared.StopMain;
                switch ((int) beiSelectRegime.EditValue)
                {
                    case (int) Regime.Online:
                        if (StartMonitoringOnLine != null) 
                            StartMonitoringOnLine();
                        _tmLastIdLog = new System.Threading.Timer(RefreshDataByTimer);
                        _tmLastIdLog.Change(0, _nseti.TimePeriod*10000);
                        RefreshOnLineData();
                        break;
                    case (int) Regime.Period:
                        if (StartMonitoringPeriod != null)
                            StartMonitoringPeriod((DateTime) beiStart.EditValue, (DateTime) beiEnd.EditValue);
                        break;
                }

            }
            else
            {
                if (bbiStart.Caption == Resources.Start) return;
                bbiStart.Caption = Resources.Start;
                bbiStart.Glyph = Shared.StartMain;

                switch ((int) beiSelectRegime.EditValue)
                {
                    case (int) Regime.Online:
                        StopMonitoringOnLine();
                        if (_tmLastIdLog != null) _tmLastIdLog.Dispose();
                        break;
                    case (int) Regime.Period:
                        StopMonitoringPeriod();
                        break;
                }
            }
            Application.DoEvents();
        }

        private void SetControls()
        {
            switch ((int) beiSelectRegime.EditValue)
            {
                case (int) Regime.Online:
                    beiStart.Enabled = false;
                    beiEnd.Enabled = false;
                    bbiRefresh.Enabled = true;
                    break;
                case (int) Regime.Period:
                    beiStart.Enabled = true;
                    beiEnd.Enabled = true;
                    bbiRefresh.Enabled = false;
                    break;
            }
        }

        private void Localization()
        {
            bbiStart.Caption = Resources.Start;
            bbiStart.Glyph = Shared.StartMain;
            bbiRefresh.Caption = Resources.Refresh;
            bbiExcel.Caption = Resources.ExcelExport;

            colTimeEvent.Caption = Resources.Time;
            colIconSmall.Caption = Resources.Type;
            colIsPopup.ToolTip = Resources.NoticePopup;
            colVehicle.Caption = Resources.Vehicle;
            colInfor.Caption = Resources.Event;

            colDateEvent.Caption = Resources.Date;
            colLocation.Caption = Resources.Location;

            colInfor.SummaryItem.DisplayFormat = Resources.LastNotification + ": {0} ";
            colVehicle.SummaryItem.DisplayFormat = Resources.Total + ": {0} ";

            riSelectRegime.Properties.Items[1].Description = Resources.Period;
        }

        private void beiSelectRegime_EditValueChanged(object sender, EventArgs e)
        {
            SetControls();
        }

        private enum Regime
        {
            Online = 1,
            Period
        }
    }
}
