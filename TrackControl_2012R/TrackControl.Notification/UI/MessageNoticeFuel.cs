﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Diagnostics; 
using System.Text;
using System.Windows.Forms;
using Graph;
using TrackControl.Notification.Properties;
using BaseReports.Procedure;
using TrackControl.Reports;


namespace TrackControl.Notification
{
    public partial class MessageNoticeFuel : TrackControl.Notification.MessageNotice
    {
        ZGraphControl _zGraphFuel;
        public MessageNoticeFuel(NoticeEventAnalyser analyzer)
        {
            InitializeComponent();
            _analyzer = analyzer;
            this.Text = _analyzer.Nsi.Ni.Title;
            SetRandomLocation();
            ShowGraphFuel();
            Show(_analyzer.Nsi.Ni); 
        }


        private void ShowGraphFuel()
        {
            _zGraphFuel = new ZGraphControl();
            _zGraphFuel.SetRunVisible();
            pcGraph.Controls.Add(_zGraphFuel);
            _zGraphFuel.Dock = DockStyle.Fill;
            _zGraphFuel.ClearGraphZoom();
            _zGraphFuel.ClearRegion();
            lbPoints.Text = string.Format("{0}: {1}",Resources.TotalPoints, _analyzer.GpsBeforEvent.Count); 
            ShowGraph();
            Application.DoEvents();  
        }

        void ShowGraph()
        {

            if (_analyzer.GpsBeforEvent != null && _analyzer.GpsBeforEvent.Count >0)
            {
                double[] fuelSensorValue = new double[_analyzer.GpsBeforEvent.Count];
                DateTime[] timeValue = new DateTime[_analyzer.GpsBeforEvent.Count];
                double minY = _analyzer.GpsBeforEvent[0].SensorValue;
                double maxY = _analyzer.GpsBeforEvent[0].SensorValue;
                for (int i = 0; i < _analyzer.GpsBeforEvent.Count; i++)
                {
                    fuelSensorValue[i] = _analyzer.GpsBeforEvent[i].SensorValue;
                    timeValue[i] = _analyzer.GpsBeforEvent[i].Time;
                    if (_analyzer.GpsBeforEvent[i].SensorValue > maxY) maxY = _analyzer.GpsBeforEvent[i].SensorValue;
                    if (_analyzer.GpsBeforEvent[i].SensorValue < minY) minY = _analyzer.GpsBeforEvent[i].SensorValue;
                }
                _analyzer.GpsBeforEvent.Clear();
                _zGraphFuel.AddSeriesL(String.Format(Resources.BuildGraphsTotalFuel, Resources.LiterShort), Color.BlueViolet, fuelSensorValue, timeValue, AlgorithmType.FUEL1, true);
                _zGraphFuel.ShowSeries(Resources.FuelGraph);
                _zGraphFuel.SetAxisYLimits(minY - maxY / 20, maxY + maxY/20); 
            }
        }
    }
}
