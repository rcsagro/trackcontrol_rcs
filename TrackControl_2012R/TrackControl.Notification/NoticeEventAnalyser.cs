﻿using System;
using System.Collections.Generic;
using System.Text;
using TrackControl.Reports;
using TrackControl.Vehicles;
using Agro.Dictionaries;



namespace TrackControl.Notification
{
    public class NoticeEventAnalyser
    {
        private Vehicle _vehicle;

        public Vehicle Vehicle
        {
            get { return _vehicle; }
            set { _vehicle = value; }
        }

        private NoticeCheckZone _nsi_chz;

        public NoticeCheckZone Nsi_chz
        {
            get { return _nsi_chz; }
            set { _nsi_chz = value; }
        }

        private NoticeSubItem _nsi;

        public NoticeSubItem Nsi
        {
            get { return _nsi; }
            set { _nsi = value; }
        }

        public List<GpsData> GpsBeforEvent { get; set; }

        public int LastLogId { get; set; }
        //List<GpsData> _dataGpsBetweenEvents ;

        //public List<GpsData> DataGpsBetweenEvents
        //{
        //    get { return _dataGpsBetweenEvents;}
        //}

        public NoticeEventAnalyser(Vehicle vehicle, NoticeCheckZone nsi_chz, NoticeSubItem nsi)
        {
            _vehicle = vehicle;
            _nsi_chz = nsi_chz;
            _nsi = nsi;
            //_dataGpsBetweenEvents = new List<GpsData>();
            if (nsi != null)
            {
                // датчики хранятся в транспорте 
                switch (nsi.TypeEvent)
                {
                    case NoticeSubItemTypes.SensorLogicControl:
                    {
                        ((NoticeLogicSensor) _nsi).Sensor = vehicle.LogicSensor;
                        break;
                    }
                    case NoticeSubItemTypes.SensorControl:
                    {
                        ((NoticeSensor) _nsi).Sensor = vehicle.Sensor;
                        ((NoticeSensor) _nsi).Vehicle = vehicle;
                        break;
                    }
                    case NoticeSubItemTypes.SensorFlowTypeControl:
                    {
                        ((NoticeFlowTypeSensor) _nsi).Sensor = vehicle.Sensor;
                        ((NoticeFlowTypeSensor) _nsi).VehicleForAnalize = vehicle;
                        break;
                    }
                    case NoticeSubItemTypes.FuelDischarge:
                    {
                        ((NoticeFuelDischarge) _nsi).VehicleForAnalize = vehicle;
                        break;
                    }
                    case NoticeSubItemTypes.VehicleUnload:
                    {
                        ((NoticeVehicleUnload) _nsi).VehicleUnloadSensor = vehicle.LogicSensor;
                        ((NoticeVehicleUnload) _nsi).SetVehicleUnload(vehicle);
                        break;
                    }
                    case NoticeSubItemTypes.StopControl:
                        var rfidDriverStop = new Sensor((int) AlgorithmType.DRIVER, vehicle.Mobitel.Id);
                        if (rfidDriverStop.Id > 0) ((NoticeStop) _nsi).SensorRfidDriver = rfidDriverStop;
                        break;
                    case NoticeSubItemTypes.SpeedAgroWorksControl:
                    {
                        if (vehicle.Sensor != null) ((NoticeSpeedAgroWork) _nsi).SensorRfidAgregat = vehicle.Sensor;
                        var rfidDriver = new Sensor((int) AlgorithmType.DRIVER, vehicle.Mobitel.Id);
                        if (rfidDriver.Id > 0) ((NoticeSpeedAgroWork) _nsi).SensorRfidDriver = rfidDriver;
                        ((NoticeSpeedAgroWork) _nsi).VehicleSubItem = vehicle;
                        if (vehicle.IdAgroWorkType > 0)
                        {
                            ((NoticeSpeedAgroWork) _nsi).DefaultAgroWork =
                                new DictionaryAgroWorkType(vehicle.IdAgroWorkType);
                            ((NoticeSpeedAgroWork) _nsi).DefaultAgregat =
                                new DictionaryAgroAgregat(vehicle.IdAgroAgregat);
                        }

                        break;
                    }
                    default:
                        break;
                }
            }
        }

        public bool CheckEvent(GpsData gps)
        {
            if (_vehicle.Mobitel.Id == gps.Mobitel)
            {
                //_dataGpsBetweenEvents.Add(gps);
                //учитывается сочетание контрольной зоны с другими событиями
                if ((_nsi_chz == null ? true : _nsi_chz.CheckEventWithPeriodLimit(gps)) &
                    (_nsi == null ? true : _nsi.CheckEventWithPeriodLimit(gps)))
                {

                    //_vehicle = ((NoticeSensor)_nsi).Vehicle;

                    //if (!_is_active)
                    //{
                    //    _is_active = true;
                    if (_nsi != null && (_nsi is NoticeFuelDischarge))
                    {
                        if (GpsBeforEvent == null)
                        {
                            GpsBeforEvent = new List<GpsData>();
                        }
                        else
                        {
                            GpsBeforEvent.Clear();
                        }
                        foreach (GpsData  gpsFromNi in ((NoticeFuelDischarge) _nsi).GpsBeforEvent)
                        {
                            GpsBeforEvent.Add(gpsFromNi);
                        }
                        ((NoticeFuelDischarge) _nsi).GpsBeforEvent.Clear();
                    }
                    return true;
                    //    }
                    //}
                    //else
                    //{
                    //    _is_active = false;
                }
                return false;
            }
            return false;
        }

        public bool CheckState()
        {
            if (_nsi is NoticeLossData)
            {
                return _nsi.CheckState();
            }
            else
                return false;
        }
    }
}
