﻿using System;
using System.Collections.Generic;
using System.Text;
using TrackControl.Vehicles;

namespace TrackControl.Notification
{
    public class TimeAccumulator
    {
        
        TimeSpan _timeAccumule;
        DateTime _prevDate = DateTime.MinValue;

        public TimeSpan TimeAccumule 
        {
            get { return _timeAccumule; }
        }

        public DateTime PrevDate
        {
            get { return _prevDate; }
            set { _prevDate = value; }
        }

        bool _isReady;

        public bool IsReady
        {
            get { return _isReady; }
            set { _isReady = value;
            if (!_isReady) StopAccumulate();
            }
        }
        TimeSpan _timeForCompare;
        public TimeSpan TimeForCompare
        {
            get { return _timeForCompare; }
            set { _timeForCompare = value; }
        }

        bool _isActive;
        public bool IsActive
        {
            get { return _isActive; }
            set { _isActive = value; }
        }

        public TimeAccumulator(TimeSpan timeForCompare)
        {
            _timeForCompare = timeForCompare;
        }

        public void AddAcumulate(DateTime date)
        {
            if (_prevDate != DateTime.MinValue) _timeAccumule = _timeAccumule.Add(date.Subtract(_prevDate));
            _prevDate = date;
        }

        public void StartAccumulate(DateTime date)
        {
            _timeAccumule = TimeSpan.Zero;
            _prevDate = DateTime.MinValue;
            _isActive = true;
            TestAccumulate(date);
        }

        public void StopAccumulate()
        {
            _timeAccumule = TimeSpan.Zero;
            _prevDate = DateTime.MinValue;
            _isActive = false;
        }
        public bool TestAccumulate(DateTime date)
        {
            if (!_isActive) return false;
            AddAcumulate(date);
            if (_timeAccumule.TotalMinutes >= _timeForCompare.TotalMinutes)
            {
                StopAccumulate();
                _isReady = true;
                _isActive = false;
                return true;
            }
            else
                return false;
        }
    }
}
