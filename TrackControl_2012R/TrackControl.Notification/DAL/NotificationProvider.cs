﻿using System;
using System.Collections.Generic;
using System.Text;
using TrackControl.Notification;
using TrackControl.General;
using MySql.Data.MySqlClient;
using TrackControl.Vehicles;
using TrackControl.General.DAL;

namespace TrackControl.MySqlDal
{
    public class NotificationProvider
    {
        string _sql;

        public bool INSERT (NoticeItem ni)
        {
            try {
                using (ConnectMySQL _cn = new ConnectMySQL()) {
                    _sql = "INSERT INTO ntf_main (IsActive, Title, IconName, SoundName) VALUES (?IsActive, ?Title, ?IconName, ?SoundName)";
                    MySqlParameter[] _parSql = new MySqlParameter[4];
                    _parSql [0] = new MySqlParameter ("?IsActive", MySqlDbType.Bit);
                    _parSql [0].Value = ni.IsActivate;
                    _parSql [1] = new MySqlParameter ("?Title", MySqlDbType.String);
                    _parSql [1].Value = ni.Title;
                    _parSql [2] = new MySqlParameter ("?IconName", MySqlDbType.String);
                    _parSql [2].Value = ni.IconName;
                    _parSql [3] = new MySqlParameter ("?SoundName", MySqlDbType.String);
                    _parSql [3].Value = ni.SoundName;
                    ni.Id = _cn.ExecuteReturnLastInsert (_sql, _parSql);
                    //foreach (Mobitel mb in ni.Mobitels)
                    //{
                    //    _sql = "INSERT INTO ntf_mobitels (Id_main, MobitelId) VALUES (?Id_main, ?MobitelId)";
                    //    MySqlParameter[] _parSqlmob = new MySqlParameter[2];
                    //    _parSqlmob[0] = new MySqlParameter("?Id_main", MySqlDbType.Int32);
                    //    _parSqlmob[0].Value = ni.Id;
                    //    _parSqlmob[1] = new MySqlParameter("?Description", MySqlDbType.String);
                    //    _parSqlmob[1].Value = mb.Id;
                    //    _cn.ExecuteNonQueryCommand(_sql, _parSqlmob); 
                    //}
                    foreach (NoticeSubItem nsi in ni.NoticeSIs) {
                        _sql = "INSERT INTO ntf_events (Id_main, TypeEvent,ObjectId) VALUES (?Id_main, ?TypeEvent,?ObjectId)";
                        MySqlParameter[] _parSqlnsi = new MySqlParameter[2];
                        _parSqlnsi [0] = new MySqlParameter ("?Id_main", MySqlDbType.Int32);
                        _parSqlnsi [0].Value = ni.Id;
                        _parSqlnsi [1] = new MySqlParameter ("?TypeEvent", MySqlDbType.Int16);
                        _parSqlnsi [1].Value = nsi.TypeEvent;
                        _parSqlnsi [2] = new MySqlParameter ("?ObjectId", MySqlDbType.Int32);
                        _parSqlnsi [2].Value = nsi.Id_object;
                        nsi.Id = _cn.ExecuteReturnLastInsert (_sql, _parSqlnsi);
                        ;
                    }
                    return true;
                }
            } catch (Exception ex) {
                return false;
            }
        }
    }
}
