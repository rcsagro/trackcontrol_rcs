﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Agro.Dictionaries;
using TrackControl.General.DAL;
using TrackControl.General.DatabaseDriver;
using TrackControl.MySqlDal;
using TrackControl.Reports;
using TrackControl.Vehicles;

namespace TrackControl.Notification
{
    public static class NoticeVehicleProvider
    {
        public static List<Vehicle> GetVehicles(NoticeItem ni)
        {
            try
            {
                List<Vehicle> vhs = new List<Vehicle>();
                DriverDb db = new DriverDb();
                db.ConnectDb();
                {
                    string sSQL = string.Format(TrackControlQuery.NoticeProvider.SelectNtfMobitels, ni.Id);

                    GetVehiclesFromDataReder(vhs, db, sSQL);
                }

                db.CloseDbConnection();
                return vhs;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "GetVehicles(NoticeItem ni)");
                return null;
            }
        }

        public static List<Vehicle> GetVehiclesWithSensor(NoticeItem ni)
        {
            try
            {
                List<Vehicle> vhs = new List<Vehicle>();
                DriverDb db = new DriverDb();
                db.ConnectDb();
                {
                    string sSQL = string.Format(TrackControlQuery.NoticeProvider.SelectNtfMobitelsId_main,
                        ni.Id);

                    GetVehiclesFromDataReder(vhs, db, sSQL);
                }
                db.CloseDbConnection();
                return vhs;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "GetVehiclesWithSensor(NoticeItem ni)");
                return null;
            }
        }

        public static List<Vehicle> GetVehiclesWithoutSensor(NoticeItem ni)
        {
            try
            {
                List<Vehicle> vhs = new List<Vehicle>();
                DriverDb db = new DriverDb();
                db.ConnectDb();
                {
                    string sSQL = string.Format(TrackControlQuery.NoticeProvider.SelectNtfMobitelsSensorId, ni.Id);

                    GetVehiclesFromDataReder(vhs, db, sSQL);
                }
                db.CloseDbConnection();
                return vhs;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "GetVehiclesGetVehiclesWithoutSensor");
                return null;
            }
        }

        public static List<Vehicle> GetVehiclesFromAgroWorks(NoticeItem ni)
        {
            var vhs = new List<Vehicle>();
            if (ni.AgroWorks == null || ni.AgroWorks.Count == 0) return vhs;
            using (var db = new DriverDb())
            {
                db.ConnectDb();

                string sWhere = "";
                foreach (DictionaryAgroWorkType agroWork in ni.AgroWorks)
                {
                    if (sWhere.Length == 0)
                    {
                        sWhere = string.Format(" AND (agro_agregat.Id_work = {0}", agroWork.Id);
                    }
                    else
                    {
                        sWhere = string.Format("{0} OR agro_agregat.Id_work = {1}", sWhere, agroWork.Id);
                    }
                }
                if (sWhere.Length > 0) sWhere = string.Format("{0})", sWhere);
                string sSQL = string.Format("{0} {1}", TrackControlQuery.NoticeProvider.SelectNtfMobitelsFromAgroWorks,
                    sWhere);
                GetVehiclesAgroWorksFromDataReder(vhs, db, sSQL);
            }
            var vhsRfid = GetVehiclesFromSensorAlgorithm(ni, (int) AlgorithmType.AGREGAT);
            return GetConcatVehiclesList(vhs, vhsRfid);
        }

        private static List<Vehicle> GetVehiclesFromSensorAlgorithm(NoticeItem ni, int algorithm)
        {
            var vhs = new List<Vehicle>();
            if (ni.AgroWorks == null || ni.AgroWorks.Count == 0) return vhs;
            using (var db = new DriverDb())
            {
                db.ConnectDb();
                string sSql = string.Format(TrackControlQuery.NoticeProvider.SelectNtfMobitelsFromSensorAlgorithm,
                    algorithm);
                GetVehiclesSensorFromDataReder(vhs, db, sSql);
            }
            return vhs;

        }

        private static void GetVehiclesFromDataReder(List<Vehicle> vhs, DriverDb db, string sSQL)
        {
            db.GetDataReader(sSQL);
            //while (dr.Read())
            while (db.Read())
            {
                //int idMobitel = dr.GetInt32("MobitelId");
                int idMobitel = db.GetInt32("MobitelId");
                Vehicle vh = new Vehicle(idMobitel);
                //vh.VehicleUnloadSensor = new VehicleUnloadSensor(dr.GetInt32("SensorId")); 
                vh.Sensor = new Sensor(db.GetInt32("SensorId"));
                //vh.LogicSensor = new LogicSensor(dr.GetInt32("SensorLogicId")); 
                vh.LogicSensor = new LogicSensor(db.GetInt32("SensorLogicId"));

                SettingsProvider sProvider = new SettingsProvider(db);

                if (vh.Settings != null) // не включаем в список те машины у которых Settings == null
                {
                    sProvider.GetOne(vh.Settings);
                    vhs.Add(vh);
                }
            }
        }

        private static void GetVehiclesAgroWorksFromDataReder(List<Vehicle> vhs, DriverDb db, string sSQL)
        {
            db.GetDataReader(sSQL);
            while (db.Read())
            {
                int idMobitel = db.GetInt32("Mobitel_id");
                var vh = new Vehicle(idMobitel)
                {
                    IdAgroWorkType = db.GetInt32("Id_work"),
                    IdAgroAgregat = db.GetInt32("AgregatId")
                };
                vhs.Add(vh);
            }
        }

        private static void GetVehiclesSensorFromDataReder(List<Vehicle> vhs, DriverDb db, string sSQL)
        {
            db.GetDataReader(sSQL);
            while (db.Read())
            {
                int idMobitel = db.GetInt32("mobitel_id");
                var vh = new Vehicle(idMobitel)
                {
                    Mobitel = new Mobitel(idMobitel, "", ""),
                    Sensor = new Sensor(db.GetInt32("SensorId"))
                };
                vhs.Add(vh);
            }
        }

        private static List<Vehicle> GetConcatVehiclesList(List<Vehicle> vhsAgroWork, List<Vehicle> vhsRfid)
        {
            var concatVehs = new List<Vehicle>();
            if (vhsAgroWork.Any() && vhsRfid.Any())
            {
                foreach (var vhAgroWork in vhsAgroWork)
                {
                    foreach (var vhRfid in vhsRfid)
                    {
                        if (vhAgroWork.Id == vhRfid.Id) vhAgroWork.Sensor = vhRfid.Sensor;
                    }
                    concatVehs.Add(vhAgroWork);
                }
                foreach (var vhRfid in vhsRfid)
                {
                    if (concatVehs.All(concatVeh => concatVeh.Id != vhRfid.Id)) concatVehs.Add(vhRfid);
                    ;
                }

            }
            else if (vhsAgroWork.Any())
            {
                return vhsAgroWork;
            }
            else if (vhsRfid.Any())
            {
                return vhsRfid;
            }
            return concatVehs;
        }
    }
}
