﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using MySql.Data.MySqlClient;
using TrackControl.General;
using TrackControl.MySqlDal;
using TrackControl.Notification.Properties;
using TrackControl.Vehicles;
using TrackControl.General.DatabaseDriver;
using TrackControl.General.DAL;
using Agro.Dictionaries;
using TrackControl.Zones;

namespace TrackControl.Notification
{
    public static class NoticeProvider
    {
        public static event ProgressChanged StartLoading = delegate { };
        public static event ProgressChanged ProgressChanged = delegate { };
        public static event MethodInvoker EndLoading = delegate { };
        
        public static int InsertRecord(NoticeItem ni)
        {
            try
            {
                //using (ConnectMySQL cn = new ConnectMySQL())
                using (DriverDb db = new DriverDb())
                {
                    string sql = TrackControlQuery.NoticeProvider.InsertIntoNtf_Main;

                    //ni.Id = cn.ExecuteReturnLastInsert(sql, GetNoticeParameters(ni));

                    db.ConnectDb();
                    ni.Id = db.ExecuteReturnLastInsert(sql, GetNoticeParameters(ni), "ntf_main");

                    WriteSubItems(ni);
                    return ni.Id;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "InsertRecord");
                return ConstsGen.RECORD_MISSING;
            }
        }

        public static bool UpdateRecord(NoticeItem ni)
        {
            try
            {
                //using (ConnectMySQL cn = new ConnectMySQL())
                using (DriverDb db = new DriverDb())
                {
                    db.ConnectDb();
                    string sql = string.Format(TrackControlQuery.NoticeProvider.UpdateNtf_Main, ni.Id);

                    //cn.ExecuteNonQueryCommand(sql, GetNoticeParameters(ni));
                    db.ExecuteNonQueryCommand(sql, GetNoticeParameters(ni));
                    WriteSubItems(ni);
                    return true;
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
                return false;

            }
        }

        private static void WriteSubItems(NoticeItem ni)
        {
            using (var db = new DriverDb())
            {
                db.ConnectDb();
                if (!ni.IsTypeExistInSubItems<NoticeSpeedAgroWork>()) WriteVehicles(ni, db);
                WriteEmails(ni, db);
                WriteZones(ni, db);
                WriteStops(ni, db);
                WriteSpeeds(ni, db);
                WriteLossData(ni, db);
                WriteLossGps(ni, db);
                WriteLossPower(ni, db);
                WriteLogicSensor(ni, db);
                WriteSensor(ni, db);
                WriteTimeControl(ni, db);
                WriteFlowTypeSensor(ni, db);
                WritePopupUsers(ni, db);
                WriteFuelDischarge(ni, db);
                WriteVehileUnload(ni, db);
                WriteAgroWorks(ni, db);
                WriteSpeedAgroWorks(ni, db);
                db.CloseDbConnection();
            }
        }

        private static bool WriteVehicles(NoticeItem ni, DriverDb db)
        {
            try
            {
                if (ni.Id == ConstsGen.RECORD_MISSING)
                    return false;
                bool insertVehicle = false;
                //using (ConnectMySQL cn = new ConnectMySQL())

                string sql = String.Format(TrackControlQuery.NoticeProvider.DeleteFromNtf_mobitels, ni.Id);

                //cn.ExecuteNonQueryCommand(sql);
                db.ExecuteNonQueryCommand(sql);
                if (ni.Vehicles == null) return false;
                foreach (Vehicle vh in ni.Vehicles)
                {
                    if (vh.Id != ConstsGen.RECORD_MISSING)
                    {

                        sql = string.Format(TrackControlQuery.NoticeProvider.InsertIntoNtf_mobitels, db.ParamPrefics);

                        //MySqlParameter[] parSqlqlmob = new MySqlParameter[4];
                        db.NewSqlParameterArray(4);
                        //parSqlqlmob[0] = new MySqlParameter("?Id_main", ni.Id);
                        db.NewSqlParameter(db.ParamPrefics + "Id_main", ni.Id, 0);
                        //parSqlqlmob[1] = new MySqlParameter("?MobitelId", vh.Mobitel.Id);
                        db.NewSqlParameter(db.ParamPrefics + "MobitelId", vh.Mobitel.Id, 1);
                        //parSqlqlmob[2] = new MySqlParameter("?SensorLogicId", vh.LogicSensor == null ? 0 : vh.LogicSensor.Id);
                        db.NewSqlParameter(db.ParamPrefics + "SensorLogicId",
                            vh.LogicSensor == null ? 0 : vh.LogicSensor.Id, 2);
                        //parSqlqlmob[3] = new MySqlParameter("?SensorId", vh.VehicleUnloadSensor == null ? 0 : vh.VehicleUnloadSensor.Id);
                        db.NewSqlParameter(db.ParamPrefics + "SensorId", vh.Sensor == null ? 0 : vh.Sensor.Id, 3);

                        //cn.ExecuteNonQueryCommand(sql, parSqlqlmob);
                        db.ExecuteNonQueryCommand(sql, db.GetSqlParameterArray);

                        insertVehicle = true;
                    }
                }
                if (ni.VehiclesForAddFunction != null)
                {
                    foreach (Vehicle vh in ni.VehiclesForAddFunction)
                    {
                        if (vh.Id != ConstsGen.RECORD_MISSING)
                        {

                            sql = string.Format(TrackControlQuery.NoticeProvider.InsertIntoNtf_mobitels, db.ParamPrefics);

                            db.NewSqlParameterArray(4);
                            db.NewSqlParameter(db.ParamPrefics + "Id_main", ni.Id, 0);
                            db.NewSqlParameter(db.ParamPrefics + "MobitelId", vh.Mobitel.Id, 1);
                            db.NewSqlParameter(db.ParamPrefics + "SensorLogicId", 0, 2);
                            db.NewSqlParameter(db.ParamPrefics + "SensorId", 0, 3);
                            db.ExecuteNonQueryCommand(sql, db.GetSqlParameterArray);
                            insertVehicle = true;
                        }
                    }
                }
                return insertVehicle;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "WriteVehicles");
                return false;
            }
        }

        public static bool WriteZones(NoticeItem ni, DriverDb db)
        {
            try
            {
                if (ni.Id == ConstsGen.RECORD_MISSING)
                    return false;
                //using (ConnectMySQL cn = new ConnectMySQL())
                string sql = String.Format(TrackControlQuery.NoticeProvider.DeleteFromNtf_events, ni.Id,
                    (int) NoticeSubItemTypes.CheckZoneControl);

                //cn.ExecuteNonQueryCommand(sql);
                db.ExecuteNonQueryCommand(sql);

                ni.NoticeSIs.ForEach(delegate(NoticeSubItem nsi)
                {
                    if ((nsi is NoticeCheckZone))
                    {

                        sql = string.Format(TrackControlQuery.NoticeProvider.InsertIntoNtf_events, db.ParamPrefics);

                        //MySqlParameter[] parSql = new MySqlParameter[4];
                        db.NewSqlParameterArray(4);
                        //parSql[0] = new MySqlParameter("?Id_main", MySqlDbType.Int32) {Value = ni.Id};
                        db.NewSqlParameter(db.ParamPrefics + "Id_main", db.GettingInt32(), 0);
                        db.SetSqlParameterValue((int) ni.Id, 0);

                        //parSql[1] = new MySqlParameter("?TypeEvent", MySqlDbType.Int16)
                        //                {Value = (int) NoticeSubItemTypes.ChackZoneControl};
                        db.NewSqlParameter(db.ParamPrefics + "TypeEvent", db.GettingInt16(), 1);
                        db.SetSqlParameterValue((Int16) NoticeSubItemTypes.CheckZoneControl, 1);

                        //parSql[2] = new MySqlParameter("?EventId", MySqlDbType.Int32)
                        //                {Value = ((NoticeCheckZone) nsi).Zone_Id};
                        db.NewSqlParameter(db.ParamPrefics + "EventId", db.GettingInt32(), 2);
                        db.SetSqlParameterValue((int) ((NoticeCheckZone) nsi).Zone_Id, 2);

                        //parSql[3] = new MySqlParameter("?ParInt1", MySqlDbType.Int32)
                        //                {Value = ((NoticeCheckZone) nsi).ZoneEventType};
                        db.NewSqlParameter(db.ParamPrefics + "ParInt1", db.GettingInt32(), 3);
                        db.SetSqlParameterValue((int) ((NoticeCheckZone) nsi).ZoneEventType, 3);

                        //nsi.Id = cn.ExecuteReturnLastInsert(sql, parSql);
                        nsi.Id = db.ExecuteReturnLastInsert(sql, db.GetSqlParameterArray, "ntf_events");
                    }
                    else
                    {
                    }
                });
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "WriteZones");
                return false;
            }
        }

        public static bool WriteStops(NoticeItem ni, DriverDb db)
        {
            try
            {
                if (ni.Id == ConstsGen.RECORD_MISSING)
                    return false;
                //using (ConnectMySQL cn = new ConnectMySQL())
                string sql = String.Format(TrackControlQuery.NoticeProvider.DeleteNtf_events, ni.Id,
                    (int) NoticeSubItemTypes.StopControl);

                //cn.ExecuteNonQueryCommand(sql);
                db.ExecuteNonQueryCommand(sql);

                ni.NoticeSIs.ForEach(delegate(NoticeSubItem nstop)
                {
                    if (nstop is NoticeStop)
                    {

                        sql = string.Format(TrackControlQuery.NoticeProvider.InsertNtf_events, db.ParamPrefics);

                        //MySqlParameter[] parSql = new MySqlParameter[3];
                        db.NewSqlParameterArray(3);
                        //parSql[0] = new MySqlParameter("?Id_main", MySqlDbType.Int32) {Value = ni.Id};
                        db.NewSqlParameter(db.ParamPrefics + "Id_main", db.GettingInt32(), 0);
                        db.SetSqlParameterValue(ni.Id, 0);
                        //parSql[1] = new MySqlParameter("?TypeEvent", MySqlDbType.Int16)
                        //                {Value = (int) NoticeSubItemTypes.StopControl};
                        db.NewSqlParameter(db.ParamPrefics + "TypeEvent", db.GettingInt16(), 1);
                        db.SetSqlParameterValue((int) NoticeSubItemTypes.StopControl, 1);

                        //parSql[2] = new MySqlParameter("?ParDbl1", MySqlDbType.Double)
                        //              {Value = ((NoticeStop) nstop).Stop_time};
                        db.NewSqlParameter(db.ParamPrefics + "ParDbl1", db.GettingDouble(), 2);
                        db.SetSqlParameterValue(((NoticeStop) nstop).StopTime, 2);

                        //nstop.Id = cn.ExecuteReturnLastInsert(sql, parSql);
                        nstop.Id = db.ExecuteReturnLastInsert(sql, db.GetSqlParameterArray, "ntf_events");
                    }
                });
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

        public static bool WriteLossData(NoticeItem ni, DriverDb db)
        {
            try
            {
                if (ni.Id == ConstsGen.RECORD_MISSING)
                    return false;
                //using (ConnectMySQL cn = new ConnectMySQL())
                string sql = String.Format(TrackControlQuery.NoticeProvider.DeleteNtfEventsIdMain, ni.Id,
                    (int) NoticeSubItemTypes.LossData);
                //cn.ExecuteNonQueryCommand(sql);
                db.ExecuteNonQueryCommand(sql);
                ni.NoticeSIs.ForEach(delegate(NoticeSubItem nldata)
                {
                    if (nldata is NoticeLossData)
                    {

                        sql = string.Format(TrackControlQuery.NoticeProvider.InsertNtf_events, db.ParamPrefics);

                        //MySqlParameter[] parSql = new MySqlParameter[3];
                        db.NewSqlParameterArray(3);
                        //parSql[0] = new MySqlParameter("?Id_main", MySqlDbType.Int32) {Value = ni.Id};
                        db.NewSqlParameter(db.ParamPrefics + "Id_main", db.GettingInt32(), 0);
                        db.SetSqlParameterValue(ni.Id, 0);
                        //parSql[1] = new MySqlParameter("?TypeEvent", MySqlDbType.Int16)
                        //                {Value = (int) NoticeSubItemTypes.LossData};
                        db.NewSqlParameter(db.ParamPrefics + "TypeEvent", db.GettingInt16(), 1);
                        db.SetSqlParameterValue((int) NoticeSubItemTypes.LossData, 1);
                        //parSql[2] = new MySqlParameter("?ParDbl1", MySqlDbType.Double)
                        //                {Value = ((NoticeLossData) nldata).Loss_time};
                        db.NewSqlParameter(db.ParamPrefics + "ParDbl1", db.GettingDouble(), 2);
                        db.SetSqlParameterValue(((NoticeLossData) nldata).Loss_time, 2);
                        //nldata.Id = cn.ExecuteReturnLastInsert(sql, parSql);
                        nldata.Id = db.ExecuteReturnLastInsert(sql, db.GetSqlParameterArray, "ntf_events");
                    }
                });
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

        public static bool WriteLossGps(NoticeItem ni, DriverDb db)
        {
            try
            {
                if (ni.Id == ConstsGen.RECORD_MISSING)
                    return false;
                //using (ConnectMySQL cn = new ConnectMySQL())

                string sql = String.Format(TrackControlQuery.NoticeProvider.DeleteNtf_events, ni.Id,
                    (int) NoticeSubItemTypes.LossGps);

                //cn.ExecuteNonQueryCommand(sql);
                db.ExecuteNonQueryCommand(sql);
                ni.NoticeSIs.ForEach(delegate(NoticeSubItem nlgps)
                {
                    if (nlgps is NoticeLossGPS)
                    {

                        sql = string.Format(TrackControlQuery.NoticeProvider.InsertNtf_events, db.ParamPrefics);

                        //MySqlParameter[] parSql = new MySqlParameter[3];
                        db.NewSqlParameterArray(3);
                        //parSql[0] = new MySqlParameter("?Id_main", MySqlDbType.Int32) {Value = ni.Id};
                        db.NewSqlParameter(db.ParamPrefics + "Id_main", db.GettingInt32(), 0);
                        db.SetSqlParameterValue(ni.Id, 0);
                        //parSql[1] = new MySqlParameter("?TypeEvent", MySqlDbType.Int16)
                        //                {Value = (int) NoticeSubItemTypes.LossGPS};
                        db.NewSqlParameter(db.ParamPrefics + "TypeEvent", db.GettingInt16(), 1);
                        db.SetSqlParameterValue((int) NoticeSubItemTypes.LossGps, 1);
                        //parSql[2] = new MySqlParameter("?ParDbl1", MySqlDbType.Double)
                        //                {Value = ((NoticeLossGPS) nlgps).Loss_time};
                        db.NewSqlParameter(db.ParamPrefics + "ParDbl1", db.GettingDouble(), 2);
                        db.SetSqlParameterValue(((NoticeLossGPS) nlgps).Loss_time, 2);
                        //nlgps.Id = cn.ExecuteReturnLastInsert(sql, parSql);
                        nlgps.Id = db.ExecuteReturnLastInsert(sql, db.GetSqlParameterArray, "ntf_events");
                    }
                });
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

        public static bool WriteLossPower(NoticeItem ni, DriverDb db)
        {
            try
            {
                if (ni.Id == ConstsGen.RECORD_MISSING)
                    return false;
                //using (ConnectMySQL cn = new ConnectMySQL())
                string sql = String.Format(TrackControlQuery.NoticeProvider.DeleteNtf_events, ni.Id,
                    (int) NoticeSubItemTypes.LossPower);
                //cn.ExecuteNonQueryCommand(sql);
                db.ExecuteNonQueryCommand(sql);
                ni.NoticeSIs.ForEach(delegate(NoticeSubItem nlpower)
                {
                    if (nlpower is NoticeLossPower)
                    {

                        sql = string.Format(TrackControlQuery.NoticeProvider.InsertNtf_events, db.ParamPrefics);

                        //MySqlParameter[] parSql = new MySqlParameter[3];
                        db.NewSqlParameterArray(3);
                        //parSql[0] = new MySqlParameter("?Id_main", ni.Id);
                        db.NewSqlParameter(db.ParamPrefics + "Id_main", ni.Id, 0);
                        //parSql[1] = new MySqlParameter("?TypeEvent", (int)NoticeSubItemTypes.LossPower);
                        db.NewSqlParameter(db.ParamPrefics + "TypeEvent", (int) NoticeSubItemTypes.LossPower, 1);
                        //parSql[2] = new MySqlParameter("?ParDbl1", ((NoticeLossPower)nlpower).Loss_time);
                        db.NewSqlParameter(db.ParamPrefics + "ParDbl1", ((NoticeLossPower) nlpower).Loss_time, 2);
                        //nlpower.Id = cn.ExecuteReturnLastInsert(sql, parSql);
                        nlpower.Id = db.ExecuteReturnLastInsert(sql, db.GetSqlParameterArray, "ntf_events");
                    }
                });
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "WriteLossPower");
                return false;
            }
        }

        public static bool WriteLogicSensor(NoticeItem ni, DriverDb db)
        {
            try
            {
                if (ni.Id == ConstsGen.RECORD_MISSING)
                    return false;
                //using (ConnectMySQL cn = new ConnectMySQL())
                string sql = String.Format(TrackControlQuery.NoticeProvider.DeleteNtf_events, ni.Id,
                    (int) NoticeSubItemTypes.SensorLogicControl);
                //cn.ExecuteNonQueryCommand(sql);
                db.ExecuteNonQueryCommand(sql);
                ni.NoticeSIs.ForEach(delegate(NoticeSubItem nsl)
                {
                    if (nsl is NoticeLogicSensor)
                    {
                        sql = string.Format(TrackControlQuery.NoticeProvider.Insert_Into_Ntf_Events, db.ParamPrefics);
                        //MySqlParameter[] parSql = new MySqlParameter[3];
                        db.NewSqlParameterArray(3);
                        //parSql[0] = new MySqlParameter("?Id_main", ni.Id);
                        db.NewSqlParameter(db.ParamPrefics + "Id_main", ni.Id, 0);
                        //parSql[1] = new MySqlParameter("?TypeEvent", (int)NoticeSubItemTypes.SensorLogicControl);
                        db.NewSqlParameter(db.ParamPrefics + "TypeEvent", (int) NoticeSubItemTypes.SensorLogicControl, 1);
                        //parSql[2] = new MySqlParameter("?ParBool", ((NoticeLogicSensor)nsl).Event_value);
                        db.NewSqlParameter(db.ParamPrefics + "ParBool", ((NoticeLogicSensor) nsl).EventValue, 2);
                        //nsl.Id = cn.ExecuteReturnLastInsert(sql, parSql);
                        nsl.Id = db.ExecuteReturnLastInsert(sql, db.GetSqlParameterArray, "ntf_events");
                    }
                });
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

        public static bool WriteSpeedAgroWorks(NoticeItem ni, DriverDb db)
        {
            try
            {
                if (ni.Id == ConstsGen.RECORD_MISSING)
                    return false;
                string sql = String.Format(TrackControlQuery.NoticeProvider.DeleteNtf_events, ni.Id,
                    (int)NoticeSubItemTypes.SpeedAgroWorksControl);
                db.ExecuteNonQueryCommand(sql);
                ni.NoticeSIs.ForEach(delegate(NoticeSubItem nsl)
                {
                    if (nsl is NoticeSpeedAgroWork)
                    {
                        sql = string.Format(TrackControlQuery.NoticeProvider.Insert_Into_Ntf_Events_WithoutParams, db.ParamPrefics);
                        db.NewSqlParameterArray(2);
                        db.NewSqlParameter(db.ParamPrefics + "Id_main", ni.Id, 0);
                        db.NewSqlParameter(db.ParamPrefics + "TypeEvent", (int)NoticeSubItemTypes.SpeedAgroWorksControl, 1);
                        nsl.Id = db.ExecuteReturnLastInsert(sql, db.GetSqlParameterArray, "ntf_events");
                    }
                });
                return true;}
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

        public static bool WriteSensor(NoticeItem ni, DriverDb db)
        {
            try
            {
                if (ni.Id == ConstsGen.RECORD_MISSING)
                    return false;
                //using (ConnectMySQL cn = new ConnectMySQL())
                string sql = String.Format(TrackControlQuery.NoticeProvider.DeleteNtf_events, ni.Id,
                    (int) NoticeSubItemTypes.SensorControl);
                //cn.ExecuteNonQueryCommand(sql);
                db.ExecuteNonQueryCommand(sql);
                ni.NoticeSIs.ForEach(delegate(NoticeSubItem ns)
                {
                    if (ns is NoticeSensor)
                    {

                        sql = string.Format(TrackControlQuery.NoticeProvider.Insert_Into_Ntf_Events_Id, db.ParamPrefics);

                        //MySqlParameter[] parSql = new MySqlParameter[5];
                        db.NewSqlParameterArray(5);
                        //parSql[0] = new MySqlParameter("?Id_main", ni.Id);
                        db.NewSqlParameter(db.ParamPrefics + "Id_main", ni.Id, 0);
                        //parSql[1] = new MySqlParameter("?TypeEvent", (int)NoticeSubItemTypes.SensorControl);
                        db.NewSqlParameter(db.ParamPrefics + "TypeEvent", (int) NoticeSubItemTypes.SensorControl, 1);
                        //parSql[2] = new MySqlParameter("?ParBool", ((NoticeSensor)ns).InRange);
                        db.NewSqlParameter(db.ParamPrefics + "ParBool", ((NoticeSensor) ns).InRange, 2);
                        //parSql[3] = new MySqlParameter("?ParDbl1", ((NoticeSensor)ns).ValueBottom);
                        db.NewSqlParameter(db.ParamPrefics + "ParDbl1", ((NoticeSensor) ns).ValueBottom, 3);
                        //parSql[4] = new MySqlParameter("?ParDbl2", ((NoticeSensor)ns).ValueTop);
                        db.NewSqlParameter(db.ParamPrefics + "ParDbl2", ((NoticeSensor) ns).ValueTop, 4);
                        //ns.Id = cn.ExecuteReturnLastInsert(sql, parSql);
                        ns.Id = db.ExecuteReturnLastInsert(sql, db.GetSqlParameterArray, "ntf_events");
                    }
                });
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

        public static bool WriteSpeeds(NoticeItem ni, DriverDb db)
        {
            try
            {
                if (ni.Id == ConstsGen.RECORD_MISSING)
                    return false;
                //using (ConnectMySQL cn = new ConnectMySQL())

                string sql = String.Format(TrackControlQuery.NoticeProvider.DeleteNtf_events, ni.Id,
                    (int) NoticeSubItemTypes.SpeedControl);

                //cn.ExecuteNonQueryCommand(sql);
                db.ExecuteNonQueryCommand(sql);
                ni.NoticeSIs.ForEach(delegate(NoticeSubItem nspeed)
                {
                    if (nspeed is NoticeSpeed)
                    {

                        sql = string.Format(TrackControlQuery.NoticeProvider.InsertIntoNtfEventsValue, db.ParamPrefics);

                        //MySqlParameter[] parSql = new MySqlParameter[4];
                        db.NewSqlParameterArray(4);
                        //parSql[0] = new MySqlParameter("?Id_main", MySqlDbType.Int32) {Value = ni.Id};
                        db.NewSqlParameter(db.ParamPrefics + "Id_main", db.GettingInt32(), 0);
                        db.SetSqlParameterValue(ni.Id, 0);
                        //parSql[1] = new MySqlParameter("?TypeEvent", MySqlDbType.Int16)
                        //                {Value = (int) NoticeSubItemTypes.SpeedControl};
                        db.NewSqlParameter(db.ParamPrefics + "TypeEvent", db.GettingInt16(), 1);
                        db.SetSqlParameterValue((int) NoticeSubItemTypes.SpeedControl, 1);
                        //parSql[2] = new MySqlParameter("?ParDbl1", MySqlDbType.Double)
                        //                {Value = ((NoticeSpeed) nspeed).Speed_min};
                        db.NewSqlParameter(db.ParamPrefics + "ParDbl1", db.GettingDouble(), 2);
                        db.SetSqlParameterValue(((NoticeSpeed) nspeed).SpeedMin, 2);
                        //parSql[3] = new MySqlParameter("?ParDbl2", MySqlDbType.Double)
                        //                {Value = ((NoticeSpeed) nspeed).Speed_max};
                        db.NewSqlParameter(db.ParamPrefics + "ParDbl2", db.GettingDouble(), 3);
                        db.SetSqlParameterValue(((NoticeSpeed) nspeed).SpeedMax, 3);
                        //nspeed.Id = cn.ExecuteReturnLastInsert(sql, parSql);
                        db.ExecuteReturnLastInsert(sql, db.GetSqlParameterArray, "ntf_events");
                    }
                });
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

        public static bool WriteFlowTypeSensor(NoticeItem ni, DriverDb db)
        {
            try
            {
                if (ni.Id == ConstsGen.RECORD_MISSING)
                    return false;

                string sql = String.Format(TrackControlQuery.NoticeProvider.DeleteNtf_events, ni.Id,
                    (int) NoticeSubItemTypes.SensorFlowTypeControl);

                db.ExecuteNonQueryCommand(sql);
                ni.NoticeSIs.ForEach(delegate(NoticeSubItem sensorFlowType)
                {
                    if (sensorFlowType is NoticeFlowTypeSensor)
                    {

                        sql = string.Format(TrackControlQuery.NoticeProvider.InsertIntoNtfEventsParam,
                            db.ParamPrefics);

                        db.NewSqlParameterArray(7);
                        db.SetNewSqlParameter(db.ParamPrefics + "Id_main", ni.Id);
                        db.SetNewSqlParameter(db.ParamPrefics + "TypeEvent",
                            (int) NoticeSubItemTypes.SensorFlowTypeControl);
                        db.SetNewSqlParameter(db.ParamPrefics + "ParInt1",
                            ((NoticeFlowTypeSensor) sensorFlowType).SearchRadius);
                        db.SetNewSqlParameter(db.ParamPrefics + "ParInt2",
                            ((NoticeFlowTypeSensor) sensorFlowType).MinActionTime);
                        db.SetNewSqlParameter(db.ParamPrefics + "ParDbl1",
                            (Double) ((NoticeFlowTypeSensor) sensorFlowType).SearchTime);
                        db.SetNewSqlParameter(db.ParamPrefics + "ParDbl2",
                            (Double) ((NoticeFlowTypeSensor) sensorFlowType).TimeForBreakSearch);
                        db.SetNewSqlParameter(db.ParamPrefics + "ParBool",
                            (bool) ((NoticeFlowTypeSensor) sensorFlowType).IsDutSearch);
                        db.ExecuteReturnLastInsert(sql, db.GetSqlParameterArray, "ntf_events");
                    }
                });
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

        public static bool WriteTimeControl(NoticeItem ni, DriverDb db)
        {
            try
            {
                if (ni.Id == ConstsGen.RECORD_MISSING)
                    return false;
                //using (ConnectMySQL cn = new ConnectMySQL())

                string sql = String.Format(TrackControlQuery.NoticeProvider.DeleteNtf_events, ni.Id,
                    (int) NoticeSubItemTypes.TimeControl);

                //cn.ExecuteNonQueryCommand(sql);
                db.ExecuteNonQueryCommand(sql);
                ni.NoticeSIs.ForEach(delegate(NoticeSubItem nTime)
                {
                    if (nTime is NoticeTimeControl)
                    {

                        sql = string.Format(TrackControlQuery.NoticeProvider.InsertIntoNtfEventsTypeEvent,
                            db.ParamPrefics);
                        //MySqlParameter[] parSql = new MySqlParameter[3];
                        db.NewSqlParameterArray(3);
                        //parSql[0] = new MySqlParameter("?Id_main", ni.Id);
                        db.NewSqlParameter(db.ParamPrefics + "Id_main", ni.Id, 0);
                        //parSql[1] = new MySqlParameter("?TypeEvent", (int)NoticeSubItemTypes.TimeControl);
                        db.NewSqlParameter(db.ParamPrefics + "TypeEvent", (int) NoticeSubItemTypes.TimeControl, 1);
                        //parSql[2] = new MySqlParameter("?ParDbl1", ((NoticeTimeControl)nTime).TimeForContrrol);
                        db.NewSqlParameter(db.ParamPrefics + "ParDbl1", ((NoticeTimeControl) nTime).TimeForContrrol, 2);
                        //nTime.Id = cn.ExecuteReturnLastInsert(sql, parSql);
                        nTime.Id = db.ExecuteReturnLastInsert(sql, db.GetSqlParameterArray, "ntf_events");
                    }
                });

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

        public static bool WriteFuelDischarge(NoticeItem ni, DriverDb db)
        {
            try
            {
                if (ni.Id == ConstsGen.RECORD_MISSING)
                    return false;
                //using (ConnectMySQL cn = new ConnectMySQL())

                string sql = String.Format(TrackControlQuery.NoticeProvider.DeleteNtf_events, ni.Id,
                    (int) NoticeSubItemTypes.FuelDischarge);

                //cn.ExecuteNonQueryCommand(sql);
                db.ExecuteNonQueryCommand(sql);
                ni.NoticeSIs.ForEach(delegate(NoticeSubItem fuelDisch)
                {
                    if (fuelDisch is NoticeFuelDischarge)
                    {

                        sql = string.Format(TrackControlQuery.NoticeProvider.InsertIntoNtfEventsParDbl, db.ParamPrefics);

                        db.NewSqlParameterArray(5);
                        db.NewSqlParameter(db.ParamPrefics + "Id_main", ni.Id, 0);
                        db.NewSqlParameter(db.ParamPrefics + "TypeEvent", (int) NoticeSubItemTypes.FuelDischarge, 1);
                        db.NewSqlParameter(db.ParamPrefics + "ParDbl1", ((NoticeFuelDischarge) fuelDisch).Threshold, 2);
                        db.NewSqlParameter(db.ParamPrefics + "ParDbl2", ((NoticeFuelDischarge) fuelDisch).AvgFuelExpense,
                            3);
                        db.NewSqlParameter(db.ParamPrefics + "ParInt1", ((NoticeFuelDischarge) fuelDisch).ZeroPointCount,
                            4);
                        fuelDisch.Id = db.ExecuteReturnLastInsert(sql, db.GetSqlParameterArray, "ntf_events");
                    }
                });
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "WriteFuelDischarge");
                return false;
            }
        }

        public static bool WriteVehileUnload(NoticeItem ni, DriverDb db)
        {
            try
            {
                if (ni.Id == ConstsGen.RECORD_MISSING)
                    return false;

                string sql = String.Format(TrackControlQuery.NoticeProvider.DeleteNtf_events, ni.Id,
                    (int)NoticeSubItemTypes.VehicleUnload);

                db.ExecuteNonQueryCommand(sql);
                ni.NoticeSIs.ForEach(delegate(NoticeSubItem vehUnload)
                {
                    if (vehUnload is NoticeVehicleUnload)
                    {

                        sql = string.Format(TrackControlQuery.NoticeProvider.InsertIntoNtf_events, db.ParamPrefics);

                        db.NewSqlParameterArray(4);
                        db.NewSqlParameter(db.ParamPrefics + "Id_main", ni.Id, 0);
                        db.NewSqlParameter(db.ParamPrefics + "TypeEvent", (int)NoticeSubItemTypes.VehicleUnload, 1);
                        db.NewSqlParameter(db.ParamPrefics + "EventId", 0, 2);
                        db.NewSqlParameter(db.ParamPrefics + "ParInt1", ((NoticeVehicleUnload)vehUnload).TimeMaxUnload,
                            3);
                        vehUnload.Id = db.ExecuteReturnLastInsert(sql, db.GetSqlParameterArray, "ntf_events");
                    }
                });
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "WriteVehicleUnload");
                return false;
            }
        }

        public static DataTable GetNoticeList()
        {
            //using (ConnectMySQL cn = new ConnectMySQL())
            DriverDb db = new DriverDb();
            db.ConnectDb();
            {
                string sql = string.Format(TrackControlQuery.NoticeProvider.Select_ntf_main_Id,
                    (int) NoticeSubItemTypes.CheckZoneControl, (int) NoticeSubItemTypes.SpeedControl,
                    (int) NoticeSubItemTypes.StopControl, (int) NoticeSubItemTypes.LossData,
                    (int) NoticeSubItemTypes.LossGps);

                //return cn.GetDataTable(sql);
                return db.GetDataTable(sql);
            }
        }

        public static List<NoticeItem> GetNoticeEntityListForAnaliz()
        {
            List<NoticeItem> nis = new List<NoticeItem>();
            //using (ConnectMySQL cn = new ConnectMySQL())
            DriverDb db = new DriverDb();
            db.ConnectDb();
            {
                string sql = TrackControlQuery.NoticeProvider.SelectNtfMainFromNtfMain;

                //MySqlDataReader dr = cn.GetDataReader(sql);
                db.GetDataReader(sql);
                //while (dr.Read())
                while (db.Read())
                {
                    int idNotice;
                    //if (Int32.TryParse(dr.GetInt32("Id").ToString(), out idNotice))
                    if (Int32.TryParse(db.GetInt32("Id").ToString(), out idNotice))
                    {
                        NoticeItem ni = new NoticeItem(idNotice, NoticeItem.LoadRegime.EventsZonesFullVehAnaliz);
                        nis.Add(ni);
                    }
                    Application.DoEvents();
                }
                //dr.Close(); 
                db.CloseDataReader();
            }
            db.CloseDbConnection();
            return nis;
        }

        public static List<NoticeItem> GetNoticeEntityListForAnalizOneQuery()
        {
            var nis = new List<NoticeItem>();

            try
            {
                //using (ConnectMySQL cn = new ConnectMySQL())
                var db = new DriverDb();
                int counter = 0;
                db.ConnectDb();
                {
                    string sql = TrackControlQuery.NoticeProvider.SelectFromNtfMainIsActive;
                    int maxRecords = db.GetDataReaderRecordsCount(sql);
                    sql = TrackControlQuery.NoticeProvider.SelectFromNtfMainIsActiveOrderBy;
                    StartLoading(Resources.ConfiguringNotifications, "", maxRecords + 1);
                    Application.DoEvents();
                    //MySqlDataReader dr = cn.GetDataReader(sql);
                    db.GetDataReader(sql);
                    //while (dr.Read())
                    while (db.Read())
                    {
                        int idNotice;
                        //if (Int32.TryParse(dr.GetInt32("Id").ToString(), out idNotice))
                        if (Int32.TryParse(db.GetInt32("Id").ToString(), out idNotice))
                        {
                            counter++;
                            ProgressChanged(Resources.ConfiguringNotifications, db.GetString("Title") ?? "", counter);
                            Application.DoEvents();
                            var ni = new NoticeItem {Id = idNotice, Title = db.GetString("Title") ?? ""};
                            //ni.Title = dr.GetString("Title") ?? "";

                            //if (dr["SoundName"] != DBNull.Value)
                            //ni.SoundName = dr.GetString("SoundName") ?? "";
                            if (db["SoundName"] != DBNull.Value)
                                ni.SoundName = db.GetString("SoundName") ?? "";

                            //if (dr["IconSmall"] != DBNull.Value)
                            //    ni.IconSmall = (byte[])dr["IconSmall"];
                            if (db["IconSmall"] != DBNull.Value)
                                ni.IconSmall = (byte[]) db["IconSmall"];

                            //if (dr["IconLarge"] != DBNull.Value)
                            //    ni.IconLarge = (byte[])dr["IconLarge"];
                            if (db["IconLarge"] != DBNull.Value)
                                ni.IconLarge = (byte[]) db["IconLarge"];

                            //ni.IsActive = dr.GetBoolean("IsActive");
                            ni.IsActive = db.GetBoolean("IsActive");

                            //ni.IsEmailActive = dr.GetBoolean("IsEmailActive");
                            ni.IsEmailActive = db.GetBoolean("IsEmailActive");

                            //ni.IsPopup = dr.GetBoolean("IsPopup");
                            ni.IsPopup = db.GetBoolean("IsPopup");

                            //if (dr["TimeForControl"] != DBNull.Value)
                            //    ni.TimeForControl = dr.GetInt32("TimeForControl");
                            if (db["TimeForControl"] != DBNull.Value)
                                ni.TimeForControl = db.GetDouble("TimeForControl");
                            ni.Period = db.GetInt32("Period");
                            ni.SetSubItems(NoticeItem.LoadRegime.EventsZonesFullVehAnaliz);
                            ni.Emails = GetNoticeEmails(ni);
                            ni.PopupUsers = GetNoticePopupUsers(ni);
                            nis.Add(ni);
                        }
                    }
                    //dr.Close();
                    db.CloseDataReader();
                }
                db.CloseDbConnection();
                EndLoading();
                Application.DoEvents();
                return nis;
            }
            catch (Exception e)
            {
                XtraMessageBox.Show(e.Message + "\n" + e.StackTrace,
                    "NoticeProvider.GetNoticeEntityListForAnalizOneQuery", MessageBoxButtons.OK);
                return nis;
            }
        }

        private static object[] GetNoticeParameters(NoticeItem ni)
        {
            //MySqlParameter[] parSqlql = new MySqlParameter[8];
            DriverDb db = new DriverDb();
            db.NewSqlParameterArray(9);
            //parSqlql[0] = new MySqlParameter("?IsActive", ni.IsActive);
            db.NewSqlParameter(db.ParamPrefics + "IsActive", ni.IsActive, 0);
            //parSqlql[1] = new MySqlParameter("?IsPopup", ni.IsPopup);
            db.NewSqlParameter(db.ParamPrefics + "IsPopup", ni.IsPopup, 1);
            //parSqlql[2] = new MySqlParameter("?Title", ni.Title);
            db.NewSqlParameter(db.ParamPrefics + "Title", ni.Title, 2);
            //parSqlql[3] = new MySqlParameter("?IconSmall",  ni.IconSmall);
            db.NewSqlParameter(db.ParamPrefics + "IconSmall", ni.IconSmall, 3);
            //parSqlql[4] = new MySqlParameter("?IconLarge", ni.IconLarge);
            db.NewSqlParameter(db.ParamPrefics + "IconLarge", ni.IconLarge, 4);
            //parSqlql[5] = new MySqlParameter("?SoundName", ni.SoundName);
            db.NewSqlParameter(db.ParamPrefics + "SoundName", ni.SoundName, 5);
            //parSqlql[6] = new MySqlParameter("?TimeForControl", ni.TimeForControl);
            db.NewSqlParameter(db.ParamPrefics + "TimeForControl", ni.TimeForControl, 6);
            //parSqlql[7] = new MySqlParameter("?IsEmailActive", ni.IsEmailActive);
            db.NewSqlParameter(db.ParamPrefics + "IsEmailActive", ni.IsEmailActive, 7);
            db.NewSqlParameter(db.ParamPrefics + "Period", ni.Period, 8);
            //return parSqlql;
            return db.GetSqlParameterArray;
        }

        private static DriverDb GetNoticeParametersDb(NoticeItem ni)
        {
            //MySqlParameter[] parSqlql = new MySqlParameter[8];
            DriverDb db = new DriverDb();
            db.NewSqlParameterArray(8);
            //parSqlql[0] = new MySqlParameter("?IsActive", ni.IsActive);
            db.NewSqlParameter(db.ParamPrefics + "IsActive", ni.IsActive, 0);
            //parSqlql[1] = new MySqlParameter("?IsPopup", ni.IsPopup);
            db.NewSqlParameter(db.ParamPrefics + "IsPopup", ni.IsPopup, 1);
            //parSqlql[2] = new MySqlParameter("?Title", ni.Title);
            db.NewSqlParameter(db.ParamPrefics + "Title", ni.Title, 2);
            //parSqlql[3] = new MySqlParameter("?IconSmall",  ni.IconSmall);
            db.NewSqlParameter(db.ParamPrefics + "IconSmall", ni.IconSmall, 3);
            //parSqlql[4] = new MySqlParameter("?IconLarge", ni.IconLarge);
            db.NewSqlParameter(db.ParamPrefics + "IconLarge", ni.IconLarge, 4);
            //parSqlql[5] = new MySqlParameter("?SoundName", ni.SoundName);
            db.NewSqlParameter(db.ParamPrefics + "SoundName", ni.SoundName, 5);
            //parSqlql[6] = new MySqlParameter("?TimeForControl", ni.TimeForControl);
            db.NewSqlParameter(db.ParamPrefics + "TimeForControl", ni.TimeForControl, 6);
            //parSqlql[7] = new MySqlParameter("?IsEmailActive", ni.IsEmailActive);
            db.NewSqlParameter(db.ParamPrefics + "IsEmailActive", ni.IsEmailActive, 7);
            //return parSqlql;
            return db;
        }

        public static bool DeleteRecord(int idNotice)
        {
            DriverDb db = new DriverDb();
            try
            {
                //using (ConnectMySQL cn = new ConnectMySQL())

                db.ConnectDb();
                {
                    string sSql = string.Format(TrackControlQuery.NoticeProvider.DeleteFromNtfLog, idNotice);
                    db.ExecuteNonQueryCommand(sSql);

                    sSql = string.Format(TrackControlQuery.NoticeProvider.DeleteFromNtfEvents, idNotice);
                    db.ExecuteNonQueryCommand(sSql);

                    sSql = string.Format(TrackControlQuery.NoticeProvider.DeleteFromNtfEmails, idNotice);
                    db.ExecuteNonQueryCommand(sSql);

                    sSql = string.Format(TrackControlQuery.NoticeProvider.DeleteFromNtfMobitels, idNotice);
                    db.ExecuteNonQueryCommand(sSql);

                    sSql = string.Format(TrackControlQuery.NoticeProvider.DeleteFromNtfPopupusers, idNotice);
                    db.ExecuteNonQueryCommand(sSql);

                    sSql = string.Format(TrackControlQuery.NoticeProvider.DeleteFromNtfMainId, idNotice);
                    //cn.ExecuteNonQueryCommand(sSql);
                    db.ExecuteNonQueryCommand(sSql);
                }
                db.CloseDbConnection();
                return true;
            }
            catch (Exception ex)
            {
                db.CloseDbConnection();
                MessageBox.Show(ex.Message);
                return false;
            }
        }


        public static List<NoticeSubItem> GetNoticeSubItems(NoticeItem ni, NoticeItem.LoadRegime loadRegime)
        {
            //try
            //{
            //ZonesProvider zonesProvider = new ZonesProvider();
            var nsis = new List<NoticeSubItem>();
            //using (ConnectMySQL cn = new ConnectMySQL())
           var db = new DriverDb();
            db.ConnectDb();
            {
                string sSQL = string.Format(TrackControlQuery.NoticeProvider.SelectNtfEventsMainId, ni.Id);

                if (loadRegime == NoticeItem.LoadRegime.Events)
                    sSQL = string.Format(TrackControlQuery.NoticeProvider.SelectNtfEventsTypeEvent, ni.Id,
                        (int) NoticeSubItemTypes.CheckZoneControl);

                //MySqlDataReader dr = cn.GetDataReader(sSQL);
                db.GetDataReader(sSQL);
                //while (dr.Read())
                while (db.Read())
                {
                    //switch ((NoticeSubItemTypes)dr.GetInt32("TypeEvent"))
                    switch ((NoticeSubItemTypes) db.GetInt16("TypeEvent"))
                    {
                        case (NoticeSubItemTypes.CheckZoneControl):
                        {
                            switch (loadRegime)
                            {
                                case NoticeItem.LoadRegime.Events:
                                    break;
                                case NoticeItem.LoadRegime.EventsZonesBriefVeh:

                                    sSQL = string.Format(TrackControlQuery.NoticeProvider.SelectNtfEventsMainId, ni.Id);

                                    //NoticeCheckZone nchzBrief = new NoticeCheckZone(ni, dr.GetInt32("EventId"), (ZoneEventTypes)dr.GetInt32("ParInt1"))
                                    //                                {Id = dr.GetInt32("Id")};
                                    var nchzBrief = new NoticeCheckZone(ni, db.GetInt32("EventId"),
                                        (ZoneEventTypes) db.GetInt32("ParInt1")) {Id = db.GetInt32("Id")};
                                    nsis.Add(nchzBrief);
                                    break;
                                case NoticeItem.LoadRegime.EventsZonesFullVehAnaliz:
                                    //NoticeCheckZone nchzFull = new NoticeCheckZone(ni, dr.GetInt32("EventId"), (ZoneEventTypes)dr.GetInt32("ParInt1"))
                                    //                               {Id = dr.GetInt32("Id")};
                                    var nchzFull = new NoticeCheckZone(ni, db.GetInt32("EventId"),
                                        (ZoneEventTypes) db.GetInt32("ParInt1")) {Id = db.GetInt32("Id")};
                                    nsis.Add(nchzFull);
                                    break;
                            }
                            break;
                        }
                        case (NoticeSubItemTypes.StopControl):
                        {
                            //NoticeStop nstop = new NoticeStop(ni,dr.GetDouble("ParDbl1"))
                            //                       {Id = dr.GetInt32("Id")};
                            var nstop = new NoticeStop(ni, db.GetDouble("ParDbl1")) { Id = db.GetInt32("Id") };
                            nsis.Add(nstop);
                            break;
                        }
                        case (NoticeSubItemTypes.SpeedControl):
                        {
                            var nspeed = new NoticeSpeed(ni, db.GetDouble("ParDbl1"), db.GetDouble("ParDbl2"))
                            {
                                Id = db.GetInt32("Id")
                            };
                            nsis.Add(nspeed);
                            break;
                        }
                        case (NoticeSubItemTypes.SpeedAgroWorksControl):
                        {
                            var nspeedAgroWork = new NoticeSpeedAgroWork(ni)
                            {
                                Id = db.GetInt32("Id")
                            };
                            nsis.Add(nspeedAgroWork);
                            break;
                        }
                        case (NoticeSubItemTypes.LossPower):
                        {
                            var nlpower = new NoticeLossPower(ni, db.GetDouble("ParDbl1"))
                            {
                                Id = db.GetInt32("Id")
                            };
                            nsis.Add(nlpower);
                            break;
                        }
                        case (NoticeSubItemTypes.LossData):
                        {
                            var nldata = new NoticeLossData(ni, db.GetDouble("ParDbl1"))
                            {
                                Id = db.GetInt32("Id")
                            };
                            nsis.Add(nldata);
                            break;
                        }
                        case (NoticeSubItemTypes.LossGps):
                        {
                            var nlgps = new NoticeLossGPS(ni, db.GetDouble("ParDbl1"))
                            {
                                Id = db.GetInt32("Id")
                            };
                            nsis.Add(nlgps);
                            break;
                        }
                        case (NoticeSubItemTypes.SensorControl):
                        {
                            var ns = new NoticeSensor(ni, 0, db.GetDouble("ParDbl1"), db.GetDouble("ParDbl2"),
                                db.GetBit("ParBool")) {Id = db.GetInt32("Id")};
                            nsis.Add(ns);
                            break;
                        }
                        case (NoticeSubItemTypes.SensorLogicControl):
                        {
                            var nls = new NoticeLogicSensor(ni, 0, db.GetBit("ParBool"))
                            {
                                Id = db.GetInt32("Id")
                            };
                            nsis.Add(nls);
                            break;
                        }
                        case (NoticeSubItemTypes.TimeControl):
                        {
                            var nls = new NoticeTimeControl(ni, db.GetDouble("ParDbl1"))
                            {
                                Id = db.GetInt32("Id")
                            };
                            nsis.Add(nls);
                            break;
                        }
                        case (NoticeSubItemTypes.SensorFlowTypeControl):
                        {
                            var nfts = new NoticeFlowTypeSensor(ni, 0, db.GetInt32("ParInt1"),
                                (int) db.GetDouble("ParDbl1"), db.GetInt32("ParInt2"), (int) db.GetDouble("ParDbl2"),
                                db.GetBit("ParBool")) {Id = db.GetInt32("Id")};
                            nsis.Add(nfts);
                            break;
                        }
                        case (NoticeSubItemTypes.FuelDischarge):
                        {
                            var nls = new NoticeFuelDischarge(ni, db.GetDouble("ParDbl1"),
                                db.GetInt32("ParInt1"), db.GetDouble("ParDbl2")) {Id = db.GetInt32("Id")};
                            nsis.Add(nls);
                            break;
                        }
                        case (NoticeSubItemTypes.VehicleUnload):
                        {
                            var nvu = new NoticeVehicleUnload(ni, db.GetInt32("ParInt1")) { Id = db.GetInt32("Id") };
                            nsis.Add(nvu);
                            break;
                        }
                        default:
                            break;
                    }
                }
                //dr.Close(); 
                db.CloseDataReader();
            }
            db.CloseDbConnection();
            return nsis;
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.Message, "GetNoticeSubItems");
            //    return null;
            //}
        }

        public static List<DictionaryAgroWorkType> GetAgroWorks(NoticeItem ni)
        {
            try
            {
                var agroWorks = new List<DictionaryAgroWorkType>();
                var db = new DriverDb();
                db.ConnectDb();
                {
                    string sSQL = string.Format(TrackControlQuery.NoticeProvider.SelectNtfAgroWorks, ni.Id);
                    db.GetDataReader(sSQL);
                    while (db.Read())
                    {
                        agroWorks.Add(new DictionaryAgroWorkType(db.GetInt32("WorkId")));
                    }
                }

                db.CloseDbConnection();
                return agroWorks;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "GetAgroWorks(NoticeItem ni)");
                return null;
            }
        }

        public static NoticeItem GetNotice(int idNotice)
        {
            DriverDb db = new DriverDb();
            try
            {
                NoticeItem ni = new NoticeItem();
                //using (ConnectMySQL cn = new ConnectMySQL())

                db.ConnectDb();
                {
                    ni.Id = idNotice;
                    string sSql = string.Format(TrackControlQuery.NoticeProvider.SelectFromNtfMainID, ni.Id);

                    //MySqlDataReader dr = cn.GetDataReader(sSql);
                    db.GetDataReader(sSql);
                    //if (dr.Read())
                    if (db.Read())
                    {
                        //ni.Title = dr.GetString("Title")?? "";
                        ni.Title = db.GetString("Title") ?? "";

                        //if (dr["SoundName"] != DBNull.Value)
                        //    ni.SoundName = dr.GetString("SoundName") ?? "";
                        if (db["SoundName"] != DBNull.Value)
                            ni.SoundName = db.GetString("SoundName") ?? "";

                        //if (dr["IconSmall"] != DBNull.Value)
                        //ni.IconSmall = (byte[])dr["IconSmall"];
                        if (db["IconSmall"] != DBNull.Value)
                            ni.IconSmall = (byte[]) db["IconSmall"];

                        //if (dr["IconLarge"] != DBNull.Value)
                        //    ni.IconLarge = (byte[])dr["IconLarge"]; 
                        if (db["IconLarge"] != DBNull.Value)
                            ni.IconLarge = (byte[]) db["IconLarge"];

                        //ni.IsActive = dr.GetBoolean("IsActive");
                        ni.IsActive = db.GetBoolean("IsActive");//ni.IsEmailActive = dr.GetBoolean("IsEmailActive");
                        ni.IsEmailActive = db.GetBoolean("IsEmailActive");

                        //ni.IsPopup = dr.GetBoolean("IsPopup");
                        ni.IsPopup = db.GetBoolean("IsPopup");

                        //if (dr["TimeForControl"] != DBNull.Value)
                        //ni.TimeForControl = dr.GetInt32("TimeForControl");
                        if (db["TimeForControl"] != DBNull.Value)
                            ni.TimeForControl = db.GetDouble("TimeForControl");
                        ni.Period = db.GetInt32("Period");
                    }
                    ni.Emails = GetNoticeEmails(ni);
                    ni.PopupUsers = GetNoticePopupUsers(ni);
                    //dr.Close(); 
                    db.CloseDataReader();
                }
                db.CloseDbConnection();
                return ni;
            }
            catch (Exception ex)
            {
                db.CloseDbConnection();
                MessageBox.Show(ex.Message, "GetNotice");return null;
            }
        }

        public static void WriteLog(NoticeSubItem nsi, int mobitelId, string infor, DateTime dateReceive)
        {
            //using (ConnectMySQL cn = new ConnectMySQL())
            if (nsi.Ni.Id <= 0) return;
            DriverDb db = new DriverDb();
            db.ConnectDb();
            {
                try
                {
                    // для пропажи GPS - сигнала - накопительная записью Старая удаляется - новая добавляется
                    if (nsi.TypeEvent == NoticeSubItemTypes.LossGps)
                    {
                        string sql = string.Format(TrackControlQuery.NoticeProvider.DeleteFromNtfLogByDataGps_ID,
                                                   nsi.GpsEvent.Id, (int)nsi.TypeEvent);
                        db.ExecuteNonQueryCommand(sql);
                    }
                    //MySqlParameter[] parSql = new MySqlParameter[13];
                    db.NewSqlParameterArray(13);
                    //parSql[0] = new MySqlParameter("?DateEvent", nsi.Gps_event.Time);
                    db.NewSqlParameter(db.ParamPrefics + "DateEvent", dateReceive, 0);
                    //parSql[1] = new MySqlParameter("?Mobitel_id", mobitelId);
                    db.NewSqlParameter(db.ParamPrefics + "Mobitel_id", mobitelId, 1);
                    //parSql[2] = new MySqlParameter("?Location", nsi.Location);
                    db.NewSqlParameter(db.ParamPrefics + "Location", nsi.Location, 2);
                    //parSql[3] = new MySqlParameter("?Infor", infor);
                    db.NewSqlParameter(db.ParamPrefics + "Infor", infor.Substring(0, Math.Min(infor.Length, 255)), 3);
                    //parSql[4] = new MySqlParameter("?Lng", nsi.Gps_event.LatLng.Lng );
                    db.NewSqlParameter(db.ParamPrefics + "Lng", nsi.GpsEvent.LatLng.Lng, 4);
                    //parSql[5] = new MySqlParameter("?Lat", nsi.Gps_event.LatLng.Lat);
                    db.NewSqlParameter(db.ParamPrefics + "Lat", nsi.GpsEvent.LatLng.Lat, 5);
                    //parSql[6] = new MySqlParameter("?DataGps_ID", nsi.Gps_event.Id);
                    db.NewSqlParameter(db.ParamPrefics + "DataGps_ID", nsi.GpsEvent.Id, 6);
                    //parSql[7] = new MySqlParameter("?Id_main", nsi.Ni.Id);
                    db.NewSqlParameter(db.ParamPrefics + "Id_main", nsi.Ni.Id, 7);
                    //parSql[8] = new MySqlParameter("?TypeEvent", nsi.TypeEvent);
                    db.NewSqlParameter(db.ParamPrefics + "TypeEvent", nsi.TypeEvent, 8);

                    if (nsi is NoticeCheckZone)
                        //parSql[9] = new MySqlParameter("?Zone_id", ((NoticeCheckZone)nsi).Zone_Id);
                        db.NewSqlParameter(db.ParamPrefics + "Zone_id", ((NoticeCheckZone) nsi).Zone_Id, 9);
                    else if (nsi is NoticeVehicleUnload)
                    {
                        int idZone = ((NoticeVehicleUnload)nsi).ZoneUnload == null ? ConstsGen.RECORD_MISSING : ((NoticeVehicleUnload)nsi).ZoneUnload.Id; ;
                        db.NewSqlParameter(db.ParamPrefics + "Zone_id", idZone, 9);
                    }
                    else
                        //parSql[9] = new MySqlParameter("?Zone_id", ConstsGen.RECORD_MISSING);
                        db.NewSqlParameter(db.ParamPrefics + "Zone_id", ConstsGen.RECORD_MISSING, 9);

                    //parSql[10] = new MySqlParameter("?Speed", nsi.Gps_event.Speed);
                    db.NewSqlParameter(db.ParamPrefics + "Speed", nsi.GpsEvent.Speed, 10);
                    //parSql[11] = new MySqlParameter("?DateWrite", DateTime.Now);
                    db.NewSqlParameter(db.ParamPrefics + "DateWrite", DateTime.Now, 11);
                    //parSql[12] = new MySqlParameter("?Value", nsi.Value);
                    db.NewSqlParameter(db.ParamPrefics + "Value", nsi.Value, 12);

//                  cn.ExecuteNonQueryCommand(@"INSERT INTO ntf_log (DateEvent,Mobitel_id, Location,Infor,Lng,Lat,DataGps_ID,Id_main,TypeEvent,Zone_id,Speed,DateWrite,Value) 
//                  VALUES (?DateEvent,?Mobitel_id, ?Location,?Infor,?Lng,?Lat,?DataGps_ID,?Id_main,?TypeEvent,?Zone_id,?Speed,?DateWrite,?Value)", parSql);
                    
                    string sqlstr =
                        string.Format(TrackControlQuery.NoticeProvider.InsertIntoNtfLogValue, db.ParamPrefics);

                    db.ExecuteNonQueryCommand(sqlstr, db.GetSqlParameterArray);

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "WriteLog");
                }
            }
            db.CloseDbConnection();
        }

        public static bool IsEventInLog(System.Int64 gpsId, int niId)
        {
            //using (ConnectMySQL cn = new ConnectMySQL())
            using (DriverDb db = new DriverDb())
            {
                db.ConnectDb();

                if (gpsId > 0)
                {
                    //return (0 != cn.GetScalarValueIntNull(string.Format(@"SELECT count(ntf_log.DataGps_ID) AS CNT FROM   
                    //ntf_log WHERE   ntf_log.DataGps_ID = {0} AND ntf_log.Id_main = {1}", gpsId, niId)));
                    return (0 !=
                            db.GetScalarValueIntNull(string.Format(TrackControlQuery.NoticeProvider.SelectCountNtf_Log,
                                gpsId, niId)));
                }
                return false;
            }
        }

        public static bool IsStateInLog(int niId, int mobitelId, DateTime timeEvent, int periodMinute)
        {
            using (DriverDb db = new DriverDb())
            {
                db.ConnectDb();

                string sql = string.Format(TrackControlQuery.NoticeProvider.SelectCountNtfLogDataGps, db.ParamPrefics, mobitelId, niId);

                //MySqlParameter[] parSql = new MySqlParameter[2];
                db.NewSqlParameterArray(2);
                //parSql[0] = new MySqlParameter("?DateStart", timeEvent.Subtract(new TimeSpan(0, periodMinute, 0)));
                db.NewSqlParameter(db.ParamPrefics + "DateStart", timeEvent.Subtract(new TimeSpan(0, periodMinute, 0)), 0);
                //parSql[1] = new MySqlParameter("?DateEnd", timeEvent);
                db.NewSqlParameter(db.ParamPrefics + "DateEnd", timeEvent, 1);
                //return (0 != cn.GetScalarValueIntNull(sql, parSql));
                return (0 != db.GetScalarValueIntNull(sql, db.GetSqlParameterArray));
            }
        }

        public static DataTable GetLog()
        {
            //автоудаление записей журнала событий
            NoticeSetItem nsi = new NoticeSetItem();
            int hoursForDelete = nsi.TimeForDelete;
            string sql = "";
            //using (ConnectMySQL cn = new ConnectMySQL())
            using (DriverDb db = new DriverDb())
            {
                db.ConnectDb();

                if (hoursForDelete > 0)
                {
                    sql = string.Format(TrackControlQuery.NoticeProvider.DeleteFromNtfLogDateWrite, db.ParamPrefics);
                    //MySqlParameter[] par = new MySqlParameter[1];
                    db.NewSqlParameterArray(1);
                    //par[0] = new MySqlParameter("?DateWrite", DateTime.Now.AddHours(-hoursForDelete));
                    db.NewSqlParameter(db.ParamPrefics + "DateWrite", DateTime.Now.AddHours(-hoursForDelete), 0);
                    //cn.ExecuteNonQueryCommand(sql, par); 
                    db.ExecuteNonQueryCommand(sql, db.GetSqlParameterArray);
                }
                string filterVehicles = "";
                if (!UserBaseCurrent.Instance.Admin || UserBaseCurrent.Instance.Role != null)
                {
                    filterVehicles =
                        string.Format(
                            TrackControlQuery.NoticeProvider.WhereVehicleId, UserBaseCurrent.Instance.Role.Id,
                            (int) UserAccessObjectsTypes.Vehicles);
                }

                sql = string.Format(
                    TrackControlQuery.NoticeProvider.SelectNtfLogParam, TrackControlQuery.VehicleIdent, filterVehicles);

                //DataSet dsNtf = cn.GetDataSet(sql, "ntf_master");
                DataSet dsNtf = db.GetDataSet(sql, "ntf_master");

                if (dsNtf == null)
                    return null;

                sql = string.Format(TrackControlQuery.NoticeProvider.SelectNtfLogData,
                    filterVehicles);

                //cn.FillDataSet(sql, dsNtf, "ntf_slave");
                db.FillDataSet(sql, dsNtf, "ntf_slave");
                DataColumn keyColumn = dsNtf.Tables["ntf_master"].Columns["Id"];
                DataColumn foreignKeyColumn = dsNtf.Tables["ntf_slave"].Columns["Id"];
                dsNtf.Relations.Add(Resources.Location, keyColumn, foreignKeyColumn);
                return dsNtf.Tables["ntf_master"];
            }
        }

        public static void ClearLog()
        {
            string sql = TrackControlQuery.NoticeProvider.Delete_Ntf_Log;
            //using (ConnectMySQL cn = new ConnectMySQL())
            DriverDb db = new DriverDb();
            db.ConnectDb();
            {
                //cn.ExecuteNonQueryCommand(sql);
                db.ExecuteNonQueryCommand(sql);
            }
            db.CloseDbConnection();
        }

        public static void UpdateActivity(int recordId, object isActive)
        {
            int valueIsActive = 0;
            if (isActive.GetType().ToString() == "System.Boolean")
                valueIsActive = (bool)isActive == true ? 1 : 0;
            else if (isActive.GetType().ToString() == "System.String")
                valueIsActive = isActive.ToString() == "1" ? 1 : 0;
            string sql = string.Format(TrackControlQuery.NoticeProvider.UpdateNtfMain, recordId, valueIsActive);
            //using (ConnectMySQL cn = new ConnectMySQL())
            DriverDb db = new DriverDb();
            db.ConnectDb();
            {
                //cn.ExecuteNonQueryCommand(sql);
                db.ExecuteNonQueryCommand(sql);
            }
            db.CloseDbConnection();
        }

        public static void UpdateReading(int recordId)
        {
            string sql = string.Format(TrackControlQuery.NoticeProvider.UpdateNtfLog, recordId);
            //using (ConnectMySQL cn = new ConnectMySQL())
            DriverDb db = new DriverDb();
            db.ConnectDb();
            {
                //cn.ExecuteNonQueryCommand(sql);
                db.ExecuteNonQueryCommand(sql);
            }
            db.CloseDbConnection();
        }

        public static int GetLastLogId()
        {
            //using (ConnectMySQL cn = new ConnectMySQL())
            using (DriverDb db = new DriverDb())
            {
                db.ConnectDb();

                string sSQL = TrackControlQuery.NoticeProvider.SelectNtfLogIdFromNtfLog;

                //return  cn.GetScalarValueNull<int>(sSQL,0);

                return db.GetScalarValueNull<int>(sSQL, 0);
            }

            return 0;
        }

        public static List<Vehicle> GetMobitelsWithRfidEvent(DateTime begin, DateTime end, int RFID)
        {
            try
            {
                List<Vehicle> vhs = new List<Vehicle>();
                List<int> mobitels = new List<int>();
                //using (ConnectMySQL cn = new ConnectMySQL())
                DriverDb db = new DriverDb();
                db.ConnectDb();
                {
                    string sSQL = string.Format(TrackControlQuery.NoticeProvider.SelectNtfLogMobitelId,
                        (int) NoticeSubItemTypes.SensorControl,
                        RFID, db.ParamPrefics);

                    //MySqlParameter[] parSql = new MySqlParameter[2];
                    db.NewSqlParameterArray(2);
                    //parSql[0] = new MySqlParameter("?TimeBegin", begin);
                    db.NewSqlParameter(db.ParamPrefics + "TimeBegin", begin, 0);
                    //parSql[1] = new MySqlParameter("?TimeEnd", end);
                    db.NewSqlParameter(db.ParamPrefics + "TimeEnd", end, 1);
                    //MySqlDataReader dr = cn.GetDataReader(sSQL, parSql);
                    db.GetDataReader(sSQL, db.GetSqlParameterArray);
                    //while (dr.Read())
                    while (db.Read())
                    {
                        //int idMobitel = dr.GetInt32("Mobitel_id");
                        int idMobitel = db.GetInt32("Mobitel_id");
                        if (mobitels.Contains(idMobitel))
                            continue;
                        mobitels.Add(idMobitel);
                        Vehicle vh = new Vehicle(idMobitel);
                        vh.Mobitel = new Mobitel(idMobitel, "", "");
                        if (!vhs.Contains(vh))
                            vhs.Add(vh);
                    }
                }
                db.CloseDbConnection();
                return vhs;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "GetMobitelsWithRFIDEvent");
                return null;
            }
        }

        public static int GetZoneIdFromTimeControlEvent(DateTime begin, DateTime end, int Id_Mobitel)
        {
            try
            {
                //using (ConnectMySQL cn = new ConnectMySQL())
                using (DriverDb db = new DriverDb())
                {
                    db.ConnectDb();

                    string sSQL = string.Format(TrackControlQuery.NoticeProvider.SelectNtfLogZoneId,
                        (int) NoticeSubItemTypes.CheckZoneControl, Id_Mobitel, (int) NoticeSubItemTypes.TimeControl,
                        db.ParamPrefics);

                    //MySqlParameter[] parSQL = new MySqlParameter[2];
                    db.NewSqlParameterArray(2);
                    //parSQL[0] = new MySqlParameter("?TimeBegin", begin);
                    db.NewSqlParameter(db.ParamPrefics + "TimeBegin", begin, 0);
                    //parSQL[1] = new MySqlParameter("?TimeEnd", end);
                    db.NewSqlParameter(db.ParamPrefics + "TimeEnd", end, 1);
                    //MySqlDataReader dr = cn.GetDataReader(sSQL, parSQL);
                    db.GetDataReader(sSQL, db.GetSqlParameterArray);
                    //if (dr.Read())
                    if (db.Read())
                    {
                        //return dr.GetInt32("Zone_id");
                        return db.GetInt32("Zone_id");
                    }
                }

                return ConstsGen.RECORD_MISSING;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "GetZoneIdFromTimeControlEvent");
                return ConstsGen.RECORD_MISSING;
            }
        }

        public static BindingList<EMail> GetNoticeEmails(NoticeItem ni)
        {
            DriverDb db = new DriverDb();
            try
            {
                BindingList<EMail> eMails = new BindingList<EMail>();
                //using (ConnectMySQL cn = new ConnectMySQL())

                db.ConnectDb();
                {
                    string sql = string.Format(TrackControlQuery.NoticeProvider.SelectNtfEmailsId, ni.Id);
                    //MySqlDataReader dr = cn.GetDataReader(sql);
                    db.GetDataReader(sql);
                    //while (dr.Read())
                    while (db.Read())
                    {
                        //EMail eMail = new EMail(dr.GetInt32("Id"), dr.GetString("Email"), dr.GetBoolean("IsActive"));
                        EMail eMail = new EMail(db.GetInt32("Id"), db.GetString("Email"), db.GetBoolean("IsActive"));
                        eMails.Add(eMail);
                    }
                }
                db.CloseDbConnection();
                return eMails;
            }
            catch (Exception ex)
            {
                db.CloseDbConnection();
                MessageBox.Show(ex.Message, "GetNoticeEmails");
                return null;
            }
        }

        public static BindingList<PopupUser> GetNoticePopupUsers(NoticeItem ni)
        {
            DriverDb db = new DriverDb();
            try
            {
                BindingList<PopupUser> popUsers = new BindingList<PopupUser>();
                db.ConnectDb();
                {
                    string sql = string.Format(TrackControlQuery.NoticeProvider.SelectNtfPopupUsersId, ni.Id);
                    //MySqlDataReader dr = cn.GetDataReader(sql);
                    db.GetDataReader(sql);
                    //while (dr.Read())
                    while (db.Read())
                    {
                        //EMail eMail = new EMail(dr.GetInt32("Id"), dr.GetString("Email"), dr.GetBoolean("IsActive"));
                        PopupUser popUser = new PopupUser(db.GetInt32("Id"), db.GetInt32("Id_user"));
                        popUsers.Add(popUser);
                    }
                }
                db.CloseDbConnection();
                return popUsers;
            }
            catch (Exception ex)
            {
                db.CloseDbConnection();
                MessageBox.Show(ex.Message, "GetNoticePopupUsers");
                return null;
            }
        }

        private static bool WriteEmails(NoticeItem ni, DriverDb db)
        {
            try
            {
                if (ni.Id == ConstsGen.RECORD_MISSING || ni.Emails == null)
                    return false;
                bool insertEmails = false;
                //using (ConnectMySQL cn = new ConnectMySQL())
                string sql = String.Format(TrackControlQuery.NoticeProvider.DeleteFromNtfEmailsWhere, ni.Id);
                //cn.ExecuteNonQueryCommand(sql);
                db.ExecuteNonQueryCommand(sql);
                foreach (EMail email in ni.Emails)
                {

                    sql = string.Format(TrackControlQuery.NoticeProvider.InsertIntoNtfEmails, db.ParamPrefics);

                    //MySqlParameter[] parSqlqlmob = new MySqlParameter[3];
                    db.NewSqlParameterArray(3);
                    //parSqlqlmob[0] = new MySqlParameter("?Id_main", ni.Id);
                    db.NewSqlParameter(db.ParamPrefics + "Id_main", ni.Id, 0);
                    //parSqlqlmob[1] = new MySqlParameter("?IsActive", email.IsActive);
                    db.NewSqlParameter(db.ParamPrefics + "IsActive", email.IsActive, 1);
                    //parSqlqlmob[2] = new MySqlParameter("?Email", email.Addres);
                    db.NewSqlParameter(db.ParamPrefics + "Email", email.Addres, 2);
                    //cn.ExecuteNonQueryCommand(sql, parSqlqlmob);
                    db.ExecuteNonQueryCommand(sql, db.GetSqlParameterArray);
                    insertEmails = true;
                }
                return insertEmails;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "WriteEmails");
                return false;
            }
        }

        private static bool WritePopupUsers(NoticeItem ni, DriverDb db)
        {
            try
            {
                if (ni.Id == ConstsGen.RECORD_MISSING || ni.PopupUsers == null)
                    return false;
                bool insertPopupUsers = false;
                //using (ConnectMySQL cn = new ConnectMySQL())
                string sql = String.Format(TrackControlQuery.NoticeProvider.DeleteFromNtfPopupUsersWhere, ni.Id);
                //cn.ExecuteNonQueryCommand(sql);
                db.ExecuteNonQueryCommand(sql);
                foreach (PopupUser pUser in ni.PopupUsers)
                {
                    sql = string.Format(TrackControlQuery.NoticeProvider.InsertIntoNtfPopupUsers, db.ParamPrefics);
                    db.NewSqlParameterArray(2);
                    db.NewSqlParameter(db.ParamPrefics + "Id_main", ni.Id, 0);
                    db.NewSqlParameter(db.ParamPrefics + "Id_user", pUser.User.Id, 1);
                    db.ExecuteNonQueryCommand(sql, db.GetSqlParameterArray);
                    insertPopupUsers = true;
                }

                return insertPopupUsers;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "WritePopupUsers");
                return false;
            }
        }

        private static bool WriteAgroWorks(NoticeItem ni, DriverDb db)
        {
            try
            {
                if (ni.Id == ConstsGen.RECORD_MISSING || ni.AgroWorks == null || ni.AgroWorks.Count == 0)
                    return false;
                bool insertAgroWorks = false;
                string sql = String.Format(TrackControlQuery.NoticeProvider.DeleteFromNtfAgroWorksWhere, ni.Id);
                db.ExecuteNonQueryCommand(sql);
                foreach (var agroWork in ni.AgroWorks)
                {
                    sql = string.Format(TrackControlQuery.NoticeProvider.InsertIntoNtfAgroWorks, db.ParamPrefics);
                    db.NewSqlParameterArray(2);
                    db.NewSqlParameter(db.ParamPrefics + "Id_main", ni.Id, 0);
                    db.NewSqlParameter(db.ParamPrefics + "WorkId", agroWork.Id, 1);
                    db.ExecuteNonQueryCommand(sql, db.GetSqlParameterArray);
                    insertAgroWorks = true;
                }

                return insertAgroWorks;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "WriteAgroWorks");
                return false;
            }
        }

        public static List<Zone> GetAgroZones(NoticeItem ni, IZonesManager zonesModel)
        {
            var agroZones = new List<Zone>();
            using (var db = new DriverDb())
            {
                db.ConnectDb();
                string sql = TrackControlQuery.NoticeProvider.SelectAgroZones;
                db.GetDataReader(sql);
                while (db.Read())
                {
                    var zone = (Zone)zonesModel.GetZoneById(db.GetInt32("Zone_ID"));
                    agroZones.Add(zone);
                }
            }
            return agroZones;
        }
    }
}
