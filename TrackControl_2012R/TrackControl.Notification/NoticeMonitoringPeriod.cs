﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using TrackControl.MySqlDal;
using TrackControl.Notification.Properties;
using TrackControl.Vehicles;
using TrackControl.General;
using System.Diagnostics;
using DevExpress.XtraEditors;
namespace TrackControl.Notification
{
    public  class NoticeMonitoringPeriod
    {

        List<NoticeLogRecord> _periodRecords;
        NoticeSetItem _nseti;
        private List<NoticeItem> _nis = new List<NoticeItem>();

        private static bool _isActive;

        public static bool IsActive
        {
            get { return _isActive; }
            set { _isActive = value; }
        }
        public static event Action<List<NoticeLogRecord>> RecordsCreatedPeriod;
        public static event MethodInvoker StopPeriodAnaliz;

        public static event ProgressChanged AlgorithmStarted ;
        public static event Action<Int32> ProgressChanged ;
        public static event MethodInvoker AlgorithmFinished;

        public NoticeMonitoringPeriod()
        {
            _nseti = new NoticeSetItem();
        }

        private void LoadNotices()
        {
            _nis = NoticeProvider.GetNoticeEntityListForAnalizOneQuery ();
        }


        public void StartPeriod(DateTime start, DateTime finish)
        {
            if (_isActive)
            {
                if (StopPeriodAnaliz != null) StopPeriodAnaliz();
                return;
            }
            if (AlgorithmStarted != null) AlgorithmStarted(Resources.AnalizNotice, "", 0);
            LoadNotices();
            if (!IsNoticeItemsExists())
            {
                if (StopPeriodAnaliz != null) StopPeriodAnaliz();
                return;
            }
            //GpsDataProvider provider = new GpsDataProvider();
            IList<Vehicle> vehicles = GetVehiclesFromActiveNotice();
            _isActive = true;
            _periodRecords = new List<NoticeLogRecord>();
            foreach (Vehicle vehicle in vehicles)
            {
                if (AlgorithmStarted != null) AlgorithmStarted(string.Format("{0}:{1}", Resources.DataSelecting , vehicle.RegNumber), "", 0);
                Application.DoEvents();
                IList<GpsData> gpsData = GpsDataProvider.GetGpsDataAllProtocolsAllPonts(vehicle.Mobitel.Id, start, finish);
                if (gpsData.Count > 0)
                {
                    if (AlgorithmStarted != null) AlgorithmStarted(vehicle.Info, "", gpsData.Count);
                    MonitoringListGpsData(gpsData);
                    Application.DoEvents();
                }
                if (!_isActive) break;
            }
            _isActive = false;
            if (AlgorithmFinished != null) AlgorithmFinished();
            if (StopPeriodAnaliz != null) StopPeriodAnaliz();
        }

        public void StopPeriod()
        {
            _isActive = false;
            if (_periodRecords!=null) _periodRecords.Clear();
            if (_nis != null) _nis.Clear();
        }

        bool  IsNoticeItemsExists()
        {
            if (_nis.Count == 0)
            {
                XtraMessageBox.Show(Resources.NotificationsAbsent, Resources.Notifications);
                return false;
            }
            return true;
        }


        private void MonitoringListGpsData(IList<GpsData> gpss)
        {
            int cnt = 0;
            foreach (GpsData gps in gpss)
            {
                _nis.ForEach(delegate(NoticeItem ni)
                {
                    if (ni.CheckEventPeriod(gps, _periodRecords)) RecordsCreatedPeriod(_periodRecords);
                }
                );
                cnt++;
                if (cnt % 100 == 0 && ProgressChanged != null) ProgressChanged(cnt);
                Application.DoEvents();
                if (!_isActive) return;
            }
            if (!_isActive) return;
            _nis.ForEach(delegate(NoticeItem ni)
            {
                if (ni.CheckStatePeriod(_periodRecords))
                {
                    RecordsCreatedPeriod(_periodRecords);
                }
            }
            );
        }

        IList<Vehicle> GetVehiclesFromActiveNotice()
        {
            IList<Vehicle> vehicles = new List<Vehicle>();
            _nis.ForEach(delegate(NoticeItem ni)
            {
                foreach (Vehicle vehicle in ni.Vehicles)
	                {
                        if (!vehicles.Contains(vehicle)) vehicles.Add(vehicle); 
	                } 
            }
            );
            return vehicles;
        }
    }
}
