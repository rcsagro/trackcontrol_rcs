﻿using System;
using Agro.Utilites;

namespace TrackControl.Notification
{
    public class StateTime : StateBase
    {

        public override event StateHandler NextState;

        public StateTime(NoticeWizard formWizard)
            : base(formWizard)
        {

        }

        public override void InitPage()
        {
            if (!_formWizard.Ni.IsNew)
            {
                NoticeTimeControl nTime = (NoticeTimeControl)_formWizard.Ni.GetSubItem<NoticeTimeControl>();
                if (nTime != null)
                {
                    _formWizard.teTime.Text = nTime.TimeForContrrol.ToString();
                }
            }
        }

        public override void SelectPage()
        {
            if (NextState != null)
            {
                NextState(new StatePopupUsers(_formWizard));
            }
        }

        public override int PageIndex()
        {
            return 12;
        }

        public override bool SetPage()
        {
            if (UpdateSubItem())
            {
                _formWizard.PageChanged = false;
                if (!DicUtilites.ValidateFieldsTextDoublePositive(_formWizard.teTime, _formWizard.dxErrP)) return false;
                _formWizard.Ni.ClearSubItems<NoticeTimeControl>();
                double _timeFoControln;
                if (Double.TryParse(_formWizard.teTime.Text, out _timeFoControln))
                {
                    NoticeTimeControl ns = new NoticeTimeControl(_formWizard.Ni, _timeFoControln);
                    _formWizard.Ni.AddSubItem(ns);
                    return true;
                }
                return false;
            }
            return true;
        }
    }
}
