﻿using System;
using System.Collections.Generic;
using System.Text;
using DevExpress.XtraWizard;
using TrackControl.MySqlDal;

namespace TrackControl.Notification
{
    public delegate void StateHandler(IState state);
    public class PageSelector: IPageSelector 
    {
        IState _state;
        List<IState> Prev_states;

        public int SelectPageIndex()
        {
            _state.SelectPage();
            return _state.PageIndex();
        }

        public IState State
        {
            get { return _state; }
            set { _state = value; }
        }

        public PageSelector(IState state)
        {
            Prev_states = new List<IState>(); 
            OnNextState(state);
        }

        private void OnNextState(IState state)
        {
            if (state == null)
                throw new ArgumentNullException("state");

            if (State != state)
            {
                if (Prev_states != null && State!=null &&  !Prev_states.Contains(State)) Prev_states.Add(State); 
                State = state;
                State.NextState += new StateHandler(OnNextState);
            }
        }
        public void SetPageIndexToState(int pageIndex)
        {
            foreach (IState prev_state in Prev_states)
            {
                if (prev_state.PageIndex() == pageIndex)
                {
                    _state = prev_state;
                }
            }
        }
        public bool SetPage()
        {
            return _state.SetPage(); 
        }
    }
}
