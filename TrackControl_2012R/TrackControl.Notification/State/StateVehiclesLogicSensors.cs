﻿using System;
using System.Collections.Generic;
using System.Data; 
using System.Text;
using TrackControl.Reports;
using TrackControl.Vehicles;
using TrackControl.MySqlDal;

namespace TrackControl.Notification
{
    public class StateVehiclesLogicSensors : StateBase
    {
        public override event StateHandler NextState;

        public StateVehiclesLogicSensors(NoticeWizard formWizard)
            : base(formWizard)
        {

        }

        public override void InitPage()
        {
            VehicleProvider _vprov = new VehicleProvider();
            //_formWizard.gcLogicSensors.DataSource = _vprov.GetAllDataTableLogicSensors();
            SetGridDataSource(_vprov);
            _formWizard.gvLogicSensors.ClearSelection();
            if (!_formWizard.Ni.IsNew)
            {
                foreach (Vehicle vh in _formWizard.Ni.Vehicles)
                {
                    int Row_Find = _formWizard.gvLogicSensors.LocateByValue(0, _formWizard.gvLogicSensors.Columns["SID"], vh.LogicSensor.Id);
                    if (Row_Find >= 0)
                    {
                        _formWizard.gvLogicSensors.SelectRow(Row_Find);
                        _formWizard.gvLogicSensors.FocusedRowHandle = Row_Find; 
                    }
                }
            }
        }

        public override void SelectPage()
        {
            if (NextState != null)
            {
                NextState(new StateLogicSensors(_formWizard));

                switch (Convert.ToInt16(_formWizard.grSelectType.EditValue))
                {
                    case (int)NoticeSubItemTypes.VehicleUnload:
                        {
                            NextState(new StateVehicleUnload(_formWizard));
                            break;
                        }
                    default:
                        {
                            NextState(new StateLogicSensors(_formWizard));
                            break;
                        }
                }
            }
        }

        public override int PageIndex()
        {
            return 3;
        }
        public override bool SetPage()
        {
            if (UpdateSubItem())
            {
                _formWizard.PageChanged = false;
                _formWizard.Ni.ClearVehicles();
                if (_formWizard.gvLogicSensors.SelectedRowsCount == 0) return false;
                for (int i = 0; i < _formWizard.gvLogicSensors.SelectedRowsCount; i++)
                {
                    DataRow row_veh = _formWizard.gvLogicSensors.GetDataRow(_formWizard.gvLogicSensors.GetSelectedRows()[i]);
                    _formWizard.Ni.AddVehicle((int)row_veh["TID"], (int)row_veh["SID"], 0);
                }
            }
            return true;
        }

        private void SetGridDataSource(VehicleProvider vprov)
        {
            switch (Convert.ToInt16(_formWizard.grSelectType.EditValue))
            {
                case (int)NoticeSubItemTypes.VehicleUnload:
                    {
                        _formWizard.gcLogicSensors.DataSource = vprov.GetAllDataTableLogicSensorsAlgorithm((int)AlgorithmType.VEHICLE_UNLOAD);
                        break;
                    }
                default:
                    {
                        _formWizard.gcLogicSensors.DataSource = vprov.GetAllDataTableLogicSensors();
                        break;
                    }
            }
        }
    }
}
