﻿using System;
using System.Collections.Generic;
using System.Text;
using DevExpress.XtraWizard;

namespace TrackControl.Notification
{
        public interface IPageSelector
        {
            int SelectPageIndex();
        }
}
