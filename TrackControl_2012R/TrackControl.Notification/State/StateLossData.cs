﻿using System;
using Agro.Utilites;

namespace TrackControl.Notification
{
    public class StateLossData : StateBase
    {
        public override event StateHandler NextState;

        public StateLossData(NoticeWizard formWizard)
            : base(formWizard)
        {

        }

        public override void InitPage()
        {
            if (!_formWizard.Ni.IsNew)
            {
                NoticeLossData nldata = (NoticeLossData)_formWizard.Ni.GetSubItem<NoticeLossData>();
                if (nldata != null)
                {
                    _formWizard.teLossData.Text = nldata.Loss_time.ToString();
                }
            }
        }

        public override void SelectPage()
        {
            if (NextState != null)
            {
                NextState(new StatePopupUsers(_formWizard));
            }
        }

        public override int PageIndex()
        {
            return 7;
        }

        public override bool SetPage()
        {
            if (UpdateSubItem())
            {
                _formWizard.PageChanged = false;
                if (!DicUtilites.ValidateFieldsTextDoublePositive(_formWizard.teLossData, _formWizard.dxErrP)) return false;
                _formWizard.Ni.ClearSubItems<NoticeLossData>();
                double loss_time_min;
                if (Double.TryParse(_formWizard.teLossData.Text, out loss_time_min))
                {
                    NoticeLossData ns = new NoticeLossData(_formWizard.Ni, loss_time_min);
                    _formWizard.Ni.AddSubItem(ns);
                    return true;
                }
                return false;
            }
            return true;
        }
    }
}
