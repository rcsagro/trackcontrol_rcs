﻿using System;
using System.Collections.Generic;
using System.Text;
using DevExpress.XtraWizard;

namespace TrackControl.Notification
{
    public interface IState
    {
        event StateHandler NextState;
        void SelectPage();
        void InitPage();
        int PageIndex();
        bool SetPage();
    }


}
