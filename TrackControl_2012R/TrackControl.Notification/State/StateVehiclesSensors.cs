﻿using System;
using System.Data;
using TrackControl.Vehicles;
using TrackControl.MySqlDal;
using TrackControl.Reports;

namespace TrackControl.Notification
{
    public class StateVehiclesSensors : StateBase
    {
        public override event StateHandler NextState;

        public StateVehiclesSensors(NoticeWizard formWizard)
            : base(formWizard)
        {
        }

        public override void InitPage()
        {
            var _vprov = new VehicleProvider();
            SetGridDataSource(_vprov);
            _formWizard.gvSensors.ClearSelection();
            if (!_formWizard.Ni.IsNew)
            {
                foreach (Vehicle vh in _formWizard.Ni.Vehicles)
                {
                    int Row_Find = _formWizard.gvSensors.LocateByValue(0, _formWizard.gvSensors.Columns["SID"], vh.Sensor.Id);
                    if (Row_Find >= 0)
                    {
                        _formWizard.gvSensors.SelectRow(Row_Find);
                        _formWizard.gvSensors.FocusedRowHandle = Row_Find;
                    }
                }
            }
        }

        private void SetGridDataSource(VehicleProvider vprov)
        {
            switch (Convert.ToInt16(_formWizard.grSelectType.EditValue))
            {
                case (int) NoticeSubItemTypes.SensorFlowTypeControl:
                {
                    _formWizard.gcSensors.DataSource = vprov.GetListSensorsAlgoritm((int) AlgorithmType.FUELER);
                    break;
                }
                case (int) NoticeSubItemTypes.FuelDischarge:
                {
                    _formWizard.gcSensors.DataSource = vprov.GetListSensorsAlgoritm((int) AlgorithmType.FUEL1);
                    break;
                }
                default:
                {
                    _formWizard.gcSensors.DataSource = vprov.GetAllDataTableSensors();
                    break;
                }
            }
        }

        public override void SelectPage()
        {
            if (NextState != null)
            {
                switch (Convert.ToInt16(_formWizard.grSelectType.EditValue))
                {
                    case (int) NoticeSubItemTypes.SensorFlowTypeControl:
                    {
                        NextState(new StateVehicles(_formWizard));
                        break;
                    }
                    case (int) NoticeSubItemTypes.FuelDischarge:
                    {
                        NextState(new StateFuelDescharge(_formWizard));
                        break;
                    }
                    default:
                    {
                        NextState(new StateSensors(_formWizard));
                        break;
                    }
                }
            }
        }

        public override int PageIndex()
        {
            return 2;
        }

        public override bool SetPage()
        {
            if (UpdateSubItem())
            {
                _formWizard.PageChanged = false;
                _formWizard.Ni.ClearVehicles();
                if (_formWizard.gvSensors.SelectedRowsCount == 0) return false;
                for (int i = 0; i < _formWizard.gvSensors.SelectedRowsCount; i++)
                {
                    DataRow row_veh = _formWizard.gvSensors.GetDataRow(_formWizard.gvSensors.GetSelectedRows()[i]);
                    switch (Convert.ToInt16(_formWizard.grSelectType.EditValue))
                    {
                        case (int) NoticeSubItemTypes.FuelDischarge:
                        {
                            if (!_formWizard.Ni.IsVehicleContain((int) row_veh["TID"]))
                            {
                                _formWizard.Ni.AddVehicle((int) row_veh["TID"], 0, (int) row_veh["SID"]);
                            }
                            break;
                        }
                        default:
                        {
                            _formWizard.Ni.AddVehicle((int) row_veh["TID"], 0, (int) row_veh["SID"]);
                            break;
                        }
                    }

                }
            }
            return true;
        }
    }
}
