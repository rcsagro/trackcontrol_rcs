﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TrackControl.Notification
{
    public class StateLogicSensors : StateBase
    {
        public override event StateHandler NextState;

        public StateLogicSensors(NoticeWizard formWizard)
            : base(formWizard)
        {

        }

        public override void InitPage()
        {
            if (!_formWizard.Ni.IsNew)
            {
                NoticeLogicSensor nls = (NoticeLogicSensor)_formWizard.Ni.GetSubItem<NoticeLogicSensor>();
                if (nls != null)
                {
                    if (nls.EventValue)
                        _formWizard.rgLogicSensors.EditValue = 1;
                    else
                        _formWizard.rgLogicSensors.EditValue = 0;
                }
                if (_formWizard.Ni.GetNoticeSubItemsCount<NoticeCheckZone>() > 0)
                    _formWizard.chLogicSensorZones.EditValue = true;
            }
        }

        public override void SelectPage()
        {
            if (NextState != null)
            {
                if ((bool)_formWizard.chLogicSensorZones.EditValue)
                    NextState(new StateCheckZone(_formWizard));
                else
                    NextState(new StatePopupUsers(_formWizard));
            }
        }

        public override int PageIndex()
        {
            return 10;
        }

        public override bool SetPage()
        {
            if (UpdateSubItem())
            {
                try
                {
                    _formWizard.PageChanged = false;
                    _formWizard.Ni.ClearSubItems<NoticeLogicSensor>();
                    NoticeLogicSensor ns = new NoticeLogicSensor(_formWizard.Ni, 0, Convert.ToBoolean(_formWizard.rgLogicSensors.EditValue));
                    _formWizard.Ni.AddSubItem(ns);
                    return true;
                }
                catch
                {
                    return false;
                }
            }
            return true;
        }
    }

}
