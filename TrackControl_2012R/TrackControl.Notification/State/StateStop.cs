﻿using System;
using System.Collections.Generic;
using System.Text;
using Agro.Utilites;

namespace TrackControl.Notification
{
    public class StateStop : StateBase
    {
        public override event StateHandler NextState;

        public StateStop(NoticeWizard formWizard)
            : base(formWizard)
        {
        }

        public override void InitPage()
        {
            if (!_formWizard.Ni.IsNew)
            {
                NoticeStop nstop = (NoticeStop)_formWizard.Ni.GetSubItem<NoticeStop>();
                if (nstop != null)
                {
                    _formWizard.teStopTime.Text = nstop.StopTime.ToString();
                }
            }
        }

        public override void SelectPage()
        {
            if (NextState != null)
            {
                NextState(new StatePopupUsers(_formWizard));
            }
        }

        public override int PageIndex()
        {
            return 6;
        }

        public override bool SetPage()
        {
            if (UpdateSubItem())
            {
                _formWizard.PageChanged = false;
                if (!DicUtilites.ValidateFieldsTextDoublePositive(_formWizard.teStopTime, _formWizard.dxErrP)) return false;
                _formWizard.Ni.ClearSubItems<NoticeStop>();
                double stop_time;
                if (Double.TryParse(_formWizard.teStopTime.Text, out stop_time))
                {
                    NoticeStop ns = new NoticeStop(_formWizard.Ni, stop_time);
                    _formWizard.Ni.AddSubItem(ns);
                    return true;
                }
                return false;
            }
            return true;
        }
    }
}
