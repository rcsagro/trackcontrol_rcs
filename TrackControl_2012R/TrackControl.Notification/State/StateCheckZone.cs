﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using TrackControl.MySqlDal;
using TrackControl.General;

namespace TrackControl.Notification
{
    public class StateCheckZone : StateBase
    {
        public override event StateHandler NextState;

        public StateCheckZone(NoticeWizard formWizard)
            : base(formWizard)
        {

        }

        public override void InitPage()
        {
            InitGrid();
            if (_formWizard.Ni.IsNew)
            {
                InitForSpecificTypes();
            }
            else
            {
                SetControls();

            }
        }

        private void SetControls()
        {
            int zoneTypeEvent = FillGrid();
            switch (zoneTypeEvent)
            {
                case (int)ZoneEventTypes.EntryToZone:
                    _formWizard.chZoneCross.EditValue = true;
                    _formWizard.rgZoneTypeEventCross.EditValue = zoneTypeEvent;
                    break;
                case (int)ZoneEventTypes.ExitFromZone:
                    _formWizard.chZoneCross.EditValue = true;
                    _formWizard.rgZoneTypeEventCross.EditValue = zoneTypeEvent;
                    break;
                case (int)ZoneEventTypes.LocateInZone:
                    _formWizard.chZoneLocate.EditValue = true;
                    _formWizard.rgZoneTypeEventLocate.EditValue = zoneTypeEvent;
                    SetBeforAddItems();
                    break;
                case (int)ZoneEventTypes.LocateOutZone:
                    _formWizard.chZoneLocate.EditValue = true;
                    _formWizard.rgZoneTypeEventLocate.EditValue = zoneTypeEvent;
                    SetBeforAddItems();
                    break;
                default:
                    _formWizard.chZoneLocate.EditValue = true;
                    _formWizard.rgZoneTypeEventLocate.EditValue = (int)ZoneEventTypes.LocateInZone;
                    SetBeforAddItems();
                    break;
            }
        }

        private int FillGrid()
        {
            _formWizard.gvZones.ClearSelection();
            int zoneTypeEvent = ConstsGen.RECORD_MISSING;
            if (_formWizard.Ni.NoticeSIs == null) return 0;
            foreach (NoticeSubItem nsi in _formWizard.Ni.NoticeSIs)
            {

                if (nsi is NoticeCheckZone)
                {
                    int Row_Find = _formWizard.gvZones.LocateByValue(0, _formWizard.gvZones.Columns["Zone_ID"], ((NoticeCheckZone)nsi).Zone_Id);
                    if (Row_Find >= 0)
                    {
                        _formWizard.gvZones.SelectRow(Row_Find);
                        _formWizard.gvZones.FocusedRowHandle = Row_Find;
                        _formWizard.rgZoneTypeEventCross.EditValue = (int)((NoticeCheckZone)nsi).ZoneEventType;
                        if (zoneTypeEvent == ConstsGen.RECORD_MISSING)
                        {
                            zoneTypeEvent = (int)((NoticeCheckZone)nsi).ZoneEventType;
                        }
                    }
                }
            }
            return zoneTypeEvent;
        }

        private void InitGrid()
        {
            ZonesProvider _zprov = new ZonesProvider();
            _formWizard.gcZones.DataSource = _zprov.GetAllDataTable();
        }

        private void InitForSpecificTypes()
        {
            switch (Convert.ToInt16(_formWizard.grSelectType.EditValue))
            {
                case (int)NoticeSubItemTypes.SensorLogicControl:
                    {
                        _formWizard.chZoneLocate.EditValue = true;
                        SetBeforAddItems();
                        break;
                    }
                case (int)NoticeSubItemTypes.SensorControl:
                    {
                        _formWizard.chZoneLocate.EditValue = true;
                        SetBeforAddItems();
                        break;
                    }
            }
        }

        public override void SelectPage()
        {
            if (NextState != null)
            {
                //факт пересечения зоны не требует доп настроек + если доп настройки отключены, то уже выбраны на пред шаге 
                if ((bool)_formWizard.chZoneCross.EditValue || !_formWizard.rgZoneAddItem.Visible)
                {
                    NextState(new StatePopupUsers(_formWizard)); ;
                }
                else
                {
                    switch ((int)_formWizard.rgZoneAddItem.EditValue)
                    {
                        case (int)NoticeSubItemTypes.SpeedControl:
                            {
                                NextState(new StateSpeed(_formWizard));
                                break;
                            }
                        case (int)NoticeSubItemTypes.StopControl:
                            {
                                NextState(new StateStop(_formWizard));
                                break;
                            }
                        case (int)NoticeSubItemTypes.TimeControl:
                            {
                                NextState(new StateTime(_formWizard));
                                break;
                            }
                        default:
                            {
                                NextState(new StatePopupUsers(_formWizard));
                                break;
                            }
                    }
                }
            }
        }

        public override int PageIndex()
        {
            return 4;
        }

        public override bool SetPage()
        {
            if (UpdateSubItem())
            {
                _formWizard.PageChanged = false;
                _formWizard.Ni.ClearSubItems<NoticeCheckZone>();
                if (_formWizard.gvZones.SelectedRowsCount == 0) return false;
                for (int i = 0; i < _formWizard.gvZones.SelectedRowsCount; i++)
                {
                    DataRow row_zone = _formWizard.gvZones.GetDataRow(_formWizard.gvZones.GetSelectedRows()[i]);
                    int idZone;
                    if (Int32.TryParse(row_zone["Zone_ID"].ToString(), out idZone))
                    {
                        NoticeCheckZone ncz = new NoticeCheckZone(_formWizard.Ni, idZone, ((bool)_formWizard.chZoneCross.EditValue) ? (ZoneEventTypes)_formWizard.rgZoneTypeEventCross.EditValue : (ZoneEventTypes)_formWizard.rgZoneTypeEventLocate.EditValue);
                        _formWizard.Ni.AddSubItem(ncz);
                    }
                }

            }
            return true;
        }

        private void SetBeforAddItems()
        {
            if (_formWizard.Ni.GetNoticeSubItemsCount<NoticeSpeed>() > 0)
            {
                _formWizard.rgZoneAddItem.Visible = true;
                _formWizard.rgZoneAddItem.EditValue = (int)NoticeSubItemTypes.SpeedControl;
            }
            else if (_formWizard.Ni.GetNoticeSubItemsCount<NoticeStop>() > 0)
            {
                _formWizard.rgZoneAddItem.Visible = true;
                _formWizard.rgZoneAddItem.EditValue = (int)NoticeSubItemTypes.StopControl;
            }
            else if (_formWizard.Ni.GetNoticeSubItemsCount<NoticeTimeControl>() > 0)
            {
                _formWizard.rgZoneAddItem.Visible = true;
                _formWizard.rgZoneAddItem.EditValue = (int)NoticeSubItemTypes.TimeControl;
            }
            else if (_formWizard.Ni.GetNoticeSubItemsCount<NoticeLogicSensor>() > 0 || _formWizard.Ni.GetNoticeSubItemsCount<NoticeSensor>() > 0)
            {
                _formWizard.rgZoneAddItem.Visible = false;
                _formWizard.chZoneCross.Enabled  = false;
            }
        }


    }
}
