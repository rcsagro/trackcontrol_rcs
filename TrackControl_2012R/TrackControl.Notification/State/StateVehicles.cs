﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using TrackControl.Vehicles;
using TrackControl.MySqlDal;

namespace TrackControl.Notification
{
    public class StateVehicles : StateBase
    {
        public override event StateHandler NextState;

        public StateVehicles(NoticeWizard formWizard)
            : base(formWizard)
        {

        }

        public override void InitPage()
        {
            VehicleProvider _vprov = new VehicleProvider();
            _formWizard.gcVehicles.DataSource = _vprov.GetAllDataTable();
            _formWizard.gvVehicles.ClearSelection();
            if (!_formWizard.Ni.IsNew)
            {
                List<Vehicle> vehicles = GetVehiclesList();
                if (vehicles == null) return;
                foreach (Vehicle vh in vehicles)
                {
                    int Row_Find = _formWizard.gvVehicles.LocateByValue(0, _formWizard.gvVehicles.Columns["TID"], vh.Mobitel.Id);
                    if (Row_Find >= 0)
                    {
                        _formWizard.gvVehicles.SelectRow(Row_Find);
                        _formWizard.gvVehicles.FocusedRowHandle = Row_Find; 
                    }
                }
            }
        }

        private List<Vehicle> GetVehiclesList()
        {
            List<Vehicle> vehicles;
            switch (Convert.ToInt16(_formWizard.grSelectType.EditValue))
            {
                case (int)NoticeSubItemTypes.SensorFlowTypeControl:
                    {
                        vehicles = _formWizard.Ni.VehiclesForAddFunction;
                        break;
                    }
                default:
                    {
                        vehicles = _formWizard.Ni.Vehicles;
                        break;
                    }
            }
            return vehicles;
        }

        public override void SelectPage()
        {
            if (NextState != null)
            {
                switch (Convert.ToInt16(_formWizard.grSelectType.EditValue))
                {
                    case (int)NoticeSubItemTypes.CheckZoneControl:
                        {
                            NextState(new StateCheckZone(_formWizard));
                            break;
                        }
                    case (int)NoticeSubItemTypes.SpeedControl:
                        {
                            NextState(new StateSpeed(_formWizard));
                            break;
                        }
                    case (int)NoticeSubItemTypes.StopControl:
                        {
                            NextState(new StateStop(_formWizard));
                            break;
                        }
                    case (int)NoticeSubItemTypes.LossData:
                        {
                            NextState(new StateLossData(_formWizard));
                            break;
                        }
                    case (int)NoticeSubItemTypes.LossGps:
                        {
                            NextState(new StateLossGPS(_formWizard));
                            break;
                        }
                    case (int)NoticeSubItemTypes.LossPower:
                        {
                            NextState(new StateLossPower(_formWizard));
                            break;
                        }
                    case (int)NoticeSubItemTypes.SensorFlowTypeControl:
                        {
                            NextState(new StateFlowTypeSensor(_formWizard));
                            break;
                        }
                        
                }
            }
        }

        public override int PageIndex()
        {
            return 1;
        }
        public override bool SetPage()
        {
            if (UpdateSubItem())
            {
                _formWizard.PageChanged = false;
                ClearVehicleList();
                if (_formWizard.gvVehicles.SelectedRowsCount == 0) return false;
                for (int i = 0; i < _formWizard.gvVehicles.SelectedRowsCount; i++)
                {
                    DataRow row_veh = _formWizard.gvVehicles.GetDataRow(_formWizard.gvVehicles.GetSelectedRows()[i]);
                    AddVehicle(row_veh);
                }

            }
            return true;
        }

        private void AddVehicle(DataRow row_veh)
        {

            switch (Convert.ToInt16(_formWizard.grSelectType.EditValue))
            {
                case (int)NoticeSubItemTypes.SensorFlowTypeControl:
                    {
                        _formWizard.Ni.AddVehicleForAddFunction((int)row_veh["TID"]);
                        break;
                    }
                default:
                    {
                        _formWizard.Ni.AddVehicle((int)row_veh["TID"]);
                        break;
                    }
            }
        }

        private void ClearVehicleList()
        {

            switch (Convert.ToInt16(_formWizard.grSelectType.EditValue))
            {
                case (int)NoticeSubItemTypes.SensorFlowTypeControl:
                    {
                        _formWizard.Ni.ClearVehiclesForAddFunction();
                        break;
                    }
                default:
                    {
                        _formWizard.Ni.ClearVehicles();
                        break;
                    }
            }
        }
    }
}
