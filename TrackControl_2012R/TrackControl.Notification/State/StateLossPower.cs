﻿using System;
using System.Collections.Generic;
using System.Text;
using Agro.Utilites;

namespace TrackControl.Notification
{
    public class StateLossPower : StateBase
    {
        public override event StateHandler NextState;
        public StateLossPower(NoticeWizard formWizard):base(formWizard)
        {
        }

        public override void InitPage()
        {
            if (!_formWizard.Ni.IsNew)
            {
                NoticeLossPower npower = (NoticeLossPower)_formWizard.Ni.GetSubItem<NoticeLossPower>();
                if (npower != null)
                {
                    _formWizard.tePower.Text = npower.Loss_time.ToString();
                }
            }
        }

        public override void SelectPage()
        {
            if (NextState != null)
            {
                NextState(new StatePopupUsers(_formWizard));
            }
        }

        public override int PageIndex()
        {
            return 11;
        }
        public override bool SetPage()
        {
            if (UpdateSubItem())
            {
                _formWizard.PageChanged = false;
                if (!DicUtilites.ValidateFieldsTextDoublePositive(_formWizard.tePower, _formWizard.dxErrP)) return false;
                _formWizard.Ni.ClearSubItems<NoticeLossPower>();
                double loss_time;
                if (Double.TryParse(_formWizard.tePower.Text, out loss_time))
                {
                    NoticeLossPower ns = new NoticeLossPower(_formWizard.Ni, loss_time);
                    _formWizard.Ni.AddSubItem(ns);
                    return true;
                }
                return false;
            }
            return true;
        }
    }
}
