﻿using TrackControl.Notification.Properties;
using System;

namespace TrackControl.Notification
{
    public class StateSelectType : StateBase
    {
        public override event StateHandler NextState;
        public StateSelectType(NoticeWizard formWizard)
            : base(formWizard)
        {
        }

        public override void InitPage()
        {
            if (_formWizard.Ni.GetNoticeSubItemsCount<NoticeLogicSensor>() > 0)
                _formWizard.grSelectType.EditValue = (short)NoticeSubItemTypes.SensorLogicControl;
            else if (_formWizard.Ni.GetNoticeSubItemsCount<NoticeSensor>() > 0)
                _formWizard.grSelectType.EditValue = (short)NoticeSubItemTypes.SensorControl;
            else if (_formWizard.Ni.GetNoticeSubItemsCount<NoticeCheckZone>() > 0)
                _formWizard.grSelectType.EditValue = (short)NoticeSubItemTypes.CheckZoneControl;
            else if (_formWizard.Ni.GetNoticeSubItemsCount<NoticeSpeed>() > 0)
                _formWizard.grSelectType.EditValue = (short)NoticeSubItemTypes.SpeedControl;
            else if (_formWizard.Ni.GetNoticeSubItemsCount<NoticeStop>() > 0)
                _formWizard.grSelectType.EditValue = (short)NoticeSubItemTypes.StopControl;
            else if (_formWizard.Ni.GetNoticeSubItemsCount<NoticeLossData>() > 0)
                _formWizard.grSelectType.EditValue = (short)NoticeSubItemTypes.LossData;
            else if (_formWizard.Ni.GetNoticeSubItemsCount<NoticeLossGPS>() > 0)
                _formWizard.grSelectType.EditValue = (short)NoticeSubItemTypes.LossGps;
            else if (_formWizard.Ni.GetNoticeSubItemsCount<NoticeLossPower>() > 0)
                _formWizard.grSelectType.EditValue = (short)NoticeSubItemTypes.LossPower;
            else if (_formWizard.Ni.GetNoticeSubItemsCount<NoticeFlowTypeSensor>() > 0)
                _formWizard.grSelectType.EditValue = (short)NoticeSubItemTypes.SensorFlowTypeControl;
            else if (_formWizard.Ni.GetNoticeSubItemsCount<NoticeFuelDischarge>() > 0)
                _formWizard.grSelectType.EditValue = (short)NoticeSubItemTypes.FuelDischarge;
            else if (_formWizard.Ni.GetNoticeSubItemsCount<NoticeVehicleUnload>() > 0)
                _formWizard.grSelectType.EditValue = (short)NoticeSubItemTypes.VehicleUnload;
            else if (_formWizard.Ni.GetNoticeSubItemsCount<NoticeSpeedAgroWork>() > 0)
                _formWizard.grSelectType.EditValue = (short)NoticeSubItemTypes.SpeedAgroWorksControl;
        }

        public override void SelectPage()
        {
            if (NextState != null)
            {
                switch (Convert.ToInt16(_formWizard.grSelectType.EditValue))
                {
                    case (int)NoticeSubItemTypes.CheckZoneControl:
                        {

                            NextState(new StateVehicles(_formWizard));
                            break;
                        }
                    case (int)NoticeSubItemTypes.SpeedControl:
                        {
                            NextState(new StateVehicles(_formWizard));
                            break;
                        }
                    case (int)NoticeSubItemTypes.StopControl:
                        {
                            NextState(new StateVehicles(_formWizard));
                            break;
                        }
                    case (int)NoticeSubItemTypes.LossData:
                        {
                            NextState(new StateVehicles(_formWizard));
                            break;
                        }
                    case (int)NoticeSubItemTypes.LossGps:
                        {
                            NextState(new StateVehicles(_formWizard));
                            break;
                        }
                    case (int)NoticeSubItemTypes.SensorControl:
                        {
                            NextState(new StateVehiclesSensors(_formWizard));
                            break;
                        }
                    case (int)NoticeSubItemTypes.SensorLogicControl:
                        {
                            NextState(new StateVehiclesLogicSensors(_formWizard));
                            break;
                        }
                    case (int)NoticeSubItemTypes.LossPower:
                        {
                            NextState(new StateVehicles(_formWizard));
                            break;
                        }
                    case (int)NoticeSubItemTypes.SensorFlowTypeControl :
                        {
                            NextState(new StateVehiclesSensors(_formWizard));
                            _formWizard.wpVehicles.Text = Resources.SelectVehicleFuelSensor;
                            _formWizard.wpVehicleSensors.Text = Resources.SelectSensorsFuel;
                            break;
                        }
                    case (int)NoticeSubItemTypes.FuelDischarge:
                        {
                            NextState(new StateVehiclesSensors(_formWizard));
                            break;
                        }
                    case (int)NoticeSubItemTypes.VehicleUnload:
                        {
                            NextState(new StateVehiclesLogicSensors(_formWizard));
                            _formWizard.wpVehicleSensors.Text = Resources.SelectVehicleUnload;
                            break;
                        }
                    case (int)NoticeSubItemTypes.SpeedAgroWorksControl:
                        {
                            NextState(new StateAgroWorks(_formWizard));
                            break;
                        }
                }
            }
        }

        public override int PageIndex()
        {
            return 0;
        }
    }
}
