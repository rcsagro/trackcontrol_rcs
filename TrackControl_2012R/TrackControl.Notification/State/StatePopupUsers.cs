﻿using System.Collections.Generic;
using System.Data;
using TrackControl.MySqlDal;
using TrackControl.Vehicles;
using TrackControl.General;

namespace TrackControl.Notification
{
    public class StatePopupUsers : StateBase
    {

        public override event StateHandler NextState;

            public StatePopupUsers(NoticeWizard formWizard)
            : base(formWizard)
        {

        }

        public override void InitPage()
        {
            _formWizard.gcPopupUsers.DataSource = UserBaseProvider.GetListDataTable() ;
            _formWizard.gvPopupUsers.ClearSelection();
            //_formWizard.gvPopupUsers.OptionsSelection.EnableAppearanceFocusedRow = false;
            //if (_formWizard.gvPopupUsers.SelectedRowsCount > 0) _formWizard.gvPopupUsers.DeleteSelectedRows();
            if (!_formWizard.Ni.IsNew)
            {
                IList<PopupUser> pUsers = _formWizard.Ni.PopupUsers;
                if (pUsers == null) return;
                foreach (PopupUser pUser in pUsers)
                {
                    int Row_Find = _formWizard.gvPopupUsers.LocateByValue(0, _formWizard.gvPopupUsers.Columns["Id"], pUser.User.Id);
                    if (Row_Find >= 0)
                    {
                        _formWizard.gvPopupUsers.SelectRow(Row_Find);
                        _formWizard.gvPopupUsers.FocusedRowHandle = Row_Find;
                    }
                }
            }
        }

        public override int PageIndex()
        {
            return 14;
        }

        public override bool SetPage()
        {
            if (UpdateSubItem())
            {
                _formWizard.PageChanged = false;
                _formWizard.Ni.PopupUsers.Clear(); 
                if (_formWizard.gvPopupUsers.SelectedRowsCount == 0) return false;
                for (int i = 0; i < _formWizard.gvPopupUsers.SelectedRowsCount; i++)
                {
                    DataRow rowPUser = _formWizard.gvPopupUsers.GetDataRow(_formWizard.gvPopupUsers.GetSelectedRows()[i]);
                    if (rowPUser != null) _formWizard.Ni.AddPopupUser((int)rowPUser["Id"]);
                }

            }
            return true;
        }

        public override void SelectPage()
        {
            if (NextState != null)
            {
                NextState(new StateProperty(_formWizard));

            }
        }
    }
}

