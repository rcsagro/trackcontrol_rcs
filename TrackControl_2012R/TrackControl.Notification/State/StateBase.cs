﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TrackControl.Notification
{
    public abstract class StateBase:IState
    {
        public abstract event StateHandler NextState;

        protected NoticeWizard _formWizard;

        public StateBase(NoticeWizard formWizard)
        {
            _formWizard = formWizard;
            InitPage();
        }
        public virtual void InitPage()
        {

        }
        public virtual void SelectPage()
        {

        }
        public virtual int PageIndex()
        {
            return 0;
        }
        public virtual bool SetPage()
        {
            return true;
        }
        protected bool UpdateSubItem()
        {
            if (_formWizard.PageChanged || _formWizard.Ni.IsNew)
                return true;
            else
                return false;
        }
    }
}
