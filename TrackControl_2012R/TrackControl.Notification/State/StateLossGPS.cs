﻿using System;
using System.Collections.Generic;
using System.Text;
using Agro.Utilites;

namespace TrackControl.Notification
{
    public class StateLossGPS : StateBase
    {
        public override event StateHandler NextState;

        public  StateLossGPS(NoticeWizard formWizard)
            : base(formWizard)
        {
        }

        public override void InitPage()
        {
            if (!_formWizard.Ni.IsNew)
            {
                NoticeLossGPS nlGPS = (NoticeLossGPS)_formWizard.Ni.GetSubItem<NoticeLossGPS>();
                if (nlGPS != null)
                {
                    _formWizard.teLossGPS.Text = nlGPS.Loss_time.ToString();
                }
            }
        }
        public override void SelectPage()
        {
            if (NextState != null)
            {
                NextState(new StatePopupUsers(_formWizard));
            }
        }

        public override int PageIndex()
        {
            return 8;
        }

        public override bool SetPage()
        {
            if (UpdateSubItem())
            {
                _formWizard.PageChanged = false;
                if (!DicUtilites.ValidateFieldsTextDoublePositive(_formWizard.teLossGPS, _formWizard.dxErrP)) return false;
                _formWizard.Ni.ClearSubItems<NoticeLossGPS>();
                double loss_time_min;
                if (Double.TryParse(_formWizard.teLossGPS.Text, out loss_time_min))
                {
                    NoticeLossGPS ns = new NoticeLossGPS(_formWizard.Ni, loss_time_min);
                    _formWizard.Ni.AddSubItem(ns);
                    return true;
                }
                return false;
            }
            return true;
        }
    }


}
