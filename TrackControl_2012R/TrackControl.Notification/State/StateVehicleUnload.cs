﻿using System;
using Agro.Utilites;

namespace TrackControl.Notification
{
    public class StateVehicleUnload : StateBase 
    {
        public override event StateHandler NextState;

        public StateVehicleUnload(NoticeWizard formWizard)
            : base(formWizard)
        {

        }

        public override void InitPage()
        {
            if (!_formWizard.Ni.IsNew)
            {
                var vehUnload = (NoticeVehicleUnload)_formWizard.Ni.GetSubItem<NoticeVehicleUnload>();
                if (vehUnload != null)
                {
                    _formWizard.teTimeMaxUnload.Text = vehUnload.TimeMaxUnload.ToString();

                }
            }
            else
            {
                    _formWizard.teTimeMaxUnload.Text = "0";
            }
        }

        public override void SelectPage()
        {
            if (NextState != null)
            {
                NextState(new StatePopupUsers(_formWizard));
            }
        }


        public override int PageIndex()
        {
            return 16;
        }

        public override bool SetPage()
        {
            if (UpdateSubItem())
            {
                _formWizard.PageChanged = false;
                if (!DicUtilites.ValidateFieldsTextIntPositive(_formWizard.teTimeMaxUnload, _formWizard.dxErrP)) return false;
                _formWizard.Ni.ClearSubItems<NoticeVehicleUnload>();
                int maxTimeUnload = 0;
                if (Int32.TryParse(_formWizard.teTimeMaxUnload.Text, out maxTimeUnload))
                {
                    var ns = new NoticeVehicleUnload(_formWizard.Ni, maxTimeUnload);
                    _formWizard.Ni.AddSubItem(ns);
                    return true;
                }
                return false;
            }
            return true;
        }
    }
}
