﻿using System;
using System.Collections.Generic;
using System.Text;
using Agro.Utilites;

namespace TrackControl.Notification
{
    public class StateSpeed : StateBase
    {
        public override event StateHandler NextState;

        public StateSpeed(NoticeWizard formWizard)
            : base(formWizard)
        {

        }

        public override void InitPage()
        {
            if (!_formWizard.Ni.IsNew)
            {
                var nspeed = (NoticeSpeed)_formWizard.Ni.GetSubItem<NoticeSpeed>();
                if (nspeed != null)
                {
                    _formWizard.teSpeedMin.Text = nspeed.SpeedMin.ToString();
                    _formWizard.teSpeedMax.Text = nspeed.SpeedMax.ToString();
                }
            }
        }

        public override void SelectPage()
        {
            if (NextState != null)
            {
                NextState(new StatePopupUsers(_formWizard));
            }
        }

        public override int PageIndex()
        {
            return 5;
        }

        public override bool SetPage()
        {
            if (UpdateSubItem())
            {
                _formWizard.PageChanged = false;
                if (!DicUtilites.ValidateFieldsTextDoublePositive(_formWizard.teSpeedMin, _formWizard.dxErrP) |
                   !DicUtilites.ValidateFieldsTextDoublePositive(_formWizard.teSpeedMax, _formWizard.dxErrP)) return false;
                _formWizard.Ni.ClearSubItems<NoticeSpeed>();
                double speed_min;
                double speed_max;
                if (Double.TryParse(_formWizard.teSpeedMin.Text, out speed_min) && Double.TryParse(_formWizard.teSpeedMax.Text, out speed_max))
                {
                    var ns = new NoticeSpeed(_formWizard.Ni, speed_min, speed_max);
                    _formWizard.Ni.AddSubItem(ns);
                    return true;
                }
                return false;
            }
            return true;
        }
    }
}
