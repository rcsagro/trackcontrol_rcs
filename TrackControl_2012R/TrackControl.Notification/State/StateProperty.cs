﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using TrackControl.General;
using System.Reflection;
using Agro.Utilites;

namespace TrackControl.Notification
{
    public class StateProperty : StateBase
    {
        public override event StateHandler NextState;

        /// <summary>
        /// класс параметров уведомлений
        /// </summary>
        NoticeSetItem _nsi;
        public StateProperty(NoticeWizard formWizard)
            : base(formWizard)
        {
            
        }

        public override void InitPage()
        {
            _nsi = new NoticeSetItem();
            var rw = new ResourceWorker(Assembly.GetExecutingAssembly());
            _formWizard.leSounds.Properties.DataSource = rw.GetSoundNames();
            if (_formWizard.Ni.IsNew)
            {
                int index;
                if (Convert.ToInt16(_formWizard.grSelectType.EditValue) == (int)NoticeSubItemTypes.SensorFlowTypeControl)
                    index = Convert.ToInt16(_formWizard.grSelectType.EditValue) - 1;
                else if (Convert.ToInt16(_formWizard.grSelectType.EditValue) == (int)NoticeSubItemTypes.FuelDischarge ||
                    Convert.ToInt16(_formWizard.grSelectType.EditValue) == (int)NoticeSubItemTypes.VehicleUnload)
                    index = Convert.ToInt16(_formWizard.grSelectType.EditValue) - 2;
                else if (Convert.ToInt16(_formWizard.grSelectType.EditValue) == (int)NoticeSubItemTypes.SpeedAgroWorksControl)
                    index = 1;
                else
                    index = Convert.ToInt16(_formWizard.grSelectType.EditValue);
                _formWizard.peNotice.Image = _formWizard.icTypesSmall.Images[index];
                _formWizard.pePopup.Image = _formWizard.icTypesLarge.Images[index];
                _formWizard.teTimeControl.Text = _nsi.TimeForControl.ToString();
            }
            else
            {
                _formWizard.teTitle.Text = _formWizard.Ni.Title;
                _formWizard.cheActiveNotice.EditValue = _formWizard.Ni.IsActive;
                _formWizard.cheActiveEmail.EditValue = _formWizard.Ni.IsEmailActive;
                _formWizard.gcMail.Enabled = (bool)_formWizard.cheActiveEmail.EditValue;
                _formWizard.chePopup.EditValue = _formWizard.Ni.IsPopup;
                if (_formWizard.Ni.IconSmall != null)
                    _formWizard.peNotice.Image = ImageConvert.byteArrayToImage(_formWizard.Ni.IconSmall);
                if (_formWizard.Ni.IconLarge != null)
                    _formWizard.pePopup.Image = ImageConvert.byteArrayToImage(_formWizard.Ni.IconLarge);
                _formWizard.leSounds.EditValue = _formWizard.Ni.SoundName;
                _formWizard.teTimeControl.Text = _formWizard.Ni.TimeForControl.ToString();
                _formWizard.tePeriod.Text = _formWizard.Ni.Period.ToString();
            }
            _formWizard.gcMail.DataSource = _formWizard.Ni.Emails;
        }
        public override void SelectPage()
        {
            if (NextState != null)
            {
                NextState(new StatePopupUsers(_formWizard));
            }
        }

        public override int PageIndex()
        {
            return 18;
        }
        public override bool SetPage()
        {
            if (_formWizard.Ni != null && UpdateSubItem())
            {
                _formWizard.PageChanged = false;
                if (!DicUtilites.ValidateFieldsText(_formWizard.teTitle, _formWizard.dxErrP)) return false;
                _formWizard.Ni.Title = _formWizard.teTitle.Text;
                if (!DicUtilites.ValidateFieldsTextDoublePositive(_formWizard.teTimeControl, _formWizard.dxErrP)) return false;
                _formWizard.Ni.TimeForControl = Convert.ToDouble(_formWizard.teTimeControl.Text);
                if (_formWizard.peNotice.Image == null)
                    _formWizard.Ni.IconSmall = null;
                else
                    _formWizard.Ni.IconSmall = ImageConvert.imageToByteArray(_formWizard.peNotice.Image);
                if (_formWizard.pePopup.Image == null)
                    _formWizard.Ni.IconLarge = null;
                else
                    _formWizard.Ni.IconLarge = ImageConvert.imageToByteArray(_formWizard.pePopup.Image);
                _formWizard.Ni.IsActive = (bool)_formWizard.cheActiveNotice.EditValue;
                _formWizard.Ni.IsEmailActive = (bool)_formWizard.cheActiveEmail.EditValue;
                _formWizard.Ni.IsPopup = (bool)_formWizard.chePopup.EditValue;if (_formWizard.leSounds != null) _formWizard.Ni.SoundName = _formWizard.leSounds.Text;
                if (!DicUtilites.ValidateFieldsTextIntPositive(_formWizard.tePeriod, _formWizard.dxErrP)) return false;
                _formWizard.Ni.Period = Convert.ToInt32(_formWizard.tePeriod.Text);
            }
            return true;
        }
    }
}
