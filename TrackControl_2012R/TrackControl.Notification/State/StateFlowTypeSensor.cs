﻿using System;
using Agro.Utilites;

namespace TrackControl.Notification
{
    public class StateFlowTypeSensor: StateBase
    {


            public StateFlowTypeSensor(NoticeWizard formWizard)
            : base(formWizard)
        {

        }

        public override event StateHandler NextState;

        public override void InitPage()
        {
            if (!_formWizard.Ni.IsNew)
            {
                NoticeFlowTypeSensor ftSensor = (NoticeFlowTypeSensor)_formWizard.Ni.GetSubItem<NoticeFlowTypeSensor>();
                if (ftSensor != null)
                {
                    _formWizard.teFlowTypeRadius.Text = ftSensor.SearchRadius.ToString() ;
                    _formWizard.teFlowTypeTimeForFind.Text = ftSensor.SearchTime.ToString();
                    _formWizard.teFlowTypeTimeForAction.Text = ftSensor.MinActionTime.ToString();
                    _formWizard.teFlowTypeTimeForStop.Text = ftSensor.TimeForBreakSearch.ToString();
                    _formWizard.cheFlowTypeDUT.EditValue = ftSensor.IsDutSearch;
                }
            }
        }

        public override void SelectPage()
        {
            if (NextState != null)
            {
                NextState(new StatePopupUsers(_formWizard));
            }
        }

        public override int PageIndex()
        {
            return 12;
        }

        public override bool SetPage()
        {
            if (UpdateSubItem())
            {
                _formWizard.PageChanged = false;
                if (!DicUtilites.ValidateFieldsTextIntPositive(_formWizard.teFlowTypeRadius, _formWizard.dxErrP)) return false;
                if (!DicUtilites.ValidateFieldsTextIntPositive(_formWizard.teFlowTypeTimeForFind, _formWizard.dxErrP)) return false;
                if (!DicUtilites.ValidateFieldsTextIntPositive(_formWizard.teFlowTypeTimeForStop, _formWizard.dxErrP)) return false;
                if (!DicUtilites.ValidateFieldsTextIntPositive(_formWizard.teFlowTypeTimeForAction, _formWizard.dxErrP)) return false;
                _formWizard.Ni.ClearSubItems<NoticeFlowTypeSensor>();
                int radius;
                int timeForFind;
                int timeForAction;
                int timeForStop;
                if (Int32.TryParse(_formWizard.teFlowTypeRadius.Text, out radius)
                    && Int32.TryParse(_formWizard.teFlowTypeTimeForFind.Text, out timeForFind)
                    && Int32.TryParse(_formWizard.teFlowTypeTimeForAction.Text, out timeForAction)
                    && Int32.TryParse(_formWizard.teFlowTypeTimeForStop.Text, out timeForStop))
                {
                    NoticeFlowTypeSensor ns = new NoticeFlowTypeSensor(_formWizard.Ni, 0, radius, timeForFind,timeForAction, timeForStop, (bool)_formWizard.cheFlowTypeDUT.EditValue);
                    _formWizard.Ni.AddSubItem(ns);
                    return true;
                }
                return false;
            }
            return true;
        }
    }
}
