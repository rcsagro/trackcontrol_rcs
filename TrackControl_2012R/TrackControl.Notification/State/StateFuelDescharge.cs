﻿using System;
using Agro.Utilites;

namespace TrackControl.Notification
{
    public class StateFuelDescharge:StateBase 
    {
        
        public override event StateHandler NextState;

        public StateFuelDescharge(NoticeWizard formWizard)
            : base(formWizard)
        {

        }


        public override void InitPage()
        {
            if (!_formWizard.Ni.IsNew)
            {
                NoticeFuelDischarge fuelDisch = (NoticeFuelDischarge)_formWizard.Ni.GetSubItem<NoticeFuelDischarge>();
                if (fuelDisch != null)
                {
                    _formWizard.teFuelDischarge.Text = fuelDisch.Threshold.ToString();
                    _formWizard.teFuelZeroPointsCount.Text = fuelDisch.ZeroPointCount.ToString();
                    _formWizard.teFuelAvgExpense.Text = fuelDisch.AvgFuelExpense.ToString();

                }
            }
            else
            {
                if (_formWizard.Ni.Vehicles[0].Settings != null)
                _formWizard.teFuelAvgExpense.Text = _formWizard.Ni.Vehicles[0].Settings.AvgFuelRatePerHour.ToString();
            }
        }

        public override void SelectPage()
        {
            if (NextState != null)
            {
                NextState(new StatePopupUsers(_formWizard));
            }
        }


        public override int PageIndex()
        {
            return 15;
        }

        public override bool SetPage()
        {
            if (UpdateSubItem())
            {
                _formWizard.PageChanged = false;
                if (!DicUtilites.ValidateFieldsTextDoublePositive(_formWizard.teFuelDischarge, _formWizard.dxErrP)) return false;
                if (!DicUtilites.ValidateFieldsTextDoublePositive(_formWizard.teFuelAvgExpense, _formWizard.dxErrP)) return false;
                if (!DicUtilites.ValidateFieldsTextIntPositive(_formWizard.teFuelZeroPointsCount, _formWizard.dxErrP)) return false;
                _formWizard.Ni.ClearSubItems<NoticeFuelDischarge>();
                double threshold = 0;
                double avgExpense = 0;
                int zeroCount = 0;
                if (double.TryParse(_formWizard.teFuelDischarge.Text, out threshold)
                    && Int32.TryParse(_formWizard.teFuelZeroPointsCount.Text, out zeroCount)
                    && double.TryParse(_formWizard.teFuelAvgExpense.Text, out avgExpense))
                {
                    NoticeFuelDischarge ns = new NoticeFuelDischarge(_formWizard.Ni, threshold, zeroCount, avgExpense);
                    _formWizard.Ni.AddSubItem(ns);
                    return true;
                }
                return false;
            }
            return true;
        }
    }
}
