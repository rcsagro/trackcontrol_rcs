﻿using System;
using System.Collections.Generic;
using System.Text;
using Agro.Utilites;
using DevExpress.XtraScheduler;
using TrackControl.Notification.Properties;
using TrackControl.Reports;

namespace TrackControl.Notification
{
    public class StateSensors : StateBase
    {
        public override event StateHandler NextState;

        public StateSensors(NoticeWizard formWizard)
            : base(formWizard)
        {
        }

        public override void InitPage()
        {
            if (!_formWizard.Ni.IsNew)
            {
                NoticeSensor ns = (NoticeSensor)_formWizard.Ni.GetSubItem<NoticeSensor>();
                if (ns != null)
                {
                    bool rfidPresent = false;
                    foreach (var vehicle in _formWizard.Ni.Vehicles)
                    {
                        if (vehicle.Sensor.Algoritm == (int)AlgorithmType.DRIVER)
                        {
                            rfidPresent = true;
                            break;
                        }
                    }

                    if (rfidPresent)
                    {
                        _formWizard.rgSensorRange.EditValue = ns.InRange;

                        _formWizard.teValueBottom.EditValue = "1"; // min value rfid
                        _formWizard.teValueTop.EditValue = "4095"; // max value rfid  
                    }
                    else
                    {
                        _formWizard.rgSensorRange.EditValue = ns.InRange;
                        _formWizard.teValueBottom.EditValue = ns.ValueBottom;
                        _formWizard.teValueTop.EditValue = ns.ValueTop;   
                    }
                }
                if (_formWizard.Ni.GetNoticeSubItemsCount<NoticeCheckZone>() > 0)
                    _formWizard.chSensorZones.EditValue = true;
            }
            else
            {
                bool rfidPresent = false;
                foreach(var vehicle in _formWizard.Ni.Vehicles)
                {
                    if (vehicle.Sensor.Algoritm == (int)AlgorithmType.DRIVER)
                    {
                        rfidPresent = true;
                        break;
                    }
                }
                
                if(rfidPresent)
                {
                    _formWizard.rgSensorRange.EditValue = true;
                    _formWizard.teValueBottom.EditValue = "1"; // min value rfid
                    _formWizard.teValueTop.EditValue = "4095"; // max value rfid  
                }
            }
        }

        public override void SelectPage()
        {
            if (NextState != null)
            {
                if ((bool)_formWizard.chSensorZones.EditValue)
                    NextState(new StateCheckZone(_formWizard));
                else
                    NextState(new StatePopupUsers(_formWizard));
            }
        }

        public override int PageIndex()
        {
            return 9;
        }

        public override bool SetPage()
        {
            if (UpdateSubItem())
            {
                _formWizard.PageChanged = false;
                if (!DicUtilites.ValidateFieldsTextDouble(_formWizard.teValueBottom, _formWizard.dxErrP) |
                   !DicUtilites.ValidateFieldsTextDouble(_formWizard.teValueTop, _formWizard.dxErrP)) return false;
                _formWizard.Ni.ClearSubItems<NoticeSensor>();
                double valueBottom;
                double valueTop;

                
                if (Double.TryParse(_formWizard.teValueBottom.Text, out valueBottom) && Double.TryParse(_formWizard.teValueTop.Text, out valueTop))
                {
                    NoticeSensor ns = new NoticeSensor(_formWizard.Ni, 0, valueBottom, valueTop, (bool)_formWizard.rgSensorRange.EditValue);
                    _formWizard.Ni.AddSubItem(ns);
                    return true;
                }
                return false;
            }
            return true;
        }
    }
}
