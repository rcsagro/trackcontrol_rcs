﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using TrackControl.Vehicles;
using TrackControl.MySqlDal;
using Agro.Dictionaries;

namespace TrackControl.Notification
{
    public class StateAgroWorks : StateBase
    {
        public override event StateHandler NextState;

        public StateAgroWorks(NoticeWizard formWizard)
            : base(formWizard)
        {

        }

        public override void InitPage()
        {
            _formWizard.gcWorks.DataSource = DictionaryAgroWorkType.GetListForJournal();
            _formWizard.gvWorks.ClearSelection();
            if (!_formWizard.Ni.IsNew)
            {
                List<DictionaryAgroWorkType> agroWorks = _formWizard.Ni.AgroWorks;
                if (agroWorks == null) return;
                foreach (var agroWork in agroWorks)
                {
                    int rowFind = _formWizard.gvWorks.LocateByValue(0, _formWizard.gvWorks.Columns["Id"], agroWork.Id);
                    if (rowFind >= 0)
                    {
                        _formWizard.gvWorks.SelectRow(rowFind);
                        _formWizard.gvWorks.FocusedRowHandle = rowFind; 
                    }
                }
            }
        }



        public override void SelectPage()
        {
            if (NextState != null)
            {
                NextState(new StatePopupUsers(_formWizard));
            }
        }

        public override int PageIndex()
        {
            return 17;
        }
        public override bool SetPage()
        {
            if (UpdateSubItem())
            {
                _formWizard.PageChanged = false;
                _formWizard.Ni.ClearAgroWorks();
                _formWizard.Ni.DeleteSubItems();
                if (_formWizard.gvWorks.SelectedRowsCount == 0) return false;
                for (int i = 0; i < _formWizard.gvWorks.SelectedRowsCount; i++)
                {
                    DataRow rowAgroWork = _formWizard.gvWorks.GetDataRow(_formWizard.gvWorks.GetSelectedRows()[i]);
                    _formWizard.Ni.AddAgroWork((int)rowAgroWork["Id"]);
                }
                if (_formWizard.Ni.AgroWorks.Count > 0)
                {
                    var ns = new NoticeSpeedAgroWork(_formWizard.Ni);
                    _formWizard.Ni.AddSubItem(ns);
                    return true;
                }
                return false;
            }
            return true;
        }

    }
}
