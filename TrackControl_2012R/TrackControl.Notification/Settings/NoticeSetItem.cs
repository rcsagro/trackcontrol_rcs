﻿using System;
using System.Collections.Generic;
using System.Text;
using TrackControl.General;

namespace TrackControl.Notification
{
    public class NoticeSetItem
    {
        /// <summary>
        /// Количество часов хранения прочитанных записей журналов
        /// </summary>
        public  int TimeForDelete
        {
            get
            {
                return Convert.ToInt32(Parameters.ReadParTotal(Parameters.Numpar.NtfTimeForDelete, "24"));
            }
            set
            {
                int iNewValue;
                if (Int32.TryParse(value.ToString(), out iNewValue))
                    Parameters.SetParTotal(Parameters.Numpar.NtfTimeForDelete, iNewValue.ToString());
            }
        }
        /// <summary>
        /// Количество часов контроля отнсительно текущего времени 
        /// </summary>
        public  int TimeForControl
        {
            get
            {
                return Convert.ToInt32(Parameters.ReadParTotal(Parameters.Numpar.NtfTimeForControl, "12"));
            }
            set
            {
                int iNewValue;
                if (Int32.TryParse(value.ToString(), out iNewValue))
                    Parameters.SetParTotal(Parameters.Numpar.NtfTimeForControl, iNewValue.ToString());
            }
        }

        /// <summary>
        /// Период опроса входных данных, с
        /// </summary>
        public int TimePeriod
        {
            get
            {
                return Convert.ToInt32(Parameters.ReadParTotal(Parameters.Numpar.NtfTimePeriod, "5"));
            }
            set
            {
                int iNewValue;
                if (Int32.TryParse(value.ToString(), out iNewValue))
                    Parameters.SetParTotal(Parameters.Numpar.NtfTimePeriod, iNewValue.ToString());
            }
        }

        /// <summary>
        /// Последнее значение счетчика в журнале событий уведомлений
        /// </summary>
        public int LastLogId
        {
            get
            {
                return Convert.ToInt32(Parameters.ReadParTotal(Parameters.Numpar.NtfLastIdLog, "0"));
            }
            set
            {
                int iNewValue;
                if (Int32.TryParse(value.ToString(), out iNewValue))
                    Parameters.SetParTotal(Parameters.Numpar.NtfLastIdLog, iNewValue.ToString());
            }
        }

        /// <summary>
        /// Адресс Smtp сервера
        /// </summary>
        public string MailSmtpAddress
        {
            get { return Parameters.ReadParTotal(Parameters.Numpar.MailSmtpAddress, ""); }
            set
            {
                Parameters.SetParTotal(Parameters.Numpar.MailSmtpAddress, value.ToString());
            }
        }

        /// <summary>
        /// Порт Smtp сервера
        /// </summary>
        public int MailSmtpPort
        {
            get
            {
                return Convert.ToInt32(Parameters.ReadParTotal(Parameters.Numpar.MailSmtpPort, "0"));
            }
            set
            {
                int iNewValue;
                if (Int32.TryParse(value.ToString(), out iNewValue))
                    Parameters.SetParTotal(Parameters.Numpar.MailSmtpPort, iNewValue.ToString());
            }
        }

        /// <summary>
        /// логин Smtp сервера
        /// </summary>
        public string MailSmtpLogin
        {
            get { return Parameters.ReadParTotal(Parameters.Numpar.MailSmtpLogin, ""); }
            set
            {
                Parameters.SetParTotal(Parameters.Numpar.MailSmtpLogin, value.ToString());
            }
        }

        /// <summary>
        /// Пароль Smtp сервера
        /// </summary>
        public string MailSmtpPassword
        {
            get
            {
                return Parameters.ReadParTotal(Parameters.Numpar.MailSmtpPassword, "");
            }
            set
            {
                Parameters.SetParTotal(Parameters.Numpar.MailSmtpPassword, value.ToString());
            }
        }

        /// <summary>
        /// Email адрес отправителя
        /// </summary>
        public string MailAddressFrom
        {
            get
            {
                return Parameters.ReadParTotal(Parameters.Numpar.MailAddressFrom, "");
            }
            set
            {
                Parameters.SetParTotal(Parameters.Numpar.MailAddressFrom, value.ToString());
            }
        }

        /// <summary>
        /// Email адрес отправителя
        /// </summary>
        public bool MailStartTls
        {
            get
            {
                string strBool = Parameters.ReadParTotal( Parameters.Numpar.MailStartTls, "0" );
                if (strBool.Length > 2)
                {
                    return Convert.ToBoolean(strBool);
                }
                else
                return Convert.ToBoolean(Convert.ToInt32(strBool));
            }
            set
            {
                bool newValue;
                if (bool.TryParse(value.ToString(), out newValue))
                    Parameters.SetParTotal(Parameters.Numpar.MailStartTls, newValue.ToString());
            }
        }

        /// <summary>
        /// Специализация водителя для функции СОМ объекта GetHarvestSource
        /// (необходимо определить rfid именно комбайнера, предшествующий rfid водителя разгрузочной техники)
        /// </summary>
        public int ComDriverType
        {
            get
            {
                return Convert.ToInt32(Parameters.ReadParTotal(Parameters.Numpar.ComDriverType, "0"));
            }
            set
            {
                int iNewValue;
                if (Int32.TryParse(value.ToString(), out iNewValue))
                    Parameters.SetParTotal(Parameters.Numpar.ComDriverType, iNewValue.ToString());
            }
        }
    }
}
