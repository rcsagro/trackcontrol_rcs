﻿using TrackControl.Vehicles;

namespace TrackControl.Notification
{
    public   class NoticeStop : NoticeSubItem 
    {
        public Sensor SensorRfidDriver;
        public Driver EventDriver { get; private set; }
        double _stopTime;
        private bool _waitForSpeed;

        public double StopTime
        {
            get { return _stopTime; }
            set { _stopTime = value; }
        }
        public NoticeStop(NoticeItem ni,double stopTime)
            : base(ni)
        {
            _stopTime = stopTime;
            TypeEvent = NoticeSubItemTypes.StopControl;
        }
        public override bool CheckEvent(GpsData gps)
        {
            if (!gps.Valid) return false;
            if (gps.Speed == 0 )
            {
                
                if (_waitForSpeed)
                {
                    return false;
                }
                if (!_isActive)
                {
                    StartAccumulate(gps);
                    return false;
                }
                bool isEvent = TestAccumulate(gps, _stopTime);
                if (isEvent) { _waitForSpeed = true;
                    EventDriver = GetDriverFromRfid(SensorRfidDriver, gps);
                }
                return isEvent;
            }
            StopAccumulate();
            _waitForSpeed = false;
            return false;
        }

    }
}
