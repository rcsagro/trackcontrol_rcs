using System;
using System.Collections.Generic;
using System.Text;

namespace TrackControl.Notification
{
    public enum ZoneEventTypes
    {
        EntryToZone,
        ExitFromZone,
        LocateInZone,
        LocateOutZone
    }
}
