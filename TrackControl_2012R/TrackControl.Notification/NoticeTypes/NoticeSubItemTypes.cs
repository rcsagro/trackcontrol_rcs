﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TrackControl.Notification
{
    /// <summary>
    /// Контрольные зоны (вход,выход),контроль скорости, срабатывание датчика, 
    /// потеря связи, контроль времени стоянки, контроль сообщений ,срабатывание логического датчика,
    /// Заправщик/Выгрузчик зерна, Слив топлива, разгрузка комбайнов в поле
    /// </summary>
    public enum NoticeSubItemTypes
    {
        CheckZoneControl,
        SpeedControl,
        SensorControl,
        LossData,
        StopControl,
        LossPower,
        LossGps,
        SensorLogicControl,
        TimeControl,
        SensorFlowTypeControl,
        FuelDischarge,
        VehicleUnload,
        SpeedAgroWorksControl
    }
}
