﻿using System;
using System.Collections.Generic;
using BaseReports;
using BaseReports.Procedure;
using BaseReports.Procedure.Calibration;
using TrackControl.General;
using TrackControl.General.DatabaseDriver;
using TrackControl.MySqlDal;
using TrackControl.Notification.Properties;
using TrackControl.Vehicles;

namespace TrackControl.Notification
{
    public class NoticeFlowTypeSensor : NoticeSubItem 
    {

        public NoticeFlowTypeSensor(NoticeItem ni, int idSensor, int searchRadius, int searchTime, int minActionTime, int timeForBreakSearch, bool isDutSearch)
            : base(ni)
        {
            _searchRadius = searchRadius;
            _searchTime = searchTime;
            _minActionTime = minActionTime;
            _timeForBreakSearch = timeForBreakSearch;
            taFinish = new TimeAccumulator(new TimeSpan(0, 0, _timeForBreakSearch));
            taAction = new TimeAccumulator(new TimeSpan(0, 0, _minActionTime));
            _isDutSearch = isDutSearch;
            if (idSensor > 0) _sensor = new Sensor(idSensor);
            TypeEvent = NoticeSubItemTypes.SensorFlowTypeControl;
            _calibrate = new Calibrate();
            _startVehicles = new List<Vehicle>();
            _crossListVehicles = new List<Vehicle>();
            _pa = new PartialAlgorithms();
        }
        
        Vehicle _vehicleForAnalize;

        public Vehicle VehicleForAnalize
        {
            get { return _vehicleForAnalize; }
            set {
                _vehicleForAnalize = value; 
                if (_vehicleForAnalize.Settings != null)
                {
                    _fuelerMinFuelRate = _vehicleForAnalize.Settings.FuelerMinFuelRate;
                    _avgFuelRatePerHour = _vehicleForAnalize.Settings.AvgFuelRatePerHour;
                }
            }
        }


        //по факту начала выгрузки должны быть зафиксированы все транспортные средства, находящиеся в радиусе SearchRadius от комбайна
        List<Vehicle> _startVehicles;
        //по факту окончания выгрузки должны быть зафиксированы все транспортные средства, находящиеся в радиусе SearchRadius от комбайна
         //Оператору должен выдаваться список транспортных средств, являющихся пересечением множеств, полученных на первом и втором этапе
        List<Vehicle> _crossListVehicles;
        TimeAccumulator taAction;
        TimeAccumulator taFinish;

        double _fuelerMinFuelRate = 0.5;
        double _avgFuelRatePerHour = 8;

        double _sensorValuePrev = -1;
        GpsData _gpsPrev;
        GpsData _gpsStart;
        GpsData _gpsEnd;

        PartialAlgorithms _pa;

        public string CrossListVehiclesRegNumber
        {
            get 
            {
                string CrossListVehiclesRegNumber = "";
                if (_crossListVehicles.Count > 0)
                {
                    foreach (Vehicle vh in _crossListVehicles)
                    {
                        CrossListVehiclesRegNumber = GetVehicleInfor(CrossListVehiclesRegNumber, vh, vh.RegNumber);
                    }
                }
                return CrossListVehiclesRegNumber;
            }

        }

        public string CrossListVehiclesInfo
        {
            get
            {
                string crossVehiclesInfo = "";
                if (_crossListVehicles.Count > 0)
                {
                    foreach (Vehicle vh in _crossListVehicles)
                    {
                        crossVehiclesInfo = GetVehicleInfor(crossVehiclesInfo,vh, vh.Info);
                    }
                }
                return crossVehiclesInfo;
            }
        }

        private string GetVehicleInfor(string crossVehiclesInfo,Vehicle vh, string infoVeh)
        {
            if (_isDutSearch)
            {
                double liters;
                if (vh.Tag == null)
                    liters = 0;
                else
                    liters = (double)vh.Tag;
                crossVehiclesInfo = string.Format("{0}{1} -> {2} {3};", crossVehiclesInfo, infoVeh, liters.ToString(), Resources.LiterShort);

            }
            else
            {
                crossVehiclesInfo = string.Format("{0}{1};", crossVehiclesInfo, infoVeh);
            }
            return crossVehiclesInfo;
        }

        
        Calibrate _calibrate ;
        Sensor _sensor;
        public Sensor Sensor
        {
            get { return _sensor; }
            set { 
                _sensor = value;
                if (_sensor !=null && _sensor.Id >0)
                    CalibrationProvider.SetCalibration(_sensor.Id, ref _calibrate); 
            }
        }
        int _searchRadius;
        /// <summary>
        /// радиус поиска транспорта
        /// </summary>
        public int SearchRadius
        {
            get { return _searchRadius; }
            set { _searchRadius = value; }
        }

        int _searchTime;
        /// <summary>
        /// промежуток времени для данных транспорта
        /// </summary>
        public int SearchTime
        {
            get { return _searchTime; }
            set { _searchTime = value; }
        }

        int _timeForBreakSearch;
        /// <summary>
        ///  время по прошествии которого заправка/выгрузка зерна считается завершенной, если не было активных данных от датчика
        /// </summary>
        public int TimeForBreakSearch
        {
            get { return _timeForBreakSearch; }
            set { _timeForBreakSearch = value;}
        }

        
        bool _isDutSearch;
        /// <summary>
        /// для варианта с заправщиком.Фиксируется прирост уровня топлива на обнаруженнном возле заправщика транспорте
        /// </summary>
        public bool IsDutSearch
        {
            get { return _isDutSearch; }
            set { _isDutSearch = value; }
        }

        public DateTime StartDate
        {
            get { return _gpsStart.Time; }

        }

        public string StartDateFormated
        {
            get
            {
                if (_gpsStart == null)
                {
                    return "";
                }
                else
                return _gpsStart.Time.ToString("dd.MM.yyyy HH:mm"); }
        }

        public DateTime EndDate
        {
            get { return _gpsEnd.Time; }
        }

        // минимальное время заправки/выгрузки зерна 
        int _minActionTime;
        public int MinActionTime
        {
            get { return _minActionTime; }
            set { _minActionTime = value; }
        }

        public override bool CheckEvent(GpsData gps)
        {
            if (gps.Valid && _sensor != null)
            {
                if (SensorValueChange(gps))
                {
                    if (taFinish.IsActive) taFinish.StopAccumulate();  

                    if (taAction.IsActive)
                    {
                        taAction.TestAccumulate(gps.Time);  
                     }
                    else if (!taAction.IsReady)
                    {
                        if (_gpsPrev != null) taAction.PrevDate = _gpsPrev.Time;
                        taAction.StartAccumulate(gps.Time);
                        if (_gpsPrev == null)
                        {
                            _gpsStart = gps; 
                        }
                        else
                            _gpsStart = _gpsPrev; 
                    }
                      
                }
                else
                {
                    if (taAction.IsActive) taAction.StopAccumulate();

                    if (taFinish.IsActive)
                    {
                        taFinish.TestAccumulate(gps.Time);
                    }
                    else if (!taFinish.IsReady && taAction.IsReady)
                    {
                        if (_gpsPrev != null) taFinish.PrevDate = _gpsPrev.Time;
                        taFinish.StartAccumulate(gps.Time);
                        _gpsEnd = _gpsPrev;
                        GpsEvent = _gpsPrev;
                    }
                }
            }
            //Debug.Print("val {0} time {1} taAAct {2} taARd {3} taFAct {4} taFRd {5} m {6} ", _sensor.Value, gps.Time,
            //    taAction.IsActive, taAction.IsReady, taFinish.IsActive, taFinish.IsReady, gps.Mobitel);
            if (taAction.IsReady & taFinish.IsReady )
            {
                if (_gpsStart == null || _gpsEnd == null)
                {
                    taAction.IsReady = false;
                    taFinish.IsReady = false;
                    return false;
                }
                DetectStartPointVehicleList(_gpsStart);
                DetectEndPointCrossVehicleList(_gpsEnd);
                taAction.IsReady = false;
                taFinish.IsReady = false;
                _sensorValuePrev = -1;
                return true;
            }
            else
            {
                _gpsPrev = gps;
                return false;
            }
        }

        private void DetectStartPointVehicleList(GpsData gps)
        {
            _startVehicles.Clear();
            if (Ni.VehiclesForAddFunction == null || _gpsStart == null) return;
            using (DriverDb db = new DriverDb())
            {
            db.ConnectDb();
            GpsDataProvider gpsDP = new GpsDataProvider(db);
            foreach (Vehicle vh in Ni.VehiclesForAddFunction)
            {
                IList<GpsData> gpsDatas = gpsDP.GetValidDataForPeriod(vh.Mobitel.Id  , _gpsStart.Time.AddMinutes(- _searchTime),  _gpsEnd.Time);

                if (vh.MobitelId != _vehicleForAnalize.MobitelId && gpsDatas != null && gpsDatas.Count >0)
                    {
                        foreach (GpsData gpsData in gpsDatas)
                        {
                            double dist = Math.Round(Math.Abs(СGeoDistance.GetDistance(gpsData.LatLng,gps.LatLng)) * 1000,2);
                            //Debug.Print(" t {0} d {1} s {2} v {3} id {4}", gpsData.Time, dist, Math.Round(gpsData.Speed, 2), vh.RegNumber, gpsData.Id);
                            if (dist <= _searchRadius)
                            {
                                if (!_startVehicles.Contains(vh)) _startVehicles.Add(vh);
                                break;
                            }
                        }
                    }
                    
                }
            db.CloseDbConnection();
            }
        }

        private void DetectEndPointCrossVehicleList(GpsData gps)
        {
            _crossListVehicles.Clear();
            using (DriverDb db = new DriverDb())
            {
                db.ConnectDb();
                GpsDataProvider gpsDP = new GpsDataProvider(db);
                foreach (Vehicle vh in _startVehicles)
                {
                    IList<GpsData> gpsDatas = gpsDP.GetValidDataForPeriod(vh, _gpsStart.Time, _gpsEnd.Time.AddMinutes(_searchTime));
                    if (gpsDatas != null)
                    {
                        foreach (GpsData gpsData in gpsDatas)
                        {
                            double dist = Math.Round(Math.Abs(СGeoDistance.GetDistance(gpsData.LatLng, gps.LatLng)) * 1000, 2);
                            //Debug.Print(" t {0} d {1} s {2} v {3} id {4}", gpsData.Time, dist, Math.Round(gpsData.Speed, 2), vh.RegNumber, gpsData.Id);
                            if (dist <= _searchRadius)
                            {
                                if (!_crossListVehicles.Contains(vh))
                                {
                                    if (IsDutSearch) SetVehicleChangeInFuel(vh);
                                    _crossListVehicles.Add(vh);
                                    break;
                                }
                            }
                        }
                    }

                }
                db.CloseDbConnection();
                if (_startVehicles!=null) _startVehicles.Clear(); 
            }
        }

        bool SensorValueChange(GpsData gps)
        {
            //_sensor.Value = GetTestSensorValue();
            bool change = false;
            double senValue = _calibrate.GetUserValue(gps.Sensors, _sensor.BitLength, _sensor.StartBit, _sensor.K, _sensor.B, _sensor.S);
            _sensor.Value = senValue;
            if (_sensorValuePrev >= 0)
            {
                if ((_sensor.Value - _sensorValuePrev) > _fuelerMinFuelRate)
                {
                    change = true;
                }
                else if (_sensor.Value < _sensorValuePrev)
                {
                    _sensor.Value = _sensor.Value + _sensorValuePrev + _pa.GetCalculatedValue(_sensorValuePrev, _gpsPrev.Time, _sensor.Value, gps.Time, _sensor.K, _avgFuelRatePerHour);
                    if ((_sensor.Value - _sensorValuePrev) > _fuelerMinFuelRate) change = true;
                }


            }
            _sensorValuePrev = senValue;
            return change;

        }

        void SetVehicleChangeInFuel(Vehicle vh)
        {
            using (DriverDb db = new DriverDb())
            {
                db.ConnectDb();
                GpsDataProvider gpsDP = new GpsDataProvider(db);
                IList<GpsData> gpsDatas = gpsDP.GetValidDataForPeriod(vh, _gpsStart.Time.AddMinutes(-_searchTime), _gpsEnd.Time);
                vh.Tag = Math.Round(FuelLevel.GetFuelValueOnPoint(vh, gpsDatas[gpsDatas.Count - 1]) - FuelLevel.GetFuelValueOnPoint(vh, gpsDatas[0]), 2);
                db.CloseDbConnection();
            }
        }

    }
}
