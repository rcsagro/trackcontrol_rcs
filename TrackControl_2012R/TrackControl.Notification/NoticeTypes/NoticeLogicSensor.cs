﻿using TrackControl.Vehicles;

namespace TrackControl.Notification
{
    public class NoticeLogicSensor : NoticeSubItem 
    {
        public LogicSensor Sensor { get; set; }

        public bool EventValue { get; set; }

        public NoticeLogicSensor(NoticeItem ni,int idSensor, bool eventValue)
            : base(ni)
        {
            EventValue = eventValue;
            if (idSensor >0) Sensor = new LogicSensor(idSensor);
            TypeEvent = NoticeSubItemTypes.SensorLogicControl;
            _isActive = true;// срабатывает только при переходе с !eventValue в eventValue
        }
        public override bool CheckEvent(GpsData gps)
        {
            if (!gps.Valid || Sensor == null || !Sensor.Valid) return false;
            if (EventValue == Sensor.GetValue(gps.Sensors))
            {
                if (_isActive)
                    return false;
                else
                {
                    _isActive = true;
                    GpsEvent = gps;
                    return true;
                }
            }
            else
            {
                _isActive = false;
                return false;
            }
        }
    }
}
