﻿using System;
using System.Collections.Generic;
using System.Text;
using TrackControl.General;
using TrackControl.Vehicles;

namespace TrackControl.Notification
{
    class NoticeLossGPS: NoticeSubItem 
    {
       double _loss_time;
        public double Loss_time
        {
            get { return _loss_time; }
            set { _loss_time = value; }
        }

        public double Loss_time_total { get; set; }

        public NoticeLossGPS(NoticeItem ni, double loss_time)
            : base(ni)
        {
            _loss_time = loss_time;
            TypeEvent = NoticeSubItemTypes.LossGps;
        }

        public override bool CheckEvent(GpsData gps)
        {
            if (!gps.Valid )
            {
                if (!_isActive)
                {
                    StartAccumulate(gps);
                    return false;
                }else
                {
                    bool testResult = TestAccumulateManyEvents(gps, _loss_time);
                    if (testResult) Loss_time_total += _loss_time;
                    return testResult;
                }
            }
            else
            {
                StopAccumulate();
                Loss_time_total = 0;
                return false;
            }
        }
    }
}
