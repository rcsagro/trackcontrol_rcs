﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using TrackControl.Zones;
using TrackControl.Vehicles;
using TrackControl.Notification.Properties; 

namespace TrackControl.Notification
{
    /// <summary>
    /// используется при анализе ВНЕ ЗОН  
    /// </summary>
    public class NoticeCheckZoneList : NoticeCheckZone
    {
        List<Zone> _zones;
        public NoticeCheckZoneList(NoticeItem ni, int zoneId, ZoneEventTypes zoneEventType, List<Zone> zonesLocateOut)
            : base(ni, zoneId, zoneEventType)
        {
            _zones = zonesLocateOut;
            if (_zones.Count == 1)
            {
                _zone_id = _zones[0].Id;
                _zone_name = _zones[0].Name;
            }
            else
            {
                _zone_id = zoneId;
                _zone_name = Resources.ControleZoneOutside;
            }

        }
        public override bool CheckEvent(GpsData gps)
        {
            if (!PointInZones(gps))
            {
                GpsEvent = gps;
                _inZone = true;
                return true;
            }
            else
            {
                _inZone = false;
                return false;
            }
        }

        private bool PointInZones(GpsData gps)
        {
            try
            {
                foreach (Zone zone in _zones)
                {
                    if (zone.Contains(gps.LatLng))
                        return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message + "\n" + ex.StackTrace, "Error point in zones", MessageBoxButtons.OK);
                return false;
            }
        }
    }
}
