﻿using System;
using System.Collections.Generic;
using System.Text;
using TrackControl.Vehicles; 
using System.Drawing;

namespace TrackControl.Notification
{
    public class NoticeSpeed : NoticeSubItem 
    {
        double _speedMin;

        public double SpeedMin
        {
            get { return _speedMin; }
            set { _speedMin = value; }
        }
        double _speedMax;

        public double SpeedMax
        {
            get { return _speedMax; }
            set { _speedMax = value; }
        }

        public NoticeSpeed(NoticeItem ni,double speed_min, double speed_max):base(ni)
        {
            _speedMin = speed_min;
            _speedMax = speed_max;
            TypeEvent = NoticeSubItemTypes.SpeedControl;
        }
        public override bool CheckEvent(GpsData gps)
        {
            if (!gps.Valid || gps.Speed == 0) return false;
            if (gps.Speed < _speedMin | gps.Speed > _speedMax )
            {
                gps.Speed = (float)Math.Round(gps.Speed,1); 
                GpsEvent = gps;
                
                return true;
            }
            else
                return false;
        }
    }
}
