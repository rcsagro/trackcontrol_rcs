﻿using TrackControl.Vehicles;

namespace TrackControl.Notification
{
    public class NoticeTimeControl : NoticeSubItem 
    {
        double _timeForContrrol;

        public double TimeForContrrol
        {
            get { return _timeForContrrol; }
            set { _timeForContrrol = value; }
        }


        public NoticeTimeControl(NoticeItem ni, double timeForContrrol)
            : base(ni)
        {
            _timeForContrrol = timeForContrrol;
            TypeEvent = NoticeSubItemTypes.TimeControl;
        }

        public override bool CheckEvent(GpsData gps)
        {
                if (!_isActive)
                {
                    StartAccumulate(gps);
                    return false;
                }
                else
                {
                    bool valueOverLimit = TestAccumulate(gps, _timeForContrrol);
                    if (valueOverLimit) StopAccumulate();
                    return valueOverLimit;
                }

        }

    }
}
