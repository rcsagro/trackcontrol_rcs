﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TrackControl.Reports;
using TrackControl.Vehicles;
using Agro.Dictionaries;
using TrackControl.Zones;

namespace TrackControl.Notification
{
    public class NoticeSpeedAgroWork : NoticeSubItem
    {
        public Vehicle VehicleSubItem { get;  set; }
        public DictionaryAgroWorkType DefaultAgroWork;
        public DictionaryAgroAgregat DefaultAgregat;
        private readonly NoticeSpeed _speedControl;
        public Sensor SensorRfidAgregat;
        public Sensor SensorRfidDriver;
        public DictionaryAgroWorkType EventAgroWork { get; private set; }
        public DictionaryAgroAgregat EventAgregat { get; private set; }
        public Driver EventDriver { get; private set; }
        /// <summary>
        /// датчики агрегатов
        /// </summary>
        private readonly Dictionary<int, ExternalDevice> _agroSensors = new Dictionary<int, ExternalDevice>();

        public NoticeSpeedAgroWork(NoticeItem ni)
            : base(ni)
        {
            _speedControl = new NoticeSpeed(ni, 0, 0);
            TypeEvent = NoticeSubItemTypes.SpeedAgroWorksControl;
        }


        public override bool CheckEvent(GpsData gps)
        {
            bool findWork = false;
            //если есть RFID - он высший приоритет
            if (SensorRfidAgregat != null)
            {
                var rfid = (ushort)SensorRfidAgregat.GetValue(gps.Sensors);
                if (rfid > 0 && rfid < SensorRfidAgregat.GetMaxRfidValue())
                {
                    var daa = new DictionaryAgroAgregat();
                    if (daa.InitFromIdentifier(rfid))
                    {
                        foreach (var agroWorkForRfid in Ni.AgroWorks)
                        {
                            if (agroWorkForRfid.Id == daa.IdWork)
                            {
                               if (!(agroWorkForRfid.SpeedBottom==0 & agroWorkForRfid.SpeedTop==0))
                                {
                               _speedControl.SpeedMin = agroWorkForRfid.SpeedBottom;
                                _speedControl.SpeedMax = agroWorkForRfid.SpeedTop;
                                EventAgroWork = agroWorkForRfid;
                                EventAgregat = daa;
                                findWork = true;
                                break;
                                }
 
                            }
                        }
                        //агрегат опознан по RFID, но его работа по умолчанию не входит в список контролируемых работ 
                        if (!findWork) return false;
                    }
                }
            }
            if (!findWork && DefaultAgroWork != null)
            {
                _speedControl.SpeedMin = DefaultAgroWork.SpeedBottom;
                _speedControl.SpeedMax = DefaultAgroWork.SpeedTop;
                EventAgroWork = DefaultAgroWork;
                EventAgregat = DefaultAgregat;
                findWork = true;
            }


            if (findWork)
            {
                if (_speedControl.CheckEvent(gps) && IsValueActual(gps))
                {
                    if (!_isActive)
                    {
                        gps.Speed = (float)Math.Round(gps.Speed, 1);
                        GpsEvent = gps;
                        EventDriver = GetDriverFromRfid(SensorRfidDriver,gps);
                        _isActive = true;
                        return true;
                    }

                }
                else
                {
                    _isActive = false;
                }
            }
            return false;
        }




        private bool PointInZones(GpsData gps)
        {
            //return Ni.AgroZones.Any(zone => zone.Contains(gps.LatLng));
            foreach (Zone zone in Ni.AgroZones)
            {
                if (zone.Contains(gps.LatLng))
                {
                    return true;
                }

            }
            return false;
        }

        private bool IsSensorActive(GpsData gpsData)
        {
            if (_agroSensors.ContainsKey(EventAgregat.Id))
            {
                if (_agroSensors[EventAgregat.Id].Id > 0)
                {
                    return _agroSensors[EventAgregat.Id].IsActive(gpsData.Sensors, _agroSensors[EventAgregat.Id].Algoritm == (int)AlgorithmType.ANGLE);
                }
            }
            else
            {
                using (var daa = new DictionaryAgroAgregat(EventAgregat.Id))
                {
                    int idSensor = 0;
                    if (daa.HasSensor)
                    {
                        ExternalDevice agroSensor = daa.GetAgroSensor(VehicleSubItem.Id, gpsData.Mobitel, ref idSensor);
                        _agroSensors.Add(EventAgregat.Id, agroSensor);
                        if (idSensor > 0)
                        {
                            return _agroSensors[EventAgregat.Id].IsActive(gpsData.Sensors, _agroSensors[EventAgregat.Id].Algoritm == (int)AlgorithmType.ANGLE);
                        }
                    }

                }
            }
            return false;
        }

        private bool IsValueActual(GpsData gpsData)
        {
            bool sensorActive = IsSensorActive(gpsData);
            if (sensorActive) return true;
            if (_agroSensors.ContainsKey(EventAgregat.Id)) return false;
            return PointInZones(gpsData);
        }
    }
}
