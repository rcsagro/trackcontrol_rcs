﻿using System;
using TrackControl.Vehicles;
using TrackControl.General;

namespace TrackControl.Notification
{
    class NoticeLossData: NoticeSubItem 
    {
       double _loss_time;
       double _diff_time;
       public double Diff_time
       {
           get { return _diff_time; }
           set { _diff_time = value; }
       }

       DateTime _time_last_data = DateTime.MinValue;

        public double Loss_time
        {
            get { return _loss_time; }
            set { _loss_time = value; }
        }

        public NoticeLossData(NoticeItem ni,double loss_time):base(ni)
        {
            _loss_time = loss_time;
            TypeEvent = NoticeSubItemTypes.LossData;
            _time_last_data = ni.TimeStartControl;
            SetStartGps(ni);
        }

        private void SetStartGps(NoticeItem ni)
        {
            GpsEvent = new GpsData();
            GpsEvent.Time = ni.TimeStartControl;
            GpsEvent.LatLng = new PointLatLng(0, 0);
            GpsEvent.Id = 0;
        }

        public override bool CheckEvent(GpsData gps)
        {
            GpsEvent = gps;
            _time_last_data = gps.Time;
             return false;
        }

        public override bool CheckState()
        {
            _diff_time = Math.Round(DateTime.Now.Subtract(_time_last_data).TotalMinutes,0);
            if (_diff_time >= _loss_time)
            {
                _time_last_data = DateTime.Now ;
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
