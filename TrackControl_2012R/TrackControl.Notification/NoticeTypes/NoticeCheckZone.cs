﻿using System;
using System.Collections.Generic;
using System.Text;
using TrackControl.Zones;
using TrackControl.Vehicles; 


namespace TrackControl.Notification
{
    public class NoticeCheckZone:NoticeSubItem 
    {

        Zone _zone;
        protected bool _inZone;

        public Zone Zone
        {
            get { return _zone; }
            set { _zone = value; }
        }
        public NoticeCheckZone(NoticeItem ni, int zoneId, ZoneEventTypes zoneEventType)
            : base(ni)
        {
            TypeEvent = NoticeSubItemTypes.CheckZoneControl;
            _zone_id = zoneId;
            _zoneEventType = zoneEventType;
            _zone = (Zone)NoticeItem.ZonesModel.GetById(zoneId);
            if (_zone != null)
            {
                _zone_name = _zone.Name;
                Location = _zone.Name;
                switch (_zoneEventType)
                {
                    case ZoneEventTypes.EntryToZone:
                        _inZone = true;
                        break;
                    case ZoneEventTypes.ExitFromZone:
                        _inZone = false;
                        break;
                }
            }
        }

        private ZoneEventTypes _zoneEventType;

        public ZoneEventTypes ZoneEventType
        {
            get { return _zoneEventType; }
            set { _zoneEventType = value; }
        }

        protected string _zone_name;

        public string Zone_name
        {
            get { return _zone_name; }
            set { _zone_name = value; }
        }

        protected int _zone_id;
        public int Zone_Id
        {
            get { return _zone_id; }
            set
            {
                _zone_id = value;
                _zone = (Zone)NoticeItem.ZonesModel.GetById(_zone_id); 
            }
        }


        public override bool CheckEvent(GpsData gps)
        {
            if (!gps.Valid || _zone==null) return false;
            switch (_zoneEventType)
            {
                case ZoneEventTypes.EntryToZone:
                    {
                        if (_zone.Contains(gps.LatLng))
                        {
                            if (!_inZone)
                            {
                                _inZone = true;
                                GpsEvent = gps;
                                return true;
                                
                            }
                            else
                                return false;
                        }
                        else
                        {
                            _inZone = false;
                            return false;
                        }
                    }
                case ZoneEventTypes.ExitFromZone:
                    {
                        if (_zone.Contains(gps.LatLng))
                        {
                            _inZone = true;
                            return false;
                        }
                        else
                        {
                            if (_inZone)
                            {
                                GpsEvent = gps;
                                _inZone = false;
                                return true;
                                
                            }
                            else
                                return false;
                        }
                    }
                case ZoneEventTypes.LocateInZone:
                    {
                        if (_zone.Contains(gps.LatLng))
                        {
                            GpsEvent = gps;
                            _inZone = true;
                            return true;
                        }
                        else
                        {
                            _inZone = false;
                            return false;
                        }
                    }
                case ZoneEventTypes.LocateOutZone:
                    {
                        // проверяется в классе NoticeCheckZoneList
                        return false;
                    }
                default:
                    return false;
             }
        }
    }
}
