﻿using System;
using System.Collections.Generic;
using System.Text;
using TrackControl.General;
using TrackControl.TtEvents;
using TrackControl.Vehicles;

namespace TrackControl.Notification
{
    class NoticeLossPower:NoticeSubItem
    {
       double _loss_time;
        public double Loss_time
        {
            get { return _loss_time; }
            set { _loss_time = value; }
        }
        public NoticeLossPower(NoticeItem ni, double loss_time)
            : base(ni)
        {
            _loss_time = loss_time;
            TypeEvent = NoticeSubItemTypes.LossPower;
        }

        public override bool CheckEvent(GpsData gps)
        {
            if ((gps.Events & TtEventsDescr.RESERVE_POWER_ON) > 0)
            {
                if (!_isActive)
                {
                    StartAccumulate(gps);
                    return false;
                }
                else
                {
                    return TestAccumulate(gps);
                }
            }
            else if ((gps.Events & TtEventsDescr.MAIN_POWER_ON) > 0)
            {
                StopAccumulate();
                return false;
            }
            else
            {
                if (_isActive)
                {
                    return TestAccumulate(gps);
                }
                else
                    return false;
            }
        }

        private bool TestAccumulate(GpsData gps)
        {
            AddAcumulate(gps);
            if (_time_accumulate.TotalMinutes  >= _loss_time)
            {
                _time_accumulate = TimeSpan.Zero;
                GpsEvent = gps; 
                return true;
            }
            else
                return false;
        }
    }
}
