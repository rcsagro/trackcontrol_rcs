﻿using System;
using TrackControl.General;
using TrackControl.Reports;
using TrackControl.Vehicles;


namespace TrackControl.Notification
{
    public class NoticeVehicleUnload : NoticeSubItem 
    {
        //TODO messages to resourses
        public LogicSensor VehicleUnloadSensor { private get; set; }

        private Vehicle VehicleUnload { get; set; }

        private Sensor RfidSensor {  get; set; }

        public int TimeMaxUnload { get; private set; }

        public string NoticeMessage { get; private set; }

        public IZone ZoneUnload { get; private set; }

        readonly TimeAccumulator _taAction;

        private readonly int _bouncePoints = 1;

        private int _bounceCounterStart;

        private GpsData _bounceGpsStart;

        private  int _bounceCounterEnd;

        private GpsData _bounceGpsEnd;

        public NoticeVehicleUnload(NoticeItem ni, int timeMaxUnload)
            : base(ni)
        {
            TypeEvent = NoticeSubItemTypes.VehicleUnload;
            TimeMaxUnload = timeMaxUnload;
            _taAction = new TimeAccumulator(new TimeSpan(0, TimeMaxUnload, 0));
           
        }

        public override bool CheckEvent(GpsData gps)
        {
            if (!gps.Valid || VehicleUnloadSensor == null || !VehicleUnloadSensor.Valid) return false;
            if (VehicleUnloadSensor.IsActive(gps.Sensors))
            {
                _bounceCounterEnd = 0;
                if (_isActive)
                {
                    _taAction.TestAccumulate(gps.Time);  
                }
                else
                {
                    if (CheckBounce(gps, ref _bounceCounterStart, ref _bounceGpsStart))
                    {
                        SetStartUnload(_bounceGpsStart);
                        SetEventsZone();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                return CheckExcessTimeUnload(gps);
            }
            else
            {
                _bounceCounterStart = 0;
                if (_isActive)
                {
                   if (_taAction.IsActive)
                   {
                      if (CheckBounce(gps, ref _bounceCounterEnd, ref _bounceGpsEnd))
                       {
                           _taAction.AddAcumulate(_bounceGpsEnd.Time);
                           SetEndUnload(_bounceGpsEnd);
                           return true;
                       }
                       else
                       {
                           return false;
                       }
                   }
                   else
                   {
                       _isActive = false;
                       return false;
                   }

                }
                else
                {
                    return false;
                }
            }


        }

        private bool CheckBounce(GpsData gps, ref int bounceCounter, ref GpsData bounceGps)
        {
            if (bounceCounter == 0)
            {
                bounceGps = gps;
            }
            bounceCounter++;
            if (bounceCounter > _bouncePoints)
            {
                bounceCounter = 0;
                return true;
            }
            else
            {
                return false;
            }
        }

        private void SetEventsZone()
        {
            ZoneUnload = null;
            foreach (var zone in NoticeItem.ZonesModel.Zones)
            {
                if (zone.Contains(GpsEvent.LatLng))
                {
                    ZoneUnload = zone;
                    break;
                }
            }
        }

        private void SetEndUnload(GpsData gps)
        {
            _isActive = false;
            SetNoticeData(gps);
            if (_taAction.TimeAccumule.TotalMinutes >= TimeMaxUnload)
                NoticeMessage = string.Format(" Окончание выгрузки.Превышен лимит времени {0} минут", TimeMaxUnload);
            else
                NoticeMessage = string.Format(" Окончание выгрузки.Время {0}", _taAction.TimeAccumule);
            _taAction.StopAccumulate();
        }

        private void SetStartUnload(GpsData gps)
        {
            _taAction.StartAccumulate(gps.Time);
            SetNoticeData(gps);
            _isActive = true;
            NoticeMessage = " Начало выгрузки";
        }

        private bool CheckExcessTimeUnload(GpsData gps)
        {
            if (_taAction.IsReady)
            {
                _isActive = true;
                _taAction.IsReady = false;
                _taAction.IsActive = false;
                SetNoticeData(gps);
                NoticeMessage = string.Format(" Окончание выгрузки.Превышен лимит времени {0} минут", TimeMaxUnload);
                return true;
            }
            else
            {
                return false;
            }
        }

        private void SetNoticeData(GpsData gps)
        {
            if (RfidSensor.Valid) Value = RfidSensor.GetValue(gps.Sensors);
            GpsEvent = gps;
        }

        private void SetRfidSensor()
        {
            if (VehicleUnload == null) return;
            RfidSensor = new Sensor((int)AlgorithmType.DRIVER, VehicleUnload.MobitelId);
        }

        public void SetVehicleUnload(Vehicle vehicleUnload)
        {
            VehicleUnload = vehicleUnload;
            SetRfidSensor();
        }
    }
}
