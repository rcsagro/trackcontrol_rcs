﻿using System;
using System.Collections.Generic;
using BaseReports;
using BaseReports.Procedure;
using BaseReports.Procedure.Calibration;
using TrackControl.General;
using TrackControl.General.DatabaseDriver;
using TrackControl.MySqlDal;
using TrackControl.Notification.Properties;
using TrackControl.Vehicles;
using System.Diagnostics;
using System.Windows.Forms; 
 

namespace TrackControl.Notification
{
    public class NoticeFuelDischarge : NoticeSubItem 
    {
        private const int maxPointsInFuelGraph = 100;
        public double Threshold { get; set; }
        public double AvgFuelExpense { get; set; }
        public int ZeroPointCount { get; set; }
        public double PrevFuelValue { get; set; }
        public string ErrorSensorMessage { get; set; }

        Vehicle _vehicleForAnalize;

        public List<GpsData> GpsBeforEvent{ get; set; }

        public Vehicle VehicleForAnalize
        {
            get { return _vehicleForAnalize; }
            set
            {
                _vehicleForAnalize = value;
            }
        }

        private int _pointsZero;

        public NoticeFuelDischarge(NoticeItem ni, double threshold, int zeroPointCount, double avgFuelExpense)
            : base(ni)
        {
            Threshold = threshold;
            ZeroPointCount = zeroPointCount;
            AvgFuelExpense = avgFuelExpense;
            TypeEvent = NoticeSubItemTypes.FuelDischarge;
            ErrorSensorMessage = "";
            
        }

        public override bool CheckEvent(GpsData gps)
        {
            if (gps.Valid && gps.Speed == 0 && ErrorSensorMessage.Length == 0 && IsTimeAcsFromPrev(gps) )
            {
                if (GpsBeforEvent==null) GpsBeforEvent = new List<GpsData>(); 
                gps.SensorValue  = FuelLevel.GetFuelValueOnPoint(_vehicleForAnalize, gps);
                if (IsNotCheckZeroValue(gps)) return true; ;
                if (_isActive)
                {
                    GpsBeforEvent.Add(gps);
                    if ((PrevFuelValue - gps.SensorValue) >= Threshold && gps.SensorValue>0)
                    {
                        int uBound = GpsBeforEvent.Count;
                        if (uBound < 2)
                        {
                            return false;
                        }
                        if (AvgFuelExpense > 0)
                        {
                            double FuelExpense = (PrevFuelValue - gps.SensorValue)/(gps.Time.Subtract(GpsBeforEvent[0].Time).TotalHours);
                            if (FuelExpense <= AvgFuelExpense) return false;
                        }
                        GpsEvent = gps;
                        _isActive = false;
                        if (GpsBeforEvent.Count > maxPointsInFuelGraph) GpsBeforEvent.RemoveRange(0, uBound - maxPointsInFuelGraph);
                        //for (int i = 0; i < GpsBeforEvent.Count; i++)
                        //{
                        //    Debug.Print("{0} {1} {2} {3} {4}", i + 1, GpsBeforEvent[i].Time, GpsBeforEvent[i].SensorValue, GpsBeforEvent[i].Mobitel, VehicleForAnalize.VehicleUnloadSensor.Name);
                        //}
                        return true;
                    }
                    else if (gps.SensorValue > PrevFuelValue)
                    {
                        PrevFuelValue = gps.SensorValue;
                    }
                }
                else
                {
                    _isActive = true;
                    GpsBeforEvent.Clear();
                    PrevFuelValue = gps.SensorValue;
                    GpsBeforEvent.Add(gps);
                }
            }
            else
            {
                _isActive = false;
            }
            return false;
        }

        private bool IsNotCheckZeroValue(GpsData gps)
        {
            if (gps.SensorValue == 0)
            {
                if (ZeroPointCount > 0) _pointsZero++;
                if (_pointsZero > ZeroPointCount && ZeroPointCount>0)
                {
                    ErrorSensorMessage = Resources.IsSensorDisconnected;  
                    _pointsZero = 0;
                    GpsEvent = gps;
                    return true;
                }
                else
                {
                    gps.SensorValue = PrevFuelValue;
                }
            }
            else
            {
                _pointsZero = 0;
            }
            return false;
        }

        private bool IsTimeAcsFromPrev(GpsData gps)
        {
            if (GpsBeforEvent == null || GpsBeforEvent.Count == 0) return true;
            if (gps.Time.Subtract(GpsBeforEvent[GpsBeforEvent.Count - 1].Time).TotalSeconds > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }
}
