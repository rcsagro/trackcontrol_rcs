﻿using BaseReports;
using BaseReports.Procedure.Calibration;
using DevExpress.XtraReports.Serialization;
using TrackControl.Notification.Properties;
using TrackControl.Reports;
using TrackControl.Vehicles;

namespace TrackControl.Notification
{
    public class NoticeSensor : NoticeSubItem 
    {
        Calibrate _calibrate ;
        Sensor _sensor;
        Vehicle _vehicle;
        public Sensor Sensor
        {
            get { return _sensor; }
            set { 
                _sensor = value;
                if (_sensor !=null && _sensor.Id > 0)
                    CalibrationProvider.SetCalibration(_sensor.Id, ref _calibrate); 
            }
        }

        public Vehicle Vehicle
        {
            get { return _vehicle; }
            set { _vehicle = value; }
        }

        double _valueBottom;
        public double ValueBottom
        {
            get { return _valueBottom; }
            set { _valueBottom = value; }
        }

        double _valueTop;
        public double ValueTop
        {
            get { return _valueTop; }
            set { _valueTop = value; }
        }

        bool _inRange;
        public bool InRange
        {
            get { return _inRange; }
            set { _inRange = value; }
        }

        public NoticeSensor(NoticeItem ni, int idSensor, double valueBottom, double valueTop, bool inRange)
            : base(ni)
        {
            _valueBottom = valueBottom;
            _valueTop = valueTop;
            _inRange = inRange;
            if (idSensor > 0) _sensor = new Sensor(idSensor);
            TypeEvent = NoticeSubItemTypes.SensorControl;
            //_isActive = true;// срабатывает только при переходе с неактивного диапазона в активный
            _calibrate = new Calibrate();
        }

        public override bool CheckEvent(GpsData gps)
        {
            if (gps.Valid && _sensor != null)
            {
                _sensor.Value = _calibrate.GetUserValue(gps.Sensors, _sensor.BitLength, _sensor.StartBit, _sensor.K, _sensor.B, _sensor.S);
                //_sensor.Value = 723;
                bool valueInRange = (_sensor.Value >= _valueBottom && _sensor.Value <= ValueTop);
                //bool sensorActive = (valueInRange && _inRange) || (!valueInRange && !_inRange);

                if (valueInRange)
                {
                    if (Vehicle.Sensor.Algoritm == (int)AlgorithmType.DRIVER)
                        Vehicle.VehicleInfo(Vehicle.MobitelId, (ulong) _sensor.Value);
                }

                bool sensorActive = !(valueInRange ^ _inRange);
                if (sensorActive)
                {
                    if (_isActive)
                        return false;
                    else
                    {
                        _isActive = true;
                        GpsEvent = gps;
                        return true;
                    }
                }
                else
                {
                    _isActive = false;
                    return false;
                }
            }
            return false;
        }
    }
}
