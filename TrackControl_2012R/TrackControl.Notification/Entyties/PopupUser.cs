﻿using System;
using System.Collections.Generic;
using System.Text;
using TrackControl.General;

namespace TrackControl.Notification
{
    // пользователь, у которого будут всплывать попап уведомления
    public class PopupUser : Entity
    {
        UserBase user;

        public UserBase User
        {
            get { return user; }
            set { user = value; }
        }

        public PopupUser()
        {

        }
        public PopupUser(int id, int UserId)
        {
            Id = id;
            user = UserBaseProvider.GetUserBase(UserId);
        }

    }
}
