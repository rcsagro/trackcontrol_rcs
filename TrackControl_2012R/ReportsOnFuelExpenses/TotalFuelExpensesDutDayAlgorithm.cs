using System;
using System.ComponentModel;
using BaseReports.Procedure;
using LocalCache;
using TrackControl.Reports;
using TrackControl.Vehicles;
using ReportsOnFuelExpenses.Properties;

namespace ReportsOnFuelExpenses
{
    /// <summary>
    /// ������������ ����������� ������ �� �������
    /// </summary>
    public class TotalFuelExpensesDutDayAlgorithm : PartialAlgorithms
    {

        private FuelExpensesDataSet feDataSet;
        private SummaryDutDay _summaryDutDay;
        private int UseDischargeInFuelrate = 0;
        private bool RotatePresent = true;
        private AlgorithmType algorType;

        public TotalFuelExpensesDutDayAlgorithm(object algParam)
        {
            algorType = (AlgorithmType)algParam;
            feDataSet = FuelExpensesDataSetCreator.GetDataSet();
        }

        public void IsRotateSensorPresent()
        {
            RotatePresent = true;

            atlantaDataSet.sensorsRow sensor = FindSensor( AlgorithmType.ROTATE_E );

            if (sensor == null)
                RotatePresent = false;
        }

        /// <summary>
        /// ������ ��� �������
        /// </summary>
        public override void Run()
        {
            if ((GpsDatas.Length == 0) || m_row == null)
            {
                return;
            }

            //if (feDataSet != null)
            //{
            //    if (feDataSet.FuelExpensesDUT_Day.Select(string.Format("Mobitel_ID = {0}", m_row.Mobitel_ID)).Length > 0)
            //        return;
            //}

            _summaryDutDay = new SummaryDutDay();

            UseDischargeInFuelrate = 0;

            if (set_row != null)
                UseDischargeInFuelrate = set_row.FuelrateWithDischarge > 0 ? 1 : 0;

            try
            {
                IsRotateSensorPresent();
                FillFuelDayExpenses();
            }
            finally
            {
                AfterReportFilling();
                Algorithm.OnAction(this, new FuelExpensesDutDayEventArgs(m_row.Mobitel_ID, (int)algorType, _summaryDutDay));
            }
        }

		// ������� �������� ���
        private void FillFuelDayExpenses()
        {
            double FuelBeginTotal = 0;
            double FuelEndTotal = 0;

            LocalCache.atlantaDataSet.KilometrageReportDayRow[] lDataRows =
                (atlantaDataSet.KilometrageReportDayRow[])
                    AtlantaDataSet.KilometrageReportDay.Select("MobitelId=" + m_row.Mobitel_ID, "InitialTime");

            BeforeReportFilling(Resources.TotalFuelExDutDayName, lDataRows.Length); // ���������� �������� �� �����

            //��������� ������ �� ������ �������
            FuelDictionarys fuelDict = new FuelDictionarys();
            Fuel fuelAlg = new Fuel();
            fuelAlg.SettingAlgoritm(algorType);
			fuelAlg.SelectItem(m_row);  // GpsDatas = DataSetManager.GetDataGpsArray(m_row)
            fuelAlg.GettingValuesDUT(fuelDict);
            if (fuelDict.fuelSensors.Count == 0) return;
            int counter = 0;
            double totalDebite = 0.0;
            double deltaFuel = 1.0; // ���������� ������������ ������� � �
            bool isBateLimit = false;
            double prevFuelEnd = 0.0;
            double valueBegin = 0.0;

            foreach (atlantaDataSet.KilometrageReportDayRow tmp_dk_row in lDataRows)
            {
                FuelExpensesDataSet.FuelExpensesDUT_DayRow row =
                    feDataSet.FuelExpensesDUT_Day.NewFuelExpensesDUT_DayRow();

                row.Mobitel_Id = m_row.Mobitel_ID;
                row.InitialTime = tmp_dk_row.InitialTime;
                row.FinalTime = tmp_dk_row.FinalTime;
                row.Distance = tmp_dk_row.Distance;
                row.IntervalMove = tmp_dk_row.IntervalMove;
                row.IntervalWork = tmp_dk_row.IntervalWork;
                TimeSpanConverter tsc = new TimeSpanConverter();
                TimeSpan tsWork = (TimeSpan) tsc.ConvertFromInvariantString(tmp_dk_row.IntervalWork);
                TimeSpan tsMove = (TimeSpan) tsc.ConvertFromInvariantString(tmp_dk_row.IntervalMove);
                row.IntervalStop = (tsWork.Subtract(tsMove)).ToString();

                if (row.IntervalMove.Equals("00:00") && row.IntervalWork.Equals("00:00") &&
                    row.Distance <= 0)
                {
                    DateTime tm = new DateTime(row.InitialTime.Year, row.InitialTime.Month, row.InitialTime.Day, 0, 0, 0);
                    row.InitialTime = tm;
                }

                if( row.IntervalStop.Length > 5 ) // ����� ����������� �� ������
                {
                    int length = row.IntervalStop.Length;
                    int indx = length - 2 - 1;

                    row.IntervalStop = row.IntervalStop.Substring( 0, indx );
                }

                //������� � ������ � ����� ����� � ��������� ��������
                row.FuelBegin = 0;
                row.FuelEnd = 0;
                DateTime dtSeek = tmp_dk_row.InitialTime;
                dtSeek = new DateTime(dtSeek.Year, dtSeek.Month, dtSeek.Day);
                GpsData[] dArray = DataSetManager.GetDataGpsForPeriod(m_row, dtSeek, dtSeek.AddDays(1));
                //(LocalCache.atlantaDataSet.dataviewRow[])atlantaDataSet.dataview.Select("Mobitel_Id=" + m_row.Mobitel_ID.ToString() + " AND time >= #" + dtSeek.ToString("MM.dd.yyyy") + "# and time  < #" + dtSeek.AddDays(1).ToString("MM.dd.yyyy") + "#", "time");

                if (dArray.Length > 0 && fuelDict.fuelSensors.Count >0 )
                {
                    if (isBateLimit)
                    {
                        if (Math.Abs(fuelDict.ValueFuelSum[dArray[0].Id].value - prevFuelEnd) >= deltaFuel)
                        {
                            valueBegin = prevFuelEnd;
                        }
                        else
                        {
                            valueBegin = fuelDict.ValueFuelSum[dArray[0].Id].value;
                        }
                    }
                    else
                    {
                        isBateLimit = true;
                        valueBegin = fuelDict.ValueFuelSum[dArray[0].Id].value;
                    }

                    if (fuelDict.ValueFuelSum.Count > 0)
                        row.FuelBegin = valueBegin;

                    if (FuelBeginTotal == 0 && FuelEndTotal == 0)
                        FuelBeginTotal = valueBegin;

                    if (fuelDict.ValueFuelSum.Count > 0)
                    {
                        row.FuelEnd = fuelDict.ValueFuelSum[dArray[dArray.Length - 1].Id].value;
                        FuelEndTotal = fuelDict.ValueFuelSum[dArray[dArray.Length - 1].Id].value;
                        prevFuelEnd = fuelDict.ValueFuelSum[dArray[dArray.Length - 1].Id].value;
                    }

                    row.InitialPointId = (int) dArray[0].Id;
                    row.FinalPointId = (int) dArray[dArray.Length - 1].Id;
                    DateTime dt1 = dArray[0].Time;
                    DateTime dt2 = dArray[dArray.Length - 1].Time;
                }

                LocalCache.atlantaDataSet.FuelReportRow[] FRRows =
                    (LocalCache.atlantaDataSet.FuelReportRow[])
                        AtlantaDataSet.FuelReport.Select(
                            "mobitel_id=" + m_row.Mobitel_ID + " AND time_ >= #" +
                            dtSeek.ToString("MM.dd.yyyy") + "# and time_  < #" +
                            dtSeek.AddDays(1).ToString("MM.dd.yyyy") + "#", "time_");

                double dbZapr = 0;
                double dbSliv = 0;

                if (FRRows.Length > 0)
                {
                    foreach (atlantaDataSet.FuelReportRow tmp_FR_row in FRRows)
                    {
                        if (tmp_FR_row.dValue > 0)
                            dbZapr += tmp_FR_row.dValue;
                        else
                            dbSliv += tmp_FR_row.dValue;
                    }
                }

                // ������� ������ ������� = ������� � ������ ������� - ������� � ����� ������� + 
                // ����� ����������.
                row.TotalFuelAdd = dbZapr;
                row.TotalFuelSub = -dbSliv;
                if (row["FuelBegin"] != DBNull.Value)
                    row.TotalFuelUse = row.FuelBegin + row.TotalFuelAdd - row.FuelEnd - UseDischargeInFuelrate*row.TotalFuelSub;

                if (row.TotalFuelUse < 0) 
                    row.TotalFuelUse = 0;

                row.FuelUseMiddleInMotion = 0.00; // ��������� �������������, ��� �������

                if (row.Distance > 0)
                    row.FuelUseMiddleInMotion = row.TotalFuelUse * 100.0 / row.Distance;

                //������ �� ��������
                atlantaDataSet.RotateReportRow[] rot_rows = (atlantaDataSet.RotateReportRow[])
                        AtlantaDataSet.RotateReport.Select(
                            "State = '" + Resources.Working + "' AND MobitelId=" + m_row.Mobitel_ID
                            + " AND ((InitialTime >= #" + dtSeek.ToString("MM.dd.yyyy") + "# and InitialTime  < #" +
                            dtSeek.AddDays(1).ToString("MM.dd.yyyy") + "#" +
                            ") OR (FinalTime >= #" + dtSeek.ToString("MM.dd.yyyy") + "# and FinalTime  < #" +
                            dtSeek.AddDays(1).ToString("MM.dd.yyyy") + "#  and InitialTime  < #" +
                            dtSeek.ToString("MM.dd.yyyy") + "#)" +
                            " OR (InitialTime < #" + dtSeek.ToString("MM.dd.yyyy") + "# and FinalTime > #" +
                            dtSeek.AddDays(1).ToString("MM.dd.yyyy") + "#))", "InitialTime");

                TimeSpan tsEngineOnTime = new TimeSpan();
                DateTime dtBegin = new DateTime(dtSeek.Year, dtSeek.Month, dtSeek.Day);
                DateTime dtEnd = dtBegin.AddDays(1);
                
                row.idleFuelRate = 0.00;

                if (RotatePresent)
                {
                    row.TimeWithEngineOn = "00:00";
                }
                else
                {
                    row.TimeWithEngineOn = "��� ������";
                }
               
                if (rot_rows.Length > 0)
                {
                    foreach (atlantaDataSet.RotateReportRow tmp_rot_row in rot_rows)
                    {
                        //������� ����� ���� - ������ - ���������
                        DateTime dtBWork = dtBegin;
                        DateTime dtEWork = dtEnd;

                        if (tmp_rot_row.InitialTime > dtBegin) 
                            dtBWork = tmp_rot_row.InitialTime;

                        if (tmp_rot_row.FinalTime < dtEnd) 
                            dtEWork = tmp_rot_row.FinalTime;

                        tsEngineOnTime = tsEngineOnTime.Add(dtEWork - dtBWork);
                    }

                    row.TimeWithEngineOn = tsEngineOnTime.ToString();

                    if (tsEngineOnTime.TotalHours != 0)
                        row.idleFuelRate = Math.Round(row.TotalFuelUse/tsEngineOnTime.TotalHours, 2);

                    if (row.TimeWithEngineOn.Length > 5) // ����� ����������� �� ������
                    {
                        int length = row.TimeWithEngineOn.Length;
                        int indx = length - 2 - 1;

                        row.TimeWithEngineOn = row.TimeWithEngineOn.Substring(0, indx);
                    }
                }

                row.TypeAlgorythm = (int) algorType;
                feDataSet.FuelExpensesDUT_Day.AddFuelExpensesDUT_DayRow(row);
                System.Windows.Forms.Application.DoEvents();

                totalDebite += row.TotalFuelUse;
                _summaryDutDay.TotalFuelAdd += row.TotalFuelAdd;
                _summaryDutDay.TotalFuelSub += row.TotalFuelSub;
                _summaryDutDay.Distance += row.Distance;
                _summaryDutDay.TimeTotalWork = _summaryDutDay.TimeTotalWork.Add(tsWork);
                _summaryDutDay.TimeTotalMove = _summaryDutDay.TimeTotalMove.Add(tsMove);
                _summaryDutDay.TimeTotalWithEngineOnHour = _summaryDutDay.TimeTotalWithEngineOnHour.Add(tsEngineOnTime);

                if (counter == 0) 
                    _summaryDutDay.FuelBegin = row.FuelBegin;

                _summaryDutDay.FuelEnd = row.FuelEnd;
                ReportProgressChanged(++counter);
            } // foreach

            _summaryDutDay.Distance = Math.Round(m_row.path, 2);
            _summaryDutDay.TotalDebit = Math.Round(totalDebite, 2);
            _summaryDutDay.id = m_row.Mobitel_ID;
            _summaryDutDay.typeAlg = (int)algorType;
        }

        private double FuelDebitBetweenDataGPS(int iBeginGPS, int iEndGPS)
        {
            double dbFuelBegin = 0;
            double dbFuelEnd = 0;
            double dbZapr = 0;
            atlantaDataSet.dataviewRow dr_begin = AtlantaDataSet.dataview.FindByDataGps_ID(iBeginGPS);
            atlantaDataSet.dataviewRow dr_end = AtlantaDataSet.dataview.FindByDataGps_ID(iEndGPS);
            //������� �� ������ � ����� �������
            LocalCache.atlantaDataSet.FuelValueRow[] fuel_val_rows =
                (LocalCache.atlantaDataSet.FuelValueRow[]) dr_begin.GetChildRows("dataview_FuelValue");
            if (fuel_val_rows.Length > 0) dbFuelBegin = fuel_val_rows[0].Value;
            fuel_val_rows = (LocalCache.atlantaDataSet.FuelValueRow[]) dr_end.GetChildRows("dataview_FuelValue");
            if (fuel_val_rows.Length > 0)
            {
                dbFuelEnd = fuel_val_rows[0].Value;
                //�������� � ������ MM.dd.yyyy
                LocalCache.atlantaDataSet.FuelReportRow[] FRRows =
                    (LocalCache.atlantaDataSet.FuelReportRow[])
                        AtlantaDataSet.FuelReport.Select(
                            "mobitel_id=" + m_row.Mobitel_ID.ToString() + " AND time_ >= #" +
                            dr_begin.time.ToString("MM.dd.yyyy HH:mm:ss") + "# and time_  <= #" +
                            dr_end.time.ToString("MM.dd.yyyy HH:mm:ss") + "#", "time_");
                if (FRRows.Length > 0)
                {
                    foreach (atlantaDataSet.FuelReportRow tmp_FR_row in FRRows)
                    {
                        if (tmp_FR_row.dValue > 0) dbZapr += tmp_FR_row.dValue;
                    }
                }
            }
            return dbFuelBegin + dbZapr - dbFuelEnd;
        }

        #region Nested Types

        /// <summary>
        /// ������������ �������� ������ ������ � ������� � ����������
        /// �� ����������� ��������� (������������� ��������).
        /// </summary>
        public struct SummaryDutDay
        {
            /// <summary>
            /// ����� ������ �������, �
            /// </summary>
            public double TotalDebit;

            /// <summary>
            /// ����� ����������, �
            /// </summary>
            public double TotalFuelAdd;

            /// <summary>
            /// ����� �����, �
            /// </summary>
            public double TotalFuelSub;

            /// <summary>
            /// ����� ���������� ����
            /// </summary>
            public double Distance;

            /// <summary>
            /// ����� ����������������� ����� , �
            /// </summary>
            public TimeSpan TimeTotalWork;

            /// <summary>
            /// ����� ����� � �������� , �
            /// </summary>
            public TimeSpan TimeTotalMove;

            /// <summary>
            /// ����� ����� ������ �� ���������� ����������, �
            /// </summary>
            public TimeSpan TimeTotalWithEngineOnHour;

            /// <summary>
            /// ������� � ������ ����������
            /// </summary>
            public double FuelBegin;

            /// <summary>
            /// ������� � ����� ����������
            /// </summary>
            public double FuelEnd;

            public int id; // mobitel

            public int typeAlg; // type of algorythm
        }

        #endregion
    }

    /// <summary>
    /// �����, ���������� ���������, ������� ����� �������� � ����������
    /// ������� �� ��������� ������ ��������� ���������� ������ ��� ������
    /// � �������� �������� �������.
    /// </summary>
    public class FuelExpensesDutDayEventArgs : EventArgs
    {
        #region Fields

        /// <summary>
        /// ID ���������
        /// </summary>
        private int _id;

        private int _typeAlg;

        /// <summary>
        /// �������� ������ ������ � ������� � ���������� �� �����������
        /// ��������� (������������� ��������). 
        /// </summary>
        private TotalFuelExpensesDutDayAlgorithm.SummaryDutDay _summaryDutDay;

        #endregion

        #region .ctor

        /// <summary>
        /// �����������
        /// </summary>
        /// <param name="id">ID ���������</param>
        /// <param name="summary">�������� ������ ������</param>
        public FuelExpensesDutDayEventArgs(int id, int typeAlg, TotalFuelExpensesDutDayAlgorithm.SummaryDutDay SummaryDutDay)
        {
            _id = id;
            _summaryDutDay = SummaryDutDay;
            _typeAlg = typeAlg;
        }

        #endregion

        #region Properties

        /// <summary>
        /// ID ���������
        /// </summary>
        public int Id
        {
            get { return _id; }
        }

        public int TypeAlgorythm
        {
            get { return _typeAlg; }
        }

        /// <summary>
        /// �������� ������ ������ � ������� � ���������� �� �����������
        /// ��������� (������������� ��������).
        /// </summary>
        public TotalFuelExpensesDutDayAlgorithm.SummaryDutDay SummaryDutDay
        {
            get
            {
                return _summaryDutDay;
            }
        }

        #endregion
    }
}
