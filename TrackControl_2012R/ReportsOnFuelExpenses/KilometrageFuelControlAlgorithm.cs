﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BaseReports.Procedure;
using BaseReports.Procedure.Calibration;
using DevExpress.XtraEditors;
using LocalCache;
using ReportsOnFuelExpenses.Properties;
using TrackControl.General.DatabaseDriver;
using TrackControl.Reports;
using TrackControl.Vehicles;

namespace ReportsOnFuelExpenses
{
    public class KilometrageFuelControlAlgorithm : PartialAlgorithms
    {
        public struct Summary
        {
            public double TotalTravel;
            public double TotalFuel;
            public double FuelAvg;
            public double FuelNorma;
            public double Deviation;
            public int id;
            public int typeAlgo;
        }

        private FuelExpensesDataSet feDataSet;
        private Summary _summar;
        private int UseDischargeInFuelrate = 0;
        private bool RotatePresent = true;
        private AlgorithmType algorythmType;
        private double valueFueling = 0;

        public KilometrageFuelControlAlgorithm(object paramAlgorythm)
        {
            algorythmType = (AlgorithmType) paramAlgorythm;
            feDataSet = FuelExpensesDataSetCreator.GetDataSet();
        }

        public double SetValueFueling
        {
            set { valueFueling = value; }
        }

        public int TypeAlgorythm
        {
            get { return (int) algorythmType; }
        }

        public void IsRotateSensorPresent()
        {
            RotatePresent = true;

            atlantaDataSet.sensorsRow sensor = FindSensor(AlgorithmType.ROTATE_E);

            if (sensor == null)
                RotatePresent = false;
        }

        /// <summary>
        /// Работа над отчетом
        /// </summary>
        public override void Run()
        {
            if ((GpsDatas.Length == 0) || m_row == null)
            {
                return;
            }

            if (feDataSet != null)
            {
                if (feDataSet.FuelExpensesDUT_Day.Select(string.Format("Mobitel_ID = {0}", m_row.Mobitel_ID)).Length > 0)
                    return;
            }

            _summar = new Summary();

            UseDischargeInFuelrate = 0;

            if (set_row != null)
                UseDischargeInFuelrate = set_row.FuelrateWithDischarge > 0 ? 1 : 0;

            try
            {
                IsRotateSensorPresent();
                FillFuelDayExpenses();
            }
            finally
            {
                AfterReportFilling();
                Algorithm.OnAction(this, new KilometrageDutDayEventArgs(m_row.Mobitel_ID, (int)algorythmType, _summar));
            }
        } // Run

        // Топливо Суточный ДУТ
        private void FillFuelDayExpenses()
        {
            double FuelBeginTotal = 0;
            double FuelEndTotal = 0;

            _summar.Deviation = 0;
            _summar.FuelAvg = 0;
            _summar.FuelNorma = 0;
            _summar.TotalFuel = 0;
            _summar.TotalTravel = 0;
            _summar.id = m_row.Mobitel_ID;
            _summar.typeAlgo = (int)algorythmType;

            LocalCache.atlantaDataSet.KilometrageReportRow[] klmtDataRows =
                (atlantaDataSet.KilometrageReportRow[])
                    AtlantaDataSet.KilometrageReport.Select("MobitelId=" + m_row.Mobitel_ID, "InitialTime");

            if (klmtDataRows.Length <= 0)
                return;

            BeforeReportFilling(Resources.TotalFuelExDutDayName, klmtDataRows.Length);

            VehicleInfo vehicle = new VehicleInfo(m_row);
            atlantaDataSet.vehicleRow[] vehicles = m_row.GetvehicleRows();

            double fuelNormaWay100 = 0.0;
            if (vehicles != null || vehicles.Length > 0) 
                fuelNormaWay100 = vehicles[0].FuelWayLiter;

            int begin = feDataSet.KilometrageControlFuelDUT.Count;
            
            // вначале собираем данные от отчета Пробег
            foreach (var krow in klmtDataRows)
            {
                FuelExpensesDataSet.KilometrageControlFuelDUTRow drow =
                    feDataSet.KilometrageControlFuelDUT.NewKilometrageControlFuelDUTRow();

                drow.Id = krow.Id;
                drow.MobitelId = krow.MobitelId;
                drow.State = krow.State;
                drow.Location = krow.Location;
                drow.InitialTime = krow.InitialTime;
                drow.FinalTime = krow.FinalTime;
                drow.Interval = krow.Interval;
                drow.Distance = krow.Distance;

                _summar.TotalTravel += krow.Distance;

                drow.Latitude = krow.Latitude;
                drow.Longitude = krow.Longitude;
                drow.InitialPointId = krow.InitialPointId;
                drow.FinalPointId = krow.FinalPointId;
                drow.DateTime = krow.DateTime;
                drow.NormalFuel100 = fuelNormaWay100;
                drow.FuelAvgNormal = Math.Round(krow.Distance / 100.0 * fuelNormaWay100, 2);

                feDataSet.KilometrageControlFuelDUT.AddKilometrageControlFuelDUTRow(drow);
            } // foreach

            //Получение данных об уровне топлива
            FuelDictionarys fuelDict = new FuelDictionarys();
            Fuel fuelAlg = new Fuel();
            fuelAlg.SettingAlgoritm(algorythmType);
            fuelAlg.SelectItem(m_row); 
            fuelAlg.GettingValuesDUT(fuelDict);
            if (fuelDict.fuelSensors.Count == 0) 
                return;
            int counter = 0;
            double totalDebite = 0.0;
            double deltaFuel = 1.0; // допустимая максимальная разница в л
            bool isBateLimit = false;
            double prevFuelEnd = 0.0;
            double valueBegin = 0.0;

            for (int i = begin; i < feDataSet.KilometrageControlFuelDUT.Count; i++)
            {
                var tmprow = feDataSet.KilometrageControlFuelDUT[i];

                if (tmprow.State == Resources.Movement || tmprow.State == Resources.Parking) // работаем только с движениями
                {
                    //топливо в начале движения и в конце движения с удалением разрывов
                    tmprow.FuelBegin = 0;
                    tmprow.FuelEnd = 0;
                    DateTime dtSeekBegin = tmprow.InitialTime;
                    DateTime dtSeekEnd = tmprow.FinalTime;
                    dtSeekBegin = new DateTime(dtSeekBegin.Year, dtSeekBegin.Month, dtSeekBegin.Day, dtSeekBegin.Hour, dtSeekBegin.Minute, dtSeekBegin.Second);
                    dtSeekEnd = new DateTime(dtSeekEnd.Year, dtSeekEnd.Month, dtSeekEnd.Day, dtSeekEnd.Hour, dtSeekEnd.Minute, dtSeekEnd.Second);
                    GpsData[] dArray = DataSetManager.GetDataGpsForPeriod(m_row, dtSeekBegin, dtSeekEnd);

                    if (dArray.Length > 0 && fuelDict.fuelSensors.Count > 0)
                    {
                        if (isBateLimit)
                        {
                            if (Math.Abs(fuelDict.ValueFuelSum[dArray[0].Id].value - prevFuelEnd) >= deltaFuel)
                            {
                                valueBegin = prevFuelEnd;
                            }
                            else
                            {
                                valueBegin = fuelDict.ValueFuelSum[dArray[0].Id].value;
                            }
                        }
                        else
                        {
                            isBateLimit = true;
                            valueBegin = fuelDict.ValueFuelSum[dArray[0].Id].value;
                        }

                        if (fuelDict.ValueFuelSum.Count > 0)
                            tmprow.FuelBegin = valueBegin;

                        if (FuelBeginTotal == 0 && FuelEndTotal == 0)
                            FuelBeginTotal = valueBegin;

                        if (fuelDict.ValueFuelSum.Count > 0)
                        {
                            tmprow.FuelEnd = fuelDict.ValueFuelSum[dArray[dArray.Length - 1].Id].value;
                            FuelEndTotal = fuelDict.ValueFuelSum[dArray[dArray.Length - 1].Id].value;
                            prevFuelEnd = fuelDict.ValueFuelSum[dArray[dArray.Length - 1].Id].value;
                        }

                        tmprow.InitialPointId = (int) dArray[0].Id;
                        tmprow.FinalPointId = (int) dArray[dArray.Length - 1].Id;
                    }

                    LocalCache.atlantaDataSet.FuelReportRow[] FRRows =
                        (LocalCache.atlantaDataSet.FuelReportRow[])
                            AtlantaDataSet.FuelReport.Select("mobitel_id=" + m_row.Mobitel_ID + " AND time_ >= #" +
                                dtSeekBegin.ToString("MM.dd.yyyy HH:mm:ss") + "# and time_  < #" +
                                dtSeekEnd.ToString("MM.dd.yyyy HH:mm:ss") + "#", "time_");

                    double dbZapr = 0;
                    double dbSliv = 0;

                    if (FRRows.Length > 0)
                    {
                        foreach (atlantaDataSet.FuelReportRow tmp_FR_row in FRRows)
                        {
                            if (tmp_FR_row.dValue > 0)
                                dbZapr += tmp_FR_row.dValue;
                            else
                                dbSliv += tmp_FR_row.dValue;
                        }
                    }
                    else
                    {
                        int y = 500;
                    }

                    // Подсчет Общего расхода = Топливо в начале периода - Топливо в конце периода + 
                    // Всего заправлено.
                    double TotalFuelAdd = dbZapr;
                    double TotalFuelSub = -dbSliv;
                    if (tmprow["FuelBegin"] != DBNull.Value)
                        tmprow.TotalFuelUse = tmprow.FuelBegin + TotalFuelAdd - tmprow.FuelEnd -
                                           UseDischargeInFuelrate * TotalFuelSub;

                    if (tmprow.TotalFuelUse < 0)
                        tmprow.TotalFuelUse = 0;

                    tmprow.FuelUseAvg = 0.00; // начальная инициализация, для таблицы

                    if (tmprow.Distance > 0)
                    {
                        tmprow.FuelUseAvg = tmprow.TotalFuelUse * 100.0 / tmprow.Distance;
                        _summar.TotalFuel += tmprow.TotalFuelUse;
                    }

                    System.Windows.Forms.Application.DoEvents();

                    totalDebite += tmprow.TotalFuelUse;

                    ReportProgressChanged(++counter);

                    if (tmprow.FuelUseAvg > 0)
                        tmprow.DeviationFuel = tmprow.FuelUseAvg - tmprow.FuelAvgNormal;

                    tmprow.TypeAlgorythm = (int)algorythmType;
                } // if
            } // foreach

            var tmprowed = feDataSet.KilometrageControlFuelDUT[0];
            double fuelBegin = 0.0;
            if (tmprowed["FuelBegin"] != DBNull.Value)
                fuelBegin = Math.Round(tmprowed.FuelBegin, 2);

            tmprowed = feDataSet.KilometrageControlFuelDUT[feDataSet.KilometrageControlFuelDUT.Count - 1];
            double fuelEnd = 0.0;
            if (tmprowed["FuelBegin"] != DBNull.Value)
                fuelEnd = Math.Round(tmprowed.FuelBegin, 2);

            _summar.TotalFuel = (fuelBegin - fuelEnd) + Math.Round(valueFueling, 2);

            _summar.FuelNorma = fuelNormaWay100;
            _summar.FuelAvg = _summar.TotalFuel / _summar.TotalTravel * 100.0;
            if (_summar.TotalFuel == 0 || _summar.TotalTravel == 0)
                _summar.FuelAvg = 0.0;
            _summar.FuelNorma = _summar.TotalTravel / 100.0 * _summar.FuelNorma;
            _summar.Deviation = _summar.FuelAvg - _summar.FuelNorma;
        }

        private double FuelDebitBetweenDataGPS(int iBeginGPS, int iEndGPS)
        {
            double dbFuelBegin = 0;
            double dbFuelEnd = 0;
            double dbZapr = 0;
            atlantaDataSet.dataviewRow dr_begin = AtlantaDataSet.dataview.FindByDataGps_ID(iBeginGPS);
            atlantaDataSet.dataviewRow dr_end = AtlantaDataSet.dataview.FindByDataGps_ID(iEndGPS);
            //Уровень на начало и конец периода
            LocalCache.atlantaDataSet.FuelValueRow[] fuel_val_rows =
                (LocalCache.atlantaDataSet.FuelValueRow[]) dr_begin.GetChildRows("dataview_FuelValue");
            if (fuel_val_rows.Length > 0) dbFuelBegin = fuel_val_rows[0].Value;
            fuel_val_rows = (LocalCache.atlantaDataSet.FuelValueRow[]) dr_end.GetChildRows("dataview_FuelValue");
            if (fuel_val_rows.Length > 0)
            {
                dbFuelEnd = fuel_val_rows[0].Value;
                //Заправки в период MM.dd.yyyy
                LocalCache.atlantaDataSet.FuelReportRow[] FRRows =
                    (LocalCache.atlantaDataSet.FuelReportRow[])
                        AtlantaDataSet.FuelReport.Select(
                            "mobitel_id=" + m_row.Mobitel_ID.ToString() + " AND time_ >= #" +
                            dr_begin.time.ToString("MM.dd.yyyy HH:mm:ss") + "# and time_  <= #" +
                            dr_end.time.ToString("MM.dd.yyyy HH:mm:ss") + "#", "time_");
                if (FRRows.Length > 0)
                {
                    foreach (atlantaDataSet.FuelReportRow tmp_FR_row in FRRows)
                    {
                        if (tmp_FR_row.dValue > 0) dbZapr += tmp_FR_row.dValue;
                    }
                }
            }

            return dbFuelBegin + dbZapr - dbFuelEnd;
        }

        #region Nested Types

        /// <summary>
        /// Представляет итоговые данные отчета о пробеге и остановках
        /// по конкретному телетреку (транспортному средству).
        /// </summary>
        public struct SummaryDutDay
        {
            /// <summary>
            /// Общий расход топлива, л
            /// </summary>
            public double TotalDebit;

            /// <summary>
            /// Всего заправлено, л
            /// </summary>
            public double TotalFuelAdd;

            /// <summary>
            /// Всего слито, л
            /// </summary>
            public double TotalFuelSub;

            /// <summary>
            /// Общий пройденный путь
            /// </summary>
            public double Distance;

            /// <summary>
            /// Общая продолжительность смены , ч
            /// </summary>
            public TimeSpan TimeTotalWork;

            /// <summary>
            /// Общее время в движении , ч
            /// </summary>
            public TimeSpan TimeTotalMove;

            /// <summary>
            /// Общее время работы со включенным двигателем, ч
            /// </summary>
            public TimeSpan TimeTotalWithEngineOnHour;

            /// <summary>
            /// топливо в начале промежутка
            /// </summary>
            public double FuelBegin;

            /// <summary>
            /// топливо в конце промежутка
            /// </summary>
            public double FuelEnd;
        }

        #endregion
    }

    /// <summary>
    /// Класс, содержащий параметры, которые нужно передать в обработчик
    /// события по окончанию работы алгоритма подготовки данных для отчета
    /// о суточных расходах топлива.
    /// </summary>
    public class KilometrageDutDayEventArgs : EventArgs
    {
        #region Fields

        /// <summary>
        /// ID телетрека
        /// </summary>
        private int _id;

        /// <summary>
        /// Итоговые данные отчета о пробеге и остановках по конкретному
        /// телетреку (транспортному средству). 
        /// </summary>
        private KilometrageFuelControlAlgorithm.Summary _summary;

        private int _typeAlgo;

        #endregion

        #region .ctor

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="id">ID телетрека</param>
        /// <param name="summary">Итоговые данные отчета</param>
        public KilometrageDutDayEventArgs(int id, int typeAlgo, KilometrageFuelControlAlgorithm.Summary SummaryDutDay)
        {
            _id = id;
            _typeAlgo = typeAlgo;
            _summary = SummaryDutDay;
        }

        #endregion

        #region Properties

        /// <summary>
        /// ID телетрека
        /// </summary>
        public int Id
        {
            get { return _id; }
        }

        public int TypeAlg
        {
            get { return _typeAlgo; }
        }

        /// <summary>
        /// Итоговые данные отчета о пробеге и остановках по конкретному
        /// телетреку (транспортному средству).
        /// </summary>
        public KilometrageFuelControlAlgorithm.Summary SummaryDutDay
        {
            get
            {
                return _summary;
            }
        }

        #endregion
    }
}
