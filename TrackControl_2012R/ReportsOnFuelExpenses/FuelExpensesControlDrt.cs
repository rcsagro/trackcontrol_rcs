﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using BaseReports.ReportsDE;
using BaseReports.Procedure;
using BaseReports.Properties;
using BaseReports.RFID;
using LocalCache;
using TrackControl.General;
using TrackControl.Reports;
using TrackControl.Vehicles;
using TrackControl.Reports.Graph;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrinting.Drawing;
using DevExpress.XtraExport;
using DevExpress.XtraReports;
using DevExpress.XtraReports.UI;
using DevExpress.XtraPrinting.Native;
using DevExpress.XtraPrinting.Control;
using DevExpress.XtraPrinting.Preview;
using DevExpress.XtraPrintingLinks;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views;
using DevExpress.XtraGrid.Views.Grid;
using ReportsOnFuelExpenses;
using BaseReports;
using ReportsOnFuelExpenses.Properties;
using Report;

namespace ReportsOnFuelExpenses
{
    public partial class FuelExpensesControlDrt : BaseReports.ReportsDE.BaseControl
    {
        public FuelExpensesDataSet feDataSet;
        private float totalTravel; // Общий пробег
        private double totalFuelRate; // Общий расход топлива
        private double motionFuelRate; // Расход топлива в движении
        private double avgMotionFuelRate; // Средний расход топлива в движении
        private double parkingFuelRate; // Расход топлива на стоянках
        private double avgParkingFuelRate; // Средний расход топлива на стоянках
        private static IBuildGraphs buildGraph; // Интерфейс для построение графиков по SeriesL
        private static atlantaDataSet dataset;
        //private VehicleInfo vehicleInfo;
        ReportClass<TInfoDut> ReportingFuelCtrlDrt;
        private List<atlantaDataSet.mobitelsRow> list_mobitel_id;
        
        public FuelExpensesControlDrt()
        {
            InitializeComponent();
            Localization();
            HideStatusBar();
            DisableButton();
            DisableRun();
            DisableMenuFirstButton();
            VisionPanel(gvFuelCntrlDrt, gcFuelCntrlDrt, null);

            feDataSet = FuelExpensesDataSetCreator.GetDataSet();
            atlantaDataSetBindingSource.DataSource = feDataSet;
            atlantaDataSetBindingSource.DataMember = "FuelExpensesDRT_Total";
            gcFuelCntrlDrt.DataSource = atlantaDataSetBindingSource;
            fuelExpensesDataSet = null;
            buildGraph = new BuildGraphs();
            dataset = ReportTabControl.Dataset;
            list_mobitel_id = new List<atlantaDataSet.mobitelsRow>();

            AddAlgorithm(new Kilometrage());
            // AddAlgorithm(new Rotate(AlgorithmType.ROTATE_E));
            AddAlgorithm(new Rotation());
            AddAlgorithm(FlowMeter.GetFlowMeter()); // new FlowMeter()); 
            // FlowMeter(AlgorithmType.FUEL2));
            AddAlgorithm(new TotalFuelExpensesDrtAlgorithm());

            gvFuelCntrlDrt.RowClick +=new RowClickEventHandler(gvFuelCntrlDrt_RowClick);

            compositeReportLink.CreateMarginalHeaderArea +=
                new CreateAreaEventHandler(composLink_CreateMarginalHeaderArea);

            ReportingFuelCtrlDrt =
                new ReportClass<TInfoDut>(compositeReportLink);

            ReportingFuelCtrlDrt.getStrBrckLeft =
                new ReportClass<TInfoDut>.GetStrBreackLeft(GetStringBreackLeft);

            ReportingFuelCtrlDrt.getStrBrckRight =
                new ReportClass<TInfoDut>.GetStrBreackRight(GetStringBreackRight);

            ReportingFuelCtrlDrt.getStrBrckUp =
                new ReportClass<TInfoDut>.GetStrBreackUp(GetStringBreackUp);
        } // DevFuelExpensesCntrlDrt

        public override string Caption
        {
            get
            {
                return Resources.TotalFuelExDrtName;
            }

        } // Caption

        public override void Select(atlantaDataSet.mobitelsRow mobitel)
        {
            // base.Select(mobitel);
            if (mobitel != null)
            {
                curMrow = mobitel;
                atlantaDataSetBindingSource.Filter = "Mobitel_ID >= 0";

                if (mobitel.Check)
                {
                    if (atlantaDataSetBindingSource.Count > 0)
                    {
                        gcFuelCntrlDrt.DataSource = atlantaDataSetBindingSource;
                        EnableButton();
                        CalcTotals();
                    } // if
                } // if
                else
                {
                    DisableButton();
                    gcFuelCntrlDrt.DataSource = null;
                } // else
            } // if
        } // Select

        private void CalcTotals()
        {
            totalTravel = 0;
            totalFuelRate = 0;
            motionFuelRate = 0;
            avgMotionFuelRate = 0;
            parkingFuelRate = 0;
            avgParkingFuelRate = 0;

            double totalParkingTimeWithEngineOn = 0;

            foreach (DataRowView dR in atlantaDataSetBindingSource)
            {
                FuelExpensesDataSet.FuelExpensesDRT_TotalRow row =
                    (FuelExpensesDataSet.FuelExpensesDRT_TotalRow)dR.Row;

                totalTravel += row.Travel;
                totalFuelRate += row.TotalFuelUse;
                motionFuelRate += row.FuelUseInMotion;
                parkingFuelRate += row.FuelUseOnStop;
                totalParkingTimeWithEngineOn += row.TimeStopWithEngineOn;
            } // foreach

            avgMotionFuelRate = totalTravel == 0 ? 0 : motionFuelRate / totalTravel * 100;
            avgParkingFuelRate = totalParkingTimeWithEngineOn == 0 ? 0 : parkingFuelRate 
                / totalParkingTimeWithEngineOn;
        } // CalcTotals

        protected override void Algorithm_Action(object sender, EventArgs e)
        {
            if ((sender is TotalFuelExpensesDrtAlgorithm)) // || (sender is TotalFuelExpensesDutAlgorithm))
            {
                // EnableButton();
            } // if (sender)
        } // Algorithm_Action

        // Очищает данные отчета
        public override void ClearReport()
        {
            feDataSet.FuelExpensesDRT_Total.Clear();
            list_mobitel_id.Clear();
        }

        // Нажатие кнопки формирования отчета
        protected override void bbiStart_ItemClick(object sender, ItemClickEventArgs e)
        {
            bool noData = true; // За выбранный период нет данных ни по одной машине

            if (bbiStart.Caption == Resources.Start)
            {
                SetStopButton();
                DisableButton();
                ClearReport();
                BeginReport();
                _stopRun = false;
                atlantaDataSetBindingSource.RaiseListChangedEvents = false;
                atlantaDataSetBindingSource.SuspendBinding();
                
                foreach (atlantaDataSet.mobitelsRow m_row in dataset.mobitels)
                {
                    if (m_row.Check && DataSetManager.IsGpsDataExist(m_row))
                    {
                        SelectItem(m_row);

                        atlantaDataSetBindingSource.Filter = String.Format("Mobitel_id={0}",
                            m_row.Mobitel_ID);

                        if (atlantaDataSetBindingSource.Count > 0)
                            list_mobitel_id.Add(m_row);

                        noData = false;
                        if (_stopRun)
                            break;
                    } // if
                } // foreach

                if (noData)
                {
                    XtraMessageBox.Show(Resources.WarningNoData, Resources.Notification,
                                MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

                atlantaDataSetBindingSource.RaiseListChangedEvents = true;
                atlantaDataSetBindingSource.ResumeBinding();
                atlantaDataSetBindingSource.Filter = ""; // "Mobitel_ID >= 0";
                Select(curMrow);
                SetStartButton();
            } // if
            else
            {
                SetStartButton();
                StopReport();
                _stopRun = true;

                if (!noData)
                    EnableButton();
            } // else
        } // bbiStart_ItemClick

        // show track on map
        protected override void bbiShowOnMap_ItemClick(object sender, ItemClickEventArgs e)
        {
            ReportsControl.OnMapShowNeeded();
            int indexRow = gvFuelCntrlDrt.GetFocusedDataSourceRowIndex();
            ShowTrackOnMap();  
        }

        // показать график
        protected override void bbiShowOnGraf_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (list_mobitel_id.Count > 0)
            {
                ReportsControl.OnGraphShowNeeded();
                int selected_row = gvFuelCntrlDrt.GetFocusedDataSourceRowIndex();
                SelectGraphic(list_mobitel_id[selected_row]);
            } // if
        }

        protected void gvFuelCntrlDrt_RowClick(object sender, RowClickEventArgs e)
        {
            if (e.RowHandle >= 0)
            {
                if (list_mobitel_id.Count > 0)
                {
                    int selectedRow = gvFuelCntrlDrt.GetFocusedDataSourceRowIndex();
                    if (selectedRow >= 0)
                    {
                        ShowTrackOnMap();
                        SelectGraphic(list_mobitel_id[selectedRow]);
                    }

                } // if
            } // if
        } // gvFuelCntrlDrt_RowClick

        public void ShowTrackOnMap()
        {
            try
            {
                ReportsControl.OnClearMapObjectsNeeded();
                List<Track> segments = new List<Track>();
                atlantaDataSet.KilometrageReportRow nRow;
                int selectedRow = gvFuelCntrlDrt.GetFocusedDataSourceRowIndex();
                atlantaDataSet.mobitelsRow mobRow = list_mobitel_id[selectedRow];

                DataRow[] datRow = dataset.KilometrageReport.Select("MobitelId=" +
                                                                    mobRow.Mobitel_ID, "Id ASC");

                for (int i = 0; i < datRow.Length; i++)
                {
                    nRow = (atlantaDataSet.KilometrageReportRow) datRow[i];
                    segments.Add(GetTrackSegment(nRow));
                }

                if (segments.Count > 0)
                {
                    ReportsControl.OnTrackSegmentsShowNeededWith(segments);
                    //ReportsControl.OnTrackSegmentsShowNeeded(segments);
                    var seg = segments[segments.Count - 1];
                    if (seg.GeoPoints != null)
                    {
                        showMark(segments[segments.Count - 1].GeoPoints[0].LatLng, mobRow.Mobitel_ID, mobRow.Name);
                    }
                }
            }
            catch (Exception e)
            {
                XtraMessageBox.Show(e.Message + "\n" + e.StackTrace, "ShowTrackOnMap", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        } // ShowTrackOnMap

        static void showMark(PointLatLng latlng, int mobitelId, string message)
        {
            PointLatLng location = new PointLatLng(latlng.Lat, latlng.Lng);
            List<Marker> markers = new List<Marker>();
            markers.Add(new Marker(MarkerType.Report, mobitelId, location, message, ""));
            ReportsControl.OnMarkersShowNeededWith(markers);
        }

        public Track GetTrackSegment(atlantaDataSet.KilometrageReportRow row)
        {
            List<IGeoPoint> data = new List<IGeoPoint>();
            GpsData [] gpsDatasOrdByTime  = DataSetManager.GetDataGpsArray(row.mobitelsRow);

            bool inside = false;

            foreach (GpsData gpsData in gpsDatasOrdByTime)
            {
                if (gpsData.Id == row.InitialPointId)
                {
                    inside = true;
                }

                if (inside)
                {
                    data.Add(gpsData);
                }

                if (gpsData.Id == row.FinalPointId)
                {
                    break;
                }
            } // foreach

            return new Track(row.MobitelId, Color.DarkBlue, 2f, data);
        } // GetTrackSegment

        // Построение графика
        public void SelectGraphic(atlantaDataSet.mobitelsRow m_row)
        {
            if (m_row != null)
            {
                try
                {
                    if (m_row.GetdataviewRows() == null || m_row.GetdataviewRows().Length == 0)
                              return;

                    ReportsControl.GraphClearSeries();
                    Graph.ClearRegion();

                    buildGraph.AddGraphFlowmeter(graph, dataset, m_row);

                    //Graph.ShowSeries(vehicleInfo.Info);
                    ReportsControl.ShowGraph(m_row);
                } // try
                catch (Exception ex)
                {
                    string message = String.Format(Resources.FuelExControlDrtException + ": {0}", ex.Message);
                    throw new Exception(message);
                }
            } // if
        } // SelectGraphic

        // Выборка данных
        public void SelectItem(atlantaDataSet.mobitelsRow m_row)
        {
            Application.DoEvents();

            foreach (IAlgorithm alg in _algoritms)
            {
                alg.SelectItem(m_row);
                alg.Run();

                if (_stopRun)
                    return;
            }

            Application.DoEvents();
            // ReportsControl.ShowGraph(m_row);
        } // SelectItem

        // Формирует данные для отчета по всем активным машинам
        protected override void ExportAllDevToReport()
        {
            XtraGridService.SetupGidViewForPrint(gvFuelCntrlDrt, true, true);
            TInfoDut info = new TInfoDut();

            info.periodBeging = Algorithm.Period.Begin;
            info.periodEnd = Algorithm.Period.End;
            info.infoVehicle = " ";
            info.infoDriverName = " ";
            info.totalWay = Math.Round(totalTravel, 2);
            info.totalFuel = Math.Round(totalFuelRate, 2); 
            info.totalFuelAdd = Math.Round(motionFuelRate, 2);
            info.MiddleFuel = Math.Round(avgMotionFuelRate, 2);
            info.MotoHour = Math.Round(parkingFuelRate, 2);
            info.totalFuelSub = Math.Round(avgParkingFuelRate, 2);

            ReportingFuelCtrlDrt.AddInfoStructToList(info);
            ReportingFuelCtrlDrt.CreateAndShowReport(gcFuelCntrlDrt);
        } // ExportToExcelDevExpress

        // формирование колонтитулов верхних отчета
        private void composLink_CreateMarginalHeaderArea(object sender, CreateAreaEventArgs e)
        {
            DevExpressReportHeader(Resources.FuelReportConsumptionDRT, e);
            TInfoDut info = ReportingFuelCtrlDrt.GetInfoStructure;
            string strPeriod = Resources.PeriodFrom + " " + info.periodBeging + " " +
                Resources.PeriodTo + " " + info.periodEnd;
            DevExpressReportSubHeader(strPeriod, 22, e);
        }

        /* функция для формирования левой части заголовка отчета */
        protected string GetStringBreackLeft()
        {
            TInfoDut info = ReportingFuelCtrlDrt.GetInfoStructure;
            ReportingFuelCtrlDrt.SetRectangleBrckLetf(0, 0, 300, 85);
            return (Resources.TotalWay + ": " + String.Format("{0:f2}", info.totalWay) + "\n" +
                Resources.TotalFuel + ": " + String.Format("{0:f2}", info.totalFuel));
        }

        /* функция для формирования верхней/средней части заголовка отчета*/
        protected string GetStringBreackUp()
        {
            TInfoDut info = ReportingFuelCtrlDrt.GetInfoStructure;
            ReportingFuelCtrlDrt.SetRectangleBrckUP(370, 0, 340, 85);
            return (Resources.FuelExRate + ": " + String.Format("{0:f2}", info.totalFuelAdd) + "\n" +
                Resources.FuelExMiddle + ": " + String.Format("{0:f2}", info.MiddleFuel));
        }

        /* функция для формирования правой части заголовка отчета */
        protected string GetStringBreackRight()
        {
            TInfoDut info = ReportingFuelCtrlDrt.GetInfoStructure;
            ReportingFuelCtrlDrt.SetRectangleBrckRight(680, 0, 300, 85);
            return (Resources.FuelExParking + ": " + String.Format("{0:f2}", info.MotoHour) + "\n" +
                Resources.FuelExMiddleParking + ": " + String.Format("{0:f2}", info.totalFuelSub));
        } // GetStringBreackRight

        protected override void barButtonGroupPanel_ItemClick(object sender, ItemClickEventArgs e)
        {
            GroupPanel(gvFuelCntrlDrt);
        }

        protected override void barButtonFooterPanel_ItemClick(object sender, ItemClickEventArgs e)
        {
            FooterPanel(gvFuelCntrlDrt);
        }

        protected override void barButtonNavigator_ItemClick(object sender, ItemClickEventArgs e)
        {
            NavigatorPanel(gcFuelCntrlDrt);
        }

        private void Localization()
        {
            colNameCar.Caption = Resources.FuelExControlNameCar;
            colNameCar.ToolTip = Resources.FuelExControlNameCar;

            colFIO.Caption = Resources.FuelExControlDriverFIO;
            colFIO.ToolTip = Resources.FuelExControlDriverFIO;

            colTravel.Caption = Resources.FuelExControlTravel;
            colTravel.ToolTip = Resources.FuelExControlTravel;

            colTimeMoving.Caption = Resources.FuelExControlTimeMove;
            colTimeMoving.ToolTip = Resources.FuelExControlTimeMove;

            colFuelUseInMotion.Caption = Resources.FuelExControlDrtFuelrateInMove;
            colFuelUseInMotion.ToolTip = Resources.FuelExControlDrtFuelrateInMove;

            colFuelMiddleInMotion.Caption = Resources.FuelExControlMiddleFuelrateInMove;
            colFuelMiddleInMotion.ToolTip = Resources.FuelExControlMiddleFuelrateInMove;

            colTimeStopWithEngineOn.Caption = Resources.FuelExControlTimeStopWithEngineOn;
            colTimeStopWithEngineOn.ToolTip = Resources.FuelExControlTimeStopWithEngineOn;

            colFuelUseOnStop.Caption = Resources.FuelExControlDrtFuelrateOnStops;
            colFuelUseOnStop.ToolTip = Resources.FuelExControlDrtFuelrateOnStops;

            colFuelMiddleOnStop.Caption = Resources.FuelExControlDrtMiddleFuelrateOnStops;
            colFuelMiddleOnStop.ToolTip = Resources.FuelExControlDrtMiddleFuelrateOnStops;

            colTotalFuelUse.Caption = Resources.FuelExControlCommonFuelrate;
            colTotalFuelUse.ToolTip = Resources.FuelExControlCommonFuelrate;

            colTotalMiddleFuelUseOneTime.Caption = Resources.FuelExControlMiddleFuelrateOnTime;
            colTotalMiddleFuelUseOneTime.ToolTip = Resources.FuelExControlMiddleFuelrateOnTime;
        }
    } // DevFuelExpensesCntrlDrt
} // ReportsOnFuelExpenses
