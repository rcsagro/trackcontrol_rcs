﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Drawing;
using System.Windows.Forms;
using BaseReports;
using BaseReports.Procedure;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraPrinting;
using LocalCache;
using Report;
using ReportsOnFuelExpenses.Properties;
using TrackControl.General;
using TrackControl.Vehicles;
using TrackControl.Reports;

namespace ReportsOnFuelExpenses
{
    public partial class FuelExpensesControlDut : BaseReports.ReportsDE.BaseControl
    {
        public class MobitelData
        {
            public int idmobi;
            public int typealg;
        }

        public FuelExpensesDataSet feDataSet; // Датасет
        private float totalTravel; // общий пробег
        private double totalFuelRate; // общий расход топлива
        private double avgFuelRateInMachineHour; // Средний расход на моточас
        private double avgMotionFuelRate; // Средний расход топлива в движении
        private double fuelAdd; // Всего заправлено
        private double fuelSub; // всего слито
        protected VehicleInfo vehicleInfo; // Описание текущей машины
        private static IBuildGraphs buildGraph; // Интерфейс для построение графиков
        protected static atlantaDataSet dataset;
        List<atlantaDataSet.mobitelsRow> listCurMrow;
        List<MobitelData> listCurMrowTs;
        ReportClass<TInfo> ReportingFuelDUTTotal;
        private string NameReport = Resources.FuelExpensesCtrlCaption;
        private int globalCode = (int)AlgorithmType.FUEL1;
        ComboBoxEdit comboAlgorithm = null;
        Fuel fuelAlgorythm = null;
        
        // Главный конструктор
        public FuelExpensesControlDut()
        {
            InitializeComponent();
            Localization();
            HideStatusBar();
            DisableButton();
            DisableRun();
            DisableMenuFirstButton();
            SetVisibilityAlgoChoiser(true);
            VisionPanel(gvFuelControlDut, gcFuelControlDut, null);
            setSafeMode();
            EnableRun();
            feDataSet = FuelExpensesDataSetCreator.GetDataSet();
            atlantaDataSetBindingSource.DataSource = feDataSet;
            atlantaDataSetBindingSource.DataMember = "FuelExpensesDUT_Total";
            gcFuelControlDut.DataSource = atlantaDataSetBindingSource;
            buildGraph = new BuildGraphs();
            dataset = ReportTabControl.Dataset;
            fuelExpensesDataSet = null;
            AddAlgorithm(new Kilometrage());
            //AddAlgorithm(new Rotate(AlgorithmType.ROTATE_E));
            AddAlgorithm(new Rotation());

            fuelAlgorythm = new Fuel(AlgorithmType.FUEL1);
            AddAlgorithm(fuelAlgorythm);
            AddAlgorithm(new Fuel(AlgorithmType.FUEL1_AGREGAT));
            AddAlgorithm(new Fuel(AlgorithmType.FUEL2_AGREGAT));
            AddAlgorithm(new Fuel(AlgorithmType.FUEL3_AGREGAT));
            AddAlgorithm(new Fuel(AlgorithmType.FUEL_AGREGAT_ALL));

            AddAlgorithm(new TotalFuelExpensesDutAlgorithm(AlgorithmType.FUEL1));
            AddAlgorithm(new TotalFuelExpensesDutAlgorithm(AlgorithmType.FUEL1_AGREGAT));
            AddAlgorithm(new TotalFuelExpensesDutAlgorithm(AlgorithmType.FUEL2_AGREGAT));
            AddAlgorithm(new TotalFuelExpensesDutAlgorithm(AlgorithmType.FUEL3_AGREGAT));
            AddAlgorithm(new TotalFuelExpensesDutAlgorithm(AlgorithmType.FUEL_AGREGAT_ALL));
            
            listCurMrowTs = new List<MobitelData>();

            //gvFuelControlDut.RowClick += new RowClickEventHandler(gvFuelControlDut_ClickRow);
            gvFuelControlDut.FocusedRowChanged += gvFuelControlDut_FocusedRowChanged;

            compositeReportLink.CreateMarginalHeaderArea +=
                new CreateAreaEventHandler(composLink_CreateMarginalHeaderArea);

            ReportingFuelDUTTotal =
                new ReportClass<TInfo>(compositeReportLink);

            ReportingFuelDUTTotal.getStrBrckLeft =
                new ReportClass<TInfo>.GetStrBreackLeft(GetStringBreackLeft);

            ReportingFuelDUTTotal.getStrBrckRight =
                new ReportClass<TInfo>.GetStrBreackRight(GetStringBreackRight);

            ReportingFuelDUTTotal.getStrBrckUp =
                new ReportClass<TInfo>.GetStrBreackUp(GetStringBreackUp);
        } // DevFuelExpensesCtrlDut

        public override string Caption // Заголовок отчета
        {
            get { return Resources.TotalFuelExDutName; }
        }

        public override void Select(atlantaDataSet.mobitelsRow m_row)
        {
            if (m_row != null)
            {
                curMrow = m_row;
                int index = 0;
                if (comboAlgorithm != null)
                {
                    index = comboAlgorithm.SelectedIndex;
                }

                if (m_row.Check)
                {
                    //if (CurrentActivateReport == NameReport)
                      //  SelectGraphic(curMrow, (int) AlgorithmType.FUEL1);

                    if (atlantaDataSetBindingSource.Count == 0)
                    {
                        gcFuelControlDut.DataSource = atlantaDataSetBindingSource;

                        if (index == 0)
                        {
                            atlantaDataSetBindingSource.Filter = "TypeAlgorythm=" + (int) AlgorithmType.FUEL1;
                            int rowHandle = gvFuelControlDut.LocateByValue("Mobitel_ID", m_row.Mobitel_ID);
                            if (rowHandle != DevExpress.XtraGrid.GridControl.InvalidRowHandle)
                                gvFuelControlDut.FocusedRowHandle = rowHandle;
                        }
                        else if (index == 1)
                        {
                            atlantaDataSetBindingSource.Filter = "TypeAlgorythm=" + (int) AlgorithmType.FUEL1_AGREGAT;
                            int rowHandle = gvFuelControlDut.LocateByValue("Mobitel_ID", m_row.Mobitel_ID);
                            if (rowHandle != DevExpress.XtraGrid.GridControl.InvalidRowHandle)
                                gvFuelControlDut.FocusedRowHandle = rowHandle;
                        }
                        else if (index == 2)
                        {
                            atlantaDataSetBindingSource.Filter = "TypeAlgorythm=" + (int) AlgorithmType.FUEL2_AGREGAT;
                            int rowHandle = gvFuelControlDut.LocateByValue("Mobitel_ID", m_row.Mobitel_ID);
                            if (rowHandle != DevExpress.XtraGrid.GridControl.InvalidRowHandle)
                                gvFuelControlDut.FocusedRowHandle = rowHandle;
                        }
                        else if (index == 3)
                        {
                            atlantaDataSetBindingSource.Filter = "TypeAlgorythm=" + (int) AlgorithmType.FUEL3_AGREGAT;
                            int rowHandle = gvFuelControlDut.LocateByValue("Mobitel_ID", m_row.Mobitel_ID);
                            if (rowHandle != DevExpress.XtraGrid.GridControl.InvalidRowHandle)
                                gvFuelControlDut.FocusedRowHandle = rowHandle;
                        }
                        else if (index == 4)
                        {
                            atlantaDataSetBindingSource.Filter =
                                "TypeAlgorythm=" + (int) AlgorithmType.FUEL_AGREGAT_ALL;
                            int rowHandle = gvFuelControlDut.LocateByValue("Mobitel_ID", m_row.Mobitel_ID);
                            if (rowHandle != DevExpress.XtraGrid.GridControl.InvalidRowHandle)
                                gvFuelControlDut.FocusedRowHandle = rowHandle;
                        }
                    }

                    if (CurrentActivateReport == NameReport)
                    {
                        if (index == 0)
                        {
                            SelectGraphic(curMrow, (int) AlgorithmType.FUEL1);
                            atlantaDataSetBindingSource.Filter = "TypeAlgorythm=" + (int) AlgorithmType.FUEL1;
                            int rowHandle = gvFuelControlDut.LocateByValue("Mobitel_ID", m_row.Mobitel_ID);
                            if (rowHandle != DevExpress.XtraGrid.GridControl.InvalidRowHandle)
                                gvFuelControlDut.FocusedRowHandle = rowHandle;
                        }
                        else if (index == 1)
                        {
                            SelectGraphic(curMrow, (int) AlgorithmType.FUEL1_AGREGAT);
                            atlantaDataSetBindingSource.Filter = "TypeAlgorythm=" + (int) AlgorithmType.FUEL1_AGREGAT;
                            int rowHandle = gvFuelControlDut.LocateByValue("Mobitel_ID", m_row.Mobitel_ID);
                            if (rowHandle != DevExpress.XtraGrid.GridControl.InvalidRowHandle)
                                gvFuelControlDut.FocusedRowHandle = rowHandle;
                        }
                        else if (index == 2)
                        {
                            SelectGraphic(curMrow, (int) AlgorithmType.FUEL2_AGREGAT);
                            atlantaDataSetBindingSource.Filter = "TypeAlgorythm=" + (int) AlgorithmType.FUEL2_AGREGAT;
                            int rowHandle = gvFuelControlDut.LocateByValue("Mobitel_ID", m_row.Mobitel_ID);
                            if (rowHandle != DevExpress.XtraGrid.GridControl.InvalidRowHandle)
                                gvFuelControlDut.FocusedRowHandle = rowHandle;
                        }
                        else if (index == 3)
                        {
                            SelectGraphic(curMrow, (int) AlgorithmType.FUEL3_AGREGAT);
                            atlantaDataSetBindingSource.Filter = "TypeAlgorythm=" + (int) AlgorithmType.FUEL3_AGREGAT;
                            int rowHandle = gvFuelControlDut.LocateByValue("Mobitel_ID", m_row.Mobitel_ID);
                            if (rowHandle != DevExpress.XtraGrid.GridControl.InvalidRowHandle)
                                gvFuelControlDut.FocusedRowHandle = rowHandle;
                        }
                        else if (index == 4)
                        {
                            SelectGraphic(curMrow, (int) AlgorithmType.FUEL_AGREGAT_ALL);

                            atlantaDataSetBindingSource.Filter = "TypeAlgorythm=" + (int) AlgorithmType.FUEL_AGREGAT_ALL;
                            int rowHandle = gvFuelControlDut.LocateByValue("Mobitel_ID", m_row.Mobitel_ID);
                            if (rowHandle != DevExpress.XtraGrid.GridControl.InvalidRowHandle)
                                gvFuelControlDut.FocusedRowHandle = rowHandle;
                        }
                    }

                    CalcTotals(m_row.Mobitel_ID, globalCode);
                } // if
                else
                {
                    //DisableButton();
                    //gcFuelControlDut.DataSource = null;
                } // else

                //atlantaDataSetBindingSource.Filter = "Mobitel_ID >= 0";
            } // if
            else
            {
                DisableButton();
                ReportsControl.GraphClearSeries();
                Graph.ClearLabel();
                atlantaDataSetBindingSource.Filter = "TypeAlgorythm=" + (int)AlgorithmType.FUEL1;
            } // else
        } // Select

        protected double dRoundValue(double x)
        {
            int koeff = 100;

            int number = ( int ) x;
            int parth = (int)(( x - number ) * koeff);
            
            return ((double)((double)number + (double)parth / (double)koeff));
        } // RoundValue

        protected float fRoundValue( float x )
        {
            int koeff = 100;

            int number = ( int ) x;
            int parth = ( int ) ( ( x - number ) * koeff );

            return ( ( float ) ( ( float ) number + ( float ) parth / ( float ) koeff ) );
        } // RoundValue



        // Расчет итогов
        private void CalcTotals(int idmobil, int algo)
        {
            totalTravel = 0;
            totalFuelRate = 0;
            avgFuelRateInMachineHour = 0;
            avgMotionFuelRate = 0;
            fuelAdd = 0;
            fuelSub = 0;

            TimeSpan totalTimeEngineOn = new TimeSpan(0);
            listCurMrowTs.Clear();

            foreach (DataRowView dr in atlantaDataSetBindingSource)
            {
                FuelExpensesDataSet.FuelExpensesDUT_TotalRow row =
                    (FuelExpensesDataSet.FuelExpensesDUT_TotalRow) dr.Row;

                //if (row.TypeAlgorythm == algo)
                //    continue;

                MobitelData mbData = new MobitelData();
                mbData.idmobi = row.Mobitel_ID;
                mbData.typealg = row.TypeAlgorythm;

                listCurMrowTs.Add(mbData);

                // сдесь попробуем округлить
                row.Travel = fRoundValue(row.Travel);
                row.FuelBegin = dRoundValue(row.FuelBegin);
                row.FuelEnd = dRoundValue(row.FuelEnd);
                row.TotalFuelAdd = dRoundValue(row.TotalFuelAdd);
                row.TotalFuelSub = dRoundValue(row.TotalFuelSub);
                row.TotalFuelUse = dRoundValue(row.TotalFuelUse);
                row.FuelUseAvg = dRoundValue(row.FuelUseAvg);

                if (!row.IsLitrePerHourNull())
                    row.LitrePerHour = fRoundValue(row.LitrePerHour);

                row.MaxSpeed = fRoundValue(row.MaxSpeed);
                // сдесь конец округления

                totalTravel += row.Travel;
                totalFuelRate += row.TotalFuelUse;
                avgMotionFuelRate += row.FuelUseAvg;
                fuelAdd += row.TotalFuelAdd;
                fuelSub += row.TotalFuelSub;

                if (!row.IsLitrePerHourNull())
                    totalTimeEngineOn += (row.TimeMotion + (row.IsTimeStopWithEngineOnNull()
                        ? new TimeSpan(0)
                        : row.TimeStopWithEngineOn));
            } // foreach

            avgFuelRateInMachineHour = totalTimeEngineOn.TotalHours == 0 ? 0 : Math.Round((totalFuelRate - fuelSub)/totalTimeEngineOn.TotalHours, 2);

            // aketner - 28.08.2012
            if (atlantaDataSetBindingSource.Count == 1)
            {
                avgMotionFuelRate = atlantaDataSetBindingSource.Count == 0 ? 0 : Math.Round(avgMotionFuelRate / atlantaDataSetBindingSource.Count, 2);
            }
            else if (atlantaDataSetBindingSource.Count > 1)
            {
                avgMotionFuelRate = atlantaDataSetBindingSource.Count == 0 ? 0 : Math.Round((totalFuelRate - fuelSub) / totalTravel * 100, 2); 
            }
        } // CalcTotals

        protected override void Algorithm_Action(object sender, EventArgs e)
        {
            if (sender is TotalFuelExpensesDutAlgorithm) { }
                //EnableButton();
        }

        // очистим структуры данных отчета
        public override void ClearReport()
        {
            fuelAlgorythm.ClearAtlantaFuelReport();
            listCurMrowTs.Clear();
            feDataSet.FuelExpensesDUT_Total.Clear();
        }

        // Выборка данных
        public void SelectItem(atlantaDataSet.mobitelsRow m_row)
        {
            Application.DoEvents();
            foreach (IAlgorithm alg in _algoritms)
            {
                alg.SelectItem(m_row);
                alg.Run();

                if (_stopRun)
                    return;
            }

            Application.DoEvents();
            //ReportsControl.ShowGraph(m_row);
        } // SelectItem

        static Track GetTrackSegment(atlantaDataSet.KilometrageReportRow row)
        {
            List<IGeoPoint> data = new List<IGeoPoint>();
            bool inside = false;

            foreach (GpsData gpsData in Algorithm.GpsDatas.OrderBy(gps => gps.Time).ToList())
            {
                if (gpsData.Id == row.InitialPointId)
                    inside = true;

                if (inside)
                    data.Add(gpsData);

                if (gpsData.Id == row.FinalPointId)
                    break;
            } // foreach

            return new Track(row.MobitelId, Color.DarkBlue, 2f, data);
        } // GetTrackSegment

        static void showMark(PointLatLng latlng, int mobitelId, string message)
        {
            PointLatLng location = new PointLatLng(latlng.Lat, latlng.Lng);
            List<Marker> markers = new List<Marker>();
            markers.Add(new Marker(MarkerType.Report, mobitelId, location, message, ""));
            ReportsControl.OnMarkersShowNeededWith(markers);
        }

        // строим трек на карте на основе отчета Пробег
        protected void ShowTrackOnMap(int iRow)
        {
            try
            {
                atlantaDataSet.KilometrageReportRow klmtRow;
                List<Track> segments = new List<Track>();
                ReportsControl.OnClearMapObjectsNeeded();
                MobitelData mobRow = listCurMrowTs[iRow];
                DataRow[] dRow = dataset.KilometrageReport.Select("MobitelId=" + mobRow.idmobi + " and " + "TypeAlgorythm=" + mobRow.typealg, "Id ASC");

                for (int i = 0; i < dRow.Length; i++)
                {
                    klmtRow = (atlantaDataSet.KilometrageReportRow) ((DataRow) dRow[i]);
                    segments.Add(GetTrackSegment(klmtRow));
                } // for

                if (segments.Count > 0)
                {
                    ReportsControl.OnTrackSegmentsShowNeededWith(segments);
                    DataRow Row = gvFuelControlDut.GetDataRow(iRow);
                    int icount = segments.Count - 1;
                    int ipoint = segments[icount].GeoPoints.Count - 1;
                    if (Row != null)
                    {
                        showMark(segments[icount].GeoPoints[ipoint].LatLng, mobRow.idmobi, Row["NameCar"].ToString());
                    }
                } // if
            }
            catch
            {
                // to do
            }
        } // ShowTrackOnMap

        // show track on map - click button map
        protected override void bbiShowOnMap_ItemClick(object sender, ItemClickEventArgs e)
        {
            ReportsControl.OnMapShowNeeded();
            int indexRow = gvFuelControlDut.GetFocusedDataSourceRowIndex();
            ShowTrackOnMap(indexRow); /* построить трек на карте */
        } // bbiShowOnMap_ItemClick

        // show graph - click button graph
        protected override void bbiShowOnGraf_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                ReportsControl.OnGraphShowNeeded();
                int indexRow = gvFuelControlDut.GetFocusedDataSourceRowIndex();
                MobitelData mob_id = listCurMrowTs[indexRow];
                atlantaDataSet.mobitelsRow  mrow =  dataset.mobitels.FindByMobitel_ID(mob_id.idmobi);
                
                SelectGraphic(mrow); // перерисовать график;
            }
            catch
            {
                // to do
            }
        } // bbiShowOnGraf_ItemClick

        //клик по строке таблицы
        //private void gvFuelControlDut_ClickRow(object sender, RowClickEventArgs e)
        //{
        //    if (e.RowHandle >= 0)
        //    {
        //        int indexRow = gvFuelControlDut.GetFocusedDataSourceRowIndex();
        //        SelectGraphic(listCurMrow[indexRow]); // перерисовать график
        //        ShowTrackOnMap(indexRow); // перерисовать карту
        //    } // if
        //} // gvDriver_FocusedRowChanged

        private void gvFuelControlDut_FocusedRowChanged( object sender, FocusedRowChangedEventArgs e )
        {
            if( e.FocusedRowHandle >= 0 )
            {
                int indexRow = gvFuelControlDut.GetVisibleIndex(e.FocusedRowHandle);

                if (listCurMrowTs.Count > 0)
                {
                    if (indexRow >= 0)
                    {
                        MobitelData mob_id = listCurMrowTs[indexRow];
                        atlantaDataSet.mobitelsRow mrow = dataset.mobitels.FindByMobitel_ID(mob_id.idmobi);
                        SelectGraphic(mrow); // перерисовать график
                        ShowTrackOnMap(indexRow); // перерисовать карту
                    }
                }
            } // if
        }

        // Нажатие кнопки формирования таблицы отчета
        protected override void bbiStart_ItemClick(object sender, ItemClickEventArgs e)
        {
            bool noData = true; // За выбранный период нет данных ни по одной машине

            if (bchSafetyMode.Checked)
            {
                RunInSafeMode(Caption);
                return;
            }

            if (bbiStart.Caption == Resources.Start)
            {
                SetStopButton();
                DisableButton();
                ClearReport();
                BeginReport();
                _stopRun = false;

                atlantaDataSetBindingSource.RaiseListChangedEvents = false;
                atlantaDataSetBindingSource.SuspendBinding();

                foreach (atlantaDataSet.mobitelsRow m_row in dataset.mobitels)
                {
                    if (m_row.Check && DataSetManager.IsGpsDataExist(m_row))
                    {
                        SelectItem(m_row);
                        noData = false;
                        if (_stopRun)
                            break;
                    } // if
                } // foreach

                if (noData)
                {
                    XtraMessageBox.Show(Resources.WarningNoData, Resources.Notification,
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

                atlantaDataSetBindingSource.RaiseListChangedEvents = true;
                atlantaDataSetBindingSource.ResumeBinding();
                atlantaDataSetBindingSource.Filter = "TypeAlgorythm=" + (int)AlgorithmType.FUEL1;
                Select(curMrow);

                SetStartButton();

                if (atlantaDataSetBindingSource.Count > 0)
                {
                    EnableButton();
                }
            }
            else
            {
                SetStartButton();
                _stopRun = true;
                StopReport();
                if (!noData)
                    EnableButton();
            } // else
        } // bbiStart_ItemClick

        public void SelectGraphic(atlantaDataSet.mobitelsRow mobitel, int type)
        {
            if (mobitel != null)
            {
                curMrow = mobitel;
                vehicleInfo = new VehicleInfo(mobitel);
                atlantaDataSetBindingSource.Filter = "Mobitel_id=" + mobitel.Mobitel_ID + " and " + "TypeAlgorythm=" + type;

                try
                {
                    ReportsControl.GraphClearSeries();

                    if (type == (int)AlgorithmType.FUEL1)
                    {
                        CreateGraph(mobitel, AlgorithmType.FUEL1);
                    }
                    else if (type == (int)AlgorithmType.FUEL1_AGREGAT)
                    {
                        CreateGraph(mobitel, AlgorithmType.FUEL1_AGREGAT);
                    }
                    else if (type == (int)AlgorithmType.FUEL2_AGREGAT)
                    {
                        CreateGraph(mobitel, AlgorithmType.FUEL2_AGREGAT);
                    }
                    else if (type == (int)AlgorithmType.FUEL3_AGREGAT)
                    {
                        CreateGraph(mobitel, AlgorithmType.FUEL3_AGREGAT);
                    }
                    else if (type == (int)AlgorithmType.FUEL_AGREGAT_ALL)
                    {
                        CreateGraph(mobitel, AlgorithmType.FUEL_AGREGAT_ALL);
                    }
                    else
                    {
                        throw new NotImplementedException("Unknown type algorythm!");
                    }
                }
                catch (Exception ex)
                {
                    string message = String.Format("Exception in FuelExpensesControlDut: {0}", ex.Message);
                    XtraMessageBox.Show(message, Caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            } // if
        }

        private void CreateGraph(atlantaDataSet.mobitelsRow mobitel_row, object algoParam)
        {
            if (!DataSetManager.IsGpsDataExist(mobitel_row)) return;

            buildGraph.SetAlgorithm(algoParam);
            buildGraph.AddGraphFuel(algoParam, Graph, dataset, mobitel_row);
            buildGraph.AddGraphVoltage(Graph, dataset, mobitel_row); // add 04.07.2017 aketner
            ReportsControl.ShowGraph(mobitel_row);

            if (buildGraph.AddGraphInclinometers(Graph, dataset, mobitel_row))
            {
                vehicleInfo = new VehicleInfo(mobitel_row);
                Graph.ShowSeries(vehicleInfo.Info);
            }
        } // CreateGraph

        public void SelectGraphic(atlantaDataSet.mobitelsRow mobitel_row)
        {
            if (mobitel_row == null)
                return;

            if (!DataSetManager.IsGpsDataExist(mobitel_row)) 
                return;

            curMrow = mobitel_row;
            vehicleInfo = new VehicleInfo(mobitel_row.Mobitel_ID, mobitel_row);
            ReportsControl.GraphClearSeries();
            Graph.ClearRegion();
            buildGraph.SetAlgorithm(AlgorithmType.FUEL1);
            buildGraph.AddGraphFuel(AlgorithmType.FUEL1, Graph, dataset, mobitel_row);
            buildGraph.SetAlgorithm(AlgorithmType.FUEL1_AGREGAT);
            buildGraph.AddGraphFuel(AlgorithmType.FUEL1_AGREGAT, Graph, dataset, mobitel_row);
            buildGraph.SetAlgorithm(AlgorithmType.FUEL2_AGREGAT);
            buildGraph.AddGraphFuel(AlgorithmType.FUEL2_AGREGAT, Graph, dataset, mobitel_row);
            buildGraph.SetAlgorithm(AlgorithmType.FUEL3_AGREGAT);
            buildGraph.AddGraphFuel(AlgorithmType.FUEL3_AGREGAT, Graph, dataset, mobitel_row);
            buildGraph.SetAlgorithm(AlgorithmType.FUEL_AGREGAT_ALL);
            buildGraph.AddGraphFuel(AlgorithmType.FUEL_AGREGAT_ALL, Graph, dataset, mobitel_row);
            
            string nameSeries = vehicleInfo.RegistrationNumber + vehicleInfo.CarMaker + vehicleInfo.CarModel;
            Graph.ShowSeries(nameSeries);
            ReportsControl.ShowGraph(curMrow);

            if (buildGraph.AddGraphInclinometers(Graph, dataset, curMrow))
            {
                vehicleInfo = new VehicleInfo(curMrow);
                Graph.ShowSeries(vehicleInfo.Info);
            }
        } // SelectGraphic

        // формирование колонтитулов верхних отчета
        private void composLink_CreateMarginalHeaderArea(object sender, CreateAreaEventArgs e)
        {
            DevExpressReportHeader(Resources.TotalFuelExpensesDutReportName, e);
            TInfo info = ReportingFuelDUTTotal.GetInfoStructure;
            string strPeriod = Resources.PeriodFrom + " " + info.periodBeging + " " +
                Resources.PeriodTo + " " + info.periodEnd;
            DevExpressReportSubHeader(strPeriod, 22, e);
        }

        /* функция для формирования левой части заголовка отчета */
        protected string GetStringBreackLeft()
        {
	        ReportingFuelDUTTotal.SetRectangleBrckLetf(0, 0, 300, 85);
            return (Resources.TotalTransportTraveled + ": " + totalTravel.ToString("N2") + "\n" +
                Resources.TotalFuelConsumption + ": " + totalFuelRate.ToString("N2") + "\n" +
                Resources.FuelExControlDutCommonFuelAdd + ": " + fuelAdd.ToString("N2"));
        }

        /* функция для формирования верхней части заголовка отчета - будет пусто*/
        protected string GetStringBreackUp()
        {
	        ReportingFuelDUTTotal.SetRectangleBrckUP(380, 0, 320, 85);
            return ("");
        }

        /* функция для формирования правой части заголовка отчета */
        protected string GetStringBreackRight()
        {
	        ReportingFuelDUTTotal.SetRectangleBrckRight(700, 0, 250, 85);
            return (Resources.FuelExControlMiddleFuelrateInMove + ": " + avgMotionFuelRate.ToString("N2") + "\n" +
                Resources.FuelExControlMotorHour + ": " + avgFuelRateInMachineHour.ToString("N2") + "\n" +
                Resources.FuelExControlDutCommonFuelPourOff + ": " + fuelSub.ToString("N2"));
        }

        // Формирует данные для отчета по всем активным машинам
        protected override void ExportAllDevToReport()
        {
            XtraGridService.SetupGidViewForPrint(gvFuelControlDut, true, true);

            TInfo info = new TInfo();

            // заголовок для таблиц отчета, для каждого отчета разный
            info.periodBeging = Algorithm.Period.Begin;
            info.periodEnd = Algorithm.Period.End;

            ReportingFuelDUTTotal.AddInfoStructToList(info); /* формируем заголовки таблиц отчета */
            ReportingFuelDUTTotal.CreateAndShowReport(gcFuelControlDut);
        } // ExportToExcelDevExpress
        
        protected override void barButtonGroupPanel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GroupPanel(gvFuelControlDut);
        }

        protected override void barButtonFooterPanel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            FooterPanel(gvFuelControlDut);
        }
        
        protected override void barButtonNavigator_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            NavigatorPanel(gcFuelControlDut);
        }

        private void setSafeMode()
        {
            bchSafetyMode.Visibility = BarItemVisibility.Always;
        }

        public void Localization()
        {
            colNameCar.Caption = Resources.FuelExControlNameCar;
            colNameCar.ToolTip = Resources.FuelExControlNameCar;

            colFIO.Caption = Resources.FuelExControlDriverFIO;
            colFIO.ToolTip = Resources.FuelExControlDriverFIO;

            colTravel.Caption = Resources.FuelExControlTravel;
            colTravel.ToolTip = Resources.FuelExControlTravel;

            colFuelBegin.Caption = Resources.FuelExControlDutFuelInBeginTime;
            colFuelBegin.ToolTip = Resources.FuelExControlDutFuelInBeginTime;

            colFuelEnd.Caption = Resources.FuelExControlDutFuelInEndTime;
            colFuelEnd.ToolTip = Resources.FuelExControlDutFuelInEndTime;

            colFuelAddCount.Caption = Resources.FuelExControlDutNumbersFuelAdd;
            colFuelAddCount.ToolTip = Resources.FuelExControlDutNumbersFuelAdd;

            colFuelSubCount.Caption = Resources.FuelExControlDutNumberFuelPourOff;
            colFuelSubCount.ToolTip = Resources.FuelExControlDutNumberFuelPourOff;

            colTotalFuelAdd.Caption = Resources.FuelExControlDutCommonFuelAdd;
            colTotalFuelAdd.ToolTip = Resources.FuelExControlDutCommonFuelAdd;

            colTotalFuelSub.Caption = Resources.FuelExControlDutCommonFuelPourOff;
            colTotalFuelSub.ToolTip = Resources.FuelExControlDutCommonFuelPourOff;

            colTotalFuelUse.Caption = Resources.FuelExControlCommonFuelrate;
            colTotalFuelUse.ToolTip = Resources.FuelExControlCommonFuelrate;

            colFuelUseAvg.Caption = Resources.FuelExControlMiddleFuelrateInMove;
            colFuelUseAvg.ToolTip = Resources.FuelExControlMiddleFuelrateInMove;

            colLitrePerHour.Caption = Resources.FuelExControlMiddleFuelrateOnTime;
            colLitrePerHour.ToolTip = Resources.FuelExControlMiddleFuelrateOnTime;

            colTimeMotion.Caption = Resources.FuelExControlTimeMove;
            colTimeMotion.ToolTip = Resources.FuelExControlTimeMove;

            colTimeStopWithEngineOn.Caption = Resources.FuelExControlTimeStopWithEngineOn;
            colTimeStopWithEngineOn.ToolTip = Resources.FuelExControlTimeStopWithEngineOn;

            colTimeStopWithEngineOff.Caption = Resources.FuelExControlDutTimeStopsWithEngineOff;
            colTimeStopWithEngineOff.ToolTip = Resources.FuelExControlDutTimeStopsWithEngineOff;
            
            colMaxSpeed.Caption = Resources.FuelExControlMaxSpeed;
            colMaxSpeed.ToolTip = Resources.FuelExControlMaxSpeed;
            
            colNormaFuelWay.Caption = Resources.NormalFuelWay;
            colNormaFuelWay.ToolTip = Resources.NormalFuelWayTip;
            
            colNormalMotoHr.Caption = Resources.NormalMotorHr;
            colNormalMotoHr.ToolTip = Resources.NormalMotorHrTip;

            colSummNormal.Caption = Resources.SummaNormaTotal;
            colSummNormal.ToolTip = Resources.SummaNormaTotalTip;

            colMotoHours.Caption = Resources.SummaMotoHours;
            colMotoHours.ToolTip = Resources.SummaMotoHoursTip;
        }

        protected override void comboAlgoChoiser_ItemClick(object sender, EventArgs e)
        {
            ComboBoxEdit combo = (ComboBoxEdit)sender;
            comboAlgorithm = combo;
            Select(curMrow);
        } // comboAlgoChoiser_ItemClick

        private void gcFuelControlDut_Click(object sender, EventArgs e)
        {
            // to do
        } 
    } // FuelExpensesCtrlDut
} // ReportsOnFuelExpenses
