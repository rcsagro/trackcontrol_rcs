#region Using directives
using System;
using System.Windows.Forms.VisualStyles;
using BaseReports.Procedure;
using TrackControl.Reports;
using ReportsOnFuelExpenses.Properties;
#endregion

namespace ReportsOnFuelExpenses
{
    /// <summary>
    /// ������������ ������ ������ �� ������� �� ��������� ��������
    /// ��������� ����, ����� ��������, ������ ������� � ��������, ������ �� 100 ��,
    /// ����� ������� � ���. ����, ������ �� ��������, ������� ������ �� ��������, ����� ������,
    /// ������� ������ �� �������, ����� ������ �������������, ������� ������ ������������� � ���,
    /// ������ ������� �� �������������.
    /// <para>��� ������� ������� ������� ����������� ������ ������� �������</para>
    /// </summary>
    public class TotalFuelExpensesDrtAlgorithm : PartialAlgorithms
    {
        private FuelExpensesDataSet feDataSet;


        public TotalFuelExpensesDrtAlgorithm()
        {
            feDataSet = FuelExpensesDataSetCreator.GetDataSet();
        }

        /// <summary>
        /// ������ ��� �������
        /// </summary>
        public override void Run()
        {
            if ((GpsDatas.Length == 0) || m_row == null)
            {
                return;
            }
            if (feDataSet != null)
            {
                if (feDataSet.FuelExpensesDRT_Total.Select(string.Format("Mobitel_ID = {0}", m_row.Mobitel_ID)).Length > 0)
                    return;
            }

            BeforeReportFilling(Resources.TotalFuelExDrtName, 1);

            try
            {
                FillFuelExpenses();
            }
            finally
            {
                AfterReportFilling();
                Algorithm.OnAction(this, null);
            }
        }

        /// <summary>
        /// ����� ����������� �� ��������� m_row. 
        /// </summary>
        private void FillFuelExpenses()
        {
            // ������� ��� ��������� ������ � ������� RotateValue ������ �� ���������� ����� ������
            if (m_row.GetflowmeterReportRows().Length > 0) //.GetflowmeterValueRows().Length > 0)
            {
                FuelExpensesDataSet.FuelExpensesDRT_TotalRow row =
                    feDataSet.FuelExpensesDRT_Total.NewFuelExpensesDRT_TotalRow();
                row.Mobitel_ID = m_row.Mobitel_ID;
                row.NameCar = Vehicle.Info;
                row.FIO = Vehicle.DriverFullName;

                // ������� ������
                //LocalCache.atlantaDataSet.dataviewRow[] dataRows = m_row.GetdataviewRows();//atlantaDataSet.dataview.Select("Mobitel_Id=" + m_row.Mobitel_ID.ToString(), "time");
                // ����� ����� ��������, �
                TimeSpan travelTime = BasicMethods.GetTotalMotionTime(GpsDatas);
                row.TimeMoving = (float) travelTime.TotalHours;

                if(travelTime.Days != 0)
                    row.TimeMovingStr = travelTime.ToString( @"dd\.hh\:mm\:ss" );
                else
                    row.TimeMovingStr = travelTime.ToString( @"hh\:mm\:ss" );

                // ���������� ����, ��
                row.Travel = (float) m_row.path;
                    //// Convert.ToInt32(row.TimeMoving) == 0 ? 0 : (float)GetTravel(dataRows);
                // ������ ������� � ��������, �
                row.FuelUseInMotion = GetFuelUseInMotionDrt(GpsDatas, 0);
                // ������� ������ � ��������, �/100��
                row.FuelMiddleInMotion = (row.Travel == 0 ? 0 : (row.FuelUseInMotion/row.Travel*100));

                double timeTotal = (BasicMethods.GetTotalStopsTime(GpsDatas, 0).TotalHours - GetTimeStopEnginOff(GpsDatas, 0, 0).TotalHours);

                row.TimeStopWithEngineOn = (float) timeTotal;
                
                double ds = timeTotal / 24.0;
                int days = (int) (ds);
                double h = ((ds - days) * 24.0);
                int hr = (int) h;
                double mn = (h - hr) * 60.0;
                int minute = (int) mn ;
                double sc = ( mn - minute ) * 60.0;
                int second = ( int )sc;

                TimeSpan tmsp = new TimeSpan(days, hr, minute, second);
                if(tmsp.Days != 0)
                    row.TimeStopWithEngineOnStr = tmsp.ToString( @"dd\.hh\:mm\:ss" );
                else
                    row.TimeStopWithEngineOnStr = tmsp.ToString( @"hh\:mm\:ss" );

                // ������ ������� �� ��������, �
                row.FuelUseOnStop = GetFuelUseInStopDrt(GpsDatas, 0, 0);
                // ������� ������ ������� �� ��������, �
                row.FuelMiddleOnStop = row.TimeStopWithEngineOn == 0
                    ? 0
                    : row.FuelUseOnStop/row.TimeStopWithEngineOn;

                // ����� ������ ������� � ����, �
                row.TotalFuelUse = row.FuelUseOnStop + row.FuelUseInMotion;
                //  ������ �� �������
                row.TotalMiddleFuelUseOneTime = row.TotalFuelUse/(row.TimeStopWithEngineOn + row.TimeMoving);

                feDataSet.FuelExpensesDRT_Total.AddFuelExpensesDRT_TotalRow(row);
            }

            ReportProgressChanged(1);
        }
    }
}
