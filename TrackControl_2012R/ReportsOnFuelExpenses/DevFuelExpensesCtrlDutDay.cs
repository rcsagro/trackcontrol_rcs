﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using BaseReports.ReportsDE;
using BaseReports.Procedure;
using BaseReports.Properties;
using BaseReports.RFID;
using LocalCache;
using TrackControl.General;
using TrackControl.Reports;
using TrackControl.Vehicles;
using TrackControl.Reports.Graph;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrinting.Drawing;
using DevExpress.XtraExport;
using DevExpress.XtraReports;
using DevExpress.XtraReports.UI;
using DevExpress.XtraPrinting.Native;
using DevExpress.XtraPrinting.Control;
using DevExpress.XtraPrinting.Preview;
using DevExpress.XtraPrintingLinks;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views;
using DevExpress.XtraGrid.Views.Grid;
using ReportsOnFuelExpenses;
using BaseReports;
using ReportsOnFuelExpenses.Properties;
using Report;

namespace ReportsOnFuelExpenses
{
    public partial class DevFuelExpensesCtrlDutDay : BaseReports.ReportsDE.BaseControl
    {
        public class repFuelDutDay
        {
            string date;
            string iniTime;
            string finTime;
            string intervWork;
            string intervMove;
            string intervStops;
            string timeEngineOn;
            double distance;
            double fuel_begin;
            double fuel_end;
            double total_fuel_add;
            double total_fuel_sub;
            double total_fuel_use;
            double fuel_use_middle_motion;
            double idle_fuel_rate;

            public repFuelDutDay(string date, string iniTime, string finTime, string intervWork, string intervMove,
                string intervStops, string timeEngineOn, double distance, double fuel_begin, double fuel_end,
                double total_fuel_add, double total_fuel_sub, double total_fuel_use, double fuel_use_middle_motion,
                double idle_fuel_rate)
            {
                this.date = date;
                this.iniTime = iniTime;
                this.finTime = finTime;
                this.intervWork = intervWork;
                this.intervMove = intervMove;
                this.intervStops = intervMove;
                this.timeEngineOn = timeEngineOn;
                this.distance = distance;
                this.fuel_begin = fuel_begin;
                this.fuel_end = fuel_end;
                this.total_fuel_add = total_fuel_add;
                this.total_fuel_sub = total_fuel_sub;
                this.total_fuel_use = total_fuel_use;
                this.fuel_use_middle_motion = fuel_use_middle_motion;
                this.idle_fuel_rate = idle_fuel_rate;
            } // repFuelDutDay

            public string Date
            {
                get { return date; }
            }

            public string InitialTime
            {
                get { return iniTime; }
            }

            public string FinalTime
            {
                get { return finTime; }
            }

            public string IntervalWork
            {
                get { return intervWork; }
            }

            public string IntervalMove
            {
                get { return intervMove; }
            }

            public string IntervalStop
            {
                get { return intervStops; }
            }

            public string TimeWithEngineOn
            {
                get { return timeEngineOn; }
            }

            public double Distance
            {
                get { return distance; }
            }

            public double FuelBegin
            {
                get { return fuel_begin; }
            }

            public double FuelEnd
            {
                get { return fuel_end; }
            }

            public double TotalFuelAdd
            {
                get { return total_fuel_add; }
            }

            public double TotalFuelSub
            {
                get { return total_fuel_sub; }
            }

            public double TotalFuelUse
            {
                get { return total_fuel_use; }
            }

            public double FuelUseMiddleInMotion
            {
                get { return fuel_use_middle_motion; }
            }

            public double idleFuelRate
            {
                get { return idle_fuel_rate; }
            }
        } // repFuelDutDay

        // Словарь, содержащий итоговые данные для каждого телетрека (транспортного
        // средства). Ключом является ID телетрека.
        private Dictionary<int, TotalFuelExpensesDutDayAlgorithm.SummaryDutDay> _summariesDutDay =
            new Dictionary<int, TotalFuelExpensesDutDayAlgorithm.SummaryDutDay>();

        // датасет
        public FuelExpensesDataSet feDataSet;
        // Интерфейс для построение графиков по SeriesL
        private static IBuildGraphs buildGraph;
        protected static atlantaDataSet dataset;
        protected VehicleInfo vehicleInfo;
        ReportBase<repFuelDutDay, TInfoDut> ReportingFuelDutDay;

        public DevFuelExpensesCtrlDutDay()
        {
            InitializeComponent();
            Localization();
            HideStatusBar();
            DisableButton();
            DisableRun();
            VisionPanel(gvFuelCtrlDutDay, gcFuelCtrlDutDay, bar3);
            feDataSet = FuelExpensesDataSetCreator.GetDataSet();
            atlantaDataSetBindingSource.DataSource = feDataSet;
            atlantaDataSetBindingSource.DataMember = "FuelExpensesDUT_Day";
            gcFuelCtrlDutDay.DataSource = atlantaDataSetBindingSource;
            buildGraph = new BuildGraphs();
            fuelExpensesDataSet = null;
            dataset = ReportTabControl.Dataset;
            ClearStatusLine();
            AddAlgorithm(new KilometrageDays());
            AddAlgorithm(new Rotation());
            AddAlgorithm(new Fuel(AlgorithmType.FUEL1));
            AddAlgorithm(new TotalFuelExpensesDutDayAlgorithm());
            
            gvFuelCtrlDutDay.RowClick += new RowClickEventHandler(gvFuelControlDutDay_ClickRow);

            compositeReportLink.CreateMarginalHeaderArea +=
                new CreateAreaEventHandler(composLink_CreateMarginalHeaderArea);

            ReportingFuelDutDay =
                new ReportBase<repFuelDutDay, TInfoDut>(Controls, compositeReportLink, gvFuelCtrlDutDay,
                    GetStringBreackLeft, GetStringBreackRight, GetStringBreackUp);
        } // DevFuelExpensesCtrlDutDay

        public override string Caption
        {
            get
            {
                return Resources.TotalFuelExDutDayName;
            }
        }

        // Отображение  итогов
        void ClearStatusLine()
        {
            lblTotalDebitText.Caption = Resources.FuelExControlCommonFuelrate + ": ---";
            lblTotalFuelAddText.Caption = Resources.FuelExControlDutCommonFuelAdd + ": ---";
            lblTotalFuelSubText.Caption = Resources.FuelExControlDutCommonFuelPourOff + ": ---";
            lblAvgFuelDebitText.Caption = Resources.FuelExControlDutDayFuelrate + ": ---";
            lblTotalMotoText.Caption = Resources.FuelExControDutDayFuelrateOnTime + ": ---";
        } // clearStatusLine

        public override void Select(atlantaDataSet.mobitelsRow m_row)
        {
            if (m_row != null)
            {
                curMrow = m_row;

                if (_summariesDutDay.ContainsKey(m_row.Mobitel_ID))
                {
                    // Заполняем данными итоговую статусную панель
                    lblTotalDebitText.Caption = String.Concat(Resources.FuelExControlCommonFuelrate, ": ", _summariesDutDay[m_row.Mobitel_ID].TotalDebit.ToString("F2"));
                    lblTotalFuelAddText.Caption = String.Concat(Resources.FuelExControlDutCommonFuelAdd, ": ", _summariesDutDay[m_row.Mobitel_ID].TotalFuelAdd.ToString("F2"));
                    lblTotalFuelSubText.Caption = String.Concat(Resources.FuelExControlDutCommonFuelPourOff, ": ", _summariesDutDay[m_row.Mobitel_ID].TotalFuelSub.ToString("F2"));
                    lblAvgFuelDebitText.Caption = String.Concat(Resources.FuelExControlDutDayFuelrate, ": ", _summariesDutDay[m_row.Mobitel_ID].Distance == 0 ?
                        "0" : Math.Round((100 * _summariesDutDay[m_row.Mobitel_ID].TotalDebit /
                        _summariesDutDay[m_row.Mobitel_ID].Distance), 2).ToString("F2"));
                    lblTotalMotoText.Caption = String.Concat(Resources.FuelExControDutDayFuelrateOnTime, ": ", _summariesDutDay[m_row.Mobitel_ID].TimeTotalWithEngineOnHour.TotalHours == 0 ?
                        "0" : Math.Round((_summariesDutDay[m_row.Mobitel_ID].TotalDebit /
                        _summariesDutDay[m_row.Mobitel_ID].TimeTotalWithEngineOnHour.TotalHours), 2).ToString("F2"));

                    EnableButton();
                } // if
                else
                {
                    DisableButton();
                    ClearStatusLine();
                    ReportsControl.GraphClearSeries();
                    Graph.ClearLabel();
                } // else

                atlantaDataSetBindingSource.Filter = String.Format("Mobitel_Id={0}", m_row.Mobitel_ID);
                SelectGraphic(curMrow);
            } // if
        } // Select

        // Нажатие кнопки формирования таблицы отчета
        protected override void bbiStart_ItemClick(object sender, ItemClickEventArgs e)
        {
            bool noData = true; // За выбранный период нет данных ни по одной машине
            if (bbiStart.Caption == Resources.Start)
            {
                SetStopButton();
                DisableButton();
                ClearReport();
                ClearStatusLine();
                BeginReport();
                _stopRun = false;

                //atlantaDataSetBindingSource.RaiseListChangedEvents = false;
                atlantaDataSetBindingSource.SuspendBinding();
                
                foreach (atlantaDataSet.mobitelsRow m_row in dataset.mobitels)
                {
                    if (m_row.Check && m_row.GetdataviewRows().Length > 0)
                    {
                        SelectItem(m_row);
                        noData = false;
                        if (_stopRun)
                            break;
                    } // if
                } // foreach

                if (noData)
                {
                    XtraMessageBox.Show(Resources.WarningNoData, Resources.Notification,
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

                atlantaDataSetBindingSource.ResumeBinding();
                // atlantaDataSetBindingSource.RaiseListChangedEvents = true;
                atlantaDataSetBindingSource.Filter = ""; // "Mobitel_ID >= 0";
                Select(curMrow);
                SetStartButton();
                EnableButton();
            } // if
            else
            {
                SetStartButton();
                _stopRun = true;
                StopReport();

                if (!noData)
                    EnableButton();
            } // else
        } // bbiStart_ItemClick

        // show graph - click button graph
        protected override void bbiShowOnGraf_ItemClick(object sender, ItemClickEventArgs e)
        {
            ReportsControl.OnGraphShowNeeded();
            DrawGraph();
        } // bbiShowOnGraf_ItemClick

        protected void DrawGraph()
        {
            Graph.ClearRegion();
            Graph.ClearLabel();

            Int32 [] list_row = gvFuelCtrlDutDay.GetSelectedRows();

            for (int i = 0; i < list_row.Length; i++)
            {
                FuelExpensesDataSet.FuelExpensesDUT_DayRow fd_row = (FuelExpensesDataSet.FuelExpensesDUT_DayRow)
                    ((DataRowView)atlantaDataSetBindingSource.List[list_row[i]]).Row;
                Graph.AddTimeRegion(fd_row.InitialTime, fd_row.FinalTime);

                atlantaDataSet.FuelReportRow[] FRRows = (atlantaDataSet.FuelReportRow[])dataset.FuelReport.Select(
                    String.Format("mobitel_id={0} AND time_ >= #{1:MM.dd.yyyy}# AND time_ < #{2:MM.dd.yyyy}#",
                    fd_row.Mobitel_Id, fd_row.FinalTime, fd_row.FinalTime.AddDays(1)), "time_");

                if (FRRows.Length > 0)
                {
                    foreach (atlantaDataSet.FuelReportRow tmp_FR_row in FRRows)
                    {
                        if (tmp_FR_row["time_"] != DBNull.Value)
                        {
                            string val;
                            Color color;
                            ShowGraphToolStripButton_ClickExtracted(tmp_FR_row, out val, out color);
                            Graph.AddLabel(tmp_FR_row.time_, tmp_FR_row.beginValue, tmp_FR_row.pointValue, val, color);
                        } // if
                    } // foreach
                } // if
            } // for
            Graph.SellectZoom();
        } // DrawGraph

        static void ShowGraphToolStripButton_ClickExtracted(atlantaDataSet.FuelReportRow f_row,
            out string val, out Color color)
        {
            val = String.Format("{0:#.#}л.", f_row.dValue);

            if (f_row.dValue > 0)
            {
                val = String.Format(Resources.FuelExControlFuelAdd + " {0}", val);
                color = Color.Green;
            } // if
            else
            {
                val = String.Format(Resources.FuelExControFuelPourOff + "{0}", val);
                color = Color.Red;
            } // else
        } // ShowGraphToolStripButton_ClickExtracted

        // show track on map - click button map
        protected override void bbiShowOnMap_ItemClick(object sender, ItemClickEventArgs e)
        {
            ReportsControl.OnMapShowNeeded();
            DrawMaps();
        } // bbiShowOnMap_ItemClick

        protected void DrawMaps()
        {
            ReportsControl.OnClearMapObjectsNeeded();
            List<Track> segments = new List<Track>();

            Int32[] list_row = gvFuelCtrlDutDay.GetSelectedRows();

            for (int i = 0; i < list_row.Length; i++)
            {
                FuelExpensesDataSet.FuelExpensesDUT_DayRow fd_row = (FuelExpensesDataSet.FuelExpensesDUT_DayRow)
                    ((DataRowView)atlantaDataSetBindingSource.List[list_row[i]]).Row;
                segments.Add(getTrackSegment(fd_row));
            }

            if (segments.Count > 0)
                ReportsControl.OnTrackSegmentsShowNeeded(segments);
        } // DrawMaps

        static Track getTrackSegment(FuelExpensesDataSet.FuelExpensesDUT_DayRow row)
        {
            List<IGeoPoint> data = new List<IGeoPoint>();
            bool inside = false;
            foreach (atlantaDataSet.dataviewRow d_row in dataset.dataview.Select("Mobitel_ID=" + row.Mobitel_Id, "time ASC"))
            {
                if (d_row.DataGps_ID == row.InitialPointId)
                    inside = true;
                if (inside)
                    data.Add(DataSetManager.ConvertDataviewRowToDataGps(d_row));
                if (d_row.DataGps_ID == row.FinalPointId)
                    break;
            } // foreach
            return new Track(row.Mobitel_Id, Color.Red, 2f, data);
        } // getTrackSegment

        public void SelectItem(atlantaDataSet.mobitelsRow m_row)
        {
            Application.DoEvents();
            foreach (IAlgorithm alg in _algoritms)
            {
                alg.SelectItem(m_row);
                alg.Run();

                if (_stopRun) 
                    return;
            }
        } // SelctItem

        protected override void Algorithm_Action(object sender, EventArgs e)
        {
            if (sender is TotalFuelExpensesDutDayAlgorithm)
            {
                FuelExpensesDutDayEventArgs args = (FuelExpensesDutDayEventArgs)e;
                if (_summariesDutDay.ContainsKey(args.Id))
                {
                    _summariesDutDay.Remove(args.Id);
                } // if
                _summariesDutDay.Add(args.Id, args.SummaryDutDay);
            } // if
        } // Algorithm_Action

        private void gvFuelControlDutDay_ClickRow(object sender, RowClickEventArgs e)
        {
            if (e.RowHandle >= 0)
            {
                DrawMaps();
                DrawGraph();
            }
        }

        public override void ClearReport()
        {
            if (feDataSet != null)
                feDataSet.FuelExpensesDUT_Day.Clear();
            feDataSet.FuelExpensesDUT_Day.Clear();
            _summariesDutDay.Clear();
        }

        public void SelectGraphic(atlantaDataSet.mobitelsRow mobitel_row)
        {
            if ((mobitel_row == null) || (mobitel_row.GetdataviewRows().Length == 0))
                return;

            curMrow = mobitel_row;
            vehicleInfo = new VehicleInfo(mobitel_row);
            string nameSeries = vehicleInfo.RegistrationNumber + vehicleInfo.CarMaker + vehicleInfo.CarModel;
            ReportsControl.GraphClearSeries();
            Graph.ClearRegion();
            buildGraph.AddGraphFuel(Graph, dataset, mobitel_row);
            Graph.ShowSeries(nameSeries);
            ReportsControl.ShowGraph(curMrow);
        } // SelectGraphic

        // формирование отчета по текущей машине
        protected override void ExportToExcelDevExpress()
        {
            XtraGridService.SetupGidViewForPrint(gvFuelCtrlDutDay, true, true);

            TInfoDut t_info = new TInfoDut();

            t_info.periodBeging = Algorithm.Period.Begin;
            t_info.periodEnd = Algorithm.Period.End;

            atlantaDataSet.mobitelsRow mobitel = dataset.mobitels.FindByMobitel_ID(curMrow.Mobitel_ID);
            VehicleInfo info = new VehicleInfo(mobitel);
           
            t_info.infoVehicle = info.Info; // Транспорт
            t_info.infoDriverName = info.DriverFullName; // Водитель транспорта
            t_info.totalWay = _summariesDutDay[curMrow.Mobitel_ID].Distance; // Общий пройденный путь
            t_info.totalTimerTour = _summariesDutDay[curMrow.Mobitel_ID].TimeTotalWork; // Общая продолжительность смен
            t_info.totalTimeWay = _summariesDutDay[curMrow.Mobitel_ID].TimeTotalMove; // Общее время в движении , ч
            t_info.totalStops = _summariesDutDay[curMrow.Mobitel_ID].TimeTotalWork.Subtract(_summariesDutDay[curMrow.Mobitel_ID].TimeTotalMove); // Общее время стоянок , ч
            t_info.totalEngineOn = _summariesDutDay[curMrow.Mobitel_ID].TimeTotalWithEngineOnHour; // Общее время работы со включенным двигателем, ч
            t_info.totalFuel = _summariesDutDay[curMrow.Mobitel_ID].TotalDebit; // Общий расход топлива, л
            t_info.totalFuelAdd = _summariesDutDay[curMrow.Mobitel_ID].TotalFuelAdd; // Всего заправлено, л
            t_info.totalFuelSub = _summariesDutDay[curMrow.Mobitel_ID].TotalFuelSub; // Всего слито, л
            t_info.MiddleFuel = _summariesDutDay[curMrow.Mobitel_ID].Distance == 0 ? 0 : Math.Round((100 * _summariesDutDay[curMrow.Mobitel_ID].TotalDebit / _summariesDutDay[curMrow.Mobitel_ID].Distance), 2); // Средний расход топлива, л/100 км
            t_info.MotoHour = _summariesDutDay[curMrow.Mobitel_ID].TimeTotalWithEngineOnHour.TotalHours == 0 ? 0 : Math.Round((_summariesDutDay[curMrow.Mobitel_ID].TotalDebit / _summariesDutDay[curMrow.Mobitel_ID].TimeTotalWithEngineOnHour.TotalHours), 2); // Средний расход топлива, л/моточас

            ReportingFuelDutDay.AddInfoStructToList(t_info); /* формируем заголовки таблиц отчета */
            ReportingFuelDutDay.CreateAndShowReport(gcFuelCtrlDutDay);
        } // ExportToExcelDevExpress

        // Формирует данные для отчета по всем активным машинам
        protected override void ExportAllDevToReport()
        {
            foreach (int mobitelId in _summariesDutDay.Keys)
            {
                TInfoDut t_info = new TInfoDut();
                atlantaDataSet.mobitelsRow mobitel = dataset.mobitels.FindByMobitel_ID(mobitelId);
                VehicleInfo info = new VehicleInfo(mobitel);
                t_info.periodBeging = Algorithm.Period.Begin;
                t_info.periodEnd = Algorithm.Period.End;
                t_info.infoVehicle = info.Info; // Транспорт
                t_info.infoDriverName = info.DriverFullName; // Водитель транспорта
                t_info.totalWay = _summariesDutDay[mobitelId].Distance; // Общий пройденный путь
                t_info.totalTimerTour = _summariesDutDay[mobitelId].TimeTotalWork; // Общая продолжительность смен
                t_info.totalTimeWay = _summariesDutDay[mobitelId].TimeTotalMove; // Общее время в движении , ч
                t_info.totalStops = _summariesDutDay[mobitelId].TimeTotalWork.Subtract(_summariesDutDay[mobitelId].TimeTotalMove); // Общее время стоянок , ч
                t_info.totalEngineOn = _summariesDutDay[mobitelId].TimeTotalWithEngineOnHour; // Общее время работы со включенным двигателем, ч
                t_info.totalFuel = _summariesDutDay[mobitelId].TotalDebit; // Общий расход топлива, л
                t_info.totalFuelAdd = _summariesDutDay[mobitelId].TotalFuelAdd; // Всего заправлено, л
                t_info.totalFuelSub = _summariesDutDay[mobitelId].TotalFuelSub; // Всего слито, л
                t_info.MiddleFuel = _summariesDutDay[mobitelId].Distance == 0 ? 0 : Math.Round((100 * _summariesDutDay[mobitelId].TotalDebit / _summariesDutDay[mobitelId].Distance), 2); // Средний расход топлива, л/100 км
                t_info.MotoHour = _summariesDutDay[mobitelId].TimeTotalWithEngineOnHour.TotalHours == 0 ? 0 : Math.Round((_summariesDutDay[mobitelId].TotalDebit / _summariesDutDay[mobitelId].TimeTotalWithEngineOnHour.TotalHours), 2); // Средний расход топлива, л/моточас

                ReportingFuelDutDay.AddInfoStructToList(t_info);
                ReportingFuelDutDay.CreateBindDataList();

                foreach (FuelExpensesDataSet.FuelExpensesDUT_DayRow row in feDataSet.FuelExpensesDUT_Day.Select("Mobitel_Id=" + mobitelId, " Id ASC"))
                {
                    string date = row.InitialTime.ToString("dd.MM.yyyy");
                    string iniTime = row.InitialTime.ToString("t");
                    string finTime = row.FinalTime.ToString("t");
                    string intervWork = row.IntervalWork;
                    string intervMove = row.IntervalMove;
                    string intervStops = row.IntervalStop;
                    string timeEngineOn = (row["TimeWithEngineOn"] != DBNull.Value) ? row.TimeWithEngineOn : " - ";
                    double distance = Math.Round(row.Distance, 2);
                    double fuel_begin = Math.Round(row.FuelBegin, 2);
                    double fuel_end = Math.Round(row.FuelEnd, 2);
                    double total_fuel_add = Math.Round(row.TotalFuelAdd, 2);
                    double total_fuel_sub = Math.Round(row.TotalFuelSub, 2);
                    double total_fuel_use = Math.Round(row.TotalFuelUse, 2);
                    double fuel_use_middle_motion = (row["FuelUseMiddleInMotion"] != DBNull.Value) ? Math.Round(row.FuelUseMiddleInMotion, 2) : 0;
                    double idle_fuel_rate = (row["idleFuelRate"] != DBNull.Value) ? row.idleFuelRate : 0; 

                    // сохранить данные таблицы отчета в списке таблиц
                    ReportingFuelDutDay.AddDataToBindList(new repFuelDutDay(date, iniTime, finTime, intervWork,
                        intervMove, intervStops, timeEngineOn, distance, fuel_begin, fuel_end, total_fuel_add,
                        total_fuel_sub, total_fuel_use, fuel_use_middle_motion, idle_fuel_rate));
                } // foreach

                ReportingFuelDutDay.CreateElementReport();
            } // foreach

            ReportingFuelDutDay.CreateAndShowReport();
            ReportingFuelDutDay.DeleteData();
        } // ExportAllDevToReport

        // формирование колонтитулов верхних отчета
        private void composLink_CreateMarginalHeaderArea(object sender, CreateAreaEventArgs e)
        {
            DevExpressReportHeader(Resources.TotalFuelExpensesDutDayReportName, e);
            TInfoDut info = ReportingFuelDutDay.GetInfoStructure;
            string strPeriod = Resources.PeriodFrom + " " + info.periodBeging + " " +
                Resources.PeriodTo + " " + info.periodEnd;
            DevExpressReportSubHeader(strPeriod, 22, e);
        }

        /* функция для формирования левой части заголовка отчета */
        protected string GetStringBreackLeft()
        {
            TInfoDut info = ReportingFuelDutDay.GetInfoStructure;
            ReportingFuelDutDay.SetRectangleBrckLetf(0, 0, 300, 85);
            return (Resources.Vehicle + ": " + info.infoVehicle + "\n" +
                Resources.Driver + ": " + info.infoDriverName);
        }

        /* функция для формирования верхней/средней части заголовка отчета*/
        protected string GetStringBreackUp()
        {
            TInfoDut info = ReportingFuelDutDay.GetInfoStructure;
            ReportingFuelDutDay.SetRectangleBrckUP(380, 0, 320, 85);
            return (Resources.FuelExControlTravel + ": " + String.Format("{0:f2}", info.totalWay) + "\n" +
                Resources.FuelExControlPeriodWork + ": " + info.totalTimerTour + "\n" +
                Resources.FuelExControlTotalTimeMove + ": " + info.totalTimeWay + "\n" +
                Resources.FuelExControDutDayTimeStops + ": " + info.totalStops + "\n" +
                Resources.FuelExControlTimeEngineOn + ": " + info.totalEngineOn + "\n");
        }

        /* функция для формирования правой части заголовка отчета */
        protected string GetStringBreackRight()
        {
            TInfoDut info = ReportingFuelDutDay.GetInfoStructure;
            ReportingFuelDutDay.SetRectangleBrckRight(770, 0, 300, 85);
            return (Resources.FuelExControlCommonFuelrate + ": " + String.Format("{0:f2}", info.totalFuelAdd) + "\n" +
                Resources.FuelExControlDutCommonFuelAdd + ": " + String.Format("{0:f2}", info.totalFuelAdd) + "\n" +
                Resources.FuelExControlDutCommonFuelPourOff + ": " + String.Format("{0:f2}", info.totalFuelSub) + "\n" +
                Resources.FuelExControlDutDayFuelrate + ": " + String.Format("{0:f2}", info.MiddleFuel) + "\n" +
                Resources.FuelExControDutDayFuelrateOnTime + ": " + String.Format("{0:f2}", info.MotoHour));
        } // GetStringBreackRight
        
        protected override void barButtonGroupPanel_ItemClick(object sender, ItemClickEventArgs e)
        {
            GroupPanel(gvFuelCtrlDutDay);
        }
        
        protected override void barButtonFooterPanel_ItemClick(object sender, ItemClickEventArgs e)
        {
            FooterPanel(gvFuelCtrlDutDay);
        }
        
        protected override void barButtonNavigator_ItemClick(object sender, ItemClickEventArgs e)
        {
            NavigatorPanel(gcFuelCtrlDutDay);
        }

        protected override void barButtonStatusPanel_ItemClick(object sender, ItemClickEventArgs e)
        {
            StatusBar(bar3);
        }

        private void Localization()
        {
            colInitialTime.Caption = Resources.FuelExControDutDayDate;
            colInitialTime.ToolTip = Resources.FuelExControDutDayDate;

            colBeginMotionTime.Caption = Resources.FuelExControDutDayTimeBeginMoving;
            colBeginMotionTime.ToolTip = Resources.FuelExControDutDayTimeBeginMoving;

            colEndMotionTime.Caption = Resources.FuelExControDutDayTimeEndMoving;
            colEndMotionTime.ToolTip = Resources.FuelExControDutDayTimeEndMoving;

            colIntervalWork.Caption = Resources.FuelExControDutDayTimeWorkingShift;
            colIntervalWork.ToolTip = Resources.FuelExControDutDayTimeWorkingShift;

            colIntervalMove.Caption = Resources.FuelExControlTimeMove;
            colIntervalMove.ToolTip = Resources.FuelExControlTimeMove;

            colIntervalStop.Caption = Resources.FuelExControDutDayTimeStops;
            colIntervalStop.ToolTip = Resources.FuelExControDutDayTimeStops;

            colTimeWithEngineOn.Caption = Resources.FuelExControDutDayTimeWorkEngine;
            colTimeWithEngineOn.ToolTip = Resources.FuelExControDutDayTimeWorkEngine;

            colDistance.Caption = Resources.FuelExControlTravel;
            colDistance.ToolTip = Resources.FuelExControlTravel;

            colFuelBegin.Caption = Resources.FuelExControDutDayFuelInBegin;
            colFuelBegin.ToolTip = Resources.FuelExControDutDayFuelInBegin;

            colFuelEnd.Caption = Resources.FuelExControDutDayFuelInEnd;
            colFuelEnd.ToolTip = Resources.FuelExControDutDayFuelInEnd;

            colTotalFuelAdd.Caption = Resources.FuelExControDutDayFuelAdds;
            colTotalFuelAdd.ToolTip = Resources.FuelExControDutDayFuelAdds;

            colTotalFuelSub.Caption = Resources.FuelExControDutDayFuelPoursOff;
            colTotalFuelSub.ToolTip = Resources.FuelExControDutDayFuelPoursOff;

            colTotalFuelUse.Caption = Resources.FuelExControlCommonFuelrate;
            colTotalFuelUse.ToolTip = Resources.FuelExControlCommonFuelrate;

            colFuelUseMiddleInMotion.Caption = Resources.FuelExControDutDayFuelrate;
            colFuelUseMiddleInMotion.ToolTip = Resources.FuelExControDutDayFuelrate;

            colIdleFuelRate.Caption = Resources.FuelExControDutDayFuelOnTime;
            colIdleFuelRate.ToolTip = Resources.FuelExControDutDayFuelOnTime;

            lblTotalDebitText.Caption = Resources.FuelExControlCommonFuelrate + ": ---";
            lblTotalFuelAddText.Caption = Resources.FuelExControlDutCommonFuelAdd + ": ---";
            lblTotalFuelSubText.Caption = Resources.FuelExControlDutCommonFuelPourOff + ": ---";
            lblAvgFuelDebitText.Caption = Resources.FuelExControlDutDayFuelrate + ": ---";
            lblTotalMotoText.Caption = Resources.FuelExControDutDayFuelrateOnTime + ": ---";
        } // Localization
    } // DevFuelExpensesCtrlDutDay
} // ReportsOnFuelExpenses
