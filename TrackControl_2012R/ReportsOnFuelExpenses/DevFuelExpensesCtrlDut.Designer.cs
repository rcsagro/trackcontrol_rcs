﻿namespace ReportsOnFuelExpenses
{
    partial class DevFuelExpensesCtrlDut
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager( typeof( DevFuelExpensesCtrlDut ) );
            this.gcFuelControlDut = new DevExpress.XtraGrid.GridControl();
            this.gvFuelControlDut = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colNameCar = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFIO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTravel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFuelBegin = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFuelEnd = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFuelAddCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFuelSubCount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalFuelAdd = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalFuelSub = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalFuelUse = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFuelUseAvg = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLitrePerHour = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimeMotion = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colTimeStopWithEngineOn = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimeStopWithEngineOff = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMaxSpeed = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.fuelExpensesBindingSource = new System.Windows.Forms.BindingSource( this.components );
            this.fuelExpensesDataSet = new ReportsOnFuelExpenses.FuelExpensesDataSet();
            this.atlantaDataSetBindingSource = new System.Windows.Forms.BindingSource( this.components );
            this.printingSystem1 = new DevExpress.XtraPrinting.PrintingSystem( this.components );
            this.compositeReportLink = new DevExpress.XtraPrintingLinks.CompositeLink( this.components );
            ( ( System.ComponentModel.ISupportInitialize ) ( this.gcFuelControlDut ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.gvFuelControlDut ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.repositoryItemTextEdit1 ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.gridView2 ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.fuelExpensesBindingSource ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.fuelExpensesDataSet ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.atlantaDataSetBindingSource ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.printingSystem1 ) ).BeginInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.compositeReportLink.ImageCollection ) ).BeginInit();
            this.SuspendLayout();
            // 
            // gcFuelControlDut
            // 
            this.gcFuelControlDut.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcFuelControlDut.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gcFuelControlDut.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gcFuelControlDut.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gcFuelControlDut.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gcFuelControlDut.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gcFuelControlDut.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gcFuelControlDut.Location = new System.Drawing.Point( 0, 26 );
            this.gcFuelControlDut.MainView = this.gvFuelControlDut;
            this.gcFuelControlDut.Name = "gcFuelControlDut";
            this.gcFuelControlDut.RepositoryItems.AddRange( new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit1} );
            this.gcFuelControlDut.Size = new System.Drawing.Size( 827, 453 );
            this.gcFuelControlDut.TabIndex = 10;
            this.gcFuelControlDut.UseEmbeddedNavigator = true;
            this.gcFuelControlDut.ViewCollection.AddRange( new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvFuelControlDut,
            this.gridView2} );
            // 
            // gvFuelControlDut
            // 
            this.gvFuelControlDut.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.DarkGray;
            this.gvFuelControlDut.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.DarkGray;
            this.gvFuelControlDut.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.DimGray;
            this.gvFuelControlDut.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvFuelControlDut.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvFuelControlDut.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvFuelControlDut.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.DarkGray;
            this.gvFuelControlDut.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.DarkGray;
            this.gvFuelControlDut.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Gainsboro;
            this.gvFuelControlDut.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvFuelControlDut.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvFuelControlDut.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvFuelControlDut.Appearance.Empty.BackColor = System.Drawing.Color.DimGray;
            this.gvFuelControlDut.Appearance.Empty.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.gvFuelControlDut.Appearance.Empty.Options.UseBackColor = true;
            this.gvFuelControlDut.Appearance.EvenRow.BackColor = System.Drawing.Color.White;
            this.gvFuelControlDut.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvFuelControlDut.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.Gray;
            this.gvFuelControlDut.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.Gray;
            this.gvFuelControlDut.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvFuelControlDut.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvFuelControlDut.Appearance.FilterPanel.BackColor = System.Drawing.Color.Gray;
            this.gvFuelControlDut.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvFuelControlDut.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvFuelControlDut.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvFuelControlDut.Appearance.FocusedRow.BackColor = System.Drawing.Color.LightSlateGray;
            this.gvFuelControlDut.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gvFuelControlDut.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvFuelControlDut.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvFuelControlDut.Appearance.FooterPanel.BackColor = System.Drawing.Color.DarkGray;
            this.gvFuelControlDut.Appearance.FooterPanel.BorderColor = System.Drawing.Color.DarkGray;
            this.gvFuelControlDut.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvFuelControlDut.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvFuelControlDut.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.gvFuelControlDut.Appearance.FooterPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gvFuelControlDut.Appearance.FooterPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gvFuelControlDut.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.gvFuelControlDut.Appearance.GroupButton.BackColor = System.Drawing.Color.Silver;
            this.gvFuelControlDut.Appearance.GroupButton.BorderColor = System.Drawing.Color.Silver;
            this.gvFuelControlDut.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvFuelControlDut.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvFuelControlDut.Appearance.GroupFooter.BackColor = System.Drawing.Color.Silver;
            this.gvFuelControlDut.Appearance.GroupFooter.BorderColor = System.Drawing.Color.Silver;
            this.gvFuelControlDut.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvFuelControlDut.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvFuelControlDut.Appearance.GroupPanel.BackColor = System.Drawing.Color.DimGray;
            this.gvFuelControlDut.Appearance.GroupPanel.ForeColor = System.Drawing.Color.White;
            this.gvFuelControlDut.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvFuelControlDut.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvFuelControlDut.Appearance.GroupRow.BackColor = System.Drawing.Color.Silver;
            this.gvFuelControlDut.Appearance.GroupRow.Font = new System.Drawing.Font( "Tahoma", 8F, System.Drawing.FontStyle.Bold );
            this.gvFuelControlDut.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvFuelControlDut.Appearance.GroupRow.Options.UseFont = true;
            this.gvFuelControlDut.Appearance.HeaderPanel.BackColor = System.Drawing.Color.DarkGray;
            this.gvFuelControlDut.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.DarkGray;
            this.gvFuelControlDut.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvFuelControlDut.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvFuelControlDut.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightSlateGray;
            this.gvFuelControlDut.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvFuelControlDut.Appearance.HorzLine.BackColor = System.Drawing.Color.LightGray;
            this.gvFuelControlDut.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvFuelControlDut.Appearance.OddRow.BackColor = System.Drawing.Color.WhiteSmoke;
            this.gvFuelControlDut.Appearance.OddRow.Options.UseBackColor = true;
            this.gvFuelControlDut.Appearance.Preview.BackColor = System.Drawing.Color.Gainsboro;
            this.gvFuelControlDut.Appearance.Preview.ForeColor = System.Drawing.Color.DimGray;
            this.gvFuelControlDut.Appearance.Preview.Options.UseBackColor = true;
            this.gvFuelControlDut.Appearance.Preview.Options.UseForeColor = true;
            this.gvFuelControlDut.Appearance.Row.BackColor = System.Drawing.Color.White;
            this.gvFuelControlDut.Appearance.Row.Options.UseBackColor = true;
            this.gvFuelControlDut.Appearance.RowSeparator.BackColor = System.Drawing.Color.DimGray;
            this.gvFuelControlDut.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvFuelControlDut.Appearance.SelectedRow.BackColor = System.Drawing.Color.DimGray;
            this.gvFuelControlDut.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvFuelControlDut.Appearance.VertLine.BackColor = System.Drawing.Color.LightGray;
            this.gvFuelControlDut.Appearance.VertLine.Options.UseBackColor = true;
            this.gvFuelControlDut.AppearancePrint.FooterPanel.Options.UseTextOptions = true;
            this.gvFuelControlDut.AppearancePrint.FooterPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gvFuelControlDut.AppearancePrint.FooterPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gvFuelControlDut.AppearancePrint.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.gvFuelControlDut.BestFitMaxRowCount = 2;
            this.gvFuelControlDut.ColumnPanelRowHeight = 40;
            this.gvFuelControlDut.Columns.AddRange( new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colNameCar,
            this.colFIO,
            this.colTravel,
            this.colFuelBegin,
            this.colFuelEnd,
            this.colFuelAddCount,
            this.colFuelSubCount,
            this.colTotalFuelAdd,
            this.colTotalFuelSub,
            this.colTotalFuelUse,
            this.colFuelUseAvg,
            this.colLitrePerHour,
            this.colTimeMotion,
            this.colTimeStopWithEngineOn,
            this.colTimeStopWithEngineOff,
            this.colMaxSpeed} );
            this.gvFuelControlDut.GridControl = this.gcFuelControlDut;
            this.gvFuelControlDut.GroupSummary.AddRange( new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Distance", this.colTotalFuelSub, "")} );
            this.gvFuelControlDut.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always;
            this.gvFuelControlDut.Name = "gvFuelControlDut";
            this.gvFuelControlDut.OptionsDetail.AllowZoomDetail = false;
            this.gvFuelControlDut.OptionsDetail.EnableMasterViewMode = false;
            this.gvFuelControlDut.OptionsDetail.ShowDetailTabs = false;
            this.gvFuelControlDut.OptionsDetail.SmartDetailExpand = false;
            this.gvFuelControlDut.OptionsSelection.MultiSelect = true;
            this.gvFuelControlDut.OptionsView.ColumnAutoWidth = false;
            this.gvFuelControlDut.OptionsView.EnableAppearanceEvenRow = true;
            this.gvFuelControlDut.OptionsView.EnableAppearanceOddRow = true;
            this.gvFuelControlDut.OptionsView.ShowFooter = true;
            // 
            // colNameCar
            // 
            this.colNameCar.AppearanceCell.Options.UseTextOptions = true;
            this.colNameCar.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colNameCar.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colNameCar.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colNameCar.AppearanceHeader.Options.UseTextOptions = true;
            this.colNameCar.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNameCar.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colNameCar.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colNameCar.Caption = "Название машины";
            this.colNameCar.FieldName = "NameCar";
            this.colNameCar.ImageAlignment = System.Drawing.StringAlignment.Center;
            this.colNameCar.MinWidth = 10;
            this.colNameCar.Name = "colNameCar";
            this.colNameCar.OptionsColumn.AllowEdit = false;
            this.colNameCar.OptionsColumn.AllowFocus = false;
            this.colNameCar.OptionsColumn.FixedWidth = true;
            this.colNameCar.OptionsColumn.ReadOnly = true;
            this.colNameCar.ToolTip = "Название машины";
            this.colNameCar.Visible = true;
            this.colNameCar.VisibleIndex = 0;
            this.colNameCar.Width = 74;
            // 
            // colFIO
            // 
            this.colFIO.AppearanceCell.Options.UseTextOptions = true;
            this.colFIO.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colFIO.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colFIO.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFIO.AppearanceHeader.Options.UseTextOptions = true;
            this.colFIO.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFIO.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colFIO.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFIO.Caption = "ФИО водителя";
            this.colFIO.FieldName = "FIO";
            this.colFIO.MinWidth = 10;
            this.colFIO.Name = "colFIO";
            this.colFIO.OptionsColumn.AllowEdit = false;
            this.colFIO.OptionsColumn.AllowFocus = false;
            this.colFIO.OptionsColumn.FixedWidth = true;
            this.colFIO.OptionsColumn.ReadOnly = true;
            this.colFIO.ToolTip = "ФИО водителя";
            this.colFIO.Visible = true;
            this.colFIO.VisibleIndex = 1;
            this.colFIO.Width = 90;
            // 
            // colTravel
            // 
            this.colTravel.AppearanceCell.Options.UseTextOptions = true;
            this.colTravel.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTravel.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTravel.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTravel.AppearanceHeader.Options.UseTextOptions = true;
            this.colTravel.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTravel.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTravel.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTravel.Caption = "Пройденный путь, км";
            this.colTravel.DisplayFormat.FormatString = "{0:f2}";
            this.colTravel.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTravel.FieldName = "Travel";
            this.colTravel.MinWidth = 10;
            this.colTravel.Name = "colTravel";
            this.colTravel.OptionsColumn.AllowEdit = false;
            this.colTravel.OptionsColumn.AllowFocus = false;
            this.colTravel.OptionsColumn.FixedWidth = true;
            this.colTravel.OptionsColumn.ReadOnly = true;
            this.colTravel.SummaryItem.DisplayFormat = "{0:f2}";
            this.colTravel.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.colTravel.ToolTip = "Пройденный путь, км";
            this.colTravel.Visible = true;
            this.colTravel.VisibleIndex = 2;
            // 
            // colFuelBegin
            // 
            this.colFuelBegin.AppearanceCell.Options.UseTextOptions = true;
            this.colFuelBegin.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelBegin.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colFuelBegin.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFuelBegin.AppearanceHeader.Options.UseTextOptions = true;
            this.colFuelBegin.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelBegin.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colFuelBegin.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFuelBegin.Caption = "Топлива в начале периода, л";
            this.colFuelBegin.DisplayFormat.FormatString = "N2";
            this.colFuelBegin.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colFuelBegin.FieldName = "FuelBegin";
            this.colFuelBegin.MinWidth = 10;
            this.colFuelBegin.Name = "colFuelBegin";
            this.colFuelBegin.OptionsColumn.AllowEdit = false;
            this.colFuelBegin.OptionsColumn.AllowFocus = false;
            this.colFuelBegin.OptionsColumn.FixedWidth = true;
            this.colFuelBegin.OptionsColumn.ReadOnly = true;
            this.colFuelBegin.SummaryItem.DisplayFormat = "{0:f2}";
            this.colFuelBegin.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.colFuelBegin.ToolTip = "Топлива в начале периода, л";
            this.colFuelBegin.Visible = true;
            this.colFuelBegin.VisibleIndex = 3;
            // 
            // colFuelEnd
            // 
            this.colFuelEnd.AppearanceCell.Options.UseTextOptions = true;
            this.colFuelEnd.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelEnd.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colFuelEnd.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFuelEnd.AppearanceHeader.Options.UseTextOptions = true;
            this.colFuelEnd.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelEnd.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colFuelEnd.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFuelEnd.Caption = "Топлива в конце периода, л";
            this.colFuelEnd.DisplayFormat.FormatString = "N2";
            this.colFuelEnd.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colFuelEnd.FieldName = "FuelEnd";
            this.colFuelEnd.MinWidth = 10;
            this.colFuelEnd.Name = "colFuelEnd";
            this.colFuelEnd.OptionsColumn.AllowEdit = false;
            this.colFuelEnd.OptionsColumn.AllowFocus = false;
            this.colFuelEnd.OptionsColumn.FixedWidth = true;
            this.colFuelEnd.OptionsColumn.ReadOnly = true;
            this.colFuelEnd.SummaryItem.DisplayFormat = "{0:f2}";
            this.colFuelEnd.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.colFuelEnd.ToolTip = "Топлива в конце периода, л";
            this.colFuelEnd.Visible = true;
            this.colFuelEnd.VisibleIndex = 4;
            // 
            // colFuelAddCount
            // 
            this.colFuelAddCount.AppearanceCell.Options.UseTextOptions = true;
            this.colFuelAddCount.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelAddCount.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colFuelAddCount.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFuelAddCount.AppearanceHeader.Options.UseTextOptions = true;
            this.colFuelAddCount.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelAddCount.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colFuelAddCount.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFuelAddCount.Caption = "Количество заправок";
            this.colFuelAddCount.FieldName = "FuelAddCount";
            this.colFuelAddCount.MinWidth = 10;
            this.colFuelAddCount.Name = "colFuelAddCount";
            this.colFuelAddCount.OptionsColumn.AllowEdit = false;
            this.colFuelAddCount.OptionsColumn.AllowFocus = false;
            this.colFuelAddCount.OptionsColumn.FixedWidth = true;
            this.colFuelAddCount.OptionsColumn.ReadOnly = true;
            this.colFuelAddCount.SummaryItem.DisplayFormat = "{0:f1}";
            this.colFuelAddCount.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.colFuelAddCount.ToolTip = "Количество заправок";
            this.colFuelAddCount.Visible = true;
            this.colFuelAddCount.VisibleIndex = 5;
            this.colFuelAddCount.Width = 58;
            // 
            // colFuelSubCount
            // 
            this.colFuelSubCount.AppearanceCell.Options.UseTextOptions = true;
            this.colFuelSubCount.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelSubCount.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colFuelSubCount.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFuelSubCount.AppearanceHeader.Options.UseTextOptions = true;
            this.colFuelSubCount.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelSubCount.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colFuelSubCount.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFuelSubCount.Caption = "Количество сливов";
            this.colFuelSubCount.FieldName = "FuelSubCount";
            this.colFuelSubCount.MinWidth = 10;
            this.colFuelSubCount.Name = "colFuelSubCount";
            this.colFuelSubCount.OptionsColumn.AllowEdit = false;
            this.colFuelSubCount.OptionsColumn.AllowFocus = false;
            this.colFuelSubCount.OptionsColumn.FixedWidth = true;
            this.colFuelSubCount.OptionsColumn.ReadOnly = true;
            this.colFuelSubCount.SummaryItem.DisplayFormat = "{0:f1}";
            this.colFuelSubCount.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.colFuelSubCount.ToolTip = "Количество сливов";
            this.colFuelSubCount.Visible = true;
            this.colFuelSubCount.VisibleIndex = 6;
            this.colFuelSubCount.Width = 64;
            // 
            // colTotalFuelAdd
            // 
            this.colTotalFuelAdd.AppearanceCell.Options.UseTextOptions = true;
            this.colTotalFuelAdd.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTotalFuelAdd.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTotalFuelAdd.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTotalFuelAdd.AppearanceHeader.Options.UseTextOptions = true;
            this.colTotalFuelAdd.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTotalFuelAdd.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTotalFuelAdd.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTotalFuelAdd.Caption = "Всего заправлено, л";
            this.colTotalFuelAdd.DisplayFormat.FormatString = "N2";
            this.colTotalFuelAdd.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTotalFuelAdd.FieldName = "TotalFuelAdd";
            this.colTotalFuelAdd.MinWidth = 10;
            this.colTotalFuelAdd.Name = "colTotalFuelAdd";
            this.colTotalFuelAdd.OptionsColumn.AllowEdit = false;
            this.colTotalFuelAdd.OptionsColumn.AllowFocus = false;
            this.colTotalFuelAdd.OptionsColumn.FixedWidth = true;
            this.colTotalFuelAdd.OptionsColumn.ReadOnly = true;
            this.colTotalFuelAdd.SummaryItem.DisplayFormat = "{0:f2}";
            this.colTotalFuelAdd.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.colTotalFuelAdd.ToolTip = "Общее время остановок во время смены";
            this.colTotalFuelAdd.Visible = true;
            this.colTotalFuelAdd.VisibleIndex = 7;
            this.colTotalFuelAdd.Width = 54;
            // 
            // colTotalFuelSub
            // 
            this.colTotalFuelSub.AppearanceCell.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.colTotalFuelSub.AppearanceCell.Options.UseTextOptions = true;
            this.colTotalFuelSub.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTotalFuelSub.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTotalFuelSub.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTotalFuelSub.AppearanceHeader.Options.UseTextOptions = true;
            this.colTotalFuelSub.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTotalFuelSub.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTotalFuelSub.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTotalFuelSub.Caption = "Всего слито, л";
            this.colTotalFuelSub.DisplayFormat.FormatString = "N2";
            this.colTotalFuelSub.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTotalFuelSub.FieldName = "TotalFuelSub";
            this.colTotalFuelSub.MinWidth = 10;
            this.colTotalFuelSub.Name = "colTotalFuelSub";
            this.colTotalFuelSub.OptionsColumn.AllowEdit = false;
            this.colTotalFuelSub.OptionsColumn.AllowFocus = false;
            this.colTotalFuelSub.OptionsColumn.FixedWidth = true;
            this.colTotalFuelSub.OptionsColumn.ReadOnly = true;
            this.colTotalFuelSub.SummaryItem.DisplayFormat = "{0:f2}";
            this.colTotalFuelSub.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.colTotalFuelSub.ToolTip = "Всего слито, л";
            this.colTotalFuelSub.Visible = true;
            this.colTotalFuelSub.VisibleIndex = 8;
            this.colTotalFuelSub.Width = 52;
            // 
            // colTotalFuelUse
            // 
            this.colTotalFuelUse.AppearanceCell.Options.UseTextOptions = true;
            this.colTotalFuelUse.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTotalFuelUse.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTotalFuelUse.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTotalFuelUse.AppearanceHeader.Options.UseTextOptions = true;
            this.colTotalFuelUse.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTotalFuelUse.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTotalFuelUse.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTotalFuelUse.Caption = "Общий расход, л";
            this.colTotalFuelUse.DisplayFormat.FormatString = "N2";
            this.colTotalFuelUse.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTotalFuelUse.FieldName = "TotalFuelUse";
            this.colTotalFuelUse.MinWidth = 10;
            this.colTotalFuelUse.Name = "colTotalFuelUse";
            this.colTotalFuelUse.OptionsColumn.AllowEdit = false;
            this.colTotalFuelUse.OptionsColumn.AllowFocus = false;
            this.colTotalFuelUse.OptionsColumn.FixedWidth = true;
            this.colTotalFuelUse.OptionsColumn.ReadOnly = true;
            this.colTotalFuelUse.SummaryItem.DisplayFormat = "{0:f2}";
            this.colTotalFuelUse.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.colTotalFuelUse.ToolTip = "Общий расход, л";
            this.colTotalFuelUse.Visible = true;
            this.colTotalFuelUse.VisibleIndex = 9;
            this.colTotalFuelUse.Width = 55;
            // 
            // colFuelUseAvg
            // 
            this.colFuelUseAvg.AppearanceCell.Options.UseTextOptions = true;
            this.colFuelUseAvg.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelUseAvg.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colFuelUseAvg.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFuelUseAvg.AppearanceHeader.Options.UseTextOptions = true;
            this.colFuelUseAvg.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelUseAvg.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colFuelUseAvg.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFuelUseAvg.Caption = "Средний расход в движении, л/100 км";
            this.colFuelUseAvg.DisplayFormat.FormatString = "N2";
            this.colFuelUseAvg.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colFuelUseAvg.FieldName = "FuelUseAvg";
            this.colFuelUseAvg.MinWidth = 10;
            this.colFuelUseAvg.Name = "colFuelUseAvg";
            this.colFuelUseAvg.OptionsColumn.AllowEdit = false;
            this.colFuelUseAvg.OptionsColumn.AllowFocus = false;
            this.colFuelUseAvg.OptionsColumn.FixedWidth = true;
            this.colFuelUseAvg.OptionsColumn.ReadOnly = true;
            this.colFuelUseAvg.Visible = true;
            this.colFuelUseAvg.VisibleIndex = 10;
            this.colFuelUseAvg.Width = 48;
            // 
            // colLitrePerHour
            // 
            this.colLitrePerHour.AppearanceCell.Options.UseTextOptions = true;
            this.colLitrePerHour.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLitrePerHour.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colLitrePerHour.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colLitrePerHour.AppearanceHeader.Options.UseTextOptions = true;
            this.colLitrePerHour.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLitrePerHour.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colLitrePerHour.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colLitrePerHour.Caption = "Средний расход, л/моточас";
            this.colLitrePerHour.DisplayFormat.FormatString = "N2";
            this.colLitrePerHour.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colLitrePerHour.FieldName = "LitrePerHour";
            this.colLitrePerHour.MinWidth = 10;
            this.colLitrePerHour.Name = "colLitrePerHour";
            this.colLitrePerHour.OptionsColumn.AllowEdit = false;
            this.colLitrePerHour.OptionsColumn.AllowFocus = false;
            this.colLitrePerHour.OptionsColumn.FixedWidth = true;
            this.colLitrePerHour.OptionsColumn.ReadOnly = true;
            this.colLitrePerHour.ToolTip = "Средний расход, л/моточас";
            this.colLitrePerHour.Visible = true;
            this.colLitrePerHour.VisibleIndex = 11;
            this.colLitrePerHour.Width = 57;
            // 
            // colTimeMotion
            // 
            this.colTimeMotion.AppearanceCell.Options.UseTextOptions = true;
            this.colTimeMotion.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeMotion.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTimeMotion.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTimeMotion.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeMotion.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeMotion.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTimeMotion.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTimeMotion.Caption = "Время в движении, ч";
            this.colTimeMotion.ColumnEdit = this.repositoryItemTextEdit1;
            this.colTimeMotion.DisplayFormat.FormatString = "N2";
            this.colTimeMotion.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTimeMotion.FieldName = "TimeMotion";
            this.colTimeMotion.MinWidth = 10;
            this.colTimeMotion.Name = "colTimeMotion";
            this.colTimeMotion.OptionsColumn.AllowEdit = false;
            this.colTimeMotion.OptionsColumn.AllowFocus = false;
            this.colTimeMotion.OptionsColumn.FixedWidth = true;
            this.colTimeMotion.OptionsColumn.ReadOnly = true;
            this.colTimeMotion.ToolTip = "Время в движении, ч";
            this.colTimeMotion.Visible = true;
            this.colTimeMotion.VisibleIndex = 12;
            this.colTimeMotion.Width = 58;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.ExportMode = DevExpress.XtraEditors.Repository.ExportMode.DisplayText;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // colTimeStopWithEngineOn
            // 
            this.colTimeStopWithEngineOn.AppearanceCell.Options.UseTextOptions = true;
            this.colTimeStopWithEngineOn.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeStopWithEngineOn.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTimeStopWithEngineOn.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTimeStopWithEngineOn.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeStopWithEngineOn.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeStopWithEngineOn.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTimeStopWithEngineOn.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTimeStopWithEngineOn.Caption = "Время стоянок с включенным двигателем, ч";
            this.colTimeStopWithEngineOn.ColumnEdit = this.repositoryItemTextEdit1;
            this.colTimeStopWithEngineOn.FieldName = "TimeStopWithEngineOn";
            this.colTimeStopWithEngineOn.MinWidth = 10;
            this.colTimeStopWithEngineOn.Name = "colTimeStopWithEngineOn";
            this.colTimeStopWithEngineOn.OptionsColumn.AllowEdit = false;
            this.colTimeStopWithEngineOn.OptionsColumn.AllowFocus = false;
            this.colTimeStopWithEngineOn.OptionsColumn.FixedWidth = true;
            this.colTimeStopWithEngineOn.OptionsColumn.ReadOnly = true;
            this.colTimeStopWithEngineOn.ToolTip = "Время стоянок с включенным двигателем, ч";
            this.colTimeStopWithEngineOn.Visible = true;
            this.colTimeStopWithEngineOn.VisibleIndex = 13;
            this.colTimeStopWithEngineOn.Width = 82;
            // 
            // colTimeStopWithEngineOff
            // 
            this.colTimeStopWithEngineOff.AppearanceCell.Options.UseTextOptions = true;
            this.colTimeStopWithEngineOff.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeStopWithEngineOff.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTimeStopWithEngineOff.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTimeStopWithEngineOff.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeStopWithEngineOff.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeStopWithEngineOff.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTimeStopWithEngineOff.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTimeStopWithEngineOff.Caption = "Время стоянок с выключенным двигателем, ч";
            this.colTimeStopWithEngineOff.ColumnEdit = this.repositoryItemTextEdit1;
            this.colTimeStopWithEngineOff.FieldName = "TimeStopWithEngineOff";
            this.colTimeStopWithEngineOff.MinWidth = 10;
            this.colTimeStopWithEngineOff.Name = "colTimeStopWithEngineOff";
            this.colTimeStopWithEngineOff.OptionsColumn.AllowEdit = false;
            this.colTimeStopWithEngineOff.OptionsColumn.AllowFocus = false;
            this.colTimeStopWithEngineOff.OptionsColumn.FixedWidth = true;
            this.colTimeStopWithEngineOff.OptionsColumn.ReadOnly = true;
            this.colTimeStopWithEngineOff.ToolTip = "Время стоянок с выключенным двигателем, ч";
            this.colTimeStopWithEngineOff.Visible = true;
            this.colTimeStopWithEngineOff.VisibleIndex = 14;
            this.colTimeStopWithEngineOff.Width = 239;
            // 
            // colMaxSpeed
            // 
            this.colMaxSpeed.AppearanceCell.Options.UseTextOptions = true;
            this.colMaxSpeed.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMaxSpeed.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colMaxSpeed.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colMaxSpeed.AppearanceHeader.Options.UseTextOptions = true;
            this.colMaxSpeed.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMaxSpeed.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colMaxSpeed.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colMaxSpeed.Caption = "Максимальная скорость, км/ч";
            this.colMaxSpeed.DisplayFormat.FormatString = "N2";
            this.colMaxSpeed.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colMaxSpeed.FieldName = "MaxSpeed";
            this.colMaxSpeed.MinWidth = 10;
            this.colMaxSpeed.Name = "colMaxSpeed";
            this.colMaxSpeed.OptionsColumn.AllowEdit = false;
            this.colMaxSpeed.OptionsColumn.AllowFocus = false;
            this.colMaxSpeed.OptionsColumn.FixedWidth = true;
            this.colMaxSpeed.OptionsColumn.ReadOnly = true;
            this.colMaxSpeed.ToolTip = "Максимальная скорость, км/ч";
            this.colMaxSpeed.Visible = true;
            this.colMaxSpeed.VisibleIndex = 15;
            this.colMaxSpeed.Width = 186;
            // 
            // gridView2
            // 
            this.gridView2.GridControl = this.gcFuelControlDut;
            this.gridView2.Name = "gridView2";
            // 
            // fuelExpensesBindingSource
            // 
            this.fuelExpensesBindingSource.AllowNew = true;
            this.fuelExpensesBindingSource.DataMember = "FuelExpensesDUT_Total";
            this.fuelExpensesBindingSource.DataSource = this.fuelExpensesDataSet;
            // 
            // fuelExpensesDataSet
            // 
            this.fuelExpensesDataSet.DataSetName = "FuelExpensesDut_Total";
            this.fuelExpensesDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // atlantaDataSetBindingSource
            // 
            this.atlantaDataSetBindingSource.AllowNew = true;
            this.atlantaDataSetBindingSource.DataSource = this.fuelExpensesDataSet;
            this.atlantaDataSetBindingSource.Position = 0;
            // 
            // printingSystem1
            // 
            this.printingSystem1.ExportOptions.Html.ExportMode = DevExpress.XtraPrinting.HtmlExportMode.SingleFilePageByPage;
            this.printingSystem1.ExportOptions.Image.ExportMode = DevExpress.XtraPrinting.ImageExportMode.SingleFilePageByPage;
            this.printingSystem1.ExportOptions.Mht.ExportMode = DevExpress.XtraPrinting.HtmlExportMode.SingleFilePageByPage;
            this.printingSystem1.ExportOptions.Xls.ShowGridLines = true;
            this.printingSystem1.ExportOptions.Xlsx.ShowGridLines = true;
            this.printingSystem1.Links.AddRange( new object[] {
            this.compositeReportLink} );
            // 
            // compositeReportLink
            // 
            // 
            // 
            // 
            this.compositeReportLink.ImageCollection.ImageStream = ( ( DevExpress.Utils.ImageCollectionStreamer ) ( resources.GetObject( "compositeReportLink.ImageCollection.ImageStream" ) ) );
            this.compositeReportLink.Landscape = true;
            this.compositeReportLink.Margins = new System.Drawing.Printing.Margins( 25, 25, 70, 25 );
            this.compositeReportLink.MinMargins = new System.Drawing.Printing.Margins( 25, 25, 15, 25 );
            this.compositeReportLink.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.compositeReportLink.PrintingSystem = this.printingSystem1;
            this.compositeReportLink.PrintingSystemBase = this.printingSystem1;
            // 
            // DevFuelExpensesCtrlDut
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.Controls.Add( this.gcFuelControlDut );
            this.Name = "DevFuelExpensesCtrlDut";
            this.Size = new System.Drawing.Size( 827, 503 );
            this.Controls.SetChildIndex( this.gcFuelControlDut, 0 );
            ( ( System.ComponentModel.ISupportInitialize ) ( this.gcFuelControlDut ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.gvFuelControlDut ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.repositoryItemTextEdit1 ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.gridView2 ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.fuelExpensesBindingSource ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.fuelExpensesDataSet ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.atlantaDataSetBindingSource ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.printingSystem1 ) ).EndInit();
            ( ( System.ComponentModel.ISupportInitialize ) ( this.compositeReportLink.ImageCollection ) ).EndInit();
            this.ResumeLayout( false );

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gcFuelControlDut;
        private DevExpress.XtraGrid.Views.Grid.GridView gvFuelControlDut;
        private DevExpress.XtraGrid.Columns.GridColumn colNameCar;
        private DevExpress.XtraGrid.Columns.GridColumn colFIO;
        private DevExpress.XtraGrid.Columns.GridColumn colTravel;
        private DevExpress.XtraGrid.Columns.GridColumn colFuelBegin;
        private DevExpress.XtraGrid.Columns.GridColumn colFuelEnd;
        private DevExpress.XtraGrid.Columns.GridColumn colFuelAddCount;
        private DevExpress.XtraGrid.Columns.GridColumn colFuelSubCount;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalFuelAdd;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalFuelSub;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalFuelUse;
        private DevExpress.XtraGrid.Columns.GridColumn colFuelUseAvg;
        private DevExpress.XtraGrid.Columns.GridColumn colLitrePerHour;
        private DevExpress.XtraGrid.Columns.GridColumn colTimeMotion;
        private DevExpress.XtraGrid.Columns.GridColumn colTimeStopWithEngineOn;
        private DevExpress.XtraGrid.Columns.GridColumn colTimeStopWithEngineOff;
        private DevExpress.XtraGrid.Columns.GridColumn colMaxSpeed;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private System.Windows.Forms.BindingSource fuelExpensesBindingSource;
        private FuelExpensesDataSet fuelExpensesDataSet;
        protected System.Windows.Forms.BindingSource atlantaDataSetBindingSource;
        private DevExpress.XtraPrinting.PrintingSystem printingSystem1;
        private DevExpress.XtraPrintingLinks.CompositeLink compositeReportLink;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
    }
}
