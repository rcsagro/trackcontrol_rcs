﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using BaseReports.ReportsDE;
using BaseReports.Procedure;
using BaseReports.Properties;
using BaseReports.RFID;
using LocalCache;
using TrackControl.General;
using TrackControl.Reports;
using TrackControl.Vehicles;
using TrackControl.Reports.Graph;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraPrinting;
using DevExpress.XtraPrinting.Drawing;
using DevExpress.XtraExport;
using DevExpress.XtraReports;
using DevExpress.XtraReports.UI;
using DevExpress.XtraPrinting.Native;
using DevExpress.XtraPrinting.Control;
using DevExpress.XtraPrinting.Preview;
using DevExpress.XtraPrintingLinks;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Views;
using DevExpress.XtraGrid.Views.Grid;
using ReportsOnFuelExpenses;
using BaseReports;
using ReportsOnFuelExpenses.Properties;
using Report;

namespace ReportsOnFuelExpenses
{
    public partial class DevFuelExpensesCtrlDut : BaseReports.ReportsDE.BaseControl
    {
        public FuelExpensesDataSet feDataSet; // Датасет
        private float totalTravel; // общий пробег
        private double totalFuelRate; // общий расход топлива
        private double avgFuelRateInMachineHour; // Средний расход на моточас
        private double avgMotionFuelRate; // Средний расход топлива в движении
        private double fuelAdd; // Всего заправлено
        private double fuelSub; // всего слито
        protected VehicleInfo vehicleInfo; // Описание текущей машины
        private static IBuildGraphs buildGraph; // Интерфейс для построение графиков
        protected static atlantaDataSet dataset;
        List<atlantaDataSet.mobitelsRow> listCurMrow;
        ReportClass<TInfo> ReportingFuelDUTTotal;
        
        // Главный конструктор
        public DevFuelExpensesCtrlDut()
        {
            InitializeComponent();
            Localization();
            HideStatusBar();
            DisableButton();
            DisableRun();
            DisableMenuFirstButton();
            VisionPanel(gvFuelControlDut, gcFuelControlDut, null);
            feDataSet = FuelExpensesDataSetCreator.GetDataSet();
            atlantaDataSetBindingSource.DataSource = feDataSet;
            atlantaDataSetBindingSource.DataMember = "FuelExpensesDUT_Total";
            gcFuelControlDut.DataSource = atlantaDataSetBindingSource;
            buildGraph = new BuildGraphs();
            dataset = ReportTabControl.Dataset;
            fuelExpensesDataSet = null;
            AddAlgorithm(new Kilometrage());
            //AddAlgorithm(new Rotate(AlgorithmType.ROTATE_E));
            AddAlgorithm(new Rotation());
            AddAlgorithm(new Fuel(AlgorithmType.FUEL1));
            AddAlgorithm(new TotalFuelExpensesDutAlgorithm());
            listCurMrow = new List<atlantaDataSet.mobitelsRow>();

            gvFuelControlDut.RowClick += new RowClickEventHandler(gvFuelControlDut_ClickRow);

            compositeReportLink.CreateMarginalHeaderArea +=
                new CreateAreaEventHandler(composLink_CreateMarginalHeaderArea);

            ReportingFuelDUTTotal =
                new ReportClass<TInfo>(compositeReportLink);

            ReportingFuelDUTTotal.getStrBrckLeft =
                new ReportClass<TInfo>.GetStrBreackLeft(GetStringBreackLeft);

            ReportingFuelDUTTotal.getStrBrckRight =
                new ReportClass<TInfo>.GetStrBreackRight(GetStringBreackRight);

            ReportingFuelDUTTotal.getStrBrckUp =
                new ReportClass<TInfo>.GetStrBreackUp(GetStringBreackUp);
        } // DevFuelExpensesCtrlDut

        public override string Caption // Заголовок отчета
        {
            get { return Resources.TotalFuelExDutName; }
        }

        public override void Select(atlantaDataSet.mobitelsRow m_row)
        {
            if (m_row != null)
            {
                curMrow = m_row;
                if (m_row.Check)
                {
                    if (atlantaDataSetBindingSource.Count > 0)
                    {
                        gcFuelControlDut.DataSource = atlantaDataSetBindingSource;
                        EnableButton();
                        CalcTotals();
                    } // if
                } // if
                else
                {
                    DisableButton();
                    gcFuelControlDut.DataSource = null;
                } // else

                atlantaDataSetBindingSource.Filter = "Mobitel_ID >= 0";
            } // if
            else
            {
                DisableButton();
                ReportsControl.GraphClearSeries();
                Graph.ClearLabel();
            } // else
        } // Select

        protected double dRoundValue(double x)
        {
            int koeff = 100;

            int number = ( int ) x;
            int parth = (int)(( x - number ) * koeff);
            
            return ((double)((double)number + (double)parth / (double)koeff));
        } // RoundValue

        protected float fRoundValue( float x )
        {
            int koeff = 100;

            int number = ( int ) x;
            int parth = ( int ) ( ( x - number ) * koeff );

            return ( ( float ) ( ( float ) number + ( float ) parth / ( float ) koeff ) );
        } // RoundValue

        // Расчет итогов
        private void CalcTotals()
        {
            totalTravel = 0;
            totalFuelRate = 0;
            avgFuelRateInMachineHour = 0;
            avgMotionFuelRate = 0;
            fuelAdd = 0;
            fuelSub = 0;
            TimeSpan totalTimeEngineOn = new TimeSpan(0);

            foreach (DataRowView dr in atlantaDataSetBindingSource)
            {
                FuelExpensesDataSet.FuelExpensesDUT_TotalRow row =
                    (FuelExpensesDataSet.FuelExpensesDUT_TotalRow)dr.Row;
           
                    // сдесь попробуем округлить
                    row.Travel = fRoundValue( row.Travel );
                    row.FuelBegin = dRoundValue( row.FuelBegin );
                    row.FuelEnd = dRoundValue( row.FuelEnd );
                    row.TotalFuelAdd = dRoundValue( row.TotalFuelAdd );
                    row.TotalFuelSub = dRoundValue( row.TotalFuelSub );
                    row.TotalFuelUse = dRoundValue( row.TotalFuelUse );
                    row.FuelUseAvg = dRoundValue( row.FuelUseAvg );

                    if ( !row.IsLitrePerHourNull() )
                        row.LitrePerHour = fRoundValue( row.LitrePerHour );

                    row.MaxSpeed = fRoundValue( row.MaxSpeed );
                    // сдесь конец округления

                totalTravel += row.Travel;
                totalFuelRate += row.TotalFuelUse;
                avgMotionFuelRate += row.FuelUseAvg;
                fuelAdd += row.TotalFuelAdd;
                fuelSub += row.TotalFuelSub;

                if(!row.IsLitrePerHourNull())
                    totalTimeEngineOn += (row.TimeMotion + (row.IsTimeStopWithEngineOnNull() ? 
                        new TimeSpan(0) : row.TimeStopWithEngineOn));
            } // foreach

            avgFuelRateInMachineHour = totalTimeEngineOn.TotalHours == 0 ? // aketner - 28.08.2012
                0 : (Math.Round((totalFuelRate - fuelSub) / totalTimeEngineOn.TotalHours, 2));

            //avgMotionFuelRate = atlantaDataSetBindingSource.Count == 0 ?
            //    0 : avgMotionFuelRate / atlantaDataSetBindingSource.Count;

            avgMotionFuelRate = atlantaDataSetBindingSource.Count == 0 ?
                0 : (totalFuelRate - fuelSub) / totalTravel * 100; // aketner - 28.08.2012
        } // CalcTotals

        protected override void Algorithm_Action(object sender, EventArgs e)
        {
            if (sender is TotalFuelExpensesDutAlgorithm) { }
                //EnableButton();
        }

        // очистим структуры данных отчета
        public override void ClearReport()
        {
            listCurMrow.Clear();
            feDataSet.FuelExpensesDUT_Total.Clear();
        }

        // Выборка данных
        public void SelectItem(atlantaDataSet.mobitelsRow m_row)
        {
            Application.DoEvents();
            foreach (IAlgorithm alg in _algoritms)
            {
                alg.SelectItem(m_row);
                alg.Run();

                if (_stopRun)
                    return;
            }

            Application.DoEvents();
            //ReportsControl.ShowGraph(m_row);
        } // SelectItem

        static Track GetTrackSegment(atlantaDataSet.KilometrageReportRow row)
        {
            List<IGeoPoint> data = new List<IGeoPoint>();
            bool inside = false;

            foreach (atlantaDataSet.dataviewRow d_row in dataset.dataview.Select(
                "Mobitel_ID=" + row.MobitelId, "time ASC"))
            {
                if (d_row.DataGps_ID == row.InitialPointId)
                    inside = true;

                if (inside)
                    data.Add(DataSetManager.ConvertDataviewRowToDataGps(d_row));

                if (d_row.DataGps_ID == row.FinalPointId)
                    break;
            } // foreach

            return new Track(row.MobitelId, Color.DarkBlue, 2f, data);
        } // GetTrackSegment

        static void showMark(PointLatLng latlng, int mobitelId, string message)
        {
            PointLatLng location = new PointLatLng(latlng.Lat, latlng.Lng);
            List<Marker> markers = new List<Marker>();
            markers.Add(new Marker(MarkerType.Report, mobitelId, location, message, ""));
            ReportsControl.OnMarkersShowNeededWith(markers);
        }

        // строим трек на карте на основе отчета Пробег
        protected void ShowTrackOnMap(int iRow)
        {
            atlantaDataSet.KilometrageReportRow klmtRow;
            List<Track> segments = new List<Track>();
            ReportsControl.OnClearMapObjectsNeeded();
            atlantaDataSet.mobitelsRow mobRow = listCurMrow[iRow];
            DataRow[] dRow = dataset.KilometrageReport.Select("MobitelId=" + mobRow.Mobitel_ID, "Id ASC");

            for(int i = 0; i < dRow.Length; i++)
            {
                klmtRow = (atlantaDataSet.KilometrageReportRow)((DataRow)dRow[i]);
                segments.Add(GetTrackSegment(klmtRow));
            } // for

            if (segments.Count > 0)
            {
                ReportsControl.OnTrackSegmentsShowNeededWith(segments);
                DataRow Row = gvFuelControlDut.GetDataRow(iRow);
                if (Row != null)
                {
                    showMark(segments[0].GeoPoints[0].LatLng, mobRow.Mobitel_ID, Row["NameCar"].ToString());
                }
            } // if
        } // ShowTrackOnMap

        // show track on map - click button map
        protected override void bbiShowOnMap_ItemClick(object sender, ItemClickEventArgs e)
        {
            ReportsControl.OnMapShowNeeded();
            int indexRow = gvFuelControlDut.GetFocusedDataSourceRowIndex();
            ShowTrackOnMap(indexRow); /* построить трек на карте */
        } // bbiShowOnMap_ItemClick

        // show graph - click button graph
        protected override void bbiShowOnGraf_ItemClick(object sender, ItemClickEventArgs e)
        {
            ReportsControl.OnGraphShowNeeded();
            int indexRow = gvFuelControlDut.GetFocusedDataSourceRowIndex();
            SelectGraphic(listCurMrow[indexRow]); // перерисовать график;
        } // bbiShowOnGraf_ItemClick

        //клик по строке таблицы
        private void gvFuelControlDut_ClickRow(object sender, RowClickEventArgs e)
        {
            if (e.RowHandle >= 0)
            {
                int indexRow = gvFuelControlDut.GetFocusedDataSourceRowIndex();
                SelectGraphic(listCurMrow[indexRow]); // перерисовать график
                ShowTrackOnMap(indexRow); // перерисовать карту
            } // if
        } // gvDriver_FocusedRowChanged

        // Нажатие кнопки формирования таблицы отчета
        protected override void bbiStart_ItemClick(object sender, ItemClickEventArgs e)
        {
            bool noData = true; // За выбранный период нет данных ни по одной машине

            if (bbiStart.Caption == Resources.Start)
            {
                SetStopButton();
                DisableButton();
                ClearReport();
                BeginReport();
                _stopRun = false;

                atlantaDataSetBindingSource.RaiseListChangedEvents = false;
                atlantaDataSetBindingSource.SuspendBinding();

                foreach (atlantaDataSet.mobitelsRow m_row in dataset.mobitels)
                {
                    if (m_row.Check && m_row.GetdataviewRows().Length > 0)
                    {
                        listCurMrow.Add(m_row);
                        SelectItem(m_row);
                        noData = false;
                        if (_stopRun)
                            break;
                    } // if
                } // foreach

                if (noData)
                {
                    XtraMessageBox.Show(Resources.WarningNoData, Resources.Notification,
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

                atlantaDataSetBindingSource.RaiseListChangedEvents = true;
                atlantaDataSetBindingSource.ResumeBinding();
                atlantaDataSetBindingSource.Filter = ""; // Mobitel_ID >= 0";
                Select(curMrow);
                SelectGraphic(curMrow);

                SetStartButton();

                if (atlantaDataSetBindingSource.Count > 0)
                {
                    EnableButton();
                }
            }
            else
            {
                SetStartButton();
                _stopRun = true;
                StopReport();
                if (!noData)
                    EnableButton();
            } // else
        } // bbiStart_ItemClick

        public void SelectGraphic(atlantaDataSet.mobitelsRow mobitel_row)
        {
            if (mobitel_row == null)
                return;

            int length = mobitel_row.GetdataviewRows().Length;

            if (length == 0)
                return;

            curMrow = mobitel_row;
            vehicleInfo = new VehicleInfo(mobitel_row);
            ReportsControl.GraphClearSeries();
            Graph.ClearRegion();
            buildGraph.AddGraphFuel(Graph, dataset, mobitel_row);
            string nameSeries = vehicleInfo.RegistrationNumber + vehicleInfo.CarMaker + vehicleInfo.CarModel;
            Graph.ShowSeries(nameSeries);
            ReportsControl.ShowGraph(curMrow);
        } // SelectGraphic

        // формирование колонтитулов верхних отчета
        private void composLink_CreateMarginalHeaderArea(object sender, CreateAreaEventArgs e)
        {
            DevExpressReportHeader(Resources.TotalFuelExpensesDutReportName, e);
            TInfo info = ReportingFuelDUTTotal.GetInfoStructure;
            string strPeriod = Resources.PeriodFrom + " " + info.periodBeging + " " +
                Resources.PeriodTo + " " + info.periodEnd;
            DevExpressReportSubHeader(strPeriod, 22, e);
        }

        /* функция для формирования левой части заголовка отчета */
        protected string GetStringBreackLeft()
        {
	    ReportingFuelDUTTotal.SetRectangleBrckLetf(0, 0, 300, 85);
            return (Resources.TotalTransportTraveled + ": " + totalTravel.ToString("N2") + "\n" +
                Resources.TotalFuelConsumption + ": " + totalFuelRate.ToString("N2") + "\n" +
                Resources.FuelExControlDutCommonFuelAdd + ": " + fuelAdd.ToString("N2"));
        }

        /* функция для формирования верхней части заголовка отчета - будет пусто*/
        protected string GetStringBreackUp()
        {
	    ReportingFuelDUTTotal.SetRectangleBrckUP(380, 0, 320, 85);
            return ("");
        }

        /* функция для формирования правой части заголовка отчета */
        protected string GetStringBreackRight()
        {
	    ReportingFuelDUTTotal.SetRectangleBrckRight(770, 0, 300, 85);
            return (Resources.FuelExControlMiddleFuelrateInMove + ": " + avgMotionFuelRate.ToString("N2") + "\n" +
                Resources.FuelExControlMotorHour + ": " + avgFuelRateInMachineHour.ToString("N2") + "\n" +
                Resources.FuelExControlDutCommonFuelPourOff + ": " + fuelSub.ToString("N2"));
        }

        // Формирует данные для отчета по всем активным машинам
        protected override void ExportAllDevToReport()
        {
            XtraGridService.SetupGidViewForPrint(gvFuelControlDut, true, true);

            TInfo t_info = new TInfo();
            VehicleInfo info = new VehicleInfo(curMrow.Mobitel_ID);

            // заголовок для таблиц отчета, для каждого отчета разный
            t_info.periodBeging = Algorithm.Period.Begin;
            t_info.periodEnd = Algorithm.Period.End;

            ReportingFuelDUTTotal.AddInfoStructToList(t_info); /* формируем заголовки таблиц отчета */
            ReportingFuelDUTTotal.CreateAndShowReport(gcFuelControlDut);
        } // ExportToExcelDevExpress
        
        protected override void barButtonGroupPanel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            GroupPanel(gvFuelControlDut);
        }

        protected override void barButtonFooterPanel_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            FooterPanel(gvFuelControlDut);
        }
        
        protected override void barButtonNavigator_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            NavigatorPanel(gcFuelControlDut);
        }

        public void Localization()
        {
            colNameCar.Caption = Resources.FuelExControlNameCar;
            colNameCar.ToolTip = Resources.FuelExControlNameCar;

            colFIO.Caption = Resources.FuelExControlDriverFIO;
            colFIO.ToolTip = Resources.FuelExControlDriverFIO;

            colTravel.Caption = Resources.FuelExControlTravel;
            colTravel.ToolTip = Resources.FuelExControlTravel;

            colFuelBegin.Caption = Resources.FuelExControlDutFuelInBeginTime;
            colFuelBegin.ToolTip = Resources.FuelExControlDutFuelInBeginTime;

            colFuelEnd.Caption = Resources.FuelExControlDutFuelInEndTime;
            colFuelEnd.ToolTip = Resources.FuelExControlDutFuelInEndTime;

            colFuelAddCount.Caption = Resources.FuelExControlDutNumbersFuelAdd;
            colFuelAddCount.ToolTip = Resources.FuelExControlDutNumbersFuelAdd;

            colFuelSubCount.Caption = Resources.FuelExControlDutNumberFuelPourOff;
            colFuelSubCount.ToolTip = Resources.FuelExControlDutNumberFuelPourOff;

            colTotalFuelAdd.Caption = Resources.FuelExControlDutCommonFuelAdd;
            colTotalFuelAdd.ToolTip = Resources.FuelExControlDutCommonFuelAdd;

            colTotalFuelSub.Caption = Resources.FuelExControlDutCommonFuelPourOff;
            colTotalFuelSub.ToolTip = Resources.FuelExControlDutCommonFuelPourOff;

            colTotalFuelUse.Caption = Resources.FuelExControlCommonFuelrate;
            colTotalFuelUse.ToolTip = Resources.FuelExControlCommonFuelrate;

            colFuelUseAvg.Caption = Resources.FuelExControlMiddleFuelrateInMove;
            colFuelUseAvg.ToolTip = Resources.FuelExControlMiddleFuelrateInMove;

            colLitrePerHour.Caption = Resources.FuelExControlMiddleFuelrateOnTime;
            colLitrePerHour.ToolTip = Resources.FuelExControlMiddleFuelrateOnTime;

            colTimeMotion.Caption = Resources.FuelExControlTimeMove;
            colTimeMotion.ToolTip = Resources.FuelExControlTimeMove;

            colTimeStopWithEngineOn.Caption = Resources.FuelExControlTimeStopWithEngineOn;
            colTimeStopWithEngineOn.ToolTip = Resources.FuelExControlTimeStopWithEngineOn;

            colTimeStopWithEngineOff.Caption = Resources.FuelExControlDutTimeStopsWithEngineOff;
            colTimeStopWithEngineOff.ToolTip = Resources.FuelExControlDutTimeStopsWithEngineOff;
            
            colMaxSpeed.Caption = Resources.FuelExControlMaxSpeed;
            colMaxSpeed.ToolTip = Resources.FuelExControlMaxSpeed;
        } // Localization
    } // FuelExpensesCtrlDut
} // ReportsOnFuelExpenses
