using System;

namespace ReportsOnFuelExpenses
{
    /// <summary>
    /// ������� FuelExpensesDataSet.
    /// ��� �������� � ���������, ������� ��������
    /// � ������ ��������� ������� �������� ��������� �
    /// ������� ������, � �� �������������� ��������������.
    /// </summary> 
    internal static class FuelExpensesDataSetCreator
    {
        /// <summary>
        /// ������ �������������
        /// </summary>
        private static volatile object syncObject = new object();
        /// <summary>
        /// ��������� ��������
        /// </summary>
        private static volatile FuelExpensesDataSet dataSet;

        /// <summary>
        /// ���������� ��������� ��������
        /// </summary>
        /// <returns>CheckZonesDataSett</returns>
        internal static FuelExpensesDataSet GetDataSet()
        {
            if (dataSet == null)
            {
                lock (syncObject)
                {
                    if (dataSet == null)
                        dataSet = new FuelExpensesDataSet();
                }
            }

            return dataSet;
        } // GetDataSet()
    }
}
