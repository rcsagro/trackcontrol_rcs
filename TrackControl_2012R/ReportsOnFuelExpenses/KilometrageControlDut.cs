﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BaseReports;
using BaseReports.Procedure;
using BaseReports.ReportsDE;
using DevExpress.Utils;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraPrinting;
using LocalCache;
using Report;
using ReportsOnFuelExpenses.Properties;
using TrackControl.General;
using TrackControl.Reports;
using TrackControl.Reports.Graph;

namespace ReportsOnFuelExpenses
{
    public partial class KilometrageControlDut : BaseReports.ReportsDE.BaseControl
    {
        public class reportKlmtFuel
        {
            string state;
            string location;
            string datetime;
            string initial_time;
            string final_time;
            string interval;
            double distance;
            double fuelbegin;
            double fuelend;
            double totalfueluse;
            double fueluseavg;
            double normalfuel100;
            double fuelavgnormal;
            double deviationfuel;

            public reportKlmtFuel(string state, string location, string datetime, string initial_time,
                    string final_time, string interval, double distance,
                double fuelbegin, double fuelend, double totalfueluse, double fueluseavg, double normalfuel100,
                double fuelavgnormal, double deviationfuel)
            {
                this.state = state;
                this.location = location;
                this.datetime = datetime;
                this.initial_time = initial_time;
                this.final_time = final_time;
                this.interval = interval;
                this.distance = distance;
                this.fuelbegin = fuelbegin;
                this.fuelend = fuelend;
                this.totalfueluse = totalfueluse;
                this.fueluseavg = fueluseavg;
                this.normalfuel100 = normalfuel100;
                this.fuelavgnormal = fuelavgnormal;
                this.deviationfuel = deviationfuel;
            } // reportKlmtFuel

            public int Id
            {
                get { return 0; }
            }

            public int MobitelId
            {
                get { return 1; }
            }

            public string State
            {
                get { return state; }
            }

            public string Location
            {
                get { return location; }
            }

            public string DateTime
            {
                get { return datetime; }
            }

            public string InitialTime
            {
                get { return initial_time; }
            }

            public string FinalTime
            {
                get { return final_time; }
            }

            public string Interval
            {
                get { return interval; }
            }

            public double Distance
            {
                get { return distance; }
            }

            public double FuelBegin
            {
                get { return fuelbegin; }
            }

            public double FuelEnd
            {
                get { return fuelend; }
            }

            public double TotalFuelUse
            {
                get { return totalfueluse; }
            }

            public double FuelUseAvg
            {
                get { return fueluseavg; }
            }

            public double NormalFuel100
            {
                get { return normalfuel100; }
            }

            public double FuelAvgNormal
            {
                get { return fuelavgnormal; }
            }

            public double DeviationFuel
            {
                get { return deviationfuel; }
            }

            public int TypeAlgorythm
            {
                get { return 7; }
            }
        } // reportKlmt

        // датасет
        public FuelExpensesDataSet feDataSet;
        // Интерфейс для построение графиков по SeriesL
        private static IBuildGraphs buildGraph;
        protected static atlantaDataSet dataset;
        protected VehicleInfo vehicleInfo;
        Kilometrage algoritm;
        ComboBoxEdit comboAlgorithm = null;
        /// <summary>
        /// Список, содержащий итоговые данные для каждого телетрека (транспортного
        /// средства).
        /// </summary>
        private List<KilometrageFuelControlAlgorithm.Summary> _summariesData = new List<KilometrageFuelControlAlgorithm.Summary>();
        BarButtonItem _bbiShowParking;
        bool _allStops;
        bool _flagShowParking = true;
        BarManager _bManager;
        Bar _barC;
        ReportBase<reportKlmtFuel, TInfo> ReportingKlmtFuel;
        private string NameThisReport = Resources.KilometrageControlDutName;
        Fuel fuelAlgorythm1 = null;

        public KilometrageControlDut()
        {
            InitializeComponent();
            Localization();
            HideStatusBar();
            DisableButton();
            VisionPanel(gridView1, gridControl1, bar3);
            SetVisibilityAlgoChoiser(true);
            EnableRun();
            feDataSet = FuelExpensesDataSetCreator.GetDataSet();
            atlantaDataSetBindingSource.DataSource = feDataSet;
            atlantaDataSetBindingSource.DataMember = "KilometrageControlFuelDUT";
            //gridControl1.DataSource = atlantaDataSetBindingSource;
            dataset = ReportTabControl.Dataset;
            buildGraph = new BuildGraphs();
            fuelExpensesDataSet = null;
            AddAlgorithm(new Kilometrage());
            AddAlgorithm(new Rotation());
            fuelAlgorythm1 = new Fuel(AlgorithmType.FUEL1);
            AddAlgorithm(fuelAlgorythm1);
            AddAlgorithm(new Fuel(AlgorithmType.FUEL1_AGREGAT));
            AddAlgorithm(new Fuel(AlgorithmType.FUEL2_AGREGAT));
            AddAlgorithm(new Fuel(AlgorithmType.FUEL3_AGREGAT));
            AddAlgorithm(new Fuel(AlgorithmType.FUEL_AGREGAT_ALL));
            
            KilometrageFuelControlAlgorithm algoritm1 = new KilometrageFuelControlAlgorithm(AlgorithmType.FUEL1);
            KilometrageFuelControlAlgorithm algoritm2 = new KilometrageFuelControlAlgorithm(AlgorithmType.FUEL1_AGREGAT);
            KilometrageFuelControlAlgorithm algoritm3 = new KilometrageFuelControlAlgorithm(AlgorithmType.FUEL2_AGREGAT);
            KilometrageFuelControlAlgorithm algoritm4 = new KilometrageFuelControlAlgorithm(AlgorithmType.FUEL3_AGREGAT);
            KilometrageFuelControlAlgorithm algoritm5 = new KilometrageFuelControlAlgorithm(AlgorithmType.FUEL_AGREGAT_ALL);
            
            AddAlgorithm(algoritm1);
            AddAlgorithm(algoritm2);
            AddAlgorithm(algoritm3);
            AddAlgorithm(algoritm4);
            AddAlgorithm(algoritm5);
            
            _summariesData = new List<KilometrageFuelControlAlgorithm.Summary>();
            ClearStatusLine();
            gridView1.Columns.View.OptionsBehavior.AutoPopulateColumns = false;
            gridView1.KeyDown += GvKlmtCOnKeyDown;
            gridView1.RowClick += gvKlmtC_RowClick;
            gridView1.CustomDrawFooterCell += GvKlmtCOnCustomDrawFooterCell;
            gridView1.CustomColumnGroup += gvKlmtC_CustomColumnGroup;
            gridView1.CustomDrawGroupRow += gvKlmtC_CustomDrawGroupRow;
            gridView1.BeforePrintRow += gvKlmtC_BeforePrintRow;
            _graph = ReportTabControl.Graph;
            compositeLink1.CreateMarginalHeaderArea +=
                new CreateAreaEventHandler(composLink_CreateMarginalHeaderArea);
            ReportingKlmtFuel =
                new ReportBase<reportKlmtFuel, TInfo>(Controls, compositeLink1, gridView1,
                    GetStringBreackLeft, GetStringBreackRight, GetStringBreackUp);
        }

        protected override void bbiShowOnGraf_ItemClick(object sender, ItemClickEventArgs e)
        {
            ReportsControl.OnGraphShowNeeded();
            ShowSpeedOnGraph();
        }

        private void AddSummary(KilometrageFuelControlAlgorithm.Summary summary)
        {
            _summariesData.Add(summary);
        }

        private void ClearSummary()
        {
            _summariesData.Clear();
        }

        private void RemoveSummary(int mobitelId, int type)
        {
            for (int i = 0; i < _summariesData.Count; i++)
            {
                if (_summariesData[i].id == mobitelId && _summariesData[i].typeAlgo == type)
                {
                    _summariesData.RemoveAt(i);
                }
            }
        }

        private KilometrageFuelControlAlgorithm.Summary getSummary(int mobitelId, int type)
        {
            for (int i = 0; i < _summariesData.Count; i++)
            {
                if (_summariesData[i].id == mobitelId && _summariesData[i].typeAlgo == type)
                {
                    return _summariesData[i];
                }
            }

            return new KilometrageFuelControlAlgorithm.Summary();
        }

        private int ContainsData(int id, int algo)
        {
            for (int i = 0; i < _summariesData.Count; i++)
            {
                if (_summariesData[i].id == id && _summariesData[i].typeAlgo == algo)
                {
                    return i;
                } // if
            } // for

            return -1;
        }

        private void ShowSpeedOnGraph()
        {
            Graph.ClearRegion();
            int rowIndex = gridView1.GetFocusedDataSourceRowIndex();
            if (rowIndex >= 0)
            {
                FuelExpensesDataSet.KilometrageControlFuelDUTRow row = (FuelExpensesDataSet.KilometrageControlFuelDUTRow)
                    ((DataRowView)atlantaDataSetBindingSource.List[rowIndex]).Row;

                Graph.AddTimeRegion(row.InitialTime, row.FinalTime);
            }
        }

        // show track on map
        protected override void bbiShowOnMap_ItemClick(object sender, ItemClickEventArgs e)
        {
            ReportsControl.OnMapShowNeeded();
            ShowTrackOnGraph();
        }

        private void ShowTrackOnGraph()
        {
            ReportsControl.OnClearMapObjectsNeeded();
            List<Track> segments = new List<Track>();
            Int32[] selected_row = gridView1.GetSelectedRows();

            for (int i = 0; i < selected_row.Length; i++)
            {
                FuelExpensesDataSet.KilometrageControlFuelDUTRow nrow = ( FuelExpensesDataSet.KilometrageControlFuelDUTRow)
                    ((DataRowView)atlantaDataSetBindingSource.List[selected_row[i]]).Row;

                segments.Add(GetTrackSegment(nrow));
            } // for

            if (segments.Count > 0)
                ReportsControl.OnTrackSegmentsShowNeeded(segments);
        } // ShowTrackOnGraph

        private Track GetTrackSegment(FuelExpensesDataSet.KilometrageControlFuelDUTRow row)
        {
            List<IGeoPoint> data = new List<IGeoPoint>();
            Algorithm.GetPointsInsided(row.InitialPointId, row.FinalPointId, data);

            if (data.Count <= 0)
                return null;

            return new Track(row.MobitelId, Color.Red, 2f, data);
        } // GetTrackSegment

        public override void DisableButton()
        {
            bbiShowOnMap.Enabled = false;
            bbiShowOnGraf.Enabled = false;
            bbiPrintReport.Enabled = false;
            _bbiShowParking.Enabled = false;
        }

        public override void EnableButton()
        {
            bbiShowOnMap.Enabled = true;
            bbiShowOnGraf.Enabled = true;
            bbiPrintReport.Enabled = true;
            _bbiShowParking.Enabled = true;
        }

        protected void bbiShowParking_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            _allStops = !_allStops;
            ReportsControl.OnMapShowNeeded();
            redrawMapAndGraph();

            if (_flagShowParking)
            {
                _bbiShowParking.Hint = Resources.ShowParkingOnMap;
                _flagShowParking = false;
            }
            else
            {
                _bbiShowParking.Hint = Resources.RemoveParkingFromMap;
                _flagShowParking = true;
            }
        } // bbiShowParking_ItemClick

        void _algorithm_ReportCompleted(KilometrageDutDayEventArgs args)
        {
            int index = ContainsData(args.Id, globalCode);

            if (index != -1)
            {
                _summariesData.RemoveAt(index);
            }

            _summariesData.Add(args.SummaryDutDay);
        } // _algorithm_ReportCompleted

        IGraph _graph;
        private int globalCode = (int)AlgorithmType.FUEL1;

        void redrawMapAndGraph()
        {
            ReportsControl.OnClearMapObjectsNeeded();
            _graph.ClearRegion();
            DataRow dR = gridView1.GetFocusedDataRow();

            if (dR != null)
            {
                FuelExpensesDataSet.KilometrageControlFuelDUTRow row = (FuelExpensesDataSet.KilometrageControlFuelDUTRow)dR;
                _graph.AddTimeRegion(row.InitialTime, row.FinalTime);

                if (Resources.Movement == row.State)
                    showTrackSegment(row);
                else if (!_allStops)
                    showStop(row);
            } // if

            if (_allStops)
                showAllStops();
        } // redrawMapAndGraph

        static void showStop(FuelExpensesDataSet.KilometrageControlFuelDUTRow row)
        {
            PointLatLng location = new PointLatLng(row.Latitude, row.Longitude);
            string message = row.Interval.ToString();
            List<Marker> markers = new List<Marker>();
            markers.Add(new Marker(MarkerType.Parking, row.MobitelId, location, message, ""));
            ReportsControl.OnMarkersShowNeeded(markers);
        }

        void showAllStops()
        {
            List<Marker> markers = new List<Marker>();
            string filterExpression = String.Format("MobitelId={0}", curMrow.Mobitel_ID);

            foreach (FuelExpensesDataSet.KilometrageControlFuelDUTRow row in feDataSet.KilometrageControlFuelDUT.Select(filterExpression))
            {
                if (Resources.Parking == row.State)
                {
                    PointLatLng location = new PointLatLng(row.Latitude, row.Longitude);
                    string message = row.Interval.ToString();
                    markers.Add(new Marker(MarkerType.Parking, row.MobitelId, location, message, ""));
                } // if
            } // foreach

            ReportsControl.OnMarkersShowNeeded(markers);
        } // showAllStops

        static void showTrackSegment(FuelExpensesDataSet.KilometrageControlFuelDUTRow row)
        {
            List<IGeoPoint> data = new List<IGeoPoint>();
            Algorithm.GetPointsInsided(row.InitialPointId, row.FinalPointId, data);
            if (data.Count > 0)
            {
                List<Track> segments = new List<Track>();
                segments.Add(new Track(row.MobitelId, Color.Red, 2f, data));
                ReportsControl.OnTrackSegmentsShowNeeded(segments);
            } // if
        } // showTrackSegment

        private void GvKlmtCOnKeyDown(object sender, KeyEventArgs keyEventArgs)
        {
            if (keyEventArgs.KeyCode == Keys.Down || keyEventArgs.KeyCode == Keys.Up)
            {
                redrawMapAndGraph();
            }
        }

        void gvKlmtC_RowClick(object sender, RowClickEventArgs e)
        {
            redrawMapAndGraph();
        }

        void gvKlmtC_BeforePrintRow(object sender, DevExpress.XtraGrid.Views.Printing.CancelPrintRowEventArgs e)
        {
            GridView view = sender as GridView;
            int hndl = e.RowHandle;
            int countRow = 0;
            TimeSpan duration = new TimeSpan();
            string GroupText = "";

            if (hndl < 0 && hndl > -2147483648)
            {
                e.Cancel = true;
                GroupText = view.GetGroupRowDisplayText(hndl);
                int count = view.GetChildRowCount(hndl);
                for (int i = 0; i < count; i++)
                {
                    int handle = view.GetChildRowHandle(hndl, i);
                    if (string.Equals(view.GetRowCellValue(handle, "State"), Resources.Parking))
                    {
                        countRow++;
                        string period = view.GetRowCellValue(handle, "Interval").ToString();
                        TimeSpan speriod = TimeSpan.Parse(period);
                        duration += speriod;
                    }
                } // for

                GroupText += ". " + Resources.KilometrageTotalNumber + countRow + ". ";
                GroupText += Resources.KilometrageNumberStops + duration;

                TextBrick tb = e.PS.CreateTextBrick() as TextBrick;
                tb.Text = GroupText;
                tb.Font = new Font(tb.Font, FontStyle.Bold);
                tb.HorzAlignment = DevExpress.Utils.HorzAlignment.Near;
                tb.Padding = new PaddingInfo(5, 0, 0, 0);
                tb.BackColor = Color.LightGray;
                tb.ForeColor = Color.Black;
                SizeF clientPageSize = (e.BrickGraphics as BrickGraphics).ClientPageSize;
                float textBrickHeight = ((GridViewInfo)view.GetViewInfo()).CalcRowHeight(e.Graphics, e.RowHandle, e.Level);
                RectangleF textBrickRect = new RectangleF(0, e.Y, (int)clientPageSize.Width, textBrickHeight);
                e.BrickGraphics.DrawBrick(tb, textBrickRect);
                e.Y += (int)textBrickHeight;
            } // if
        }

        void gvKlmtC_CustomDrawGroupRow(object sender, RowObjectCustomDrawEventArgs e)
        {
            GridView view = sender as GridView;
            GridGroupRowInfo info = e.Info as GridGroupRowInfo;
            int countRow = 0;
            TimeSpan duration = new TimeSpan();

            if (info.Column.Caption == Resources.KilometrageDateTimeCaption)
            {
                int count = view.GetChildRowCount(info.RowHandle);
                for (int i = 0; i < count; i++)
                {
                    int handle = view.GetChildRowHandle(info.RowHandle, i);

                    if (string.Equals(view.GetRowCellValue(handle, "State"), Resources.Parking))
                    {
                        countRow++;
                        string period = view.GetRowCellValue(handle, "Interval").ToString();
                        TimeSpan speriod = TimeSpan.Parse(period);
                        duration += speriod;
                    }
                }

                info.GroupText = Resources.KilometrageDateTimeCaption + ": " + info.GroupValueText + ". ";
                info.GroupText += ". " + Resources.KilometrageTotalNumber + countRow + ". ";
                info.GroupText += Resources.KilometrageNumberStops + duration;
            }
        }

        void gvKlmtC_CustomColumnGroup(object sender, CustomColumnSortEventArgs e)
        {
            if (e.Column.FieldName == "DateTime")
            {
                e.Handled = true;
            }
        }

        private void GvKlmtCOnCustomDrawFooterCell(object sender, FooterCellCustomDrawEventArgs e)
        {
            e.Appearance.TextOptions.HAlignment = HorzAlignment.Center;
        }

        public override string Caption
        {
            get
            {
                return Resources.KilometrageControlDutName;
            }
        }

        public void SelectItem(atlantaDataSet.mobitelsRow m_row)
        {
            Application.DoEvents();
            double fuel1Subline = 0.0;
            double fuel1agrSubline = 0.0;
            double fuel2agrSubline = 0.0;
            double fuel3agrSubline = 0.0;
            double fuel4agrSubline = 0.0;

            foreach (IAlgorithm alg in _algoritms)
            {
                alg.SelectItem(m_row);

                if (alg is KilometrageFuelControlAlgorithm)
                {
                    KilometrageFuelControlAlgorithm _klmFuelCon = (KilometrageFuelControlAlgorithm)alg;
                    if (_klmFuelCon.TypeAlgorythm == (int) AlgorithmType.FUEL1)
                        _klmFuelCon.SetValueFueling = fuel1Subline;
                    if (_klmFuelCon.TypeAlgorythm == (int)AlgorithmType.FUEL1_AGREGAT)
                        _klmFuelCon.SetValueFueling = fuel1agrSubline;
                    if (_klmFuelCon.TypeAlgorythm == (int)AlgorithmType.FUEL2_AGREGAT)
                        _klmFuelCon.SetValueFueling = fuel2agrSubline;
                    if (_klmFuelCon.TypeAlgorythm == (int)AlgorithmType.FUEL3_AGREGAT)
                        _klmFuelCon.SetValueFueling = fuel3agrSubline;
                    if (_klmFuelCon.TypeAlgorythm == (int)AlgorithmType.FUEL_AGREGAT_ALL)
                        _klmFuelCon.SetValueFueling = fuel4agrSubline;
                }

                alg.Run();

                if(alg is Fuel)
                {
                    Fuel _fuel = (Fuel) alg;
                    if (_fuel.TypeAlgorithm == (int) AlgorithmType.FUEL1)
                        fuel1Subline = _fuel.summary.Fueling;
                    if (_fuel.TypeAlgorithm == (int)AlgorithmType.FUEL1_AGREGAT)
                        fuel1agrSubline = _fuel.summary.Fueling;
                    if (_fuel.TypeAlgorithm == (int)AlgorithmType.FUEL2_AGREGAT)
                        fuel2agrSubline = _fuel.summary.Fueling;
                    if (_fuel.TypeAlgorithm == (int)AlgorithmType.FUEL3_AGREGAT)
                        fuel3agrSubline = _fuel.summary.Fueling;
                    if (_fuel.TypeAlgorithm == (int)AlgorithmType.FUEL_AGREGAT_ALL)
                        fuel4agrSubline = _fuel.summary.Fueling;
                }

                if (_stopRun)
                    return;
            }
        } // SelctItem

        protected override void Algorithm_Action(object sender, EventArgs e)
        {
            if (sender is KilometrageFuelControlAlgorithm)
            {
                KilometrageDutDayEventArgs args = (KilometrageDutDayEventArgs)e;
                int indx = ContainsData(args.Id, args.TypeAlg);
                if (indx != -1)
                {
                    _summariesData.RemoveAt(indx);
                } // if

                _summariesData.Add(args.SummaryDutDay);
            } // if
        } // Algorithm_Action

        public override void ClearReport()
        {
            fuelAlgorythm1.ClearAtlantaFuelReport();
            if (feDataSet != null)
            {
                feDataSet.FuelExpensesDUT_Day.Clear();
                feDataSet.KilometrageControlFuelDUT.Clear();
            }
            gridControl1.DataSource = null;
            ClearStatusLine();
        }

        // Нажатие кнопки формирования таблицы отчета
        protected override void bbiStart_ItemClick(object sender, ItemClickEventArgs e)
        {
            bool noData = true; // За выбранный период нет данных ни по одной машине

            if (bbiStart.Caption == Resources.Start)
            {
                SetStopButton();
                DisableButton();
                ClearReport();
                ClearStatusLine();
                BeginReport();
                _stopRun = false;
                ClearSummary();

                atlantaDataSetBindingSource.SuspendBinding();

                foreach (atlantaDataSet.mobitelsRow m_row in dataset.mobitels)
                {
                    if (m_row.Check && DataSetManager.IsGpsDataExist(m_row))
                    {
                        SelectItem(m_row);
                        noData = false;
                        if (_stopRun)
                            break;
                    } // if
                } // foreach

                if (noData)
                {
                    XtraMessageBox.Show(Resources.WarningNoData, Resources.Notification,
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

                atlantaDataSetBindingSource.ResumeBinding();
                atlantaDataSetBindingSource.Filter = "";
                Select(curMrow);
                SetStartButton();
                EnableButton();
            } // if
            else
            {
                SetStartButton();
                _stopRun = true;
                StopReport();

                if (!noData)
                    EnableButton();
            } // else
        } // bbiStart_ItemClick

        void ClearStatusLine()
        {
            _distance.Caption = "Общий пробег, км:----";
            _totalfuel.Caption = "Общий расход, л:----";
            _fuelavg.Caption = "Расход, л/100км:----";
            _fuelavgnorma.Caption = "Расход по норме, в л/100км:----";
            _deviation.Caption = "Отклонение, в л:----";
        } // clearStatusLine

        private bool ContainsID(int id)
        {
            for (int i = 0; i < _summariesData.Count; i++)
            {
                if (_summariesData[i].id == id)
                {
                    return true;
                }
            }

            return false;
        }

        public override void Select(atlantaDataSet.mobitelsRow m_row)
        {
            if (m_row != null)
            {
                curMrow = m_row;
                int index = 0;
                if (comboAlgorithm != null)
                {
                    index = comboAlgorithm.SelectedIndex;
                }

                gridControl1.DataSource = atlantaDataSetBindingSource;

                if (atlantaDataSetBindingSource.Count > 0)
                {
                    if (index == 0)
                    {
                        atlantaDataSetBindingSource.Filter = "MobitelId=" + m_row.Mobitel_ID + " and " +
                                                             "TypeAlgorythm=" +
                                                             (int) AlgorithmType.FUEL1;
                    }
                    else if (index == 1)
                    {
                        atlantaDataSetBindingSource.Filter = "MobitelId=" + m_row.Mobitel_ID + " and " +
                                                             "TypeAlgorythm=" +
                                                             (int) AlgorithmType.FUEL1_AGREGAT;
                    }
                    else if (index == 2)
                    {
                        atlantaDataSetBindingSource.Filter = "MobitelId=" + m_row.Mobitel_ID + " and " +
                                                             "TypeAlgorythm=" +
                                                             (int) AlgorithmType.FUEL2_AGREGAT;
                    }
                    else if (index == 3)
                    {
                        atlantaDataSetBindingSource.Filter = "MobitelId=" + m_row.Mobitel_ID + " and " +
                                                             "TypeAlgorythm=" +
                                                             (int) AlgorithmType.FUEL3_AGREGAT;
                    }
                    else if (index == 4)
                    {
                        atlantaDataSetBindingSource.Filter = "MobitelId=" + m_row.Mobitel_ID + " and " +
                                                             "TypeAlgorythm=" + (int)AlgorithmType.FUEL_AGREGAT_ALL;
                    }
                }


                if (ContainsID(m_row.Mobitel_ID))
                    {
                        ApdateCommandStrip(m_row.Mobitel_ID, globalCode);
                    }

                    if (CurrentActivateReport == NameThisReport)
                    {
                        ReportsControl.GraphClearSeries();
                        Graph.ClearLabel();
                        if (index == 0)
                        {
                            SelectGraphic(curMrow, (int) AlgorithmType.FUEL1);
                        }
                        else if (index == 1)
                        {
                            SelectGraphic(curMrow, (int) AlgorithmType.FUEL1_AGREGAT);
                        }
                        else if (index == 2)
                        {
                            SelectGraphic(curMrow, (int) AlgorithmType.FUEL2_AGREGAT);
                        }
                        else if (index == 3)
                        {
                            SelectGraphic(curMrow, (int) AlgorithmType.FUEL3_AGREGAT);
                        }
                        else if (index == 4)
                        {
                            SelectGraphic(curMrow, (int)AlgorithmType.FUEL_AGREGAT_ALL);
                        }
                    }
            }
        }

        public void SelectGraphic(atlantaDataSet.mobitelsRow mobitel, int type)
        {
            if (mobitel != null)
            {
                curMrow = mobitel;
                vehicleInfo = new VehicleInfo(mobitel);
                atlantaDataSetBindingSource.Filter = "MobitelId=" + mobitel.Mobitel_ID + " and " + "TypeAlgorythm=" + type;

                try
                {
                    ReportsControl.GraphClearSeries();

                    if (type == (int)AlgorithmType.FUEL1)
                    {
                        CreateGraph(mobitel, AlgorithmType.FUEL1);
                    }
                    else if (type == (int)AlgorithmType.FUEL1_AGREGAT)
                    {
                        CreateGraph(mobitel, AlgorithmType.FUEL1_AGREGAT);
                    }
                    else if (type == (int)AlgorithmType.FUEL2_AGREGAT)
                    {
                        CreateGraph(mobitel, AlgorithmType.FUEL2_AGREGAT);
                    }
                    else if (type == (int)AlgorithmType.FUEL3_AGREGAT)
                    {
                        CreateGraph(mobitel, AlgorithmType.FUEL3_AGREGAT);
                    }
                    else if (type == (int)AlgorithmType.FUEL_AGREGAT_ALL)
                    {
                        CreateGraph(mobitel, AlgorithmType.FUEL_AGREGAT_ALL);
                    }
                    else
                    {
                        throw new NotImplementedException("Unknown type algorythm!");
                    }
                }
                catch (Exception ex)
                {
                    string message = String.Format("Exception in FuelExpensesControlDut: {0}", ex.Message);
                    XtraMessageBox.Show(message, Caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            } // if
        }

        private void CreateGraph(atlantaDataSet.mobitelsRow mobitel_row, object algoParam)
        {
            if (!DataSetManager.IsGpsDataExist(mobitel_row)) return;

            buildGraph.SetAlgorithm(algoParam);
            buildGraph.AddGraphFuel(algoParam, Graph, dataset, mobitel_row);
            buildGraph.AddGraphVoltage(Graph, dataset, mobitel_row); // add 04.07.2017 aketner
            ReportsControl.ShowGraph(mobitel_row);

            if (buildGraph.AddGraphInclinometers(Graph, dataset, mobitel_row))
            {
                vehicleInfo = new VehicleInfo(mobitel_row);
                Graph.ShowSeries(vehicleInfo.Info);
            }
        } // CreateGraph

        private void ApdateCommandStrip(int mobitelId, int code)
        {
            KilometrageFuelControlAlgorithm.Summary _s = getSummary(mobitelId, code);
            _distance.Caption = "Общий пробег, км:" +
                                String.Format("{0:f2}", _s.TotalTravel);
            _totalfuel.Caption = "Общий расход, л:" +
                                 String.Format("{0:f2}", _s.TotalFuel);
            _fuelavg.Caption = "Расход, л/100км:" +
                               String.Format("{0:f2}", _s.FuelAvg);
            _fuelavgnorma.Caption = "Расход по норме, л/100км:" +
                                    String.Format("{0:f2}", _s.FuelNorma);
            _deviation.Caption = "Отклонение, в л:" +
                                 String.Format("{0:f2}", _s.Deviation);
        }

        public void SelectGraphic(atlantaDataSet.mobitelsRow mobitel_row)
        {
            if ((mobitel_row == null) || !DataSetManager.IsGpsDataExist(mobitel_row))
                return;

            curMrow = mobitel_row;
            vehicleInfo = new VehicleInfo(mobitel_row);
            string nameSeries = vehicleInfo.RegistrationNumber + vehicleInfo.CarMaker + vehicleInfo.CarModel;
            ReportsControl.GraphClearSeries();
            Graph.ClearRegion();
            ReportsControl.ShowGraph(curMrow);
            buildGraph.SetAlgorithm(AlgorithmType.FUEL1);
            buildGraph.AddGraphFuel(AlgorithmType.FUEL1, Graph, dataset, curMrow);
            buildGraph.SetAlgorithm(AlgorithmType.FUEL1_AGREGAT);
            buildGraph.AddGraphFuel(AlgorithmType.FUEL1_AGREGAT, Graph, dataset, curMrow);
            buildGraph.SetAlgorithm(AlgorithmType.FUEL2_AGREGAT);
            buildGraph.AddGraphFuel(AlgorithmType.FUEL2_AGREGAT, Graph, dataset, curMrow);
            buildGraph.SetAlgorithm(AlgorithmType.FUEL3_AGREGAT);
            buildGraph.AddGraphFuel(AlgorithmType.FUEL3_AGREGAT, Graph, dataset, curMrow);
            Graph.ShowSeries(nameSeries);

            if (buildGraph.AddGraphInclinometers(Graph, dataset, curMrow))
            {
                vehicleInfo = new VehicleInfo(curMrow);
                Graph.ShowSeries(vehicleInfo.Info);
            }
        } // SelectGraphic

        public void Localization()
        {
            _imageComboRepo.Items[0].Description = Resources.Parking;
            _imageComboRepo.Items[0].Value = Resources.Parking;
            _imageComboRepo.Items[0].ImageIndex = 0;

            _imageComboRepo.Items[1].Description = Resources.Movement;
            _imageComboRepo.Items[1].Value = Resources.Movement;
            _imageComboRepo.Items[1].ImageIndex = 1;

            bchSafetyMode.Visibility = BarItemVisibility.Always;
            _bbiShowParking = new BarButtonItem();
            _bManager = GetBarManager();
            _bManager.Items.Add(_bbiShowParking);
            _barC = GetToolBar();
            _barC.LinksPersistInfo.Insert(3, new LinkPersistInfo(BarLinkUserDefines.PaintStyle,
                _bbiShowParking, BarItemPaintStyle.CaptionGlyph));
            _bbiShowParking.Caption = "";
            _bbiShowParking.Glyph = imageCollection.Images[1];
            _bbiShowParking.Id = 3;
            _bbiShowParking.Name = "bbiShowParking";
            _bbiShowParking.Hint = Resources.ShowParkingOnMap;
            _bbiShowParking.ItemClick += new ItemClickEventHandler(bbiShowParking_ItemClick);

            _bbiShowParking.Hint = Resources.ShowParkingOnMap;
        } // Localization

        // формирование отчета по текущей машине
        protected override void ExportToExcelDevExpress()
        {
            try
            {
                XtraGridService.SetupGidViewForPrint(gridView1, true, true);

                TInfo t_info = new TInfo();
                VehicleInfo info = new VehicleInfo(curMrow.Mobitel_ID, curMrow);

                // заголовок для таблиц отчета, для каждого отчета разный
                t_info.infoVehicle = info.Info; // Транспорт/Автомобиль
                t_info.infoDriverName = info.DriverFullName; // Водитель транспорта
                KilometrageFuelControlAlgorithm.Summary _s = getSummary(curMrow.Mobitel_ID, globalCode);
                t_info.distance = _s.TotalTravel; // Пройденный путь
                t_info.totalfuel = _s.TotalFuel; // Общий расход топлива
                t_info.fuelavg = _s.FuelAvg; // Расход в л/100км
                t_info.fuelavgnorma = _s.FuelNorma; // Норма расхода в л/100км
                t_info.deviation = _s.Deviation; // Отклонение
                t_info.periodBeging = Algorithm.Period.Begin;
                t_info.periodEnd = Algorithm.Period.End;

                ReportingKlmtFuel.AddInfoStructToList(t_info); /* формируем заголовки таблиц отчета */
                ReportingKlmtFuel.CreateAndShowReport(gridControl1);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message + "\n" + ex.StackTrace, "Error importing. Try again.",
                    MessageBoxButtons.OK);
            }
        }

        private IEnumerable<int> getSummaryKeys(int type)
        {
            List<int> keys = new List<int>();
            for (int i = 0; i < _summariesData.Count; i++)
            {
                if (type == _summariesData[i].typeAlgo)
                    keys.Add(_summariesData[i].id);
            }

            return keys;
        }

        // Формирует данные для отчета по всем активным машинам
        protected override void ExportAllDevToReport()
        {
            try
            {
                string format;
                string culture = CultureInfo.CurrentCulture.Name;

                if ("ru-RU" == culture)
                    format = "HH:mm";
                else
                    format = "HH:mm";

                foreach (int mobitelId in getSummaryKeys(globalCode))
                {
                    try
                    {
                        atlantaDataSet.mobitelsRow mobi = _dataset.mobitels.FindByMobitel_ID(mobitelId);
                        VehicleInfo info = new VehicleInfo(mobi.Mobitel_ID, mobi);
                        TInfo t_info = new TInfo();

                        // заголовок для таблиц отчета, для каждого отчета разный
                        t_info.infoVehicle = info.Info; // Транспорт/Автомобиль
                        t_info.infoDriverName = info.DriverFullName; // Водитель транспорта
                        KilometrageFuelControlAlgorithm.Summary _sum = getSummary(curMrow.Mobitel_ID, globalCode);
                        t_info.distance = _sum.TotalTravel; // Пройденный путь
                        t_info.totalfuel = _sum.TotalFuel; // Общий расход топлива
                        t_info.fuelavg = _sum.FuelAvg; // Расход в л/100км
                        t_info.fuelavgnorma = _sum.FuelNorma; // Норма расхода в л/100км
                        t_info.deviation = _sum.Deviation; // Отклонение
                        t_info.periodBeging = Algorithm.Period.Begin;
                        t_info.periodEnd = Algorithm.Period.End;

                        ReportingKlmtFuel.AddInfoStructToList(t_info); /* формируем заголовки таблиц отчета */
                        ReportingKlmtFuel.CreateBindDataList(); // создать новый список данных таблицы отчета

                        foreach (FuelExpensesDataSet.KilometrageControlFuelDUTRow row in
                            feDataSet.KilometrageControlFuelDUT.Select(
                                "MobitelId=" + mobitelId + " and " + "TypeAlgorythm=" + globalCode, "Id ASC"))
                        {
                            string datetime = "";
                            try
                            {
                                string state = row.State;
                                string location = (row["Location"] != DBNull.Value) ? row.Location : " - ";

                                string initial_time = row.InitialTime.ToString(format);
                                string final_time = row.FinalTime.ToString(format);
                                string interval = row.Interval.ToString();
                                datetime = row.DateTime.ToString("dd.MM.yyyy HH:mm:ss");

                                double distance = 0.0;
                                double fuelbegin = 0.0;
                                double fuelend = 0.0;
                                double totalfueluse = 0.0;
                                double fueluseavg = 0.0;

                                if (row["Distance"] != DBNull.Value)
                                    distance = Math.Round(row.Distance, 2);

                                if (row["FuelBegin"] != DBNull.Value)
                                    fuelbegin = Math.Round(row.FuelBegin, 2);

                                if (row["FuelEnd"] != DBNull.Value)
                                    fuelend = Math.Round(row.FuelEnd, 2);

                                if (row["TotalFuelUse"] != DBNull.Value)
                                    totalfueluse = Math.Round(row.TotalFuelUse, 2);

                                if (row["FuelUseAvg"] != DBNull.Value)
                                    fueluseavg = Math.Round(row.FuelUseAvg, 2);

                                double normalfuel100 = row.NormalFuel100;

                                double fuelavgnormal = 0.0;
                                if (row["FuelAvgNormal"] != DBNull.Value)
                                    fuelavgnormal = Math.Round(row.FuelAvgNormal, 2);

                                double deviationfuel = 0.0;
                                if (row["DeviationFuel"] != DBNull.Value)
                                    deviationfuel = Math.Round(row.DeviationFuel, 2);

                                ReportingKlmtFuel.AddDataToBindList(new reportKlmtFuel(state, location, datetime,
                                    initial_time,
                                    final_time, interval, distance, fuelbegin, fuelend, totalfueluse, fueluseavg,
                                    normalfuel100, fuelavgnormal,
                                    deviationfuel));
                            }
                            catch (Exception ee)
                            {
                                XtraMessageBox.Show(ee.Message + "\n" + ee.StackTrace,
                                    "Error import report! MobitelID=" + mobitelId + ", datetime=" + datetime + " Try again.",
                                    MessageBoxButtons.OK);
                            }
                        } // foreach 1

                        ReportingKlmtFuel.CreateElementReport();
                    }
                    catch (Exception er)
                    {
                        XtraMessageBox.Show(er.Message + "\n" + er.StackTrace, "Error import report for mobitelID= " + mobitelId + "! Try again.",
                            MessageBoxButtons.OK);
                    }
                } // foreach 2

                ReportingKlmtFuel.CreateAndShowReport();
                ReportingKlmtFuel.DeleteData();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message + "\n" + ex.StackTrace, "Error import reports! Try again.",
                    MessageBoxButtons.OK);
            }
        }

        // формирование колонтитулов верхних отчета
        private void composLink_CreateMarginalHeaderArea(object sender, CreateAreaEventArgs e)
        {
            DevExpressReportHeader(Resources.DetailReportTravel, e);
            TInfo info = ReportingKlmtFuel.GetInfoStructure;
            string strPeriod = Resources.PeriodFrom + " " + info.periodBeging + " " +
                Resources.PeriodTo + " " + info.periodEnd;
            DevExpressReportSubHeader(strPeriod, 22, e);
        }

        /* функция для формирования левой части заголовка отчета */
        protected string GetStringBreackLeft()
        {
            TInfo info = ReportingKlmtFuel.GetInfoStructure;
            return (Resources.Vehicle + ": " + info.infoVehicle + "\n" +
                Resources.Driver + ": " + info.infoDriverName);
            return "";
        }

        /* функция для формирования верхней/средней части заголовка отчета*/
        protected string GetStringBreackUp()
        {
            // to do
            return ("");
        }

        /* функция для формирования правой части заголовка отчета */
        protected string GetStringBreackRight()
        {
            TInfo info = ReportingKlmtFuel.GetInfoStructure;
            return ("Общий пробег, км:" + String.Format("{0:f2}", info.distance) + "\n" +
                "Общий расход, л:" + String.Format("{0:f2}", info.totalfuel) + "\n" +
                "Расход, л на 100км:" + String.Format("{0:f2}", info.fuelavg) + "\n" +
                "Расход по норме, в л 100км:" + String.Format("{0:f2}", info.fuelavgnorma) + "\n" +
                "Отклонение, в л:" + String.Format("{0:f2}", info.deviation));
            return "";
        }

        protected override void barButtonGroupPanel_ItemClick(object sender, ItemClickEventArgs e)
        {
            GroupPanel(gridView1);
        }

        protected override void barButtonFooterPanel_ItemClick(object sender, ItemClickEventArgs e)
        {
            FooterPanel(gridView1);
        }

        protected override void barButtonNavigator_ItemClick(object sender, ItemClickEventArgs e)
        {
            NavigatorPanel(gridControl1);
        }

        protected override void barButtonStatusPanel_ItemClick(object sender, ItemClickEventArgs e)
        {
            StatusBar(bar3);
        }

        protected override void comboAlgoChoiser_ItemClick(object sender, EventArgs e)
        {
            if (curMrow == null)
                return;

            ComboBoxEdit combo = (ComboBoxEdit)sender;
            comboAlgorithm = combo;
            int index = combo.SelectedIndex;
            int code = -1;

            if (index == 0)
            {
                code = (int)AlgorithmType.FUEL1;
                globalCode = code;
                ApdateCommandStrip(curMrow.Mobitel_ID, globalCode);
                atlantaDataSetBindingSource.Filter = "MobitelId=" + curMrow.Mobitel_ID + " and " + "TypeAlgorythm=" + code;
                SelectGraphic(curMrow, code);
            }
            else if (index == 1)
            {
                code = (int)AlgorithmType.FUEL1_AGREGAT;
                globalCode = code;
                ApdateCommandStrip(curMrow.Mobitel_ID, globalCode);
                atlantaDataSetBindingSource.Filter = "MobitelId=" + curMrow.Mobitel_ID + " and " + "TypeAlgorythm=" + code;
                SelectGraphic(curMrow, code);
            }
            else if (index == 2)
            {
                code = (int)AlgorithmType.FUEL2_AGREGAT;
                globalCode = code;
                ApdateCommandStrip(curMrow.Mobitel_ID, globalCode);
                atlantaDataSetBindingSource.Filter = "MobitelId=" + curMrow.Mobitel_ID + " and " + "TypeAlgorythm=" + code;
                SelectGraphic(curMrow, code);
            }
            else if (index == 3)
            {
                code = (int)AlgorithmType.FUEL3_AGREGAT;
                globalCode = code;
                ApdateCommandStrip(curMrow.Mobitel_ID, globalCode);
                atlantaDataSetBindingSource.Filter = "MobitelId=" + curMrow.Mobitel_ID + " and " + "TypeAlgorythm=" + code;
                SelectGraphic(curMrow, code);
            }
            else if (index == 4)
            {
                code = (int)AlgorithmType.FUEL_AGREGAT_ALL;
                globalCode = code;
                ApdateCommandStrip(curMrow.Mobitel_ID, globalCode);
                atlantaDataSetBindingSource.Filter = "MobitelId=" + curMrow.Mobitel_ID + " and " + "TypeAlgorythm=" + code;
                SelectGraphic(curMrow, code);
            }
            else
            {
                XtraMessageBox.Show("Неизвестный алгоритм", "Выбор алгоритма", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        } // comboAlgoChoiser_ItemClick
    } // Kilometrage
}
