﻿namespace ReportsOnFuelExpenses
{
    partial class FuelExpensesControlDrt
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FuelExpensesControlDrt));
            this.gcFuelCntrlDrt = new DevExpress.XtraGrid.GridControl();
            this.fuelExpensesBindingSource = new System.Windows.Forms.BindingSource();
            this.fuelExpensesDataSet = new ReportsOnFuelExpenses.FuelExpensesDataSet();
            this.gvFuelCntrlDrt = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colNameCar = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFIO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTravel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimeMoving = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colFuelUseInMotion = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFuelMiddleInMotion = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimeStopWithEngineOn = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colFuelUseOnStop = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFuelMiddleOnStop = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalFuelUse = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalMiddleFuelUseOneTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.printingSystem1 = new DevExpress.XtraPrinting.PrintingSystem();
            this.compositeReportLink = new DevExpress.XtraPrintingLinks.CompositeLink();
            this.atlantaDataSetBindingSource = new System.Windows.Forms.BindingSource();
            ((System.ComponentModel.ISupportInitialize)(this.gcFuelCntrlDrt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fuelExpensesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fuelExpensesDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvFuelCntrlDrt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.compositeReportLink.ImageCollection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.atlantaDataSetBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // gcFuelCntrlDrt
            // 
            this.gcFuelCntrlDrt.DataSource = this.fuelExpensesBindingSource;
            this.gcFuelCntrlDrt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcFuelCntrlDrt.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gcFuelCntrlDrt.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gcFuelCntrlDrt.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gcFuelCntrlDrt.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gcFuelCntrlDrt.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gcFuelCntrlDrt.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gcFuelCntrlDrt.Location = new System.Drawing.Point(0, 24);
            this.gcFuelCntrlDrt.MainView = this.gvFuelCntrlDrt;
            this.gcFuelCntrlDrt.Name = "gcFuelCntrlDrt";
            this.gcFuelCntrlDrt.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit1,
            this.repositoryItemTextEdit2,
            this.repositoryItemTextEdit3});
            this.gcFuelCntrlDrt.Size = new System.Drawing.Size(811, 448);
            this.gcFuelCntrlDrt.TabIndex = 11;
            this.gcFuelCntrlDrt.UseEmbeddedNavigator = true;
            this.gcFuelCntrlDrt.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvFuelCntrlDrt,
            this.gridView2});
            // 
            // fuelExpensesBindingSource
            // 
            this.fuelExpensesBindingSource.DataMember = "FuelExpensesDrt_Total";
            this.fuelExpensesBindingSource.DataSource = this.fuelExpensesDataSet;
            // 
            // fuelExpensesDataSet
            // 
            this.fuelExpensesDataSet.DataSetName = "FuelExpensesDrt_Total";
            this.fuelExpensesDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gvFuelCntrlDrt
            // 
            this.gvFuelCntrlDrt.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.DarkGray;
            this.gvFuelCntrlDrt.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.DarkGray;
            this.gvFuelCntrlDrt.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.DimGray;
            this.gvFuelCntrlDrt.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvFuelCntrlDrt.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvFuelCntrlDrt.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvFuelCntrlDrt.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.DarkGray;
            this.gvFuelCntrlDrt.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.DarkGray;
            this.gvFuelCntrlDrt.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Gainsboro;
            this.gvFuelCntrlDrt.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvFuelCntrlDrt.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvFuelCntrlDrt.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvFuelCntrlDrt.Appearance.Empty.BackColor = System.Drawing.Color.DimGray;
            this.gvFuelCntrlDrt.Appearance.Empty.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.gvFuelCntrlDrt.Appearance.Empty.Options.UseBackColor = true;
            this.gvFuelCntrlDrt.Appearance.EvenRow.BackColor = System.Drawing.Color.White;
            this.gvFuelCntrlDrt.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvFuelCntrlDrt.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.Gray;
            this.gvFuelCntrlDrt.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.Gray;
            this.gvFuelCntrlDrt.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvFuelCntrlDrt.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvFuelCntrlDrt.Appearance.FilterPanel.BackColor = System.Drawing.Color.Gray;
            this.gvFuelCntrlDrt.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvFuelCntrlDrt.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvFuelCntrlDrt.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvFuelCntrlDrt.Appearance.FocusedRow.BackColor = System.Drawing.Color.LightSlateGray;
            this.gvFuelCntrlDrt.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gvFuelCntrlDrt.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvFuelCntrlDrt.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvFuelCntrlDrt.Appearance.FooterPanel.BackColor = System.Drawing.Color.DarkGray;
            this.gvFuelCntrlDrt.Appearance.FooterPanel.BorderColor = System.Drawing.Color.DarkGray;
            this.gvFuelCntrlDrt.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvFuelCntrlDrt.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvFuelCntrlDrt.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.gvFuelCntrlDrt.Appearance.FooterPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gvFuelCntrlDrt.Appearance.FooterPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gvFuelCntrlDrt.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.gvFuelCntrlDrt.Appearance.GroupButton.BackColor = System.Drawing.Color.Silver;
            this.gvFuelCntrlDrt.Appearance.GroupButton.BorderColor = System.Drawing.Color.Silver;
            this.gvFuelCntrlDrt.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvFuelCntrlDrt.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvFuelCntrlDrt.Appearance.GroupFooter.BackColor = System.Drawing.Color.Silver;
            this.gvFuelCntrlDrt.Appearance.GroupFooter.BorderColor = System.Drawing.Color.Silver;
            this.gvFuelCntrlDrt.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvFuelCntrlDrt.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvFuelCntrlDrt.Appearance.GroupPanel.BackColor = System.Drawing.Color.DimGray;
            this.gvFuelCntrlDrt.Appearance.GroupPanel.ForeColor = System.Drawing.Color.White;
            this.gvFuelCntrlDrt.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvFuelCntrlDrt.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvFuelCntrlDrt.Appearance.GroupRow.BackColor = System.Drawing.Color.Silver;
            this.gvFuelCntrlDrt.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.gvFuelCntrlDrt.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvFuelCntrlDrt.Appearance.GroupRow.Options.UseFont = true;
            this.gvFuelCntrlDrt.Appearance.HeaderPanel.BackColor = System.Drawing.Color.DarkGray;
            this.gvFuelCntrlDrt.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.DarkGray;
            this.gvFuelCntrlDrt.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvFuelCntrlDrt.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvFuelCntrlDrt.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightSlateGray;
            this.gvFuelCntrlDrt.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvFuelCntrlDrt.Appearance.HorzLine.BackColor = System.Drawing.Color.LightGray;
            this.gvFuelCntrlDrt.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvFuelCntrlDrt.Appearance.OddRow.BackColor = System.Drawing.Color.WhiteSmoke;
            this.gvFuelCntrlDrt.Appearance.OddRow.Options.UseBackColor = true;
            this.gvFuelCntrlDrt.Appearance.Preview.BackColor = System.Drawing.Color.Gainsboro;
            this.gvFuelCntrlDrt.Appearance.Preview.ForeColor = System.Drawing.Color.DimGray;
            this.gvFuelCntrlDrt.Appearance.Preview.Options.UseBackColor = true;
            this.gvFuelCntrlDrt.Appearance.Preview.Options.UseForeColor = true;
            this.gvFuelCntrlDrt.Appearance.Row.BackColor = System.Drawing.Color.White;
            this.gvFuelCntrlDrt.Appearance.Row.Options.UseBackColor = true;
            this.gvFuelCntrlDrt.Appearance.RowSeparator.BackColor = System.Drawing.Color.DimGray;
            this.gvFuelCntrlDrt.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvFuelCntrlDrt.Appearance.SelectedRow.BackColor = System.Drawing.Color.DimGray;
            this.gvFuelCntrlDrt.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvFuelCntrlDrt.Appearance.VertLine.BackColor = System.Drawing.Color.LightGray;
            this.gvFuelCntrlDrt.Appearance.VertLine.Options.UseBackColor = true;
            this.gvFuelCntrlDrt.AppearancePrint.FooterPanel.Options.UseTextOptions = true;
            this.gvFuelCntrlDrt.AppearancePrint.FooterPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gvFuelCntrlDrt.AppearancePrint.FooterPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gvFuelCntrlDrt.AppearancePrint.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.gvFuelCntrlDrt.BestFitMaxRowCount = 2;
            this.gvFuelCntrlDrt.ColumnPanelRowHeight = 40;
            this.gvFuelCntrlDrt.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colNameCar,
            this.colFIO,
            this.colTravel,
            this.colTimeMoving,
            this.colFuelUseInMotion,
            this.colFuelMiddleInMotion,
            this.colTimeStopWithEngineOn,
            this.colFuelUseOnStop,
            this.colFuelMiddleOnStop,
            this.colTotalFuelUse,
            this.colTotalMiddleFuelUseOneTime});
            this.gvFuelCntrlDrt.GridControl = this.gcFuelCntrlDrt;
            this.gvFuelCntrlDrt.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always;
            this.gvFuelCntrlDrt.Name = "gvFuelCntrlDrt";
            this.gvFuelCntrlDrt.OptionsDetail.AllowZoomDetail = false;
            this.gvFuelCntrlDrt.OptionsDetail.EnableMasterViewMode = false;
            this.gvFuelCntrlDrt.OptionsDetail.ShowDetailTabs = false;
            this.gvFuelCntrlDrt.OptionsDetail.SmartDetailExpand = false;
            this.gvFuelCntrlDrt.OptionsSelection.MultiSelect = true;
            this.gvFuelCntrlDrt.OptionsView.ColumnAutoWidth = false;
            this.gvFuelCntrlDrt.OptionsView.EnableAppearanceEvenRow = true;
            this.gvFuelCntrlDrt.OptionsView.EnableAppearanceOddRow = true;
            this.gvFuelCntrlDrt.OptionsView.ShowFooter = true;
            // 
            // colNameCar
            // 
            this.colNameCar.AppearanceCell.Options.UseTextOptions = true;
            this.colNameCar.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colNameCar.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colNameCar.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colNameCar.AppearanceHeader.Options.UseTextOptions = true;
            this.colNameCar.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNameCar.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colNameCar.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colNameCar.Caption = "Название машины";
            this.colNameCar.FieldName = "NameCar";
            this.colNameCar.ImageAlignment = System.Drawing.StringAlignment.Center;
            this.colNameCar.MinWidth = 10;
            this.colNameCar.Name = "colNameCar";
            this.colNameCar.OptionsColumn.AllowEdit = false;
            this.colNameCar.OptionsColumn.AllowFocus = false;
            this.colNameCar.OptionsColumn.FixedWidth = true;
            this.colNameCar.OptionsColumn.ReadOnly = true;
            this.colNameCar.ToolTip = "Название машины";
            this.colNameCar.Visible = true;
            this.colNameCar.VisibleIndex = 0;
            this.colNameCar.Width = 100;
            // 
            // colFIO
            // 
            this.colFIO.AppearanceCell.Options.UseTextOptions = true;
            this.colFIO.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colFIO.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colFIO.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFIO.AppearanceHeader.Options.UseTextOptions = true;
            this.colFIO.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFIO.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colFIO.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFIO.Caption = "ФИО водителя";
            this.colFIO.FieldName = "FIO";
            this.colFIO.MinWidth = 10;
            this.colFIO.Name = "colFIO";
            this.colFIO.OptionsColumn.AllowEdit = false;
            this.colFIO.OptionsColumn.AllowFocus = false;
            this.colFIO.OptionsColumn.FixedWidth = true;
            this.colFIO.OptionsColumn.ReadOnly = true;
            this.colFIO.ToolTip = "ФИО водителя";
            this.colFIO.Visible = true;
            this.colFIO.VisibleIndex = 1;
            this.colFIO.Width = 80;
            // 
            // colTravel
            // 
            this.colTravel.AppearanceCell.Options.UseTextOptions = true;
            this.colTravel.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTravel.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTravel.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTravel.AppearanceHeader.Options.UseTextOptions = true;
            this.colTravel.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTravel.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTravel.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTravel.Caption = "Пройденный путь, км";
            this.colTravel.DisplayFormat.FormatString = "{0:f2}";
            this.colTravel.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTravel.FieldName = "Travel";
            this.colTravel.MinWidth = 10;
            this.colTravel.Name = "colTravel";
            this.colTravel.OptionsColumn.AllowEdit = false;
            this.colTravel.OptionsColumn.AllowFocus = false;
            this.colTravel.OptionsColumn.FixedWidth = true;
            this.colTravel.OptionsColumn.ReadOnly = true;
            this.colTravel.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Travel", "{0:f2}")});
            this.colTravel.ToolTip = "Пройденный путь, км";
            this.colTravel.Visible = true;
            this.colTravel.VisibleIndex = 2;
            this.colTravel.Width = 80;
            // 
            // colTimeMoving
            // 
            this.colTimeMoving.AppearanceCell.Options.UseTextOptions = true;
            this.colTimeMoving.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeMoving.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTimeMoving.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTimeMoving.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeMoving.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeMoving.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTimeMoving.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTimeMoving.Caption = "Время движения, ч";
            this.colTimeMoving.ColumnEdit = this.repositoryItemTextEdit2;
            this.colTimeMoving.FieldName = "TimeMovingStr";
            this.colTimeMoving.MinWidth = 10;
            this.colTimeMoving.Name = "colTimeMoving";
            this.colTimeMoving.OptionsColumn.AllowEdit = false;
            this.colTimeMoving.OptionsColumn.AllowFocus = false;
            this.colTimeMoving.OptionsColumn.FixedWidth = true;
            this.colTimeMoving.OptionsColumn.ReadOnly = true;
            this.colTimeMoving.ToolTip = "Время движения, ч";
            this.colTimeMoving.Visible = true;
            this.colTimeMoving.VisibleIndex = 3;
            this.colTimeMoving.Width = 80;
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            // 
            // colFuelUseInMotion
            // 
            this.colFuelUseInMotion.AppearanceCell.Options.UseTextOptions = true;
            this.colFuelUseInMotion.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelUseInMotion.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colFuelUseInMotion.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFuelUseInMotion.AppearanceHeader.Options.UseTextOptions = true;
            this.colFuelUseInMotion.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelUseInMotion.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colFuelUseInMotion.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFuelUseInMotion.Caption = "Расход топлива в движении, л";
            this.colFuelUseInMotion.DisplayFormat.FormatString = "{0:f2}";
            this.colFuelUseInMotion.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colFuelUseInMotion.FieldName = "FuelUseInMotion";
            this.colFuelUseInMotion.MinWidth = 10;
            this.colFuelUseInMotion.Name = "colFuelUseInMotion";
            this.colFuelUseInMotion.OptionsColumn.AllowEdit = false;
            this.colFuelUseInMotion.OptionsColumn.AllowFocus = false;
            this.colFuelUseInMotion.OptionsColumn.FixedWidth = true;
            this.colFuelUseInMotion.OptionsColumn.ReadOnly = true;
            this.colFuelUseInMotion.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "FuelUseInMotion", "{0:f2}")});
            this.colFuelUseInMotion.ToolTip = "Расход топлива в движении, л";
            this.colFuelUseInMotion.Visible = true;
            this.colFuelUseInMotion.VisibleIndex = 4;
            this.colFuelUseInMotion.Width = 80;
            // 
            // colFuelMiddleInMotion
            // 
            this.colFuelMiddleInMotion.AppearanceCell.Options.UseTextOptions = true;
            this.colFuelMiddleInMotion.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelMiddleInMotion.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colFuelMiddleInMotion.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFuelMiddleInMotion.AppearanceHeader.Options.UseTextOptions = true;
            this.colFuelMiddleInMotion.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelMiddleInMotion.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colFuelMiddleInMotion.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFuelMiddleInMotion.Caption = "Средний расход в движении, л/100 км";
            this.colFuelMiddleInMotion.DisplayFormat.FormatString = "{0:f2}";
            this.colFuelMiddleInMotion.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colFuelMiddleInMotion.FieldName = "FuelMiddleInMotion";
            this.colFuelMiddleInMotion.MinWidth = 10;
            this.colFuelMiddleInMotion.Name = "colFuelMiddleInMotion";
            this.colFuelMiddleInMotion.OptionsColumn.AllowEdit = false;
            this.colFuelMiddleInMotion.OptionsColumn.AllowFocus = false;
            this.colFuelMiddleInMotion.OptionsColumn.FixedWidth = true;
            this.colFuelMiddleInMotion.OptionsColumn.ReadOnly = true;
            this.colFuelMiddleInMotion.ToolTip = "Средний расход в движении, л/100 км";
            this.colFuelMiddleInMotion.Visible = true;
            this.colFuelMiddleInMotion.VisibleIndex = 5;
            this.colFuelMiddleInMotion.Width = 80;
            // 
            // colTimeStopWithEngineOn
            // 
            this.colTimeStopWithEngineOn.AppearanceCell.Options.UseTextOptions = true;
            this.colTimeStopWithEngineOn.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeStopWithEngineOn.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTimeStopWithEngineOn.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTimeStopWithEngineOn.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeStopWithEngineOn.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeStopWithEngineOn.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTimeStopWithEngineOn.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTimeStopWithEngineOn.Caption = "Время стоянок с включенным двигателем, ч";
            this.colTimeStopWithEngineOn.ColumnEdit = this.repositoryItemTextEdit3;
            this.colTimeStopWithEngineOn.FieldName = "TimeStopWithEngineOnStr";
            this.colTimeStopWithEngineOn.Name = "colTimeStopWithEngineOn";
            this.colTimeStopWithEngineOn.OptionsColumn.AllowEdit = false;
            this.colTimeStopWithEngineOn.OptionsColumn.AllowFocus = false;
            this.colTimeStopWithEngineOn.OptionsColumn.ReadOnly = true;
            this.colTimeStopWithEngineOn.ToolTip = "Время стоянок с включенным двигателем, ч";
            this.colTimeStopWithEngineOn.Visible = true;
            this.colTimeStopWithEngineOn.VisibleIndex = 6;
            this.colTimeStopWithEngineOn.Width = 90;
            // 
            // repositoryItemTextEdit3
            // 
            this.repositoryItemTextEdit3.AutoHeight = false;
            this.repositoryItemTextEdit3.Name = "repositoryItemTextEdit3";
            // 
            // colFuelUseOnStop
            // 
            this.colFuelUseOnStop.AppearanceCell.Options.UseTextOptions = true;
            this.colFuelUseOnStop.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelUseOnStop.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colFuelUseOnStop.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFuelUseOnStop.AppearanceHeader.Options.UseTextOptions = true;
            this.colFuelUseOnStop.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelUseOnStop.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colFuelUseOnStop.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFuelUseOnStop.Caption = "Расход топлива на стоянках, л";
            this.colFuelUseOnStop.DisplayFormat.FormatString = "{0:f2}";
            this.colFuelUseOnStop.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colFuelUseOnStop.FieldName = "FuelUseOnStop";
            this.colFuelUseOnStop.MinWidth = 10;
            this.colFuelUseOnStop.Name = "colFuelUseOnStop";
            this.colFuelUseOnStop.OptionsColumn.AllowEdit = false;
            this.colFuelUseOnStop.OptionsColumn.AllowFocus = false;
            this.colFuelUseOnStop.OptionsColumn.FixedWidth = true;
            this.colFuelUseOnStop.OptionsColumn.ReadOnly = true;
            this.colFuelUseOnStop.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "FuelUseOnStop", "{0:f2}")});
            this.colFuelUseOnStop.ToolTip = "Расход топлива на стоянках, л";
            this.colFuelUseOnStop.Visible = true;
            this.colFuelUseOnStop.VisibleIndex = 7;
            this.colFuelUseOnStop.Width = 90;
            // 
            // colFuelMiddleOnStop
            // 
            this.colFuelMiddleOnStop.AppearanceCell.Options.UseTextOptions = true;
            this.colFuelMiddleOnStop.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelMiddleOnStop.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colFuelMiddleOnStop.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFuelMiddleOnStop.AppearanceHeader.Options.UseTextOptions = true;
            this.colFuelMiddleOnStop.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelMiddleOnStop.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colFuelMiddleOnStop.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFuelMiddleOnStop.Caption = "Средний расход топлива на стоянках, л/ч";
            this.colFuelMiddleOnStop.DisplayFormat.FormatString = "{0:f2}";
            this.colFuelMiddleOnStop.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colFuelMiddleOnStop.FieldName = "FuelMiddleOnStop";
            this.colFuelMiddleOnStop.MinWidth = 10;
            this.colFuelMiddleOnStop.Name = "colFuelMiddleOnStop";
            this.colFuelMiddleOnStop.OptionsColumn.AllowEdit = false;
            this.colFuelMiddleOnStop.OptionsColumn.AllowFocus = false;
            this.colFuelMiddleOnStop.OptionsColumn.FixedWidth = true;
            this.colFuelMiddleOnStop.OptionsColumn.ReadOnly = true;
            this.colFuelMiddleOnStop.ToolTip = "Средний расход топлива на стоянках, л/ч";
            this.colFuelMiddleOnStop.Visible = true;
            this.colFuelMiddleOnStop.VisibleIndex = 8;
            this.colFuelMiddleOnStop.Width = 80;
            // 
            // colTotalFuelUse
            // 
            this.colTotalFuelUse.AppearanceCell.Options.UseTextOptions = true;
            this.colTotalFuelUse.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTotalFuelUse.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTotalFuelUse.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTotalFuelUse.AppearanceHeader.Options.UseTextOptions = true;
            this.colTotalFuelUse.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTotalFuelUse.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTotalFuelUse.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTotalFuelUse.Caption = "Общий расход, л";
            this.colTotalFuelUse.DisplayFormat.FormatString = "{0:f2}";
            this.colTotalFuelUse.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTotalFuelUse.FieldName = "TotalFuelUse";
            this.colTotalFuelUse.MinWidth = 10;
            this.colTotalFuelUse.Name = "colTotalFuelUse";
            this.colTotalFuelUse.OptionsColumn.AllowEdit = false;
            this.colTotalFuelUse.OptionsColumn.AllowFocus = false;
            this.colTotalFuelUse.OptionsColumn.FixedWidth = true;
            this.colTotalFuelUse.OptionsColumn.ReadOnly = true;
            this.colTotalFuelUse.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalFuelUse", "{0:f2}")});
            this.colTotalFuelUse.ToolTip = "Общий расход, л";
            this.colTotalFuelUse.Visible = true;
            this.colTotalFuelUse.VisibleIndex = 9;
            this.colTotalFuelUse.Width = 90;
            // 
            // colTotalMiddleFuelUseOneTime
            // 
            this.colTotalMiddleFuelUseOneTime.AppearanceCell.Options.UseTextOptions = true;
            this.colTotalMiddleFuelUseOneTime.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTotalMiddleFuelUseOneTime.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTotalMiddleFuelUseOneTime.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTotalMiddleFuelUseOneTime.AppearanceHeader.Options.UseTextOptions = true;
            this.colTotalMiddleFuelUseOneTime.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTotalMiddleFuelUseOneTime.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTotalMiddleFuelUseOneTime.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTotalMiddleFuelUseOneTime.Caption = "Средний расход, л/моточас";
            this.colTotalMiddleFuelUseOneTime.DisplayFormat.FormatString = "{0:f2}";
            this.colTotalMiddleFuelUseOneTime.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTotalMiddleFuelUseOneTime.FieldName = "TotalMiddleFuelUseOneTime";
            this.colTotalMiddleFuelUseOneTime.MinWidth = 10;
            this.colTotalMiddleFuelUseOneTime.Name = "colTotalMiddleFuelUseOneTime";
            this.colTotalMiddleFuelUseOneTime.OptionsColumn.AllowEdit = false;
            this.colTotalMiddleFuelUseOneTime.OptionsColumn.AllowFocus = false;
            this.colTotalMiddleFuelUseOneTime.OptionsColumn.FixedWidth = true;
            this.colTotalMiddleFuelUseOneTime.OptionsColumn.ReadOnly = true;
            this.colTotalMiddleFuelUseOneTime.ToolTip = "Средний расход, л/моточас";
            this.colTotalMiddleFuelUseOneTime.Visible = true;
            this.colTotalMiddleFuelUseOneTime.VisibleIndex = 10;
            this.colTotalMiddleFuelUseOneTime.Width = 80;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.ExportMode = DevExpress.XtraEditors.Repository.ExportMode.DisplayText;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // gridView2
            // 
            this.gridView2.GridControl = this.gcFuelCntrlDrt;
            this.gridView2.Name = "gridView2";
            // 
            // printingSystem1
            // 
            this.printingSystem1.Links.AddRange(new object[] {
            this.compositeReportLink});
            // 
            // compositeReportLink
            // 
            // 
            // 
            // 
            this.compositeReportLink.ImageCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("compositeReportLink.ImageCollection.ImageStream")));
            this.compositeReportLink.Landscape = true;
            this.compositeReportLink.Margins = new System.Drawing.Printing.Margins(25, 25, 70, 25);
            this.compositeReportLink.MinMargins = new System.Drawing.Printing.Margins(25, 25, 15, 25);
            this.compositeReportLink.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.compositeReportLink.PrintingSystemBase = this.printingSystem1;
            // 
            // atlantaDataSetBindingSource
            // 
            this.atlantaDataSetBindingSource.DataSource = this.fuelExpensesBindingSource;
            // 
            // FuelExpensesControlDrt
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.Controls.Add(this.gcFuelCntrlDrt);
            this.Name = "FuelExpensesControlDrt";
            this.Size = new System.Drawing.Size(811, 472);
            this.Controls.SetChildIndex(this.gcFuelCntrlDrt, 0);
            ((System.ComponentModel.ISupportInitialize)(this.gcFuelCntrlDrt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fuelExpensesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fuelExpensesDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvFuelCntrlDrt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.compositeReportLink.ImageCollection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.atlantaDataSetBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gcFuelCntrlDrt;
        private DevExpress.XtraGrid.Views.Grid.GridView gvFuelCntrlDrt;
        private DevExpress.XtraGrid.Columns.GridColumn colNameCar;
        private DevExpress.XtraGrid.Columns.GridColumn colFIO;
        private DevExpress.XtraGrid.Columns.GridColumn colTravel;
        private DevExpress.XtraGrid.Columns.GridColumn colTimeMoving;
        private DevExpress.XtraGrid.Columns.GridColumn colFuelUseInMotion;
        private DevExpress.XtraGrid.Columns.GridColumn colFuelMiddleInMotion;
        private DevExpress.XtraGrid.Columns.GridColumn colFuelUseOnStop;
        private DevExpress.XtraGrid.Columns.GridColumn colFuelMiddleOnStop;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalFuelUse;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalMiddleFuelUseOneTime;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn colTimeStopWithEngineOn;
        private FuelExpensesDataSet fuelExpensesDataSet;
        private DevExpress.XtraPrinting.PrintingSystem printingSystem1;
        private System.Windows.Forms.BindingSource fuelExpensesBindingSource;
        protected System.Windows.Forms.BindingSource atlantaDataSetBindingSource;
        private DevExpress.XtraPrintingLinks.CompositeLink compositeReportLink;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit3;
    }
}
