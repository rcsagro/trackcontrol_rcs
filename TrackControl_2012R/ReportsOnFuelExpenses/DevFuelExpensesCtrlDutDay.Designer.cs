﻿namespace ReportsOnFuelExpenses
{
    partial class DevFuelExpensesCtrlDutDay
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DevFuelExpensesCtrlDutDay));
            this.gcFuelCtrlDutDay = new DevExpress.XtraGrid.GridControl();
            this.gvFuelCtrlDutDay = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colInitialTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBeginMotionTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEndMotionTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIntervalWork = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIntervalMove = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIntervalStop = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimeWithEngineOn = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDistance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFuelBegin = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFuelEnd = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalFuelAdd = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalFuelSub = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalFuelUse = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFuelUseMiddleInMotion = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIdleFuelRate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.statusStrip1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.lblTotalDebitText = new DevExpress.XtraBars.BarStaticItem();
            this.lblTotalFuelAddText = new DevExpress.XtraBars.BarStaticItem();
            this.lblTotalFuelSubText = new DevExpress.XtraBars.BarStaticItem();
            this.lblAvgFuelDebitText = new DevExpress.XtraBars.BarStaticItem();
            this.lblTotalMotoText = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.fuelExpensesDataSet = new ReportsOnFuelExpenses.FuelExpensesDataSet();
            this.atlantaDataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.printingSystem1 = new DevExpress.XtraPrinting.PrintingSystem(this.components);
            this.compositeReportLink = new DevExpress.XtraPrintingLinks.CompositeLink(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.gcFuelCtrlDutDay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvFuelCtrlDutDay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.statusStrip1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fuelExpensesDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.atlantaDataSetBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.compositeReportLink.ImageCollection)).BeginInit();
            this.SuspendLayout();
            // 
            // gcFuelCtrlDutDay
            // 
            this.gcFuelCtrlDutDay.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcFuelCtrlDutDay.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gcFuelCtrlDutDay.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gcFuelCtrlDutDay.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gcFuelCtrlDutDay.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gcFuelCtrlDutDay.EmbeddedNavigator.Buttons.Remove.Enabled = false;
            this.gcFuelCtrlDutDay.EmbeddedNavigator.Buttons.Remove.Visible = false;
            this.gcFuelCtrlDutDay.Location = new System.Drawing.Point(0, 26);
            this.gcFuelCtrlDutDay.MainView = this.gvFuelCtrlDutDay;
            this.gcFuelCtrlDutDay.Name = "gcFuelCtrlDutDay";
            this.gcFuelCtrlDutDay.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit1});
            this.gcFuelCtrlDutDay.Size = new System.Drawing.Size(821, 443);
            this.gcFuelCtrlDutDay.TabIndex = 11;
            this.gcFuelCtrlDutDay.UseEmbeddedNavigator = true;
            this.gcFuelCtrlDutDay.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvFuelCtrlDutDay,
            this.gridView2});
            // 
            // gvFuelCtrlDutDay
            // 
            this.gvFuelCtrlDutDay.Appearance.ColumnFilterButton.BackColor = System.Drawing.Color.DarkGray;
            this.gvFuelCtrlDutDay.Appearance.ColumnFilterButton.BorderColor = System.Drawing.Color.DarkGray;
            this.gvFuelCtrlDutDay.Appearance.ColumnFilterButton.ForeColor = System.Drawing.Color.DimGray;
            this.gvFuelCtrlDutDay.Appearance.ColumnFilterButton.Options.UseBackColor = true;
            this.gvFuelCtrlDutDay.Appearance.ColumnFilterButton.Options.UseBorderColor = true;
            this.gvFuelCtrlDutDay.Appearance.ColumnFilterButton.Options.UseForeColor = true;
            this.gvFuelCtrlDutDay.Appearance.ColumnFilterButtonActive.BackColor = System.Drawing.Color.DarkGray;
            this.gvFuelCtrlDutDay.Appearance.ColumnFilterButtonActive.BorderColor = System.Drawing.Color.DarkGray;
            this.gvFuelCtrlDutDay.Appearance.ColumnFilterButtonActive.ForeColor = System.Drawing.Color.Gainsboro;
            this.gvFuelCtrlDutDay.Appearance.ColumnFilterButtonActive.Options.UseBackColor = true;
            this.gvFuelCtrlDutDay.Appearance.ColumnFilterButtonActive.Options.UseBorderColor = true;
            this.gvFuelCtrlDutDay.Appearance.ColumnFilterButtonActive.Options.UseForeColor = true;
            this.gvFuelCtrlDutDay.Appearance.Empty.BackColor = System.Drawing.Color.DimGray;
            this.gvFuelCtrlDutDay.Appearance.Empty.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.BackwardDiagonal;
            this.gvFuelCtrlDutDay.Appearance.Empty.Options.UseBackColor = true;
            this.gvFuelCtrlDutDay.Appearance.EvenRow.BackColor = System.Drawing.Color.White;
            this.gvFuelCtrlDutDay.Appearance.EvenRow.Options.UseBackColor = true;
            this.gvFuelCtrlDutDay.Appearance.FilterCloseButton.BackColor = System.Drawing.Color.Gray;
            this.gvFuelCtrlDutDay.Appearance.FilterCloseButton.BorderColor = System.Drawing.Color.Gray;
            this.gvFuelCtrlDutDay.Appearance.FilterCloseButton.Options.UseBackColor = true;
            this.gvFuelCtrlDutDay.Appearance.FilterCloseButton.Options.UseBorderColor = true;
            this.gvFuelCtrlDutDay.Appearance.FilterPanel.BackColor = System.Drawing.Color.Gray;
            this.gvFuelCtrlDutDay.Appearance.FilterPanel.ForeColor = System.Drawing.Color.Black;
            this.gvFuelCtrlDutDay.Appearance.FilterPanel.Options.UseBackColor = true;
            this.gvFuelCtrlDutDay.Appearance.FilterPanel.Options.UseForeColor = true;
            this.gvFuelCtrlDutDay.Appearance.FocusedRow.BackColor = System.Drawing.Color.LightSlateGray;
            this.gvFuelCtrlDutDay.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.gvFuelCtrlDutDay.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gvFuelCtrlDutDay.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gvFuelCtrlDutDay.Appearance.FooterPanel.BackColor = System.Drawing.Color.DarkGray;
            this.gvFuelCtrlDutDay.Appearance.FooterPanel.BorderColor = System.Drawing.Color.DarkGray;
            this.gvFuelCtrlDutDay.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gvFuelCtrlDutDay.Appearance.FooterPanel.Options.UseBorderColor = true;
            this.gvFuelCtrlDutDay.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.gvFuelCtrlDutDay.Appearance.FooterPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gvFuelCtrlDutDay.Appearance.FooterPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gvFuelCtrlDutDay.Appearance.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.gvFuelCtrlDutDay.Appearance.GroupButton.BackColor = System.Drawing.Color.Silver;
            this.gvFuelCtrlDutDay.Appearance.GroupButton.BorderColor = System.Drawing.Color.Silver;
            this.gvFuelCtrlDutDay.Appearance.GroupButton.Options.UseBackColor = true;
            this.gvFuelCtrlDutDay.Appearance.GroupButton.Options.UseBorderColor = true;
            this.gvFuelCtrlDutDay.Appearance.GroupFooter.BackColor = System.Drawing.Color.Silver;
            this.gvFuelCtrlDutDay.Appearance.GroupFooter.BorderColor = System.Drawing.Color.Silver;
            this.gvFuelCtrlDutDay.Appearance.GroupFooter.Options.UseBackColor = true;
            this.gvFuelCtrlDutDay.Appearance.GroupFooter.Options.UseBorderColor = true;
            this.gvFuelCtrlDutDay.Appearance.GroupPanel.BackColor = System.Drawing.Color.DimGray;
            this.gvFuelCtrlDutDay.Appearance.GroupPanel.ForeColor = System.Drawing.Color.White;
            this.gvFuelCtrlDutDay.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gvFuelCtrlDutDay.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gvFuelCtrlDutDay.Appearance.GroupRow.BackColor = System.Drawing.Color.Silver;
            this.gvFuelCtrlDutDay.Appearance.GroupRow.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.gvFuelCtrlDutDay.Appearance.GroupRow.Options.UseBackColor = true;
            this.gvFuelCtrlDutDay.Appearance.GroupRow.Options.UseFont = true;
            this.gvFuelCtrlDutDay.Appearance.HeaderPanel.BackColor = System.Drawing.Color.DarkGray;
            this.gvFuelCtrlDutDay.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.DarkGray;
            this.gvFuelCtrlDutDay.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gvFuelCtrlDutDay.Appearance.HeaderPanel.Options.UseBorderColor = true;
            this.gvFuelCtrlDutDay.Appearance.HideSelectionRow.BackColor = System.Drawing.Color.LightSlateGray;
            this.gvFuelCtrlDutDay.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gvFuelCtrlDutDay.Appearance.HorzLine.BackColor = System.Drawing.Color.LightGray;
            this.gvFuelCtrlDutDay.Appearance.HorzLine.Options.UseBackColor = true;
            this.gvFuelCtrlDutDay.Appearance.OddRow.BackColor = System.Drawing.Color.WhiteSmoke;
            this.gvFuelCtrlDutDay.Appearance.OddRow.Options.UseBackColor = true;
            this.gvFuelCtrlDutDay.Appearance.Preview.BackColor = System.Drawing.Color.Gainsboro;
            this.gvFuelCtrlDutDay.Appearance.Preview.ForeColor = System.Drawing.Color.DimGray;
            this.gvFuelCtrlDutDay.Appearance.Preview.Options.UseBackColor = true;
            this.gvFuelCtrlDutDay.Appearance.Preview.Options.UseForeColor = true;
            this.gvFuelCtrlDutDay.Appearance.Row.BackColor = System.Drawing.Color.White;
            this.gvFuelCtrlDutDay.Appearance.Row.Options.UseBackColor = true;
            this.gvFuelCtrlDutDay.Appearance.RowSeparator.BackColor = System.Drawing.Color.DimGray;
            this.gvFuelCtrlDutDay.Appearance.RowSeparator.Options.UseBackColor = true;
            this.gvFuelCtrlDutDay.Appearance.SelectedRow.BackColor = System.Drawing.Color.DimGray;
            this.gvFuelCtrlDutDay.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gvFuelCtrlDutDay.Appearance.VertLine.BackColor = System.Drawing.Color.LightGray;
            this.gvFuelCtrlDutDay.Appearance.VertLine.Options.UseBackColor = true;
            this.gvFuelCtrlDutDay.AppearancePrint.FooterPanel.Options.UseTextOptions = true;
            this.gvFuelCtrlDutDay.AppearancePrint.FooterPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gvFuelCtrlDutDay.AppearancePrint.FooterPanel.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gvFuelCtrlDutDay.AppearancePrint.FooterPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.gvFuelCtrlDutDay.BestFitMaxRowCount = 2;
            this.gvFuelCtrlDutDay.ColumnPanelRowHeight = 40;
            this.gvFuelCtrlDutDay.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colInitialTime,
            this.colBeginMotionTime,
            this.colEndMotionTime,
            this.colIntervalWork,
            this.colIntervalMove,
            this.colIntervalStop,
            this.colTimeWithEngineOn,
            this.colDistance,
            this.colFuelBegin,
            this.colFuelEnd,
            this.colTotalFuelAdd,
            this.colTotalFuelSub,
            this.colTotalFuelUse,
            this.colFuelUseMiddleInMotion,
            this.colIdleFuelRate});
            this.gvFuelCtrlDutDay.GridControl = this.gcFuelCtrlDutDay;
            this.gvFuelCtrlDutDay.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Distance", this.colFuelBegin, "")});
            this.gvFuelCtrlDutDay.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always;
            this.gvFuelCtrlDutDay.Name = "gvFuelCtrlDutDay";
            this.gvFuelCtrlDutDay.OptionsDetail.AllowZoomDetail = false;
            this.gvFuelCtrlDutDay.OptionsDetail.EnableMasterViewMode = false;
            this.gvFuelCtrlDutDay.OptionsDetail.ShowDetailTabs = false;
            this.gvFuelCtrlDutDay.OptionsDetail.SmartDetailExpand = false;
            this.gvFuelCtrlDutDay.OptionsSelection.MultiSelect = true;
            this.gvFuelCtrlDutDay.OptionsView.ColumnAutoWidth = false;
            this.gvFuelCtrlDutDay.OptionsView.EnableAppearanceEvenRow = true;
            this.gvFuelCtrlDutDay.OptionsView.EnableAppearanceOddRow = true;
            this.gvFuelCtrlDutDay.OptionsView.ShowFooter = true;
            // 
            // colInitialTime
            // 
            this.colInitialTime.AppearanceCell.Options.UseTextOptions = true;
            this.colInitialTime.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colInitialTime.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colInitialTime.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colInitialTime.AppearanceHeader.Options.UseTextOptions = true;
            this.colInitialTime.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colInitialTime.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colInitialTime.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colInitialTime.Caption = "Дата";
            this.colInitialTime.DisplayFormat.FormatString = "dd.MM.yyyy";
            this.colInitialTime.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colInitialTime.FieldName = "InitialTime";
            this.colInitialTime.ImageAlignment = System.Drawing.StringAlignment.Center;
            this.colInitialTime.MinWidth = 10;
            this.colInitialTime.Name = "colInitialTime";
            this.colInitialTime.OptionsColumn.AllowEdit = false;
            this.colInitialTime.OptionsColumn.AllowFocus = false;
            this.colInitialTime.OptionsColumn.FixedWidth = true;
            this.colInitialTime.OptionsColumn.ReadOnly = true;
            this.colInitialTime.ToolTip = "Дата";
            this.colInitialTime.Visible = true;
            this.colInitialTime.VisibleIndex = 0;
            this.colInitialTime.Width = 74;
            // 
            // colBeginMotionTime
            // 
            this.colBeginMotionTime.AppearanceCell.Options.UseTextOptions = true;
            this.colBeginMotionTime.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBeginMotionTime.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colBeginMotionTime.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBeginMotionTime.AppearanceHeader.Options.UseTextOptions = true;
            this.colBeginMotionTime.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colBeginMotionTime.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colBeginMotionTime.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colBeginMotionTime.Caption = "Время начала движения";
            this.colBeginMotionTime.DisplayFormat.FormatString = "HH:mm";
            this.colBeginMotionTime.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colBeginMotionTime.FieldName = "InitialTime";
            this.colBeginMotionTime.MinWidth = 10;
            this.colBeginMotionTime.Name = "colBeginMotionTime";
            this.colBeginMotionTime.OptionsColumn.AllowEdit = false;
            this.colBeginMotionTime.OptionsColumn.AllowFocus = false;
            this.colBeginMotionTime.OptionsColumn.FixedWidth = true;
            this.colBeginMotionTime.OptionsColumn.ReadOnly = true;
            this.colBeginMotionTime.ToolTip = "Время начала движения";
            this.colBeginMotionTime.Visible = true;
            this.colBeginMotionTime.VisibleIndex = 1;
            this.colBeginMotionTime.Width = 60;
            // 
            // colEndMotionTime
            // 
            this.colEndMotionTime.AppearanceCell.Options.UseTextOptions = true;
            this.colEndMotionTime.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colEndMotionTime.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colEndMotionTime.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colEndMotionTime.AppearanceHeader.Options.UseTextOptions = true;
            this.colEndMotionTime.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colEndMotionTime.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colEndMotionTime.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colEndMotionTime.Caption = "Время окончания движения";
            this.colEndMotionTime.DisplayFormat.FormatString = "HH:mm";
            this.colEndMotionTime.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colEndMotionTime.FieldName = "FinalTime";
            this.colEndMotionTime.MinWidth = 10;
            this.colEndMotionTime.Name = "colEndMotionTime";
            this.colEndMotionTime.OptionsColumn.AllowEdit = false;
            this.colEndMotionTime.OptionsColumn.AllowFocus = false;
            this.colEndMotionTime.OptionsColumn.FixedWidth = true;
            this.colEndMotionTime.OptionsColumn.ReadOnly = true;
            this.colEndMotionTime.ToolTip = "Время окончания движения";
            this.colEndMotionTime.Visible = true;
            this.colEndMotionTime.VisibleIndex = 2;
            this.colEndMotionTime.Width = 60;
            // 
            // colIntervalWork
            // 
            this.colIntervalWork.AppearanceCell.Options.UseTextOptions = true;
            this.colIntervalWork.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIntervalWork.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colIntervalWork.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colIntervalWork.AppearanceHeader.Options.UseTextOptions = true;
            this.colIntervalWork.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIntervalWork.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colIntervalWork.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colIntervalWork.Caption = "Продолжительность смены";
            this.colIntervalWork.DisplayFormat.FormatString = "HH:mm";
            this.colIntervalWork.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colIntervalWork.FieldName = "IntervalWork";
            this.colIntervalWork.MinWidth = 10;
            this.colIntervalWork.Name = "colIntervalWork";
            this.colIntervalWork.OptionsColumn.AllowEdit = false;
            this.colIntervalWork.OptionsColumn.AllowFocus = false;
            this.colIntervalWork.OptionsColumn.FixedWidth = true;
            this.colIntervalWork.OptionsColumn.ReadOnly = true;
            this.colIntervalWork.ToolTip = "Продолжительность смены";
            this.colIntervalWork.Visible = true;
            this.colIntervalWork.VisibleIndex = 3;
            this.colIntervalWork.Width = 60;
            // 
            // colIntervalMove
            // 
            this.colIntervalMove.AppearanceCell.Options.UseTextOptions = true;
            this.colIntervalMove.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIntervalMove.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colIntervalMove.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colIntervalMove.AppearanceHeader.Options.UseTextOptions = true;
            this.colIntervalMove.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIntervalMove.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colIntervalMove.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colIntervalMove.Caption = "Общее время движения";
            this.colIntervalMove.DisplayFormat.FormatString = "HH:mm";
            this.colIntervalMove.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colIntervalMove.FieldName = "IntervalMove";
            this.colIntervalMove.MinWidth = 10;
            this.colIntervalMove.Name = "colIntervalMove";
            this.colIntervalMove.OptionsColumn.AllowEdit = false;
            this.colIntervalMove.OptionsColumn.AllowFocus = false;
            this.colIntervalMove.OptionsColumn.FixedWidth = true;
            this.colIntervalMove.OptionsColumn.ReadOnly = true;
            this.colIntervalMove.ToolTip = "Общее время движения";
            this.colIntervalMove.Visible = true;
            this.colIntervalMove.VisibleIndex = 4;
            this.colIntervalMove.Width = 60;
            // 
            // colIntervalStop
            // 
            this.colIntervalStop.AppearanceCell.Options.UseTextOptions = true;
            this.colIntervalStop.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIntervalStop.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colIntervalStop.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colIntervalStop.AppearanceHeader.Options.UseTextOptions = true;
            this.colIntervalStop.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIntervalStop.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colIntervalStop.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colIntervalStop.Caption = "Общее время стоянок";
            this.colIntervalStop.DisplayFormat.FormatString = "HH:mm";
            this.colIntervalStop.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colIntervalStop.FieldName = "IntervalStop";
            this.colIntervalStop.MinWidth = 10;
            this.colIntervalStop.Name = "colIntervalStop";
            this.colIntervalStop.OptionsColumn.AllowEdit = false;
            this.colIntervalStop.OptionsColumn.AllowFocus = false;
            this.colIntervalStop.OptionsColumn.FixedWidth = true;
            this.colIntervalStop.OptionsColumn.ReadOnly = true;
            this.colIntervalStop.ToolTip = "Общее время стоянок";
            this.colIntervalStop.Visible = true;
            this.colIntervalStop.VisibleIndex = 5;
            this.colIntervalStop.Width = 60;
            // 
            // colTimeWithEngineOn
            // 
            this.colTimeWithEngineOn.AppearanceCell.Options.UseTextOptions = true;
            this.colTimeWithEngineOn.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeWithEngineOn.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTimeWithEngineOn.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTimeWithEngineOn.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeWithEngineOn.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeWithEngineOn.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTimeWithEngineOn.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTimeWithEngineOn.Caption = "Время работы с включенным двигателем";
            this.colTimeWithEngineOn.DisplayFormat.FormatString = "HH:mm";
            this.colTimeWithEngineOn.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colTimeWithEngineOn.FieldName = "TimeWithEngineOn";
            this.colTimeWithEngineOn.MinWidth = 10;
            this.colTimeWithEngineOn.Name = "colTimeWithEngineOn";
            this.colTimeWithEngineOn.OptionsColumn.AllowEdit = false;
            this.colTimeWithEngineOn.OptionsColumn.AllowFocus = false;
            this.colTimeWithEngineOn.OptionsColumn.FixedWidth = true;
            this.colTimeWithEngineOn.OptionsColumn.ReadOnly = true;
            this.colTimeWithEngineOn.ToolTip = "Время работы с включенным двигателем";
            this.colTimeWithEngineOn.Visible = true;
            this.colTimeWithEngineOn.VisibleIndex = 6;
            this.colTimeWithEngineOn.Width = 60;
            // 
            // colDistance
            // 
            this.colDistance.AppearanceCell.Options.UseTextOptions = true;
            this.colDistance.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDistance.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colDistance.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDistance.AppearanceHeader.Options.UseTextOptions = true;
            this.colDistance.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDistance.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colDistance.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDistance.Caption = "Пройденный путь, км";
            this.colDistance.DisplayFormat.FormatString = "{0:f2}";
            this.colDistance.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colDistance.FieldName = "Distance";
            this.colDistance.MinWidth = 10;
            this.colDistance.Name = "colDistance";
            this.colDistance.OptionsColumn.AllowEdit = false;
            this.colDistance.OptionsColumn.AllowFocus = false;
            this.colDistance.OptionsColumn.FixedWidth = true;
            this.colDistance.OptionsColumn.ReadOnly = true;
            this.colDistance.SummaryItem.DisplayFormat = "{0:f2}";
            this.colDistance.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.colDistance.ToolTip = "Пройденный путь, км";
            this.colDistance.Visible = true;
            this.colDistance.VisibleIndex = 7;
            this.colDistance.Width = 60;
            // 
            // colFuelBegin
            // 
            this.colFuelBegin.AppearanceCell.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.colFuelBegin.AppearanceCell.Options.UseTextOptions = true;
            this.colFuelBegin.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelBegin.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colFuelBegin.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFuelBegin.AppearanceHeader.Options.UseTextOptions = true;
            this.colFuelBegin.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelBegin.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colFuelBegin.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFuelBegin.Caption = "Топливо в начале, л";
            this.colFuelBegin.DisplayFormat.FormatString = "{0:f2}";
            this.colFuelBegin.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colFuelBegin.FieldName = "FuelBegin";
            this.colFuelBegin.MinWidth = 10;
            this.colFuelBegin.Name = "colFuelBegin";
            this.colFuelBegin.OptionsColumn.AllowEdit = false;
            this.colFuelBegin.OptionsColumn.AllowFocus = false;
            this.colFuelBegin.OptionsColumn.FixedWidth = true;
            this.colFuelBegin.OptionsColumn.ReadOnly = true;
            this.colFuelBegin.SummaryItem.DisplayFormat = "{0:f2}";
            this.colFuelBegin.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.colFuelBegin.ToolTip = "Топливо в начале, л";
            this.colFuelBegin.Visible = true;
            this.colFuelBegin.VisibleIndex = 8;
            this.colFuelBegin.Width = 60;
            // 
            // colFuelEnd
            // 
            this.colFuelEnd.AppearanceCell.Options.UseTextOptions = true;
            this.colFuelEnd.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelEnd.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colFuelEnd.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFuelEnd.AppearanceHeader.Options.UseTextOptions = true;
            this.colFuelEnd.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelEnd.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colFuelEnd.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFuelEnd.Caption = "Топливо в конце, л";
            this.colFuelEnd.DisplayFormat.FormatString = "{0:f2}";
            this.colFuelEnd.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colFuelEnd.FieldName = "FuelEnd";
            this.colFuelEnd.MinWidth = 10;
            this.colFuelEnd.Name = "colFuelEnd";
            this.colFuelEnd.OptionsColumn.AllowEdit = false;
            this.colFuelEnd.OptionsColumn.AllowFocus = false;
            this.colFuelEnd.OptionsColumn.FixedWidth = true;
            this.colFuelEnd.OptionsColumn.ReadOnly = true;
            this.colFuelEnd.SummaryItem.DisplayFormat = "{0:f2}";
            this.colFuelEnd.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.colFuelEnd.ToolTip = "Топливо в конце, л";
            this.colFuelEnd.Visible = true;
            this.colFuelEnd.VisibleIndex = 9;
            this.colFuelEnd.Width = 60;
            // 
            // colTotalFuelAdd
            // 
            this.colTotalFuelAdd.AppearanceCell.Options.UseTextOptions = true;
            this.colTotalFuelAdd.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTotalFuelAdd.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTotalFuelAdd.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTotalFuelAdd.AppearanceHeader.Options.UseTextOptions = true;
            this.colTotalFuelAdd.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTotalFuelAdd.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTotalFuelAdd.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTotalFuelAdd.Caption = "Заправки, л";
            this.colTotalFuelAdd.DisplayFormat.FormatString = "{0:f2}";
            this.colTotalFuelAdd.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTotalFuelAdd.FieldName = "TotalFuelAdd";
            this.colTotalFuelAdd.MinWidth = 10;
            this.colTotalFuelAdd.Name = "colTotalFuelAdd";
            this.colTotalFuelAdd.OptionsColumn.AllowEdit = false;
            this.colTotalFuelAdd.OptionsColumn.AllowFocus = false;
            this.colTotalFuelAdd.OptionsColumn.FixedWidth = true;
            this.colTotalFuelAdd.OptionsColumn.ReadOnly = true;
            this.colTotalFuelAdd.SummaryItem.DisplayFormat = "{0:f2}";
            this.colTotalFuelAdd.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.colTotalFuelAdd.ToolTip = "Заправки, л";
            this.colTotalFuelAdd.Visible = true;
            this.colTotalFuelAdd.VisibleIndex = 10;
            this.colTotalFuelAdd.Width = 50;
            // 
            // colTotalFuelSub
            // 
            this.colTotalFuelSub.AppearanceCell.Options.UseTextOptions = true;
            this.colTotalFuelSub.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTotalFuelSub.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTotalFuelSub.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTotalFuelSub.AppearanceHeader.Options.UseTextOptions = true;
            this.colTotalFuelSub.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTotalFuelSub.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTotalFuelSub.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTotalFuelSub.Caption = "Сливы, л";
            this.colTotalFuelSub.DisplayFormat.FormatString = "{0:f2}";
            this.colTotalFuelSub.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTotalFuelSub.FieldName = "TotalFuelSub";
            this.colTotalFuelSub.MinWidth = 10;
            this.colTotalFuelSub.Name = "colTotalFuelSub";
            this.colTotalFuelSub.OptionsColumn.AllowEdit = false;
            this.colTotalFuelSub.OptionsColumn.AllowFocus = false;
            this.colTotalFuelSub.OptionsColumn.FixedWidth = true;
            this.colTotalFuelSub.OptionsColumn.ReadOnly = true;
            this.colTotalFuelSub.SummaryItem.DisplayFormat = "{0:f2}";
            this.colTotalFuelSub.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.colTotalFuelSub.ToolTip = "Сливы, л";
            this.colTotalFuelSub.Visible = true;
            this.colTotalFuelSub.VisibleIndex = 11;
            this.colTotalFuelSub.Width = 57;
            // 
            // colTotalFuelUse
            // 
            this.colTotalFuelUse.AppearanceCell.Options.UseTextOptions = true;
            this.colTotalFuelUse.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTotalFuelUse.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTotalFuelUse.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTotalFuelUse.AppearanceHeader.Options.UseTextOptions = true;
            this.colTotalFuelUse.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTotalFuelUse.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTotalFuelUse.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTotalFuelUse.Caption = "Общий расход, л";
            this.colTotalFuelUse.DisplayFormat.FormatString = "{0:f2}";
            this.colTotalFuelUse.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTotalFuelUse.FieldName = "TotalFuelUse";
            this.colTotalFuelUse.MinWidth = 10;
            this.colTotalFuelUse.Name = "colTotalFuelUse";
            this.colTotalFuelUse.OptionsColumn.AllowEdit = false;
            this.colTotalFuelUse.OptionsColumn.AllowFocus = false;
            this.colTotalFuelUse.OptionsColumn.FixedWidth = true;
            this.colTotalFuelUse.OptionsColumn.ReadOnly = true;
            this.colTotalFuelUse.SummaryItem.DisplayFormat = "{0:f2}";
            this.colTotalFuelUse.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum;
            this.colTotalFuelUse.ToolTip = "Общий расход, л";
            this.colTotalFuelUse.Visible = true;
            this.colTotalFuelUse.VisibleIndex = 12;
            this.colTotalFuelUse.Width = 60;
            // 
            // colFuelUseMiddleInMotion
            // 
            this.colFuelUseMiddleInMotion.AppearanceCell.Options.UseTextOptions = true;
            this.colFuelUseMiddleInMotion.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelUseMiddleInMotion.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colFuelUseMiddleInMotion.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFuelUseMiddleInMotion.AppearanceHeader.Options.UseTextOptions = true;
            this.colFuelUseMiddleInMotion.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelUseMiddleInMotion.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colFuelUseMiddleInMotion.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFuelUseMiddleInMotion.Caption = "Расход, л/100 км";
            this.colFuelUseMiddleInMotion.DisplayFormat.FormatString = "{0:f2}";
            this.colFuelUseMiddleInMotion.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colFuelUseMiddleInMotion.FieldName = "FuelUseMiddleInMotion";
            this.colFuelUseMiddleInMotion.MinWidth = 10;
            this.colFuelUseMiddleInMotion.Name = "colFuelUseMiddleInMotion";
            this.colFuelUseMiddleInMotion.OptionsColumn.AllowEdit = false;
            this.colFuelUseMiddleInMotion.OptionsColumn.AllowFocus = false;
            this.colFuelUseMiddleInMotion.OptionsColumn.FixedWidth = true;
            this.colFuelUseMiddleInMotion.OptionsColumn.ReadOnly = true;
            this.colFuelUseMiddleInMotion.ToolTip = "Расход, л/100 км";
            this.colFuelUseMiddleInMotion.Visible = true;
            this.colFuelUseMiddleInMotion.VisibleIndex = 13;
            this.colFuelUseMiddleInMotion.Width = 60;
            // 
            // colIdleFuelRate
            // 
            this.colIdleFuelRate.AppearanceCell.Options.UseTextOptions = true;
            this.colIdleFuelRate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIdleFuelRate.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colIdleFuelRate.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colIdleFuelRate.AppearanceHeader.Options.UseTextOptions = true;
            this.colIdleFuelRate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colIdleFuelRate.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colIdleFuelRate.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colIdleFuelRate.Caption = "Расход, л/моточас";
            this.colIdleFuelRate.DisplayFormat.FormatString = "{0:f2}";
            this.colIdleFuelRate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colIdleFuelRate.FieldName = "idleFuelRate";
            this.colIdleFuelRate.MinWidth = 10;
            this.colIdleFuelRate.Name = "colIdleFuelRate";
            this.colIdleFuelRate.OptionsColumn.AllowEdit = false;
            this.colIdleFuelRate.OptionsColumn.AllowFocus = false;
            this.colIdleFuelRate.OptionsColumn.FixedWidth = true;
            this.colIdleFuelRate.OptionsColumn.ReadOnly = true;
            this.colIdleFuelRate.ToolTip = "Расход, л/моточас";
            this.colIdleFuelRate.Visible = true;
            this.colIdleFuelRate.VisibleIndex = 14;
            this.colIdleFuelRate.Width = 60;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.ExportMode = DevExpress.XtraEditors.Repository.ExportMode.DisplayText;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // gridView2
            // 
            this.gridView2.GridControl = this.gcFuelCtrlDutDay;
            this.gridView2.Name = "gridView2";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar3});
            this.statusStrip1.DockControls.Add(this.barDockControl1);
            this.statusStrip1.DockControls.Add(this.barDockControl2);
            this.statusStrip1.DockControls.Add(this.barDockControl3);
            this.statusStrip1.DockControls.Add(this.barDockControl4);
            this.statusStrip1.Form = this;
            this.statusStrip1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.lblTotalDebitText,
            this.lblTotalFuelAddText,
            this.lblTotalFuelSubText,
            this.lblAvgFuelDebitText,
            this.lblTotalMotoText});
            this.statusStrip1.MaxItemId = 8;
            this.statusStrip1.StatusBar = this.bar3;
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.lblTotalDebitText),
            new DevExpress.XtraBars.LinkPersistInfo(this.lblTotalFuelAddText),
            new DevExpress.XtraBars.LinkPersistInfo(this.lblTotalFuelSubText),
            new DevExpress.XtraBars.LinkPersistInfo(this.lblAvgFuelDebitText),
            new DevExpress.XtraBars.LinkPersistInfo(this.lblTotalMotoText)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // lblTotalDebitText
            // 
            this.lblTotalDebitText.Caption = "Общий расход топлива, л: ---";
            this.lblTotalDebitText.Id = 0;
            this.lblTotalDebitText.Name = "lblTotalDebitText";
            this.lblTotalDebitText.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // lblTotalFuelAddText
            // 
            this.lblTotalFuelAddText.Caption = "Всего заправлено, л: ---";
            this.lblTotalFuelAddText.Id = 1;
            this.lblTotalFuelAddText.Name = "lblTotalFuelAddText";
            this.lblTotalFuelAddText.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // lblTotalFuelSubText
            // 
            this.lblTotalFuelSubText.Caption = "Всего слито, л: ---";
            this.lblTotalFuelSubText.Id = 5;
            this.lblTotalFuelSubText.Name = "lblTotalFuelSubText";
            this.lblTotalFuelSubText.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // lblAvgFuelDebitText
            // 
            this.lblAvgFuelDebitText.Caption = "Средний расход топлива, л/100 км: ---";
            this.lblAvgFuelDebitText.Id = 6;
            this.lblAvgFuelDebitText.Name = "lblAvgFuelDebitText";
            this.lblAvgFuelDebitText.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // lblTotalMotoText
            // 
            this.lblTotalMotoText.Caption = "Средний расход, л/моточас: ---";
            this.lblTotalMotoText.Id = 7;
            this.lblTotalMotoText.Name = "lblTotalMotoText";
            this.lblTotalMotoText.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // fuelExpensesDataSet
            // 
            this.fuelExpensesDataSet.DataSetName = "FuelExpensesDUT_Day";
            this.fuelExpensesDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // printingSystem1
            // 
            this.printingSystem1.ExportOptions.Html.ExportMode = DevExpress.XtraPrinting.HtmlExportMode.SingleFilePageByPage;
            this.printingSystem1.ExportOptions.Image.ExportMode = DevExpress.XtraPrinting.ImageExportMode.SingleFilePageByPage;
            this.printingSystem1.ExportOptions.Mht.ExportMode = DevExpress.XtraPrinting.HtmlExportMode.SingleFilePageByPage;
            this.printingSystem1.ExportOptions.Xls.ShowGridLines = true;
            this.printingSystem1.ExportOptions.Xlsx.ShowGridLines = true;
            // 
            // compositeReportLink
            // 
            // 
            // 
            // 
            this.compositeReportLink.ImageCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("compositeReportLink.ImageCollection.ImageStream")));
            this.compositeReportLink.Landscape = true;
            this.compositeReportLink.Margins = new System.Drawing.Printing.Margins(25, 25, 70, 25);
            this.compositeReportLink.MinMargins = new System.Drawing.Printing.Margins(25, 25, 15, 25);
            this.compositeReportLink.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.compositeReportLink.PrintingSystem = this.printingSystem1;
            this.compositeReportLink.PrintingSystemBase = this.printingSystem1;
            // 
            // DevFuelExpensesCtrlDutDay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.Controls.Add(this.gcFuelCtrlDutDay);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.Name = "DevFuelExpensesCtrlDutDay";
            this.Size = new System.Drawing.Size(821, 518);
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.gcFuelCtrlDutDay, 0);
            ((System.ComponentModel.ISupportInitialize)(this.gcFuelCtrlDutDay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvFuelCtrlDutDay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.statusStrip1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fuelExpensesDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.atlantaDataSetBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.compositeReportLink.ImageCollection)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gcFuelCtrlDutDay;
        private DevExpress.XtraGrid.Views.Grid.GridView gvFuelCtrlDutDay;
        private DevExpress.XtraGrid.Columns.GridColumn colInitialTime;
        private DevExpress.XtraGrid.Columns.GridColumn colBeginMotionTime;
        private DevExpress.XtraGrid.Columns.GridColumn colEndMotionTime;
        private DevExpress.XtraGrid.Columns.GridColumn colIntervalWork;
        private DevExpress.XtraGrid.Columns.GridColumn colIntervalMove;
        private DevExpress.XtraGrid.Columns.GridColumn colIntervalStop;
        private DevExpress.XtraGrid.Columns.GridColumn colTimeWithEngineOn;
        private DevExpress.XtraGrid.Columns.GridColumn colDistance;
        private DevExpress.XtraGrid.Columns.GridColumn colFuelBegin;
        private DevExpress.XtraGrid.Columns.GridColumn colFuelEnd;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalFuelAdd;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalFuelSub;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalFuelUse;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colFuelUseMiddleInMotion;
        private DevExpress.XtraGrid.Columns.GridColumn colIdleFuelRate;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraBars.BarManager statusStrip1;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem lblTotalDebitText;
        private DevExpress.XtraBars.BarStaticItem lblTotalFuelAddText;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraBars.BarStaticItem lblTotalFuelSubText;
        private DevExpress.XtraBars.BarStaticItem lblAvgFuelDebitText;
        private DevExpress.XtraBars.BarStaticItem lblTotalMotoText;
        private FuelExpensesDataSet fuelExpensesDataSet;
        protected System.Windows.Forms.BindingSource atlantaDataSetBindingSource;
        private DevExpress.XtraPrinting.PrintingSystem printingSystem1;
        private DevExpress.XtraPrintingLinks.CompositeLink compositeReportLink;
    }
}
