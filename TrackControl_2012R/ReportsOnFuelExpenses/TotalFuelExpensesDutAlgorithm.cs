#region Using directives
using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using BaseReports.Procedure;
using BaseReports.Procedure.Calibration;
using DevExpress.XtraEditors;
using LocalCache;
using LocalCache.atlantaDataSetTableAdapters;
using TrackControl.Reports;
using TrackControl.Vehicles;
using ReportsOnFuelExpenses.Properties;
using TrackControl.General.DatabaseDriver;

#endregion

namespace ReportsOnFuelExpenses
{
    /// <summary>
    /// ������������ ������ ������ �� ������� �� ��������� ��������
    /// <para>��� ������� ������� ������� ����������� ������ ������ �������</para>
    /// </summary>
    public class TotalFuelExpensesDutAlgorithm : Fuel // PartialAlgorithms//
    {
        private FuelExpensesDataSet feDataSet;
        private AlgorithmType algorythmType;


        public TotalFuelExpensesDutAlgorithm(object algParam)
        {
            algorythmType = (AlgorithmType)algParam;
            feDataSet = FuelExpensesDataSetCreator.GetDataSet();
        }

        /// <summary>
        /// ������ ��� �������
        /// </summary>
        public override void Run()
        {
            if ((GpsDatas.Length == 0) || m_row == null)
            {
                return;
            }
            //if (feDataSet != null)
            //{
            //    if (feDataSet.FuelExpensesDUT_Total.Select(string.Format("Mobitel_ID = {0}", m_row.Mobitel_ID)).Length > 0)
            //        return;
            //}

            BeforeReportFilling(Resources.TotalFuelExDutName, 1);

            try
            {
                FillFuelExpenses();
            }
            finally
            {
                AfterReportFilling();
                Algorithm.OnAction(this, null);
            }
        }

        private double GetKilometrageTotal(atlantaDataSet.KilometrageReportRow[] kmrows)
        {
            if (kmrows == null)
            {
                throw new ArgumentNullException("Parametr in GetKilometrageTotal can not Null");
            }

            double result = 0;
            for (int i = 0; i < kmrows.Length; i++)
            {
                if (kmrows[i].State == Resources.Movement)
                {
                    result += kmrows[i].Distance;
                }
            }

            return result;
        }

        private TimeSpan GetTotalMotionTime(atlantaDataSet.KilometrageReportRow[] kmrows)
        {
            if (kmrows == null)
            {
                throw new ArgumentNullException("Parametr in GetKilometrageTotal can not Null");
            }

            TimeSpan result = new TimeSpan();
            TimeSpan[] interv = new TimeSpan[kmrows.Length];

            for (int i = 0; i < kmrows.Length; i++)
            {
                if (kmrows[i].State == Resources.Movement)
                {
                    result += kmrows[i].Interval;
                    interv[i] = kmrows[i].Interval;
                }
            }

            return result;
        }

        private TimeSpan GetTotalStopsTime(atlantaDataSet.KilometrageReportRow[] kmrows)
        {
            if (kmrows == null)
            {
                throw new ArgumentNullException("Parametr in GetKilometrageTotal can not Null");
            }

            TimeSpan result = new TimeSpan();

            for (int i = 0; i < kmrows.Length; i++)
            {
                if (kmrows[i].State == Resources.Parking)
                {
                    result += kmrows[i].Interval;
                }
            }

            return result;
        }

        private string getRfidSensors(int mobitelid)
        {
            UInt16 identifier = 0;
            string driverName = "";

            try
            {
                if (GpsDatas.Length <= 0)
                    return driverName;

                atlantaDataSet.sensorsRow[] sensors = (atlantaDataSet.sensorsRow[])
                    AtlantaDataSet.sensors.Select(String.Format("mobitel_id = {0}", mobitelid));

                if (sensors.Length > 0)
                {
                    for (int i = 0; i < sensors.Length; i++)
                    {
                        atlantaDataSet.sensorsRow sensor = sensors[i];
                        atlantaDataSet.relationalgorithmsRow[] algs = sensor.GetrelationalgorithmsRows();

                        if (null != algs)
                        {
                            if (algs.Length > 0)
                            {
                                if(algs[0].AlgorithmID == (int) AlgorithmType.DRIVER)
                                {
                                    identifier =
                                        Convert.ToUInt16(Calibrate.ulongSector(GpsDatas[GpsDatas.Length - 1].Sensors, sensor.Length, sensor.StartBit));

                                    if (identifier > 0)
                                    {
                                        driverName = "";
                                        //atlantaDataSet.driverRow[] driver =
                                        //    (atlantaDataSet.driverRow[])
                                        //        AtlantaDataSet.driver.Select(String.Format("Identifier = {0}",
                                        //            identifier));
                                        DataTable driver = null;
                                        using (DriverDb db = new DriverDb())
                                        {
                                            db.ConnectDb();
                                            driver = db.GetDataTable("SELECT * FROM driver WHERE Identifier = " + identifier);
                                        }

                                        if (driver != null)
                                        {
                                            if (driver.Rows.Count > 0)
                                            {
                                                driverName = driver.Rows[0]["Family"] + " " + driver.Rows[0]["Name"];
                                            } // if
                                        } // if
                                    } // if
                                    return driverName;
                                } // if
                            } // if
                        } // if
                    } // for
                } // if

                return driverName;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message + '\n' + ex.StackTrace, Resources.ErrorFuel, MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                return driverName;
            }
        }

        /// <summary>
        /// ����� ����������� �� ��������� m_row
		/// ����� ������ ���
        /// </summary>
        private void FillFuelExpenses() //LocalCache.atlantaDataSet.CrossZonesRow[] crossRows)
        {
            // ������� ��� ��������� ������ � ������� RotateValue ������ �� ���������� ����� ������
            bool rotateValuesExist = m_row.GetTachometerValueRows().Length > 0;
            //��������� ������ � ������ �������
            FuelDictionarys fuelDict = new FuelDictionarys();
            Fuel fuelAlg = new Fuel();
            fuelAlg.SelectItem(m_row);
            fuelAlg.SettingAlgoritm(algorythmType);
            fuelAlg.GettingValuesDUT(fuelDict);
            string rfidDriverName = getRfidSensors(m_row.Mobitel_ID);

            if (fuelDict.ValueFuelSum.Count > 0)
            {
                FuelExpensesDataSet.FuelExpensesDUT_TotalRow row =
                    feDataSet.FuelExpensesDUT_Total.NewFuelExpensesDUT_TotalRow();
                row.Mobitel_ID = m_row.Mobitel_ID;
                row.NameCar = Vehicle.Info;
                row.FIO = rfidDriverName;

                if(row.FIO == "")
                    row.FIO = Vehicle.DriverFullName;

                atlantaDataSet.KilometrageReportRow[] km_rows = (atlantaDataSet.KilometrageReportRow[])
                    AtlantaDataSet.KilometrageReport.Select("MobitelId = " + m_row.Mobitel_ID);

                // ���������� ����, ��
                row.Travel = km_rows.Length == 0
                   ? 0
                   : (float)GetKilometrageTotal(km_rows);

                atlantaDataSet.FuelReportRow[] fuel_rep_rows = GetFuelReportRow(GpsDatas);//GetFuelReportRow(lDataRows)

                // ������� � ������ �������, �
                row.FuelBegin = fuelDict.ValueFuelSum[GpsDatas[0].Id].averageValue;
                // ������� � ����� �������, �
                row.FuelEnd = fuelDict.ValueFuelSum[GpsDatas[GpsDatas.Length - 1].Id].averageValue;

                row.TotalFuelAdd = 0;
                row.FuelAddCount = 0;
                row.FuelSubCount = 0;
                row.TotalFuelSub = 0;
                if (fuel_rep_rows.Length > 0)
                {
                    foreach (atlantaDataSet.FuelReportRow tmp_fr_row in fuel_rep_rows)
                    {
                        if (tmp_fr_row.dValue >= 0)
                        {
                            row.FuelAddCount++; // ���-�� ��������
                            row.TotalFuelAdd += tmp_fr_row.dValue; // ����� ����������, �
                        }
                        else
                        {
                            row.FuelSubCount++; // ���-�� ������
                            row.TotalFuelSub += Math.Abs(tmp_fr_row.dValue); // ����� �����, �
                        }
                    }
                }
                int UseDischargeInFuelrate = 0;
                if (set_row != null)
                    UseDischargeInFuelrate = set_row.FuelrateWithDischarge > 0 ? 1 : 0;
                // ����� ������, �
                row.TotalFuelUse = row.TotalFuelAdd + row.FuelBegin - row.FuelEnd -
                                   UseDischargeInFuelrate*row.TotalFuelSub; //GetFuelUseDut(lDataRows);//

                // ����� � �������� �����, �
                //row.TimeMotion = BasicMethods.GetTotalMotionTime(lDataRows);

                // ����� � �������� �����, �
                row.TimeMotion = GetTotalMotionTime(km_rows);
                row.TypeAlgorythm = (int)algorythmType;

                if (rotateValuesExist)
                {
                    // ����� ������� � ����. ����������, �
                    //row.TimeStopWithEngineOff = GetTimeStopEnginOff(lDataRows, 0, 0);
                    row.TimeStopWithEngineOff = GetTimeStopEnginOff(GpsDatas, 0, 0);
                    // ����� ������� � ���. ����������, �
                    //TimeSpan timeParkingTotal = BasicMethods.GetTotalStopsTime(lDataRows, 0);
                    TimeSpan timeParkingTotal = GetTotalStopsTime(km_rows);
                    row.TimeStopWithEngineOn = timeParkingTotal - row.TimeStopWithEngineOff;

                    // ����� � �������(������-�� ��� � ����-�� ���������� ������ ��������)
                    double engineWorkingTime = (row.TimeStopWithEngineOn + row.TimeMotion).TotalHours;
                    row.LitrePerHour = (float) (engineWorkingTime == 0 ? 0 : row.TotalFuelUse/engineWorkingTime);
                }

                // ������� ������, �/100��
                //row.FuelUseAvg = row.Travel == 0 ? 0 : row.TotalFuelUse / row.Travel * 100;
                row.FuelUseAvg = Math.Round(m_row.path == 0 ? 0 : row.TotalFuelUse / m_row.path * 100.0, 2);
                // ������������ ��������
                row.MaxSpeed = BasicMethods.GetMaxSpeed(GpsDatas);

                try
                {
                    row.MotoHours = Math.Round((row.TimeMotion + row.TimeStopWithEngineOn).TotalSeconds / 3600.0, 2);
                }
                catch(Exception ex)
                {
                    row.MotoHours = Math.Round(row.TimeMotion.TotalSeconds / 3600.0, 2);
                }

                // aketner 24.05.2013
                foreach (atlantaDataSet.vehicleRow rowVehic in AtlantaDataSet.vehicle)
                {
                    if (rowVehic.Mobitel_id == row.Mobitel_ID)
                    {
                        double normaKmLtr = 0.0;
                        double normaMotoLtr = 0.0;

                        if (rowVehic.FuelWayLiter == 0.0 && rowVehic.FuelMotorLiter == 0.0)
                        {
                            foreach (atlantaDataSet.teamRow teamRow in AtlantaDataSet.team)
                            {
                                try
                                {
                                    if (rowVehic.Team_id != -1)
                                    {
                                        if (teamRow.id == rowVehic.Team_id)
                                        {
                                            normaKmLtr = teamRow.FuelWayKmGrp;
                                            normaMotoLtr = teamRow.FuelMotoHrGrp;
                                            break;
                                        } // if
                                    } // if
                                }
                                catch(Exception e)
                                {
                                    normaKmLtr = 0.0;
                                    normaMotoLtr = 0.0;
                                }
                            } // foreach
                        } // if
                        else
                        {
                            normaKmLtr = rowVehic.FuelWayLiter;
                            normaMotoLtr = rowVehic.FuelMotorLiter;
                        }

                        row.NormaWayKm =
                            Convert.ToString(Math.Round(Convert.ToDouble(row.Travel)*normaKmLtr/100, 1));
                        double hours = 0.0;
                        if (rotateValuesExist)
                        {
                            double seconds = row.TimeStopWithEngineOn.Seconds + row.TimeStopWithEngineOn.Minutes*60 +
                                             row.TimeStopWithEngineOn.Hours*3600;
                            hours = seconds/3600.0;
                            row.NormaMotoHr = Convert.ToString(Math.Round(hours*normaMotoLtr, 1));
                        }

                        row.SummaNormalTotal = Convert.ToString(Math.Round(hours*normaMotoLtr, 1) +
                                               Math.Round(Convert.ToDouble(row.Travel)*normaKmLtr/100, 1));
                        break;
                    } // if
                } // foreach
                ///////////////////////
                feDataSet.FuelExpensesDUT_Total.AddFuelExpensesDUT_TotalRow(row);
            } // if

            fuelDict.Clear();
            ReportProgressChanged(1);
        }

        public TimeSpan CalcTimeEngineOn(int mobitelId)
        {
            // ������� ��� ��������� ������ � ������� RotateValue ������ �� ���������� ����� ������
            bool rotateValuesExist = m_row.GetTachometerValueRows().Length > 0;

            // ������� ������
            GpsData[] lDataRows = DataSetManager.ConvertDataviewRowsToDataGpsArray(
                (atlantaDataSet.dataviewRow[]) AtlantaDataSet.dataview.Select(
                    String.Format("Mobitel_Id={0}", mobitelId), "time"));

            atlantaDataSet.KilometrageReportRow[] km_rows = (atlantaDataSet.KilometrageReportRow[])
                   AtlantaDataSet.KilometrageReport.Select("MobitelId = " + mobitelId);

            TimeSpan TimeStopWithEngineOn = new TimeSpan();

            if (rotateValuesExist)
            {
                // ����� ������� � ����. ����������, �
                TimeSpan TimeStopWithEngineOff = GetTimeStopEnginOff(lDataRows, 0, 0);
                // ����� ������� � ���. ����������, �
                //TimeSpan timeParkingTotal = BasicMethods.GetTotalStopsTime(lDataRows, 0);
                TimeSpan timeParkingTotal = GetTotalStopsTime(km_rows);
                TimeStopWithEngineOn = timeParkingTotal - TimeStopWithEngineOff;
            }

            return TimeStopWithEngineOn;
        }
    }
}
