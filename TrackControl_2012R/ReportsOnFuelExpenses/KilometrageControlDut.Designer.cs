﻿namespace ReportsOnFuelExpenses
{
    partial class KilometrageControlDut
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(KilometrageControlDut));
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar3 = new DevExpress.XtraBars.Bar();
            this._distance = new DevExpress.XtraBars.BarStaticItem();
            this._totalfuel = new DevExpress.XtraBars.BarStaticItem();
            this._fuelavg = new DevExpress.XtraBars.BarStaticItem();
            this._fuelavgnorma = new DevExpress.XtraBars.BarStaticItem();
            this._deviation = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this._colid = new DevExpress.XtraGrid.Columns.GridColumn();
            this._colMobitelId = new DevExpress.XtraGrid.Columns.GridColumn();
            this._colState = new DevExpress.XtraGrid.Columns.GridColumn();
            this._imageComboRepo = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this._images = new DevExpress.Utils.ImageCollection(this.components);
            this._colLocation = new DevExpress.XtraGrid.Columns.GridColumn();
            this._colInitialdateTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this._colInitialTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this._colFinalTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this._colInterval = new DevExpress.XtraGrid.Columns.GridColumn();
            this._colDistance = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFuelBegin = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFuelEnd = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalFuelUse = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFuelUseAvg = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNormaFuel100 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFuelAvgNormal = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDeviationFuel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColTpAlgorythm = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageComboBox2 = new DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox();
            this.imageCollection = new DevExpress.Utils.ImageCollection(this.components);
            this.printingSystem = new DevExpress.XtraPrinting.PrintingSystem(this.components);
            this.fuelExpensesDataSet = new ReportsOnFuelExpenses.FuelExpensesDataSet();
            this.atlantaDataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.compositeLink1 = new DevExpress.XtraPrintingLinks.CompositeLink(this.components);
            ((System.ComponentModel.ISupportInitialize) (this.repAlgoChoiser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this._imageComboRepo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this._images)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.repositoryItemImageComboBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.imageCollection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.printingSystem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.fuelExpensesDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.atlantaDataSetBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize) (this.compositeLink1.ImageCollection)).BeginInit();
            this.SuspendLayout();
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {this.bar3});
            this.barManager1.DockControls.Add(this.barDockControl1);
            this.barManager1.DockControls.Add(this.barDockControl2);
            this.barManager1.DockControls.Add(this.barDockControl3);
            this.barManager1.DockControls.Add(this.barDockControl4);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {this._distance, this._totalfuel, this._fuelavg, this._fuelavgnorma, this._deviation});
            this.barManager1.MaxItemId = 5;
            this.barManager1.StatusBar = this.bar3;
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {new DevExpress.XtraBars.LinkPersistInfo(this._distance), new DevExpress.XtraBars.LinkPersistInfo(this._totalfuel), new DevExpress.XtraBars.LinkPersistInfo(this._fuelavg), new DevExpress.XtraBars.LinkPersistInfo(this._fuelavgnorma), new DevExpress.XtraBars.LinkPersistInfo(this._deviation)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // _distance
            // 
            this._distance.Caption = "Общий пробег, км:----";
            this._distance.Id = 0;
            this._distance.Name = "_distance";
            this._distance.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // _totalfuel
            // 
            this._totalfuel.Caption = "Общий расход, л:----";
            this._totalfuel.Id = 1;
            this._totalfuel.Name = "_totalfuel";
            this._totalfuel.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // _fuelavg
            // 
            this._fuelavg.Caption = "Расход, л на 100км:----";
            this._fuelavg.Id = 2;
            this._fuelavg.Name = "_fuelavg";
            this._fuelavg.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // _fuelavgnorma
            // 
            this._fuelavgnorma.Caption = "Расход по норме, в л 100км:----";
            this._fuelavgnorma.Id = 3;
            this._fuelavgnorma.Name = "_fuelavgnorma";
            this._fuelavgnorma.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // _deviation
            // 
            this._deviation.Caption = "Отклонение, в л:----";
            this._deviation.Id = 4;
            this._deviation.Name = "_deviation";
            this._deviation.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Size = new System.Drawing.Size(799, 0);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 378);
            this.barDockControl2.Size = new System.Drawing.Size(799, 25);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 0);
            this.barDockControl3.Size = new System.Drawing.Size(0, 378);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(799, 0);
            this.barDockControl4.Size = new System.Drawing.Size(0, 378);
            // 
            // gridControl1
            // 
            this.gridControl1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 24);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {this._imageComboRepo, this.repositoryItemImageComboBox2});
            this.gridControl1.Size = new System.Drawing.Size(799, 354);
            this.gridControl1.TabIndex = 8;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.ColumnPanelRowHeight = 40;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {this._colid, this._colMobitelId, this._colState, this._colLocation, this._colInitialdateTime, this._colInitialTime, this._colFinalTime, this._colInterval, this._colDistance, this.colFuelBegin, this.colFuelEnd, this.colTotalFuelUse, this.colFuelUseAvg, this.colNormaFuel100, this.colFuelAvgNormal, this.colDeviationFuel, this.gridColTpAlgorythm});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.EnableAppearanceEvenRow = true;
            this.gridView1.OptionsView.EnableAppearanceOddRow = true;
            this.gridView1.OptionsView.GroupDrawMode = DevExpress.XtraGrid.Views.Grid.GroupDrawMode.Office;
            this.gridView1.OptionsView.ShowFooter = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // _colid
            // 
            this._colid.AppearanceCell.Options.UseTextOptions = true;
            this._colid.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._colid.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._colid.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._colid.AppearanceHeader.Options.UseTextOptions = true;
            this._colid.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._colid.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._colid.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._colid.Caption = "ID";
            this._colid.FieldName = "Id";
            this._colid.Name = "_colid";
            this._colid.OptionsColumn.AllowEdit = false;
            this._colid.OptionsColumn.AllowFocus = false;
            this._colid.OptionsColumn.ReadOnly = true;
            this._colid.ToolTip = "ID";
            // 
            // _colMobitelId
            // 
            this._colMobitelId.AppearanceCell.Options.UseTextOptions = true;
            this._colMobitelId.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._colMobitelId.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._colMobitelId.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._colMobitelId.AppearanceHeader.Options.UseTextOptions = true;
            this._colMobitelId.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._colMobitelId.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._colMobitelId.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._colMobitelId.Caption = "MobitelID";
            this._colMobitelId.FieldName = "MobitelId";
            this._colMobitelId.Name = "_colMobitelId";
            this._colMobitelId.OptionsColumn.AllowEdit = false;
            this._colMobitelId.OptionsColumn.AllowFocus = false;
            this._colMobitelId.OptionsColumn.ReadOnly = true;
            this._colMobitelId.ToolTip = "MobitelId";
            // 
            // _colState
            // 
            this._colState.AppearanceCell.Options.UseTextOptions = true;
            this._colState.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._colState.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._colState.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._colState.AppearanceHeader.Options.UseTextOptions = true;
            this._colState.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._colState.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._colState.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._colState.Caption = "Состояние транспорта";
            this._colState.ColumnEdit = this._imageComboRepo;
            this._colState.FieldName = "State";
            this._colState.Name = "_colState";
            this._colState.OptionsColumn.AllowEdit = false;
            this._colState.OptionsColumn.AllowFocus = false;
            this._colState.OptionsColumn.ReadOnly = true;
            this._colState.ToolTip = "Состояние транспорта";
            this._colState.Visible = true;
            this._colState.VisibleIndex = 0;
            // 
            // _imageComboRepo
            // 
            this._imageComboRepo.AutoHeight = false;
            this._imageComboRepo.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this._imageComboRepo.Items.AddRange(new DevExpress.XtraEditors.Controls.ImageComboBoxItem[] {new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", null, 0), new DevExpress.XtraEditors.Controls.ImageComboBoxItem("", null, 1)});
            this._imageComboRepo.Name = "_imageComboRepo";
            this._imageComboRepo.ReadOnly = true;
            this._imageComboRepo.SmallImages = this._images;
            // 
            // _images
            // 
            this._images.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer) (resources.GetObject("_images.ImageStream")));
            this._images.Images.SetKeyName(0, "Parking.png");
            this._images.Images.SetKeyName(1, "Highway.png");
            // 
            // _colLocation
            // 
            this._colLocation.AppearanceCell.Options.UseTextOptions = true;
            this._colLocation.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._colLocation.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._colLocation.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._colLocation.AppearanceHeader.Options.UseTextOptions = true;
            this._colLocation.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._colLocation.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._colLocation.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._colLocation.Caption = "Местоположение";
            this._colLocation.FieldName = "Location";
            this._colLocation.Name = "_colLocation";
            this._colLocation.OptionsColumn.AllowEdit = false;
            this._colLocation.OptionsColumn.AllowFocus = false;
            this._colLocation.OptionsColumn.ReadOnly = true;
            this._colLocation.ToolTip = "Местоположение";
            this._colLocation.Visible = true;
            this._colLocation.VisibleIndex = 1;
            // 
            // _colInitialdateTime
            // 
            this._colInitialdateTime.AppearanceCell.Options.UseTextOptions = true;
            this._colInitialdateTime.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._colInitialdateTime.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._colInitialdateTime.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._colInitialdateTime.AppearanceHeader.Options.UseTextOptions = true;
            this._colInitialdateTime.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._colInitialdateTime.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._colInitialdateTime.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._colInitialdateTime.Caption = "Дата";
            this._colInitialdateTime.DisplayFormat.FormatString = "dd.MM.yyyy";
            this._colInitialdateTime.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this._colInitialdateTime.FieldName = "DateTime";
            this._colInitialdateTime.Name = "_colInitialdateTime";
            this._colInitialdateTime.OptionsColumn.AllowEdit = false;
            this._colInitialdateTime.OptionsColumn.AllowFocus = false;
            this._colInitialdateTime.OptionsColumn.ReadOnly = true;
            this._colInitialdateTime.ToolTip = "Дата";
            this._colInitialdateTime.Visible = true;
            this._colInitialdateTime.VisibleIndex = 2;
            // 
            // _colInitialTime
            // 
            this._colInitialTime.AppearanceCell.Options.UseTextOptions = true;
            this._colInitialTime.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._colInitialTime.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._colInitialTime.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._colInitialTime.AppearanceHeader.Options.UseTextOptions = true;
            this._colInitialTime.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._colInitialTime.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._colInitialTime.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._colInitialTime.Caption = "Время начала";
            this._colInitialTime.DisplayFormat.FormatString = "HH:mm";
            this._colInitialTime.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this._colInitialTime.FieldName = "InitialTime";
            this._colInitialTime.Name = "_colInitialTime";
            this._colInitialTime.OptionsColumn.AllowEdit = false;
            this._colInitialTime.OptionsColumn.AllowFocus = false;
            this._colInitialTime.OptionsColumn.ReadOnly = true;
            this._colInitialTime.ToolTip = "Время начала";
            this._colInitialTime.Visible = true;
            this._colInitialTime.VisibleIndex = 3;
            // 
            // _colFinalTime
            // 
            this._colFinalTime.AppearanceCell.Options.UseTextOptions = true;
            this._colFinalTime.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._colFinalTime.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._colFinalTime.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._colFinalTime.AppearanceHeader.Options.UseTextOptions = true;
            this._colFinalTime.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._colFinalTime.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._colFinalTime.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._colFinalTime.Caption = "Время окончания";
            this._colFinalTime.DisplayFormat.FormatString = "HH:mm";
            this._colFinalTime.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this._colFinalTime.FieldName = "FinalTime";
            this._colFinalTime.Name = "_colFinalTime";
            this._colFinalTime.OptionsColumn.AllowEdit = false;
            this._colFinalTime.OptionsColumn.AllowFocus = false;
            this._colFinalTime.OptionsColumn.ReadOnly = true;
            this._colFinalTime.ToolTip = "Время окончания";
            this._colFinalTime.Visible = true;
            this._colFinalTime.VisibleIndex = 4;
            // 
            // _colInterval
            // 
            this._colInterval.AppearanceCell.Options.UseTextOptions = true;
            this._colInterval.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._colInterval.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._colInterval.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._colInterval.AppearanceHeader.Options.UseTextOptions = true;
            this._colInterval.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._colInterval.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._colInterval.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._colInterval.Caption = "Интервал";
            this._colInterval.DisplayFormat.FormatString = "dd.HH:mm";
            this._colInterval.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this._colInterval.FieldName = "Interval";
            this._colInterval.Name = "_colInterval";
            this._colInterval.OptionsColumn.AllowEdit = false;
            this._colInterval.OptionsColumn.AllowFocus = false;
            this._colInterval.OptionsColumn.ReadOnly = true;
            this._colInterval.ToolTip = "Интервал";
            this._colInterval.Visible = true;
            this._colInterval.VisibleIndex = 5;
            // 
            // _colDistance
            // 
            this._colDistance.AppearanceCell.Options.UseTextOptions = true;
            this._colDistance.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._colDistance.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._colDistance.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._colDistance.AppearanceHeader.Options.UseTextOptions = true;
            this._colDistance.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this._colDistance.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this._colDistance.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this._colDistance.Caption = "Пробег";
            this._colDistance.DisplayFormat.FormatString = "N2";
            this._colDistance.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this._colDistance.FieldName = "Distance";
            this._colDistance.Name = "_colDistance";
            this._colDistance.OptionsColumn.AllowEdit = false;
            this._colDistance.OptionsColumn.AllowFocus = false;
            this._colDistance.OptionsColumn.ReadOnly = true;
            this._colDistance.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Distance", "{0:f2}")});
            this._colDistance.ToolTip = "Пробег, км";
            this._colDistance.Visible = true;
            this._colDistance.VisibleIndex = 6;
            // 
            // colFuelBegin
            // 
            this.colFuelBegin.AppearanceCell.Options.UseTextOptions = true;
            this.colFuelBegin.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelBegin.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colFuelBegin.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFuelBegin.AppearanceHeader.Options.UseTextOptions = true;
            this.colFuelBegin.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelBegin.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colFuelBegin.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFuelBegin.Caption = "Топлива в начале";
            this.colFuelBegin.DisplayFormat.FormatString = "N2";
            this.colFuelBegin.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colFuelBegin.FieldName = "FuelBegin";
            this.colFuelBegin.Name = "colFuelBegin";
            this.colFuelBegin.OptionsColumn.AllowEdit = false;
            this.colFuelBegin.OptionsColumn.AllowFocus = false;
            this.colFuelBegin.OptionsColumn.ReadOnly = true;
            this.colFuelBegin.ToolTip = "Топлива в начале периода, л";
            this.colFuelBegin.Visible = true;
            this.colFuelBegin.VisibleIndex = 7;
            // 
            // colFuelEnd
            // 
            this.colFuelEnd.AppearanceCell.Options.UseTextOptions = true;
            this.colFuelEnd.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelEnd.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colFuelEnd.AppearanceHeader.Options.UseTextOptions = true;
            this.colFuelEnd.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelEnd.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colFuelEnd.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFuelEnd.Caption = "Топлива в конце";
            this.colFuelEnd.DisplayFormat.FormatString = "N2";
            this.colFuelEnd.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colFuelEnd.FieldName = "FuelEnd";
            this.colFuelEnd.Name = "colFuelEnd";
            this.colFuelEnd.OptionsColumn.AllowEdit = false;
            this.colFuelEnd.OptionsColumn.AllowFocus = false;
            this.colFuelEnd.OptionsColumn.ReadOnly = true;
            this.colFuelEnd.ToolTip = "Топлива в конце периода, л";
            this.colFuelEnd.Visible = true;
            this.colFuelEnd.VisibleIndex = 8;
            // 
            // colTotalFuelUse
            // 
            this.colTotalFuelUse.AppearanceCell.Options.UseTextOptions = true;
            this.colTotalFuelUse.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTotalFuelUse.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTotalFuelUse.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTotalFuelUse.AppearanceHeader.Options.UseTextOptions = true;
            this.colTotalFuelUse.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTotalFuelUse.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colTotalFuelUse.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colTotalFuelUse.Caption = "Расход";
            this.colTotalFuelUse.DisplayFormat.FormatString = "N2";
            this.colTotalFuelUse.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTotalFuelUse.FieldName = "TotalFuelUse";
            this.colTotalFuelUse.Name = "colTotalFuelUse";
            this.colTotalFuelUse.OptionsColumn.AllowEdit = false;
            this.colTotalFuelUse.OptionsColumn.AllowFocus = false;
            this.colTotalFuelUse.OptionsColumn.ReadOnly = true;
            this.colTotalFuelUse.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalFuelUse", "{0:f2}")});
            this.colTotalFuelUse.ToolTip = "Расход, л";
            this.colTotalFuelUse.Visible = true;
            this.colTotalFuelUse.VisibleIndex = 9;
            // 
            // colFuelUseAvg
            // 
            this.colFuelUseAvg.AppearanceCell.Options.UseTextOptions = true;
            this.colFuelUseAvg.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelUseAvg.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colFuelUseAvg.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFuelUseAvg.AppearanceHeader.Options.UseTextOptions = true;
            this.colFuelUseAvg.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelUseAvg.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colFuelUseAvg.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFuelUseAvg.Caption = "Расход на 100км";
            this.colFuelUseAvg.DisplayFormat.FormatString = "N2";
            this.colFuelUseAvg.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colFuelUseAvg.FieldName = "FuelUseAvg";
            this.colFuelUseAvg.Name = "colFuelUseAvg";
            this.colFuelUseAvg.OptionsColumn.AllowEdit = false;
            this.colFuelUseAvg.OptionsColumn.AllowFocus = false;
            this.colFuelUseAvg.OptionsColumn.ReadOnly = true;
            this.colFuelUseAvg.ToolTip = "Расход в л на 100км";
            this.colFuelUseAvg.Visible = true;
            this.colFuelUseAvg.VisibleIndex = 10;
            // 
            // colNormaFuel100
            // 
            this.colNormaFuel100.AppearanceCell.Options.UseTextOptions = true;
            this.colNormaFuel100.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNormaFuel100.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colNormaFuel100.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colNormaFuel100.AppearanceHeader.Options.UseTextOptions = true;
            this.colNormaFuel100.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNormaFuel100.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colNormaFuel100.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colNormaFuel100.Caption = "Нормированный расход";
            this.colNormaFuel100.DisplayFormat.FormatString = "N2";
            this.colNormaFuel100.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colNormaFuel100.FieldName = "NormalFuel100";
            this.colNormaFuel100.Name = "colNormaFuel100";
            this.colNormaFuel100.OptionsColumn.AllowEdit = false;
            this.colNormaFuel100.OptionsColumn.AllowFocus = false;
            this.colNormaFuel100.OptionsColumn.ReadOnly = true;
            this.colNormaFuel100.ToolTip = "Нормированный расход в л на 100км";
            this.colNormaFuel100.Visible = true;
            this.colNormaFuel100.VisibleIndex = 11;
            // 
            // colFuelAvgNormal
            // 
            this.colFuelAvgNormal.AppearanceCell.Options.UseTextOptions = true;
            this.colFuelAvgNormal.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelAvgNormal.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colFuelAvgNormal.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFuelAvgNormal.AppearanceHeader.Options.UseTextOptions = true;
            this.colFuelAvgNormal.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFuelAvgNormal.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colFuelAvgNormal.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colFuelAvgNormal.Caption = "Расход по норме";
            this.colFuelAvgNormal.DisplayFormat.FormatString = "N2";
            this.colFuelAvgNormal.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colFuelAvgNormal.FieldName = "FuelAvgNormal";
            this.colFuelAvgNormal.Name = "colFuelAvgNormal";
            this.colFuelAvgNormal.OptionsColumn.AllowEdit = false;
            this.colFuelAvgNormal.OptionsColumn.AllowFocus = false;
            this.colFuelAvgNormal.OptionsColumn.ReadOnly = true;
            this.colFuelAvgNormal.ToolTip = "Расход по норме в л на 100км";
            this.colFuelAvgNormal.Visible = true;
            this.colFuelAvgNormal.VisibleIndex = 12;
            // 
            // colDeviationFuel
            // 
            this.colDeviationFuel.AppearanceCell.Options.UseTextOptions = true;
            this.colDeviationFuel.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDeviationFuel.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colDeviationFuel.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDeviationFuel.AppearanceHeader.Options.UseTextOptions = true;
            this.colDeviationFuel.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDeviationFuel.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.colDeviationFuel.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.colDeviationFuel.Caption = "Отклонение";
            this.colDeviationFuel.DisplayFormat.FormatString = "N2";
            this.colDeviationFuel.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colDeviationFuel.FieldName = "DeviationFuel";
            this.colDeviationFuel.Name = "colDeviationFuel";
            this.colDeviationFuel.OptionsColumn.AllowEdit = false;
            this.colDeviationFuel.OptionsColumn.AllowFocus = false;
            this.colDeviationFuel.OptionsColumn.ReadOnly = true;
            this.colDeviationFuel.ToolTip = "Отклонение в л от нормы";
            this.colDeviationFuel.Visible = true;
            this.colDeviationFuel.VisibleIndex = 13;
            // 
            // gridColTpAlgorythm
            // 
            this.gridColTpAlgorythm.AppearanceCell.Options.UseTextOptions = true;
            this.gridColTpAlgorythm.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColTpAlgorythm.AppearanceCell.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColTpAlgorythm.AppearanceCell.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            this.gridColTpAlgorythm.AppearanceHeader.Options.UseTextOptions = true;
            this.gridColTpAlgorythm.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.gridColTpAlgorythm.AppearanceHeader.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.gridColTpAlgorythm.AppearanceHeader.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridColTpAlgorythm.Caption = "Тип алгоритма";
            this.gridColTpAlgorythm.FieldName = "TypeAlgorythm";
            this.gridColTpAlgorythm.Name = "gridColTpAlgorythm";
            this.gridColTpAlgorythm.ToolTip = "Тип алгоритма";
            this.gridColTpAlgorythm.Visible = true;
            this.gridColTpAlgorythm.VisibleIndex = 14;
            // 
            // repositoryItemImageComboBox2
            // 
            this.repositoryItemImageComboBox2.AutoHeight = false;
            this.repositoryItemImageComboBox2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageComboBox2.Name = "repositoryItemImageComboBox2";
            // 
            // imageCollection
            // 
            this.imageCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer) (resources.GetObject("imageCollection.ImageStream")));
            this.imageCollection.Images.SetKeyName(0, "ParkingMaps.png");
            this.imageCollection.Images.SetKeyName(1, "Parking.png");
            // 
            // printingSystem
            // 
            this.printingSystem.ExportOptions.Html.ExportMode = DevExpress.XtraPrinting.HtmlExportMode.SingleFilePageByPage;
            this.printingSystem.ExportOptions.Image.ExportMode = DevExpress.XtraPrinting.ImageExportMode.SingleFilePageByPage;
            this.printingSystem.ExportOptions.Mht.ExportMode = DevExpress.XtraPrinting.HtmlExportMode.SingleFilePageByPage;
            this.printingSystem.ExportOptions.Xls.ShowGridLines = true;
            this.printingSystem.ExportOptions.Xlsx.ShowGridLines = true;
            // 
            // fuelExpensesDataSet
            // 
            this.fuelExpensesDataSet.DataSetName = "FuelExpensesDUT_Day";
            this.fuelExpensesDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // compositeLink1
            // 
            // 
            // 
            // 
            this.compositeLink1.ImageCollection.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer) (resources.GetObject("compositeLink1.ImageCollection.ImageStream")));
            this.compositeLink1.Landscape = true;
            this.compositeLink1.Margins = new System.Drawing.Printing.Margins(25, 25, 70, 25);
            this.compositeLink1.MinMargins = new System.Drawing.Printing.Margins(25, 25, 15, 25);
            this.compositeLink1.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.compositeLink1.PrintingSystemBase = this.printingSystem;
            // 
            // KilometrageControlDut
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridControl1);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.Name = "KilometrageControlDut";
            this.Controls.SetChildIndex(this.barDockControl1, 0);
            this.Controls.SetChildIndex(this.barDockControl2, 0);
            this.Controls.SetChildIndex(this.barDockControl4, 0);
            this.Controls.SetChildIndex(this.barDockControl3, 0);
            this.Controls.SetChildIndex(this.gridControl1, 0);
            ((System.ComponentModel.ISupportInitialize) (this.repAlgoChoiser)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this._imageComboRepo)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this._images)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.repositoryItemImageComboBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.imageCollection)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.printingSystem)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.fuelExpensesDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.atlantaDataSetBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize) (this.compositeLink1.ImageCollection)).EndInit();
            this.ResumeLayout(false);
        }

        private DevExpress.XtraGrid.Columns.GridColumn _colDistance;
        private DevExpress.XtraGrid.Columns.GridColumn _colFinalTime;
        private DevExpress.XtraGrid.Columns.GridColumn _colid;
        private DevExpress.XtraGrid.Columns.GridColumn _colInitialdateTime;
        private DevExpress.XtraGrid.Columns.GridColumn _colInitialTime;
        private DevExpress.XtraGrid.Columns.GridColumn _colInterval;
        private DevExpress.XtraGrid.Columns.GridColumn _colLocation;
        private DevExpress.XtraGrid.Columns.GridColumn _colMobitelId;
        private DevExpress.XtraGrid.Columns.GridColumn _colState;
        private DevExpress.XtraBars.BarStaticItem _deviation;
        private DevExpress.XtraBars.BarStaticItem _distance;
        private DevExpress.XtraBars.BarStaticItem _fuelavg;
        private DevExpress.XtraBars.BarStaticItem _fuelavgnorma;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox _imageComboRepo;
        private DevExpress.Utils.ImageCollection _images;
        private DevExpress.XtraBars.BarStaticItem _totalfuel;
        protected System.Windows.Forms.BindingSource atlantaDataSetBindingSource;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraGrid.Columns.GridColumn colDeviationFuel;
        private DevExpress.XtraGrid.Columns.GridColumn colFuelAvgNormal;
        private DevExpress.XtraGrid.Columns.GridColumn colFuelBegin;
        private DevExpress.XtraGrid.Columns.GridColumn colFuelEnd;
        private DevExpress.XtraGrid.Columns.GridColumn colFuelUseAvg;
        private DevExpress.XtraGrid.Columns.GridColumn colNormaFuel100;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalFuelUse;
        private DevExpress.XtraPrintingLinks.CompositeLink compositeLink1;
        private ReportsOnFuelExpenses.FuelExpensesDataSet fuelExpensesDataSet;
        private DevExpress.XtraGrid.Columns.GridColumn gridColTpAlgorythm;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.Utils.ImageCollection imageCollection;
        private DevExpress.XtraPrinting.PrintingSystem printingSystem;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageComboBox repositoryItemImageComboBox2;

        #endregion
    }
}
