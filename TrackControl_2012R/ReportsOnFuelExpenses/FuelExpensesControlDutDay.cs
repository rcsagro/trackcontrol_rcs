﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using BaseReports;
using BaseReports.Procedure;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraPrinting;
using LocalCache;
using Report;
using ReportsOnFuelExpenses.Properties;
using TrackControl.General;
using TrackControl.Reports;

namespace ReportsOnFuelExpenses
{
    public partial class FuelExpensesControlDutDay : BaseReports.ReportsDE.BaseControl
    {
        public class repFuelDutDay
        {
            string date;
            string iniTime;
            string finTime;
            string intervWork;
            string intervMove;
            string intervStops;
            string timeEngineOn;
            double distance;
            double fuel_begin;
            double fuel_end;
            double total_fuel_add;
            double total_fuel_sub;
            double total_fuel_use;
            double fuel_use_middle_motion;
            double idle_fuel_rate;

            public repFuelDutDay(string date, string iniTime, string finTime, string intervWork, string intervMove,
                string intervStops, string timeEngineOn, double distance, double fuel_begin, double fuel_end,
                double total_fuel_add, double total_fuel_sub, double total_fuel_use, double fuel_use_middle_motion,
                double idle_fuel_rate)
            {
                this.date = date;
                this.iniTime = iniTime;
                this.finTime = finTime;
                this.intervWork = intervWork;
                this.intervMove = intervMove;
                this.intervStops = intervStops;
                this.timeEngineOn = timeEngineOn;
                this.distance = distance;
                this.fuel_begin = fuel_begin;
                this.fuel_end = fuel_end;
                this.total_fuel_add = total_fuel_add;
                this.total_fuel_sub = total_fuel_sub;
                this.total_fuel_use = total_fuel_use;
                this.fuel_use_middle_motion = fuel_use_middle_motion;
                this.idle_fuel_rate = idle_fuel_rate;
            } // repFuelDutDay

            public string Date
            {
                get { return date; }
            }

            public string InitialTime
            {
                get { return iniTime; }
            }

            public string FinalTime
            {
                get { return finTime; }
            }

            public string IntervalWork
            {
                get { return intervWork; }
            }

            public string IntervalMove
            {
                get { return intervMove; }
            }

            public string IntervalStop
            {
                get { return intervStops; }
            }

            public string TimeWithEngineOn
            {
                get { return timeEngineOn; }
            }

            public double Distance
            {
                get { return distance; }
            }

            public double FuelBegin
            {
                get { return fuel_begin; }
            }

            public double FuelEnd
            {
                get { return fuel_end; }
            }

            public double TotalFuelAdd
            {
                get { return total_fuel_add; }
            }

            public double TotalFuelSub
            {
                get { return total_fuel_sub; }
            }

            public double TotalFuelUse
            {
                get { return total_fuel_use; }
            }

            public double FuelUseMiddleInMotion
            {
                get { return fuel_use_middle_motion; }
            }

            public double idleFuelRate
            {
                get { return idle_fuel_rate; }
            }
        } // repFuelDutDay

        // Словарь, содержащий итоговые данные для каждого телетрека (транспортного
        // средства). Ключом является ID телетрека и алгоритма
        private List<TotalFuelExpensesDutDayAlgorithm.SummaryDutDay> _summDutDay =
            new List<TotalFuelExpensesDutDayAlgorithm.SummaryDutDay>();

        // датасет
        public FuelExpensesDataSet feDataSet;
        // Интерфейс для построение графиков по SeriesL
        private static IBuildGraphs buildGraph;
        protected static atlantaDataSet dataset;
        protected VehicleInfo vehicleInfo;
        ReportBase<repFuelDutDay, TInfoDut> ReportingFuelDutDay;
        private string NameThisReport = Resources.TotalFuelExDutDayName;
        private int globalCode = (int) AlgorithmType.FUEL1;
        ComboBoxEdit comboAlgorithm = null;
        Fuel fuelAlgorythm = null;

        public FuelExpensesControlDutDay()
        {
            InitializeComponent();
            Localization();
            HideStatusBar();
            DisableButton();
            DisableRun();
            SetVisibilityAlgoChoiser(true);
            VisionPanel(gvFuelCtrlDutDay, gcFuelCtrlDutDay, bar3);
            feDataSet = FuelExpensesDataSetCreator.GetDataSet();
            atlantaDataSetBindingSource.DataSource = feDataSet;
            atlantaDataSetBindingSource.DataMember = "FuelExpensesDUT_Day";
            gcFuelCtrlDutDay.DataSource = atlantaDataSetBindingSource;
            buildGraph = new BuildGraphs();
            fuelExpensesDataSet = null;
            dataset = ReportTabControl.Dataset;
            ClearStatusLine();
            AddAlgorithm(new KilometrageDays());
            AddAlgorithm(new Rotation());
            fuelAlgorythm = new Fuel(AlgorithmType.FUEL1);
            AddAlgorithm(fuelAlgorythm);
            AddAlgorithm(new Fuel(AlgorithmType.FUEL1_AGREGAT));
            AddAlgorithm(new Fuel(AlgorithmType.FUEL2_AGREGAT));
            AddAlgorithm(new Fuel(AlgorithmType.FUEL3_AGREGAT));
            AddAlgorithm(new Fuel(AlgorithmType.FUEL_AGREGAT_ALL));
            
            AddAlgorithm(new TotalFuelExpensesDutDayAlgorithm(AlgorithmType.FUEL1));
            AddAlgorithm(new TotalFuelExpensesDutDayAlgorithm(AlgorithmType.FUEL1_AGREGAT));
            AddAlgorithm(new TotalFuelExpensesDutDayAlgorithm(AlgorithmType.FUEL2_AGREGAT));
            AddAlgorithm(new TotalFuelExpensesDutDayAlgorithm(AlgorithmType.FUEL3_AGREGAT));
            AddAlgorithm(new TotalFuelExpensesDutDayAlgorithm(AlgorithmType.FUEL_AGREGAT_ALL));
            
            gvFuelCtrlDutDay.RowClick += new RowClickEventHandler(gvFuelControlDutDay_ClickRow);
            gvFuelCtrlDutDay.BeforePrintRow += gvFuelCtrlDutDay_BeforePrintRow;

            compositeReportLink.CreateMarginalHeaderArea +=
                new CreateAreaEventHandler(composLink_CreateMarginalHeaderArea);

            ReportingFuelDutDay =
                new ReportBase<repFuelDutDay, TInfoDut>(Controls, compositeReportLink, gvFuelCtrlDutDay,
                    GetStringBreackLeft, GetStringBreackRight, GetStringBreackUp);
        }

        void gvFuelCtrlDutDay_BeforePrintRow(object sender, DevExpress.XtraGrid.Views.Printing.CancelPrintRowEventArgs e)
        {
            return;
        } // DevFuelExpensesCtrlDutDay

        public override string Caption
        {
            get
            {
                return Resources.TotalFuelExDutDayName;
            }
        }

        // Отображение  итогов
        void ClearStatusLine()
        {
            lblTotalDebitText.Caption = Resources.FuelExControlCommonFuelrate + ": ---";
            lblTotalFuelAddText.Caption = Resources.FuelExControlDutCommonFuelAdd + ": ---";
            lblTotalFuelSubText.Caption = Resources.FuelExControlDutCommonFuelPourOff + ": ---";
            lblAvgFuelDebitText.Caption = Resources.FuelExControlDutDayFuelrate + ": ---";
            lblTotalMotoText.Caption = Resources.FuelExControDutDayFuelrateOnTime + ": ---";
        } // clearStatusLine

        private bool ContainsID(int id)
        {
            for (int i = 0; i < _summDutDay.Count; i++)
            {
                if (_summDutDay[i].id == id)
                {
                    return true;
                }
            }

            return false;
        }

        private TotalFuelExpensesDutDayAlgorithm.SummaryDutDay getSummary(int mobitelId, int type)
        {
            for (int i = 0; i < _summDutDay.Count; i++)
            {
                if (_summDutDay[i].id == mobitelId && _summDutDay[i].typeAlg == type)
                {
                    return _summDutDay[i];
                }
            }

            return new TotalFuelExpensesDutDayAlgorithm.SummaryDutDay();
        }

        private void UpdateStatusStrip(int mobi, int type)
        {
            TotalFuelExpensesDutDayAlgorithm.SummaryDutDay _summariesDutDay = getSummary(mobi,
                        type);
            lblTotalDebitText.Caption = String.Concat(Resources.FuelExControlCommonFuelrate, ": ", _summariesDutDay.TotalDebit.ToString("F2"));
            lblTotalFuelAddText.Caption = String.Concat(Resources.FuelExControlDutCommonFuelAdd, ": ", _summariesDutDay.TotalFuelAdd.ToString("F2"));
            lblTotalFuelSubText.Caption = String.Concat(Resources.FuelExControlDutCommonFuelPourOff, ": ", _summariesDutDay.TotalFuelSub.ToString("F2"));
            lblAvgFuelDebitText.Caption = String.Concat(Resources.FuelExControlDutDayFuelrate, ": ", _summariesDutDay.Distance == 0 ?
                "0" : Math.Round((100 * _summariesDutDay.TotalDebit /
                _summariesDutDay.Distance), 2).ToString("F2"));
            lblTotalMotoText.Caption = String.Concat(Resources.FuelExControDutDayFuelrateOnTime, ": ", _summariesDutDay.TimeTotalWithEngineOnHour.TotalHours == 0 ?
                "0" : Math.Round((_summariesDutDay.TotalDebit /
                _summariesDutDay.TimeTotalWithEngineOnHour.TotalHours), 2).ToString("F2"));
        }

        public override void Select(atlantaDataSet.mobitelsRow m_row)
        {
            if (m_row != null)
            {
                curMrow = m_row;

                if (ContainsID(m_row.Mobitel_ID))
                {
                    // Заполняем данными итоговую статусную панель

                    UpdateStatusStrip(m_row.Mobitel_ID, globalCode);
                    EnableButton();
                } // if

                int index = 0;
                if (comboAlgorithm != null)
                {
                    index = comboAlgorithm.SelectedIndex;
                }

                if (atlantaDataSetBindingSource.Count > 0)
                {

                    if (index == 0)
                    {
                        atlantaDataSetBindingSource.Filter = "Mobitel_Id=" + m_row.Mobitel_ID + " and " +
                                                             "TypeAlgorythm=" +
                                                             (int) AlgorithmType.FUEL1;
                    }
                    else if (index == 1)
                    {
                        atlantaDataSetBindingSource.Filter = "Mobitel_Id=" + m_row.Mobitel_ID + " and " +
                                                             "TypeAlgorythm=" +
                                                             (int) AlgorithmType.FUEL1_AGREGAT;
                    }
                    else if (index == 2)
                    {
                        atlantaDataSetBindingSource.Filter = "Mobitel_Id=" + m_row.Mobitel_ID + " and " +
                                                             "TypeAlgorythm=" +
                                                             (int) AlgorithmType.FUEL2_AGREGAT;
                    }
                    else if (index == 3)
                    {
                        atlantaDataSetBindingSource.Filter = "Mobitel_Id=" + m_row.Mobitel_ID + " and " +
                                                             "TypeAlgorythm=" +
                                                             (int) AlgorithmType.FUEL3_AGREGAT;
                    }
                    else if (index == 4)
                    {
                        atlantaDataSetBindingSource.Filter = "Mobitel_Id=" + m_row.Mobitel_ID + " and " +
                                                             "TypeAlgorythm=" + (int) AlgorithmType.FUEL_AGREGAT_ALL;
                    }
                }

                if (CurrentActivateReport == NameThisReport)
                    {
                        //DisableButton();
                        //ClearStatusLine();
                        ReportsControl.GraphClearSeries();
                        Graph.ClearLabel();
                        if (index == 0)
                        {
                            SelectGraphic(curMrow, (int)AlgorithmType.FUEL1);
                        }
                        else if (index == 1)
                        {
                            SelectGraphic(curMrow, (int)AlgorithmType.FUEL1_AGREGAT);
                        }
                        else if (index == 2)
                        {
                            SelectGraphic(curMrow, (int)AlgorithmType.FUEL2_AGREGAT);
                        }
                        else if (index == 3)
                        {
                            SelectGraphic(curMrow, (int)AlgorithmType.FUEL3_AGREGAT);
                        }
                        else if (index == 4)
                        {
                            SelectGraphic(curMrow, (int)AlgorithmType.FUEL_AGREGAT_ALL);
                        }
                        ReportsControl.OnClearMapObjectsNeeded();
                }
            } // if
        } // Select

        public void SelectGraphic(atlantaDataSet.mobitelsRow mobitel, int type)
        {
            if (mobitel != null)
            {
                curMrow = mobitel;
                vehicleInfo = new VehicleInfo(mobitel);
                atlantaDataSetBindingSource.Filter = "Mobitel_id=" + mobitel.Mobitel_ID + " and " + "TypeAlgorythm=" + type;

                try
                {
                    ReportsControl.GraphClearSeries();

                    if (type == (int)AlgorithmType.FUEL1)
                    {
                        CreateGraph(mobitel, AlgorithmType.FUEL1);
                    }
                    else if (type == (int)AlgorithmType.FUEL1_AGREGAT)
                    {
                        CreateGraph(mobitel, AlgorithmType.FUEL1_AGREGAT);
                    }
                    else if (type == (int)AlgorithmType.FUEL2_AGREGAT)
                    {
                        CreateGraph(mobitel, AlgorithmType.FUEL2_AGREGAT);
                    }
                    else if (type == (int)AlgorithmType.FUEL3_AGREGAT)
                    {
                        CreateGraph(mobitel, AlgorithmType.FUEL3_AGREGAT);
                    }
                    else if (type == (int)AlgorithmType.FUEL_AGREGAT_ALL)
                    {
                        CreateGraph(mobitel, AlgorithmType.FUEL_AGREGAT_ALL);
                    }
                    else
                    {
                        throw new NotImplementedException("Unknown type algorythm!");
                    }
                }
                catch (Exception ex)
                {
                    string message = String.Format("Exception in FuelExpensesControlDutDay: {0}", ex.Message);
                    XtraMessageBox.Show(message, Caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            } // if
        }

        private void CreateGraph(atlantaDataSet.mobitelsRow mobitel_row, object algoParam)
        {
            if (!DataSetManager.IsGpsDataExist(mobitel_row)) return;

            buildGraph.SetAlgorithm(algoParam);
            buildGraph.AddGraphFuel(algoParam, Graph, dataset, mobitel_row);
            buildGraph.AddGraphVoltage(Graph, dataset, mobitel_row); // add 04.07.2017 aketner
            ReportsControl.ShowGraph(mobitel_row);

            if (buildGraph.AddGraphInclinometers(Graph, dataset, mobitel_row))
            {
                vehicleInfo = new VehicleInfo(mobitel_row);
                Graph.ShowSeries(vehicleInfo.Info);
            }
        } // CreateGraph

        // Нажатие кнопки формирования таблицы отчета
        protected override void bbiStart_ItemClick(object sender, ItemClickEventArgs e)
        {
            bool noData = true; // За выбранный период нет данных ни по одной машине
            if (bbiStart.Caption == Resources.Start)
            {
                SetStopButton();
                DisableButton();
                ClearReport();
                ClearStatusLine();
                BeginReport();
                _stopRun = false;

                //atlantaDataSetBindingSource.RaiseListChangedEvents = false;
                atlantaDataSetBindingSource.SuspendBinding();
                
                foreach (atlantaDataSet.mobitelsRow m_row in dataset.mobitels)
                {
                    if (m_row.Check && DataSetManager.IsGpsDataExist(m_row))
                    {
                        SelectItem(m_row);
                        noData = false;
                        if (_stopRun)
                            break;
                    } // if
                } // foreach

                if (noData)
                {
                    XtraMessageBox.Show(Resources.WarningNoData, Resources.Notification,
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

                atlantaDataSetBindingSource.ResumeBinding();
                // atlantaDataSetBindingSource.RaiseListChangedEvents = true;
                atlantaDataSetBindingSource.Filter = ""; // "Mobitel_ID >= 0";
                Select(curMrow);
                SetStartButton();
                EnableButton();
            } // if
            else
            {
                SetStartButton();
                _stopRun = true;
                StopReport();

                if (!noData)
                    EnableButton();
            } // else
        } // bbiStart_ItemClick

        // show graph - click button graph
        protected override void bbiShowOnGraf_ItemClick(object sender, ItemClickEventArgs e)
        {
            ReportsControl.OnGraphShowNeeded();
            DrawGraph();
        } // bbiShowOnGraf_ItemClick

        protected void DrawGraph()
        {
            Graph.ClearRegion();
            Graph.ClearLabel();

            Int32 [] list_row = gvFuelCtrlDutDay.GetSelectedRows();

            for (int i = 0; i < list_row.Length; i++)
            {
                FuelExpensesDataSet.FuelExpensesDUT_DayRow fd_row = (FuelExpensesDataSet.FuelExpensesDUT_DayRow)
                    ((DataRowView)atlantaDataSetBindingSource.List[list_row[i]]).Row;
                Graph.AddTimeRegion(fd_row.InitialTime, fd_row.FinalTime);

                atlantaDataSet.FuelReportRow[] FRRows = (atlantaDataSet.FuelReportRow[])dataset.FuelReport.Select(
                    String.Format("mobitel_id={0} AND time_ >= #{1:MM.dd.yyyy}# AND time_ < #{2:MM.dd.yyyy}#",
                    fd_row.Mobitel_Id, fd_row.FinalTime, fd_row.FinalTime.AddDays(1)), "time_");

                if (FRRows.Length > 0)
                {
                    foreach (atlantaDataSet.FuelReportRow tmp_FR_row in FRRows)
                    {
                        if (tmp_FR_row["time_"] != DBNull.Value)
                        {
                            string val;
                            Color color;
                            ShowGraphToolStripButton_ClickExtracted(tmp_FR_row, out val, out color);
                            Graph.AddLabel(tmp_FR_row.time_, tmp_FR_row.beginValue, tmp_FR_row.pointValue, val, color);
                        } // if
                    } // foreach
                } // if
            } // for
            if (buildGraph.AddGraphInclinometers(Graph, dataset, curMrow))
            {
                vehicleInfo = new VehicleInfo(curMrow);
                Graph.ShowSeries(vehicleInfo.Info);
            }
            Graph.SellectZoom();
        } // DrawGraph

        static void ShowGraphToolStripButton_ClickExtracted(atlantaDataSet.FuelReportRow f_row,
            out string val, out Color color)
        {
            val = String.Format("{0:#.#}л.", f_row.dValue);

            if (f_row.dValue > 0)
            {
                val = String.Format(Resources.FuelExControlFuelAdd + " {0}", val);
                color = Color.Green;
            } // if
            else
            {
                val = String.Format(Resources.FuelExControFuelPourOff + "{0}", val);
                color = Color.Red;
            } // else
        } // ShowGraphToolStripButton_ClickExtracted

        // show track on map - click button map
        protected override void bbiShowOnMap_ItemClick(object sender, ItemClickEventArgs e)
        {
            ReportsControl.OnMapShowNeeded();
            DrawMaps();
        } // bbiShowOnMap_ItemClick

        protected void DrawMaps()
        {
            ReportsControl.OnClearMapObjectsNeeded();
            Int32[] list_row = gvFuelCtrlDutDay.GetSelectedRows();

            if (list_row.Length == 0)
                return;

            FuelExpensesDataSet.FuelExpensesDUT_DayRow fd_row = (FuelExpensesDataSet.FuelExpensesDUT_DayRow)
                    ((DataRowView)atlantaDataSetBindingSource.List[list_row[0]]).Row;

            List<IGeoPoint> data = new List<IGeoPoint>();
            Algorithm.GetPointsInsided(fd_row.InitialPointId, fd_row.FinalPointId, data);

            if (data.Count > 0)
            {
                List<Track> segments = new List<Track>();
                segments.Add(new Track(fd_row.Mobitel_Id, Color.Red, 2f, data));
                ReportsControl.OnTrackSegmentsShowNeeded(segments);
            } // if
        } // DrawMaps

        static Track getTrackSegment(FuelExpensesDataSet.FuelExpensesDUT_DayRow row)
        {
            List<IGeoPoint> data = new List<IGeoPoint>();
            bool inside = false;
            foreach (atlantaDataSet.dataviewRow d_row in dataset.dataview.Select("Mobitel_ID=" + row.Mobitel_Id, "time ASC"))
            {
                if (!Convert.IsDBNull(row.InitialPointId) && d_row.DataGps_ID == row.InitialPointId)
                    inside = true;

                if (inside)
                    data.Add(DataSetManager.ConvertDataviewRowToDataGps(d_row));
                if (d_row.DataGps_ID == row.FinalPointId)
                    break;
            } // foreach
            return new Track(row.Mobitel_Id, Color.Red, 2f, data);
        } // getTrackSegment

        public void SelectItem(atlantaDataSet.mobitelsRow m_row)
        {
            Application.DoEvents();

            foreach (IAlgorithm alg in _algoritms)
            {
                alg.SelectItem(m_row);
                alg.Run();

                if (_stopRun) 
                    return;
            }
        } // SelctItem

        private int ContainsData(int id, int algo)
        {
            for (int i = 0; i < _summDutDay.Count; i++)
            {
                if (_summDutDay[i].id == id && _summDutDay[i].typeAlg == algo)
                {
                    return i;
                } // if
            } // for

            return -1;
        }

        protected override void Algorithm_Action(object sender, EventArgs e)
        {
            if (sender is TotalFuelExpensesDutDayAlgorithm)
            {
                FuelExpensesDutDayEventArgs args = (FuelExpensesDutDayEventArgs)e;
                int index = ContainsData(args.Id, args.TypeAlgorythm);

                if (index != -1)
                {
                    _summDutDay.RemoveAt(index);
                }

                _summDutDay.Add(args.SummaryDutDay);
            } // if
        } // Algorithm_Action

        private void gvFuelControlDutDay_ClickRow(object sender, RowClickEventArgs e)
        {
            if (e.RowHandle >= 0)
            {
                DrawMaps();
                DrawGraph();
            }
        }

        public override void ClearReport()
        {
            fuelAlgorythm.ClearAtlantaFuelReport();
            if (feDataSet != null)
                feDataSet.FuelExpensesDUT_Day.Clear();
            feDataSet.FuelExpensesDUT_Day.Clear();
            _summDutDay.Clear();
        }

        public void SelectGraphic(atlantaDataSet.mobitelsRow mobitel_row)
        {
            if ((mobitel_row == null) || !DataSetManager.IsGpsDataExist(mobitel_row))
            //if ((mobitel_row == null) || (mobitel_row.GetdataviewRows().Length == 0))
                return;

            curMrow = mobitel_row;
            vehicleInfo = new VehicleInfo(mobitel_row);
            string nameSeries = vehicleInfo.RegistrationNumber + vehicleInfo.CarMaker + vehicleInfo.CarModel;
            ReportsControl.GraphClearSeries();
            Graph.ClearRegion();
            ReportsControl.ShowGraph( curMrow );
            buildGraph.SetAlgorithm(AlgorithmType.FUEL1);
            buildGraph.AddGraphFuel(AlgorithmType.FUEL1, Graph, dataset, mobitel_row);
            buildGraph.SetAlgorithm(AlgorithmType.FUEL1_AGREGAT);
            buildGraph.AddGraphFuel(AlgorithmType.FUEL1_AGREGAT, Graph, dataset, mobitel_row);
            buildGraph.SetAlgorithm(AlgorithmType.FUEL2_AGREGAT);
            buildGraph.AddGraphFuel(AlgorithmType.FUEL2_AGREGAT, Graph, dataset, mobitel_row);
            buildGraph.SetAlgorithm(AlgorithmType.FUEL3_AGREGAT);
            buildGraph.AddGraphFuel(AlgorithmType.FUEL3_AGREGAT, Graph, dataset, mobitel_row);
            buildGraph.SetAlgorithm(AlgorithmType.FUEL_AGREGAT_ALL);
            buildGraph.AddGraphFuel(AlgorithmType.FUEL_AGREGAT_ALL, Graph, dataset, mobitel_row);
            Graph.ShowSeries(nameSeries);
            
            if (buildGraph.AddGraphInclinometers(Graph, dataset, curMrow))
            {
                vehicleInfo = new VehicleInfo(curMrow);
                Graph.ShowSeries(vehicleInfo.Info);
            }
        } // SelectGraphic

        // формирование отчета по текущей машине
        protected override void ExportToExcelDevExpress()
        {
            XtraGridService.SetupGidViewForPrint(gvFuelCtrlDutDay, true, true);

            TInfoDut t_info = new TInfoDut();

            t_info.periodBeging = Algorithm.Period.Begin;
            t_info.periodEnd = Algorithm.Period.End;

            atlantaDataSet.mobitelsRow mobitel = dataset.mobitels.FindByMobitel_ID(curMrow.Mobitel_ID);
            VehicleInfo info = new VehicleInfo(mobitel.Mobitel_ID, mobitel);
           
            t_info.infoVehicle = info.Info; // Транспорт
            t_info.infoDriverName = info.DriverFullName; // Водитель транспорта
            TotalFuelExpensesDutDayAlgorithm.SummaryDutDay _summariesDutDay = getSummary(mobitel.Mobitel_ID, globalCode);
            t_info.totalWay = _summariesDutDay.Distance; // Общий пройденный путь
            t_info.totalTimerTour = _summariesDutDay.TimeTotalWork; // Общая продолжительность смен
            t_info.totalTimeWay = _summariesDutDay.TimeTotalMove; // Общее время в движении , ч
            t_info.totalStops = _summariesDutDay.TimeTotalWork.Subtract(_summariesDutDay.TimeTotalMove); // Общее время стоянок , ч
            t_info.totalEngineOn = _summariesDutDay.TimeTotalWithEngineOnHour; // Общее время работы со включенным двигателем, ч
            t_info.totalFuel = _summariesDutDay.TotalDebit; // Общий расход топлива, л
            t_info.totalFuelAdd = _summariesDutDay.TotalFuelAdd; // Всего заправлено, л
            t_info.totalFuelSub = _summariesDutDay.TotalFuelSub; // Всего слито, л
            t_info.MiddleFuel = _summariesDutDay.Distance == 0 ? 0 : Math.Round((100 * _summariesDutDay.TotalDebit / _summariesDutDay.Distance), 2); // Средний расход топлива, л/100 км
            t_info.MotoHour = _summariesDutDay.TimeTotalWithEngineOnHour.TotalHours == 0 ? 0 : Math.Round((_summariesDutDay.TotalDebit / _summariesDutDay.TimeTotalWithEngineOnHour.TotalHours), 2); // Средний расход топлива, л/моточас

            ReportingFuelDutDay.AddInfoStructToList(t_info); /* формируем заголовки таблиц отчета */
            ReportingFuelDutDay.CreateAndShowReport(gcFuelCtrlDutDay);
        } // ExportToExcelDevExpress

        private IEnumerable<int> getSummaryKeys(int type)
        {
            List<int> keys = new List<int>();
            for (int i = 0; i < _summDutDay.Count; i++)
            {
                if (type == _summDutDay[i].typeAlg)
                    keys.Add(_summDutDay[i].id);
            }

            return keys;
        }

        // Формирует данные для отчета по всем активным машинам
        protected override void ExportAllDevToReport()
        {
            IEnumerable<int> keys = getSummaryKeys(globalCode);

            if (keys.Count() == 0)
                return;

            foreach (int mobitelId in keys)
            {
                TInfoDut t_info = new TInfoDut();
                atlantaDataSet.mobitelsRow mobitel = dataset.mobitels.FindByMobitel_ID(mobitelId);
                VehicleInfo info = new VehicleInfo(mobitel.Mobitel_ID, mobitel);
                t_info.periodBeging = Algorithm.Period.Begin;
                t_info.periodEnd = Algorithm.Period.End;
                t_info.infoVehicle = info.Info; // Транспорт
                t_info.infoDriverName = info.DriverFullName; // Водитель транспорта
                TotalFuelExpensesDutDayAlgorithm.SummaryDutDay _summariesDutDay = getSummary(mobitelId, globalCode);
                t_info.totalWay = _summariesDutDay.Distance; // Общий пройденный путь
                t_info.totalTimerTour = _summariesDutDay.TimeTotalWork; // Общая продолжительность смен
                t_info.totalTimeWay = _summariesDutDay.TimeTotalMove; // Общее время в движении , ч
                t_info.totalStops = _summariesDutDay.TimeTotalWork.Subtract(_summariesDutDay.TimeTotalMove); // Общее время стоянок , ч
                t_info.totalEngineOn = _summariesDutDay.TimeTotalWithEngineOnHour; // Общее время работы со включенным двигателем, ч
                t_info.totalFuel = _summariesDutDay.TotalDebit; // Общий расход топлива, л
                t_info.totalFuelAdd = _summariesDutDay.TotalFuelAdd; // Всего заправлено, л
                t_info.totalFuelSub = _summariesDutDay.TotalFuelSub; // Всего слито, л
                t_info.MiddleFuel = _summariesDutDay.Distance == 0 ? 0 : Math.Round((100 * _summariesDutDay.TotalDebit / _summariesDutDay.Distance), 2); // Средний расход топлива, л/100 км
                t_info.MotoHour = _summariesDutDay.TimeTotalWithEngineOnHour.TotalHours == 0 ? 0 : Math.Round((_summariesDutDay.TotalDebit / _summariesDutDay.TimeTotalWithEngineOnHour.TotalHours), 2); // Средний расход топлива, л/моточас

                ReportingFuelDutDay.AddInfoStructToList(t_info);
                ReportingFuelDutDay.CreateBindDataList();

                foreach (FuelExpensesDataSet.FuelExpensesDUT_DayRow row in feDataSet.FuelExpensesDUT_Day.Select("Mobitel_Id=" + mobitelId + " and " + "TypeAlgorythm=" + globalCode, " Id ASC"))
                {
                    string date = row.InitialTime.ToString("dd.MM.yyyy");
                    string iniTime = row.InitialTime.ToString("t");
                    string finTime = row.FinalTime.ToString("t");
                    string intervWork = row.IntervalWork;
                    string intervMove = row.IntervalMove;
                    string intervStops = row.IntervalStop;
                    string timeEngineOn = (row["TimeWithEngineOn"] != DBNull.Value) ? row.TimeWithEngineOn : " нет данных ";
                    double distance = Math.Round(row.Distance, 2);
                    double fuel_begin = Math.Round(row.FuelBegin, 2);
                    double fuel_end = Math.Round(row.FuelEnd, 2);
                    double total_fuel_add = Math.Round(row.TotalFuelAdd, 2);
                    double total_fuel_sub = Math.Round(row.TotalFuelSub, 2);
                    double total_fuel_use = Math.Round(row.TotalFuelUse, 2);
                    double fuel_use_middle_motion = (row["FuelUseMiddleInMotion"] != DBNull.Value) ? Math.Round(row.FuelUseMiddleInMotion, 2) : 0;
                    double idle_fuel_rate = (row["idleFuelRate"] != DBNull.Value) ? row.idleFuelRate : 0; 

                    // сохранить данные таблицы отчета в списке таблиц
                    ////
                    ReportingFuelDutDay.AddDataToBindList(new repFuelDutDay(date, iniTime, finTime, intervWork,
                        intervMove, intervStops, timeEngineOn, distance, fuel_begin, fuel_end, total_fuel_add,
                        total_fuel_sub, total_fuel_use, fuel_use_middle_motion, idle_fuel_rate));
                } // foreach

                ReportingFuelDutDay.CreateElementReport();
            } // foreach

            ReportingFuelDutDay.CreateAndShowReport();
            ReportingFuelDutDay.DeleteData();
        } // ExportAllDevToReport

        // формирование колонтитулов верхних отчета
        private void composLink_CreateMarginalHeaderArea(object sender, CreateAreaEventArgs e)
        {
            DevExpressReportHeader(Resources.TotalFuelExpensesDutDayReportName, e);
            TInfoDut info = ReportingFuelDutDay.GetInfoStructure;
            string strPeriod = Resources.PeriodFrom + " " + info.periodBeging + " " +
                Resources.PeriodTo + " " + info.periodEnd;
            DevExpressReportSubHeader(strPeriod, 22, e);
        }

        /* функция для формирования левой части заголовка отчета */
        protected string GetStringBreackLeft()
        {
            TInfoDut info = ReportingFuelDutDay.GetInfoStructure;
            ReportingFuelDutDay.SetRectangleBrckLetf(0, 0, 300, 85);
            return (Resources.Vehicle + ": " + info.infoVehicle + "\n" +
                Resources.Driver + ": " + info.infoDriverName);
        }

        /* функция для формирования верхней/средней части заголовка отчета*/
        protected string GetStringBreackUp()
        {
            TInfoDut info = ReportingFuelDutDay.GetInfoStructure;
            ReportingFuelDutDay.SetRectangleBrckUP(380, 0, 320, 85);
            return (Resources.FuelExControlTravel + ": " + String.Format("{0:f2}", info.totalWay) + "\n" +
                Resources.FuelExControlPeriodWork + ": " + info.totalTimerTour + "\n" +
                Resources.FuelExControlTotalTimeMove + ": " + info.totalTimeWay + "\n" +
                Resources.FuelExControDutDayTimeStops + ": " + info.totalStops + "\n" +
                Resources.FuelExControlTimeEngineOn + ": " + info.totalEngineOn + "\n");
        }

        /* функция для формирования правой части заголовка отчета */
        protected string GetStringBreackRight()
        {
            TInfoDut info = ReportingFuelDutDay.GetInfoStructure;
            ReportingFuelDutDay.SetRectangleBrckRight(770, 0, 300, 85);
            return (Resources.FuelExControlCommonFuelrate + ": " + String.Format("{0:f2}", info.totalFuel) + "\n" +
                Resources.FuelExControlDutCommonFuelAdd + ": " + String.Format("{0:f2}", info.totalFuelAdd) + "\n" +
                Resources.FuelExControlDutCommonFuelPourOff + ": " + String.Format("{0:f2}", info.totalFuelSub) + "\n" +
                Resources.FuelExControlDutDayFuelrate + ": " + String.Format("{0:f2}", info.MiddleFuel) + "\n" +
                Resources.FuelExControDutDayFuelrateOnTime + ": " + String.Format("{0:f2}", info.MotoHour));
        } // GetStringBreackRight
        
        protected override void barButtonGroupPanel_ItemClick(object sender, ItemClickEventArgs e)
        {
            GroupPanel(gvFuelCtrlDutDay);
        }
        
        protected override void barButtonFooterPanel_ItemClick(object sender, ItemClickEventArgs e)
        {
            FooterPanel(gvFuelCtrlDutDay);
        }
        
        protected override void barButtonNavigator_ItemClick(object sender, ItemClickEventArgs e)
        {
            NavigatorPanel(gcFuelCtrlDutDay);
        }

        protected override void barButtonStatusPanel_ItemClick(object sender, ItemClickEventArgs e)
        {
            StatusBar(bar3);
        }

        private void Localization()
        {
            colInitialTime.Caption = Resources.FuelExControDutDayDate;
            colInitialTime.ToolTip = Resources.FuelExControDutDayDate;

            colBeginMotionTime.Caption = Resources.FuelExControDutDayTimeBeginMoving;
            colBeginMotionTime.ToolTip = Resources.FuelExControDutDayTimeBeginMoving;

            colEndMotionTime.Caption = Resources.FuelExControDutDayTimeEndMoving;
            colEndMotionTime.ToolTip = Resources.FuelExControDutDayTimeEndMoving;

            colIntervalWork.Caption = Resources.FuelExControDutDayTimeWorkingShift;
            colIntervalWork.ToolTip = Resources.FuelExControDutDayTimeWorkingShift;

            colIntervalMove.Caption = Resources.FuelExControlTimeMove;
            colIntervalMove.ToolTip = Resources.FuelExControlTimeMove;

            colIntervalStop.Caption = Resources.FuelExControDutDayTimeStops;
            colIntervalStop.ToolTip = Resources.FuelExControDutDayTimeStops;

            colTimeWithEngineOn.Caption = Resources.FuelExControDutDayTimeWorkEngine;
            colTimeWithEngineOn.ToolTip = Resources.FuelExControDutDayTimeWorkEngine;

            colDistance.Caption = Resources.FuelExControlTravel;
            colDistance.ToolTip = Resources.FuelExControlTravel;

            colFuelBegin.Caption = Resources.FuelExControDutDayFuelInBegin;
            colFuelBegin.ToolTip = Resources.FuelExControDutDayFuelInBegin;

            colFuelEnd.Caption = Resources.FuelExControDutDayFuelInEnd;
            colFuelEnd.ToolTip = Resources.FuelExControDutDayFuelInEnd;

            colTotalFuelAdd.Caption = Resources.FuelExControDutDayFuelAdds;
            colTotalFuelAdd.ToolTip = Resources.FuelExControDutDayFuelAdds;

            colTotalFuelSub.Caption = Resources.FuelExControDutDayFuelPoursOff;
            colTotalFuelSub.ToolTip = Resources.FuelExControDutDayFuelPoursOff;

            colTotalFuelUse.Caption = Resources.FuelExControlCommonFuelrate;
            colTotalFuelUse.ToolTip = Resources.FuelExControlCommonFuelrate;

            colFuelUseMiddleInMotion.Caption = Resources.FuelExControDutDayFuelrate;
            colFuelUseMiddleInMotion.ToolTip = Resources.FuelExControDutDayFuelrate;

            colIdleFuelRate.Caption = Resources.FuelExControDutDayFuelOnTime;
            colIdleFuelRate.ToolTip = Resources.FuelExControDutDayFuelOnTime;

            lblTotalDebitText.Caption = Resources.FuelExControlCommonFuelrate + ": ---";
            lblTotalFuelAddText.Caption = Resources.FuelExControlDutCommonFuelAdd + ": ---";
            lblTotalFuelSubText.Caption = Resources.FuelExControlDutCommonFuelPourOff + ": ---";
            lblAvgFuelDebitText.Caption = Resources.FuelExControlDutDayFuelrate + ": ---";
            lblTotalMotoText.Caption = Resources.FuelExControDutDayFuelrateOnTime + ": ---";
        } // Localization

        protected override void comboAlgoChoiser_ItemClick(object sender, EventArgs e)
        {
            if (curMrow == null)
                return;

            ComboBoxEdit combo = (ComboBoxEdit)sender;
            comboAlgorithm = combo;
            int index = combo.SelectedIndex;
            int code = -1;

            if (index == 0)
            {
                code = (int)AlgorithmType.FUEL1;
                globalCode = code;
                UpdateStatusStrip(curMrow.Mobitel_ID, globalCode);
                atlantaDataSetBindingSource.Filter = "Mobitel_Id=" + curMrow.Mobitel_ID + " and " + "TypeAlgorythm=" + code;
                SelectGraphic(curMrow, code);
            }
            else if (index == 1)
            {
                code = (int)AlgorithmType.FUEL1_AGREGAT;
                globalCode = code;
                UpdateStatusStrip(curMrow.Mobitel_ID, globalCode);
                atlantaDataSetBindingSource.Filter = "Mobitel_Id=" + curMrow.Mobitel_ID + " and " + "TypeAlgorythm=" + code;
                SelectGraphic(curMrow, code);
            }
            else if (index == 2)
            {
                code = (int)AlgorithmType.FUEL2_AGREGAT;
                globalCode = code;
                UpdateStatusStrip(curMrow.Mobitel_ID, globalCode);
                atlantaDataSetBindingSource.Filter = "Mobitel_Id=" + curMrow.Mobitel_ID + " and " + "TypeAlgorythm=" + code;
                SelectGraphic(curMrow, code);
            }
            else if (index == 3)
            {
                code = (int)AlgorithmType.FUEL3_AGREGAT;
                globalCode = code;
                UpdateStatusStrip(curMrow.Mobitel_ID, globalCode);
                atlantaDataSetBindingSource.Filter = "Mobitel_Id=" + curMrow.Mobitel_ID + " and " + "TypeAlgorythm=" + code;
                SelectGraphic(curMrow, code);
            }
            else if (index == 4)
            {
                code = (int)AlgorithmType.FUEL_AGREGAT_ALL;
                globalCode = code;
                UpdateStatusStrip(curMrow.Mobitel_ID, globalCode);
                atlantaDataSetBindingSource.Filter = "Mobitel_Id=" + curMrow.Mobitel_ID + " and " + "TypeAlgorythm=" + code;
                SelectGraphic(curMrow, code);
            }
            else
            {
                XtraMessageBox.Show("Неизвестный алгоритм", "Выбор алгоритма", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        } // comboAlgoChoiser_ItemClick
    } // DevFuelExpensesCtrlDutDay
} // ReportsOnFuelExpenses
