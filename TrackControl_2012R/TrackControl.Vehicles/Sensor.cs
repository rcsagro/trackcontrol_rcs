﻿using System;
using System.Collections;
using TrackControl.Vehicles.Properties;
using DevExpress.XtraEditors;


namespace TrackControl.Vehicles
{
    public class Sensor : ExternalDevice
    {
        public SensorState SensState { get; set; }

        public Sensor(string name, string description, int startBit, int bitLength, double k, double b, bool s)
            : base(name, description, startBit, bitLength)
        {
            K = k;
            B = b;
            S = s;
        }

        public double Value { get; set; }

        public string ValueStr { get; set; }

        public double K { get; set; }

        public double B { get; set; }

        public bool S { get; set; }

        public string SensorValue { get; private set; }

        public string UnitName { get; set; }

        public Sensor(int idSensor)
        {
            if (idSensor > 0)
            {
                var sensor = SensorProvider.GetSensor(idSensor);
                if (sensor != null)
                {
                    SetSensorsParams(sensor);
                }
            }
        }

        public Sensor(int idAlgoritm, int idMobitel)
        {
            var sensor = SensorProvider.GetSensorWithAlgoritm(idAlgoritm, idMobitel);

            if (sensor != null)
            {
                SetSensorsParams(sensor);
            }
        }

        public Sensor(int idAlgoritm, int idMobitel, int sensorExcludeId)
        {
            var sensor = SensorProvider.GetSensorWithAlgoritmExcludeSensor(idAlgoritm, idMobitel, sensorExcludeId);
            if (sensor != null)
            {
                SetSensorsParams(sensor);
            }
        }

        public Sensor(string name, string sensorValue, string rfid)
        {
            Name = name;
            SensorValue = sensorValue;
            RfidValue = rfid;
        }

        public Sensor(string name, string sensorValue)
        {
            Name = name;
            SensorValue = sensorValue;
        }

        public Sensor(Sensor sensor)
        {
            if (sensor != null)
            {
                SetSensorsParams(sensor);
            }
        }

        private void SetSensorsParams(Sensor sensor)
        {
            Id = sensor.Id;
            this.Name = sensor.Name;
            this.Description = sensor.Description;
            this.AssignBits(sensor.StartBit, sensor.BitLength);
            this.K = sensor.K;
            B = sensor.B;
            S = sensor.S;
            this.Algoritm = sensor.Algoritm;
        }

        public virtual double GetValue(byte[] sensorDataGps)
        {
            if (!Valid)
            {
                XtraMessageBox.Show(Resources.WrongSensorSetup, Name);
                //throw new Exception(String.Format("Sensor has wrong settings StartBit and BitLength (Current value: {0} {1})", StartBit, BitLength));
            }

            Value = 0;
            var bits = new BitArray(sensorDataGps);

            for (int i = 0; i < BitLength; i++)
            {
                ulong d = Convert.ToUInt64(bits[i + (int) StartBit]);
                Value += d << i;
            }

            return Value;
        }

        public int MobitelId { get; set; }
        public string NumTelephone { get; set; }
        public int IDSensor { get; set; }
        public string RfidValue { get; set; }

        /// <summary>
        /// Заложено для угломера агро + дальномер
        /// </summary>
        /// <param name="sensorDataGps">значения датчика из DataGps</param>
        /// <param name="isAngle"> для угломерного датчика делаем преобразование в градусы</param>
        /// <returns></returns>
        public override bool IsActive(byte[] sensorDataGps,bool isAngle = false)
        {
            if (SensState == null) return false;
            double value = GetValue(sensorDataGps);
            if (isAngle)
                value = GetConvertAngleSensorValue((int)value);
            if (value >= SensState.MinValue && value <= SensState.MaxValue)
                return true;
            else
                return false;

        }

        private double GetConvertAngleSensorValue(int sensorValue)
        {
            return sensorValue;
            //if (sensorValue > Math.Pow(2, BitLength - 1) - 1)
            //{
            //    sensorValue = sensorValue - (int)Math.Pow(2, BitLength);
            //}
            //return (double)sensorValue * Math.Pow(2, 16 - BitLength) / 100;
            //для INCLINOMETER_X вместо / 100 нужно /256
            //return (sensorValue - Math.Pow(2, BitLength - 1))*180/Math.Pow(2, BitLength - 1);
        }

        public double GetAngleSensorValue(byte[] sensorDataGps)
        {
            double value = GetValue(sensorDataGps);
            return GetConvertAngleSensorValue((int)value);
        }


        public int GetMaxRfidValue()
        {
            return  ((1 << BitLength) - 1);
        }

        public bool SetSensorStateAgregat(int idAgregat)
        {
            SensState = SensorProvider.GetSensorStateAgregat(Id, idAgregat); 
            if (SensState!=null && SensState.Id >0)
                return true;
            else
                return false;
        }

        public bool SetSensorStateAgregat(double MinValue,double MaxValue)
        {
            SensState = new SensorState {MinValue = MinValue, MaxValue = MaxValue};
            if (SensState != null)
                return true;
            else
                return false;
        }
    }
}
