using System;
using TrackControl.General;

namespace TrackControl.Vehicles
{
    /// <summary>
    /// �������� �������� ������������� ��������. ������������ �������� ������ atlantaDataset.settingRow
    /// � ��������� ������ ���� ������������.
    /// </summary>
    public class VehicleSettings : Entity
    {
        TimeSpan _timeBreak = new TimeSpan(0, 5, 0);

        /// <summary>
        /// ����������� ����� ���������
        /// </summary>
        public TimeSpan TimeBreak
        {
            get { return _timeBreak; }
            set { _timeBreak = value; }
        }


        /// <summary>
        /// ����. ����� ����������� �������
        /// </summary>
        TimeSpan _timeBreakMaxPermitted = new TimeSpan(3, 0, 0);

        public TimeSpan TimeBreakMaxPermitted
        {
            get { return _timeBreakMaxPermitted; }
            set { _timeBreakMaxPermitted = value; }
        }

        float _speedMax = 60F;

        public float SpeedMax
        {
            get { return _speedMax; }
            set { _speedMax = value; }
        }

        /// <summary>
        /// ������� ������ ������� � ���
        /// </summary>
        double _avgFuelRatePerHour;

        public double AvgFuelRatePerHour
        {
            get { return _avgFuelRatePerHour; }
            set { _avgFuelRatePerHour = value; }
        }

        /// <summary>
        /// ����������� �������� �������� �������� �������, ��� ����� �������� � ����� � �����������
        /// </summary>
        int _fuelerMinFuelRate;

        public int FuelerMinFuelRate
        {
            get { return _fuelerMinFuelRate; }
            set { _fuelerMinFuelRate = value; }
        }


        public VehicleSettings()
        {

        }

        public VehicleSettings(int settingId)
        {
            VehicleProvider2.GetSettings(settingId, this);  
        }

    }
}