﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Schema;
using TrackControl.General;
using TrackControl.Vehicles.Properties;

namespace TrackControl.Vehicles
{
    public abstract class EntityPcb : IEntityPcb
    {
        private int Id;
        private string name;

        public EntityPcb()
        {
            Id = -1;
            name = "";
        }

        public int ID 
        {
            get
            {
                return Id;
            }
        }

        public string Name
        {
            get { return name; }
        }
    }

    public class PcbVersions : EntityPcb
    {
        private int IdPcb;
        private string NamePcb;
        private string CommentPcb;
        //private int MobiId;
        private const int EMPTY_ID = -1;

        public PcbVersions(int id, string name, string comment)
        {
            IdPcb = id;
            NamePcb = name;
            CommentPcb = comment;
        }

        public int ID
        {
            get { return IdPcb; }
        }

        public string Name
        {
            get { return NamePcb; }
        }

        public bool IsEmpty
        {
            get { return IdPcb == EMPTY_ID; }
        }

        public static PcbVersions Empty
        {
            get
            {
                PcbVersions empty = new PcbVersions( -1, String.Format( "-- {0} -- ", Resources.PcbEmptys ), "" );
                return empty;
            }
        }

        public override string ToString()
        {
            return String.Format( "{0}", this.NamePcb );
        }
    }
}
