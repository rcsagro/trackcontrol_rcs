using System;
using TrackControl.General;
using System.Diagnostics;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace TrackControl.Vehicles
{
    public class GpsData : Entity64, IGeoPoint
    {
        public int LogId
        {
            [DebuggerStepThrough] get { return _logID; }
            [DebuggerStepThrough] set { _logID = value; }
        }

        private int _logID;

        public PointLatLng LatLng
        {
            [DebuggerStepThrough] get { return _latLng; }
            [DebuggerStepThrough] set { _latLng = value; }
        }

        private PointLatLng _latLng;

        public int Altitude
        {
            [DebuggerStepThrough] get { return _altitude; }
            [DebuggerStepThrough] set { _altitude = value; }
        }

        private int _altitude;

        public int Mobitel
        {
            [DebuggerStepThrough] get { return _mobitel; }
            [DebuggerStepThrough] set { _mobitel = value; }
        }

        private int _mobitel;

        public DateTime Time
        {
            [DebuggerStepThrough] get { return _time; }
            [DebuggerStepThrough] set { _time = value; }
        }

        private DateTime _time;

        public bool Valid
        {
            [DebuggerStepThrough] get { return _valid; }
            [DebuggerStepThrough] set { _valid = value; }
        }

        private bool _valid;

        public float Speed
        {
            [DebuggerStepThrough] get { return _speed; }
            [DebuggerStepThrough] set { _speed = value; }
        }

        private float _speed;

        public byte[] Sensors { get; set; }

        //public ulong Sensors
        //{
        //    [DebuggerStepThrough]
        //    get { return _sensors; }
        //    [DebuggerStepThrough]
        //    set { _sensors = value; }
        //}
        //ulong _sensors;

        //public byte[] Sensors64 { get; set; }


        public UInt32 Events
        {
            [DebuggerStepThrough] get { return _events; }
            [DebuggerStepThrough] set { _events = value; }
        }

        private UInt32 _events;

        public string sEvents
        {
            get { return _sevents; }
            set { _sevents = value; }
        }

        private string _sevents;

        /// <summary>
        /// ����������� ��������.
        /// </summary>
        public double Accel
        {
            //[DebuggerStepThrough]
            get { return _accel; }
            //[DebuggerStepThrough]
            set
            {
                _accel = value;
            }
        }

        private double _accel;

        /// <summary>
        /// ����������� ��������.
        /// </summary>
        public double Dist
        {
            [DebuggerStepThrough] get { return _dist; }
            [DebuggerStepThrough] set { _dist = value; }
        }

        private double _dist;

        public int UnixTime { get; set; }

        public double Direction { get; set; }

        public override string ToString()
        {
            return String.Format("Time:{0:g} Lat:{1:N5} Lng:{2:N5} Speed:{3} Dist:{4}", _time, _latLng.Lat, _latLng.Lng,
                _speed, _dist);
        }

        /// <summary>
        /// ��������������� � ����������� �������� �������� ������ �� �������� 
        /// </summary>
        public double SensorValue { get; set; }

        public short Voltage { get; set; }

        public double sVoltage { get; set; }

        public short RssiGsm { get; set; }

        public short SensorsSet { get; set; }

        public short Satellites { get; set; }

        public PointLatLng LatLngDgps { get; set; }

        public Int64 SrvPacketID { get; set; }
    }
}
