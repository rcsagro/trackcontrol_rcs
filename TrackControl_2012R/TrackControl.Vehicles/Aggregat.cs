﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TrackControl.General;
using TrackControl.General.DAL;
using TrackControl.Vehicles.DAL;
using TrackControl.Vehicles.Properties;

namespace TrackControl.Vehicles
{
    /// <summary>
    /// Сущность, представляющая абстракцию прицепного агрегата транспортного средства
    /// </summary>
    public class Aggregat : Entity
    {
        const int EMPTY_ID = Int32.MinValue;
        UInt16? _identifier; // RFID
        private string _aggregateName;
        private int _IdAggregate; 

        /// <summary>
        /// Идентификатор агрегата
        /// </summary>
        public UInt16? Identifier
        {
            get { return _identifier; }
            set { _identifier = value; }
        }

        /// <summary>
        /// Идентификатор агрегата
        /// </summary>
        public int IDAggregate
        {
            get { return _IdAggregate; }
            set { _IdAggregate = value; }
        }
        
        /// <summary>
        /// Название агрегата
        /// </summary>
        public string AggregateName
        {
            get { return _aggregateName; }
            set { _aggregateName = value; }
        }

        public Aggregat()
        {
            this.Id = 0;
            this.Identifier = 0;
            this.AggregateName = "";
        }

        public Aggregat(int idmobitel, UInt16? identifier)
        {
            Aggregat aggregat = null;

            if (identifier > 0)
            {
                aggregat = VehicleProvider2.GetAggregateFromIdentifier(identifier);
                if (aggregat.AggregateName == "")
                    aggregat = VehicleProvider2.GetAggregateFromMobitel(idmobitel);
            }
            else
                aggregat = VehicleProvider2.GetAggregateFromMobitel(idmobitel);


            if (aggregat != null)
            {
                if (aggregat.AggregateName == "")
                    aggregat.AggregateName = Resources.NoData;
                this.IDAggregate = aggregat.Id;
                this.Identifier = aggregat.Identifier;
                this.AggregateName = aggregat.AggregateName;
            }
        }

        public static Aggregat Empty
        {
            get
            {
                Aggregat empty = new Aggregat(0, 0);
                empty.Id = EMPTY_ID;
                return empty;
            }
        }

        public bool IsEmpty
        {
            get { return Id == EMPTY_ID; }
        }

        public override string ToString()
        {
            return String.Format("name={0}, rfid={1}, id={2}", _aggregateName, _identifier, _IdAggregate);
        }
    } // Aggregat
}
