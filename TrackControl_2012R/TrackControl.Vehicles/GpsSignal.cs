using System;

namespace TrackControl.Vehicles
{
  public enum GpsSignal
  {
    NotValid = 0,
    Valid = 1,
    NotDetected = 2
  }
}
