﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TrackControl.General;

namespace TrackControl.Vehicles
{
    public class VehicleCategory3 : Entity
    {
        public VehicleCategory3()
        {
            // to do this
        }

        public VehicleCategory3(int id, string name)
        {
            Id = id;
            Name = name;
        }

        public VehicleCategory3(int id)
        {
            VehicleCategory3 vc = VehicleCategoryProvader.GetCategory3(id);

            if (vc != null)
            {
                Id = vc.Id;
                Name = vc.Name;
            }
        }

        public string Name { get; set; }

        public bool Save()
        {
            if (IsNew)
            {
                Id = VehicleCategoryProvader.Insert3(this);
                return (Id != ConstsGen.RECORD_MISSING);
            }
            else
            {
                return VehicleCategoryProvader.Update3(this);
            }
        }

        public bool Delete()
        {
            return VehicleCategoryProvader.Delete3(this);
        }

        public override string ToString()
        {
            return String.Format("{0}", Name);
        }
    }
}
