using System;
using TrackControl.General;
using TrackControl.Vehicles.Properties;

namespace TrackControl.Vehicles
{
    public abstract class ExternalDevice : Entity
    {
        string _name;
        string _description;
        int _startBit;
        int _bitsLength;
        bool _valid;
        int _algoritm;
        string numTelephone = "";

        public int Algoritm
        {
            get { return _algoritm; }
            set { _algoritm = value; }
        }

        public string NumTelephone
        {
            get { return numTelephone; }
            set { numTelephone = value; }
        }

        public ExternalDevice()
        {

        }

        public ExternalDevice(string name, string description, int startBit, int bitsLength)
        {
            Name = name;
            Description = description;
            AssignBits(startBit, bitsLength);
        }

        public string Name
        {
            get { return _name; }
            set
            {
                if (String.IsNullOrEmpty(value))
                {
                    _name = Resources.NotDefined;
                }
                else
                {
                    value = value.Trim();
                    if (value.Length == 0)
                        _name = Resources.NotDefined;
                    else if (value.Length > 45)
                        _name = String.Format("{0}...", value.Remove(40));
                    else
                        _name = value;
                }
            }
        }

        public string Description
        {
            get { return _description; }
            set
            {
                if (null == value)
                {
                    _description = String.Empty;
                }
                else
                {
                    value = value.Trim();
                    if (value.Length > 200)
                        _description = String.Format("{0}...", value.Remove(196));
                    else
                        _description = value;
                }
            }
        }

        public bool Valid
        {
            get { return _valid; }
        }

        public int StartBit
        {
            get { return _startBit; }
        }

        public int BitLength
        {
            get { return _bitsLength; }
        }

        protected void AssignBits(int startBit, int bitLength)
        {
            _startBit = startBit;
            _bitsLength = bitLength;
            _valid = ValidateAssignedBits();
        }

        bool ValidateAssignedBits()
        {
            //return _startBit >= 0 && _startBit < 64 && _bitsLength > 0 && _bitsLength <= 64 && (_startBit + _bitsLength) <= 64;
            return _startBit >= 0;
        }

        public void SetAlgoritm()
        {
            if (Id > 0)
            {
                _algoritm = SensorProvider.GetAlgoritm(Id);
            }
        }

        public virtual bool IsActive(byte[] sensorDataGps, bool isAngle = false)
        {
            return false;
        }
    }
}
