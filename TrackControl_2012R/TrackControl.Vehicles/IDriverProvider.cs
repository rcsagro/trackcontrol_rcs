using System.Collections.Generic;

namespace TrackControl.Vehicles
{
  public interface IDriverProvider
  {

    bool Save(Driver driver);
    bool Delete(Driver driver);

    bool IsPermitToDelete(Driver driver);
  }
}
