using System;
using System.Collections.Generic;

namespace TrackControl.Vehicles
{
  public interface IGpsDataProvider
  {
    IList<GpsData> GetValidDataForPeriod(Vehicle vehicle, DateTime begin, DateTime end);
  }
}
