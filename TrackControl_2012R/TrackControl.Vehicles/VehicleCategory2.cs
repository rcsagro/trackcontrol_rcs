﻿using System;
using System.Collections.Generic;
using System.Text;
using TrackControl.General;

namespace TrackControl.Vehicles
{
    public class VehicleCategory2 : Entity
    {
        public VehicleCategory2()
        {
            // to do this
        }

        public VehicleCategory2(int id, string name)
        {
            Id = id;
            Name = name;
        }

        public VehicleCategory2(int id)
        {
            VehicleCategory2 vc = VehicleCategoryProvader.GetCategory2(id);
            if (vc != null)
            {
                Id = vc.Id;
                Name = vc.Name;
            }
        }

        public string Name { get; set; }

        public bool Save()
        {
            if (IsNew)
            {
                Id = VehicleCategoryProvader.Insert2(this);
                return (Id != ConstsGen.RECORD_MISSING);
            }
            else
            {
                return VehicleCategoryProvader.Update2(this);
            }
        }

        public bool Delete()
        {
            return VehicleCategoryProvader.Delete2(this);
        }

        public override string ToString()
        {
            return String.Format("{0}", Name);
        }
    }
}
