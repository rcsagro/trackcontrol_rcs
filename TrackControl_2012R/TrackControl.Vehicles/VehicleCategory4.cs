﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TrackControl.General;

namespace TrackControl.Vehicles
{
    public class VehicleCategory4 : Entity
    {
        public VehicleCategory4()
        {
            // to do this
        }

        public VehicleCategory4(int id, string name)
        {
            Id = id;
            Name = name;
        }

        public VehicleCategory4(int id)
        {
            VehicleCategory4 vc = VehicleCategoryProvader.GetCategory4(id);
            if (vc != null)
            {
                Id = vc.Id;
                Name = vc.Name;
            }
        }

        public string Name { get; set; }

        public bool Save()
        {
            if (IsNew)
            {
                Id = VehicleCategoryProvader.Insert4(this);

                return (Id != ConstsGen.RECORD_MISSING);
            }
            else
            {
                return VehicleCategoryProvader.Update4(this);
            }
        }

        public bool Delete()
        {
            return VehicleCategoryProvader.Delete4(this);
        }

        public override string ToString()
        {
            return String.Format("{0}", Name);
        }
    }
}
