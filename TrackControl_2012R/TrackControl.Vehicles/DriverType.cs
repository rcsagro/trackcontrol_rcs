﻿using System;
using System.Collections.Generic;
using System.Text;
using TrackControl.General;
using TrackControl.Vehicles.DAL;

namespace TrackControl.Vehicles
{
   public class DriverType : Entity
    {
        public string TypeName { set; get; }

        public DriverType()
        {
            // to do this
        }

        public DriverType(int id, string typeName)
        {
            Id = id;
            TypeName = typeName;
        }

        public DriverType(int id)
        {
            Id = id;
        }

        public bool Save()
        {
            if (IsNew)
            {
                Id = DriverVehicleProvider.InsertDriverType(this);
                return (Id != ConstsGen.RECORD_MISSING);
            }
            else
            {
                return DriverVehicleProvider.UpdateDriverType(this);
            }
        }

        public bool Delete()
        {
            return DriverVehicleProvider.DeleteDriverType(this);
        }
    }
}
