using System;
using TrackControl.General;
using System.Collections.Generic;

namespace TrackControl.Vehicles
{
    public interface IVehicle : IEntity
    {
        /// <summary>
        /// ������������ ��������
        /// </summary>
        Mobitel Mobitel { get; }

        /// <summary>
        /// ������������� ������������� ��������
        /// </summary>
        string CarMaker { get; }

        /// <summary>
        /// ������ ������������� ��������
        /// </summary>
        string CarModel { get; }

        /// <summary>
        /// ��������������� ����� ������������� ��������
        /// </summary>
        string RegNumber { get; }

        /// <summary>
        /// �������� ������������� ��������
        /// </summary>
        string Info { get; }

        /// <summary>
        /// ��������, ������������ �� ������������ ���������
        /// </summary>
        Driver Driver { get; }

        /// <summary>
        /// ����� �����������, ��������������� � ������������ ���������
        /// </summary>
        VehiclesGroup Group { get; set; }

        VehicleStyle Style { get; }

        /// <summary>
        /// ��������� 
        /// </summary>
        VehicleSettings Settings { get; }

        ///
        /// ������ ������� � �/100 �� ���� 
        /// 
        string FuelWays { get; }

        ///
        /// ������ ������� � �/����� ���  
        /// 
        string FuelMotor { get; }

        ///
        /// ������������� ������
        /// 
        int Identifier { get; }

        /// <summary>
        /// ������, ��������������� � ������������ ���������
        /// </summary>
        object Tag { get; set; }

        /// <summary>
        /// �������� ��������� ������� ��������� ������
        /// </summary>
        /// <returns></returns>
        int TimeStatus();

        List<Sensor> Sensors { get; set; }

        int LogicSensorState { get; set; }

        GpsData LastPoint { get; set; }

        bool Checked { get; set; }

        VehicleCategory  Category { get; set; }

        VehicleCategory2 Category2 { get; set; }

        VehicleCategory3 Category3 { get; set; }

        VehicleCategory4 Category4 { get; set; }
        
        string VehicleComment { get; set; }

        string CategoryMixed { get; }

        bool Is64BitPackets { get; set; }
    }
}