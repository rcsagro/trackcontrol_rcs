using System.Collections.Generic;
using System.Data ;
namespace TrackControl.Vehicles
{
  public interface IVehicleProvider
  {
    List<Vehicle> GetAll();
    void Save(Vehicle vehicle);
    DataTable GetAllDataTable();
  }
}
