using System;
using System.Data; 
using System.Collections.Generic;
using System.Windows.Forms;
using TrackControl.General;

namespace TrackControl.Vehicles
{
    /// <summary>
    /// ����� ������ ������������ �������.
    /// </summary>
    public class VehiclesModel
    {
        private VehiclesGroup _root;
        private List<Driver> _drivers;
        private List<PcbVersions> _pcbversions;

        public event Action<Driver> DriverDeleted;
        public event MethodInvoker RefreshTreeView;

        public VehiclesModel(VehiclesGroup root)
        {
            if (null == root)
                throw new ArgumentNullException("root");

            _root = root;
            _drivers = new List<Driver>();
            _pcbversions = new List<PcbVersions>();
        }

        public VehiclesGroup Root
        {
            get { return _root; }
        }

        public IList<Vehicle> Vehicles
        {
            get { return _root.AllItems; }
        }

        public IList<Driver> Drivers
        {
            get { return _drivers.AsReadOnly(); }
        }

        public void AddDriver(Driver driver)
        {
            _drivers.Add(driver);
        }

        public void AddPcbVersion( PcbVersions pcbVer )
        {
            _pcbversions.Add( pcbVer );
        }

        public void ClearPcbVersion()
        {
           _pcbversions.Clear();
        }

        public void DeleteDriver(Driver driver)
        {
            if (null == driver)
                throw new ArgumentNullException("driver");

            onDriverDeleted(driver);
            _drivers.Remove(driver);
        }

        private void onDriverDeleted(Driver driver)
        {
            Action<Driver> handler = DriverDeleted;
            if (null != handler)
                handler(driver);
        }

        public void RefreshLastTimeGps()
        {
            DataTable dt = MobitelProvider.GetAllLastTimeGps();

            if (dt == null) 
                return;

            foreach (Vehicle veh in Vehicles)
            {
                DataRow[] dr = dt.Select(string.Format("Mobitel_ID = {0}", veh.Mobitel.Id));
                
                if (dr != null)
                {
                    veh.Mobitel.LastTimeGps = (DateTime)dr[0]["LastTime"];
                }
            }

            RefreshTreeView();
        }

        public IList<VehiclesGroup> RootGroupsWithItems()
        {
            IList<Group<Vehicle>> rootGroups = Root.AllGroupsWithItems;
            IList<VehiclesGroup> groups = new List<VehiclesGroup>();

            if (_root.AllItems.Count > 0)
            {
                groups.Add(_root);
            }

            foreach (VehiclesGroup vg in rootGroups)
            {
                groups.Add(vg);
            }

            return groups;
        }

        public Vehicle GetVehicleById(int vehicleId)
        {
            foreach (Vehicle vh in _root.AllItems)
            {
                if (vehicleId == vh.Id)
                    return vh;
            }
            return null;
        }

        public IList<PcbVersions> PcbVersion
        {
            get { return _pcbversions.AsReadOnly(); }
        }
    }
}
