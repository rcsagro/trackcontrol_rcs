using System;
using DevExpress.XtraEditors.Controls;
using TrackControl.General;
using TrackControl.General.DatabaseDriver;
using TrackControl.Vehicles.Properties;
using MySql.Data.MySqlClient;
using System.Collections.Generic;

namespace TrackControl.Vehicles
{
    /// <summary>
    /// ��������, ���������� ����������� ������ ������������ �������
    /// </summary>
    public class VehiclesGroup : Group<Vehicle>
    {
        /// <summary>
        /// ��������������� � ������� ����� ��� �����������
        /// </summary>
        private GroupStyle _style;

        private bool _isRoot;
        private double _fuelWayKmGrp;
        private double _fuelMotoHrGrp;

        public VehiclesGroup(int teamId)
        {
            GetDataTeam(teamId);
        }

        public void GetDataTeam(int teamId)
        {
            //using (ConnectMySQL cn = new ConnectMySQL())
            DriverDb db = new DriverDb();
            db.ConnectDb();
            {
                string SQL = @"SELECT team.* FROM  team WHERE team.id = @ID";
                //MySqlParameter[] pars = new MySqlParameter[1];
                db.NewSqlParameterArray(1);
                //pars[0] = new MySqlParameter("@ID", MySqlDbType.Int32);
                db.NewSqlParameter("@ID", db.GettingInt32(), 0);
                //pars[0].Value = teamId;
                db.SetSqlParameterValue(teamId, 0);
                //MySqlDataReader dread = cn.GetDataReader(SQL, pars);
                db.GetDataReader(SQL, db.GetSqlParameterArray);
                //if (dread.Read())
                if (db.Read())
                {
                    // Id = dread.GetInt32("id");
                    Id = db.GetInt32("id");
                    //Name = dread.IsDBNull(dread.GetOrdinal("Name")) ? "" : dread.GetString("Name");
                    Name = db.IsDbNull(db.GetOrdinal("Name")) ? "" : db.GetString("Name");
                    //Description = dread.IsDBNull(dread.GetOrdinal("Descr")) ? "" : dread.GetString("Descr");
                    Description = db.IsDbNull(db.GetOrdinal("Descr")) ? "" : db.GetString("Descr");
                    //_IdOutLink = dread.IsDBNull(dread.GetOrdinal("OutLinkId")) ? "" : dread.GetString("OutLinkId");
                    _IdOutLink = db.IsDbNull(db.GetOrdinal("OutLinkId")) ? "" : db.GetString("OutLinkId");
                    _fuelWayKmGrp = db.IsDbNull(db.GetOrdinal("FuelWayKmGrp")) ? 0.0 : db.GetDouble("FuelWayKmGrp");
                    _fuelMotoHrGrp = db.IsDbNull(db.GetOrdinal("FuelMotoHrGrp")) ? 0.0 : db.GetDouble("FuelMotoHrGrp");
                }
            }
            db.CloseDbConnection();
        }

        /// <summary>
        /// ����������� ��� TrackControl.Vehicles.VehiclesGroup
        /// </summary>
        /// <param name="name">�������� ������</param>
        /// <param name="description">�������� ������</param>
        /// <param name="style">����� ��� �����������</param>
        public VehiclesGroup(string name, string description, GroupStyle style, string IdOutLink)
        {
            Name = name;
            Description = description;
            Style = style;
            _IdOutLink = IdOutLink;
        }

        /// <summary>
        /// ����������� ��� TrackControl.Vehicles.VehiclesGroup
        /// </summary>
        /// <param name="name">�������� ������</param>
        /// <param name="description">�������� ������</param>
        /// <param name="style">����� ��� �����������</param>
        public VehiclesGroup(string name, string description, GroupStyle style, string IdOutLink,
                             double fuelkmway, double fuelmotorhr)
        {
            Name = name;
            Description = description;
            Style = style;
            _IdOutLink = IdOutLink;
            _fuelWayKmGrp = fuelkmway;
            _fuelMotoHrGrp = fuelmotorhr;
        }
        /// <summary>
        /// ����������� ��� �������� �� ������� �������, ��� ����������� �������������
        /// �����������
        /// </summary>
        public VehiclesGroup(string name, string description)
            : this(name, description, GroupStyle.Empty, "", 0, 0)
        {
            int ident = Id;
        }

        public GroupStyle Style
        {
            get { return _style; }
            set
            {
                if (null == value)
                    throw new Exception("Rendering Style can not be NULL");

                _style = value;
            }
        }

        public bool IsRoot
        {
            get { return _isRoot; }
        }

        public string FuelMotoHrGrp
        {
            get
            {
                if (_fuelMotoHrGrp == 0.0)
                    return "0";
                else
                    return Convert.ToString(_fuelMotoHrGrp);
            }
            set
            {
                if (value == "")
                    _fuelMotoHrGrp = 0.0;
                else
                    _fuelMotoHrGrp = Convert.ToDouble(value);
            }
        }

        public string FuelWayKmGrp
        {
            get
            {
                if (_fuelWayKmGrp == 0.0)
                    return "0";
                else
                    return Convert.ToString(Math.Round(_fuelWayKmGrp, 1));
            }
            set
            {
                if (value == "")
                    _fuelWayKmGrp = 0.0;
                else
                    _fuelWayKmGrp = Convert.ToDouble(value);
            }
        }

        public static VehiclesGroup CreateRoot()
        {
            VehiclesGroup root = new VehiclesGroup(Resources.AllVehicles, "");
            root._isRoot = true;
            return root;
        }

        public List<Vehicle> GetVehicleGroup()
        {
            List<Vehicle> ownItem = GetItem();

            List<Vehicle> items = new List<Vehicle>();

            for (int i = 0; i < ownItem.Count; i++)
            {
                if (ownItem[i].Group == this)
                {
                    items.Add(ownItem[i]);
                }
            }

            return items;
        }

        public void AddVehicle(Vehicle vehicle)
        {
            if (null != vehicle.Group)
                vehicle.Group.RemoveItem(vehicle);

            AddItem(vehicle);
            vehicle.Group = this;
        }

        public void RemoveVehicle(Vehicle vehicle)
        {
            RemoveItem(vehicle);
        }

        public override string ToString()
        {
            return Name;
        }

        public List<Vehicle> OwnRealItems()
        {
            List<Vehicle> ownRealItems = new List<Vehicle>();
            foreach (Vehicle vh in OwnItems)
            {
                if (!vh.IsNew)
                {
                    if (!ownRealItems.Contains(vh))
                        ownRealItems.Add(vh);
                }
            }

            ownRealItems.Sort();
            return ownRealItems;
        }

        public List<Vehicle> OwnRealItemsWithCategory(int idCategory)
        {
            List<Vehicle> ownRealItems = new List<Vehicle>();
            foreach (Vehicle vh in OwnItems)
            {
                if (!vh.IsNew && vh.Category != null && vh.Category.Id == idCategory)
                {
                    if (!ownRealItems.Contains(vh))
                        ownRealItems.Add(vh);
                }
            }
            ownRealItems.Sort();
            return ownRealItems;
        }
    }
}
