using System;
using System.Collections;
using TrackControl.Vehicles.Properties;  
using DevExpress.XtraEditors;


namespace TrackControl.Vehicles
{
    /// <summary>
    /// ���������� ������
    /// </summary>
    public class LogicSensor : ExternalDevice
    {
        private const int BitsLength = 1;

        private string _zeroState;
        private string _oneState;
        /// <summary>
        /// ����� �������� ������� �� ��������� � ���������� ��������� ��� ���������� ������� ������� ��� ����������
        /// </summary>
        public TimeSpan BounceTimeSensorNoActive;

        private bool K { get; set; }

        public LogicSensor()
        {

        }

        public LogicSensor(int idSensor)
        {
            if (idSensor > 0)
            {
                Id = idSensor;
                Sensor sensor = SensorProvider.GetSensor(idSensor);
                if (sensor != null) SetLogicSensorValues(sensor);
            }
        }

        public LogicSensor(Sensor sensor)
        {
            if (sensor != null)
            {
                SetLogicSensorValues(sensor);
            }
        }

        private void SetLogicSensorValues(Sensor ls)
        {
            this.Id = ls.Id;
            this.Name = ls.Name;
            this.Description = ls.Description;
            SetK(ls.K);
            AssignBits(ls.StartBit, BitsLength);
        }

        public void SetK(double k)
        {
            K = k == 1;
        }

        public void SetK(bool k)
        {
            K = k;
        }

        public LogicSensor(int idAlgoritm, int idMobitel)
        {
            Sensor sensor = SensorProvider.GetSensorWithAlgoritm(idAlgoritm, idMobitel);
            if (sensor != null) SetLogicSensorValues(sensor);
        }

        public LogicSensor(string name, string description, int startBit)
            : base(name, description, startBit, BitsLength)
        {
            _zeroState = String.Empty;
            _oneState = String.Empty;
        }

        public string ZeroState
        {
            get { return _zeroState; }
            set { _zeroState = value; }
        }

        public string OneState
        {
            get { return _oneState; }
            set { _oneState = value; }
        }

        public bool GetValue(byte[] sensorDataGps)
        {
            if (!Valid)
            {
                XtraMessageBox.Show(Resources.WrongSensorSetup, Name);
                return false;
            }
            //throw new Exception(String.Format("Sensor has wrong settings. Start Bit must be in the range 0..63 (Current value: {0})", StartBit));

            BitArray bits = null;
            if (sensorDataGps != null)
            {
                bits = new BitArray(sensorDataGps);
                return bits[(int) StartBit];
            }

            return false;
        }

        public override bool IsActive(byte[] sensorDataGps, bool isAngle = false)
        {
            bool value = GetValue(sensorDataGps);

            if (value == K)
                return true;
            else
                return false;
        }

        public bool IsActiveInTime(DateTime date)
        {
            if (date.Hour == 11 && (date.Minute > 45 & date.Minute < 50))
                return true;
            else if (date.Hour == 13 && (date.Minute > 12 & date.Minute < 20))
                return true;
            else if (date.Hour == 14 && (date.Minute > 45 & date.Minute < 50))
                return true;
            else if (date.Hour == 23 && (date.Minute > 17 & date.Minute < 30))
                return true;
            else
            {
                return false;
            }

        }

        public string GetState(bool key)
        {
            return key ? _oneState : _zeroState;
        }

    }
}
