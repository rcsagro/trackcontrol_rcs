using System;
using TrackControl.General;

namespace TrackControl.Vehicles
{
    /// <summary>
    /// ��������, �������������� ���������� ���������
    /// </summary>
    public class Mobitel : Entity
    {
        /// <summary>
        /// ����� ���������
        /// </summary>
        public string Login
        {
            get { return _login; }
        }

        string _login;

        /// <summary>
        /// ���������� ����� SIM-�����
        /// </summary>
        public string SimNumber
        {
            get { return _simNumber; }
            set { _simNumber = value; }
        }

        string _simNumber;


        /// <summary>
        /// ����� ��������� ������ �� ���������
        /// </summary>
        public DateTime LastTimeGps
        {
            get { return _lastTimeGps; }
            set { _lastTimeGps = value; }
        }

        DateTime _lastTimeGps;

        /// <summary>
        /// ���������� �� ��������� �������� 64 ������� ��������
        /// </summary>
        public bool Is64BitPackets { get; set; }

        /// <summary>
        /// ���������� ��� ��� ������ � ������� Dgps ������ �����������
        /// </summary>
        public bool IsNotDrawDgps { get; set; }

        /// <summary>
        /// ����������� ��� TrackControl.Vehicles.Mobitel
        /// </summary>
        /// <param name="id">Id ��������� � ��</param>
        /// <param name="login">����� ��������� ��� ����������� � �������</param>
        /// <param name="simNumber">���. ����� SIM-�����</param>
        public Mobitel(int id, string login, string simNumber)
        {
            Id = id;
            _login = login;
            _simNumber = simNumber;
        }

        public Mobitel(int id, string login, string simNumber, DateTime lastTimeGps)
        {
            Id = id;
            _login = login;
            _simNumber = simNumber;
            _lastTimeGps = lastTimeGps;
        }
    }
}
