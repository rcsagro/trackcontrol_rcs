using System;
using System.Collections.Generic;

namespace TrackControl.Vehicles
{
  public interface IVehiclesGroupProvider
  {
    IList<VehiclesGroup> GetAll();
    bool Save(VehiclesGroup group);
    bool Delete(VehiclesGroup group);
  }
}
