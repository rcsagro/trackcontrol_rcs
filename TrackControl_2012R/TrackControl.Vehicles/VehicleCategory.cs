﻿using System;
using System.Collections.Generic;
using System.Text;
using TrackControl.General;

namespace TrackControl.Vehicles
{
    public class VehicleCategory:Entity
    {
        public VehicleCategory()
        {

        }

        public VehicleCategory(int id, string name, double tonnage, double fuelNormMoving, double fuelNormParking)
        {
            Id = id;
            Name = name;
            Tonnage = tonnage;
            FuelNormMoving = fuelNormMoving;
            FuelNormParking = fuelNormParking;
        }

        public VehicleCategory(int id)
        {
            VehicleCategory vc = VehicleCategoryProvader.GetCategory(id);
            if (vc != null)
            {
                Id = vc.Id;
                Name = vc.Name;
                Tonnage = vc.Tonnage;
                FuelNormMoving = vc.FuelNormMoving;
                FuelNormParking = vc.FuelNormParking;
            }
        }

        public string Name { get; set; }

        /// <summary>
        /// Грузоподъемность
        /// </summary>
        public double Tonnage { get; set; }

        /// <summary>
        /// норма расхода топлива в движении л/100 км
        /// </summary>
        public double FuelNormMoving { get; set; }

        /// <summary>
        /// норма расхода топлива на холостых оборотах л/час
        /// </summary>
        public double FuelNormParking { get; set; }

        public bool Save()
        {
            if (IsNew)
            {
                Id = VehicleCategoryProvader.Insert(this);
                return (Id != ConstsGen.RECORD_MISSING);
            }
            else
            {
                return VehicleCategoryProvader.Update(this);
            }
        }

        public bool Delete()
        {
            return VehicleCategoryProvader.Delete(this);
        }

        public override string ToString()
        {
            return String.Format("{0}", Name);
        }
    }
}
