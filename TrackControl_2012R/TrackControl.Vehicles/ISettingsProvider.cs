﻿using System;
namespace TrackControl.Vehicles
{
    public interface ISettingsProvider
    {
        void GetOne(VehicleSettings Settings);
    }
}
