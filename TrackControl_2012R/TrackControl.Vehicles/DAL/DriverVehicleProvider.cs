﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Text;
using TrackControl.General;
using MySql.Data.MySqlClient;
using TrackControl.General.DAL;
using TrackControl.General.DatabaseDriver;

namespace TrackControl.Vehicles
{
    public static class DriverVehicleProvider
    {
        public static Driver GetDriver(int idDriver)
        {
            Driver driver = null;
            //using (ConnectMySQL cn = new ConnectMySQL())
            DriverDb db = new DriverDb();
            db.ConnectDb();
            {
                string sSQL = string.Format("SELECT   driver.* FROM   driver WHERE   driver.id = {0}", idDriver);
                //MySqlDataReader dr = cn.GetDataReader(sSQL);
                db.GetDataReader(sSQL);

                //if (dr.Read())
                if (db.Read())
                {
                    //driver = new Driver(dr.IsDBNull(dr.GetOrdinal("Name")) ? "" : dr.GetString("Name"),
                    //    dr.IsDBNull(dr.GetOrdinal("Family")) ? "" : dr.GetString("Family"));
                    driver = new Driver(db.IsDbNull(db.GetOrdinal("Name")) ? "" : db.GetString("Name"),
                        db.IsDbNull(db.GetOrdinal("Family")) ? "" : db.GetString("Family"));
                    driver.Id = idDriver;

                    //if (!dr.IsDBNull(dr.GetOrdinal("ByrdDay"))) driver.DayOfBirth  = dr.GetDateTime("ByrdDay");
                    if (!db.IsDbNull(db.GetOrdinal("ByrdDay")))
                        driver.DayOfBirth = db.GetDateTime("ByrdDay");

                    //driver.Categories = dr.IsDBNull(dr.GetOrdinal("Category")) ? "" : dr.GetString("Category");
                    driver.Categories = db.IsDbNull(db.GetOrdinal("Category")) ? "" : db.GetString("Category");

                    //if (!dr.IsDBNull(dr.GetOrdinal("Identifier"))) driver.Identifier = dr.GetUInt16("Identifier");
                    if (!db.IsDbNull(db.GetOrdinal("Identifier")))
                        driver.Identifier = db.GetUInt16("Identifier");

                    //if (!dr.IsDBNull(dr.GetOrdinal("Permis"))) driver.License = dr.GetString("Permis");
                    if (!db.IsDbNull(db.GetOrdinal("Permis")))
                        driver.License = db.GetString("Permis");

                    //driver.IdOutLink = dr.IsDBNull(dr.GetOrdinal("OutLinkId")) ? "" : dr.GetString("OutLinkId");
                    driver.IdOutLink = db.IsDbNull(db.GetOrdinal("OutLinkId")) ? "" : db.GetString("OutLinkId");

                    driver.NumTelephone =
                        db.IsDbNull(db.GetOrdinal("numTelephone")) ? "" : db.GetString("numTelephone");
                    driver.Department = db.IsDbNull(db.GetOrdinal("Department")) ? "" : db.GetString("Department");
                    //int idType = dr.IsDBNull(dr.GetOrdinal("idType")) ? 0 : dr.GetInt32("idType");
                    int idType = db.IsDbNull(db.GetOrdinal("idType")) ? 0 : db.GetInt32("idType");

                    if (idType > 0)
                        driver.TypeDriver = GetDriverType(idType);
                }

                //dr.Close();
                db.CloseDataReader();
            }
            db.CloseDbConnection();
            return driver;
        }

        public static Driver GetDriverFromIdentifier(UInt16? identifier)
        {
            Driver driver = null;
            //using (ConnectMySQL cn = new ConnectMySQL())
            DriverDb db = new DriverDb();
            db.ConnectDb();
            {
                string sql = string.Format("SELECT   driver.Id FROM   driver WHERE   driver.Identifier = {0}",
                    identifier);
                //MySqlDataReader dr = cn.GetDataReader(sql);
                db.GetDataReader(sql);
                //if (dr.Read())
                if (db.Read())
                {
                    //driver = GetDriver(dr.GetInt32("Id"));
                    driver = GetDriver(db.GetInt32("Id"));
                }

                //dr.Close();
                db.CloseDataReader();
            }
            db.CloseDbConnection();
            return driver;
        }

        public static Driver GetDriverFromOutLinkId(string idOutLink)
        {
            Driver driver = null;
            var db = new DriverDb();
            db.ConnectDb();
            {
                string sql = string.Format(TrackControlQuery.DriverProvider.SelectIdByOutLinkId, idOutLink);
                db.GetDataReader(sql);
                if (db.Read())
                {
                    driver = GetDriver(db.GetInt32("Id"));
                }

                db.CloseDataReader();
            }
            db.CloseDbConnection();
            return driver;
        }

        public static DataTable GetList()
        {
            //using (ConnectMySQL cnn = new ConnectMySQL())
            using (var db = new DriverDb())
            {
                db.ConnectDb();
                string sql = String.Format(" SELECT   driver.id, {0} as Family  FROM   driver ORDER BY   driver.Family",
                    TrackControlQuery.SqlDriverIdent);
                //return cnn.GetDataTable(sql);
                return db.GetDataTable(sql);
            }
        }

        public static DriverType GetDriverType(int idType)
        {
            DriverType type = null;
            //using (ConnectMySQL cn = new ConnectMySQL())
            DriverDb db = new DriverDb();
            db.ConnectDb();
            {
                string sql = string.Format("SELECT   driver_types.* FROM   driver_types WHERE   driver_types.id = {0}",
                    idType);
                //MySqlDataReader dr = cn.GetDataReader(sql);
                db.GetDataReader(sql);
                //if (dr.Read())
                if (db.Read())
                {
                    //type = new DriverType(idType, dr.IsDBNull(dr.GetOrdinal("TypeName")) ? "" : dr.GetString("TypeName"));
                    type = new DriverType(idType,
                        db.IsDbNull(db.GetOrdinal("TypeName")) ? "" : db.GetString("TypeName"));
                }

                //dr.Close();
                db.CloseDataReader();
            }
            db.CloseDbConnection();
            return type;
        }

        public static BindingList<DriverType> GetListDriverTypes()
        {
            BindingList<DriverType> driverTypes = new BindingList<DriverType>();
            string sql = "SELECT   driver_types.* FROM   driver_types Order By TypeName";
            DriverType dt = null;

            //using (ConnectMySQL cn = new ConnectMySQL())
            DriverDb db = new DriverDb();
            db.ConnectDb();
            {
                //MySqlDataReader dr = cn.GetDataReader(sql);
                db.GetDataReader(sql);
                //while (dr.Read())
                while (db.Read())
                {
                    //dt = new DriverType(dr.GetInt32("Id"), dr.GetString("TypeName"));
                    dt = new DriverType(db.GetInt32("Id"), db.GetString("TypeName"));
                    driverTypes.Add(dt);
                }
            }
            db.CloseDbConnection();
            return driverTypes;
        }

        public static int InsertDriverType(DriverType dt)
        {
            try
            {
                //using (ConnectMySQL cn = new ConnectMySQL())
                using (DriverDb db = new DriverDb())
                {
                    db.ConnectDb();

                    string sql = "Insert into driver_types (TypeName) values (" + db.ParamPrefics + "TypeName)";

                    //MySqlParameter[] parSql = new MySqlParameter[1];
                    db.NewSqlParameterArray(1);
                    //CreatedDriverTypeParameters(dt, parSql);
                    CreatedDriverTypeParameters(dt, db);

                    //return cn.ExecuteReturnLastInsert(sql, parSql);
                    return db.ExecuteReturnLastInsert(sql, db.GetSqlParameterArray, "driver_types");
                }
            }
            catch
            {
                return ConstsGen.RECORD_MISSING;
            }
        }

        public static bool UpdateDriverType(DriverType dt)
        {
            DriverDb db = new DriverDb();
            try
            {
                //using (ConnectMySQL cn = new ConnectMySQL())
                db.ConnectDb();
                {
                    string sql = string.Format("UPDATE  driver_types Set TypeName = " + db.ParamPrefics + "TypeName" +
                                               "WHERE Id = {0}", dt.Id);

                    //MySqlParameter[] parSql = new MySqlParameter[1];
                    db.NewSqlParameterArray(1);
                    //CreatedDriverTypeParameters(dt, parSql);
                    CreatedDriverTypeParameters(dt, db);
                    //cn.ExecuteNonQueryCommand(sql, parSql);
                    db.ExecuteNonQueryCommand(sql, db.GetSqlParameterArray);
                    // return true;
                }
                db.CloseDbConnection();
                return true;
            }
            catch
            {
                db.CloseDbConnection();
                return false;
            }
        }

        public static bool DeleteDriverType(DriverType dt)
        {
            DriverDb db = new DriverDb();
            try
            {
                //using (ConnectMySQL cn = new ConnectMySQL())

                db.ConnectDb();
                {
                    string sql = string.Format(@"DELETE  FROM driver_types WHERE Id = {0}", dt.Id);
                    //cn.ExecuteNonQueryCommand(sql);
                    db.ExecuteNonQueryCommand(sql);
                    //return true;
                }
                db.CloseDbConnection();
                return true;
            }
            catch
            {
                db.CloseDbConnection();
                return false;
            }
        }

        //private static void CreatedDriverTypeParameters(DriverType dt, MySqlParameter[] parSql)
        //{
        //    parSql[0] = new MySqlParameter("?TypeName", dt.TypeName);
        //}

        private static void CreatedDriverTypeParameters(DriverType dt, DriverDb dbs)
        {
            //parSql[0] = new MySqlParameter( "?TypeName", dt.TypeName );
            dbs.NewSqlParameter(dbs.ParamPrefics + "TypeName", dt.TypeName, 0);
        }

        public static int CountDriversDriverType(DriverType dt)
        {
            //using (ConnectMySQL cn = new ConnectMySQL())
            using (DriverDb db = new DriverDb())
            {
                db.ConnectDb();

                string sql = string.Format(@"SELECT count(driver.Id) AS CNT
                 FROM   driver WHERE   driver.idType = {0}
                GROUP BY   driver.idType", dt.Id);
                //return cn.GetScalarValueNull<int>(sql, 0);
                return db.GetScalarValueNull<int>(sql, 0);
            }
        }
    }
}
