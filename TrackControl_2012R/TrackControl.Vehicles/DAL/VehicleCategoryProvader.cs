﻿using System.ComponentModel;
using System.Collections.Generic;
using TrackControl.General;
using TrackControl.General.DatabaseDriver;
using TrackControl.General.DAL;
using System;
using DevExpress.XtraEditors;

namespace TrackControl.Vehicles
{
    public static class VehicleCategoryProvader
    {

        #region CATEGORY

        public static BindingList<VehicleCategory> GetList ()
        {
            BindingList<VehicleCategory> vCats = new BindingList<VehicleCategory> ();
            VehicleCategory vCat = null;
            using (DriverDb db = new DriverDb()) {
                db.ConnectDb ();
                db.GetDataReader (TrackControlQuery.VehicleCategoryProvider.SelectAll);

                while (db.Read ()) {
                    // исправил aketner - 22.07.2013
                    vCat = GetRecord (vCat, db);
                    vCats.Add (vCat);
                }
            }
            return vCats;
        }

        private static VehicleCategory GetRecord (VehicleCategory vCat, DriverDb db)
        {
            vCat = new VehicleCategory (db.GetInt32 ("Id"), db.GetString ("Name"), (double)db.GetDecimal ("Tonnage"),
                                        db.GetDouble ("FuelNormMoving"), db.GetDouble ("FuelNormParking"));
            return vCat;
        }

        public static VehicleCategory GetCategory (int idCategory)
        {
            VehicleCategory vCat = null;
            using (DriverDb db = new DriverDb()) {
                db.ConnectDb ();
                db.GetDataReader (string.Format (TrackControlQuery.VehicleCategoryProvider.SelectOne, idCategory));

                if (db.Read ()) {
                    vCat = GetRecord (vCat, db);
                }
            }
            return vCat;
        }

        public static bool Update (VehicleCategory vc)
        {
            try {
                using (DriverDb db = new DriverDb()) {
                    db.ConnectDb ();

                    db.NewSqlParameterArray (5);
                   
                    db.SetNewSqlParameter (db.ParamPrefics + "Id", vc.Id);
                    db.SetNewSqlParameter (db.ParamPrefics + "Name", vc.Name);
                    db.SetNewSqlParameter (db.ParamPrefics + "Tonnage", vc.Tonnage);
                    db.SetNewSqlParameter (db.ParamPrefics + "FuelNormMoving", vc.FuelNormMoving);
                    db.SetNewSqlParameter (db.ParamPrefics + "FuelNormParking", vc.FuelNormParking);
                    db.ExecuteNonQueryCommand (TrackControlQuery.VehicleCategoryProvider.UpdateOne, db.GetSqlParameterArray);
                 
                    return true;
                }
            } catch {
                return false;
            }
        }

        public static int Insert (VehicleCategory vc)
        {
            try {
                using (DriverDb db = new DriverDb()) {
                    db.ConnectDb ();
                    db.NewSqlParameterArray (4);
               
                    db.SetNewSqlParameter (db.ParamPrefics + "Name", vc.Name);
                    db.SetNewSqlParameter (db.ParamPrefics + "Tonnage", vc.Tonnage);
                    db.SetNewSqlParameter (db.ParamPrefics + "FuelNormMoving", vc.FuelNormMoving);
                    db.SetNewSqlParameter (db.ParamPrefics + "FuelNormParking", vc.FuelNormParking);
                    return db.ExecuteReturnLastInsert (TrackControlQuery.VehicleCategoryProvider.InsertOne, db.GetSqlParameterArray, "vehicle_category");
                }
            } catch {

                return ConstsGen.RECORD_MISSING;
            }

            return ConstsGen.RECORD_MISSING;
        }

        public static bool Delete (VehicleCategory vc)
        {

            try {
                using (DriverDb db = new DriverDb()) {
                    db.ConnectDb ();
                    db.NewSqlParameterArray (1);
                  
                    db.SetNewSqlParameter (db.ParamPrefics + "Id", vc.Id);
                    db.ExecuteNonQueryCommand (TrackControlQuery.VehicleCategoryProvider.DeleteOne, db.GetSqlParameterArray);
                   
                    return true;
                }
            } catch {

                return false;
            }
        }

        public static int CountVehiclesWithCategory (VehicleCategory vc)
        {
            using (DriverDb db = new DriverDb()) 
            {
                db.ConnectDb ();
                db.NewSqlParameterArray (1);
               
                db.SetNewSqlParameter (db.ParamPrefics + "Id", vc.Id);
                return db.GetScalarValueNull<int> (TrackControlQuery.VehicleCategoryProvider.CountVehiclesWithCategory, db.GetSqlParameterArray, 0);
            }
            return 0;
        }

        #endregion

        #region CATEGORY2

        public static BindingList<VehicleCategory2> GetList2 ()
        {
            BindingList<VehicleCategory2> vCats = new BindingList<VehicleCategory2> ();

            VehicleCategory2 vCat = null;

            using (DriverDb db = new DriverDb()) {
                db.ConnectDb ();
                db.GetDataReader (TrackControlQuery.VehicleCategoryProvider.SelectAll2);

                while (db.Read ()) {
                    // исправил aketner - 22.07.2013
                    vCat = GetRecord2 (vCat, db);
                    vCats.Add (vCat);
                }
            }

            return vCats;
        }

        private static VehicleCategory2 GetRecord2 (VehicleCategory2 vCat, DriverDb db)
        {
            vCat = new VehicleCategory2 (db.GetInt32 ("Id"), db.GetString ("Name"));
            return vCat;
        }

        private static VehicleCategory3 GetRecord3( VehicleCategory3 vCat, DriverDb db )
        {
            vCat = new VehicleCategory3( db.GetInt32( "Id" ), db.GetString( "Name" ) );
            return vCat;
        }

        private static VehicleCategory4 GetRecord4( VehicleCategory4 vCat, DriverDb db )
        {
            vCat = new VehicleCategory4( db.GetInt32( "Id" ), db.GetString( "Name" ) );
            return vCat;
        }

        public static VehicleCategory2 GetCategory2 (int idCategory)
        {
            VehicleCategory2 vCat = null;
            using (DriverDb db = new DriverDb()) {
                db.ConnectDb ();
               
                db.GetDataReader (string.Format (TrackControlQuery.VehicleCategoryProvider.SelectOne2, idCategory));
               
                if (db.Read ()) {
                    vCat = GetRecord2 (vCat, db);
                }
            }
            return vCat;
        }

        public static VehicleCategory3 GetCategory3( int idCategory )
        {
            VehicleCategory3 vCat = null;

            using( DriverDb db = new DriverDb() )
            {
                db.ConnectDb();

                db.GetDataReader( string.Format( TrackControlQuery.VehicleCategoryProvider.SelectOne3, idCategory ) );

                if( db.Read() )
                {
                    vCat = GetRecord3( vCat, db );
                }
            }

            return vCat;
        }

        public static VehicleCategory4 GetCategory4( int idCategory )
        {
            VehicleCategory4 vCat = null;

            using( DriverDb db = new DriverDb() )
            {
                db.ConnectDb();

                db.GetDataReader( string.Format( TrackControlQuery.VehicleCategoryProvider.SelectOne4, idCategory ) );

                if( db.Read() )
                {
                    vCat = GetRecord4( vCat, db );
                }
            }

            return vCat;
        }

        public static bool Update2 (VehicleCategory2 vc)
        {
            try {
                using (DriverDb db = new DriverDb()) {
                    db.ConnectDb ();

                    db.NewSqlParameterArray (2);
                   
                    db.SetNewSqlParameter (db.ParamPrefics + "Id", vc.Id);
                    db.SetNewSqlParameter (db.ParamPrefics + "Name", vc.Name);
                    db.ExecuteNonQueryCommand (TrackControlQuery.VehicleCategoryProvider.UpdateOne2, db.GetSqlParameterArray);
                  
                    return true;
                }
            } catch {
                return false;
            }
        }

        public static bool Update3( VehicleCategory3 vc )
        {
            try
            {
                using( DriverDb db = new DriverDb() )
                {
                    db.ConnectDb();

                    db.NewSqlParameterArray( 2 );

                    db.SetNewSqlParameter( db.ParamPrefics + "Id", vc.Id );
                    db.SetNewSqlParameter( db.ParamPrefics + "Name", vc.Name );
                    db.ExecuteNonQueryCommand( TrackControlQuery.VehicleCategoryProvider.UpdateOne3, db.GetSqlParameterArray );

                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        public static bool Update4( VehicleCategory4 vc )
        {
            try
            {
                using( DriverDb db = new DriverDb() )
                {
                    db.ConnectDb();
                    db.NewSqlParameterArray( 2 );
                    db.SetNewSqlParameter( db.ParamPrefics + "Id", vc.Id );
                    db.SetNewSqlParameter( db.ParamPrefics + "Name", vc.Name );
                    db.ExecuteNonQueryCommand( TrackControlQuery.VehicleCategoryProvider.UpdateOne4, db.GetSqlParameterArray );

                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        public static int Insert2 (VehicleCategory2 vc)
        {
            try {
                using (DriverDb db = new DriverDb()) {
                    db.ConnectDb ();
                    db.NewSqlParameterArray (1);
                   
                    db.SetNewSqlParameter (db.ParamPrefics + "Name", vc.Name);
                    return db.ExecuteReturnLastInsert (TrackControlQuery.VehicleCategoryProvider.InsertOne2, db.GetSqlParameterArray, "vehicle_category2");
                }          
            } catch {

                return ConstsGen.RECORD_MISSING;
            }

            return ConstsGen.RECORD_MISSING;
        }

        public static int Insert3( VehicleCategory3 vc )
        {
            try
            {
                using( DriverDb db = new DriverDb() )
                {
                    db.ConnectDb();
                    db.NewSqlParameterArray( 1 );

                    db.SetNewSqlParameter( db.ParamPrefics + "Name", vc.Name );
                    return db.ExecuteReturnLastInsert( TrackControlQuery.VehicleCategoryProvider.InsertOne3, db.GetSqlParameterArray, "vehicle_category3" );
                }
            }
            catch
            {

                return ConstsGen.RECORD_MISSING;
            }
        }

        public static int Insert4( VehicleCategory4 vc )
        {
            try
            {
                using( DriverDb db = new DriverDb() )
                {
                    db.ConnectDb();
                    db.NewSqlParameterArray( 1 );

                    db.SetNewSqlParameter( db.ParamPrefics + "Name", vc.Name );
                    return db.ExecuteReturnLastInsert( TrackControlQuery.VehicleCategoryProvider.InsertOne4, db.GetSqlParameterArray, "vehicle_category3" );
                }
            }
            catch
            {

                return ConstsGen.RECORD_MISSING;
            }
        }

        public static bool Delete2 (VehicleCategory2 vc)
        {

            try {
                using (DriverDb db = new DriverDb()) {
                    db.ConnectDb ();
                    db.NewSqlParameterArray (1);
                  
                    db.SetNewSqlParameter (db.ParamPrefics + "Id", vc.Id);
                    db.ExecuteNonQueryCommand (TrackControlQuery.VehicleCategoryProvider.DeleteOne2, db.GetSqlParameterArray);
                    return true;
                }
            } catch {

                return false;
            }
        }

        public static bool Delete3( VehicleCategory3 vc )
        {

            try
            {
                using( DriverDb db = new DriverDb() )
                {
                    db.ConnectDb();
                    db.NewSqlParameterArray( 1 );

                    db.SetNewSqlParameter( db.ParamPrefics + "Id", vc.Id );
                    db.ExecuteNonQueryCommand( TrackControlQuery.VehicleCategoryProvider.DeleteOne3, db.GetSqlParameterArray );
                    return true;
                }
            }
            catch
            {

                return false;
            }
        }

        public static bool Delete4( VehicleCategory4 vc )
        {

            try
            {
                using( DriverDb db = new DriverDb() )
                {
                    db.ConnectDb();
                    db.NewSqlParameterArray( 1 );

                    db.SetNewSqlParameter( db.ParamPrefics + "Id", vc.Id );
                    db.ExecuteNonQueryCommand( TrackControlQuery.VehicleCategoryProvider.DeleteOne4, db.GetSqlParameterArray );
                    return true;
                }
            }
            catch
            {

                return false;
            }
        }

        public static int CountVehiclesWithCategory2 (VehicleCategory2 vc)
        {
            try {
                using (DriverDb db = new DriverDb()) {
                    db.ConnectDb ();
                    db.NewSqlParameterArray (1);
               
                    db.SetNewSqlParameter (db.ParamPrefics + "Id", vc.Id);
                    return db.GetScalarValueNull<int> (TrackControlQuery.VehicleCategoryProvider.CountVehicleWithCategory2, db.GetSqlParameterArray, 0);
                }
            } catch (Exception ex) {
                XtraMessageBox.Show (ex.Message);
                return 0;
            }
        }
        
        #endregion

        #region CATEGORY3

        public static int CountVehiclesWithCategory3( VehicleCategory3 vc )
        {
            try
            {
                using( DriverDb db = new DriverDb() )
                {
                    db.ConnectDb();
                    db.NewSqlParameterArray( 1 );

                    db.SetNewSqlParameter( db.ParamPrefics + "Id", vc.Id );
                    return db.GetScalarValueNull<int>( TrackControlQuery.VehicleCategoryProvider.CountVehicleWithCategory3, db.GetSqlParameterArray, 0 );
                }
            }
            catch( Exception ex )
            {
                XtraMessageBox.Show( ex.Message );
                return 0;
            }
        }

        public static BindingList<VehicleCategory3> GetList3()
        {
            BindingList<VehicleCategory3> vCats = new BindingList<VehicleCategory3>();

            VehicleCategory3 vCat = null;

            using( DriverDb db = new DriverDb() )
            {
                db.ConnectDb();
                db.GetDataReader( TrackControlQuery.VehicleCategoryProvider.SelectAll3 );

                while( db.Read() )
                {
                    vCat = GetRecord3( vCat, db );
                    vCats.Add( vCat );
                }
            }

            return vCats;
        }

        #endregion

        #region CATEGORY4

        public static int CountVehiclesWithCategory4( VehicleCategory4 vc )
        {
            try
            {
                using( DriverDb db = new DriverDb() )
                {
                    db.ConnectDb();
                    db.NewSqlParameterArray( 1 );

                    db.SetNewSqlParameter( db.ParamPrefics + "Id", vc.Id );
                    return db.GetScalarValueNull<int>( TrackControlQuery.VehicleCategoryProvider.CountVehicleWithCategory4, db.GetSqlParameterArray, 0 );
                }
            }
            catch( Exception ex )
            {
                XtraMessageBox.Show( ex.Message );
                return 0;
            }
        }

        public static BindingList<VehicleCategory4> GetList4()
        {
            BindingList<VehicleCategory4> vCats = new BindingList<VehicleCategory4>();

            VehicleCategory4 vCat = null;

            using( DriverDb db = new DriverDb() )
            {
                db.ConnectDb();
                db.GetDataReader( TrackControlQuery.VehicleCategoryProvider.SelectAll4 );

                while( db.Read() )
                {
                    // исправил aketner - 08.10.2014
                    vCat = GetRecord4( vCat, db );
                    vCats.Add( vCat );
                }
            }
            return vCats;
        }
        #endregion
    }
}
