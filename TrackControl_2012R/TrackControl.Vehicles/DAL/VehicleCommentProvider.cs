﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using TrackControl.General;
using TrackControl.General.DAL;
using TrackControl.General.DatabaseDriver;

namespace TrackControl.Vehicles.DAL
{
    public class VehicleCommentProvider : Entity
    {
        public VehicleCommentProvider()
        {
            // to do this
        }

        public VehicleCommentProvider( int mobitelid, string comment )
        {
            MobitelId = mobitelid;
            VehComment = comment;
        }

        public VehicleCommentProvider(int mobitelid)
        {
            string cmnt = GetComment(mobitelid);

            {
                MobitelId = mobitelid;
                VehComment = cmnt;
            }
        }

        public string VehComment { get; set; }

        public bool Save()
        {
            if( IsNew )
            {
                int res = InsertComment(MobitelId, VehComment);

                return ( res != ConstsGen.RECORD_MISSING );
            }
            else
            {
                return UpdateComment( MobitelId, VehComment );
            }
        }

        public static int InsertComment( int mobitelid, string name )
        {
            try
            {
                using( DriverDb db = new DriverDb() )
                {
                    db.ConnectDb();
                    db.NewSqlParameterArray( 2 );

                    db.SetNewSqlParameter( db.ParamPrefics + "Comment", name );
                    db.SetNewSqlParameter( db.ParamPrefics + "Mobitel_id", mobitelid );
                    return db.ExecuteReturnLastInsert( TrackControlQuery.VehicleCommentProvider.InsertOne, db.GetSqlParameterArray, "vehicle_commentary" );
                }
            }
            catch
            {

                return ConstsGen.RECORD_MISSING;
            }
        }

        public static bool UpdateComment( int mobitelid, string name )
        {
            try
            {
                using( DriverDb db = new DriverDb() )
                {
                    db.ConnectDb();

                    db.NewSqlParameterArray( 2 );

                    db.SetNewSqlParameter( db.ParamPrefics + "Comment", name );
                    db.SetNewSqlParameter( db.ParamPrefics + "Mobitel_id", mobitelid );
                    db.ExecuteNonQueryCommand( TrackControlQuery.VehicleCommentProvider.UpdateComment, db.GetSqlParameterArray );

                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        public static string GetComment(int idMobitel)
        {
            string comment = "";
            string command = TrackControlQuery.VehicleCommentProvider.SelectComment + idMobitel;
            
            using( DriverDb db = new DriverDb() )
            {
                db.ConnectDb();
                db.GetDataReader( command );

                while( db.Read() )
                {
                    comment = GetRecord( db );
                }
            }

            return comment;
        }

        public static bool DeleteComment( int mobitelid )
        {
            try
            {
                using( DriverDb db = new DriverDb() )
                {
                    db.ConnectDb();
                    db.NewSqlParameterArray( 1 );
                    db.SetNewSqlParameter( db.ParamPrefics + "Mobitel_id", mobitelid );
                    db.ExecuteNonQueryCommand( TrackControlQuery.VehicleCommentProvider.DeleteComment, db.GetSqlParameterArray );
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        private static string GetRecord( DriverDb db )
        {
            return db.GetString( "Comment" );
        }
    }
}
