﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using TrackControl.General;
using TrackControl.General.DatabaseDriver;
using TrackControl.Vehicles.DAL;
using TrackControl.Vehicles.Properties;
using TrackControl.General.DAL;

namespace TrackControl.Vehicles
{
    public static class VehicleProvider2
    {
        public static Aggregat GetAggregateFromIdentifier(UInt16? identifier) // RFID
        {
            Aggregat aggregat = new Aggregat();

            if (identifier > 0)
            {

                DriverDb db = new DriverDb();
                db.ConnectDb();
                {
                    string SQLS = "SELECT aa.Id,aa.Name,aa.Identifier FROM agro_agregat aa WHERE aa.Identifier = " +
                                  identifier;

                    db.GetDataReader(SQLS);
                    if (db.Read())
                    {
                        aggregat.IDAggregate = db.GetInt32("Id");
                        aggregat.AggregateName = db.GetString("Name");
                        aggregat.Identifier = (UInt16) db.GetInt16("Identifier");
                    }
                    else
                    {
                        aggregat.IDAggregate = 0;
                        aggregat.AggregateName = "";
                        aggregat.Identifier = 0;
                    }

                    db.CloseDataReader();
                }
                db.CloseDbConnection();
            }
            else
            {
                aggregat.IDAggregate = 0;
                aggregat.AggregateName = "";
                aggregat.Identifier = 0;
            }

            return aggregat;
        }

        public static void GetAggregateFromMobitel(int mobitelId, Vehicle vh)
        {
            if (vh.Aggregate == null)
                vh.Aggregate = new Aggregat();

            DriverDb db = new DriverDb();
            db.ConnectDb();
            {
                string SQLS = "SELECT aa.Id,aa.Name,aa.Identifier FROM vehicle v, agro_agregat_vehicle aav, agro_agregat aa WHERE v.Mobitel_id = " + 
                             mobitelId + " AND aav.Id_vehicle = v.id AND aav.Id_agregat = aa.Id";
                db.GetDataReader(SQLS);
                if (db.Read())
                {
                    vh.Aggregate.IDAggregate = db.GetInt32("Id");
                    vh.Aggregate.AggregateName = db.GetString("Name");
                    vh.Aggregate.Identifier = (UInt16)db.GetInt32("Identifier");
                }
                else
                {
                    vh.Aggregate.IDAggregate = 0;
                    vh.Aggregate.AggregateName = "";
                    vh.Aggregate.Identifier = 0;
                }

                db.CloseDataReader();
            }
            db.CloseDbConnection();
        }

        public static Aggregat GetAggregateFromMobitel(int mobitelId)
        {
            Aggregat aggregate = new Aggregat();

            DriverDb db = new DriverDb();
            db.ConnectDb();
            {
                string SQLS = "SELECT aa.Id,aa.Name,aa.Identifier FROM vehicle v, agro_agregat_vehicle aav, agro_agregat aa WHERE v.Mobitel_id = " +
                              mobitelId + " AND aav.Id_vehicle = v.id AND aav.Id_agregat = aa.Id";
                db.GetDataReader(SQLS);
                if (db.Read())
                {
                    aggregate.IDAggregate = db.GetInt32("Id");
                    aggregate.AggregateName = db.GetString("Name");
                    aggregate.Identifier = (UInt16)db.GetInt32("Identifier");
                }
                else
                {
                    aggregate.IDAggregate = 0;
                    aggregate.AggregateName = "";
                    aggregate.Identifier = 0;
                }

                db.CloseDataReader();
            }
            db.CloseDbConnection();

            return aggregate;
        }

        public static void GetOne(int mobitelId, Vehicle vh)
        {
            //using (ConnectMySQL cn = new ConnectMySQL())
            DriverDb db = new DriverDb();
            db.ConnectDb();
            {
                string SQL = TrackControlQuery.VehicleProvider2.SelectVehicleFrom;

                //MySqlParameter[] pars = new MySqlParameter[1];
                db.NewSqlParameterArray(1);
                //pars[0] = new MySqlParameter("@ID", MySqlDbType.Int32);
                db.NewSqlParameter("@ID", db.GettingInt32(), 0);
                //pars[0].Value = mobitelId;
                db.SetSqlParameterValue(mobitelId, 0);
                //MySqlDataReader dread = cn.GetDataReader(SQL, pars);
                db.GetDataReader(SQL, db.GetSqlParameterArray);
                //if (dread.Read())
                if (db.Read())
                {
                    //Id = dread.GetInt32("id");
                    vh.Id = db.GetInt32("id");

                    if (db.GetInt32("Mobitel_id") > 0)
                    {
                        vh.Mobitel = GetMobitel(db.GetInt32("Mobitel_id"));
                        if (vh.Mobitel == null) vh.Mobitel = new Mobitel(mobitelId, "", "");
                    }
                    else
                        vh.Mobitel = new Mobitel(0, "", "");
                    //_regNumber = dread.IsDBNull(dread.GetOrdinal("NumberPlate")) ? "" : dread.GetString("NumberPlate");
                    vh.RegNumber = db.IsDbNull(db.GetOrdinal("NumberPlate")) ? "" : db.GetString("NumberPlate");
                    //_carMaker = dread.IsDBNull(dread.GetOrdinal("MakeCar")) ? "" : dread.GetString("MakeCar");
                    vh.CarMaker = db.IsDbNull(db.GetOrdinal("MakeCar")) ? "" : db.GetString("MakeCar");
                    //_carModel = dread.IsDBNull(dread.GetOrdinal("CarModel")) ? "" : dread.GetString("CarModel");
                    vh.CarModel = db.IsDbNull(db.GetOrdinal("CarModel")) ? "" : db.GetString("CarModel");
                    vh.IdOutLink = db.IsDbNull(db.GetOrdinal("OutLinkId")) ? "" : db.GetString("OutLinkId");

                    //if (!dread.IsDBNull(dread.GetOrdinal("driver_id"))) _driver = new Driver(dread.GetInt32("driver_id"),0);
                    if (vh.RfidDriver <= 0)
                    {
                        if (!db.IsDbNull(db.GetOrdinal("driver_id")))
                            vh.Driver = new Driver(db.GetInt32("driver_id"), 0);
                    }
                    else
                    {
                        vh.Driver = new Driver(db.GetInt32("driver_id"), vh.RfidDriver);
                    }

                    if (db.GetInt32("setting_id") > 0)
                        vh.Settings = new VehicleSettings(db.GetInt32("setting_id"));
                    else
                        vh.Settings = new VehicleSettings();
                    //vh.Settings.Id = db.GetInt32("setting_id");
                    //if (dread.IsDBNull(dread.GetOrdinal("Team_id")))
                    if (db.IsDbNull(db.GetOrdinal("Team_id")))
                        vh.Group = new VehiclesGroup(Resources.AllVehicles, "");
                    else
                    // _group = new VehiclesGroup(dread.GetInt32("Team_id"));
                        vh.Group = new VehiclesGroup(db.GetInt32("Team_id"));

                    if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                    {
                        vh.FuelWaysSet = db.IsDbNull(db.GetOrdinal("FuelWayLiter"))
                            ? 0.0
                            : Math.Round(db.GetFloat("FuelWayLiter"), 1);

                        vh.FuelMotorSet = db.IsDbNull(db.GetOrdinal("FuelMotorLiter"))
                            ? 0.0
                            : Math.Round(db.GetFloat("FuelMotorLiter"), 1);

                    }
                    else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                    {
                        vh.FuelWaysSet = db.IsDbNull(db.GetOrdinal("FuelWayLiter"))
                            ? 0.0
                            : Math.Round(db.GetDouble("FuelWayLiter"), 1);

                        vh.FuelMotorSet = db.IsDbNull(db.GetOrdinal("FuelMotorLiter"))
                            ? 0.0
                            : Math.Round(db.GetDouble("FuelMotorLiter"), 1);

                    }

                    vh.Identifier = db.IsDbNull(db.GetOrdinal("Identifier"))
                        ? 0
                        : db.GetInt32("Identifier");

                    if (!db.IsDbNull(db.GetOrdinal("Category_id")) && db.GetInt32("Category_id") > 0)
                        vh.Category = new VehicleCategory(db.GetInt32("Category_id"));

                    if (!db.IsDbNull(db.GetOrdinal("Category_id2")) && db.GetInt32("Category_id2") > 0)
                        vh.Category2 = new VehicleCategory2(db.GetInt32("Category_id2"));

                    if (!db.IsDbNull(db.GetOrdinal("Category_id3")) && db.GetInt32("Category_id3") > 0)
                        vh.Category3 = new VehicleCategory3(db.GetInt32("Category_id3"));

                    if (!db.IsDbNull(db.GetOrdinal("Category_id4")) && db.GetInt32("Category_id4") > 0)
                        vh.Category4 = new VehicleCategory4(db.GetInt32("Category_id4"));

                    vh.VehicleComment = VehicleCommentProvider.GetComment(vh.MobitelId);

                    if (!db.IsDbNull(db.GetOrdinal("Color")) && db.GetInt32("Color") > 0)
                        vh.Style = new VehicleStyle(Color.FromArgb(db.GetInt32("Color")));
                    else
                    {
                        vh.Style = new VehicleStyle();
                    }
                    //vh.PcbVersionId = db.GetInt32( "PcbVersionId" ); -- пока отставим
                }

                db.CloseDataReader();
            }

            string sqlstr = "SELECT m.InternalMobitelConfig_ID FROM mobitels m WHERE m.Mobitel_ID = " + vh.MobitelId;
            DataTable dtTable = db.GetDataTable(sqlstr);
            if (dtTable.Rows.Count > 0)
            {
                sqlstr = "SELECT i.devIdShort FROM internalmobitelconfig  i WHERE i.InternalMobitelConfig_ID = " +
                         Convert.ToInt32(dtTable.Rows[0]["InternalMobitelConfig_ID"]);
                dtTable = db.GetDataTable(sqlstr);
                if (dtTable.Rows.Count > 0)
                    vh.DevIdShort = Convert.ToString(dtTable.Rows[dtTable.Rows.Count - 1]["devIdShort"]);
                else
                    vh.DevIdShort = "NoData";
            }
            else
            {
                vh.DevIdShort = "NoData";
            }

            db.CloseDbConnection();
        }

        public static void GetSettings(int settingsId, VehicleSettings settings)
        {
            //using (ConnectMySQL cn = new ConnectMySQL())
            DriverDb db = new DriverDb();
            db.ConnectDb();
            {
                string SQL = string.Format(TrackControlQuery.VehicleProvider2.SelectSettingFrom, settingsId);

                db.GetDataReader(SQL);
                if (db.Read())
                {
                    settings.Id = settingsId;
                    if (!db.IsDbNull(db.GetOrdinal("TimeBreak")))
                        settings.TimeBreak = db.GetTimeSpan("TimeBreak");
                    if (!db.IsDbNull(db.GetOrdinal("TimeBreakMaxPermitted")))
                        settings.TimeBreakMaxPermitted = db.GetTimeSpan("TimeBreakMaxPermitted");
                    if (!db.IsDbNull(db.GetOrdinal("TimeBreakMaxPermitted")))
                        settings.TimeBreakMaxPermitted = db.GetTimeSpan("TimeBreakMaxPermitted");
                    if (!db.IsDbNull(db.GetOrdinal("speedMax")))
                        settings.SpeedMax = (float) db.GetDouble("speedMax");
                    if (!db.IsDbNull(db.GetOrdinal("AvgFuelRatePerHour")))
                        settings.AvgFuelRatePerHour = (float) db.GetDouble("AvgFuelRatePerHour");
                    if (!db.IsDbNull(db.GetOrdinal("FuelerMinFuelrate")))
                        settings.FuelerMinFuelRate = db.GetInt32("FuelerMinFuelrate");
                }
            }
            db.CloseDbConnection();
        }

        public static Mobitel GetMobitel(int MobitelId)
        {
            //using (ConnectMySQL cn = new ConnectMySQL())
            DriverDb db = new DriverDb();
            Mobitel mobitel = null;
            db.ConnectDb();
            {
                string SQL = string.Format(TrackControlQuery.VehicleProvider2.SelectMobitelsId, MobitelId);
                db.GetDataReader(SQL);
                if (db.Read())
                {
                    DateTime lastInsertTime = DateTime.MinValue;
                    string login = "";
                    if (!db.IsDbNull(db.GetOrdinal("LastTimeGps")))
                        lastInsertTime = db.GetDateTime("LastTimeGps");
                    if (!db.IsDbNull(db.GetOrdinal("devIdShort")))
                        login = db.GetString("devIdShort");
                    mobitel = new Mobitel(MobitelId, login, "", lastInsertTime);

                    if (!db.IsDbNull(db.GetOrdinal("Is64bitPackets")))
                        mobitel.Is64BitPackets = db.GetBoolean("Is64bitPackets");
                    else
                    {
                        mobitel.Is64BitPackets = false;
                    }
                    if (!db.IsDbNull(db.GetOrdinal("IsNotDrawDgps")))
                        mobitel.IsNotDrawDgps = db.GetBoolean("IsNotDrawDgps");
                    else
                    {
                        mobitel.IsNotDrawDgps = false;
                    }
                }
            }
            db.CloseDbConnection();
            return mobitel;
        }

        public static int GetMobitelIdByOutLinkId(string idOutLink)
        {
            var db = new DriverDb();
            int mobitelId = 0;
            db.ConnectDb();
            {
                string sql = string.Format(TrackControlQuery.VehicleProvider2.SelectMobitelIdByOutLinkId, idOutLink);
                db.GetDataReader(sql);
                if (db.Read())
                {
                    mobitelId = db.GetInt32("Mobitel_id");
                }
            }
            db.CloseDbConnection();
            return mobitelId;
        }
    }
}
