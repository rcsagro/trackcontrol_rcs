﻿using System;
using System.Collections.Generic;
using System.Data;
using TrackControl.General.DatabaseDriver;

namespace TrackControl.Vehicles
{
    public static class SensorProvider
    {
        public static Sensor GetSensor(int idSensor)
        {
            Sensor sensor = null;
            //using (ConnectMySQL cn = new ConnectMySQL())
            using (var db = new DriverDb())
            {
                db.ConnectDb();
                {
                    string sSql = string.Format("SELECT * FROM sensors WHERE id = {0}", idSensor);
                    //MySqlDataReader dr = cn.GetDataReader(sSQL);
                    db.GetDataReader(sSql);
                    //if (dr.Read())
                    if (db.Read())
                    {
                        string name;
                        string description;
                        int startBit;
                        int length;
                        double k;
                        double b;
                        bool s;
                        GetSensorFields(db, out name, out description, out startBit, out length, out k, out b, out s);
                        sensor = new Sensor(name, description, startBit, length, k, b, s)
                            {
                                Id = idSensor,
                                MobitelId = db.GetInt32("mobitel_id"),
                                Algoritm = GetSensorAlgoritm(idSensor)
                            };
                    }
                    //dr.Close();
                    db.CloseDataReader();
                }
                db.CloseDbConnection();
            }
            return sensor;
        }

        private static void GetSensorFields(DriverDb db, out string name, out string description, out int startBit,
            out int length, out double k, out double b, out bool s)
        {
            name = db.IsDbNull(db.GetOrdinal("Name")) ? "" : db.GetString("Name");
            description = db.IsDbNull(db.GetOrdinal("Description")) ? "" : db.GetString("Description");
            startBit = db.IsDbNull(db.GetOrdinal("StartBit")) ? 0 : Convert.ToInt32(db.GetValue(db.GetOrdinal("StartBit")));//db.GetInt32("StartBit")
            length = db.IsDbNull(db.GetOrdinal("Length")) ? 0 : Convert.ToInt32(db.GetValue(db.GetOrdinal("Length")));//db.GetInt32("Length")
            k = db.IsDbNull(db.GetOrdinal("K")) ? 0 : db.GetDouble("K");
            b = db.IsDbNull(db.GetOrdinal("B")) ? 0 : db.GetDouble("B");
            int ss = db.IsDbNull(db.GetOrdinal("S")) ? 0 : db.GetTinyInt("S");
            s = false;
            if (ss > 0) 
                s = true;
        }

        public static Sensor GetSensorWithAlgoritm(int idAlgoritm, int idMobitel)
        {
            Sensor sensor = null;
            //using (ConnectMySQL cn = new ConnectMySQL())
            using (var db = new DriverDb())
            {
                db.ConnectDb();
                {
                    string sSQL = string.Format(@"SELECT sensors.id
                            FROM  relationalgorithms
                            INNER JOIN sensors
                            ON relationalgorithms.SensorID = sensors.id
                            WHERE
                            relationalgorithms.AlgorithmID = {0}
                            AND sensors.mobitel_id = {1}", idAlgoritm, idMobitel);
                    //MySqlDataReader dr = cn.GetDataReader(sSQL);
                    db.GetDataReader(sSQL);
                    //if (dr.Read())
                    if (db.Read())
                    {
                        //sensor = GetSensor(dr.GetInt32("id")); 
                        sensor = GetSensor(db.GetInt32("id"));
                    }
                    //dr.Close();
                    db.CloseDataReader();
                }
                db.CloseDbConnection();
            }
            return sensor;
        }

        public static Sensor GetSensorWithAlgoritmExcludeSensor(int id_algoritm, int id_mobitel, int sensorExcludeId)
        {
            Sensor sensor = null;
            //using (ConnectMySQL cn = new ConnectMySQL())
            var db = new DriverDb();
            db.ConnectDb();
            {
                string sSql = string.Format(@"SELECT sensors.id
                            FROM  relationalgorithms
                            INNER JOIN sensors
                            ON relationalgorithms.SensorID = sensors.id
                            WHERE
                            relationalgorithms.AlgorithmID = {0}
                            AND sensors.mobitel_id = {1} AND sensors.id <> {2}", id_algoritm, id_mobitel,
                    sensorExcludeId);
                //MySqlDataReader dr = cn.GetDataReader(sSQL);
                db.GetDataReader(sSql);
                //if (dr.Read())
                if (db.Read())
                {
                    //sensor = GetSensor(dr.GetInt32("id")); 
                    sensor = GetSensor(db.GetInt32("id"));
                }
                //dr.Close();
                db.CloseDataReader();
            }
            db.CloseDbConnection();
            return sensor;
        }

        public static DataTable GetListLogics()
        {
            //using (ConnectMySQL cn = new ConnectMySQL())
            using (DriverDb db = new DriverDb())
            {
                db.ConnectDb();

                string sSQL = @"SELECT sensors.id , sensors.Name , vehicle.id as VID
                FROM  sensors
                INNER JOIN vehicle ON sensors.mobitel_id = vehicle.Mobitel_id 
                WHERE   sensors.Length = 1 
                ORDER BY sensors.Name";
                //return cn.GetDataTable(sSQL);
                return db.GetDataTable(sSQL);
            }
        }

        public static int GetAlgoritm(int id_sensor)
        {
            //using (ConnectMySQL cn = new ConnectMySQL())
            using (DriverDb db = new DriverDb())
            {
                db.ConnectDb();

                string sSQL =
                    string.Format(
                        "SELECT relationalgorithms.AlgorithmID FROM relationalgorithms WHERE relationalgorithms.SensorID = {0}",
                        id_sensor);
                //return cn.GetScalarValueNull<int> (sSQL,0); 
                return db.GetScalarValueNull<int>(sSQL, 0);
            }
        }

        public static List<Sensor> GetVehicleSensors(int mobitelId)
        {
            var sensors = new List<Sensor>();
            var db = new DriverDb();
            db.ConnectDb();
            {
                string sSQL = string.Format("SELECT * FROM sensors WHERE mobitel_id = {0}", mobitelId);
                //MySqlDataReader dr = cn.GetDataReader(sSQL);
                db.GetDataReader(sSQL);
                //if (dr.Read())
                while (db.Read())
                {
                    string name;
                    string description;
                    int startBit;
                    int length;
                    double k;
                    double b;
                    bool s;
                    GetSensorFields(db, out name, out description, out startBit, out length, out k, out b, out s);
                    Sensor sensor = new Sensor(name, description, startBit, length, k, b, s);
                    sensor.Id = db.GetInt32("id");
                    sensor.UnitName = db.IsDbNull(db.GetOrdinal("NameUnit")) ? "" : db.GetString("NameUnit");
                    sensors.Add(sensor);
                }
                //dr.Close();
                db.CloseDataReader();
            }
            db.CloseDbConnection();
            return sensors;
        }

        public static int GetSensorAlgoritm(int idSensor)
        {
            int idAlgoritm = 0;
            //using (ConnectMySQL cn = new ConnectMySQL())
            using (var db = new DriverDb())
            {
                db.ConnectDb();
                {
                    string sSql = string.Format(@"SELECT AlgorithmID
                            FROM  relationalgorithms
                            WHERE
                            relationalgorithms.SensorID = {0}", idSensor);
                    //MySqlDataReader dr = cn.GetDataReader(sSQL);
                    db.GetDataReader(sSql);
                    //if (dr.Read())
                    if (db.Read())
                    {
                        //sensor = GetSensor(dr.GetInt32("id")); 
                        idAlgoritm = db.GetInt32("AlgorithmID");
                    }
                    //dr.Close();
                    db.CloseDataReader();
                }
                db.CloseDbConnection();
            }
            return idAlgoritm;
        }

        public static SensorState GetSensorState(int idState)
        {
            SensorState sensorState = null;
            using (var db = new DriverDb())
            {
                db.ConnectDb();
                {
                    string sSql = string.Format("SELECT * FROM state WHERE id = {0}", idState);
                    db.GetDataReader(sSql);
                    if (db.Read())
                    {

                        sensorState = GetSensorStateFields(db);
                    }
                    db.CloseDataReader();
                }
                db.CloseDbConnection();
            }
            return sensorState;
        }

        public static SensorState GetSensorStateAgregat(int idSensor,int idAgregat)
        {
            SensorState sensorState = null;
            using (var db = new DriverDb())
            {
                db.ConnectDb();
                {
                    string sSql = string.Format(@"SELECT 
                    state.Id,
                    state.MinValue,
                    state.MaxValue,
                    state.Title
                    FROM agro_agregat_vehicle
                    INNER JOIN state
                    ON agro_agregat_vehicle.Id_state = state.Id
                    WHERE agro_agregat_vehicle.Id_sensor = {0} AND agro_agregat_vehicle.Id_agregat = {1}", idSensor, idAgregat);
                    db.GetDataReader(sSql);
                    if (db.Read())
                    {
                        sensorState = GetSensorStateFields(db);
                    }
                    db.CloseDataReader();
                }
                db.CloseDbConnection();
            }
            return sensorState;
        }

        private static SensorState GetSensorStateFields(DriverDb db)
        {
            int idState = db.IsDbNull(db.GetOrdinal("Id")) ? 0 : db.GetInt32("Id");
            string title = db.IsDbNull(db.GetOrdinal("Title")) ? "" : db.GetString("Title");
            double minValue = db.IsDbNull(db.GetOrdinal("MinValue")) ? 0 : db.GetDouble("MinValue");
            double maxValue = db.IsDbNull(db.GetOrdinal("MaxValue")) ? 0 : db.GetDouble("MaxValue");
            var  sensorState = new SensorState()
                {
                    Id = idState,
                    Title = title,
                    MinValue = minValue,
                    MaxValue = maxValue
                };
            return sensorState;
        }
    }
}
