﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using DevExpress.Internal;
using TrackControl.General;
using TrackControl.General.DatabaseDriver;
using TrackControl.General.DAL;

namespace TrackControl.Vehicles
{
    public static class MobitelProvider
    {
        public static DataTable GetAllLastTimeGps ()
        {
            using (DriverDb db = new DriverDb()) 
            { 
                db.ConnectDb ();

                string sql = TrackControlQuery.MobitelProvider.SelectMobitelId;
               
                return db.GetDataTable (sql);
            }
        }

        public static DataTable GetAllLastTimeGpsFromOnline ()
        {
            //using (ConnectMySQL cn = new ConnectMySQL())
            using (DriverDb db = new DriverDb()) { 
                db.ConnectDb ();

                string sql = TrackControlQuery.MobitelProvider.SelectOnlineMobitelId;

                //return cn.GetDataTable(sql);
                return db.GetDataTable (sql);
            }
        }

        public static DateTime GetLastTimeGps (int id_mobitel)
        {
            //using (ConnectMySQL cn = new ConnectMySQL())
            using (DriverDb db = new DriverDb()) { 
                db.ConnectDb ();

                string sql = string.Format (TrackControlQuery.MobitelProvider.SelectFromUnixTime,
                                            id_mobitel);

                //return cn.GetScalarValueDateTimeNull(sql,DateTime.MinValue) ; 
                return db.GetScalarValueDateTimeNull (sql, DateTime.MinValue); 
            }
        }

        public static GpsData GetOneRecord64(DriverDb db, bool dgpsToGps = false, float minimumSpeedVehicle = 0, bool getSrvPacketId = true)
        {
            const double latLngKf = 10000000.0;
            GpsData data = new GpsData
                {
                    Id = db.GetInt64("DataGps_ID"),
                    LogId = db.GetInt32("LogID"),
                    Time = db.GetDateTime("Time"),
                };

            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                double lat = db.GetDouble("Lat");
                double lng = db.GetDouble("Lng");
                data.LatLng = new PointLatLng(lat, lng);
                data.Speed = (float) db.GetInt16("Speed");
                data.RssiGsm = db.GetInt16("RssiGsm");
                data.Satellites = db.GetInt16("Satellites");
                data.Voltage = db.GetInt16("Voltage");
                data.sVoltage = Math.Round((double)db.GetInt16("Voltage") * 0.15, 3);
                data.SensorsSet = db.GetInt16("SensorsSet");
                data.Accel = db.GetInt16("Acceleration");
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                double lat = db.GetInt32("Lat");
                double lng = db.GetInt32("Lon");

                double dlat = 0.0;
                double dlng = 0.0;

                if (lat != 0)
                    dlat = lat / latLngKf;

                if (lng != 0)
                    dlng = lng / latLngKf;

                data.LatLng = new PointLatLng(dlat, dlng);
                data.Speed = (float)Convert.ToDouble(db.GetValue(db.GetOrdinal("Speed"))) / 10;
                data.RssiGsm = Convert.ToInt16(db.GetValue(db.GetOrdinal("RssiGsm")));
                data.Satellites = Convert.ToInt16(db.GetValue(db.GetOrdinal("Satellites")));
                data.Voltage = Convert.ToInt16(db.GetValue(db.GetOrdinal("Voltage")));
                data.sVoltage = Math.Round((double)Convert.ToInt16(db.GetValue(db.GetOrdinal("Voltage"))) * 0.15, 3);
                data.SensorsSet = Convert.ToInt16(db.GetValue(db.GetOrdinal("SensorsSet")));
                data.Accel = Convert.ToDouble(db.GetValue(db.GetOrdinal("Acceleration")));
            }

            // -- 20.03.2014 - aketner ---------------------------
            if (data.Speed < minimumSpeedVehicle)
                data.Speed = 0;
            // ---------------------------------------------------
            
            data.Valid = db.GetBoolean("Valid");
            byte[] initSensors = new byte[25];
            string sensorsBuf = db.GetString("Sensors");
            byte[] sensors = Convert.FromBase64String(db.GetString("Sensors"));
            sensors.CopyTo(initSensors, 25 - sensors.Length);
            Array.Reverse(initSensors); 
            data.Sensors = initSensors;
            string dGps = db.GetString("DGPS");
            double latitude = 0;
            double longitude = 0;
            short altitude = 0;
            if (dGps.Length > 1)
            {
                string[] dGpsParts = dGps.Split(new char[]{'/'},StringSplitOptions.RemoveEmptyEntries);
                
                if (dGpsParts.Length== 3)
                {
                    if (Double.TryParse(dGpsParts[0], out latitude)) 
                        latitude = latitude / latLngKf;

                    if (Double.TryParse(dGpsParts[1], out longitude)) 
                        longitude = longitude / latLngKf;

                    Int16.TryParse(dGpsParts[2], out altitude);
                }

                data.LatLngDgps = new PointLatLng(latitude, longitude);

                if (dgpsToGps) 
                    data.LatLng = data.LatLngDgps;

                data.Altitude = altitude;
            }

            data.Events = db.GetUInt32("Events");
            data.sEvents = db.GetUInt32("Events").ToString("X");

            data.Mobitel = db.GetInt32("Mobitel_ID");
            if (getSrvPacketId) data.SrvPacketID = db.GetInt64("SrvPacketID");
            return data;
        }
    }
}
