using System;
using System.Collections.Generic;
using DevExpress.Data.Filtering;
using TrackControl.General;
using TrackControl.Vehicles.Properties;


namespace TrackControl.Vehicles
{
    /// <summary>
    /// ��������, �������������� ���������� ������������� ��������
    /// </summary>
    /// </summary>
    public class Vehicle : Entity, IVehicle, IComparable<Vehicle>
    {
        public static IList<PcbVersions> PcbVersionData;
        private int PcbVerId = -1;
        protected ulong _rfidNumber = 0; // driver
        protected UInt16? _aggregateRfidNumber = 0; // aggregate

        public Vehicle(int mobitelId, ulong rfidDriver)
        {
            _rfidNumber = rfidDriver;
            VehicleProvider2.GetOne(mobitelId, this);
        }

        public Vehicle(int mobitelId, ulong rfidDriver, UInt16? rfidaggregate)
        {
            _rfidNumber = rfidDriver;
            Aggregate = VehicleProvider2.GetAggregateFromIdentifier(rfidaggregate);
            _aggregateRfidNumber = Aggregate.Identifier;
            VehicleProvider2.GetOne(mobitelId, this);
        }

        public void VehicleInfo(int mobitelId, ulong rfid)
        {
            _rfidNumber = rfid;
           VehicleProvider2.GetOne(mobitelId, this);

            if (Group != null)
                GroupStr = Group.Name;
            _info = Info;
        }

        public void VehicleInfoAggregate(int mobitelId)
        {
            VehicleProvider2.GetAggregateFromMobitel(mobitelId, this);
            if (Aggregate != null)
            {
                if (Aggregate.AggregateName == "")
                {
                    Aggregate.AggregateName = Resources.NoData;
                }

                _aggregateRfidNumber = Aggregate.Identifier;
            }
        }

        public string GroupStr { get; set; }


        public Vehicle(int mobitelId, string idOutLink ="")
        {
            if (idOutLink.Trim().Length > 0) 
                mobitelId = VehicleProvider2.GetMobitelIdByOutLinkId(idOutLink);
            if (mobitelId > 0) 
                VehicleProvider2.GetOne(mobitelId, this);
        }

        /// <summary>
        /// ������� � �������������� ����� ��������� ������ TrackControl.General.Model.Vehicles.Vehicle
        /// </summary>
        /// <param name="mobitel">��������, ������������ � ������������� ��������</param>
        /// <param name="carMaker">������������� ������������� ��������</param>
        /// <param name="carModel">������ ������������� ��������</param>
        /// <param name="regNumber">��������������� ����� ������������� ��������</param>
        /// <param name="group">������, � ������� ����������� ������������ ��������</param>
        /// <param name="style">��������������� � ����������� ����� ��� �����������</param>
        public Vehicle(Mobitel mobitel, string carMaker, string carModel, string regNumber, VehiclesGroup group,
            VehicleStyle style, string IdOutLink, double fuel_way, double fuel_motor, int identif, string cmmnts)
        {
            if (null == mobitel)
                throw new ArgumentNullException("mobitel");
            _mobitel = mobitel;

            if (null == carMaker)
                throw new ArgumentNullException("carMaker");
            _carMaker = carMaker;

            if (null == carModel)
                throw new ArgumentNullException("carModel");
            _carModel = carModel;

            if (null == regNumber)
                throw new ArgumentNullException("regNumber");
            _regNumber = regNumber;

            if (null == group)
                throw new ArgumentNullException("group");
            Group = group;

            if (null == style)
                throw new ArgumentNullException("style");
            _style = style;
            _IdOutLink = IdOutLink;
            Sensors = new List<Sensor>();

            fuelWays = fuel_way;
            fuelMotor = fuel_motor;
            VehicleComment = cmmnts;

            if (fuelWays <= 0.0)
            {
                fuelWays = 0.0;
                if (group.FuelWayKmGrp != "")
                    fuelWays = Convert.ToDouble(group.FuelWayKmGrp);
            }

            if (fuelMotor <= 0.0)
            {
                fuelMotor = 0.0;
                if (group.FuelMotoHrGrp != "")
                    fuelMotor = Convert.ToDouble(group.FuelMotoHrGrp);
            }

            if (identif <= 0)
            {
                vehIdentif = 0;
            }
            else
            {
                vehIdentif = identif;
            }
        }

        public int PcbVersionId
        {
            get
            {
               return PcbVerId;
            }
        }

        /// <summary>
        /// ����������� ��� ������������� �� ������� �������, ��� ��� ������������� ��������
        /// � ������� � �����������.
        /// </summary>
        public Vehicle(Mobitel mobitel, string carMaker, string carModel, string regNumber, double fuelway,
            double fuelmotor, int identif, string cmmnts )
            : this(mobitel, carMaker, carModel, regNumber, new VehiclesGroup("", ""), VehicleStyle.Empty, "", fuelway,
                fuelmotor, identif, cmmnts)
        {
            // to do this
            return;
        }

        /// <summary>
        /// ��������, ������������ � ������������� ��������
        /// </summary>
        public Mobitel Mobitel
        {
            get { return _mobitel; }
            set { _mobitel = value; }
        }

        private Mobitel _mobitel;

        public VehiclesGroup Group
        {
            get { return _group; }
            set
            {
                if (null == value)
                    throw new Exception("Vehicle Group can not be NULL");

                if (_group != value)
                {
                    if (null != _group)
                        _group.RemoveVehicle(this);

                    _group = value;
                    _group.AddVehicle(this);
                }
            }
        }

        private VehiclesGroup _group;

        /// <summary>
        /// ������������� ������������� ��������
        /// </summary>
        public string CarMaker
        {
            get
            {
                if (_carMaker == null)
                    return "";
                else
                    return _carMaker;
            }
            set
            {
                if (null == value)
                    throw new Exception("Vehicle Brand can not be NULL");
                if (value.Length > 200)
                    throw new Exception("Vehicle Brands length can not be more then 200");
                _carMaker = value;
            }
        }

        private string _carMaker;

        /// <summary>
        /// ������ ������������� ��������
        /// </summary>
        public string CarModel
        {
            get
            {
                if (_carModel == null)
                    return "";
                else
                    return _carModel;
            }
            set
            {
                if (null == value)
                    throw new Exception("Vehicle Model can not be NULL");
                if (value.Length > 60)
                    throw new Exception("Vehicle Models length can not be more then 60");
                _carModel = value;
            }
        }

        private string _carModel;

        public string FuelWays
        {
            get { return Convert.ToString(Math.Round(fuelWays, 1)); }
            set
            {
                if (null == value)
                    throw new Exception("Vehicle fuel consumption can not be NULL");

                //if (Convert.ToDouble(value) <= 0.0)
                //    throw new Exception("Vehicle fuel consumption can not be equal or less then 0");
                fuelWays = Convert.ToDouble(value);
            }
        }

        private double fuelWays;

        public double FuelWaysSet
        {
            set
            {

                fuelWays = value;
            }
        }

        public string FuelMotor
        {
            get { return Convert.ToString(Math.Round(fuelMotor, 1)); }
            set
            {
                if (null == value)
                    throw new Exception("Vehicle fuel consumption can not be NULL");

                //if (Convert.ToDouble(value) <= 0.0)
                //    throw new Exception("Vehicle fuel consumption can not be equal or less then 0");
                fuelMotor = Convert.ToDouble(value);
            }
        }

        private double fuelMotor;

        public double FuelMotorSet
        {
            set
            {
                fuelMotor = value;
            }
        }

        public int Identifier
        {
            get
            {
                return vehIdentif;
            }
            set
            {
                if ( 0 > value)
                    throw new Exception("Vehicle fuel consumption can not be -1");

                //if (Convert.ToDouble(value) <= 0.0)
                //    throw new Exception("Vehicle fuel consumption can not be equal or less then 0");
                vehIdentif = value;
            }
        }

        private int vehIdentif;

        /// <summary>
        /// ��������������� ����� ������������� ��������
        /// </summary>
        public string RegNumber
        {
            get
            {
                if (_regNumber == null)
                    return "";
                else
                    return _regNumber;
            }
            set
            {
                if (null == value)
                    throw new Exception("Registration Number can not be NULL");
                if (value.Length > 60)
                    throw new Exception("Registration Numbers length can not be more then 60");
                _regNumber = value;
            }
        }

        private string _regNumber;

        /// <summary>
        /// �������� ������������� ��������
        /// </summary>
        public Driver Driver
        {
            get { return _driver; }
            set { _driver = value; }
        }

        private Driver _driver;

        /// <summary>
        /// ������� ������������� ��������
        /// </summary>
        public Aggregat Aggregate
        {
            get { return _aggregate; }
            set { _aggregate = value; }
        }

        private Aggregat _aggregate;

        /// <summary>
        /// ����� �������� ������������� ��������
        /// </summary>
        public PcbVersions PcbVersion
        {
            get { return _pcbver; }
            set { _pcbver = value; }
        }

        private PcbVersions _pcbver;

        /// <summary>
        /// ����� ��� ������������ ������������� ��������
        /// </summary>
        public VehicleStyle Style
        {
            get { return _style; }
            set { _style = value; }
        }

        private VehicleStyle _style;

        /// <summary>
        /// ��������� ��� ������������� ��������
        /// </summary>
        public VehicleSettings Settings
        {
            get { return _settings; }
            set { _settings = value; }
        }

        private VehicleSettings _settings;

        /// <summary>
        /// �������� ��������� ������� ��������� ������
        /// </summary>
        public int TimeStatus()
        {
            if (Mobitel == null)
                return (int) StaticMethods.TimeStatuses.Offline;
            return StaticMethods.GetStatusColor(Mobitel.LastTimeGps);
        }

        public object Tag
        {
            get { return _tag; }set { _tag = value; }
        }

        private object _tag;
        private string _info = "";

        /// <summary>
        /// ���������� ���������� � ������������ ��������.
        /// </summary>
        public string Info
        {
            get
            {
                if (_carMaker != null)
                {
                    if (_carMaker.Length > 0)
                    {
                        return String.Format("{0} \"{1} {2}\"",
                            _regNumber,
                            _carMaker,
                            _carModel);
                    }
                    else if (_carModel.Length > 0)
                    {
                        return String.Format("{0} \"{1}\"",
                            _regNumber,
                            _carModel);
                    }
                    return _info;
                }
                return "";
            }
        }

        public LogicSensor LogicSensor { get; set; }

        // VEHICLE_UNLOAD = 23
        public LogicSensor LogicSensorUpload { get; set; }

        public Sensor Sensor { get; set; }

        public int MobitelId
        {
            get
            {
                if (Mobitel != null) return Mobitel.Id;
                return 0;
            }
        }

        public List<Sensor> Sensors { get; set; }

        /// <summary>
        /// ���������� ������ on-line ��������.������ ���������� - ��������/������ �����
        /// </summary>
        private int _logicSensorState = (int) LogicSensorStates.Absence;

        public int LogicSensorState
        {
            get { return _logicSensorState; }
            set { _logicSensorState = value; }
        }

        public GpsData LastPoint { get; set; }

        public bool Checked { get; set; }

        //public  enum TimeStatuses
        //{
        //    Active,
        //    IntervalNear,
        //    IntervalFar,
        //    Offline
        //}

        /// <summary>
        /// ��������� ������������� �������� - ��� ����������� ������� � ������ ���������������
        /// </summary>
        public VehicleCategory Category { get; set; }

        public VehicleCategory2 Category2 { get; set; }

        public VehicleCategory3 Category3 { get; set; }

        public VehicleCategory4 Category4 { get; set; }

        public string VehicleComment { get; set; }

        public string CategoryMixed
        {
            get
            {
                string mixedCategory = "";
                if (Category != null)
                {
                    mixedCategory = Category.Name;
                    if (Category2 != null)
                        mixedCategory = string.Format("{0} {1}", mixedCategory, Category2.Name);
                }
                else
                {
                    if (Category2 != null)
                        mixedCategory = Category2.Name;
                }

                return mixedCategory;
            }
        }

        public  bool Is64BitPackets { get; set; }

        public int IdAgroWorkType { get; set; }

        public int IdAgroAgregat { get; set; }

        public ushort RfidDriver
        {
            get { return (ushort)_rfidNumber; }
        }

        public string DevIdShort { get; set; }

        public override string ToString()
        {
            return String.Format("{0}", Info);
        }


        #region ����� IComparable<Vehicle>

        public int CompareTo(Vehicle other)
        {
            if (other == null)
                return 1;
            return Info.CompareTo(other.Info);
        }

        #endregion
    }
}
