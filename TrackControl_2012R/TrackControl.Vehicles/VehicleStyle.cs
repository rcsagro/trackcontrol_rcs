using System;
using System.Drawing;
using System.Drawing.Text;
using System.Windows.Forms;  
using TrackControl.General;
namespace TrackControl.Vehicles
{
  /// <summary>
  /// ��������, �������������� ���������� ����� ��� ������������.
  /// ������ ��� ���� � ������ ,���� ��������� ������.
  /// </summary>
  public class VehicleStyle : Entity
  {
    Image _icon = Shared.Truck;
    Color _colorTrack = Color.FromArgb(Globals.TRACK_COLOR_DEFAULT);
    public Color ColorTrack
    {
        get {

            return _colorTrack; 
        }
        set { _colorTrack = value; }
    }

    public VehicleStyle()
    {

    }
    public VehicleStyle(Image icon)
    {
      Icon = icon;
    }
    public VehicleStyle(Color colorTrack)
    {
        _colorTrack = colorTrack;
    }
     
    public Image Icon
    {
      get { return _icon; }
      set
      {
        if (null == value)
          throw new Exception("Style Icon can not be NULL");

        _icon = value;
      }
    }

    public static VehicleStyle Empty
    {
      get
      {
        return new VehicleStyle(Shared.Truck);
      }
    }
  }
}
