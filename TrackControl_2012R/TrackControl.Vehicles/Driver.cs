using System;
using System.Drawing;
using MySql.Data.MySqlClient;
using TrackControl.General;
using TrackControl.Vehicles.Properties;

namespace TrackControl.Vehicles
{
    /// <summary>
    /// ��������, �������������� ���������� �������� ������������� ��������
    /// </summary>
    public class Driver : Entity
    {
        const int EMPTY_ID = Int32.MinValue;

        public string FirstName
        {
            get { return _firstName; }
            set { _firstName = value; }
        }

        string _firstName;

        public string LastName
        {
            get { return _lastName; }
            set { _lastName = value; }
        }

        string _lastName;

        public DateTime? DayOfBirth
        {
            get { return _dayOfBirth; }
            set { _dayOfBirth = value; }
        }

        DateTime? _dayOfBirth;

        /// <summary>
        /// �������� ���������
        /// </summary>
        public string Categories
        {
            get { return _categories; }
            set { _categories = value; }
        }

        string _categories;

        /// <summary>
        /// ���������� � ������������ �������������
        /// �������� �����, ���� ������...
        /// </summary>
        public string License
        {
            get { return _license; }
            set { _license = value; }
        }

        string _license;

        /// <summary>
        /// ������������� �������� ��������
        /// </summary>
        public UInt16? Identifier
        {
            get { return _identifier; }
            set { _identifier = value; }
        }

        UInt16? _identifier;
        
        public string NumTelephone
        {
            get { return _numtelephone; }
            set { _numtelephone = value; }
        }

        string _numtelephone = "";

        public string Department { get; set; }

        public Image Photo
        {
            get { return _photo; }
            set { _photo = value; }
        }

        Image _photo;

        public object Tag
        {
            get { return _tag; }
            set { _tag = value; }
        }

        object _tag;

        /// <summary>
        /// ������� � ��� ��������.
        /// </summary>
        public string DriverFullName
        {
            get { return String.Format("{0} {1}", _lastName, _firstName); }
        }

        /// <summary>
        /// ������������� ��������
        /// </summary>
        public DriverType TypeDriver
        {
            get { return _typeDriver; }
            set { _typeDriver = value; }
        }

        DriverType _typeDriver;

        public int TypeDriverId
        {
            get
            {
                if (_typeDriver == null)
                {
                    return 0;
                }
                else
                {
                    return _typeDriver.Id;
                }
            }
            set { _typeDriver = new DriverType(value); }
        }

        public Driver(int idDriver, UInt16? identifier, string idOutLink ="")
        {
            Driver driver = null;
            if (idOutLink.Trim().Length >0)
                driver = DriverVehicleProvider.GetDriverFromOutLinkId(idOutLink);
            else if (identifier > 0)
                driver = DriverVehicleProvider.GetDriverFromIdentifier(identifier);
            else
                driver = DriverVehicleProvider.GetDriver(idDriver);
            if (driver != null)
            {
                this.Id = driver.Id;
                this.FirstName = driver.FirstName;
                this.LastName = driver.LastName;
                this.DayOfBirth = driver.DayOfBirth;
                this.Categories = driver.Categories;
                this.Identifier = driver.Identifier;
                this.License = driver.License;
                this.IdOutLink = driver.IdOutLink;
                this.TypeDriver = driver.TypeDriver;
                this.NumTelephone = driver.NumTelephone;
            }
        }


        /// <summary>
        /// ����������� ��� TrackControl.Vehicles.Driver
        /// </summary>
        /// <param name="firstName">���</param>
        /// <param name="lastName">�������</param>
        /// <param name="dayOfBirth">���� ��������</param>
        /// <param name="categories">�������� ���������</param>
        /// <param name="license">���������� � ������������ �������������</param>
        /// <param name="identifier">������������� ��������</param>
        /// <param name="photo">����������</param>
        /// <param name="idOutLink">��� �������������� ���� ��������� (��� 1C)</param>
        /// <param name="numTelephone">����� �������� �������� ��� �����</param>
        public Driver(string firstName, string lastName, DateTime? dayOfBirth,
            string categories, string license, UInt16? identifier, Image photo, string idOutLink, string numTelephone)
        {
            _firstName = firstName;
            _lastName = lastName;
            _dayOfBirth = dayOfBirth;
            _categories = categories;
            _license = license;
            _identifier = identifier;
            _photo = photo;
            _IdOutLink = idOutLink;
			_numtelephone = numTelephone;
        }

        public Driver(string firstName, string lastName)
            : this(firstName, lastName, null, String.Empty, String.Empty, null, null, "", "")
        {
        }

        public static Driver NewDriver
        {
            get
            {
                return new Driver(
                    String.Format("-- {0} --", Resources.Name),
                    String.Format("-- {0} --", Resources.Surname));
            }
        }

        public static Driver Empty
        {
            get
            {
                Driver empty = new Driver(String.Format("-- {0} -- ", Resources.NotAssigned), "");
                empty.Id = EMPTY_ID;
                return empty;
            }
        }

        public bool IsEmpty
        {
            get { return Id == EMPTY_ID; }
        }

        public string FullName
        {
            get { return String.Format("{0} {1}", _lastName, _firstName); }
        }

        public override string ToString()
        {
            return String.Format("{0} {1}", _lastName, _firstName);
        }
    }
}
