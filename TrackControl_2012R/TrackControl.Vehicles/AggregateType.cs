﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TrackControl.General;
using TrackControl.Vehicles.DAL;

namespace TrackControl.Vehicles
{
    // виды, модели, типы агрегатов
    public class AggregateType : Entity
    {
        public string TypeName { set; get; }

        public AggregateType()
        {
            // to do this
        }

        public AggregateType(int id, string typeAggregate)
        {
            Id = id;
            TypeName = typeAggregate;
        }

        public AggregateType(int id)
        {
            Id = id;
        }
    }
}
