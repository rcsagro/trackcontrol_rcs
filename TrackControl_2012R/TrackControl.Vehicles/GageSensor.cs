using System;

namespace TrackControl.Vehicles
{
  /// <summary>
  /// ������������� (�� ����������) ������
  /// </summary>
  public abstract class GageSensor : ExternalDevice
  {
    public GageSensor(string name, string description, int startBit, int bitLength)
      : base(name, description, startBit, bitLength)
    {
    }

    public GageSensor(int id_sensor)
    {
    }
    public abstract double GetValue(ulong data);
  }
}
