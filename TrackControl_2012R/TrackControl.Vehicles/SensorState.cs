﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TrackControl.General;

namespace TrackControl.Vehicles
{
    public class SensorState : Entity
    {
        public string Title { get; set; }

        public double MinValue { get; set; }

        public double MaxValue { get; set; }

        public SensorState()
        {
            
        }
        public SensorState(int idState)
        {
            if (idState > 0)
            {
                Id = idState;
                var sensorState = SensorProvider.GetSensorState(idState);
                if (sensorState != null)
                {
                    this.Title = sensorState.Title;
                    this.MinValue = sensorState.MinValue;
                    this.MaxValue = sensorState.MaxValue;
                }
            }
        }
    }
}
