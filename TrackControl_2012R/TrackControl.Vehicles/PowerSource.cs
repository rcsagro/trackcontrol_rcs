using System;

namespace TrackControl.Vehicles
{
  public enum PowerSource
  {
    OnBoard = 0,
    Reserve = 1,
    NotDetected = 2
  }
}
