using Aladdin.HASP;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
//using HaspWrapConsole.;

namespace WrapDriver
{
    public static class HaspService 
  {
    const string HASP_ID_NODE = "hasp_info/keyspec/hasp/haspid";
    const string HASP_RES = "BFz2gF7MZAPoDKKIagtyI67kHws+K2gJKzdawd56VhqYoIvRvIfMjb2h7OCU+Lj9nPxyGJaUDSZUuB8iP1t/2st6NHUONcGKX8sNiRcFQPAVP46F2cQvlLAszPb4Z+Ua0WwHck7F4FmMD9jfL+PtR5vS4L//lV5E9+2fKMwNThea/lx2+zEDbN31TcSd2NLCgtbazHWFSZJk5ScaOyWBd0hbnKATi3JY7bQAetGPIhNbD4hxFGq9b2PxoWeBihLQMUrdunkUHlBR9BZyQjthCQepiaqZUQwDppj5JTFbp8nwM/i3ytBH44mbanclQl44e/DLCx2ozux5IAvHHRbA9l5z1KAbQqXGvk5m8PYnJR2Y0MuWg8dN46LCg+6AIGL90MnjcRorKoFem+vBxJwTAshKFGBi4/fQS+10ipiiqHWCGOiJF/yMyWJ4squO6nEAYI2NDndiyVYnpgKXOGkHsMCf1QMjdOAZoeOFj5XQNtNsEntf7B31aaXgDaAk+Y1O68GuEWvrCnbft9KDKA5iLDIpziiuAmci5CWI08LOGm5+KneSpm3cCP1ZWPPphjjFymjaRUMzfUetcI4xYFNxOX9HIYIp41bYc+4PajOeraIUCZQAlcnYs9daCxe+Eu3OJmQDTHjb8Nbwmw2U0S6SqelPJikZuonuDAk6ACuL5CjyMB29ILnN8KWjVJTOdKeNSX1n75T/0dXzXimX2HwHBnZY103g";

    static HaspStatus _status;
    static List<UInt32> _mapCodes;
    public static HaspState GetHaspNumberState(int L_NUMBER)
      {
          using (Aladdin.HASP.Hasp hasp = new Aladdin.HASP.Hasp(HaspFeature.FromProgNum(L_NUMBER)))
          {
              _status = hasp.Login(ASCIIEncoding.Default.GetBytes(HASP_RES));
              return State;
              
          }
      }
    static HaspService()
    {
        using (Aladdin.HASP.Hasp hasp = new Aladdin.HASP.Hasp(HaspFeature.FromProgNum(GlobalsHasp.LICENSE_NUMBER)))
      {
          _status = hasp.Login(ASCIIEncoding.Default.GetBytes(HASP_RES));
        _mapCodes = getMapCodes(hasp);
      }
    }

    public static HaspState State
    {
      get
      {
        if (HaspStatus.StatusOk == _status)
          return HaspState.OK;

        if (HaspStatus.FeatureNotFound == _status)
          return HaspState.LicenseNotFound;
        
        return HaspState.HaspNotFound;
      }
    }

    /// <summary>
    /// ��������� ����� ��� ���� EPRASYS (GIS-���������)
    /// </summary>
    public static List<UInt32> MapCodes
    {
      get { return _mapCodes; }
    }

    #region --   �������   --
    /// <summary>
    /// ���������� ���������� � ������
    /// </summary>
    static string getSessionInfo(Aladdin.HASP.Hasp hasp, string nodename)
    {
      // firstly we will retrieve the key info.
      string info = null;
      HaspStatus status = hasp.GetSessionInfo(Aladdin.HASP.Hasp.KeyInfo, ref info);
      XmlDocument xmlInfo = new XmlDocument();
      xmlInfo.LoadXml(info);
      XmlNode node = xmlInfo.SelectSingleNode(nodename);
      return node.InnerText;
    }

    /// <summary>
    /// ���������� ��������� ������ ��� ����
    /// </summary>
    static List<UInt32> getMapCodes(Aladdin.HASP.Hasp hasp)
    {
       List<UInt32> result = new List<uint>();
        try
        {
            
            if ((null != hasp) && hasp.IsLoggedIn())
            {
                int id = Convert.ToInt32(getSessionInfo(hasp, HASP_ID_NODE));
                HaspFile file = hasp.GetFile(HaspFiles.Main);
                if (file.IsLoggedIn())
                {
                    int size = 0;
                    file.FileSize(ref size);
                    byte[] bytes = new byte[size];
                    file.Read(bytes, 0, bytes.Length);

                    int firstBlock = bytes[0];
                    int secondBlock = bytes[firstBlock + 1];

                    for (int i = 0; i < secondBlock / 4; i++)
                    {
                        uint code = bytes[firstBlock + i * 4 + 2];
                        code += (uint)bytes[firstBlock + i * 4 + 3] << 8;
                        code += (uint)bytes[firstBlock + i * 4 + 4] << 16;
                        result.Add(Convert.ToUInt32(code ^ id));
                    }
                }
            }
        }
        catch
        {
            //MessageBox.Show(ex.Message, "HASP");
        }
      return result;
    }


    #endregion
  }
}
