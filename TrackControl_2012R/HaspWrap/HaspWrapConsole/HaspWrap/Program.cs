﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WrapDriver
{
    public class WrapDriver
    {
        static void Main(string[] args)
        {
            if (!String.IsNullOrEmpty(args[0]))
            {
                if (args[0] == "MapCodes")
                {
                    WriteMapCodesToStdOut();
                }
                else if (args[0] == "Lic")
                {
                    int licNumber = 0;
                    if (Int32.TryParse( args[1],out licNumber))
                        WriteLicNumberToStdOut(licNumber);
                }
            }
        }

        static void WriteMapCodesToStdOut()
        {
            List<UInt32> mapCodes = HaspService.MapCodes;
            Console.WriteLine((int)HaspService.State);
            Console.WriteLine(mapCodes.Count);
            if (mapCodes.Count > 0)
            {
                for (int i = 0; i < mapCodes.Count; i++)
                {
                    Console.WriteLine(mapCodes[i].ToString());
                }
            }
            Console.WriteLine("End");
        }

        static void WriteLicNumberToStdOut(int L_NUMBER)
        {
            HaspState hs = HaspService.GetHaspNumberState(L_NUMBER);
            Console.WriteLine((int)hs);
            Console.WriteLine("End");
        }
    }
}
