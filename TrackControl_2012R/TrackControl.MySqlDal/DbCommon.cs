using System;
using System.Data;
using System.Windows.Forms; 
using MySql.Data.MySqlClient;
using TrackControl.General.DAL;
using TrackControl.General.DatabaseDriver;


namespace TrackControl.MySqlDal
{
    public class DbCommon
    {
        private string _cs; // ������ ����������� � ��

        public DbCommon(string cs)
        {
            if (String.IsNullOrEmpty(cs))
                throw new ArgumentException("Connection String can not be NULL or Empty.");

            _cs = cs;
        }

        public string CS
        {
            get { return _cs; }
        }

        public string ServerName
        {
            get
            {
                //using (MySqlConnection conn = GetConnection())
                var db = new DriverDb();
                db.SqlConnection = GetConnection();
                {
                    return db.ConnectionDataSource; //conn.DataSource;
                }
            }
        }

        public string DbName
        {
            get
            {
                //using (MySqlConnection conn = GetConnection())
                var db = new DriverDb();
                db.SqlConnection = GetConnection();
                {
                    return db.ConnectionDataBase; //conn.Database;
                }
            }
        }

        public object GetConnection()
        {
            var db = new DriverDb();
            db.NewSqlConnection(_cs);
            return db.SqlConnection; //new MySqlConnection(_cs);
        }

        /// <summary>
        /// ��������� ���������� � �� + ������� ����� ��������
        /// </summary>
        public bool CheckConnection()
        {
            if (String.IsNullOrEmpty(_cs))
                return false;

            //MySqlConnection connection = new MySqlConnection(_cs);
            var db = new DriverDb();
            db.NewSqlConnection(_cs);
            try
            {
                db.SqlConnectionOpen(); //connection.Open();
                TestNewDataBaseObjects testObj = new TestNewDataBaseObjects(this);
                if (!testObj.TestIsExistNewObjects())
                {
                    db.SqlConnectionClose(); //connection.Close();
                    return false;
                }
                db.SqlConnectionClose(); //connection.Close();
            }
            catch (Exception)
            {
                return false;
            }
            finally
            {
                db.SqlConnectionDispose(); //connection.Dispose();
            }

            return true;
        }

        /// <summary>
        /// ��������� ������������� ������� ��.
        /// </summary>
        /// <param name="tableName">�������� �������.</param>
        public bool IsTableExist(string tableName)
        {
            bool result = false;
            //using (MySqlCommand command = new MySqlCommand())
            var db = new DriverDb();
            db.NewSqlCommand();
            {
                //using (MySqlConnection conn = GetConnection())
                db.SqlConnection = GetConnection();
                {
                    //command.CommandType = CommandType.Text;
                    db.CommandType(CommandType.Text);
                    //command.Connection = conn;
                    db.CommandSqlConnection();
                    //command.CommandText = String.Format(CHECK_TABLE, conn.Database, tableName);
                    
                    db.CommandText( TrackControlQuery.DbCommon.CheckTable(db.ConnectionDataBase, tableName ) );
                   
                    db.SqlConnectionOpen(); //conn.Open();

                    //result = Convert.ToBoolean(command.ExecuteScalar());
                    object xxO = db.CommandExecuteScalar();

                    if (xxO != null)
                        result = true;
                }
                db.SqlConnectionClose();
            }
            return result;
        }

        /// <summary>
        /// ��������� ������������� � �� ������� � ������������� �������.
        /// </summary>
        /// <param name="tableName">�������� �������.</param>
        /// <param name="columnName">�������� �������.</param>
        public bool IsColumnExist(string tableName, string columnName)
        {
            bool result = false;
            //using (MySqlCommand command = new MySqlCommand())
            var db = new DriverDb();
            db.NewSqlCommand();
            {
                //using (MySqlConnection conn = GetConnection())
                db.SqlConnection = GetConnection();
                {
                    //command.CommandType = CommandType.Text;
                    db.CommandType(CommandType.Text);
                    //command.Connection = conn;
                    db.CommandSqlConnection();
                    //command.CommandText = String.Format(CHECK_COLUMN, conn.Database, tableName, columnName);

                    db.CommandText(TrackControlQuery.DbCommon.CheckColumn(db.ConnectionDataBase, tableName, columnName));
                   
                    db.SqlConnectionOpen(); //conn.Open();
                    //result = Convert.ToBoolean(command.ExecuteScalar());

                    object xxO = db.CommandExecuteScalar();

                    if (xxO != null)
                        result = true;
                }
                db.SqlConnectionClose();
            }
            return result;
        }
    }
}
