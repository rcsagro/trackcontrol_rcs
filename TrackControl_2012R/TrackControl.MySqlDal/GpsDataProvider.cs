using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using DevExpress.XtraEditors;
using MySql.Data.MySqlClient;
using TrackControl.General;
using TrackControl.General.DAL;
using TrackControl.General.DatabaseDriver;
using TrackControl.Vehicles;
using TrackControl.Statistics;
using TrackControl.Reports;
using System.Windows.Forms;

namespace TrackControl.MySqlDal{
    public class GpsDataProvider : IGpsDataProvider
    {
        private static string Cmd_Type = "";
        //DbCommon _dbCommon;
        private DriverDb _dbCommon;

        public GpsDataProvider()
        {
            // to do
        }

        //public GpsDataProvider(DbCommon dbCommon)
        public GpsDataProvider(DriverDb dbCommon)
        {
            _dbCommon = dbCommon;
        }

        public IList<GpsData> GetValidDataForPeriod(Vehicle vehicle, DateTime begin, DateTime end)
        {
            var result = new List<GpsData>();
            if (vehicle.Mobitel.Is64BitPackets)
            {
                result = GetGpsData64FromDbMobitelId(vehicle.MobitelId, begin, end);
            }
            else
            {
                

                //var db = new DriverDb();
                _dbCommon.GetCommand = CreateSelectCommand(vehicle.Mobitel.Id, begin, end, true);
                //using (MySqlCommand cmd = CreateSelectCommand(vehicle.Mobitel.Id, begin, end, true))
                {
                    //using (MySqlConnection conn = _dbCommon.GetConnection())
                    _dbCommon.SqlConnection = _dbCommon.GetConnection();
                    {
                        //cmd.Connection = conn;
                        _dbCommon.CommandSqlConnection();
                        //cmd.Connection.Open();
                        _dbCommon.CommandConnectionOpen();

                        //using (MySqlDataReader reader = cmd.ExecuteReader())
                        _dbCommon.SqlDataReader = _dbCommon.CommandExecuteReader();
                        {
                            while (_dbCommon.Read())
                            {
                                ReadRecordDataGps(result, _dbCommon.SqlDataReader);
                            }
                        }
                        _dbCommon.CloseDataReader();
                    }
                    _dbCommon.SqlConnectionClose();
                }
             }
            TrackRepairService.CheckTrack(result);
            return result;
        }

        private static void ReadRecordDataGps(List<GpsData> result, object read)
        {
            if (DriverDb.MySqlUse == DriverDb.TypeDataBaseUsing)
            {
                MySqlDataReader reader = read as MySqlDataReader;

                if (reader == null)
                    throw new Exception("Type MySqlDataReader is not peresent!");

                GpsData dataGps = new GpsData();
                GetDataGpsRecord(reader, dataGps);
                result.Add(dataGps);
            } // if
            else if (DriverDb.MssqlUse == DriverDb.TypeDataBaseUsing)
            {
                SqlDataReader reader = read as SqlDataReader;

                if (reader == null)
                    throw new Exception("Type SqlDataReader is not peresent!");

                GpsData dataGps = new GpsData();

                if (Cmd_Type == "dataview")
                {
                    GetDataGpsRecordSql(reader, dataGps);
                }
                else
                {
                    GetDataGpsRecord(reader, dataGps);
                }

                result.Add(dataGps);
            } // else if
        } // ReadRecordDataGps

        // ����������� ������ � ������ MS SQL
        private static void GetDataGpsRecordSql(object read, GpsData dataGps)
        {
            if (DriverDb.MssqlUse == DriverDb.TypeDataBaseUsing)
            {
                SqlDataReader reader = read as SqlDataReader;

                if (reader == null)
                    throw new Exception("Type SqlDataReader is not peresent!");

                int k = reader.GetOrdinal("DataGps_ID");
                dataGps.Id = reader.GetInt32(k);
                k = reader.GetOrdinal("LogID");
                dataGps.LogId = reader.GetInt32(k);
                k = reader.GetOrdinal("Altitude");
                dataGps.Altitude = reader.GetInt32(k);
                k = reader.GetOrdinal("Lat");
                int m = reader.GetOrdinal("Lon");
                double lat = Convert.ToDouble(reader.GetValue(k).ToString());
                double lng = Convert.ToDouble(reader.GetValue(m).ToString());
                dataGps.LatLng = new PointLatLng(lat, lng);
                k = reader.GetOrdinal("Events");
                dataGps.Events = (UInt32) reader.GetInt64(k);
                k = reader.GetOrdinal("sensor");
                ulong sensors = (ulong) Convert.ToUInt64(reader.GetValue(k).ToString());
                dataGps.Sensors = BitConverter.GetBytes(sensors);
                k = reader.GetOrdinal("Speed");
                dataGps.Speed = (float) Convert.ToDouble(reader.GetValue(k).ToString());
                k = reader.GetOrdinal("time");
                dataGps.Time = reader.GetDateTime(k);
                k = reader.GetOrdinal("Valid");
                int vald = Convert.ToInt32(reader.GetValue(k).ToString());

                if (vald == 0)
                    dataGps.Valid = false;
                else
                    dataGps.Valid = true;

                k = reader.GetOrdinal("Mobitel_ID");
                dataGps.Mobitel = reader.GetInt32(k);
            } // else if
        } // GetDataGpsRecordSql

        private static void GetDataGpsRecord(object read, GpsData dataGps, bool addSrvPacketId = false)
        {
            try
            {
                if (DriverDb.MySqlUse == DriverDb.TypeDataBaseUsing)
                {
                    var reader = read as MySqlDataReader;
                    if (reader == null)
                        throw new Exception("Type MySqlDataReader is not peresent!");

                    dataGps.Id = reader.GetInt64("DataGps_ID");
                    dataGps.LogId = reader.GetInt32("LogID");
                    dataGps.Altitude = reader.GetInt32("Altitude");
                    dataGps.LatLng = new PointLatLng(reader.GetDouble("Lat"), reader.GetDouble("Lon"));
                    dataGps.Events = reader.GetUInt32("Events");
                    dataGps.Sensors = BitConverter.GetBytes(reader.GetUInt64("sensor"));
                    dataGps.Speed = reader.GetFloat("speed");
                    dataGps.Time = reader.GetDateTime("time");
                    dataGps.Valid = reader.GetBoolean("Valid");
                    dataGps.Mobitel = reader.GetInt32("Mobitel_ID");

                    if (addSrvPacketId)
                        dataGps.SrvPacketID = reader.GetInt64("SrvPacketID");
                } // if
                else if (DriverDb.MssqlUse == DriverDb.TypeDataBaseUsing)
                {
                    var reader = read as SqlDataReader;

                    if (reader == null)
                        throw new Exception("Type SqlDataReader is not peresent!");

                    int k = reader.GetOrdinal("DataGps_ID");
                    try
                    {
                        try
                        {
                            dataGps.Id = reader.GetInt32(k);
                        }
                        catch (Exception ex)
                        {
                            dataGps.Id = reader.GetInt64(k);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }

                    k = reader.GetOrdinal("LogID");
                    dataGps.LogId = reader.GetInt32(k);
                    k = reader.GetOrdinal("Altitude");
                    dataGps.Altitude = reader.GetInt32(k);
                    k = reader.GetOrdinal("Lat");
                    int m = reader.GetOrdinal("Lon");
                    double lat = reader.GetInt32(k)/600000.0;
                    double lng = reader.GetInt32(m)/600000.0;
                    dataGps.LatLng = new PointLatLng(lat, lng);
                    k = reader.GetOrdinal("Events");
                    dataGps.Events = (UInt32) reader.GetInt64(k);

                    //k = reader.GetOrdinal("sensor");
                    //ulong sensors = (ulong)Convert.ToUInt64(reader.GetValue(k).ToString());
                    //dataGps.Sensors = BitConverter.GetBytes(sensors);

                    ulong s8 = reader.GetByte(reader.GetOrdinal("Sensor8"));
                    ulong s7 = reader.GetByte(reader.GetOrdinal("Sensor7"));
                    ulong s6 = reader.GetByte(reader.GetOrdinal("Sensor6"));
                    ulong s5 = reader.GetByte(reader.GetOrdinal("Sensor5"));
                    ulong s4 = reader.GetByte(reader.GetOrdinal("Sensor4"));
                    ulong s3 = reader.GetByte(reader.GetOrdinal("Sensor3"));
                    ulong s2 = reader.GetByte(reader.GetOrdinal("Sensor2"));
                    ulong s1 = reader.GetByte(reader.GetOrdinal("Sensor1"));

                    ulong sensors = (s8 + (s7 << 8) + (s6 << 16) + (s5 << 24) + (s4 << 32) + (s3 << 40) + (s2 << 48) +
                               (s1 << 56));

                    k = reader.GetOrdinal("speed");
                    dataGps.Speed = (float) (reader.GetInt16(k)*1.852);
                    k = reader.GetOrdinal("time");
                    dataGps.Time = reader.GetDateTime(k);
                    k = reader.GetOrdinal("Valid");
                    int vald = reader.GetInt16(k);

                    if (vald == 0)
                        dataGps.Valid = false;
                    else
                        dataGps.Valid = true;

                    k = reader.GetOrdinal("Mobitel_ID");
                    dataGps.Mobitel = reader.GetInt32(k);

                    if (addSrvPacketId)
                    {
                        k = reader.GetOrdinal("SrvPacketID");
                        dataGps.SrvPacketID = reader.GetInt64(k);
                    }
                } // else if
            }
            catch (Exception ex){
                XtraMessageBox.Show(ex.Message + "\n" + ex.StackTrace, "Error Get Data GPS Record", MessageBoxButtons.OK);
            }
        } // GetDataGpsRecord

        /// <summary>
        /// �������� ������� ������� ������ MySqlCommand.
        /// </summary>
        /// <param name="mobitelId">������������� ���������.</param>
        /// <param name="beginTime">��������� �����.</param>
        /// <param name="endTime">�������� �����.</param>
        /// <param name="valid">���������� ������.</param>
        /// <returns>MySqlCommand</returns>
        private object CreateSelectCommand(int mobitelId, DateTime beginTime, DateTime endTime, bool valid)
        {
            //MySqlCommand cmd = new MySqlCommand();
            var db = new DriverDb();
            db.NewSqlCommand();
            //cmd.CommandText = "dataview";
            db.CommandText("dataview");
            //cmd.CommandType = CommandType.StoredProcedure;
            db.CommandType(CommandType.StoredProcedure);
            //cmd.CommandTimeout = 300;
            db.CommandTimeout(300);

            //cmd.Parameters.Add("?m_id", MySqlDbType.Int32);
            db.CommandParametersAdd(db.ParamPrefics + "m_id", db.GettingInt32(), mobitelId);
            //cmd.Parameters.Add("?val", MySqlDbType.Bit);
            db.CommandParametersAdd(db.ParamPrefics + "val", db.GettingBit(), valid);
            //cmd.Parameters.Add("?begin_time", MySqlDbType.DateTime);
            db.CommandParametersAdd(db.ParamPrefics + "begin_time", db.GettingDateTime(), beginTime);
            //cmd.Parameters.Add("?end_time", MySqlDbType.DateTime);
            db.CommandParametersAdd(db.ParamPrefics + "end_time", db.GettingDateTime(), endTime);
            //cmd.Parameters["?m_id"].Value = mobitelId;
            //cmd.Parameters["?val"].Value = valid;
            //cmd.Parameters["?begin_time"].Value = beginTime;
            //cmd.Parameters["?end_time"].Value = endTime;
            //return cmd;
            return db.GetCommand;
        } // CreateSelectCommand

        public static DateTime GetLastDataTime()
        {
            try
            {
                //using (ConnectMySQL cn = new ConnectMySQL() )
                using (var db = new DriverDb())
                {
                    db.ConnectDb();

                    string sql = TrackControlQuery.GpsDataProvider.SELECT_From_Unixtime;
                    return db.GetScalarValueNull<DateTime>(sql, DateTime.MinValue);
                }
            }
            catch
            {
                return DateTime.MinValue;
            }

            return DateTime.MinValue;
        }

        // GetLastDataTime

        public List<GpsData> GetValidDataForPeriod(int mobitelId, DateTime begin, DateTime end)
        {
            List<GpsData> result = new List<GpsData>();

            //using (MySqlCommand cmd = CreateSelectCommand(Mobitel_id, begin, end, true))
            var db = new DriverDb();
            db.ConnectDb();
            db.GetCommand = CreateSelectCommand(mobitelId, begin, end, true);

            if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                object CommandSqlType = db.GetCommand;
                SqlCommand commCmd = CommandSqlType as SqlCommand;
                if (commCmd == null)
                    return result;
                Cmd_Type = commCmd.CommandText;
            }

            {
                //using (ConnectMySQL cn = new ConnectMySQL())    
                {
                    //cmd.Connection = cn.Connect();
                    db.CommandConnection();
                    //using (MySqlDataReader reader = cmd.ExecuteReader())
                    //using( MySqlDataReader reader = db.CommandExecuteReader() )
                    db.SqlDataReader = db.CommandExecuteReader();
                    {
                        while (db.Read())
                        {
                            ReadRecordDataGps(result, db.SqlDataReader);
                        }
                    }
                    db.CloseDataReader();
                }
                db.CloseDbConnection();
            }
            TrackRepairService.CheckTrack(result);
            return result;
        }

        public static GpsData GetLastActiveDataGps(int mobitelId)
        {
            var dataGps = new GpsData();
            try
            {
                //using (ConnectMySQL cn = new ConnectMySQL())
                var db = new DriverDb();
                db.ConnectDb();
                {
                    Mobitel mobitel = VehicleProvider2.GetMobitel(mobitelId);
                    if (mobitel.Is64BitPackets)
                        dataGps = GetLastActiveDatagps64(mobitelId, db, dataGps);
                    else
                        dataGps = GetLastActiveDatagps(mobitelId, db, dataGps);
                }
                db.CloseDbConnection();
                return dataGps;
            }
            catch (Exception e)
            {
                XtraMessageBox.Show(e.Message + "\n" + e.StackTrace, "Error Gps Data Provider", MessageBoxButtons.OK);
                return dataGps;
            }
        }

        private static GpsData GetLastActiveDatagps(int mobitelId, DriverDb db, GpsData dataGps)
        {
            string sSql = string.Format(TrackControlQuery.GpsDataProvider.SelectFromMobitels,
                                        TrackControlQuery.SelDataGps, mobitelId);

            db.GetDataReader(sSql);

            if (db.Read())
            {
                //GetDataGpsRecord(dr, dataGps);
                GetDataGpsRecord(db.GetSqlDataReader(), dataGps);
            }
            else
            {
                //dr.Close();
                db.CloseDataReader();
                sSql = string.Format(TrackControlQuery.GpsDataProvider.SelectFromDatagps,
                                     TrackControlQuery.SelDataGps, mobitelId);

                //MySqlParameter[] parSql = new MySqlParameter[1];
                db.NewSqlParameterArray(1);
                //parSql[0] = new MySqlParameter("?TodayNow", DateTime.Now);
                db.NewSqlParameter(db.ParamPrefics + "TodayNow", DateTime.Now, 0);
                //dr = cn.GetDataReader(sSQL, parSql);
                db.GetDataReader(sSql, db.GetSqlParameterArray);
                //if (dr.Read())
                if (db.Read())
                    //GetDataGpsRecord(dr, dataGps);
                    GetDataGpsRecord(db.GetSqlDataReader(), dataGps);
                else
                    dataGps = null;
            }
            //dr.Close(); 
            db.CloseDataReader();
            return dataGps;
        }

        private static GpsData GetLastActiveDatagps64(int mobitelId, DriverDb db, GpsData dataGps)
        {
            string sSql = string.Format(TrackControlQuery.GpsDataProvider.SelectFromMobitels64,
                                        TrackControlQuery.SelDataGps64, mobitelId);

            db.GetDataReader(sSql);

            if (db.Read())
            {
                   dataGps = MobitelProvider.GetOneRecord64(db);
            }
            else
            {
                db.CloseDataReader();
                sSql = string.Format(TrackControlQuery.GpsDataProvider.SelectFromDatagps64,
                                     TrackControlQuery.SelDataGps64, mobitelId);

                db.NewSqlParameterArray(1);
                db.NewSqlParameter(db.ParamPrefics + "TodayNow", DateTime.Now, 0);
                db.GetDataReader(sSql, db.GetSqlParameterArray);
                if (db.Read())
                    dataGps = MobitelProvider.GetOneRecord64(db);
                else
                    dataGps = null;
            }
            //dr.Close(); 
            db.CloseDataReader();
            return dataGps;
        }

        public static List<GpsData> GetValidDataForPeriod(DateTime begin, DateTime end)
        {
            List<GpsData> gpsData = new List<GpsData>();
            //using (ConnectMySQL cn = new ConnectMySQL())
            var db = new DriverDb();
            db.ConnectDb();
            {
                string sSQL = string.Format(TrackControlQuery.GpsDataProvider.SelectFromDatagpsUnixTime,
                    TrackControlQuery.SelDataGps);

                //MySqlParameter[] parSql = new MySqlParameter[2];
                db.NewSqlParameterArray(2);
                //parSql[0] = new MySqlParameter("?TimeStart", begin);
                db.NewSqlParameter(db.ParamPrefics + "TimeStart", begin, 0);
                //parSql[1] = new MySqlParameter("?TimeEnd", end);
                db.NewSqlParameter(db.ParamPrefics + "TimeEnd", end, 1);
                //MySqlDataReader dr = cn.GetDataReader(sSQL, parSql);
                db.GetDataReader(sSQL, db.GetSqlParameterArray);
                //while (dr.Read())
                while (db.Read())
                {
                    GpsData data = new GpsData();
                    //GetDataGpsRecord(dr, data);
                    GetDataGpsRecord(db.GetSqlDataReader(), data);
                    gpsData.Add(data);
                }
                //dr.Close();  
                db.CloseDataReader();
            }
            db.CloseDbConnection();
            return gpsData;
        }

        public static List<GpsData> GetValidDataForPeriod(int mobitelId, Int64 firstPoint, Int64 lastPoint)
        {
            List<GpsData> gpsData = new List<GpsData>();
            var db = new DriverDb();
            db.ConnectDb();
            {
                string sSQL = string.Format(TrackControlQuery.GpsDataProvider.SelectFromDatagpsId,
                    TrackControlQuery.SelDataGps, mobitelId, firstPoint, lastPoint);

                db.GetDataReader(sSQL);
                while (db.Read())
                {
                    GpsData data = new GpsData();
                    GetDataGpsRecord(db.GetSqlDataReader(), data);
                    gpsData.Add(data);
                }
                db.CloseDataReader();
            }
            db.CloseDbConnection();
            TrackRepairService.CheckTrack(gpsData);
            return gpsData;
        }

        public static GpsData GetValidDataGpsOnTime(Vehicle vehicle, DateTime time)
        {
            var gpsDatas = GpsDataProvider.GetGpsDataAllProtocols(vehicle.MobitelId, time.Subtract(new TimeSpan(0, 30, 0)), time);
            if (gpsDatas.Count > 0)
                return gpsDatas[gpsDatas.Count - 1] ;
            return new GpsData();
        }

        public static List<GpsData> GetGpsData64FromDbMobitelId(int mobitelId, DateTime startTime, DateTime finishTime, bool isNotDrawDgps = false, float speedMinim = 0)
        {
            List<GpsData> gpsDatas = new List<GpsData>();
            using (var db = new DriverDb())
            {
                db.ConnectDb();
                db.NewSqlParameterArray(3);
                db.SetNewSqlParameter(db.ParamPrefics + "MobitelId", mobitelId);
                db.SetNewSqlParameter(db.ParamPrefics + "Begin", StaticMethods.ConvertToUnixTimestamp(startTime));
                db.SetNewSqlParameter(db.ParamPrefics + "End", StaticMethods.ConvertToUnixTimestamp(finishTime));
                db.GetDataReader(TrackControlQuery.DataviewTableAdapter.SelectDatagps64, db.GetSqlParameterArray);

                while (db.Read())
                {
                    GpsData gpsData = MobitelProvider.GetOneRecord64(db, isNotDrawDgps, speedMinim);
                    gpsDatas.Add(gpsData);
                }
                db.CloseDataReader();
                db.CloseDbConnection();
            }
            TrackRepairService.CheckTrack(gpsDatas);
            return gpsDatas;
        }

        public static List<GpsData> GetGpsData64FromDbMobitelIdAllPoints(int mobitelId, DateTime startTime, DateTime finishTime, bool isNotDrawDgps = false, float speedMinim = 0)
        {
            List<GpsData> gpsDatas = new List<GpsData>();
            using (var db = new DriverDb())
            {
                db.ConnectDb();
                db.NewSqlParameterArray(3);
                db.SetNewSqlParameter(db.ParamPrefics + "MobitelId", mobitelId);
                db.SetNewSqlParameter(db.ParamPrefics + "Begin", StaticMethods.ConvertToUnixTimestamp(startTime));
                db.SetNewSqlParameter(db.ParamPrefics + "End", StaticMethods.ConvertToUnixTimestamp(finishTime));
                db.GetDataReader(TrackControlQuery.DataviewTableAdapter.SelectDatagps64AllPoints, db.GetSqlParameterArray);

                while (db.Read())
                {
                    GpsData gpsData = MobitelProvider.GetOneRecord64(db, isNotDrawDgps, speedMinim);
                    gpsDatas.Add(gpsData);
                }
                db.CloseDataReader();
            }
            return gpsDatas;
        }

        public static List<GpsData> GetLast100GpsData64FromDbMobitelIdAllPoints(int mobitelId,int lastLogId, bool isNotDrawDgps = false, float speedMinim = 0)
        {
            var gpsDatas = new List<GpsData>();
            using (var db = new DriverDb())
            {
                db.ConnectDb();
                db.NewSqlParameterArray(3);
                db.SetNewSqlParameter(db.ParamPrefics + "MobitelId", mobitelId);
                db.SetNewSqlParameter(db.ParamPrefics + "LoginId", lastLogId);
                db.SetNewSqlParameter(db.ParamPrefics + "FUTime", DateTime.Now);
                db.GetDataReader(TrackControlQuery.DataviewTableAdapter.SelectLast100Datagps64AllPoints, db.GetSqlParameterArray);

                while (db.Read())
                {
                    GpsData gpsData = MobitelProvider.GetOneRecord64(db, isNotDrawDgps, speedMinim);
                    gpsDatas.Add(gpsData);
                }
                db.CloseDataReader();
            }
            return gpsDatas;
        }

        public static List<GpsData> GetGpsDataFromDbMobitelIdAllPoints(int mobitelId, DateTime startTime, DateTime finishTime, bool isNotDrawDgps = false, float speedMinim = 0)
        {
            List<GpsData> gpsDatas = new List<GpsData>();
            using (var db = new DriverDb())
            {
                db.ConnectDb();
                db.NewSqlParameterArray(3);
                db.SetNewSqlParameter(db.ParamPrefics + "MobitelId", mobitelId);
                db.SetNewSqlParameter(db.ParamPrefics + "Begin", StaticMethods.ConvertToUnixTimestamp(startTime));
                db.SetNewSqlParameter(db.ParamPrefics + "End", StaticMethods.ConvertToUnixTimestamp(finishTime));
                db.GetDataReader(TrackControlQuery.DataviewTableAdapter.SelectDatagpsAllPoints, db.GetSqlParameterArray);

                while (db.Read())
                {
                    GpsData dataGps = new GpsData();
                    GetDataGpsRecord(db.GetSqlDataReader(), dataGps,true);
                    gpsDatas.Add(dataGps);
                }
                db.CloseDataReader();
            }
            return gpsDatas;
        }

        public static List<GpsData> GetLast100GpsDataFromDbMobitelIdAllPoints(int mobitelId, int lastLogId)
        {
            var gpsDatas = new List<GpsData>();
            using (var db = new DriverDb())
            {
                db.ConnectDb();
                db.NewSqlParameterArray(3);
                db.SetNewSqlParameter(db.ParamPrefics + "MobitelId", mobitelId);
                db.SetNewSqlParameter(db.ParamPrefics + "LogId", lastLogId);
                db.SetNewSqlParameter(db.ParamPrefics + "FUTime", DateTime.Now);
                db.GetDataReader(TrackControlQuery.DataviewTableAdapter.SelectLast100DatagpsAllPoints, db.GetSqlParameterArray);

                while (db.Read())
                {
                    GpsData dataGps = new GpsData();
                    GetDataGpsRecord(db.GetSqlDataReader(), dataGps, true);
                    gpsDatas.Add(dataGps);
                }
                db.CloseDataReader();
            }
            return gpsDatas;
        }

        public static List<GpsData> GetGpsDataAllProtocols(int mobitelId, DateTime startTime, DateTime finishTime)
        {
            if (startTime >= finishTime)
            {
                return null;
            }
            List<GpsData> result; 
            Mobitel mobitel = VehicleProvider2.GetMobitel(mobitelId);
            if (mobitel.Is64BitPackets)
            {
                result= GetGpsData64FromDbMobitelId(mobitelId, startTime, finishTime);
            }
            else
            {
                var gpsProv = new GpsDataProvider();
                result= gpsProv.GetValidDataForPeriod(mobitelId, startTime, finishTime);
            }
            
            return result;
        }

        static bool _isglobaldata64packet = false;

        static public bool IsGlobal64Packet
        {
            get { return _isglobaldata64packet; }
        }

        public static IList<GpsData> GetGpsDataAllProtocolsAllPonts(int mobitelId, DateTime startTime, DateTime finishTime)
        {
            IList<GpsData> result = new List<GpsData>();

            try
            {
                if (startTime >= finishTime)
                {
                    throw new Exception("Start time must be less than the final.");
                }

                Mobitel mobitel = VehicleProvider2.GetMobitel(mobitelId);
                _isglobaldata64packet = mobitel.Is64BitPackets;

                if (mobitel.Is64BitPackets)
                {
                    result = GetGpsData64FromDbMobitelIdAllPoints(mobitelId, startTime, finishTime);
                }
                else
                {
                    result = GetGpsDataFromDbMobitelIdAllPoints(mobitelId, startTime, finishTime);
                }

                return result;
            }
            catch (Exception e)
            {
                return result;
            }
        }

        public static IList<GpsData> GetLast100GpsDataAllProtocolsAllPonts(int mobitelId, int lastLogId, bool is64BitPackets)
        {
            List<GpsData> result;
            if (is64BitPackets)
            {
                result = GetLast100GpsData64FromDbMobitelIdAllPoints(mobitelId, lastLogId);
            }
            else
            {
                result = GetLast100GpsDataFromDbMobitelIdAllPoints(mobitelId, lastLogId);
            }
            result.Reverse();
            return result;
        }

        public static void WritreSensorsData(int mobitelId, DateTime startTime, DateTime finishTime)
        {
            var gpoints = GetGpsDataAllProtocols(mobitelId, startTime, finishTime);
            if (gpoints != null && gpoints.Count > 0)
            {
                var sensors = SensorProvider.GetVehicleSensors(mobitelId);

                foreach (Sensor sensor in sensors)
                {
                    sensor.Algoritm = SensorProvider.GetSensorAlgoritm(sensor.Id);
                    switch (sensor.Algoritm)
                    {
                        case 7:
                            sensor.IdOutLink = "Fuel1";
                            break;
                        case 13:
                            sensor.IdOutLink = "Rfid1";
                            break;
                        case 14:
                            sensor.IdOutLink = "Rfid2";
                            break;
                        case 8:
                            sensor.IdOutLink = "Count1";
                            break;
                        case 22:
                            sensor.IdOutLink = "Param1";
                            break;
                        case 26:
                            sensor.IdOutLink = "Param2";
                            break;
                    }

                }
                MessageBox.Show("Write!");
                using (var db = new DriverDb())
                {
                    db.ConnectDb();
                    foreach (Sensor sensor in sensors)
                    {
                        foreach (GpsData gpsData in gpoints)
                        {
                            string value = sensor.GetValue(gpsData.Sensors).ToString();
                            value = value.Replace(",", ".");
                            string sql =
                                string.Format(
                                    "INSERT INTO datagpssensors (DataGps_ID, SensorId, Value,WebFieldName) VALUES ({0}, {1}, {2},'{3}')", gpsData.Id,
                                    sensor.Id, value, sensor.IdOutLink);
                            db.ExecuteNonQueryCommand(sql);
                            
                        }
                    }
                }
                MessageBox.Show("Finita!");

            }
        }
    }
}
