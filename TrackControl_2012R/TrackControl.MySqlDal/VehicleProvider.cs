using System;
using System.Data;
using System.Drawing;
using System.Collections.Generic;
using System.Security.Cryptography;
using MySql.Data.MySqlClient;
using TrackControl.General.DatabaseDriver;
using TrackControl.Vehicles;
using TrackControl.General;
using System.Collections;
using System.Windows.Forms;
using TrackControl.General.DAL;
using TrackControl.Vehicles.DAL;

namespace TrackControl.MySqlDal
{
    public sealed class VehicleProvider : IVehicleProvider
    {
        private const int DEFAULT_GROUP_ID = 1;
        private const int DEFAULT_SETTINGS_ID = 1;
        private const int DEFAULT_DRIVER_ID = 1;
        //DbCommon _dbCommon;
        private DriverDb _dbCommon;
        //public VehicleProvider(DbCommon dbCommon)
        public VehicleProvider(DriverDb dbCommon)
        {
            if (null == dbCommon)
                throw new ArgumentNullException("dbCommon");

            _dbCommon = dbCommon;
        }

        public VehicleProvider()
        {
            // to do
        }

        public List<Vehicle> GetAll()
        {
            List<Vehicle> vehicles = new List<Vehicle>();
            using (var db = new DriverDb())
            {
                db.ConnectDb();

                string sql = TrackControlQuery.VehicleProvider.SelectVehicleFromVehicle;

                db.GetDataReader(sql);
                while (db.Read())
                {
                    Vehicle vh = new Vehicle(db.GetInt32("Mobitel_id"));
                    vehicles.Add(vh);
                }
            }

            foreach (Vehicle vehic in vehicles)
            {
                vehic.VehicleComment = VehicleCommentProvider.GetComment(vehic.MobitelId);
            }

            return vehicles;
        }

        public List<Vehicle> GetAllOneQuery()
        {
            List<Vehicle> vehicles = new List<Vehicle>();
            Dictionary<int, VehiclesGroup> groups = new Dictionary<int, VehiclesGroup>();

            using (var db = new DriverDb())
            {
                db.ConnectDb();

                string sql = TrackControlQuery.VehicleProvider.SelectVehicleFromVehicleMobitelSettings;

                db.GetDataReader(sql);

                while (db.Read())
                {
                    DateTime lastInsertTime = DateTime.MinValue;
                    string login = "";
                    if (!db.IsDbNull(db.GetOrdinal("LastTimeGps")))
                        lastInsertTime = db.GetDateTime("LastTimeGps");
                    if (!db.IsDbNull(db.GetOrdinal("devIdShort")))
                        login = db.GetString("devIdShort");

                    Mobitel mobitel = new Mobitel(db.GetInt32("Mid"), login, "", lastInsertTime);
                    mobitel.Is64BitPackets = db.GetBoolean("Is64bitPackets");
                    mobitel.IsNotDrawDgps = db.GetBoolean("IsNotDrawDgps");

                    Vehicle vh = new Vehicle(mobitel, db.GetString("MakeCar"), db.GetString("CarModel"),
                        db.GetString("NumberPlate"),
                        0, 0, 0, "");
                    vh.Id = db.GetInt32("id");
                    if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                    {
                        vh.FuelWaysSet = db.IsDbNull(db.GetOrdinal("FuelWayLiter"))
                            ? 0.0
                            : Math.Round(db.GetFloat("FuelWayLiter"), 1);

                        vh.FuelMotorSet = db.IsDbNull(db.GetOrdinal("FuelMotorLiter"))
                            ? 0.0
                            : Math.Round(db.GetFloat("FuelMotorLiter"), 1);

                    }
                    else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                    {
                        vh.FuelWaysSet = db.IsDbNull(db.GetOrdinal("FuelWayLiter"))
                            ? 0.0
                            : Math.Round(db.GetDouble("FuelWayLiter"), 1);

                        vh.FuelMotorSet = db.IsDbNull(db.GetOrdinal("FuelMotorLiter"))
                            ? 0.0
                            : Math.Round(db.GetDouble("FuelMotorLiter"), 1);

                    }

                    vh.Identifier = db.IsDbNull(db.GetOrdinal("Identifier"))
                        ? 0
                        : db.GetInt32("Identifier");

                    if (db.IsDbNull(db.GetOrdinal("Team_id")))
                        SetVehicleGroup(groups, vh, 0, "���", "");
                    else
                        SetVehicleGroup(groups, vh, db.GetInt32("Team_id"), db.GetString("Tname"), db.GetString("Descr"));

                    VehicleSettings settings = new VehicleSettings();

                    if (!db.IsDbNull(db.GetOrdinal("Sid")))
                    {
                        if (db.GetInt32("Sid") > 0)
                            settings.Id = db.GetInt32("Sid");
                    }
                    if (!db.IsDbNull(db.GetOrdinal("TimeBreak")))
                        settings.TimeBreak = db.GetTimeSpan("TimeBreak");
                    if (!db.IsDbNull(db.GetOrdinal("TimeBreakMaxPermitted")))
                        settings.TimeBreakMaxPermitted = db.GetTimeSpan("TimeBreakMaxPermitted");
                    if (!db.IsDbNull(db.GetOrdinal("TimeBreakMaxPermitted")))
                        settings.TimeBreakMaxPermitted = db.GetTimeSpan("TimeBreakMaxPermitted");
                    if (!db.IsDbNull(db.GetOrdinal("speedMax")))
                        settings.SpeedMax = (float) db.GetDouble("speedMax");
                    if (!db.IsDbNull(db.GetOrdinal("AvgFuelRatePerHour")))
                        settings.AvgFuelRatePerHour = (float) db.GetDouble("AvgFuelRatePerHour");
                    if (!db.IsDbNull(db.GetOrdinal("FuelerMinFuelrate")))
                        settings.FuelerMinFuelRate = db.GetInt32("FuelerMinFuelrate");
                    vh.Settings = settings;
                    vehicles.Add(vh);
                }
            }

            foreach( Vehicle vehic in vehicles )
            {
                vehic.VehicleComment = VehicleCommentProvider.GetComment( vehic.MobitelId );
            }

            return vehicles;
        }

        private static void SetVehicleGroup(Dictionary<int, VehiclesGroup> groups, Vehicle vh, int groupCode,
            string groupName, string groupDisc)
        {
            if (groups.ContainsKey(groupCode))
            {
                vh.Group = groups[groupCode];
            }
            else
            {
                VehiclesGroup group = new VehiclesGroup(groupName, groupDisc);
                vh.Group = group;
                groups.Add(groupCode, group);
            }
        }

        public DataTable GetAllDataTable()
        {
            //using (ConnectMySQL cn=new ConnectMySQL())
            using (var db = new DriverDb())
            {
                db.ConnectDb();

                string sql = TrackControlQuery.VehicleProvider.SelectVehicleMobitel_id;

                //return cn.GetDataTable(sql); 
                return db.GetDataTable(sql);
            }
        }

        public DataTable GetAllDataTableLogicSensors()
        {
            //using (ConnectMySQL cn = new ConnectMySQL())
            using (var db = new DriverDb())
            {
                db.ConnectDb();

                string sql = TrackControlQuery.VehicleProvider.SelectVehicleMobitelIDLogic;
                //return cn.GetDataTable(sql);
                return db.GetDataTable(sql);
            }
        }

        public DataTable GetAllDataTableLogicSensorsAlgorithm(int idAlgoritm)
        {
            using (var db = new DriverDb())
            {
                db.ConnectDb();

                string sql = string.Format(TrackControlQuery.VehicleProvider.SelectVehicleMobitelIDLogicAlgorithm, idAlgoritm);
                return db.GetDataTable(sql);
            }
        }

        

        public DataTable GetAllDataTableSensors()
        {
            //using (ConnectMySQL cn = new ConnectMySQL())
            using (var db = new DriverDb())
            {
                db.ConnectDb();

                string sql = TrackControlQuery.VehicleProvider.SelectFromVehicleMobitelId;
                //return cn.GetDataTable(sql);
                return db.GetDataTable(sql);
            }
        }

        public DataTable GetListSensorsAlgoritm(int idAlgoritm)
        {
            using (var db = new DriverDb())
            {
                db.ConnectDb();

                string sql = string.Format(TrackControlQuery.VehicleProvider.SelectVehicleMobitelTid, idAlgoritm);

                return db.GetDataTable(sql);
            }
        }

        public void Save(Vehicle vehicle)
        {
            if (null == vehicle)
                throw new ArgumentNullException("vehicle");

            if (vehicle.IsNew)
            {
                insertVehicleInDB(vehicle);
            }
            else
            {
                updateVehicleInDB(vehicle);
                SaveMobitel(vehicle.Mobitel);
            }
        }

        private void insertVehicleInDB(Vehicle vehicle)
        {
            if (IsVehicleHasMobitel(vehicle.Mobitel.Id))
                return;
            //using (MySqlConnection connection = _dbCommon.GetConnection())
            var db = new DriverDb();
            db.SqlConnection = _dbCommon.GetConnection();
            //using (MySqlCommand command = new MySqlCommand(INSERT_ONE, connection))
            {
                db.NewSqlCommand(TrackControlQuery.VehicleProvider.InsertOne, db.SqlConnection);

                //adjustSaveCommand(command, vehicle);
                adjustSaveCommand(db, vehicle);
                //command.Parameters.Add("@MobitelId", MySqlDbType.Int32).Value = vehicle.Mobitel.Id;
                //db.CommandParametersAdd ("@MobitelId", db.GettingInt32 (), vehicle.Mobitel.Id);
                //command.Connection.Open();
                db.CommandConnectionOpen();
                //vehicle.Id = (int)(long)command.ExecuteScalar();
                string value = db.CommandExecuteScalar().ToString();
                vehicle.Id = Convert.ToInt32(value);
            }
            updateStyle(vehicle);
            db.SqlConnectionClose();
        }

        private void updateStyle(Vehicle vehicle)
        {
            //using (ConnectMySQL cnn = new ConnectMySQL() )
            var db = new DriverDb();
            db.ConnectDb();
            {
                int id = 0;
                string sql = String.Format(TrackControlQuery.VehicleProvider.SelectIdFromLines, vehicle.Mobitel.Id);

                //id = cnn.GetScalarValueNull<int>(sql,0) ;
                id = db.GetScalarValueNull<int>(sql, 0);

                if (id == 0)
                {
                    sql = String.Format(TrackControlQuery.VehicleProvider.InsertIntoLines, vehicle.Mobitel.Id,
                        vehicle.Style.ColorTrack.ToArgb());
                    //cnn.ExecuteNonQueryCommand(sql); 
                    db.ExecuteNonQueryCommand(sql);
                }
                else
                {
                    sql = String.Format(TrackControlQuery.VehicleProvider.UpdateLines, id,
                        vehicle.Style.ColorTrack.ToArgb());
                    //cnn.ExecuteNonQueryCommand(sql); 
                    db.ExecuteNonQueryCommand(sql);
                }

                db.CloseDbConnection();
            }
        }

        private void updateVehicleInDB(Vehicle vehicle)
        {
            try
            {
                var db = new DriverDb();
                //using(MySqlConnection connection = _dbCommon.GetConnection())
                db.SqlConnection = _dbCommon.GetConnection();
                //  using (MySqlCommand command = new MySqlCommand(UPDATE_ONE, (MySqlConnection)db.SqlConnection))
                //{

                db.NewSqlCommand(TrackControlQuery.VehicleProvider.UpdateOne, db.SqlConnection);

                //adjustSaveCommand(command, vehicle);
                adjustSaveCommand(db, vehicle);

                //command.Parameters.Add("@Id", MySqlDbType.Int32).Value = vehicle.Id;

                db.CommandParametersAdd("@Id", db.GettingInt32(), vehicle.Id);
                //command.Connection.Open();
                //command.ExecuteNonQuery();
                db.CommandConnectionOpen();
                db.CommandExecuteNonQuery();
                //}
                updateStyle(vehicle);
                db.SqlConnectionClose();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + "!");
            }
        }

        private static void adjustSaveCommand(DriverDb db, Vehicle vehicle)
        {
            db.CommandParametersAdd("@CarMaker", db.GettingString(), vehicle.CarMaker);
            db.CommandParametersAdd("@RegNumber", db.GettingString(), vehicle.RegNumber);
            db.CommandParametersAdd("@MobitelId", db.GettingInt32(), vehicle.Mobitel.Id);

            if (null != vehicle.Group && !vehicle.Group.IsRoot)
            {
                db.CommandParametersAdd("@GroupId", db.GettingInt32(), vehicle.Group.Id);
            }
            else
            {
                db.CommandParametersAdd("@GroupId", db.GettingInt32(), DBNull.Value);
            }

            db.CommandParametersAdd("@CarModel", db.GettingString(), vehicle.CarModel);

            db.CommandParametersAdd("@SettingsId", db.GettingInt32(),
                (vehicle.Settings.Id != ConstsGen.RECORD_MISSING) ? vehicle.Settings.Id : DEFAULT_SETTINGS_ID);
            db.CommandParametersAdd("@DriverId", db.GettingInt32(),
                (null != vehicle.Driver) ? vehicle.Driver.Id : DEFAULT_DRIVER_ID);

            if (vehicle.IdOutLink == null)
            {
                db.CommandParametersAdd("@OutLinkId", db.GettingVarChar(), DBNull.Value);
            }
            else
            {
                db.CommandParametersAdd("@OutLinkId", db.GettingVarChar(), vehicle.IdOutLink);
            }

            db.CommandParametersAdd("@FuelWayLiter", db.GettingDouble(),
                (null != vehicle.FuelWays) ? Convert.ToDouble(vehicle.FuelWays) : 0.0);
            db.CommandParametersAdd("@FuelMotorLiter", db.GettingDouble(),
                (null != vehicle.FuelMotor) ? Convert.ToDouble(vehicle.FuelMotor) : 0.0);
            db.CommandParametersAdd("@Identifier", db.GettingInt32(),
                (0 < vehicle.Identifier) ? Convert.ToInt32(vehicle.Identifier) : 0);

            if (vehicle.Category != null && vehicle.Category.Id > 0)
            {
                db.CommandParametersAdd("@Category_id", db.GettingVarChar(), vehicle.Category.Id);
            }
            else
            {
                db.CommandParametersAdd("@Category_id", db.GettingVarChar(), DBNull.Value);
            }

            if (vehicle.Category2 != null && vehicle.Category2.Id > 0)
            {
                db.CommandParametersAdd("@Category_id2", db.GettingVarChar(), vehicle.Category2.Id);
            }
            else
            {
                db.CommandParametersAdd("@Category_id2", db.GettingVarChar(), DBNull.Value);
            }

            if (vehicle.PcbVersion == null)
            {
                db.CommandParametersAdd("@PcbVersionId", db.GettingInt32(), -1);
            }
            else
            {
                db.CommandParametersAdd("@PcbVersionId", db.GettingInt32(), vehicle.PcbVersion.ID);
            }

            if (vehicle.Category3 != null && vehicle.Category3.Id > 0)
            {
                db.CommandParametersAdd("@Category_id3", db.GettingVarChar(), vehicle.Category3.Id);
            }
            else
            {
                db.CommandParametersAdd("@Category_id3", db.GettingVarChar(), DBNull.Value);
            }

            if (vehicle.Category4 != null && vehicle.Category4.Id > 0)
            {
                db.CommandParametersAdd("@Category_id4", db.GettingVarChar(), vehicle.Category4.Id);
            }
            else
            {
                db.CommandParametersAdd("@Category_id4", db.GettingVarChar(), DBNull.Value);
            }
        } // adjustSaveCommand

        private bool IsVehicleHasMobitel(int MobitelID)
        {
            //using (ConnectMySQL cnn = new ConnectMySQL())
            using (var db = new DriverDb())
            {
                db.ConnectDb();

                string sql = string.Format(TrackControlQuery.VehicleProvider.SelectFromVehicle,
                    TrackControlQuery.VehicleIdent, MobitelID);

                //MySqlDataReader dr = cnn.GetDataReader(sql);
                db.GetDataReader(sql);
                //if (dr.Read())
                if (db.Read())
                {
                    string mes = string.Format("�������� {0} ��� �������� ���������� {1}!", MobitelID, db.GetString(0)
                        /*dr.GetString(0)*/);
                    MessageBox.Show(mes, "������� ���������");
                    return true;
                }
                else
                    return false;
            }
        }

        private void SaveMobitel(Mobitel mobitel)
        {
            using (var db = new DriverDb())
            {
                db.ConnectDb();
                string sql = string.Format(TrackControlQuery.VehicleProvider.UpdateMobitel, db.ParamPrefics, mobitel.Id);
                db.NewSqlParameterArray(2);
                db.SetNewSqlParameter(db.ParamPrefics + "Is64bitPackets", mobitel.Is64BitPackets);
                db.SetNewSqlParameter(db.ParamPrefics + "IsNotDrawDgps", mobitel.IsNotDrawDgps);
                db.ExecuteNonQueryCommand(sql, db.GetSqlParameterArray);

                // save telefon number in telnambers table with controling
                string simNumber = mobitel.SimNumber;
                int mobitelId = mobitel.Id;
                sql = "SELECT ServiceSend_ID FROM mobitels WHERE Mobitel_ID = " + mobitelId;
                DataTable dtb = db.GetDataTable(sql);
                if (dtb.Rows.Count == 0)
                    throw new Exception("Record Mobitel_ID = " + mobitelId + " in table mobitels is absent.");

                if (dtb.Rows[0]["ServiceSend_ID"] == DBNull.Value)
                    throw new Exception("Record ServiceSend_ID in table mobitels Mobitel_ID = " + mobitelId + " is DBNull.");
        
                int service_send_id = Convert.ToInt32(dtb.Rows[0]["ServiceSend_ID"].ToString());
                sql = "SELECT telMobitel FROM servicesend WHERE ServiceSend_ID = " + service_send_id;
                DataTable dtbserv = db.GetDataTable(sql);
                if (dtbserv.Rows.Count == 0)
                    throw new Exception("Record ServiceSend_ID = " + service_send_id + " in table servicesend is absent.");

                if (dtbserv.Rows[0]["telMobitel"] == DBNull.Value)
                {
                    sql = "INSERT INTO telnumbers (Name, Descr, ID, Flags, TelNumber) VALUES ('rcs', 'mtel', 0, 0, '" + simNumber + "')";
                    int last_ins_id = db.ExecuteReturnLastInsert(sql);
                    sql = "UPDATE servicesend SET telMobitel = " + last_ins_id + " WHERE ServiceSend_ID = " + service_send_id;
                    db.ExecuteNonQueryCommand(sql);
                }
                else
                {
                    int telNumId = Convert.ToInt32(dtbserv.Rows[0]["telMobitel"].ToString());
                    sql = "SELECT * FROM telnumbers WHERE TelNumber_ID = " + telNumId;
                    DataTable dtbtel = db.GetDataTable(sql);
                    if (dtbtel.Rows.Count == 0)
                    {
                        sql = "INSERT INTO telnumbers (Name, Descr, ID, Flags, TelNumber) VALUES ('rcs', 'mtel', 0, 0, '" + simNumber + "')";
                        int last_ins_id = db.ExecuteReturnLastInsert(sql);
                        sql = "UPDATE servicesend SET telMobitel = " + last_ins_id + " WHERE ServiceSend_ID = " + service_send_id;
                        db.ExecuteNonQueryCommand(sql);
                    }
                    else
                    {
                        int telId = Convert.ToInt32(dtbtel.Rows[0]["TelNumber_ID"].ToString());
                        sql = "UPDATE telnumbers SET telNumber = '" + simNumber + "'WHERE TelNumber_ID = " + telId;
                        db.ExecuteNonQueryCommand(sql);   
                    }
                } 
            }
        }
    }
}
