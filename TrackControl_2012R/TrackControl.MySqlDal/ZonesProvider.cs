using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using ReportsOnPassengerTraffic;
using TrackControl.General;
using TrackControl.General.DatabaseDriver;
using TrackControl.Zones;
using DevExpress.XtraEditors;
using TrackControl.MySqlDal.Properties;
using TrackControl.General.DAL;

namespace TrackControl.MySqlDal
{
    public class ZonesProvider : IZonesProvider
    {
        //DbCommon _dbCommon;
        private DriverDb _dbCommon;
        //public ZonesProvider(DbCommon dbCommon)

        public ZonesProvider(DriverDb dbCommon)
        {
            _dbCommon = dbCommon;
        }

        public ZonesProvider()
        {
            // to do this
        }

        public void Save(IZone zone)
        {
            if (zone.Name.Trim().Length == 0) return;

            if (String.IsNullOrEmpty(zone.Name))
            {
                throw new ArgumentException("name can not be NULL or Empty");
                return;
            }

            var db = new DriverDb();
            db.NewSqlCommand();
            db.CommandType(CommandType.Text);

            SetCommandZoneParameters(zone, db);
            
            if (zone.Id < 0)
            {
                InsertZoneWithPoints(zone, db);
            }
            else
            {
                IZone zoneBeforUpdate = GetZone(zone.Id);

                if (zone.IsGeometryChanged)
                {
                    UpdatePoints(zone, db);
                }
                else
                {
                    UpdateZone(zone, db);
                }

                LogZoneChanges(zoneBeforUpdate, zone);
            }
        }

        private static void SetCommandZoneParameters(IZone zone, DriverDb db)
        {
            //command.Parameters.Add("@Title", MySqlDbType.VarChar).Value = zone.Name;
            db.CommandParametersAdd("@Title", db.GettingVarChar(), zone.Name);
            //command.Parameters.Add("@Description", db.GettingVarChar(), zone.Description);
            db.CommandParametersAdd("@Description", db.GettingVarChar(), zone.Description);
            //command.Parameters.Add("@Color", MySqlDbType.Int32).Value = zone.Style.Color.ToArgb();
            db.CommandParametersAdd("@Color", db.GettingInt32(), zone.Style.Color.ToArgb());
            //command.Parameters.Add("@Square", MySqlDbType.Double).Value = zone.AreaKm;
            db.CommandParametersAdd("@Square", db.GettingDouble(), zone.AreaKm);
            //command.Parameters.Add("@DateCreate", MySqlDbType.DateTime).Value = DateTime.Now;
            db.CommandParametersAdd("@DateCreate", db.GettingDateTime(), DateTime.Now);
            int groupId = zone.Group.IsRoot ? 1 : zone.Group.Id;
            //command.Parameters.Add("@ZonesGroupId", MySqlDbType.Int32).Value = groupId;
            db.CommandParametersAdd("@ZonesGroupId", db.GettingInt32(), groupId);
            //command.Parameters.Add("@OutLinkId", MySqlDbType.VarChar).Value = zone.IdOutLink;
            db.CommandParametersAdd("@OutLinkId", db.GettingVarChar(), zone.IdOutLink);

            if( zone.Category != null)
                db.CommandParametersAdd( "@Category_id", db.GettingInt32(), ((ZoneCategory)zone.Category).Id );
            else
                db.CommandParametersAdd( "@Category_id", db.GettingInt32(), -1 );

            if( zone.Category2 != null )
                db.CommandParametersAdd( "@Category_id2", db.GettingInt32(), ((ZoneCategory2)zone.Category2).Id );
            else
                db.CommandParametersAdd( "@Category_id2", db.GettingInt32(), -1 );
        }

        private void UpdateZone(IZone zone, DriverDb db)
        {
            string CMD3 = TrackControlQuery.ZonesProvider.UpdateZones;

            //command.CommandText = @"UPDATE `zones` SET `Name`=@Title, `Descr`=@Description, `ZoneColor`=@Color, `Square`=@Square, `ZonesGroupId`=@ZonesGroupId , `ZonesGroupId`=@ZonesGroupId,`OutLinkId`=@OutLinkId  WHERE `Zone_ID`=@ZoneId";
            db.CommandText(CMD3);
            //command.Parameters.Add("@ZoneId", MySqlDbType.Int32).Value = zone.Id;
            db.CommandParametersAdd("@ZoneId", db.GettingInt32(), zone.Id);
            //using (MySqlConnection conn = _dbCommon.GetConnection())
            db.SqlConnection = _dbCommon.GetConnection();
            {
                //command.Connection = conn;
                db.CommandSqlConnection();
                db.SqlConnectionOpen(); //conn.Open();
                //command.ExecuteNonQuery();
                db.CommandExecuteNonQuery();
            }
            db.SqlConnectionClose();
        }

        private void LogZoneChanges(IZone zoneBeforUpdate, IZone zone)
        {
            if (zoneBeforUpdate.Name != zone.Name)
            {
                UserLog.InsertLog(UserLogTypes.CheckZone,
                    string.Format(Resources.ChangeName, zoneBeforUpdate.Name, zone.Name), zone.Id);
            }
            if (zoneBeforUpdate.Group.Name != zone.Group.Name)
            {
                UserLog.InsertLog(UserLogTypes.CheckZone,
                    string.Format(Resources.ChangeGroupe, zoneBeforUpdate.Group.Name, zone.Group.Name), zone.Id);
            }
            if (Math.Round(zoneBeforUpdate.AreaGa, 0) != Math.Round(zone.AreaGa, 0))
            {
                UserLog.InsertLog(UserLogTypes.CheckZone,
                    string.Format(Resources.ChangeArea, Math.Round(zoneBeforUpdate.AreaGa, 2),
                        Math.Round(zone.AreaGa, 2)), zone.Id);
            }
        }

        private void UpdatePoints(IZone zone, DriverDb db)
        {
            string CMD = TrackControlQuery.ZonesProvider.DeleteFromPoints;
            string CMD2 = TrackControlQuery.ZonesProvider.InsertIntoPoints;

            db.CommandText(CMD);
            db.CommandParametersAdd("@ZoneId", db.GettingInt32(), zone.Id);
            db.SqlConnection = _dbCommon.GetConnection();

            {
                db.CommandSqlConnection();
                db.SqlConnectionOpen(); 
                db.SqlConnectBeginTransaction( IsolationLevel.ReadCommitted );
                db.SetTransaction = db.GetTransaction;

                try
                {
                    db.CommandExecuteNonQuery();
                    StringBuilder builder = new StringBuilder(CMD2);
                    
                    List<string> lstStr = new List<string>();
                    int iCnt = 0;
                    int iNum = 1000;

                    foreach (PointLatLng p in zone.Points)
                    {
                        if (iCnt < iNum)
                        {
                            builder.AppendFormat("({0},{1},{2}),", Math.Ceiling(p.Lat * 600000),
                                Math.Ceiling(p.Lng * 600000), zone.Id);
                            iCnt++;
                        }
                        else
                        {
                            builder.Length--;
                            lstStr.Add(builder.ToString());
                            iCnt = 0;
                            builder = new StringBuilder( CMD2 );
                        }
                    } // foreach

                    builder.Length--;
                    if(builder.Length > 1)
                        lstStr.Add( builder.ToString() );

                    for (int j = 0; j < lstStr.Count; j++)
                    {
                        db.CommandParametersClear();
                        db.CommandText(lstStr[j]);
                        db.CommandExecuteNonQuery();
                        db.TransactCommit();

                        if( j < (lstStr.Count - 1) )
                        {
                            db.SqlConnectBeginTransaction( IsolationLevel.ReadCommitted );
                            db.SetTransaction = db.GetTransaction;
                        }
                    } // for

                    zone.IsGeometryChanged = false;
                } // try
                catch (Exception e)
                {
                    db.TransactRollback();
                    XtraMessageBox.Show( e.Message + "\n" + e.StackTrace, "Insert into Data Base Error",
                        MessageBoxButtons.OK );
                } // catch
            } // using

            db.SqlConnectionClose();
        } // UpdatePoints

        private void InsertZoneWithPoints(IZone zone, DriverDb db)
        {
            string COMMAND_INSERT = TrackControlQuery.ZonesProvider.CommandSel;
            string CMM = TrackControlQuery.ZonesProvider.InsertPoints;

            try
            {
                db.CommandText( COMMAND_INSERT );
                db.SqlConnection = _dbCommon.GetConnection();
                db.CommandSqlConnection();
                db.SqlConnectionOpen();
                zone.Id = db.ExecuteReturnLastInsertWithTransition();

                //db.SqlConnectBeginTransaction(IsolationLevel.ReadCommitted);
                //db.SetTransaction = db.GetTransaction;
                                        
                //zone.Id = db.ExecuteReturnLastInsertWithTransition(); // ==============================
                //zone.Id = (int) db.ExecuteReturnLastInsert(COMMAND_INSERT, db.SqlConnection); //=======================================
                //db.CommandParametersClear();

                StringBuilder builder = new StringBuilder(CMM);

                List<string> lstStr = new List<string>();
                int iCnt = 0;
                int iNum = 1000;

                for (int i = 0; i < zone.Points.Length; i++ )
                {
                    PointLatLng p = zone.Points[i];
                    if (iCnt < iNum)
                    {
                        builder.AppendFormat("({0},{1},{2}),", Math.Ceiling(p.Lat * 600000),
                            Math.Ceiling(p.Lng * 600000), zone.Id);
                        iCnt++;
                    }
                    else
                    {
                        builder.Length--;
                        lstStr.Add(builder.ToString());
                        builder = new StringBuilder(CMM);
                        builder.AppendFormat("({0},{1},{2}),", Math.Ceiling(p.Lat * 600000),
                            Math.Ceiling(p.Lng * 600000), zone.Id);
                        iCnt = 0;
                    }
                } // foreach

                builder.Length--;
                if (builder.Length > 1)
                {
                    lstStr.Add(builder.ToString());
                }

                for( int j = 0; j < lstStr.Count; j++ )
                {
                    db.CommandParametersClear();
                    db.CommandText( lstStr[j] );
                    db.CommandExecuteNonQuery();

                    if( j < 1 )
                    {
                        db.SqlConnectBeginTransaction( IsolationLevel.ReadCommitted );
                        db.SetTransaction = db.GetTransaction;
                    }
                    else if (j == lstStr.Count -1)
                    {
                        db.TransactCommit();
                    }
                } // for

                zone.IsGeometryChanged = false;
                db.SqlConnectionClose();
            }
            catch (Exception ex)
            {
                db.TransactRollback(); 
                XtraMessageBox.Show(ex.Message + "\n" + ex.StackTrace, "Insert zone with points", MessageBoxButtons.OK);
            }
        } // InsertZoneWithPoints

        public void SaveGroupChanging(IEnumerable<IZone> zones)
        {
            int groupId = -1;
            StringBuilder builder = new StringBuilder();
            foreach (IZone zone in zones)
            {
                groupId = zone.Group.Id;
                if (!zone.IsNew)
                    builder.AppendFormat("{0},", zone.Id);
            }
            groupId = (groupId > 0) ? groupId : 1;
            if (builder.Length > 0)
            {
                builder.Length--;

                string query = String.Format(TrackControlQuery.ZonesProvider.UpdateZonesSetZonesGroupId, builder);

                //using (MySqlConnection conn = _dbCommon.GetConnection())
                var db = new DriverDb();
                db.SqlConnection = _dbCommon.GetConnection();
                {
                    //using (MySqlCommand command = new MySqlCommand(query, conn))
                    db.NewSqlCommand(query, db.SqlConnection);
                    {
                        //command.Parameters.Add("@GroupId", MySqlDbType.Int32).Value = groupId;
                        db.CommandParametersAdd("@GroupId", db.GettingInt32(), groupId);
                        //command.Connection.Open();
                        db.CommandConnectionOpen();
                        //command.ExecuteNonQuery();
                        db.CommandExecuteNonQuery();
                    }
                }
                db.SqlConnectionClose();
            }
        }

        public bool Delete(IZone zone)
        {
            if (IsExistLinkToAgroField(zone))
                return false;

            var db = new DriverDb();
            try
            {
                //using (MySqlCommand command = new MySqlCommand())

                db.NewSqlCommand();
                {
                    //command.CommandType = CommandType.Text;
                    db.CommandType(CommandType.Text);
                    //command.CommandText = @"DELETE FROM `zones` WHERE `Zone_ID`=@ZoneId";

                    db.CommandText(TrackControlQuery.ZonesProvider.DeleteZones);

                    //command.Parameters.Add("@ZoneId", MySqlDbType.Int32).Value = zone.Id;
                    db.CommandParametersAdd("@ZoneId", db.GettingInt32(), zone.Id);
                    //using (MySqlConnection conn = _dbCommon.GetConnection())
                    db.SqlConnection = _dbCommon.GetConnection();
                    {
                        //command.Connection = conn;
                        db.CommandSqlConnection();
                        db.SqlConnectionOpen(); //conn.Open();
                        //command.ExecuteNonQuery();
                        db.CommandExecuteNonQuery();
                    }
                    db.SqlConnectionClose();
                    UserLog.InsertLog(UserLogTypes.CheckZone, Resources.Delete, zone.Id);
                }
                return true;
            }
            catch
            {
                db.SqlConnectionClose();
                return false;
            }
        }

        public void Delete(IEnumerable<IZone> zones)
        {
            // TODO: ���������� ��� ���� ������ � ��
            foreach (IZone zone in zones)
            {
                Delete(zone);
            }
        }

        public bool IsExistLinkToAgroField(IZone zone)
        {

            //   using (MySqlCommand command = new MySqlCommand())
            var db = new DriverDb();
            db.NewSqlCommand();
            {
                //command.CommandType = CommandType.Text;
                db.CommandType(CommandType.Text);
                //command.CommandText = @"SELECT agro_field.Name FROM   agro_field WHERE   agro_field.Id_zone = @ZoneId";

                db.CommandText(TrackControlQuery.ZonesProvider.SelectAgrofildName);

                //command.Parameters.Add("@ZoneId", MySqlDbType.Int32).Value = zone.Id;
                db.CommandParametersAdd("@ZoneId", db.GettingInt32(), zone.Id);
                //using (MySqlConnection conn = _dbCommon.GetConnection())
                db.SqlConnection = _dbCommon.GetConnection();
                {
                    if (_dbCommon.IsTableExist("agro_field"))
                    {
                        //command.Connection = conn;
                        db.CommandSqlConnection();
                        db.SqlConnectionOpen(); //conn.Open();
                        //MySqlDataReader dr = command.ExecuteReader();
                        db.SqlDataReader = db.CommandExecuteReader();
                        if (db.Read())
                        {
                            XtraMessageBox.Show(string.Format("{0}: {1}", Resources.LinkedField, db.GetString("Name")),
                                Resources.DeleteBan);
                            db.SqlConnectionClose();
                            return true;
                        }
                        else
                        {
                            db.SqlConnectionClose();
                            return false;
                        }
                    }
                    else
                    {
                        db.SqlConnectionClose();
                        return false;
                    }
                }
            }
        }

        /// <summary>
        /// �������� ������ ������ �������� ReportPassZones 
        /// �� ������� report_pass_zones.
        /// </summary>
        /// <returns>������ ReportPassZones.</returns>
        public IList<ReportPassZone> GetRepportPassList()
        {
            List<ReportPassZone> result = new List<ReportPassZone>();

            var db = new DriverDb();
            db.NewSqlCommand();
            {
                db.CommandType( CommandType.Text );
                //command.CommandText = "SELECT * FROM report_pass_zones";
                db.CommandText( TrackControlQuery.ReportPassZonesProvider.SelectReportPassZones );
                db.SqlConnection = db.GetConnection();
                {
                    db.CommandSqlConnection();
                    db.SqlConnectionOpen();
                    db.SqlDataReader = db.CommandExecuteReader();
                    {
                        while( db.Read() )
                        {
                            ReportPassZone r = new ReportPassZone(
                                db.GetInt32( "ID" ),
                                db.GetInt32( "ZoneID" ) );
                            result.Add( r );
                        }
                    }

                    db.CloseDataReader();
                }

                db.SqlConnectionClose();
            }

            return result;
        }

        public IList<IZone> GetForGroup(ZonesGroup group)
        {
            List<IZone> zones = null;

            //int countid = 0;

            try
            {
                zones = new List<IZone>();

                //   using (MySqlCommand command = new MySqlCommand())
                var db = new DriverDb();
                db.NewSqlCommand();
                {
                    //command.CommandType = CommandType.Text;
                    db.CommandType(CommandType.Text);

                    string command1 = TrackControlQuery.ZonesProvider.SelectFromZones;
                    string command2 = TrackControlQuery.ZonesProvider.Select_From_Zones;

                    #region --   ����������� ������ �� ������� 'zones'   --

                    if (null != group)
                    {
                        //command.CommandText = @"SELECT * FROM `zones` WHERE `ZonesGroupId`=@ZonesGroupId";
                        db.CommandText(command1);

                        //command.Parameters.Add("@ZonesGroupId", MySqlDbType.Int32).Value = group.Id;
                        db.CommandParametersAdd("@ZonesGroupId", db.GettingInt32(), group.Id);
                    }
                    else
                    {
                        //command.CommandText = @"SELECT * FROM `zones`";
                        db.CommandText(command2);
                    }

                    //using (MySqlConnection connection = _dbCommon.GetConnection())
                    db.SqlConnection = _dbCommon.GetConnection();
                    {
                        //command.Connection = connection;
                        db.CommandSqlConnection();
                        db.SqlConnectionOpen(); // connection.Open();
                        //using (MySqlDataReader reader = command.ExecuteReader())
                        //using( MySqlDataReader reader = db.CommandExecuteReader() )
                        db.SqlDataReader = db.CommandExecuteReader();
                        {
                            while (db.Read())
                            {
                                if (UserBaseCurrent.Instance.IsZoneVisibleForRole(db.GetInt32("Zone_ID")))
                                {
                                    //countid = db.GetInt32("Zone_ID");
                                    if (String.IsNullOrEmpty(db.GetString("Name"))) continue;

                                    DateTime? dateChange = (db.IsDbNull(db.GetOrdinal("DateCreate")))
                                        ? null
                                        : (DateTime?) db.GetDateTime("DateCreate");

                                    string OutLinkId = (db.IsDbNull(db.GetOrdinal("OutLinkId")))
                                        ? ""
                                        : db.GetString("OutLinkId");

                                    int iCategoty = (db.IsDbNull(db.GetOrdinal("Category_id")))
                                        ? -1
                                        : db.GetInt32("Category_id");
                                    int iCategoty2 = (db.IsDbNull(db.GetOrdinal("Category_id2")))
                                        ? -1
                                        : db.GetInt32("Category_id2");

                                    ZoneCategory znCategory = null;
                                    ZoneCategory2 znCategory2 = null;

                                    if (iCategoty > 0)
                                        znCategory = new ZoneCategory(iCategoty);
                                    if (iCategoty2 > 0)
                                        znCategory2 = new ZoneCategory2(iCategoty2);

                                    IZone z = new Zone(db.GetString("Name"), db.GetString("Descr"),
                                        group, new ZonesStyle(), dateChange, OutLinkId, znCategory, znCategory2);

                                    z.Id = db.GetInt32("Zone_ID");
                                    z.AreaKm = db.GetDouble("Square");
                                    z.Style.Color = Color.FromArgb(db.GetInt32("ZoneColor"));

                                    //z.PassengerCalc = ReportPassZonesModel.GetPassagerZoneState(z.Id);

                                    zones.Add(z);
                                } // 
                            }
                        }db.CloseDataReader();
                    }
                    db.SqlConnectionClose();

                    #endregion

                    #region --   ����������� ����� ��� ������ ���� �� ������� 'points'   --

                    List<IZone> badZones = new List<IZone>();

                    db.CommandParametersClear();
                    db.CommandText(TrackControlQuery.ZonesProvider.Select_Points);
                    db.CommandParametersAdd("@ZoneId", db.GettingInt32());

                    zones.ForEach(delegate(IZone z)
                    {
                        db.CommandParametersValue(0, z.Id);
                        //using (MySqlConnection connection = _dbCommon.GetConnection())
                        db.SqlConnection = _dbCommon.GetConnection();
                        {
                            //command.Connection = connection;
                            db.CommandSqlConnection();
                            db.SqlConnectionOpen(); //connection.Open();
                            //using (MySqlDataReader reader = command.ExecuteReader())
                            //using( MySqlDataReader reader = db.CommandExecuteReader() )
                            db.SqlDataReader = db.CommandExecuteReader();
                            {
                                List<PointLatLng> points = new List<PointLatLng>();
                                double lat = 0.0;
                                double lng = 0.0;

                                while (db.Read())
                                {
                                    if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                                    {
                                        lat = db.GetDouble("Lat"); //GetInt32("Lat");
                                        lng = db.GetDouble("Lng"); //GetInt32("Lng");
                                    }
                                    else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                                    {
                                        lat = Math.Round(db.GetInt32("Latitude")/600000.0000000, 7);
                                        lng = Math.Round(db.GetInt32("Longitude")/600000.0000000, 7);
                                    }

                                    PointLatLng p = new PointLatLng(lat, lng);
                                    points.Add(p);
                                }

                                if (points.Count > 2)
                                {
                                    z.Points = points.ToArray();

                                    z.IsGeometryChanged = false;
                                }
                                else
                                    badZones.Add(z);
                            }
                            db.CloseDataReader();
                        }
                        db.SqlConnectionClose();
                    });

                    if (badZones.Count > 0)
                        badZones.ForEach(z => zones.Remove(z));

                    #endregion
                }

                return zones;
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Problem getting zones from database", MessageBoxButtons.OK);
                return null;
            }
        }

        public IZone GetZone(int zoneId)
        {
            //using (ConnectMySQL cn = new ConnectMySQL())
            using (var db = new DriverDb())
            {
                db.ConnectDb();

                string SQL = TrackControlQuery.ZonesProvider.SelectZonesId;

                //MySqlParameter[] pars = new MySqlParameter[1];
                db.NewSqlParameterArray(1);
                //pars[0] = new MySqlParameter("@ID", MySqlDbType.Int32);
                db.NewSqlParameter("@ID", db.GettingInt32(), 0);
                //pars[0].Value = zoneId;
                db.SetSqlParameterValue(zoneId, 0);

                //using (MySqlDataReader dread = cn.GetDataReader(SQL, pars))
                db.GetDataReader(SQL, db.GetSqlParameterArray);
                {
                    //if (dread.Read())
                    if (db.Read())
                    {
                        //string name = dread.IsDBNull(dread.GetOrdinal("Name")) ? "" : dread.GetString("Name");
                        string name = db.IsDbNull(db.GetOrdinal("Name")) ? "" : db.GetString("Name");
                        //string descr = dread.IsDBNull(dread.GetOrdinal("Descr")) ? "" : dread.GetString("Descr");
                        string descr = db.IsDbNull(db.GetOrdinal("Descr")) ? "" : db.GetString("Descr");
                        //string IdOutLink = dread.IsDBNull(dread.GetOrdinal("OutLinkId")) ? "" : dread.GetString("OutLinkId");
                        string IdOutLink = db.IsDbNull(db.GetOrdinal("OutLinkId")) ? "" : db.GetString("OutLinkId");
                        //Zone zone = new Zone(name, descr, GetZoneGroupe(dread.GetInt32("ZonesGroupId")), new ZonesStyle(),dread.IsDBNull(dread.GetOrdinal("DateCreate"))? DateTime.MinValue : dread.GetDateTime("DateCreate"), IdOutLink);

                        //int ordinal = db.GetOrdinal( "ZonesGroupId" );
                        long zoneGroup = db.GetInt64("ZonesGroupId");

                        int ordinal = db.GetOrdinal("DateCreate");
                        DateTime tmDt = db.GetDateTime(ordinal);

                        int category_index = db.IsDbNull(db.GetOrdinal("Category_id")) ? -1 : db.GetInt32("Category_id");
                        int category2_index = db.IsDbNull( db.GetOrdinal( "Category_id2" ) ) ? -1 : db.GetInt32( "Category_id2" );

                        ZoneCategory znCategory = null;
                        if(category_index > 0)
                            znCategory = new ZoneCategory(category_index);

                        ZoneCategory2 znCategory2 = null;
                        if(category2_index > 0)
                            znCategory2 = new ZoneCategory2(category2_index);

                        Zone zone = new Zone(name, descr, GetZoneGroupe((int) zoneGroup), new ZonesStyle(),
                            db.IsDbNull(db.GetOrdinal("DateCreate")) ? DateTime.MinValue : tmDt,
                            IdOutLink, znCategory, znCategory2);

                        zone.Id = zoneId;
                        GetPoints(zone);
                        db.CloseDataReader();
                        return zone;
                    }
                    else
                    {
                        db.CloseDataReader();
                        return null;
                    }
                }
            }
        }

        public ZonesGroup GetZoneGroupe(int zoneGroupeId)
        {
            //using (ConnectMySQL cn = new ConnectMySQL())
            using (var db = new DriverDb())
            {
                db.ConnectDb();

                string SQL = TrackControlQuery.ZonesProvider.SelectZonesGroup;

                //MySqlParameter[] pars = new MySqlParameter[1];
                db.NewSqlParameterArray(1);
                //pars[0] = new MySqlParameter("@ID", MySqlDbType.Int32);
                db.NewSqlParameter("@ID", db.GettingInt32(), 0);
                //pars[0].Value = zoneGroupeId;
                db.SetSqlParameterValue(zoneGroupeId, 0);

                //using (MySqlDataReader dread = cn.GetDataReader(SQL, pars))
                db.GetDataReader(SQL, db.GetSqlParameterArray);
                {
                    //if (dread.Read())
                    if (db.Read())
                    {
                        //string _name = dread.IsDBNull(dread.GetOrdinal("Title")) ? "" : dread.GetString("Title");
                        string _name = db.IsDbNull(db.GetOrdinal("Title")) ? "" : db.GetString("Title");
                        //string _descr = dread.IsDBNull(dread.GetOrdinal("Description")) ? "" : dread.GetString("Description");
                        string _descr = db.IsDbNull(db.GetOrdinal("Description")) ? "" : db.GetString("Description");
                        ZonesGroup zgroupe = new ZonesGroup(_name, _descr);
                        zgroupe.Id = zoneGroupeId;
                        db.CloseDataReader();
                        return zgroupe;
                    }
                    else
                    {
                        db.CloseDataReader();
                        return new ZonesGroup("", "");
                    }
                }
            }
        }

        public bool GetPoints(IZone z)
        {
            //using (ConnectMySQL cn = new ConnectMySQL())
            using (var db = new DriverDb())
            {
                db.ConnectDb();

                //MySqlParameter[] pars = new MySqlParameter[1];
                db.NewSqlParameterArray(1);
                //pars[0] = new MySqlParameter("@ZoneId", MySqlDbType.Int32);
                db.NewSqlParameter("@ZoneId", db.GettingInt32(), 0);
                //pars[0].Value = z.Id;
                db.SetSqlParameterValue(z.Id, 0);
                //using (MySqlDataReader dread = cn.GetDataReader(SELECT_POINTS, pars))
                db.GetDataReader(TrackControlQuery.ZonesProvider.Select_Points, db.GetSqlParameterArray);
                {
                    List<PointLatLng> points = new List<PointLatLng>();
                    //while (dread.Read())
                    double lat = 0.0;
                    double lng = 0.0;

                    while (db.Read())
                    {
                        //PointLatLng p = new PointLatLng(dread.GetDouble("Lat"), dread.GetDouble("Lng"));

                        if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                        {
                            lat = db.GetDouble("Lat"); //GetInt32("Lat");
                            lng = db.GetDouble("Lng"); //GetInt32("Lng");
                        }
                        else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                        {
                            lat = Math.Round(db.GetInt32("Latitude") / 600000.000000, 7);
                            lng = Math.Round(db.GetInt32("Longitude") / 600000.000000, 7);
                        }

                        PointLatLng p = new PointLatLng(lat, lng);
                        points.Add(p);
                    }

                    if (points.Count > 2)
                    {
                        z.Points = points.ToArray();
                        z.IsGeometryChanged = false;
                        db.CloseDataReader();
                        return true;
                    }

                    db.CloseDataReader();
                    return false;
                }
            }
        }

        public DataTable GetAllDataTable()
        {
            //using (ConnectMySQL cn = new ConnectMySQL())
            using (var db = new DriverDb())
            {
                db.ConnectDb();

                string sql = TrackControlQuery.ZonesProvider.SelectZonesListWithGroups;
                //return cn.GetDataTable(sql);
                return db.GetDataTable(sql);
            }
        }

        public List<Zone> GetAll()
        {
            List<Zone> zones = new List<Zone>();
            using (var db = new DriverDb())
            {db.ConnectDb();

                string sql = TrackControlQuery.ZonesProvider.SelectZonesListWithGroups;

                db.GetDataReader(sql);
                while (db.Read())
                {
                    int category_index = db.IsDbNull( db.GetOrdinal( "Category_id" ) ) ? -1 : db.GetInt32( "Category_id" );
                    int category2_index = db.IsDbNull( db.GetOrdinal( "Category_id2" ) ) ? -1 : db.GetInt32( "Category_id2" );

                    ZoneCategory znCategory = null;
                    if( category_index > 0 )
                        znCategory = new ZoneCategory( category_index );

                    ZoneCategory2 znCategory2 = null;
                    if( category2_index > 0 )
                        znCategory2 = new ZoneCategory2( category2_index );

                    Zone zone = new Zone(db.GetString("Name"), "", new ZonesGroup(db.GetString("Title"), ""),
                        new ZonesStyle(Color.FromArgb(db.GetInt32("ZoneColor"))), null, "", znCategory, znCategory2);

                    zone.Id = db.GetInt32("Zone_ID");
                    zone.AreaKm = db.GetDouble("Square");
                    zones.Add(zone);
                }
            }
            return zones;
        }
    }
}
