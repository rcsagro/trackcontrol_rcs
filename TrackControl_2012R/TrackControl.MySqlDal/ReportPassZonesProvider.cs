using System.Collections.Generic;
using System.Data;
using MySql.Data.MySqlClient;
using ReportsOnPassengerTraffic;
using TrackControl.General.DatabaseDriver;
using TrackControl.General.DAL;

namespace TrackControl.MySqlDal
{
    /// <summary>
    /// ������ � ������ �������� ������� �� ���������������
    /// </summary>
    public class ReportPassZonesProvider : IReportPassZonesProvider
    {
        //DbCommon _dbCommon;
        private DriverDb _dbCommon;
        //public ReportPassZonesProvider(DbCommon dbCommon)
        public ReportPassZonesProvider (DriverDb dbCommon)
        {
            _dbCommon = dbCommon;
        }

        /// <summary>
        /// �������� ������ ������ �������� ReportPassZones 
        /// �� ������� report_pass_zones.
        /// </summary>
        /// <returns>������ ReportPassZones.</returns>
        public IList<ReportPassZone> GetAll ()
        {
            List<ReportPassZone> result = new List<ReportPassZone> ();

            DriverDb db = new DriverDb ();
            db.NewSqlCommand ();
            {
                db.CommandType (CommandType.Text);
                //command.CommandText = "SELECT * FROM report_pass_zones";
                db.CommandText (TrackControlQuery.ReportPassZonesProvider.SelectReportPassZones);
                db.SqlConnection = _dbCommon.GetConnection ();
                {
                    db.CommandSqlConnection ();
                    db.SqlConnectionOpen ();
                    db.SqlDataReader = db.CommandExecuteReader ();
                    {
                        while (db.Read()) {
                            ReportPassZone r = new ReportPassZone (
                                db.GetInt32 ("ID"),
                                db.GetInt32 ("ZoneID"));
                            result.Add (r);
                        }
                    }

                    db.CloseDataReader ();
                }
                db.SqlConnectionClose ();
            }

            return result;
        }

        /// <summary>
        /// ������� ����� ������ � ������� report_pass_zones.
        /// </summary>
        /// <param name="entity">������ ReportPassZones.</param>
        /// <returns>True - ���� ���������� ��������� �������.</returns>
        public bool Save (ReportPassZone entity)
        {
            //using (MySqlCommand command = new MySqlCommand())
            DriverDb db = new DriverDb ();
            db.NewSqlCommand ();
            {
                //command.CommandType = CommandType.Text;
                db.CommandType (CommandType.Text);
                //command.UpdatedRowSource = UpdateRowSource.FirstReturnedRecord;
                db.CommandUpdatedRowSource (UpdateRowSource.FirstReturnedRecord);
                //command.CommandText =
                //  "INSERT INTO report_pass_zones (ZoneID) VALUES (@ZoneID); " +
                //  "SELECT ID FROM report_pass_zones WHERE ID = LAST_INSERT_ID()";
               
                db.CommandText (TrackControlQuery.ReportPassZonesProvider.InsertIntoReportPassZones);
             
                //command.Parameters.Add("@ZoneID", MySqlDbType.Int32).Value = entity.ZoneID;
                db.CommandParametersAdd ("@ZoneID", db.GettingInt32 (), entity.ZoneID);
                //using (MySqlConnection conn = _dbCommon.GetConnection())
                db.SqlConnection = _dbCommon.GetConnection ();
                {
                    //command.Connection = conn;
                    db.CommandSqlConnection ();
                    db.SqlConnectionOpen (); //conn.Open();
                    //int? newId = (int?)command.ExecuteScalar();
                    int? newId = (int?)db.CommandExecuteScalar ();
                    if (newId.HasValue) {
                        entity.Id = newId.Value;
                        db.SqlConnectionClose ();
                        return true;
                    } else {
                        db.SqlConnectionClose ();
                        return false;
                    }
                }
                //db.SqlConnectionClose ();
            }
        }

        /// <summary>
        /// ������� ������ �� ������� report_pass_zones.
        /// </summary>
        /// <param name="entity">������ ReportPassZones.</param>
        /// <returns>True - ���� �������� ��������� �������.</returns>
        public bool Delete (ReportPassZone entity)
        {
            //using (MySqlCommand command = new MySqlCommand())
            DriverDb db = new DriverDb ();
            db.NewSqlCommand ();
            {
                //command.CommandType = CommandType.Text;
                db.CommandType (CommandType.Text);
                //command.CommandText = "DELETE FROM report_pass_zones WHERE ID = @ID";
                db.CommandText (TrackControlQuery.ReportPassZonesProvider.DeleteFromReportPassZones);
                //command.Parameters.Add("@ID", MySqlDbType.Int32).Value = entity.Id;
                db.CommandParametersAdd ("@ID", db.GettingInt32 (), entity.Id);
                //using (MySqlConnection conn = _dbCommon.GetConnection())
                db.SqlConnection = _dbCommon.GetConnection ();
                {
                    //command.Connection = conn;
                    db.CommandSqlConnection ();
                    db.SqlConnectionOpen (); //conn.Open();
                    //return command.ExecuteNonQuery() > 0;
                    return db.CommandExecuteNonQuery () > 0;
                }
            }
        }
    }
}
