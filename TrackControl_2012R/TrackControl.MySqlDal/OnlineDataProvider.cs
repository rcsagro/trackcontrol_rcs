using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using TrackControl.General;
using TrackControl.General.DAL;
using TrackControl.General.DatabaseDriver;
using TrackControl.Online;
using TrackControl.Statistics;
using TrackControl.Vehicles;

namespace TrackControl.MySqlDal
{
    public class OnlineDataProvider : IOnlineDataProvider
    {
        //DbCommon _dbCommon;
        private DriverDb _dbCommon;
        //public OnlineDataProvider(DbCommon dbCommon)
        public OnlineDataProvider(DriverDb dbCommon)
        {
            _dbCommon = dbCommon;
        }

        public OnlineDataProvider()
        {
            // to do
        }

        public IList<GpsData> GetLastData(OnlineVehicle ov)
        {
            return GetLastDataMobitelAllProtocols(ov.MobitelId, ov.LastLogId, ov.Mobitel.Is64BitPackets, ov.Mobitel.IsNotDrawDgps);
        }

        public IList<GpsData> GetLastDataMobitelAllProtocols(int idMobitel, int idLastLog, bool is64BitPackets = false, bool isNotDrawDgps = true)
        {
            var result = new List<GpsData>();
            var db = new DriverDb();

            if (DriverDb.TypeDataBaseForUsing < 0)
                db.AnalizeTypeDB();

            //Mobitel mobitel = VehicleProvider2.GetMobitel(idMobitel);
            string script = "";

            if (is64BitPackets)
                script = String.Format(TrackControlQuery.OnlineDataProvider.GetOnlineDataMobitel64, idMobitel, idLastLog);
            else
                script = String.Format( TrackControlQuery.OnlineDataProvider.GetOnlineDataMobitel, idMobitel, idLastLog);

            using (db)
            {
                db.ConnectDb();
                db.GetDataReader(script);
                SetRecordFromReaderToList(result, db, idMobitel, is64BitPackets, !isNotDrawDgps);
                db.CloseDataReader();
                db.SqlConnectionClose();
            }

            TrackRepairService.CheckTrack(result);
            return result;
        }

        public GpsData GetMostRecentDataMobitel(int idMobitel)
        {
            string script = "";

            GpsData result = null;
            // using (MySqlCommand command = new MySqlCommand(script))
            var db = new DriverDb();
            if (DriverDb.TypeDataBaseForUsing < 0) db.AnalizeTypeDB();
            Mobitel mobitel = VehicleProvider2.GetMobitel(idMobitel);
            if (mobitel.Is64BitPackets)
                script = String.Format(TrackControlQuery.OnlineDataProvider.GetOnlineDataMobitelLast64, idMobitel);
            else
                script = String.Format(TrackControlQuery.OnlineDataProvider.GetOnlineDataMobitelLast, idMobitel);

            using (db)
            {
                db.ConnectDb();

                // ������� setting_id �������� mobitel_id
                db.GetDataReader("select setting_id from vehicle where Mobitel_id = " + idMobitel);
                int settingId = -1;
                if (db.Read())
                {
                    settingId = db.GetInt32("setting_id");
                }
                db.CloseDataReader(); 
                // �������� speedMin �������� setting_id 
                db.GetDataReader("select speedMin from setting where id = " + settingId);
                float minimSpeed = (float)-1.0;
                if( db.Read() )
                {
                    minimSpeed = (float)db.GetDouble( "speedMin" );
                }
                db.CloseDataReader(); 
                db.GetDataReader(script);
                if (db.Read())
                {
                    if (mobitel.Is64BitPackets)
                        result = MobitelProvider.GetOneRecord64(db, !mobitel.IsNotDrawDgps, minimSpeed,false);
                    else
                        result = GetOneRecord(db);
                }
                db.CloseDataReader();
                db.SqlConnectionClose();
            }
            return result;
        }

        private static void SetRecordFromReaderToList(List<GpsData> result, DriverDb db, int idMobitel, bool is64BitPackets = false, 
            bool isDrawDgps = true)
        {
            float minimSpeed = (float) -1.0;

            if( is64BitPackets )
            {
                DriverDb dbs = new DriverDb();
                dbs.ConnectDb();
                // ������� setting_id �������� mobitel_id
                dbs.GetDataReader( "select setting_id from vehicle where Mobitel_id = " + idMobitel );
                int settingId = -1;
                if( dbs.Read() )
                {
                    settingId = dbs.GetInt32( "setting_id" );
                }

                dbs.CloseDataReader();

                // �������� speedMin �������� setting_id 
                dbs.GetDataReader( "select speedMin from setting where id = " + settingId );
                minimSpeed = ( float )-1.0;
                if( dbs.Read() )
                {
                    minimSpeed = ( float )dbs.GetDouble( "speedMin" );
                }

                dbs.CloseDataReader();
                dbs.CloseDbConnection();
            } // if

            while (db.Read())
            {
                if (is64BitPackets)
                {
                    GpsData data = MobitelProvider.GetOneRecord64(db, isDrawDgps, minimSpeed,false);
                    result.Add(data);
                }
                else
                {
                    GpsData data = GetOneRecord(db);
                    result.Add(data);
                }
            }
        } // SetRecordFromReaderToList

        private static GpsData GetOneRecord(DriverDb db)
        {
            GpsData data = new GpsData();

            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                data.Id = db.GetInt64("DataGps_ID");
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                data.Id = db.GetInt32("DataGps_ID");
            }

            data.LogId = db.GetInt32("LogID");
            data.Time = db.GetDateTime("Time");

            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                data.LatLng = new PointLatLng(db.GetDouble("Lat"), db.GetDouble("Lon"));
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                double lat = db.GetInt32("Lat") / 600000.0;
                double lng = db.GetInt32("Lon") / 600000.0;
                data.LatLng = new PointLatLng(lat, lng);
            }
            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                data.Speed = db.GetFloat("Speed");
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                double speed = db.GetInt16("Speed")*1.852;
                data.Speed = (float) speed;
            }
            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                data.Valid = db.GetBoolean("Valid");
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                int vld = db.GetInt16("Valid");

                if (vld == 0)
                    data.Valid = false;
                else
                    data.Valid = true;
            }
            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                data.Sensors = BitConverter.GetBytes(db.GetUInt64("Sensor"));
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                ulong sensors = 0;

                ulong s8 = db.GetTinyInt( "Sensor8" );
                ulong s7 = db.GetTinyInt( "Sensor7" );
                ulong s6 = db.GetTinyInt( "Sensor6" );
                ulong s5 = db.GetTinyInt( "Sensor5" );
                ulong s4 = db.GetTinyInt( "Sensor4" );
                ulong s3 = db.GetTinyInt( "Sensor3" );
                ulong s2 = db.GetTinyInt( "Sensor2" );
                ulong s1 = db.GetTinyInt( "Sensor1" );

                sensors = ( s8 + ( s7 << 8 ) + ( s6 << 16 ) + ( s5 << 24 ) + ( s4 << 32 ) + ( s3 << 40 ) + ( s2 << 48 ) + ( s1 << 56 ) );

                data.Sensors = BitConverter.GetBytes(sensors);
            }
            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
            {
                data.Events = db.GetUInt32("Events");
            }
            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
            {
                data.Events = (uint) db.GetInt64("Events");
            }
            data.Mobitel = db.GetInt32("Mobitel_ID");
            return data;
        }

        public IList<GpsData> GetLastData(Int64 LastGPSId)
        {
            var mobitelsOnline = new Dictionary<int, List<GpsData>>();
            List<GpsData> g_dates = new List<GpsData>();
            try
            {
                //using (ConnectMySQL cn = new ConnectMySQL())
                var db = new DriverDb();
                db.ConnectDb();
                {
                    string SQL = String.Format(TrackControlQuery.OnlineDataProvider.GetOnlineData, LastGPSId);

                    //�������� ������� ���������� ������� ����� 2030 � �.�.  

                    //MySqlParameter[] pars = new MySqlParameter[1];
                    db.NewSqlParameterArray(1);
                    //pars[0] = new MySqlParameter("?FUTime", DateTime.Now);
                    db.NewSqlParameter(db.ParamPrefics + "FUTime", DateTime.Now, 0);
                    //using (MySqlDataReader dread = cn.GetDataReader(SQL,pars))
                    db.GetDataReader(SQL, db.GetSqlParameterArray);
                    {
                        //while (dread.Read())
                        while (db.Read())
                        {
                            GpsData data = new GpsData();
                            //data.Id = dread.GetInt64("DataGps_ID");

                            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                            {
                                data.Id = db.GetInt64("DataGps_ID");
                            }
                            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                            {
                                data.Id = (long) db.GetInt32("DataGps_ID");
                            }

                            //data.LogID = dread.GetInt32("LogID");
                            data.LogId = db.GetInt32("LogID");
                            //data.Time = dread.GetDateTime("Time");
                            data.Time = db.GetDateTime("Time");
                            //data.LatLng = new PointLatLng(dread.GetDouble("Lat"), dread.GetDouble("Lng"));

                            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                            {
                                data.LatLng = new PointLatLng(db.GetDouble("Lat"), db.GetDouble("Lng"));
                            }
                            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                            {
                                double lat = db.GetInt32("Latitude") / 600000.0;
                                double lng = db.GetInt32("Longitude") / 600000.0;

                                data.LatLng = new PointLatLng(lat, lng);
                            }

                            //data.Speed = dread.GetFloat("Speed");
                            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                            {
                                data.Speed = db.GetFloat("Speed");
                            }
                            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                            {
                                data.Speed = (float) (db.GetInt16("Speed")*1.852);
                            }
                            //data.Valid = dread.GetBoolean("Valid");
                            data.Valid = db.GetBoolean("Valid");
                            //data.Sensors = dread.GetUInt64("Sensor");
                            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                            {
                                data.Sensors = BitConverter.GetBytes(db.GetUInt64("Sensor"));
                            }
                            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                            {
                                ulong sensors = 0;

                                ulong s8 = db.GetTinyInt( "Sensor8" );
                                ulong s7 = db.GetTinyInt( "Sensor7" );
                                ulong s6 = db.GetTinyInt( "Sensor6" );
                                ulong s5 = db.GetTinyInt( "Sensor5" );
                                ulong s4 = db.GetTinyInt( "Sensor4" );
                                ulong s3 = db.GetTinyInt( "Sensor3" );
                                ulong s2 = db.GetTinyInt( "Sensor2" );
                                ulong s1 = db.GetTinyInt( "Sensor1" );

                                sensors = ( s8 + ( s7 << 8 ) + ( s6 << 16 ) + ( s5 << 24 ) + ( s4 << 32 ) + ( s3 << 40 ) + ( s2 << 48 ) + ( s1 << 56 ) );

                                data.Sensors = BitConverter.GetBytes(sensors);
                            }

                            //data.Events = dread.GetUInt32("Events");
                            if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                            {
                                data.Events = db.GetUInt32("Events");
                            }
                            else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                            {
                                data.Events = (uint) db.GetInt64("Events");
                            }
                            //data.Mobitel = dread.GetInt32("Mobitel_ID");
                            data.Mobitel = db.GetInt32("Mobitel_ID");
                            if (!mobitelsOnline.ContainsKey(db.GetInt32("Mobitel_ID"))) mobitelsOnline.Add(db.GetInt32("Mobitel_ID"),new List<GpsData>());
                            mobitelsOnline[db.GetInt32("Mobitel_ID")].Add(data);
                            //g_dates.Add(data);
                        } // while
                    }
                    db.CloseDataReader();
                }
                db.CloseDbConnection();
                foreach (var pair in mobitelsOnline)
                {
                    TrackRepairService.CheckTrack(pair.Value);
                    g_dates.AddRange(pair.Value);
                }
                return g_dates;
            }
            catch (Exception e)
            {
                XtraMessageBox.Show(e.Message + "\n" + e.StackTrace, "Error Notice", MessageBoxButtons.OK);
                return g_dates;
            }
        }



    }
}
