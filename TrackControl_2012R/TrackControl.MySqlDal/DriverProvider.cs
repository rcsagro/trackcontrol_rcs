using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using MySql.Data.MySqlClient;
using TrackControl.General;
using TrackControl.General.DAL;
using TrackControl.General.DatabaseDriver;
using TrackControl.MySqlDal.Properties;
using TrackControl.Vehicles;
using DevExpress.XtraEditors;

namespace TrackControl.MySqlDal
{
    public class DriverProvider : IDriverProvider
    {
        //DbCommon _dbCommon;
        private DriverDb _dbCommon;

        //public DriverProvider(DbCommon dbCommon)
        public DriverProvider(DriverDb dbCommon)
        {
            if (null == dbCommon)
                throw new ArgumentNullException("dbCommon");

            _dbCommon = dbCommon;
        }

        public bool Save(Driver driver)
        {
            try
            {
                //using (MySqlCommand command = new MySqlCommand())
                var db = new DriverDb();
                db.NewSqlCommand();
                {
                    //command.Parameters.Add("@FirstName", MySqlDbType.VarChar).Value = driver.FirstName;
                    db.CommandParametersAdd("@FirstName", db.GettingVarChar(), driver.FirstName);
                    //command.Parameters.Add("@LastName", MySqlDbType.VarChar).Value = driver.LastName;
                    db.CommandParametersAdd("@LastName", db.GettingVarChar(), driver.LastName);
                    //command.Parameters.Add("@DateOfBirth", MySqlDbType.DateTime).Value = driver.DayOfBirth;
                    if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                    {
                        db.CommandParametersAdd("@DateOfBirth", db.GettingDateTime(), driver.DayOfBirth);
                    }
                    else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                    {
                        if (driver.DayOfBirth == null)
                        {
                            DateTime dt = new DateTime(1900, 1, 1);
                            db.CommandParametersAdd("@DateOfBirth", db.GettingDateTime(), dt);
                        }
                        else
                        {
                            db.CommandParametersAdd("@DateOfBirth", db.GettingDateTime(), driver.DayOfBirth);
                        }
                    }

                    //command.Parameters.Add("@Categories", MySqlDbType.VarChar).Value = driver.Categories;
                    if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                    {
                        if (driver.Categories == "" || driver.Categories == null)
                        {
                            db.CommandParametersAdd( "@Categories", db.GettingVarChar(), " " );
                        }
                        else
                        {
                            db.CommandParametersAdd( "@Categories", db.GettingVarChar(), driver.Categories );
                        }
                    }
                    else if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                    {
                        db.CommandParametersAdd( "@Categories", db.GettingVarChar(), driver.Categories );
                    }

                    //command.Parameters.Add("@DrivingLicence", MySqlDbType.VarChar).Value = driver.License;

                    if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                    {
                        db.CommandParametersAdd("@DrivingLicence", db.GettingVarChar(), driver.License);
                    }
                    else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                    {
                        if (driver.License == "" || driver.License == null)
                        {
                            db.CommandParametersAdd("@DrivingLicence", db.GettingVarChar(), " ");
                        }
                        else
                        {
                            db.CommandParametersAdd("@DrivingLicence", db.GettingVarChar(), driver.License);
                        }
                    }
                    //command.Parameters.Add("@Identifier", MySqlDbType.UInt16).Value = driver.Identifier;
                    if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                    {
                        db.CommandParametersAdd("@Identifier", db.GettingInt16(), driver.Identifier);
                    }
                    else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                    {
                        if (driver.Identifier == null)
                            db.CommandParametersAdd("@Identifier", db.GettingInt32(), 0);
                        else
                            db.CommandParametersAdd("@Identifier", db.GettingInt32(), driver.Identifier);
                    }

                    if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                    {
                        db.CommandParametersAdd("@numTelephone", db.GettingString(), driver.NumTelephone);
                    }
                    else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                    {
                        if (driver.NumTelephone == null)
                            db.CommandParametersAdd("@numTelephone", db.GettingString(), "");
                        else
                            db.CommandParametersAdd("@numTelephone", db.GettingString(), driver.NumTelephone);
                    }

                    //command.Parameters.Add("@Photo", MySqlDbType.Binary).Value = null != driver.Photo ? ImageService.ToBytes(driver.Photo) : null;
                    if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                    {
                        db.CommandParametersAdd("@Photo", db.GettingBinary(),
                            null != driver.Photo ? ImageService.ToBytes(driver.Photo) : null);
                    }
                    else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                    {
                        if (driver.Photo == null)
                        {
                            byte[] bimg = new byte[0];
                            db.CommandParametersAdd("@Photo", db.GettingVarBinary(), bimg);
                        }
                        else
                        {
                            db.CommandParametersAdd("@Photo", db.GettingVarBinary(), null != driver.Photo
                                ? ImageService.ToBytes(driver.Photo)
                                : null);
                        }
                    }

                    //command.Parameters.Add("@OutLinkId", MySqlDbType.VarChar).Value = driver.IdOutLink;
                    if (driver.IdOutLink == null)
                        db.CommandParametersAdd("@OutLinkId", db.GettingVarChar(), "");
                    else
                        db.CommandParametersAdd("@OutLinkId", db.GettingVarChar(), driver.IdOutLink);

                    if (driver.Department == null)
                        db.CommandParametersAdd("@Department", db.GettingVarChar(), "");
                    else
                        db.CommandParametersAdd("@Department", db.GettingVarChar(), driver.Department);

                    //command.Parameters.Add("@TypeDriverId", MySqlDbType.Int32).Value = driver.TypeDriverId;
                    db.CommandParametersAdd("@TypeDriverId", db.GettingInt32(), driver.TypeDriverId);

                    if (driver.IsNew)
                    {
                        //command.CommandText = INSERT_ONE;
                        db.CommandText(TrackControlQuery.DriverProvider.InsertIntoDriver);
                        
                        //using (MySqlConnection conn = _dbCommon.GetConnection())
                        db.SqlConnection = _dbCommon.GetConnection();
                        {
                            //command.Connection = conn;
                            db.CommandSqlConnection();
                            //conn.Open();
                            db.SqlConnectionOpen();
                            //driver.Id = (int)(long)command.ExecuteScalar();
                            string id = db.CommandExecuteScalar().ToString();
                            driver.Id = Convert.ToInt32(id);
                        }
                        db.SqlConnectionClose();
                        return true;
                    }
                    else
                    {
                        //command.CommandText = UPDATE_ONE;
                        db.CommandText(TrackControlQuery.DriverProvider.UpdateDriver);
                       
                        //command.Parameters.Add("@Id", MySqlDbType.Int32).Value = driver.Id;
                        db.CommandParametersAdd("@Id", db.GettingInt32(), driver.Id);
                        //using (MySqlConnection connection = _dbCommon.GetConnection())
                        db.SqlConnection = _dbCommon.GetConnection();
                        {
                            //command.Connection = connection;
                            db.CommandSqlConnection();
                            db.SqlConnectionOpen(); //connection.Open();
                            //command.ExecuteNonQuery();
                            db.CommandExecuteNonQuery();
                        }

                        db.SqlConnectionClose();
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(
                    "Driver: " + driver.Id + ", " + driver.FirstName + ", " + driver.LastName + ", " + "is not insert. " +
                    ex.Message, "Error insert into table Driver");
                return false;
            }
        }

        public bool Delete(Driver driver)
        {
            if (!driver.IsNew)
            {
                var db = new DriverDb();
                //using (MySqlConnection conn = _dbCommon.GetConnection())
                db.SqlConnection = _dbCommon.GetConnection();
                //using (MySqlCommand command = new MySqlCommand(DELETE_ONE, conn))
                {
                    db.NewSqlCommand(TrackControlQuery.DriverProvider.DeleteFromDriver, db.SqlConnection);
                    //command.Parameters.Add("@Id", MySqlDbType.Int32).Value = driver.Id;
                    db.CommandParametersAdd("@Id", db.GettingInt32(), driver.Id);
                    //command.Connection.Open();
                    db.CommandConnectionOpen();
                    //command.ExecuteNonQuery();
                    db.CommandExecuteNonQuery();
                }
                db.SqlConnectionClose();
            }
            return true;
        }

        public bool IsPermitToDelete(Driver driver)
        {
            return !IsExistLinkInVehicles(driver);

        }

        public bool IsExistLinkInVehicles(Driver driver)
        {
            //using (MySqlCommand command = new MySqlCommand())
            var db = new DriverDb();
            db.NewSqlCommand();
            {
                //command.CommandType = CommandType.Text;
                db.CommandType(CommandType.Text);
                //command.CommandText = string.Format(@"SELECT {0} FROM   vehicle WHERE   vehicle.driver_id = {1}", ConstsGen.SqlVehicleIdent, driver.Id);

                db.CommandText(string.Format(TrackControlQuery.DriverProvider.SelectFromVehicle,
                        TrackControlQuery.VehicleIdent, driver.Id));
               
                //using (MySqlConnection conn = _dbCommon.GetConnection())
                db.SqlConnection = _dbCommon.GetConnection();
                {
                    //command.Connection = conn;
                    db.CommandSqlConnection();
                    db.SqlConnectionOpen(); //conn.Open();
                    //MySqlDataReader dr = command.ExecuteReader();
                    db.SqlDataReader = db.CommandExecuteReader();
                    string vehicleList = "";
                    while (db.Read())
                    {
                        vehicleList += string.Format("{0} {1}", db.GetString(0), Environment.NewLine);
                    }

                    if (vehicleList.Length > 0)
                    {
                        XtraMessageBox.Show(
                            string.Format("{0}:{1}{2}", Resources.LinkedVehicles, Environment.NewLine, vehicleList),
                            Resources.DeleteBan);
                        db.CloseDataReader();
                        db.SqlConnectionClose();
                        return true;
                    }
                    else
                    {
                        db.CloseDataReader();
                        db.SqlConnectionClose();
                        return false;
                    }
                }
            }
        }
    }
}
