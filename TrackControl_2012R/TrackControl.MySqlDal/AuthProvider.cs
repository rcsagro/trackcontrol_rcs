using System;
using TrackControl.General;
using MySql.Data.MySqlClient;
using TrackControl.General.DatabaseDriver;

namespace TrackControl.MySqlDal
{
  public class AuthProvider : IAuthProvider
  {
    const string CHECK_COUNT = "SELECT COUNT(*) FROM `Users`";
    const string FIRST_USER = "SELECT `Login` FROM `Users` LIMIT 1";
    const string CHECK_USER = "SELECT `Pass` FROM `Users` WHERE `Login` = @login";

    //DbCommon _dbCommon;
      private DriverDb _dbCommon;

    //public AuthProvider(DbCommon dbCommon)
      public AuthProvider( DriverDb dbCommon )
    {
      if (null == dbCommon)
        throw new ArgumentNullException("dbCommon");

      _dbCommon = dbCommon;
    }

    public bool IsAuthorizationNeeded()
    {
      #region -- ���� �� ������� "Users" --
      if (!_dbCommon.IsTableExist("Users"))
      {
        return false;
      }
      #endregion

      #region -- ���� �� ������ � ������� --
      int count = 0;

      //using (MySqlCommand command = new MySqlCommand(CHECK_COUNT))
        var db = new DriverDb();
        db.NewSqlCommand( CHECK_COUNT );
      {
        //using (MySqlConnection conn = _dbCommon.GetConnection())
          db.SqlConnection = _dbCommon.GetConnection();
        {
          //command.Connection = conn;
            db.CommandSqlConnection();
          db.SqlConnectionOpen(); //conn.Open();
          //count = Convert.ToInt32(command.ExecuteScalar());
          count = Convert.ToInt32( db.CommandExecuteScalar() );
        }
        db.SqlConnectionClose();
      }
      if (0 == count)
      {
        return false;
      }
      #endregion

      #region -- �������� �� ������ ������ �������� --
      string login = String.Empty;

      //using (MySqlCommand command = new MySqlCommand(FIRST_USER))
      db.NewSqlCommand( FIRST_USER );
      {
        //using (MySqlConnection connection = _dbCommon.GetConnection())
          db.SqlConnection = _dbCommon.GetConnection();
        {
          //command.Connection = connection;
            db.CommandSqlConnection();
          db.SqlConnectionOpen(); //connection.Open();
          //login = Convert.ToString(command.ExecuteScalar());
          login = Convert.ToString( db.CommandExecuteScalar() );
        }
      }
      if (0 == login.Trim().Length)
      {
        return false;
      }
      #endregion

      return true;
    }

    public bool IsValidLogin(string login, string password)
    {
      //using (MySqlCommand command = new MySqlCommand(CHECK_USER))
        var db = new DriverDb();
        db.NewSqlCommand( CHECK_USER );
      {
        //using (MySqlConnection connection = _dbCommon.GetConnection())
          db.SqlConnection = _dbCommon.GetConnection();
        {
          //command.Connection = connection;
            db.CommandSqlConnection();
          //command.Parameters.Add("@login", MySqlDbType.VarChar).Value = login;
            db.CommandParametersAdd("@login", db.GettingVarChar(), login);
          db.SqlConnectionOpen(); //connection.Open();
          //using (MySqlDataReader reader = command.ExecuteReader())
            db.SqlDataReader = db.CommandExecuteReader();
          {
            while (db.Read())
            {
              string pass = db.GetString("Pass");
              if (pass == password)
              {
                  db.CloseDataReader();
                return true;
              }
            }
          }
            db.CloseDataReader();
        }
        db.SqlConnectionClose();
      }
      return false;
    }
  }
}
