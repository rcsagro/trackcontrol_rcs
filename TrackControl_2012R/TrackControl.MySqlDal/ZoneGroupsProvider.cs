using System;
using System.Data;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using TrackControl.General;
using TrackControl.General.DatabaseDriver;
using TrackControl.Zones;
using TrackControl.General.DAL;

namespace TrackControl.MySqlDal
{
    public class ZoneGroupsProvider : IZoneGroupsProvider
    {
        private DriverDb _dbCommon;
        //public ZoneGroupsProvider(DbCommon dbCommon)
        public ZoneGroupsProvider(DriverDb dbCommon)
        {
            _dbCommon = dbCommon;
        }

        public IList<ZonesGroup> GetAll()
        {
            List<ZonesGroup> groups = new List<ZonesGroup>();
            // using (MySqlCommand command = new MySqlCommand(SELECT_ALL))
            {
                var db = new DriverDb();

                db.NewSqlCommand(TrackControlQuery.ZoneGroupsProvider.SelectAll);

                //command.CommandType = CommandType.Text;
                db.CommandType(CommandType.Text);
                //using (MySqlConnection connection = _dbCommon.GetConnection())
                db.SqlConnection = _dbCommon.GetConnection();
                {
                    //command.Connection = connection;
                    db.CommandSqlConnection();
                    db.SqlConnectionOpen(); //connection.Open();
                    //using (MySqlDataReader reader = command.ExecuteReader())
                    // using( MySqlDataReader reader = db.CommandExecuteReader() )
                    db.SqlDataReader = db.CommandExecuteReader();
                    {
                        while (db.Read())
                        {
                            ZonesGroup g = new ZonesGroup(db.GetString("Title"), db.GetString("Description"));
                            g.Id = (int) db.GetInt64("Id");
                            groups.Add(g);
                        }
                    }
                    db.CloseDataReader();
                }
                db.SqlConnectionClose();
            }
            return groups;
        }

        public bool Save(ZonesGroup group)
        {
            //using (MySqlCommand command = new MySqlCommand())
            var db = new DriverDb();
            db.NewSqlCommand();
            {
                //command.CommandType = CommandType.Text;
                db.CommandType(CommandType.Text);
                //command.Parameters.Add("@Title", MySqlDbType.VarChar).Value = group.Name;
                db.CommandParametersAdd("@Title", db.GettingVarChar(), group.Name);
                //command.Parameters.Add("@Description", MySqlDbType.VarChar).Value = group.Description;
                db.CommandParametersAdd("@Description", db.GettingVarChar(), group.Description);
                //command.Parameters.Add("@OutLinkId", MySqlDbType.VarChar).Value = group.IdOutLink;

                if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                {
                    db.CommandParametersAdd("@OutLinkId", db.GettingVarChar(), group.IdOutLink);
                }
                else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                {
                    if (group.IdOutLink == null)
                    {
                        db.CommandParametersAdd("@OutLinkId", db.GettingVarChar(), "");
                    }
                    else
                    {
                        db.CommandParametersAdd("@OutLinkId", db.GettingVarChar(), group.IdOutLink);
                    }
                }
                if (group.IsNew)
                {
                    //command.CommandText = INSERT_ONE;
                    db.CommandText(TrackControlQuery.ZoneGroupsProvider.InsertOne);

                    //using (MySqlConnection connection = _dbCommon.GetConnection())
                    db.SqlConnection = _dbCommon.GetConnection();
                    //command.Connection = connection;
                    db.CommandSqlConnection();
                    db.SqlConnectionOpen(); //connection.Open();
                    //group.Id = (int)(long)command.ExecuteScalar();
                    object id = db.CommandExecuteScalar();

                    if (id == null)
                        throw new Exception("Command Execute Scalar return null!");
                        
                    string idstr = id.ToString();

                    if (idstr != "")
                    {
                        group.Id = Convert.ToInt32(idstr);
                    }
                    db.SqlConnectionClose();
                    return true;
                }
                else
                {
                    //command.CommandText = UPDATE_ONE;
                    db.CommandText(TrackControlQuery.ZoneGroupsProvider.UpdateOne);

                    //command.Parameters.Add("@Id", MySqlDbType.Int32).Value = group.Id;
                    db.CommandParametersAdd("@Id", db.GettingInt32(), group.Id);
                    //using (MySqlConnection connection = _dbCommon.GetConnection())
                    db.SqlConnection = _dbCommon.GetConnection();
                    {
                        //command.Connection = connection;
                        db.CommandSqlConnection();
                        db.SqlConnectionOpen(); //connection.Open();
                        //command.ExecuteNonQuery();
                        db.CommandExecuteNonQuery();
                    }
                    db.SqlConnectionClose();
                    return true;
                }
            }
        }

        public bool Delete(ZonesGroup group)
        {
            if (group.IsNew)
            {
                return true;
            }
            else if (1 == group.Id)
            {
                throw new Exception("It's not appropriate to remove root element in zones tree.");
            }
            else
            {
                //using (MySqlCommand command = new MySqlCommand())
                var db = new DriverDb();
                db.NewSqlCommand();
                {
                    //command.CommandType = CommandType.Text;
                    db.CommandType(CommandType.Text);
                    //command.CommandText = DELETE_ONE;
                    db.CommandText(TrackControlQuery.ZoneGroupsProvider.DeleteOne);

                    //command.Parameters.Add("@Id", MySqlDbType.Int32).Value = group.Id;
                    db.CommandParametersAdd("@Id", db.GettingInt32(), group.Id);
                    //using (MySqlConnection connection = _dbCommon.GetConnection())
                    db.SqlConnection = _dbCommon.GetConnection();
                    {
                        //command.Connection = connection;
                        db.CommandSqlConnection();
                        db.SqlConnectionOpen(); //connection.Open();
                        //command.ExecuteNonQuery();
                        db.CommandExecuteNonQuery();
                    }
                    db.SqlConnectionClose();
                    return true;
                }
            }
        }
    }
}
