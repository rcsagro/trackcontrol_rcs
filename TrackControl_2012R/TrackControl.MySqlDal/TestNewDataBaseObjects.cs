﻿using System;
using System.Collections.Generic;
using System.Text;
using DevExpress.XtraEditors;
using TrackControl.General;
using TrackControl.General.DatabaseDriver;

namespace TrackControl.MySqlDal
{
    public class TestNewDataBaseObjects
    {
        DbCommon _dbCom;
        //DriverDb _dbCom;

        public TestNewDataBaseObjects(DbCommon dbCom)
        //public TestNewDataBaseObjects( DriverDb dbCom )
        {
            _dbCom = dbCom;
        }

        public bool TestIsExistNewObjects()
        {
            string absenceMes;

            if (!_dbCom.IsTableExist("setting"))
                absenceMes = "Отсутствует  таблица setting\n";
            else
                absenceMes = "";
            TestIsExistTable( "vehicle_commentary", ref absenceMes );
            TestIsExistTable( "vehicle_category3", ref absenceMes );
            TestIsExistTable( "vehicle_category4", ref absenceMes );
            TestIsExistTable( "zone_category", ref absenceMes );
            TestIsExistTable( "zone_category2", ref absenceMes );
            TestIsExistColumn("sensors", "B", ref absenceMes);
            TestIsExistColumn("sensors", "S", ref absenceMes);
            TestIsExistColumn("driver", "numTelephone", ref absenceMes);
            TestIsExistColumn("setting", "TimeMinimMovingSize", ref absenceMes);
            TestIsExistColumn( "setting", "FuelingMinMaxAlgorithm", ref absenceMes );
            TestIsExistColumn( "zones", "Category_id", ref absenceMes );
            TestIsExistColumn( "zones", "Category_id2", ref absenceMes );
            TestIsExistColumn( "vehicle", "Category_id3", ref absenceMes );
            TestIsExistColumn( "vehicle", "Category_id4", ref absenceMes );
            TestIsExistTable("mobitels_rotate_bands", ref absenceMes);
            TestIsExistTable("ntf_events", ref absenceMes);
            TestIsExistTable("ntf_log", ref absenceMes);
            TestIsExistTable("ntf_main", ref absenceMes);
            TestIsExistTable("ntf_mobitels", ref absenceMes);
            TestIsExistTable("parameters", ref absenceMes);
            TestIsExistTable("users_list", ref absenceMes);
            TestIsExistTable("users_actions_log", ref absenceMes);
            TestIsExistColumn("zones", "OutLinkId", ref absenceMes);
            TestIsExistColumn("zonesgroup", "OutLinkId", ref absenceMes);
            TestIsExistColumn("driver", "OutLinkId", ref absenceMes);
            TestIsExistColumn("vehicle", "OutLinkId", ref absenceMes);
            TestIsExistColumn("team", "OutLinkId", ref absenceMes);
            TestIsExistColumn("setting", "TimeBreakMaxPermitted", ref absenceMes);
            TestIsExistColumn("users_list", "Id_role", ref absenceMes);
            TestIsExistTable("users_acs_roles", ref absenceMes);
            TestIsExistTable("users_acs_objects", ref absenceMes);
            TestIsExistTable("users_reports_list", ref absenceMes);
            TestIsExistColumn("ntf_log", "Value", ref absenceMes);
            TestIsExistTable("ntf_emails", ref absenceMes);
            TestIsExistColumn("ntf_main", "IsEMailActive", ref absenceMes);
            TestIsExistTable("driver_types", ref absenceMes);
            TestIsExistColumn("driver", "idType", ref absenceMes);
            TestIsExistTable("vehicle_category", ref absenceMes);
            TestIsExistColumn("vehicle", "Category_id", ref absenceMes);
            TestIsExistColumn("setting", "InclinometerMaxAngleAxisX", ref absenceMes);
            TestIsExistColumn("setting", "InclinometerMaxAngleAxisY", ref absenceMes);
            TestIsExistColumn("ntf_events", "ParInt2", ref absenceMes);
            TestIsExistTable("ntf_popupusers", ref absenceMes);
            TestIsExistTable("vehicle_category2", ref absenceMes);
            TestIsExistColumn("vehicle", "Category_id2", ref absenceMes);
            TestIsExistColumn("mobitels", "Is64bitPackets", ref absenceMes);
            TestIsExistColumn("mobitels", "IsNotDrawDgps", ref absenceMes);
            TestIsExistColumn("setting", "speedMin", ref absenceMes);
            TestIsExistColumn("setting", "MinTimeSwitch", ref absenceMes);
            TestIsExistTable("rt_shedule", ref absenceMes);
            TestIsExistTable("rt_shedulet", ref absenceMes);
            TestIsExistTable("rt_shedule_dc", ref absenceMes);
            TestIsExistTable("rt_shedulet_dc", ref absenceMes);
            TestIsExistColumn("driver", "Department", ref absenceMes);
            TestIsExistColumn("ntf_main", "Period", ref absenceMes);
            TestIsExistTable("pcbdata", ref absenceMes);
            TestIsExistColumn("pcbdata", "NameVal", ref absenceMes);
            TestIsExistColumn("pcbdata", "NumAlg", ref absenceMes);
            TestIsExistColumn("rt_route", "OutLinkId", ref absenceMes);
            TestIsExistColumn("rt_sample", "OutLinkId", ref absenceMes);
            absenceMes = TestAgroNewObjects(absenceMes);
            if (absenceMes.Length == 0)
                return true;
            else
            {
                XtraMessageBox.Show(absenceMes, "Запустите патч базы данных!");
                return false;
            }
        }

        private string TestAgroNewObjects(string absenceMes)
        {
            if (_dbCom.IsTableExist("agro_order"))
            {
                TestIsExistTable("agro_agregat_vehicle", ref absenceMes);
                TestIsExistColumn("agro_order", "SquareWorkDescripOverlap", ref absenceMes);
                TestIsExistColumn("agro_order", "FactSquareCalcOverlap", ref absenceMes);
                TestIsExistColumn("agro_fieldgroupe", "Id_Zonesgroup", ref absenceMes);
                TestIsExistColumn("agro_order", "DateLastRecalc", ref absenceMes);
                TestIsExistColumn("agro_order", "UserLastRecalc", ref absenceMes);
                TestIsExistColumn("agro_order", "UserCreated", ref absenceMes);
                TestIsExistColumn("agro_order", "PointsValidity", ref absenceMes);
                TestIsExistColumn("agro_order", "PointsCalc", ref absenceMes);
                TestIsExistColumn("agro_order", "PointsFact", ref absenceMes);
                TestIsExistColumn("agro_order", "PointsIntervalMax", ref absenceMes);
                TestIsExistColumn("agro_ordert", "PointsValidity", ref absenceMes);
                TestIsExistColumn("agro_ordert", "LockRecord", ref absenceMes);
                TestIsExistColumn("agro_order", "StateOrder", ref absenceMes);
                TestIsExistColumn("agro_order", "BlockUserId", ref absenceMes);
                TestIsExistColumn("agro_order", "BlockDate", ref absenceMes);
                TestIsExistColumn("agro_agregat", "OutLinkId", ref absenceMes);
                TestIsExistTable("agro_fueling", ref absenceMes);
                TestIsExistColumn("agro_agregat", "HasSensor", ref absenceMes);
                TestIsExistTable("agro_work_types", ref absenceMes);
                TestIsExistColumn("agro_work", "TypeWork", ref absenceMes);
                TestIsExistTable("agro_workgroup", ref absenceMes);
                TestIsExistColumn("agro_work", "Id_main", ref absenceMes);
                TestIsExistColumn("agro_ordert", "AgrInputSource", ref absenceMes);
                TestIsExistColumn("agro_ordert", "DrvInputSource", ref absenceMes);
                TestIsExistColumn("agro_work", "OutLinkId", ref absenceMes);
                TestIsExistColumn("agro_plant", "TimeStart", ref absenceMes);
                TestIsExistTable("agro_season", ref absenceMes);
                TestIsExistColumn("agro_field", "IsOutOfDate", ref absenceMes);
                TestIsExistColumn("agro_work", "TrackColor", ref absenceMes);
                TestIsExistTable("agro_fieldseason_tc", ref absenceMes);
                TestIsExistTable("agro_fieldseason_tct", ref absenceMes);
                TestIsExistTable("agro_fieldseason_tct_fact", ref absenceMes);
                TestIsExistColumn("agro_fieldseason_tc", "DateStart", ref absenceMes);
                TestIsExistColumn("agro_fieldseason_tc", "DateEnd", ref absenceMes);
                TestIsExistColumn("agro_agregat_vehicle", "Id_state", ref absenceMes);
                TestIsExistTable("ntf_agro_works", ref absenceMes);
                TestIsExistColumn("agro_agregat", "IsUseAgregatState", ref absenceMes);
                TestIsExistColumn("agro_agregat", "MinStateValue", ref absenceMes);
                TestIsExistColumn("agro_agregat", "MaxStateValue", ref absenceMes);
                TestIsExistColumn("agro_agregat", "LogicState", ref absenceMes);
                TestIsExistColumn("agro_agregat", "SensorAlgorithm", ref absenceMes);
                TestIsExistColumn("agro_ordert", "Id_event_manually", ref absenceMes);
                TestIsExistTable("agro_event_manually", ref absenceMes);
                TestIsExistColumn("agro_agregat", "LogicStateBounce", ref absenceMes);
                TestIsExistTable("agro_sensorkoefficients", ref absenceMes);
                TestIsExistColumn("agro_agregat", "K", ref absenceMes);
                TestIsExistColumn("agro_agregat", "B", ref absenceMes);
                TestIsExistColumn("agro_agregat", "S", ref absenceMes);
                TestIsExistColumn("agro_ordert", "FactSquareJointProcessing", ref absenceMes);
                TestIsExistColumn("agro_workgroup", "IsSpeedControl", ref absenceMes);
                TestIsExistColumn("agro_workgroup", "IsDepthControl", ref absenceMes);
                TestIsExistColumn("agro_fieldseason_tct", "IsClosed", ref absenceMes);
                TestIsExistColumn("agro_season", "IsClosed", ref absenceMes);
            }
            return absenceMes;
        }
        void TestIsExistColumn(string table, string column, ref string mess)
        {
            if (!_dbCom.IsColumnExist(table, column))
            {
                mess = String.Format("{0} Отсутствует поле {2} в таблице {1}\n", mess, table, column);
            }
        }
        void TestIsExistTable(string table, ref string mess)
        {
            if (!_dbCom.IsTableExist(table))
            {
                mess = String.Format("{0} Отсутствует таблица {1}\n", mess, table);
            }
        }
    }
}
