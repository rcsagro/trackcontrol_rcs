using System;
using System.Collections.Generic;
using TrackControl.General.DatabaseDriver;
using TrackControl.Vehicles;using TrackControl.General.DAL;

namespace TrackControl.MySqlDal
{
    public class VehiclesGroupProvider : IVehiclesGroupProvider
    {
        //DbCommon _dbCommon;
        private DriverDb _dbCommon;
        //public VehiclesGroupProvider(DbCommon dbCommon)
        public VehiclesGroupProvider(DriverDb dbCommon)
        {
            if (null == dbCommon)
                throw new ArgumentNullException("dbCommon");

            _dbCommon = dbCommon;
        }

        public IList<VehiclesGroup> GetAll()
        {
            List<VehiclesGroup> groups = new List<VehiclesGroup>();

            var db = new DriverDb();
            //using (MySqlCommand command = new MySqlCommand(SELECT_ALL))
            {
                //using (MySqlConnection conn = _dbCommon.GetConnection())
                db.SqlConnection = _dbCommon.GetConnection();
                {
                    db.NewSqlCommand(TrackControlQuery.VehiclesGroupProvider.SelectAll);

                    //command.Connection = conn;
                    db.CommandSqlConnection();
                    db.SqlConnectionOpen(); //conn.Open();
                    //using (MySqlDataReader reader = command.ExecuteReader())
                    //using( MySqlDataReader reader = db.CommandExecuteReader() )
                    db.SqlDataReader = db.CommandExecuteReader();
                    {
                        VehiclesGroup group = new VehiclesGroup(db.GetString("Name"), db.GetString("Descr"));
                        group.Id = db.GetInt32("id");
                        group.FuelWayKmGrp = Convert.ToString(Math.Round(db.GetDouble("FuelWayKmGrp"), 1));
                        group.FuelMotoHrGrp = Convert.ToString(Math.Round(db.GetDouble("FuelMotoHrGrp"), 1));
                        group.IdOutLink = db.IsDbNull(db.GetOrdinal("OutLinkId")) ? "" : db.GetString("OutLinkId");
                        groups.Add(group);
                    }
                    db.CloseDataReader();
                }
                db.SqlConnectionClose();
            }
            return groups;
        }

        public bool Save(VehiclesGroup group)
        {
            //using (MySqlCommand command = new MySqlCommand())
            var db = new DriverDb();
            db.NewSqlCommand();
            {
                //command.Parameters.Add("@Name", MySqlDbType.VarChar).Value = group.Name;
                db.CommandParametersAdd("@Name", db.GettingVarChar(), group.Name);
                //command.Parameters.Add("@Description", MySqlDbType.VarChar).Value = group.Description;
                if (group.Description == null)
                    group.Description = "null";
                db.CommandParametersAdd("@Description", db.GettingVarChar(), group.Description);
                //command.Parameters.Add("@OutLinkId", MySqlDbType.VarChar).Value = group.IdOutLink;
                if (DriverDb.TypeDataBaseUsing == DriverDb.MySqlUse)
                {
                    db.CommandParametersAdd("@OutLinkId", db.GettingVarChar(), group.IdOutLink);
                }
                else if (DriverDb.TypeDataBaseUsing == DriverDb.MssqlUse)
                {
                    if (group.IdOutLink == null)
                    {
                        db.CommandParametersAdd("@OutLinkId", db.GettingVarChar(), "");
                    }
                    else
                    {
                        db.CommandParametersAdd("@OutLinkId", db.GettingVarChar(), group.IdOutLink);
                    }
                }

                db.CommandParametersAdd("@FuelWayKmGrp", db.GettingDouble(), Convert.ToDouble(group.FuelWayKmGrp));

                db.CommandParametersAdd("@FuelMotoHrGrp", db.GettingDouble(), Convert.ToDouble(group.FuelMotoHrGrp));

                if (group.IsNew)
                {
                    //command.CommandText = INSERT_ONE;

                    db.CommandText(TrackControlQuery.VehiclesGroupProvider.InsertOne);

                    //using (MySqlConnection conn = _dbCommon.GetConnection())
                    db.SqlConnection = _dbCommon.GetConnection();
                    {
                        //command.Connection = conn;
                        db.CommandSqlConnection();
                        db.SqlConnectionOpen(); //conn.Open();
                        //group.Id = (int)(long)command.ExecuteScalar();
                        string obj = db.CommandExecuteScalar().ToString();
                        if (obj != "")
                            group.Id = Convert.ToInt32(obj);
                    }
                    db.SqlConnectionClose();
                    return true;
                }
                else
                {
                    //command.CommandText = UPDATE_ONE;
                    db.CommandText(TrackControlQuery.VehiclesGroupProvider.UpdateOne);

                    //command.Parameters.Add("@Id", MySqlDbType.Int32).Value = group.Id;
                    db.CommandParametersAdd("@Id", db.GettingInt32(), group.Id);
                    //using (MySqlConnection connection = _dbCommon.GetConnection())
                    db.SqlConnection = _dbCommon.GetConnection();
                    {
                        //command.Connection = connection;
                        db.CommandSqlConnection();
                        db.SqlConnectionOpen(); //connection.Open();
                        //command.ExecuteNonQuery();
                        db.CommandExecuteNonQuery();
                    }
                    db.SqlConnectionClose();
                    return true;
                }
            }
        }

        public bool Delete(VehiclesGroup group)
        {
            if (!group.IsNew)
            {
                //using (MySqlCommand command = new MySqlCommand())
                var db = new DriverDb();
                db.NewSqlCommand();
                {
                    //command.CommandText = DELETE_ONE;
                    db.CommandText(TrackControlQuery.VehiclesGroupProvider.DeleteOne);

                    //command.Parameters.Add("@Id", MySqlDbType.Int32).Value = group.Id;
                    db.CommandParametersAdd("@Id", db.GettingInt32(), group.Id);
                    //using (MySqlConnection connection = _dbCommon.GetConnection())
                    db.SqlConnection = _dbCommon.GetConnection();
                    {
                        //command.Connection = connection;
                        db.CommandSqlConnection();
                        //connection.Open();
                        db.SqlConnectionOpen();
                        //command.ExecuteNonQuery();
                        db.CommandExecuteNonQuery();
                    }
                    db.SqlConnectionClose();
                }
            }

            return true;
        }
    }
}
