﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using MySql.Data.MySqlClient;
using System.Data;
using TrackControl.General.DatabaseDriver;
using TrackControl.Vehicles;

namespace TrackControl.MySqlDal
{
    public class SettingsProvider : ISettingsProvider
    {
        //DbCommon _dbCommon;
        DriverDb _dbCommon;
        //public SettingsProvider(DbCommon dbCommon)
        public SettingsProvider( DriverDb dbCommon )
        {
            _dbCommon = dbCommon;
        }

        public void GetOne(VehicleSettings Settings)
        {
            //using (MySqlCommand command = new MySqlCommand())
            var db = new DriverDb();
            try
            {
                db.NewSqlCommand();
                {
                    //command.CommandType = CommandType.Text;
                    db.CommandType(CommandType.Text);
                    //command.CommandText = @"SELECT * FROM setting WHERE setting.id = @Id";
                    db.CommandText(@"SELECT * FROM setting WHERE setting.id = @Id");
                    //command.Parameters.Add("@Id", MySqlDbType.Int32).Value = Settings.Id;
                   
                    db.CommandParametersAdd("@Id", db.GettingInt32(), Settings.Id);
                    //using (MySqlConnection connection = _dbCommon.GetConnection())
                    db.SqlConnection = _dbCommon.GetConnection();
                    {
                        //command.Connection = connection;
                        db.CommandSqlConnection();
                        db.SqlConnectionOpen(); //connection.Open();
                        //using (MySqlDataReader reader = command.ExecuteReader())
                        //using( MySqlDataReader reader = db.CommandExecuteReader() )
                        db.SqlDataReader = db.CommandExecuteReader();
                        {
                            if (db.Read())
                            {
                                Settings.SpeedMax = (float) db.GetDouble("speedMax");
                                Settings.TimeBreak = db.GetTimeSpan("TimeBreak");
                                Settings.TimeBreakMaxPermitted = db.GetTimeSpan("TimeBreakMaxPermitted");
                                Settings.AvgFuelRatePerHour = db.GetDouble("AvgFuelRatePerHour");
                                Settings.FuelerMinFuelRate = db.GetInt32("FuelerMinFuelrate");
                            }
                        }
                        db.CloseDataReader();
                    }
                    db.SqlConnectionClose();
                }
            }
            catch (Exception e)
            {
                XtraMessageBox.Show(e.Message + "\n" + e.StackTrace, "MySqlDal.SettingsProvider", MessageBoxButtons.OK);
                db.CloseDbConnection();
            }
        }
    }
}