using System;
using System.Drawing;
using NUnit.Framework;

namespace TrackControl.General.Test
{
  [TestFixture]
  public class ImageServiceTest
  {
    [Test]
    [ExpectedException(typeof(ArgumentNullException))]
    public void Inscribe_should_throw_anException_when_image_isNull()
    {
      ImageService.Inscribe(null, 20, 20);
    }

    [Test]
    [ExpectedException(typeof(ArgumentException))]
    public void Inscribe_should_throw_anException_when_widthLimit_less_thanOne()
    {
      using (Image image = new Bitmap(1, 1))
      {
        ImageService.Inscribe(image, 0, 20);
      }
    }

    [Test]
    [ExpectedException(typeof(ArgumentException))]
    public void Inscribe_should_throw_anException_when_heightLimit_less_thanOne()
    {
      using (Image image = new Bitmap(1, 1))
      {
        ImageService.Inscribe(image, 20, 0);
      }
    }

    [Test]
    public void Inscribe_16x20_into_20x20()
    {
      using (Image image = new Bitmap(16, 20))
      {
        using (Image result = ImageService.Inscribe(image, 20, 20))
        {
          Assert.AreEqual(new Size(16, 20), result.Size);
        }
      }
    }

    [Test]
    public void Inscribe_200x100_into_100x100()
    {
      using (Image image = new Bitmap(200, 100))
      {
        using (Image result = ImageService.Inscribe(image, 100, 100))
        {
          Assert.AreEqual(new Size(100, 50), result.Size);
        }
      }
    }

    [Test]
    public void Inscribe_100x200_into_100x100()
    {
      using (Image image = new Bitmap(100, 200))
      {
        using (Image result = ImageService.Inscribe(image, 100, 100))
        {
          Assert.AreEqual(new Size(50, 100), result.Size);
        }
      }
    }

    [Test]
    public void Inscribe_200x100_into_10x20()
    {
      using (Image image = new Bitmap(200, 100))
      {
        using (Image result = ImageService.Inscribe(image, 10, 20))
        {
          Assert.AreEqual(new Size(10, 5), result.Size);
        }
      }
    }

    [Test]
    public void Inscribe_100x200_into_20x10()
    {
      using (Image image = new Bitmap(100, 200))
      {
        using (Image result = ImageService.Inscribe(image, 20, 10))
        {
          Assert.AreEqual(new Size(5, 10), result.Size);
        }
      }
    }

    [Test]
    public void Inscribe_200x100_into_15x10()
    {
      using (Image image = new Bitmap(200, 100))
      {
        using (Image result = ImageService.Inscribe(image, 15, 10))
        {
          Assert.AreEqual(new Size(15, 8), result.Size);
        }
      }
    }

    [Test]
    public void Inscribe_200x100_into_30x10()
    {
      using (Image image = new Bitmap(200, 100))
      {
        using (Image result = ImageService.Inscribe(image, 30, 10))
        {
          Assert.AreEqual(new Size(20, 10), result.Size);
        }
      }
    }

    [Test]
    public void Inscribe_100x200_into_10x15()
    {
      using (Image image = new Bitmap(100, 200))
      {
        using (Image result = ImageService.Inscribe(image, 10, 15))
        {
          Assert.AreEqual(new Size(8, 15), result.Size);
        }
      }
    }

    [Test]
    public void Inscribe_100x200_into_10x30()
    {
      using (Image image = new Bitmap(100, 200))
      {
        using (Image result = ImageService.Inscribe(image, 10, 30))
        {
          Assert.AreEqual(new Size(10, 20), result.Size);
        }
      }
    }

    [Test]
    public void Inscribe_256x256_into_100x120()
    {
      using (Image image = new Bitmap(256, 256))
      {
        using (Image result = ImageService.Inscribe(image, 100, 120))
        {
          Assert.AreEqual(new Size(100, 100), result.Size);
        }
      }
    }
  }
}
