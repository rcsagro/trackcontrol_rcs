using System;
using System.Data;
using System.Diagnostics;
using Agro.Utilites;
using BaseReports.Procedure;
using NUnit.Framework;
using TrackControl;
using TrackControl.General;
using TrackControl.MySqlDal;
using TrackControl.Reports;
using TrackControl.UkrGIS.SearchEngine;
namespace Agro.Test
{
    [TestFixture]
    public class AgroTest
    {
    [SetUp]
    public void ModelInit()
    {
        AppModel _appModel = AppModel.Instance;
        TrackControl.General.DatabaseDriver.DriverDb dbCommon = new TrackControl.General.DatabaseDriver.DriverDb(ConnectMySQL.ConnectionString);
        AppModel.Instance.DbCommon = dbCommon;
        OrderItem.ZonesModel = _appModel.ZonesManager;
        ReportZonesModel _reportZonesModel = new ReportZonesModel(_appModel.ZonesManager);
        Algorithm.ZonesModel = _reportZonesModel;
        AppController _controller = AppController.Instance;
        AppController.initUkrGIS();
        Algorithm.GeoLocator = GeoLocator.Instance;
    }
        [Test, Description("���� �� ������������ �������� ������ ��� ���������")]
        public void CreateOrderWithZeroMobitel()
        {
            OrderItem oi = new OrderItem(0);
            Assert.AreEqual(oi.AddDoc(), 0);
        }
        [Test, Description("���� �� �������� � �������� ����� ������")]
        public void CreateOrderHeader()
        {
            int newID;
            int mobitelID;
            using (ConnectMySQL cn = new ConnectMySQL())
            {
                string sSQL = "SELECT agro_order.Id FROM  agro_order ORDER BY  Id DESC LIMIT 1";
                newID = cn.GetScalarValueIntNull(sSQL);
                sSQL = "SELECT   mobitels.Mobitel_ID FROM   mobitels   LIMIT 1";
                mobitelID = cn.GetScalarValueIntNull(sSQL);
            }
            if (mobitelID > 0)
            {
                using (OrderItem oi = new OrderItem(DateTime.Today,mobitelID))
                {
                    Assert.GreaterOrEqual(oi.AddDoc(), newID + 1);
                    Assert.AreEqual (oi.UpdateDoc() , true);
                    oi.DeleteDoc(false);
                }
            }

        }
        [Test, Description("�������� ������ � �������������� ����� ��������� ����������� ������ (��������, ��������, ����, �� ������� ������ �������� �������)")]
        public void CompareWithTamplate144()
        {
            using (OrderItem oi = new OrderItem(144))
            {
                oi.Remark = "�� �������! ������ ��� �����!";
                Assert.AreEqual(oi.UpdateDoc(),true);
                //Assert.AreEqual(oi.Exist() , true);
                Assert.AreEqual(oi.Date , new DateTime(2010, 10, 19));
                Assert.AreEqual(oi.Mobitel_Id, 124);
                oi.ChangeStatusEvent += PrintStstusEvent;
                if (oi.ContentAutoCreate(true))
                {
                    Assert.AreEqual(oi.TimeStart, new DateTime(2010, 10, 19, 10, 43, 19));//19.10.2010  10:43:19
                    Assert.AreEqual(oi.TimeEnd, new DateTime(2010, 10, 19, 17, 52, 37));//19.10.2010  17:52:37
                    Assert.AreEqual(oi.Distance, 34.95);
                    Assert.AreEqual(oi.SpeedAvg, 7.91);
                    Assert.AreEqual(oi.TimeWork, "07:09");
                    Assert.AreEqual(oi.TimeMove, "04:24");
                    Assert.AreEqual(oi.TimeStop, "02:45");
                    Assert.AreEqual(oi.SquareWorkDescript, "0301-01005_2: 31,52 (70,04%) ");
                    Assert.AreEqual(oi.PathWithoutWork , 2.43);
                    //--------------------------------------------------
                    Assert.AreEqual(oi.FuelDUTExpensSquare, 392.5);
                    Assert.AreEqual(oi.FuelDUTExpensAvgSquare, 12.45);
                    Assert.AreEqual(oi.FuelDUTExpensWithoutWork, 7.73);
                    Assert.AreEqual(oi.FuelDUTExpensAvgWithoutWork, 318.11);
                    //--------------------------------------------------
                    Assert.AreEqual(oi.FuelDRTExpensSquare, 260.1);
                    Assert.AreEqual(oi.FuelDRTExpensAvgSquare, 8.25);
                    Assert.AreEqual(oi.FuelDRTExpensWithoutWork, 2.81);
                    Assert.AreEqual(oi.FuelDRTExpensAvgWithoutWork, 115.64);
                    //--------------------------------------------------
                    DataTable dt = oi.GetContent();
                    Assert.AreEqual(dt.Rows.Count, 4);
                    dt = oi.GetContentGrouped();
                    Assert.AreEqual(dt.Rows.Count, 2);
                  }
                oi.ChangeStatusEvent -= PrintStstusEvent;
            }
        }
        [Test, Description("�������� ������ ��� ������������� ����������� ������")]
        public void CompareWithTamplate54()
        {
            using (OrderItem oi = new OrderItem(54))
            {
                oi.Remark = "�� �������! ������ ��� �����!";
                Assert.AreEqual(oi.UpdateDoc(), true);
                //Assert.AreEqual(oi.Exist(), true);
                Assert.AreEqual(oi.Date, new DateTime(2010, 10, 14));
                Assert.AreEqual(oi.Mobitel_Id, 114);
                Assert.AreEqual(oi.MobitelName, "���  | TT_B831");
                oi.ChangeStatusEvent += PrintStstusEvent;

                if (oi.ContentAutoCreate(false))
                {
                    Assert.AreEqual(oi.TimeStart, new DateTime(2010, 10, 14, 0, 0, 38));
                    Assert.AreEqual(oi.TimeEnd, new DateTime(2010, 10, 14, 23, 8, 50));
                    Assert.AreEqual(oi.Distance, 41.14);
                    Assert.AreEqual(oi.SpeedAvg, 8.4);
                    Assert.AreEqual(oi.TimeWork, "23:08");
                    Assert.AreEqual(oi.TimeMove, "04:53");
                    Assert.AreEqual(oi.TimeStop, "18:15");
                    Assert.AreEqual(oi.SquareWorkDescript, "0301-06001: 34,5 (25,94%)    ����0: 5,4 (5,93%) ");
                    Assert.AreEqual(oi.PathWithoutWork, 0);
                    //--------------------------------------------------
                    Assert.AreEqual(oi.FuelDUTExpensSquare, 0);
                    Assert.AreEqual(oi.FuelDUTExpensAvgSquare, 0);
                    Assert.AreEqual(oi.FuelDUTExpensWithoutWork, 0);
                    Assert.AreEqual(oi.FuelDUTExpensAvgWithoutWork, 0);
                    //--------------------------------------------------
                    Assert.AreEqual(oi.FuelDRTExpensSquare, 0);
                    Assert.AreEqual(oi.FuelDRTExpensAvgSquare, 0);
                    Assert.AreEqual(oi.FuelDRTExpensWithoutWork, 0);
                    Assert.AreEqual(oi.FuelDRTExpensAvgWithoutWork, 0);
                    //--------------------------------------------------
                    DataTable dt = oi.GetContent();
                    Assert.AreEqual(dt.Rows.Count, 3);
                    dt = oi.GetContentGrouped();
                    Assert.AreEqual(dt.Rows.Count, 2);
                    dt = oi.GetControlData((int)Consts.TypeControlObject.Field);
                    Assert.AreEqual(dt.Rows.Count, 3);
                    dt = oi.GetControlData((int)Consts.TypeControlObject.Driver);
                    Assert.AreEqual(dt.Rows.Count, 1);
                    dt = oi.GetControlData((int)Consts.TypeControlObject.Agregat);
                    Assert.AreEqual(dt.Rows.Count, 1);
                    dt = oi.GetFuelDRT() ;
                    Assert.AreEqual(dt.Rows.Count, 3);
                    dt = oi.GetFuelDUT();
                    Assert.AreEqual(dt.Rows.Count, 3);
                }
                oi.ChangeStatusEvent -= PrintStstusEvent;
            }
        }
        public void PrintStstusEvent(string sMes)
        {
            Debug.Print(sMes);
        }
    }
}
