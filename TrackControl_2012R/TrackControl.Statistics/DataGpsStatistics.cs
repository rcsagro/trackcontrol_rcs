using System;

namespace TrackControl.Statistics
{
  /// <summary>
  /// ���������� ������� ������ DataGps.
  /// </summary>
  public class DataGpsStatistics
  {
    private Int32 pointCount;
    /// <summary>
    /// ����� ���-�� ����� GPS � �������.
    /// </summary>
    public int PointCount
    {
      get { return pointCount; }
      set { pointCount = value; }
    }

    private Int32 notValidPointCount;
    /// <summary>
    /// ���-�� ���������� ����� GPS � �������.
    /// </summary>
    public Int32 NotValidPointCount
    {
      get { return notValidPointCount; }
      set { notValidPointCount = value; }
    }

    private Int32 missedPointCount;
    /// <summary>
    /// ���-�� ����������� ����� GPS � �������.
    /// </summary>
    public Int32 MissedPointCount
    {
      get { return missedPointCount; }
      set { missedPointCount = value; }
    }

    /// <summary>
    /// �����������.
    /// </summary>
    public DataGpsStatistics()
    {
      PointCount = 0;
      NotValidPointCount = 0;
      MissedPointCount = 0;
    }

    /// <summary>
    /// �����������.
    /// </summary>
    /// <param name="pointCount">����� ���-�� ����� GPS � �������.</param>
    /// <param name="notValidPointCount">���-�� ���������� ����� GPS � �������.</param>
    /// <param name="missedPointCount">���-�� ����������� ����� GPS � �������.</param>
    public DataGpsStatistics(Int32 pointCount, Int32 notValidPointCount, Int32 missedPointCount)
    {
      PointCount = pointCount;
      NotValidPointCount = notValidPointCount;
      MissedPointCount = missedPointCount;
    }
  }
}
