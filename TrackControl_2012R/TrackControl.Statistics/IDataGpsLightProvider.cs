using System;
using System.Collections.Generic;

namespace TrackControl.Statistics
{
  public interface IDataGpsLightProvider
  {
    IList<DataGpsLight> GetValidDataGpsList(int mobitelId, DateTime beginTime, DateTime endTime);
  }
}
