using System;
using TrackControl.General;

namespace TrackControl.Statistics
{
  /// <summary>
  /// ����������� ������ �������� DataGps.
  /// ������������ � �������������� �������.
  /// </summary>
  public class DataGpsLight : Entity
  {
    private int logId;
    /// <summary>
    /// ������������� ������ ���������.
    /// </summary>
    public int LogId
    {
      get { return logId; }
      set { logId = value; }
    }

    private long srvPacketId;
    /// <summary>
    /// ������������� ���������� ������.
    /// </summary>
    public long SrvPacketId
    {
      get { return srvPacketId; }
      set { srvPacketId = value; }
    }

    private DateTime time;
    /// <summary>
    /// ����-�����.
    /// </summary>
    public DateTime Time
    {
      get { return time; }
      set { time = value; }
    }

    private byte valid;
    /// <summary>
    /// ����������.
    /// </summary>
    public byte Valid
    {
      get { return valid; }
      set { valid = value; }
    }

    /// <summary>
    /// �����������.
    /// </summary>
    public DataGpsLight()
    {
    }

    /// <summary>
    /// �����������.
    /// </summary>
    /// <param name="logId">������������� ������ ���������.</param>
    /// <param name="srvPacketId">������������� ���������� ������.</param>
    /// <param name="time">����-�����.</param>
    /// <param name="valid">����������.</param>
    public DataGpsLight(int dataGpsId, int logId, long srvPacketId, DateTime time, byte valid)
    {
      Id = dataGpsId;
      LogId = logId;
      SrvPacketId = srvPacketId;
      Time = time;
      Valid = valid;
    }
  }
}
