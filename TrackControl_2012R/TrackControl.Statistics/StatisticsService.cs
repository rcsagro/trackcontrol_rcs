using System;
using System.Collections.Generic;

namespace TrackControl.Statistics
{
  /// <summary>
  /// ������ ���������� ������� ������ DataGps.
  /// </summary>
  public static class StatisticsService
  {
    /// <summary>
    /// 
    /// </summary>
    /// <param name="mobitelId">�������������� ���������.</param>
    /// <param name="beginTime">��������� ����� �������</param>
    /// <param name="endTime"></param>
    /// <returns></returns>
    public static DataGpsStatistics GetStatistics(int mobitelId,
      DateTime beginTime, DateTime endTime, IDataGpsLightProvider provider)
    {
      if (beginTime >= endTime)
      {
        throw new ArgumentException(
          "��� ������� ������ ��������� ���� ������ ���� ������ ��������.");
      }

      DataGpsStatistics stat = new DataGpsStatistics();
      IList<DataGpsLight> dataGps = provider.GetValidDataGpsList(mobitelId, beginTime, endTime);

      return stat;
    }
  }
}
