using System;
using System.Collections.Generic;
using TrackControl.General;
using TrackControl.Vehicles;

namespace TrackControl.Statistics
{
  /// <summary>
  /// ���������� "������" ����� - ������������ ��������� "�������".
  /// </summary>
  public static class TrackRepairService
  {
    /// <summary>
    /// �������� ����� �� ��������� "�������", ��������� ��������� ����� � ������
    /// ������������� � �����������.
    /// ���������� ������ ��� ��������� ����� �� ��������������, � ���������.
    /// </summary>
    /// <param name="track">���� - ��������� DataGps.</param>
    public static void CheckTrack(IList<GpsData> track)
    {
      if (track.Count == 0)
      {
        return;
      }

      track[0].Dist = 0;

      if (track.Count == 1)
      {
        return;
      }

      if (track.Count == 2)
      {
        CheckTwoPoinsTrack(track);
        return;
      }

      // ������ �����, ����� ���������� ��������
      CheckTrackExceptLastSegment(track);
      // ������� ��������� ������� �����
      CheckLastSegment(track);
    }

    /// <summary>
    /// �������� ���������� �������, ��������� ������ �� ���� �����. 
    /// </summary>
    /// <param name="dataGpsList">��������� DataGps.</param>
    private static void CheckTwoPoinsTrack(IList<GpsData> dataGpsList)
    {
      GpsData first = dataGpsList[0];
      GpsData second = dataGpsList[1];
      first.Dist = 0;
      second.Dist = �GeoDistance.GetDistance(first.LatLng, second.LatLng);

      if (CurrentRowNotValid(first, second))
      {
        // ������� ��� �����, ������ ��� �� �����, ����� �� ��� ����������.
        dataGpsList.Clear();
      }
    }

    /// <summary>
    /// �������� ���������� � ���������, � ������ �������������,
    /// ������� ����� �� ��������� � ����������.
    /// ��������� ������� �� �����������.
    /// </summary>
    /// <param name="dataGpsList">��������� DataGps.</param>
    private static void CheckTrackExceptLastSegment(IList<GpsData> dataGpsList)
    {
      GpsData previous, current, next;
      bool deleteFirstRow = false;

      for (int i = 1; i < dataGpsList.Count - 1; i++)
      {
        previous = dataGpsList[i - 1];
        current = dataGpsList[i];
        next = dataGpsList[i + 1];

        current.Dist = �GeoDistance.GetDistance(previous.LatLng, current.LatLng);

        if (CurrentRowNotValid(previous, current))
        {
          if ((i == 1) && (!CurrentRowNotValid(current, next)))
          {
            // ������ ����� ���������� ��� ���������� �� ��������� � ������, 
            // � ������ �������� �� ��������� �� ������ - ������ ���������� �������� ������ �����.
            // ������ ����� ������, �.�. �� ����� ��� ��������� ����������.
            deleteFirstRow = true;
            current.Dist = 0;
          }
          else
          {
            if ((current.Speed == 0.0) && (current.LogId - previous.LogId == 1))
            {
              // � ����� �� ����������, � ���������� ����� ���, ���������� ��� � ���������� �����
              current.LatLng = previous.LatLng;
              current.Dist = 0;
            }
            else
            {
              // ���������� ���������
              current.LatLng = new PointLatLng(
                (previous.LatLng.Lat + next.LatLng.Lat) / 2,
                (previous.LatLng.Lng + next.LatLng.Lng) / 2);

              current.Dist = �GeoDistance.GetDistance(previous.LatLng, current.LatLng);
            }
          }
        }
      }

      if (deleteFirstRow)
      {
        dataGpsList.RemoveAt(0);
      }
    }

    /// <summary>
    /// �������� ���������� ���������� �������� �����. 
    /// </summary>
    /// <param name="dataGpsList">��������� DataGps.</param>
    private static void CheckLastSegment(IList<GpsData> dataGpsList)
    {
      GpsData previous = dataGpsList[dataGpsList.Count - 2];
      GpsData current = dataGpsList[dataGpsList.Count - 1];

      current.Dist = �GeoDistance.GetDistance(previous.LatLng, current.LatLng);

      if (CurrentRowNotValid(previous, current))
      {
        if ((current.Speed == 0.0) && (current.LogId - previous.LogId == 1))
        {
          // � ����� �� ����������, � ���������� ����� ��� ����� prevRow - lastRow,
          // ������������� ���������� ��� � ���������� �����
          current.LatLng = previous.LatLng;
          current.Dist = 0;
        }
        else
        {
          // ��������� ����� ������, �.�. �� ����� ��� ��������� ����������.
          dataGpsList.RemoveAt(dataGpsList.Count - 1);
        }
      }
    }

    /// <summary>
    /// �������� ����, ��� ������� ����� ����������, ��������� ��������
    /// ���������� ��������� �������� ������ 200 ��/�. 
    /// </summary>
    /// <param name="previous">���������� ������ DataGps.</param>
    /// <param name="current">������� ������ DataGps.</param>
    /// <returns>true - ���������� ��������.</returns>
    private static bool CurrentRowNotValid(GpsData previous, GpsData current)
    {
      if (current.Dist == 0.0)
      {
        return false;
      }

      TimeSpan dT = current.Time - previous.Time;
      return dT.TotalSeconds == 0 ?
        current.Dist >= .1 :
        current.Dist * 1000 / dT.TotalSeconds > 56; // ~200 ��/�
    }
  }
}
