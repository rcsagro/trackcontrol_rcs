using System;
using System.Collections.Generic;
using TrackControl.Vehicles;

namespace TrackControl.Statistics
{
  public static class DataGpsService
  {
    /// <summary>
    /// ������� �������� ������ ��������� �� ��������
    /// ���������� �������.
    /// </summary>
    /// <exception cref="ArgumentException">
    /// ��� ������� ������ ��������� ���� ������ ���� ������ ��������.</exception>
    /// <param name="mobitelId">������������� ���������.</param>
    /// <param name="beginTime">��������� �����.</param>
    /// <param name="endTime">�������� �����.</param>
    /// <returns>������ DataGps.</returns>
    public static IList<GpsData> GetValidDataGps(Vehicle vehicle,
      DateTime beginTime, DateTime endTime, IGpsDataProvider provider)
    {
      if (beginTime >= endTime)
      {
        throw new ArgumentException(
          "��� ������� ������ ��������� ���� ������ ���� ������ ��������.");
      }

      IList<GpsData> result = provider.GetValidDataForPeriod(vehicle, beginTime, endTime);

      //IList<GpsData> result = provider.GetValidDataForPeriod(vehicle.MobitelId, beginTime, endTime);

      //TrackRepairService.CheckTrack(result);
      CalcAcceleration(result);
      return result;
    }

    /// <summary>
    /// ������ ��������� � ������ �����.
    /// </summary>
    /// <param name="track">���� - ��������� DataGps.</param>
    private static void CalcAcceleration(IList<GpsData> track)
    {
      if (track.Count == 0)
      {
        return;
      }

      GpsData curDataGps;
      track[0].Accel = 0;

      for (int i = 1; i < track.Count; i++)
      {
        curDataGps = track[i];
        if ((curDataGps.Events & 0x800000) != 0)
        {
          double acc = 0;
          acc = curDataGps.Altitude > 127 ? curDataGps.Altitude - 255 : curDataGps.Altitude;
          curDataGps.Accel = acc * 10 / 36;
        }
        else
        {
          GpsData prevDataGps = track[i - 1];
          TimeSpan dT = curDataGps.Time - prevDataGps.Time;
          double dV = curDataGps.Speed - prevDataGps.Speed;
          dV = dV * 1000 / 3600;
          curDataGps.Accel = dT.TotalSeconds > 0 ? dV / dT.TotalSeconds : 0;
        }
      }
    }
  }
}
