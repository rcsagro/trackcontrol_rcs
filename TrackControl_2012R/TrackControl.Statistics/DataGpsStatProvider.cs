//using System;
//using System.Collections.Generic;
//using MySql.Data.MySqlClient;
//using TrackControl.General.Model.Statistics;

//namespace TrackControl.Statistics
//{
//  /// <summary>
//  /// ������� ������ �� ������� DataGps ��� ����������.
//  /// </summary>
//  public class DataGpsStatProvider : IDataGpsLightProvider
//  {
//    /// <summary>
//    /// ������� �� �� �������� ������ ��������� �� ��������
//    /// ��������� ��������.
//    /// </summary>
//    /// <param name="mobitelId">������������� ���������.</param>
//    /// <param name="beginTime">��������� �����, �������� � �������.</param>
//    /// <param name="endTime">�������� �����, �������� � �������.</param>
//    /// <returns>������ DataGpsLight c ����������� ���������� 
//    /// MobitelID, LogID, Time, SrvPacketID.</returns>
//    internal List<DataGpsLight> GetValidDataGpsList(int mobitelId,
//      DateTime beginTime, DateTime endTime)
//    {
//      const string SELECT_VALID_DATA =
//      @"SELECT DataGps_ID, LogID, FROM_UNIXTIME(unixtime) AS unixtime, SrvPacketID
//        FROM DataGps
//        WHERE (Mobitel_Id = @MobitelId) AND
//          (UnixTime BETWEEN UNIX_TIMESTAMP(@BeginTime) AND UNIX_TIMESTAMP(@EndTime)) AND
//          (Valid = 1)
//        ORDER BY unixtime";

//      List<DataGpsLight> result = new List<DataGpsLight>();

//      using (MySqlCommand cmd = new MySqlCommand(SELECT_VALID_DATA))
//      {
//        cmd.CommandTimeout = 300;
//        cmd.Parameters.AddWithValue("@MobitelId", mobitelId);
//        cmd.Parameters.AddWithValue("@BeginTime", beginTime);
//        cmd.Parameters.AddWithValue("@EndTime", endTime);

//        using (MySqlConnection con = new MySqlConnection(MySqlDAL.CS))
//        {
//          cmd.Connection = con;
//          cmd.Connection.Open();

//          using (MySqlDataReader reader = cmd.ExecuteReader())
//          {
//            while (reader.Read())
//            {
//              result.Add(new DataGpsLight(
//                reader.GetInt64("DataGps_ID"),
//                reader.GetInt32("LogID"),
//                reader.GetInt64("SrvPacketID"),
//                reader.GetDateTime("unixtime"),
//                1));
//            }
//          }
//        }
//      }
//      return result;
//    }

//    /// <summary>
//    /// ������� �� �� ������ ��������� �� ��������� ��������� LogId
//    /// � �������������� ��������� �������� SrvPacketID.
//    /// </summary>
//    /// <param name="mobitelId">������������� ���������.</param>
//    /// <param name="beginLogId">��������� LogId, ������� � �������.</param>
//    /// <param name="endLogId">�������� LogId, ������� � �������.</param>
//    /// <param name="beginSrvPacketID">��������� SrvPacketID, ������� � �������.</param>
//    /// <param name="endSrvPacketID">�������� SrvPacketID, ������� � �������.</param> 
//    /// <returns>������ DataGpsLight c ����������� ���������� 
//    /// MobitelID, LogID, Time, SrvPacketID.</returns>
//    internal List<DataGpsLight> GetDataGpsList(int mobitelId,
//      int beginLogId, int endLogId, long beginSrvPacketID, long endSrvPacketID)
//    {
//      const string SELECT_VALID_DATA =
//      @"SELECT DataGps_ID, LogID, FROM_UNIXTIME(unixtime) AS unixtime, SrvPacketID, Valid
//        FROM DataGps
//        WHERE (Mobitel_Id = @MobitelId) AND
//          (LogID BETWEEN @BeginLogId AND @EndLogId) AND
//          (SrvPacketID BETWEEN @BeginSrvPacketID AND @EndSrvPacketID)";

//      List<DataGpsLight> result = new List<DataGpsLight>();

//      using (MySqlCommand cmd = new MySqlCommand(SELECT_VALID_DATA))
//      {
//        cmd.CommandTimeout = 300;
//        cmd.Parameters.AddWithValue("@MobitelId", mobitelId);
//        cmd.Parameters.AddWithValue("@BeginLogId", beginLogId);
//        cmd.Parameters.AddWithValue("@EndLogId", endLogId);
//        cmd.Parameters.AddWithValue("@BeginSrvPacketID", beginSrvPacketID);
//        cmd.Parameters.AddWithValue("@EndSrvPacketID", endSrvPacketID);

//        using (MySqlConnection con = new MySqlConnection(MySqlDAL.CS))
//        {
//          cmd.Connection = con;
//          cmd.Connection.Open();

//          using (MySqlDataReader reader = cmd.ExecuteReader())
//          {
//            while (reader.Read())
//            {
//              result.Add(new DataGpsLight(
//                reader.GetInt64("DataGps_ID"),
//                reader.GetInt32("LogID"),
//                reader.GetInt64("SrvPacketID"),
//                reader.GetDateTime("unixtime"),
//                reader.GetByte("Valid")));
//            }
//          }
//        }
//      }
//      return result;
//    }

//    /// <summary>
//    /// ����� ��������� �������� ����� ��������� ���������, ������������� 
//    /// "�����" �������� �������.
//    /// </summary>
//    /// <param name="mobitelId">������������� ���������.</param>
//    /// <param name="currentTime">������� �����, �� �������� � �������.</param>
//    /// <returns>DataGpsLight.</returns>
//    internal DataGpsLight GetNearestValidDataGpsBefore(int mobitelId, DateTime currentTime)
//    {
//      const string SELECT_VALID_DATA =
//      @"SELECT DataGps_ID, LogID, FROM_UNIXTIME(unixtime) AS unixtime, SrvPacketID
//        FROM DataGps
//        WHERE (Mobitel_Id = @MobitelId) AND
//          (UnixTime < UNIX_TIMESTAMP(@CurrentTime)) AND
//          (Valid = 1)
//        ORDER BY unixtime DESC
//        LIMIT 1";

//      DataGpsLight result = null;

//      using (MySqlCommand cmd = new MySqlCommand(SELECT_VALID_DATA))
//      {
//        cmd.CommandTimeout = 300;
//        cmd.Parameters.AddWithValue("@MobitelId", mobitelId);
//        cmd.Parameters.AddWithValue("@CurrentTime", currentTime);

//        using (MySqlConnection con = new MySqlConnection(MySqlDAL.CS))
//        {
//          cmd.Connection = con;
//          cmd.Connection.Open();

//          using (MySqlDataReader reader = cmd.ExecuteReader())
//          {
//            if (reader.Read())
//            {
//              result = new DataGpsLight(
//                reader.GetInt64("DataGps_ID"),
//                reader.GetInt32("LogID"),
//                reader.GetInt64("SrvPacketID"),
//                reader.GetDateTime("unixtime"),
//                1);
//            }
//          }
//        }
//      }
//      return result;
//    }

//    /// <summary>
//    /// ����� ��������� �������� ����� ��������� ���������, ������������� 
//    /// "������" �������� �������.
//    /// </summary>
//    /// <param name="mobitelId">������������� ���������.</param>
//    /// <param name="currentTime">������� �����, �� �������� � �������.</param>
//    /// <returns>DataGpsLight.</returns>
//    internal DataGpsLight GetNearestValidDataGpsAfter(int mobitelId, DateTime currentTime)
//    {
//      const string SELECT_VALID_DATA =
//      @"SELECT DataGps_ID, LogID, FROM_UNIXTIME(unixtime) AS unixtime, SrvPacketID
//        FROM DataGps
//        WHERE (Mobitel_Id = @MobitelId) AND
//          (UnixTime > UNIX_TIMESTAMP(@CurrentTime)) AND
//          (Valid = 1)
//        ORDER BY unixtime
//        LIMIT 1";

//      DataGpsLight result = null;

//      using (MySqlCommand cmd = new MySqlCommand(SELECT_VALID_DATA))
//      {
//        cmd.CommandTimeout = 300;
//        cmd.Parameters.AddWithValue("@MobitelId", mobitelId);
//        cmd.Parameters.AddWithValue("@CurrentTime", currentTime);

//        using (MySqlConnection con = new MySqlConnection(MySqlDAL.CS))
//        {
//          cmd.Connection = con;
//          cmd.Connection.Open();

//          using (MySqlDataReader reader = cmd.ExecuteReader())
//          {
//            if (reader.Read())
//            {
//              result = new DataGpsLight(
//                reader.GetInt64("DataGps_ID"),
//                reader.GetInt32("LogID"),
//                reader.GetInt64("SrvPacketID"),
//                reader.GetDateTime("unixtime"),
//                1);
//            }
//          }
//        }
//      }
//      return result;
//    }
//  }
//}
